package com.swiggy.api.sf.checkout.dp;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.EDVOConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.Addon;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CartBuilder;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import com.swiggy.api.sf.checkout.pojo.Variant;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;
import com.swiggy.api.sf.rng.pojo.RestaurantList;

import framework.gameofthrones.Tyrion.JsonHelper;


public class CheckoutDP {
    CheckoutHelper checkoutHelper= new CheckoutHelper();

    RngHelper rngHelper = new RngHelper();
    HashMap<Integer, HashMap<String, String>> cartMenu;
    static int order_id = -1;

    @BeforeClass
    public void GenertaeRandomOrderID() {
        UUID orderid = UUID.randomUUID();
        order_id = Math.abs(orderid.hashCode());
       //cartMenu =checkoutHelper.getCartMenuAddonMapOfRes("223");
    }


    @DataProvider(name = "inventoryExpire")
    public static Object[][] inventoryExpire() {

        return new Object[][]{
                {"401", "1", "9415", "String.valueOf(order_id)"}
        };
    }


    @DataProvider(name = "getinventory")
    public static Object[][] getInventory() {

        return new Object[][]{
                {"401"}
        };
    }


    @DataProvider(name = "getInventoryLock")
    public static Object[][] getInventoryLock() {

        return new Object[][]{
                {"401", "1", "9415", String.valueOf(order_id)}
        };
    }

    @DataProvider(name = "getInventoryLockSameOrderId")
    public static Object[][] getInventoryLockSameOrderId() {

        return new Object[][]{
                {"401", "1", "9415", String.valueOf(order_id)}
        };
    }


    @DataProvider(name = "confirmInventoryLock")
    public static Object[][] confirmInventoryLock() {

        return new Object[][]{
                {"401", "1", "9415", String.valueOf(order_id)}
        };
    }

    @DataProvider(name = "confirmInventoryLockSameOrderId")
    public static Object[][] confirmInventoryLockSameOrderId() {

        return new Object[][]{
                {"401", "1", "9415", String.valueOf(order_id)}
        };
    }

    @DataProvider(name = "removeInventoryLock")
    public static Object[][] removeInventoryLock() {

        return new Object[][]{
                {"401", "1", "9415", String.valueOf(order_id)}
        };
    }
    

    @DataProvider(name = "cancelInventory")
    public static Object[][] cancelInventory() {

        return new Object[][]{
                {"401", "1", "9415", String.valueOf(order_id)}
        };
    }


    @DataProvider(name = "menu")
    public static Object[][] menu() {

        return new Object[][]{
                {"223"}
        };
    }

    @DataProvider(name = "login")
    public static Object[][] login() {

        return new Object[][]{
                {"test@1234", "9008480480"}
        };
    }

    @DataProvider(name = "tag")
    public static Object[][] tag() {

        return new Object[][]{
                {"Pizza", "chinese"}
        };
    }


    @DataProvider(name = "cartCreationV2")
    public static Object[][] cartCreationV2() {
        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9279", "77.6271", "1"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "3"}
        };
    }

    @DataProvider(name = "cartCreationCheck")
    public static Object[][] cartCreationCheck() {
        return new Object[][]{
                { CheckoutConstants.mobile1, CheckoutConstants.password1,"12.9279", "77.6271", "1"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "2"}
        };
    }

    @DataProvider(name = "cartCreationClosed")
    public static Object[][] cartCreationClosed() {
        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9279", "77.6271", "1", CheckoutConstants.restClosed}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "3", CheckoutConstants.restClosed}
        };
    }

    @DataProvider(name = "cartDeletion")
    public static Object[][] cartDeletion() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.88334", "77.59674", "1", CheckoutConstants.deleteCartMessage}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "2", CheckoutConstants.deleteCartMessage}
        };
    }


    @DataProvider(name = "cartCreationAddon")
    public static Object[][] cartCreationAddon() {
        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, CheckoutConstants.testLat, CheckoutConstants.testLng, "1","223"},
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", "223"}};
    }

    @DataProvider(name = "cartCreationDeleteAddon")
    public static Object[][] cartCreationDeleteAddon() {
        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", CheckoutConstants.deleteCartMessage}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "2", CheckoutConstants.deleteCartMessage}};
    }

    @DataProvider(name = "cartCreationWithCoupon")
    public static Object[][] cartCreationWithCoupon() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", "RKO"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "2", "RKO"}
        };
    }

    @DataProvider(name = "deleteCouponOnCart")
    public static Object[][] deleteCouponOnCart() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.88334", "77.59674", "1", "GRUBFEST75"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "2", "GRUBFEST75"}
        };
    }

    @DataProvider(name = "addonWithCoupon")
    public static Object[][] addonWithCoupon() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", "TRYCART"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "3", "TRYCART"}
        };
    }

    @DataProvider(name = "addonWithDeleteCoupon")
    public static Object[][] addonWithDeleteCoupon() {

        return new Object[][]{

                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", "TRYCART"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "2", "TRYCART"}
        };
    }

    @DataProvider(name = "negativeQuantity")
    public static Object[][] negativeQuantity() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", "273", "0", CheckoutConstants.status, CheckoutConstants.statusMessage},
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "2", "273", "-1", CheckoutConstants.status, CheckoutConstants.statusMessage},
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", "273", "1.25", CheckoutConstants.status, CheckoutConstants.statusMessage},
                };
    }

    @DataProvider(name = "createCartTrade")
    public static Object[][] createCartTrade() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "1"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "2"}

        };
    }

    @DataProvider(name = "createCartFlatTrade")
    public static Object[][] createCartFlatTrade() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "1"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "2"}

        };
    }

    @DataProvider(name = "tradeDiscountFlatAddon")
    public static Object[][] tradeDiscountFlatAddon() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "1"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "2"}

        };
    }

    @DataProvider(name = "tradeDiscountFreeDel")
    public static Object[][] tradeDiscountFreeDel() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "1"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "2"}};
    }

    @DataProvider(name = "createCartTradeAndCoupon")
    public static Object[][] createCartTradeAndCoupon() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", "INT"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "4", "INT"}

        };
    }

    @DataProvider(name = "createCartTradeAndCouponAndDelete")
    public static Object[][] createCartTradeAndCouponAndDelete() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", "INT", CheckoutConstants.deleteCartMessage}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "2", "INT", CheckoutConstants.deleteCartMessage}

        };
    }

    @DataProvider(name = "tradeDiscountPercentAddon")
    public static Object[][] tradeDiscountPercentAddon() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "1"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "2"}

        };
    }

    @DataProvider(name = "createCartSchema")
    public static Object[][] createCartSchema() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101","1", "223"},
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101","2", "223"}
        };
    }


    @DataProvider(name = "minimalCartV2")
    public static Object[][] minimalCartV2() {

        return new Object[][]{

                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1"}
        };
    }

    @DataProvider(name = "cartTestSet1")
    public static Object[][] cartTestSet1() {

        return new Object[][]{

                {"12.9166", "77.6101"}
        };
    }

    @DataProvider(name = "cartCheckTotalV2")
    public static Object[][] checkTotal() {

        return new Object[][]{

                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1"}
        };
    }


    @DataProvider(name = "applyCouponSchema")
    public static Object[][] applyCouponSchema() {

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "1", "273", "TRYCART"}, {CheckoutConstants.mobile1, CheckoutConstants.password1, "12.9166", "77.6101", "4", "273", "TRYCART"}
        };
    }

    @DataProvider(name = "updateInventory")
    public static Object[][] updateInventory() {

        return new Object[][]{
                {"877269", "8", "1", "0", "160"}
        };
    }

    @DataProvider(name = "orderPlace")
    public static Object[][] orderPlace() {

        return new Object[][]{
                {"781936", "Cash", "Test User"}
        };
    }

    @DataProvider(name = "app")
    public static Object[][] app() {

        return new Object[][]{
                {"chaseit", "9008480480"}
        };
    }

    @DataProvider(name = "placeOrder")
    public static Object[][] placeOrder() {
        return new Object[][]{
                {CheckoutConstants.mobile, CheckoutConstants.password, "1104748", "2", "6495"}
        };
    }

    @DataProvider(name = "createCartVariant")
    public Object[][] createCartVariant() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        CheckoutHelper helper = new CheckoutHelper();
        String rest = helper.restId(CheckoutConstants.lat, CheckoutConstants.lng);
        List<String> menuList = helper.pickNRandom(rest);
        List<Addon> addon = new ArrayList<>();
        //addon.add(new Addon("213386","36249","Extra Veggies",20));

        List<Variant> variant = new ArrayList<>();
        //variant.add(new Variant(931,310421,"Fresh Pan Pizza",20.0));
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, menuList.get(0), 2));
        CreateMenuEntry createMenuEntry = new CartBuilder()
                .cart(cart)
                .restaurant(Integer.parseInt(rest))
                .build();

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, jsonHelper.getObjectToJSON(createMenuEntry)}
        };
    }

    @DataProvider(name = "createCartAddonVariant")
    public Object[][] createCartAddonVariant() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        CheckoutHelper helper = new CheckoutHelper();
        RngHelper rngHelper = new RngHelper();
        List<Map<String, Object>> menuList = helper.Addons(rngHelper.getRestaurantList().get(0));
        List<Addon> addon = new ArrayList<>();
        String p = String.valueOf(Double.parseDouble(menuList.get(0).get("price").toString()) * 100).replace(".", "");
        String p1 = p.substring(0, p.length() - 1);
        addon.add(new Addon(menuList.get(0).get("choiceid").toString(), menuList.get(0).get("groupid").toString(), menuList.get(0).get("name").toString(), Integer.parseInt(p1)));
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, menuList.get(0).get("menuid").toString(), 2));
        CreateMenuEntry createMenuEntry = new CartBuilder()
                .cart(cart)
                .restaurant(Integer.parseInt(rngHelper.getRestaurantList().get(0)))
                .build();
        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, jsonHelper.getObjectToJSON(createMenuEntry)}
        };
    }

    @DataProvider(name = "createCartAddonVariants")
    public Object[][] createCartAddonVariants() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        CheckoutHelper helper = new CheckoutHelper();
        RngHelper rngHelper = new RngHelper();
        List<Map<String, Object>> menuList = helper.Addons(rngHelper.getRestaurantList().get(0));
        List<Addon> addon = new ArrayList<>();
        String p = String.valueOf(Double.parseDouble(menuList.get(0).get("price").toString()) * 100).replace(".", "");
        String p1 = p.substring(0, p.length() - 1);
        addon.add(new Addon(menuList.get(0).get("choiceid").toString(), menuList.get(0).get("groupid").toString(), menuList.get(0).get("name").toString(), Integer.parseInt(p1)));
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, menuList.get(0).get("menuid").toString(), 2));
        CreateMenuEntry createMenuEntry = new CartBuilder()
                .cart(cart)
                .restaurant(Integer.parseInt(rngHelper.getRestaurantList().get(0)))
                .build();

        return new Object[][]{
                {CheckoutConstants.mobile1, CheckoutConstants.password1, jsonHelper.getObjectToJSON(createMenuEntry)}
        };
    }

    @DataProvider(name = "test")
    public Object[][] test() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        CheckoutHelper helper = new CheckoutHelper();
        List<String> list = helper.pickNRandom("229");
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, list.get(0), 1));
        cart.add(new Cart(addon, variant, list.get(1), 1));
        cart.add(new Cart(addon, variant, list.get(2), 1));
        CreateMenuEntry login = new CreateOrderBuilder()
                .buildLogin();
        CreateMenuEntry createCart = new CreateOrderBuilder()
                .cart(cart)
                .restaurant(229)
                .buildCart();
        CreateMenuEntry orderPlace = new CreateOrderBuilder()
                .buildOrderPlace();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(login), jsonHelper.getObjectToJSON(createCart), jsonHelper.getObjectToJSON(orderPlace)}
        };
    }



    @DataProvider(name = "userSingleOrder")
    public Object[][] userSingleOrder() throws IOException {
            CheckoutHelper checkoutHelper = new CheckoutHelper();
            List<Cart> cart = new ArrayList<>();
            cart.add(new Cart(null, null, "141634", 1));
            CreateMenuEntry createOrder = new CreateOrderBuilder()
                    .password("rkonowhere")
                    .mobile(7406734416l)
                    .cart(cart)
                    .restaurant(1087)
                    .Address(11131974)
                    .paymentMethod("Cash")
                    .orderComments("Test-Order")
                    .buildAll();
        return new Object[][]{
                {createOrder}
        };
    }
    

    
    @DataProvider(name = "orderplaceRegression")
    public Object[][] orderwithoutTIDTOKEN() throws IOException {
         	HashMap<String, String> hashMap=null;
            CheckoutHelper checkoutHelper = new CheckoutHelper();
            List<Cart> cart = new ArrayList<>();
            cart.add(new Cart(null, null, "141634", 1));
            
            CreateMenuEntry OrderWithoutAuth = new CreateOrderBuilder()
            		.password(CheckoutConstants.password1)
                    .mobile(Long.parseLong(CheckoutConstants.mobile1))
                    .cart(cart)
                    .restaurant(CheckoutConstants.restaurantId)
                    .Address(CheckoutConstants.address_id)
                    .paymentMethod(CheckoutConstants.paymentCod)
                    .orderComments(CheckoutConstants.orderComments)
                    .buildAll();
            
         
            CreateMenuEntry orderwithoutAddressID = new CreateOrderBuilder()
            		.password(CheckoutConstants.password1)
                    .mobile(Long.parseLong(CheckoutConstants.mobile1))
                    .cart(cart)
                    .restaurant(CheckoutConstants.restaurantId)
                    .Address(null)
                    .paymentMethod(CheckoutConstants.paymentCod)
                    .orderComments(CheckoutConstants.orderComments)
                    .buildAll();
            
            CreateMenuEntry orderwithInvalidAddressID = new CreateOrderBuilder()
            		.password(CheckoutConstants.password1)
                    .mobile(Long.parseLong(CheckoutConstants.mobile1))
                    .cart(cart)
                    .restaurant(CheckoutConstants.restaurantId)
                    .Address(CheckoutConstants.invalidAddress)
                    .paymentMethod(CheckoutConstants.paymentCod)
                    .orderComments(CheckoutConstants.orderComments)
                    .buildAll();
            
            CreateMenuEntry orderwithNonServicableAddressID = new CreateOrderBuilder()
            		.password(CheckoutConstants.password1)
                    .mobile(Long.parseLong(CheckoutConstants.mobile1))
                    .cart(cart)
                    .restaurant(CheckoutConstants.restaurantId)
                    .Address(CheckoutConstants.unServicableAddress)
                    .paymentMethod(CheckoutConstants.paymentCod)
                    .orderComments(CheckoutConstants.orderComments)
                    .buildAll();
            
            CreateMenuEntry orderwithPaymentNull = new CreateOrderBuilder()
            		.password(CheckoutConstants.password1)
                    .mobile(Long.parseLong(CheckoutConstants.mobile1))
                    .cart(cart)
                    .restaurant(CheckoutConstants.restaurantId)
                    .Address(CheckoutConstants.address_id)
                    .paymentMethod(CheckoutConstants.paymentNullCase)
                    .orderComments(CheckoutConstants.orderComments)
                    .buildAll();
            
            CreateMenuEntry InvalidPaymentfield = new CreateOrderBuilder()
            		  .password(CheckoutConstants.password1)
                      .mobile(Long.parseLong(CheckoutConstants.mobile1))
                      .cart(cart)
                      .restaurant(CheckoutConstants.restaurantId)
                      .Address(CheckoutConstants.address_id)
                      .paymentMethod(CheckoutConstants.paymentInvalidCase)
                      .orderComments(CheckoutConstants.orderComments)
                      .buildAll();
            
            
            
            CreateMenuEntry paymentIgnoreCase = new CreateOrderBuilder()
            		   .password(CheckoutConstants.password1)
                       .mobile(Long.parseLong(CheckoutConstants.mobile1))
                       .cart(cart)
                       .restaurant(CheckoutConstants.restaurantId)
                       .Address(CheckoutConstants.address_id)
                       .paymentMethod(CheckoutConstants.paymentCodIgnoreCases)
                       .orderComments(CheckoutConstants.orderComments)
                       .buildAll();
   
        	hashMap = checkoutHelper.createLogin(CheckoutConstants.password1, Long.parseLong(CheckoutConstants.mobile1));
        return new Object[][]{
             
               {OrderWithoutAuth,null,null,999,"Session expired. Please login again."},
              {orderwithoutAddressID,hashMap.get("Tid"), hashMap.get("Token"),1,"AddressId cannot be null"},
                {orderwithInvalidAddressID,hashMap.get("Tid"), hashMap.get("Token"),1,"Address does not belong to user, comparing with cust id."},
                {orderwithNonServicableAddressID,hashMap.get("Tid"), hashMap.get("Token"),1,"Sorry, the address isn't serviceable for this order"},
                {orderwithPaymentNull,hashMap.get("Tid"), hashMap.get("Token"),1,"Invalid payment method - NULL"},
                {InvalidPaymentfield,hashMap.get("Tid"), hashMap.get("Token"),1,"Invalid payment method - CASH-00"},
               {paymentIgnoreCase,hashMap.get("Tid"), hashMap.get("Token"),0,"done successfully"}
                
                                
        };
    }


    @DataProvider(name = "defaultSingleOrder")
    public Object[][] defaultSingleOrder() throws IOException {

        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .Address(782234)
                .build();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name = "MultipleOrder")
    public Object[][] MultipleOrder() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        List<String> list = helper.pickNRandom("300");
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, list.get(0), 1));
        cart.add(new Cart(addon, variant, list.get(1), 1));
        cart.add(new Cart(addon, variant, list.get(2), 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .cart(cart)
                .restaurant(300)
                .Address(782234)
                .build();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name = "MultipleAddonOrder")
    public Object[][] MultipleAddonOrder() throws IOException {
        List<Addon> addon = new ArrayList<>();
        addon.add(new Addon("213386", "36249", "Extra Veggies", 20));
        List<Addon> addons = new ArrayList<>();
        addons.add(new Addon("213389", "36250", "Extra Meat", 20));
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "677793", 1));
        cart.add(new Cart(addons, variant, "677802", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .cart(cart)
                .restaurant(273)
                .Address(782234)
                .build();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name = "MultipleAddonVariantOrder")
    public Object[][] MultipleAddonVariantOrder() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        List<Map<String, Object>> list = helper.Addons("223");
        List<Addon> addon = new ArrayList<>();
        addon.add(new Addon("111154", "19216", "Add-ons", 20));
        List<Variant> variant = new ArrayList<>();
        //Integer variation_id, Integer group_id, String name, Double price
        //variant.add(new Variant(515,241,"Gg1",10.0));
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "545264", 1));
        CreateMenuEntry createCart = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(223)
                .Address(782234)
                .paymentMethod("Cash")
                .orderComments("Test-Order")
                .build();
        return new Object[][]{
                {createCart}
        };
    }

    @DataProvider(name = "outofstock")
    public Object[][] outofstock() throws IOException {
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "545379", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .cart(cart)
                .restaurant(223)
                .build();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name = "DominoOrder")
    public Object[][] DominoOrder() throws IOException {
        List<Addon> addon = new ArrayList<>();
        List<Addon> addons = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "1755280", 1));
        //cart.add(new Cart(addons, variant, "677802",1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .cart(cart)
                .restaurant(8087)
                .build();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name = "POPCart")
    public Object[][] POPCart() throws IOException {

        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(null, null, "1765568", 1));
        //cart.add(new Cart(addons, variant, "677802",1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .cart(cart)
                .restaurant(18990)
                .build();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name = "VariantsDominoOrder")
    public Object[][] VariantsDominoOrder() throws IOException {
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        variant.add(new Variant(491, 229, 10.00));
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "545291", 1));
        //cart.add(new Cart(addons, variant, "677802",1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .cart(cart)
                .restaurant(223)
                .build();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name = "outofstock1")
    public Object[][] outofstock1() throws IOException {

        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(null, null, "545379", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .cart(cart)
                .restaurant(223)
                .build();
        return new Object[][]{
                {createOrder}
        };
    }


    @DataProvider(name = "TDForNormalCart")
    public Object[][] TDForNormalCart() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        JsonHelper jsonHelper = new JsonHelper();
        String rest = helper.restId("12.9278", "77.6264");
        List<String> list = helper.pickNRandom(rest);
        List<RestaurantList> restaurantLists = new ArrayList<>();
        restaurantLists.add(new RestaurantList(rest, "Test", new ArrayList<>()));
        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder()
                .nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 10).toInstant().getEpochSecond() * 1000))
                .campaign_type("Percentage")
                .restaurant_hit("40")
                .enabled(true)
                .swiggy_hit("60")
                .createdBy("Manu")
                .discountLevel("Restaurant")
                .restaurantList(restaurantLists)
                //.slots(slots)
                .ruleDiscount("Percentage", "Restaurant", "10", "10", "2000")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(false)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .restaurantFirstOrder(false)
                .build();

        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, list.get(0), 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt(rest))
                .build();

        List<Addon> addons = new ArrayList<>();
        List<Variant> variants = new ArrayList<>();
        List<Cart> cartForMultipleMenuItems = new ArrayList<>();
        cartForMultipleMenuItems.add(new Cart(addons, variants, list.get(0), 1));
        cartForMultipleMenuItems.add(new Cart(addons, variants, list.get(1), 1));
        CreateMenuEntry createOrder1 = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt(rest))
                .build();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder},
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder1}


        };
    }

    @DataProvider(name = "TDForAddonCart")
    public Object[][] TDForAddonCart() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        JsonHelper jsonHelper = new JsonHelper();
        //List<Map<String, Object>> list=helper.Addons("1725");
        List<RestaurantList> restaurantLists = new ArrayList<>();
        restaurantLists.add(new RestaurantList("223", "Test", new ArrayList<>()));
        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder()
                .nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
                .campaign_type("Percentage")
                .restaurant_hit("40")
                .enabled(true)
                .swiggy_hit("60")
                .createdBy("Manu")
                .discountLevel("Restaurant")
                .restaurantList(restaurantLists)
                //.slots(slots)
                .ruleDiscount("Percentage", "Restaurant", "10", "10", "2000")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(false)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .restaurantFirstOrder(false)
                .build();

        List<Addon> addon = new ArrayList<>();
        addon.add(new Addon("625070", "19212", "Addons one", 20));
        List<Addon> addon1 = new ArrayList<>();
        addon1.add(new Addon("111175", "19222", "Coleslaw", 20));
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "545263", 1));
        cart.add(new Cart(addon1, variant, "545265", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt("223"))
                .build();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder}
        };
    }

    @DataProvider(name = "TDForFlatCart")
    public Object[][] TDForFlatCart() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        JsonHelper jsonHelper = new JsonHelper();
        //String rest=helper.restId("12.9278","77.6264");
        //List<String> list = helper.pickNRandom(rest);

        //List<Map<String, Object>> list=helper.Addons("1725");
        List<RestaurantList> restaurantLists = new ArrayList<>();
        restaurantLists.add(new RestaurantList("6131", "Test", new ArrayList<>()));
        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder()
                .nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
                .campaign_type("Flat")
                .restaurant_hit("40")
                .enabled(true)
                .swiggy_hit("60")
                .createdBy("Manu")
                .discountLevel("Restaurant")
                .restaurantList(restaurantLists)
                //.slots(slots)
                .ruleDiscount("Flat", "Restaurant", "10", "100", "2000")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(false)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .restaurantFirstOrder(false)
                .build();

        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "1007285", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(6131)
                .build();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder}
        };
    }

    @DataProvider(name = "TDForFreeDelCart")
    public Object[][] TDForFreeDelCart() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        JsonHelper jsonHelper = new JsonHelper();
        //String rest=helper.restId("12.9278","77.6264");
        String restID="223";
        List<String> list = helper.pickNRandom(restID);

        //List<Map<String, Object>> list=helper.Addons("1725");
        List<RestaurantList> restaurantLists = new ArrayList<>();
        restaurantLists.add(new RestaurantList(restID, "Test", new ArrayList<>()));
        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder()
                .nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
                .campaign_type("FREE_DELIVERY")
                .restaurant_hit("40")
                .enabled(true)
                .swiggy_hit("60")
                .createdBy("Manu")
                .discountLevel("Restaurant")
                .restaurantList(restaurantLists)
                //.slots(slots)
                .ruleDiscount("FREE_DELIVERY", "Restaurant", "10", "10", "2000")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(false)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .restaurantFirstOrder(false)
                .build();

        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, list.get(0), 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt(restID))
                .build();

        List<Addon> addons = new ArrayList<>();
        List<Variant> variants = new ArrayList<>();
        List<Cart> cart1 = new ArrayList<>();
        cart1.add(new Cart(addons, variants, list.get(1), 1));
        cart1.add(new Cart(addons, variants, list.get(2), 1));
        CreateMenuEntry createOrder1 = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt(restID))
                .build();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder},
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder1}
        };
    }

    @DataProvider(name = "TDForFreeDelAddonCart")
    public Object[][] TDForFreeDelAddonCart() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        JsonHelper jsonHelper = new JsonHelper();
        List<RestaurantList> restaurantLists = new ArrayList<>();
        restaurantLists.add(new RestaurantList("1256", "Test", new ArrayList<>()));
        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder()
                .nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
                .campaign_type("FREE_DELIVERY")
                .restaurant_hit("40")
                .enabled(true)
                .swiggy_hit("60")
                .createdBy("Manu")
                .discountLevel("Restaurant")
                .restaurantList(restaurantLists)
                //.slots(slots)
                .ruleDiscount("FREE_DELIVERY", "Restaurant", "10", "10", "2000")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(false)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .restaurantFirstOrder(false)
                .build();

        List<Addon> addon = new ArrayList<>();
        addon.add(new Addon("86304", "15686", "Mango-Banana Smoothie (Brain Freezer)", 0));
        List<Variant> variants = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variants, "269250", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(1256)
                .build();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder}
        };
    }

    @DataProvider(name = "TDAndCouponOnCart")
    public Object[][] TDAndCouponOnCart() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        JsonHelper jsonHelper = new JsonHelper();
        //List<Map<String, Object>> list=helper.Addons("1725");
        List<RestaurantList> restaurantLists = new ArrayList<>();
        restaurantLists.add(new RestaurantList("223", "Test", new ArrayList<>()));
        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder()
                .nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
                .campaign_type("Percentage")
                .restaurant_hit("40")
                .enabled(true)
                .swiggy_hit("60")
                .createdBy("Manu")
                .discountLevel("Restaurant")
                .restaurantList(restaurantLists)
                //.slots(slots)
                .ruleDiscount("Percentage", "Restaurant", "10", "10", "2000")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(false)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .restaurantFirstOrder(false)
                .build();

        List<Addon> addon = new ArrayList<>();
        addon.add(new Addon("625070", "19212", "Addons one", 20));
        List<Addon> addon1 = new ArrayList<>();
        addon1.add(new Addon("111175", "19222", "Coleslaw", 20));
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "545263", 1));
        cart.add(new Cart(addon1, variant, "545265", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt("223"))
                .build();

        List<Addon> addon2 = new ArrayList<>();
        List<Variant> variant2 = new ArrayList<>();
        List<Cart> cart1 = new ArrayList<>();
        cart1.add(new Cart(addon2, variant2, "545263", 1));
        CreateMenuEntry createOrder1 = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt("223"))
                .build();

        List<Addon> addon3 = new ArrayList<>();
        List<Variant> variant3 = new ArrayList<>();
        List<Cart> cart3 = new ArrayList<>();
        cart3.add(new Cart(addon3, variant3, "545263", 1));
        cart3.add(new Cart(addon3, variant3, "545224", 1));
        CreateMenuEntry createOrder3 = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt("223"))
                .build();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder,"INT"},
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder1,"INT"},
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder3,"INT"}

        };
    }

    @DataProvider(name = "TDForCateogoryCart")
    public Object[][] TDForCateogoryCart() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        JsonHelper jsonHelper = new JsonHelper();
        String rest = helper.restId("12.9278", "77.6264");
        List<String> list = helper.pickNRandom(rest);
        List<RestaurantList> restaurantLists = new ArrayList<>();
        restaurantLists.add(new RestaurantList(rest, "Test", new ArrayList<>()));
        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder()
                .nameSpace("categoryLevel")
                .header("categoryLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
                .campaign_type("Percentage")
                .restaurant_hit("40")
                .enabled(true)
                .swiggy_hit("60")
                .createdBy("Manu")
                .discountLevel("Category")
                .restaurantList(rngHelper.createRandomRestaurantList(rest,true,2, false,2, false,1))
                //.slots(slots)
                .ruleDiscount("Percentage", "Category", "10", "10","2000")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(false)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .restaurantFirstOrder(false)
                .build();

        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, list.get(0), 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt(rest))
                .build();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder}
        };
    }

    @DataProvider(name = "TDAndCouponAndDeleteCart")
    public Object[][] TDAndCouponAndDeleteCart() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        JsonHelper jsonHelper = new JsonHelper();
        //List<Map<String, Object>> list=helper.Addons("1725");
        List<RestaurantList> restaurantLists = new ArrayList<>();
        restaurantLists.add(new RestaurantList("223", "Test", new ArrayList<>()));
        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder()
                .nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
                .campaign_type("Percentage")
                .restaurant_hit("40")
                .enabled(true)
                .swiggy_hit("60")
                .createdBy("Manu")
                .discountLevel("Restaurant")
                .restaurantList(restaurantLists)
                //.slots(slots)
                .ruleDiscount("Percentage", "Restaurant", "10", "10", "2000")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(false)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .restaurantFirstOrder(false)
                .build();

        
        List<Addon> addon = new ArrayList<>();
        addon.add(new Addon("625070", "19212", "Addons one", 20));
        List<Addon> addon1 = new ArrayList<>();
        addon1.add(new Addon("111175", "19222", "Coleslaw", 20));
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "545263", 1));
        cart.add(new Cart(addon1, variant, "545265", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt("223"))
                .build();

        List<Addon> addon2 = new ArrayList<>();
        List<Variant> variant2 = new ArrayList<>();
        List<Cart> cart1 = new ArrayList<>();
        cart1.add(new Cart(addon2, variant2, "545263", 1));
        CreateMenuEntry createOrder1 = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt("223"))
                .build();

        List<Addon> addon3 = new ArrayList<>();
        List<Variant> variant3 = new ArrayList<>();
        List<Cart> cart3 = new ArrayList<>();
        cart3.add(new Cart(addon3, variant3, "545263", 1));
        cart3.add(new Cart(addon3, variant3, "545224", 1));
        CreateMenuEntry createOrder3 = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt("223"))
                .build();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder,"INT", CheckoutConstants.deleteCartMessage},
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder1,"INT",CheckoutConstants.deleteCartMessage},
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder3,"INT",CheckoutConstants.deleteCartMessage}

        };
    }



    /*@DataProvider(name = "TDForFreeCart")
    public Object[][] TDForFreeCart() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        JsonHelper jsonHelper = new JsonHelper();
        List allList = rngHelper.getList(cmsHelper.getRestListTD());

        Iterator it = allList.iterator();
        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder()
                .nameSpace("categoryLevel")
                .header("categoryLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
                .campaign_type("Percentage")
                .restaurant_hit("40")
                .enabled(true)
                .swiggy_hit("60")
                .createdBy("Manu")
                .discountLevel("Category")
                .restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
                *//**.slots(slots)**//*
                .ruleDiscount("Percentage", "Category", "10", "0", "2000")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(false)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .restaurantFirstOrder(false)
                .build();
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, list.get(2), 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(Integer.parseInt(rest))
                .build();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(percetageTradeDiscount), createOrder}
        };
    }*/
    
    @DataProvider(name="updateCafeCartData")
    public static Object[][] updateCafeCartData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {"229",false, true,"cash","TestCafeOrder", 0, "CART_UPDATED_SUCCESSFULLY"}
        };
    }

    @DataProvider(name = "TDAndCouponAndDeleteCat")
    public Object[][] TDAndCouponAndDeleteCat() throws IOException {
        //JsonHelper jsonHelper= new JsonHelper();
        List<Addon> addon2 = new ArrayList<>();
        List<Variant> variant2 = new ArrayList<>();
        List<Cart> cart1 = new ArrayList<>();
        cart1.add(new Cart(addon2, variant2, "545263", 1));
        CreateMenuEntry createOrder1 = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart1)
                .restaurant(Integer.parseInt("223"))
                .paymentMethod("Cash")
                .orderComments("Test-Order")
                .buildAll();
        return new Object[][]{
                { createOrder1}
        };

    }

    
    @DataProvider(name = "TDForFlatCart1")
    public static Object[][] thresholdFee() {
        return new Object[][]{
               
               {"1007107","1","6131"},
        };
    }
}


