package com.swiggy.api.sf.snd.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.EndToEndConstants;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.json.simple.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.util.*;

public class OrderPlace {
    Initialize gameofthrones= new Initialize();
    JsonHelper jsonHelper= new JsonHelper();
    SANDHelper sandHelper= new SANDHelper();
    AddressHelper addressHelper= new AddressHelper();
    Random random= new Random();
    HeaderHelper headerHelper= new HeaderHelper();



    public String createOrder(CreateMenuEntry payload) throws IOException {
        String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMapLogin = createLogin(payload.getpassword(), payload.getmobile());
        HashMap<String, String> cartHashMap = CreateCartAddon(hashMapLogin.get("Tid"), hashMapLogin.get("Token"), cartPayload, payload.getRestaurantId());
        HashMap<String, String> order = orderPlace(hashMapLogin.get("Tid"), hashMapLogin.get("Token"), Integer.parseInt(cartHashMap.get("addressId")), payload.getpayment_cod_method(), payload.getorder_comments());
        return order.get("order_id");
    }

    public String createOrder(String tid, String token , String lat, String lng){
        List<String> restID= Aggregator(new String[]{lat,lng}, tid, token);
        int n= new Random().nextInt(restID.size());
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(null, null, getRestaurantMenu(restID.get(n), tid, token), 1));
        try {
            CreateMenuEntry payload = new CreateOrderBuilder()
                    .cart(cart)
                    .restaurant(Integer.valueOf(restID.get(n)))
                    .paymentMethod("Cash")
                    .orderComments("Test-Order")
                    .buildPayment();
            String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
            HashMap<String, String> cartHashMap = CreateCartAddon(tid, token, cartPayload, payload.getRestaurantId());
            HashMap<String, String> order = orderPlace(tid, token, Integer.parseInt(cartHashMap.get("addressId")), payload.getpayment_cod_method(), payload.getorder_comments());
            Assert.assertNotNull(order.get("order_id"), "Order generation failed");
            return order.get("order_id");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public void createOrderSuper(String tid, String token, String lat, String lng, String plan){
        List<String> restID= Aggregator(new String[]{lat,lng}, tid, token);
        int n= new Random().nextInt(restID.size());
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(null, null, getRestaurantMenu(restID.get(n), tid, token), 1));
        try {
            CreateMenuEntry payload = new CreateOrderBuilder()
                    .cart(cart)
                    .restaurant(Integer.valueOf(restID.get(n)))
                    .paymentMethod("Cash")
                    .orderComments("Test-Order")
                    .buildPayment();
            String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
            HashMap<String, String> cartHashMap = createCartSuper(tid, token, cartPayload, payload.getRestaurantId(),plan);
            HashMap<String, String> order = orderPlace(tid, token, Integer.parseInt(cartHashMap.get("addressId")), payload.getpayment_cod_method(), payload.getorder_comments());
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }

    public HashMap<String, String> createLogin(String password, Long mobile)
    {
        HashMap<String, String> hashMap= new HashMap<>();
        try {
            HashMap<String, String> requestheaders = new HashMap<String, String>(){{contentType(); put("deviceId", "bd6a6df1-a77f-4c69-baa6-5e378dad49a5");}};
            GameOfThronesService service = new GameOfThronesService("sand", "login", gameofthrones);
            String[] payloadParam = {password, String.valueOf(mobile)};
            String resp = new Processor(service, requestheaders, payloadParam).ResponseValidator.GetBodyAsText();
            String stat= JsonPath.read(resp,"$.statusCode").toString().replace("[","").replace("]","");
            String r= JsonPath.read(resp, "$.statusCode").toString().replace("[","").replace("]","");


            //Assert.assertEquals(EndToEndConstants.statusCode,r,"Status code is not 0");
            //System.out.println("----"+processor.ResponseValidator.GetResponseCode());

            Assert.assertEquals(EndToEndConstants.statusCode,r,"Status code is not 0");


           String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
            String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
            hashMap.put("Tid", tid);
            hashMap.put("Token", token);
        }
        catch(Exception e)
        { e.printStackTrace(System.out);
           }
        catch(AssertionError e)
        {
            e.printStackTrace();
           }
        return hashMap;
    }


    public Processor DeleteCart(String tid, String Token) {
        GameOfThronesService service = new GameOfThronesService("checkout", "deletecart", gameofthrones);
       return new Processor(service, headerHelper.requestHeader(tid,Token), null, null);
    }


    public HashMap<String, String> CreateCartAddon(String tid, String token, String cart, Integer rest)
    {
        HashMap<String, String> hashMap= new HashMap<>();
        try {
            HashMap<String, String> requestheaders = requestHeader(tid, token);
            JSONObject jsonHeader = new JSONObject();
            jsonHeader.putAll(requestheaders);
            DeleteCart(tid, token);
            GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2nitems", gameofthrones);
            String[] payloadparams = {cart, String.valueOf(rest)};
            Processor processor = new Processor(service, requestheaders, payloadparams);
            String response = processor.ResponseValidator.GetBodyAsText();
            String statusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
            String msg= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","");

            String lat= JsonPath.read(response,"$.data.restaurant_details.lat").toString().replace("[","").replace("]","");
            String lng= JsonPath.read(response,"$.data.restaurant_details.lng").toString().replace("[","").replace("]","");

            hashMap.put("addressId", checkAddressServicibility(response,tid, token, lat, lng));
        }
        catch(Exception e)
        {
            e.printStackTrace(System.out);
        }
        catch(AssertionError e)
        {e.printStackTrace();
            }
        return hashMap;
    }

    public HashMap<String, String> createCartSuper(String tid, String token, String cart, Integer rest,String plan)
    {
        HashMap<String, String> hashMap= new HashMap<>();
        try {
            HashMap<String, String> requestheaders = requestHeader(tid, token);
            JSONObject jsonHeader = new JSONObject();
            jsonHeader.putAll(requestheaders);
            DeleteCart(tid, token);
            GameOfThronesService service = new GameOfThronesService("checkout", "supersub", gameofthrones);
            String[] payloadparams = {cart, String.valueOf(rest),plan};
            Processor processor = new Processor(service, requestheaders, payloadparams);
            String response = processor.ResponseValidator.GetBodyAsText();
            String lat= JsonPath.read(response,"$.data.restaurant_details.lat").toString().replace("[","").replace("]","");
            String lng= JsonPath.read(response,"$.data.restaurant_details.lng").toString().replace("[","").replace("]","");
            hashMap.put("addressId", checkAddressServicibility(response,tid, token, lat, lng));
        }
        catch(Exception e)
        {
            e.printStackTrace(System.out);
        }
        catch(AssertionError e)
        {e.printStackTrace();
        }
        return hashMap;
    }

    public String checkAddressServicibility(String  cartResponse, String tid, String token, String lat, String lng){
        Object AddressId = JsonPath.read(cartResponse, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String[] AddressIdArray = (AddressId.toString().split(","));
        String deliveryValid = JsonPath.read(cartResponse, "$.data..addresses..delivery_valid").toString().replace("[", "").replace("]", "");
        String[] delivery = deliveryValid.split(",");
        List<String> addressIdList = new ArrayList<>();
        try {
            for (int i = 0; i < delivery.length; i++) {
                if (delivery[i].equals("1")) {
                    addressIdList.add(AddressIdArray[i]);
                } else {
                    String valid = addressHelper.NewAddress(tid, token, "name", "7406734416", "near budhwal", "234", "area", lat, lng, "223", "ggn", "WORK").ResponseValidator.GetBodyAsText();
                    String j = JsonPath.read(valid, "data.address_id").toString().replace("[", "").replace("]", "");
                    addressIdList.add(j);
                    break;
                }
            }
        }
        catch(Exception e)
        {
            String valid=addressHelper.NewAddress(tid,token,"name","7406734416","near budhwal", "234","area",lat,lng,"223","ggn","WORK").ResponseValidator.GetBodyAsText();
            String j= JsonPath.read(valid,"data.address_id").toString().replace("[","").replace("]","");
            addressIdList.add(j);
        }
        System.out.println(addressIdList.get(0));
        return addressIdList.get(0);
    }

    public List<String> aggregator(String[] payload, String tid, String token) {
        List<String> restIds = new ArrayList<>();
        try {
            GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
            Processor processor = new Processor(service, headerHelper.requestHeader(tid, token), payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            String statusCode= JsonPath.read(response, "$.statusCode").toString().replace("[","").replace("]","");
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200,"ResponseCode is not 200");
            Assert.assertEquals(EndToEndConstants.statusCode,statusCode,"Status code is not 0");
            restIds = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].id").split(","));

            }
        catch(Exception e)
        {e.printStackTrace();
            }
        catch(AssertionError e)
        { e.printStackTrace(); }
        return restIds;
    }

    public List<String> aggregatorTD(String[] payload, String tid, String token) {
        List<String> restIds = new ArrayList<>();
        List<String> restTd = new ArrayList<>();
        try {
            GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
            Processor processor = new Processor(service, headerHelper.requestHeader(tid, token), payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            String statusCode= JsonPath.read(response, "$.statusCode").toString().replace("[","").replace("]","");
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200,"ResponseCode is not 200");
            Assert.assertEquals(EndToEndConstants.statusCode,statusCode,"Status code is not 0");
            restIds = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].id").split(","));
            List<String> restTds=Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..tradeDiscountInfo").split(","));
            for(int i=0; i<restIds.size(); i++) {
                if(restTds.get(i).equals("null"))
                {
                    restTd.add(restIds.get(i));
                }
            }
        }
        catch(Exception e)
        {e.printStackTrace();
        }
        catch(AssertionError e)
        { e.printStackTrace(); }
        return restIds;
    }


   /* public List<String> searchTD(String keyword, String lat, String lng, String tid, String token) {
        List<String> restIds = new ArrayList<>();
        List<String> restTd = new ArrayList<>();
        try {
            String[] queryParams = {lat, lng, keyword};
            HashMap<String, String> requestHeader = new HashMap<String, String>();
            requestHeader.put("Content-Type", "application/json");
            GameOfThronesService gots = new GameOfThronesService("sand", "searchV2", gameofthrones);
            String response= new Processor(gots, requestHeader, null, queryParams).ResponseValidator.GetBodyAsText();
            restIds=Arrays.asList(sandHelper.JsonString(response, " $.data.restaurants..id").split(","));
            for(int i=0; i<restIds.size(); i++) {
                if(restTds.get(i).equals("null"))
                {
                    restTd.add(restIds.get(i));
                }
            }
        }
        catch(Exception e)
        {e.printStackTrace();
        }
        catch(AssertionError e)
        { e.printStackTrace(); }
        return restIds;
    }
*/


    public List<String> Aggregator(String[] payload, String tid, String token) {
        List<String> openRestList = new ArrayList<>();
        try {
            GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
            Processor processor = new Processor(service, headerHelper.requestHeader(tid, token), payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            String statusCode= JsonPath.read(response, "$.statusCode").toString().replace("[","").replace("]","");
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200,"ResponseCode is not 200");
            Assert.assertEquals(EndToEndConstants.statusCode,statusCode,"Status code is not 0");
            List<String> restIds = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].id").split(","));
            List<String> open = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].availability.opened").split(","));
            List<String> restaurantServicibility = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants.[*].sla.serviceability").split(","));
            for (int i = 0; i < restIds.size(); i++) {
                if (open.get(i).equalsIgnoreCase("true") && restaurantServicibility.get(i).equalsIgnoreCase("SERVICEABLE")) {
                    openRestList.add(restIds.get(i));
                }
            }
        }
        catch(Exception e)
        {e.printStackTrace();
        }
        catch(AssertionError e)
        { e.printStackTrace(); }
        return openRestList;
    }

    public HashMap<String, String> orderPlace(String tid, String token,Integer addressId, String payment, String comments)
    {
        HashMap<String, String> hashMap= new HashMap<>();
        try {
            GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
            Processor processor = new Processor(service, headerHelper.requestHeader(tid,token), new String[]{String.valueOf(addressId), payment, comments});
            String order = processor.ResponseValidator.GetBodyAsText();
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("data.order_id"),"order is not generated");
            hashMap.put("order_id", headerHelper.JsonString(order, "$.data.order_id"));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        catch(AssertionError e)
        {e.printStackTrace();
              }
        return hashMap;
    }

    public HashMap<String, String> requestHeader(String tid, String token) {
        HashMap<String, String> requestheaders = new HashMap<String, String>(){{ put("tid", tid); put("token", token); contentType();}};
        return requestheaders;
    }

    public HashMap<String, String> contentType(){
        HashMap<String, String> requestheaders = new HashMap<String, String>(){{put("Content-type", "application/json");}};
        return requestheaders;
    }

    public String getRestaurantMenu(String restId, String tid, String token){
        List<String> itemIds= new ArrayList<>();
        sandHelper.TMenu(restId);
        try{
            GameOfThronesService service = new GameOfThronesService("checkout", "menuv4withoutlatlng", gameofthrones);
            Processor processor = new Processor(service, requestHeader(tid, token), null, new String[]{restId});
            String rest= processor.ResponseValidator.GetBodyAsText();
            List<String> menuLists = Arrays.asList(headerHelper.JsonString(rest, "$.data.menu.items[*].id").split(","));
            List<String> stockList= Arrays.asList(headerHelper.JsonString(rest, "$.data.menu.items[*].inStock").split(","));
            List<String> enabledList= Arrays.asList(headerHelper.JsonString(rest, "$.data.menu.items[*].enabled").split(","));
            for(int i=0; i<menuLists.size(); i++) {
                if (stockList.get(i).equals("1") && enabledList.get(i).equals("1"))
                {
                    itemIds.add(menuLists.get(i));
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace(); }
        catch(AssertionError e)
        {
            e.printStackTrace();  }
        int n=random.nextInt(itemIds.size());
        return itemIds.get(n);
    }




    public String getItemFromMenu(String restId, String tid, String token){
        List<String> menuLists= new ArrayList<>();
        try{
            GameOfThronesService service = new GameOfThronesService("sand", "menuv4withoutlatlng", gameofthrones);
            Processor processor = new Processor(service, requestHeader(tid, token), null, new String[]{restId});
            String rest= processor.ResponseValidator.GetBodyAsText();
            menuLists = Arrays.asList(headerHelper.JsonString(rest, "$.data.menu.items[*].id").split(","));
        }
        catch(Exception e)
        {
            e.printStackTrace(); }
        catch(AssertionError e)
        {
            e.printStackTrace();  }
        int n=random.nextInt(menuLists.size());
        return menuLists.get(n);
    }

    public String getOrderID(String tid, String token , String lat, String lng){
        List<String> restID= Aggregator(new String[]{lat,lng}, tid, token);
        int n= new Random().nextInt(restID.size());
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(null, null, getRestaurantMenu(restID.get(n), tid, token), 1));
        try {
            CreateMenuEntry payload = new CreateOrderBuilder()
                    .cart(cart)
                    .restaurant(Integer.valueOf(restID.get(n)))
                    .paymentMethod("Cash")
                    .orderComments("Test-Order")
                    .buildPayment();
            String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
            HashMap<String, String> cartHashMap = CreateCartAddon(tid, token, cartPayload, payload.getRestaurantId());
            HashMap<String, String> order = orderPlace(tid, token, Integer.parseInt(cartHashMap.get("addressId")), payload.getpayment_cod_method(), payload.getorder_comments());
            Assert.assertNotNull(order.get("order_id"), "Order generation failed");
            return order.get("order_id");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

}
