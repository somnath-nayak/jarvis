package com.swiggy.api.sf.rng.pojo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "coupon_type",
    "name",
    "code",
    "description",
    "is_private",
    "valid_from",
    "valid_till",
    "total_available",
    "totalPerUser",
    "customer_restriction",
    "city_restriction",
    "area_restriction",
    "restaurant_restriction",
    "category_restriction",
    "item_restriction",
    "discount_percentage",
    "discount_amount",
    "free_shipping",
    "free_gifts",
    "first_order_restriction",
    "preferred_payment_method",
    "user_client",
    "slot_restriction",
    "createdOn",
    "image",
    "coupon_bucket",
    "cuisine_restriction",
    "discount_item",
    "usage_count",
    "expiry_offset",
    "applicable_with_swiggy_money",
    "is_bank_discount",
    "refund_source",
    "pg_message",
    "supported_ios_version",
    "created_by",
    "title",
    "tnc",
    "logo",
    "min_amountObject",
    "min_quantityObject"
})
public class CouponPojo {

    @JsonProperty("coupon_type")
    private String couponType;
    @JsonProperty("name")
    private String name;
    @JsonProperty("code")
    private String code;
    @JsonProperty("description")
    private String description;
    @JsonProperty("is_private")
    private Integer isPrivate;
    @JsonProperty("valid_from")
    private String validFrom;
    @JsonProperty("valid_till")
    private String validTill;
    @JsonProperty("total_available")
    private Integer totalAvailable;
    @JsonProperty("totalPerUser")
    private Integer totalPerUser;
    @JsonProperty("customer_restriction")
    private Integer customerRestriction;
    @JsonProperty("city_restriction")
    private Integer cityRestriction;
    @JsonProperty("area_restriction")
    private Integer areaRestriction;
    @JsonProperty("restaurant_restriction")
    private Integer restaurantRestriction;
    @JsonProperty("category_restriction")
    private Integer categoryRestriction;
    @JsonProperty("item_restriction")
    private Integer itemRestriction;
    @JsonProperty("discount_percentage")
    private Integer discountPercentage;
    @JsonProperty("discount_amount")
    private Integer discountAmount;
    @JsonProperty("free_shipping")
    private Integer freeShipping;
    @JsonProperty("free_gifts")
    private Integer freeGifts;
    @JsonProperty("first_order_restriction")
    private Integer firstOrderRestriction;
    @JsonProperty("preferred_payment_method")
    private String preferredPaymentMethod;
    @JsonProperty("user_client")
    private Integer userClient;
    @JsonProperty("slot_restriction")
    private Integer slotRestriction;
    @JsonProperty("createdOn")
    private String createdOn;
    @JsonProperty("image")
    private String image;
    @JsonProperty("coupon_bucket")
    private String couponBucket;
    @JsonProperty("cuisine_restriction")
    private Integer cuisineRestriction;
    @JsonProperty("discount_item")
    private Integer discountItem;
    @JsonProperty("usage_count")
    private Integer usageCount;
    @JsonProperty("expiry_offset")
    private Integer expiryOffset;
    @JsonProperty("applicable_with_swiggy_money")
    private Boolean applicableWithSwiggyMoney;
    @JsonProperty("is_bank_discount")
    private Integer isBankDiscount;
    @JsonProperty("refund_source")
    private String refundSource;
    @JsonProperty("pg_message")
    private String pgMessage;
    @JsonProperty("supported_ios_version")
    private Integer supportedIosVersion;
    @JsonProperty("created_by")
    private String createdBy;

    public CouponPojo(String couponType, String name, String code, String description, Integer isPrivate, String validFrom, String validTill, Integer totalAvailable, Integer totalPerUser, Integer customerRestriction, Integer cityRestriction, Integer areaRestriction, Integer restaurantRestriction, Integer categoryRestriction, Integer itemRestriction, Integer discountPercentage, Integer discountAmount, Integer freeShipping, Integer freeGifts, Integer firstOrderRestriction, String preferredPaymentMethod, Integer userClient, Integer slotRestriction, String createdOn, String image, String couponBucket, Integer cuisineRestriction, Integer discountItem, Integer usageCount, Integer expiryOffset, Boolean applicableWithSwiggyMoney, Integer isBankDiscount, String refundSource, String pgMessage, Integer supportedIosVersion, String createdBy, String title, String[] tnc, String logo, MinAmountObject minAmountObject, MinQuantityObject minQuantityObject) {
        this.couponType = couponType;
        this.name = name;
        this.code = code;
        this.description = description;
        this.isPrivate = isPrivate;
        this.validFrom = validFrom;
        this.validTill = validTill;
        this.totalAvailable = totalAvailable;
        this.totalPerUser = totalPerUser;
        this.customerRestriction = customerRestriction;
        this.cityRestriction = cityRestriction;
        this.areaRestriction = areaRestriction;
        this.restaurantRestriction = restaurantRestriction;
        this.categoryRestriction = categoryRestriction;
        this.itemRestriction = itemRestriction;
        this.discountPercentage = discountPercentage;
        this.discountAmount = discountAmount;
        this.freeShipping = freeShipping;
        this.freeGifts = freeGifts;
        this.firstOrderRestriction = firstOrderRestriction;
        this.preferredPaymentMethod = preferredPaymentMethod;
        this.userClient = userClient;
        this.slotRestriction = slotRestriction;
        this.createdOn = createdOn;
        this.image = image;
        this.couponBucket = couponBucket;
        this.cuisineRestriction = cuisineRestriction;
        this.discountItem = discountItem;
        this.usageCount = usageCount;
        this.expiryOffset = expiryOffset;
        this.applicableWithSwiggyMoney = applicableWithSwiggyMoney;
        this.isBankDiscount = isBankDiscount;
        this.refundSource = refundSource;
        this.pgMessage = pgMessage;
        this.supportedIosVersion = supportedIosVersion;
        this.createdBy = createdBy;
        this.title = title;
        this.tnc = tnc;
        this.logo = logo;
        this.minAmountObject = minAmountObject;
        this.minQuantityObject = minQuantityObject;
    }

    @JsonProperty("title")
    private String title;
    @JsonProperty("tnc")
    private String[] tnc = null;
    @JsonProperty("logo")
    private String logo;
    @JsonProperty("min_amountObject")
    private MinAmountObject minAmountObject;
    @JsonProperty("min_quantityObject")
    private MinQuantityObject minQuantityObject;

    /**
     * No args constructor for use in serialization
     */

    /**
     * @param pgMessage
     * @param isBankDiscount
     * @param usageCount
     * @param supportedIosVersion
     * @param freeShipping
     * @param cuisineRestriction
     * @param itemRestriction
     * @param userClient
     * @param validTill
     * @param discountAmount
     * @param couponBucket
     * @param title
     * @param firstOrderRestriction
     * @param expiryOffset
     * @param minAmountObject
     * @param isPrivate
     * @param cityRestriction
     * @param description
     * @param name
     * @param discountPercentage
     * @param totalPerUser
     * @param logo
     * @param tnc
     * @param couponType
     * @param restaurantRestriction
     * @param applicableWithSwiggyMoney
     * @param image
     * @param customerRestriction
     * @param totalAvailable
     * @param code
     * @param preferredPaymentMethod
     * @param freeGifts
     * @param createdOn
     * @param categoryRestriction
     * @param createdBy
     * @param slotRestriction
     * @param discountItem
     * @param areaRestriction
     * @param validFrom
     * @param minQuantityObject
     * @param refundSource
     */

    @JsonProperty("coupon_type")
    public String getCouponType() {
        return couponType;
    }

    @JsonProperty("coupon_type")
    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public CouponPojo withCouponType(String couponType) {
        this.couponType = couponType;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public CouponPojo withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public CouponPojo withCode(String code) {
        this.code = code;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public CouponPojo withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("is_private")
    public Integer getIsPrivate() {
        return isPrivate;
    }

    @JsonProperty("is_private")
    public void setIsPrivate(Integer isPrivate) {
        this.isPrivate = isPrivate;
    }

    public CouponPojo withIsPrivate(Integer isPrivate) {
        this.isPrivate = isPrivate;
        return this;
    }

    @JsonProperty("valid_from")
    public String getValidFrom() {
        return validFrom;
    }

    @JsonProperty("valid_from")
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public CouponPojo withValidFrom(String validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    @JsonProperty("valid_till")
    public String getValidTill() {
        return validTill;
    }

    @JsonProperty("valid_till")
    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public CouponPojo withValidTill(String validTill) {
        this.validTill = validTill;
        return this;
    }

    @JsonProperty("total_available")
    public Integer getTotalAvailable() {
        return totalAvailable;
    }

    @JsonProperty("total_available")
    public void setTotalAvailable(Integer totalAvailable) {
        this.totalAvailable = totalAvailable;
    }

    public CouponPojo withTotalAvailable(Integer totalAvailable) {
        this.totalAvailable = totalAvailable;
        return this;
    }

    @JsonProperty("totalPerUser")
    public Integer getTotalPerUser() {
        return totalPerUser;
    }

    @JsonProperty("totalPerUser")
    public void setTotalPerUser(Integer totalPerUser) {
        this.totalPerUser = totalPerUser;
    }

    public CouponPojo withTotalPerUser(Integer totalPerUser) {
        this.totalPerUser = totalPerUser;
        return this;
    }

    @JsonProperty("customer_restriction")
    public Integer getCustomerRestriction() {
        return customerRestriction;
    }

    @JsonProperty("customer_restriction")
    public void setCustomerRestriction(Integer customerRestriction) {
        this.customerRestriction = customerRestriction;
    }

    public CouponPojo withCustomerRestriction(Integer customerRestriction) {
        this.customerRestriction = customerRestriction;
        return this;
    }

    @JsonProperty("city_restriction")
    public Integer getCityRestriction() {
        return cityRestriction;
    }

    @JsonProperty("city_restriction")
    public void setCityRestriction(Integer cityRestriction) {
        this.cityRestriction = cityRestriction;
    }

    public CouponPojo withCityRestriction(Integer cityRestriction) {
        this.cityRestriction = cityRestriction;
        return this;
    }

    @JsonProperty("area_restriction")
    public Integer getAreaRestriction() {
        return areaRestriction;
    }

    @JsonProperty("area_restriction")
    public void setAreaRestriction(Integer areaRestriction) {
        this.areaRestriction = areaRestriction;
    }

    public CouponPojo withAreaRestriction(Integer areaRestriction) {
        this.areaRestriction = areaRestriction;
        return this;
    }

    @JsonProperty("restaurant_restriction")
    public Integer getRestaurantRestriction() {
        return restaurantRestriction;
    }

    @JsonProperty("restaurant_restriction")
    public void setRestaurantRestriction(Integer restaurantRestriction) {
        this.restaurantRestriction = restaurantRestriction;
    }

    public CouponPojo withRestaurantRestriction(Integer restaurantRestriction) {
        this.restaurantRestriction = restaurantRestriction;
        return this;
    }

    @JsonProperty("category_restriction")
    public Integer getCategoryRestriction() {
        return categoryRestriction;
    }

    @JsonProperty("category_restriction")
    public void setCategoryRestriction(Integer categoryRestriction) {
        this.categoryRestriction = categoryRestriction;
    }

    public CouponPojo withCategoryRestriction(Integer categoryRestriction) {
        this.categoryRestriction = categoryRestriction;
        return this;
    }

    @JsonProperty("item_restriction")
    public Integer getItemRestriction() {
        return itemRestriction;
    }

    @JsonProperty("item_restriction")
    public void setItemRestriction(Integer itemRestriction) {
        this.itemRestriction = itemRestriction;
    }

    public CouponPojo withItemRestriction(Integer itemRestriction) {
        this.itemRestriction = itemRestriction;
        return this;
    }

    @JsonProperty("discount_percentage")
    public Integer getDiscountPercentage() {
        return discountPercentage;
    }

    @JsonProperty("discount_percentage")
    public void setDiscountPercentage(Integer discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public CouponPojo withDiscountPercentage(Integer discountPercentage) {
        this.discountPercentage = discountPercentage;
        return this;
    }

    @JsonProperty("discount_amount")
    public Integer getDiscountAmount() {
        return discountAmount;
    }

    @JsonProperty("discount_amount")
    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

    public CouponPojo withDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
        return this;
    }

    @JsonProperty("free_shipping")
    public Integer getFreeShipping() {
        return freeShipping;
    }

    @JsonProperty("free_shipping")
    public void setFreeShipping(Integer freeShipping) {
        this.freeShipping = freeShipping;
    }

    public CouponPojo withFreeShipping(Integer freeShipping) {
        this.freeShipping = freeShipping;
        return this;
    }

    @JsonProperty("free_gifts")
    public Integer getFreeGifts() {
        return freeGifts;
    }

    @JsonProperty("free_gifts")
    public void setFreeGifts(Integer freeGifts) {
        this.freeGifts = freeGifts;
    }

    public CouponPojo withFreeGifts(Integer freeGifts) {
        this.freeGifts = freeGifts;
        return this;
    }

    @JsonProperty("first_order_restriction")
    public Integer getFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    @JsonProperty("first_order_restriction")
    public void setFirstOrderRestriction(Integer firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
    }

    public CouponPojo withFirstOrderRestriction(Integer firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
        return this;
    }

    @JsonProperty("preferred_payment_method")
    public String getPreferredPaymentMethod() {
        return preferredPaymentMethod;
    }

    @JsonProperty("preferred_payment_method")
    public void setPreferredPaymentMethod(String preferredPaymentMethod) {
        this.preferredPaymentMethod = preferredPaymentMethod;
    }

    public CouponPojo withPreferredPaymentMethod(String preferredPaymentMethod) {
        this.preferredPaymentMethod = preferredPaymentMethod;
        return this;
    }

    @JsonProperty("user_client")
    public Integer getUserClient() {
        return userClient;
    }

    @JsonProperty("user_client")
    public void setUserClient(Integer userClient) {
        this.userClient = userClient;
    }

    public CouponPojo withUserClient(Integer userClient) {
        this.userClient = userClient;
        return this;
    }

    @JsonProperty("slot_restriction")
    public Integer getSlotRestriction() {
        return slotRestriction;
    }

    @JsonProperty("slot_restriction")
    public void setSlotRestriction(Integer slotRestriction) {
        this.slotRestriction = slotRestriction;
    }

    public CouponPojo withSlotRestriction(Integer slotRestriction) {
        this.slotRestriction = slotRestriction;
        return this;
    }

    @JsonProperty("createdOn")
    public String getCreatedOn() {
        return createdOn;
    }

    @JsonProperty("createdOn")
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public CouponPojo withCreatedOn(String createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    public CouponPojo withImage(String image) {
        this.image = image;
        return this;
    }

    @JsonProperty("coupon_bucket")
    public String getCouponBucket() {
        return couponBucket;
    }

    @JsonProperty("coupon_bucket")
    public void setCouponBucket(String couponBucket) {
        this.couponBucket = couponBucket;
    }

    public CouponPojo withCouponBucket(String couponBucket) {
        this.couponBucket = couponBucket;
        return this;
    }

    @JsonProperty("cuisine_restriction")
    public Integer getCuisineRestriction() {
        return cuisineRestriction;
    }

    @JsonProperty("cuisine_restriction")
    public void setCuisineRestriction(Integer cuisineRestriction) {
        this.cuisineRestriction = cuisineRestriction;
    }

    public CouponPojo withCuisineRestriction(Integer cuisineRestriction) {
        this.cuisineRestriction = cuisineRestriction;
        return this;
    }

    @JsonProperty("discount_item")
    public Integer getDiscountItem() {
        return discountItem;
    }

    @JsonProperty("discount_item")
    public void setDiscountItem(Integer discountItem) {
        this.discountItem = discountItem;
    }

    public CouponPojo withDiscountItem(Integer discountItem) {
        this.discountItem = discountItem;
        return this;
    }

    @JsonProperty("usage_count")
    public Integer getUsageCount() {
        return usageCount;
    }

    @JsonProperty("usage_count")
    public void setUsageCount(Integer usageCount) {
        this.usageCount = usageCount;
    }

    public CouponPojo withUsageCount(Integer usageCount) {
        this.usageCount = usageCount;
        return this;
    }

    @JsonProperty("expiry_offset")
    public Integer getExpiryOffset() {
        return expiryOffset;
    }

    @JsonProperty("expiry_offset")
    public void setExpiryOffset(Integer expiryOffset) {
        this.expiryOffset = expiryOffset;
    }

    public CouponPojo withExpiryOffset(Integer expiryOffset) {
        this.expiryOffset = expiryOffset;
        return this;
    }

    @JsonProperty("applicable_with_swiggy_money")
    public Boolean getApplicableWithSwiggyMoney() {
        return applicableWithSwiggyMoney;
    }

    @JsonProperty("applicable_with_swiggy_money")
    public void setApplicableWithSwiggyMoney(Boolean applicableWithSwiggyMoney) {
        this.applicableWithSwiggyMoney = applicableWithSwiggyMoney;
    }

    public CouponPojo withApplicableWithSwiggyMoney(Boolean applicableWithSwiggyMoney) {
        this.applicableWithSwiggyMoney = applicableWithSwiggyMoney;
        return this;
    }

    @JsonProperty("is_bank_discount")
    public Integer getIsBankDiscount() {
        return isBankDiscount;
    }

    @JsonProperty("is_bank_discount")
    public void setIsBankDiscount(Integer isBankDiscount) {
        this.isBankDiscount = isBankDiscount;
    }

    public CouponPojo withIsBankDiscount(Integer isBankDiscount) {
        this.isBankDiscount = isBankDiscount;
        return this;
    }

    @JsonProperty("refund_source")
    public String getRefundSource() {
        return refundSource;
    }

    @JsonProperty("refund_source")
    public void setRefundSource(String refundSource) {
        this.refundSource = refundSource;
    }

    public CouponPojo withRefundSource(String refundSource) {
        this.refundSource = refundSource;
        return this;
    }

    @JsonProperty("pg_message")
    public String getPgMessage() {
        return pgMessage;
    }

    @JsonProperty("pg_message")
    public void setPgMessage(String pgMessage) {
        this.pgMessage = pgMessage;
    }

    public CouponPojo withPgMessage(String pgMessage) {
        this.pgMessage = pgMessage;
        return this;
    }

    @JsonProperty("supported_ios_version")
    public Integer getSupportedIosVersion() {
        return supportedIosVersion;
    }

    @JsonProperty("supported_ios_version")
    public void setSupportedIosVersion(Integer supportedIosVersion) {
        this.supportedIosVersion = supportedIosVersion;
    }

    public CouponPojo withSupportedIosVersion(Integer supportedIosVersion) {
        this.supportedIosVersion = supportedIosVersion;
        return this;
    }

    @JsonProperty("created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("created_by")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public CouponPojo withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public CouponPojo withTitle(String title) {
        this.title = title;
        return this;
    }

    @JsonProperty("tnc")
    public String[] getTnc() {
        return tnc;
    }

    @JsonProperty("tnc")
    public void setTnc(String[] tnc) {
        this.tnc = tnc;
    }

    public CouponPojo withTnc(String[] tnc) {
        this.tnc = tnc;
        return this;
    }

    @JsonProperty("logo")
    public String getLogo() {
        return logo;
    }

    @JsonProperty("logo")
    public void setLogo(String logo) {
        this.logo = logo;
    }

    public CouponPojo withLogo(String logo) {
        this.logo = logo;
        return this;
    }

    @JsonProperty("min_amountObject")
    public MinAmountObject getMinAmountObject() {
        return minAmountObject;
    }

    @JsonProperty("min_amountObject")
    public void setMinAmountObject(MinAmountObject minAmountObject) {
        this.minAmountObject = minAmountObject;
    }

    public CouponPojo withMinAmountObject(MinAmountObject minAmountObject) {
        this.minAmountObject = minAmountObject;
        return this;
    }

    @JsonProperty("min_quantityObject")
    public MinQuantityObject getMinQuantityObject() {
        return minQuantityObject;
    }

    @JsonProperty("min_quantityObject")
    public void setMinQuantityObject(MinQuantityObject minQuantityObject) {
        this.minQuantityObject = minQuantityObject;
    }

    public CouponPojo withMinQuantityObject(MinQuantityObject minQuantityObject) {
        this.minQuantityObject = minQuantityObject;
        return this;
    }
    public CouponPojo build()
    {
        return this;
    }

//    public CouponPojo buildDefaultData() {
//
//        withCouponType("Discount").withName("Mycoupon").withCode("").withDescription("Public Coupon").withIsPrivate(0).withValidFrom("").withValidTill("")
//                .withTotalAvailable(this.).withTotalPerUser(3).withMinAmountObject(this.minAmountObject).withCustomerRestriction(0)
//                .withCityRestriction(0).withAreaRestriction(0).withRestaurantRestriction(0).withCategoryRestriction(0).withItemRestriction(0)
//                .withDiscountPercentage(14).withDiscountAmount(50).withFreeShipping(0).withFreeGifts(0).withFirstOrderRestriction(0)
//                .withPreferredPaymentMethod("Juspay-NB").withUserClient(0).withSlotRestriction(1).withCreatedOn("")
//                .withImage("NULL").withCouponBucket("Internal").withMinQuantityObject(this.minQuantityObject)
//                .withCuisineRestriction(0).withDiscountItem(-1)
//                .withUsageCount(0).withExpiryOffset(1).withApplicableWithSwiggyMoney(true).withIsBankDiscount(0)
//                .withRefundSource("").withPgMessage("").withSupportedIosVersion(1).withCreatedBy("user@swiggy.in").withTitle("automation").withTnc(RngHelper.getTncDefaultData()).withLogo("lbx02iaashsnhea5fpwa");
//        return this;
//    }
}
