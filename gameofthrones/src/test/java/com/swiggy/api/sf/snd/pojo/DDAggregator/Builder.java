package com.swiggy.api.sf.snd.pojo.DDAggregator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Builder {
    private CollectionData cd;
    public Builder(){
        cd = new CollectionData();
    }

    RESTAURANTLISTING rl= new RESTAURANTLISTING();
    CAROUSEL c= new CAROUSEL();
    COLLECTION col= new COLLECTION();
    COLLECTIONV2 colV2= new COLLECTIONV2();
    ViewTypeRequest viewType= new ViewTypeRequest();

    public Builder restList(RESTAURANTLISTING rl ) {
        cd.setRESTAURANTLISTING(rl);
        return this;
    }

    public Builder carousel(CAROUSEL c) {
        cd.setCAROUSEL(c);
        return this;
    }

    public Builder collection(COLLECTION c) {
        cd.setCOLLECTION(c);
        return this;
    }

    public Builder collectionV2(COLLECTIONV2 c) {
        cd.setCOLLECTIONV2(c);
        return this;
    }

    public CollectionData build() throws IOException {
        defaultCD();
        return cd;
    }

    private void defaultCD() throws IOException{
        List<ViewTypeRequest> vw= new ArrayList<>();
        if(rl.getLatlng()== null){
            rl.setLatlng("12.9326,77.6036");
        }
        if(c.getLatlng()==null)
            c.setLatlng("12.9326,77.6036");

        if(col.getLatlng()==null)
            col.setLatlng("12.9326,77.6036");

        if(colV2.getLatlng()==null)
            colV2.setLatlng("12.9326,77.6036");

        if(colV2.getCustomerPage()==null)
            colV2.setCustomerPage("OFFERS");
        if(colV2.getViewTypeRequests()==null){
            if(viewType.getViewType()==null)
                viewType.setViewType("VERTICAL_GRID_WITH_IMAGE");
            if(viewType.getMaxCount()==null)
                viewType.setMaxCount(1);
            vw.add(viewType);
            colV2.setViewTypeRequests(vw);
        }
        if(cd.getRESTAURANTLISTING()==null)
            cd.setRESTAURANTLISTING(rl);
        if(cd.getCAROUSEL()==null)
            cd.setCAROUSEL(c);
        if(cd.getCOLLECTION()==null)
            cd.setCOLLECTION(col);
        if(cd.getCOLLECTIONV2()==null)
            cd.setCOLLECTIONV2(colV2);

    }


}
