package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "Vat",
        "Service Charges",
        "Service Tax",
        "GST"
})
public class ItemCharges {

    @JsonProperty("Vat")
    private String vat;
    @JsonProperty("Service Charges")
    private String serviceCharges;
    @JsonProperty("Service Tax")
    private String serviceTax;
    @JsonProperty("GST")
    private String gST;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemCharges() {
    }

    /**
     *
     * @param serviceCharges
     * @param serviceTax
     * @param vat
     * @param gST
     */
    public ItemCharges(String vat, String serviceCharges, String serviceTax, String gST) {
        super();
        this.vat = vat;
        this.serviceCharges = serviceCharges;
        this.serviceTax = serviceTax;
        this.gST = gST;
    }

    @JsonProperty("Vat")
    public String getVat() {
        return vat;
    }

    @JsonProperty("Vat")
    public void setVat(String vat) {
        this.vat = vat;
    }

    @JsonProperty("Service Charges")
    public String getServiceCharges() {
        return serviceCharges;
    }

    @JsonProperty("Service Charges")
    public void setServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    @JsonProperty("Service Tax")
    public String getServiceTax() {
        return serviceTax;
    }

    @JsonProperty("Service Tax")
    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    @JsonProperty("GST")
    public String getGST() {
        return gST;
    }

    @JsonProperty("GST")
    public void setGST(String gST) {
        this.gST = gST;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("vat", vat).append("serviceCharges", serviceCharges).append("serviceTax", serviceTax).append("gST", gST).toString();
    }

}