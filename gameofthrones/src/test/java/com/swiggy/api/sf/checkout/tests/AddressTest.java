package com.swiggy.api.sf.checkout.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.AddressConstants;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.SchemaValidationConstants;
import com.swiggy.api.sf.checkout.dp.AddressDP;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;
//import java.util.HashMap;
import org.testng.SkipException;

public class AddressTest extends AddressDP{
    Initialize gameofthrones = new Initialize();
    AddressHelper helper= new AddressHelper();
    CheckoutHelper checkoutHelper= new CheckoutHelper();
    SnDHelper sndHelper= new SnDHelper();
    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();
    String schemaPath= SchemaValidationConstants.SCHEMA_PATH;
    JsonHelper jsonHelper= new JsonHelper();

    String mobile= System.getenv("mobile");
    String password= System.getenv("password");
    static String tid;
    static String token;

    @BeforeTest
    public void setLoginData(){
        if (mobile == null || mobile.isEmpty()){
            mobile= CheckoutConstants.mobile;}

        if (password == null ||password.isEmpty()){
            password=CheckoutConstants.password;}

        HashMap<String, String> hashMap=CheckoutHelper.loginData(mobile,password);
        tid=hashMap.get("Tid");
        token=hashMap.get("Token");
    }


    @Test(dataProvider = "newAddressAnnotation", groups = {"Regression", "Sanity","Chandan" },
                                                                    description = "Add new Address with Annotation")
    public void addNewAddress(String userAgent,String versionCode,String addressPayload) throws IOException {

        HashMap<String, String> requestHeader=AddressHelper.setCompleteHeader(tid,token,userAgent,versionCode);
        String response= invokeNewAddress(requestHeader,userAgent,versionCode,addressPayload);

        String addressId=getAddressId(response);
        Assert.assertNotNull(addressId,"Address Id is null");

        String getAllResponse= helper.getAllAddress(requestHeader).ResponseValidator.GetBodyAsText();
        Assert.assertTrue((getAllAddressId(getAllResponse)).contains(addressId),"Added addressId is missing");
    }


    @Test(dataProvider = "deleteAddress", groups = {"Regression","Sanity","Chandan"}, description = "Add and delete address")
    public void addAndDeleteAddress(String userAgent,String versionCode,String addressPayload){

        HashMap<String, String> requestHeader=AddressHelper.setCompleteHeader(tid,token,userAgent,versionCode);
        String response= invokeNewAddress(requestHeader,userAgent,versionCode,addressPayload);

        String addressId= getAddressId(response);
        Assert.assertNotNull(addressId,"AddressId is null");

        String getAllResponse= helper.getAllAddress(requestHeader).ResponseValidator.GetBodyAsText();
        Assert.assertTrue((getAllAddressId(getAllResponse)).contains(addressId),"Added addressId is missing");

        String deleteAddressRes= helper.deleteAddress(requestHeader,addressId).ResponseValidator.GetBodyAsText();
        AddressHelper.validateApiResStatusData(deleteAddressRes);

        String getAllResponse1= helper.getAllAddress(requestHeader).ResponseValidator.GetBodyAsText();
        AddressHelper.validateApiResStatusData(deleteAddressRes);

        String AddressIdList1= getAllAddressId(getAllResponse1);
        Assert.assertNotNull(addressId,"AddressId is null");
        Assert.assertFalse((AddressIdList1).contains(addressId),"AddressId not deleted");
    }


    @Test(dataProvider = "deleteExistingAddress", groups = {"Regression","Sanity","Chandan"}, description = "delete Existing Address")
    public void deleteExistingAddress(String userAgent,String versionCode) {

        HashMap<String, String> requestHeader=AddressHelper.setCompleteHeader(tid,token,userAgent,versionCode);

        String getAllResponse= helper.getAllAddress(requestHeader).ResponseValidator.GetBodyAsText();
        AddressHelper.validateApiResStatusData(getAllResponse);

        String addressIdLists= getAllAddressId(getAllResponse);

        List<String> addressIdList= Arrays.asList(addressIdLists.split(","));
        String deletedAddressId= addressIdList.get(0);

        if (deletedAddressId== null || deletedAddressId.isEmpty()){
            throw new SkipException("Skipping this exception");
        }
        String deleteResponse= helper.deleteAddress(requestHeader, deletedAddressId).ResponseValidator.GetBodyAsText();
        AddressHelper.validateApiResStatusData(getAllResponse);

        getAllResponse= helper.getAllAddress(requestHeader).ResponseValidator.GetBodyAsText();
        AddressHelper.validateApiResStatusData(getAllResponse);

        String AddressIdList1= getAllAddressId(getAllResponse);
        Assert.assertFalse((AddressIdList1).contains(deletedAddressId),"Address not deleted");
    }


    @Test(dataProvider = "deleteNonExistingAddress", groups = {"Regression","Sanity","Chandan"}, description = "delete Non Existing Address")
    public void deleteNonExistingAddress(String userAgent,String versionCode,String deleteMessage) {

        HashMap<String, String> requestHeader=AddressHelper.setCompleteHeader(tid,token,userAgent,versionCode);
        String getAllResponse= helper.getAllAddress(requestHeader).ResponseValidator.GetBodyAsText();
        AddressHelper.validateApiResStatusData(getAllResponse);

        String addressIdLists= getAllAddressId(getAllResponse);

        List<String> addressIdList= Arrays.asList(addressIdLists.split(","));
        String deletedAddressId= addressIdList.get(0).replace("\"", "")+"1";

           if(deletedAddressId==null || deletedAddressId.isEmpty()){
               Random rand = new Random();
               int randomID=rand.nextInt();
               deletedAddressId=Integer.toString(randomID);
           }

        String deleteResponse= helper.deleteAddress(requestHeader, deletedAddressId).ResponseValidator.GetBodyAsText();
        String deleteStatusMessage= getstatusMessage(deleteResponse);
        Assert.assertEquals(deleteMessage,deleteStatusMessage,"Mismatch in deleted message response");
        Assert.assertEquals(Integer.toString(AddressConstants.invalidStatusCode),getstatusCode(deleteResponse),
                                    "Mismatch in deleted message response");
    }


    @Test(dataProvider = "UpdateAddressAnnotation", groups = { "Regression", "Sanity","Smoke", "Jitender" }, description = "1.Get Tid, token from login 2.Add address for a user 3.Update the same address 4. Verify it using GET ALL address")
    public void AddAndUpdateAddress(String tid,String token, String name, String mobile, String address, String landmark, String updateLandmark, String area, String lat, String lng, String flat_no, String city,String annotation, String message, int statusCode)
     {

         String response= helper.NewAddress(tid,token,name, mobile, address,landmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
         String AddressId= JsonPath.read(response, "$.data..address_id").toString().replace("[", "").replace("]","");
         String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]","");
         Assert.assertEquals(AddressConstants.statusmessage,statusMessage);
         Assert.assertNotNull(AddressId);
         String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
         String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
         Assert.assertTrue((AddressIdList).contains(AddressId));
        String UpdateResponse = helper.UpdateAddress(tid,token,AddressId, name, mobile, address,updateLandmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
        String updateAddressMessage= JsonPath.read(UpdateResponse,"$.statusMessage").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(message,updateAddressMessage,"Address not updated");
        String UpdateAddressId = JsonPath.read(UpdateResponse, "$.data.address_id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(UpdateAddressId);
        String getAllResponse1 = helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList1 = JsonPath.read(getAllResponse1, "$.data.addresses..id").toString().replace("[", "").replace("]", "");
        Assert.assertTrue((AddressIdList1).contains(UpdateAddressId));
    }

    @Test(dataProvider = "UpdateExistAddress", groups = { "Regression", "Sanity","Smoke", "Jitender" }, description = "\n1.Get Tid, token from login 2.Update the same address 3.Verify it using GET ALL address")
    public void UpdateExistingAddress(String tid,String token, String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation, String updateMessage)
    {

        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","").replace("\"","");
        List<String> list= Arrays.asList(AddressIdList.split(","));
        String UpdateResponse = helper.UpdateAddress(tid,token,list.get(1), name, mobile, address,landmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
        String UpdateAddressId = JsonPath.read(UpdateResponse, "$.data.address_id").toString().replace("[", "").replace("]", "");
        String updateAddressMessage= JsonPath.read(UpdateResponse,"$.statusMessage").toString().replace("[","").replace("[","").replace("\"","");
        Assert.assertEquals(updateMessage,updateAddressMessage,"Address not updated");
        Assert.assertNotNull(UpdateAddressId);
        String getAllResponse1 = helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList1 = JsonPath.read(getAllResponse1, "$.data.addresses..id").toString().replace("[", "").replace("]", "");
        Assert.assertTrue((AddressIdList1).contains(UpdateAddressId));
    }

    @Test(dataProvider = "MandatoryFields", groups = { "Regression" , "Sanity", "Smoke", "Jitender"}, description = "\n 1.Get Tid, token from login 2.Add addrees for a user and check mandatory fields ")
    public void CheckMandatoryFieldsAddress(String tid,String token, String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation, String message,int statusCode)
    {

        String response= helper.NewAddress(tid,token,name, mobile, address,landmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
        String AddressId= JsonPath.read(response, "$.data..address_id").toString().replace("[", "").replace("]","");
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]","");
        String statCode= JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]","");
        Assert.assertEquals(message,statusMessage);
        Assert.assertEquals(statusCode, Integer.parseInt(statCode));
        Assert.assertNotNull(AddressId);
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        Assert.assertTrue((AddressIdList).contains(AddressId));
    }

    @Test(dataProvider = "NewAddressAnnotation", groups = { "Regression", "Sanity","Smoke", "Jitender" }, description = "\n 1.Get Tid, token from login, 2.Add addrees for a user,  3.Verify it using Get ALL Api ")
    public void AddNewHome(String tid,String token,  String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation, String message, int statusCode)
    {

        String response= helper.NewAddress(tid,token,name, mobile, address,landmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
        String AddressId= JsonPath.read(response, "$.data..address_id").toString().replace("[", "").replace("]","");
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]","");
        String statCode= JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]","");
        Assert.assertEquals(message,statusMessage);
        Assert.assertEquals(statusCode, Integer.parseInt(statCode),"status code is not same");
        Assert.assertNotNull(AddressId);
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        //Assert.assertEquals(AddressId, AddressIdList.);
        Assert.assertTrue((AddressIdList).contains(AddressId));
        String addressIds= JsonPath.read(getAllResponse,"$.data.addresses..id").toString().replace("[", "").replace("]","").replace("\"","");
        String[] addressIDArray= addressIds.split(",");
        String home= JsonPath.read(getAllResponse,"$.data.addresses..annotation").toString().replace("[", "").replace("]","").replace("\"","");
        String[] homeOnlyArray= home.split(",");
        for(int i=0; i<addressIDArray.length; i++) {
            if (addressIDArray[i].equals(AddressId)) {
                Assert.assertEquals(homeOnlyArray[i], "WORK");
            }
        }

    }

    @Test(dataProvider = "NewAddressAnnotation", description = "1.Get Tid, token from login 2.Add addrees for a user 3.Update the same address 4. Verify it using GET ALL address")
    public void mobileCheckInAddress(String tid,String token,  String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation, String message, int statusCode)
    {

        String response= helper.NewAddress(tid,token,name, mobile, address,landmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
        String AddressId= JsonPath.read(response, "$.data..address_id").toString().replace("[", "").replace("]","").replace("\"","");
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]","");
        String statCode= JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]","");
        Assert.assertEquals(message,statusMessage);
        Assert.assertEquals(statusCode, Integer.parseInt(statCode));
        Assert.assertNotNull(AddressId);
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","").replace("\"","");
        String[] addressListArray= AddressIdList.split(",");
        String mobileList= JsonPath.read(getAllResponse, "$.data.addresses..mobile").toString().replace("[", "").replace("]","").replace("\"","");
        String[] mobileListArray= mobileList.split(",");
        for(int i=0; i<addressListArray.length; i++){
        if(addressListArray[i].contains(AddressId))
        {
            Assert.assertEquals(mobileListArray[i],mobile,"mobile no. not same");
        }
        }
        Assert.assertTrue((AddressIdList).contains(AddressId));
    }

    @Test(dataProvider = "ServicableAddress", groups = { "Regression" , "Sanity","Smoke", "Jitender"}, description = "1.Get Tid, token from login 2.Add addrees for a user 3.Take Latitude and longitude from response for a valid 4.Verify it using GET servicable api")
    public void CheckServicableAddress(String tid,String token,  String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation, String message, int statusCode)
    {

        String response= helper.NewAddress(tid,token,name, mobile, address,landmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
        String AddressId= JsonPath.read(response, "$.data..address_id").toString().replace("[", "").replace("]","");
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]","");
        String statCode= JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]","");
        Assert.assertEquals(message,statusMessage);
        Assert.assertEquals(statusCode, Integer.parseInt(statCode));
        Assert.assertNotNull(AddressId);
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        Assert.assertTrue((AddressIdList).contains(AddressId));
        String latitude= JsonPath.read(getAllResponse,"$.data.addresses..lat").toString().replace("[", "").replace("]","").replace("\"","");
        String[] latarray= (latitude.split(","));
        String longitude= JsonPath.read(getAllResponse,"$.data.addresses..lng").toString().replace("[", "").replace("]","").replace("\"","");
        String[] lngArray= (longitude.split(","));
        String deliveryValid= JsonPath.read(getAllResponse, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]","");
        String[] delivery= (deliveryValid.split(","));
        List<String> latitudeList= new ArrayList<>();
        List<String> longitudeList= new ArrayList<>();
        int i= 0;
        if(delivery.length>i)
        {
            if(delivery[i].equals("1"))
            {
                latitudeList.add(latarray[i]);
                longitudeList.add(lngArray[i]);
                String serviceDelivery= helper.Addresservicable(tid,token,latitudeList.get(i),longitudeList.get(i)).ResponseValidator.GetBodyAsText();
                String servicable= JsonPath.read(serviceDelivery, "$.data..is_address_serviceable").toString().replace("[","").replace("]","");
                Assert.assertEquals(servicable, "true");
            }else{
            	latitudeList.add(latarray[i]);
                longitudeList.add(lngArray[i]);
            	String serviceDelivery= helper.Addresservicable(tid,token,latitudeList.get(i),longitudeList.get(i)).ResponseValidator.GetBodyAsText();
                String servicable= JsonPath.read(serviceDelivery, "$.data..is_address_serviceable").toString().replace("[","").replace("]","");
            	Assert.assertEquals(servicable, "false");
            }
        }
        
        
    }

    @Test(dataProvider = "ServicableAddress", groups = { "Regression" , "Sanity","Smoke", "Jitender"}, description = "1.Get Tid, token from login 2.Add addrees for a user 3.Take Latitude and longitude from response for a valid 4.Verify it using GET servicable api")
    public void CheckNonServicableAddress(String tid,String token,  String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation, String message, int statusCode)
    {

        String response= helper.NewAddress(tid,token,name, mobile, address,landmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
        String AddressId= JsonPath.read(response, "$.data..address_id").toString().replace("[", "").replace("]","");
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]","");
        String statCode= JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]","");
        Assert.assertEquals(message,statusMessage);
        Assert.assertEquals(statusCode, Integer.parseInt(statCode));
        Assert.assertNotNull(AddressId);
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        Assert.assertTrue((AddressIdList).contains(AddressId));
        String latitude= JsonPath.read(getAllResponse,"$.data.addresses..lat").toString().replace("[", "").replace("]","").replace("\"","");
        String[] latarray= (latitude.split(","));
        String longitude= JsonPath.read(getAllResponse,"$.data.addresses..lng").toString().replace("[", "").replace("]","").replace("\"","");
        String[] lngArray= (longitude.split(","));
        String deliveryValid= JsonPath.read(getAllResponse, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]","");
        String[] delivery= (deliveryValid.split(","));
        List<String> latitudeList= new ArrayList<>();
        List<String> longitudeList= new ArrayList<>();
        for(int i=0; i<delivery.length; i++)
        {
            if(delivery[i].equals("0"))
            {
                latitudeList.add(latarray[i]);
                longitudeList.add(lngArray[i]);
            }
        }
        String serviceDelivery= helper.Addresservicable(tid,token,latitudeList.get(0),longitudeList.get(0)).ResponseValidator.GetBodyAsText();
        String servicable= JsonPath.read(serviceDelivery, "$.data..is_address_serviceable").toString().replace("[","").replace("]","");
        Assert.assertEquals(servicable, "false");
    }

    @Test(dataProvider = "MultipleOrderAddAddress", groups = { "Regression" , "Sanity","Smoke", "Jitender"}, description = "1.Get Tid, token from login 2.Add addrees for a user 3.Take Latitude and longitude from response for a valid 4.Verify it using GET servicable api")
    public void createCartAndAddress(CreateMenuEntry payload) throws IOException {
       String addressId =helper.createCartAddress(payload);
       HashMap<String, String> hashMap = checkoutHelper.TokenData(String.valueOf(payload.getmobile()), payload.getpassword());
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        Assert.assertTrue((AddressIdList).contains(addressId));
    }

    @Test(dataProvider = "AddAndDeleteAddress", groups = { "Regression" , "Sanity","Smoke", "Jitender"}, description = "1.Get Tid, token from login 2.Add addrees for a user 3.Take Latitude and longitude from response for a valid 4.Verify it using GET servicable api")
    public void createCartAddAndDeleteAddress(CreateMenuEntry payload,String deleteMessage) throws IOException {
        String addressId =helper.createCartAddress(payload);
        HashMap<String, String> hashMap = checkoutHelper.TokenData(String.valueOf(payload.getmobile()), payload.getpassword());
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        Assert.assertTrue((AddressIdList).contains(addressId));
        String DeleteResponse= helper.DeleteAddress(tid,token, addressId).ResponseValidator.GetBodyAsText();
        String DeleteStatusMessage= JsonPath.read(DeleteResponse, "$.statusMessage").toString().replace("[","").replace("]","");
        Assert.assertEquals(deleteMessage,DeleteStatusMessage);
        String getAllResponse1= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList1= JsonPath.read(getAllResponse1, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        Assert.assertFalse((AddressIdList1).contains(addressId));

    }

    /*@Test(dataProvider = "AddAndUPdateAddress", groups = { "Regression" , "Sanity","Smoke", "Jitender"}, description = "1.Get Tid, token from login 2.Add addrees for a user 3.Take Latitude and longitude from response for a valid 4.Verify it using GET servicable api")
    public void createCartAddAndUpdateAddress(CreateMenuEntry payload,String updateMessage) throws IOException {
        String addressId =helper.createCartAddress(payload);
        HashMap<String, String> hashMap = checkoutHelper.TokenData(String.valueOf(payload.getmobile()), payload.getpassword());
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        Assert.assertTrue((AddressIdList).contains(addressId));
        String UpdateResponse = helper.UpdateAddress(tid,token,addressId, payload.getName(), ,updateLandmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
        String updateAddressMessage= JsonPath.read(UpdateResponse,"").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(updateMessage,updateAddressMessage,"Address not updated");
        String UpdateAddressId = JsonPath.read(UpdateResponse, "$.data.address_id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(UpdateAddressId);
        String getAllResponse1 = helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList1 = JsonPath.read(getAllResponse1, "$.data.addresses..id").toString().replace("[", "").replace("]", "");
        Assert.assertTrue((AddressIdList1).contains(UpdateAddressId));

    }*/

    @Test(dataProvider = "addAddressSchemaValidation", groups={"Regression", "Sanity", "Smoke"}, description = "1.Get Tid, token from login 2.Add address for a user 3. Validate Schema for add new Address")
    public  void addNewAddressSchemaValidation(CreateMenuEntry payload ) throws IOException, ProcessingException {
        HashMap<String, String> hashMap = checkoutHelper.TokenData(payload.getmobile().toString(), payload.getpassword());
        String response= helper.NewAddress(tid,token,payload.getName(),payload.getMobile1(),payload.getAddress(), payload.getLandmark(),payload.getArea(),payload.getLat(),payload.getLng(),payload.getflat_no(),payload.getCity(),payload.getAnnotation()).ResponseValidator.GetBodyAsText();
        String AddressId= JsonPath.read(response, "$.data..address_id").toString().replace("[", "").replace("]","");
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]","");
        String statCode= JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]","");
        String filepath=System.getProperty("user.dir") + schemaPath+"addnewaddress.txt";
        String jsonschema = new ToolBox().readFileAsString(filepath);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,response);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For Add address API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "deleteAddressSchemaValidation", groups={"Regression", "Sanity", "Smoke"}, description = "1.Get Tid, token from login 2.Delete address for a user 3. Validate Schema for delete Address")
    public  void deleteAddressSchemaValidation(CreateMenuEntry payload ) throws IOException, ProcessingException {
        HashMap<String, String> hashMap = checkoutHelper.TokenData(payload.getmobile().toString(), payload.getpassword());
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        List<String> addressIdList= Arrays.asList(AddressIdList.split(","));
        String deletedAddressId= addressIdList.get(0);
        String DeleteResponse= helper.DeleteAddress(tid,token, deletedAddressId).ResponseValidator.GetBodyAsText();
        String DeleteStatusMessage= JsonPath.read(DeleteResponse, "$.statusMessage").toString().replace("[","").replace("]","");
        String filepath=System.getProperty("user.dir") + schemaPath+"deleteaddress.txt";
        String jsonschema = new ToolBox().readFileAsString(filepath);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,DeleteResponse);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete address API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(DeleteResponse);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "updateAddressSchemaValidation", groups = { "Regression", "Sanity","Smoke", "Jitender" }, description = "\n1.Get Tid, token from login 2.Update the same address 3.Verify it using GET ALL address")
    public void UpdateAddressSchemaValidation(CreateMenuEntry payload) throws IOException, ProcessingException
    {
        HashMap<String, String> hashMap = checkoutHelper.TokenData(payload.getmobile().toString(), payload.getpassword());
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","").replace("\"","");
        List<String> list= Arrays.asList(AddressIdList.split(","));
        String UpdateResponse = helper.UpdateAddress(tid,token,list.get(1),payload.getName(),payload.getMobile1(),payload.getAddress(), payload.getLandmark(),payload.getArea(),payload.getLat(),payload.getLng(),payload.getflat_no(),payload.getCity(),payload.getAnnotation() ).ResponseValidator.GetBodyAsText();
        String UpdateAddressId = JsonPath.read(UpdateResponse, "$.data.address_id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(UpdateAddressId);
        String getAllResponse1 = helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList1 = JsonPath.read(getAllResponse1, "$.data.addresses..id").toString().replace("[", "").replace("]", "");
        Assert.assertTrue((AddressIdList1).contains(UpdateAddressId));
        String filepath=System.getProperty("user.dir") + schemaPath+"updateaddress.txt";
        String jsonschema = new ToolBox().readFileAsString(filepath);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,UpdateResponse);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete address API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(UpdateResponse);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "getAllAddressSchemaValidation", groups = { "Regression", "Sanity","Smoke", "Jitender" }, description = "\n1.Get Tid, token from login 2.Verify GET ALL address 3.Verify schema validation")
    public void getAllAddressSchemaValidation(CreateMenuEntry payload) throws IOException, ProcessingException {
        HashMap<String, String> hashMap = checkoutHelper.TokenData(payload.getmobile().toString(), payload.getpassword());
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        String filepath=System.getProperty("user.dir") + schemaPath+"getalladdress.txt";
        String jsonschema = new ToolBox().readFileAsString(filepath);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,getAllResponse);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete address API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(getAllResponse);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "getAllAddressSchemaValidation", groups = { "Regression", "Sanity","Smoke", "Jitender" }, description = "\n1.Get Tid, token from login 2.check address is Servicable 3.Verify schema validation")
    public void isAddressServicableSchemaValidation(CreateMenuEntry payload) throws IOException, ProcessingException {
        HashMap<String, String> hashMap = checkoutHelper.TokenData(payload.getmobile().toString(), payload.getpassword());
        String getAllResponse= helper.GetAllAdress(tid,token).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        String latitude= JsonPath.read(getAllResponse,"$.data.addresses..lat").toString().replace("[", "").replace("]","").replace("\"","");
        String[] latarray= (latitude.split(","));
        String longitude= JsonPath.read(getAllResponse,"$.data.addresses..lng").toString().replace("[", "").replace("]","").replace("\"","");
        String[] lngArray= (longitude.split(","));
        String deliveryValid= JsonPath.read(getAllResponse, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]","");
        String[] delivery= (deliveryValid.split(","));
        List<String> latitudeList= new ArrayList<>();
        List<String> longitudeList= new ArrayList<>();
        for(int i=0; i<delivery.length; i++)
        {
            if(delivery[i].equalsIgnoreCase("0"))
            {
                latitudeList.add(latarray[i]);
                longitudeList.add(lngArray[i]);
            }
        }
        String serviceDelivery= helper.Addresservicable(tid,token,latitudeList.get(0),longitudeList.get(0)).ResponseValidator.GetBodyAsText();
        String servicable= JsonPath.read(serviceDelivery, "$.data..is_address_serviceable").toString().replace("[","").replace("]","");
        Assert.assertEquals(servicable, "false");
        String filepath=System.getProperty("user.dir") + schemaPath+"isaddressservicable.txt";
        String jsonschema = new ToolBox().readFileAsString(filepath);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,serviceDelivery);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete address API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(getAllResponse);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }


    public String getAddressId(String response){
        String addressId= JsonPath.read(response, "$.data..address_id")
                .toString().replace("[", "").replace("]","");
        return addressId;
    }

    public String getstatusMessage(String response){
        String statusMessage= JsonPath.read(response, "$.statusMessage")
                .toString().replace("[", "").replace("]","");
        return statusMessage;
    }

    public String getstatusCode(String response){
        String statCode= JsonPath.read(response, "$.statusCode")
                .toString().replace("[", "").replace("]","");
        return statCode;
    }

    public String getAllAddressId(String getAllResponse){
        String addressIdList=null;
        try {
            addressIdList = JsonPath.read(getAllResponse, "$.data.addresses..id")
                    .toString().replace("[", "").replace("]", "");
        } catch (Exception e) {
            throw new SkipException("Skipping this exception");
        }
            return addressIdList;
    }

    private String invokeNewAddress(HashMap<String, String> requestHeader,String userAgent,String versionCode,String addressPayload){
        String response= helper.newAddress(requestHeader,addressPayload).ResponseValidator.GetBodyAsText();
        AddressHelper.validateApiResStatusData(response);
        return response;
    }


    @AfterTest
    public void deleteAllAddress(){

        HashMap<String, String> requestHeader=AddressHelper.setCompleteHeader(tid,token,"","");
        String getAllResponse= helper.getAllAddress(requestHeader).ResponseValidator.GetBodyAsText();
        AddressHelper.validateApiResStatusData(getAllResponse);

        String addressIdLists= getAllAddressId(getAllResponse);
        List<String> addressIdList= Arrays.asList(addressIdLists.split(","));
        Reporter.log("Count of saved address:-> "+addressIdList.size(),true);
        int i;
        for (i=0;i<addressIdList.size()-3;i++){
            helper.deleteAddress(requestHeader, addressIdList.get(0)).ResponseValidator.GetBodyAsText();
        }
        i++;
        Reporter.log("Count of deleted saved address:-> "+i,true);
    }
}
