package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.EditTimeConstants;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.getLastOrderResponse.GetLastOrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.getSingleOrderResponse.GetSingleOrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.OrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.Validator;
import net.minidev.json.JSONArray;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class  EditTimeHelper {

    Initialize gameofthrones = new Initialize();
    CheckoutHelper helper= new CheckoutHelper();
    private AddressHelper addressHelper = new AddressHelper();
    private String orderKey = null;
    private String orderID;
    EDVOCartHelper edvoCartHelper = new EDVOCartHelper();


    public void statusCodeCheck(int statusCode) {
        Assert.assertEquals(statusCode,EditTimeConstants.HTTP_CODE);
    }

    public void cartResCheck(String response,String statusMessage) {
        String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getStatusCode,EditTimeConstants.STATUS_CODE);
        String getMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getMessage,EditTimeConstants.CART_UPDATED_SUCCESSFULLY);

    }

    public Validator placeOrder(String itemId, String quantity, String restId, String tid, String token)
    {
        /* Login */
        HashMap<String, String> hashMap=helper.getTokenDataCheckout(EditTimeConstants.mobile1, EditTimeConstants.password1);
        tid=hashMap.get("Tid");
        token=hashMap.get("Token");

        /*Cart Creation*/

        Validator cartValidator = updateCartET(tid, token, itemId, quantity, restId).ResponseValidator;
        statusCodeCheck(cartValidator.GetResponseCode());
        cartResCheck(cartValidator.GetBodyAsText(), EditTimeConstants.CART_UPDATED_SUCCESSFULLY);

        /* Place order */
        String addressID=getAddressIDfromCartResponse(tid, token,cartValidator);
        System.out.println(addressID);
        Validator orderValidator=orderPlace(tid, token, addressID, CheckoutConstants.paymentMethod, CheckoutConstants.orderComments).ResponseValidator;
        statusCodeCheck(orderValidator.GetResponseCode());
        orderResCheck(orderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        orderID=JsonPath.read(orderValidator.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
        System.out.println(orderID);
        orderKey=fetchOrderKey(orderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        orderKey=orderKey.replace("\"", "").replace("\"", "");
        // this.orderKey=orderKey;
        System.out.println(orderKey);
        return orderValidator;
    }

    public Validator placeOrderPayTM(String itemId, String quantity, String restId, String tid, String token)
    {
        /* Login */
        HashMap<String, String> hashMap=helper.getTokenDataCheckout(EditTimeConstants.mobile1, EditTimeConstants.password1);
        tid=hashMap.get("Tid");
        token=hashMap.get("Token");

        /*Cart Creation*/

        Validator cartValidator = updateCartET(tid, token, itemId, quantity, restId).ResponseValidator;
        statusCodeCheck(cartValidator.GetResponseCode());
        cartResCheck(cartValidator.GetBodyAsText(), EditTimeConstants.CART_UPDATED_SUCCESSFULLY);

        /* Place order */
        String addressID=getAddressIDfromCartResponse(tid, token,cartValidator);
        System.out.println(addressID);
        Validator orderValidator=orderPlace(tid, token, addressID, CheckoutConstants.paymentPaytm, CheckoutConstants.orderComments).ResponseValidator;
        statusCodeCheck(orderValidator.GetResponseCode());
        orderResCheck(orderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        orderID=JsonPath.read(orderValidator.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
        System.out.println(orderID);
        orderKey=fetchOrderKey(orderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        orderKey=orderKey.replace("\"", "").replace("\"", "");
        //this.orderKey=orderKey;
        System.out.println(orderKey);
        return orderValidator;
    }


    public Validator editOrderCheck(String tid, String token, String type,String itemId, String quantity, String restId,String newItemId,String newquantity)
    {
        Validator placeOrder = placeOrder(itemId, quantity, restId, tid, token) ;
        orderID=JsonPath.read(placeOrder.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");

        /* Edit order check*/

        Validator editCheckOrderValidator=editOrderCheckET(Long.valueOf(orderID),type,newItemId,newquantity,restId).ResponseValidator;
        System.out.println(editCheckOrderValidator.GetBodyAsText());
        statusCodeCheck(editCheckOrderValidator.GetResponseCode());
        return editCheckOrderValidator;
        //  cartResCheck(editCheckOrderValidator.GetBodyAsText(),cartEditStatusCode,cartEditmessage);
    }


    public Validator editOrderConfirm(String tid, String token,String type,String itemId, String quantity, String restId,String newItemId,String newquantity)
    {

        Validator editCheckOrderValidator = editOrderCheck(tid, token, type,itemId, quantity, restId, newItemId, newquantity);
        /* Edit order confirm*/
        Validator editConfirmOrderValidator = editOrderCheckConfirm(Long.valueOf(orderID),type,newItemId,newquantity, restId).ResponseValidator;
        System.out.println(editConfirmOrderValidator.GetBodyAsText());
        statusCodeCheck(editConfirmOrderValidator.GetResponseCode());
        return editConfirmOrderValidator;
    }

    public Validator getSingleOrder(String tid, String token, String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
        Validator orderConfirm = editOrderConfirm (tid, token, type, itemId, quantity, restId, newItemId, newquantity);
        orderID=JsonPath.read(orderConfirm.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
        Processor getSingleOrder=helper.getSingleOrder(orderID, tid, token);
        System.out.println(getSingleOrder);
        statusCodeCheck(getSingleOrder.ResponseValidator.GetResponseCode());
        orderResCheck(getSingleOrder.ResponseValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        Validator getSingleOrderV = getSingleOrder.ResponseValidator;
        return getSingleOrderV;
    }

    public Validator getSingleOrderKey(String tid, String token, String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
        Validator orderConfirm = editOrderConfirm (tid, token, type, itemId, quantity, restId, newItemId, newquantity);
        Validator  orderKeyValidator=helper.getSingleOrderKey(orderKey,tid,token).ResponseValidator;
        System.out.println(orderKeyValidator);
        statusCodeCheck(orderKeyValidator.GetResponseCode());
        orderResCheck(orderKeyValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        return orderKeyValidator;
    }

    public Validator getLastOrderET(String tid, String token, String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
        Validator orderConfirm = editOrderConfirm (tid, token, type, itemId, quantity, restId, newItemId, newquantity);
        Validator  lastOrderValidator=helper.getLastOrder(tid,token).ResponseValidator;
        System.out.println(lastOrderValidator);
        statusCodeCheck(lastOrderValidator.GetResponseCode());
        orderResCheck(lastOrderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        return lastOrderValidator;
    }

    public String getOrderET(String tid, String token, String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
        Validator orderConfirm = editOrderConfirm (tid, token, type, itemId, quantity, restId, newItemId, newquantity);
        Validator  getOrderValidator=helper.getOrder(tid,token).ResponseValidator;
        System.out.println(getOrderValidator);
        String getInititaionCheck = getOrderCheck(getOrderValidator.GetBodyAsText(), orderID);
        statusCodeCheck(getOrderValidator.GetResponseCode());
        orderResCheck(getOrderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        return getInititaionCheck;
    }



    public void cartResCheck(String response,String expectedStatusCode,String statusMessage) {
        String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getStatusCode,expectedStatusCode);
        String getMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getMessage,statusMessage);

    }

    public void orderResCheck(String response,String statusMessage) {
        String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getStatusCode,EditTimeConstants.STATUS_CODE);
        String getMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getMessage,EditTimeConstants.STATUS_MESSAGE);

    }

    public void orderResCheck(String response,String expectedStatusCode,String expectedStatusMessage) {
        String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getStatusCode,expectedStatusCode);
        String getMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getMessage,expectedStatusMessage);

    }

    public String fetchOrderKey(String response,String statusMessage)
    {
        OrderResponse orderResponse = Utility.jsonDecode(response, OrderResponse.class);
        return orderResponse != null ? orderResponse.getData().getKey() : null;
    }



    public void initiationCheck(String response,String statusMessage) {

        String getInitSource = JsonPath.read(response, "$.data.initiation_source").toString().replace("[", "").replace("]", "");
        if(Integer.parseInt(getInitSource)==1) {
            Assert.assertEquals(getInitSource,EditTimeConstants.restInitSource);

        } else if(Integer.parseInt(getInitSource)==2)
        {

            Assert.assertEquals(getInitSource,EditTimeConstants.custInitSource);
        } else
        {
            Assert.assertEquals(getInitSource,EditTimeConstants.defaultInitSource);
        }

    }


    public  String getOrderCheck(String response,String orderId){

        List<String> orderIds = JsonPath.read(response, "$.data.orders[*].order_id");
        System.out.println(orderIds);
        for(int i = 0; i<orderIds.size(); i++) {
            if(String.valueOf(orderIds.get(i)).equals(orderId)) {
                Integer temp = JsonPath.read(response, "$.data.orders[" + String.valueOf(i) + "].initiation_source");
                System.out.println(temp);
                return String.valueOf(temp);

            }
            break;
        }

        Assert.fail("OrderId = '" + orderId  + "' does not contain initiation source...!!");

        return null;

    }


    public void orderInitiationCheck(String response,String statusMessage) {
        GetSingleOrderResponse getSingleOrderResponse = Utility.jsonDecode(response, GetSingleOrderResponse.class);
        String getInitSource = getSingleOrderResponse.getData().getOrders().get(0).getInitiationSource();
        if(Integer.parseInt(getInitSource)==1) {
            Assert.assertEquals(getInitSource,EditTimeConstants.restInitSource);
        } else if(Integer.parseInt(getInitSource)==2)
        {
            Assert.assertEquals(getInitSource,EditTimeConstants.custInitSource);
        } else
        {
            Assert.assertEquals(getInitSource,EditTimeConstants.defaultInitSource);
        }

    }

    public void lastOrderInitiationCheck(String response,String statusMessage) {
        GetLastOrderResponse getLastOrderResponse = Utility.jsonDecode(response, GetLastOrderResponse.class);
        String getInitSource = getLastOrderResponse.getData().getLastOrder().getInitiationSource();
        if(Integer.parseInt(getInitSource)==1) {
            Assert.assertEquals(getInitSource,EditTimeConstants.restInitSource);
        } else if(Integer.parseInt(getInitSource)==2)
        {
            Assert.assertEquals(getInitSource,EditTimeConstants.custInitSource);
        } else
        {
            Assert.assertEquals(getInitSource,EditTimeConstants.defaultInitSource);
        }

    }


    public void getOrderInitiationCheck(String source ,String statusMessage) {
        if(Integer.parseInt(source)==1) {
            Assert.assertEquals(source,EditTimeConstants.restInitSource);

        } else if(Integer.parseInt(source)==2)
        {
            Assert.assertEquals(source,EditTimeConstants.custInitSource);
        } else
        {
            Assert.assertEquals(source,EditTimeConstants.defaultInitSource);
        }
    }

    public String tradeDiscountCheck(String response,String statusMessage) {
        OrderResponse orderResponse = Utility.jsonDecode(response, OrderResponse.class);
        String tradeDiscountEdit = orderResponse.getData().getTradeDiscount().toString();
        System.out.println(tradeDiscountEdit);
        return tradeDiscountEdit;
    }

    public Processor updateCartET(String tid,String token,String itemId, String quantity, String restId) {
        GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2", gameofthrones);
        HashMap<String, String> requestheaders=createReqHeader(tid, token);
        System.out.println("#####" + requestheaders);
        String[] payloadparams = {itemId,quantity,restId};
        return new Processor(service, requestheaders, payloadparams);
    }

    public Processor orderPlace(String tid, String token,String addressId, String payment, String comments)
    {
        HashMap<String, String> requestheaders=createReqHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
        String []payloadparams = {String.valueOf(addressId),payment,comments};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }


    public Processor editOrderCheckET(Long orderId,String init_source,String menu_item_id,String quantity,String restId)
    {
        HashMap<String, String> requestheaders=createReqHeader("EditTimeConstants.AUTHORIZATION", "null");
        GameOfThronesService service = new GameOfThronesService("checkout", "editordercheck", gameofthrones);
        String[] payloadparams = {String.valueOf(orderId),init_source,menu_item_id,quantity,restId};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }


    public Processor editOrderCheckConfirm(Long orderId,String init_source,String menu_item_id,String quantity,String restId)
    {
        HashMap<String, String> requestheaders=createReqHeader("EditTimeConstants.AUTHORIZATION", "null");
        GameOfThronesService service = new GameOfThronesService("checkout", "editorderconfirm", gameofthrones);
        String[] payloadparams = {String.valueOf(orderId),init_source,menu_item_id,quantity,restId};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }


    public HashMap<String, String> createReqHeader(String tid, String token){
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Tid", tid);
        requestheaders.put("token", token);
        requestheaders.put("version-code", EditTimeConstants.VERSION_CODE);
        requestheaders.put("User-Agent", EditTimeConstants.USER_AGENT);
        requestheaders.put("Authorization", EditTimeConstants.AUTHORIZATION);
        return requestheaders;
    }


    public String getAddressIDfromCartResponse(String tid,String token,Validator ResponseValidator){
        String addressId="";
        System.out.println(ResponseValidator.GetBodyAsText());
        String[] validAddressList=JsonPath.read(ResponseValidator.GetBodyAsText(), "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]", "").split(",");
        for(int i=0;i<validAddressList.length;i++){
            if(validAddressList[i].equals("1")){
                addressId=JsonPath.read(ResponseValidator.GetBodyAsText(), "$.data.addresses.["+i+"]..id").toString().replace("[", "").replace("]", "").replace("\"","");
                return addressId;
            }
        }
        String lat= JsonPath.read(ResponseValidator.GetBodyAsText(), "$.data.restaurant_details.lat").toString().replace("[", "").replace("]","").replace("\"","");
        String lng= JsonPath.read(ResponseValidator.GetBodyAsText(), "$.data.restaurant_details.lng").toString().replace("[", "").replace("]","").replace("\"","");
        String mobileNum= JsonPath.read(ResponseValidator.GetBodyAsText(), "$.data.phone_no").toString().replace("[", "").replace("]","").replace("\"","");
        String addResponse= addressHelper.NewAddress(tid,token,"TestName",mobileNum,"TestAddress","TestLandMark","TestArea" ,lat, lng, "","","Home").ResponseValidator.GetBodyAsText();
        addressId= JsonPath.read(addResponse, "$.data..address_id").toString().replace("[", "").replace("]","");
        System.out.println("New created Serviceable Address ID -: "+addressId);
        return addressId;
    }

    public List<Integer> getItemsWithoutVariant(String restId) {
        Processor processor = edvoCartHelper.getMenuV4(restId);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "[Menu V4 Call Failed for restId = " + restId + "]");
        HashMap<String, Object> itemObject;
        HashMap<String, Object> variantsNew;
        HashMap<String, Object> variantsV2;
        JSONArray variantsGroups;
        List<Integer> itemIds = new ArrayList<>();

        List<Integer> allItemIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.id");
        HashMap<String, Object> items = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items");

        for (int i = 0; i < allItemIds.size(); i++) {
            itemObject = (HashMap<String, Object>) items.get(String.valueOf(allItemIds.get(i)));
            if (itemObject.get("inStock").equals(1) && (itemObject.get("enabled")).equals(1)) {
                if (itemObject.containsKey("variants_new")) {
                    variantsNew = (HashMap<String, Object>) itemObject.get("variants_new");
                    if (variantsNew.containsKey("variant_groups")) {
                        variantsGroups = (JSONArray) variantsNew.get("variant_groups");
                        if (variantsGroups.isEmpty()) {
                            itemIds.add(allItemIds.get(i));
                        }
                    }
                } else if (itemObject.containsKey("variantsV2")) {
                    variantsV2 = (HashMap<String, Object>) itemObject.get("variantsV2");
                    if (variantsV2.containsKey("variant_groups")) {
                        variantsGroups = (JSONArray) variantsV2.get("variant_groups");
                        if (variantsGroups.isEmpty()) {
                            itemIds.add(allItemIds.get(i));
                        }
                    }
                } else {
                    itemIds.add(allItemIds.get(i));
                }
            }
        }
        if(itemIds.isEmpty())
            Assert.fail("No items found without variants for restId = " + restId);
        return itemIds;
    }
}
