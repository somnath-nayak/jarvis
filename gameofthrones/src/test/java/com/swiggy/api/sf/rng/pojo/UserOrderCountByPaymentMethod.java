package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

public class UserOrderCountByPaymentMethod {
    @JsonProperty("Credit/Debit card")
    private long creditDebitCard;
    @JsonProperty("AmazonPay")
    private long amazonPay;
    @JsonProperty("Cash")
    private long cash;
    @JsonProperty("Netbanking")
    private long netbanking;
    @JsonProperty("PayTM")
    private long payTM;
    @JsonProperty("Sodexo")
    private long sodexo;
    @JsonProperty("Juspay")
    private long juspay;
    @JsonProperty("Juspay-NB")
    private long juspayNB;
    @JsonProperty("PayTM-SSO")
    private long payTMSSO;
    @JsonProperty("Mobikwik")
    private long mobikwikSSO;
    @JsonProperty("Freecharge")
    private long freecharge;
    @JsonProperty("Freecharge-SSO")
    private long freechargeSSO;
    @JsonProperty("third-party-cash")
    private long thirdPartyCash;
    @JsonProperty("third-party-online")
    private long thirdPartyOnline;
    @JsonProperty("PayLater_Lazypay")
    private long payLaterLazypay;
    @JsonProperty("PhonePe")
    private long phonePe;
    @JsonProperty("AmazonPayWallet")
    private long amazonPayWallet;
    @JsonProperty("AmazonPayWeb")
    private long amazonPayWeb;

    public long getCreditDebitCard() {
        return creditDebitCard;
    }

    public void setCreditDebitCard(long creditDebitCard) {
        this.creditDebitCard = creditDebitCard;
    }

    public UserOrderCountByPaymentMethod withCreditDebitCard(long creditDebitCard) {
        this.creditDebitCard = creditDebitCard;
        return this;
    }

    public long getAmazonPay() {
        return amazonPay;
    }

    public void setAmazonPay(long amazonPay) {
        amazonPay = amazonPay;
    }

    public UserOrderCountByPaymentMethod withAmazonPay(long amazonPay) {
        this.amazonPay = amazonPay;
        return this;
    }

    public long getCash() {
        return cash;
    }

    public void setCash(long cash) {
        this.cash = cash;
    }

    public UserOrderCountByPaymentMethod withCash(long cash) {
        this.cash = cash;
        return this;
    }

    public long getNetbanking() {
        return netbanking;
    }

    public void setNetbanking(long netbanking) {
        this.netbanking = netbanking;
    }

    public UserOrderCountByPaymentMethod withNetbanking(long netbanking) {
        this.netbanking = netbanking;
        return this;
    }

    public long getPayTM() {
        return payTM;
    }

    public void setPayTM(long payTM) {
        this.payTM = payTM;
    }

    public UserOrderCountByPaymentMethod withPayTM(long payTM) {
        this.payTM = payTM;
        return this;
    }

    public long getSodexo() {
        return sodexo;
    }

    public void setSodexo(long sodexo) {
        this.sodexo = sodexo;
    }

    public UserOrderCountByPaymentMethod withSodexo(long sodexo) {
        this.sodexo = sodexo;
        return this;
    }

    public long getJuspay() {
        return juspay;
    }

    public void setJuspay(long juspay) {
        this.juspay = juspay;
    }

    public UserOrderCountByPaymentMethod withJusPay(long jusPay) {
        this.juspay = jusPay;
        return this;
    }

    public long getJuspayNB() {
        return juspayNB;
    }

    public void setJuspayNB(long juspayNB) {
        this.juspayNB = juspayNB;
    }

    public UserOrderCountByPaymentMethod withJuspayNB(long juspayNB) {
        this.juspayNB = juspayNB;
        return this;
    }

    public long getPayTMSSO() {
        return payTMSSO;
    }

    public void setPayTMSSO(long payTMSSO) {
        this.payTMSSO = payTMSSO;
    }

    public UserOrderCountByPaymentMethod withPayTMSSo(long payTMSSO) {
        this.payTMSSO = payTMSSO;
        return this;
    }

    public long getMobikwikSSO() {
        return mobikwikSSO;
    }

    public void setMobikwikSSO(long mobikwikSSO) {
        this.mobikwikSSO = mobikwikSSO;
    }

    public UserOrderCountByPaymentMethod withMobiwikSSO(long mobikwikSSO) {
        this.mobikwikSSO = mobikwikSSO;
        return this;
    }

    public long getFreecharge() {
        return freecharge;
    }

    public void setFreecharge(long freecharge) {
        this.freecharge = freecharge;
    }

    public UserOrderCountByPaymentMethod withFreeCharge(long freeCharge) {
        this.freecharge = freeCharge;
        return this;
    }

    public long getFreechargeSSO() {
        return freechargeSSO;
    }

    public void setFreechargeSSO(long freechargeSSO) {
        this.freechargeSSO = freechargeSSO;
    }

    public UserOrderCountByPaymentMethod withFreeChargeSSO(long freeChargeSSO) {
        this.freechargeSSO = freeChargeSSO;
        return this;
    }

    public long getThirdPartyCash() {
        return thirdPartyCash;
    }

    public void setThirdPartyCash(long thirdPartyCash) {
        this.thirdPartyCash = thirdPartyCash;
    }

    public UserOrderCountByPaymentMethod withThirdPartyCash(long thirdPartyCash) {
        this.thirdPartyCash = thirdPartyCash;
        return this;
    }

    public long getThirdPartyOnline() {
        return thirdPartyOnline;
    }

    public void setThirdPartyOnline(long thirdPartyOnline) {
        this.thirdPartyOnline = thirdPartyOnline;
    }

    public UserOrderCountByPaymentMethod withThirdPartyOnline(long thirdPartyOnline) {
        this.thirdPartyOnline = thirdPartyOnline;
        return this;
    }

    public long getPayLaterLazypay() {
        return payLaterLazypay;
    }

    public void setPayLaterLazypay(long payLaterLazypay) {
        this.payLaterLazypay = payLaterLazypay;
    }

    public UserOrderCountByPaymentMethod withPayLaterLazypay(long payLaterLazypay) {
        this.payLaterLazypay = payLaterLazypay;
        return this;
    }

    public long getPhonePe() {
        return phonePe;
    }

    public void setPhonePe(long phonePe) {
        this.phonePe = phonePe;
    }

    public UserOrderCountByPaymentMethod withPhonePe(long phonePe) {
        this.phonePe = phonePe;
        return this;
    }

    public long getAmazonPayWallet() {
        return amazonPayWallet;
    }

    public void setAmazonPayWallet(long amazonPayWallet) {
        this.amazonPayWallet = amazonPayWallet;
    }

    public UserOrderCountByPaymentMethod withAmazonPayWallet(long amazonPayWallet) {
        this.amazonPayWallet = amazonPayWallet;
        return this;
    }

    public long getAmazonPayWeb() {
        return amazonPayWeb;
    }

    public void setAmazonPayWeb(long amazonPayWeb) {
        this.amazonPayWeb = amazonPayWeb;
    }

    public UserOrderCountByPaymentMethod withAmazonPayWeb(long amazonPayWeb) {
        this.amazonPayWeb = amazonPayWeb;
        return this;
    }

    public UserOrderCountByPaymentMethod setDefaultData() {
        this.payTM = 0;
        this.sodexo = 0;
        this.creditDebitCard = 0;
        this.amazonPay = 0;
        this.phonePe = 0;
        this.payLaterLazypay = 0;
        return this;
    }
}
