package com.swiggy.api.sf.checkout.helper.edvo.pojo.placeOrder;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "address_id",
        "payment_cod_method",
        "order_comments"
})
public class PlaceOrderRequest {

    @JsonProperty("address_id")
    private Integer addressId;

    @JsonProperty("payment_cod_method")
    private String paymentMenthod;

    @JsonProperty("order_comments")
    private String orderComments;

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getPaymentMenthod() {
        return paymentMenthod;
    }

    public void setPaymentMenthod(String paymentMenthod) {
        this.paymentMenthod = paymentMenthod;
    }

    public String getOrderComments() {
        return orderComments;
    }

    public void setOrderComments(String orderComments) {
        this.orderComments = orderComments;
    }
}
