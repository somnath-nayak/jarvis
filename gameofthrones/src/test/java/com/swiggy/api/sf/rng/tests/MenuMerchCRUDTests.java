package com.swiggy.api.sf.rng.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.constants.MultiTDConstants;
import com.swiggy.api.sf.rng.dp.CarouselBannerDP;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.Processor;
import net.minidev.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MenuMerchCRUDTests extends CarouselBannerDP {
    String[] latLng =  {"12.9326","77.6036"};
    String versionCode="5000";
    SANDHelper sandHelper = new SANDHelper();
    RngHelper rngHelper = new RngHelper();
    List<String> restIds;

    @BeforeClass
    public void getServiceableAndOpenRestaurant(){
        restIds = sandHelper.aggregatorRestList(latLng);
        Assert.assertFalse(restIds.isEmpty(),"Assertion Failed :: No serviceable and open rest found.");
        sandHelper.setSectionsForHighTouch();
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testMenuMerch(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testExpiredSlotCarousel(CarouselPOJO carouselPOJO) throws IOException{
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(),menuItemID;
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));

            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }


            carouselPOJO.getAllDaysTimeSlot().remove(rngHelper.getTodaysDay()-1);
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertFalse(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");


        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }


    public void testCarouselWidgetForSuperUser(CarouselPOJO carouselPOJO) throws IOException{
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(),menuItemID;
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            carouselPOJO.setSuperType("SUPER");
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }
            HashMap<String, String> loginData =  sandHelper.loginData(MultiTDConstants.mobile[1],"swiggy");
            processor = sandHelper.menuV4RestIdWithChannel(restID,loginData.get("Tid"),loginData.get("Token"),versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer>  menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible for super user in rest menu");

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertFalse(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel shouldn't visible for non super user rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }


    public void testCarouselWidgetForNonSuperUser(CarouselPOJO carouselPOJO) throws IOException{
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            carouselPOJO.setSuperType("NOT_SUPER");
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }
            HashMap<String, String> loginData =  sandHelper.loginData(MultiTDConstants.mobile[4],"swiggy");
            processor = sandHelper.menuV4RestIdWithChannel(restID,loginData.get("Tid"),loginData.get("Token"),versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer>  menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible for non super user in rest menu");

        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselPriority(CarouselPOJO carouselPOJO) throws IOException{
        Processor processor;
        String restID, response;
        List<Integer>carouselsID = new ArrayList<>(), menuItemID;
        boolean flag = true;
        List<Integer>  menuCarousels;
        int count, carouselID, priority, size = restIds.size();
        int[] minMaxCount;
        try{
            do {
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID, null, null, versionCode, carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                Assert.assertEquals(JsonPath.read(response, "$.data.menu.layoutType"), "high_touch", "Assertion Failed :: high touch isn't enabled");
                priority = rngHelper.getNextPriority(response);
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode, carouselPOJO.getChannel(), restID);
                if (priority == 100) {
                    count = minMaxCount[1];
                    flag = false;
                } else {
                    menuCarousels = JsonPath.read(response, "$.data.menu.menuCarousels..data.bannerId");
                    count = 1;
                    if (menuCarousels.size() < minMaxCount[1]) {
                        count = minMaxCount[1] - menuCarousels.size();
                        flag= false;
                    }
                }
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while(flag && (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0)));

            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));

            for(int i=0;i<count;i++,priority--) {
                carouselPOJO.setPriority(String.valueOf(priority));
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselsID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselsID),"Assertion Failed :: carousel not visible in rest menu");
            Assert.assertEquals(menuCarousels.size(),minMaxCount[1]);

//          carousels with higher priority should be able to replace others
            carouselPOJO.setPriority(String.valueOf(priority));
            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");
            carouselsID.add(carouselID);
            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);

            Assert.assertEquals(menuCarousels.size(),minMaxCount[1]);
            Assert.assertTrue(menuCarousels.contains(carouselID),"Assertion Failed :: carousel not visible in rest menu");

//          carousels with lower priority shoudln't be able to replace other
            carouselPOJO.setPriority("101");
            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");
            carouselsID.add(carouselID);
            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);

            Assert.assertEquals(menuCarousels.size(),minMaxCount[1]);
            Assert.assertFalse(menuCarousels.contains(carouselID),"Assertion Failed :: carousel not visible in rest menu");


        }finally {
            if(carouselsID.size()!=0) {
                for(int carousel: carouselsID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselMinMaxCount(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        List<Integer> carouselsID = new ArrayList<>(), menuItemID, menuCarousels;

        int count, carouselID, priority, size = restIds.size();
        int[] minMaxCount;
        try {

            do {
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                processor = sandHelper.menuV4RestIdWithChannel(restID, null, null, versionCode, carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                priority = rngHelper.getNextPriority(response);
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (priority!=100 && (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0)));

            rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
            minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);

            carouselPOJO.setPriority(String.valueOf(priority));
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
//          assertion for min count

            if(menuCarousels.size()!=0) {
                Assert.assertFalse(menuCarousels.contains(carouselID), "Assertion Failed :: carousel not visible in rest menu");
            }

//            for max count
//            subtracting extra 1 as one carousel is already created that is not counted in menuCarousels
            count = minMaxCount[1] - menuCarousels.size() -1;

//            as 0 priority is already been used
            priority--;
            for(int i=0;i<count;i++,priority--) {
                carouselPOJO.setPriority(String.valueOf(priority));
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselsID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            menuCarousels = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
//            adding the first carousel
            carouselsID.add(carouselID);
            Assert.assertTrue(menuCarousels.containsAll(carouselsID),"Assertion Failed :: carousel not visible in rest menu");
            Assert.assertEquals(menuCarousels.size(),minMaxCount[1]);

            carouselPOJO.setPriority(String.valueOf(priority));

            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");

            carouselID = JsonPath.read(response, "$.data");
            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.contains(carouselID),"Assertion Failed :: carousel not visible in rest menu");
            Assert.assertEquals(menuCarousels.size(),minMaxCount[1]);
           carouselsID.add(carouselID);

        }finally {
            if(carouselsID.size()!=0) {
                for(int carousel: carouselsID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselForInAndOutofStockItem(CarouselPOJO carouselPOJO) throws IOException{
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuCarousels, menuItemID;
        int count, menuItem = 0, size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());

                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            menuItem =  menuItemID.get(rngHelper.getRandomNo(menuItemID.size()));
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItem));

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }
//          updating sand redis; setting item out of stock
            rngHelper.setInOrOutStock(String.valueOf(menuItem),"false");

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertFalse(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");

//          updating sand redis; setting same item in stock
            rngHelper.setInOrOutStock(String.valueOf(menuItem),"true");

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");

        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }

        }
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselWithFlatTD(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        List<String> tdID = new ArrayList<String>();
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do {
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID, versionCode, carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID, null, null, versionCode, carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            rngHelper.enableTradeDiscountInSAND(restID);
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));

            response = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel("99","20",restID, false, false).ResponseValidator.GetBodyAsText();

            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselWithFreeDeliveryTD(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        List<String> tdID = new ArrayList<String>();
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do {
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID, versionCode, carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID, null, null, versionCode, carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            rngHelper.enableTradeDiscountInSAND(restID);
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));

            response = rngHelper.createFreeDeliveryTD(false, false, false, false, true, restID);
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }
//    free del


//    freebie

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselWithFreebieTD(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response, menuItem;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        List<String> tdID = new ArrayList<String>();
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            rngHelper.enableTradeDiscountInSAND(restID);

            menuItem = String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size())));
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(menuItem);
            response = rngHelper.createFreebieTDWithFirstOrderRestrictionAtRestaurantLevel("0",restID,menuItem).ResponseValidator.GetBodyAsText();

            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }
//    percentage
    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselWithPercentageTD(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response, menuItem;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        List<String> tdID = new ArrayList<String>();
        int count,  size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            rngHelper.enableTradeDiscountInSAND(restID);
            menuItem = String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size())));
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(menuItem);
            response = rngHelper.createPercentageTD(true,restID);

            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

//    category
    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselWithCategoryTD(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        List<String> tdID = new ArrayList<String>();
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            rngHelper.enableTradeDiscountInSAND(restID);
            HashMap<String, String> itemInfo = rngHelper.getItemCategoryAndSubCategory(restID,response);
            response = rngHelper.createFlatCategoryMenu(restID,itemInfo.get("Cat"), true);

            tdID.add(response);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);

            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(itemInfo.get("Item"));
            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

//    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselWithDifferentCategoryTDNegative(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        List<String> tdID = new ArrayList<String>();
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            rngHelper.enableTradeDiscountInSAND(restID);

            HashMap<String, String> itemInfo = rngHelper.getItemCategoryAndSubCategory(restID,response);

            response = rngHelper.createFlatCategoryMenu(restID,itemInfo.get("Cat"), true);
            tdID.add(response);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(itemInfo.get("Item"));

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }


//    subcategory

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselWithSubCategoryTD(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        List<String> tdID = new ArrayList<String>();
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));

            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            rngHelper.enableTradeDiscountInSAND(restID);
            HashMap<String, String> itemInfo = rngHelper.getItemCategoryAndSubCategory(restID,response);
            response = rngHelper.createFlatSubcatagoryMenu(restID,itemInfo.get("Cat"),itemInfo.get("Sub"),itemInfo.get("Item"));

            tdID.add(response);
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(itemInfo.get("Item"));
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselsWithMissingAndInvalidHexFontColor(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        int count;
        int[] minMaxCount;

        restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
        rngHelper.enableHighTouch(restID,versionCode,carouselPOJO.getChannel());
        processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
        response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
        int priority = rngHelper.getNextPriority(response);
        if(priority==100){
            minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
            carouselPOJO.setPriority("100");
            count =minMaxCount[0];
        } else {
            carouselPOJO.setPriority(String.valueOf(priority));
            count=1;
        }

        List<Integer> menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
        carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));
        carouselPOJO.setFontColor("#4799A97");

        for(int i=0;i<count;i++) {
            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 1, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).equalsIgnoreCase("Font color cannot be empty"), "Assertion Failed :: carousel creation failed");
        }

        processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
        response = processor.ResponseValidator.GetBodyAsText();

        menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
        carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));
        carouselPOJO.setFontColor(null);

        for(int i=0;i<count;i++) {
            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 1, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).equalsIgnoreCase("Font color cannot be empty"), "Assertion Failed :: carousel creation failed");
        }
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselExpiredValidity(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response,menuItem = null;
        List<Integer>carouselID = new ArrayList<>(),menuItemID;
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do {
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID, versionCode, carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID, null, null, versionCode, carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response, "$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));

            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            menuItem = String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size())));
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).withEndDateTime(RngHelper.getCurrentDate("yyyy-MM-dd HH:mm:ss")).setRedirectLink(menuItem);

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertFalse(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void testCarouselExpiredTD(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        List<String> tdID = new ArrayList<String>();
        int count, size = restIds.size();
        int[] minMaxCount;
        try{
            do {
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                rngHelper.enableHighTouch(restID, versionCode, carouselPOJO.getChannel());
                processor = sandHelper.menuV4RestIdWithChannel(restID, null, null, versionCode, carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));
            Assert.assertEquals(JsonPath.read(response,"$.data.menu.layoutType"),"high_touch","Assertion Failed :: high touch isn't enabled");
            int priority = rngHelper.getNextPriority(response);
            if(priority==100){
                minMaxCount = rngHelper.getMinMaxCarouselCount(versionCode,carouselPOJO.getChannel(),restID);
                carouselPOJO.setPriority("100");
                count =minMaxCount[0];
            } else {
                carouselPOJO.setPriority(String.valueOf(priority));
                count=1;
            }

            rngHelper.enableTradeDiscountInSAND(restID);
            carouselPOJO.withRestaurantId(Integer.valueOf(restID)).setRedirectLink(String.valueOf(menuItemID.get(rngHelper.getRandomNo(menuItemID.size()))));

            response = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel("99","20",restID, false, true).ResponseValidator.GetBodyAsText();

            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);

            for(int i=0;i<count;i++) {
                response = rngHelper.createCarousel(carouselPOJO);
                Assert.assertEquals((int) JsonPath.read(response, "$.statusCode"), 0, "Assertion Failed :: carousel creation failed");
                Assert.assertTrue(((String) JsonPath.read(response, "$.statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
                carouselID.add(JsonPath.read(response, "$.data"));
            }

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            JSONArray  jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            List<Integer> menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertTrue(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel not visible in rest menu");

            rngHelper.updateExpiredTimeSlotFirstOrderRestrictionAtRestaurantLevel("99","20",restID,Integer.valueOf(tdID.get(0)));

            processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
            jsonArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.menu.menuCarousels..data.bannerId");
            menuCarousels = rngHelper.jsonArrayToArrayList(jsonArray);
            Assert.assertFalse(menuCarousels.containsAll(carouselID),"Assertion Failed :: carousel shouldn't be visible in rest menu");
        }finally {
            if(carouselID.size()!=0) {
                for(int carousel: carouselID)
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carousel).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }


    @Test(dataProvider = "menuMerchCarouselWidgetsWithChannel")
    public void test(CarouselPOJO carouselPOJO) throws IOException {
        Processor processor;
        String restID, response;
        List<Integer>carouselID = new ArrayList<>(), menuItemID;
        List<String> tdID = new ArrayList<String>();
        int count, size = restIds.size();
        int[] minMaxCount;

            do{
                restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
                processor = sandHelper.menuV4RestIdWithChannel(restID,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                menuItemID = JsonPath.read(response,"$.data.menu.items[*].id");
                size--;
            }while (((null == menuItemID) || (menuItemID.size() == 0))&& (size!=0));


            HashMap<String, String> itemInfo = rngHelper.getItemCategoryAndSubCategory(restID,response);
            response = rngHelper.createFlatCategoryMenu(restID,itemInfo.get("Cat"), true);

            tdID.add(response);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);

        }
}
