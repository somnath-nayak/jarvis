package com.swiggy.api.sf.checkout.tests;

import java.io.File;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.PaasConstant;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.dp.PayTMMockDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutMockHelper;
import org.apache.commons.io.FileUtils;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;

public class PayTMMockTest extends PayTMMockDP{

    Initialize gameofthrones = new Initialize();
    CheckoutMockHelper CheckoutMockHelper=new CheckoutMockHelper();
    CheckoutHelper helper= new CheckoutHelper();
    
    String tid;
    String token;
    String userid;
    HashMap<String, String> hashMap;
    
    @BeforeClass
    public void login(){
    	hashMap=helper.TokenData(CheckoutConstants.mobile,CheckoutConstants.password);
    	tid=hashMap.get("Tid");
        token=hashMap.get("Token");
        userid=hashMap.get("CustomerId");
    }
    
   @Test(dataProvider="payTMWithdraw")
   public void placeOrderUsingPayTM(Cart cartPayload) throws IOException{
	   createPayTMMockRes();
	   placeOrder(cartPayload);
   }
   
    public void createPayTMMockRes() throws IOException{
	   String urlPattern=PaasConstant.PAYTM_WITHDRAW_URL;
	   String method=PaasConstant.HTTP_METHOD;
	   String statusCode=PaasConstant.HTTP_200;
	   File file = new File("../Data/MockAPI/ResponseBody/checkout/PayTmWithdrawResponse");
       String resBody = FileUtils.readFileToString(file);	   
	   String[] payload=CheckoutMockHelper.createPaytmWithdrawMockRes(urlPattern, method, statusCode, resBody);
	   Processor processor =CheckoutMockHelper.wiremockMappingNew(payload);
	   Assert.assertEquals(PaasConstant.HTTP_201, processor.ResponseValidator.GetResponseCode(),"wiremock Stub data is failed");
    }
    
    public String placeOrder(Cart cartPayload){
    	String cartResponse=helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cartResponse);
   	    String addressId=helper.getAddressIDfromCartResponse(tid, token, cartResponse);
        String placeOrderRes=helper.orderPlaceV2(tid, token, addressId,PaasConstant.PAYMENT_PAYTM,superConstant.DEF_STRING)
		                            .ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(placeOrderRes);
       String orderID=getOrderId(placeOrderRes);
       return orderID;
   }
    
    public String getOrderId(String placeOrderRes){
    	return JsonPath.read(placeOrderRes, "$.data..order_id").toString().replace("[","").replace("]","");
    }
}