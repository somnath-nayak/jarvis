package com.swiggy.api.sf.snd.pojo.event;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class VariantGroup {

    private String name;
    private List<Variation> variations = null;
    private Integer order;
    @JsonProperty("group_id")
    private String groupId;
    @JsonProperty("external_group_id")
    private String externalGroupId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Variation> getVariations() {
        return variations;
    }

    public void setVariations(List<Variation> variations) {
        this.variations = variations;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getExternalGroupId() {
        return externalGroupId;
    }

    public void setExternalGroupId(String externalGroupId) {
        this.externalGroupId = externalGroupId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("variations", variations).append("order", order).append("groupId", groupId).append("externalGroupId", externalGroupId).toString();
    }

}
