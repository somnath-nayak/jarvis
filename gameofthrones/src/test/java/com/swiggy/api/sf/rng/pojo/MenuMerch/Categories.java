package com.swiggy.api.sf.rng.pojo.MenuMerch;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class Categories {

    @JsonProperty("id")
    private String id;
    @JsonProperty("subCategories")
    private List<SubCategories> subCategories;
    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    public String getId ()
    {
        return id;
    }
    @JsonProperty("id")
    public void setId (String id)
    {
        this.id = id;
    }

    public Categories withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("subCategories")
    public List<SubCategories> getSubCategories ()
    {
        return subCategories;
    }
    @JsonProperty("subCategories")
    public void setSubCategories (List<SubCategories> subCategories)
    {
        this.subCategories = subCategories;
    }

    public Categories withSubCategories(List<SubCategories> subCategories) {
        this.subCategories = subCategories;
        return this;
    }

    @JsonProperty("name")
    public String getName ()
    {
        return name;
    }
    @JsonProperty("name")
    public void setName (String name)
    {
        this.name = name;
    }

    public Categories withName(String name) {
        this.name = name;
        return this;
    }

    public Categories(String id,String name, List<SubCategories> subCategories){
        this.id=id;
        this.name="Category";
        this.subCategories=subCategories;
    }

    public Categories setDefault(){
        return this.withId("12345")
                .withName("CategoryId")
                .withSubCategories(getSubCategories());
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("subCategories",subCategories).toString();
    }
}
