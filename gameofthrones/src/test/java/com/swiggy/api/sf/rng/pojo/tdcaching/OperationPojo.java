package com.swiggy.api.sf.rng.pojo.tdcaching;

public class OperationPojo {
    private Long campaignOperationId = 1L;
    private String opertationType = "RESTAURANT";
    private Long operationId = 1L;
    private String operationMeta = null;

    public void setCampaignOperationId(Long campaignOperationId) {
        this.campaignOperationId = campaignOperationId;
    }

    public void setOpertationType(String opertationType) {
        this.opertationType = opertationType;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }

    public void setOperationMeta(String operationMeta) {
        this.operationMeta = operationMeta;
    }

    public Long getCampaignOperationId() {
        return this.campaignOperationId;
    }

    public String getOpertationType() {
        return this.opertationType;
    }

    public Long getOperationId() {
        return this.operationId;
    }

    public String getOperationMeta() {
        return this.operationMeta;
    }

}
