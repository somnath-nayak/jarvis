package com.swiggy.api.sf.rng.dp;

import com.swiggy.api.sf.rng.helper.EDVOHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.HashMap;

import static com.swiggy.api.sf.rng.helper.TdCachingHelper.getRestaurantIds;

public class TdCachingDP {

    EDVOHelper edvoHelper = new EDVOHelper();

    @DataProvider(name = "guavaEnabledForCampaignTestData")
    public static Object[][] guavaEnabledForCampaignTest() {
        EDVOHelper edvoHelper = new EDVOHelper();
        ArrayList<String> allMealIds = new ArrayList();
        ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
        ArrayList<String> listOfItemIds = new ArrayList<String>();
        HashMap<String, String> cartPayload = new HashMap<String, String>();
        String mealItemRequestPayload = "";

        ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
        ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
        ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
        ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();
        String testRestaurantId = getRestaurantIds();
        String mealId = getRestaurantIds();
        String groupId = getRestaurantIds();

        // list of item count in each meal
        listOfItemCountInMeal.add(0, "1");

        // list of item's price
        listOfAllItemsPrice.add(0, "200");

        // numOfMealItemRequestInCart
        numOfMealItemRequestInCart.add(0, "1");

        listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

        // listOfAllRestaurantIds
        listOfRestIdsInCart.add(0, testRestaurantId);
        allMealIds.add(0,mealId);
        allGroupIdsOfMeals.add(0,groupId);
        mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
                listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
        cartPayload.put("0", "10");
        cartPayload.put("1", "false");
        cartPayload.put("2", "");
        cartPayload.put("3", mealItemRequestPayload);
        cartPayload.put("4", "9901");
        cartPayload.put("5", "\"ANDROID\"");
        cartPayload.put("6", "229");

        return new Object[][] { { cartPayload,testRestaurantId, mealId,groupId} };
    }


    @DataProvider(name = "guavaAndRedisEnabledForCampaignData")
    public static Object[][] guavaAndRedisEnabledForCampaignTest() {
        EDVOHelper edvoHelper = new EDVOHelper();
        ArrayList<String> allMealIds = new ArrayList();
        ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
        ArrayList<String> listOfItemIds = new ArrayList<String>();
        HashMap<String, String> cartPayload = new HashMap<String, String>();
        String mealItemRequestPayload = "";

        ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
        ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
        ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
        ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();
        String testRestaurantId = getRestaurantIds();
        String mealId = getRestaurantIds();
        String groupId = getRestaurantIds();

        // list of item count in each meal
        listOfItemCountInMeal.add(0, "1");

        // list of item's price
        listOfAllItemsPrice.add(0, "200");

        // numOfMealItemRequestInCart
        numOfMealItemRequestInCart.add(0, "1");

        listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

        // listOfAllRestaurantIds
        listOfRestIdsInCart.add(0, testRestaurantId);
        allMealIds.add(0,mealId);
        allGroupIdsOfMeals.add(0,groupId);
        mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
                listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
        cartPayload.put("0", "10");
        cartPayload.put("1", "false");
        cartPayload.put("2", "");
        cartPayload.put("3", mealItemRequestPayload);
        cartPayload.put("4", "9901");
        cartPayload.put("5", "\"ANDROID\"");
        cartPayload.put("6", "229");

        return new Object[][] { { cartPayload,testRestaurantId, mealId,groupId} };
    }

    @DataProvider(name = "redisEnabledForRulesTestData")
    public static Object[][] redisEnabledForRulesTest() {
        EDVOHelper edvoHelper = new EDVOHelper();
        ArrayList<String> allMealIds = new ArrayList();
        ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
        ArrayList<String> listOfItemIds = new ArrayList<String>();
        HashMap<String, String> cartPayload = new HashMap<String, String>();
        String mealItemRequestPayload = "";

        ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
        ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
        ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
        ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();
        String testRestaurantId = getRestaurantIds();
        String mealId = getRestaurantIds();
        String groupId = getRestaurantIds();

        // list of item count in each meal
        listOfItemCountInMeal.add(0, "1");

        // list of item's price
        listOfAllItemsPrice.add(0, "200");

        // numOfMealItemRequestInCart
        numOfMealItemRequestInCart.add(0, "1");

        listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

        // listOfAllRestaurantIds
        listOfRestIdsInCart.add(0, testRestaurantId);
        allMealIds.add(0,mealId);
        allGroupIdsOfMeals.add(0,groupId);
        mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
                listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
        cartPayload.put("0", "100");
        cartPayload.put("1", "false");
        cartPayload.put("2", "");
        cartPayload.put("3", mealItemRequestPayload);
        cartPayload.put("4", "9901");
        cartPayload.put("5", "\"ANDROID\"");
        cartPayload.put("6", "229");

        return new Object[][] { { cartPayload,testRestaurantId, mealId,groupId} };
    }

}
