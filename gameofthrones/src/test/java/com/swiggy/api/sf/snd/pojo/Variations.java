package com.swiggy.api.sf.snd.pojo;

public class Variations
{
    private String id;

    private Catalog_attributes1 catalog_attributes1;

    private String isVeg;

    private String price;

    private String order;

    private String name;

    private String inStock;

    private String uniqueId;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Catalog_attributes1 getCatalog_attributes1 ()
    {
        return catalog_attributes1;
    }

    public void setCatalog_attributes1 (Catalog_attributes1 catalog_attributes1)
    {
        this.catalog_attributes1 = catalog_attributes1;
    }

    public String getIsVeg ()
    {
        return isVeg;
    }

    public void setIsVeg (String isVeg)
    {
        this.isVeg = isVeg;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getOrder ()
    {
        return order;
    }

    public void setOrder (String order)
    {
        this.order = order;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getInStock ()
    {
        return inStock;
    }

    public void setInStock (String inStock)
    {
        this.inStock = inStock;
    }

    public String getUniqueId ()
    {
        return uniqueId;
    }

    public void setUniqueId (String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    public Variations build(){
        setDefaultValues();
        return this;
    }

    public void setDefaultValues(){
        Catalog_attributes1 catalog_attributes1=new Catalog_attributes1();
        catalog_attributes1.build();

            if(this.getId()==null){
                this.setId("905144");
            }
            if(this.getIsVeg()==null){
                this.setIsVeg("0");
            }
            if(this.getPrice()==null){
                this.setPrice("0.0");
            }
            if(this.getOrder()==null){
                this.setOrder("1");
            }
            if(this.getName()==null){
                this.setName("Half");
            }
            if(this.getInStock()==null){
                this.setInStock("1");
            }

            if(this.getUniqueId()==null){
                this.setUniqueId("905144");
            }

        }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", catalog_attributes1 = "+catalog_attributes1+", isVeg = "+isVeg+", price = "+price+", order = "+order+", name = "+name+", inStock = "+inStock+", uniqueId = "+uniqueId+"]";
    }
}
