package com.swiggy.api.sf.rng.pojo.SwiggySuper;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.SwiggySuper
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.sf.rng.helper.SuperMultiTDHelper;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "title",
        "description",
        "name",
        "logo_id",
        "valid_from",
        "valid_till",
        "tenure",
        "base_price",
        "cgst",
        "sgst",
        "price",
        "enabled",
        "created_by",
        "total_available",
        "user_restriction",
        "incentive_mapping",
        "priority",
        "valid_payment_methods",
        "tnc"
})
public class CreatePlanV2 {

    SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();

    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("name")
    private String name;
    @JsonProperty("logo_id")
    private String logo_id;
    @JsonProperty("valid_from")
    private Long valid_from;
    @JsonProperty("valid_till")
    private Long valid_till;
    @JsonProperty("tenure")
    private Integer tenure;
    @JsonProperty("base_price")
    private Integer base_price;
    @JsonProperty("cgst")
    private Double cgst;
    @JsonProperty("sgst")
    private Double sgst;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("total_available")
    private Integer total_available;
    @JsonProperty("user_restriction")
    private Boolean user_restriction;
    @JsonProperty("incentive_mapping")
    private Boolean incentive_mapping;
    @JsonProperty("priority")
    private Integer priority;
    @JsonProperty("valid_payment_methods")
    private String valid_payment_methods;
    @JsonProperty("tnc")
    private List<String> tnc = null;

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("logo_id")
    public String getLogo_id() {
        return logo_id;
    }

    @JsonProperty("logo_id")
    public void setLogo_id(String logo_id) {
        this.logo_id = logo_id;
    }

    @JsonProperty("valid_from")
    public Long getValid_from() {
        return valid_from;
    }

    @JsonProperty("valid_from")
    public void setValid_from(Long valid_from) {
        this.valid_from = valid_from;
    }

    @JsonProperty("valid_till")
    public Long getValid_till() {
        return valid_till;
    }

    @JsonProperty("valid_till")
    public void setValid_till(Long valid_till) {
        this.valid_till = valid_till;
    }

    @JsonProperty("tenure")
    public Integer getTenure() {
        return tenure;
    }

    @JsonProperty("tenure")
    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    @JsonProperty("base_price")
    public Integer getBase_price() {
        return base_price;
    }

    @JsonProperty("base_price")
    public void setBase_price(Integer base_price) {
        this.base_price = base_price;
    }

    @JsonProperty("cgst")
    public Double getCgst() {
        return cgst;
    }

    @JsonProperty("cgst")
    public void setCgst(Double cgst) {
        this.cgst = cgst;
    }

    @JsonProperty("sgst")
    public Double getSgst() {
        return sgst;
    }

    @JsonProperty("sgst")
    public void setSgst(Double sgst) {
        this.sgst = sgst;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("total_available")
    public Integer getTotal_available() {
        return total_available;
    }

    @JsonProperty("total_available")
    public void setTotal_available(Integer total_available) {
        this.total_available = total_available;
    }

    @JsonProperty("user_restriction")
    public Boolean getUser_restriction() {
        return user_restriction;
    }

    @JsonProperty("user_restriction")
    public void setUser_restriction(Boolean user_restriction) {
        this.user_restriction = user_restriction;
    }

    @JsonProperty("incentive_mapping")
    public Boolean getIncentive_mapping() {
        return incentive_mapping;
    }

    @JsonProperty("incentive_mapping")
    public void setIncentive_mapping(Boolean incentive_mapping) {
        this.incentive_mapping = incentive_mapping;
    }

    @JsonProperty("priority")
    public Integer getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @JsonProperty("valid_payment_methods")
    public String getValid_payment_methods() {
        return valid_payment_methods;
    }

    @JsonProperty("valid_payment_methods")
    public void setValid_payment_methods(String valid_payment_methods) {
        this.valid_payment_methods = valid_payment_methods;
    }

    @JsonProperty("tnc")
    public List<String> getTnc() {
        return tnc;
    }

    @JsonProperty("tnc")
    public void setTnc(List<String> tnc) {
        this.tnc = tnc;
    }

    public void setDefaultValues() {

        ArrayList<String> tnc = new ArrayList<>();
        tnc.add("terms");
        tnc.add("conditions");

        if (this.getTitle() == null)
            this.setTitle("Testing Plan");
        if (this.getDescription() == null)
            this.setDescription("Testing plan for automation");
        if (this.getName() == null)
            this.setName("Super MultiTD Plan");
        if (this.getLogo_id() == null)
            this.setLogo_id("Dummy_logo_id");
        if (this.getValid_from() == null)
            this.setValid_from(superMultiTDHelper.getCurrentEpochTimeInMillis());
        if (this.getValid_till() == null)
            this.setValid_till(superMultiTDHelper.getMonthsAddedToCurrentEpochInMillis(5));
        if (this.getTenure() == null)
            this.setTenure(3);
        if (this.getBase_price() == null)
            this.setBase_price(199);
        if (this.getCgst() == null)
            this.setCgst(9.0);
        if (this.getSgst() == null)
            this.setSgst(9.0);
        if (this.getPrice() == null)
            this.setPrice(199);
        if (this.getEnabled() == null)
            this.setEnabled(true);
        if (this.getCreated_by() == null)
            this.setCreated_by("Automation_Super_MultiTD");
        if (this.getTotal_available() == null)
            this.setTotal_available(2000);
        if (this.getUser_restriction() == null)
            this.setUser_restriction(true);
        if (this.getIncentive_mapping() == null)
            this.setIncentive_mapping(true);
        if (this.getValid_payment_methods() == null)
            this.setValid_payment_methods("Cash,Paytm,PhonePe");
        if (this.getTnc() == null)
            this.setTnc(tnc);
    }

    public CreatePlanV2 build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("title", title).append("description", description).append("name", name).append("logo_id", logo_id).append("valid_from", valid_from).append("valid_till", valid_till).append("tenure", tenure).append("base_price", base_price).append("cgst", cgst).append("sgst", sgst).append("price", price).append("enabled", enabled).append("created_by", created_by).append("total_available", total_available).append("user_restriction", user_restriction).append("incentive_mapping", incentive_mapping).append("priority", priority).append("valid_payment_methods", valid_payment_methods).append("tnc", tnc).toString();
    }

}
