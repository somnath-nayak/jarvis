package com.swiggy.api.sf.rng.dp.coupondp;

import com.swiggy.api.sf.rng.helper.CouponHelper;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import org.testng.annotations.DataProvider;

import java.util.HashMap;

public class TDAndCouponCappingDP {

    @DataProvider(name = "cappingTDAsZero")
    public static Object[][] cappingTDAsZero() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",30,10,20);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingTDAsMinusOne")
    public static Object[][] cappingTDAsMinusOne() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",30,10,20);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingTDWithDiscountTypeAsCashback")
    public static Object[][] cappingTDWithDiscountTypeAsCashback() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("CashBack",30,10,20);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }


    @DataProvider(name = "cappingTDWithDiscountTypeAsMarketing")
    public static Object[][] cappingTDWithDiscountTypeAsMarketing() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Marketing",30,100,40);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }
    @DataProvider(name = "cappingTDWithCappingPercentageAsZero")
    public static Object[][] cappingTDWithCappingPercentageAsZero() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",30,100,0);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingTDWithCappingPercentageAsMinusOne")
    public static Object[][] cappingTDWithCappingPercentageAsMinusOne() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",30,100,-1);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingTDWithDiscountPercentageAsLessThanCappingPercentage")
    public static Object[][] cappingTDWithDiscountPercentageAsLessThanCappingPercentage() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",5,100,40);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }
    @DataProvider(name = "cappingTDWithDiscountAmountAsLessThanCappingPercentageAndDiscountPercentage")
    public static Object[][] cappingTDWithDiscountAmountAsLessThanCappingPercentageAndDiscountPercentage() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",30,10,40);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingTDWithCappingPercentageAsHundredPercentage")
    public static Object[][] cappingTDWithCappingPercentageAsHundredPercentage() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",5,40,100);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingTDWithCappingPercentageAsOnePercentage")
    public static Object[][] cappingTDWithCappingPercentageAsOnePercentage() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",30,40,1);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingTDWithCappingPercentageAsHundredAndTenPercentage")
    public static Object[][] cappingTDWithCappingPercentageAsHundredAndTenPercentage() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",30,40,110);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingTDMoreThanHundredWithCappingPercentageAsSeventy")
    public static Object[][] cappingTDMoreThanHundredWithCappingPercentageAsSeventy() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",80,200,70);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }
    @DataProvider(name = "cappingTDMoreThanHundredWithCappingPercentageAsSeventyWithDiscountAmountSameAsCappingCalculatedAmount")
    public static Object[][] cappingTDMoreThanHundredWithCappingPercentageAsSeventyWithDiscountAmountSameAsCappingCalculatedAmount() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",80,150,70);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingTDAsCartValue")
    public static Object[][] cappingTDAsCartValue() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",80,150,70);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingPercentageAsFifty")
    public static Object[][] cappingPercentageAsFifty() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",40,150,50);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cappingPercentageAsFiftyWithCouponDiscountAsFlat")
    public static Object[][] cappingPercentageAsFiftyWithCouponDiscountAsFlat() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",0,199,50);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "cartTotalGreaterThanFiveHundred")
    public static Object[][] cartTotalGreaterThanFiveHundred() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().couponCappingWithTDAsZero("Discount",0,199,50);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }
}
