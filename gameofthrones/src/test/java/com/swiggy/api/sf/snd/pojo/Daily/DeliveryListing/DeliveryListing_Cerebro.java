package com.swiggy.api.sf.snd.pojo.Daily.DeliveryListing;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.pojo.Daily.DeliveryListing
 **/
import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;
        import com.fasterxml.jackson.annotation.JsonAnyGetter;
        import com.fasterxml.jackson.annotation.JsonAnySetter;
        import com.fasterxml.jackson.annotation.JsonIgnore;
        import com.fasterxml.jackson.annotation.JsonInclude;
        import com.fasterxml.jackson.annotation.JsonProperty;
        import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import framework.gameofthrones.JonSnow.DateHelper;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "customer_location",
        "pack_size",
        "restaurant_ids",
        "city_id",
        "time_slot",
        "customer_segment",
        "request_id",
        "session_id",
        "cluster_debug_enabled"
})
public class DeliveryListing_Cerebro {

    @JsonProperty("customer_location")
    private Customer_location customer_location;
    @JsonProperty("pack_size")
    private Integer pack_size;
    @JsonProperty("restaurant_ids")
    private List<String> restaurant_ids = new ArrayList<String>();
    @JsonProperty("city_id")
    private Integer city_id;
    @JsonProperty("time_slot")
    private Time_slot time_slot;
    @JsonProperty("customer_segment")
    private Customer_segment customer_segment;
    @JsonProperty("request_id")
    private String request_id;
    @JsonProperty("session_id")
    private String session_id;
    @JsonProperty("cluster_debug_enabled")
    private Boolean cluster_debug_enabled;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("customer_location")
    public Customer_location getCustomer_location() {
        return customer_location;
    }

    @JsonProperty("customer_location")
    public void setCustomer_location(Customer_location customer_location) {
        this.customer_location = customer_location;
    }

    @JsonProperty("pack_size")
    public Integer getPack_size() {
        return pack_size;
    }

    @JsonProperty("pack_size")
    public void setPack_size(Integer pack_size) {
        this.pack_size = pack_size;
    }

    @JsonProperty("restaurant_ids")
    public List<String> getRestaurant_ids() {
        return restaurant_ids;
    }

    @JsonProperty("restaurant_ids")
    public void setRestaurant_ids(List<String> restaurant_ids) {
        this.restaurant_ids = restaurant_ids;
    }

    @JsonProperty("city_id")
    public Integer getCity_id() {
        return city_id;
    }

    @JsonProperty("city_id")
    public void setCity_id(Integer city_id) {
        this.city_id = city_id;
    }

    @JsonProperty("time_slot")
    public Time_slot getTime_slot() {
        return time_slot;
    }

    @JsonProperty("time_slot")
    public void setTime_slot(Time_slot time_slot) {
        this.time_slot = time_slot;
    }

    @JsonProperty("customer_segment")
    public Customer_segment getCustomer_segment() {
        return customer_segment;
    }

    @JsonProperty("customer_segment")
    public void setCustomer_segment(Customer_segment customer_segment) {
        this.customer_segment = customer_segment;
    }

    @JsonProperty("request_id")
    public String getRequest_id() {
        return request_id;
    }

    @JsonProperty("request_id")
    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    @JsonProperty("session_id")
    public String getSession_id() {
        return session_id;
    }

    @JsonProperty("session_id")
    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    @JsonProperty("cluster_debug_enabled")
    public Boolean getCluster_debug_enabled() {
        return cluster_debug_enabled;
    }

    @JsonProperty("cluster_debug_enabled")
    public void setCluster_debug_enabled(Boolean cluster_debug_enabled) {
        this.cluster_debug_enabled = cluster_debug_enabled;
    }

    private void setDefaultValues(long start_time, long end_time, String lat, String lng, int city_id) {
        DateHelper dateHelper = new DateHelper();
        if(this.getTime_slot() == null)
            this.setTime_slot(new Time_slot().build(start_time,end_time));
        if(this.getCustomer_location() == null)
            this.setCustomer_location(new Customer_location().build(lat,lng));
        if(this.getCity_id() == null)
            this.setCity_id(city_id);
        if(this.getRestaurant_ids() == null)
            this.setRestaurant_ids(new ArrayList<>());
        if(this.getRequest_id() == null)
            this.setRequest_id(String.valueOf(dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss()));
        if(this.getSession_id() == null)
            this.setSession_id(String.valueOf(dateHelper.getCurrentEpochTimeInSeconds()));
        if(this.getCluster_debug_enabled() == null)
            this.setCluster_debug_enabled(false);
        if(this.getCustomer_segment() == null)
            this.setCustomer_segment(new Customer_segment());
        if(this.getPack_size() == null)
            this.setPack_size(1);
    }

    public DeliveryListing_Cerebro build(long start_time, long end_time, String lat, String lng, int city_id) {
        setDefaultValues(start_time,end_time,lat,lng,city_id);
        return this;
    }

    //Overload
    public DeliveryListing_Cerebro build(String start_time, String end_time, String lat, String lng, String city_id) {
        setDefaultValues(Long.parseLong(start_time),Long.parseLong(end_time),lat,lng,Integer.parseInt(city_id));
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("customer_location", customer_location).append("pack_size", pack_size).append("restaurant_ids", restaurant_ids).append("city_id", city_id).append("time_slot", time_slot).append("customer_segment", customer_segment).append("request_id", request_id).append("session_id", session_id).append("cluster_debug_enabled", cluster_debug_enabled).append("additionalProperties", additionalProperties).toString();
    }

}
