package com.swiggy.api.sf.rng.pojo.edvo;

import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu.ItemRequests;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class EDVOCartV3Pogo {
    @JsonProperty("minCartAmount")
    private int minCartAmount;
    @JsonProperty("firstOrder")
    private boolean firstOrder = false;
    @JsonProperty("itemRequests")
    private  List<ItemRequests> itemRequests;
    @JsonProperty("mealItemRequests")
    private List<MealItemRequest> mealItemRequests;
    @JsonProperty("restaurantFirstOrder")
    private boolean restaurantFirstOrder;
    @JsonProperty("userId")
    private long userId;
    @JsonProperty("userAgent")
    private String userAgent ="ANDROID";
    @JsonProperty("versionCode")
    private String versionCode = "400";

    public EDVOCartV3Pogo(){}

public EDVOCartV3Pogo(int minCartAmount, boolean firstOrder, List<ItemRequests> itemRequests, List<MealItemRequest> mealItemRequests, boolean restaurantFirstOrder, long userId){
    super();
    this.minCartAmount = minCartAmount;
    this.firstOrder = firstOrder;
    this.itemRequests = itemRequests;
    this.mealItemRequests = mealItemRequests ;
    this.restaurantFirstOrder = restaurantFirstOrder;
    this.userId = userId;
}
    public int getMinCartAmount() {
        return minCartAmount;
    }

    public void setMinCartAmount(int minCartAmount) {
        this.minCartAmount = minCartAmount;
    }

    public boolean isFirstOrder() {
        return firstOrder;
    }

    public void setFirstOrder(boolean firstOrder) {
        this.firstOrder = firstOrder;
    }

    public List<ItemRequests> getItemRequests() {
        return itemRequests;
    }

    public void setItemRequests(List<ItemRequests> itemRequests) {
        this.itemRequests = itemRequests;
    }

    public List<MealItemRequest> getMealItemRequests() {
        return mealItemRequests;
    }

    public void setMealItemRequests(List<MealItemRequest> mealItemRequests) {
        this.mealItemRequests = mealItemRequests;
    }

    public boolean isRestaurantFirstOrder() {
        return restaurantFirstOrder;
    }

    public void setRestaurantFirstOrder(boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }


}
