package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import java.util.ArrayList;
import java.util.List;

public class CollectionLayoutBuilder {
    private CollectionLayout collectionLayout;
    private CampaignSlots campaignSlots;
    private CollectionValidate collectionValidate;

    public CollectionLayoutBuilder()
    {
        collectionLayout = new CollectionLayout();
        campaignSlots= new CampaignSlots();
        collectionValidate= new CollectionValidate();
    }

    public CollectionLayoutBuilder collectionId(Integer id) {
       collectionLayout.setCollectionId(id);
       return this;
    }

    public CollectionLayoutBuilder enabledLayouts(EnabledLayouts enabledLayouts) {
        collectionLayout.setEnabledLayouts(enabledLayouts);
        return this;
    }

    private void defaultCollectionLayout()  {
        if(collectionLayout.getCollectionId()==null)
            collectionLayout.setCollectionId(23);

        if(collectionLayout.getEnabledLayouts()==null)
            collectionLayout.setEnabledLayouts(new EnabledLayouts());
    }

    public CollectionLayout buildCollectionLayout() {
        defaultCollectionLayout();
        return collectionLayout;
    }

    public CollectionLayoutBuilder collection(Integer id) {
        campaignSlots.setCollection(id);
        return this;
    }

    public CollectionLayoutBuilder enabled(Integer id) {
        campaignSlots.setEnabled(id);
        return this;
    }

    public CollectionLayoutBuilder priority(Priority pri) {
        campaignSlots.setPriority(pri);
        return this;
    }

    public CollectionLayoutBuilder startTime(String start) {
        campaignSlots.setStartTime(start);
        return this;
    }

    public CollectionLayoutBuilder endTime(String end) {
        campaignSlots.setEndTime(end);
        return this;
    }

    public CollectionLayoutBuilder slots(List<Slot> slot) {
        campaignSlots.setSlots(slot);
        return this;
    }



    private void defaultCampaign(){
        if(campaignSlots.getCollection()==null)
            campaignSlots.setCollection(23);
        if(campaignSlots.getEnabled()==null)
            campaignSlots.setEnabled(1);
        if(campaignSlots.getPriority()==null)
            campaignSlots.setPriority(new Priority());
        if(campaignSlots.getStartTime()==null)
            campaignSlots.setStartTime("12222222222");
        if(campaignSlots.getEndTime()==null)
            campaignSlots.setEndTime("12222222222");
        if(campaignSlots.getSlots()==null){
            List<Slot> slots= new ArrayList<>();
            Slot slot=new Slot();
            if(slot.getDay()==null)
                slot.setDay("MON");
            if(slot.getOpenTime()==null)
                slot.setOpenTime(233);
            if(slot.getCloseTime()==null)
                slot.setCloseTime(233);
            slots.add(slot);
            campaignSlots.setSlots(slots);
        }
    }

    public CampaignSlots buildCampagin() {
        defaultCampaign();
        return campaignSlots;
    }

    public CollectionLayoutBuilder collectins(String name) {
        collectionValidate.setName(name);
        return this;
    }

    public CollectionLayoutBuilder enable(boolean value) {
        collectionValidate.setEnabled(value);
        return this;
    }

    public CollectionLayoutBuilder entityType(String entity) {
        collectionValidate.setEntityType(entity);
        return this;
    }

    public CollectionLayoutBuilder description(String desc) {
        collectionValidate.setDescription(desc);
        return this;
    }

    public CollectionLayoutBuilder slug(String slug) {
        collectionValidate.setSlug(slug);
        return this;
    }

    public CollectionLayoutBuilder enabledFilters(EnabledFilters filter) {
        collectionValidate.setEnabledFilters(filter);
        return this;
    }

    private void defaultCollectionData(){
        if(collectionValidate.getName()==null)
            collectionValidate.setName("Collections");
        if(collectionValidate.getEnabled()==null)
            collectionValidate.setEnabled(true);
        if(collectionValidate.getEntityType()==null)
            collectionValidate.setEntityType("ITEM");
        if(collectionValidate.getDescription()==null)
            collectionValidate.setDescription("122222");
        if(collectionValidate.getSlug()==null)
            collectionValidate.setSlug("12222222222");
        if(collectionValidate.getEnabledFilters()==null)
            collectionValidate.setEnabledFilters(new EnabledFilters());
    }

    public CollectionValidate buildCollectionData() {
        defaultCollectionData();
        return collectionValidate;
    }




}
