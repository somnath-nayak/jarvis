package com.swiggy.api.sf.rng.pojo.TDTemplate;

import java.util.ArrayList;
import java.util.List;

public class TemplateValidationBuilder {


    public TemplateValidation templateValidation;

    public TemplateValidationBuilder() {
        templateValidation = new TemplateValidation();
    }


    public TemplateValidationBuilder withTemplateIds(List<Integer> templateIds) {
        templateValidation.setTemplateIds(templateIds);

        return this;
    }


    public TemplateValidation build() {
        getDefaultValue();
        return templateValidation;
    }

    private void getDefaultValue() {
        if (templateValidation.getTemplateIds() == null) {
            List<Integer> templateIds = new ArrayList<>();
            templateIds.add(1);
            templateIds.add(2);
            templateValidation.setTemplateIds(templateIds);
        }
    }
}
