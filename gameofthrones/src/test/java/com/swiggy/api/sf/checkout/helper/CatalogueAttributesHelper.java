package com.swiggy.api.sf.checkout.helper;

import com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId.Data;
import com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId.GetMultipleItemsUsingItemId;
import com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId.VariantGroup;
import com.swiggy.api.sf.checkout.constants.CatalogueAttributesConstants;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.*;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartMenuItem;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.getLastOrderResponse.GetLastOrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.getSingleOrderResponse.GetSingleOrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.OrderItem;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.OrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.Category;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class CatalogueAttributesHelper implements CatalogueAttributesConstants {
    EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
    HashMap<String, String> headers = new HashMap<>();
    Processor cmsProcessor;
    GetMultipleItemsUsingItemId getMultipleItemsUsingItemId;
    RngHelper rngHelper = new RngHelper();
    Long orderId;
    String accompaniments = "";
    String spiceLevel = "";
    String vegClassifier = "";
    String portionSize = "";


    //Check if all the itemIds provided belongs to same restaurant
    public void checkIfAllItemsBelongToSameRestaurant(String[] itemIds, Processor processor){
        GetMultipleItemsUsingItemId getMultipleItemsUsingItemId = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), GetMultipleItemsUsingItemId.class);
        if (itemIds != null & itemIds.length>1) {
            Integer[] restIds = new Integer[itemIds.length];
            List<Data> dataList = getMultipleItemsUsingItemId.getData();
            for (int i = 0; i<itemIds.length; i++) {
                restIds[i] = dataList.get(i).getRestaurantId();
            }

            for (int i = 0; i<itemIds.length-1; i++) {
                if(!restIds[i].equals(restIds[i+1])){
                    Reporter.log("[ALL ITEMS DOES NOT BELONG TO SAME RESTAURANT]", true);
                    Assert.fail("[ALL ITEMS DOES NOT BELONG TO SAME RESTAURANT]");
                }
            }
        }
    }

    //Update CartV2 and get the Processor
    public Processor updateCartV2Processor(String[] itemIds, boolean isAddonRequired, String cartType){
        Cart cart = getCartV2Payload(itemIds, isAddonRequired, cartType);
        String payload = Utility.jsonEncode(cart);
        headers = edvoCartHelper.getHeaders(CatalogueAttributesConstants.MOBILE, CatalogueAttributesConstants.PASSWORD);
        return edvoCartHelper.createCartV2(payload, headers);
    }

    //Update CartV2 and get the Pojo
    public CartV2Response updateCartV2(String[] itemIds, boolean isAddonRequired, String cartType){
        Cart cart = getCartV2Payload(itemIds, isAddonRequired, cartType);
        String payload = Utility.jsonEncode(cart);
        headers = edvoCartHelper.getHeaders(CatalogueAttributesConstants.MOBILE, CatalogueAttributesConstants.PASSWORD);
        Processor processor = edvoCartHelper.createCartV2(payload, headers);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
    }

    //Get Cart
    public CartV2Response getCartV2(HashMap<String, String> header){
        Processor processor = edvoCartHelper.getCart(header);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
    }

    //Get Cart
    public CartV2Response getCartV2(){
        return getCartV2(headers);
    }

    //Get Check Totals
    public CartV2Response checkTotals(String[] itemIds, boolean isAddonRequired, String cartType){
        return checkTotals(getCartV2Payload(itemIds, isAddonRequired, cartType));
    }

    //Get Check Totals
    public CartV2Response checkTotals(Cart cart){
        Processor processor = edvoCartHelper.checkTotalCart(Utility.jsonEncode(cart));
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
    }

    //Get Check Totals
    public CartV2Response checkTotals(String payload){
        Processor processor = edvoCartHelper.checkTotalCart(payload);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
    }

    //Get Cart Minimal
    public CartV2Response cartMinimal(String[] itemIds, boolean isAddonRequired, String cartType){
        Cart cart = getCartV2Payload(itemIds, isAddonRequired, cartType);
        headers = getHeaders(CatalogueAttributesConstants.MOBILE, CatalogueAttributesConstants.PASSWORD);
        Processor processor = edvoCartHelper.createMinimalCart(Utility.jsonEncode(cart), headers);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
    }

    //Gets the CartV2 payload from CMS-API
    public Cart getCartV2Payload(String[] itemIds, boolean isAddonRequired, String cartType){
        cmsProcessor = edvoCartHelper.getItemFromCMS(itemIds, true);
        //Checking if all the item ID's belong to same restaurant
        checkIfAllItemsBelongToSameRestaurant(itemIds, cmsProcessor);
        getMultipleItemsUsingItemId = Utility.jsonDecode(cmsProcessor.ResponseValidator.GetBodyAsText(), GetMultipleItemsUsingItemId.class);
        List<Data> dataList = getMultipleItemsUsingItemId.getData();
        Cart cart = new CartBuilder()
                .cartItems(getCartMenuItemFromCMSAPI(dataList, 1, isAddonRequired))
                .restaurantId(String.valueOf(dataList.get(0).getRestaurantId()))
                .cartType(cartType)
                .buildCart();
        return cart;
    }

    //Gets the CART_MENU_ITEM from CMS-API
    public List<CartItem> getCartMenuItemFromCMSAPI(List<Data> dataList, Integer quantity, boolean isAddonRequired){
        List<CartItem> cartItems = new ArrayList<>();
        CartItem cartItem = null;

        for (Data data : dataList) {
            cartItem = new CartItemBuilder()
                    .menuItemId(String.valueOf(data.getId()))
                    .variants(getVariants(data))
                    .addons(getAddons(data, isAddonRequired))
                    .quantity(quantity)
                    .build();
            cartItems.add(cartItem);
        }
        return cartItems;
    }

    //Gets the variant list for cart, if available for provided ITEM ID
    public List<Variant> getVariants(Data data){
        List<Variant> variants = new ArrayList<>();
        Variant variant = new Variant();
        List<VariantGroup> variantGroups = new ArrayList<>();

        try{
            variantGroups = data.getVariants().getVariantGroups();
        }catch (NullPointerException e){
            Reporter.log("[NO VARIANTS FOUND FOR ITEM_ID '" + data.getId() + "']", true);
            return variants;
        }

        for(VariantGroup variantGroup : variantGroups){
            variant.setGroupId(Integer.valueOf(variantGroup.getGroupId()));
            variant.setVariationId(variantGroup.getVariations().get(0).getId());
            variants.add(variant);
        }

        return variants;
    }

    //Gets the Addon list for Cart from CMS-API
    public List<Addon> getAddons(Data data, boolean isAddonRequired){
        List<Addon> addons = new ArrayList<>();
        Set<String> addonKeys;
        Addon addon = new Addon();
        List<String> mandatoryAddonKeys = new ArrayList<>();

        try {
            addonKeys = data.getAddonMap().keySet();
        }catch (NullPointerException e){
            if(isAddonRequired){
                Assert.fail("'isAddonRequired' flag is 'true', but no addon found for itemId '" + data.getId() + "'...!!");
            }
            return addons;
        }

        //Check if any mandatory Addon is present
        for (String addonKey: addonKeys) {
            if(data.getAddonMap().get(addonKey).getMinAddons() > 0){
                mandatoryAddonKeys.add(addonKey);
            }
        }
        //If mandatory addon is present, then adding the respective addon
        if (mandatoryAddonKeys.size()>0){
            Set<String> choices;
            for(int i=0; i<mandatoryAddonKeys.size(); i++){
                addon.setGroupId(String.valueOf(data.getAddonMap().get(mandatoryAddonKeys.get(i)).getGroupId()));
                choices = data.getAddonMap().get(mandatoryAddonKeys.get(i)).getChoices().keySet();
                addon.setChoiceId((String)choices.toArray()[0]);
                addons.add(addon);
            }
        }

        //If addon is required and mandatory addon is not present then adding the first addon
        if(isAddonRequired && mandatoryAddonKeys.size()==0){
            com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId.Addon addon1 = data.getAddonMap()
                    .get(addonKeys.toArray()[0]);
            addon.setGroupId(String.valueOf(addon1.getGroupId()));
            Set<String> choices = addon1.getChoices().keySet();
            addon.setChoiceId((String)choices.toArray()[0]);
            addons.add(addon);
        }

        return addons;
    }

    public HashMap<String,String> getHeaders(String mobile, String password){
        return edvoCartHelper.getHeaders(mobile, password);
    }

    public void validateCartV2(String[] itemIds, CartV2Response cartV2Response, boolean isAddonRequired){
        validateCartForAttributes(cartV2Response, true, false);
    }

    public void validateCartForAttributes(CartV2Response cartV2Response, boolean isItemLevelCheckRequired,
                                        boolean isAddonLevelCheckRequired){
        List<CartMenuItem> cartMenuItems = cartV2Response.getData().getCartMenuItems();
        //Checks for Attributes for Cart Menu Item
        for (CartMenuItem cartMenuItem : cartMenuItems) { //getMultipleItemsUsingItemId
            List<com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Variant> variants = cartMenuItem.getVariants();
            List<com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Addon> addons = cartMenuItem.getAddons();

            //Checks if Attributes is present at Item Level
            if(isItemLevelCheckRequired) {
                Assert.assertNotNull(cartMenuItem.getAttributes(), "Attributes are null for itemId = '" +
                        cartMenuItem.getMenuItemId() + "'");
                for(Data item : getMultipleItemsUsingItemId.getData()){
                    accompaniments = cartMenuItem.getAttributes().getAccompaniments();
                    spiceLevel = cartMenuItem.getAttributes().getSpiceLevel();
                    vegClassifier = cartMenuItem.getAttributes().getVegClassifier();
                    portionSize = cartMenuItem.getAttributes().getPortionSize();
                    if(cartMenuItem.getMenuItemId().equals(item.getId())){
                        //Validates Accompaniments at Item Level
                        if(accompaniments!=null && !accompaniments.equals("")) {
                            Assert.assertTrue(cartMenuItem.getAttributes().getAccompaniments().contains(
                                    item.getCatalogAttributes().getAccompaniments().get(0)),
                                    "Accompaniments mismatch at Item Level for Item Id '" + cartMenuItem.getMenuItemId() + "'");
                        }
                        //Validates Spice Level at Item Level
                        if(spiceLevel!=null && !spiceLevel.equals("")) {
                            Assert.assertEquals(cartMenuItem.getAttributes().getSpiceLevel(),
                                    item.getCatalogAttributes().getSpiceLevel(),
                                    "Spice Level mismatch at Item Level for Item Id '" + cartMenuItem.getMenuItemId() + "'");
                        }
                        //Validates Veg-Classifier at Item Level
                        if(vegClassifier!=null && !vegClassifier.equals("")) {
                            Assert.assertEquals(cartMenuItem.getAttributes().getVegClassifier(),
                                    item.getCatalogAttributes().getVegClassifier(),
                                    "Veg-Classifier mismatch at Item Level for Item Id '" + cartMenuItem.getMenuItemId() + "'");
                        }
                        //Validates Portion-Size at Item Level
                        if(portionSize!=null && !portionSize.equals("")) {
                            Assert.assertEquals(cartMenuItem.getAttributes().getPortionSize(),
                                    "Serves " + item.getCatalogAttributes().getServesHowMany(),
                                    "Portion-Size mismatch at Item Level for Item Id '" + cartMenuItem.getMenuItemId() + "'");
                        }

                        //Checks if Attributes are present at Variant Level
                        if(variants.size()>0) {
                            for (com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Variant variant : variants) {
                                //Checks if Attributes are null at Variant Level
                                Assert.assertNotNull(variant.getAttributes(),
                                        "Attributes are not present at Variant Level for " +
                                        "itemId = '" + cartMenuItem.getMenuItemId() + "' and variant group ID = '" +
                                                variant.getGroupId() + "'");

                                for(VariantGroup variantGroup : item.getVariants().getVariantGroups() ){
                                    if(variant.getGroupId().equals(variantGroup.getGroupId())){
                                        accompaniments = variant.getAttributes().getVegClassifier();
                                        spiceLevel = variant.getAttributes().getSpiceLevel();
                                        vegClassifier = variant.getAttributes().getVegClassifier();
                                        portionSize = variant.getAttributes().getPortionSize();

                                        //Validates Accompaniments at Variant Level
                                        if(accompaniments != null && !accompaniments.equals("")) {
                                            Assert.assertEquals(accompaniments,
                                                    variantGroup.getVariations().get(0).getCatalogAttributes().getAccompaniments(),
                                                    "Accompaniments mismatch at Variant Level for Item Id '" + cartMenuItem.getMenuItemId() + "'");
                                        }
                                        //Validates Spice Level at Item Level
                                        if(spiceLevel != null && !spiceLevel.equals("")) {
                                            Assert.assertEquals(spiceLevel,
                                                    variantGroup.getVariations().get(0).getCatalogAttributes().getSpiceLevel(),
                                                    "Spice Level mismatch at Variant Level for Item Id '" + cartMenuItem.getMenuItemId() + "'");
                                        }
                                        //Validates Veg-Classifier at Item Level
                                        if(vegClassifier != null && !vegClassifier.equals("")) {
                                            Assert.assertEquals(vegClassifier,
                                                    variantGroup.getVariations().get(0).getCatalogAttributes().getVegClassifier(),
                                                    "Veg-Classifier mismatch at Variant Level for Item Id '" + cartMenuItem.getMenuItemId() + "'");
                                        }
                                        //Validates Portion-Size at Item Level
                                        if(portionSize != null && !portionSize.equals("")) {
                                            Assert.assertEquals(portionSize,
                                                    "Serves " + variantGroup.getVariations().get(0).getCatalogAttributes().getServesHowMany(),
                                                    "Portion-Size mismatch at Variant Level for Item Id '" + cartMenuItem.getMenuItemId() + "'");
                                        }
                                    }
                                }
                            }
                        }

                        //Checks if Attributes are present at Addon Level
                        if(isAddonLevelCheckRequired) {
                            for (com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Addon addon : addons) {
                                Assert.assertNotNull(addon, "Attributes are not present at Variant Level for " +
                                        "itemId = '" + cartMenuItem.getMenuItemId() + "' and addon group ID = '" + addon.getGroupId() + "'");
                            }
                        }
                    }


                }
            }
        }


    }

    //Creates Trade Discount based on the parameter provided
    public HashMap<String, String> createOrUpdateTradeDiscount(String restaurantId, String tdType){
        HashMap<String, String> keys = new HashMap<>();
        rngHelper.disabledActiveTD(restaurantId);
        if(tdType.equals(CatalogueAttributesConstants.FREE_DELIVERY)) {
            keys = createFreeDelTD(CatalogueAttributesConstants.MIN_CART_AMOUNT, restaurantId, CatalogueAttributesConstants.FIRST_ORDER_RESTRICTION,
                    CatalogueAttributesConstants.RESTAURANT_FIRST_ORDER, CatalogueAttributesConstants.USER_RESTRICTION, CatalogueAttributesConstants.DORMANT_USER_TYPE,
                    CatalogueAttributesConstants.SURGE);
        }else if(tdType.equals(CatalogueAttributesConstants.FREEBIE)){
            keys = createFreebieTD(CatalogueAttributesConstants.MIN_CART_AMOUNT, restaurantId, getItemIdForFreebie(restaurantId));
            Utility.wait(CatalogueAttributesConstants.FREEBIE_CACHE_DELAY);
            System.out.println("FREEBIE Created Successfully...");
        }else if (tdType.equals(CatalogueAttributesConstants.FLAT_TD)){
            List<RestaurantList> restaurantLists = new ArrayList<>();
            RestaurantList restaurantList = new RestaurantList();
            restaurantList.setId(restaurantId);
            restaurantList.setCategories(new ArrayList<Category>());
            restaurantList.setName("SP");
            restaurantLists.add(restaurantList);
            try {
                rngHelper.createFlatWithNoMinAmountAtCategoryLevel(restaurantLists, CatalogueAttributesConstants.MIN_CART_AMOUNT, CatalogueAttributesConstants.FLAT_TD_AMOUNT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if (tdType.equals(CatalogueAttributesConstants.PERCENTAGE_TD)){
            try {
                rngHelper.createPercentageWithNoMinAmountAtRestaurantLevel(restaurantId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return keys;
    }

    //Creates Free Delivery Trade Discount for provided Restaurant ID
    public HashMap<String, String> createFreeDelTD(String minCartAmount, String restId, boolean firstOrderRestriction,
                                                   boolean restaurantFirstOrder, boolean userRestriction, String dormant_user_type, boolean isSurge){
        HashMap<String, String> keys = new HashMap<>();
        try {
            keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, firstOrderRestriction,
                    restaurantFirstOrder, userRestriction, dormant_user_type, isSurge);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return keys;
    }

    //Returns the Item-Id from a restaurant to create a "FREEBIE" TD
    public String getItemIdForFreebie(String restaurantId){
        List<CartItem> cartItems = edvoCartHelper.getCartItems(restaurantId, 1, false, true);
        return cartItems.get(0).getMenuItemId();
    }

    //Creates Freebie Trade Discount for provided Restaurant ID and provided Item ID
    public HashMap<String, String> createFreebieTD(String minCartAmount, String restId,
                                                   String freebieItemId){
        HashMap<String, String> keys = new HashMap<>();
        try {
            keys = rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, freebieItemId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return keys;
    }

    //Disables active TD
    public void disableActiveTD(String restaurantId){
        rngHelper.disabledActiveTD(restaurantId);
    }

    //Place Order
    public OrderResponse placeOrder(String[] itemIds, boolean isAddonRequired, String cartType, String paymentMethod,
                           String orderComments){
        CartV2Response cartV2Response = updateCartV2(itemIds, isAddonRequired, cartType);
        String address = edvoCartHelper.getValidAddress(cartV2Response);
        if(address == null){
            address = edvoCartHelper.addNewAddressFromCartV2Response(cartV2Response);
        }
        Processor processor = edvoCartHelper.placeOrder(address, paymentMethod, orderComments);
        OrderResponse orderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(),
                OrderResponse.class);
        return orderResponse;
    }

    //Validates Order Response for Catalogue Attributes
    public void validateOrderForAttributes(String[] itemIds, OrderResponse orderResponse, boolean isItemLevelCheckRequired, boolean isAddonReqired){
        List<OrderItem> orderItems = orderResponse.getData().getOrderItems();

        //Checks if all items provided are present in the order response
        checkIfAllItemsArePresentInOrderResponse(itemIds, orderItems, isAddonReqired);
        //Checks for Attributes for Cart Menu Item
        for(OrderItem orderItem : orderItems) {
            List<com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Variant> variants = orderItem.getVariants();
//            List<com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Addon> addons = orderItem.getAddons();

            //Checks if Attributes is present at Item Level
            if (isItemLevelCheckRequired) {
                Assert.assertNotNull(orderItem.getAttributes(), "Attributes are null for itemId = '" +
                        orderItem.getItemId() + "'");
                for (Data item : getMultipleItemsUsingItemId.getData()) {
                    accompaniments = orderItem.getAttributes().getAccompaniments();
                    spiceLevel = orderItem.getAttributes().getSpiceLevel();
                    vegClassifier = orderItem.getAttributes().getVegClassifier();
                    portionSize = orderItem.getAttributes().getPortionSize();
                    if (orderItem.getItemId().equals(item.getId())) {
                        //Validates Accompaniments at Item Level
                        if (accompaniments != null && !accompaniments.equals("")) {
                            Assert.assertTrue(orderItem.getAttributes().getAccompaniments().contains(
                                    item.getCatalogAttributes().getAccompaniments().get(0)),
                                    "Accompaniments mismatch at Item Level for Item Id '" + orderItem.getItemId() + "'");
                        }
                        //Validates Spice Level at Item Level
                        if (spiceLevel != null && !spiceLevel.equals("")) {
                            Assert.assertEquals(orderItem.getAttributes().getSpiceLevel(),
                                    item.getCatalogAttributes().getSpiceLevel(),
                                    "Spice Level mismatch at Item Level for Item Id '" + orderItem.getItemId() + "'");
                        }
                        //Validates Veg-Classifier at Item Level
                        if (vegClassifier != null && !vegClassifier.equals("")) {
                            Assert.assertEquals(orderItem.getAttributes().getVegClassifier(),
                                    item.getCatalogAttributes().getVegClassifier(),
                                    "Veg-Classifier mismatch at Item Level for Item Id '" + orderItem.getItemId() + "'");
                        }
                        //Validates Portion-Size at Item Level
                        if (portionSize != null && !portionSize.equals("")) {
                            Assert.assertEquals(orderItem.getAttributes().getPortionSize(),
                                    "Serves " + item.getCatalogAttributes().getServesHowMany(),
                                    "Portion-Size mismatch at Item Level for Item Id '" + orderItem.getItemId() + "'");
                        }

                        //Checks if Attributes are present at Variant Level
                        if(variants.size()>0) {
                            for (com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Variant variant : variants) {
                                //Checks if Attributes are null at Variant Level
                                Assert.assertNotNull(variant.getAttributes(),
                                        "Attributes are not present at Variant Level for " +
                                                "itemId = '" + orderItem.getItemId() + "' and variant group ID = '" +
                                                variant.getGroupId() + "'");

                                for(VariantGroup variantGroup : item.getVariants().getVariantGroups() ){
                                    if(variant.getGroupId().equals(variantGroup.getGroupId())){
                                        accompaniments = variant.getAttributes().getVegClassifier();
                                        spiceLevel = variant.getAttributes().getSpiceLevel();
                                        vegClassifier = variant.getAttributes().getVegClassifier();
                                        portionSize = variant.getAttributes().getPortionSize();

                                        //Validates Accompaniments at Variant Level
                                        if(accompaniments != null && !accompaniments.equals("")) {
                                            Assert.assertEquals(accompaniments,
                                                    variantGroup.getVariations().get(0).getCatalogAttributes().getAccompaniments(),
                                                    "Accompaniments mismatch at Variant Level for Item Id '" + orderItem.getItemId() + "'");
                                        }
                                        //Validates Spice Level at Item Level
                                        if(spiceLevel != null && !spiceLevel.equals("")) {
                                            Assert.assertEquals(spiceLevel,
                                                    variantGroup.getVariations().get(0).getCatalogAttributes().getSpiceLevel(),
                                                    "Spice Level mismatch at Variant Level for Item Id '" + orderItem.getItemId() + "'");
                                        }
                                        //Validates Veg-Classifier at Item Level
                                        if(vegClassifier != null && !vegClassifier.equals("")) {
                                            Assert.assertEquals(vegClassifier,
                                                    variantGroup.getVariations().get(0).getCatalogAttributes().getVegClassifier(),
                                                    "Veg-Classifier mismatch at Variant Level for Item Id '" + orderItem.getItemId() + "'");
                                        }
                                        //Validates Portion-Size at Item Level
                                        if(portionSize != null && !portionSize.equals("")) {
                                            Assert.assertEquals(portionSize,
                                                    "Serves " + variantGroup.getVariations().get(0).getCatalogAttributes().getServesHowMany(),
                                                    "Portion-Size mismatch at Variant Level for Item Id '" + orderItem.getItemId() + "'");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }

        }
        Reporter.log("[ORDER ID '" + orderResponse.getData().getOrderId() + "' -- VALIDATION SUCCESS FOR ATTRIBUTES]", true);
    }

    //Checks if all items provided are present in the order response
    public void checkIfAllItemsArePresentInOrderResponse(String[] itemIds, List<OrderItem> orderItems, boolean isAddonReqired){
        if(itemIds != null && itemIds.length >0){
            for(String itemId : itemIds){
                boolean isItemFound = false;
                for(OrderItem orderItem : orderItems){
                    if (itemId.equals(orderItem.getItemId())){
                        isItemFound = true;
                        break;
                    }
                }
                //Fails the test, if any item not found in order response
                if(!isItemFound){
                    Reporter.log("[PROVIDED ITEM ID NOT FOUND IN ORDER RESPONSE]" +
                            "\n Expected Item ID '" + itemId + "'", true);
                    Assert.fail("[PROVIDED ITEM ID NOT FOUND IN ORDER RESPONSE]" +
                            "\n Expected Item ID '" + itemId + "'");
                }
            }
        }else {
            Reporter.log("[NO ITEM ID FOUND]", true);
        }
    }

    //Cancel all orders
    public void cancelAllOrder(HashMap<String, String> headers){
        edvoCartHelper.cancelAllOrders(headers);
    }

    //Cancel all orders for default user
    public void cancelAllOrder() {
        edvoCartHelper.cancelAllOrders(headers);
    }

    //Cancel Order and return processor
    public Processor cancelOrder(Long orderId, HashMap<String, String> headers){
        return edvoCartHelper.cancelOrder(String.valueOf(orderId), headers);
    }

    //Cancel order and return pojo
    public OrderResponse getCancelOrder(Long orderId, HashMap<String, String> headers){
        return Utility.jsonDecode(cancelOrder(orderId, headers).ResponseValidator.GetBodyAsText(), OrderResponse.class);
    }

    //Edit Order Check and return Processor
    public Processor editOrderCheck(String[] itemIds, boolean isAddonRequired, String cartType, String paymentMethod,
                               String orderComments){
        OrderResponse orderResponse = placeOrder(itemIds, isAddonRequired, cartType, paymentMethod, orderComments);
        orderId = orderResponse.getData().getOrderId();
        Cart cart = getCartV2Payload(itemIds, isAddonRequired, cartType);
        String cartItemEntity = getEditOrderPayload(cart);
        String[] payload = new String[]{String.valueOf(orderId), "2", cartItemEntity};
        return edvoCartHelper.editOrderCheck(payload, headers);
    }

    //Edit Order Check and return response in pojo
    public CartV2Response getEditOrderCheck(String[] itemIds, boolean isAddonRequired, String cartType, String paymentMethod,
                                            String orderComments){
        Processor processor = editOrderCheck(itemIds, isAddonRequired, cartType, paymentMethod, orderComments);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
    }

    //Edit Order Confirm and return Processor
    public Processor editOrderConfirm(String[] itemIds, boolean isAddonRequired, String cartType, String paymentMethod,
                                    String orderComments){
        CartV2Response orderResponse = getEditOrderCheck(itemIds, isAddonRequired, cartType, paymentMethod, orderComments);
        Cart cart = getCartV2Payload(itemIds, isAddonRequired, cartType);
        String cartItemEntity = getEditOrderPayload(cart);
        String[] payload = new String[]{String.valueOf(orderId), "2", cartItemEntity};
        return edvoCartHelper.editOrderConfirm(payload, headers);
    }

    //Edit Order Confirm and return response in pojo
    public OrderResponse getEditOrderConfirm(String[] itemIds, boolean isAddonRequired, String cartType, String paymentMethod,
                                            String orderComments){
        Processor processor = editOrderConfirm(itemIds, isAddonRequired, cartType, paymentMethod, orderComments);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
    }

    //Gets Edit Order Payload
    public String getEditOrderPayload(Cart cart){
        String restaurantId = cart.getRestaurantId();
        String cartType = cart.getCartType();
        List<CartItem> cartItems = new ArrayList<>();
        for(CartItem cartItem : cart.getCartItems()){
            cartItem.setQuantity(cartItem.getQuantity() + 1);
            cartItems.add(cartItem);
        }

        cart = new CartBuilder()
                .restaurantId(restaurantId)
                .cartItems(cartItems)
                .cartType(cartType)
                .build();

        String cartItemEntity = Utility.jsonEncode(cart);
        cartItemEntity = cartItemEntity.substring(1,cartItemEntity.length()-1);
        return cartItemEntity;
    }

    //Clone Order Check
    public CartV2Response cloneOrderCheck(String[] itemIds, boolean isAddonRequired, String cartType, String paymentMethod,
                                     String orderComments){
        OrderResponse orderResponse = placeOrder(itemIds, isAddonRequired, cartType, paymentMethod, orderComments);
        orderId = orderResponse.getData().getOrderId();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(orderResponse.getStatusCode().intValue(), 0, "Status code mismatch for place order");
        softAssert.assertEquals(orderResponse.getStatusMessage(), "done successfully", "Status message mismatch for place order");

        orderResponse = getCancelOrder(orderResponse.getData().getOrderId(), headers);
        softAssert.assertEquals(orderResponse.getStatusCode().intValue(), 0, "Status code mismatch for cancel order");
        softAssert.assertEquals(orderResponse.getStatusMessage(), "done successfully", "Status message mismatch for cancel order");
        softAssert.assertAll();

        String cartEntity = getCloneOrderPayload(itemIds, isAddonRequired, cartType);
        String[] payload = new String[]{String.valueOf(orderId), "2", cartEntity};
        Processor processor = edvoCartHelper.cloneOrderCheck(payload, headers);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
    }

    //Clone Order Confirm
    public OrderResponse cloneOrderConfirm(String[] itemIds, boolean isAddonRequired, String cartType, String paymentMethod,
                                  String orderComments){
        CartV2Response cloneOrderCheck = cloneOrderCheck(itemIds, isAddonRequired, cartType, paymentMethod, orderComments);
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(cloneOrderCheck.getStatusCode().intValue(), CatalogueAttributesConstants.STATUS_CODE_CARTV2_SUCCESS, "Status code mismatch for clone order check");
        softAssert.assertEquals(cloneOrderCheck.getStatusMessage(), CatalogueAttributesConstants.STATUS_MESSAGE_CARTV2_SUCCESS, "Status message mismatch for clone order check");
        softAssert.assertAll();

        String cartEntity = getCloneOrderPayload(itemIds, isAddonRequired, cartType);
        String[] payload = new String[]{String.valueOf(orderId), "2", cartEntity};
        Processor processor = edvoCartHelper.cloneOrderConfirm(payload, headers);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
    }

    //Get Clone Order Payload
    public String getCloneOrderPayload(String[] itemIds, boolean isAddonRequired, String cartType){
        Cart cart = getCartV2Payload(itemIds, isAddonRequired, cartType);
        String cartEntity = Utility.jsonEncode(cart);
        cartEntity = cartEntity.substring(1, cartEntity.length()-1);
        return cartEntity;
    }

    //Get Single Order
    public GetSingleOrderResponse singleOrder(String orderId){
        Processor processor = edvoCartHelper.singleOrder(orderId, headers);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), GetSingleOrderResponse.class);
    }

    //Get Last Order
    public GetLastOrderResponse lastOrder(){
        Processor processor = edvoCartHelper.getLastOrder(headers);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), GetLastOrderResponse.class);
    }

    //Get All Order
    public GetSingleOrderResponse getAllOrders(){
        Processor processor = edvoCartHelper.getAllOrders(headers);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), GetSingleOrderResponse.class);
    }


}