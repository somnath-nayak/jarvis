package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class CartUser {
	@JsonProperty("userId")
	private Integer userId;
	@JsonProperty("swiggyMoney")
	private Double swiggyMoney;
	@JsonProperty("firstOrder")
	private Boolean firstOrder;

	@JsonProperty("userId")
	public Integer getUserId() {
	return userId;
	}

	@JsonProperty("userId")
	public void setUserId(Integer userId) {
	this.userId = userId;
	}

	public CartUser withUserId(Integer userId) {
	this.userId = userId;
	return this;
	}

	@JsonProperty("swiggyMoney")
	public Double getSwiggyMoney() {
	return swiggyMoney;
	}

	@JsonProperty("swiggyMoney")
	public void setSwiggyMoney(Double swiggyMoney) {
	this.swiggyMoney = swiggyMoney;
	}

	public CartUser withSwiggyMoney(Double swiggyMoney) {
	this.swiggyMoney = swiggyMoney;
	return this;
	}

	@JsonProperty("firstOrder")
	public Boolean getFirstOrder() {
	return firstOrder;
	}

	@JsonProperty("firstOrder")
	public void setFirstOrder(Boolean firstOrder) {
	this.firstOrder = firstOrder;
	}

	public CartUser withFirstOrder(Boolean firstOrder) {
	this.firstOrder = firstOrder;
	return this;
	}
}
