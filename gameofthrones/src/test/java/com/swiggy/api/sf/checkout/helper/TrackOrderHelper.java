package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceApiHelper;
import com.swiggy.api.erp.delivery.helper.OrderStatus;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.trackOrderResponse.TrackOrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;

public class TrackOrderHelper {
    private EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
    private CheckoutAPIHelper checkoutAPIHelper = new CheckoutAPIHelper();
    private DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    private DeliveryServiceApiHelper deliveryServiceApiHelper  = new DeliveryServiceApiHelper();
    public static final String UNASSIGNED = "unassigned";

    public Processor placeOrder(String restaurantId, HashMap<String, String> headers, String paymentMethod, String orderComments) {
        Cart cart = edvoCartHelper.getCartPayload(
                null,
                restaurantId,
                false,
                false,
                true);

        Processor cartResponse = edvoCartHelper.createCartV2(Utility.jsonEncode(cart), headers);
        CartV2Response cartV2Response = Utility.jsonDecode(cartResponse.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        String addressId = getServiceableAddress(cartV2Response);
        return edvoCartHelper.placeOrder(addressId, paymentMethod, orderComments, headers);
    }

    private String getServiceableAddress(CartV2Response cartV2Response) {
        String addressId = edvoCartHelper.getValidAddress(cartV2Response);
        return addressId.equals(null) ? edvoCartHelper.addNewAddressFromCartV2Response(cartV2Response) : addressId;
    }

    public String getNewOrderId(String restaurantId, HashMap<String, String> headers, String paymentMethod, String orderComments) {
        Processor orderResponse = placeOrder(restaurantId, headers, paymentMethod, orderComments);
        Integer orderId = JsonPath.read(orderResponse.ResponseValidator.GetBodyAsText(), "$.data.order_data.order_id");
        return String.valueOf(orderId);
    }

    public String[] getNewOrderKeyWithOrderID(String restaurantId, HashMap<String, String> headers, String paymentMethod, String orderComments) {
        Processor orderResponse = placeOrder(restaurantId, headers, paymentMethod, orderComments);
        String orderKey = JsonPath.read(orderResponse.ResponseValidator.GetBodyAsText(), "$.data.order_data.key");
        Integer orderId = JsonPath.read(orderResponse.ResponseValidator.GetBodyAsText(), "$.data.order_data.order_id");
        return new String[]{orderKey, String.valueOf(orderId)};
    }

    public HashMap<String, String> loginV2(String mobole, String password) {
        return edvoCartHelper.getHeaders(mobole, password);
    }

    public TrackOrderResponse trackOrderByOrderId(String orderId, HashMap<String, String> headers) {
        Processor processor = checkoutAPIHelper.trackOrder(orderId, headers);
        TrackOrderResponse trackOrderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), TrackOrderResponse.class);
        return trackOrderResponse;
    }

    public TrackOrderResponse trackOrderMinimal(String orderKey){
        Processor processor = checkoutAPIHelper.trackOrderMinimal(orderKey);
        TrackOrderResponse trackOrderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), TrackOrderResponse.class);
        return trackOrderResponse;
    }

    public void cancelOrder(long orderId, HashMap<String, String> headers) {
        edvoCartHelper.cancelOrder(String.valueOf(orderId), headers);
    }

    public String createDE(int zoneId) {
        return deliveryDataHelper.CreateDE(zoneId);
    }

    public void assignDE(String orderId, String deId) {
        deliveryServiceApiHelper.assignDE(orderId, deId);
    }

    public void changeOrderStatus(String orderId, String orderStatus){
        deliveryDataHelper.changeStatefromController(orderStatus, orderId);
    }

    public void makeDEFree(String deId){
        deliveryServiceApiHelper.markDEAvailable(deId);
    }

    public void validateTrackOrder(TrackOrderResponse trackOrderResponse, String orderStatus){
        String orderState = trackOrderResponse.getData().getDeliveryStatus();
        Reporter.log("[ACTUAL ORDER STATE] --> " + orderState, true);
        if(orderState.equals(OrderStatus.REACHED))
            trackOrderResponse.getData().setDeliveryStatus(OrderStatus.PICKEDUP);

        switch (orderStatus) {
            case TrackOrderHelper.UNASSIGNED :
                Assert.assertEquals(orderState, TrackOrderHelper.UNASSIGNED);
                break;
            case OrderStatus.ASSIGNED :
                Assert.assertEquals(orderState, OrderStatus.ASSIGNED);
                break;
            case OrderStatus.CONFIRMED :
                Assert.assertEquals(orderState, OrderStatus.CONFIRMED);
                break;
            case OrderStatus.ARRIVED :
                Assert.assertEquals(orderState, OrderStatus.ARRIVED);
                break;
            case OrderStatus.PICKEDUP :
                Assert.assertEquals(orderState, OrderStatus.PICKEDUP);
                break;
            case OrderStatus.REACHED :
                Assert.assertEquals(orderState, OrderStatus.PICKEDUP);
                break;
            case OrderStatus.DELIVERED :
                Assert.assertEquals(orderState, OrderStatus.DELIVERED);
                break;
            default:
                Assert.fail("[Order State does not matched with any available state] \n [Actual Order State --> " +orderState+ "]");
                break;
        }
    }

    public void smokeCheck(int statusCode, String statusMessage, TrackOrderResponse trackOrderResponse){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(trackOrderResponse.getStatusCode(), statusCode, "[STATUS CODE MISMATCH]");
        softAssert.assertEquals(trackOrderResponse.getStatusMessage(), statusMessage, "[STATUS MESSAGE MISMATCH]");
        softAssert.assertAll();
    }

    public void updateDELocationInRedis(String deId){
        deliveryDataHelper.updateDELocationInRedis(deId);
    }
}