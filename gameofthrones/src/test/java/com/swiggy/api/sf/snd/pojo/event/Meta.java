package com.swiggy.api.sf.snd.pojo.event;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class Meta {

    @JsonProperty("change_type")
    private String changeType;
    private String from;

    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("changeType", changeType).append("from", from).toString();
    }

}