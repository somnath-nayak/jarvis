package com.swiggy.api.sf.rng.helper;

import framework.gameofthrones.Tyrion.RedisHelper;

public class TDRedisHelper {

    public static final String redisHost = "rngredis";
    public static final int dataBase = 0;
    public static final int ruleDataBase = 1;

    private RedisHelper redisHelper = new RedisHelper();

    public void setKeyValue(String key, String value) {
        redisHelper.setValue(redisHost, dataBase, key, value);
    }

    public void delete(String key) {
        redisHelper.deleteKey(redisHost, dataBase, key);
    }

    public String getKey(String key) {
        return String.valueOf(redisHelper.getValue(redisHost, dataBase, key));
    }

    public void deleteRuleKeys(String key) {
        redisHelper.deleteKey(redisHost, ruleDataBase, key);
    }
}

