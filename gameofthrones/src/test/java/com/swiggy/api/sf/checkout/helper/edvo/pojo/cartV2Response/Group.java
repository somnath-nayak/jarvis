package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "groupId",
        "items"
})
public class Group {

    @JsonProperty("groupId")
    private Integer groupId;
    @JsonProperty("items")
    private List<Item> items = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Group() {
    }

    /**
     *
     * @param groupId
     * @param items
     */
    public Group(Integer groupId, List<Item> items) {
        super();
        this.groupId = groupId;
        this.items = items;
    }

    @JsonProperty("groupId")
    public Integer getGroupId() {
        return groupId;
    }

    @JsonProperty("groupId")
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Group withGroupId(Integer groupId) {
        this.groupId = groupId;
        return this;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Group withItems(List<Item> items) {
        this.items = items;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("groupId", groupId).append("items", items).toString();
    }

}