package com.swiggy.api.sf.rng.pojo.edvo;

import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu.ItemRequests;

import java.util.List;

public class ItemRequestCartV3Builder {

    public EDVOCartV3Pogo edvoCartV3Pogo;

    public ItemRequestCartV3Builder() {
        edvoCartV3Pogo = new EDVOCartV3Pogo();
    }

    public ItemRequestCartV3Builder withMinCartAmount(int minCartAmount) {
        edvoCartV3Pogo.setMinCartAmount(minCartAmount);
        return this;
    }

    public ItemRequestCartV3Builder withFirstOrder(boolean firstOrder) {
        edvoCartV3Pogo.setFirstOrder(firstOrder);
        return this;
    }

    public ItemRequestCartV3Builder withItemRequest(List<ItemRequests> itemRequests) {
        edvoCartV3Pogo.setItemRequests(itemRequests);
        return this;
    }


    public ItemRequestCartV3Builder withMealItemRequest(List<MealItemRequest> mealItemRequests) {
        edvoCartV3Pogo.setMealItemRequests(mealItemRequests);
        return this;
    }

    public ItemRequestCartV3Builder withRestaurantFirstOrder(boolean restaurantFirstOrder) {
        edvoCartV3Pogo.setRestaurantFirstOrder(restaurantFirstOrder);
        return this;
    }

    public ItemRequestCartV3Builder withUserAgent(String ANDROID) {
        edvoCartV3Pogo.setUserAgent(ANDROID);
        return this;
    }

    public ItemRequestCartV3Builder withVersionCode(String versionCode) {
        edvoCartV3Pogo.setVersionCode(versionCode);
        return this;
    }

    public ItemRequestCartV3Builder withItemRequestData(List<ItemRequests> ItemRequests, int minCartAmount, boolean firstOrder, boolean restFirstOrder, long userId, String userAgent, String versionCode) {
        edvoCartV3Pogo.setMinCartAmount(minCartAmount);
        edvoCartV3Pogo.setItemRequests(ItemRequests);
        edvoCartV3Pogo.setFirstOrder(firstOrder);
        edvoCartV3Pogo.setRestaurantFirstOrder(restFirstOrder);
        edvoCartV3Pogo.setUserId(userId);
        edvoCartV3Pogo.setUserAgent(userAgent);
        edvoCartV3Pogo.setVersionCode(versionCode);
        return this;
    }
}
