package com.swiggy.api.sf.rng.pojo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateFlatTdBuilder {

    public CreateFlatTdEntry createFlatTdEntry;

    public CreateFlatTdBuilder() {
        createFlatTdEntry = new CreateFlatTdEntry();
    }

    public CreateFlatTdBuilder nameSpace(String nameSpace) {
        createFlatTdEntry.setNamespace(nameSpace);
        return this;
    }

    public CreateFlatTdBuilder header(String header) {
        createFlatTdEntry.setHeader(header);
        return this;
    }

    public CreateFlatTdBuilder description(String description) {
        createFlatTdEntry.setDescription(description);
        return this;
    }

    public CreateFlatTdBuilder valid_from(String valid_from) {
        createFlatTdEntry.setValidFrom(valid_from);
        return this;
    }

    public CreateFlatTdBuilder valid_till(String valid_till) {
        createFlatTdEntry.setValidTill(valid_till);
        return this;
    }

    public CreateFlatTdBuilder campaign_type(String campaign_type) {
        createFlatTdEntry.setCampaignType(campaign_type);
        return this;
    }

    public CreateFlatTdBuilder id(Integer id) {
        createFlatTdEntry.setId(id);
        return this;
    }

    public CreateFlatTdBuilder updatedBy(String name) {
        createFlatTdEntry.setUpdatedBy(name);
        return this;
    }

    public CreateFlatTdBuilder restaurant_hit(String restaurant_hit) {
        createFlatTdEntry.setRestaurantHit(restaurant_hit);
        return this;
    }

    public CreateFlatTdBuilder enabled(boolean enabled) {
        createFlatTdEntry.setEnabled(enabled);
        return this;
    }

    public CreateFlatTdBuilder swiggy_hit(String swiggy_hit) {
        createFlatTdEntry.setSwiggyHit(swiggy_hit);
        return this;
    }

    public CreateFlatTdBuilder createdBy(String createdBy) {
        createFlatTdEntry.setCreatedBy(createdBy);
        return this;
    }

    public CreateFlatTdBuilder discountLevel(String discountLevel) {
        createFlatTdEntry.setDiscountLevel(discountLevel);
        return this;
    }

    public CreateFlatTdBuilder commissionOnFullBill(boolean commissionOnFullBill) {
        createFlatTdEntry.setCommissionOnFullBill(commissionOnFullBill);
        return this;
    }

    public CreateFlatTdBuilder taxesOnDiscountedBill(boolean taxesOnDiscountedBill) {
        createFlatTdEntry.setTaxesOnDiscountedBill(taxesOnDiscountedBill);
        return this;
    }

    public CreateFlatTdBuilder firstOrderRestriction(boolean firstOrderRestriction) {
        createFlatTdEntry.setFirstOrderRestriction(firstOrderRestriction);
        return this;
    }

    public CreateFlatTdBuilder restaurantFirstOrder(boolean restaurantFirstOrder) {
        createFlatTdEntry.setRestaurantFirstOrder(restaurantFirstOrder);
        return this;
    }

    public CreateFlatTdBuilder timeSlotRestriction(boolean timeSlotRestriction) {
        createFlatTdEntry.setTimeSlotRestriction(timeSlotRestriction);
        return this;
    }

    public CreateFlatTdBuilder userRestriction(boolean userRestriction) {
        createFlatTdEntry.setUserRestriction(userRestriction);
        return this;
    }

    public CreateFlatTdBuilder dormant_user_type(String dormant_user_type) {
        createFlatTdEntry.setDormantUserType(dormant_user_type);
        return this;
    }

    public CreateFlatTdBuilder slots(List<Slot> slotsDetails) {
        ArrayList<Slot> slotEntries = new ArrayList<>();
        for (Slot slotEntry : slotsDetails) {
            slotEntries.add(slotEntry);
        }
        createFlatTdEntry.setSlots(slotEntries);
        return this;
    }

    public CreateFlatTdBuilder ruleDiscount(String type, String discountLevel, String flatDiscount, String minCartAmount) {
        createFlatTdEntry.setFlatRuleDiscount(new FlatRuleDiscount(type, discountLevel, flatDiscount, minCartAmount));
        return this;
    }


    public CreateFlatTdBuilder restaurantList(List<RestaurantList> restaurantLists) {
        createFlatTdEntry.setRestaurantList(restaurantLists);
        return this;
    }

    public CreateFlatTdBuilder withShortDescription(String shortDescription) {
        createFlatTdEntry.setShortDescription(shortDescription);
        return this;
    }

    public CreateFlatTdBuilder withCopyOverridden(Boolean copyOverridden) {
        createFlatTdEntry.setCopyOverridden(copyOverridden);
        return this;
    }
    public CreateFlatTdBuilder withSuper(Boolean isSuper) {
        createFlatTdEntry.setSuper(isSuper);
        return this;
    }
    public CreateFlatTdBuilder withLevelPlaceholder(String levelPlaceholder) {
        createFlatTdEntry.setLevelPlaceHolder(levelPlaceholder);
        return this;
    }
    public CreateFlatTdEntry build() throws IOException {
        defaultData();
        return createFlatTdEntry;
    }


    private void defaultData() throws IOException {
        if (createFlatTdEntry.getNamespace() == null) {
            createFlatTdEntry.setNamespace("Default Namespace");
        }
        if (createFlatTdEntry.getDescription() == null) {
            createFlatTdEntry.setDescription("Default Description");
        }
        if (createFlatTdEntry.getSlots() == null) {
            createFlatTdEntry.setSlots(new ArrayList<>());
        }
        if (createFlatTdEntry.getHeader() == null) {
            createFlatTdEntry.setHeader("Default Header");
        }
        if ((createFlatTdEntry.getCreatedBy() == null) && (createFlatTdEntry.getUpdatedBy() == null)) {
            createFlatTdEntry.setCreatedBy("Default Created By-Manu");
        }
        if (createFlatTdEntry.getUserRestriction() == null) {
            createFlatTdEntry.setUserRestriction(false);
        }
        if (createFlatTdEntry.getTimeSlotRestriction() == null) {
            createFlatTdEntry.setTimeSlotRestriction(false);
        }
        if (createFlatTdEntry.getFirstOrderRestriction() == null) {
            createFlatTdEntry.setFirstOrderRestriction(false);
        }
        if (createFlatTdEntry.getTaxesOnDiscountedBill() == null) {
            createFlatTdEntry.setTaxesOnDiscountedBill(false);
        }
        if (createFlatTdEntry.getCommissionOnFullBill() == null) {
            createFlatTdEntry.setCommissionOnFullBill(false);
        }
        if (createFlatTdEntry.getDormantUserType() == null) {
            createFlatTdEntry.setDormantUserType("ZERO_DAYS_DORMANT");
        }

        //set values if required...
    }

    public CreateFlatTdBuilder createFlatTdWithCopy() throws IOException {
        if (createFlatTdEntry.getNamespace() == null) {
            createFlatTdEntry.setNamespace("Default");
        }
        if (createFlatTdEntry.getDescription() == null) {
            createFlatTdEntry.setDescription("Default Description");
        }
        if (createFlatTdEntry.getSlots() == null) {
            createFlatTdEntry.setSlots(new ArrayList<>());
        }
        if (createFlatTdEntry.getHeader() == null) {
            createFlatTdEntry.setHeader("Default Header");
        }
        if ((createFlatTdEntry.getCreatedBy() == null) && (createFlatTdEntry.getUpdatedBy() == null)) {
            createFlatTdEntry.setCreatedBy("Ankita-Resolver_Automation");
        }
        if (createFlatTdEntry.getUserRestriction() == null) {
            createFlatTdEntry.setUserRestriction(false);
        }
        if (createFlatTdEntry.getTimeSlotRestriction() == null) {
            createFlatTdEntry.setTimeSlotRestriction(false);
        }
        if (createFlatTdEntry.getFirstOrderRestriction() == null) {
            createFlatTdEntry.setFirstOrderRestriction(false);
        }
        if (createFlatTdEntry.getTaxesOnDiscountedBill() == null) {
            createFlatTdEntry.setTaxesOnDiscountedBill(false);
        }
        if (createFlatTdEntry.getCommissionOnFullBill() == null) {
            createFlatTdEntry.setCommissionOnFullBill(false);
        }
        if (createFlatTdEntry.getDormantUserType() == null) {
            createFlatTdEntry.setDormantUserType("ZERO_DAYS_DORMANT");
        }
        createFlatTdEntry.setCampaignType("Flat");
        createFlatTdEntry.setRestaurantHit("50");
        createFlatTdEntry.setEnabled(true);
        createFlatTdEntry.setSwiggyHit("50");
        createFlatTdEntry.setDiscountLevel("Restaurant");
        createFlatTdEntry.setFirstOrderRestriction(false);
        createFlatTdEntry.setUpdatedBy("Ankita-Resolver_Automation");
        createFlatTdEntry.setCopyOverridden(false);
        createFlatTdEntry.setSuper(false);
        createFlatTdEntry.setLevelPlaceHolder("Default Flat-Td placeholder");
        return this;
    }

}