package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderCancelCheck;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "cancellation_fee",
        "cancellation_fee_applicability",
        "cancellation_fee_on_previous_orders",
        "cancellation_fee_on_next_order",
        "refund_to",
        "refund_tat",
        "refund_amount",
        "refund_pg_amount",
        "refund_sm_amount"
})
public class Data {

    @JsonProperty("cancellation_fee")
    private Integer cancellationFee;
    @JsonProperty("cancellation_fee_applicability")
    private Boolean cancellationFeeApplicability;
    @JsonProperty("cancellation_fee_on_previous_orders")
    private Integer cancellationFeeOnPreviousOrders;
    @JsonProperty("cancellation_fee_on_next_order")
    private Integer cancellationFeeOnNextOrder;
    @JsonProperty("refund_to")
    private String refundTo;
    @JsonProperty("refund_tat")
    private String refundTat;
    @JsonProperty("refund_amount")
    private Integer refundAmount;
    @JsonProperty("refund_pg_amount")
    private Integer refundPgAmount;
    @JsonProperty("refund_sm_amount")
    private Integer refundSmAmount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    /**
     *
     * @param refundSmAmount
     * @param refundAmount
     * @param refundTo
     * @param cancellationFeeOnPreviousOrders
     * @param cancellationFeeOnNextOrder
     * @param cancellationFeeApplicability
     * @param cancellationFee
     * @param refundPgAmount
     * @param refundTat
     */
    public Data(Integer cancellationFee, Boolean cancellationFeeApplicability, Integer cancellationFeeOnPreviousOrders, Integer cancellationFeeOnNextOrder, String refundTo, String refundTat, Integer refundAmount, Integer refundPgAmount, Integer refundSmAmount) {
        super();
        this.cancellationFee = cancellationFee;
        this.cancellationFeeApplicability = cancellationFeeApplicability;
        this.cancellationFeeOnPreviousOrders = cancellationFeeOnPreviousOrders;
        this.cancellationFeeOnNextOrder = cancellationFeeOnNextOrder;
        this.refundTo = refundTo;
        this.refundTat = refundTat;
        this.refundAmount = refundAmount;
        this.refundPgAmount = refundPgAmount;
        this.refundSmAmount = refundSmAmount;
    }

    @JsonProperty("cancellation_fee")
    public Integer getCancellationFee() {
        return cancellationFee;
    }

    @JsonProperty("cancellation_fee")
    public void setCancellationFee(Integer cancellationFee) {
        this.cancellationFee = cancellationFee;
    }

    @JsonProperty("cancellation_fee_applicability")
    public Boolean getCancellationFeeApplicability() {
        return cancellationFeeApplicability;
    }

    @JsonProperty("cancellation_fee_applicability")
    public void setCancellationFeeApplicability(Boolean cancellationFeeApplicability) {
        this.cancellationFeeApplicability = cancellationFeeApplicability;
    }

    @JsonProperty("cancellation_fee_on_previous_orders")
    public Integer getCancellationFeeOnPreviousOrders() {
        return cancellationFeeOnPreviousOrders;
    }

    @JsonProperty("cancellation_fee_on_previous_orders")
    public void setCancellationFeeOnPreviousOrders(Integer cancellationFeeOnPreviousOrders) {
        this.cancellationFeeOnPreviousOrders = cancellationFeeOnPreviousOrders;
    }

    @JsonProperty("cancellation_fee_on_next_order")
    public Integer getCancellationFeeOnNextOrder() {
        return cancellationFeeOnNextOrder;
    }

    @JsonProperty("cancellation_fee_on_next_order")
    public void setCancellationFeeOnNextOrder(Integer cancellationFeeOnNextOrder) {
        this.cancellationFeeOnNextOrder = cancellationFeeOnNextOrder;
    }

    @JsonProperty("refund_to")
    public String getRefundTo() {
        return refundTo;
    }

    @JsonProperty("refund_to")
    public void setRefundTo(String refundTo) {
        this.refundTo = refundTo;
    }

    @JsonProperty("refund_tat")
    public String getRefundTat() {
        return refundTat;
    }

    @JsonProperty("refund_tat")
    public void setRefundTat(String refundTat) {
        this.refundTat = refundTat;
    }

    @JsonProperty("refund_amount")
    public Integer getRefundAmount() {
        return refundAmount;
    }

    @JsonProperty("refund_amount")
    public void setRefundAmount(Integer refundAmount) {
        this.refundAmount = refundAmount;
    }

    @JsonProperty("refund_pg_amount")
    public Integer getRefundPgAmount() {
        return refundPgAmount;
    }

    @JsonProperty("refund_pg_amount")
    public void setRefundPgAmount(Integer refundPgAmount) {
        this.refundPgAmount = refundPgAmount;
    }

    @JsonProperty("refund_sm_amount")
    public Integer getRefundSmAmount() {
        return refundSmAmount;
    }

    @JsonProperty("refund_sm_amount")
    public void setRefundSmAmount(Integer refundSmAmount) {
        this.refundSmAmount = refundSmAmount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
