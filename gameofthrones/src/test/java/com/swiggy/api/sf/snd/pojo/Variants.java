package com.swiggy.api.sf.snd.pojo;




public class Variants
{
    private Variant_groups[] variant_groups;

    private String[] exclude_list;

    public Variant_groups[] getVariant_groups ()
    {
        return variant_groups;
    }

    public void setVariant_groups (Variant_groups[] variant_groups)
    {
        this.variant_groups = variant_groups;
    }

    public String[] getExclude_list ()
    {
        return exclude_list;
    }

    public void setExclude_list (String[] exclude_list)
    {
        this.exclude_list = exclude_list;
    }

    public Variants build(){
        setDefaultValues();
        return this;
    }
    public void setDefaultValues(){
        Variant_groups variant_groups = new Variant_groups();
        variant_groups.build();


        if(this.getExclude_list()==null){
            this.setExclude_list(new String[] {});
        }

    }

    @Override
    public String toString()
    {
        return " [variant_groups = "+variant_groups+", exclude_list = "+exclude_list+"]";
    }
}
