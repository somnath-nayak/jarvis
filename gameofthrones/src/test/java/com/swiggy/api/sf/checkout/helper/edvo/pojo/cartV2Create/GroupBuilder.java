package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import java.util.List;

public class GroupBuilder {
    private Group group;

    public GroupBuilder(){
        group = new Group();
    }

    public GroupBuilder groupId(Integer groupId){
        group.setGroupId(groupId);
        return this;
    }

    public GroupBuilder items(List<Item> items){
        group.setItems(items);
        return this;
    }

    public Group build(){
        return group;
    }
}
