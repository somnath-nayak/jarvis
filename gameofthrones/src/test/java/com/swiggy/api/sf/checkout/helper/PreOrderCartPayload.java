package com.swiggy.api.sf.checkout.helper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PreOrderCartPayload {
	
	CMSHelper cmsHelper= new CMSHelper();
	SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
	RedisHelper redisHelper = new RedisHelper();
	
	Cart cartPayload;
	String restId;
	String[] latLog;
	String planId="0";


 public Cart getPreOrderCartPayload(CheckoutHelper helper,EDVOCartHelper eDVOCartHelper,String couponCode) throws IOException {
	  
	latLog=new String[]{"12.935215600000001","77.6199608","0"};
	
	String listingRes=helper.getRestList(latLog).ResponseValidator.GetBodyAsText();
	String[] restList=JsonPath.read(listingRes, "$.data..id").toString().replace("[","")
			                                   .replace("]","").replace("\"","").split(",");
	String[] unServiceabilityList=JsonPath.read(listingRes, "$.data..unserviceable").toString()
			                              .replace("[","").replace("]","").replace("\"","").split(",");
	String[] isOpenedList=JsonPath.read(listingRes, "$.data..availability..opened").toString()
	                                      .replace("[","").replace("]","").replace("\"","").split(",");
			
	for(int i=0;i<restList.length;i++){
			if (isOpenedList[i].equalsIgnoreCase("true") && unServiceabilityList[i].equalsIgnoreCase("false")) {
			      	       restId=restList[i];
			      	     Cart cartPayload1=null;
			      	       try {
			      	       cartPayload1=eDVOCartHelper.getCartPayload1(null,restId, false, false, true);
			      	       }catch (Exception e){
			      	    	   e.printStackTrace();
			      	    	  continue;
			      	    	   
			      	       }
					       String statuscode=cartValidation(helper,cartPayload1);
					          if (statuscode.equals("0")){
					              break;
					              }
				            }
			          }
	//CMS PRe-Order Configuration
	
	    if (getPreOrderableFromMenu(helper, restId).equals("false")){
		   configurePreOrder(restId);
	   }
	
	
    cartPayload=eDVOCartHelper.getCartPayloadPreOrder(restId,false,true,couponCode);
    System.out.println(Utility.jsonEncode(cartPayload));
return cartPayload;
	}

		
    public String cartValidation(CheckoutHelper helper,Cart cartPayload){
			String getStatusCode;
			try{
			String response=helper.createCartCheckTotals(Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
			getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
			}catch (Exception e){
				return "97";
			}
			return getStatusCode;
		}

   
    public void enablePreOrder(String restId, String attribute, String value){
        String response=cmsHelper.preOrderEnable(restId,attribute,value).ResponseValidator.GetBodyAsText();
        int status=JsonPath.read(response,"$.status");
        Assert.assertEquals(status,1);
    }

    public void createPreOrderSlots(String rest_id, String day, String openTime, String closeTime){
    cmsHelper.preorderSlots(rest_id,day,openTime,closeTime);
    sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
    List<Map<String, Object>> sched=sqlTemplateCI.queryForList("select `rest_id`,`type` from `restaurants_schedule` where `rest_id`="+rest_id+"");
    Assert.assertNotNull(sched);
}
    


     public static String getDay(){
        Format formatter = new SimpleDateFormat("EEE"); 
        String day = formatter.format(new Date());
        return day;
      }
     
     
     public void preOrderAreaSchedule(String areaId,String menuType, String day, String slotType, String openTime,String closeTime){
         String response=cmsHelper.preOrderAreaScheduleCreate(areaId, menuType, day, slotType, openTime,closeTime).ResponseValidator.GetBodyAsText();
         int status=JsonPath.read(response,"$.status");
         Assert.assertEquals(status,1);
     }
     

     public Object getRestAreaID(String restId) {
  	   SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
  	   String[] rest={restId};
  	   return sqlTemplate.queryForObjectInteger(CheckoutConstants.restAreaId, rest, String.class);
  	  }
     
     /*public static void main(String[] args) throws IOException {
    	 PreOrderCartPayload a=new PreOrderCartPayload();
    	 a.getPreOrderCartPayload(new CheckoutHelper(),new EDVOCartHelper());
	}*/
     
     public void setResCache(String restId){
    	 String value="{\"delivery\":true ,\"is_buffet\":false,\"self_pick_up\":false,\"preorder_enabled\":true}";
    	 redisHelper.setValue("sandredisstage",2,"METADATA_RESTAURANT_"+restId,value);
     }
     
     public void configurePreOrder(String restId){
    	 enablePreOrder(restId,"preorder_enabled","1");
    		createPreOrderSlots(restId,getDay(),"0000","2359");
    		Integer areaCode=(Integer) getRestAreaID(restId);
    		preOrderAreaSchedule(areaCode.toString(), "PRE_ORDER",getDay().toUpperCase(),"PREORDER","0","2330");
    		setResCache(restId);
     }
     
     public String getPreOrderableFromMenu(CheckoutHelper helper,String restId){
    	 System.out.println("");
    	 Processor menuV4 = helper.getRestaurantMenu(restId);  
  	   String menuRes=menuV4.ResponseValidator.GetBodyAsText();
  	   String preorderable=JsonPath.read(menuRes, "$.data.preorderable").toString().replace("[", "").replace("]", "");
	return preorderable;
  	  
     }
     
     public static void main(String[] args) {
    	 new PreOrderCartPayload().configurePreOrder("223");
	}
}
