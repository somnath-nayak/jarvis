package com.swiggy.api.sf.snd.tests;

import static org.testng.Assert.assertNotNull;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.snd.constants.IStatusConstants;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.SnDHelper;

import framework.gameofthrones.JonSnow.Processor;

public class ConsumerSessionTest extends SnDDp {

	@BeforeClass
	public void setUp() {
		long userId = SnDHelper.getUserId(SANDConstants.consumerSessionTstMobile);
		SnDHelper.deleteUserEntriesFromCatalogDB(userId);
	}

	@Test(dataProvider = "logincheck", groups = { "sanity", "smoke", "regression" })
	public void verifyConsumerLoginCheckForNonRegisteredUser(String mobile) {
		Processor p = SnDHelper.consumerLoginCheck(mobile);
		String body = p.ResponseValidator.GetBodyAsText();
		int a = JsonPath.read(body, "$.statusCode");
		Assert.assertEquals(0, a);
		Assert.assertFalse(JsonPath.read(body, "$.data.registered"));
		Assert.assertFalse(JsonPath.read(body, "$.data.passwordEnabled"));
		Assert.assertFalse(JsonPath.read(body, "$.data.active"));
	}

	@Test(dataProvider = "signUpV2", groups = { "sanity", "smoke", "regression" }, dependsOnMethods = {
			"verifyConsumerLoginCheckForNonRegisteredUser" })
	public void signUpV2FirstTimeUser(String name, String mobile, String email, String password) {
		Processor p = SnDHelper.consumerSignUp(name, mobile, email, password);
		String body = p.ResponseValidator.GetBodyAsText();
		int a = JsonPath.read(body, "$.statusCode");
		System.out.println(a);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), IStatusConstants.STATUTSCODE_SUCCESS,
				"StatusCode Check");
		Assert.assertEquals(JsonPath.read(body, "$.statusMessage"), IStatusConstants.STATUSMESSAGE_SUCCESS,
				"StatusMessage Check");
		String tid = p.ResponseValidator.GetNodeValue("tid");
		p = SnDHelper.verifyOTP(SnDHelper.getOTP(tid), tid);
		body = p.ResponseValidator.GetBodyAsText();
		assertNotNull(JsonPath.read(body, "$.data.mobile"));
		assertNotNull(JsonPath.read(body, "$.data.name"));
		assertNotNull(JsonPath.read(body, "$.data.customer_id"));
		assertNotNull(JsonPath.read(body, "$.data.email"));
	}

	@Test(dataProvider = "loginV2", groups = { "sanity", "smoke", "regression" }, dependsOnMethods = {
			"signUpV2FirstTimeUser" })
	public void loginV2Test(String mobile, String password) {
		Processor p = SnDHelper.consumerLogin(mobile, password);
		String body = p.ResponseValidator.GetBodyAsText();
		int a = JsonPath.read(body, "$.statusCode");
		System.out.println(a);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), IStatusConstants.STATUTSCODE_SUCCESS,
				"StatusCode Check");
		Assert.assertEquals(JsonPath.read(body, "$.statusMessage"), IStatusConstants.STATUSMESSAGE_SUCCESS,
				"StatusMessage Check");
		assertNotNull(JsonPath.read(body, "$.data.mobile"));
		assertNotNull(JsonPath.read(body, "$.data.name"));
		assertNotNull(JsonPath.read(body, "$.data.customer_id"));
		assertNotNull(JsonPath.read(body, "$.data.email"));
	}

	@Test(dataProvider = "passwordInvalidDataSets", groups = { "regression" }, dependsOnMethods = {
			"signUpV2FirstTimeUser" })
	public void verifySetPasswordWithInvalidDataSets(String mobile, String password, String newPassword,
			Object[] expected) {
		SoftAssert s = new SoftAssert();
		Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
		String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		Processor passwordSetResponse = SnDHelper.setPassword(newPassword, tid, token);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValueAsInt("statusCode"), expected[0]);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValue("statusMessage"), expected[1]);
	}

	@Test(dataProvider = "setpassword", groups = { "sanity", "smoke", "regression" }, dependsOnMethods = {
			"signUpV2FirstTimeUser" })
	public void verifySetPassword(String mobile, String password, String newPassword) {
		SoftAssert s = new SoftAssert();
		Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
		String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		Processor passwordSetResponse = SnDHelper.setPassword(newPassword, tid, token);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValueAsInt("statusCode"),
				IStatusConstants.STATUTSCODE_SUCCESS);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValue("statusMessage"),
				IStatusConstants.STATUSMESSAGE_SUCCESS);
		loginResponse = SnDHelper.consumerLogin(mobile, newPassword);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValueAsInt("statusCode"),
				IStatusConstants.STATUTSCODE_SUCCESS);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValue("statusMessage"),
				IStatusConstants.STATUSMESSAGE_SUCCESS);
		tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		passwordSetResponse = SnDHelper.setPassword(SANDConstants.consumerSessionTstPwd, tid, token);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValueAsInt("statusCode"),
				IStatusConstants.STATUTSCODE_SUCCESS);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValue("statusMessage"),
				IStatusConstants.STATUSMESSAGE_SUCCESS);
	}

	@Test(dataProvider = "sendOTP", description = "sendOTP-->verifyOTP", groups = { "sanity", "smoke",
			"regression" }, dependsOnMethods = { "signUpV2FirstTimeUser" })
	public void validateOTPFlow(String mobile) {
		Processor p = SnDHelper.sendOTP(mobile);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), IStatusConstants.STATUTSCODE_SUCCESS);
		Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), IStatusConstants.STATUSMESSAGE_SUCCESS);
		String tid = p.ResponseValidator.GetNodeValue("tid");
		p = SnDHelper.verifyOTP(SnDHelper.getOTP(tid), tid);
		String body = p.ResponseValidator.GetBodyAsText();
		int a = JsonPath.read(body, "$.statusCode");
		System.out.println(a);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), IStatusConstants.STATUTSCODE_SUCCESS,
				"StatusCode Check");
		Assert.assertEquals(JsonPath.read(body, "$.statusMessage"), IStatusConstants.STATUSMESSAGE_SUCCESS,
				"StatusMessage Check");
		assertNotNull(JsonPath.read(body, "$.data.mobile"));
		assertNotNull(JsonPath.read(body, "$.data.name"));
		assertNotNull(JsonPath.read(body, "$.data.customer_id"));
		assertNotNull(JsonPath.read(body, "$.data.email"));
	}

	@Test(dataProvider = "callFlow", description = "login-->callAuth--->callverify", groups = { "sanity", "smoke",
			"regression" }, dependsOnMethods = { "signUpV2FirstTimeUser" })
	public void verifyCallFlow(String mobile, String password) {
		Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
		String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		Processor p = SnDHelper.callAuth(mobile, tid, token);
		tid = p.ResponseValidator.GetNodeValue("tid");
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), IStatusConstants.STATUTSCODE_SUCCESS);
		Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), IStatusConstants.STATUSMESSAGE_SUCCESS);
		p = SnDHelper.callVerify(tid);
		String body = p.ResponseValidator.GetBodyAsText();
		int a = JsonPath.read(body, "$.statusCode");
		System.out.println(a);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), IStatusConstants.STATUTSCODE_SUCCESS,
				"StatusCode Check");
		Assert.assertEquals(JsonPath.read(body, "$.statusMessage"), IStatusConstants.STATUSMESSAGE_SUCCESS,
				"StatusMessage Check");
		assertNotNull(JsonPath.read(body, "$.data.mobile"));
		assertNotNull(JsonPath.read(body, "$.data.name"));
		assertNotNull(JsonPath.read(body, "$.data.customer_id"));
		assertNotNull(JsonPath.read(body, "$.data.email"));
	}

	@Test(dataProvider = "callFlow", description = "login-->callAuth--->callverify", groups = {
			"regression" }, dependsOnMethods = { "signUpV2FirstTimeUser" })
	public void verifyCallFlowWithInvalidDataSets(String mobile, Object[] expected) {
		Processor loginResponse = SnDHelper.consumerLogin(SANDConstants.mobile, SANDConstants.password);
		String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		Processor p = SnDHelper.callAuth(mobile, tid, token);
		tid = p.ResponseValidator.GetNodeValue("tid");
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), IStatusConstants.STATUTSCODE_SUCCESS);
		Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), IStatusConstants.STATUSMESSAGE_SUCCESS);
		p = SnDHelper.callVerify(tid);
		String body = p.ResponseValidator.GetBodyAsText();
		int a = JsonPath.read(body, "$.statusCode");
		System.out.println(a);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), IStatusConstants.STATUTSCODE_SUCCESS,
				"StatusCode Check");
		Assert.assertEquals(JsonPath.read(body, "$.statusMessage"), IStatusConstants.STATUSMESSAGE_SUCCESS,
				"StatusMessage Check");
		assertNotNull(JsonPath.read(body, "$.data.mobile"));
		assertNotNull(JsonPath.read(body, "$.data.name"));
		assertNotNull(JsonPath.read(body, "$.data.customer_id"));
		assertNotNull(JsonPath.read(body, "$.data.email"));
	}

	@Test(dataProvider = "logincheck", groups = { "sanity", "smoke", "regression" }, dependsOnMethods = {
			"validateOTPFlow" })
	public void verifyConsumerLoginCheck(String mobile) {
		Processor p = SnDHelper.consumerLoginCheck(mobile);
		String body = p.ResponseValidator.GetBodyAsText();
		int a = JsonPath.read(body, "$.statusCode");
		Assert.assertEquals(0, a);
		Assert.assertTrue(JsonPath.read(body, "$.data.registered"));
		Assert.assertTrue(JsonPath.read(body, "$.data.passwordEnabled"));
		Assert.assertTrue(JsonPath.read(body, "$.data.active"));
	}

	@Test(dataProvider = "loginV2", groups = { "sanity", "smoke", "regression" }, dependsOnMethods = { "loginV2Test" })
	public void verifyUserProfile(String mobile, String password) {
		SoftAssert s = new SoftAssert();
		Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
		String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		Processor userProfileResp = SnDHelper.userProfile(tid, token);
		s.assertEquals(userProfileResp.ResponseValidator.GetNodeValueAsInt("statusCode"),
				IStatusConstants.STATUTSCODE_SUCCESS);
		s.assertEquals(userProfileResp.ResponseValidator.GetNodeValue("statusMessage"),
				IStatusConstants.STATUSMESSAGE_SUCCESS);
		String body = userProfileResp.ResponseValidator.GetBodyAsText();
		assertNotNull(JsonPath.read(body, "$.data.mobile"));
		assertNotNull(JsonPath.read(body, "$.data.name"));
		assertNotNull(JsonPath.read(body, "$.data.customer_id"));
		assertNotNull(JsonPath.read(body, "$.data.email"));
	}

	@Test(dataProvider = "logincheckInvalidDataSets", groups = { "regression" })
	public void verifyConsumerLoginCheckInvalidDataSets(String mobile, Object[] expected) {
		Processor p = SnDHelper.consumerLoginCheck(mobile);
		String body = p.ResponseValidator.GetBodyAsText();
		int a = JsonPath.read(body, "$.statusCode");
		String aa = JsonPath.read(body, "$.statusMessage");
		Assert.assertEquals(a, expected[0]);
		Assert.assertEquals(aa, expected[1]);
	}

	@Test(dataProvider = "signUpV2InvalidDataSets", groups = { "regression" }, dependsOnMethods = {
			"verifyConsumerLoginCheckForNonRegisteredUser" })
	public void signUpV2FirstTimeUserWithInvalidData(String name, String mobile, String email, String password,
			Object[] expectedData) {
		Processor p = SnDHelper.consumerSignUp(name, mobile, email, password);
		String body = p.ResponseValidator.GetBodyAsText();
		int a = JsonPath.read(body, "$.statusCode");
		System.out.println(expectedData);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedData[0], "StatusCode Check");
		Assert.assertEquals(JsonPath.read(body, "$.statusMessage"), expectedData[1], "StatusMessage Check");
	}

	// @AfterClass
	// public void tearDown() {
	// long userId = SnDHelper.getUserId(CheckoutConstants.mobile);
	// SnDHelper.deleteUserEntriesFromCatalogDB(userId);
	// }

}
