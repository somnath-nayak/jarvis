package com.swiggy.api.sf.rng.helper;

import com.swiggy.api.sf.rng.constants.RngConstants;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.swiggy.api.sf.rng.constants.RngConstants.NO_DATA_FOUND;
import static com.swiggy.api.sf.rng.constants.RngConstants.marketingDB;


public class TDCachingDbHelper {

    private static Logger log = Logger.getLogger(TDCachingDbHelper.class);

    static String updateDescriptionQuery = "update campaign SET description=\"#1\" where id=\"#2\";";
    static String updateHeaderQuery = "update campaign SET header=\"#1\" where id=\"#2\";";
    static String updateUserRestriction = "update campaign SET user_restriction=\"#1\" where id=\"#2\";";

    static String getDescriptionQuery = "select description from campaign where id=\"#1\";";
    static String getHeaderQuery = "select header from campaign where id=\"#1\";";
    static String deleteCampaignQuery = "delete from campaign where id = \"#1\";";
    static String deleteRestaurantMap = "delete from campaign_restaurant_map where restaurant_id=\"#1\" and campaign_id=\"#2\"";
    static String updateRestaurantMap = "update campaign_restaurant_map set restaurant_id =\"#1\" where campaign_id=\"#2\"";
    static String updateCampaignUserMapping = "update campaign_user_map set user_id=\"#1\" where campaign_id=\"#2\"";
    static String createUserMapping = "insert into campaign_user_map (`campaign_id`, `user_id`,  `updated_at`, `is_active`) values (\"#1\", \"#2\", \"#3\", true);";
    static String deleteUserMapping = "delete from campaign_user_map where campaign_id = \"#1\" and user_id = \"#2\"";
    static String deleteCampaignRestaurantMapping = "delete from campaign_restaurant_map where campaign_id = \"#1\" and restaurant_id = \"#2\"";
    static String deleteCampaignRuleMapping = "delete from campaign_rule_map where campaign_id = \"#1\"";
    static String updateCampaignRuleMap = "update campaign_rule_map set rule_id=\"#1\" where campaign_id=\"#2\"";
    static String updateRewardValue= "update rule SET reward_value =\"#1\" where id in (select `rule_id` from `campaign_rule_map` where campaign_id=\"#2\");";
    static String updateMealRewardValue = "update rule SET reward_value =\"[{\\\"rewardFunction\\\":\\\"NONE\\\",\\\"rewardType\\\":\\\"Flat\\\",\\\"rewardValue\\\":\\\"#1\\\",\\\"count\\\":1}]\" where parent_id = -1 and id in (select `rule_id` from `campaign_rule_map` where campaign_id=#2)";
    static String getRuleIdsByCampaignId = "select rule_id from campaign_rule_map where campaign_id = \"#1\"";

    public static void updateDescriptionOfCampaign(String description, String campaignId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(marketingDB);
        updateDescriptionQuery = replaceTwoWords(updateDescriptionQuery, description, campaignId);
        sqlTemplate.execute(updateDescriptionQuery);

        log.info("Update campaign description query :" + updateDescriptionQuery);
    }

    public static void updateHeaderOfCampaign(String header, String campaignId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(marketingDB);
        sqlTemplate.execute(replaceTwoWords(updateHeaderQuery, header, campaignId));
        log.info("Update campaign header " + updateHeaderQuery);
    }

    public static void createUserMapping(String campaignId, String userId, String date) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(marketingDB);
        sqlTemplate.execute(replaceThreeWords(createUserMapping, campaignId, userId, date));

    }

    public static void deleteCampaignById(String campaignId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceOneWord(deleteCampaignQuery, campaignId));
    }

    public static void deleteCampaignUserMap(String campaignId, String userId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceTwoWords(deleteUserMapping, campaignId, userId));
    }

    public static void deleteCampaignRestaurantMapping(String campaignId, String restaurantId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceTwoWords(deleteCampaignRestaurantMapping, campaignId, restaurantId));
    }

    public static void deleteRestaurantMap(String restaurantId, String campaignId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceTwoWords(deleteRestaurantMap, restaurantId, campaignId));
    }

    public static void updateCampaignRestaurantMap(String restaurantId, String campaignId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceTwoWords(updateRestaurantMap, restaurantId, campaignId));
    }

    public static String getDescriptionOfCampaign(String campaignId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(marketingDB);
        getDescriptionQuery = replaceOneWord(getDescriptionQuery, campaignId);
        List<Map<String, Object>> list = sqlTemplate.queryForList(getDescriptionQuery);
        log.info("Get campaign description query -->> " + getDescriptionQuery);
        return list.size() > 0 ? list.get(0).get("description").toString() : NO_DATA_FOUND;
    }

    public static String getHeaderOfCampaign(String campaignId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(marketingDB);
        getHeaderQuery = replaceOneWord(getHeaderQuery, campaignId);
        List<Map<String, Object>> list = sqlTemplate.queryForList(getHeaderQuery);
        log.info("Get campaign header query-- >> " + getHeaderQuery);
        return list.size() > 0 ? list.get(0).get("header").toString() : NO_DATA_FOUND;
    }


    public static String replaceOneWord(String inputString, String input1) {
        return inputString.replace("#1", input1);
    }

    public static String replaceTwoWords(String inputString, String input1, String input2) {
        return inputString.replace("#1", input1).replace("#2", input2);
    }

    public static String replaceThreeWords(String inputString, String input1, String input2, String input3) {

        inputString =  inputString.replace("#1", input1).replace("#2", input2).replace("#3", input3);
        log.info("Query is -->>  " + inputString);
        return inputString;
    }

    public static String replaceSevenWords(String inputString, String input1, String input2, String input3,String input4,String input5,String input6,String input7) {

        inputString =  inputString.replace("#1", input1).replace("#2", input2).replace("#3", input3).replace("#4" , input4).replace("#5", input5)
        .replace("#6" , input6).replace( "#7", input7);
        log.info("Query is -->>  " + inputString);
        return inputString;
    }


    public static void updateCampaignUserMap(String userId, String campaignId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceTwoWords(updateCampaignUserMapping, userId, campaignId));
    }

    public static void updateUserRestrictionOfCampaign(String campaignId, String enabled) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceTwoWords(updateUserRestriction, enabled, campaignId));
    }

    public static void deleteCampaignRuleMap(String campaignId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceOneWord(deleteCampaignRuleMapping, campaignId));
    }

    public static void updateCampaignRuleMap(String campaignId, String ruleId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceTwoWords(updateCampaignRuleMap, campaignId, ruleId));
    }

    public static  void updatCampaignRewarValue(String rewardValue,String campaignId ){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceTwoWords(updateRewardValue, rewardValue, campaignId));
    }

    public static  void updateMealRewardValue(String rewardValue,String campaignId ){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
        sqlTemplate.execute(replaceTwoWords(updateMealRewardValue, rewardValue, campaignId));
        System.out.print(updateMealRewardValue);
    }
    public static List<String> getRuleIdsByCampaign(String campaignId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(marketingDB);
        getRuleIdsByCampaignId = replaceOneWord(getRuleIdsByCampaignId, campaignId);
        List<Map<String, Object>> list = sqlTemplate.queryForList(getRuleIdsByCampaignId);
        List<String> ruleIds = new ArrayList<>();

        for(int i = 0; i < list.size(); i++) {
            ruleIds.add(list.get(i).get("rule_id").toString());
        }

        return ruleIds;
    }




}
