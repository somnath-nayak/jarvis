package com.swiggy.api.sf.rng.pojo.TDTemplate;


import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "restaurantIds",
        "templateId",
        "startDate",
        "duration",
        "ingestionSource"
})
public class OnboardTemplate {

    @JsonProperty("restaurantIds")
    private List<Integer> restaurantIds = null;
    @JsonProperty("templateId")
    private Integer templateId;
    @JsonProperty("startDate")
    private Long startDate;
    @JsonProperty("duration")
    private Integer duration;
    @JsonProperty("ingestionSource")
    private String ingestionSource;

    @JsonProperty("restaurantIds")
    public List<Integer> getRestaurantIds() {
        return restaurantIds;
    }

    @JsonProperty("restaurantIds")
    public void setRestaurantIds(List<Integer> restaurantIds) {
        this.restaurantIds = restaurantIds;
    }

    @JsonProperty("templateId")
    public Integer getTemplateId() {
        return templateId;
    }

    @JsonProperty("templateId")
    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    @JsonProperty("startDate")
    public Long getStartDate() {
        return startDate;
    }

    @JsonProperty("startDate")
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("duration")
    public Integer getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @JsonProperty("ingestionSource")
    public String getIngestionSource() {
        return ingestionSource;
    }

    @JsonProperty("ingestionSource")
    public void setIngestionSource(String ingestionSource) {
        this.ingestionSource = ingestionSource;
    }

}
