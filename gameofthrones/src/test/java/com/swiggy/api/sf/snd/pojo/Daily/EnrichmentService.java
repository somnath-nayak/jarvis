package com.swiggy.api.sf.snd.pojo.Daily;

        import com.fasterxml.jackson.annotation.JsonIgnore;
        import com.fasterxml.jackson.annotation.JsonInclude;
        import com.fasterxml.jackson.annotation.JsonProperty;
        import com.fasterxml.jackson.annotation.JsonPropertyOrder;
        import com.swiggy.api.daily.cms.pojo.AvailabilityMeta;
        import org.apache.commons.lang.builder.ToStringBuilder;

        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.pojo.Daily
 **/

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "store_id",
        "product_ids"
})
public class EnrichmentService {

    @JsonProperty("store_id")
    private Integer store_id;
    @JsonProperty("product_ids")
    private List<String> product_ids = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("store_id")
    public Integer getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(Integer store_id) {
        this.store_id = store_id;
    }

    @JsonProperty("product_ids")
    public List<String> getProduct_ids() {
        return product_ids;
    }

    @JsonProperty("product_ids")
    public void setProduct_ids(List<String> product_ids) {
        this.product_ids = product_ids;
    }

    private void setDefaultValues(Integer storeId, List<String> productIds) {
        if (this.getStore_id() == null)
            this.setStore_id(storeId);
        if (this.getProduct_ids() == null)
            this.setProduct_ids(productIds);
    }

    public EnrichmentService build(String storeId, List<String> productIds) {
        setDefaultValues(Integer.parseInt(storeId),productIds);
        return this;
    }

    //Overload
    public EnrichmentService build(Integer storeId, List<String> productIds) {
        setDefaultValues(storeId,productIds);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("store_id", store_id).append("product_ids", product_ids).append("additionalProperties", additionalProperties).toString();
    }

}

