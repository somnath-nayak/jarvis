package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

public class VariantBuilder {
    private Variant variant;

    public VariantBuilder(){
        variant = new Variant();
    }

    public VariantBuilder variationId(Integer variationId){
        variant.setVariationId(variationId);
        return this;
    }

    public VariantBuilder groupId(Integer groupId){
        variant.setGroupId(groupId);
        return this;
    }

    public VariantBuilder name(String name){
        variant.setName(name);
        return this;
    }

    public VariantBuilder price(Integer price){
        variant.setPrice(price);
        return this;
    }

    public Variant build(){
        return variant;
    }
}
