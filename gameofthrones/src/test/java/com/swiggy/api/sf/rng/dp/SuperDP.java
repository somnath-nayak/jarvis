package com.swiggy.api.sf.rng.dp;

import java.util.HashMap;

import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.rng.helper.SuperHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.CreateIncentive;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.Createbenefit;

public class SuperDP {
	static SuperHelper superhelper = new SuperHelper();

	@DataProvider(name = "publicUserSubsCriptionWithoutIncentiveData")
	public static Object[][] publicUserSubsCriptionWithoutIncentive() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload } };

	}

	@DataProvider(name = "publicUserSubsCriptionWithIncentiveData")
	public static Object[][] publicUserSubsCriptionWithIncentive() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive } };

	}

	@DataProvider(name = "mappedUserSubsCriptionWithoutIncentiveData")
	public static Object[][] mappedUserSubsCriptionWithoutIncentive() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload } };

	}

	@DataProvider(name = "mappedUserSubsCriptionWithIncentiveData")
	public static Object[][] mappedUserSubsCriptionWithIncentive() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive } };

	}

	@DataProvider(name = "publicAndMappedUserSubsCriptionWithoutIncentiveData")
	public static Object[][] publicAndMappedUserSubsCriptionWithoutIncentive() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload } };
	}

	@DataProvider(name = "publicAndMappedUserSubsCriptionWithIncentiveToMappedUserData")
	public static Object[][] publicAndMappedUserSubsCriptionWithIncentiveToMappedUser() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive } };

	}

	@DataProvider(name = "publicAndMappedUserSubsCriptionWithIncentiveToPublicUserData")
	public static Object[][] publicAndMappedUserSubsCriptionWithIncentiveToPublicUser() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive } };

	}

	@DataProvider(name = "publicAndMappedUserSubsCriptionWithIncentiveToBothPublicandMappedUserData")
	public static Object[][] publicAndMappedUserSubsCriptionWithIncentiveToBothPublicandMappedUser() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive } };

	}

	@DataProvider(name = "mappedUserTwoPlansWhereOneMappedUserNotGetPlanOfAnotherMappedUserData")
	public static Object[][] mappedUserTwoPlansWhereOneMappedUserNotGetPlanOfAnotherMappedUserData() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createMappedPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

		// value for create mapped plan-- > validFromDate , numOfMonthValidity ,
		// planTenure , planPrice , renewalOffsetDays

		createMappedPlan.put("0", "0");
		createMappedPlan.put("1", "6");
		createMappedPlan.put("2", "3");
		createMappedPlan.put("3", "199");
		createMappedPlan.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createMappedPlan, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive, planTwoPayload[0] } };

	}

	@DataProvider(name = "cancelPublicUserSubsCriptionWithOrderIdData")
	public static Object[][] cancelPublicUserSubsCriptionWithOrderId() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive } };

	}

	@DataProvider(name = "cancelPublicUserSubsCriptionWithSubscriptionIdData")
	public static Object[][] cancelPublicUserSubsCriptionWithSubscriptionId() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive } };

	}

	@DataProvider(name = "cancelMappedUserSubsCriptionWithSubscriptionIdData")
	public static Object[][] cancelMappedUserSubsCriptionWithSubscriptionId() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive } };

	}

	@DataProvider(name = "cancelMappedUserSubsCriptionWithOrderIdData")
	public static Object[][] cancelMappedUserSubsCriptionWithOrderId() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		return new Object[][] { { planPayload[0], numOfPlans, benefitPayload, createIncentive } };

	}

	@DataProvider(name = "checkGetSubscriptionHistoryOfNonSubscribedUserData")
	public static Object[][] checkGetSubscriptionHistoryOfNonSubscribedUser() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "checkGetSubscriptionHistoryWhereUserIdIsNullData")
	public static Object[][] checkGetSubscriptionHistoryWhereUserIdIsNull() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "checkGetSubscriptionHistoryWhereUserIdAsZeroData")
	public static Object[][] checkGetSubscriptionHistoryWhereUserIdAsZero() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "checkSubsHistoryForPastSubsOfUserData")
	public static Object[][] checkSubsHistoryForPastSubsOfUser() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "checkSubsHistoryForExpiredSubsOfUserData")
	public static Object[][] checkSubsHistoryForExpiredSubsOfUser() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "checkSubsHistoryForUserWhoseSubsIsAboutToExpireData")
	public static Object[][] checkSubsHistoryForUserWhoseSubsIsAboutToExpire() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "checkSubsHistoryForUserWhoIsHavingMultipleSubsData")
	public static Object[][] checkSubsHistoryForUserWhoIsHavingMultipleSubs() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "checkSubsHistoryWhenPlanIsHavingFlatTypIncentiveForUserData")
	public static Object[][] checkSubsHistoryWhenPlanIsHavingFlatTypIncentiveForUser() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "checkSubsHistoryForUserWhoTookDifferentTypeOfSubsData")
	public static Object[][] checkSubsHistoryForUserWhoTookDifferentTypeOfSubs() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String renewalOffsetDay = "5";
		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", renewalOffsetDay);

		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "6");
		createPlanTwo.put("2", "3");
		createPlanTwo.put("3", "300");
		createPlanTwo.put("4", "5");

		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] {
				{ planPayload[0], planTwoPayload[0], renewalOffsetDay, benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "checkSubsHistoryForValidTillAndValidFromOfPlanData")
	public static Object[][] checkSubsHistoryForValidTillAndValidFromOfPlan() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "getPlanBenefitsData")
	public static Object[][] getPlanBenefits() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "getPlanBenefitsOfDisabledPlanData")
	public static Object[][] getPlanBenefitsOfDisabledPlan() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "getPlanBenefitsOfExpiredPlanData")
	public static Object[][] getPlanBenefitsOfExpiredPlan() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "getPlanBenefitsOfTenureThreeData")
	public static Object[][] getPlanBenefitsOfTenureThree() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "getPlanForDeletedBenefitsData")
	public static Object[][] getPlanForDeletedBenefits() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "getPlansWithMultipleBenefitsAndIncentivesData")
	public static Object[][] getPlansWithMultipleBenefitsAndIncentives() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createIncentiveTwo = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentiveTwo.put("0", "PERCENTAGE");
		createIncentiveTwo.put("1", "1");
		createIncentiveTwo.put("2", "10");
		createIncentiveTwo.put("3", "null");
		createIncentiveTwo.put("4", "0");
		createIncentiveTwo.put("5", "100");
		createIncentiveTwo.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		String precentageIncentivePayload = superhelper.populateIncentivePayload(createIncentiveTwo);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload, incentivePayload,
				precentageIncentivePayload } };

	}

	@DataProvider(name = "getPlansBenefitsWhereSameIncentivesInMappedTwiceData")
	public static Object[][] getPlansBenefitsWhereSameIncentivesInMappedTwice() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createIncentiveTwo = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload, incentivePayload } };

	}

	@DataProvider(name = "getPlansBenefitsWhereNoBenefitIsMappedWithPlanData")
	public static Object[][] getPlansBenefitsWhereNoBenefitIsMappedWithPlan() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createIncentiveTwo = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "6");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

		return new Object[][] { { planPayload[0] } };

	}

	@DataProvider(name = "getPlanBenefitsOfPlanWhichWilBeActiveInFutureData")
	public static Object[][] getPlanBenefitsOfPlanWhichWilBeActiveInFuture() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validFromDate = Utility.getFutureMonthInMillisecond(3);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", (validFromDate));
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		String[] planPayload = superhelper.populateFuturePlanPayload(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "getPlanBenefitsOfFuturePlanAndDeletedBenefitsData")
	public static Object[][] getPlanBenefitsOfFuturePlanAndDeletedBenefits() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validFromDate = Utility.getFutureMonthInMillisecond(3);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", (validFromDate));
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		String[] planPayload = superhelper.populateFuturePlanPayload(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPlanWhenPlanIsNotMappedWithUserIncentiveData")
	public static Object[][] verifyPlanWhenPlanIsNotMappedWithUserIncentive() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPlanWithTheExpiredPlanIdData")
	public static Object[][] verifyPlanWithTheExpiredPlanId() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPlanNonMappedUserIdData")
	public static Object[][] verifyPlanNonMappedUserId() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPlanForMappedUserIdData")
	public static Object[][] verifyPlanForMappedUserId() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPublicPlanWithDifferentUserIdsData")
	public static Object[][] verifyPublicPlanWithDifferentUserIds() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPublicPlanWhichIsMappedWithBothPubliUserAndMappedUserIdsData")
	public static Object[][] verifyPublicPlanWhichIsMappedWithBothPubliUserAndMappedUserIds() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPublicPlanWhichWillBeExpireInFewDaysData")
	public static Object[][] verifyPublicPlanWhichWillBeExpireInFewDays() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPublicPlanWhenPlanTotalAvailableIsZeroData")
	public static Object[][] verifyPublicPlanWhenPlanTotalAvailableIsZero() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPublicPlanWhenNoBenefitIsMappedWithPlanData")
	public static Object[][] verifyPublicPlanWhenNoBenefitIsMappedWithPlan() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPlanWhenPlanUserIncentiveMappingIsNotPresentData")
	public static Object[][] verifyPlanWhenPlanUserIncentiveMappingIsNotPresent() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPlanWhereUserIdIsInvalidData")
	public static Object[][] verifyPlanWhereUserIdIsInvalid() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPlanWhereUsageCountEqualsToTOtalAvailableData")
	public static Object[][] verifyPlanWhereUsageCountEqualsToTOtalAvailable() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		createFreeBieBenefit.put("0", "Freebie");
		createFreeBieBenefit.put("1", "1");
		String freebieBenefitPayload = superhelper.populateBenefitData(createFreeBieBenefit.get(Integer.toString(0)),
				createFreeBieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, freebieBenefitPayload } };

	}

	@DataProvider(name = "verifyPlanWherNoBenefitMappedButIncentiveIsMappedData")
	public static Object[][] verifyPlanWherNoBenefitMappedButIncentiveIsMapped() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreeBieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "3");
		createPublicPlan.put("3", "3");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "1");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");

		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], incentivePayload } };

	}

	@DataProvider(name = "createSubscriptionWithUserIdAsZeroData")
	public static Object[][] createSubscriptionWithUserIdAsZero() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "createSubscriptionWithPlanIdAsZeroData")
	public static Object[][] createSubscriptionWithPlanIdAsZero() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };
	}

	@DataProvider(name = "createSubscriptionWithOrderIdAsZeroData")
	public static Object[][] createSubscriptionWithOrderIdAsZero() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "createSubscriptionForUserForFuturePlansData")
	public static Object[][] createSubscriptionForUserForFuturePlans() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validFrom = Utility.getFutureMonthInMillisecond(3);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", validFrom);
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateFuturePlanPayload(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "createSubscriptionForFuturePlansToWhichNoBenefitIncenticeMappedData")
	public static Object[][] createSubscriptionForFuturePlansToWhichNoBenefitIncenticeMapped() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validFrom = Utility.getFutureMonthInMillisecond(3);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", validFrom);
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		String[] planPayload = superhelper.populateFuturePlanPayload(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "createSubscriptionForFuturePlansToWhichOnlyIncentiveMappedData")
	public static Object[][] createSubscriptionForFuturePlansToWhichOnlyIncentiveMapped() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validFrom = Utility.getFutureMonthInMillisecond(3);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", validFrom);
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		String[] planPayload = superhelper.populateFuturePlanPayload(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "createSubscriptionForExpiredPlansData")
	public static Object[][] createSubscriptionForExpiredPlans() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validtill = Utility.getPastMonthDate(3);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, validtill } };

	}

	@DataProvider(name = "createOneMoreSubscriptionForUserAlreadyHaveSubsData")
	public static Object[][] createOneMoreSubscriptionForUserAlreadyHaveSubs() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "5");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "createOneMoreSubscriptionFutureSubscriptionWithInRenewalOffsetData")
	public static Object[][] createOneMoreSubscriptionFutureSubscriptionWithInRenewalOffset() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "cancelCurrentSubsWithOrderIdWhenFutureSubsIsAlsoThereData")
	public static Object[][] cancelCurrentSubsWithOrderIdWhenFutureSubsIsAlsoThere() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyOneSubsThenCancelAndAgainBuySamePlanData")
	public static Object[][] buyOneSubsThenCancelAndAgainBuySamePlanData() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buySubsWithSameUserIdPlanIdOrderIdMultipleTimesData")
	public static Object[][] buySubsWithSameUserIdPlanIdOrderIdMultipleTimes() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPublicPlanForUserWHoAlreadyHaveMappedPlanSubsData")
	public static Object[][] buyPublicPlanForUserWHoAlreadyHaveMappedPlanSubs() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buySubscriptionWhenTenurePeriodNotLyingUnderValidTillData")
	public static Object[][] buySubscriptionWhenTenurePeriodNotLyingUnderValidTill() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buySubscriptionWhenPlanUsageCountIsEqualToTotalAvailableData")
	public static Object[][] buySubscriptionWhenPlanUsageCountIsEqualToTotalAvailable() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String usageCount = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, usageCount } };

	}

	@DataProvider(name = "buySubscriptionWhenPlanUsageCountIsAboutToreachItsMaxLimitOrTotalAvailIsZeroData")
	public static Object[][] buySubscriptionWhenPlanUsageCountIsAboutToreachItsMaxLimitOrTotalAvailIsZero() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String usageCount = "1999";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "3");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "99");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, usageCount } };

	}

	@DataProvider(name = "buySubscriptionWhenPlanIsDisabledData")
	public static Object[][] buySubscriptionWhenPlanIsDisabled() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buySubscriptionWhenPatchPlanForTotalCountOfPlanData")
	public static Object[][] buySubscriptionWhenPatchPlanForTotalCountOfPlan() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buySubscriptionAfterPatchPlanWithValidTillData")
	public static Object[][] buySubscriptionAfterPatchPlanWithValidTill() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String patchValidTill = Utility.getFutureMonthInMillisecond(3);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, patchValidTill } };

	}

	@DataProvider(name = "buySubscriptionWithNonExistingPlanIdData")
	public static Object[][] buySubscriptionWithNonExistingPlanId() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buySubscriptionForDifferentPlanWhenCurrentPlanRenewOffsetIsNotStartedData")
	public static Object[][] buySubscriptionForDifferentPlanWhenCurrentPlanRenewOffsetIsNotStarted() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionToWhichNoBenefitIsAttahcedData")
	public static Object[][] buyPlanSubscriptionToWhichNoBenefitIsAttahced() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionToWhichIncentiveTypeZeroAndMinusOneIsMappedData")
	public static Object[][] buyPlanSubscriptionToWhichIncentiveTypeZeroAndMinusOneIsMapped() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createIncentiveTenureMinusOne = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentiveTenureMinusOne.put("0", "FLAT");
		createIncentiveTenureMinusOne.put("1", "10");
		createIncentiveTenureMinusOne.put("2", "-1");
		createIncentiveTenureMinusOne.put("3", "null");
		createIncentiveTenureMinusOne.put("4", "-1");
		createIncentiveTenureMinusOne.put("5", "100");
		createIncentiveTenureMinusOne.put("6", "true");
		String incentiveTenureMinusOnePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, incentiveTenureMinusOnePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionWhichIsForBothMappedUserAndPublicData")
	public static Object[][] buyPlanSubscriptionWhichIsForBothMappedUserAndPublic() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionWhichIsForBothMappedUserAndPublicButForMappedIncentiveIsAlsoMappedData")
	public static Object[][] buyPlanSubscriptionWhichIsForBothMappedUserAndPublicButForMappedIncentiveIsAlsoMapped() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionWhereNoPlanUserIncentiveMappingData")
	public static Object[][] buyPlanSubscriptionWhereNoPlanUserIncentiveMapping() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionWhereNoPlanBenefitMappingData")
	public static Object[][] buyPlanSubscriptionWhereNoPlanBenefitMapping() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionWhereNoPlanBenefitAndPlanUserIncentiveMappingData")
	public static Object[][] buyPlanSubscriptionWhereNoPlanBenefitAndPlanUserIncentiveMapping() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionWithUserIdZeroData")
	public static Object[][] buyPlanSubscriptionWithUserIdZero() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionWithOrderIdZeroData")
	public static Object[][] buyPlanSubscriptionWithOrderIdZero() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionWithTheDuplicateOrderIdData")
	public static Object[][] buyPlanSubscriptionWithTheDuplicateOrderId() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionWhereMultipleBenefitsAreMappedData")
	public static Object[][] buyPlanSubscriptionWhereMultipleBenefitsAreMapped() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createBenefitTwo = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createBenefitTwo.put("0", "Freebie");
		createBenefitTwo.put("1", "1");
		String benefitTwoPayload = superhelper.populateBenefitData(createBenefitTwo.get(Integer.toString(0)),
				createBenefitTwo.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, benefitTwoPayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionCheckSubscriptionRemainSameData")
	public static Object[][] buyPlanSubscriptionCheckSubscriptionRemainSame() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createBenefitTwo = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createBenefitTwo.put("0", "Freebie");
		createBenefitTwo.put("1", "1");
		String benefitTwoPayload = superhelper.populateBenefitData(createBenefitTwo.get(Integer.toString(0)),
				createBenefitTwo.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, benefitTwoPayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionCheckPlanIdInesponseData")
	public static Object[][] buyPlanSubscriptionCheckPlanIdInesponse() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createBenefitTwo = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createBenefitTwo.put("0", "Freebie");
		createBenefitTwo.put("1", "1");
		String benefitTwoPayload = superhelper.populateBenefitData(createBenefitTwo.get(Integer.toString(0)),
				createBenefitTwo.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, benefitTwoPayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionCheckPlanIdInResponseData")
	public static Object[][] buyPlanSubscriptionCheckPlanIdInResponse() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createBenefitTwo = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createBenefitTwo.put("0", "Freebie");
		createBenefitTwo.put("1", "1");
		String benefitTwoPayload = superhelper.populateBenefitData(createBenefitTwo.get(Integer.toString(0)),
				createBenefitTwo.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, benefitTwoPayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionCheckValidTillAndValidFromResponseData")
	public static Object[][] buyPlanSubscriptionCheckValidTillAndValidFromResponse() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createBenefitTwo = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createBenefitTwo.put("0", "Freebie");
		createBenefitTwo.put("1", "1");
		String benefitTwoPayload = superhelper.populateBenefitData(createBenefitTwo.get(Integer.toString(0)),
				createBenefitTwo.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, benefitTwoPayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionCheckMappedUserNotAbleToBuyPublicPlanInRenewalPeriodData")
	public static Object[][] buyPlanSubscriptionCheckMappedUserNotAbleToBuyPublicPlanInRenewalPeriod() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createBenefitTwo = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createBenefitTwo.put("0", "Freebie");
		createBenefitTwo.put("1", "1");
		String benefitTwoPayload = superhelper.populateBenefitData(createBenefitTwo.get(Integer.toString(0)),
				createBenefitTwo.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, benefitTwoPayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionCheckWhenBenfitDeletedData")
	public static Object[][] buyPlanSubscriptionCheckWhenBenfitDeleted() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionCheckForDisabledIncentiveData")
	public static Object[][] buyPlanSubscriptionCheckForDisabledIncentive() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "buyPlanSubscriptionCheckAfterBuyingSubsDisabledIncentiveData")
	public static Object[][] buyPlanSubscriptionCheckAfterBuyingSubsDisabledIncentive() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "getuserSubscriptionCheckCurrentSubsDetailsWhenUserHaveRecentlyCanceledOneSubsData")
	public static Object[][] getuserSubscriptionCheckCurrentSubsDetailsWhenUserHaveRecentlyCanceledOneSubs() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "getuserSubscriptionCheckCanceledSubsData")
	public static Object[][] getuserSubscriptionCheckCanceledSubs() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "getuserSubscriptionCheckWhenBenefitParamIsFalseData")
	public static Object[][] getuserSubscriptionCheckWhenBenefitParamIsFalse() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "getuserSubscriptionCheckTenureZeroIncentiveIsNotVisibleData")
	public static Object[][] getuserSubscriptionCheckTenureZeroIncentiveIsNotVisible() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckAfterBuyingPlanWhenMoreIncentivesAddedToPlanData")
	public static Object[][] getUserSubscriptionCheckAfterBuyingPlanWhenMoreIncentivesAddedToPlan() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "10");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "101");
		createIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, incentiveFlatPayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckWhenMultipleIncentiveAddedThenOnlyOneWillGetBasedOnPriorityData")
	public static Object[][] getUserSubscriptionCheckWhenMultipleIncentiveAddedThenOnlyOneWillGetBasedOnPriority() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "10");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "111");
		createIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, incentiveFlatPayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckWhenSubscriptionGotExpiredData")
	public static Object[][] getUserSubscriptionCheckWhenSubscriptionGotExpired() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "FLAT");
		createIncentive.put("1", "10");
		createIncentive.put("2", "-1");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "111");
		createIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, incentiveFlatPayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckFutureSubsNotVisibleOnlyCurrentSubsVisibleData")
	public static Object[][] getUserSubscriptionCheckFutureSubsNotVisibleOnlyCurrentSubsVisible() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "1");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);
		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, benefitFreebiePayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckBenefitsAppearsOnTheBasesOfTheirPriorityData")
	public static Object[][] getUserSubscriptionCheckBenefitsAppearsOnTheBasesOfTheirPriorityData() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] {
				{ planPayload[0], benefitPayload, incentivePayload, benefitFreebiePayload, incentiveFlatPayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckMappedUserIncentiveIsNotVisibleToPublicData")
	public static Object[][] getUserSubscriptionCheckMappedUserIncentiveIsNotVisibleToPublic() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] {
				{ planPayload[0], benefitPayload, incentivePayload, benefitFreebiePayload, incentiveFlatPayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckPublicUserIncentiveIsNotVisibleToMappedData")
	public static Object[][] getUserSubscriptionCheckPublicUserIncentiveIsNotVisibleToMapped() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] {
				{ planPayload[0], benefitPayload, incentivePayload, benefitFreebiePayload, incentiveFlatPayload } };

	}


	@DataProvider(name = "getUserSubscriptionCheckIncentiveIsVisibleToBothMappedAndPublicUserData")
	public static Object[][] getUserSubscriptionCheckIncentiveIsVisibleToBothMappedAndPublicUser() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] {
				{ planPayload[0], benefitPayload, incentivePayload, benefitFreebiePayload, incentiveFlatPayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckWhenPlanGotDisableAfterTakingSubscriptionData")
	public static Object[][] getUserSubscriptionCheckWhenPlanGotDisableAfterTakingSubscription() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] {
				{ planPayload[0], benefitPayload, incentivePayload, benefitFreebiePayload, incentiveFlatPayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckWhenOnePlanIsExpiredAndAnothPlanIsAboutToStartData")
	public static Object[][] getUserSubscriptionCheckWhenOnePlanIsExpiredAndAnothPlanIsAboutToStart() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validFrom = Utility.getPresentDate();
		String validTill = Utility.getFutureMonthDate(1);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "1");
		String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "100");
		createIncentive.put("6", "true");
		String incentivePayload = superhelper.populateIncentivePayload(createIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitPayload, incentivePayload, benefitFreebiePayload,
				incentiveFlatPayload, validTill, validFrom } };

	}

	@DataProvider(name = "getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsPlanData")
	public static Object[][] getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsPlan() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validFrom = Utility.getPresentDate();
		String validTill = Utility.getFutureMonthDate(1);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, validTill, validFrom } };

	}

	@DataProvider(name = "getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsIncentiveData")
	public static Object[][] getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsIncentive() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload } };

	}

	@DataProvider(name = "getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsBenefitData")
	public static Object[][] getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsBenefit() {

		HashMap<String, String> createPublicPlan = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays

		createPublicPlan.put("0", "0");
		createPublicPlan.put("1", "9");
		createPublicPlan.put("2", "1");
		createPublicPlan.put("3", "199");
		createPublicPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload } };

	}

	@DataProvider(name = "getPlanByUserAsNonLoggedPublicAndMappedUserData")
	public static Object[][] getPlanByUserAsNonLoggedPublicAndMappedUserData() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckAllActivePublicPlansForPublicUserData")
	public static Object[][] getPlanByUserCheckAllActivePublicPlansForPublicUser() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckAllActiveMappedUserPlansForMappedUserData")
	public static Object[][] getPlanByUserCheckAllActiveMappedUserPlansForMappedUser() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckExpiredMappedUserPlanData")
	public static Object[][] getPlanByUserCheckExpiredMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckExpiredPublicUserPlanData")
	public static Object[][] getPlanByUserCheckExpiredPublicUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckDisablePublicUserPlanData")
	public static Object[][] getPlanByUserCheckDisablePublicUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckDisableMappedUserPlanData")
	public static Object[][] getPlanByUserCheckDisableMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckWhereMappedUserPlanTenureIsGreaterThanValidTillOfPlanData")
	public static Object[][] getPlanByUserCheckWhereMappedUserPlanTenureIsGreaterThanValidTillOfPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");

		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckWherePublicPlanTenureIsGreaterThanValidTillOfPlanData")
	public static Object[][] getPlanByUserCheckWherePublicPlanTenureIsGreaterThanValidTillOfPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckFuturePublicPlanData")
	public static Object[][] getPlanByUserCheckFuturePublicPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validTill = Utility.getFutureMonthInMillisecond(1);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", validTill);
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateFuturePlanPayload(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckFutureMappedUserPlanData")
	public static Object[][] getPlanByUserCheckFutureMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String validTill = Utility.getFutureMonthInMillisecond(1);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", validTill);
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateFuturePlanPayload(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };

	}

	@DataProvider(name = "getPlanByUserCheckTotalAvailableOfMappedUserPlanData")
	public static Object[][] getPlanByUserCheckTotalAvailableOfMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String totalAvaliable = "1999";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, totalAvaliable } };

	}

	@DataProvider(name = "getPlanByUserCheckTotalAvailableOfPublicPlanData")
	public static Object[][] getPlanByUserCheckTotalAvailableOfPublicPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String totalAvaliable = "1999";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, totalAvaliable } };

	}

	@DataProvider(name = "getPlanByUserWhenTotalAvailableReachedItsMaxLimitPublicPlanData")
	public static Object[][] getPlanByUserWhenTotalAvailableReachedItsMaxLimitPublicPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String totalAvaliable = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, totalAvaliable } };

	}

	@DataProvider(name = "getPlanByUserWhenTotalAvailableReachedItsMaxLimitMappedUserPlanData")
	public static Object[][] getPlanByUserWhenTotalAvailableReachedItsMaxLimitMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String totalAvaliable = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, totalAvaliable } };
	}

	@DataProvider(name = "getPlanByUserWhenTotalAvailableIsZeroMappedUserPlanData")
	public static Object[][] getPlanByUserWhenTotalAvailableIsZeroMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String totalAvaliable = "0";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, totalAvaliable } };
	}

	@DataProvider(name = "getPlanByUserWhenTotalAvailableIsZeroPublicPlanData")
	public static Object[][] getPlanByUserWhenTotalAvailableIsZeroPublicPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String totalAvaliable = "0";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, totalAvaliable } };
	}

	@DataProvider(name = "getPlanByUserWhenUsageCountIsEqualToTotalAvailablePublicPlanData")
	public static Object[][] getPlanByUserWhenUsageCountIsEqualToTotalAvailablePublicPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String usageCount = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, usageCount } };
	}

	@DataProvider(name = "getPlanByUserWhenUsageCountIsEqualToTotalAvailableMappedUserPlanData")
	public static Object[][] getPlanByUserWhenUsageCountIsEqualToTotalAvailableMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String usageCount = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, usageCount } };
	}

	@DataProvider(name = "getPlanByUserWhenNoBenefitIsMappedWithMappedUserPlanData")
	public static Object[][] getPlanByUserWhenNoBenefitIsMappedWithMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String usageCount = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, usageCount } };
	}

	@DataProvider(name = "getPlanByUserWhenNoBenefitIsMappedWithPublicUserPlanData")
	public static Object[][] getPlanByUserWhenNoBenefitIsMappedWithPublicUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String usageCount = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, usageCount } };
	}

	@DataProvider(name = "getPlanByUserWhenMappedPlanIsMappedTwiceWithUserData")
	public static Object[][] getPlanByUserWhenMappedPlanIsMappedTwiceWithUser() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String usageCount = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, usageCount } };
	}

	@DataProvider(name = "getPlanByUserWhenPublicPlanIsMappedTwiceWithUserData")
	public static Object[][] getPlanByUserWhenPublicPlanIsMappedTwiceWithUser() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String usageCount = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, usageCount } };
	}

	@DataProvider(name = "getPlanByUserWhenFreeDelBenefitIsMappedTwiceWithPublicPlanData")
	public static Object[][] getPlanByUserWhenFreeDelBenefitIsMappedTwiceWithPublicPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";
		String usageCount = "2000";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, usageCount } };
	}

	@DataProvider(name = "getPlanByUserCheckPriorityOrderOfBenefitsAttachedWithPlanData")
	public static Object[][] getPlanByUserCheckPriorityOrderOfBenefitsAttachedWithPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };
	}

	@DataProvider(name = "getPlanByUserCheckPriorityOrderOfBenefitsAttachedWithMappedUserPlanData")
	public static Object[][] getPlanByUserCheckPriorityOrderOfBenefitsAttachedWithMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "10");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };
	}

	@DataProvider(name = "getPlanByUserCheckPriorityOrderOfBenefitsOfDiffTypeWithSamePriorityMappedUserPlanData")
	public static Object[][] getPlanByUserCheckPriorityOrderOfBenefitsOfDiffTypeWithSamePriorityMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };
	}

	@DataProvider(name = "getPlanByUserCheckPriorityOrderOfBenefitsOfDiffTypeWithSamePriorityPublicPlanData")
	public static Object[][] getPlanByUserCheckPriorityOrderOfBenefitsOfDiffTypeWithSamePriorityPublicPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };
	}

	@DataProvider(name = "getPlanByUserCheckWhenDisableIncentivePublicPlanData")
	public static Object[][] getPlanByUserCheckWhenDisableIncentivePublicPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// patch incentive ->> incentive_id, enabled
		patchIncentive.put("0", "");
		patchIncentive.put("1", "false");

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, patchIncentive } };
	}

	@DataProvider(name = "getPlanByUserCheckWhenDisableIncentiveMappedUserPlanData")
	public static Object[][] getPlanByUserCheckWhenDisableIncentiveMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// patch incentive ->> incentive_id, enabled
		patchIncentive.put("0", "");
		patchIncentive.put("1", "false");

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload, patchIncentive } };
	}
	@DataProvider(name = "getPlanByUserCheckWhenIncentiveOfTenureZeroMappedUserPlanData")
	public static Object[][] getPlanByUserCheckWhenIncentiveOfTenureZeroMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure , planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "0");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };
	}

	@DataProvider(name = "getPlanByUserCheckWhenIncentiveOfTenureZeroPublicUserPlanData")
	public static Object[][] getPlanByUserCheckWhenIncentiveOfTenureZeroPublicUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure , planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "0");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload } };
	}

	@DataProvider(name = "getPlanByUserCheckWhenDisableIncentiveOfTenureZeroPublicUserPlanData")
	public static Object[][] getPlanByUserCheckWhenDisableIncentiveOfTenureZeroPublicUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure , planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// patch incentive ->> incentive_id, enabled
		patchIncentive.put("0", "");
		patchIncentive.put("1", "false");

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "0");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload,patchIncentive } };
	}
	@DataProvider(name = "getPlanByUserCheckWhenDisableIncentiveOfTenureZeroMappedUserPlanData")
	public static Object[][] getPlanByUserCheckWhenDisableIncentiveOfTenureZeroMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure , planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// patch incentive ->> incentive_id, enabled
		patchIncentive.put("0", "");
		patchIncentive.put("1", "false");

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "0");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload,patchIncentive } };
	}

	@DataProvider(name = "getPlanByUserCheckWhenIncentiveOfTenureZeroAndMinusOneMappedUserPlanData")
	public static Object[][] getPlanByUserCheckWhenIncentiveOfTenureZeroAndMinusOneMappedUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure , planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// patch incentive ->> incentive_id, enabled
		patchIncentive.put("0", "");
		patchIncentive.put("1", "false");

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "0");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "101");
		createIncentive.put("6", "true");
		String incentivePecentagePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload,patchIncentive,incentivePecentagePayload } };
	}

	@DataProvider(name = "getPlanByUserCheckWhenIncentiveOfTenureZeroAndMinusOnePublicUserPlanData")
	public static Object[][] getPlanByUserCheckWhenIncentiveOfTenureZeroAndMinusOnePublicUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure , planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// patch incentive ->> incentive_id, enabled
		patchIncentive.put("0", "");
		patchIncentive.put("1", "false");

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "0");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "101");
		createIncentive.put("6", "true");
		String incentivePecentagePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload,patchIncentive,incentivePecentagePayload } };
	}

	@DataProvider(name = "getPlanByUserCheckWhenIncentiveOfSameTenureZeroPublicUserPlanData")
	public static Object[][] getPlanByUserCheckWhenIncentiveOfSameTenureZeroPublicUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure , planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// patch incentive ->> incentive_id, enabled
		patchIncentive.put("0", "");
		patchIncentive.put("1", "false");

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "0");
		createFlatIncentive.put("5", "101");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "0");
		createIncentive.put("5", "101");
		createIncentive.put("6", "true");
		String incentivePecentagePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload,patchIncentive,incentivePecentagePayload } };
	}

	@DataProvider(name = "getPlanByUserCheckWhenIncentiveOfSameTenureMinusOnePublicUserPlanData")
	public static Object[][] getPlanByUserCheckWhenIncentiveOfSameTenureMinusOnePublicUserPlan() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		HashMap<String, String> createPlanTwo = new HashMap<String, String>();
		HashMap<String, String> createBenefit = new HashMap<String, String>();
		HashMap<String, String> createFreebieBenefit = new HashMap<String, String>();
		HashMap<String, String> createIncentive = new HashMap<String, String>();
		HashMap<String, String> createFlatIncentive = new HashMap<String, String>();
		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String numOfPlans = "1";

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, numOfPlans);

		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure , planPrice , renewalOffsetDays
		createPlanTwo.put("0", "0");
		createPlanTwo.put("1", "10");
		createPlanTwo.put("2", "1");
		createPlanTwo.put("3", "99");
		createPlanTwo.put("4", "27");
		String[] planTwoPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlanTwo, numOfPlans);

		// type, priority
		createBenefit.put("0", "FREE_DELIVERY");
		createBenefit.put("1", "11");
		String benefitFreeDelPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
				createBenefit.get(Integer.toString(1)));

		// type, priority
		createFreebieBenefit.put("0", "Freebie");
		createFreebieBenefit.put("1", "11");
		String benefitFreebiePayload = superhelper.populateBenefitData(createFreebieBenefit.get(Integer.toString(0)),
				createFreebieBenefit.get(Integer.toString(1)));

		// patch incentive ->> incentive_id, enabled
		patchIncentive.put("0", "");
		patchIncentive.put("1", "false");

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createFlatIncentive.put("0", "FLAT");
		createFlatIncentive.put("1", "10");
		createFlatIncentive.put("2", "-1");
		createFlatIncentive.put("3", "null");
		createFlatIncentive.put("4", "-1");
		createFlatIncentive.put("5", "100");
		createFlatIncentive.put("6", "true");
		String incentiveFlatPayload = superhelper.populateIncentivePayload(createFlatIncentive);

		// type, value, cap, paymentMethods, tenure, priority, enabled
		createIncentive.put("0", "PERCENTAGE");
		createIncentive.put("1", "10");
		createIncentive.put("2", "10");
		createIncentive.put("3", "null");
		createIncentive.put("4", "-1");
		createIncentive.put("5", "101");
		createIncentive.put("6", "true");
		String incentivePecentagePayload = superhelper.populateIncentivePayload(createIncentive);

		return new Object[][] { { planPayload[0], benefitFreebiePayload, incentiveFlatPayload, planTwoPayload[0],
				benefitFreeDelPayload,patchIncentive,incentivePecentagePayload } };
	}

	@DataProvider(name = "createPlanWhereTitleIsEmptyData")
	public static Object[][] createPlanWhereTitleIsEmpty() {
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "test_plan");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "1");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "99");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereDescriptionIsEmptyData")
	public static Object[][] createPlanWhereDescriptionIsEmpty() {
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "");
		createPlan.put("2", "test_plan");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "1");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "99");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereNameIsEmptyData")
	public static Object[][] createPlanWhereNameIsEmpty() {
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "1");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "99");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}
	@DataProvider(name = "createPlanWhereDuplicatePlanNameData")
	public static Object[][] createPlanWhereDuplicatePlanName() {
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "TEST_AUTOMATION_PLAN");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "1");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "99");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "2");

		return new Object[][] { {createPlanPayload[0], createPlanPayload[1]  } };
	}

	@DataProvider(name = "createPlanWhereValidFromIsGreaterThanValidTillData")
	public static Object[][] createPlanWhereValidFromIsGreaterThanValidTill() {
		HashMap<String, String> createPlan = new HashMap<String, String>();
		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure , planPrice , renewalOffsetDays
		createPlan.put("0", Utility.getFutureMonthInMillisecond(7));
		createPlan.put("1", "6");
		createPlan.put("2", "1");
		createPlan.put("3", "99");

		String[] createPlanPayload=superhelper.populateFuturePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0] } };
	}


	@DataProvider(name = "createPlanWhereTenureOtherThanThreeTwoAndOneValueData")
	public static Object[][] createPlanWhereTenureOtherThanThreeTwoAndOneValue() {
		//10- tenure (6)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "10");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "99");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}
	@DataProvider(name = "createPlanWhereTenureIsZeroData")
	public static Object[][] createPlanWhereTenureIsZero() {
		//0- tenure (6)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "0");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "99");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereTenureIsMinusOneData")
	public static Object[][] createPlanWhereTenureIsMinusOne() {
		//0- tenure (6)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "-1");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "99");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereTenureIsGreaterThanPlanPeriodData")
	public static Object[][] createPlanWhereTenureIsGreaterThanPlanPeriod() {
		//0- tenure (6)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "7");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "99");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereTenureIsEqualToPlanPeriodData")
	public static Object[][] createPlanWhereTenureIsEqualToPlanPeriod() {
		//0- tenure (6)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "7");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "99");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}


	@DataProvider(name = "createPlanWherePlanPriceIsZeroData")
	public static Object[][] createPlanWherePlanPriceIsZero() {
		//0- price (9)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "0");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}


	@DataProvider(name = "createPlanWherePlanPriceIsNegativeValueData")
	public static Object[][] createPlanWherePlanPriceIsNegativeValue() {
		//0- price (9)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "-1");
		createPlan.put("10", "true");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWherePlanEnabledIsFalseData")
	public static Object[][] createPlanWherePlanEnabledIsFalse() {
		//0- enabled (10)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWherePlanOffsetDaysGreaterThanTwentySevenData")
	public static Object[][] createPlanWherePlanOffsetDaysGreaterThanTwentySeven() {
		//28- renewal offset (11)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "28");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}
	@DataProvider(name = "createPlanWherePlanCreatedByIsEmptyStringData")
	public static Object[][] createPlanWherePlanCreatedByIsEmptyString() {
		// created by (12)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWherePlanTotalAvailableIsZeroData")
	public static Object[][] createPlanWherePlanTotalAvailableIsZero() {
		// 0-total available (15)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "0");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWherePlanTotalAvailableIsNullData")
	public static Object[][] createPlanWherePlanTotalAvailableIsNull() {
		// null-total available (15)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "null");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWherePlanTncIsEmptyData")
	public static Object[][] createPlanWherePlanTncIsEmpty() {
		// -tnc (17)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "100");
		createPlan.put("16", "5");
		createPlan.put("17", "");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createMultiplePlanWithInSameTimeTenurePriceData")
	public static Object[][] createMultiplePlanWithInSameTimeTenurePrice() {

		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "100");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "2");

		return new Object[][] { {createPlanPayload[0],createPlanPayload[1]  } };
	}

	@DataProvider(name = "createPlanWhereTitleIsBlankData")
	public static Object[][] createPlanWhereTitleIsBlank() {
		// -title (0)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "      ");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "100");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereDescriptionIsBlankData")
	public static Object[][] createPlanWhereDescriptionIsBlank() {
		// -Description (02)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "     ");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "100");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereNameIsBlankData")
	public static Object[][] createPlanWhereNameIsBlank() {
		// -name (02)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "100");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereCreatedByIsBlankData")
	public static Object[][] createPlanWhereCreatedByIsBlank() {
		// -created by (12)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "         ");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "100");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereIsRecommendedIsTrueData")
	public static Object[][] createPlanWhereIsRecommendedIsTrue() {
		// -isRecommended (14)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "true");
		createPlan.put("15", "100");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereIsRecommendedIsFalseData")
	public static Object[][] createPlanWhereIsRecommendedIsFalse() {
		// -isRecommended (14)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "100");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "createPlanWhereExpiryOffsetIsNullData")
	public static Object[][] createPlanWhereExpiryOffsetIsNull() {
		// -isRecommended (14)
		HashMap<String, String> createPlan = new HashMap<String, String>();
		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "By_Super_Automation");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "100");
		createPlan.put("16", "null");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");

		return new Object[][] { {createPlanPayload[0]  } };
	}

	@DataProvider(name = "patchPlanWithNonExistingPlanIdData")
	public static Object[][] patchPlanWithNonExistingPlanId() {
		// -isRecommended (14)
		HashMap<String, String> patchPlan = new HashMap<String, String>();
		String planId = Integer.toString(Utility.getRandom(120000,200000 ));
		// plan_id, enabled, valid_till, count
		patchPlan.put("0", planId);
		patchPlan.put("1", "true");
		patchPlan.put("2", Utility.getFutureMonthInMillisecond(5));
		patchPlan.put("3", "50");

		String patchPlanPayload=superhelper.populatePatchPlanPayload(patchPlan);

		return new Object[][] { {patchPlanPayload  } };
	}
	@DataProvider(name = "patchPlanWithCountAsZeroData")
	public static Object[][] patchPlanWithCountAsZero() {
		HashMap<String, String> patchPlan = new HashMap<String, String>();
		HashMap<String ,String > createPlan= new HashMap<String, String >();
		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, "1");
		String planId=superhelper.createPlanHelper(planPayload[0]);

		// plan_id, enabled, valid_till, count
		patchPlan.put("0", planId);
		patchPlan.put("1", "true");
		patchPlan.put("2", Utility.getFutureMonthInMillisecond(5));
		patchPlan.put("3", "0");

		String patchPlanPayload=superhelper.populatePatchPlanPayload(patchPlan);

		return new Object[][] { {patchPlanPayload  } };
	}

	@DataProvider(name = "patchPlanDisablePlanData")
	public static Object[][] patchPlanDisablePlan() {

		HashMap<String, String> patchPlan = new HashMap<String, String>();
		HashMap<String ,String > createPlan= new HashMap<String, String >();
		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "3");
		createPlan.put("3", "199");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, "1");
		String planId=superhelper.createPlanHelper(planPayload[0]);

		// plan_id, enabled, valid_till, count
		patchPlan.put("0", planId);
		patchPlan.put("1", "false");
		patchPlan.put("2", Utility.getFutureMonthInMillisecond(5));
		patchPlan.put("3", "0");

		String patchPlanPayload=superhelper.populatePatchPlanPayload(patchPlan);

		return new Object[][] { {patchPlanPayload  } };
	}

	@DataProvider(name = "patchPlanEnablePlanData")
	public static Object[][] patchPlanEnablePlan() {

		HashMap<String, String> patchPlan = new HashMap<String, String>();
		HashMap<String ,String > createPlan= new HashMap<String, String >();

		createPlan.put("0", "Test_Plan");
		createPlan.put("1", "Test_Automation_Plan_description");
		createPlan.put("2", "Test_Plan_Name");
		createPlan.put("3", "tdxctewyhd2gfd");
		createPlan.put("4", "0");
		createPlan.put("5", "6");
		createPlan.put("6", "3");
		createPlan.put("7", "1.111");
		createPlan.put("8", "2.111");
		createPlan.put("9", "199");
		createPlan.put("10", "false");
		createPlan.put("11", "27");
		createPlan.put("12", "Test-Automation-Script");
		createPlan.put("13", "false");
		createPlan.put("14", "false");
		createPlan.put("15", "1000");
		createPlan.put("16", "5");
		createPlan.put("17", "offer valid on... Payment types, test-automation");

		String[] createPlanPayload=superhelper.populatePlanPayload(createPlan, "1");
		String planId=superhelper.createPlanHelper(createPlanPayload[0]);

		// plan_id, enabled, valid_till, count
		patchPlan.put("0", planId);
		patchPlan.put("1", "true");
		patchPlan.put("2", Utility.getFutureMonthInMillisecond(5));
		patchPlan.put("3", "0");

		String patchPlanPayload=superhelper.populatePatchPlanPayload(patchPlan);

		return new Object[][] { {patchPlanPayload  } };
	}


	@DataProvider(name = "patchPlanUpdateTotalAvailableCountData")
	public static Object[][] patchPlanUpdateTotalAvailableCount() {

		HashMap<String, String> patchPlan = new HashMap<String, String>();
		HashMap<String ,String > createPlan= new HashMap<String, String >();
		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "9");
		createPlan.put("2", "1");
		createPlan.put("3", "99");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, "1");
		String planId=superhelper.createPlanHelper(planPayload[0]);

		// plan_id, enabled, valid_till, count
		patchPlan.put("0", planId);
		patchPlan.put("1", "true");
		patchPlan.put("2", Utility.getFutureMonthInMillisecond(5));
		patchPlan.put("3", "100");

		String patchPlanPayload=superhelper.populatePatchPlanPayload(patchPlan);

		return new Object[][] { {patchPlanPayload  } };
	}

	@DataProvider(name = "patchPlanValidTillSmallerThanValidTillData")
	public static Object[][] patchPlanValidTillSmallerThanValidTill() {

		HashMap<String, String> patchPlan = new HashMap<String, String>();
		HashMap<String ,String > createPlan= new HashMap<String, String >();
		// value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
		// planPrice , renewalOffsetDays
		createPlan.put("0", "0");
		createPlan.put("1", "4");
		createPlan.put("2", "1");
		createPlan.put("3", "99");
		createPlan.put("4", "27");
		String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPlan, "1");
		String planId=superhelper.createPlanHelper(planPayload[0]);

		// plan_id, enabled, valid_till, count
		patchPlan.put("0", planId);
		patchPlan.put("1", "true");
		patchPlan.put("2", Utility.getFutureMonthInMillisecond(-5));
		patchPlan.put("3", "100");

		String patchPlanPayload=superhelper.populatePatchPlanPayload(patchPlan);

		return new Object[][] { {patchPlanPayload  } };
	}


	@DataProvider(name = "createBenefitCheckTitleData")
	public static Object[][] createBenefitCheckTitle() {
		HashMap<String ,String > createBenefit= new HashMap<String, String >();
		HashMap<String ,String > createBenefitTwo= new HashMap<String, String >();
		HashMap<String ,String > createBenefitThree= new HashMap<String, String >();
		HashMap<String ,String > createBenefitFour= new HashMap<String, String >();

		// title, description, name, logo id, meta, type, created by, priority
//		createBenefit.put("0", "Automated_Benefit");
//		createBenefit.put("1", "Automated_Benefit_Description_Test");
//		createBenefit.put("2", "Auto-Benefit_name");
//		createBenefit.put("3", "z1d3z4ws42ryer");
//		createBenefit.put("4", "null");
//		createBenefit.put("5", "FREE_DELIVERY");
//		createBenefit.put("6", "SuperAutomationScript");
//		createBenefit.put("7", "10");
//
//		String benefitPayload=new Createbenefit(createBenefit).toString();
		String titleName = "";
		createBenefitTwo.put("0", titleName);
		createBenefitTwo.put("1", "Automated_Benefit_Description_Test");
		createBenefitTwo.put("2", "Auto-Benefit_name");
		createBenefitTwo.put("3", "z1d3z4ws42ryer");
		createBenefitTwo.put("4", "null");
		createBenefitTwo.put("5", "FREE_DELIVERY");
		createBenefitTwo.put("6", "SuperAutomationScript");
		createBenefitTwo.put("7", "10");

		String benefitTwoPayload=new Createbenefit(createBenefitTwo).toString();

		titleName = "  ";
		createBenefitThree.put("0", titleName);
		createBenefitThree.put("1", "Automated_Benefit_Description_Test");
		createBenefitThree.put("2", "Auto-Benefit_name");
		createBenefitThree.put("3", "z1d3z4ws42ryer");
		createBenefitThree.put("4", "null");
		createBenefitThree.put("5", "FREE_DELIVERY");
		createBenefitThree.put("6", "SuperAutomationScript");
		createBenefitThree.put("7", "10");

		String benefitThreePayload=new Createbenefit(createBenefitThree).toString();

		char titleNameWithNull = '#';
		createBenefitFour.put("0", " "); //TODO check for null
		createBenefitFour.put("1", "Automated_Benefit_Description_Test");
		createBenefitFour.put("2", "Auto-Benefit_name");
		createBenefitFour.put("3", "z1d3z4ws42ryer");
		createBenefitFour.put("4", "null");
		createBenefitFour.put("5", "FREE_DELIVERY");
		createBenefitFour.put("6", "SuperAutomationScript");
		createBenefitFour.put("7", "10");

		Createbenefit benefit = new Createbenefit(createBenefitFour);

		return new Object[][] { {benefitTwoPayload },{benefitThreePayload},{benefit.toString()} };
	}

	@DataProvider(name = "createBenefitCheckDescriptionData")
	public static Object[][] createBenefitCheckDescription() {
		HashMap<String ,String > createBenefit= new HashMap<String, String >();
		HashMap<String ,String > createBenefitTwo= new HashMap<String, String >();
		HashMap<String ,String > createBenefitThree= new HashMap<String, String >();
		HashMap<String ,String > createBenefitFour= new HashMap<String, String >();

		// title, description, name, logo id, meta, type, created by, priority
//		createBenefit.put("0", "Automated_Benefit");
//		createBenefit.put("1", "Automated_Benefit_Description_Test");
//		createBenefit.put("2", "Auto-Benefit_name");
//		createBenefit.put("3", "z1d3z4ws42ryer");
//		createBenefit.put("4", "null");
//		createBenefit.put("5", "FREE_DELIVERY");
//		createBenefit.put("6", "SuperAutomationScript");
//		createBenefit.put("7", "10");
//
//		String benefitPayload=new Createbenefit(createBenefit).toString();
		String titleName = "Automated_Benefit";
		createBenefitTwo.put("0","Automated_Benefit");
		createBenefitTwo.put("1", "");
		createBenefitTwo.put("2", "Auto-Benefit_name");
		createBenefitTwo.put("3", "z1d3z4ws42ryer");
		createBenefitTwo.put("4", "null");
		createBenefitTwo.put("5", "FREE_DELIVERY");
		createBenefitTwo.put("6", "SuperAutomationScript");
		createBenefitTwo.put("7", "10");

		String benefitTwoPayload=new Createbenefit(createBenefitTwo).toString();

		titleName = "Automated_Benefit";
		createBenefitThree.put("0", "Automated_Benefit");
		createBenefitThree.put("1", "  ");
		createBenefitThree.put("2", "Auto-Benefit_name");
		createBenefitThree.put("3", "z1d3z4ws42ryer");
		createBenefitThree.put("4", "null");
		createBenefitThree.put("5", "FREE_DELIVERY");
		createBenefitThree.put("6", "SuperAutomationScript");
		createBenefitThree.put("7", "10");

		String benefitThreePayload=new Createbenefit(createBenefitThree).toString();

		char titleNameWithNull = '#';
		createBenefitFour.put("0", "Automated_Benefit");
		createBenefitFour.put("1", " ");	// TODO need to check for null
		createBenefitFour.put("2", "Auto-Benefit_name");
		createBenefitFour.put("3", "z1d3z4ws42ryer");
		createBenefitFour.put("4", "null");
		createBenefitFour.put("5", "FREE_DELIVERY");
		createBenefitFour.put("6", "SuperAutomationScript");
		createBenefitFour.put("7", "10");

		String benefitFourPayload=new Createbenefit(createBenefitFour).toString();
		return new Object[][] { {benefitTwoPayload },{benefitThreePayload},{benefitFourPayload} };
	}

	@DataProvider(name = "createBenefitCheckNameData")
	public static Object[][] createBenefitCheckName() {
		HashMap<String ,String > createBenefit= new HashMap<String, String >();
		HashMap<String ,String > createBenefitTwo= new HashMap<String, String >();
		HashMap<String ,String > createBenefitThree= new HashMap<String, String >();
		HashMap<String ,String > createBenefitFour= new HashMap<String, String >();

		String titleName = "Automated_Benefit";
		createBenefitTwo.put("0","Automated_Benefit");
		createBenefitTwo.put("1", "Automated_Benefit_Description_Test");
		createBenefitTwo.put("2", "");
		createBenefitTwo.put("3", "z1d3z4ws42ryer");
		createBenefitTwo.put("4", "null");
		createBenefitTwo.put("5", "FREE_DELIVERY");
		createBenefitTwo.put("6", "SuperAutomationScript");
		createBenefitTwo.put("7", "10");

		String benefitTwoPayload=new Createbenefit(createBenefitTwo).toString();

		titleName = "Automated_Benefit";
		createBenefitThree.put("0", "Automated_Benefit");
		createBenefitThree.put("1", "Automated_Benefit_Description_Test");
		createBenefitThree.put("2", "  ");
		createBenefitThree.put("3", "z1d3z4ws42ryer");
		createBenefitThree.put("4", "null");
		createBenefitThree.put("5", "FREE_DELIVERY");
		createBenefitThree.put("6", "SuperAutomationScript");
		createBenefitThree.put("7", "10");

		String benefitThreePayload=new Createbenefit(createBenefitThree).toString();

		char titleNameWithNull = '#';
		createBenefitFour.put("0", "Automated_Benefit");
		createBenefitFour.put("1", "Automated_Benefit_Description_Test");
		createBenefitFour.put("2", " ");	//TODO need to check for null
		createBenefitFour.put("3", "z1d3z4ws42ryer");
		createBenefitFour.put("4", "null");
		createBenefitFour.put("5", "FREE_DELIVERY");
		createBenefitFour.put("6", "SuperAutomationScript");
		createBenefitFour.put("7", "10");

		String benefitFourPayload=new Createbenefit(createBenefitFour).toString();
		return new Object[][] { {benefitTwoPayload },{benefitThreePayload},{benefitFourPayload} };
	}

	@DataProvider(name = "createBenefitCheckLogoIdData")
	public static Object[][] createBenefitCheckLogoId() {
		HashMap<String ,String > createBenefit= new HashMap<String, String >();
		HashMap<String ,String > createBenefitTwo= new HashMap<String, String >();
		HashMap<String ,String > createBenefitThree= new HashMap<String, String >();
		HashMap<String ,String > createBenefitFour= new HashMap<String, String >();

		createBenefitTwo.put("0","Automated_Benefit");
		createBenefitTwo.put("1", "Automated_Benefit_Description_Test");
		createBenefitTwo.put("2", "Auto-Benefit_name");
		createBenefitTwo.put("3", "");
		createBenefitTwo.put("4", "null");
		createBenefitTwo.put("5", "FREE_DELIVERY");
		createBenefitTwo.put("6", "SuperAutomationScript");
		createBenefitTwo.put("7", "10");

		String benefitTwoPayload=new Createbenefit(createBenefitTwo).toString();

		createBenefitThree.put("0", "Automated_Benefit");
		createBenefitThree.put("1", "Automated_Benefit_Description_Test");
		createBenefitThree.put("2", "Auto-Benefit_name");
		createBenefitThree.put("3", "  ");
		createBenefitThree.put("4", "null");
		createBenefitThree.put("5", "FREE_DELIVERY");
		createBenefitThree.put("6", "SuperAutomationScript");
		createBenefitThree.put("7", "10");

		String benefitThreePayload=new Createbenefit(createBenefitThree).toString();

		char titleNameWithNull = '#';
		createBenefitFour.put("0", "Automated_Benefit");
		createBenefitFour.put("1", "Automated_Benefit_Description_Test");
		createBenefitFour.put("2", "Auto-Benefit_name");
		createBenefitFour.put("3", " ");  // TODO need to check for null
		createBenefitFour.put("4", "null");
		createBenefitFour.put("5", "FREE_DELIVERY");
		createBenefitFour.put("6", "SuperAutomationScript");
		createBenefitFour.put("7", "10");

		String benefitFourPayload=new Createbenefit(createBenefitFour).toString();
		return new Object[][] { {benefitTwoPayload },{benefitThreePayload},{benefitFourPayload} };
	}

	@DataProvider(name = "createBenefitCreatedByData")
	public static Object[][] createBenefitCreatedBy() {
		HashMap<String ,String > createBenefit= new HashMap<String, String >();
		HashMap<String ,String > createBenefitTwo= new HashMap<String, String >();
		HashMap<String ,String > createBenefitThree= new HashMap<String, String >();
		HashMap<String ,String > createBenefitFour= new HashMap<String, String >();

		createBenefitTwo.put("0","Automated_Benefit");
		createBenefitTwo.put("1", "Automated_Benefit_Description_Test");
		createBenefitTwo.put("2", "Auto-Benefit_name");
		createBenefitTwo.put("3", "z1d3z4ws42ryer");
		createBenefitTwo.put("4", "null");
		createBenefitTwo.put("5", "FREE_DELIVERY");
		createBenefitTwo.put("6", "");
		createBenefitTwo.put("7", "10");

		String benefitTwoPayload=new Createbenefit(createBenefitTwo).toString();

		createBenefitThree.put("0", "Automated_Benefit");
		createBenefitThree.put("1", "Automated_Benefit_Description_Test");
		createBenefitThree.put("2", "Auto-Benefit_name");
		createBenefitThree.put("3", "z1d3z4ws42ryer");
		createBenefitThree.put("4", "null");
		createBenefitThree.put("5", "FREE_DELIVERY");
		createBenefitThree.put("6", "  ");
		createBenefitThree.put("7", "10");

		String benefitThreePayload=new Createbenefit(createBenefitThree).toString();

		createBenefitFour.put("0", "Automated_Benefit");
		createBenefitFour.put("1", "Automated_Benefit_Description_Test");
		createBenefitFour.put("2", "Auto-Benefit_name");
		createBenefitFour.put("3", "z1d3z4ws42ryer");
		createBenefitFour.put("4", "null");
		createBenefitFour.put("5", "FREE_DELIVERY");
		createBenefitFour.put("6", " "); //TODO need to check for null
		createBenefitFour.put("7", "10");

		String benefitFourPayload=new Createbenefit(createBenefitFour).toString();
		return new Object[][] { {benefitTwoPayload },{benefitThreePayload},{benefitFourPayload} };
	}

	@DataProvider(name = "createBenefitCheckPriorityData")
	public static Object[][] createBenefitCheckPriority() {
		HashMap<String ,String > createBenefit= new HashMap<String, String >();
		HashMap<String ,String > createBenefitTwo= new HashMap<String, String >();
		HashMap<String ,String > createBenefitThree= new HashMap<String, String >();
		HashMap<String ,String > createBenefitFour= new HashMap<String, String >();
		//Priority (7)
		String titleName = "Automated_Benefit";
		createBenefitTwo.put("0","Automated_Benefit");
		createBenefitTwo.put("1", "Automated_Benefit_Description_Test");
		createBenefitTwo.put("2", "Auto-Benefit_name");
		createBenefitTwo.put("3", "z1d3z4ws42ryer");
		createBenefitTwo.put("4", "null");
		createBenefitTwo.put("5", "FREE_DELIVERY");
		createBenefitTwo.put("6", "SuperAutomationScript");
		createBenefitTwo.put("7", "99");

		String benefitTwoPayload=new Createbenefit(createBenefitTwo).toString();

		titleName = "Automated_Benefit";
		createBenefitThree.put("0", "Automated_Benefit");
		createBenefitThree.put("1", "Automated_Benefit_Description_Test");
		createBenefitThree.put("2", "Auto-Benefit_name");
		createBenefitThree.put("3", "z1d3z4ws42ryer");
		createBenefitThree.put("4", "null");
		createBenefitThree.put("5", "FREE_DELIVERY");
		createBenefitThree.put("6", "SuperAutomationScript");
		createBenefitThree.put("7", "0");

		String benefitThreePayload=new Createbenefit(createBenefitThree).toString();

		createBenefitFour.put("0", "Automated_Benefit");
		createBenefitFour.put("1", "Automated_Benefit_Description_Test");
		createBenefitFour.put("2", "Auto-Benefit_name");
		createBenefitFour.put("3", "z1d3z4ws42ryer");
		createBenefitFour.put("4", "null");
		createBenefitFour.put("5", "FREE_DELIVERY");
		createBenefitFour.put("6", null);
		createBenefitFour.put("7", "-1");

		String benefitFourPayload=new Createbenefit(createBenefitFour).toString();
		return new Object[][] { {benefitTwoPayload },{benefitThreePayload},{benefitFourPayload} };
	}


	@DataProvider(name = "createBenefitCheckBenefitTypeData")
	public static Object[][] createBenefitCheckBenefitType() {
		HashMap<String ,String > createBenefit= new HashMap<String, String >();
		HashMap<String ,String > createBenefitTwo= new HashMap<String, String >();
		HashMap<String ,String > createBenefitThree= new HashMap<String, String >();
		HashMap<String ,String > createBenefitFour= new HashMap<String, String >();
		//Priority (7)
		String titleName = "Automated_Benefit";
		createBenefitTwo.put("0","Automated_Benefit");
		createBenefitTwo.put("1", "Automated_Benefit_Description_Test");
		createBenefitTwo.put("2", "Auto-Benefit_name");
		createBenefitTwo.put("3", "z1d3z4ws42ryer");
		createBenefitTwo.put("4", "null");
		createBenefitTwo.put("5", "FREE_DELIVERY");
		createBenefitTwo.put("6", "SuperAutomationScript");
		createBenefitTwo.put("7", "99");

		String benefitTwoPayload=new Createbenefit(createBenefitTwo).toString();

		titleName = "Automated_Benefit";
		createBenefitThree.put("0", "Automated_Benefit");
		createBenefitThree.put("1", "Automated_Benefit_Description_Test");
		createBenefitThree.put("2", "Auto-Benefit_name");
		createBenefitThree.put("3", "z1d3z4ws42ryer");
		createBenefitThree.put("4", "null");
		createBenefitThree.put("5", "Freebie");
		createBenefitThree.put("6", "SuperAutomationScript");
		createBenefitThree.put("7", "9");

		String benefitThreePayload=new Createbenefit(createBenefitThree).toString();

		createBenefitFour.put("0", "Automated_Benefit");
		createBenefitFour.put("1", "Automated_Benefit_Description_Test");
		createBenefitFour.put("2", "Auto-Benefit_name");
		createBenefitFour.put("3", "z1d3z4ws42ryer");
		createBenefitFour.put("4", "null");
		createBenefitFour.put("5", "CC_PRIORITY");
		createBenefitFour.put("6", "SuperAutomationScript");
		createBenefitFour.put("7", "10");

		String benefitFourPayload=new Createbenefit(createBenefitFour).toString();
		return new Object[][] { {benefitTwoPayload },{benefitThreePayload},{benefitFourPayload} };
	}

	@DataProvider(name = "createSameBenefitData")
	public static Object[][] createSameBenefit() {
		HashMap<String ,String > createBenefit= new HashMap<String, String >();
		HashMap<String ,String > createBenefitTwo= new HashMap<String, String >();
		HashMap<String ,String > createBenefitThree= new HashMap<String, String >();

		createBenefitTwo.put("0","Automated_Benefit");
		createBenefitTwo.put("1", "Automated_Benefit_Description_Test");
		createBenefitTwo.put("2", "Auto-Benefit_name");
		createBenefitTwo.put("3", "z1d3z4ws42ryer");
		createBenefitTwo.put("4", "null");
		createBenefitTwo.put("5", "FREE_DELIVERY");
		createBenefitTwo.put("6", "SuperAutomationScript");
		createBenefitTwo.put("7", "99");

		String benefitTwoPayload=new Createbenefit(createBenefitTwo).toString();

		createBenefitThree.put("0","Automated_Benefit");
		createBenefitThree.put("1", "Automated_Benefit_Description_Test");
		createBenefitThree.put("2", "Auto-Benefit_name");
		createBenefitThree.put("3", "z1d3z4ws42ryer");
		createBenefitThree.put("4", "null");
		createBenefitThree.put("5", "Freebie");
		createBenefitThree.put("6", "SuperAutomationScript");
		createBenefitThree.put("7", "99");

		String benefitThreePayload=new Createbenefit(createBenefitThree).toString();
		return new Object[][] { {benefitTwoPayload } ,{benefitThreePayload}};
	}

	@DataProvider(name = "createIncentiveCheckTitleDescriptionNameData")
	public static Object[][] createIncentiveCheckTitleDescriptionName() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		//title (0)
		createIncentive.put("0", "");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "PERCENTAGE");
		createIncentive.put("5", "10");
		createIncentive.put("6", "15");
		createIncentive.put("7", "null");
		createIncentive.put("8", "-1");
		createIncentive.put("9", "10");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");
		//description
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "PERCENTAGE");
		createIncentiveTwo.put("5", "10");
		createIncentiveTwo.put("6", "15");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "-1");
		createIncentiveTwo.put("9", "10");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "By-Super-Atomation");

		//name
		createIncentiveThree.put("0", "Automated_Incentive");
		createIncentiveThree.put("1", "Automated_Incentive_Description_Test");
		createIncentiveThree.put("2", "");
		createIncentiveThree.put("3", "z1d3z4ws42");
		createIncentiveThree.put("4", "PERCENTAGE");
		createIncentiveThree.put("5", "10");
		createIncentiveThree.put("6", "15");
		createIncentiveThree.put("7", "null");
		createIncentiveThree.put("8", "-1");
		createIncentiveThree.put("9", "10");
		createIncentiveThree.put("10", "1");
		createIncentiveThree.put("11", "By-Super-Atomation");

		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();
		String incentiveThreePayload=new CreateIncentive( createIncentiveThree).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload },{incentiveThreePayload } };
	}

	@DataProvider(name = "createIncentiveCheckMetaTypeFlatData")
	public static Object[][] createIncentiveCheckMetaTypeFlat() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		// (0)
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "FLAT");
		createIncentive.put("5", "10");
		createIncentive.put("6", "15");
		createIncentive.put("7", "null");
		createIncentive.put("8", "-1");
		createIncentive.put("9", "10");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");

		String incentivePayload=new CreateIncentive( createIncentive).toString();

		return new Object[][] { {incentivePayload }};
	}
	@DataProvider(name = "createIncentiveCheckMetaValueData")
	public static Object[][] createIncentiveCheckMetaValue() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		// meta-value 
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "FLAT");
		createIncentive.put("5", "null");
		createIncentive.put("6", "15");
		createIncentive.put("7", "null");
		createIncentive.put("8", "-1");
		createIncentive.put("9", "10");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");

		// meta-value 
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "Automated_Incentive_Description_Test");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "FLAT");
		createIncentiveTwo.put("5", "0");
		createIncentiveTwo.put("6", "15");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "-1");
		createIncentiveTwo.put("9", "10");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "By-Super-Atomation");

		// meta-value 
		createIncentiveThree.put("0", "Automated_Incentive");
		createIncentiveThree.put("1", "Automated_Incentive_Description_Test");
		createIncentiveThree.put("2", "Automate-Incentive_Name");
		createIncentiveThree.put("3", "z1d3z4ws42");
		createIncentiveThree.put("4", "FLAT");
		createIncentiveThree.put("5", "-1");
		createIncentiveThree.put("6", "15");
		createIncentiveThree.put("7", "null");
		createIncentiveThree.put("8", "-1");
		createIncentiveThree.put("9", "10");
		createIncentiveThree.put("10", "1");
		createIncentiveThree.put("11", "By-Super-Atomation");

		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();
		String incentiveThreePayload=new CreateIncentive( createIncentiveThree).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload }};
	}

	@DataProvider(name = "createIncentiveCheckIncentiveTenureData")
	public static Object[][] createIncentiveCheckIncentiveTenure() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		// tenure
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "FLAT");
		createIncentive.put("5", "10");
		createIncentive.put("6", "15");
		createIncentive.put("7", "null");
		createIncentive.put("8", "0");
		createIncentive.put("9", "10");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");

		// tenure 
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "Automated_Incentive_Description_Test");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "PERCENTAGE");
		createIncentiveTwo.put("5", "10");
		createIncentiveTwo.put("6", "15");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "-1");
		createIncentiveTwo.put("9", "10");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "By-Super-Atomation");


		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload }};
	}

	@DataProvider(name = "createIncentiveCheckIncentiveTenureOtherThanZeroMinusOneData")
	public static Object[][] createIncentiveCheckIncentiveTenureOtherThanZeroMinusOne() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		// tenure
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "FLAT");
		createIncentive.put("5", "10");
		createIncentive.put("6", "15");
		createIncentive.put("7", "null");
		createIncentive.put("8", "2");
		createIncentive.put("9", "10");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");

		// tenure 
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "Automated_Incentive_Description_Test");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "PERCENTAGE");
		createIncentiveTwo.put("5", "10");
		createIncentiveTwo.put("6", "15");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "3");
		createIncentiveTwo.put("9", "10");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "By-Super-Atomation");


		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload }};
	}

	@DataProvider(name = "createIncentiveCheckIncentivePriorityData")
	public static Object[][] createIncentiveCheckIncentivePriorityTest() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		// priority (9)
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "FLAT");
		createIncentive.put("5", "10");
		createIncentive.put("6", "15");
		createIncentive.put("7", "null");
		createIncentive.put("8", "0");
		createIncentive.put("9", "-1");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");

		// priority  (9)
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "Automated_Incentive_Description_Test");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "PERCENTAGE");
		createIncentiveTwo.put("5", "10");
		createIncentiveTwo.put("6", "15");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "-1");
		createIncentiveTwo.put("9", "0");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "By-Super-Atomation");

		// priority (9)
		createIncentiveThree.put("0", "Automated_Incentive");
		createIncentiveThree.put("1", "Automated_Incentive_Description_Test");
		createIncentiveThree.put("2", "Automate-Incentive_Name");
		createIncentiveThree.put("3", "z1d3z4ws42");
		createIncentiveThree.put("4", "PERCENTAGE");
		createIncentiveThree.put("5", "10");
		createIncentiveThree.put("6", "15");
		createIncentiveThree.put("7", "null");
		createIncentiveThree.put("8", "0");
		createIncentiveThree.put("9", "100");
		createIncentiveThree.put("10", "1");
		createIncentiveThree.put("11", "By-Super-Atomation");


		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();
		String incentiveThreePayload=new CreateIncentive( createIncentiveThree).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload },{incentiveThreePayload }};
	}

	@DataProvider(name = "createIncentiveCheckIncentiveEnabledData")
	public static Object[][] createIncentiveCheckIncentiveEnabled() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		// enabled (10)
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "FLAT");
		createIncentive.put("5", "10");
		createIncentive.put("6", "15");
		createIncentive.put("7", "null");
		createIncentive.put("8", "-1");
		createIncentive.put("9", "-1");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");

		// enabled (10)
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "Automated_Incentive_Description_Test");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "PERCENTAGE");
		createIncentiveTwo.put("5", "10");
		createIncentiveTwo.put("6", "15");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "0");
		createIncentiveTwo.put("9", "0");
		createIncentiveTwo.put("10", "0");
		createIncentiveTwo.put("11", "By-Super-Atomation");

		// enabled (10)
		createIncentiveThree.put("0", "Automated_Incentive");
		createIncentiveThree.put("1", "Automated_Incentive_Description_Test");
		createIncentiveThree.put("2", "Automate-Incentive_Name");
		createIncentiveThree.put("3", "z1d3z4ws42");
		createIncentiveThree.put("4", "PERCENTAGE");
		createIncentiveThree.put("5", "10");
		createIncentiveThree.put("6", "15");
		createIncentiveThree.put("7", "null");
		createIncentiveThree.put("8", "0");
		createIncentiveThree.put("9", "100");
		createIncentiveThree.put("10", "0");
		createIncentiveThree.put("11", "By-Super-Atomation");


		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();
		String incentiveThreePayload=new CreateIncentive( createIncentiveThree).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload },{incentiveThreePayload }};
	}

	@DataProvider(name = "createIncentiveCheckIncentiveCreatedByData")
	public static Object[][] createIncentiveCheckIncentiveCreatedBy() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		// created by (11)
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "FLAT");
		createIncentive.put("5", "10");
		createIncentive.put("6", "15");
		createIncentive.put("7", "null");
		createIncentive.put("8", "0");
		createIncentive.put("9", "5");
		createIncentive.put("10", "1");
		createIncentive.put("11", "");

		// created by (11)
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "Automated_Incentive_Description_Test");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "PERCENTAGE");
		createIncentiveTwo.put("5", "10");
		createIncentiveTwo.put("6", "15");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "0");
		createIncentiveTwo.put("9", "1");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "   ");

		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload }};
	}


	@DataProvider(name = "createIncentiveCheckTitleDescriptionNameAsBlankData")
	public static Object[][] createIncentiveCheckTitleDescriptionNameAsBlank() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		//title (0)
		createIncentive.put("0", "  ");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "PERCENTAGE");
		createIncentive.put("5", "10");
		createIncentive.put("6", "15");
		createIncentive.put("7", "null");
		createIncentive.put("8", "0");
		createIncentive.put("9", "10");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");
		//description
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "  ");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "PERCENTAGE");
		createIncentiveTwo.put("5", "10");
		createIncentiveTwo.put("6", "15");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "0");
		createIncentiveTwo.put("9", "10");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "By-Super-Atomation");

		//name
		createIncentiveThree.put("0", "Automated_Incentive");
		createIncentiveThree.put("1", "Automated_Incentive_Description_Test");
		createIncentiveThree.put("2", "   ");
		createIncentiveThree.put("3", "z1d3z4ws42");
		createIncentiveThree.put("4", "PERCENTAGE");
		createIncentiveThree.put("5", "10");
		createIncentiveThree.put("6", "15");
		createIncentiveThree.put("7", "null");
		createIncentiveThree.put("8", "0");
		createIncentiveThree.put("9", "10");
		createIncentiveThree.put("10", "1");
		createIncentiveThree.put("11", "By-Super-Atomation");

		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();
		String incentiveThreePayload=new CreateIncentive( createIncentiveThree).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload },{incentiveThreePayload } };
	}

	@DataProvider(name = "createIncentiveCheckIncentivePercentageGreaterThanHunderedData")
	public static Object[][] createIncentiveCheckIncentivePercentageGreaterThanHundered() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		//value- 101 (5)
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "PERCENTAGE");
		createIncentive.put("5", "101");
		createIncentive.put("6", "101");
		createIncentive.put("7", "null");
		createIncentive.put("8", "0");
		createIncentive.put("9", "10");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");
		//value
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "Automated_Incentive_Description_Test");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "PERCENTAGE");
		createIncentiveTwo.put("5", "110");
		createIncentiveTwo.put("6", "15");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "0");
		createIncentiveTwo.put("9", "10");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "By-Super-Atomation");

		//value
		createIncentiveThree.put("0", "Automated_Incentive");
		createIncentiveThree.put("1", "Automated_Incentive_Description_Test");
		createIncentiveThree.put("2", "Automate-Incentive_Name");
		createIncentiveThree.put("3", "z1d3z4ws42");
		createIncentiveThree.put("4", "PERCENTAGE");
		createIncentiveThree.put("5", "100.02");
		createIncentiveThree.put("6", "150");
		createIncentiveThree.put("7", "null");
		createIncentiveThree.put("8", "0");
		createIncentiveThree.put("9", "10");
		createIncentiveThree.put("10", "1");
		createIncentiveThree.put("11", "By-Super-Atomation");

		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();
		String incentiveThreePayload=new CreateIncentive( createIncentiveThree).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload },{incentiveThreePayload } };
	}

	@DataProvider(name = "createIncentiveCheckIncentivePercentageCappingData")
	public static Object[][] createIncentiveCheckIncentivePercentageCapping() {
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		//cap- -1 (6)
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "Automated_Incentive_Description_Test");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "PERCENTAGE");
		createIncentiveTwo.put("5", "100");
		createIncentiveTwo.put("6", "-1");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "0");
		createIncentiveTwo.put("9", "10");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "By-Super-Atomation");

		//cap  (6)
		createIncentiveThree.put("0", "Automated_Incentive");
		createIncentiveThree.put("1", "Automated_Incentive_Description_Test");
		createIncentiveThree.put("2", "Automate-Incentive_Name");
		createIncentiveThree.put("3", "z1d3z4ws42");
		createIncentiveThree.put("4", "PERCENTAGE");
		createIncentiveThree.put("5", "99");
		createIncentiveThree.put("6", "0");
		createIncentiveThree.put("7", "null");
		createIncentiveThree.put("8", "-1");
		createIncentiveThree.put("9", "10");
		createIncentiveThree.put("10", "1");
		createIncentiveThree.put("11", "By-Super-Atomation");

		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();
		String incentiveThreePayload=new CreateIncentive( createIncentiveThree).toString();

		return new Object[][] { {incentiveTwoPayload },{incentiveThreePayload } };
	}

	@DataProvider(name = "createIncentiveCheckIncentivePercentageCappingAsNullData")
	public static Object[][] createIncentiveCheckIncentivePercentageCappingAsNull() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createTwoIncentive= new HashMap<String, String >();

		//cap- null (6)
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "PERCENTAGE");
		createIncentive.put("5", "99.9999");
		createIncentive.put("6", "null");
		createIncentive.put("7", "null");
		createIncentive.put("8", "0");
		createIncentive.put("9", "10");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");
		//cap- null (6)
		createTwoIncentive.put("0", "Automated_Incentive");
		createTwoIncentive.put("1", "Automated_Incentive_Description_Test");
		createTwoIncentive.put("2", "Automate-Incentive_Name");
		createTwoIncentive.put("3", "z1d3z4ws42");
		createTwoIncentive.put("4", "FLAT");
		createTwoIncentive.put("5", "99.9999");
		createTwoIncentive.put("6", "-2");
		createTwoIncentive.put("7", "null");
		createTwoIncentive.put("8", "0");
		createTwoIncentive.put("9", "10");
		createTwoIncentive.put("10", "1");
		createTwoIncentive.put("11", "By-Super-Atomation");

		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createTwoIncentive).toString();


		return new Object[][] { {incentivePayload },{incentiveTwoPayload } };
	}

	@DataProvider(name = "createIncentiveCheckIncentiveFlatCappingAsNullData")
	public static Object[][] createIncentiveCheckIncentiveFlatCappingAsNull() {
		HashMap<String ,String > createIncentive= new HashMap<String, String >();
		HashMap<String ,String > createTwoIncentive= new HashMap<String, String >();

		//cap- null (6)
		createIncentive.put("0", "Automated_Incentive");
		createIncentive.put("1", "Automated_Incentive_Description_Test");
		createIncentive.put("2", "Automate-Incentive_Name");
		createIncentive.put("3", "z1d3z4ws42");
		createIncentive.put("4", "FLAT");
		createIncentive.put("5", "99.9999");
		createIncentive.put("6", "null");
		createIncentive.put("7", "null");
		createIncentive.put("8", "0");
		createIncentive.put("9", "10");
		createIncentive.put("10", "1");
		createIncentive.put("11", "By-Super-Atomation");

		//cap- null (6)
		createTwoIncentive.put("0", "Automated_Incentive");
		createTwoIncentive.put("1", "Automated_Incentive_Description_Test");
		createTwoIncentive.put("2", "Automate-Incentive_Name");
		createTwoIncentive.put("3", "z1d3z4ws42");
		createTwoIncentive.put("4", "FLAT");
		createTwoIncentive.put("5", "99.9999");
		createTwoIncentive.put("6", "-2");
		createTwoIncentive.put("7", "null");
		createTwoIncentive.put("8", "0");
		createTwoIncentive.put("9", "10");
		createTwoIncentive.put("10", "1");
		createTwoIncentive.put("11", "By-Super-Atomation");

		String incentivePayload=new CreateIncentive( createIncentive).toString();
		String incentiveTwoPayload=new CreateIncentive( createTwoIncentive).toString();

		return new Object[][] { {incentivePayload },{incentiveTwoPayload} };
	}

	@DataProvider(name = "createIncentiveCheckIncentiveFlatCappingData")
	public static Object[][] createIncentiveCheckIncentiveFlatCapping() {
		HashMap<String ,String > createIncentiveTwo= new HashMap<String, String >();
		HashMap<String ,String > createIncentiveThree= new HashMap<String, String >();
		//cap- -1 (6)
		createIncentiveTwo.put("0", "Automated_Incentive");
		createIncentiveTwo.put("1", "Automated_Incentive_Description_Test");
		createIncentiveTwo.put("2", "Automate-Incentive_Name");
		createIncentiveTwo.put("3", "z1d3z4ws42");
		createIncentiveTwo.put("4", "FLAT");
		createIncentiveTwo.put("5", "100");
		createIncentiveTwo.put("6", "-1");
		createIncentiveTwo.put("7", "null");
		createIncentiveTwo.put("8", "0");
		createIncentiveTwo.put("9", "10");
		createIncentiveTwo.put("10", "1");
		createIncentiveTwo.put("11", "By-Super-Atomation");

		//cap  (6)
		createIncentiveThree.put("0", "Automated_Incentive");
		createIncentiveThree.put("1", "Automated_Incentive_Description_Test");
		createIncentiveThree.put("2", "Automate-Incentive_Name");
		createIncentiveThree.put("3", "z1d3z4ws42");
		createIncentiveThree.put("4", "FLAT");
		createIncentiveThree.put("5", "99");
		createIncentiveThree.put("6", "0");
		createIncentiveThree.put("7", "null");
		createIncentiveThree.put("8", "-1");
		createIncentiveThree.put("9", "10");
		createIncentiveThree.put("10", "1");
		createIncentiveThree.put("11", "By-Super-Atomation");

		String incentiveTwoPayload=new CreateIncentive( createIncentiveTwo).toString();
		String incentiveThreePayload=new CreateIncentive( createIncentiveThree).toString();

		return new Object[][] { {incentiveTwoPayload },{incentiveThreePayload } };
	}
}
