package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

import java.util.HashMap;

public class  SuperHelper {
    Initialize gameofthrones = new Initialize();
    
    public Processor updateSuperCart(String tid, String token, String menu_item_id,String quantity,String restId,
    		String planID,String planQuantity){
        GameOfThronesService service = new GameOfThronesService("checkout", "SuperCart2", gameofthrones);
        String superJson=createSubDetails(planID,planQuantity);
       	String[] payloadparams = {menu_item_id,quantity,restId,superJson};	
        HashMap<String, String> requestheaders=createReqHeader(tid, token);
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }
    
    public Processor cartOnlySubscription(String tid, String token,String planID,String planQuantity){
        GameOfThronesService service = new GameOfThronesService("checkout", "cartOnlySubscription", gameofthrones);
        String a=createSubDetails(planID,planQuantity);
        System.out.println("object---------------------"+a);
       	String[] payloadparams = {a};	
        HashMap<String, String> requestheaders=createReqHeader(tid, token);
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }
      
    public Processor superOrderCancel(String tid, String token, String reason, String feeApplicability,
			String cancellation_fee, String cancellation_reason, String orderType,String orderId) {
		HashMap<String, String> requestheaders = createReqHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "SuperOrderCancel", gameofthrones);
		String[] payloadparams = { reason, feeApplicability, cancellation_fee, cancellation_reason,orderType };
		String[] urlParams = { orderId };
		Processor processor = new Processor(service, requestheaders, payloadparams, urlParams);
		return processor;
	}
    
    public HashMap<String, String> createReqHeader(String tid, String token){
            HashMap<String, String> requestheaders = new HashMap<String, String>();
            requestheaders.put("content-type", "application/json");
            requestheaders.put("Tid", tid);
            requestheaders.put("token", token);
            return requestheaders;
        }
     
    	public String createSubDetails(String planID, String planQuantity) {
    		if(planID.equals("")||planID.isEmpty()){
    			return "[]";
    		} else {
    		JSONArray obj = new JSONArray();
    		try {
    				JSONObject list1 = new JSONObject();
    				list1.put("plan_id", Integer.parseInt(planID));
    				list1.put("quantity", Integer.parseInt(planQuantity));
    				obj.put(list1);
    		} catch (JSONException e1) {
    			e1.printStackTrace();
    		}
    		return obj.toString();
    		}
    	}
     
    	 public void validateSubscriptionNudge(String response,String expectedNudgeCTAMessag){
         	String subNudge = JsonPath.read(response, "$.data.subscription_nudge").toString().replace("[", "").replace("]", "").replace("\"", "");
         	Assert.assertNotNull(subNudge,"Subscription Nudge is missing");
         	String nudgeCta = JsonPath.read(response, "$.data.subscription_nudge.subscription_nudge_cta").toString().replace("[", "").replace("]", "").replace("\"", "");
         	//Assert.assertEquals(nudgeCta, expectedNudgeCTAMessag, "Nudge Cta mismatched");
         	String planID = JsonPath.read(response, "$.data.subscription_nudge..cards..planId").toString().replace("[", "").replace("]", "").replace("\"", "");
         	Assert.assertNotNull(planID,"planID is missing");
         }
    	 
    	 public void validateSubscriptionItemNBenifits(String response,String expectedsubItemflag,String expectedPlanID){
          	String subItem = JsonPath.read(response, "$.data.subscription_items").toString().replace("[", "").replace("]", "").replace("\"", "");
          	Assert.assertNotNull(subItem,"Subscription item is missing");
          	String planID = JsonPath.read(response, "$.data.subscription_items..plan_id").toString().replace("[", "").replace("]", "").replace("\"", "");
          	Assert.assertEquals(planID, expectedPlanID, "Plan id is missing");
          	String subItemIsInvalid = JsonPath.read(response, "$.data.subscription_items..is_invalid").toString().replace("[", "").replace("]", "").replace("\"", "");
          	Assert.assertEquals(subItemIsInvalid, expectedsubItemflag, "subscription_items is invalid");
          	}
    	 
    	 public void validateSubscriptionItemOrderRes(String response,String expectedPlanID){
           	String subItem = JsonPath.read(response, "$.data..order_subscriptions").toString().replace("[", "").replace("]", "").replace("\"", "");
           	Assert.assertNotNull(subItem,"Subscription item is missing");
           	String planID = JsonPath.read(response, "$.data..order_subscriptions..plan_id").toString().replace("[", "").replace("]", "").replace("\"", "");
           	Assert.assertEquals(planID, expectedPlanID, "Plan id is missing");
    	 }
    	 
    	 public void validateCODFlag(String cartResponse,String expectedCod){
           	String isCodEnabled = JsonPath.read(cartResponse, "$.data..cod_enabled").toString().replace("[", "").replace("]", "").replace("\"", "");
           	Assert.assertEquals(isCodEnabled, expectedCod, "COD flag is invalid");
           	}
    	 
    	 public void validatePlaceOrderResDiscountBlob(String cartResponse,String ExpecteddisMessageType,String ExpectedtradeDiscountTotal,String expectedRewardList){
            	//String discount_message = JsonPath.read(cartResponse, "$.data..discount_message").toString().replace("[", "").replace("]", "").replace("\"", "");
            	//Assert.assertEquals(getTDRewardList(discount_message), true, "discount_message has not containing Super");
            	//Assert.assertNotNull(discount_message, "discount_message is null for Super user");
            	String discountMessageType = JsonPath.read(cartResponse, "$.data..success_message_type").toString().replace("[", "").replace("]", "").replace("\"", "");
            	Assert.assertEquals(discountMessageType, ExpecteddisMessageType,"success_message_type is not correct");
            	//String tradeDiscountTotal = JsonPath.read(cartResponse, "$.data..trade_discount_total").toString().replace("[", "").replace("]", "").replace("\"", "");
            	//Assert.assertNotEquals(tradeDiscountTotal, ExpectedtradeDiscountTotal," tradeDiscountTotal is 0 for Super user");
            	//String tradeDiscountRewardList = JsonPath.read(cartResponse, "$.data..trade_discount_reward_list").toString().replace("[", "").replace("]", "").replace("\"", "");
            	//Assert.assertEquals(getTDRewardList(tradeDiscountRewardList), expectedRewardList,"tradeDiscountRewardList has no Super benifits");
            	}
    	 
    	 public void validateResFees(String cartResponse,String fee){
         	String thresholdFee = JsonPath.read(cartResponse, "$.data..threshold_fee").toString().replace("[", "").replace("]", "").replace("\"", "");
         	Assert.assertEquals(thresholdFee, fee,"thresholdFee is not 0 for Super user");
         	String distanceFee = JsonPath.read(cartResponse, "$.data..distance_fee").toString().replace("[", "").replace("]", "").replace("\"", "");
         	Assert.assertEquals(distanceFee,fee, "distance_fee is not 0 for Super user");
         	String timeFee = JsonPath.read(cartResponse, "$.data..time_fee").toString().replace("[", "").replace("]", "").replace("\"", "");
         	Assert.assertEquals(timeFee,fee, "time_fee is not 0 for Super user");
         	String specialFee = JsonPath.read(cartResponse, "$.data..special_fee").toString().replace("[", "").replace("]", "").replace("\"", "");
         	Assert.assertEquals(specialFee, fee,"special_fee is not 0 for Super user");
    	 }
    	 
    	 public boolean getTDRewardList(String inputString){
             //if (tradeDiscountRewardList.contains("SUPER")){
             return StringUtils.containsIgnoreCase(inputString, superConstant.CART_TYPE_SUPER);
    	 }
    	 
    	 public void validateCartType(String cartResponse,String expectedCartType){
            	String cartType = JsonPath.read(cartResponse, "$.data..cart_type").toString().replace("[", "").replace("]", "").replace("\"", "");
            	Assert.assertEquals(cartType, expectedCartType, "cart_type is invalid");
            	}
    	 
    	 public void validateOrderDelCharge(String placeOrderResponse,String expectedDelFee){
    		String orderDelFee = JsonPath.read(placeOrderResponse, "$.data..order_data..order_delivery_charge").toString().replace("[", "").replace("]", "").replace("\"", "");
          	Assert.assertEquals(orderDelFee, expectedDelFee, "order Del Fee is not 0");
         	}
    	 
    	 public void validateCartDelCharge(String cartResponse,String expectedDelFee){
     		String orderDelFee = JsonPath.read(cartResponse, "$.data..delivery_charges").toString().replace("[", "").replace("]", "").replace("\"", "");
           	Assert.assertEquals(orderDelFee, expectedDelFee, "Cart del Fee is not 0");
          	}
    	 
    	 public boolean hasCancellationFee(String cartResponse){
            	String cancellationFee = JsonPath.read(cartResponse, "$.data..cancellation_fee").toString().replace("[", "").replace("]", "").replace("\"", "");
             return !cancellationFee.equals("0.0");
         }
    	 
     public String[] getSubscriptionID(String cartResponse){
    	 
    	 String planID = JsonPath.read(cartResponse, "$.data.subscription_nudge..cards..planId").toString().replace("[", "").replace("]", "").replace("\"", "");
    	 return planID.split(",");
     }
     
     public void sanityResponse(String response,String statusCode,String statusMessage) {
    	    String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
    		Assert.assertEquals(getStatusCode,statusCode);
    		String getMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
    		Assert.assertEquals(getMessage,statusMessage);
    }
     public String getOrderId(String Orderresponse) {
     return JsonPath.read(Orderresponse, "$.data..order_id").toString().replace("[", "").replace("]", ""); 
     }
     
     public String callCreateCartOnlySuper(String tid,String token,String planID){
         String cartResOnlySuper=cartOnlySubscription(tid,token,planID,superConstant.SUB_QUANTITY).ResponseValidator.GetBodyAsText();
         return cartResOnlySuper;
     }
     
     public String getPlanIDfromCartResponse(String tid,String token,String menu_item_id,String quantity, String restId){
     	String cartResNonSuper=updateSuperCart(tid,token,menu_item_id,quantity,restId,"","").ResponseValidator.GetBodyAsText();
         String planId=getSubscriptionID(cartResNonSuper)[superConstant.INT_ZERO];
         return planId;
     }
     
     public void validateFoodNSuperPlaceOrder(String placeOrderRes,String planId){
     	validateSubscriptionItemOrderRes(placeOrderRes,planId);
     	validateOrderDelCharge(placeOrderRes,superConstant.ZERO_AS_STRING);
        validatePlaceOrderResDiscountBlob(placeOrderRes, superConstant.CART_TYPE_SUPER, superConstant.ZERO_AS_STRING, superConstant.DISCOUNT_FREE_DELIVERY);
        validateResFees(placeOrderRes,superConstant.ZERO_AS_STRING);
        validateOrderTag(placeOrderRes, superConstant.CART_TYPE_SUPER);
        freeDelBreakUp(placeOrderRes);
        validateOrderTotal(placeOrderRes);
     }
     
     public void validateFoodNSuperCartRes(String superCartRes,String planId,String fee){
     	validateSubscriptionItemNBenifits(superCartRes,superConstant.EXPECTED_FALSE_FLAG,planId);
        validateCartType(superCartRes, superConstant.CART_TYPE_REGULAR);
         if (hasCancellationFee(superCartRes)){
      	   validateCODFlag(superCartRes, superConstant.EXPECTED_FALSE_FLAG);
         } else {
      	   validateCODFlag(superCartRes, superConstant.EXPECTED_TRUE_FLAG);
         }
         validateCartDelCharge(superCartRes, fee);
         validateResFees(superCartRes, fee);
         validateOrderTotalInCart(superCartRes);
         validateTradeDiscountRewardList(superCartRes);
     }
     
     public String createCart(String tid,String token,String menu_item_id,String quantity, String restId,String planId,String Planquantity){
     	return updateSuperCart(tid,token,menu_item_id,quantity,restId,planId,Planquantity)
       		   .ResponseValidator.GetBodyAsText();
     }
     
     public String callCreateCart(String tid,String token,String menu_item_id,String quantity, String restId,String planId,String Planquantity){
     	String cartRes=createCart(tid,token,menu_item_id,quantity,restId,planId,Planquantity);
     	if (!planId.equals("")){
     	return createCart(tid,token,menu_item_id,quantity,restId,planId,Planquantity);
     	} else {
     		return cartRes;
     	}
     }
     
     public void validateCartResOnlySubscription(String response,String expectedPlanID,String expectedcodEnabled){
    	 String subItem = JsonPath.read(response, "$.data..cart.subscription_items").toString().replace("[", "").replace("]", "").replace("\"", "");
    	 Assert.assertNotNull(subItem,"subscription_items is null");
    	 String planId = JsonPath.read(response, "$.data..cart.subscription_items..plan_id").toString().replace("[", "").replace("]", "").replace("\"", "");
       	Assert.assertEquals(planId, expectedPlanID);
       	int index=getIndexOfCOD(response,"cod");
       	String codEnabled = JsonPath.read(response, "$.data..payments.payment_group.["+index+"].payment_methods..enabled").toString().replace("[", "").replace("]", "").replace("\"", "");
       	Assert.assertEquals(codEnabled, expectedcodEnabled);
     }
     
     public int getIndexOfCOD(String response,String searchString){
    	 String[] groupName = JsonPath.read(response, "$.data..payments.payment_group..group_name").toString().replace("[", "").replace("]", "").replace("\"", "").split(",");
        int index = 0;	
    	 for (int i=0;i<=groupName.length;i++){
        		if (groupName[i].equalsIgnoreCase(searchString)){
        			index=i;
        			break;
        		}
        	}
			return index;
			
     }
     
     public void validateOrderTag(String cartResponse,String ExpectedorderTag){
      	String orderTag = JsonPath.read(cartResponse, "$.data..order_tags").toString().replace("[", "").replace("]", "").replace("\"", "");
      	Assert.assertEquals(orderTag, ExpectedorderTag,"order_tags is not Super");
      	}
     
     public void validateOrderTotalInCart(String cartResponse){
       	String cartSubTotal = JsonPath.read(cartResponse, "$.data..cart_subtotal_without_packing").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String resPacCharge = JsonPath.read(cartResponse, "$.data..restaurant_packing_charges").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String subPrice = JsonPath.read(cartResponse, "$.data..subscription_items..total").toString().replace("[", "").replace("]", "").replace("\"", "");
       	if(subPrice.equals("")){
       		subPrice =superConstant.ZERO_AS_STRING;
       	}
       	float cartTotal=Float.parseFloat(cartSubTotal)+Float.parseFloat(resPacCharge)+ (Float.parseFloat(subPrice)/100);
       	String orderTotal = JsonPath.read(cartResponse, "$.data..order_total").toString().replace("[", "").replace("]", "").replace("\"", "");
       	Assert.assertEquals(Float.parseFloat(orderTotal), cartTotal,"mismatch in Cart order total");
       	}
     
     public void validateTradeDiscountRewardList(String cartResponse){
     	String tradeDiscountRewardList = JsonPath.read(cartResponse, "$.data..trade_discount_reward_list").toString().replace("[", "").replace("]", "").replace("\"", "");
     	Assert.assertNotNull(tradeDiscountRewardList,"tradeDiscountRewardList has no Super benifits");
     	}
     
     public void freeDelBreakUp(String cartResponse){
       	String thresholdFee = JsonPath.read(cartResponse, "$.data..free_del_break_up..thresholdFee").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String distanceFee = JsonPath.read(cartResponse, "$.data..free_del_break_up..distanceFee").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String timeFee = JsonPath.read(cartResponse, "$.data..free_del_break_up..timeFee").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String specialFee = JsonPath.read(cartResponse, "$.data..free_del_break_up..specialFee").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String tradeDis=JsonPath.read(cartResponse, "$.data..trade_discount").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String couDis=JsonPath.read(cartResponse, "$.data..coupon_discount").toString().replace("[", "").replace("]", "").replace("\"", ""); 
       	String superSaving = JsonPath.read(cartResponse, "$.data..savings_shown_to_customer").toString().replace("[", "").replace("]", "").replace("\"", "");
       	float sumOfFreeDel=Float.parseFloat(thresholdFee)+
       			Float.parseFloat(distanceFee)+Float.parseFloat(timeFee)+
       			Float.parseFloat(specialFee)+
       			Float.parseFloat(tradeDis)+
       			Float.parseFloat(couDis);
       	Assert.assertEquals(sumOfFreeDel, Float.parseFloat(superSaving),"mismatch in Super discount total");
       	}
     
     public void validateOrderTotal(String orderResponse){
    	String itemTotal = JsonPath.read(orderResponse, "$.data..item_total").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String orderTotal = JsonPath.read(orderResponse, "$.data..order_total").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String subTotal = JsonPath.read(orderResponse, "$.data..subscription_total").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String tradeDis=JsonPath.read(orderResponse, "$.data..trade_discount").toString().replace("[", "").replace("]", "").replace("\"", "");
       	String couDis=JsonPath.read(orderResponse, "$.data..coupon_discount").toString().replace("[", "").replace("]", "").replace("\"", ""); 
       	float actualOrderTotal=(Float.parseFloat(itemTotal)+Float.parseFloat(subTotal))-
       			(Float.parseFloat(tradeDis)+Float.parseFloat(couDis));
       	Assert.assertEquals(actualOrderTotal, Float.parseFloat(orderTotal),"Mismatch in actualOrderTotal");
       	}
     
     public void validateCancelOrderStatus(String cancelOrderRes,String ExpectedOrderStatus){
     	String orderStatus = JsonPath.read(cancelOrderRes, "$.data..order_status").toString().replace("[", "").replace("]", "").replace("\"", "");
        Assert.assertEquals(orderStatus, ExpectedOrderStatus,"Mismatch in orderStatus");
        	}
}