package com.swiggy.api.sf.checkout.helper.edvo.pojo.getPreorderSlot;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "statusCode",
        "statusMessage",
        "data",
        "tid",
        "sid",
        "deviceId"
})
public class GetPreorderSlot {

    @JsonProperty("statusCode")
    private Integer statusCode;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("data")
    private List<Data> data = null;
    @JsonProperty("tid")
    private String tid;
    @JsonProperty("sid")
    private String sid;
    @JsonProperty("deviceId")
    private String deviceId;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetPreorderSlot() {
    }

    /**
     *
     * @param statusCode
     * @param sid
     * @param data
     * @param tid
     * @param deviceId
     * @param statusMessage
     */
    public GetPreorderSlot(Integer statusCode, String statusMessage, List<Data> data, String tid, String sid, String deviceId) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.data = data;
        this.tid = tid;
        this.sid = sid;
        this.deviceId = deviceId;
    }

    @JsonProperty("statusCode")
    public Integer getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("data")
    public List<Data> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Data> data) {
        this.data = data;
    }

    @JsonProperty("tid")
    public String getTid() {
        return tid;
    }

    @JsonProperty("tid")
    public void setTid(String tid) {
        this.tid = tid;
    }

    @JsonProperty("sid")
    public String getSid() {
        return sid;
    }

    @JsonProperty("sid")
    public void setSid(String sid) {
        this.sid = sid;
    }

    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}