package com.swiggy.api.sf.rng.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.constants.MultiTDConstants;
import com.swiggy.api.sf.rng.dp.CarouselBannerDP;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.*;

public class CarouselCRUDTests extends CarouselBannerDP {

    String[] latLng =  {"12.9326","77.6036"};
    String[] hauzkhas = {"11.1271","78.6569"};
    SANDHelper sandHelper = new SANDHelper();
    RngHelper rngHelper = new RngHelper();
    String versionCode="400";
    List<String> restIds;

    @BeforeClass
    public void getServiceableAndOpenRestaurant(){
        restIds = sandHelper.aggregatorRestList(latLng);
        Assert.assertFalse(restIds.isEmpty(),"Assertion Failed :: No serviceable and open rest found.");
        Assert.assertTrue(rngHelper.createSuperUser().length!=0,"Super user creation and subscription failed");
    }

    @Test(dataProvider = "carouselWidgets")
    public void testCarouselWidgetsWithRestaurantAsBannerType(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        try {
            String restID = null, response;

            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            carouselPOJO.setRedirectLink(restID+","+(restID+1)+","+(restID+2));
            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");
            response = sandHelper.aggregator(latLng).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            if(carouselID!=0) {
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    


    @Test(dataProvider = "carouselWidgets")
    public void testCarouselWidgetsWithCollectionAsBannerType(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        try {
        String response, collectionID =   rngHelper.createCollection(latLng[0],latLng[1],"ALL",restIds);

        carouselPOJO.setRedirectLink(collectionID);
        carouselPOJO.setType("collection");

        response = rngHelper.createCarousel(carouselPOJO);
        Assert.assertEquals((int)JsonPath.read(response,"statusCode"),0,"Assertion Failed :: carousel creation failed");
        Assert.assertTrue(((String)JsonPath.read(response,"statusMessage")).toLowerCase().contains("banner created successfully"),"Assertion Failed :: carousel creation failed");
        carouselID = JsonPath.read(response,"$.data");
        response  = sandHelper.aggregator(latLng).ResponseValidator.GetBodyAsText();
        List<Integer> carouselsID = JsonPath.read(response,"$.data.carousels..bannerId");
        Assert.assertTrue(carouselsID.contains(carouselID),"Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            if(carouselID!=0) {
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel", description = "test carousels widget with restaurant as banner type and diferent channel type")
    public void testCarouselWidgetsWithRestAndChannel(CarouselPOJO carouselPOJO) throws IOException{
        int carouselID = 0;
        try {
            String restID = null, response;
            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            carouselPOJO.setRedirectLink(restID);
            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");
            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            if(carouselID!=0) {
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }

    }

    @Test(dataProvider = "carouselWidgetsWithChannel", description = "test carousels widget with collection as banner type and diferent channel type")
    public void testCarouselWidgetsWithCollectionAndChannel(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        try {
            String response, collectionID =   rngHelper.createCollection(latLng[0],latLng[1],"ALL",restIds);

            carouselPOJO.setRedirectLink(collectionID);
            carouselPOJO.setType("collection");

            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int)JsonPath.read(response,"statusCode"),0,"Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String)JsonPath.read(response,"statusMessage")).toLowerCase().contains("banner created successfully"),"Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response,"$.data");
            response  = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response,"$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID),"Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            if(carouselID!=0) {
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

     @Test(dataProvider = "carouselWidgetsWithChannel", description = "test carousels widget with restaurant as banner type and diferent channel type")
    public void testCarouselWidgetsWithRestAndChannelNegative(CarouselPOJO carouselPOJO) throws IOException{
        int carouselID = 0;
        try {
            String restID = null, response;
            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            carouselPOJO.setRedirectLink(restID);
            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");
            response = sandHelper.aggregator(latLng).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            if(carouselPOJO.getChannel().equalsIgnoreCase("web")) {
                Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel should be visible in aggregator response.");
             }else {
                Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel should't be visible in aggregator response.");
            }
        }finally {
            if(carouselID!=0) {
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }

    }

    @Test(dataProvider = "carouselWidgetsWithChannel", description = "test carousels widget with collection as banner type and diferent channel type")
    public void testCarouselWidgetsWithCollectionAndChannelNegative(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        try {
            String response, collectionID =   rngHelper.createCollection(latLng[0],latLng[1],"ALL",restIds);

            carouselPOJO.setRedirectLink(collectionID);
            carouselPOJO.setType("collection");

            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int)JsonPath.read(response,"statusCode"),0,"Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String)JsonPath.read(response,"statusMessage")).toLowerCase().contains("banner created successfully"),"Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response,"$.data");
            response  = sandHelper.aggregator(latLng).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response,"$.data.carousels..bannerId");
            if(carouselPOJO.getChannel().equalsIgnoreCase("web")){
                Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel should be visible in aggregator response.");
            }else {
                Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel shouldn't be visible in aggregator response.");
            }
        }finally {
            if(carouselID!=0) {
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithPercentageTradeDiscount(CarouselPOJO carouselPOJO) throws IOException, JSONException {
        int carouselID = 0;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            for (int i = 0, restIdsSize = restIds.size(); i < restIdsSize && i<5; i++) {
                String rest = restIds.get(i);
                response = rngHelper.createPercentageTD(rest);
                if (((int) JsonPath.read(response, "$.statusCode")) == 0) {
                    tdID.add(JsonPath.read(response, "$.data"));
                    openRest.append(rest + " ");
                }
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Percentage");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
                for (int td: tdID) {
                    rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
                }
                if(carouselID!=0) {
                    carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                    rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
                 }
        }
    }

     @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithPercentageTradeDiscountNegative(CarouselPOJO carouselPOJO) throws IOException, JSONException {
        int carouselID = 0;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            for (int i = 0, restIdsSize = restIds.size(); i < restIdsSize && i<5; i++) {
                String rest = restIds.get(i);
                response = rngHelper.createPercentageTD(rest);
                if (((int) JsonPath.read(response, "$.statusCode")) == 0) {
                    tdID.add(JsonPath.read(response, "$.data"));
                    openRest.append(rest + " ");
                }
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Percentage");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            if(carouselPOJO.getChannel().equalsIgnoreCase("web")) {
                Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible for channel type as web in aggregator response.");
            } else {
                Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel shouldn't be visible in aggregator response for all channel.");
            }
            response = sandHelper.aggregator(hauzkhas, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID),"Assertion Failed :: carousel shouldn't be visible in aggregator response for different city.");

        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithFlatTradeDiscount(CarouselPOJO carouselPOJO) throws IOException, JSONException {
        int carouselID = 0;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            for (int i = 0, restIdsSize = restIds.size(); i < restIdsSize && i<5; i++) {
                String rest = restIds.get(i);
                HashMap tdresponse = rngHelper.createFlatWithNoMinAmountAtRestaurantLevel(rest);
                if ((null != tdresponse.get("TID"))) {
                    tdID.add(Integer.valueOf((String) tdresponse.get("TID")));
                    openRest.append(rest + " ");
                }
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Flat");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }


    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithFlatTradeDiscountNegative(CarouselPOJO carouselPOJO) throws IOException, JSONException {
        int carouselID = 0;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            for (int i = 0, restIdsSize = restIds.size(); i < restIdsSize && i<5; i++) {
                String rest = restIds.get(i);
                HashMap tdresponse = rngHelper.createFlatWithNoMinAmountAtRestaurantLevel(rest);
                if ((null != tdresponse.get("TID"))) {
                    tdID.add(Integer.valueOf((String) tdresponse.get("TID")));
                    openRest.append(rest + " ");
                }
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Flat");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            if(carouselPOJO.getChannel().equalsIgnoreCase("web")){
                Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel should be visible in aggregator response for all channel.");
            }else {
                Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel shouldn't be visible in aggregator response for all channel.");
            }
            response = sandHelper.aggregator(hauzkhas, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID),"Assertion Failed :: carousel shouldn't be visible in aggregator response for different city.");

        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

     @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithFreebieTradeDiscount(CarouselPOJO carouselPOJO) throws IOException, JSONException {
        int carouselID = 0;
        String minCartAmount="100",freebieItemID;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            for (int i = 0, restIdsSize = restIds.size(); i < restIdsSize && i<5; i++) {
                String rest = restIds.get(i);
                freebieItemID = rngHelper.getMenuItemWithoutVariantWithMinAmt(Collections.singletonList(rest),0).get("menuItemID");
                response = rngHelper.createFreebieTDWithMinAmountAtRestaurantLevel(minCartAmount, rest, freebieItemID).ResponseValidator.GetBodyAsText();
                if (((int) JsonPath.read(response, "$.statusCode")) == 0) {
                    tdID.add(JsonPath.read(response, "$.data"));
                    openRest.append(rest + " ");
                }

            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Freebie");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, new String[]{carouselPOJO.getChannel(),"400"}).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithFreeDeliveryTradeDiscount(CarouselPOJO carouselPOJO) throws IOException, JSONException {
        int carouselID = 0;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            for (int i = 0, restIdsSize = restIds.size(); i < restIdsSize && i<5; i++) {
                String rest = restIds.get(i);
                response = rngHelper.createFreeDeliveryTD(rest, false, false, false, false);
                if (((int) JsonPath.read(response, "$.statusCode")) == 0) {
                    tdID.add(JsonPath.read(response, "$.data"));
                    openRest.append(rest + " ");
                }
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("FREE_DELIVERY");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithExpiredTimeSlot(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            for (int i = 0, restIdsSize = restIds.size(); i < restIdsSize && i<5; i++) {
                String rest = restIds.get(i);
                openRest.append(rest + " ");
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.getAllDaysTimeSlot().remove(rngHelper.getTodaysDay()-1);
            carouselPOJO.setRedirectLink(restID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel shouldn't be visible in aggregator response.");
        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

     @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetWithOneRestOneCampaign(CarouselPOJO carouselPOJO) throws IOException {

        int carouselID = 0, i=5;
        List<String> tdID = new ArrayList<String>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            String rest = restIds.get(rngHelper.getRandomNo(restIds.size()));

            HashMap tdresponse = rngHelper.createFlatWithNoMinAmountAtRestaurantLevel(rest);
            if((null != tdresponse.get("TID"))) {
                tdID.add((String)tdresponse.get("TID"));
                openRest.append(rest+" ");
            }

            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetWithTwoRestOneCampaign(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        List<String> tdID = new ArrayList();
        StringBuilder openRest = new StringBuilder();
        Assert.assertTrue(restIds.size()>=2,"Assertion Failed :: Atleast two open and serviceable rest are required ");
        try {
            String restID, response;

            Stack<String> rest = new Stack<>();
            while(rest.size()!=2){
                rest.push(restIds.get(rngHelper.getRandomNo(restIds.size())));
                openRest.append(rest.peek()+" ");

            }
            String[] restaurants = new String[2];
            response = rngHelper.createPercentageTD(true, rest.toArray(restaurants));
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
                 tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

//    Postive
//    one campaign one rest
//    one campaign two rest
//    two campaign one rest
    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetWithOneRestTwoCampaign(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        List<String> tdID = new ArrayList();
        try {
            String restID, response;

            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
    //        I campaign
            response = rngHelper.createPercentageTD(restID);
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));
    //      II campaign
            response = rngHelper.createFreeDeliveryTD(false, false, false, false, false, restID);
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, new String[]{carouselPOJO.getChannel(),"400"}).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

//    two campaign two rest

@Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetWithTwoRestTwoCampaign(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        List<String> tdID = new ArrayList();
        StringBuilder openRest = new StringBuilder();
        Assert.assertTrue(restIds.size()>=2,"Assertion Failed :: Atleast two open and serviceable rest are required ");
        try {
            String restID, response;

            Stack<String> rest = new Stack<>();
            while(rest.size()!=2){
                rest.push(restIds.get(rngHelper.getRandomNo(restIds.size())));
                openRest.append(rest.peek()+" ");

            }
            String[] restaurants = new String[2];
            response = rngHelper.createPercentageTD(true, rest.toArray(restaurants));
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            response = rngHelper.createFreeDeliveryTD(false, false, false, false, false, restaurants);
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }



//    Negative
//    one campaign one rest: rest not running campaign, campaign expired or disabled

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetWithOneRestOneCampaignNegativeDisabledTD(CarouselPOJO carouselPOJO) throws IOException {

        int carouselID = 0;
        List<String> tdID = new ArrayList<String>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            String rest = restIds.get(rngHelper.getRandomNo(restIds.size()));

            HashMap tdresponse = rngHelper.createFlatWithNoMinAmountAtRestaurantLevel(rest);
            if((null != tdresponse.get("TID"))) {
                tdID.add((String)tdresponse.get("TID"));
                openRest.append(rest+" ");
            }

            rngHelper.disableCampaign(tdID.toArray(new String[1]));

            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
//            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
//            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetWithOneRestOneCampaignNegativeInvalidTD(CarouselPOJO carouselPOJO) throws IOException {

        int carouselID = 0;
        List<String> tdID = new ArrayList<String>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            String rest = restIds.get(rngHelper.getRandomNo(restIds.size()));

//            HashMap tdresponse = rngHelper.createFlatWithNoMinAmountAtRestaurantLevel(rest);
//            if((null != tdresponse.get("TID"))) {
//                tdID.add((String)tdresponse.get("TID"));
                openRest.append(rest+" ");
//            }

//            rngHelper.disableCampaign(tdID.toArray(new String[1]));
            tdID.add("33339999");
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
//            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
//            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }
//    one campaign two rest: one rest not running the campaign

    @Test(dataProvider = "carouselWidgetsWithChannel", description = "one rest having td disabled")
    public void testCarouselWidgetWithTwoRestOneCampaignNegative(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        List<String> tdID = new ArrayList();
        StringBuilder openRest = new StringBuilder();
        Assert.assertTrue(restIds.size()>=2,"Assertion Failed :: Atleast two open and serviceable rest are required ");
        try {
            String restID, response;

            Stack<String> rest = new Stack<>();
            while(rest.size()!=2){
                rest.push(restIds.get(rngHelper.getRandomNo(restIds.size())));
                openRest.append(rest.peek()+" ");

            }
            String[] restaurants = new String[2];
            response = rngHelper.createPercentageTD(true, rest.toArray(restaurants));
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            rngHelper.disableCampaign(tdID.get(0));

            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

//    two campaign one rest : one campaign disabled

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetWithOneRestTwoCampaignNegative(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        List<String> tdID = new ArrayList();
        try {
            String restID, response;

            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            //        I campaign
            response = rngHelper.createPercentageTD(restID);
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));
            //      II campaign
            response = rngHelper.createFreeDeliveryTD(false, false, false, false, false, restID);
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            rngHelper.disableCampaign(tdID.get(0));

            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

//    two campaign two rest : one rest not running campaign

     @Test(dataProvider = "carouselWidgetsWithChannel", description = "td on one rest is disabled")
    public void testCarouselWidgetWithTwoRestTwoCampaignNegative(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        List<String> tdID = new ArrayList();
        StringBuilder openRest = new StringBuilder();
        Assert.assertTrue(restIds.size()>=2,"Assertion Failed :: Atleast two open and serviceable rest are required ");
        try {
            String restID, response;

            Stack<String> rest = new Stack<>();
            while(rest.size()!=2){
                rest.push(restIds.get(rngHelper.getRandomNo(restIds.size())));
                openRest.append(rest.peek()+" ");

            }
            String[] restaurants = new String[2];
            response = rngHelper.createPercentageTD(true, rest.toArray(restaurants));
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            response = rngHelper.createFreeDeliveryTD(false, false, false, false, false, restaurants);
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> sortedRest = JsonPath.read(response, "$.data.sortedRestaurants..restaurantId");

            rngHelper.updateTDAtRestaurantLevel("20","0","2000",Integer.valueOf(tdID.get(0)),restaurants[1]);//will keep TD on restaurant passed only
            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselIDs = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselIDs.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(linkId.contains(restaurants[1]));
            Assert.assertFalse(linkId.contains(restaurants[0]));

            rngHelper.updateTDAtRestaurantLevel("20","0","2000",Integer.valueOf(tdID.get(0)),restaurants[0]);//will keep TD on restaurant passed only
            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            carouselIDs = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselIDs.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(linkId.contains(restaurants[0]));
            Assert.assertFalse(linkId.contains(restaurants[1]));

        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

@Test(dataProvider = "carouselWidgetsWithChannel", description = "td on two rest two campaign, in which one rest is not added to one campaign")
    public void testCarouselWidgetWithTwoRestTwoCampaignNegativeOneRestInvalid(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        List<String> tdID = new ArrayList();
        StringBuilder openRest = new StringBuilder();
        String includedRest, excludedRest;
        Assert.assertTrue(restIds.size()>=2,"Assertion Failed :: Atleast two open and serviceable rest are required ");
        try {
            String restID, response;

            Stack<String> rest = new Stack<>();
            while(rest.size()!=2){
                rest.push(restIds.get(rngHelper.getRandomNo(restIds.size())));
                openRest.append(rest.peek()+" ");

            }
            String[] restaurants = new String[2];
            response = rngHelper.createPercentageTD(true, rest.toArray(restaurants));
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> sortedRest = JsonPath.read(response, "$.data.sortedRestaurants..restaurantId");
            if(sortedRest.indexOf(restaurants[0])<sortedRest.indexOf(restaurants[1])) {//carousel will have restaurant[0]
//            adding only one rest
                response = rngHelper.createFreeDeliveryTD(false, false, false, false, false, restaurants[1]);
                includedRest = restaurants[1];
                excludedRest = restaurants[0];
            } else{
                response = rngHelper.createFreeDeliveryTD(false, false, false, false, false, restaurants[0]);
                includedRest =  restaurants[0];
                excludedRest = restaurants[1];
            }
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));


            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Campaign",tdID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselIDs = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselIDs.contains(carouselID), "Assertion Failed :: carousel should be visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(linkId.contains(includedRest));
            Assert.assertFalse(linkId.contains(excludedRest));
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

// super user : "SUPER"
// expired super user : WAS_SUPER
// user eligible for super "NOT_SUPER"
//    super type : NULL for all

//    time slot greater than validity

//    unserviceable rest
//    login1

    @Test(dataProvider = "carouselWidgetsWithChannel", description = "td on two rest two campaign, in which one rest is not added to one campaign")
    public void testCarouselWidgetForSuperUser(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        try {
            String restID = null, response;
            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            carouselPOJO.setSuperType("SUPER");
            carouselPOJO.setRedirectLink(restID);

            HashMap<String, String> loginData =  sandHelper.loginData(MultiTDConstants.mobile[1],"swiggy");

            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel(), loginData.get("Tid"), loginData.get("Token")).ResponseValidator.GetBodyAsText();

            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetForSuperUserWithTD(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        String restID, response;
        List<String> tdID = new ArrayList();
        try {
            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            response = rngHelper.createPercentageTD(true,restID);
            Assert.assertTrue(((int)JsonPath.read(response,"$.statusCode"))==0);
            tdID.add(String.valueOf((Integer) JsonPath.read(response, "$.data")));

            carouselPOJO.withCarouselTradeDiscountInfoSet("Percentage");
            carouselPOJO.setSuperType("SUPER");
            carouselPOJO.setRedirectLink(restID);

            HashMap<String, String> loginData =  sandHelper.loginData(MultiTDConstants.mobile[1],"swiggy");

            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel(), loginData.get("Tid"), loginData.get("Token")).ResponseValidator.GetBodyAsText();

            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            for (String td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(td);
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetForSuperUserWithTDNegative(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        try {
            String restID, response;

            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            rngHelper.disabledActiveTD(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Percentage");
            carouselPOJO.setSuperType("SUPER");
            carouselPOJO.setRedirectLink(restID);

            HashMap<String, String> loginData =  sandHelper.loginData(MultiTDConstants.mobile[1],"swiggy");

            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");


            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel(), loginData.get("Tid"), loginData.get("Token")).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            try {
                Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel is visible in aggregator response.");
            }catch (AssertionError e){
                System.out.println("latLng :: "+latLng+"\n channel :: "+ carouselPOJO.getChannel()+"\n tid :: "+ loginData.get("Tid")+"\n token :: "+ loginData.get("Token"));
                Assert.fail(e.getMessage());
            }

        }finally {
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetForSuperUserNegative(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        try {
            String restID = null, response;
            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            carouselPOJO.setSuperType("SUPER");
            carouselPOJO.setRedirectLink(restID);

            HashMap<String, String> loginData =  sandHelper.loginData(MultiTDConstants.mobile[4],"swiggy");

            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel(), loginData.get("Tid"), loginData.get("Token")).ResponseValidator.GetBodyAsText();

            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel", description = "td on two rest two campaign, in which one rest is not added to one campaign")
    public void testCarouselWidgetForNonSuperUser(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        try {
            String restID = null, response;
            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            carouselPOJO.setSuperType("NOT_SUPER");
            carouselPOJO.setRedirectLink(restID);

            HashMap<String, String> loginData =  sandHelper.loginData(MultiTDConstants.mobile[4],"swiggy");

            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel(), loginData.get("Tid"), loginData.get("Token")).ResponseValidator.GetBodyAsText();

            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetForNonSuperUserNegative(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        try {
            String restID = null, response;
            restID = restIds.get(rngHelper.getRandomNo(restIds.size()));
            carouselPOJO.setSuperType("NOT_SUPER");
            carouselPOJO.setRedirectLink(restID);

            HashMap<String, String> loginData =  sandHelper.loginData(MultiTDConstants.mobile[4],"swiggy");

            response = rngHelper.createCarousel(carouselPOJO);
            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();

            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
        }finally {
            if(carouselID!=0) {

                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }


//        expired td
    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithExpiredTradeDiscount(CarouselPOJO carouselPOJO) throws IOException, JSONException {
        int carouselID = 0;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

                String rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
                response = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel("99","20",rest, false, true).ResponseValidator.GetBodyAsText();
                if (((int) JsonPath.read(response, "$.statusCode")) == 0) {
                    tdID.add(JsonPath.read(response, "$.data"));
                    openRest.append(rest + " ");
                }

            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Flat");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");

            rngHelper.updateExpiredTimeSlotFirstOrderRestrictionAtRestaurantLevel("99","20",restID,Integer.valueOf(tdID.get(0)));

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousele visible in aggregator response.");


        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

//        carousel validity expired
//        update carousel timeslot then validate

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithUpdatedExpiredTimeSlot(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            for (int i = 0, restIdsSize = restIds.size(); i < restIdsSize && i<5; i++) {
                String rest = restIds.get(i);
                openRest.append(rest + " ");
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel shouldn't be visible in aggregator response.");

            carouselPOJO.getAllDaysTimeSlot().remove(rngHelper.getTodaysDay()-1);
//          this helper wont disable the carousel unless passed with false
            rngHelper.disableCarousel(carouselPOJO.withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: carousel shouldn't be visible in aggregator response.");
        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

//        category and subcategory td

    @Test(dataProvider = "carouselWidgetsWithChannel")
        public void testCarouselWithCategoryTD(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        Processor processor;
        List<Integer> tdID = new ArrayList<>(), menuItemID;
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            while(tdID.size()!=5){
                String rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
//                restIds.remove(restIds.indexOf(rest));
                processor = sandHelper.menuV4RestIdWithChannel(rest,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                HashMap<String, String> itemInfo = rngHelper.getItemCategoryAndSubCategory(rest,response);
                if(null!=itemInfo) {
                    response = rngHelper.createFlatCategoryMenu(rest, itemInfo.get("Cat"), true);
                    tdID.add(Integer.valueOf(response));
                    openRest.append(rest + " ");
                }
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Flat");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restID.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWithSubCategoryTD(CarouselPOJO carouselPOJO) throws IOException {
        int carouselID = 0;
        Processor processor;
        List<Integer> tdID = new ArrayList<>(), menuItemID;
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            while(tdID.size()!=5){
                String rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
//                restIds.remove(restIds.indexOf(rest));
                processor = sandHelper.menuV4RestIdWithChannel(rest,null,null,versionCode,carouselPOJO.getChannel());
                response = processor.ResponseValidator.GetBodyAsText();
                HashMap<String, String> itemInfo = rngHelper.getItemCategoryAndSubCategory(rest,response);
                if(null!=itemInfo) {
                    response = rngHelper.createFlatSubcatagoryMenu(rest,itemInfo.get("Cat"),itemInfo.get("Sub"),itemInfo.get("Item"));
                    tdID.add(Integer.valueOf(response));
                    openRest.append(rest + " ");
                }
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Flat");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel()).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertTrue(carouselsID.contains(carouselID), "Assertion Failed :: carousel not visible in aggregator response.");
            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
            Assert.assertTrue(restID.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

    @Test(dataProvider = "carouselWidgetsWithChannel")
    public void testCarouselWidgetsWithFreeDeliveryTDForSuperUser(CarouselPOJO carouselPOJO) throws IOException, JSONException {
        int carouselID = 0;
        List<Integer> tdID = new ArrayList<>();
        StringBuilder openRest = new StringBuilder();
        try {
            String restID, response;

            for (int i = 0, restIdsSize = restIds.size(); i < restIdsSize && i<5; i++) {
                String rest = restIds.get(i);
                response = rngHelper.createFreeDeliveryTD(rest, false, false, false, false);
                if (((int) JsonPath.read(response, "$.statusCode")) == 0) {
                    tdID.add(JsonPath.read(response, "$.data"));
                    openRest.append(rest + " ");
                }
            }
            restID = openRest.toString().trim().replace(" ",",");
            carouselPOJO.setRedirectLink(restID);
            carouselPOJO.withCarouselTradeDiscountInfoSet("Flat");
            response = rngHelper.createCarousel(carouselPOJO);

            Assert.assertEquals((int) JsonPath.read(response, "statusCode"), 0, "Assertion Failed :: carousel creation failed");
            Assert.assertTrue(((String) JsonPath.read(response, "statusMessage")).toLowerCase().contains("banner created successfully"), "Assertion Failed :: carousel creation failed");
            carouselID = JsonPath.read(response, "$.data");

            HashMap<String, String> loginData =  sandHelper.loginData(MultiTDConstants.mobile[1],"swiggy");
            response = sandHelper.aggregator(latLng, carouselPOJO.getChannel(), loginData.get("Tid"), loginData.get("Token")).ResponseValidator.GetBodyAsText();
            List<Integer> carouselsID = JsonPath.read(response, "$.data.carousels..bannerId");
            Assert.assertFalse(carouselsID.contains(carouselID), "Assertion Failed :: free del carousel should'nt be visible in aggregator for super user in response.");
//            List<String> linkId = JsonPath.read(response,"$.data.carousels[?(@.bannerId=="+carouselID+")].link");
//            Assert.assertFalse(restIds.contains(linkId.get(0)), "Assertion Failed :: link id not same added rests");
        }finally {
            for (int td: tdID) {
                rngHelper.deleteEDVOCampaignFromDB(String.valueOf(td));
            }
            if(carouselID!=0) {
                carouselPOJO.setCarouselTradeDiscountInfoSet(null);
                rngHelper.disableCarousel(carouselPOJO.withEnabled(false).withId(carouselID).withAddPolygonIds(new ArrayList<>()));
            }
        }
    }

}
//    public void testCarouselWidgetWithClosedRestaurant(){}