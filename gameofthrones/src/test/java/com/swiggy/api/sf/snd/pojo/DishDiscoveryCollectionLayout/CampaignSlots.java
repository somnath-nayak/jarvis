package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class CampaignSlots {

    private Integer collection;
    private Integer enabled;
    private Priority priority;
    private String startTime;
    private String endTime;
    private List<Slot> slots = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public CampaignSlots() {
    }

    /**
     *
     * @param startTime
     * @param enabled
     * @param slots
     * @param priority
     * @param collection
     * @param endTime
     */
    public CampaignSlots(Integer collection, Integer enabled, Priority priority, String startTime, String endTime, List<Slot> slots) {
        super();
        this.collection = collection;
        this.enabled = enabled;
        this.priority = priority;
        this.startTime = startTime;
        this.endTime = endTime;
        this.slots = slots;
    }

    public Integer getCollection() {
        return collection;
    }

    public void setCollection(Integer collection) {
        this.collection = collection;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("collection", collection).append("enabled", enabled).append("priority", priority).append("startTime", startTime).append("endTime", endTime).append("slots", slots).toString();
    }

}
