package com.swiggy.api.sf.checkout.dp;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;

public class CheckoutSchemaDP {
    
	EDVOCartHelper eDVOCartHelper=new EDVOCartHelper();
	CheckoutHelper helper= new CheckoutHelper();
	static String restId;
	String[] latLog;
	static Cart cartPayload;
	String defaultProdTestRestList="1542,4993,6209,9990,16080,19669,19684,32219,36245,36252,40562,41300,41836,43346,44348,44468,48506,49177,49308,51797,55649,57494,59838,65997";
	String prodTestRestList=System.getenv("prodTestRestList");
	String environment= System.getenv("environment");
	String[] prodResList;
	
	@BeforeClass
    public void getRestData() {
		if(environment !=null && environment.equals("prod")){
			latLog=new String[]{"27.8374445","76.17257740000002","0"};
			if(prodTestRestList==null){
				prodTestRestList=defaultProdTestRestList;
				}
			prodResList=prodTestRestList.split(",");
			}
		  else {
			  latLog=new String[]{"12.935215600000001","77.6199608","0"};}
		
		
		String listingRes=helper.getRestList(latLog).ResponseValidator.GetBodyAsText();
		String[] restList=JsonPath.read(listingRes, "$.data..id").toString()
				                  .replace("[","").replace("]","").replace("\"","").split(",");
		String[] unServiceabilityList=JsonPath.read(listingRes, "$.data..unserviceable").toString()
                .replace("[","").replace("]","").replace("\"","").split(",");
		String[] isOpenedList=JsonPath.read(listingRes, "$.data..availability..opened").toString()
                .replace("[","").replace("]","").replace("\"","").split(",");
		
		for(int i=0;i<restList.length;i++){
		   if (isOpenedList[i].equalsIgnoreCase("true") && unServiceabilityList[i].equalsIgnoreCase("false")) {
				if(environment !=null && environment.equals("prod")){
			    	if(isRestPresent(prodResList, restList[i])){
				       restId=restList[i];
				       String statuscode=cartValidation();
				          if (statuscode.equals("0")){
				              break;}
			           }
			    	}
			        else{
				       restId=restList[i];
				       String statuscode=cartValidation();
				          if (statuscode.equals("0")){
				              break;}
			            }
		          }
		   }
   }
	
	public String cartValidation(){
		String getStatusCode;
		try{
		cartPayload=eDVOCartHelper.getCartPayload1(null, restId, false, false, true);
		System.out.println(Utility.jsonEncode(cartPayload));
		String response=helper.createCartCheckTotals(Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
		getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
		}catch (Exception e){
			return "97";
		}
		return getStatusCode;
	}
	
	public boolean isRestPresent(String[] prodResList,String restId){
		for(int j=0;j<prodResList.length;j++){
			if(prodResList[j].equals(restId)){
				return true;
			}
		}
		return false;
	}
	
    @DataProvider(name = "schemaValidation")
    public static Object[][] restData() {
    	   return new Object[][]{
    		   {"Swiggy-Android", "310",cartPayload},
    		   {"Web", "311",cartPayload},
    		   {"Swiggy-iOS", "234",cartPayload}
    		 };
    }
}
