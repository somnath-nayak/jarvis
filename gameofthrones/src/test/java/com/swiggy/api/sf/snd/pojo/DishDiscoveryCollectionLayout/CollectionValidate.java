package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class CollectionValidate {

    private String name;
    private Boolean enabled;
    private String entityType;
    private String description;
    private String slug;
    private EnabledFilters enabledFilters;

    /**
     * No args constructor for use in serialization
     *
     */
    public CollectionValidate() {
    }

    /**
     *
     * @param enabled
     * @param enabledFilters
     * @param description
     * @param name
     * @param slug
     * @param entityType
     */
    public CollectionValidate(String name, Boolean enabled, String entityType, String description, String slug, EnabledFilters enabledFilters) {
        super();
        this.name = name;
        this.enabled = enabled;
        this.entityType = entityType;
        this.description = description;
        this.slug = slug;
        this.enabledFilters = enabledFilters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public EnabledFilters getEnabledFilters() {
        return enabledFilters;
    }

    public void setEnabledFilters(EnabledFilters enabledFilters) {
        this.enabledFilters = enabledFilters;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("enabled", enabled).append("entity_type", entityType).append("description", description).append("slug", slug).append("enabled_filters", enabledFilters).toString();
    }

}