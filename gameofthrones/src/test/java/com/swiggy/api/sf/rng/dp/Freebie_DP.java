package com.swiggy.api.sf.rng.dp;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;


import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.*;
import com.swiggy.api.sf.rng.pojo.freebie.CreateFreebieTDEntry;
import com.swiggy.api.sf.rng.pojo.freebie.CreateFreebieTdBuilder;
import com.swiggy.api.sf.rng.pojo.freebie.FreebieRestaurantList;
import com.swiggy.api.sf.rng.pojo.freebie.FreebieSlot;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import io.advantageous.boon.core.Sys;
import net.minidev.json.JSONArray;

import org.apache.commons.lang.time.DateUtils;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.testng.annotations.DataProvider;

public class Freebie_DP {


    RngHelper rngHelper = new RngHelper();
    RngConstants rngConstants;
    CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
    CheckoutHelper checkoutHelper = new CheckoutHelper();


	@DataProvider(name = "getTd")
	public Object[][] getTD() {
		return new Object[][]{
			{"25646"}
		};
	}

	@DataProvider(name = "tdbyrestid")
	public Object[][] tdbyrestid() {
		return new Object[][]{
			{"6120"}
		};
	}

	@DataProvider(name = "createFreebieTD")
	public Object[][] createTD() throws IOException {

		JsonHelper jsonHelper=new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<FreebieSlot> slots = new ArrayList<>();
		slots.add(new FreebieSlot(rngHelper.getCurrentTime(20), "ALL", rngHelper.getCurrentTime(0)));


	List rlist = rngHelper.getRestaurantListing("12.9719","77.6412","2");
	// Creating RestaurantLists
		List<FreebieRestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new FreebieRestaurantList(Integer.parseInt(rlist.get(0).toString())));

		List<FreebieRestaurantList> restaurantLists2 = new ArrayList<>();
		restaurantLists2.add(new FreebieRestaurantList(Integer.parseInt(rlist.get(1).toString())));
		
		List<FreebieRestaurantList> restaurantLists3 = new ArrayList<>();
		restaurantLists3.add(new FreebieRestaurantList(Integer.parseInt(rlist.get(2).toString())));
		
		List<FreebieRestaurantList> restaurantLists4 = new ArrayList<>();
		restaurantLists4.add(new FreebieRestaurantList(Integer.parseInt(rlist.get(3).toString())));
		
		List<FreebieRestaurantList> restaurantLists5 = new ArrayList<>();
		restaurantLists5.add(new FreebieRestaurantList(Integer.parseInt(rlist.get(4).toString())));
		
		List<FreebieRestaurantList> restaurantLists6 = new ArrayList<>();
		restaurantLists6.add(new FreebieRestaurantList(Integer.parseInt(rlist.get(5).toString())));
		
		List<FreebieRestaurantList> restaurantLists7 = new ArrayList<>();
		restaurantLists7.add(new FreebieRestaurantList(Integer.parseInt(rlist.get(6).toString())));
		
		List<FreebieRestaurantList> restaurantLists8 = new ArrayList<>();
		restaurantLists8.add(new FreebieRestaurantList(Integer.parseInt(rlist.get(7).toString())));
		
		List<FreebieRestaurantList> restaurantLists9 = new ArrayList<>();
		restaurantLists9.add(new FreebieRestaurantList(Integer.parseInt(rlist.get(8).toString())));
		
		
         CMSHelper cms = new CMSHelper();
         List<Map<String, Object>> items = cms.getItemDetails(rlist.get(0).toString());

	
         rngHelper.disabledActiveTD(rlist.get(0).toString());
         rngHelper.disabledActiveTD(rlist.get(1).toString());
         rngHelper.disabledActiveTD(rlist.get(2).toString());
         rngHelper.disabledActiveTD(rlist.get(3).toString());
         rngHelper.disabledActiveTD(rlist.get(4).toString());
         rngHelper.disabledActiveTD(rlist.get(5).toString());
         rngHelper.disabledActiveTD(rlist.get(6).toString());
         rngHelper.disabledActiveTD(rlist.get(7).toString());
         rngHelper.disabledActiveTD(rlist.get(8).toString());
       
		CreateFreebieTDEntry freebieTD = new CreateFreebieTdBuilder()
				.campaign_type("Freebie")
				.discountLevel("Restaurant")
				.restaurantList(restaurantLists1)
				.ruleDiscount("Freebie", "Restaurant","0",items.iterator().next().get("item_id").toString())
				.build();
		

		CreateFreebieTDEntry freebieTD2 = new CreateFreebieTdBuilder()
				.campaign_type("Freebie")
				.discountLevel("Restaurant")
				.restaurantList(restaurantLists2)
				.ruleDiscount("Freebie", "Restaurant", "100",(items.iterator().next().get("item_id")).toString())
				.build();
		
		
		
		
		CreateFreebieTDEntry freebieTD3 = new CreateFreebieTdBuilder()
		     .campaign_type("Freebie")
		     .discountLevel("Restaurant")
	         .restaurantList(restaurantLists3)
		.ruleDiscount("Freebie", "Restaurant", "0",(items.iterator().next().get("item_id")).toString())
		.firstOrderRestriction("true")
		.build();

		
		CreateFreebieTDEntry freebieTD4 = new CreateFreebieTdBuilder()
		.campaign_type("Freebie")
		.discountLevel("Restaurant")
		.restaurantList(restaurantLists3)
		.ruleDiscount("Freebie", "Restaurant", "100",(items.iterator().next().get("item_id")).toString())
		.restaurantFirstOrder("true")
		.build();

		

		CreateFreebieTDEntry freebieTD5 = new CreateFreebieTdBuilder()
		.campaign_type("Freebie")
		.discountLevel("Restaurant")
		.restaurantList(restaurantLists5)
		.ruleDiscount("Freebie", "Restaurant","100",(items.iterator().next().get("item_id")).toString())
		.timeSlotRestriction("true")
		.slots(slots)
		.build();
		
		

		CreateFreebieTDEntry freebieTD6 = new CreateFreebieTdBuilder()
		.campaign_type("Freebie")
		.discountLevel("Restaurant")
		.restaurantList(restaurantLists6)
		.ruleDiscount("Freebie", "Restaurant", "100",(items.iterator().next().get("item_id")).toString())
		.dormant_user_type("THIRTY_DAYS_DORMANT")
		.build();
		

		CreateFreebieTDEntry freebieTD7 = new CreateFreebieTdBuilder()
		.campaign_type("Freebie")
		.discountLevel("Restaurant")
		.restaurantList(restaurantLists7)
		.ruleDiscount("Freebie", "Restaurant", "100",(items.iterator().next().get("item_id")).toString())
		.dormant_user_type("SIXTY_DAYS_DORMANT")
		.build();
		
	
		CreateFreebieTDEntry freebieTD8 = new CreateFreebieTdBuilder()
				.campaign_type("Freebie")
				.discountLevel("Restaurant")
				.restaurantList(restaurantLists8)
				.ruleDiscount("Freebie", "Restaurant", "100",(items.iterator().next().get("item_id")).toString())
				.dormant_user_type("NINETY_DAYS_DORMANT")
				.build();
//		
    

	return new Object[][]{
		{jsonHelper.getObjectToJSON(freebieTD)},
		{jsonHelper.getObjectToJSON(freebieTD2)},
		{jsonHelper.getObjectToJSON(freebieTD3)},
		{jsonHelper.getObjectToJSON(freebieTD4)},
		{jsonHelper.getObjectToJSON(freebieTD5)},
		{jsonHelper.getObjectToJSON(freebieTD6)},
		{jsonHelper.getObjectToJSON(freebieTD7)},
		{jsonHelper.getObjectToJSON(freebieTD8)}
	
	        };
	}
}
