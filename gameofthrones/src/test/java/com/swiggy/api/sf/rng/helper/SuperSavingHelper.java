package com.swiggy.api.sf.rng.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.constants.SuperConstants;
import com.swiggy.api.sf.rng.pojo.ApplyCouponPOJO;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.KafkaHelper;
import org.mortbay.util.ajax.JSON;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class SuperSavingHelper implements SuperConstants {
    static Initialize gameofthrones = new Initialize();
    KafkaHelper kafkaHelper = new KafkaHelper();
    RmqHelper rmqHelper = new RmqHelper(RngConstants.RMQ_USERNAME,RngConstants.RMQ_PASSWORD);
    JsonHelper jsonHelper = new JsonHelper();
    SoftAssert softAssert = new SoftAssert();
    SuperHelper superHelper = new SuperHelper();

    SeedingInfoBuilder seedingInfoBuilder = new SeedingInfoBuilder();

    TDBreakupBuilder tdBuilder = new TDBreakupBuilder();
    CreateOrderBuilder createOrderBuilder = new CreateOrderBuilder();
    OrderDeliveryBuilder orderDeliveredBuilder = new OrderDeliveryBuilder();


    static int orderId;
    static String orderDate;

    //API call for SUPER SAVINGS
    public Processor getPlansByUserId(String user_id) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("super", "getuserplansupersaving", gameofthrones);
        String[] urlparams = new String[]{user_id};
        Processor processor = new Processor(service, requestHeader, null, urlparams);
        return processor;
    }


    public HashMap<String, String> getNonIncentiveSubscription(String planPayload, String numOfPlans, String benefitPayload) {
        /**Create User Subscription**/
        SuperDbHelper.deleteFreeDelBenefit();
        SuperDbHelper.deleteFreebieBenefit();
        Processor planResponse = superHelper.createPlan(planPayload);
        System.out.println("planResponse" + planResponse.ResponseValidator.GetBodyAsText());
        int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        Assert.assertEquals(statusCode, 1);
        Processor benefitResponse = superHelper.createBenefit(benefitPayload);
        int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(benefitStatusCode, 1);
        int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        String planBenefitMapPayload = superHelper
                .populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
        Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
        int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
        String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
        Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);
        String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
        String orderId = Integer.toString(Utility.getRandom(1, 1000000));

        String planUserIncentiveMapPayload = superHelper
                .populatePlanUserIncentiveMappingPayload(Integer.toString(planId), publicUserId, "null").toString();
        Processor planUserIncentiveMapResponse = superHelper
                .createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
        int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
                .GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(statusCodePlanUserIncentiveMap, 1);
        String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
        softAssert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);
        Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
        String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
                "$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
        softAssert.assertEquals(benefitDetails.isEmpty(), false);
        String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
        Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
        int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(verifyStatus, 1);
        String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
        softAssert.assertEquals(verifyMessage, SuperConstants.successCheck);

        boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
        softAssert.assertEquals(validPlanVerify, true);
        String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
                publicUserId, orderId);
        Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
        int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(statusCodeOfSubsResp, 1);
        String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
        int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
        softAssert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));
        Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");
        String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
                "$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
        softAssert.assertEquals(benefitDetailsAfterSubs, "[]");
        Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
        int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
        softAssert.assertEquals(subsUserPlanId, planId);
        String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
        softAssert.assertEquals(subsUserBenefitId.isEmpty(), false);
        Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
        List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
                ("$.data..subscription_id"));
        String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
                orderId);
        softAssert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
        for (String id : listOfSubsIds) {
            softAssert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
        }
        System.out.println("PLAN ID" + planId);

        HashMap<String, String> getids = new HashMap<>();
        getids.put("publicUserId",publicUserId);
        getids.put("planId",String.valueOf(planId));
        getids.put("benefitId",String.valueOf(benefitId));

        return getids;
    }




    public HashMap<String, String> getIncentiveSubscription(String planPayload, String numOfPlans, String benefitPayload, HashMap<String, String> createIncentiveData){
        SuperDbHelper.deleteFreebieBenefit();
        SuperDbHelper.deleteFreeDelBenefit();
        Processor planResponse = superHelper.createPlan(planPayload);
        int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        System.out.println("PLAN_ID " + planId);
        Assert.assertEquals(statusCode, 1);
        Processor benefitResponse = superHelper.createBenefit(benefitPayload);
        int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(benefitStatusCode, 1);
        int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

        // create Incentive (tenure zero only)
        String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
        Processor incectiveResp = superHelper.createIncentive(incentivePayload);
        Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
        softAssert.assertNotEquals(incentiveId, null);

        // create plan benefit mapping
        String planBenefitMapPayload = superHelper
                .populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
        Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
        int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
        String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
        softAssert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);

        // User id and order id
        String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
        String orderId = Integer.toString(Utility.getRandom(1, 10000000));

        // plan user incentive mapping for mapped user only
        String planUserIncentiveMapPayload = superHelper
                .populatePlanUserIncentiveMappingPayload(Integer.toString(planId), publicUserId, incentiveId.toString())
                .toString();
        Processor planUserIncentiveMapResponse = superHelper
                .createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
        int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
                .GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
        String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
        Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

        // get plan by mapped userId before taking subs.
        Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
        String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
                "$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
        String incentiveDiscountApplied = planRespByUserId.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
        String incentiveDiscountInfo = planRespByUserId.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
        softAssert.assertNotEquals(incentiveDiscountInfo, "null");
        softAssert.assertEquals(benefitDetails.isEmpty(), false);
        softAssert.assertEquals(incentiveDiscountApplied, "[true]");

        // verify plan by mapped user id
        String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
        Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
        int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(verifyStatus, 1);
        String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
        softAssert.assertEquals(verifyMessage, SuperConstants.successCheck);
        boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");


        // create subscription for mapped user
        String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
                publicUserId, orderId);
        Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
        int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(statusCodeOfSubsResp, 1);
        String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
        int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
        softAssert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));
        Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");

        // plan by mapped user id after subs.
        String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
                "$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
        softAssert.assertEquals(benefitDetailsAfterSubs, "[]");

        // get mapped User current Subs. details
        Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
        int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
        softAssert.assertEquals(subsUserPlanId, planId);
        String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
        softAssert.assertEquals(subsUserBenefitId.isEmpty(), false);
        List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
                ("$.data.benefits..benefit_id"));

        softAssert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

        // mapped user subs. history
        Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
        List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
                ("$.data..subscription_id"));
        String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
                orderId);
        Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
        for (String id : listOfSubsIds) {
            Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
        }
        HashMap<String, String> getids = new HashMap<>();
        getids.put("publicUserId",publicUserId);
        getids.put("planId",String.valueOf(planId));
        getids.put("benefitId", String.valueOf(benefitId));


        return getids;
    }
    public int getRewardTypeIndex(Processor response, String reward_type) {

        Processor api_response = response;
        String required_reward_type = reward_type;
        int index = 0;
        net.minidev.json.JSONArray arr = response.ResponseValidator.GetNodeValueAsJsonArray("$.data.super_savings");

        for (int i = 0; i <= arr.size() - 1; i++) {
            String response_reward_type = response.ResponseValidator.GetNodeValue("$.data.super_savings[" + i + "].reward_type");
            if (required_reward_type.equalsIgnoreCase(response_reward_type)) {
                return index = i;
            }
        }
        return index;
    }


    public void applyPublicDiscountTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        RngHelper rngHelper = new RngHelper();
        SuperHelper superHelper = new SuperHelper();

        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String code = payload.getCode();
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, userId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");
    }


    //Create Order Delivered Payload
    public OrderDelivery getOrderDeliveredPayload(String orderid) {
        OrderDelivery orderDelivery = orderDeliveredBuilder.withOrderId(orderid).withStatus(delivered).build();
        return orderDelivery;
    }


    //Create Order payload
    public CreateOrder getCreateOrderPayload(List<TradeDiscountBreakup> tdBuilderList, String customerid, HashMap<String, String> orderdetails) {
        /*
        ORDERDETAILS hashmap
        0 -> OrderType , 1-> COUPON , 2 ->  COUPON DISCOUNT,  3 -> OrderTags (eg : SUPER)
         */
        List<String> orderTags = new ArrayList<>();
        orderTags.add(orderdetails.get("3"));

        int orderid = createOrderId();
        CreateOrder create = createOrderBuilder.withOrderId(orderid).withCustomerId(String.valueOf(customerid)).withOrderTime(getOrderDateTime()).
                withTradeDiscountBreakup(tdBuilderList).withOrderType(orderdetails.get("0")).withCouponCode(orderdetails.get("1"))
                .withCouponDiscount(Integer.valueOf(orderdetails.get("2"))).withOrderTags(orderTags).build();
        return create;
    }

    //create seeding info
    private SeedingInfo createSeedingInfo() {
        SeedingInfo seed = seedingInfoBuilder.withMerchantType(restaurant).withCommissionLevy(commissionLevy).withShareType(percentage).withShareValue(percentagevalue).build();
        return seed;
    }

    private int createOrderId() {
        Random rand = new Random();
        orderId = rand.nextInt((99999999 - 10000000) + 1) + 10000000;
        return orderId;
    }

    private String getOrderDateTime() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateobj = new Date();
        orderDate = df.format(dateobj);
        return orderDate;
    }


    public long getOneMonthFutureInMilliSecondsFromCurrentDate()
    {

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, 1);
        return cal.getTimeInMillis();
    }


    public HashMap<String, String> createPublicPlan(String validFromDate, String numOfMonthValidity, String planTenure, String planPrice, String renewalOffsetDays){
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays
        /**sampledata**/
        //createPublicPlan.put("0", "0");createPublicPlan.put("1", "3");createPublicPlan.put("2", "1");createPublicPlan.put("3", "99");createPublicPlan.put("4", "5");
        createPublicPlan.put("0", validFromDate);
        createPublicPlan.put("1", numOfMonthValidity);
        createPublicPlan.put("2", planTenure);
        createPublicPlan.put("3", planPrice);
        createPublicPlan.put("4",renewalOffsetDays);
        return  createPublicPlan;
    }

    public HashMap<String, String> createBenefit(String freeDelFreebie, String freeDelFreebiepriority){
        HashMap<String, String> createBenefit = new HashMap<String, String>( );
        /**Free Del Data**/
        // type, priority
        /**sampledata createBenefit.put("0", "FREE_DELIVERY");  createBenefit.put("1", "1"); **/

        createBenefit.put("0", freeDelFreebie);
        createBenefit.put("1", freeDelFreebiepriority);

        return createBenefit;
    }

    public HashMap<String, String> createOrderDetailsMap(String orderType, String coupon, String couponDiscount, String orderTags){
        HashMap<String, String> orderdetails = new HashMap<>();
        // 0 -> OrderType , 1-> COUPON , 2 ->  COUPON DISCOUNT,  3 -> OrderTags (eg : SUPER)
        orderdetails.put("0", orderType);
        orderdetails.put("1", coupon);
        orderdetails.put("2", couponDiscount);
        orderdetails.put("3", orderTags);

        return orderdetails;
    }

    public HashMap<String, String> createIncentive(String flatPercentageType, String value, String cap, String paymentMethods, String tenure, String priority, String enabled ){
        HashMap<String, String> createIncentiveMap = new HashMap<String, String>();
        // type, value, cap, paymentMethods, tenure, priority, enabled
        createIncentiveMap.put("0", flatPercentageType);
        createIncentiveMap.put("1", value);
        createIncentiveMap.put("2", cap);
        createIncentiveMap.put("3", paymentMethods);
        createIncentiveMap.put("4", tenure);
        createIncentiveMap.put("5", priority);
        createIncentiveMap.put("6", enabled);

        return createIncentiveMap;

    }

    public HashMap<String, String> createWasSuperPlan(String enabled, String validTill, String count){
        HashMap<String, String> wasSuperPlan = new HashMap<String, String>();

        // plan_id, enabled, valid_till, count - plan id will be added in the testcase
        wasSuperPlan.put("1",enabled);
        wasSuperPlan.put("2",validTill);
        wasSuperPlan.put("3",count);
        return wasSuperPlan;

    }


}
