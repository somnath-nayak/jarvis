package com.swiggy.api.sf.rng.dp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import com.swiggy.api.sf.rng.tests.RandomNumber;
import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.DataProvider;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.Slot;
import framework.gameofthrones.Tyrion.JsonHelper;

public class MultiTDDP {
	JsonHelper j = new JsonHelper();

	RngConstants RngConstants;
	RngHelper rngHelper = new RngHelper();
	RandomNumber rm = new RandomNumber(2000, 3000);
	
	
	@DataProvider(name = "createTDRestaurantLevel")
	public Object[][] createTD() throws IOException {
		
		
		List<RestaurantList> restaurantLists1 = new ArrayList<>();

        rngHelper.disabledActiveTD(RngConstants.randomRestaurant);
        
		restaurantLists1.add(new RestaurantList(RngConstants.randomRestaurant, "Test", new ArrayList<>()));
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("2320", "ALL", "0005"));
		CreateTdEntry NoRestriction = new CreateTdBuilder().nameSpace("MultiTDCampaign5")
				.header("MultiTDCampaign5Header")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("90").enabled(true).swiggy_hit("10").createdBy("Ramzi")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "200").userRestriction(false)
				.firstOrderRestriction(false).restaurantFirstOrder(false).dormant_user_type("ZERO_DAYS_DORMANT")
				.timeSlotRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false).build();

		CreateTdEntry userRestriction = new CreateTdBuilder().nameSpace("MultiTDCampaign1")
				.header("MultiTDCampaign1Header")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("90").enabled(true).swiggy_hit("10").createdBy("Ramzi")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "200").userRestriction(true)
				.firstOrderRestriction(false).restaurantFirstOrder(false).dormant_user_type("ZERO_DAYS_DORMANT")
				.timeSlotRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false).build();

		CreateTdEntry firstOrderRestriction = new CreateTdBuilder().nameSpace("MultiTDCampaign2")
				.header("MultiTDCampaign2Header")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("90").enabled(true).swiggy_hit("10").createdBy("Ramzi")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "200").userRestriction(false)
				.firstOrderRestriction(true).restaurantFirstOrder(false).dormant_user_type("ZERO_DAYS_DORMANT")
				.timeSlotRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false).build();

		CreateTdEntry restaurantFirstOrder = new CreateTdBuilder().nameSpace("MultiTDCampaign3")
				.header("MultiTDCampaign3Header")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("90").enabled(true).swiggy_hit("10").createdBy("Ramzi")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "200").userRestriction(false)
				.firstOrderRestriction(false).restaurantFirstOrder(true).dormant_user_type("ZERO_DAYS_DORMANT")
				.timeSlotRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false).build();

		CreateTdEntry dormant_user_type = new CreateTdBuilder().nameSpace("MultiTDCampaign4")
				.header("MultiTDCampaign4Header")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 20000).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("90").enabled(true).swiggy_hit("10").createdBy("Ramzi")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "200").userRestriction(false)
				.firstOrderRestriction(false).restaurantFirstOrder(false).dormant_user_type("THIRTY_DAYS_DORMANT")
				.timeSlotRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false).build();

		
		return new Object[][] { 
				{ "firstOrderRestriction", j.getObjectToJSON(firstOrderRestriction) },
				{ "restaurantFirstOrder", j.getObjectToJSON(restaurantFirstOrder) },
				{ "dormant_user_type", j.getObjectToJSON(dormant_user_type) },
				{ "userRestriction", j.getObjectToJSON(userRestriction) },
				{ "NoRestriction", j.getObjectToJSON(NoRestriction) },
			
		};

	}
	
	
	@DataProvider(name = "userSegmentData")
	public Object[][] getUserSegmentData() {
		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString();
		return new Object[][] { { RngConstants.testUser} };
	}
	

	@DataProvider(name = "ListingEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder")
	public Object[][] ListingEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder() throws IOException {
		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser;  //will get it from sessionAPIs
		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString().replace("\"","").trim();
		return new Object[][]{
				{randomRestaurant, "true", userId, RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant, "true", userId, RngConstants.AndroidOS, RngConstants.versionCode200},
				{randomRestaurant, "true", userId, RngConstants.IosOS, RngConstants.versionCode200},
				{randomRestaurant, "true", userId, RngConstants.IosOS, RngConstants.versionCode250},
				{randomRestaurant, "true", userId, RngConstants.WebOS, RngConstants.versionCode250},
				{randomRestaurant, "true", userId, RngConstants.WebOS, RngConstants.versionCode200}
		};
	}

	@DataProvider(name = "MenuEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder")
	public Object[][] MenuEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder() throws IOException {

		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString().replace("\"","").trim();
		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser;  //will get it from sessionAPIs
		return new Object[][]{
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.AndroidOS, RngConstants.versionCode229},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.AndroidOS, RngConstants.versionCode200},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.IosOS, RngConstants.versionCode200},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.IosOS, RngConstants.versionCode250},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.WebOS, RngConstants.versionCode250},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.WebOS, RngConstants.versionCode200}
		};
	}
	
	@DataProvider(name = "CartEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder")
	public Object[][] CartEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder() throws IOException {

		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser;  //will get it from sessionAPIs

		//List<Map<String,Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);

		String itemId =  new Integer(rm.nextInt()).toString();//((BigInteger) itemDetails.get(0).get("item_id")).toString();
		String categoryId =  new Integer(rm.nextInt()).toString();//((BigInteger) itemDetails.get(0).get("cat_id")).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();//((BigInteger) itemDetails.get(0).get("subcat_id")).toString();
		String count = "1";
		String price="200";
		String minCartAmount=price;
		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString().replace("\"","").trim();
		
		return new Object[][]{
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,"0", userId, "true", RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.AndroidOS, RngConstants.versionCode200},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true",RngConstants.IosOS, RngConstants.versionCode200},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.IosOS, RngConstants.versionCode250},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.WebOS, RngConstants.versionCode250},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.WebOS, RngConstants.versionCode200}
				
						};
	}
	
	@DataProvider(name = "ListingEvaluation_RestaurantFirstOrder_FirstOrder")
	public Object[][] ListingEvaluation_RestaurantFirstOrder_FirstOrder() throws IOException {
		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser3;  //will get it from sessionAPIs
		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString().replace("\"","").trim();
		return new Object[][]{
				{randomRestaurant, "true", userId, RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant, "true", userId, RngConstants.AndroidOS, RngConstants.versionCode200},
				{randomRestaurant, "true", userId, RngConstants.IosOS, RngConstants.versionCode200},
				{randomRestaurant, "true", userId, RngConstants.IosOS, RngConstants.versionCode250},
				{randomRestaurant, "true", userId, RngConstants.WebOS, RngConstants.versionCode250},
				{randomRestaurant, "true", userId, RngConstants.WebOS, RngConstants.versionCode200}
		};
	}
	
	@DataProvider(name = "MenuEvaluation_RestaurantFirstOrder_FirstOrder")
	public Object[][] MenuEvaluation_RestaurantFirstOrder_FirstOrder() throws IOException {

		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString().replace("\"","").trim();
		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser3;  //will get it from sessionAPIs
		return new Object[][]{
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.AndroidOS, RngConstants.versionCode229},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.AndroidOS, RngConstants.versionCode200},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.IosOS, RngConstants.versionCode200},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.IosOS, RngConstants.versionCode250},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.WebOS, RngConstants.versionCode250},
				{RngConstants.minCartAmount,randomRestaurant, "true", userId, RngConstants.WebOS, RngConstants.versionCode200}
		};
	}
	
	@DataProvider(name = "CartEvaluation_RestaurantFirstOrder_FirstOrder")
	public Object[][] CartEvaluation_RestaurantFirstOrder_FirstOrder() throws IOException {

		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser3;  //will get it from sessionAPIs

		//List<Map<String,Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);

		String itemId = new Integer(rm.nextInt()).toString();//((BigInteger) itemDetails.get(0).get("item_id")).toString();
		String categoryId =  new Integer(rm.nextInt()).toString();//((BigInteger) itemDetails.get(0).get("cat_id")).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();//((BigInteger) itemDetails.get(0).get("subcat_id")).toString();
		String count = "1";
		String price="200";
		String minCartAmount=price;
		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString().replace("\"","").trim();
		
		return new Object[][]{
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,"0", userId, "true", RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.AndroidOS, RngConstants.versionCode200},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true",RngConstants.IosOS, RngConstants.versionCode200},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.IosOS, RngConstants.versionCode250},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.WebOS, RngConstants.versionCode250},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "true", RngConstants.WebOS, RngConstants.versionCode200}
				
						};
	}
	
	@DataProvider(name = "ListingEvaluation_PublicTD_ThirtyDaysDormant")
	public Object[][] ListingEvaluation_PublicTD_ThirtyDaysDormant() throws IOException {
		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser2;  //will get it from sessionAPIs
		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString().replace("\"","").trim();
		return new Object[][]{
				{randomRestaurant, "false", userId, RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant, "false", userId, RngConstants.AndroidOS, RngConstants.versionCode200},
				{randomRestaurant, "false", userId, RngConstants.IosOS, RngConstants.versionCode200},
				{randomRestaurant, "false", userId, RngConstants.IosOS, RngConstants.versionCode250},
				{randomRestaurant, "false", userId, RngConstants.WebOS, RngConstants.versionCode250},
				{randomRestaurant, "false", userId, RngConstants.WebOS, RngConstants.versionCode200}
		};
	}
	
	@DataProvider(name = "dormantwithrestaurant")
	public Object[][] dormantwithrestaurant()
	{
		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser2;  //will
		
		return new Object[][]{{randomRestaurant,userId}};
	}
	
	
	@DataProvider(name = "MenuEvaluation_PublicTD_ThirtyDaysDormant")
	public Object[][] MenuEvaluation_PublicTD_ThirtyDaysDormant() throws IOException {

		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString().replace("\"","").trim();
		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser2; //will get it from sessionAPIs
		return new Object[][]{
				{RngConstants.minCartAmount,randomRestaurant, "false", userId, RngConstants.AndroidOS, RngConstants.versionCode229},
				{RngConstants.minCartAmount,randomRestaurant, "false", userId, RngConstants.AndroidOS, RngConstants.versionCode200},
				{RngConstants.minCartAmount,randomRestaurant, "false", userId, RngConstants.IosOS, RngConstants.versionCode200},
				{RngConstants.minCartAmount,randomRestaurant, "false", userId, RngConstants.IosOS, RngConstants.versionCode250},
				{RngConstants.minCartAmount,randomRestaurant, "false", userId, RngConstants.WebOS, RngConstants.versionCode250},
				{RngConstants.minCartAmount,randomRestaurant, "false", userId, RngConstants.WebOS, RngConstants.versionCode200}
		};
	}
	
	@DataProvider(name = "CartEvaluation_PublicTD_ThirtyDaysDormant")
	public Object[][] CartEvaluation_PublicTD_ThirtyDaysDormant() throws IOException {

		String randomRestaurant=RngConstants.randomRestaurant;//will get it from listing
		String userId=RngConstants.testUser2; //will get it from sessionAPIs

		//List<Map<String,Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);

		String itemId =  new Integer(rm.nextInt()).toString();//((BigInteger) itemDetails.get(0).get("item_id")).toString();
		String categoryId =  new Integer(rm.nextInt()).toString();//((BigInteger) itemDetails.get(0).get("cat_id")).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();//((BigInteger) itemDetails.get(0).get("subcat_id")).toString();
		String count = "1";
		String price="200";
		String minCartAmount=price;
		//String userId = checkoutHelper.TokenData(RngConstants.mobile1, RngConstants.password1).get("userId").toString().replace("\"","").trim();
		
		return new Object[][]{
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,"0", userId, "false", RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "false", RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "false", RngConstants.AndroidOS, RngConstants.versionCode229},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "false", RngConstants.AndroidOS, RngConstants.versionCode200},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "false",RngConstants.IosOS, RngConstants.versionCode200},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "false", RngConstants.IosOS, RngConstants.versionCode250},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "false", RngConstants.WebOS, RngConstants.versionCode250},
				{randomRestaurant,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId, "false", RngConstants.WebOS, RngConstants.versionCode200}
				
						};
	}
	
	

}
