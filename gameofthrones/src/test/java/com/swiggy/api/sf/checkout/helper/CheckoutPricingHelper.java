package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;

import java.util.HashMap;

public class CheckoutPricingHelper {

	String sessionId = null;
	String token = null;
	String tid = null;
	Long serviceableAddress = null;

	Initialize gameofthrones = new Initialize();
	
    public Processor setThresholdFee(String restId,String startTime,String endTime,String fixFee,String DefaultFee,String cartTotalRangeFrom,String cartTotalRangeTo,String cartFee) {
	HashMap<String, String> requestheaders = new HashMap<>();
	requestheaders.put("content-type", "application/json");
	requestheaders.put("Authorization",CheckoutConstants.authorization);
	GameOfThronesService service = new GameOfThronesService("checkout", "threshold-fee", gameofthrones);
	String[] payloadparams = new String[8];
	payloadparams[0] = restId;
	payloadparams[1] = startTime;
	payloadparams[2] = endTime;
	payloadparams[3] = fixFee;
	payloadparams[4] = DefaultFee;
	payloadparams[5] = cartTotalRangeFrom;
	payloadparams[6] = cartTotalRangeTo;
	payloadparams[7] = cartFee;
	Processor processor = new Processor(service, requestheaders, payloadparams);
	return processor;
}

    public void validateThresholdFeeDataFromResponse(String response, String restId,String thresholdFee) {
	String restGetId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
	Assert.assertEquals(restId, restGetId, "restaurant ID mismatch");
	String getThresholdFee = JsonPath.read(response, "$.data.threshold_fee").toString().replace("[", "").replace("]", "");
	Assert.assertEquals(getThresholdFee,thresholdFee,"thresholdFee mismatch");
	}
	
    public void validateSucessStatusCode(int statusCode) {
        Assert.assertEquals(statusCode,PricingConstants.SUCCESS_CODE);
    }
    
    public void validateApiResData(String response,String statusMessage) {
    String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
	Assert.assertEquals(getStatusCode,PricingConstants.VALID_STATUS_CODE);
	String getMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
	Assert.assertEquals(getMessage,statusMessage);
	}

    public Processor setThresholdFeeCity(String cityID,String startTime,String endTime,String fixFee,String DefaultFee,String cartTotalRangeFrom,String cartTotalRangeTo,String cartFee) {
    	HashMap<String, String> requestheaders = new HashMap<>();
    	requestheaders.put("content-type", "application/json");
    	requestheaders.put("Authorization",CheckoutConstants.authorization);
    	GameOfThronesService service = new GameOfThronesService("checkout", "threshold-feeCity", gameofthrones);
    	String[] payloadparams = new String[8];
    	payloadparams[0] = cityID;
    	payloadparams[1] = startTime;
    	payloadparams[2] = endTime;
    	payloadparams[3] = fixFee;
    	payloadparams[4] = DefaultFee;
    	payloadparams[5] = cartTotalRangeFrom;
    	payloadparams[6] = cartTotalRangeTo;
    	payloadparams[7] = cartFee;
    	Processor processor = new Processor(service, requestheaders, payloadparams);
    	return processor;
    }

    public Processor setDistanceFee(String restID,String startTime,String endTime,String defaultFee,String distanceFrom,String distanceTo,String cartFee) {
    	HashMap<String, String> requestheaders = new HashMap<>();
    	requestheaders.put("content-type", "application/json");
    	requestheaders.put("Authorization",CheckoutConstants.authorization);
    	GameOfThronesService service = new GameOfThronesService("checkout", "distance-fee", gameofthrones);
    	String[] payloadparams = new String[7];
    	payloadparams[0] = restID;
    	payloadparams[1] = startTime;
    	payloadparams[2] = endTime;
    	payloadparams[3] = defaultFee;
    	payloadparams[4] = distanceFrom;
    	payloadparams[5] = distanceTo;
    	payloadparams[6] = cartFee;
    	Processor processor = new Processor(service, requestheaders, payloadparams);
    	return processor;
    }
    
    public Processor setDistanceFeeCity(String cityID,String startTime,String endTime,String defaultFee,String distanceFrom,String distanceTo,String cartFee) {
    	HashMap<String, String> requestheaders = new HashMap<>();
    	requestheaders.put("content-type", "application/json");
    	requestheaders.put("Authorization",CheckoutConstants.authorization);
    	GameOfThronesService service = new GameOfThronesService("checkout", "distance-feeCity", gameofthrones);
    	String[] payloadparams = new String[7];
    	payloadparams[0] = cityID;
    	payloadparams[1] = startTime;
    	payloadparams[2] = endTime;
    	payloadparams[3] = defaultFee;
    	payloadparams[4] = distanceFrom;
    	payloadparams[5] = distanceTo;
    	payloadparams[6] = cartFee;
    	Processor processor = new Processor(service, requestheaders, payloadparams);
    	return processor;
    }
    
    public Processor setTimeFee(String restID,String startTime,String endTime,String cartFee) {
    	HashMap<String, String> requestheaders = new HashMap<>();
    	requestheaders.put("content-type", "application/json");
    	requestheaders.put("Authorization",CheckoutConstants.authorization);
    	GameOfThronesService service = new GameOfThronesService("checkout", "time-fee", gameofthrones);
    	String[] payloadparams = new String[4];
    	payloadparams[0] = restID;
    	payloadparams[1] = startTime;
    	payloadparams[2] = endTime;
    	payloadparams[3] = cartFee;
    	Processor processor = new Processor(service, requestheaders, payloadparams);
    	return processor;
    }
    
    public Processor setTimeFeeCity(String cityID,String startTime,String endTime,String cartFee) {
    	HashMap<String, String> requestheaders = new HashMap<>();
    	requestheaders.put("content-type", "application/json");
    	requestheaders.put("Authorization",CheckoutConstants.authorization);
    	GameOfThronesService service = new GameOfThronesService("checkout", "time-feeCity", gameofthrones);
    	String[] payloadparams = new String[4];
    	payloadparams[0] = cityID;
    	payloadparams[1] = startTime;
    	payloadparams[2] = endTime;
    	payloadparams[3] = cartFee;
    	Processor processor = new Processor(service, requestheaders, payloadparams);
    	return processor;
    }
    
    public Processor setSpecialFeeCity(String cityID,String rainMode,String cartFee,String priority,String fromDate,String toDate) {
    	HashMap<String, String> requestheaders = new HashMap<>();
    	requestheaders.put("content-type", "application/json");
    	requestheaders.put("Authorization",CheckoutConstants.authorization);
    	GameOfThronesService service = new GameOfThronesService("checkout", "special-feeCity", gameofthrones);
    	String[] payloadparams = new String[6];
    	payloadparams[0] = cityID;
    	payloadparams[1] = rainMode;
    	payloadparams[2] = cartFee;
    	payloadparams[3] = priority;
    	payloadparams[4] = fromDate;
    	payloadparams[5] = toDate;
    	Processor processor = new Processor(service, requestheaders, payloadparams);
    	return processor;
    }
    
    public Processor setSpecialFee(String restID,String rainMode,String cartFee,String priority,String fromDate,String toDate) {
    	HashMap<String, String> requestheaders = new HashMap<>();
    	requestheaders.put("content-type", "application/json");
    	requestheaders.put("Authorization",CheckoutConstants.authorization);
    	GameOfThronesService service = new GameOfThronesService("checkout", "special-fee", gameofthrones);
    	String[] payloadparams = new String[6];
    	payloadparams[0] = restID;
    	payloadparams[1] = rainMode;
    	payloadparams[2] = cartFee;
    	payloadparams[3] = priority;
    	payloadparams[4] = fromDate;
    	payloadparams[5] = toDate;
    	Processor processor = new Processor(service, requestheaders, payloadparams);
    	return processor;
    }
    
    public Processor updatePricingMessage(String restID,String apiMessage) {
    	HashMap<String, String> requestheaders = new HashMap<>();
    	requestheaders.put("content-type", "application/json");
    	requestheaders.put("Authorization",CheckoutConstants.authorization);
    	GameOfThronesService service = new GameOfThronesService("checkout", "message-Update", gameofthrones);
    	String[] payloadparams = new String[2];
    	payloadparams[0] = restID;
    	payloadparams[1] = apiMessage;
    	Processor processor = new Processor(service, requestheaders, payloadparams);
    	return processor;
    }
    
    public Processor setFetchPricing(String cityID,String restID,String distance,String rainMode) {
    	HashMap<String, String> requestheaders = new HashMap<>();
    	requestheaders.put("content-type", "application/json");
    	requestheaders.put("Authorization",CheckoutConstants.authorization);
    	GameOfThronesService service = new GameOfThronesService("checkout", "fetchPricing", gameofthrones);
    	String[] payloadparams = new String[4];
    	payloadparams[0] = cityID;
    	payloadparams[1] = restID;
    	payloadparams[2] = distance;
    	payloadparams[3] = rainMode;
    	Processor processor = new Processor(service, requestheaders, payloadparams);
    	return processor;
    }
    
    public void validateDistanceFeeDataFromResponse(String response, String restId,String distanceFee) {
	String restGetId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
	Assert.assertEquals(restGetId,restId, "restaurant ID mismatch");
	String getDistanceFee = JsonPath.read(response, "$.data.distance_fee").toString().replace("[", "").replace("]", "");
	Assert.assertEquals(getDistanceFee,distanceFee, "distanceFee mismatch");
	}
    
    public void validateTimeFeeDataFromResponse(String response, String restId,String timeFee) {
    	String restGetId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
    	Assert.assertEquals(restId, restGetId, "restaurant ID mismatch");
    	String getDistanceFee = JsonPath.read(response, "$.data.time_fee").toString().replace("[", "").replace("]", "");
    	Assert.assertEquals(timeFee, getDistanceFee,"timeFee mismatch");
    }
    
    public void validateSpecialFeeDataFromResponse(String response, String restId,String specialFee) {
    	String restGetId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
    	Assert.assertEquals(restGetId,restId, "restaurant ID mismatch");
    	String getDistanceFee = JsonPath.read(response, "$.data.special_fee").toString().replace("[", "").replace("]", "");
    	Assert.assertEquals(getDistanceFee,specialFee,"special Fee mismatch");
    }

	public Processor setFetchPricing(HashMap<String, String> requestheaders,String cityID,String restID,String distance,String rainMode) {

    	GameOfThronesService service = new GameOfThronesService("checkout", "fetchPricing", gameofthrones);
		String[] payloadparams = new String[4];
		payloadparams[0] = cityID;
		payloadparams[1] = restID;
		payloadparams[2] = distance;
		payloadparams[3] = rainMode;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}


	public Processor setThresholdFee(HashMap<String, String> requestheaders,String restId,String startTime,String endTime,String fixFee,String DefaultFee,String cartTotalRangeFrom,String cartTotalRangeTo,String cartFee) {

		GameOfThronesService service = new GameOfThronesService("checkout", "threshold-fee", gameofthrones);
		String[] payloadparams = new String[8];
		payloadparams[0] = restId;
		payloadparams[1] = startTime;
		payloadparams[2] = endTime;
		payloadparams[3] = fixFee;
		payloadparams[4] = DefaultFee;
		payloadparams[5] = cartTotalRangeFrom;
		payloadparams[6] = cartTotalRangeTo;
		payloadparams[7] = cartFee;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor setDistanceFee(HashMap<String, String> requestheaders ,String restID,String startTime,String endTime,String defaultFee,String distanceFrom,String distanceTo,String cartFee) {
		GameOfThronesService service = new GameOfThronesService("checkout", "distance-fee", gameofthrones);
		String[] payloadparams = new String[7];
		payloadparams[0] = restID;
		payloadparams[1] = startTime;
		payloadparams[2] = endTime;
		payloadparams[3] = defaultFee;
		payloadparams[4] = distanceFrom;
		payloadparams[5] = distanceTo;
		payloadparams[6] = cartFee;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

}

