package com.swiggy.api.sf.snd.pojo.Super;

import org.apache.commons.lang.builder.ToStringBuilder;

public class AddSchema {

    private String key;
    private JsonSchema jsonSchema;
    private String schemaVersion;
    private String author;
    private DefaultValue defaultValue;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public JsonSchema getJsonSchema() {
        return jsonSchema;
    }

    public void setJsonSchema(JsonSchema jsonSchema) {
        this.jsonSchema = jsonSchema;
    }

    public String getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(String schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public DefaultValue getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(DefaultValue defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("key", key).append("jsonSchema", jsonSchema).append("schemaVersion", schemaVersion).append("author", author).append("defaultValue", defaultValue).toString();
    }

}