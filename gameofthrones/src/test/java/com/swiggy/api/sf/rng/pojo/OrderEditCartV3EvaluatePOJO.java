package com.swiggy.api.sf.rng.pojo;

import com.swiggy.api.sf.rng.pojo.edvo.EDVOCartMealItemRequest;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class OrderEditCartV3EvaluatePOJO {
	@JsonProperty("itemRequests")
	private List<ItemRequest> itemRequests = null;
	@JsonProperty("orderRequest")
	private OrderRequest orderRequest;
	@JsonProperty("minCartAmount")
	private Double minCartAmount;
	@JsonProperty("userId")
	private Integer userId;
	@JsonProperty("restaurantFirstOrder")
	private Boolean restaurantFirstOrder;
	@JsonProperty("userAgent")
	private String userAgent;
	@JsonProperty("versionCode")
	private String versionCode;
	@JsonProperty("firstOrder")
	private Boolean firstOrder;
	@JsonProperty("mealItemRequests")
	private List<EDVOCartMealItemRequest> mealItemRequest;
	
	@JsonProperty("itemRequests")
	public List<ItemRequest> getItemRequests() {
	return itemRequests;
	}
	@JsonProperty("itemRequests")
	public void setItemRequests(List<ItemRequest> itemRequests) {
	this.itemRequests = itemRequests;
	}

	public OrderEditCartV3EvaluatePOJO withItemRequests(List<ItemRequest> itemRequests) {
	this.itemRequests = itemRequests;
	return this;
	}

	@JsonProperty("orderRequest")
	public OrderRequest getOrderRequest() {
	return orderRequest;
	}

	@JsonProperty("orderRequest")
	public void setOrderRequest(OrderRequest orderRequest) {
	this.orderRequest = orderRequest;
	}

	public OrderEditCartV3EvaluatePOJO withOrderRequest(OrderRequest orderRequest) {
	this.orderRequest = orderRequest;
	return this;
	}

	@JsonProperty("minCartAmount")
	public Double getMinCartAmount() {
	return minCartAmount;
	}

	@JsonProperty("minCartAmount")
	public void setMinCartAmount(Double minCartAmount) {
	this.minCartAmount = minCartAmount;
	}

	public OrderEditCartV3EvaluatePOJO withMinCartAmount(Double minCartAmount) {
	this.minCartAmount = minCartAmount;
	return this;
	}

	@JsonProperty("userId")
	public Integer getUserId() {
	return userId;
	}

	@JsonProperty("userId")
	public void setUserId(Integer userId) {
	this.userId = userId;
	}

	public OrderEditCartV3EvaluatePOJO withUserId(Integer userId) {
	this.userId = userId;
	return this;
	}

	@JsonProperty("restaurantFirstOrder")
	public Boolean getRestaurantFirstOrder() {
	return restaurantFirstOrder;
	}

	@JsonProperty("restaurantFirstOrder")
	public void setRestaurantFirstOrder(Boolean restaurantFirstOrder) {
	this.restaurantFirstOrder = restaurantFirstOrder;
	}

	public OrderEditCartV3EvaluatePOJO withRestaurantFirstOrder(Boolean restaurantFirstOrder) {
	this.restaurantFirstOrder = restaurantFirstOrder;
	return this;
	}

	@JsonProperty("userAgent")
	public String getUserAgent() {
	return userAgent;
	}

	@JsonProperty("userAgent")
	public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
	}

	public OrderEditCartV3EvaluatePOJO withUserAgent(String userAgent) {
	this.userAgent = userAgent;
	return this;
	}

	@JsonProperty("versionCode")
	public String getVersionCode() {
	return versionCode;
	}

	@JsonProperty("versionCode")
	public void setVersionCode(String versionCode) {
	this.versionCode = versionCode;
	}

	public OrderEditCartV3EvaluatePOJO withVersionCode(String versionCode) {
	this.versionCode = versionCode;
	return this;
	}

	@JsonProperty("firstOrder")
	public Boolean getFirstOrder() {
	return firstOrder;
	}

	public List<EDVOCartMealItemRequest> getMealItemRequest() {
		return mealItemRequest;
	}
	public void setMealItemRequest(List<EDVOCartMealItemRequest> mealItemRequest) {
		this.mealItemRequest = mealItemRequest;
	}
	@JsonProperty("firstOrder")
	public void setFirstOrder(Boolean firstOrder) {
	this.firstOrder = firstOrder;
	}

	public OrderEditCartV3EvaluatePOJO withFirstOrder(Boolean firstOrder) {
	this.firstOrder = firstOrder;
	return this;
	}
	
	public OrderEditCartV3EvaluatePOJO setDefaultData(String price, String restaurantId, Boolean firstOrder,Boolean restFirstOrder, int orderEditedSource, List<Integer> campaignIds) {
		OrderRequest orderRequest = new OrderRequest().withOrderEdited(true).withCampaignIds(campaignIds).withOrderEditedSource(orderEditedSource);
		ItemRequest itemRequest = new ItemRequest(restaurantId, "1074155", "1079870", "66666", "1", price);
		return this.withFirstOrder(firstOrder).withUserId(1025660).withRestaurantFirstOrder(restFirstOrder).withMinCartAmount(Double.valueOf(price))
		.withUserAgent("IOS").withVersionCode("207").withOrderRequest(orderRequest).withItemRequests(new ArrayList<ItemRequest>() {{add(itemRequest);}});
	}
}
