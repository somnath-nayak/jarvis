package com.swiggy.api.sf.checkout.pojo.mySms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonPropertyOrder({
        "apiKey",
        "authToken",
        "address",
        "offset",
        "limit"
})
public class MySmsGetConversations {

    @JsonProperty("apiKey")
    private String apiKey;
    @JsonProperty("authToken")
    private String authToken;
    @JsonProperty("address")
    private String address;
    @JsonProperty("offset")
    private Integer offset;
    @JsonProperty("limit")
    private Integer limit;

    /**
     * No args constructor for use in serialization
     *
     */
    public MySmsGetConversations() {
    }

    /**
     *
     * @param limit
     * @param address
     * @param authToken
     * @param offset
     * @param apiKey
     */
    public MySmsGetConversations(String apiKey, String authToken, String address, Integer offset, Integer limit) {
        super();
        this.apiKey = apiKey;
        this.authToken = authToken;
        this.address = address;
        this.offset = offset;
        this.limit = limit;
    }

    @JsonProperty("apiKey")
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty("apiKey")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonProperty("authToken")
    public String getAuthToken() {
        return authToken;
    }

    @JsonProperty("authToken")
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("offset")
    public Integer getOffset() {
        return offset;
    }

    @JsonProperty("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @JsonProperty("limit")
    public Integer getLimit() {
        return limit;
    }

    @JsonProperty("limit")
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("apiKey", apiKey).append("authToken", authToken).append("address", address).append("offset", offset).append("limit", limit).toString();
    }

}