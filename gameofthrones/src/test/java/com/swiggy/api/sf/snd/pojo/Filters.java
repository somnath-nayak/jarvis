package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class Filters {

    @JsonProperty("itemId")
    private List<ItemId> itemId = null;
    @JsonProperty("itemName")
    private List<ItemName> itemName = null;
    private List<ItemPrice> itemPrice = null;
    private List<ItemEnabled> itemEnabled = null;
    private List<ItemLongDistanceEnabled> itemLongDistanceEnabled = null;
    private List<RestaurantId> restaurantId = null;
    private List<RestaurantName> restaurantName = null;
    private List<RestaurantLocation> restaurantLocation = null;
    private List<RestaurantEnabled> restaurantEnabled = null;
    private List<RestaurantLongDistanceEnabled> restaurantLongDistanceEnabled = null;
    private List<CityId> cityId = null;
    private List<CityName> cityName = null;
    private List<AreaId> areaId = null;
    private List<AreaName> areaName = null;
    private List<Tag> tags = null;
    @JsonProperty("orderCounts.breakfast")
    private List<OrderCountsBreakfast> orderCountsBreakfast = null;
    @JsonProperty("orderCounts.lunch")
    private List<OrderCountsBreakfast> orderCountslunch = null;
    @JsonProperty("orderCounts.dinner")
    private List<OrderCountsBreakfast> orderCountsdinner = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Filters() {
    }

    /**
     *
     * @param tags
     * @param itemName
     * @param cityId
     * @param cityName
     * @param restaurantEnabled
     * @param itemId
     * @param itemPrice
     * @param itemLongDistanceEnabled
     * @param areaId
     * @param restaurantLocation
     * @param areaName
     * @param orderCountsBreakfast
     * @param restaurantLongDistanceEnabled
     * @param itemEnabled
     * @param restaurantId
     * @param restaurantName
     */
    public Filters(List<ItemId> itemId, List<ItemName> itemName, List<ItemPrice> itemPrice, List<ItemEnabled> itemEnabled, List<ItemLongDistanceEnabled> itemLongDistanceEnabled, List<RestaurantId> restaurantId, List<RestaurantName> restaurantName, List<RestaurantLocation> restaurantLocation, List<RestaurantEnabled> restaurantEnabled, List<RestaurantLongDistanceEnabled> restaurantLongDistanceEnabled, List<CityId> cityId, List<CityName> cityName, List<AreaId> areaId, List<AreaName> areaName, List<Tag> tags, List<OrderCountsBreakfast> orderCountsBreakfast) {
        super();
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemEnabled = itemEnabled;
        this.itemLongDistanceEnabled = itemLongDistanceEnabled;
        this.restaurantId = restaurantId;
        this.restaurantName = restaurantName;
        this.restaurantLocation = restaurantLocation;
        this.restaurantEnabled = restaurantEnabled;
        this.restaurantLongDistanceEnabled = restaurantLongDistanceEnabled;
        this.cityId = cityId;
        this.cityName = cityName;
        this.areaId = areaId;
        this.areaName = areaName;
        this.tags = tags;
        this.orderCountsBreakfast = orderCountsBreakfast;
    }

    public List<ItemId> getItemId() {
        return itemId;
    }

    public void setItemId(List<ItemId> itemId) {
        this.itemId = itemId;
    }

    public List<ItemName> getItemName() {
        return itemName;
    }

    public void setItemName(List<ItemName> itemName) {
        this.itemName = itemName;
    }

    public List<ItemPrice> getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(List<ItemPrice> itemPrice) {
        this.itemPrice = itemPrice;
    }

    public List<ItemEnabled> getItemEnabled() {
        return itemEnabled;
    }

    public void setItemEnabled(List<ItemEnabled> itemEnabled) {
        this.itemEnabled = itemEnabled;
    }

    public List<ItemLongDistanceEnabled> getItemLongDistanceEnabled() {
        return itemLongDistanceEnabled;
    }

    public void setItemLongDistanceEnabled(List<ItemLongDistanceEnabled> itemLongDistanceEnabled) {
        this.itemLongDistanceEnabled = itemLongDistanceEnabled;
    }

    public List<RestaurantId> getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(List<RestaurantId> restaurantId) {
        this.restaurantId = restaurantId;
    }

    public List<RestaurantName> getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(List<RestaurantName> restaurantName) {
        this.restaurantName = restaurantName;
    }

    public List<RestaurantLocation> getRestaurantLocation() {
        return restaurantLocation;
    }

    public void setRestaurantLocation(List<RestaurantLocation> restaurantLocation) {
        this.restaurantLocation = restaurantLocation;
    }

    public List<RestaurantEnabled> getRestaurantEnabled() {
        return restaurantEnabled;
    }

    public void setRestaurantEnabled(List<RestaurantEnabled> restaurantEnabled) {
        this.restaurantEnabled = restaurantEnabled;
    }

    public List<RestaurantLongDistanceEnabled> getRestaurantLongDistanceEnabled() {
        return restaurantLongDistanceEnabled;
    }

    public void setRestaurantLongDistanceEnabled(List<RestaurantLongDistanceEnabled> restaurantLongDistanceEnabled) {
        this.restaurantLongDistanceEnabled = restaurantLongDistanceEnabled;
    }

    public List<CityId> getCityId() {
        return cityId;
    }

    public void setCityId(List<CityId> cityId) {
        this.cityId = cityId;
    }

    public List<CityName> getCityName() {
        return cityName;
    }

    public void setCityName(List<CityName> cityName) {
        this.cityName = cityName;
    }

    public List<AreaId> getAreaId() {
        return areaId;
    }

    public void setAreaId(List<AreaId> areaId) {
        this.areaId = areaId;
    }

    public List<AreaName> getAreaName() {
        return areaName;
    }

    public void setAreaName(List<AreaName> areaName) {
        this.areaName = areaName;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<OrderCountsBreakfast> getOrderCountsBreakfast() {
        return orderCountsBreakfast;
    }

    public void setOrderCountsBreakfast(List<OrderCountsBreakfast> orderCountsBreakfast) {
        this.orderCountsBreakfast = orderCountsBreakfast;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("itemId", itemId).append("itemName", itemName).append("itemPrice", itemPrice).append("itemEnabled", itemEnabled).append("itemLongDistanceEnabled", itemLongDistanceEnabled).append("restaurantId", restaurantId).append("restaurantName", restaurantName).append("restaurantLocation", restaurantLocation).append("restaurantEnabled", restaurantEnabled).append("restaurantLongDistanceEnabled", restaurantLongDistanceEnabled).append("cityId", cityId).append("cityName", cityName).append("areaId", areaId).append("areaName", areaName).append("tags", tags).append("orderCountsBreakfast", orderCountsBreakfast).toString();
    }

}
