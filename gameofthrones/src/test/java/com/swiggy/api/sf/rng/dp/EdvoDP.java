package com.swiggy.api.sf.rng.dp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.tomcat.util.buf.StringUtils;
import org.testng.annotations.DataProvider;

import com.mongodb.util.JSON;
import com.swiggy.api.sf.rng.constants.EDVOConstants;
import com.swiggy.api.sf.rng.helper.CreateEDVOOperationMeta;
import com.swiggy.api.sf.rng.helper.CreateReward;
import com.swiggy.api.sf.rng.helper.EDVOHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.edvo.EDVOTimeSlot;

import edu.emory.mathcs.backport.java.util.Arrays;

public class EdvoDP {

	static RngHelper rngHelper = new RngHelper();
	static EDVOHelper edvoHelper = new EDVOHelper();
	static String restId = "";
	static String mealId = "";
	static String secondMealId = "";
	static String groupId = "";
	static EDVOTimeSlot edvoTimeSlot = new EDVOTimeSlot();

	private String mealIdRange;
	
	
	
	
	@DataProvider(name = "createMealTypeEDVOCampData")
	public static Object[][] createMealTypeEDVOCamp() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			
			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(6, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createGroupTypeEDVOCampData")
	public static Object[][] createGroupTypeEDVOCamp() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			// allMealIds.add(1, Integer.toString(Utility.getRandom(100,5000)));
			// allMealIds.add(2, Integer.toString(Utility.getRandom(100,5000)));

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 and get 20% off in each offer",
					" buy 2 and get 20% off in each offer").toString());
			// allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of
			// one (max of 2) offer"," buy 2 at price of one (max of 2) offer").toString());
			// allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of
			// one (max of 2) offer"," buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());
			// allMealRewards.add(1, new
			// CreateReward("NONE","Percentage","10","1").toString());
			// allMealRewards.add(2, new
			// CreateReward("NONE","Percentage","10","1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			// mealCount.add(1, "2");
			// mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			// mealIsParent.add(1,"true");
			// mealIsParent.add(2,"true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);
			// numberOfGroupsInEachMeal.add(1,1 );
			// numberOfGroupsInEachMeal.add(2,3 );

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			// allGroupIdsOfMeals.add(2,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(3,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(4,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(5,Integer.toString(Utility.getRandom(1000,40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			// allGroupCounts.add(2, "1");
			// allGroupCounts.add(3, "2");
			// allGroupCounts.add(4, "1");
			// allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());
			// allGroupsRewards.add(2, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(3, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(4, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(5, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(6, new
			// CreateReward("NONE","Percentage","15","1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automatio\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createMealTypeEDVOCampForMultipleRestData")
	public static Object[][] createMealTypeEDVOCampForMultipleRest() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());
			listOfRestIds.add(1, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");
			operationTypeOfCamp.add(1, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");
			numberOfMealsInARest.add(1, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			// allMealIds.add(1, Integer.toString(Utility.getRandom(100,5000)));
			// allMealIds.add(2, Integer.toString(Utility.getRandom(100,5000)));

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			// allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of
			// one (max of 2) offer"," buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MAX", "Percentage", "100", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());
			// allMealRewards.add(2, new
			// CreateReward("NONE","Percentage","10","1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			// mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			// mealIsParent.add(2,"true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			// numberOfGroupsInEachMeal.add(2,3 );

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			// allGroupIdsOfMeals.add(2,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(3,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(4,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(5,Integer.toString(Utility.getRandom(1000,40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			// allGroupCounts.add(2, "1");
			// allGroupCounts.add(3, "2");
			// allGroupCounts.add(4, "1");
			// allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());
			// allGroupsRewards.add(2, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(3, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(4, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(5, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(6, new
			// CreateReward("NONE","Percentage","15","1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createCampaignWithMealOperationMetaAsNullData")
	public static Object[][] createCampaignWithMealOperationMetaAsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta().toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta().toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta().toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(6, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createCampaignWithOperationTypeAsNullData")
	public static Object[][] createCampaignWithOperationTypeAsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "null");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			// allMealIds.add(1, Integer.toString(Utility.getRandom(100,5000)));

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			// allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of
			// one (max of 2) offer"," buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			// allMealRewards.add(1, new
			// CreateReward("NONE","Percentage","10","1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			// mealCount.add(1, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			// mealIsParent.add(1,"true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);
			// numberOfGroupsInEachMeal.add(1,1 );

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			// allGroupIdsOfMeals.add(2,Integer.toString(Utility.getRandom(1000,40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			// allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			// allGroupsRewards.add(2, new
			// CreateReward("NONE","Percentage","15","1").toString());

			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createCampaignWithOperationTypeAsEmptyStringData")
	public static Object[][] createCampaignWithOperationTypeAsEmptyString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);
			numberOfGroupsInEachMeal.add(1, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createWithRestaurantIdAsNullData")
	public static Object[][] createWithRestaurantIdAsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, null);

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(6, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createOneEDVOCampOnTwoSameRestaurantData")
	public static Object[][] createOneEDVOCampOnTwoSameRestaurant() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String i = "";
			// listOfAllRestaurantIds
			listOfRestIds.add(0, restId = edvoHelper.getRestId());
			listOfRestIds.add(0, restId);

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");
			operationTypeOfCamp.add(1, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");
			numberOfMealsInARest.add(1, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			// allMealIds.add(2, Integer.toString(Utility.getRandom(100,5000)));

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			// allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of
			// one (max of 2) offer"," buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());
			// allMealRewards.add(2, new
			// CreateReward("NONE","Percentage","10","1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			// mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			// mealIsParent.add(2,"true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			// numberOfGroupsInEachMeal.add(2,3 );

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			// allGroupIdsOfMeals.add(2,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(3,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(4,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(5,Integer.toString(Utility.getRandom(1000,40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			// allGroupCounts.add(2, "1");
			// allGroupCounts.add(3, "2");
			// allGroupCounts.add(4, "1");
			// allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			// allGroupsRewards.add(2, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(3, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(4, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(5, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(6, new
			// CreateReward("NONE","Percentage","15","1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createTwoEDVOCampOnSameRestaurantData")
	public static Object[][] createTwoEDVOCampOnSameRestaurant() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String i = "";
			// listOfAllRestaurantIds
			listOfRestIds.add(0, restId = edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			ArrayList<String> secondCamplistOfRestIds = new ArrayList();
			ArrayList<String> secondCampoperationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> secondCampnumberOfMealsInARest = new ArrayList();
			ArrayList<String> secondCampallMealIds = new ArrayList();
			ArrayList<String> secondCampallMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> secondCampallMealRewards = new ArrayList<String>();
			ArrayList<String> secondCampmealCount = new ArrayList<String>();
			ArrayList<String> secondCampmealIsParent = new ArrayList<String>();
			ArrayList<Integer> secondCampnumberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> secondCampallGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> secondCampallGroupCounts = new ArrayList<String>();
			ArrayList<String> secondCampallGroupsRewards = new ArrayList<String>();
			HashMap<String, String> secondCampcreateEDVOCamp = new HashMap<String, String>();
			String day = "ALL";
			int closeTime = 3000;
			int openTime = 7000;

			// listOfAllRestaurantIds
			secondCamplistOfRestIds.add(0, restId);

			// list of operationType of restaurants in campaign
			secondCampoperationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			secondCampnumberOfMealsInARest.add(0, "1");

			// list of All MealIds
			secondCampallMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			secondCampallMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("2nd Camp at price of one (max of 2) offer",
							"2nd camp at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			secondCampallMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			secondCampmealCount.add(0, "1");

			// isParent value of Meals
			secondCampmealIsParent.add(0, "true");

			// list of number of Groups in each meal
			secondCampnumberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			secondCampallGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			secondCampallGroupCounts.add(0, "1");

			// list of all GroupsReward
			secondCampallGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			// secondCampcreateEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			// secondCampcreateEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			// secondCampcreateEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			// secondCampcreateEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			// secondCampcreateEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			// secondCampcreateEDVOCamp.put("5",
			// Utility.getFuturetimeInMilliSeconds(86400000));
			// secondCampcreateEDVOCamp.put("6", "\"BXGY\"");
			// secondCampcreateEDVOCamp.put("7", "100");
			// secondCampcreateEDVOCamp.put("8", "true");
			// secondCampcreateEDVOCamp.put("9", "0");
			// secondCampcreateEDVOCamp.put("10", "\"AY\"");
			// secondCampcreateEDVOCamp.put("11", "120");
			// secondCampcreateEDVOCamp.put("12", "0");
			// secondCampcreateEDVOCamp.put("13",
			// edvoHelper.discountsCreate(secondCamplistOfRestIds,
			// secondCampoperationTypeOfCamp, secondCampnumberOfMealsInARest,
			// secondCampallMealIds,
			// secondCampallMealsOperationMeta, secondCampallMealRewards,
			// secondCampmealCount,secondCampmealIsParent,
			// secondCampnumberOfGroupsInEachMeal,
			// secondCampallGroupCounts, secondCampallGroupIdsOfMeals,
			// secondCampallGroupsRewards));
			// secondCampcreateEDVOCamp.put("14", "null");
			// secondCampcreateEDVOCamp.put("15", "false");
			// secondCampcreateEDVOCamp.put("16", "false");
			// secondCampcreateEDVOCamp.put("17", "false");
			// secondCampcreateEDVOCamp.put("18", "false");
			// secondCampcreateEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			// secondCampcreateEDVOCamp.put("20", "false");
			// secondCampcreateEDVOCamp.put("21", "false");
			//
			String secondCampDiscount = edvoHelper.discountsCreate(secondCamplistOfRestIds,
					secondCampoperationTypeOfCamp, secondCampnumberOfMealsInARest, secondCampallMealIds,
					secondCampallMealsOperationMeta, secondCampallMealRewards, secondCampmealCount,
					secondCampmealIsParent, secondCampnumberOfGroupsInEachMeal, secondCampallGroupCounts,
					secondCampallGroupIdsOfMeals, secondCampallGroupsRewards);
			secondCampcreateEDVOCamp = edvoHelper.createEDVOWithDefaultValues(secondCampDiscount, day, openTime,
					closeTime, EDVOConstants.campValidTillTime);

			return new Object[][] { { createEDVOCamp, secondCampcreateEDVOCamp } };

		}
	}

	@DataProvider(name = "createOneEDVOCampOnTwoSameRestaurantAndSameMealData")
	public static Object[][] createOneEDVOCampOnTwoSameRestaurantAndSameMeal() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String i = "";
			// listOfAllRestaurantIds
			listOfRestIds.add(0, restId = edvoHelper.getRestId());
			listOfRestIds.add(0, restId);

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");
			operationTypeOfCamp.add(1, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");
			numberOfMealsInARest.add(1, "1");

			// list of All MealIds
			allMealIds.add(0, mealId = edvoHelper.getMealId());
			allMealIds.add(1, mealId);
			// allMealIds.add(2, Integer.toString(Utility.getRandom(100,5000)));

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy test at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			// allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of
			// one (max of 2) offer"," buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			// mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			// mealIsParent.add(2,"true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			// numberOfGroupsInEachMeal.add(2,3 );

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			// allGroupIdsOfMeals.add(2,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(3,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(4,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(5,Integer.toString(Utility.getRandom(1000,40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			// allGroupCounts.add(2, "1");
			// allGroupCounts.add(3, "2");
			// allGroupCounts.add(4, "1");
			// allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("MAX", "Percentage", "30", "1").toString());
			// allGroupsRewards.add(2, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(3, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(4, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(5, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(6, new
			// CreateReward("NONE","Percentage","15","1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createOneEDVOCampOnTwoDifferentRestaurantButSameMealData")
	public static Object[][] createOneEDVOCampOnTwoDifferentRestaurantButSameMeal() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String i = "";
			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");
			operationTypeOfCamp.add(1, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");
			numberOfMealsInARest.add(1, "1");

			// list of All MealIds
			allMealIds.add(0, mealId = edvoHelper.getMealId());
			allMealIds.add(1, mealId);
			// allMealIds.add(2, Integer.toString(Utility.getRandom(100,5000)));

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("test test at price of one (max of 2) offer",
					" test test at price of one (max of 2) offer").toString());
			// allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of
			// one (max of 2) offer"," buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			// mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			// mealIsParent.add(2,"true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			// numberOfGroupsInEachMeal.add(2,3 );

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			// allGroupIdsOfMeals.add(2,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(3,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(4,Integer.toString(Utility.getRandom(1000,40000)));
			// allGroupIdsOfMeals.add(5,Integer.toString(Utility.getRandom(1000,40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			// allGroupCounts.add(2, "1");
			// allGroupCounts.add(3, "2");
			// allGroupCounts.add(4, "1");
			// allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("MAX", "Percentage", "30", "1").toString());
			// allGroupsRewards.add(2, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(3, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(4, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(5, new
			// CreateReward("NONE","Percentage","15","1").toString());
			// allGroupsRewards.add(6, new
			// CreateReward("NONE","Percentage","15","1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createTwoEDVOCampOnDifferentRestaurantButSameMealData")
	public static Object[][] createTwoEDVOCampOnDifferentRestaurantButSameMeal() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String i = "";
			// listOfAllRestaurantIds
			listOfRestIds.add(0, Integer.toString(Utility.getRandom(10000, 90000)));

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, mealId = edvoHelper.getRestId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			ArrayList<String> secondCamplistOfRestIds = new ArrayList();
			ArrayList<String> secondCampoperationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> secondCampnumberOfMealsInARest = new ArrayList();
			ArrayList<String> secondCampallMealIds = new ArrayList();
			ArrayList<String> secondCampallMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> secondCampallMealRewards = new ArrayList<String>();
			ArrayList<String> secondCampmealCount = new ArrayList<String>();
			ArrayList<String> secondCampmealIsParent = new ArrayList<String>();
			ArrayList<Integer> secondCampnumberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> secondCampallGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> secondCampallGroupCounts = new ArrayList<String>();
			ArrayList<String> secondCampallGroupsRewards = new ArrayList<String>();
			HashMap<String, String> secondCampcreateEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			secondCamplistOfRestIds.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of operationType of restaurants in campaign
			secondCampoperationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			secondCampnumberOfMealsInARest.add(0, "1");

			// list of All MealIds
			secondCampallMealIds.add(0, mealId);

			// list of all Meals OperationMeta
			secondCampallMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(" 2nd camp at price of one (max of 2) offer",
							"2nd camp at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			secondCampallMealRewards.add(0, new CreateReward("MIN", "Percentage", "30", "1").toString());

			// list of number of "count" in a meal
			secondCampmealCount.add(0, "1");

			// isParent value of Meals
			secondCampmealIsParent.add(0, "true");

			// list of number of Groups in each meal
			secondCampnumberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			secondCampallGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			secondCampallGroupCounts.add(0, "1");

			// list of all GroupsReward
			secondCampallGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			secondCampcreateEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			secondCampcreateEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			secondCampcreateEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			secondCampcreateEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			secondCampcreateEDVOCamp.put("6", "\"BXGY\"");
			secondCampcreateEDVOCamp.put("7", "100");
			secondCampcreateEDVOCamp.put("8", "true");
			secondCampcreateEDVOCamp.put("9", "0");
			secondCampcreateEDVOCamp.put("10", "\"AY\"");
			secondCampcreateEDVOCamp.put("11", "120");
			secondCampcreateEDVOCamp.put("12", "0");
			secondCampcreateEDVOCamp.put("13",
					edvoHelper.discountsCreate(secondCamplistOfRestIds, secondCampoperationTypeOfCamp,
							secondCampnumberOfMealsInARest, secondCampallMealIds, secondCampallMealsOperationMeta,
							secondCampallMealRewards, secondCampmealCount, secondCampmealIsParent,
							secondCampnumberOfGroupsInEachMeal, secondCampallGroupCounts, secondCampallGroupIdsOfMeals,
							secondCampallGroupsRewards));
			secondCampcreateEDVOCamp.put("14", "null");
			secondCampcreateEDVOCamp.put("15", "false");
			secondCampcreateEDVOCamp.put("16", "false");
			secondCampcreateEDVOCamp.put("17", "false");
			secondCampcreateEDVOCamp.put("18", "false");
			secondCampcreateEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			secondCampcreateEDVOCamp.put("20", "false");
			secondCampcreateEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp, secondCampcreateEDVOCamp } };

		}
	}

	@DataProvider(name = "createThreeEDVOCampOnDifferentRestaurantMealIdIsSameInOneCampData")
	public static Object[][] createThreeEDVOCampOnDifferentRestaurantMealIdIsSameInOneCamp() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String i = "";
			// listOfAllRestaurantIds
			listOfRestIds.add(0, Integer.toString(Utility.getRandom(10000, 90000)));

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, mealId = edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			ArrayList<String> secondCamplistOfRestIds = new ArrayList();
			ArrayList<String> secondCampoperationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> secondCampnumberOfMealsInARest = new ArrayList();
			ArrayList<String> secondCampallMealIds = new ArrayList();
			ArrayList<String> secondCampallMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> secondCampallMealRewards = new ArrayList<String>();
			ArrayList<String> secondCampmealCount = new ArrayList<String>();
			ArrayList<String> secondCampmealIsParent = new ArrayList<String>();
			ArrayList<Integer> secondCampnumberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> secondCampallGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> secondCampallGroupCounts = new ArrayList<String>();
			ArrayList<String> secondCampallGroupsRewards = new ArrayList<String>();
			HashMap<String, String> secondCampcreateEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			secondCamplistOfRestIds.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of operationType of restaurants in campaign
			secondCampoperationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			secondCampnumberOfMealsInARest.add(0, "1");

			// list of All MealIds
			secondCampallMealIds.add(0, secondMealId = edvoHelper.getMealId());

			// list of all Meals OperationMeta
			secondCampallMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(" 2nd camp at price of one (max of 2) offer",
							"2nd camp at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			secondCampallMealRewards.add(0, new CreateReward("MIN", "Percentage", "30", "1").toString());

			// list of number of "count" in a meal
			secondCampmealCount.add(0, "1");

			// isParent value of Meals
			secondCampmealIsParent.add(0, "true");

			// list of number of Groups in each meal
			secondCampnumberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			secondCampallGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			secondCampallGroupCounts.add(0, "1");

			// list of all GroupsReward
			secondCampallGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			secondCampcreateEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			secondCampcreateEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			secondCampcreateEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			secondCampcreateEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			secondCampcreateEDVOCamp.put("6", "\"BXGY\"");
			secondCampcreateEDVOCamp.put("7", "100");
			secondCampcreateEDVOCamp.put("8", "true");
			secondCampcreateEDVOCamp.put("9", "0");
			secondCampcreateEDVOCamp.put("10", "\"AY\"");
			secondCampcreateEDVOCamp.put("11", "120");
			secondCampcreateEDVOCamp.put("12", "0");
			secondCampcreateEDVOCamp.put("13",
					edvoHelper.discountsCreate(secondCamplistOfRestIds, secondCampoperationTypeOfCamp,
							secondCampnumberOfMealsInARest, secondCampallMealIds, secondCampallMealsOperationMeta,
							secondCampallMealRewards, secondCampmealCount, secondCampmealIsParent,
							secondCampnumberOfGroupsInEachMeal, secondCampallGroupCounts, secondCampallGroupIdsOfMeals,
							secondCampallGroupsRewards));
			secondCampcreateEDVOCamp.put("14", "null");
			secondCampcreateEDVOCamp.put("15", "false");
			secondCampcreateEDVOCamp.put("16", "false");
			secondCampcreateEDVOCamp.put("17", "false");
			secondCampcreateEDVOCamp.put("18", "false");
			secondCampcreateEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			secondCampcreateEDVOCamp.put("20", "false");
			secondCampcreateEDVOCamp.put("21", "false");

			ArrayList<String> thirdCamplistOfRestIds = new ArrayList();
			ArrayList<String> thirdCampoperationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> thirdCampnumberOfMealsInARest = new ArrayList();
			ArrayList<String> thirdCampallMealIds = new ArrayList();
			ArrayList<String> thirdCampallMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> thirdCampallMealRewards = new ArrayList<String>();
			ArrayList<String> thirdCampmealCount = new ArrayList<String>();
			ArrayList<String> thirdCampmealIsParent = new ArrayList<String>();
			ArrayList<Integer> thirdCampnumberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> thirdCampallGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> thirdCampallGroupCounts = new ArrayList<String>();
			ArrayList<String> thirdCampallGroupsRewards = new ArrayList<String>();
			HashMap<String, String> thirdCampcreateEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			thirdCamplistOfRestIds.add(0, Integer.toString(Utility.getRandom(10000, 40000)));

			// list of operationType of restaurants in campaign
			thirdCampoperationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			thirdCampnumberOfMealsInARest.add(0, "3");

			// list of All MealIds
			thirdCampallMealIds.add(0, edvoHelper.getMealId());
			thirdCampallMealIds.add(0, mealId);
			thirdCampallMealIds.add(0, secondMealId);

			// list of all Meals OperationMeta
			thirdCampallMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(" 2nd camp at price of one (max of 2) offer",
							"2nd camp at price of one (max of 2) offer").toString());
			thirdCampallMealsOperationMeta.add(1,
					new CreateEDVOOperationMeta(" 2nd camp at price of one (max of 2) offer",
							"2nd camp at price of one (max of 2) offer").toString());
			thirdCampallMealsOperationMeta.add(2,
					new CreateEDVOOperationMeta(" 2nd camp at price of one (max of 2) offer",
							"2nd camp at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			thirdCampallMealRewards.add(0, new CreateReward().toString());
			thirdCampallMealRewards.add(0, new CreateReward().toString());
			thirdCampallMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			thirdCampmealCount.add(0, "1");
			thirdCampmealCount.add(1, "1");
			thirdCampmealCount.add(2, "1");

			// isParent value of Meals
			thirdCampmealIsParent.add(0, "true");
			thirdCampmealIsParent.add(1, "true");
			thirdCampmealIsParent.add(2, "true");

			// list of number of Groups in each meal
			thirdCampnumberOfGroupsInEachMeal.add(0, 1);
			thirdCampnumberOfGroupsInEachMeal.add(1, 1);
			thirdCampnumberOfGroupsInEachMeal.add(2, 1);

			// list of groupIds in Meal
			thirdCampallGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			thirdCampallGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			thirdCampallGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			thirdCampallGroupCounts.add(0, "1");
			thirdCampallGroupCounts.add(1, "1");
			thirdCampallGroupCounts.add(2, "1");

			// list of all GroupsReward
			thirdCampallGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			thirdCampallGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "20", "1").toString());
			thirdCampallGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			thirdCampcreateEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			thirdCampcreateEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			thirdCampcreateEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			thirdCampcreateEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			thirdCampcreateEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			thirdCampcreateEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			thirdCampcreateEDVOCamp.put("6", "\"BXGY\"");
			thirdCampcreateEDVOCamp.put("7", "100");
			thirdCampcreateEDVOCamp.put("8", "true");
			thirdCampcreateEDVOCamp.put("9", "0");
			thirdCampcreateEDVOCamp.put("10", "\"AY\"");
			thirdCampcreateEDVOCamp.put("11", "120");
			thirdCampcreateEDVOCamp.put("12", "0");
			thirdCampcreateEDVOCamp.put("13",
					edvoHelper.discountsCreate(thirdCamplistOfRestIds, thirdCampoperationTypeOfCamp,
							thirdCampnumberOfMealsInARest, thirdCampallMealIds, thirdCampallMealsOperationMeta,
							thirdCampallMealRewards, thirdCampmealCount, thirdCampmealIsParent,
							thirdCampnumberOfGroupsInEachMeal, thirdCampallGroupCounts, thirdCampallGroupIdsOfMeals,
							thirdCampallGroupsRewards));
			thirdCampcreateEDVOCamp.put("14", "null");
			thirdCampcreateEDVOCamp.put("15", "false");
			thirdCampcreateEDVOCamp.put("16", "false");
			thirdCampcreateEDVOCamp.put("17", "false");
			thirdCampcreateEDVOCamp.put("18", "false");
			thirdCampcreateEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			thirdCampcreateEDVOCamp.put("20", "false");
			thirdCampcreateEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp, secondCampcreateEDVOCamp, thirdCampcreateEDVOCamp } };

		}
	}

	@DataProvider(name = "createTwoEDVOCampOnDifferentRestaurantButSameGroupsData")
	public static Object[][] createTwoEDVOCampOnDifferentRestaurantButSameGroups() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String i = "";
			// listOfAllRestaurantIds
			listOfRestIds.add(0, Integer.toString(Utility.getRandom(10000, 90000)));

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			ArrayList<String> secondCamplistOfRestIds = new ArrayList();
			ArrayList<String> secondCampoperationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> secondCampnumberOfMealsInARest = new ArrayList();
			ArrayList<String> secondCampallMealIds = new ArrayList();
			ArrayList<String> secondCampallMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> secondCampallMealRewards = new ArrayList<String>();
			ArrayList<String> secondCampmealCount = new ArrayList<String>();
			ArrayList<String> secondCampmealIsParent = new ArrayList<String>();
			ArrayList<Integer> secondCampnumberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> secondCampallGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> secondCampallGroupCounts = new ArrayList<String>();
			ArrayList<String> secondCampallGroupsRewards = new ArrayList<String>();
			HashMap<String, String> secondCampcreateEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			secondCamplistOfRestIds.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of operationType of restaurants in campaign
			secondCampoperationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			secondCampnumberOfMealsInARest.add(0, "1");

			// list of All MealIds
			secondCampallMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			secondCampallMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(" 2nd camp at price of one (max of 2) offer",
							"2nd camp at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			secondCampallMealRewards.add(0, new CreateReward("MIN", "Percentage", "30", "1").toString());

			// list of number of "count" in a meal
			secondCampmealCount.add(0, "1");

			// isParent value of Meals
			secondCampmealIsParent.add(0, "true");

			// list of number of Groups in each meal
			secondCampnumberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			secondCampallGroupIdsOfMeals.add(0, groupId);

			// list of count in a group
			secondCampallGroupCounts.add(0, "1");

			// list of all GroupsReward
			secondCampallGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			secondCampcreateEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			secondCampcreateEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			secondCampcreateEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			secondCampcreateEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			secondCampcreateEDVOCamp.put("6", "\"BXGY\"");
			secondCampcreateEDVOCamp.put("7", "100");
			secondCampcreateEDVOCamp.put("8", "true");
			secondCampcreateEDVOCamp.put("9", "0");
			secondCampcreateEDVOCamp.put("10", "\"AY\"");
			secondCampcreateEDVOCamp.put("11", "120");
			secondCampcreateEDVOCamp.put("12", "0");
			secondCampcreateEDVOCamp.put("13",
					edvoHelper.discountsCreate(secondCamplistOfRestIds, secondCampoperationTypeOfCamp,
							secondCampnumberOfMealsInARest, secondCampallMealIds, secondCampallMealsOperationMeta,
							secondCampallMealRewards, secondCampmealCount, secondCampmealIsParent,
							secondCampnumberOfGroupsInEachMeal, secondCampallGroupCounts, secondCampallGroupIdsOfMeals,
							secondCampallGroupsRewards));
			secondCampcreateEDVOCamp.put("14", "null");
			secondCampcreateEDVOCamp.put("15", "false");
			secondCampcreateEDVOCamp.put("16", "false");
			secondCampcreateEDVOCamp.put("17", "false");
			secondCampcreateEDVOCamp.put("18", "false");
			secondCampcreateEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			secondCampcreateEDVOCamp.put("20", "false");
			secondCampcreateEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp, secondCampcreateEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithDifferentTypeOfRewardsOnMealsData")
	public static Object[][] createEDVOCampWithDifferentTypeOfRewardsOnMeals() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy 2  and get 15% off offer", " buy 2  and get 15% off offer")
							.toString());
			allMealsOperationMeta.add(1,
					new CreateEDVOOperationMeta("buy 2 and get 50 off offer", " buy 2 and get 50 off offer")
							.toString());
			allMealsOperationMeta.add(2,
					new CreateEDVOOperationMeta("buy 2 at Final price 350 offer", "buy 2 at Final price 350 offer")
							.toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Flat", "50", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "FinalPrice", "350", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);
			numberOfGroupsInEachMeal.add(1, 2);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "1");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward().toString());
			allGroupsRewards.add(4, new CreateReward().toString());
			allGroupsRewards.add(5, new CreateReward().toString());
			allGroupsRewards.add(6, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithMealIdAsNullData")
	public static Object[][] createEDVOCampWithMealIdAsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, null);
			allMealIds.add(1, null);
			allMealIds.add(2, null);

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy 2  and get 15% off offer", " buy 2  and get 15% off offer")
							.toString());
			allMealsOperationMeta.add(1,
					new CreateEDVOOperationMeta("buy 2 and get 50 off offer", " buy 2 and get 50 off offer")
							.toString());
			allMealsOperationMeta.add(2,
					new CreateEDVOOperationMeta("buy 2 at Final price 350 offer", "buy 2 at Final price 350 offer")
							.toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Flat", "50", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "FinalPrice", "350", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);
			numberOfGroupsInEachMeal.add(1, 2);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "1");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward().toString());
			allGroupsRewards.add(4, new CreateReward().toString());
			allGroupsRewards.add(5, new CreateReward().toString());
			allGroupsRewards.add(6, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createOneCampWithTwoRestaurantAndOperationTypeIsRestaurantAndMealData")
	public static Object[][] createOneCampWithTwoRestaurantAndOperationTypeIsRestaurantAndMeal() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());
			listOfRestIds.add(1, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");
			operationTypeOfCamp.add(1, "\"RESTAURANT\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");
			numberOfMealsInARest.add(1, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createOneCampWithOneRestaurantAndOperationTypeIsRestaurantData")
	public static Object[][] createOneCampWithOneRestaurantAndOperationTypeIsRestaurant() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"RESTAURANT\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createTwoEDVOCampSameRestaurantIdButOneCampOperationTypeIsRestaurantData")
	public static Object[][] createTwoEDVOCampSameRestaurantIdButOneCampOperationTypeIsRestaurant() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, restId = edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			ArrayList<String> secondCamplistOfRestIds = new ArrayList();
			ArrayList<String> secondCampoperationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> secondCampnumberOfMealsInARest = new ArrayList();
			ArrayList<String> secondCampallMealIds = new ArrayList();
			ArrayList<String> secondCampallMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> secondCampallMealRewards = new ArrayList<String>();
			ArrayList<String> secondCampmealCount = new ArrayList<String>();
			ArrayList<String> secondCampmealIsParent = new ArrayList<String>();
			ArrayList<Integer> secondCampnumberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> secondCampallGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> secondCampallGroupCounts = new ArrayList<String>();
			ArrayList<String> secondCampallGroupsRewards = new ArrayList<String>();
			HashMap<String, String> secondCampcreateEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds

			secondCamplistOfRestIds.add(0, restId);

			// list of operationType of restaurants in campaign

			secondCampoperationTypeOfCamp.add(0, "\"RESTAURANT\"");

			// list of number of meals in restaurants
			secondCampnumberOfMealsInARest.add(0, "1");

			// list of All MealIds
			secondCampallMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			secondCampallMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			secondCampallMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			secondCampmealCount.add(0, "1");

			// isParent value of Meals
			secondCampmealIsParent.add(0, "true");

			// list of number of Groups in each meal
			secondCampnumberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			secondCampallGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			secondCampallGroupCounts.add(0, "1");

			// list of all GroupsReward
			secondCampallGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			secondCampcreateEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			secondCampcreateEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			secondCampcreateEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			secondCampcreateEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			secondCampcreateEDVOCamp.put("6", "\"BXGY\"");
			secondCampcreateEDVOCamp.put("7", "100");
			secondCampcreateEDVOCamp.put("8", "true");
			secondCampcreateEDVOCamp.put("9", "0");
			secondCampcreateEDVOCamp.put("10", "\"AY\"");
			secondCampcreateEDVOCamp.put("11", "120");
			secondCampcreateEDVOCamp.put("12", "0");
			secondCampcreateEDVOCamp.put("13",
					edvoHelper.discountsCreate(secondCamplistOfRestIds, secondCampoperationTypeOfCamp,
							secondCampnumberOfMealsInARest, secondCampallMealIds, secondCampallMealsOperationMeta,
							secondCampallMealRewards, secondCampmealCount, secondCampmealIsParent,
							secondCampnumberOfGroupsInEachMeal, secondCampallGroupCounts, secondCampallGroupIdsOfMeals,
							secondCampallGroupsRewards));
			secondCampcreateEDVOCamp.put("14", "null");
			secondCampcreateEDVOCamp.put("15", "false");
			secondCampcreateEDVOCamp.put("16", "false");
			secondCampcreateEDVOCamp.put("17", "false");
			secondCampcreateEDVOCamp.put("18", "false");
			secondCampcreateEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			secondCampcreateEDVOCamp.put("20", "false");
			secondCampcreateEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp, secondCampcreateEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithMealAndGroupRewardBothAreNullTestData")
	public static Object[][] createEDVOCampWithMealAndGroupRewardBothAreNullTest() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "creatEDVOCampWithGroupsIsNullData")
	public static Object[][] creatEDVOCampWithGroupsIsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, null);

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereInAMealAllGroupIdsAreSameData")
	public static Object[][] createEDVOCampWhereInAMealAllGroupIdsAreSame() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, groupId);
			allGroupIdsOfMeals.add(2, groupId);

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereTwoDifferentMealsHaveSameGroupIdsData")
	public static Object[][] createEDVOCampWhereTwoDifferentMealsHaveSameGroupIds() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(6, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOOneCampWhereTwoMealsAreHavingSameGroupIdsData")
	public static Object[][] createEDVOOneCampWhereTwoMealsAreHavingSameGroupIds() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "2");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Flat", "100", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, groupId);

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOOneCampWhereTwoRestaurantsMealsAreHavingSameGroupIdsData")
	public static Object[][] createEDVOOneCampWhereTwoRestaurantsMealsAreHavingSameGroupIdsData() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());
			listOfRestIds.add(1, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");
			operationTypeOfCamp.add(1, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");
			numberOfMealsInARest.add(1, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Flat", "100", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, groupId);

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOTwoCampWhereTheirMealsAreHavingSameGroupIdsData")
	public static Object[][] createEDVOTwoCampWhereTheirMealsAreHavingSameGroupIds() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			ArrayList<String> secondCamplistOfRestIds = new ArrayList();
			ArrayList<String> secondCampoperationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> secondCampnumberOfMealsInARest = new ArrayList();
			ArrayList<String> secondCampallMealIds = new ArrayList();
			ArrayList<String> secondCampallMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> secondCampallMealRewards = new ArrayList<String>();
			ArrayList<String> secondCampmealCount = new ArrayList<String>();
			ArrayList<String> secondCampmealIsParent = new ArrayList<String>();
			ArrayList<Integer> secondCampnumberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> secondCampallGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> secondCampallGroupCounts = new ArrayList<String>();
			ArrayList<String> secondCampallGroupsRewards = new ArrayList<String>();
			HashMap<String, String> secondCampcreateEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			secondCamplistOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			secondCampoperationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			secondCampnumberOfMealsInARest.add(0, "1");

			// list of All MealIds
			secondCampallMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			secondCampallMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			secondCampallMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			secondCampmealCount.add(0, "1");

			// isParent value of Meals
			secondCampmealIsParent.add(0, "true");

			// list of number of Groups in each meal
			secondCampnumberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			secondCampallGroupIdsOfMeals.add(0, groupId);

			// list of count in a group
			secondCampallGroupCounts.add(0, "1");

			// list of all GroupsReward
			secondCampallGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			secondCampcreateEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			secondCampcreateEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			secondCampcreateEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			secondCampcreateEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			secondCampcreateEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			secondCampcreateEDVOCamp.put("6", "\"BXGY\"");
			secondCampcreateEDVOCamp.put("7", "100");
			secondCampcreateEDVOCamp.put("8", "true");
			secondCampcreateEDVOCamp.put("9", "0");
			secondCampcreateEDVOCamp.put("10", "\"AY\"");
			secondCampcreateEDVOCamp.put("11", "120");
			secondCampcreateEDVOCamp.put("12", "0");
			secondCampcreateEDVOCamp.put("13",
					edvoHelper.discountsCreate(secondCamplistOfRestIds, secondCampoperationTypeOfCamp,
							secondCampnumberOfMealsInARest, secondCampallMealIds, secondCampallMealsOperationMeta,
							secondCampallMealRewards, secondCampmealCount, secondCampmealIsParent,
							secondCampnumberOfGroupsInEachMeal, secondCampallGroupCounts, secondCampallGroupIdsOfMeals,
							secondCampallGroupsRewards));
			secondCampcreateEDVOCamp.put("14", "null");
			secondCampcreateEDVOCamp.put("15", "false");
			secondCampcreateEDVOCamp.put("16", "false");
			secondCampcreateEDVOCamp.put("17", "false");
			secondCampcreateEDVOCamp.put("18", "false");
			secondCampcreateEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			secondCampcreateEDVOCamp.put("20", "false");
			secondCampcreateEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp, secondCampcreateEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardFunctionAsMAXData")
	public static Object[][] createEDVOCampRewardFunctionAsMAX() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MAX", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("MAX", "Percentage", "10", "1").toString());
			allMealRewards.add(2, new CreateReward("MAX", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardFunctionAsMINData")
	public static Object[][] createEDVOCampRewardFunctionAsMIN() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MIN", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("MIN", "Percentage", "10", "1").toString());
			allMealRewards.add(2, new CreateReward("MIN", "Percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardFunctionAsNONEData")
	public static Object[][] createEDVOCampRewardFunctionAsNONE() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "12", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "13", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardFunctionAsEmptyStringData")
	public static Object[][] createEDVOCampRewardFunctionAsEmptyString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("", "Percentage", "12", "1").toString());
			allMealRewards.add(2, new CreateReward("", "Percentage", "13", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardTypeFlatData")
	public static Object[][] createEDVOCampRewardTypeFlat() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Flat", "100", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Flat", "120", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Flat", "130", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardTypePercentageData")
	public static Object[][] createEDVOCampRewardTypePercentage() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "12", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "13", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardTypeFinalPriceData")
	public static Object[][] createEDVOCampRewardTypeFinalPriceData() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "100", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "FinalPrice", "200", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "FinalPrice", "300", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardTypeEmptyStringData")
	public static Object[][] createEDVOCampRewardTypeEmptyString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "", "100", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "", "200", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "", "300", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardTypeFREE_DELIVERYData")
	public static Object[][] createEDVOCampRewardTypeFREE_DELIVERY() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FREE_DELIVERY", "100", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "FREE_DELIVERY", "200", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "FREE_DELIVERY", "300", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampRewardTypeFreebieData")
	public static Object[][] createEDVOCampRewardTypeFreebie() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Freebie", "100", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Freebie", "200", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Freebie", "300", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 3);
			numberOfGroupsInEachMeal.add(2, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(4, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "2");
			allGroupCounts.add(4, "1");
			allGroupCounts.add(5, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());
			allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
			allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "17", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithInvalidRewardTypePercentageData")
	public static Object[][] createEDVOCampWithInvalidRewardTypePercentage() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "percentage", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithInvalidRewardTypeFlatData")
	public static Object[][] createEDVOCampWithInvalidRewardTypeFlat() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "flat", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithInvalidRewardTypeFinalPriceData")
	public static Object[][] createEDVOCampWithInvalidRewardTypeFinalPrice() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Finalprice", "10", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithRewardValueData")
	public static Object[][] createEDVOCampWithRewardValue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Flat", "100", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "FinalPrice", "200", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithRewardValueAsZeroData")
	public static Object[][] createEDVOCampWithRewardValueAsZero() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "0", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Flat", "0", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "FinalPrice", "0", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithRewardValueAsNullData")
	public static Object[][] createEDVOCampWithRewardValueAsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "null", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Flat", "null", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "FinalPrice", "null", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithRewardValueGreaterThanHunderedPercentData")
	public static Object[][] createEDVOCampWithRewardValueGreaterThanHunderedPercent() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "101", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "120", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "110", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithRewardValueAsHunderedPercentData")
	public static Object[][] createEDVOCampWithRewardValueAsHunderedPercent() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "100", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "100", "1").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "100", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithRewardCountAsOneAndGreaterThanOneData")
	public static Object[][] createEDVOCampWithRewardCountAsOneAndGreaterThanOne() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "100", "1").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "100", "2").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "100", "5").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithMealRewardCountAsNullData")
	public static Object[][] createEDVOCampWithMealRewardCountAsNullData() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "3");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer",
					" buy 2 at price of one (max of 2) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "100", "null").toString());
			allMealRewards.add(1, new CreateReward("NONE", "Percentage", "100", "null").toString());
			allMealRewards.add(2, new CreateReward("NONE", "Percentage", "100", "null").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "2");
			mealCount.add(2, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetG2AtPercentOFFData")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetG2AtPercentOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1,G2 and get 20% OFF in G2 price offer",
					" buy G1,G2 and get 20% OFF in G2 price offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "30", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetG2AtFlatOFFData")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetG2AtFlatOFFData() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1, G2 and get Flat 50 OFF in G2 offer",
					"buy G1, G2 and get Flat 50 OFF in G2 offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Flat", "50", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetG2Data")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetG2() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 offer", "buy G1 ,G2 and get G2 offer")
							.toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "100", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetG2AtFinalPriceData")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetG2AtFinalPrice() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 at FINAL Price 99 offer",
					"buy G1, G2 and get G2 at FINAL Price 99 offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetMinPriceFreeInG2Data")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetMinPriceFreeInG2() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 min price FREE (100 Percent OFF) offer",
							"buy G1 ,G2 and get G2 min price FREE (100 Percent OFF) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("MIN", "Percentage", "100", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetMinPriceAtFINALPriceInG2Data")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetMinPriceAtFINALPriceInG2() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 min price at FINAL Price offer",
							"buy G1 ,G2 and get G2 min price at FINAL Price offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("MIN", "FinalPrice", "55", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetMinPriceAtPercentageOFFInG2Data")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetMinPriceAtPercentageOFFInG2() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 min price at 35% OFF offer",
					"buy G1 ,G2 and get G2 min price at 35% OFF offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("MIN", "Percentage", "35", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetMinPriceFlatOFFInG2Data")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetMinPriceFlatOFFInG2Test() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 min price at FLAT 100 OFF offer",
							"buy G1 ,G2 and get G2 min price at FLAT 100 OFF offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("MIN", "Flat", "100", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetMaxPriceFatOFFInG2Data")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetMaxPriceFatOFFInG2() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 max price.. at FLAT 100 OFF offer",
							"buy G1 ,G2 and get G2 max price.. at FLAT 100 OFF offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("MAX", "Flat", "100", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetMaxPricePercentageOFFInG2Data")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetMaxPricePercentageOFFInG2() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 max price.. 35%  OFF offer",
					"buy G1 ,G2 and get G2 max price.. 35%  OFF offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("MAX", "Percentage", "35", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetMaxPriceFreeInG2Data")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetMaxPriceFreeInG2() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 max price.. FREE (100% OFF)  OFF offer",
							"buy G1 ,G2 and get G2 max price.. FREE (100% OFF)  OFF offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("MAX", "Percentage", "100", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1AndG2AndGetMaxPriceAtFinalPriceInG2Data")
	public static Object[][] createEDVOCampWhereBuyG1AndG2AndGetMaxPriceAtFinalPriceInG2() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 and get G2 max price.. at FinalPrice (55) offer",
							"buy G1 ,G2 and get G2 max price.. at FinalPrice (55) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 2);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("MAX", "FinalPrice", "55", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}
	
	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3AtFinalPriceData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3AtFinalPriceData() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 ,G3 and get G3 at FinalPrice (55) offer",
							"buy G1 ,G2 and G3 and get G3 at FinalPrice (55) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "FinalPrice", "55", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3FREEData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3FREE() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 at Free (100 %) offer",
					"buy G1 ,G2 , G3 and get G3 at Free (100 %) offer").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "100", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3PercentageOFFData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3PercentageOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 at 15% OFF ",
					"buy G1 ,G2 , G3 and get G3 at 15% OFF  ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3FlatOFFData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3FlatOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 Flat 75 OFF ",
					"buy G1 ,G2 , G3 and get G3 Flat 75 OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Flat", "75", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceAtFinalPriceData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceAtFinalPrice() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 min price... at FinalPrice 45 ",
							"buy G1 ,G2 , G3 and get G3 min price... at FinalPrice 45 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("MIN", "FinalPrice", "45", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}
	
	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceFREEData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceFREE() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 min price... 30% OFF ",
					"buy G1 ,G2 , G3 and get G3 min price... 30% OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("MIN", "Percentage", "100", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3MinPricePercentageOFFData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3MinPricePercentageOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 min price... 30% OFF ",
					"buy G1 ,G2 , G3 and get G3 min price... 30% OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("MIN", "Percentage", "30", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceFlatOFFData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceFlatOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 min price... Flat 75 OFF ",
							"buy G1 ,G2 , G3 and get G3 min price... Flat 75 OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("MIN", "Flat", "75", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceAtFinalPriceData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceAtFinalPrice() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 max price... At FinalPrice 65 ",
							"buy G1 ,G2 , G3 and get G3 max price... At FinalPrice 65 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("MAX", "FinalPrice", "65", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceFREEData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceFREE() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 max price... FREE (100% OFF) ",
							"buy G1 ,G2 , G3 and get G3 max price... FREE (100% OFF) ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("MAX", "Percentage", "100", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3MaxPricePercentageOFFData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3MaxPricePercentageOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 max price... 15% OFF ",
					"buy G1 ,G2 , G3 and get G3 max price... 15% OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("MAX", "Percentage", "15", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceAtFlatOFFData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceAtFlatOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 ,G2 , G3 and get G3 max price... at FLAT 75 ",
							"buy G1 ,G2 , G3 and get G3 max price... at FLAT 75 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward("MAX", "Flat", "75", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3PercentageOFFData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3PercentageOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get min price...in G1,G2 and G3  at 35% OFF ",
							"buy Meal M1 and get min price...in G1,G2 and G3  at 35% OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MIN", "Percentage", "35", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3PercentageOFFData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3PercentageOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get max price...in G1,G2 and G3  at 35% OFF ",
							"buy Meal M1 and get max price...in G1,G2 and G3  at 35% OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MAX", "Percentage", "35", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3FlatOFFData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3FlatOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get min price...in G1,G2 and G3  FLAT 99 OFF ",
							"buy Meal M1 and get min price...in G1,G2 and G3  FLAT 99 OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MIN", "Flat", "99", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3FlatOFFData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3FlatOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get max price...in G1,G2 and G3  FLAT 99 OFF ",
							"buy Meal M1 and get max price...in G1,G2 and G3  FLAT 99 OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MAX", "Flat", "99", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3AtFinalPriceData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3AtFinalPrice() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get min price...in G1,G2 and G3 at Final Price 120",
							"buy Meal M1 and get min price...in G1,G2 and G3 at Final Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MIN", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3AtFinalPriceData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3AtFinalPrice() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get max price...in G1,G2 and G3 at Final Price 99 ",
							"buy Meal M1 and get max price...in G1,G2 and G3 at Final Price 99 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MAX", "FinalPrice", "99", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3FREEData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3FREE() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get min price...in G1,G2 and G3 FREE ",
							"buy Meal M1 and get min price...in G1,G2 and G3 FREE ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MIN", "Percentage", "100", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3FREEData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3FREE() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get max price...in G1,G2 and G3 FREE ",
							"buy Meal M1 and get max price...in G1,G2 and G3 FREE ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("MAX", "Percentage", "100", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetPercentageOFFData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetPercentageOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get 25% OFF ", "buy Meal M1 and get 25% OFF ")
							.toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "25", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGetFlatOFFData")
	public static Object[][] createEDVOCampWhereBuyMealAndGetFlatOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy Meal M1 and get Flat 100 OFF ",
					"buy Meal M1 and get Flat 100 OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Flat", "100", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAtFinalPriceData")
	public static Object[][] createEDVOCampWhereBuyMealAtFinalPriceData() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 at Final Price 99 ", "buy Meal M1 at Final Price 99 ")
							.toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "99", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyMealAndGet100PercentOFFData")
	public static Object[][] createEDVOCampWhereBuyMealAndGet100PercentOFF() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy Meal M1 and get 100% OFF ", "buy Meal M1 and get 100% OFF ")
							.toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "Percentage", "100", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward().toString());
			allGroupsRewards.add(2, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetXPercentOFFInG1YPercentOFFInG2ZPercentOFFInG3Data")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetXPercentOFFInG1YPercentOFFInG2ZPercentOFFInG3() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at 10% OFF , G2 at 20% OFF and G3 at 55% OFF ",
							"buy G1 at 10% OFF , G2 at 20% OFF and G3 at 55% OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "2");
			allGroupCounts.add(2, "2");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "20", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "55", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetXFlatOFFInG1YFlatOFFInG2ZFlatOFFInG3Data")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetXFlatOFFInG1YFlatOFFInG2ZFlatOFFInG3() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at 75 FLAT OFF , G2 at 50 FLAT and G3 at 99 FLAT OFF ",
							"buy G1 at 75 FLAT OFF , G2 at 50 FLAT and G3 at 99 FLAT OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "2");
			allGroupCounts.add(2, "2");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "Flat", "75", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "Flat", "50", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Flat", "99", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYG3AtFinalPriceZData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYG3AtFinalPriceZ() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and G3 at FINAL Price 135 ",
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and G3 at FINAL Price 135 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "2");
			allGroupCounts.add(2, "2");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "FinalPrice", "135", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndG3FREEData")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndG3FREE() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and G3 FREE ",
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and G3 FREE ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "2");
			allGroupCounts.add(2, "2");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "100", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndZPercentOFFInG3Data")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndZPercentOFFInG3() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and G3 at 25% OFF ",
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and G3 at 25% OFF ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "2");
			allGroupCounts.add(2, "2");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "25", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndFlatZOFFInG3Data")
	public static Object[][] createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndFlatZOFFInG3() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ",
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "2");
			allGroupCounts.add(2, "2");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Flat", "70", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereMealCountIsZeroData")
	public static Object[][] createEDVOCampWhereMealCountIsZero() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ",
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "0");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "2");
			allGroupCounts.add(2, "2");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Flat", "70", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereMealRewardCountIsZeroData")
	public static Object[][] createEDVOCampWhereMealRewardCountIsZero() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ",
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "0").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "2");
			allGroupCounts.add(2, "2");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Flat", "70", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereGroupCountIsZeroData")
	public static Object[][] createEDVOCampWhereGroupCountIsZero() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ",
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "0");
			allGroupCounts.add(1, "0");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Flat", "70", "1").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereGroupRewardCountIsZeroData")
	public static Object[][] createEDVOCampWhereGroupRewardCountIsZero() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta(
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ",
							"buy G1 at FINAL Price 120 , G2 at FINAL Price 99 and 70 Flat OFF in G3 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward().toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 3);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "0").toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "0").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Flat", "70", "0").toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createOneEDVOCampOnMultipleRestaurantsSomeOnMealLevelAndSomeOnGroupLevelData")
	public static Object[][] createOneEDVOCampOnMultipleRestaurantsSomeOnMealLevelAndSomeOnGroupLevel() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());
			listOfRestIds.add(1, edvoHelper.getRestId());
			listOfRestIds.add(2, edvoHelper.getRestId());
			listOfRestIds.add(3, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");
			operationTypeOfCamp.add(1, "\"MEAL\"");
			operationTypeOfCamp.add(2, "\"MEAL\"");
			operationTypeOfCamp.add(3, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");
			numberOfMealsInARest.add(1, "1");
			numberOfMealsInARest.add(2, "1");
			numberOfMealsInARest.add(3, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());
			allMealIds.add(1, edvoHelper.getMealId());
			allMealIds.add(2, edvoHelper.getMealId());
			allMealIds.add(3, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());
			allMealsOperationMeta.add(1,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());
			allMealsOperationMeta.add(2,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());
			allMealsOperationMeta.add(3,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
			allMealRewards.add(1, new CreateReward().toString());
			allMealRewards.add(2, new CreateReward().toString());
			allMealRewards.add(3, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");
			mealCount.add(1, "1");
			mealCount.add(2, "1");
			mealCount.add(3, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");
			mealIsParent.add(1, "true");
			mealIsParent.add(2, "true");
			mealIsParent.add(3, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);
			numberOfGroupsInEachMeal.add(1, 1);
			numberOfGroupsInEachMeal.add(2, 1);
			numberOfGroupsInEachMeal.add(3, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
			allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");
			allGroupCounts.add(1, "1");
			allGroupCounts.add(2, "1");
			allGroupCounts.add(3, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());
			allGroupsRewards.add(2, new CreateReward("NONE", "Flat", "70", "1").toString());
			allGroupsRewards.add(3, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_Test_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampToValidateNameSpaceWithValidInputValueData")
	public static Object[][] createEDVOCampToValidateNameSpaceWithValidInputValue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"NameSpace_Test\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereNameSpaceIsEmptyStringData")
	public static Object[][] createEDVOCampWhereNameSpaceIsEmptyString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereNameSpaceNullData")
	public static Object[][] createEDVOCampWhereNameSpaceNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "null");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWithValidValueForHeaderData")
	public static Object[][] createEDVOCampWithValidValueForHeader() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"Header_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereHeaderAsEmptyStringData")
	public static Object[][] createEDVOCampWhereHeaderAsEmptyString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereHeaderAsNullValueData")
	public static Object[][] createEDVOCampWhereHeaderAsNullValue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "null");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampDescriptionWithValidInputValueData")
	public static Object[][] createEDVOCampDescriptionWithValidInputValue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Meal level Reward type description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampDescriptionForEmptyStringData")
	public static Object[][] createEDVOCampDescriptionForEmptyString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampDescriptionSetAsNullData")
	public static Object[][] createEDVOCampDescriptionSetAsNullData() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 1525046400000
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "null");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereValidFromDateIsPresentData")
	public static Object[][] createEDVOCampWhereValidFromDateIsPresent() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());
			String discount = edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest,
					allMealIds, allMealsOperationMeta, allMealRewards, mealCount, mealIsParent,
					numberOfGroupsInEachMeal, allGroupCounts, allGroupIdsOfMeals, allGroupsRewards);

			createEDVOCamp = edvoHelper.createEDVOWithDefaultValues(discount, null, 0, 0, 86400000);
			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereValidFromIsFutureDateData")
	public static Object[][] createEDVOCampWhereValidFromIsFutureDate() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			//
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000 + 7200000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereValidFromIsPastDateData")
	public static Object[][] createEDVOCampWhereValidFromIsPastDate() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			//
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getFuturetimeInMilliSeconds(-86400000));
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereValidTillDateIsFutureDateData")
	public static Object[][] createEDVOCampWhereValidTillDateIsFutureDate() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			//
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000 + 86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereValidTillDateIsSmallerThanValidFromDateData")
	public static Object[][] createEDVOCampWhereValidTillDateIsSmallerThanValidFromDateData() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			//
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getFuturetimeInMilliSeconds(86400000 + 86400000));
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereValidTillDateIsPresentDateData")
	public static Object[][] createEDVOCampWhereValidTillDateIsPresentDate() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			//
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getFuturetimeInMilliSeconds(86400000 + 86400000));
			createEDVOCamp.put("5", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereValidTillDateIsSameAsValidFromDateData")
	public static Object[][] createEDVOCampWhereValidTillDateIsSameAsValidFromDate() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getFuturetimeInMilliSeconds(86400000 + 86400000));
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000 + 86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereValidTillDateIsGreaterThanValidFromDateData")
	public static Object[][] createEDVOCampWhereValidTillDateIsGreaterThanValidFromDate() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getFuturetimeInMilliSeconds(86400000 + 86400000));
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000 + 86400000 + 300000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCampaignTypeIsBXGYData")
	public static Object[][] createEDVOCampWhereCampaignTypeIsBXGY() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCampaignTypeIsFREE_DELIVERYData")
	public static Object[][] createEDVOCampWhereCampaignTypeIsFREE_DELIVERY() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"FREE_DELIVERY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCampaignTypeIsFreebieData")
	public static Object[][] createEDVOCampWhereCampaignTypeIsFreebie() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"Freebie\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCampaignTypeIsFinalPriceData")
	public static Object[][] createEDVOCampWhereCampaignTypeIsFinalPrice() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"FinalPrice\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCampaignTypeIsFlatData")
	public static Object[][] createEDVOCampWhereCampaignTypeIsFlat() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"Flat\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCampaignTypeIsPercentageData")
	public static Object[][] createEDVOCampWhereCampaignTypeIsPercentage() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"Percentage\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCampaignTypeAsEmptyStringData")
	public static Object[][] createEDVOCampWhereCampaignTypeAsEmptyString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "0").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCampaignTypeIsNullData")
	public static Object[][] createEDVOCampWhereCampaignTypeIsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "null");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereEnabledIsTrueData")
	public static Object[][] createEDVOCampWhereEnabledIsTrue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereEnabledIsFalseData")
	public static Object[][] createEDVOCampWhereEnabledIsFalse() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and enabled (8) false
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "false");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereRestaurantHitIsHunderedData")
	public static Object[][] createEDVOCampWhereRestaurantHitIsHundered() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward and 300000 is equal to 5 minutes
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and restaurant hit (7) false
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereRestaurantHitIsFiveData")
	public static Object[][] createEDVOCampWhereRestaurantHitIsFive() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "5");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "95");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereRestaurantHitIsZeroData")
	public static Object[][] createEDVOCampWhereRestaurantHitIsZero() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "0");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "100");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereRestaurantHitIsGreaterThanHunderedData")
	public static Object[][] createEDVOCampWhereRestaurantHitIsGreaterThanHundered() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "101");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereRestaurantHitIsNegativeValueData")
	public static Object[][] createEDVOCampWhereRestaurantHitIsNegativeValue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "-1");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "100");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereRestaurantHitIsInvalidValueData")
	public static Object[][] createEDVOCampWhereRestaurantHitIsInvalidValue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "-1");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "99");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereRestaurantHitAndSwiggyHitIsNotEqualToHunderedData")
	public static Object[][] createEDVOCampWhereRestaurantHitAndSwiggyHitIsNotEqualToHundered() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "17");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "82");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereRestaurantHitAndSwiggyHitIsGreaterThanHunderedData")
	public static Object[][] createEDVOCampWhereRestaurantHitAndSwiggyHitIsGreaterThanHundered() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "17.5");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "83.5");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereSwiggyHitIsValidInputData")
	public static Object[][] createEDVOCampWhereSwiggyHitIsValidInput() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "0");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "100");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereSwiggyHitIsZeroData")
	public static Object[][] createEDVOCampWhereSwiggyHitIsZero() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereSwiggyHitLesserThanHunderedData")
	public static Object[][] createEDVOCampWhereSwiggyHitLesserThanHundered() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "0.1");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "99.9");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereSwiggyHitGreaterThanHunderedData")
	public static Object[][] createEDVOCampWhereSwiggyHitGreaterThanHundered() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "0.0");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "100.1");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereSwiggyHitIsNegativeValueData")
	public static Object[][] createEDVOCampWhereSwiggyHitIsNegativeValue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "-1");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereSwiggyHitAndRestaurantHitIsNotEqualToHunderedData")
	public static Object[][] createEDVOCampWhereSwiggyHitAndRestaurantHitIsNotEqualToHundered() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "7");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "92.99");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereSwiggyHitAndRestaurantHitIsGreaterThanHunderedData")
	public static Object[][] createEDVOCampWhereSwiggyHitAndRestaurantHitIsGreaterThanHundered() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and restaurant hit
			// (7) and swiggy hit (9)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "7.2");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "92.9");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCreateByIsValidInputData")
	public static Object[][] createEDVOCampWhereCreateByIsValidInput() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and created By (10)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"ANKITA YADAV\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCreateByAsEmptyStringData")
	public static Object[][] createEDVOCampWhereCreateByAsEmptyString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and created By (10)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCreateByAsNullValueData")
	public static Object[][] createEDVOCampWhereCreateByAsNullValue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and created By (10)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "null");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCommissionOnFullBillAsTrueData")
	public static Object[][] createEDVOCampWhereCommissionOnFullBillAsTrue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and
			// commissionOnFullBill (15)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "true");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCommissionOnFullBillAsFalseData")
	public static Object[][] createEDVOCampWhereCommissionOnFullBillAsFalse() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and
			// commissionOnFullBill (15)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereTaxesOnDiscountedBillIsTrueData")
	public static Object[][] createEDVOCampWhereTaxesOnDiscountedBillIsTrue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and
			// createEDVOCampWhereTaxesOnDiscountedBillData (16)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "true");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereTaxesOnDiscountedBillIsFalseData")
	public static Object[][] createEDVOCampWhereTaxesOnDiscountedBillIsFalse() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and
			// createEDVOCampWhereTaxesOnDiscountedBillData (16)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereFirstOrderRestrictionIsTrueData")
	public static Object[][] createEDVOCampWhereFirstOrderRestrictionIsTrue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and First Order
			// Restriction (17)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "true");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereFirstOrderRestrictionIsFalseData")
	public static Object[][] createEDVOCampWhereFirstOrderRestrictionIsFalse() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and First Order
			// Restriction (17)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereTimeSlotRestrictionIsTrueData")
	public static Object[][] createEDVOCampWhereTimeSlotRestrictionIsTrue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Time Slot
			// Restriction (18)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "true");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereTimeSlotRestrictionIsFalseData")
	public static Object[][] createEDVOCampWhereTimeSlotRestrictionIsFalse() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Time Slot
			// Restriction (18)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereUserRestrictionIsTrueData")
	public static Object[][] createEDVOCampWhereUserRestrictionIsTrue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and User
			// restriction (21)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "true");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereUserRestrictionIsFalseData")
	public static Object[][] createEDVOCampWhereUserRestrictionIsFalse() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and User
			// restriction (21)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereShortDescriptionData")
	public static Object[][] createEDVOCampWhereShortDescription() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Short
			// Description (3)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_ShortDescription\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereShortDescriptionAsEmptyStringData")
	public static Object[][] createEDVOCampWhereShortDescriptionAsEmptyString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Short
			// Description (3)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereShortDescriptionAsNullData")
	public static Object[][] createEDVOCampWhereShortDescriptionAsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Short
			// Description (3)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "null");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereMealCountIsOneData")
	public static Object[][] createEDVOCampWhereMealCountIsOne() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Short
			// Description (3)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereMealCountIsTwoData")
	public static Object[][] createEDVOCampWhereMealCountIsTwo() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "2");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Short
			// Description (3)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereMealCountAsNullData")
	public static Object[][] createEDVOCampWhereMealCountAsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "0").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "null");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Short
			// Description (3)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereMealIsParentIsTrueData")
	public static Object[][] createEDVOCampWhereMealIsParentIsTrue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Short
			// Description (3)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereMealIsParentIsFalseData")
	public static Object[][] createEDVOCampWhereMealIsParentIsFalse() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "false");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Short
			// Description (3)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", "null");
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereTimeSlotRestrictionIsSetData")
	public static Object[][] createEDVOCampWhereTimeSlotRestrictionIsSet() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

			createEDVOCamp.put("14", edvoHelper.createMultipleTimeSlot(new String[] { "MON", "MON" },
					new int[] { 1200, 1900 }, new int[] { 1500, 2100 }));
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "true");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereTimeSlotRestrictionIsSetToTrueData")
	public static Object[][] createEDVOCampWhereTimeSlotRestrictionIsSetToTrue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String day = "ALL";
			int openTimeIs = 1200;
			int closeTimeIs = 1500;

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", new EDVOTimeSlot().toString());
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "true");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereTimeSlotRestrictionIsSetToFalseAndSlotIsNullData")
	public static Object[][] createEDVOCampWhereTimeSlotRestrictionIsSetToFalseAndSlotIsNull() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String day = "ALL";
			int openTimeIs = 1200;
			int closeTimeIs = 1500;

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", new EDVOTimeSlot().toString());
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereTimeSlotRestrictionIsSetToFalseAndSlotIsSetData")
	public static Object[][] createEDVOCampWhereTimeSlotRestrictionIsSetToFalseAndSlotIsSet() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
			String day = "ALL";
			int openTimeIs = 1900;
			int closeTimeIs = 2300;

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", new EDVOTimeSlot(day, openTimeIs, closeTimeIs).toString());
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCheckAllDaysOfSlotWhenTimeSlotRestrictionIsSetToTrueData")
	public static Object[][] createEDVOCampWhereCheckAllDaysOfSlotWhenTimeSlotRestrictionIsSetToTrue() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18) and slot (14)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14",
					edvoHelper.createMultipleTimeSlot(
							new String[] { "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN", "ALL" },
							new int[] { 1100, 1900, 700, 900, 800, 1030, 2030, 700 },
							new int[] { 1300, 2100, 1100, 1300, 1233, 1430, 2200, 2300 }));
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "true");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCheckDaysTimeOfSlotAsStringData")
	public static Object[][] createEDVOCampWhereCheckDaysTimeOfSlotAsString() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "0").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "null");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18) and slot (14)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14",
					edvoHelper.createMultipleTimeSlot(new String[] { "" }, new int[] { 1100 }, new int[] { 1300 }));
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "true");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereOpenIsGreaterThanCloseTimeData")
	public static Object[][] createEDVOCampWhereOpenIsGreaterThanCloseTime() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18) and slot (14)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", new EDVOTimeSlot("MON", 2100, 2000).toString());
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "true");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCloseTimeIsInvalidData")
	public static Object[][] createEDVOCampWhereCloseTimeIsInvalidTest() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18) and slot (14)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));
			createEDVOCamp.put("14", new EDVOTimeSlot("MON", 2100, 10).toString());
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "true");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereCloseTimeIsNextDayData")
	public static Object[][] createEDVOCampWhereCloseTimeIsNextDayTest() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18) and slot (14)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

			createEDVOCamp.put("14", edvoHelper.createMultipleTimeSlot(new String[] { "MON", "TUE" },
					new int[] { 2100, 15 }, new int[] { 2400, 1100 }));
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "true");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampWhereOpenTimeIsZeroData")
	public static Object[][] createEDVOCampWhereOpenTimeIsZero() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "0").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "null");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and time slot
			// restriction (18) and slot (14)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

			createEDVOCamp.put("14", new EDVOTimeSlot("ALL", 0, 0).toString());
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "true");
			createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "false");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampForThirtyDaysDormantData")
	public static Object[][] createEDVOCampForThirtyDaysDormant() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
			// type (19) and user restriction (21)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

			createEDVOCamp.put("14", new EDVOTimeSlot().toString());
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "THIRTY_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "true");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampForSixtyDaysDormantData")
	public static Object[][] createEDVOCampForSixtyDaysDormant() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
			// type (19) and user restriction (21)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

			createEDVOCamp.put("14", new EDVOTimeSlot().toString());
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "SIXTY_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "true");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampForNintyDaysDormantData")
	public static Object[][] createEDVOCampForNintyDaysDormant() {
		{
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList<String> numberOfMealsInARest = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();
			ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList<String> allMealRewards = new ArrayList<String>();
			ArrayList<String> mealCount = new ArrayList<String>();
			ArrayList<String> mealIsParent = new ArrayList<String>();
			ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
			ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList<String> allGroupCounts = new ArrayList<String>();
			ArrayList<String> allGroupsRewards = new ArrayList<String>();
			HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

			// listOfAllRestaurantIds
			listOfRestIds.add(0, edvoHelper.getRestId());

			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");

			// list of number of meals in restaurants
			numberOfMealsInARest.add(0, "1");

			// list of All MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0,
					new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

			// list of number of "count" in a meal
			mealCount.add(0, "1");

			// isParent value of Meals
			mealIsParent.add(0, "true");

			// list of number of Groups in each meal
			numberOfGroupsInEachMeal.add(0, 1);

			// list of groupIds in Meal
			allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

			// list of count in a group
			allGroupCounts.add(0, "1");

			// list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward().toString());

			// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
			// type (19) and user restriction (21)
			createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
			createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
			createEDVOCamp.put("3", "\"EDVO_Short Description\"");
			createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
			createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
			createEDVOCamp.put("6", "\"BXGY\"");
			createEDVOCamp.put("7", "100");
			createEDVOCamp.put("8", "true");
			createEDVOCamp.put("9", "0");
			createEDVOCamp.put("10", "\"AY\"");
			createEDVOCamp.put("11", "120");
			createEDVOCamp.put("12", "0");
			createEDVOCamp.put("13",
					edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
							allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
							allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

			createEDVOCamp.put("14", new EDVOTimeSlot().toString());
			createEDVOCamp.put("15", "false");
			createEDVOCamp.put("16", "false");
			createEDVOCamp.put("17", "false");
			createEDVOCamp.put("18", "false");
			createEDVOCamp.put("19", "NINETY_DAYS_DORMANT");
			createEDVOCamp.put("20", "false");
			createEDVOCamp.put("21", "true");

			return new Object[][] { { createEDVOCamp } };

		}
	}

	@DataProvider(name = "createEDVOCampForMultiTDWithThirtyDaysDormantData")
	public static Object[][] createEDVOCampForMultiTDWithThirtyDaysDormant() {
		{
			HashMap<String, String> createEDVOCampWithUserRestriction = new HashMap<String, String>();
			HashMap<String, String> createEDVOCampWithSwiggyFirstOrder = new HashMap<String, String>();
			HashMap<String, String> createEDVOCampWithRestaurantFirstOrder = new HashMap<String, String>();
			HashMap<String, String> createEDVOCampWithThirtyDaysDormant = new HashMap<String, String>();
			HashMap<String, String> createPublicTypeEDVOCamp = new HashMap<String, String>();
			ArrayList<String> listOfRestIds = new ArrayList();
			ArrayList<String> allMealIds = new ArrayList();

			// listOfAllRestaurantIds
			String restautantId = edvoHelper.getRestId();
			// list Of all MealIds
			allMealIds.add(0, edvoHelper.getMealId());

			createEDVOCampWithUserRestriction = edvoHelper.createUserCutMultiTD(true, false, false, "ZERO_DAYS_DORMANT",
					restautantId, allMealIds.get(0));
			createEDVOCampWithSwiggyFirstOrder = edvoHelper.createUserCutMultiTD(false, true, false,
					"ZERO_DAYS_DORMANT", restautantId, allMealIds.get(0));
			createEDVOCampWithRestaurantFirstOrder = edvoHelper.createUserCutMultiTD(false, false, true,
					"ZERO_DAYS_DORMANT", restautantId, allMealIds.get(0));
			createEDVOCampWithThirtyDaysDormant = edvoHelper.createUserCutMultiTD(false, false, false,
					"THIRTY_DAYS_DORMANT", restautantId, allMealIds.get(0));
			createPublicTypeEDVOCamp = edvoHelper.createUserCutMultiTD(false, false, false, "ZERO_DAYS_DORMANT",
					restautantId, allMealIds.get(0));

			return new Object[][] { { createEDVOCampWithUserRestriction, createEDVOCampWithSwiggyFirstOrder,
					createEDVOCampWithRestaurantFirstOrder, createEDVOCampWithThirtyDaysDormant,
					createPublicTypeEDVOCamp } };

		}
	}

	@DataProvider(name = "evaluateMealForMultipleMealIdsOnWhichCampaignIsRunningData")
	public static Object[][] evaluateMealForMultipleMealIdsOnWhichCampaignIsRunning() {

		HashMap<String, String> mealEvaulate = new HashMap<>();
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		String meals = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1, edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");
		operationTypeOfCamp.add(1, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");
		numberOfMealsInARest.add(1, "2");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());
		allMealIds.add(1, edvoHelper.getMealId());
		allMealIds.add(2, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());
		allMealsOperationMeta.add(1,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());
		allMealsOperationMeta.add(2,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());
		allMealRewards.add(1, new CreateReward("NONE", "Percentage", "12", "1").toString());
		allMealRewards.add(2, new CreateReward("NONE", "Percentage", "12", "1").toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");
		mealCount.add(1, "1");
		mealCount.add(1, "2");

		// isParent value of Meals
		mealIsParent.add(0, "true");
		mealIsParent.add(1, "true");
		mealIsParent.add(2, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);
		numberOfGroupsInEachMeal.add(1, 1);
		numberOfGroupsInEachMeal.add(2, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");
		allGroupCounts.add(1, "1");
		allGroupCounts.add(2, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward().toString());
		allGroupsRewards.add(1, new CreateReward().toString());
		allGroupsRewards.add(2, new CreateReward().toString());

		String discount = edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest,
				allMealIds, allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
				allGroupCounts, allGroupIdsOfMeals, allGroupsRewards);
		createEDVOCamp = edvoHelper.createEDVOWithDefaultValues(discount, null, 0, 0, 86400000);
		meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "123");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealForWithOneMealIdOnWhichCampaignIsRunningData")
	public static Object[][] evaluateMealForWithOneMealIdOnWhichCampaignIsRunning() {

		HashMap<String, String> mealEvaulate = new HashMap<>();
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		String meals = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward().toString());

		String discount = edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest,
				allMealIds, allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
				allGroupCounts, allGroupIdsOfMeals, allGroupsRewards);
		createEDVOCamp = edvoHelper.createEDVOWithDefaultValues(discount, null, 0, 0, 86400000);
		meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "123");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealForFirstOrderRestrictionData")
	public static Object[][] evaluateMealForFirstOrderRestriction() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = true;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayoad = new HashMap<String, String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayoad.put("0", meals);
		mealPayoad.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayoad.put("2", "true");
		mealPayoad.put("3", "\"ANDROID\"");
		mealPayoad.put("4", "300");
		mealPayoad.put("5", "123");

		return new Object[][] { { createEDVOCamp, mealPayoad } };
	}

	@DataProvider(name = "evaluateMealWithFirstOrderFalseData")
	public static Object[][] evaluateMealWithFirstOrderFalse() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = true;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayoad = new HashMap<String, String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayoad.put("0", meals);
		mealPayoad.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayoad.put("2", "false");
		mealPayoad.put("3", "\"ANDROID\"");
		mealPayoad.put("4", "300");
		mealPayoad.put("5", "123");

		return new Object[][] { { createEDVOCamp, mealPayoad } };
	}

	@DataProvider(name = "evaluateMealWhereMealIdIsZeroData")
	public static Object[][] evaluateMealWhereMealIdIsZero() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allMealIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		allMealIdsForMeals.add(0, "0");
		String meals = edvoHelper.createMealData(allMealIdsForMeals, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWhereMealIdIsNullData")
	public static Object[][] evaluateMealWhereMealIdIsNull() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allMealIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		allMealIdsForMeals.add(0, "null");
		String meals = edvoHelper.createMealData(allMealIdsForMeals, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWhereMealIdIsNonExistingMealIdData")
	public static Object[][] evaluateMealWhereMealIdIsNonExistingMealId() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allMealIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		allMealIdsForMeals.add(0, edvoHelper.getMealId());
		String meals = edvoHelper.createMealData(allMealIdsForMeals, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWhereRestIdIsZeroData")
	public static Object[][] evaluateMealWhereRestIdIsZero() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		allRestIdsForMeals.add(0, "0");
		String meals = edvoHelper.createMealData(allMealIds, allRestIdsForMeals, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWhereRestIdIsNullData")
	public static Object[][] evaluateMealWhereRestIdIsNull() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		allRestIdsForMeals.add(0, "null");
		String meals = edvoHelper.createMealData(allMealIds, allRestIdsForMeals, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", "null");
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWhereRestIdIsNonExistingIdData")
	public static Object[][] evaluateMealWhereRestIdIsNonExistingId() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		allRestIdsForMeals.add(0, edvoHelper.getRestId());
		String meals = edvoHelper.createMealData(allMealIds, allRestIdsForMeals, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingRestIdAsMealIdData")
	public static Object[][] evaluateMealWherePassingRestIdAsMealId() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(listOfRestIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingMealIdAsRestIdData")
	public static Object[][] evaluateMealWherePassingMealIdAsRestId() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, allMealIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingMealIdOfTheExpiredCampaignData")
	public static Object[][] evaluateMealWherePassingMealIdOfTheExpiredCampaign() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		// ArrayList<String> allRestIdsForMeals = new ArrayList<String>();
		HashMap<String, String> mealEvaulate = new HashMap<>();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward().toString());

		String discount = edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest,
				allMealIds, allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
				allGroupCounts, allGroupIdsOfMeals, allGroupsRewards);

		createEDVOCamp = edvoHelper.createEDVOWithDefaultValues(discount, null, 0, 0, 10000);
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "300");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingSupportedIOSVersionData")
	public static Object[][] evaluateMealWherePassingSupportedIOSVersion() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"IOS\"");
		mealPayload.put("4", "200");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingNonSupportedIOSVersionData")
	public static Object[][] evaluateMealWherePassingNonSupportedIOSVersion() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"IOS\"");
		mealPayload.put("4", "199");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingSupportedAndroidVersionData")
	public static Object[][] evaluateMealWherePassingSupportedAndroidVersionData() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "229");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingNonSupportedAndroidVersionData")
	public static Object[][] evaluateMealWherePassingNonSupportedAndroidVersion() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "228");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingInvalidUserAgentVersionData")
	public static Object[][] evaluateMealWherePassingInvalidUserAgentVersion() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ios\"");
		mealPayload.put("4", "200");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingInvalidVersionCodeForAndroidData")
	public static Object[][] evaluateMealWherePassingInvalidVersionCodeForAndroid() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"ANDROID\"");
		mealPayload.put("4", "null");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingInvalidVersionCodeForIOSData")
	public static Object[][] evaluateMealWherePassingInvalidVersionCodeForIOS() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"IOS\"");
		mealPayload.put("4", "null");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingInvalidUserAgentWhenVersionIs229Data")
	public static Object[][] evaluateMealWherePassingInvalidUserAgentWhenVersionIs229() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"\"");
		mealPayload.put("4", "229");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealWherePassingInvalidUserAgentWhenVersionIs200Data")
	public static Object[][] evaluateMealWherePassingInvalidUserAgentWhenVersionIs200() {
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCamp = edvoHelper.createUserCutMultiTD(userRestriction, firstOrderRestriction, restaurantFirstOrder,
				dormantType, listOfRestIds.get(0), allMealIds.get(0));
		// allRestIdsForMeals.add(0, Integer.toString(Utility.getRandom(10000,
		// 500000)));
		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"\"");
		mealPayload.put("4", "200");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCamp, mealPayload } };
	}

	@DataProvider(name = "evaluateMealCheckForMultiTDWith30DaysDormantData")
	public static Object[][] evaluateMealCheckForMultiTDWith30DaysDormant() {
		HashMap<String, String> createEDVOCampFirstOrderRestriction = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestaurantFirstOrderRestriction = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicTypeCamp = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampFor30DaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampForMappedUser = new HashMap<String, String>();

		boolean userRestriction = false;
		boolean firstOrderRestriction = false;
		boolean restaurantFirstOrder = false;
		String dormantType = "ZERO_DAYS_DORMANT";
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		HashMap<String, String> mealPayload = new HashMap<String, String>();
		ArrayList<String> allRestIdsForMeals = new ArrayList<String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		createEDVOCampFirstOrderRestriction = edvoHelper.createUserCutMultiTD(false, true, false, "ZERO_DAYS_DORMANT",
				listOfRestIds.get(0), allMealIds.get(0));
		createEDVOCampRestaurantFirstOrderRestriction = edvoHelper.createUserCutMultiTD(false, false, true,
				"ZERO_DAYS_DORMANT", listOfRestIds.get(0), allMealIds.get(0));
		createEDVOPublicTypeCamp = edvoHelper.createUserCutMultiTD(false, false, false, "ZERO_DAYS_DORMANT",
				listOfRestIds.get(0), allMealIds.get(0));
		createEDVOCampFor30DaysDormant = edvoHelper.createUserCutMultiTD(false, false, false, "THIRTY_DAYS_DORMANT",
				listOfRestIds.get(0), allMealIds.get(0));
		createEDVOCampForMappedUser = edvoHelper.createUserCutMultiTD(true, false, false, "ZERO_DAYS_DORMANT",
				listOfRestIds.get(0), allMealIds.get(0));

		String meals = edvoHelper.createMealData(allMealIds, listOfRestIds, numberOfMealsInARest);
		mealPayload.put("0", meals);
		mealPayload.put("1", StringUtils.join(listOfRestIds, ','));
		mealPayload.put("2", "false");
		mealPayload.put("3", "\"IOS\"");
		mealPayload.put("4", "200");
		mealPayload.put("5", "0");

		return new Object[][] { { createEDVOCampFirstOrderRestriction, createEDVOCampRestaurantFirstOrderRestriction,
				createEDVOPublicTypeCamp, createEDVOCampFor30DaysDormant, createEDVOCampForMappedUser, mealPayload } };
	}

	@DataProvider(name = "evaluateCartWithTwoRestIdsWhereInOfTheRestIdsCampIsActiveData")
	public static Object[][] evaluateCartWithTwoRestIdsWhereInOfTheRestIdsCampIsActive() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "2");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());
		allMealIds.add(1, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());
		allMealsOperationMeta.add(1,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());
		allMealRewards.add(1, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");
		mealCount.add(1, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");
		mealIsParent.add(1, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);
		numberOfGroupsInEachMeal.add(1, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");
		allGroupCounts.add(1, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "99", "1").toString());
		allGroupsRewards.add(1, new CreateReward("NONE", "FinalPrice", "99", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");
		listOfItemCountInMeal.add(1, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "200");
		listOfAllItemsPrice.add(1, "200");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");
		numOfMealItemRequestInCart.add(1, "1");

		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(1, Integer.toString(Utility.getRandom(10, 2300)));
		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);
		listOfRestIdsInCart.add(1, edvoHelper.getRestId());

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWhereThreeItemsOfGroupIsAddedAndCampaignIsForTwoItemsOfGroupData")
	public static Object[][] evaluateCartWhereThreeItemsOfGroupIsAddedAndCampaignIsForTwoItemsOfGroup() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 2-items and get FLAT 50 OFF", "buy G1 2-items and get FLAT 50 OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "2");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Flat", "50", "2").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "3");

		// list of item's price
		listOfAllItemsPrice.add(0, "350");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithOneMealRequestData")
	public static Object[][] evaluateCartWithOneMealRequest() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 1-items and get 20 Percent OFF",
				"buy G1 1-items and get 20 Percent OFF").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "200");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithMultipleMealRequestData")
	public static Object[][] evaluateCartWithMultipleMealRequest() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "3");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());
		allMealIds.add(1, edvoHelper.getMealId());
		allMealIds.add(2, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 1-items and get 20 Percent OFF",
				"buy G1 1-items and get 20 Percent OFF").toString());
		allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy G2 1-items and get 20 Percent OFF",
				"buy G1 1-items and get 20 Percent OFF").toString());
		allMealsOperationMeta.add(2, new CreateEDVOOperationMeta("buy G3 1-items and get 20 Percent OFF",
				"buy G1 1-items and get 20 Percent OFF").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());
		allMealRewards.add(1, new CreateReward().toString());
		allMealRewards.add(2, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");
		mealCount.add(1, "1");
		mealCount.add(2, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");
		mealIsParent.add(1, "true");
		mealIsParent.add(2, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);
		numberOfGroupsInEachMeal.add(1, 1);
		numberOfGroupsInEachMeal.add(2, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");
		allGroupCounts.add(1, "1");
		allGroupCounts.add(2, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "20", "1").toString());
		allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "20", "1").toString());
		allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "20", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");
		listOfItemCountInMeal.add(1, "1");
		listOfItemCountInMeal.add(2, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "100");
		listOfAllItemsPrice.add(1, "100");
		listOfAllItemsPrice.add(2, "100");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");
		numOfMealItemRequestInCart.add(1, "1");
		numOfMealItemRequestInCart.add(2, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(1, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(2, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);
		listOfRestIdsInCart.add(1, restId);
		listOfRestIdsInCart.add(2, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWhereMealItemRequestsRequestIsEmptyData")
	public static Object[][] evaluateCartWhereMealItemRequestsRequestIsEmpty() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 1-items and get 20 Percent OFF",
				"buy G1 1-items and get 20 Percent OFF").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "100");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "0");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithTheMealIdOnWhichNoCampaignIsActiveData")
	public static Object[][] evaluateCartWithTheMealIdOnWhichNoCampaignIsActive() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy G1 1-items and get 20 Percent OFF",
				"buy G1 1-items and get 20 Percent OFF").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "100");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		allMealIds.add(0, edvoHelper.getMealId());
		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWhenActiveCampaigneIsGroupLevelData")
	public static Object[][] evaluateCartWhenActiveCampaigneIsGroupLevel() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FinalPrice 99", "buy G1 at FinalPrice 99").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "99", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "2");
		listOfItemCountInMeal.add(1, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "150");
		listOfAllItemsPrice.add(1, "200");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");
		numOfMealItemRequestInCart.add(1, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(1, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);
		listOfRestIdsInCart.add(1, restId);

		allMealIds.add(1, edvoHelper.getMealId());
		allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithThatGroupIdForWhichCampaignIsExpiredData")
	public static Object[][] evaluateCartWithThatGroupIdForWhichCampaignIsExpired() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at Flat 40 OFF", "buy G1 at Flat 40 OFF").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Flat", "40", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "2");

		// list of item's price
		listOfAllItemsPrice.add(0, "150");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithCombinationOfTwoMealIdsInOneOfWhichCampaignIsActiveAndAnothOneNoActiveCampaignData")
	public static Object[][] evaluateCartWithCombinationOfTwoMealIdsInOneOfWhichCampaignIsActiveAndAnothOneNoActiveCampaign() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at 15 Percent OFF", "buy G1 at 15 Percent OFF").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "15", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");
		listOfItemCountInMeal.add(1, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "150");
		listOfAllItemsPrice.add(1, "100");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");
		numOfMealItemRequestInCart.add(1, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(1, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);
		listOfRestIdsInCart.add(1, restId);

		allMealIds.add(1, edvoHelper.getMealId());
		allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsWhereMaxIdsAreCampaignRunningMealIDsData")
	public static Object[][] evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsWhereMaxIdsAreCampaignRunningMealIDsData() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "8");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());
		allMealIds.add(1, edvoHelper.getMealId());
		allMealIds.add(2, edvoHelper.getMealId());
		allMealIds.add(3, edvoHelper.getMealId());
		allMealIds.add(4, mealId = edvoHelper.getMealId());
		allMealIds.add(5, edvoHelper.getMealId());
		allMealIds.add(6, edvoHelper.getMealId());
		allMealIds.add(7, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy M1 and Get 15 Percent OFF", "buy G1 and Get 15 Percent OFF")
						.toString());
		allMealsOperationMeta.add(1,
				new CreateEDVOOperationMeta("buy M2 at FinalPrice 299 ", "buy M2 at FinalPrice 299 ").toString());
		allMealsOperationMeta.add(2,
				new CreateEDVOOperationMeta("buy M3 and Get Flat 75 OFF ", "buy M3 and Get Flat 75 OFF ").toString());
		allMealsOperationMeta.add(3,
				new CreateEDVOOperationMeta("buy M4 and  Get 7 Percent OFF ", "buy M4 and  Get 7 Percent OFF ")
						.toString());
		allMealsOperationMeta.add(4, new CreateEDVOOperationMeta("buy 2 items from G1 and  Get 1 item FREE",
				"buy 2 items from G1 and  Get 1 item FREE").toString());
		allMealsOperationMeta.add(5,
				new CreateEDVOOperationMeta("buy G1 and  Get 10% OFF ", "buy G1 and  Get 10% OFF ").toString());
		allMealsOperationMeta.add(6,
				new CreateEDVOOperationMeta("buy G1 and  Get Flat 75 OFF ", "buy G1 and  Get Flat 75 OFF ").toString());
		allMealsOperationMeta.add(7,
				new CreateEDVOOperationMeta("buy G1 at FinalPrice 120 ", "buy G1 at FinalPrice 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allMealRewards.add(1, new CreateReward("NONE", "FinalPrice", "299", "1").toString());
		allMealRewards.add(2, new CreateReward("NONE", "Flat", "75", "1").toString());
		allMealRewards.add(3, new CreateReward("NONE", "Percentage", "7", "1").toString());
		allMealRewards.add(4, new CreateReward().toString());
		allMealRewards.add(5, new CreateReward().toString());
		allMealRewards.add(6, new CreateReward().toString());
		allMealRewards.add(7, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");
		mealCount.add(1, "1");
		mealCount.add(2, "1");
		mealCount.add(3, "1");
		mealCount.add(4, "1");
		mealCount.add(5, "1");
		mealCount.add(6, "1");
		mealCount.add(7, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");
		mealIsParent.add(1, "true");
		mealIsParent.add(2, "true");
		mealIsParent.add(3, "true");
		mealIsParent.add(4, "true");
		mealIsParent.add(5, "true");
		mealIsParent.add(6, "true");
		mealIsParent.add(7, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);
		numberOfGroupsInEachMeal.add(1, 1);
		numberOfGroupsInEachMeal.add(2, 1);
		numberOfGroupsInEachMeal.add(3, 1);
		numberOfGroupsInEachMeal.add(4, 1);
		numberOfGroupsInEachMeal.add(5, 1);
		numberOfGroupsInEachMeal.add(6, 1);
		numberOfGroupsInEachMeal.add(7, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(4, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(6, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(7, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");
		allGroupCounts.add(1, "1");
		allGroupCounts.add(2, "1");
		allGroupCounts.add(3, "1");
		allGroupCounts.add(4, "2");
		allGroupCounts.add(5, "1");
		allGroupCounts.add(6, "1");
		allGroupCounts.add(7, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "100", "1").toString());
		allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "10", "1").toString());
		allGroupsRewards.add(6, new CreateReward("NONE", "Flat", "75", "1").toString());
		allGroupsRewards.add(7, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");
		listOfItemCountInMeal.add(1, "1");
		listOfItemCountInMeal.add(2, "1");
		listOfItemCountInMeal.add(3, "1");
		listOfItemCountInMeal.add(4, "1");
		listOfItemCountInMeal.add(5, "1");
		listOfItemCountInMeal.add(6, "1");
		listOfItemCountInMeal.add(7, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "100");
		listOfAllItemsPrice.add(1, "500");
		listOfAllItemsPrice.add(2, "100");
		listOfAllItemsPrice.add(3, "100");
		listOfAllItemsPrice.add(4, "100");
		listOfAllItemsPrice.add(5, "100");
		listOfAllItemsPrice.add(6, "100");
		listOfAllItemsPrice.add(7, "300");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");
		numOfMealItemRequestInCart.add(1, "1");
		numOfMealItemRequestInCart.add(2, "1");
		numOfMealItemRequestInCart.add(3, "1");
		numOfMealItemRequestInCart.add(4, "1");
		numOfMealItemRequestInCart.add(5, "1");
		numOfMealItemRequestInCart.add(6, "1");
		numOfMealItemRequestInCart.add(7, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(1, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(2, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(3, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(4, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(5, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(6, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(7, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(8, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);
		listOfRestIdsInCart.add(1, restId);
		listOfRestIdsInCart.add(2, restId);
		listOfRestIdsInCart.add(3, restId);
		listOfRestIdsInCart.add(4, restId);
		listOfRestIdsInCart.add(5, restId);
		listOfRestIdsInCart.add(6, restId);
		listOfRestIdsInCart.add(7, restId);
		listOfRestIdsInCart.add(8, restId);

		allMealIds.add(5, mealId);
		allMealIds.add(6, edvoHelper.getMealId());
		allMealIds.add(7, edvoHelper.getMealId());

		allGroupIdsOfMeals.add(5, groupId);
		allGroupIdsOfMeals.add(6, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(7, Integer.toString(Utility.getRandom(1000, 40000)));

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsWhereMaxIdsAreCampaignRunningGroupIDsData")
	public static Object[][] evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsWhereMaxIdsAreCampaignRunningGroupIDs() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "8");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());
		allMealIds.add(1, edvoHelper.getMealId());
		allMealIds.add(2, edvoHelper.getMealId());
		allMealIds.add(3, edvoHelper.getMealId());
		allMealIds.add(4, mealId = edvoHelper.getMealId());
		allMealIds.add(5, edvoHelper.getMealId());
		allMealIds.add(6, edvoHelper.getMealId());
		allMealIds.add(7, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy M1 and Get 15 Percent OFF", "buy G1 and Get 15 Percent OFF")
						.toString());
		allMealsOperationMeta.add(1,
				new CreateEDVOOperationMeta("buy M2 at FinalPrice 299 ", "buy M2 at FinalPrice 299 ").toString());
		allMealsOperationMeta.add(2,
				new CreateEDVOOperationMeta("buy M3 and Get Flat 75 OFF ", "buy M3 and Get Flat 75 OFF ").toString());
		allMealsOperationMeta.add(3,
				new CreateEDVOOperationMeta("buy M4 and  Get 7 Percent OFF ", "buy M4 and  Get 7 Percent OFF ")
						.toString());
		allMealsOperationMeta.add(4, new CreateEDVOOperationMeta("buy 2 items from G1 and  Get 1 item FREE",
				"buy 2 items from G1 and  Get 1 item FREE").toString());
		allMealsOperationMeta.add(5,
				new CreateEDVOOperationMeta("buy G1 and  Get 10% OFF ", "buy G1 and  Get 10% OFF ").toString());
		allMealsOperationMeta.add(6,
				new CreateEDVOOperationMeta("buy G1 and  Get Flat 75 OFF ", "buy G1 and  Get Flat 75 OFF ").toString());
		allMealsOperationMeta.add(7,
				new CreateEDVOOperationMeta("buy G1 at FinalPrice 120 ", "buy G1 at FinalPrice 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allMealRewards.add(1, new CreateReward("NONE", "FinalPrice", "299", "1").toString());
		allMealRewards.add(2, new CreateReward("NONE", "Flat", "75", "1").toString());
		allMealRewards.add(3, new CreateReward("NONE", "Percentage", "7", "1").toString());
		allMealRewards.add(4, new CreateReward().toString());
		allMealRewards.add(5, new CreateReward().toString());
		allMealRewards.add(6, new CreateReward().toString());
		allMealRewards.add(7, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");
		mealCount.add(1, "1");
		mealCount.add(2, "1");
		mealCount.add(3, "1");
		mealCount.add(4, "1");
		mealCount.add(5, "1");
		mealCount.add(6, "1");
		mealCount.add(7, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");
		mealIsParent.add(1, "true");
		mealIsParent.add(2, "true");
		mealIsParent.add(3, "true");
		mealIsParent.add(4, "true");
		mealIsParent.add(5, "true");
		mealIsParent.add(6, "true");
		mealIsParent.add(7, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);
		numberOfGroupsInEachMeal.add(1, 1);
		numberOfGroupsInEachMeal.add(2, 1);
		numberOfGroupsInEachMeal.add(3, 1);
		numberOfGroupsInEachMeal.add(4, 1);
		numberOfGroupsInEachMeal.add(5, 1);
		numberOfGroupsInEachMeal.add(6, 1);
		numberOfGroupsInEachMeal.add(7, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(4, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(6, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(7, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");
		allGroupCounts.add(1, "1");
		allGroupCounts.add(2, "1");
		allGroupCounts.add(3, "1");
		allGroupCounts.add(4, "2");
		allGroupCounts.add(5, "1");
		allGroupCounts.add(6, "1");
		allGroupCounts.add(7, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(4, new CreateReward("MIN", "Percentage", "100", "1").toString());
		allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "10", "1").toString());
		allGroupsRewards.add(6, new CreateReward("NONE", "Flat", "75", "1").toString());
		allGroupsRewards.add(7, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");
		listOfItemCountInMeal.add(1, "1");
		listOfItemCountInMeal.add(2, "1");
		listOfItemCountInMeal.add(3, "1");
		listOfItemCountInMeal.add(4, "1");
		listOfItemCountInMeal.add(5, "1");
		listOfItemCountInMeal.add(6, "1");
		listOfItemCountInMeal.add(7, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "100");
		listOfAllItemsPrice.add(1, "500");
		listOfAllItemsPrice.add(2, "100");
		listOfAllItemsPrice.add(3, "100");
		listOfAllItemsPrice.add(4, "100");
		listOfAllItemsPrice.add(5, "100");
		listOfAllItemsPrice.add(6, "100");
		listOfAllItemsPrice.add(7, "300");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");
		numOfMealItemRequestInCart.add(1, "1");
		numOfMealItemRequestInCart.add(2, "1");
		numOfMealItemRequestInCart.add(3, "1");
		numOfMealItemRequestInCart.add(4, "1");
		numOfMealItemRequestInCart.add(5, "1");
		numOfMealItemRequestInCart.add(6, "1");
		numOfMealItemRequestInCart.add(7, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(1, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(2, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(3, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(4, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(5, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(6, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(7, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);
		listOfRestIdsInCart.add(1, restId);
		listOfRestIdsInCart.add(2, restId);
		listOfRestIdsInCart.add(3, restId);
		listOfRestIdsInCart.add(4, restId);
		listOfRestIdsInCart.add(5, restId);
		listOfRestIdsInCart.add(6, restId);
		listOfRestIdsInCart.add(7, restId);

		allMealIds.add(5, mealId);
		allMealIds.add(0, edvoHelper.getMealId());
		allMealIds.add(1, edvoHelper.getMealId());

		allGroupIdsOfMeals.add(5, groupId);
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsOnlyData")
	public static Object[][] evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsOnly() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "8");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());
		allMealIds.add(1, edvoHelper.getMealId());
		allMealIds.add(2, edvoHelper.getMealId());
		allMealIds.add(3, edvoHelper.getMealId());
		allMealIds.add(4, mealId = edvoHelper.getMealId());
		allMealIds.add(5, edvoHelper.getMealId());
		allMealIds.add(6, edvoHelper.getMealId());
		allMealIds.add(7, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy M1 and Get 15 Percent OFF", "buy G1 and Get 15 Percent OFF")
						.toString());
		allMealsOperationMeta.add(1,
				new CreateEDVOOperationMeta("buy M2 at FinalPrice 299 ", "buy M2 at FinalPrice 299 ").toString());
		allMealsOperationMeta.add(2,
				new CreateEDVOOperationMeta("buy M3 and Get Flat 75 OFF ", "buy M3 and Get Flat 75 OFF ").toString());
		allMealsOperationMeta.add(3,
				new CreateEDVOOperationMeta("buy M4 and  Get 7 Percent OFF ", "buy M4 and  Get 7 Percent OFF ")
						.toString());
		allMealsOperationMeta.add(4, new CreateEDVOOperationMeta("buy 2 items from G1 and  Get 1 item FREE",
				"buy 2 items from G1 and  Get 1 item FREE").toString());
		allMealsOperationMeta.add(5,
				new CreateEDVOOperationMeta("buy G1 and  Get 10% OFF ", "buy G1 and  Get 10% OFF ").toString());
		allMealsOperationMeta.add(6,
				new CreateEDVOOperationMeta("buy G1 and  Get Flat 75 OFF ", "buy G1 and  Get Flat 75 OFF ").toString());
		allMealsOperationMeta.add(7,
				new CreateEDVOOperationMeta("buy G1 at FinalPrice 120 ", "buy G1 at FinalPrice 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allMealRewards.add(1, new CreateReward("NONE", "FinalPrice", "299", "1").toString());
		allMealRewards.add(2, new CreateReward("NONE", "Flat", "75", "1").toString());
		allMealRewards.add(3, new CreateReward("NONE", "Percentage", "7", "1").toString());
		allMealRewards.add(4, new CreateReward().toString());
		allMealRewards.add(5, new CreateReward().toString());
		allMealRewards.add(6, new CreateReward().toString());
		allMealRewards.add(7, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");
		mealCount.add(1, "1");
		mealCount.add(2, "1");
		mealCount.add(3, "1");
		mealCount.add(4, "1");
		mealCount.add(5, "1");
		mealCount.add(6, "1");
		mealCount.add(7, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");
		mealIsParent.add(1, "true");
		mealIsParent.add(2, "true");
		mealIsParent.add(3, "true");
		mealIsParent.add(4, "true");
		mealIsParent.add(5, "true");
		mealIsParent.add(6, "true");
		mealIsParent.add(7, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);
		numberOfGroupsInEachMeal.add(1, 1);
		numberOfGroupsInEachMeal.add(2, 1);
		numberOfGroupsInEachMeal.add(3, 1);
		numberOfGroupsInEachMeal.add(4, 1);
		numberOfGroupsInEachMeal.add(5, 1);
		numberOfGroupsInEachMeal.add(6, 1);
		numberOfGroupsInEachMeal.add(7, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(1, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(2, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(3, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(4, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(5, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(6, Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(7, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");
		allGroupCounts.add(1, "1");
		allGroupCounts.add(2, "1");
		allGroupCounts.add(3, "1");
		allGroupCounts.add(4, "2");
		allGroupCounts.add(5, "1");
		allGroupCounts.add(6, "1");
		allGroupCounts.add(7, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(1, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(2, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(3, new CreateReward("NONE", "Percentage", "15", "1").toString());
		allGroupsRewards.add(4, new CreateReward("NONE", "Percentage", "100", "1").toString());
		allGroupsRewards.add(5, new CreateReward("NONE", "Percentage", "10", "1").toString());
		allGroupsRewards.add(6, new CreateReward("NONE", "Flat", "75", "1").toString());
		allGroupsRewards.add(7, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");
		listOfItemCountInMeal.add(1, "1");
		listOfItemCountInMeal.add(2, "1");
		listOfItemCountInMeal.add(3, "1");
		listOfItemCountInMeal.add(4, "1");
		listOfItemCountInMeal.add(5, "1");
		listOfItemCountInMeal.add(6, "1");
		listOfItemCountInMeal.add(7, "1");
		listOfItemCountInMeal.add(8, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "100");
		listOfAllItemsPrice.add(1, "500");
		listOfAllItemsPrice.add(2, "100");
		listOfAllItemsPrice.add(3, "100");
		listOfAllItemsPrice.add(4, "100");
		listOfAllItemsPrice.add(5, "100");
		listOfAllItemsPrice.add(6, "100");
		listOfAllItemsPrice.add(7, "300");
		listOfAllItemsPrice.add(8, "100");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");
		numOfMealItemRequestInCart.add(1, "1");
		numOfMealItemRequestInCart.add(2, "1");
		numOfMealItemRequestInCart.add(3, "1");
		numOfMealItemRequestInCart.add(4, "1");
		numOfMealItemRequestInCart.add(5, "1");
		numOfMealItemRequestInCart.add(6, "1");
		numOfMealItemRequestInCart.add(7, "1");
		numOfMealItemRequestInCart.add(8, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(1, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(2, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(3, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(4, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(5, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(6, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(7, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(8, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);
		listOfRestIdsInCart.add(1, restId);
		listOfRestIdsInCart.add(2, restId);
		listOfRestIdsInCart.add(3, restId);
		listOfRestIdsInCart.add(4, restId);
		listOfRestIdsInCart.add(5, restId);
		listOfRestIdsInCart.add(6, restId);
		listOfRestIdsInCart.add(7, restId);
		listOfRestIdsInCart.add(8, restId);

		allMealIds.add(8, mealId);

		allGroupIdsOfMeals.add(8, groupId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartForGettingRewardInOneItemIfTwoItemsAddedData")
	public static Object[][] evaluateCartForGettingRewardInOneItemIfTwoItemsAdded() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "2");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("MIN", "Percentage", "10", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		allMealIds.add(1, mealId);
		allGroupIdsOfMeals.add(1, groupId);

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");
		listOfItemCountInMeal.add(1, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");
		listOfAllItemsPrice.add(1, "500");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");
		numOfMealItemRequestInCart.add(1, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(1, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);
		listOfRestIdsInCart.add(1, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithItemIdNullData")
	public static Object[][] evaluateCartWithItemIdNull() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "2");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("MIN", "Percentage", "10", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		allMealIds.add(1, mealId);
		allGroupIdsOfMeals.add(1, groupId);

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");
		listOfItemCountInMeal.add(1, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");
		listOfAllItemsPrice.add(1, "500");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");
		numOfMealItemRequestInCart.add(1, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
		listOfItemIds.add(1, null);

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);
		listOfRestIdsInCart.add(1, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithRestIdOnWhichCampaignIsActiveButMealIdIsDifferentData")
	public static Object[][] evaluateCartWithRestIdOnWhichCampaignIsActiveButMealIdIsDifferent() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		allMealIds.add(0, edvoHelper.getMealId());
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithRestIdOnWhichCampaignIsNotActiveButMealIdIsOnWhichCampIsActiveData")
	public static Object[][] evaluateCartWithRestIdOnWhichCampaignIsNotActiveButMealIdIsOnWhichCampIsActive() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, edvoHelper.getRestId());

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartAndCheckMealLevelRewardIsGettingWhenInCampaignRewardIsSetOnBothMealAndGroupLevelData")
	public static Object[][] evaluateCartAndCheckMealLevelRewardIsGettingWhenInCampaignRewardIsSetOnBothMealAndGroupLevel() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "Percentage", "100", "1").toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "2");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "99", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		allMealIds.add(0, mealId);
		allGroupIdsOfMeals.add(0, groupId);

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "2");

		// list of item's price
		listOfAllItemsPrice.add(0, "500");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWhenItemPriceIsNullData")
	public static Object[][] evaluateCartWhenItemPriceIsNull() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "Percentage", "100", "1").toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "2");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "FinalPrice", "99", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		allMealIds.add(0, mealId);
		allGroupIdsOfMeals.add(0, groupId);

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "2");

		// list of item's price
		listOfAllItemsPrice.add(0, "null");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartFor30DaysDormantUserData")
	public static Object[][] evaluateCartFor30DaysDormantUser() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId = "354990";
		String dormantDays = "31";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp = edvoHelper.createDormantTypeEDVOCampaign("THIRTY_DAYS_DORMANT", listOfRestIds.get(0),
				allMealIds.get(0), allGroupIdsOfMeals.get(0));
		rngHelper.DormantUser(userId, mealItemRequestPayload, dormantDays);

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "500");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", userId);
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload, userId, listOfRestIds.get(0), dormantDays } };
	}

	@DataProvider(name = "evaluateCartFor60DaysDormantUserData")
	public static Object[][] evaluateCartFor60DaysDormantUser() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId = "";
		userId = Integer.toString(Utility.getRandom(100, 25000));
		String dormantDays = "70";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp = edvoHelper.createDormantTypeEDVOCampaign("SIXTY_DAYS_DORMANT", listOfRestIds.get(0),
				allMealIds.get(0), allGroupIdsOfMeals.get(0));
		rngHelper.DormantUser(userId, mealItemRequestPayload, dormantDays);

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "500");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", userId);
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload, userId, listOfRestIds.get(0), dormantDays } };
	}

	@DataProvider(name = "evaluateCartFor90DaysDormantUserData")
	public static Object[][] evaluateCartFor90DaysDormantUser() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId = "";
		userId = Integer.toString(Utility.getRandom(100, 25000));
		String dormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp = edvoHelper.createDormantTypeEDVOCampaign("NINETY_DAYS_DORMANT", listOfRestIds.get(0),
				allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// rngHelper.DormantUser(userId, mealItemRequestPayload, dormantDays);

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "500");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", userId);
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload, userId, listOfRestIds.get(0), dormantDays } };
	}

	@DataProvider(name = "evaluateCartWithRestIdOnWhichCampaignIsActiveButGroupIdIsDifferentData")
	public static Object[][] evaluateCartWithRestIdOnWhichCampaignIsActiveButGroupIdIsDifferent() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId = "";
		userId = Integer.toString(Utility.getRandom(100, 25000));

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "0");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", userId);
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartForFirstorderRestrictionData")
	public static Object[][] evaluateCartForFirstorderRestriction() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		boolean firstOrderRestriction = true;

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp = edvoHelper.createCampaignWithUserCut(false, firstOrderRestriction, false, "ZERO_DAYS_DORMANT",
				new CreateReward("NONE", "Percentage", "30", "1").toString(), new CreateReward().toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "true");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithFirstorderRestrictioFalseAndCampIsForFirstOrderTrueData")
	public static Object[][] evaluateCartWithFirstorderRestrictioFalseAndCampIsForFirstOrderTrue() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		boolean firstOrderRestriction = true;

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp = edvoHelper.createCampaignWithUserCut(false, firstOrderRestriction, false, "ZERO_DAYS_DORMANT",
				new CreateReward("NONE", "Percentage", "30", "1").toString(), new CreateReward().toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithFirstorderRestrictionTrueWhereRestIdIsDiffButMealIdAndGroupIdIsCampRunningData")
	public static Object[][] evaluateCartWithFirstorderRestrictionTrueWhereRestIdIsDiffButMealIdAndGroupIdIsCampRunning() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		boolean firstOrderRestriction = true;

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp = edvoHelper.createCampaignWithUserCut(false, firstOrderRestriction, false, "ZERO_DAYS_DORMANT",
				new CreateReward("NONE", "Percentage", "30", "1").toString(), new CreateReward().toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, edvoHelper.getRestId());

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartWithFirstorderRestrictionTrueWhereMealIdIsDiffButRestIdAndGroupIdIsCampRunningData")
	public static Object[][] evaluateCartWithFirstorderRestrictionTrueWhereMealIdIsDiffButRestIdAndGroupIdIsCampRunning() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayload = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		boolean firstOrderRestriction = true;

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp = edvoHelper.createCampaignWithUserCut(false, firstOrderRestriction, false, "ZERO_DAYS_DORMANT",
				new CreateReward("NONE", "Percentage", "30", "1").toString(), new CreateReward().toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		// list of meal id for cart
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayload.put("0", "10");
		cartPayload.put("1", "false");
		cartPayload.put("2", "");
		cartPayload.put("3", mealItemRequestPayload);
		cartPayload.put("4", "9901");
		cartPayload.put("5", "\"ANDROID\"");
		cartPayload.put("6", "229");

		return new Object[][] { { createEDVOCamp, cartPayload } };
	}

	@DataProvider(name = "evaluateCartForUserCutWith30DaysDormantData")
	public static Object[][] evaluateCartForUserCutWith30DaysDormant() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp30DaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		// HashMap<String, String> createEDVOCamp90DaysDormant = new HashMap<String,
		// String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayloadFor30Days = new HashMap<String, String>();
		// HashMap <String, String> cartPayloadFor60Days = new HashMap<String, String>
		// ();
		// HashMap <String, String> cartPayloadFor90Days = new HashMap<String, String>
		// ();
		HashMap<String, String> cartPayloadFirstOrder = new HashMap<String, String>();
		HashMap<String, String> cartPayloadRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> cartPayloadRandomUser = new HashMap<String, String>();
		HashMap<String, String> cartPayloadPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId = "";
		userId = Integer.toString(Utility.getRandom(100, 25000));
		String dormantDays = "31";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp30DaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "THIRTY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false,
		// false, false, "NINETY_DAYS_DORMANT",listOfRestIds.get(0) , allMealIds.get(0),
		// allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayloadFor30Days.put("0", "10");
		cartPayloadFor30Days.put("1", "false");
		cartPayloadFor30Days.put("2", "");
		cartPayloadFor30Days.put("3", mealItemRequestPayload);
		cartPayloadFor30Days.put("4", userId);
		cartPayloadFor30Days.put("5", "\"ANDROID\"");
		cartPayloadFor30Days.put("6", "229");

		cartPayloadFirstOrder.put("0", "10");
		cartPayloadFirstOrder.put("1", "true");
		cartPayloadFirstOrder.put("2", "");
		cartPayloadFirstOrder.put("3", mealItemRequestPayload);
		cartPayloadFirstOrder.put("4", "9902");
		cartPayloadFirstOrder.put("5", "\"ANDROID\"");
		cartPayloadFirstOrder.put("6", "229");

		cartPayloadRestFirstOrder.put("0", "10");
		cartPayloadRestFirstOrder.put("1", "false");
		cartPayloadRestFirstOrder.put("2", "");
		cartPayloadRestFirstOrder.put("3", mealItemRequestPayload);
		cartPayloadRestFirstOrder.put("4", "9909");
		cartPayloadRestFirstOrder.put("5", "\"ANDROID\"");
		cartPayloadRestFirstOrder.put("6", "229");

		cartPayloadRandomUser.put("0", "10");
		cartPayloadRandomUser.put("1", "false");
		cartPayloadRandomUser.put("2", "");
		cartPayloadRandomUser.put("3", mealItemRequestPayload);
		cartPayloadRandomUser.put("4", "9901");
		cartPayloadRandomUser.put("5", "\"ANDROID\"");
		cartPayloadRandomUser.put("6", "229");

		cartPayloadPublicUser.put("0", "10");
		cartPayloadPublicUser.put("1", "false");
		cartPayloadPublicUser.put("2", "");
		cartPayloadPublicUser.put("3", mealItemRequestPayload);
		cartPayloadPublicUser.put("4", "0");
		cartPayloadPublicUser.put("5", "\"ANDROID\"");
		cartPayloadPublicUser.put("6", "229");

		return new Object[][] { { createEDVOCamp30DaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, cartPayloadFor30Days, cartPayloadFirstOrder,
				cartPayloadRestFirstOrder, cartPayloadRandomUser, cartPayloadPublicUser, userId, restId,
				dormantDays } };
	}

	@DataProvider(name = "evaluateCartForUserCutWith60DaysDormantData")
	public static Object[][] evaluateCartForUserCutWith60DaysDormant() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp60DaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		// HashMap<String, String> createEDVOCamp90DaysDormant = new HashMap<String,
		// String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayloadFor60Days = new HashMap<String, String>();
		// HashMap <String, String> cartPayloadFor60Days = new HashMap<String, String>
		// ();
		// HashMap <String, String> cartPayloadFor90Days = new HashMap<String, String>
		// ();
		HashMap<String, String> cartPayloadFirstOrder = new HashMap<String, String>();
		HashMap<String, String> cartPayloadRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> cartPayloadRandomUser = new HashMap<String, String>();
		HashMap<String, String> cartPayloadPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId = "";
		userId = Integer.toString(Utility.getRandom(100, 25000));
		String dormantDays = "61";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp60DaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "SIXTY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false,
		// false, false, "NINETY_DAYS_DORMANT",listOfRestIds.get(0) , allMealIds.get(0),
		// allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayloadFor60Days.put("0", "10");
		cartPayloadFor60Days.put("1", "false");
		cartPayloadFor60Days.put("2", "");
		cartPayloadFor60Days.put("3", mealItemRequestPayload);
		cartPayloadFor60Days.put("4", userId);
		cartPayloadFor60Days.put("5", "\"ANDROID\"");
		cartPayloadFor60Days.put("6", "229");

		cartPayloadFirstOrder.put("0", "10");
		cartPayloadFirstOrder.put("1", "true");
		cartPayloadFirstOrder.put("2", "");
		cartPayloadFirstOrder.put("3", mealItemRequestPayload);
		cartPayloadFirstOrder.put("4", "9902");
		cartPayloadFirstOrder.put("5", "\"ANDROID\"");
		cartPayloadFirstOrder.put("6", "229");

		cartPayloadRestFirstOrder.put("0", "10");
		cartPayloadRestFirstOrder.put("1", "false");
		cartPayloadRestFirstOrder.put("2", "");
		cartPayloadRestFirstOrder.put("3", mealItemRequestPayload);
		cartPayloadRestFirstOrder.put("4", "9909");
		cartPayloadRestFirstOrder.put("5", "\"ANDROID\"");
		cartPayloadRestFirstOrder.put("6", "229");

		cartPayloadRandomUser.put("0", "10");
		cartPayloadRandomUser.put("1", "false");
		cartPayloadRandomUser.put("2", "");
		cartPayloadRandomUser.put("3", mealItemRequestPayload);
		cartPayloadRandomUser.put("4", "9901");
		cartPayloadRandomUser.put("5", "\"ANDROID\"");
		cartPayloadRandomUser.put("6", "229");

		cartPayloadPublicUser.put("0", "10");
		cartPayloadPublicUser.put("1", "false");
		cartPayloadPublicUser.put("2", "");
		cartPayloadPublicUser.put("3", mealItemRequestPayload);
		cartPayloadPublicUser.put("4", "0");
		cartPayloadPublicUser.put("5", "\"ANDROID\"");
		cartPayloadPublicUser.put("6", "229");

		return new Object[][] { { createEDVOCamp60DaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, cartPayloadFor60Days, cartPayloadFirstOrder,
				cartPayloadRestFirstOrder, cartPayloadRandomUser, cartPayloadPublicUser, userId, restId,
				dormantDays } };
	}

	@DataProvider(name = "evaluateCartForUserCutWith90DaysDormantData")
	public static Object[][] evaluateCartForUserCutWith90DaysDormant() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		HashMap<String, String> createEDVOCamp90DaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayloadFor90Days = new HashMap<String, String>();
		HashMap<String, String> cartPayloadFirstOrder = new HashMap<String, String>();
		HashMap<String, String> cartPayloadRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> cartPayloadRandomUser = new HashMap<String, String>();
		HashMap<String, String> cartPayloadPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId = "";
		userId = Integer.toString(Utility.getRandom(100, 25000));
		String dormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "NINETY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayloadFor90Days.put("0", "10");
		cartPayloadFor90Days.put("1", "false");
		cartPayloadFor90Days.put("2", "");
		cartPayloadFor90Days.put("3", mealItemRequestPayload);
		cartPayloadFor90Days.put("4", userId);
		cartPayloadFor90Days.put("5", "\"ANDROID\"");
		cartPayloadFor90Days.put("6", "229");

		cartPayloadFirstOrder.put("0", "10");
		cartPayloadFirstOrder.put("1", "true");
		cartPayloadFirstOrder.put("2", "");
		cartPayloadFirstOrder.put("3", mealItemRequestPayload);
		cartPayloadFirstOrder.put("4", "9902");
		cartPayloadFirstOrder.put("5", "\"ANDROID\"");
		cartPayloadFirstOrder.put("6", "229");

		cartPayloadRestFirstOrder.put("0", "10");
		cartPayloadRestFirstOrder.put("1", "false");
		cartPayloadRestFirstOrder.put("2", "");
		cartPayloadRestFirstOrder.put("3", mealItemRequestPayload);
		cartPayloadRestFirstOrder.put("4", "9909");
		cartPayloadRestFirstOrder.put("5", "\"ANDROID\"");
		cartPayloadRestFirstOrder.put("6", "229");

		cartPayloadRandomUser.put("0", "10");
		cartPayloadRandomUser.put("1", "false");
		cartPayloadRandomUser.put("2", "");
		cartPayloadRandomUser.put("3", mealItemRequestPayload);
		cartPayloadRandomUser.put("4", "9901");
		cartPayloadRandomUser.put("5", "\"ANDROID\"");
		cartPayloadRandomUser.put("6", "229");

		cartPayloadPublicUser.put("0", "10");
		cartPayloadPublicUser.put("1", "false");
		cartPayloadPublicUser.put("2", "");
		cartPayloadPublicUser.put("3", mealItemRequestPayload);
		cartPayloadPublicUser.put("4", "0");
		cartPayloadPublicUser.put("5", "\"ANDROID\"");
		cartPayloadPublicUser.put("6", "229");

		return new Object[][] { { createEDVOCamp90DaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, cartPayloadFor90Days, cartPayloadFirstOrder,
				cartPayloadRestFirstOrder, cartPayloadRandomUser, cartPayloadPublicUser, userId, restId,
				dormantDays } };
	}

	@DataProvider(name = "evaluateCartForUser3060And90DaysDormantWhenActiveCampFor30DaysData")
	public static Object[][] evaluateCartForUser3060And90DaysDormantWhenActiveCampFor30Days() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp30DaysDormant = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayloadFor90Days = new HashMap<String, String>();
		HashMap<String, String> cartPayloadFor60Days = new HashMap<String, String>();
		HashMap<String, String> cartPayload30Days = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId30Dormant = Integer.toString(Utility.getRandom(100, 25000));
		String userId60Dormant = Integer.toString(Utility.getRandom(100, 25000));
		String userId90Dormant = Integer.toString(Utility.getRandom(100, 25000));
		String thirtyDormantDays = "31";
		String sixtyDormantDays = "61";
		String ninetyDormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp30DaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "THIRTY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayloadFor90Days.put("0", "10");
		cartPayloadFor90Days.put("1", "false");
		cartPayloadFor90Days.put("2", "");
		cartPayloadFor90Days.put("3", mealItemRequestPayload);
		cartPayloadFor90Days.put("4", userId90Dormant);
		cartPayloadFor90Days.put("5", "\"ANDROID\"");
		cartPayloadFor90Days.put("6", "229");

		cartPayloadFor60Days.put("0", "10");
		cartPayloadFor60Days.put("1", "true");
		cartPayloadFor60Days.put("2", "");
		cartPayloadFor60Days.put("3", mealItemRequestPayload);
		cartPayloadFor60Days.put("4", userId60Dormant);
		cartPayloadFor60Days.put("5", "\"ANDROID\"");
		cartPayloadFor60Days.put("6", "229");

		cartPayload30Days.put("0", "10");
		cartPayload30Days.put("1", "false");
		cartPayload30Days.put("2", "");
		cartPayload30Days.put("3", mealItemRequestPayload);
		cartPayload30Days.put("4", userId30Dormant);
		cartPayload30Days.put("5", "\"ANDROID\"");
		cartPayload30Days.put("6", "229");

		return new Object[][] { { createEDVOCamp30DaysDormant, cartPayload30Days, cartPayloadFor60Days,
				cartPayloadFor90Days, userId30Dormant, userId60Dormant, userId90Dormant, restId, thirtyDormantDays,
				sixtyDormantDays, ninetyDormantDays } };
	}

	@DataProvider(name = "evaluateCartForUser3060And90DaysDormantWhenActiveCampFor60DaysData")
	public static Object[][] evaluateCartForUser3060And90DaysDormantWhenActiveCampFor60Days() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp60DaysDormant = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayloadFor90Days = new HashMap<String, String>();
		HashMap<String, String> cartPayloadFor60Days = new HashMap<String, String>();
		HashMap<String, String> cartPayload30Days = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId30Dormant = Integer.toString(Utility.getRandom(100, 25000));
		String userId60Dormant = Integer.toString(Utility.getRandom(100, 25000));
		String userId90Dormant = Integer.toString(Utility.getRandom(100, 25000));
		String thirtyDormantDays = "31";
		String sixtyDormantDays = "61";
		String ninetyDormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp60DaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "SIXTY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "75", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayloadFor90Days.put("0", "10");
		cartPayloadFor90Days.put("1", "false");
		cartPayloadFor90Days.put("2", "");
		cartPayloadFor90Days.put("3", mealItemRequestPayload);
		cartPayloadFor90Days.put("4", userId90Dormant);
		cartPayloadFor90Days.put("5", "\"ANDROID\"");
		cartPayloadFor90Days.put("6", "229");

		cartPayloadFor60Days.put("0", "10");
		cartPayloadFor60Days.put("1", "true");
		cartPayloadFor60Days.put("2", "");
		cartPayloadFor60Days.put("3", mealItemRequestPayload);
		cartPayloadFor60Days.put("4", userId60Dormant);
		cartPayloadFor60Days.put("5", "\"ANDROID\"");
		cartPayloadFor60Days.put("6", "229");

		cartPayload30Days.put("0", "10");
		cartPayload30Days.put("1", "false");
		cartPayload30Days.put("2", "");
		cartPayload30Days.put("3", mealItemRequestPayload);
		cartPayload30Days.put("4", userId30Dormant);
		cartPayload30Days.put("5", "\"ANDROID\"");
		cartPayload30Days.put("6", "229");

		return new Object[][] { { createEDVOCamp60DaysDormant, cartPayload30Days, cartPayloadFor60Days,
				cartPayloadFor90Days, userId30Dormant, userId60Dormant, userId90Dormant, restId, thirtyDormantDays,
				sixtyDormantDays, ninetyDormantDays } };
	}

	@DataProvider(name = "evaluateCartForUser3060And90DaysDormantWhenActiveCampFor90DaysData")
	public static Object[][] evaluateCartForUser3060And90DaysDormantWhenActiveCampFor90Days() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp90DaysDormant = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> cartPayloadFor90Days = new HashMap<String, String>();
		HashMap<String, String> cartPayloadFor60Days = new HashMap<String, String>();
		HashMap<String, String> cartPayload30Days = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		String userId30Dormant = Integer.toString(Utility.getRandom(100, 25000));
		String userId60Dormant = Integer.toString(Utility.getRandom(100, 25000));
		String userId90Dormant = Integer.toString(Utility.getRandom(100, 25000));
		String thirtyDormantDays = "31";
		String sixtyDormantDays = "61";
		String ninetyDormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "NINETY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "175", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		cartPayloadFor90Days.put("0", "10");
		cartPayloadFor90Days.put("1", "false");
		cartPayloadFor90Days.put("2", "");
		cartPayloadFor90Days.put("3", mealItemRequestPayload);
		cartPayloadFor90Days.put("4", userId90Dormant);
		cartPayloadFor90Days.put("5", "\"ANDROID\"");
		cartPayloadFor90Days.put("6", "229");

		cartPayloadFor60Days.put("0", "10");
		cartPayloadFor60Days.put("1", "true");
		cartPayloadFor60Days.put("2", "");
		cartPayloadFor60Days.put("3", mealItemRequestPayload);
		cartPayloadFor60Days.put("4", userId60Dormant);
		cartPayloadFor60Days.put("5", "\"ANDROID\"");
		cartPayloadFor60Days.put("6", "229");

		cartPayload30Days.put("0", "10");
		cartPayload30Days.put("1", "false");
		cartPayload30Days.put("2", "");
		cartPayload30Days.put("3", mealItemRequestPayload);
		cartPayload30Days.put("4", userId30Dormant);
		cartPayload30Days.put("5", "\"ANDROID\"");
		cartPayload30Days.put("6", "229");

		return new Object[][] { { createEDVOCamp90DaysDormant, cartPayload30Days, cartPayloadFor60Days,
				cartPayloadFor90Days, userId30Dormant, userId60Dormant, userId90Dormant, restId, thirtyDormantDays,
				sixtyDormantDays, ninetyDormantDays } };
	}

	// @DataProvider(name =
	// "evaluateCartForMinCartAmountWhenActiveCampForThirtyDaysWithSomeMinCartData")
	// public static Object[][]
	// evaluateCartForMinCartAmountWhenActiveCampForThirtyDaysWithSomeMinCar() {
	// ArrayList<String> listOfRestIds = new ArrayList();
	// ArrayList<String> allMealIds = new ArrayList();
	// ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
	// HashMap<String, String> createEDVOCamp30DaysDormant = new HashMap<String,
	// String>();
	// ArrayList <String> listOfItemIds= new ArrayList<String>();
	// HashMap <String, String> cartPayloadFor90Days = new HashMap<String, String>
	// ();
	// HashMap <String, String> cartPayloadFor60Days = new HashMap<String, String>
	// ();
	// HashMap <String, String> cartPayload30Days = new HashMap<String, String> ();
	// String mealItemRequestPayload= "";
	// String userId30Dormant=Integer.toString(Utility.getRandom(100, 25000));
	// String userId60Dormant=Integer.toString(Utility.getRandom(100, 25000));
	// String userId90Dormant=Integer.toString(Utility.getRandom(100, 25000));
	// String thirtyDormantDays= "31";
	// String sixtyDormantDays= "61";
	// String ninetyDormantDays= "91";
	// String minCartAMount= "99";
	//
	// // listOfAllRestaurantIds
	// listOfRestIds.add(0, restId=Integer.toString(Utility.getRandom(10000,
	// 500000)));
	//
	// // list of All MealIds
	// allMealIds.add(0, mealId=edvoHelper.getMealId());
	//
	// // list of groupIds in Meal
	// allGroupIdsOfMeals.add(0, groupId=Integer.toString(Utility.getRandom(1000,
	// 40000)));
	//
	// createEDVOCamp30DaysDormant =
	// edvoHelper.createCampaignWithUserCutAndMinCartAmount(false, false, false,
	// "THIRTY_DAYS_DORMANT",new CreateReward().toString(),new CreateReward("NONE",
	// "FinalPrice", "175", "1").toString(),listOfRestIds.get(0) ,
	// allMealIds.get(0),
	// allGroupIdsOfMeals.get(0),minCartAMount);
	//
	//
	// ArrayList <String> listOfAllItemsPrice = new ArrayList < String> ();
	// ArrayList <String> listOfItemCountInMeal= new ArrayList <String> ();
	// ArrayList <String> numOfMealItemRequestInCart =new ArrayList <String> ();
	// ArrayList <String> listOfRestIdsInCart = new ArrayList <String> ();
	//
	//
	// //list of item count in each meal
	// listOfItemCountInMeal.add(0, "1");
	//
	// // list of item's price
	// listOfAllItemsPrice.add(0, "400");
	//
	// //numOfMealItemRequestInCart
	// numOfMealItemRequestInCart.add(0, "1");
	//
	// //list Of item Ids
	// listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));
	//
	// // listOfAllRestaurantIds
	// listOfRestIdsInCart.add(0, restId);
	//
	// mealItemRequestPayload =
	// edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
	// listOfItemIds,listOfRestIdsInCart,
	// listOfAllItemsPrice,listOfItemCountInMeal,allGroupIdsOfMeals);
	// cartPayloadFor90Days.put("0", minCartAMount);
	// cartPayloadFor90Days.put("1", "false");
	// cartPayloadFor90Days.put("2", "");
	// cartPayloadFor90Days.put("3",mealItemRequestPayload);
	// cartPayloadFor90Days.put("4", userId90Dormant);
	// cartPayloadFor90Days.put("5", "\"ANDROID\"");
	// cartPayloadFor90Days.put("6", "229");
	//
	// cartPayloadFor60Days.put("0", minCartAMount);
	// cartPayloadFor60Days.put("1", "true");
	// cartPayloadFor60Days.put("2", "");
	// cartPayloadFor60Days.put("3",mealItemRequestPayload);
	// cartPayloadFor60Days.put("4", userId60Dormant);
	// cartPayloadFor60Days.put("5", "\"ANDROID\"");
	// cartPayloadFor60Days.put("6", "229");
	//
	// cartPayload30Days.put("0", minCartAMount);
	// cartPayload30Days.put("1", "false");
	// cartPayload30Days.put("2", "");
	// cartPayload30Days.put("3",mealItemRequestPayload);
	// cartPayload30Days.put("4", userId30Dormant);
	// cartPayload30Days.put("5", "\"ANDROID\"");
	// cartPayload30Days.put("6", "229");
	//
	//
	// return new Object[][] { { createEDVOCamp30DaysDormant,cartPayload30Days,
	// cartPayloadFor60Days,cartPayloadFor90Days,
	// userId30Dormant,userId60Dormant,userId90Dormant, restId,
	// thirtyDormantDays,sixtyDormantDays,ninetyDormantDays } };
	// }

	@DataProvider(name = "evaluateListingForActiveCamapignData")
	public static Object[][] evaluateListingForActiveCamapign() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayload = new HashMap<String, String>();
		String userId = Integer.toString(Utility.getRandom(2500, 7500));

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

		// 86400000 - next day time and 300000 is equal to 5 minutes and Dormant user
		// type (19) and user restriction (21)
		createEDVOCamp.put("0", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("1", "\"EDVO_TEST_Automation\"");
		createEDVOCamp.put("2", "\"EDVO_TEST_Automation Description\"");
		createEDVOCamp.put("3", "\"EDVO_Short Description\"");
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(86400000));
		createEDVOCamp.put("6", "\"BXGY\"");
		createEDVOCamp.put("7", "100");
		createEDVOCamp.put("8", "true");
		createEDVOCamp.put("9", "0");
		createEDVOCamp.put("10", "\"AY\"");
		createEDVOCamp.put("11", "120");
		createEDVOCamp.put("12", "0");
		createEDVOCamp.put("13",
				edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards));

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", "false");
		createEDVOCamp.put("16", "false");
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", "false");
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		listingPayload.put("0", restId);
		listingPayload.put("1", "false");
		listingPayload.put("2", userId);
		listingPayload.put("3", "ANDROID");
		listingPayload.put("4", "229");

		return new Object[][] { { createEDVOCamp, listingPayload } };
	}

	@DataProvider(name = "evaluateListingForAllPossibleTypeOfActiveCampaignData")
	public static Object[][] evaluateListingForAllPossibleTypeOfActiveCampaign() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createPercentageTypeEDVOCamp = new HashMap<String, String>();
		HashMap<String, String> createFinalPriceTypeEDVOCamp = new HashMap<String, String>();
		HashMap<String, String> createFlatEDVOCamp = new HashMap<String, String>();
		HashMap<String, String> createFreeEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForPercentage = new HashMap<String, String>();
		String userId = Integer.toString(Utility.getRandom(2500, 7500));

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1, edvoHelper.getRestId());
		listOfRestIds.add(2, edvoHelper.getRestId());
		listOfRestIds.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());
		allMealIds.add(1, edvoHelper.getMealId());
		allMealIds.add(2, edvoHelper.getMealId());
		allMealIds.add(3, edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(1, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(2, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(3, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createPercentageTypeEDVOCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward("NONE", "Percentage", "10", "1").toString(), new CreateReward().toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		createFinalPriceTypeEDVOCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward("NONE", "FinalPrice", "175", "1").toString(), new CreateReward().toString(),
				listOfRestIds.get(1), allMealIds.get(1), allGroupIdsOfMeals.get(1));

		createFlatEDVOCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward("NONE", "Flat", "65", "1").toString(), new CreateReward().toString(),
				listOfRestIds.get(2), allMealIds.get(2), allGroupIdsOfMeals.get(2));

		createFreeEDVOCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward("NONE", "Percentage", "100", "1").toString(), new CreateReward().toString(),
				listOfRestIds.get(3), allMealIds.get(3), allGroupIdsOfMeals.get(3));

		listingPayloadForPercentage.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForPercentage.put("1", "false");
		listingPayloadForPercentage.put("2", userId);
		listingPayloadForPercentage.put("3", "ANDROID");
		listingPayloadForPercentage.put("4", "229");

		return new Object[][] { { createPercentageTypeEDVOCamp, createFinalPriceTypeEDVOCamp, createFlatEDVOCamp,
				createFreeEDVOCamp, listingPayloadForPercentage } };
	}

	@DataProvider(name = "evaluateListingAndCheckDisableCampaignIsNotGettingData")
	public static Object[][] evaluateListingAndCheckDisableCampaignIsNotGetting() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createPercentageTypeEDVOCamp = new HashMap<String, String>();
		HashMap<String, String> createFinalPriceTypeEDVOCamp = new HashMap<String, String>();
		HashMap<String, String> createFlatEDVOCamp = new HashMap<String, String>();
		HashMap<String, String> createFreeEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForPercentage = new HashMap<String, String>();
		String userId = Integer.toString(Utility.getRandom(2500, 7500));

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1, edvoHelper.getRestId());
		listOfRestIds.add(2, edvoHelper.getRestId());
		listOfRestIds.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());
		allMealIds.add(1, edvoHelper.getMealId());
		allMealIds.add(2, edvoHelper.getMealId());
		allMealIds.add(3, edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(1, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(2, groupId = Integer.toString(Utility.getRandom(1000, 40000)));
		allGroupIdsOfMeals.add(3, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createPercentageTypeEDVOCamp = edvoHelper.createCampaignWithUserCutAndTimeValidity(false, false, false,
				"ZERO_DAYS_DORMANT", new CreateReward("NONE", "Percentage", "10", "1").toString(),
				new CreateReward().toString(), listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0),
				-86400000, -2000);

		createFinalPriceTypeEDVOCamp = edvoHelper.createCampaignWithUserCutAndTimeValidity(false, false, false,
				"ZERO_DAYS_DORMANT", new CreateReward("NONE", "FinalPrice", "175", "1").toString(),
				new CreateReward().toString(), listOfRestIds.get(1), allMealIds.get(1), allGroupIdsOfMeals.get(1),
				-86400000, -2000);

		createFlatEDVOCamp = edvoHelper.createCampaignWithUserCutAndTimeValidity(false, false, false,
				"ZERO_DAYS_DORMANT", new CreateReward("NONE", "Flat", "65", "1").toString(),
				new CreateReward().toString(), listOfRestIds.get(2), allMealIds.get(2), allGroupIdsOfMeals.get(2),
				-86400000, -2000);

		createFreeEDVOCamp = edvoHelper.createCampaignWithUserCutAndTimeValidity(false, false, false,
				"ZERO_DAYS_DORMANT", new CreateReward("NONE", "Percentage", "100", "1").toString(),
				new CreateReward().toString(), listOfRestIds.get(3), allMealIds.get(3), allGroupIdsOfMeals.get(3),
				-86400000, -2000);

		listingPayloadForPercentage.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForPercentage.put("1", "false");
		listingPayloadForPercentage.put("2", userId);
		listingPayloadForPercentage.put("3", "ANDROID");
		listingPayloadForPercentage.put("4", "229");

		return new Object[][] { { createPercentageTypeEDVOCamp, createFinalPriceTypeEDVOCamp, createFlatEDVOCamp,
				createFreeEDVOCamp, listingPayloadForPercentage } };
	}

	@DataProvider(name = "evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForThrityDormantData")
	public static Object[][] evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForThrityDormant() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createThirtyDormantTypeEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForThirtyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForSixtyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForNinetyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPublicPayload = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();
		ArrayList<String> dormantDays = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(3, "0");

		// dormant days
		dormantDays.add(0, "31");
		dormantDays.add(1, "61");
		dormantDays.add(2, "91");

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());
		listOfRestIds.add(1, edvoHelper.getRestId());
		listOfRestIds.add(2, edvoHelper.getRestId());
		listOfRestIds.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createThirtyDormantTypeEDVOCamp = edvoHelper.createCampaignWithUserCut(false, false, false,
				"THIRTY_DAYS_DORMANT", new CreateReward("NONE", "Percentage", "10", "1").toString(),
				new CreateReward().toString(), restId, allMealIds.get(0), allGroupIdsOfMeals.get(0));

		listingPayloadForThirtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForThirtyDormant.put("1", "false");
		listingPayloadForThirtyDormant.put("2", userIds.get(0));
		listingPayloadForThirtyDormant.put("3", "ANDROID");
		listingPayloadForThirtyDormant.put("4", "229");

		listingPayloadForSixtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForSixtyDormant.put("1", "false");
		listingPayloadForSixtyDormant.put("2", userIds.get(1));
		listingPayloadForSixtyDormant.put("3", "ANDROID");
		listingPayloadForSixtyDormant.put("4", "229");

		listingPayloadForNinetyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNinetyDormant.put("1", "false");
		listingPayloadForNinetyDormant.put("2", userIds.get(2));
		listingPayloadForNinetyDormant.put("3", "ANDROID");
		listingPayloadForNinetyDormant.put("4", "229");

		listingPublicPayload.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + "," + listOfRestIds.get(2)
				+ "," + listOfRestIds.get(3));
		listingPublicPayload.put("1", "false");
		listingPublicPayload.put("2", userIds.get(3));
		listingPublicPayload.put("3", "ANDROID");
		listingPublicPayload.put("4", "229");

		return new Object[][] {
				{ createThirtyDormantTypeEDVOCamp, listingPayloadForThirtyDormant, listingPayloadForSixtyDormant,
						listingPayloadForNinetyDormant, listingPublicPayload, userIds, restId, dormantDays } };
	}

	@DataProvider(name = "evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForSixtyDormantData")
	public static Object[][] evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForNinetyDormant() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createSixtyDormantTypeEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForThirtyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForSixtyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForNinetyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPublicPayload = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();
		ArrayList<String> dormantDays = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(3, "0");

		// dormant days
		dormantDays.add(0, "31");
		dormantDays.add(1, "61");
		dormantDays.add(2, "91");

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());
		listOfRestIds.add(1, edvoHelper.getRestId());
		listOfRestIds.add(2, edvoHelper.getRestId());
		listOfRestIds.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createSixtyDormantTypeEDVOCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "SIXTY_DAYS_DORMANT",
				new CreateReward("NONE", "FinalPrice", "145", "1").toString(), new CreateReward().toString(), restId,
				allMealIds.get(0), allGroupIdsOfMeals.get(0));

		listingPayloadForThirtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForThirtyDormant.put("1", "false");
		listingPayloadForThirtyDormant.put("2", userIds.get(0));
		listingPayloadForThirtyDormant.put("3", "ANDROID");
		listingPayloadForThirtyDormant.put("4", "229");

		listingPayloadForSixtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForSixtyDormant.put("1", "false");
		listingPayloadForSixtyDormant.put("2", userIds.get(1));
		listingPayloadForSixtyDormant.put("3", "ANDROID");
		listingPayloadForSixtyDormant.put("4", "229");

		listingPayloadForNinetyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNinetyDormant.put("1", "false");
		listingPayloadForNinetyDormant.put("2", userIds.get(2));
		listingPayloadForNinetyDormant.put("3", "ANDROID");
		listingPayloadForNinetyDormant.put("4", "229");

		listingPublicPayload.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + "," + listOfRestIds.get(2)
				+ "," + listOfRestIds.get(3));
		listingPublicPayload.put("1", "false");
		listingPublicPayload.put("2", userIds.get(3));
		listingPublicPayload.put("3", "ANDROID");
		listingPublicPayload.put("4", "229");

		return new Object[][] {
				{ createSixtyDormantTypeEDVOCamp, listingPayloadForThirtyDormant, listingPayloadForSixtyDormant,
						listingPayloadForNinetyDormant, listingPublicPayload, userIds, restId, dormantDays } };
	}

	@DataProvider(name = "evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForNinetyDormantData")
	public static Object[][] evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForNinetyDormantData() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createNinetyDormantTypeEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForThirtyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForSixtyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForNinetyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPublicPayload = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();
		ArrayList<String> dormantDays = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(3, "0");

		// dormant days
		dormantDays.add(0, "31");
		dormantDays.add(1, "61");
		dormantDays.add(2, "91");

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());
		listOfRestIds.add(1, edvoHelper.getRestId());
		listOfRestIds.add(2, edvoHelper.getRestId());
		listOfRestIds.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createNinetyDormantTypeEDVOCamp = edvoHelper.createCampaignWithUserCut(false, false, false,
				"SIXTY_DAYS_DORMANT", new CreateReward("NONE", "FinalPrice", "145", "1").toString(),
				new CreateReward().toString(), restId, allMealIds.get(0), allGroupIdsOfMeals.get(0));

		listingPayloadForThirtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForThirtyDormant.put("1", "false");
		listingPayloadForThirtyDormant.put("2", userIds.get(0));
		listingPayloadForThirtyDormant.put("3", "ANDROID");
		listingPayloadForThirtyDormant.put("4", "229");

		listingPayloadForSixtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForSixtyDormant.put("1", "false");
		listingPayloadForSixtyDormant.put("2", userIds.get(1));
		listingPayloadForSixtyDormant.put("3", "ANDROID");
		listingPayloadForSixtyDormant.put("4", "229");

		listingPayloadForNinetyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNinetyDormant.put("1", "false");
		listingPayloadForNinetyDormant.put("2", userIds.get(2));
		listingPayloadForNinetyDormant.put("3", "ANDROID");
		listingPayloadForNinetyDormant.put("4", "229");

		listingPublicPayload.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + "," + listOfRestIds.get(2)
				+ "," + listOfRestIds.get(3));
		listingPublicPayload.put("1", "false");
		listingPublicPayload.put("2", userIds.get(3));
		listingPublicPayload.put("3", "ANDROID");
		listingPublicPayload.put("4", "229");

		return new Object[][] {
				{ createNinetyDormantTypeEDVOCamp, listingPayloadForThirtyDormant, listingPayloadForSixtyDormant,
						listingPayloadForNinetyDormant, listingPublicPayload, userIds, restId, dormantDays } };
	}

	@DataProvider(name = "evaluateListingForUserFirstOrderAndNonFirstOrderUserIdWhenActiveCampaignIsAlsoForUserFirstOrderData")
	public static Object[][] evaluateListingForUserFirstOrderAndNonFirstOrderUserIdWhenActiveCampaignIsAlsoForUserFirstOrder() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createFirstOrderTypeEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForNonFirstOrder = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();
		ArrayList<String> dormantDays = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());
		listOfRestIds.add(1, edvoHelper.getRestId());
		listOfRestIds.add(2, edvoHelper.getRestId());
		listOfRestIds.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createFirstOrderTypeEDVOCamp = edvoHelper.createUserCutMultiTD(false, true, false, "ZERO_DAYS_DORMANT", restId,
				mealId);

		listingPayloadForFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForFirstOrder.put("1", "true");
		listingPayloadForFirstOrder.put("2", userIds.get(0));
		listingPayloadForFirstOrder.put("3", "ANDROID");
		listingPayloadForFirstOrder.put("4", "229");

		listingPayloadForNonFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNonFirstOrder.put("1", "false");
		listingPayloadForNonFirstOrder.put("2", userIds.get(1));
		listingPayloadForNonFirstOrder.put("3", "ANDROID");
		listingPayloadForNonFirstOrder.put("4", "229");

		return new Object[][] {
				{ createFirstOrderTypeEDVOCamp, listingPayloadForFirstOrder, listingPayloadForNonFirstOrder } };
	}

	@DataProvider(name = "evaluateListingForRestaurantOrderAndNonRestFIrstOrderUserIdWhenActiveCampaignIsForRestaurantFirstOrderData")
	public static Object[][] evaluateListingForRestaurantOrderAndNonRestFIrstOrderUserIdWhenActiveCampaignIsForRestaurantFirstOrder() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createFirstOrderTypeEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForNonRestFirstOrder = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();
		ArrayList<String> dormantDays = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, "0");

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());
		listOfRestIds.add(1, edvoHelper.getRestId());
		listOfRestIds.add(2, edvoHelper.getRestId());
		listOfRestIds.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createFirstOrderTypeEDVOCamp = edvoHelper.createUserCutMultiTD(false, false, true, "ZERO_DAYS_DORMANT", restId,
				mealId);

		listingPayloadForRestFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRestFirstOrder.put("1", "false");
		listingPayloadForRestFirstOrder.put("2", userIds.get(0));
		listingPayloadForRestFirstOrder.put("3", "ANDROID");
		listingPayloadForRestFirstOrder.put("4", "229");

		listingPayloadForNonRestFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNonRestFirstOrder.put("1", "false");
		listingPayloadForNonRestFirstOrder.put("2", userIds.get(1));
		listingPayloadForNonRestFirstOrder.put("3", "ANDROID");
		listingPayloadForNonRestFirstOrder.put("4", "229");

		return new Object[][] {
				{ createFirstOrderTypeEDVOCamp, listingPayloadForRestFirstOrder, listingPayloadForNonRestFirstOrder } };
	}

	@DataProvider(name = "evaluateListingWithInTheTimeSlotOfCampaignData")
	public static Object[][] evaluateListingWithInTheTimeSlotOfCampaign() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> listOfRestIdsInListing = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadWithInTimeSlot = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();
		ArrayList<String> dormantDays = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, "0");

		// listOfAllRestaurantIds
		listOfRestIdsInListing.add(0, restId = edvoHelper.getRestId());
		listOfRestIdsInListing.add(1, edvoHelper.getRestId());
		listOfRestIdsInListing.add(2, edvoHelper.getRestId());
		listOfRestIdsInListing.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCampWithTimeSlot = new HashMap<String, String>();
		String discount = "";
		String userId = Integer.toString(Utility.getRandom(2500, 7500));

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId);

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

		discount = edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
				allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
				allGroupCounts, allGroupIdsOfMeals, allGroupsRewards);

		createEDVOCampWithTimeSlot = edvoHelper.createEDVOWithDefaultValues(discount, "ALL", 1000, 2359, 86400000);

		listingPayloadWithInTimeSlot.put("0", listOfRestIdsInListing.get(0) + "," + listOfRestIdsInListing.get(1) + ","
				+ listOfRestIdsInListing.get(2) + "," + listOfRestIdsInListing.get(3));
		listingPayloadWithInTimeSlot.put("1", "false");
		listingPayloadWithInTimeSlot.put("2", userIds.get(0));
		listingPayloadWithInTimeSlot.put("3", "ANDROID");
		listingPayloadWithInTimeSlot.put("4", "229");

		return new Object[][] { { createEDVOCampWithTimeSlot, listingPayloadWithInTimeSlot } };
	}

	@DataProvider(name = "evaluateListingOutOfTheTimeSlotOfCampaignData")
	public static Object[][] evaluateListingOutOfTheTimeSlotOfCampaign() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> listOfRestIdsInListing = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadWithInTimeSlot = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();
		ArrayList<String> dormantDays = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));

		// listOfAllRestaurantIds
		listOfRestIdsInListing.add(0, restId = edvoHelper.getRestId());
		listOfRestIdsInListing.add(1, edvoHelper.getRestId());
		listOfRestIdsInListing.add(2, edvoHelper.getRestId());
		listOfRestIdsInListing.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCampWithTimeSlot = new HashMap<String, String>();
		String discount = "";
		String userId = Integer.toString(Utility.getRandom(2500, 7500));

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId);

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 and Get 10 Percent OFF", "buy G1 and Get 10 Percent OFF")
						.toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward().toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward("NONE", "Percentage", "10", "1").toString());

		discount = edvoHelper.discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
				allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
				allGroupCounts, allGroupIdsOfMeals, allGroupsRewards);

		createEDVOCampWithTimeSlot = edvoHelper.createEDVOWithDefaultValues(discount, "ALL", 1201, 1205, 86400000);
		listingPayloadWithInTimeSlot.put("0", listOfRestIdsInListing.get(0) + "," + listOfRestIdsInListing.get(1) + ","
				+ listOfRestIdsInListing.get(2) + "," + listOfRestIdsInListing.get(3));
		listingPayloadWithInTimeSlot.put("1", "false");
		listingPayloadWithInTimeSlot.put("2", userIds.get(0));
		listingPayloadWithInTimeSlot.put("3", "ANDROID");
		listingPayloadWithInTimeSlot.put("4", "229");

		return new Object[][] { { createEDVOCampWithTimeSlot, listingPayloadWithInTimeSlot } };
	}

	@DataProvider(name = "evaluateListingToCheckCampaignOutOfValidityData")
	public static Object[][] evaluateListingToCheckCampaignOutOfValidity() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> listOfRestIdsInListing = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadOutOfValidity = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));

		// listOfAllRestaurantIds
		listOfRestIdsInListing.add(0, restId = edvoHelper.getRestId());
		listOfRestIdsInListing.add(1, edvoHelper.getRestId());
		listOfRestIdsInListing.add(2, edvoHelper.getRestId());
		listOfRestIdsInListing.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		String discount = "";
		String userId = Integer.toString(Utility.getRandom(2500, 7500));

		createEDVOCamp = edvoHelper.createCampaignWithUserCutAndTimeValidity(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "10", "1").toString(), restId,
				mealId, groupId, -2000, 0);
		listingPayloadOutOfValidity.put("0", listOfRestIdsInListing.get(0) + "," + listOfRestIdsInListing.get(1) + ","
				+ listOfRestIdsInListing.get(2) + "," + listOfRestIdsInListing.get(3));
		listingPayloadOutOfValidity.put("1", "false");
		listingPayloadOutOfValidity.put("2", userIds.get(0));
		listingPayloadOutOfValidity.put("3", "ANDROID");
		listingPayloadOutOfValidity.put("4", "229");

		return new Object[][] { { createEDVOCamp, listingPayloadOutOfValidity } };
	}

	@DataProvider(name = "evaluateListingToCheckCampaignWithInValidityData")
	public static Object[][] evaluateListingToCheckCampaignWithInValidity() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> listOfRestIdsInListing = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadWithInValidity = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));

		// listOfAllRestaurantIds
		listOfRestIdsInListing.add(0, restId = edvoHelper.getRestId());
		listOfRestIdsInListing.add(1, edvoHelper.getRestId());
		listOfRestIdsInListing.add(2, edvoHelper.getRestId());
		listOfRestIdsInListing.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		String discount = "";
		String userId = Integer.toString(Utility.getRandom(2500, 7500));

		createEDVOCamp = edvoHelper.createCampaignWithUserCutAndTimeValidity(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "10", "1").toString(), restId,
				mealId, groupId, -2000, 300000);
		listingPayloadWithInValidity.put("0", listOfRestIdsInListing.get(0) + "," + listOfRestIdsInListing.get(1) + ","
				+ listOfRestIdsInListing.get(2) + "," + listOfRestIdsInListing.get(3));
		listingPayloadWithInValidity.put("1", "false");
		listingPayloadWithInValidity.put("2", userIds.get(0));
		listingPayloadWithInValidity.put("3", "ANDROID");
		listingPayloadWithInValidity.put("4", "229");

		return new Object[][] { { createEDVOCamp, listingPayloadWithInValidity } };
	}

	@DataProvider(name = "evaluateListingToCheckPublicTypeCampIsVisibleToAllTypeOfUsersWhenNoOtherUserCutCampIsActiveData")
	public static Object[][] evaluateListingToCheckPublicTypeCampIsVisibleToAllTypeOfUsersWhenNoOtherUserCutCampIsActive() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createPublicTypeEDVOCamp = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForThirtyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForSixtyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForNinetyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPublicPayload = new HashMap<String, String>();
		ArrayList<String> userIds = new ArrayList<String>();
		ArrayList<String> dormantDays = new ArrayList<String>();

		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(3, "0");

		// dormant days
		dormantDays.add(0, "31");
		dormantDays.add(1, "61");
		dormantDays.add(2, "91");

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId = edvoHelper.getRestId());
		listOfRestIds.add(1, edvoHelper.getRestId());
		listOfRestIds.add(2, edvoHelper.getRestId());
		listOfRestIds.add(3, edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createPublicTypeEDVOCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward("NONE", "Flat", "45", "1").toString(), new CreateReward().toString(), restId,
				allMealIds.get(0), allGroupIdsOfMeals.get(0));

		listingPayloadForThirtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForThirtyDormant.put("1", "false");
		listingPayloadForThirtyDormant.put("2", userIds.get(0));
		listingPayloadForThirtyDormant.put("3", "ANDROID");
		listingPayloadForThirtyDormant.put("4", "229");

		listingPayloadForSixtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForSixtyDormant.put("1", "false");
		listingPayloadForSixtyDormant.put("2", userIds.get(1));
		listingPayloadForSixtyDormant.put("3", "ANDROID");
		listingPayloadForSixtyDormant.put("4", "229");

		listingPayloadForNinetyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNinetyDormant.put("1", "false");
		listingPayloadForNinetyDormant.put("2", userIds.get(2));
		listingPayloadForNinetyDormant.put("3", "ANDROID");
		listingPayloadForNinetyDormant.put("4", "229");

		listingPublicPayload.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + "," + listOfRestIds.get(2)
				+ "," + listOfRestIds.get(3));
		listingPublicPayload.put("1", "false");
		listingPublicPayload.put("2", userIds.get(3));
		listingPublicPayload.put("3", "ANDROID");
		listingPublicPayload.put("4", "229");

		return new Object[][] {
				{ createPublicTypeEDVOCamp, listingPayloadForThirtyDormant, listingPayloadForSixtyDormant,
						listingPayloadForNinetyDormant, listingPublicPayload, userIds, restId, dormantDays } };
	}

	@DataProvider(name = "evaluateListingForUserCutWith30DaysDormantData")
	public static Object[][] evaluateListingForUserCutWith30DaysDormant() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp30DaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		// HashMap<String, String> createEDVOCamp90DaysDormant = new HashMap<String,
		// String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForThirtyDormant = new HashMap<String, String>();
		// HashMap <String, String> cartPayloadFor60Days = new HashMap<String, String>
		// ();
		// HashMap <String, String> cartPayloadFor90Days = new HashMap<String, String>
		// ();
		HashMap<String, String> listingPayloadForFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRandomUser = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		ArrayList<String> userIds = new ArrayList<String>();
		
		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(3, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(4, "0");

		String dormantDays = "31";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1,  edvoHelper.getRestId());
		listOfRestIds.add(2,  edvoHelper.getRestId());
		listOfRestIds.add(3,  edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCamp30DaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "THIRTY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false,
		// false, false, "NINETY_DAYS_DORMANT",listOfRestIds.get(0) , allMealIds.get(0),
		// allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);

		listingPayloadForThirtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForThirtyDormant.put("1", "false");
		listingPayloadForThirtyDormant.put("2", userIds.get(0));
		listingPayloadForThirtyDormant.put("3", "ANDROID");
		listingPayloadForThirtyDormant.put("4", "229");

		listingPayloadForFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForFirstOrder.put("1", "true");
		listingPayloadForFirstOrder.put("2", userIds.get(1));
		listingPayloadForFirstOrder.put("3", "ANDROID");
		listingPayloadForFirstOrder.put("4", "229");

		listingPayloadForRestFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRestFirstOrder.put("1", "false");
		listingPayloadForRestFirstOrder.put("2", userIds.get(2));
		listingPayloadForRestFirstOrder.put("3", "ANDROID");
		listingPayloadForRestFirstOrder.put("4", "229");

		listingPayloadForRandomUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRandomUser.put("1", "false");
		listingPayloadForRandomUser.put("2", userIds.get(3));
		listingPayloadForRandomUser.put("3", "ANDROID");
		listingPayloadForRandomUser.put("4", "229");

		listingPayloadForPublicUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForPublicUser.put("1", "false");
		listingPayloadForPublicUser.put("2", userIds.get(4));
		listingPayloadForPublicUser.put("3", "ANDROID");
		listingPayloadForPublicUser.put("4", "229");

		return new Object[][] { { createEDVOCamp30DaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, listingPayloadForThirtyDormant, listingPayloadForFirstOrder,
				listingPayloadForRestFirstOrder, listingPayloadForRandomUser, listingPayloadForPublicUser, userIds.get(0), listOfRestIds.get(0),
				dormantDays } };
	}
	
	@DataProvider(name = "evaluateListingForUserCutWithSixtyDaysDormantData")
	public static Object[][] evaluateListingForUserCutWithSixtyDaysDormant() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCampSixtyDaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		// HashMap<String, String> createEDVOCamp90DaysDormant = new HashMap<String,
		// String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForSixtyDormant = new HashMap<String, String>();
		// HashMap <String, String> cartPayloadFor60Days = new HashMap<String, String>
		// ();
		// HashMap <String, String> cartPayloadFor90Days = new HashMap<String, String>
		// ();
		HashMap<String, String> listingPayloadForFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRandomUser = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		ArrayList<String> userIds = new ArrayList<String>();
		
		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(3, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(4, "0");

		String dormantDays = "61";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1,  edvoHelper.getRestId());
		listOfRestIds.add(2,  edvoHelper.getRestId());
		listOfRestIds.add(3,  edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCampSixtyDaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "SIXTY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false,
		// false, false, "NINETY_DAYS_DORMANT",listOfRestIds.get(0) , allMealIds.get(0),
		// allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);

		listingPayloadForSixtyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForSixtyDormant.put("1", "false");
		listingPayloadForSixtyDormant.put("2", userIds.get(0));
		listingPayloadForSixtyDormant.put("3", "ANDROID");
		listingPayloadForSixtyDormant.put("4", "229");

		listingPayloadForFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForFirstOrder.put("1", "true");
		listingPayloadForFirstOrder.put("2", userIds.get(1));
		listingPayloadForFirstOrder.put("3", "ANDROID");
		listingPayloadForFirstOrder.put("4", "229");

		listingPayloadForRestFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRestFirstOrder.put("1", "false");
		listingPayloadForRestFirstOrder.put("2", userIds.get(2));
		listingPayloadForRestFirstOrder.put("3", "ANDROID");
		listingPayloadForRestFirstOrder.put("4", "229");

		listingPayloadForRandomUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRandomUser.put("1", "false");
		listingPayloadForRandomUser.put("2", userIds.get(3));
		listingPayloadForRandomUser.put("3", "ANDROID");
		listingPayloadForRandomUser.put("4", "229");

		listingPayloadForPublicUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForPublicUser.put("1", "false");
		listingPayloadForPublicUser.put("2", userIds.get(4));
		listingPayloadForPublicUser.put("3", "ANDROID");
		listingPayloadForPublicUser.put("4", "229");

		return new Object[][] { { createEDVOCampSixtyDaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, listingPayloadForSixtyDormant, listingPayloadForFirstOrder,
				listingPayloadForRestFirstOrder, listingPayloadForRandomUser, listingPayloadForPublicUser, userIds.get(0), listOfRestIds.get(0),
				dormantDays } };
	}
	
	@DataProvider(name = "evaluateListingForUserCutWithNinetyDaysDormantData")
	public static Object[][] evaluateListingForUserCutWithNinetyDaysDormant() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCampNinetyDaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForNinetyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRandomUser = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		ArrayList<String> userIds = new ArrayList<String>();
		
		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(3, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(4, "0");

		String dormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1,  edvoHelper.getRestId());
		listOfRestIds.add(2,  edvoHelper.getRestId());
		listOfRestIds.add(3,  edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCampNinetyDaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "NINETY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false,
		// false, false, "NINETY_DAYS_DORMANT",listOfRestIds.get(0) , allMealIds.get(0),
		// allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);

		listingPayloadForNinetyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNinetyDormant.put("1", "false");
		listingPayloadForNinetyDormant.put("2", userIds.get(0));
		listingPayloadForNinetyDormant.put("3", "ANDROID");
		listingPayloadForNinetyDormant.put("4", "229");

		listingPayloadForFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForFirstOrder.put("1", "true");
		listingPayloadForFirstOrder.put("2", userIds.get(1));
		listingPayloadForFirstOrder.put("3", "ANDROID");
		listingPayloadForFirstOrder.put("4", "229");

		listingPayloadForRestFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRestFirstOrder.put("1", "false");
		listingPayloadForRestFirstOrder.put("2", userIds.get(2));
		listingPayloadForRestFirstOrder.put("3", "ANDROID");
		listingPayloadForRestFirstOrder.put("4", "229");

		listingPayloadForRandomUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRandomUser.put("1", "false");
		listingPayloadForRandomUser.put("2", userIds.get(3));
		listingPayloadForRandomUser.put("3", "ANDROID");
		listingPayloadForRandomUser.put("4", "229");

		listingPayloadForPublicUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForPublicUser.put("1", "false");
		listingPayloadForPublicUser.put("2", userIds.get(4));
		listingPayloadForPublicUser.put("3", "ANDROID");
		listingPayloadForPublicUser.put("4", "229");

		return new Object[][] { { createEDVOCampNinetyDaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, listingPayloadForNinetyDormant, listingPayloadForFirstOrder,
				listingPayloadForRestFirstOrder, listingPayloadForRandomUser, listingPayloadForPublicUser, userIds.get(0), listOfRestIds.get(0),
				dormantDays } };
	}
	
	@DataProvider(name = "evaluateListingForIOSVersionData")
	public static Object[][] evaluateListingForIOSVersion() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCampNinetyDaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForNinetyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRandomUser = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		ArrayList<String> userIds = new ArrayList<String>();
		
		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(3, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(4, "0");

		String dormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1,  edvoHelper.getRestId());
		listOfRestIds.add(2,  edvoHelper.getRestId());
		listOfRestIds.add(3,  edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCampNinetyDaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "NINETY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false,
		// false, false, "NINETY_DAYS_DORMANT",listOfRestIds.get(0) , allMealIds.get(0),
		// allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);

		listingPayloadForNinetyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNinetyDormant.put("1", "false");
		listingPayloadForNinetyDormant.put("2", userIds.get(0));
		listingPayloadForNinetyDormant.put("3", "IOS");
		listingPayloadForNinetyDormant.put("4", "200");

		listingPayloadForFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForFirstOrder.put("1", "true");
		listingPayloadForFirstOrder.put("2", userIds.get(1));
		listingPayloadForFirstOrder.put("3", "IOS");
		listingPayloadForFirstOrder.put("4", "200");

		listingPayloadForRestFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRestFirstOrder.put("1", "false");
		listingPayloadForRestFirstOrder.put("2", userIds.get(2));
		listingPayloadForRestFirstOrder.put("3", "IOS");
		listingPayloadForRestFirstOrder.put("4", "200");

		listingPayloadForRandomUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRandomUser.put("1", "false");
		listingPayloadForRandomUser.put("2", userIds.get(3));
		listingPayloadForRandomUser.put("3", "IOS");
		listingPayloadForRandomUser.put("4", "200");

		listingPayloadForPublicUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForPublicUser.put("1", "false");
		listingPayloadForPublicUser.put("2", userIds.get(4));
		listingPayloadForPublicUser.put("3", "IOS");
		listingPayloadForPublicUser.put("4", "200");

		return new Object[][] { { createEDVOCampNinetyDaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, listingPayloadForNinetyDormant, listingPayloadForFirstOrder,
				listingPayloadForRestFirstOrder, listingPayloadForRandomUser, listingPayloadForPublicUser, userIds.get(0), restId,
				dormantDays } };
	}
	
	@DataProvider(name = "evaluateListingForInvalidIOSVersionData")
	public static Object[][] evaluateListingForInvalidIOSVersion() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCampNinetyDaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForNinetyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRandomUser = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		ArrayList<String> userIds = new ArrayList<String>();
		
		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(3, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(4, "0");

		String dormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1,  edvoHelper.getRestId());
		listOfRestIds.add(2,  edvoHelper.getRestId());
		listOfRestIds.add(3,  edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCampNinetyDaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "NINETY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false,
		// false, false, "NINETY_DAYS_DORMANT",listOfRestIds.get(0) , allMealIds.get(0),
		// allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);

		listingPayloadForNinetyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNinetyDormant.put("1", "false");
		listingPayloadForNinetyDormant.put("2", userIds.get(0));
		listingPayloadForNinetyDormant.put("3", "IOS");
		listingPayloadForNinetyDormant.put("4", "199");

		listingPayloadForFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForFirstOrder.put("1", "true");
		listingPayloadForFirstOrder.put("2", userIds.get(1));
		listingPayloadForFirstOrder.put("3", "IOS");
		listingPayloadForFirstOrder.put("4", "199");

		listingPayloadForRestFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRestFirstOrder.put("1", "false");
		listingPayloadForRestFirstOrder.put("2", userIds.get(2));
		listingPayloadForRestFirstOrder.put("3", "IOS");
		listingPayloadForRestFirstOrder.put("4", "199");

		listingPayloadForRandomUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRandomUser.put("1", "false");
		listingPayloadForRandomUser.put("2", userIds.get(3));
		listingPayloadForRandomUser.put("3", "IOS");
		listingPayloadForRandomUser.put("4", "199");

		listingPayloadForPublicUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForPublicUser.put("1", "false");
		listingPayloadForPublicUser.put("2", userIds.get(4));
		listingPayloadForPublicUser.put("3", "IOS");
		listingPayloadForPublicUser.put("4", "199");

		return new Object[][] { { createEDVOCampNinetyDaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, listingPayloadForNinetyDormant, listingPayloadForFirstOrder,
				listingPayloadForRestFirstOrder, listingPayloadForRandomUser, listingPayloadForPublicUser, userIds.get(0), listOfRestIds.get(0),
				dormantDays } };
	}
	
	@DataProvider(name = "evaluateListingForInvalidAndroidVersionData")
	public static Object[][] evaluateListingForInvalidAndroidVersion() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCampNinetyDaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForNinetyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRandomUser = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		ArrayList<String> userIds = new ArrayList<String>();
		
		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(3, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(4, "0");

		String dormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1,  edvoHelper.getRestId());
		listOfRestIds.add(2,  edvoHelper.getRestId());
		listOfRestIds.add(3,  edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCampNinetyDaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "NINETY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false,
		// false, false, "NINETY_DAYS_DORMANT",listOfRestIds.get(0) , allMealIds.get(0),
		// allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);

		listingPayloadForNinetyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNinetyDormant.put("1", "false");
		listingPayloadForNinetyDormant.put("2", userIds.get(0));
		listingPayloadForNinetyDormant.put("3", "ANDROID");
		listingPayloadForNinetyDormant.put("4", "228");

		listingPayloadForFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForFirstOrder.put("1", "true");
		listingPayloadForFirstOrder.put("2", userIds.get(1));
		listingPayloadForFirstOrder.put("3", "ANDROID");
		listingPayloadForFirstOrder.put("4", "228");

		listingPayloadForRestFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRestFirstOrder.put("1", "false");
		listingPayloadForRestFirstOrder.put("2", userIds.get(2));
		listingPayloadForRestFirstOrder.put("3", "ANDROID");
		listingPayloadForRestFirstOrder.put("4", "228");

		listingPayloadForRandomUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRandomUser.put("1", "false");
		listingPayloadForRandomUser.put("2", userIds.get(3));
		listingPayloadForRandomUser.put("3", "ANDROID");
		listingPayloadForRandomUser.put("4", "228");

		listingPayloadForPublicUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForPublicUser.put("1", "false");
		listingPayloadForPublicUser.put("2", userIds.get(4));
		listingPayloadForPublicUser.put("3", "ANDROID");
		listingPayloadForPublicUser.put("4", "228");

		return new Object[][] { { createEDVOCampNinetyDaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, listingPayloadForNinetyDormant, listingPayloadForFirstOrder,
				listingPayloadForRestFirstOrder, listingPayloadForRandomUser, listingPayloadForPublicUser, userIds.get(0), listOfRestIds.get(0),
				dormantDays } };
	}
	
	@DataProvider(name = "evaluateListingForAndroidVersionData")
	public static Object[][] evaluateListingForAndroidVersion() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCampNinetyDaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
		ArrayList<String> listOfItemIds = new ArrayList<String>();
		HashMap<String, String> listingPayloadForNinetyDormant = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForRandomUser = new HashMap<String, String>();
		HashMap<String, String> listingPayloadForPublicUser = new HashMap<String, String>();
		String mealItemRequestPayload = "";
		ArrayList<String> userIds = new ArrayList<String>();
		
		// all userIDs for listing evaluate
		userIds.add(0, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(1, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(2, Integer.toString(Utility.getRandom(2500, 7500)));
		userIds.add(3, Integer.toString(Utility.getRandom(2500, 7500)));
		// user id 0 for public user
		userIds.add(4, "0");

		String dormantDays = "91";

		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1,  edvoHelper.getRestId());
		listOfRestIds.add(2,  edvoHelper.getRestId());
		listOfRestIds.add(3,  edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCampNinetyDaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "NINETY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		// createEDVOCamp90DaysDormant = edvoHelper.createCampaignWithUserCut(false,
		// false, false, "NINETY_DAYS_DORMANT",listOfRestIds.get(0) , allMealIds.get(0),
		// allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

		ArrayList<String> listOfAllItemsPrice = new ArrayList<String>();
		ArrayList<String> listOfItemCountInMeal = new ArrayList<String>();
		ArrayList<String> numOfMealItemRequestInCart = new ArrayList<String>();
		ArrayList<String> listOfRestIdsInCart = new ArrayList<String>();

		// list of item count in each meal
		listOfItemCountInMeal.add(0, "1");

		// list of item's price
		listOfAllItemsPrice.add(0, "400");

		// numOfMealItemRequestInCart
		numOfMealItemRequestInCart.add(0, "1");

		// list Of item Ids
		listOfItemIds.add(0, Integer.toString(Utility.getRandom(10, 2300)));

		// listOfAllRestaurantIds
		listOfRestIdsInCart.add(0, restId);

//		mealItemRequestPayload = edvoHelper.createCartMealItemRequest(numOfMealItemRequestInCart, allMealIds,
//				listOfItemIds, listOfRestIdsInCart, listOfAllItemsPrice, listOfItemCountInMeal, allGroupIdsOfMeals);
		
		listingPayloadForNinetyDormant.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForNinetyDormant.put("1", "false");
		listingPayloadForNinetyDormant.put("2", userIds.get(0));
		listingPayloadForNinetyDormant.put("3", "ANDROID");
		listingPayloadForNinetyDormant.put("4", "229");

		listingPayloadForFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForFirstOrder.put("1", "true");
		listingPayloadForFirstOrder.put("2", userIds.get(1));
		listingPayloadForFirstOrder.put("3", "ANDROID");
		listingPayloadForFirstOrder.put("4", "229");

		listingPayloadForRestFirstOrder.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRestFirstOrder.put("1", "false");
		listingPayloadForRestFirstOrder.put("2", userIds.get(2));
		listingPayloadForRestFirstOrder.put("3", "ANDROID");
		listingPayloadForRestFirstOrder.put("4", "229");

		listingPayloadForRandomUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForRandomUser.put("1", "false");
		listingPayloadForRandomUser.put("2", userIds.get(3));
		listingPayloadForRandomUser.put("3", "ANDROID");
		listingPayloadForRandomUser.put("4", "229");

		listingPayloadForPublicUser.put("0", listOfRestIds.get(0) + "," + listOfRestIds.get(1) + ","
				+ listOfRestIds.get(2) + "," + listOfRestIds.get(3));
		listingPayloadForPublicUser.put("1", "false");
		listingPayloadForPublicUser.put("2", userIds.get(4));
		listingPayloadForPublicUser.put("3", "ANDROID");
		listingPayloadForPublicUser.put("4", "229");

		return new Object[][] { { createEDVOCampNinetyDaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp, listingPayloadForNinetyDormant, listingPayloadForFirstOrder,
				listingPayloadForRestFirstOrder, listingPayloadForRandomUser, listingPayloadForPublicUser, userIds.get(0), listOfRestIds.get(0),
				dormantDays } };
	}
	
	@DataProvider(name="getActiveCampaignData")
	public static Object[][] getActiveCampaign() {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		HashMap<String, String> createEDVOCampSixtyDaysDormant = new HashMap<String, String>();
		HashMap<String, String> createEDVOPublicCamp = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
		HashMap<String, String> createEDVOCampMappedUser = new HashMap<String, String>();
	
		// listOfAllRestaurantIds
		listOfRestIds.add(0, edvoHelper.getRestId());
		listOfRestIds.add(1,  edvoHelper.getRestId());
		listOfRestIds.add(2,  edvoHelper.getRestId());
		listOfRestIds.add(3,  edvoHelper.getRestId());

		// list of All MealIds
		allMealIds.add(0, mealId = edvoHelper.getMealId());

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId = Integer.toString(Utility.getRandom(1000, 40000)));

		createEDVOCampSixtyDaysDormant = edvoHelper.createCampaignWithUserCut(false, false, false, "SIXTY_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "30", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOPublicCamp = edvoHelper.createCampaignWithUserCut(false, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Flat", "50", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampFirstOrder = edvoHelper.createCampaignWithUserCut(false, true, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "99", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));
		createEDVOCampMappedUser = edvoHelper.createCampaignWithUserCut(true, false, false, "ZERO_DAYS_DORMANT",
				new CreateReward().toString(), new CreateReward("NONE", "Percentage", "5", "1").toString(),
				listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

	

		return new Object[][] { { createEDVOCampSixtyDaysDormant, createEDVOCampFirstOrder, createEDVOCampRestFirstOrder,
				createEDVOCampMappedUser, createEDVOPublicCamp } };
	}	
}
