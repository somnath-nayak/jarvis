package com.swiggy.api.sf.snd.constants;


import edu.emory.mathcs.backport.java.util.Arrays;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.DBHelper;

import java.util.List;

public interface SANDConstants {
    String sndDb="snddb";
    String mobile="6254063598";
    String name="pratick";
    String password="rkonowhere";
    String mobileId="mobile";
    String id="id";
    String sandRedis="sandredisstage";
    String user_id="user_id";
    String lat="12.9326";
    String lng="77.6036";
    String otpExpireMsg="OTP expired";
    String userEmail="user_email";
    String userMobile="user_mobile";
    String msg="done successfully";
    String email="email";
    String referal_code="referral_code";
    String[] LocationFeature = {"SWIGGY_POP", "SWIGGY_ASSURED"};
    String experimentName="firebase_perf";
    String[] UserAgent= {"WEB", "Swiggy-Android", "Swiggy-iOS"};
    String consumerSessionTstMobile="7406734416";
    String[] options= {"require_name_email", "invalid_option"};
    String[] values= {"2.0", "invalid_value"};
    String consumerSessionTstPwd="myTest12";
    String bAuth = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
    String statusCode="0";
    String message="successfully done";
    String stringFilter= "STRING_EQUALS_FILTER";
    String booleanFilter= "BOOLEAN_IS_FILTER";
    String numberFilter= "NUMBER_IN_RANGE_FILTER";
    String numberEqualsFilter= "NUMBER_EQUALS_FILTER";
    String bool= "BOOLEAN";
    String item= "ITEM";
    String latLng= "LATLONG_NEAR_POINT";
    String tagFilter= "TAG_EQUALS_FILTER";
    String edvoRestMealKey = "rest_meal_";
    String edvoMealDefKey = "meal_def_";
    String edvoMealUiKey = "meal_ui_";
    String userId= "87266";
    String userNot="User not found";
    String usernot="User Not found";
    String cityPolygonMap= "INSERT INTO city_polygon (city_id, polygon_id) VALUES(";
    String userName="ozlRZTueR2kqv7gm";
    String pass="d2f2bd530ab25fef47ab220d0e0aa108";
    String cafeZone= "select * from cafe.cafe_zones";
    String corporateUserMapping = "select * from cafe.corporate_user_mapping";
    String orders= "select * from cafe.orders";
    String cafeRestaurantMapping ="select * from swiggy.cafe_restaurant_mapping";
    String cafes= "select * from swiggy.cafes";
    String corporateCafeMapping= "select * from corporate_cafe_mapping";
    String cafeClearCache = "Basic dmFpYmhhdjp2YWliaGF2";
    String disableCafe = "update swiggy.cafes set enabled = 0 where id = ";
    String disableCorporate = "update swiggy.corporates set enabled = 0 where id = ";
    String enableCafe = "update swiggy.cafes set enabled = 1 where id = ";
    String enableCorporate = "update swiggy.corporates set enabled = 1 where id = ";
    String updateSla= "update delivery.zone set max_delivery_time = ";
    String updateSla1=", min_delivery_time = ";
    String rainModeType="UPDATE delivery.zone SET rain_mode_type = ";
    String whereId="where id= ";
    String zoneRainParams="UPDATE delivery.zone_rain_params SET last_mile = ";
    String referralCode= "select * from swiggy_user_management.wp_users where id=";
    String signUp= "select * from swiggy_user_management.wp_users where mobile=";
    String tr= "true";
    String fal= "false";
    String otp="_OTP";
    String mobileUpdateKey="mobile_update_";
    String invalidMessage="Invalid mobile number.";
    String menuKey="MENU_V3_";
    String restKey="RESTAURANT_V3_";
    int database=0;
    int database2=2;
    String getMenuId= "select * from `restaurant_menu_map` where restaurant_id='";
    String menuType="' and menu_type='";
    String regularMenu="REGULAR_MENU";
    String popMenu="POP_MENU";
    String cmsDB="cms";
    String superKey="SUPER";
    String isSuper="IS_SUPER";
    String superDetails="SUPER_DETAILS";
    String cache="CACHE__";
    String user="userId";
    String plan ="4";
    String data="ITEM_V3_";
    String rest= "RESTAURANT";
    String landmark="IBC";
    String address= "#9, IBC";
    String analytics = "analyticsredis";
    String kafkaName = "sandkafka";
    Integer index = 2;
    Integer db = 9;
    String listingTag = "listing_dynamic_tags";
    String delKey = "all_options";
    String restaurantTopicName = "restaurant_entity_change";
    String keyInitial = "DATA_RESTAURANT_INFO_";
    String freeDel= "FREE_DELIVERY";
    String freebie="Freebie";
    String percent="Percentage";
    String flat= "Flat";
    String flatNo="IBC Knowledge";
    String city="Bangalore";
    String annotation="OTHER";
    String area= "Near IBC";
    String invalidUser="Invalid user id.";
    String other ="OTHER";
    String home= "HOME";
    String work= "WORK";
    String notFound="Not found.";
    int superDb= 5;
    String delivery="deliverydb";
    String mo="mobile";
    String usedEmail="userEmail";
    String referralUsed="referralUsed";
    String emailVerificationFlag="email_verification_flag";
    String lastOrderCity="lastOrderCity";

    String collectionId= "select * from swiggy.collection ORDER BY id DESC limit 1;";
    String rainMode= "update delivery.zone set rain_mode_type='";
    String rainModeEnd= "where id='";
    String slug="select new_slug, cost_for_two from restaurants where id='";
    String minAmount="99.0";
    String fromDistance="0";
    String toDistance="2";
    String fromTime="0000";
    String toTime="2359";
    String defaultFee="2";
    String cartFe="0";
    String priority="1";
    String restName="select name from swiggy.restaurants where id='";
    String [] nodes= {"opened", "closed", "unserviceable", "unavailable"};

    String zeroDormant="ZERO_DAYS_DORMANT";
	String collectionCreation = "INSERT INTO `collection` (`channel`, `description`, `title`, `enabled`, `cloudinary_image`, `cloudinary_icon`, `web_collection_header`, `web_carousal_icon`,`priority`, `minimum_restaurant`, `maximum_restaurant`, `type`, `city_id`, `identifier`, `start_date`, `end_date`, `entity`, `minimum_count`,`maximum_count`, `background_image`, `logo`, `visible`, `web_mast_head`, `app_mast_head`, `template_style`, `name`, `sub_title`,`dweb_open_filter_title`, `dweb_open_filter_bg_color`, `dweb_open_filter_select_icon`, `dweb_open_filter_deselect_icon`) VALUES('ALL', 'List of trending restaurants in your area.', 'Testing', 1, 'ixnhuotzsfwvh3l5ftfc', 'zjpk1fphiytydurwxlbk', NULL, 'aop9e16bpkpccpet83xj', 5, 1, 10, 'STATIC', 1,'testing', '2017-05-18 01:33:32', '2019-12-31 23:59:59', 'RESTAURANT', 1, 10, NULL, NULL, 1, 're1wfsp6iiytxp4p1pig','re1wfsp6iiytxp4p1pig','WITHOUT_BACKGROUND', 'Collection', 'Trending restaurants in your area', NULL, NULL, NULL, NULL)";

    String dynamicCollection= "INSERT INTO `collection` (`channel`, `description`, `title`, `enabled`, `cloudinary_image`, `cloudinary_icon`,`web_collection_header`, `web_carousal_icon`,`priority`, `minimum_restaurant`, `maximum_restaurant`, `type`, `city_id`, `identifier`, `start_date`, `end_date`, `entity`, `minimum_count`,`maximum_count`, `background_image`, `logo`, `visible`, `web_mast_head`, `app_mast_head`, `template_style`, `name`, `sub_title`,`dweb_open_filter_title`, `dweb_open_filter_bg_color`, `dweb_open_filter_select_icon`, `dweb_open_filter_deselect_icon`) VALUES('ALL', 'List of trending restaurants in your area.', 'Testing', 1, 'ixnhuotzsfwvh3l5ftfc', 'zjpk1fphiytydurwxlbk', NULL, 'aop9e16bpkpccpet83xj', 5, 1, 10, 'DYNAMIC', 1,'free_delivery', '2018-07-03 10:00:32', '2018-07-03 10:30:32', 'RESTAURANT', 1, 10, NULL, NULL, 1, 're1wfsp6iiytxp4p1pig','re1wfsp6iiytxp4p1pig','WITHOUT_BACKGROUND', 'Collection', 'Trending restaurants in your area', NULL, NULL, NULL, NULL)";

    String dynamicCol="INSERT INTO `collection` (`channel`, `description`, `title`, `enabled`, `cloudinary_image`, `cloudinary_icon`,`web_collection_header`, `web_carousal_icon`,`priority`, `minimum_restaurant`, `maximum_restaurant`, `type`, `city_id`, `identifier`, `start_date`, `end_date`, `entity`, `minimum_count`,`maximum_count`, `background_image`, `logo`, `visible`, `web_mast_head`, `app_mast_head`, `template_style`, `name`, `sub_title`,`dweb_open_filter_title`, `dweb_open_filter_bg_color`, `dweb_open_filter_select_icon`, `dweb_open_filter_deselect_icon`) VALUES('ALL', 'List of trending restaurants in your area.', 'FREE', 1, 'ixnhuotzsfwvh3l5ftfc', 'zjpk1fphiytydurwxlbk', NULL, 'aop9e16bpkpccpet83xj', 1, 1, 5,'DYNAMIC', 1,";

    String col="'RESTAURANT', 1, 5, NULL, NULL, 1, 're1wfsp6iiytxp4p1pig','re1wfsp6iiytxp4p1pig','WITHOUT_BACKGROUND', 'Collection', 'Trending restaurants in your area', NULL, NULL, NULL, NULL)";

    static String getDynamicCollection(String identifier, String startTime, String endTime){
        System.out.println("collection query-->" +dynamicCol+"'"+identifier+"'"+","+"'"+startTime+"'"+","+"'"+endTime+"'"+","+col+";");
        return dynamicCol+"'"+identifier+"'"+","+"'"+startTime+"'"+","+"'"+endTime+"'"+","+col+";";
    }
    String[] SwiggyAssuredWpOption = {"assured_enabled","swiggy_assured_config", "swiggy_assured_help_text_city_0"};
    String cuisine = "Select cuisine from swiggy.restaurants where id='";
    String cuisineList= "Select cuisine from swiggy.restaurants;";
	String positiveWPOptions = "{\"0\":{\"NEW\":true,\"CLOUD\":true,\"REPEAT\":false,\"POPULAR\":true,\"PROMOTED\":true,\"EXCLUSIVE\":true,\"PREORDER\":true},\"1\":{\"NEW\":true,\"CLOUD\":true,\"REPEAT\":false,\"POPULAR\":true,\"PROMOTED\":true,\"EXCLUSIVE\":true,\"PREORDER\":true},\"10\":{\"NEW\":false,\"CLOUD\":true,\"REPEAT\":false,\"POPULAR\":true,\"PROMOTED\":true,\"EXCLUSIVE\":true,\"PREORDER\":true},\"11\":{\"NEW\":false,\"CLOUD\":true,\"REPEAT\":false,\"POPULAR\":true,\"PROMOTED\":true,\"EXCLUSIVE\":true,\"PREORDER\":true},\"12\":{\"NEW\":false,\"CLOUD\":true,\"REPEAT\":false,\"POPULAR\":true,\"PROMOTED\":true,\"EXCLUSIVE\":true,\"PREORDER\":true}}";

     String negativeWPOptions = "{\"0\":{\"NEW\":false,\"CLOUD\":false,\"REPEAT\":false,\"POPULAR\":false,\"PROMOTED\":false,\"EXCLUSIVE\":false,\"PREORDER\":false},\"1\":{\"NEW\":false,\"CLOUD\":false,\"REPEAT\":false,\"POPULAR\":false,\"PROMOTED\":false,\"EXCLUSIVE\":false,\"PREORDER\":false},\"10\":{\"NEW\":false,\"CLOUD\":true,\"REPEAT\":false,\"POPULAR\":true,\"PROMOTED\":true,\"EXCLUSIVE\":true,\"PREORDER\":true},\"11\":{\"NEW\":false,\"CLOUD\":true,\"REPEAT\":false,\"POPULAR\":true,\"PROMOTED\":true,\"EXCLUSIVE\":true,\"PREORDER\":true},\"12\":{\"NEW\":false,\"CLOUD\":true,\"REPEAT\":false,\"POPULAR\":true,\"PROMOTED\":true,\"EXCLUSIVE\":true,\"PREORDER\":true}}";

     default String cityMapPolygon(String cityId, String polygonId)
    {
        return cityPolygonMap+"'"+cityId+"'"+","+"'"+polygonId+"'"+")";
    }

    default String getCollectionCreation(){
        return collectionCreation;
    }
    String q= "select * from collections.collections order by id desc limit 1";


    String json_contenttype = "json";
    String xml_contenttype ="xml";
    String analyticsRedis= "analytics";
    int analyticsDb= 5;
    int responseCode = 200;
    int StatusCode = 0;
    int enabled = 1;
    int statusCodeError = 404;
    DBHelper d = new DBHelper();

    String weeks[]= {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
    String areaStatusMsgError = "Area not found";
    String tag="{\"itemPrice\": [{\"max\": 129.0, \"type\": \"NUMBER_IN_RANGE_FILTER\", \"min\": null}], \"tags\": [{\"tagGroup\": \"itemdish\", \"tagValue\": \"biryani\", \"type\": \"TAG_EQUALS_FILTER\"}]}";
    String ddCollCreation="INSERT INTO `collections` (`name`, `slug`, `description`, `version_code`, `collectable_type`, `enabled_filters`, `enabled`, `start_date`, `end_date`, `created_on`, `created_by`, `updated_on`, `updated_by`, `sort_by`, `archive`, `query`) VALUES ('Top Food Near You New DD collection', 'top-food', 'Top Food DD', 1, 'COLLECTION', '{}', 1, '2018-03-04', '2020-01-01', '2018-03-04 10:47:07', 'jitender.kumar@swiggy.in', '2018-08-20 14:38:35', 'gsiddardha@swiggy.in', '', 0, '{}');";


    String smartSuggestionTopicName = "catalog_item_updates";

    String UserName="nevia";
    String Password="password";

    public static String setMetaInfo = "UPDATE swiggy.restaurants set meta_info='";
    public static String WhereId = " ' where id=";



    String collectionCreations = "INSERT INTO `collections` ( `name`, `slug`, `description`, `version_code`, `collectable_type`, `enabled_filters`, `enabled`, `start_date`, `end_date`, `created_on`, `created_by`, `updated_on`, `updated_by`, `sort_by`, `archive`, `query`) VALUES ('Biryanis under Rs 129 for binge-eating', 'biryani-under-129', 'Biryani under 129', 1, 'ITEM', '{\"itemPrice\": [{\"max\": 129.0, \"type\": \"NUMBER_IN_RANGE_FILTER\", \"min\": null}], \"tags\": [{\"tagGroup\": \"itemdish\", \"tagValue\": \"biryani\", \"type\": \"TAG_EQUALS_FILTER\"}]}', 1, '2018-01-01', '2020-01-01', '2018-03-05 20:13:37', 'gsiddardha@swiggy.in', '2018-12-03 08:29:32', 'gsiddardha@swiggy.in', '', 1, '{}')";
    String coll="INSERT INTO `collections` ( `name`, `slug`, `description`, `version_code`, `collectable_type`, `enabled_filters`, `enabled`, `start_date`, `end_date`, `created_on`, `created_by`, `updated_on`, `updated_by`, `sort_by`, `archive`, `query`) VALUES ('Biryanis under Rs 129 for binge-eating', 'biryani-under-129', 'Biryani under 129', 1, 'ITEM', '{\"itemPrice\": [{\"max\": 129.0, \"type\": \"NUMBER_IN_RANGE_FILTER\", \"min\": null}], \"tags\": [{\"tagGroup\": \"itemdish\", \"tagValue\": \"biryani\", \"type\": \"TAG_EQUALS_FILTER\"}]}', 1, '2018-01-01', '2020-01-01', '2018-03-05 20:13:37', 'gsiddardha@swiggy.in', '2018-12-03 08:29:32', 'gsiddardha@swiggy.in', '', 1, '{}')";

    String co="INSERT INTO collections.collections ( `name`, `slug`, `description`, `version_code`, `collectable_type`, `enabled_filters`, `enabled`, `start_date`, `end_date`, `created_on`, `created_by`, `updated_on`, `updated_by`, `sort_by`, `archive`, `query`) VALUES ('Biryanis under Rs 129 for binge-eating', 'biryani-under-129', 'Biryani under 129', 1, 'ITEM', '{\\\"itemPrice\\\": [{\\\"max\\\": 129.0, \\\"type\\\": \\\"NUMBER_IN_RANGE_FILTER\\\", \\\"min\\\": null}], \\\"tags\\\": [{\\\"tagGroup\\\": \\\"itemdish\\\", \\\"tagValue\\\": \\\"biryani\\\", \\\"type\\\": \\\"TAG_EQUALS_FILTER\\\"}]}', 1, '2018-01-01', '2020-01-01', '2018-03-05 20:13:37', 'gsiddardha@swiggy.in', '2018-12-03 08:29:32', 'gsiddardha@swiggy.in', '', 1, '{}')";


    String c="insert into collections.collection_views (`collection_id`, `page`, `view_type`, `view_data`, `version_code`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES";
    String coo="{\\\"mainPageDescription\\\": \\\"Look no further to satisfy your cravings! Explore top biryanis under Rs 129 near you.\\\", \\\"minRestaurants\\\": 2, \\\"imageId\\\": \\\"Biryani-3_msxf1h\\\", \\\"maxRestaurants\\\": 10, \\\"maxItems\\\": 4, \\\"backgroundImageId\\\": \\\"Biryani-3_msxf1h\\\", \\\"subTitle\\\": \\\"Look no further to satisfy your cravings! Explore top biryanis under Rs 129 near you.\\\", \\\"title\\\": \\\"Biryanis under Rs 129 for binge-eating\\\", \\\"minItems\\\": 1, \\\"maxItemsPerRestaurant\\\": 4, \\\"viewVariables\\\": [], \\\"type\\\": \\\"MENULET\\\", \\\"minItemsPerRestaurant\\\": 1}";

    String tagNumber= "{\\\"itemPrice\\\": [{\\\"max\\\": 129.0, \\\"type\\\": \\\"NUMBER_IN_RANGE_FILTER\\\", \\\"min\\\": null}], \\\"tags\\\": [{\\\"tagGroup\\\": \\\"itemdish\\\", \\\"tagValue\\\": \\\"biryani\\\", \\\"type\\\": \\\"TAG_EQUALS_FILTER\\\"}]}";

    String tagEquals= "{\\\"tags\\\": [{\\\"tagGroup\\\": \\\"itemdish\\\", \\\"tagValue\\\": \\\"ice cream\\\", \\\"type\\\": \\\"TAG_EQUALS_FILTER\\\"}]}";

    String longDis="{\\\"hasImage\\\": [{\\\"id\\\": \\\"hasImage\\\", \\\"fieldType\\\": \\\"BOOLEAN_IS_FILTER\\\", \\\"value\\\": false, \\\"type\\\": \\\"BOOLEAN_IS_FILTER\\\"}], \\\"restaurantDiscounted\\\": [{\\\"id\\\": \\\"restaurantDiscounted\\\", \\\"fieldType\\\": \\\"BOOLEAN_IS_FILTER\\\", \\\"value\\\": false, \\\"type\\\": \\\"BOOLEAN_IS_FILTER\\\"}], \\\"tags\\\": [{\\\"id\\\": \\\"tags\\\", \\\"tagValue\\\": \\\"Biryanis_Top_15\\\", \\\"tagGroup\\\": \\\"merchTags\\\", \\\"type\\\": \\\"TAG_EQUALS_FILTER\\\"}], \\\"itemDiscounted\\\": [{\\\"id\\\": \\\"itemDiscounted\\\", \\\"fieldType\\\": \\\"BOOLEAN_IS_FILTER\\\", \\\"value\\\": false, \\\"type\\\": \\\"BOOLEAN_IS_FILTER\\\"}], \\\"itemEnabled\\\": [{\\\"id\\\": \\\"itemEnabled\\\", \\\"fieldType\\\": \\\"BOOLEAN_IS_FILTER\\\", \\\"value\\\": false, \\\"type\\\": \\\"BOOLEAN_IS_FILTER\\\"}], \\\"itemLongDistanceEnabled\\\": [{\\\"id\\\": \\\"itemLongDistanceEnabled\\\", \\\"fieldType\\\": \\\"BOOLEAN_IS_FILTER\\\", \\\"value\\\": false, \\\"type\\\": \\\"BOOLEAN_IS_FILTER\\\"}], \\\"restaurantLongDistanceEnabled\\\": [{\\\"id\\\": \\\"restaurantLongDistanceEnabled\\\", \\\"fieldType\\\": \\\"BOOLEAN_IS_FILTER\\\", \\\"value\\\": false, \\\"type\\\": \\\"BOOLEAN_IS_FILTER\\\"}], \\\"restaurantEnabled\\\": [{\\\"id\\\": \\\"restaurantEnabled\\\", \\\"fieldType\\\": \\\"BOOLEAN_IS_FILTER\\\", \\\"value\\\": false, \\\"type\\\": \\\"BOOLEAN_IS_FILTER\\\"}]}";

    String multiTd="{\\\"restaurantmultiTD\\\": [{\\\"type\\\": \\\"MATCH_EXPRESSION_IN_ANY_MAP_FIELD_FILTER\\\", \\\"query\\\": \\\"restaurantDiscountType0 AND restaurantDiscountOpType0\\\", \\\"filters\\\": {\\\"restaurantDiscountType\\\": [{\\\"type\\\": \\\"STRING_EQUALS_FILTER\\\", \\\"id\\\": \\\"restaurantDiscountType\\\", \\\"query_id\\\": \\\"restaurantDiscountType0\\\", \\\"value\\\": \\\"freebie\\\"}], \\\"restaurantDiscountOpType\\\": [{\\\"type\\\": \\\"STRING_EQUALS_FILTER\\\", \\\"id\\\": \\\"restaurantDiscountOpType\\\", \\\"query_id\\\": \\\"restaurantDiscountOpType0\\\", \\\"value\\\": \\\"super\\\"}]}, \\\"query_id\\\": \\\"restaurantmultiTD0\\\", \\\"id\\\": \\\"restaurantmultiTD\\\"}]}";

    String restSla= "{\\\"restaurantSla\\\": [{\\\"max\\\": 30.0, \\\"type\\\": \\\"NUMBER_IN_RANGE_FILTER\\\", \\\"min\\\": null}], \\\"tags\\\": [{\\\"tagGroup\\\": \\\"itemdish\\\", \\\"tagValue\\\": \\\"rolls & wraps\\\", \\\"type\\\": \\\"TAG_EQUALS_FILTER\\\"}]}";

    String price99="{\\\"itemPrice\\\": [{\\\"max\\\": 99.0, \\\"type\\\": \\\"NUMBER_IN_RANGE_FILTER\\\", \\\"min\\\": null}], \\\"tags\\\": [{\\\"tagGroup\\\": \\\"itemdish\\\", \\\"tagValue\\\": \\\"thalis\\\", \\\"type\\\": \\\"TAG_EQUALS_FILTER\\\"}]}";

    static String COC(String tag){
       return "INSERT INTO collections.collections ( `name`, `slug`, `description`, `version_code`, `collectable_type`, `enabled_filters`, `enabled`, `start_date`, `end_date`, `created_on`, `created_by`, `updated_on`, `updated_by`, `sort_by`, `archive`, `query`) VALUES ('Biryanis under Rs 129 for binge-eating', 'biryani-under-129', 'Biryani under 129', 1, 'ITEM', "+"'"+tag+"'"+", 1, '2018-01-01', '2020-01-01', '2018-03-05 20:13:37', 'gsiddardha@swiggy.in', '2018-12-03 08:29:32', 'gsiddardha@swiggy.in', '', 1, '{}')";

    }

    public static void cw(String id) {

        String cViews="INSERT INTO collections.collection_views (`collection_id`, `page`, `view_type`, `view_data`, `version_code`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES ("+id+", 'LISTING', 'MENULET', '{\\\"mainPageDescription\\\": \\\"Look no further to satisfy your cravings! Explore top biryanis under Rs 129 near you.\\\", \\\"minRestaurants\\\": 1, \\\"imageId\\\": \\\"Biryani-3_msxf1h\\\", \\\"maxRestaurants\\\": 10, \\\"maxItems\\\": 2, \\\"backgroundImageId\\\": \\\"Biryani-3_msxf1h\\\", \\\"subTitle\\\": \\\"Look no further to satisfy your cravings! Explore top biryanis under Rs 129 near you.\\\", \\\"title\\\": \\\"Biryanis under Rs 129 for binge-eating\\\", \\\"minItems\\\": 1, \\\"maxItemsPerRestaurant\\\": 4, \\\"viewVariables\\\": [], \\\"type\\\": \\\"MENULET\\\", \\\"minItemsPerRestaurant\\\": 1}', 1, '2018-03-05 20:13:37', 'gsiddardha@swiggy.in', '2018-12-03 20:13:37', 'gsiddardha@swiggy.in')";

        SystemConfigProvider.getTemplate("snddb").update(cViews);

        String colCities="insert into collections.collection_cities (collection_id, city_id) values ("+id+", 1)";
        SystemConfigProvider.getTemplate("snddb").update(colCities);

        String colPrioritySlots="INSERT INTO collections.collection_priority_slots ( `created_on`, `updated_on`, `updated_by`, `created_by`, `start_date`, `end_date`, `parent_id`, `child_id`, `position`, `enabled`) VALUES ( '2018-03-22 18:07:19', '2018-05-10 18:32:31', '', '', '2018-12-01', '2018-12-24', 1,"+id+", 1, 1)";

        SystemConfigProvider.getTemplate("snddb").update(colPrioritySlots);
    }

    public static void cc(String id) {

        String cViews="INSERT INTO collections.collection_views (`collection_id`, `page`, `view_type`, `view_data`, `version_code`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES ("+id+", 'LISTING', 'MENULET', '{\\\"mainPageDescription\\\": \\\"Look no further to satisfy your cravings! Explore top biryanis under Rs 129 near you.\\\", \\\"minRestaurants\\\": 1, \\\"imageId\\\": \\\"Biryani-3_msxf1h\\\", \\\"maxRestaurants\\\": 10, \\\"maxItems\\\": 2, \\\"backgroundImageId\\\": \\\"Biryani-3_msxf1h\\\", \\\"subTitle\\\": \\\"Look no further to satisfy your cravings! Explore top biryanis under Rs 129 near you.\\\", \\\"title\\\": \\\"Biryanis under Rs 129 for binge-eating\\\", \\\"minItems\\\": 1, \\\"maxItemsPerRestaurant\\\": 4, \\\"viewVariables\\\": [], \\\"type\\\": \\\"MENULET\\\", \\\"minItemsPerRestaurant\\\": 1}', 1, '2018-03-05 20:13:37', 'gsiddardha@swiggy.in', '2018-12-03 20:13:37', 'gsiddardha@swiggy.in')";

        SystemConfigProvider.getTemplate("snddb").update(cViews);
        for(int i=0; i<weeks.length; i++) {
            String colSlots = "insert into collections.collection_slots (collection_id, day, open_time, close_time) values ("+id+ ", '"+weeks[i]+ "'" + ", 0, 2360)";
            SystemConfigProvider.getTemplate("snddb").update(colSlots);
        }
        String colCities="insert into collections.collection_cities (collection_id, city_id) values ("+id+", 1)";
        SystemConfigProvider.getTemplate("snddb").update(colCities);

        String colPrioritySlots="INSERT INTO collections.collection_priority_slots ( `created_on`, `updated_on`, `updated_by`, `created_by`, `start_date`, `end_date`, `parent_id`, `child_id`, `position`, `enabled`) VALUES ( '2018-03-22 18:07:19', '2018-05-10 18:32:31', '', '', '2018-12-01', '2018-12-24', 1,"+id+", 1, 1)";

        SystemConfigProvider.getTemplate("snddb").update(colPrioritySlots);
    }
    String colView="{\"mainPageDescription\": \"Look no further to satisfy your cravings! Explore top biryanis under Rs 129 near you.\", \"minRestaurants\": 2, \"imageId\": \"Biryani-3_msxf1h\", \"maxRestaurants\": 10, \"maxItems\": 4, \"backgroundImageId\": \"Biryani-3_msxf1h\", \"subTitle\": \"Look no further to satisfy your cravings! Explore top biryanis under Rs 129 near you.\", \"title\": \"Biryanis under Rs 129 for binge-eating\", \"minItems\": 1, \"maxItemsPerRestaurant\": 4, \"viewVariables\": [], \"type\": \"MENULET\", \"minItemsPerRestaurant\": 1}";
   // String collectionViews="INSERT INTO `collection_views` (`collection_id`, `page`, `view_type`, `view_data`, `version_code`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES ( "+id+", 'LISTING', 'MENULET', '"+colView+"', 1, '2018-03-05 20:13:37', 'gsiddardha@swiggy.in', '2018-12-03 20:13:37', 'gsiddardha@swiggy.in')";

    public static void colSlotss(String id) {
    for(int i=0; i<weeks.length; i++) {
        String colSlots = "insert into collection_slots (collection_id, day, open_time, close_time) values ("+id+ ", '"+weeks[i]+ "'" + ", 0, 2360)";
    }
    String colCities="insert into collection_cities (collection_id, city_id) values ("+id+", 1)";
    String colPrioritySlots="INSERT INTO `collection_priority_slots` ( `created_on`, `updated_on`, `updated_by`, `created_by`, `start_date`, `end_date`, `parent_id`, `child_id`, `position`, `enabled`)\n" +
        "VALUES ( '2018-03-22 18:07:19', '2018-05-10 18:32:31', '', '', '2018-12-01', '2018-12-24', 1,"+id+", 1, 1)";

}



}


