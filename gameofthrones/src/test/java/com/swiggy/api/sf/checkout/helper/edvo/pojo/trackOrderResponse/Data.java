package com.swiggy.api.sf.checkout.helper.edvo.pojo.trackOrderResponse;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

    /**
     * Fields directly derivable from OrderEntity
     **/
    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("delivery_status")
    private String deliveryStatus;

    @JsonProperty("restaurant")
    private LatLong restaurantLocation;

    @JsonProperty("billing_address")
    private LatLong billingAddressLocation;

    /**
     * Delivery dependent fields
     **/
    @JsonProperty("delivery_boy")
    private LatLong deliveryBoyLocation;

    @JsonProperty("distance")
    private String distance;

    @JsonProperty("delivery_boy_name")
    private String deliveryBoyName;

    @JsonProperty("delivery_boy_mobile")
    private String deliveryBoyMobile;

    @JsonProperty("image_url")
    private String imageUrl;

    @JsonProperty("batched_order_message")
    private String batchedOrderMessage;

    /**
     * New Track Order Messages
     **/
    @JsonProperty("message")
    private String message;

    @JsonProperty("message_position_text")
    private String messagePositionText;

    @JsonProperty("message_position_number")
    private String messagePositionNumber;

    @JsonProperty("flag_for_expand")
    private String flagForExpand;

    @JsonProperty("message_help")
    private MessageHelp messageHelp;

    @JsonProperty("show_eta")
    private boolean showEta;

    @JsonProperty("expected_time")
    private int expectedTime;

    @JsonProperty("orderState")
    private int orderState;

    /**
     * Swiggy Assured messages
     **/
    @JsonProperty("swiggy_assured")
    private String swiggyAssured;

    @JsonProperty("order_total")
    private int orderTotal;

    @JsonProperty("order_items_count")
    private int orderItemsCount;

    @JsonProperty("configurations")
    private Map<String, Object> configurations;

    @JsonProperty("messages")
    private List<Object> messages;

    @JsonIgnore
    private String routingFlag;

    @JsonProperty("batched_destinations")
    private List<LatLong> batchedDestinations = new ArrayList<>();

    @JsonIgnore
    private List<BatchedOrderPath> path;

    @JsonIgnore
    private List<BatchedOrderStatus> statuses;

    @JsonIgnore
    private int noOfOrders;

    @JsonProperty("outlet_contact_number")
    private String restaurantContactNumber;

    @JsonProperty("annotation")
    private String addressAnnotation;

    @JsonProperty("annotation_image_url")
    private String annotationImageUrl;

    @JsonProperty("order_new_state_sequence")
    private Map<String, Object> orderNewStateSequence = new HashMap<>();

    @JsonProperty("order_state_sequence")
    private Map<String, Object> orderStateSequence = new HashMap<>();

    @JsonProperty
    private AbExperimentResponse experimentResponse;

    @JsonProperty
    private String abCase;

    @JsonProperty("on_time")
    private boolean onTimeStatus;

    @JsonProperty("delay_message")
    private String delayMessage;

    @JsonProperty("popup_details")
    private ErrorPopup popup;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public LatLong getRestaurantLocation() {
        return restaurantLocation;
    }

    public void setRestaurantLocation(LatLong restaurantLocation) {
        this.restaurantLocation = restaurantLocation;
    }

    public LatLong getBillingAddressLocation() {
        return billingAddressLocation;
    }

    public void setBillingAddressLocation(LatLong billingAddressLocation) {
        this.billingAddressLocation = billingAddressLocation;
    }

    public LatLong getDeliveryBoyLocation() {
        return deliveryBoyLocation;
    }

    public void setDeliveryBoyLocation(LatLong deliveryBoyLocation) {
        this.deliveryBoyLocation = deliveryBoyLocation;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDeliveryBoyName() {
        return deliveryBoyName;
    }

    public void setDeliveryBoyName(String deliveryBoyName) {
        this.deliveryBoyName = deliveryBoyName;
    }

    public String getDeliveryBoyMobile() {
        return deliveryBoyMobile;
    }

    public void setDeliveryBoyMobile(String deliveryBoyMobile) {
        this.deliveryBoyMobile = deliveryBoyMobile;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getBatchedOrderMessage() {
        return batchedOrderMessage;
    }

    public void setBatchedOrderMessage(String batchedOrderMessage) {
        this.batchedOrderMessage = batchedOrderMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessagePositionText() {
        return messagePositionText;
    }

    public void setMessagePositionText(String messagePositionText) {
        this.messagePositionText = messagePositionText;
    }

    public String getMessagePositionNumber() {
        return messagePositionNumber;
    }

    public void setMessagePositionNumber(String messagePositionNumber) {
        this.messagePositionNumber = messagePositionNumber;
    }

    public String getFlagForExpand() {
        return flagForExpand;
    }

    public void setFlagForExpand(String flagForExpand) {
        this.flagForExpand = flagForExpand;
    }

    public MessageHelp getMessageHelp() {
        return messageHelp;
    }

    public void setMessageHelp(MessageHelp messageHelp) {
        this.messageHelp = messageHelp;
    }

    public boolean isShowEta() {
        return showEta;
    }

    public void setShowEta(boolean showEta) {
        this.showEta = showEta;
    }

    public int getExpectedTime() {
        return expectedTime;
    }

    public void setExpectedTime(int expectedTime) {
        this.expectedTime = expectedTime;
    }

    public int getOrderState() {
        return orderState;
    }

    public void setOrderState(int orderState) {
        this.orderState = orderState;
    }

    public String getSwiggyAssured() {
        return swiggyAssured;
    }

    public void setSwiggyAssured(String swiggyAssured) {
        this.swiggyAssured = swiggyAssured;
    }

    public int getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(int orderTotal) {
        this.orderTotal = orderTotal;
    }

    public int getOrderItemsCount() {
        return orderItemsCount;
    }

    public void setOrderItemsCount(int orderItemsCount) {
        this.orderItemsCount = orderItemsCount;
    }

    public Map<String, Object> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(Map<String, Object> configurations) {
        this.configurations = configurations;
    }

    public List<Object> getMessages() {
        return messages;
    }

    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

    public String getRoutingFlag() {
        return routingFlag;
    }

    public void setRoutingFlag(String routingFlag) {
        this.routingFlag = routingFlag;
    }

    public List<LatLong> getBatchedDestinations() {
        return batchedDestinations;
    }

    public void setBatchedDestinations(List<LatLong> batchedDestinations) {
        this.batchedDestinations = batchedDestinations;
    }

    public List<BatchedOrderPath> getPath() {
        return path;
    }

    public void setPath(List<BatchedOrderPath> path) {
        this.path = path;
    }

    public List<BatchedOrderStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<BatchedOrderStatus> statuses) {
        this.statuses = statuses;
    }

    public int getNoOfOrders() {
        return noOfOrders;
    }

    public void setNoOfOrders(int noOfOrders) {
        this.noOfOrders = noOfOrders;
    }

    public String getRestaurantContactNumber() {
        return restaurantContactNumber;
    }

    public void setRestaurantContactNumber(String restaurantContactNumber) {
        this.restaurantContactNumber = restaurantContactNumber;
    }

    public String getAddressAnnotation() {
        return addressAnnotation;
    }

    public void setAddressAnnotation(String addressAnnotation) {
        this.addressAnnotation = addressAnnotation;
    }

    public String getAnnotationImageUrl() {
        return annotationImageUrl;
    }

    public void setAnnotationImageUrl(String annotationImageUrl) {
        this.annotationImageUrl = annotationImageUrl;
    }

    public Map<String, Object> getOrderNewStateSequence() {
        return orderNewStateSequence;
    }

    public void setOrderNewStateSequence(Map<String, Object> orderNewStateSequence) {
        this.orderNewStateSequence = orderNewStateSequence;
    }

    public Map<String, Object> getOrderStateSequence() {
        return orderStateSequence;
    }

    public void setOrderStateSequence(Map<String, Object> orderStateSequence) {
        this.orderStateSequence = orderStateSequence;
    }

    public AbExperimentResponse getExperimentResponse() {
        return experimentResponse;
    }

    public void setExperimentResponse(AbExperimentResponse experimentResponse) {
        this.experimentResponse = experimentResponse;
    }

    public String getAbCase() {
        return abCase;
    }

    public void setAbCase(String abCase) {
        this.abCase = abCase;
    }

    public boolean isOnTimeStatus() {
        return onTimeStatus;
    }

    public void setOnTimeStatus(boolean onTimeStatus) {
        this.onTimeStatus = onTimeStatus;
    }

    public String getDelayMessage() {
        return delayMessage;
    }

    public void setDelayMessage(String delayMessage) {
        this.delayMessage = delayMessage;
    }

    public ErrorPopup getPopup() {
        return popup;
    }

    public void setPopup(ErrorPopup popup) {
        this.popup = popup;
    }
}
