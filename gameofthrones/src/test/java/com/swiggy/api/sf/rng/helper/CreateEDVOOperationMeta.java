package com.swiggy.api.sf.rng.helper;

import org.codehaus.jackson.annotate.JsonProperty;

public class  CreateEDVOOperationMeta{
	@JsonProperty("mealHeader")
	private String mealHeader;
	@JsonProperty("mealDescription")
	private String mealDescription;
	
	public CreateEDVOOperationMeta() {
		 this.mealHeader = null;
		 this.mealDescription = null;
		
	} 
public CreateEDVOOperationMeta(String mealHeader, String mealDescription) {
	 this.mealHeader = mealHeader;
	 this.mealDescription = mealDescription;
	} 

public String getMealHeader() {
	 
	return mealHeader;
}
	
	public void setMealHeader(String mealHeader) {
		this.mealHeader= mealHeader;
	}
	
	
	public String getMealDescription() {
		return mealDescription;
	}
	
	public void setMealDescription(String mealDescription) {
		this.mealDescription = mealDescription;
	}
	
	@Override
	public String toString() {
		if(!(mealHeader==null)) {
		return " \"operationMeta\""+ ":"+ "{" 
				+  " \"mealHeader\"" + ":" + "\""+ mealHeader  + "\"" +","+
				" \"mealDescription\"" + ":" + "\"" + mealDescription + "\"" +
				" },";
		}
		
		return  " \"operationMeta\""+ ":"+ "null,";
	
	
	
	
	}
}
	
	
	
	
	
	
	
