package com.swiggy.api.sf.rng.pojo.SwiggySuper;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.SwiggySuper
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Random;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "user_id",
        "plan_id",
        "order_id"
})
public class CreateSubscriptionV2 {
    @JsonProperty("user_id")
    private Integer user_id;
    @JsonProperty("plan_id")
    private Integer plan_id;
    @JsonProperty("order_id")
    private String order_id;

    @JsonProperty("user_id")
    public Integer getUser_id() {
        return user_id;
    }

    @JsonProperty("user_id")
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    @JsonProperty("plan_id")
    public Integer getPlan_id() {
        return plan_id;
    }

    @JsonProperty("plan_id")
    public void setPlan_id(Integer plan_id) {
        this.plan_id = plan_id;
    }

    @JsonProperty("order_id")
    public String getOrder_id() {
        return order_id;
    }

    @JsonProperty("order_id")
    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    private int getRandomValue() {
        Random random = new Random();
        int r = random.nextInt(99999)+1111;
        return r;
    }

    private void setDefaultValues(int planID,int userID) {
        if (getPlan_id() == null)
            this.setPlan_id(planID);
        if (getUser_id() == null)
            this.setUser_id(userID);
        if (this.getOrder_id() == null)
            this.setOrder_id(String.valueOf(getRandomValue()));
    }

    public CreateSubscriptionV2 build(int planID,int userID) {
        setDefaultValues(planID,userID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("user_id", user_id).append("plan_id", plan_id).append("order_id", order_id).toString();
    }

}
