package com.swiggy.api.sf.rng.helper;

import com.swiggy.api.sf.rng.constants.RngConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.lang.RandomStringUtils;

import java.util.*;

public class TDPerfTest {
	Random randomInteger = new Random();

	RngHelper rngHelper = new RngHelper();

	private  String getRandomOrderId() {
		int codeLength = 6;
		// TODO Auto-generated method stub
		RandomStringUtils.randomNumeric(codeLength);
		return RandomStringUtils.randomNumeric(codeLength);
	}

	public static HashMap campNumberInput() {
		HashMap hash = new HashMap();
		System.out.println("Press 0  to Create equal no. of campaigns  OR  Press 1 to create customize campaigns");
		Scanner scn = new Scanner(System.in);
		String input = scn.nextLine();
		String a = "0";
		String types = "";
		String level = "";
		String[] all = new String[6];
		String[] def = new String[1];
		while (!input.equals("-1")) {
			switch (input) {
			case "0":
				System.out.println("enter total no. of camp. ");
				String total = scn.nextLine();
				hash.put(input, total);
				return hash;
			case "1":
				System.out.println("Please select 2 for Type or select 3 for level to create camp.");
				input = scn.nextLine();
				break;
			case "2":
				for (int i = 0; i <= 5; i++) {
					System.out.println("Select following For type of camp \n 1 -Discount type  \n 2 -Flat type "
							+ "\n 3 -Free Delivery  \n 4 -Freebie  \n 5  -MultiTD  \n 6  - 1 camp. multi. rest. ");
					types = scn.nextLine();
					System.out.println("enter no. of camp.   ");
					String num = scn.nextLine();
					all[i] = types + " type - no. of camp   " + num;
					hash.put(types, num);
				}
				return hash;
			case "3":
				for (int i = 0; i <= 3; i++) {
					System.out.println(
							"Select following for level of camp \n 11 -Restaurant level  \n 22 -category level "
									+ "\n 33 -sub category level  \n 44 -item level ");
					level = scn.nextLine();
					System.out.println("enter no. of camp.   ");
					String num = scn.nextLine();
					all[i] = level + " level - no. of camp   " + num;
					hash.put(level, num);
				}
				return hash;
			}
		}
		return hash;
	}

	public List getTdEnableRestIds() {
		String TotdayDate = RngHelper.getCurrentDate();
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(RngConstants.selectActiveCamp+TotdayDate+RngConstants.endSelectActiveCamp+TotdayDate+RngConstants.endQuotes);
		
		System.out.println("q " + RngConstants.selectActiveCamp+TotdayDate+RngConstants.endSelectActiveCamp+TotdayDate+RngConstants.endQuotes);
		
	
		int len = list.size();
		String[] campList = new String[len];
		String rest_id = "";
		for (int i = 0; i < len; i++) {
			String Camp_id = (list.size() > 0) ? (list.get(i).get("id").toString()) : "no active campaigns in DB";
			campList[i] = Camp_id;
		}

		String campaignIds = String.join(",", campList);
		System.out.println("Querry is .. "+RngConstants.selectActiveCampRest + campaignIds + RngConstants.endPart );
		List<Map<String, Object>> list2 = sqlTemplate.queryForList(RngConstants.selectActiveCampRest + campaignIds + RngConstants.endPart);
		int len2 = list2.size();
		List restList = new ArrayList();

		for (int i = 0; i < len2; i++) {
			rest_id = (list2.size() > 0) ? (list2.get(i).get("restaurant_id").toString()) : "no restaurant in DB";
			restList.add(rest_id);
		}
		return restList;
	}

	public static Map<String, List<String>> getTDEnabledItemIds(List restIds) {
		String restList = String.join(",", restIds);

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectTdActiveItemIds + restList + RngConstants.endPart);
		List entityList = new ArrayList();

		Map<String, List<String>> restIdToItemMap = new HashMap();
		for (int i = 0; i < list.size(); i++) {
			String restId = list.get(i).get("restaurant_id").toString();
			String entityId = list.get(i).get("entity_id").toString();
			if (restIdToItemMap.get(restId) != null) {
				List<String> values = restIdToItemMap.get(restId);
				values.add(entityId);
				restIdToItemMap.put(restId, values);
			} else {
				List<String> values = new ArrayList();
				values.add(entityId);
				restIdToItemMap.put(restId, values);
			}
		}
		return restIdToItemMap;
	}

	public static Map<String, List<String>> getEDVOTdEnabledGroupIds(List restIds) {
		String restList = String.join(",", restIds);

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectEDVOTdActiveGroupIds + restList + RngConstants.endPart);
		
		
		System.out.println(RngConstants.selectEDVOTdActiveGroupIds + restList + RngConstants.endPart);
		List entityList = new ArrayList();

		Map<String, List<String>> restIdToGroupMap = new HashMap();
		for (int i = 0; i < list.size(); i++) {
			String restId = list.get(i).get("restaurant_id").toString();
			String entityId = list.get(i).get("entity_id").toString();
			if (restIdToGroupMap.get(restId) != null) {
				List<String> values = restIdToGroupMap.get(restId);
				values.add(entityId);
				restIdToGroupMap.put(restId, values);
			} else {
				List<String> values = new ArrayList();
				values.add(entityId);
				restIdToGroupMap.put(restId, values);
			}
		}
		return restIdToGroupMap;
	}

	public Map<String, List<String>> getEDVORestIdMealId() {
		// String TotdayDate = rngHelper.getCurrentDate();

		String TotdayDate = "2018-02-22";
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectRestId_OperationId + TotdayDate + RngConstants.endQuotes
						+ RngConstants.endSelectRestId_OperationId + TotdayDate + RngConstants.endQuotes);
		int len2 = list.size();
		String rest_id = "";
		String rest_id1 = "";

		Map<String, List<String>> restIdAndMealIdMap = new HashMap();
		for (int i = 0; i < list.size(); i++) {
			String restId = list.get(i).get("restaurant_id").toString();
			String entityId = list.get(i).get("operation_id").toString();
			if (restIdAndMealIdMap.get(restId) != null) {
				List<String> values = restIdAndMealIdMap.get(restId);
				values.add(entityId);
				restIdAndMealIdMap.put(restId, values);
			} else {
				List<String> values = new ArrayList();
				values.add(entityId);
				restIdAndMealIdMap.put(restId, values);
			}
		}
		return restIdAndMealIdMap;
	}

	public List<String> getEDVOEnableTdRestIds() {
		 String TotdayDate = RngHelper.getCurrentDate();

		//String TotdayDate = "2018-02-22";
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
		System.out.println("query = " + RngConstants.selectEDVOActiveRestIds + TotdayDate
				+ RngConstants.endSelectEDVOActiveRestIds + TotdayDate + RngConstants.endQuotes);
		List<Map<String, Object>> list = sqlTemplate.queryForList(RngConstants.selectEDVOActiveRestIds + TotdayDate+ RngConstants.endSelectEDVOActiveRestIds + TotdayDate + RngConstants.endQuotes);
		int len = list.size();
		String[] restList = new String[len];
		String rest_id1 = "";

		System.out.println("size = " + list.size());
		List<String> rest_idList = new ArrayList();
		for (int i = 0; i < len; i++) {
			rest_id1 = (list.size() > 0) ? (list.get(i).get("restaurant_id").toString()) : "no rest. in DB";
			rest_idList.add(rest_id1);
		}
		return rest_idList;
	}

	public static void main(String[] args) {
		Initialize gameOfThrowns = new Initialize();
		long startTime;  long endTime;
		 //campNumberInput();

//				 System.out.println("Listing payload is in progess ....");
//				 startTime = new Date().getTime();
//				 PopulateListingData populateListingData = new PopulateListingData();
//				 populateListingData.createListingPayload();
//				 endTime = new Date().getTime();
//				 System.out.println("Listing execution is done in " + (endTime - startTime));
	
//		 System.out.println("Menu payload is in progess ....");
//		 startTime = new Date().getTime();
//		 PopulateMenuData populateMenuData = new PopulateMenuData();
//		 populateMenuData.createMenuPayload();
//		 endTime = new Date().getTime();
//		 System.out.println("Menu execution is done in " + (endTime - startTime));
		
		
		 System.out.println("Cart payload is in progess ....");
		 startTime = new Date().getTime();
		 PopulateCartData populateCartData = new PopulateCartData();
		 populateCartData.createCartPayload();
		 endTime = new Date().getTime();
		 System.out.println("Cart execution is done in " + (endTime - startTime));

//		 System.out.println("CartV3 execution is in progess ....");
//		 startTime = new Date().getTime();
//		 PopulateCartV3Data populateCartV3Data = new PopulateCartV3Data();
//		 populateCartV3Data.createCartPayload();
//		 endTime = new Date().getTime();
//		 System.out.println("CartV3 execution is done in " + (endTime - startTime));
//
//		System.out.println("Meal execution is in progess ....");
//		startTime = new Date().getTime();
//		PopulateMealData populateMealData = new PopulateMealData();
//		populateMealData.createMealPayload();
//		endTime = new Date().getTime();
//		System.out.println("Meal execution is done in " + (endTime - startTime));

	}

}
