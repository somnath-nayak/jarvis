package com.swiggy.api.sf.snd.pojo.MenuMerchandising;

import org.codehaus.jackson.annotate.JsonProperty;

public class CreateItemCarouselPayload {

    private String enabled;

    private String fontColor;

    private String gradientStart;

    private String startDateTime;

    private String bannerName;

    private String redirectLink;

    private String type;

    private String endDateTime;

    private TimeSlots[] timeSlots;

    private String updatedBy;

    private String bgColor;

    private String creativeId;

    private String createdBy;

    private String description;

    private String priority;

    private String createdAt;

    private String gradientEnd;

    private String metatypes;

    private String channel;

    private String restaurantId;

    public String getEnabled ()
    {
        return enabled;
    }

    public void setEnabled (String enabled)
    {
        this.enabled = enabled;
    }

    public String getFontColor ()
    {
        return fontColor;
    }

    public void setFontColor (String fontColor)
    {
        this.fontColor = fontColor;
    }

    public String getGradientStart ()
    {
        return gradientStart;
    }

    public void setGradientStart (String gradientStart)
    {
        this.gradientStart = gradientStart;
    }

    public String getStartDateTime ()
    {
        return startDateTime;
    }

    public void setStartDateTime (String startDateTime)
    {
        this.startDateTime = startDateTime;
    }

    public String getBannerName ()
    {
        return bannerName;
    }

    public void setBannerName (String bannerName)
    {
        this.bannerName = bannerName;
    }

    public String getRedirectLink ()
    {
        return redirectLink;
    }

    public void setRedirectLink (String redirectLink)
    {
        this.redirectLink = redirectLink;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getEndDateTime ()
    {
        return endDateTime;
    }

    public void setEndDateTime (String endDateTime)
    {
        this.endDateTime = endDateTime;
    }

    public TimeSlots[] getTimeSlots ()
    {
        return timeSlots;
    }

    public void setTimeSlots (TimeSlots[] timeSlots)
    {
        this.timeSlots = timeSlots;
    }

    public String getUpdatedBy ()
    {
        return updatedBy;
    }

    public void setUpdatedBy (String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getBgColor ()
    {
        return bgColor;
    }

    public void setBgColor (String bgColor)
    {
        this.bgColor = bgColor;
    }

    public String getCreativeId ()
    {
        return creativeId;
    }

    public void setCreativeId (String creativeId)
    {
        this.creativeId = creativeId;
    }

    public String getCreatedBy ()
    {
        return createdBy;
    }

    public void setCreatedBy (String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getPriority ()
    {
        return priority;
    }

    public void setPriority (String priority)
    {
        this.priority = priority;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getGradientEnd ()
    {
        return gradientEnd;
    }

    public void setGradientEnd (String gradientEnd)
    {
        this.gradientEnd = gradientEnd;
    }

    public String getMetatypes ()
    {
        return metatypes;
    }

    public void setMetatypes (String metatypes)
    {
        this.metatypes = metatypes;
    }

    public String getChannel ()
    {
        return channel;
    }

    public void setChannel (String channel)
    {
        this.channel = channel;
    }

    public String getRestaurantId ()
    {
        return restaurantId;
    }

    public void setRestaurantId (String restaurantId)
    {
        this.restaurantId = restaurantId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [enabled = "+enabled+", fontColor = "+fontColor+", gradientStart = "+gradientStart+", startDateTime = "+startDateTime+", bannerName = "+bannerName+", redirectLink = "+redirectLink+", type = "+type+", endDateTime = "+endDateTime+", timeSlots = "+timeSlots+", updatedBy = "+updatedBy+", bgColor = "+bgColor+", creativeId = "+creativeId+", createdBy = "+createdBy+", description = "+description+", priority = "+priority+", createdAt = "+createdAt+", gradientEnd = "+gradientEnd+", metatypes = "+metatypes+", channel = "+channel+", restaurantId = "+restaurantId+"]";
    }
}
