package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class OrderRequest {
	@JsonProperty("orderEdited")
	private Boolean orderEdited;
	@JsonProperty("orderEditSource")
	private Integer orderEditSource;

	@JsonProperty("orderEdited")
	public Boolean getOrderEdited() {
		return orderEdited;
	}

	@JsonProperty("orderEdited")
	public void setOrderEdited(Boolean orderEdited) {
		this.orderEdited = orderEdited;
	}

	public OrderRequest withOrderEdited(Boolean orderEdited) {
		this.orderEdited = orderEdited;
		return this;
	}

	@JsonProperty("orderEditSource")
	public Integer getOrderEditSource() {
		return orderEditSource;
	}

	@JsonProperty("orderEditSource")
	public void setOrderEditSource(Integer orderEditSource) {
		this.orderEditSource = orderEditSource;
	}

	public OrderRequest withOrderEditSource(Integer orderEditSource) {
		this.orderEditSource = orderEditSource;
		return this;
	}
}
