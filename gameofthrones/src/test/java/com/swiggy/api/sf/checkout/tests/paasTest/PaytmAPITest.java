package com.swiggy.api.sf.checkout.tests.paasTest;

import com.swiggy.api.sf.checkout.constants.PaasConstant;
import com.swiggy.api.sf.checkout.helper.paas.MySmsValidator;
import com.swiggy.api.sf.checkout.helper.paas.PaasWalletValidator;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

public class PaytmAPITest {

    PaasWalletValidator paasWalletValidator = new PaasWalletValidator();
    MySmsValidator mySmsValidator = new MySmsValidator();

    String mobile = System.getenv("mobile");
    String password = System.getenv("password");

    String tid;
    String token;
    String state;
    String authToken;

    @BeforeClass
    public void login() {
        if (mobile == null || mobile.isEmpty()) {
            mobile = PaasConstant.MOBILE;
        }
        if (password == null || password.isEmpty()) {
            password = PaasConstant.PASSWORD;
        }

        HashMap<String, String> hashMap = paasWalletValidator.loginTokenData(mobile, password);
        tid = hashMap.get("tid");
        token = hashMap.get("token");
        authToken = mySmsValidator.getAuthToken();
    }

    @Test(priority = 1)
    public void invokePaytmLinkApi() {

        int statusCode = paasWalletValidator.callPaytmCheckBalance(tid, token);

        if (statusCode != 0) {
            mySmsValidator.deleteAllConversation(authToken);

            Processor processor = paasWalletValidator.validatePaytmLink(tid,token);
            state =processor.ResponseValidator.GetNodeValue("$.data.state");
            Assert.assertNotNull(state);
        } else {
            paasWalletValidator.callPaytmDelink(tid, token);
            invokePaytmLinkApi();
        }
    }

    @Test (dependsOnMethods="invokePaytmLinkApi",priority = 2)
    public void invokePaytmValidateOTPApi() {
        String otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.PAYTM_ADDRESS);
        int retry = 0;

        while (otp==null && retry < PaasConstant.MAX_RETRY){
            invokePaytmLinkApi();
            otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.PAYTM_ADDRESS);
            retry++;
        }

        paasWalletValidator.validatePaytmOtp(tid, token, state,otp);
    }

    @Test (dependsOnMethods="invokePaytmValidateOTPApi",priority = 3)
    public void invokePaytmCheckBalanceApi() {
        paasWalletValidator.validatePaytmCheckBalance(tid, token);
    }

    @Test (dependsOnMethods="invokePaytmValidateOTPApi",priority = 4)
    public void invokePaytmHasTokenApi() {
        paasWalletValidator.validatePaytmHasToken(tid, token);
    }

    @Test (dependsOnMethods="invokePaytmValidateOTPApi",priority = 5)
    public void invokePaytmDeLinkApi() {
        paasWalletValidator.validatePaytmDelink(tid, token);
    }


}