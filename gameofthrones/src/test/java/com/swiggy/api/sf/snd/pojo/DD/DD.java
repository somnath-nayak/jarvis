package com.swiggy.api.sf.snd.pojo.DD;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class DD {

    private String description;
    private String name;
    private String slug;
    @JsonProperty("collectable_type")
    private String collectableType;
    private Boolean enabled;
    @JsonProperty("enabled_filters")
    private EnabledFilters enabledFilters;
    private List<Slot> slots = null;
    @JsonProperty("start_date")
    private String startDate;
    @JsonProperty("end_date")
    private String endDate;
    private List<Integer> cities = null;
    @JsonProperty("sort_by")
    private String sortBy;
    private List<Targetting> targetting = null;
    private List<View> view = null;
    private List<Coc> coc = null;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getCollectableType() {
        return collectableType;
    }

    public void setCollectableType(String collectableType) {
        this.collectableType = collectableType;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public EnabledFilters getEnabledFilters() {
        return enabledFilters;
    }

    public void setEnabledFilters(EnabledFilters enabledFilters) {
        this.enabledFilters = enabledFilters;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<Integer> getCities() {
        return cities;
    }

    public void setCities(List<Integer> cities) {
        this.cities = cities;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public List<Targetting> getTargetting() {
        return targetting;
    }

    public void setTargetting(List<Targetting> targetting) {
        this.targetting = targetting;
    }

    public List<View> getView() {
        return view;
    }

    public void setView(List<View> view) {
        this.view = view;
    }

    public List<Coc> getCoc() {
        return coc;
    }

    public void setCoc(List<Coc> coc) {
        this.coc = coc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("description", description).append("name", name).append("slug", slug).append("collectable_type", collectableType).append("enabled", enabled).append("enabled_filters", enabledFilters).append("slots", slots).append("startDate", startDate).append("endDate", endDate).append("cities", cities).append("sortBy", sortBy).append("targetting", targetting).append("view", view).append("coc", coc).toString();
    }

}