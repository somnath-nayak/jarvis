package com.swiggy.api.sf.rng.pojo;

public class ApplyCouponGlobalOffsetPOJO {

    private String code;
    private int areaId;
    private int cityId;
    private Double swiggyTradeDiscount;
    private Double restaurantTradeDiscount;
    private  Long itemKey;
    private Integer menuItemId;
    private int quantity;
    private String restaurantId;
    private String userId;
    private Double swiggyMoney;
    private Boolean firstOrder;
    private Integer cartPrice_Quantity;
    private Double cartTotal;
    private String itemLevel;
    private Integer itemLevelPriceQuantity;
    private Double  itemLevelPriceSubTotal;
    private String preferredPaymentMethod;
    private Boolean referralPresence;
    private Boolean swiggyMoneyApplicable;
    private Boolean superUser;
    private String typeOfPartner=null;
    private  Integer versionCode=245;
    private String userAgent="ANDROID";
    private String payment_codes=null;
    private String time=null;

    public ApplyCouponGlobalOffsetPOJO(){}
    public  ApplyCouponGlobalOffsetPOJO(String code, String userId,String time){
        this.code = code;
        this.superUser = false;
        this.cartTotal = 200.00;
        this.cartPrice_Quantity = 1;
        this.areaId = 1;
        this.cityId =1;
        this.swiggyTradeDiscount = 0.0;
        this.restaurantTradeDiscount = 0.0;
        this.itemKey = null;
        this.menuItemId = 52826;
        this.quantity = 1;
        this.restaurantId = "28157";
        this.userId = userId;
        this.swiggyMoney = 0.0;
        this.firstOrder = false;
        this.cartPrice_Quantity = cartPrice_Quantity;
        this.cartTotal =cartTotal;
        this.itemLevel = "5282696_0_variants_addons";
        this.itemLevelPriceQuantity = 1;
        this.itemLevelPriceSubTotal = 200.0;
        this.preferredPaymentMethod = "\"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb\"";
        this.referralPresence = false;
        this.swiggyMoneyApplicable = false;
        this.swiggyMoneyApplicable = false;
        this.time=time;

    }

    @Override
    public String toString() {

        return "{"
                + "\"code\"  : " + "\""+code + "\""+ ","
                + "\"cart\" :  {"
                + "\"restaurantListing\" : {"

                + "\"area\" :  {"
                + "\"areaEntity\" : {"
                + "\"id\" : " + areaId
                + "}  },"

                + "\"city\" :  {"
                + "\"cityEntity\" : {"

                + "\"id\" : " + cityId
                + " } } },"
                + "\"cartBlob\" :  {"
                + "\"swiggyTradeDiscount\" : " + swiggyTradeDiscount + ","
                + "\"restaurantTradeDiscount\" : " + restaurantTradeDiscount + ","
                + "\"cartItems\": [{"
                + "\"itemKey\" : " + itemKey + ","
                + "\"menu_item_id\":" + menuItemId + ","
                + "\"quantity\":" + quantity + ","
                + "\"restaurantId\":" + "\"" + restaurantId + "\""
                + "}]},"
                + "\"cartUser\": {"
                + "\"userId\" : " + userId + ","
                + "\"swiggyMoney\":" + swiggyMoney + ","
                + "\"firstOrder\":" + firstOrder
                + "},"
                + "\"cartPrice\": {"
                + "\"quantity\":" + cartPrice_Quantity + ","
                + "\"cartTotal\" : " + cartTotal + ","
                + "\"itemLevelPrice\": {"
                + "\"" + itemLevel + "\"  : {"
                + "\"quantity\":" + itemLevelPriceQuantity + ","
                + "\"subTotal\":" + itemLevelPriceSubTotal
                + "}},"
                + "\"preferredPaymentMethod\":" + preferredPaymentMethod + ","
                + "\"referralPresence\":" + referralPresence + ","
                + "\"swiggyMoneyApplicable\":" + swiggyMoneyApplicable
                + "},"
                + "\"superUser\": " + superUser + ","
                + "\"typeOfPartner\" : " + typeOfPartner + ","
                + "\"swiggyMoneyApplicable\":" + swiggyMoneyApplicable
                + "},"
                + "\"headers\": {"
                + "\"versionCode\":" + versionCode + ","
                + "\"userAgent\":" + "\""+userAgent+"\""
                + "},"
                + "\"paymentMethods\": {"
                + "\"payment_codes\":" + payment_codes
                + "},"
                + "\"time\":" + time
                + "}";

    }
}
