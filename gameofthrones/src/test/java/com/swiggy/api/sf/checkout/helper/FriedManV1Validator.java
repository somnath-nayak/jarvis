
package com.swiggy.api.sf.checkout.helper;

import java.util.LinkedHashMap;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import com.swiggy.api.sf.rng.helper.RngHelper;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.RedisHelper;

public class FriedManV1Validator {
	
	SoftAssert softAssertion= new SoftAssert();
	LinkedHashMap<String, Integer> map;
	LinkedHashMap<String, Integer> resMap;
	LinkedHashMap<String, Integer> delMap;
	
     public void validateCartDetails(String cartResponse){
		
		map=new LinkedHashMap<String, Integer>();
		resMap=new LinkedHashMap<String, Integer>();
		delMap=new LinkedHashMap<String, Integer>();
		
		 String[] renderingDetails=parseJson(cartResponse, "$.data.rendering_details[*].key").split(",");
		 
		 for(int i=0;i<renderingDetails.length;i++){
		     map.put(renderingDetails[i], i);
		  }
		
		Reporter.log("New rendering_details Keys:-->"+map,true);
		
		
		
		String isCouponValid=parseJson(cartResponse, "$.data..is_coupon_valid");
		if(isCouponValid.equals("0")){
			if (map.containsKey("apply_coupon_button")){
				validateRenderingData(cartResponse,map.get("apply_coupon_button"),"value","APPLY COUPON","apply_coupon_button");
			} else{
				softAssertion.assertEquals(true, false,"apply_coupon_button is not present in rendering details");
			}
		}      
		
			
		Double subsTotal=Double.parseDouble(parseJson(cartResponse, "$.data.subscription_total"));
		if (subsTotal>0){
				if (map.containsKey("subscription_total")){
					validateRenderingData(cartResponse,map.get("subscription_total"),"value",subsTotal,"subscription_total");
				} else{
					softAssertion.assertEquals(true, false,"subscription_total is not present in rendering details");
				}
		}
		
		Double canFee=Double.parseDouble(parseJson(cartResponse, "$.data..cancellation_fee"));
		Double swiggyMoney=Double.parseDouble(parseJson(cartResponse, "$.data..swiggy_money"));
		if (swiggyMoney>0){
			if (map.containsKey("swiggy_money")){
					validateRenderingData(cartResponse,map.get("swiggy_money"),"value",swiggyMoney,"swiggy_money");
				} else{
					softAssertion.assertEquals(true, false,"swiggy_money is not present in rendering details");
				}
		}
		
		if (canFee>0){
             if (map.containsKey("cancellation_fee")){
					validateRenderingData(cartResponse,map.get("cancellation_fee"),"value",canFee,"cancellation_fee");
				} else{
					softAssertion.assertEquals(true, false,"cancellation_fee is not present in rendering details");
				}   
		}
		
		
		if (isCouponValid.equals("1")){
			Double discount=Double.parseDouble(parseJson(cartResponse, "$.data..coupon_discount_total"));
				if (map.containsKey("coupon_discount")){
					validateRenderingData(cartResponse,map.get("coupon_discount"),"value",discount,"coupon_discount");
				} else{
					softAssertion.assertEquals(true, false,"coupon_discount is not present in rendering details");
				}
		}
		
		Double delCharges=Double.parseDouble(parseJson(cartResponse, "$.data..delivery_charges"));
		Double conFee=Double.parseDouble(parseJson(cartResponse, "$.data..convenience_fee"));
		String isSuperTD=parseJson(cartResponse, "$.data..is_super_trade_discount");
		
		if (delCharges.equals(0.0) && conFee.equals(0.0) && isSuperTD.equals("false")){
			  if (map.containsKey("free_non_super_delivery")){
					validateRenderingData(cartResponse,map.get("free_non_super_delivery"),"value","FREE","free_non_super_delivery");
				} else{
					softAssertion.assertEquals(true, false,"free_non_super_delivery is not present in rendering details");
				}
		}
		
		
        String showSuperNudge=parseJson(cartResponse, "$.data..show_super_nudge");
		if (showSuperNudge.equals("true")){
				if (map.containsKey("super_nudge_button")){
					int i=map.get("super_nudge_button");
					String nudgeCTA=parseJson(cartResponse, "$.data..subscription_nudge..subscription_nudge_cta");
					String nudgeTittle=parseJson(cartResponse, "$.data..subscription_nudge..subscription_nudge_title");
					String nudgeSubTittle=parseJson(cartResponse, "$.data..subscription_nudge..subscription_nudge_subtitle");
					validateRenderingData(cartResponse,i,"value",nudgeCTA,"super_nudge_button");
					validateRenderingData(cartResponse,i,"display_text",nudgeTittle,"subscription_nudge_title");
					validateRenderingData(cartResponse,i,"info_text",nudgeSubTittle,"subscription_nudge_subtitle");
				} else{
					softAssertion.assertEquals(true, false,"super_nudge_button is not present in rendering details");
				}
		}
		
		
		Double TDTotal=Double.parseDouble(parseJson(cartResponse, "$.data..trade_discount_total"));
		Double TDWOFreebie=Double.parseDouble(parseJson(cartResponse, "$.data..trade_discount_without_freebie"));
		if (TDWOFreebie>0.0){
				if (map.containsKey("offers_discount")){
					validateRenderingData(cartResponse,map.get("offers_discount"),"value",TDWOFreebie,"offers_discount");
				} else{
					softAssertion.assertEquals(true, false,"offers_discount is not present in rendering details");
				}
		}
		
		if (map.containsKey("bill_heading")){
				validateRenderingData(cartResponse,map.get("bill_heading"),"display_text","Restaurant Bill","bill_heading");
			} else{
				softAssertion.assertEquals(true, false,"bill_heading is not present in rendering details");
			}

		
    	if (map.containsKey("item_total")){
				double actualValue=Double.parseDouble(parseJson(cartResponse, "$.data.item_total"));
				validateRenderingData(cartResponse,map.get("item_total"),"value",actualValue,"item_total");
			} else{
				softAssertion.assertEquals(true, false,"item_total is not present in rendering details");
			}
		
		if (map.containsKey("grand_total")){
				double exValue=Double.parseDouble(parseJson(cartResponse, "$.data..order_total"));
				validateRenderingData(cartResponse,map.get("grand_total"),"value",exValue,"grand_total");
			} else{
				softAssertion.assertEquals(true, false,"grand_total is not present in rendering details");
		}
         
		double packingCharge = Double.parseDouble(parseJson(cartResponse, "$.data.total_packing_charges"));
		double sGST = Double.parseDouble(parseJson(cartResponse, "$.data.GST_details.cart_SGST"));
		double cGST = Double.parseDouble(parseJson(cartResponse, "$.data.GST_details.cart_CGST"));
		double iGST = Double.parseDouble(parseJson(cartResponse, "$.data.GST_details.cart_IGST"));
		double exGST =Double.parseDouble(parseJson(cartResponse, "$.data.GST_details.external_GST"));
		double actualResTotal=packingCharge+sGST+cGST+iGST+exGST;
		
		if (map.containsKey("restaurant_total")){
				//double actualValue=packingCharge+sGST+cGST+iGST+exGST;
				validateRenderingData(cartResponse,map.get("restaurant_total"),"value",actualResTotal,"restaurant_total");
			} else{
				softAssertion.assertEquals(true, false,"restaurant_total is not present in rendering details");
		}
		
		//verify restaurant_charges_title_v2
		if (actualResTotal>0 && map.containsKey("restaurant_total")){
		String actualDisText="Charged by restaurant";
		setResMap(cartResponse);
		Reporter.log("New Sub_rendering_details restaurnats Keys:-->"+resMap,true);
		validateSubRenderingData(cartResponse,
				map.get("restaurant_total"),resMap.get("restaurant_charges_title_v2"),"display_text",actualDisText,"restaurant_charges_title_v2");
		}
		
		if(packingCharge>0){
			if(resMap.containsKey("packing_charges_v2")){
				validateSubRenderingData(cartResponse, map.get("restaurant_total"), resMap.get("packing_charges_v2"),"value", packingCharge,
						"packing_charges_v2");
			}else{
				softAssertion.assertEquals(true, false,"packing_charges_v2 is not present in rendering details");
		}}
		
		if(sGST>0){
			if(resMap.containsKey("SGST_v2")){
				validateSubRenderingData(cartResponse, map.get("restaurant_total"), resMap.get("SGST_v2"),"value", sGST,"SGST_v2");
			}else{
				softAssertion.assertEquals(true, false,"SGST_v2 is not present in rendering details");
		}}
	
		if(cGST>0){
			if(resMap.containsKey("CGST_v2")){
				validateSubRenderingData(cartResponse, map.get("restaurant_total"), resMap.get("CGST_v2"),"value", cGST,"CGST_v2");
			}else{
				softAssertion.assertEquals(true, false,"CGST_v2 is not present in rendering details");
		}}
		
		if(iGST>0){
			if(resMap.containsKey("IGST_v2")){
				validateSubRenderingData(cartResponse, map.get("restaurant_total"), resMap.get("IGST_v2"),"value", iGST,"IGST_v2");
			}else{
				softAssertion.assertEquals(true, false,"IGST_v2 is not present in rendering details");
		}}
		
		if(exGST>0){
			if(resMap.containsKey("external_gst_v2")){
				validateSubRenderingData(cartResponse, map.get("restaurant_total"), resMap.get("external_gst_v2"),"value", iGST,"external_gst_v2");
			}else{
				softAssertion.assertEquals(true, false,"external_gst_v2 is not present in rendering details");
		}}
		
		
		
		
		String freeDelDiscount=parseJson(cartResponse, "$.data..free_delivery_discount");
		if (isSuperTD.equals("true")){
			if (map.containsKey("free_super_delivery_breakup")){
					validateRenderingData(cartResponse,map.get("free_super_delivery_breakup"),"value","0","free_super_delivery_breakup");
					validateRenderingData(cartResponse,map.get("free_super_delivery_breakup"),"intermediateText",freeDelDiscount,"free_delivery_discount");
				} else{
					softAssertion.assertEquals(true, false,"free_super_delivery_breakup is not present in rendering details");
				}
		}
		
		String couponValid=parseJson(cartResponse, "$.data..is_coupon_valid");
		Double couponDis=Double.parseDouble(parseJson(cartResponse, "$.data..coupon_discount_total"));
		if (couponValid.equals("1")){
			String couponCode=parseJson(cartResponse, "$.data..coupon_code");
             if (map.containsKey("remove_coupon_button")){
					validateRenderingData(cartResponse,map.get("remove_coupon_button"),"value",couponDis,"remove_coupon_button");
					validateRenderingData(cartResponse,map.get("remove_coupon_button"),"intermediateText",couponCode,"coupon_code");
				} else{
					softAssertion.assertEquals(true, false,"remove_coupon_button is not present in rendering details");
				}
		}
		
		String cartType=parseJson(cartResponse, "$.data..cart_type");
		Double delCharge=Double.parseDouble(parseJson(cartResponse, "$.data..delivery_charges"));
		Double confee=Double.parseDouble(parseJson(cartResponse, "$.data..convenience_fee"));
		String TDrewardList=parseJson(cartResponse, "$.data..trade_discount_reward_list");
		//String surgeFeeIcon=parseJson(cartResponse, "$.data..surge_fee_icon");
		
		Double thresholdFee=Double.parseDouble(parseJson(cartResponse, "$.data..threshold_fee"));
		Double disFee=Double.parseDouble(parseJson(cartResponse, "$.data..distance_fee"));
		Double timeFee=Double.parseDouble(parseJson(cartResponse, "$.data..time_fee"));
		
		if (!cartType.equals("CAFE") && delCharge >0.0 || confee >0.0 && !TDrewardList.contains("FREE_DELIVERY")){
			 if (map.containsKey("positive_delivery_charges")){
				    setDelMap(cartResponse);
				    Reporter.log("New Sub rendering_details delivery Keys:-->"+delMap,true);
				    verifyDelFeeTitleText(cartResponse);
					String surgeFeeMes=parseJson(cartResponse, "$.data..surge_fee_promotion_text");
					validateRenderingData(cartResponse,map.get("positive_delivery_charges"),"value",(delCharge+confee),"positive_delivery_charges");
					validateRenderingData(cartResponse,map.get("positive_delivery_charges"),"intermediateText",surgeFeeMes,"surge_fee_promotion_text");
					//softAssertion.assertNotNull(surgeFeeIcon);
				} else{
					softAssertion.assertEquals(true, false,"remove_coupon_button is not present in rendering details");
				}
		}
		
		
	    if(thresholdFee>0){
			if(delMap.containsKey("flat_fee_v2")){
				validateSubRenderingData(cartResponse, map.get("positive_delivery_charges"), delMap.get("flat_fee_v2"),"value", thresholdFee,
						"flat_fee_v2");
				validateSubRenderingData(cartResponse, map.get("positive_delivery_charges"), delMap.get("flat_fee_v2"),"display_text", "Fixed Fee",
						"display_text of flat_fee_v2");
			}else{
				softAssertion.assertEquals(true, false,"flat_fee_v2 is not present in rendering details");
		}}
	    
	    if(disFee>0){
			if(delMap.containsKey("distance_fee_v2")){
				validateSubRenderingData(cartResponse, map.get("positive_delivery_charges"), delMap.get("distance_fee_v2"),"value", disFee,
						"distance_fee_v2");
				validateSubRenderingData(cartResponse, map.get("positive_delivery_charges"), delMap.get("distance_fee_v2"),"display_text", "Distance Fee",
						"display_text of distance_fee_v2");
			}else{
				softAssertion.assertEquals(true, false,"distance_fee_v2 is not present in rendering details");
		}}
	    
	    if(timeFee>0){
			if(delMap.containsKey("time_fee_v2")){
				validateSubRenderingData(cartResponse, map.get("positive_delivery_charges"), delMap.get("time_fee_v2"),"value", timeFee,
						"time_fee_v2");
				validateSubRenderingData(cartResponse, map.get("positive_delivery_charges"), delMap.get("time_fee_v2"),"display_text", "Late Night Surcharge",
						"display_text of time_fee_v2");
			}else{
				softAssertion.assertEquals(true, false,"time_fee_v2 is not present in rendering details");
		}}
	    
	    if(confee>0){
			if(delMap.containsKey("surge_charges_v2")){
				validateSubRenderingData(cartResponse, map.get("positive_delivery_charges"), delMap.get("surge_charges_v2"),"value", confee,
						"surge_charges_v2");
				validateSubRenderingData(cartResponse, map.get("positive_delivery_charges"), delMap.get("surge_charges_v2"),"display_text", "Surge Fee",
						"display_text of surge_charges_v2");
			}else{
				softAssertion.assertEquals(true, false,"surge_charges_v2 is not present in rendering details");
		}}
		
	
}
		
	public String getData(String cartResponse,int n,String key){
		String data=JsonPath.read(cartResponse, "$.data..rendering_details["+n+"]."+key+"")
                .toString().replace("[","").replace("]","").replace("\"","");
		return data;
	}
	
	public String getSubData(String cartResponse,int index,int subIndex,String key){
		String data=JsonPath.read(cartResponse, "$.data..rendering_details["+index+"].sub_details["+subIndex+"].."+key+"")
                .toString().replace("[","").replace("]","").replace("\"","");
		return data;
	}
	
	public String parseJson(String cartResponse,String key){
		return JsonPath.read(cartResponse, key).toString().replace("[","").replace("]","").replace("\"","");
	}
		
	public void validateRenderingData(String cartResponse,int index,String search,Double actualValue,String assertMes){
		Double value=Double.parseDouble(getData(cartResponse,index,search));
		softAssertion.assertEquals(value, actualValue, "Value mismatched for "+assertMes+" in rendering details");
	}
	
	public void validateRenderingData(String cartResponse,int index,String search,String exValue,String assertMes){
		String value=getData(cartResponse,index,search);
		softAssertion.assertEquals(value,exValue, "Value mismatched for "+assertMes+" in rendering details");
	}
	
	public void validateSubRenderingData(String cartResponse,int index,int subIndex,String search,String exValue,String assertMes){
		String value=getSubData(cartResponse,index,subIndex,search);
		softAssertion.assertEquals(value,exValue, "Value mismatched for "+assertMes+" in rendering details");
	}
	
	public void validateSubRenderingData(String cartResponse,int index,int subIndex,String search,Double exValue,String assertMes){
		Double value=Double.parseDouble(getSubData(cartResponse,index,subIndex,search));
		softAssertion.assertEquals(value,exValue, "Value mismatched for "+assertMes+" in rendering details");
	}

   public void validateApiResStatusData(String response) {
	    String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
		Assert.assertEquals(getStatusCode,PricingConstants.VALID_STATUS_CODE,"HTTPS response is not 0");
		String isSuccessful = JsonPath.read(response, "$.successful").toString().replace("[", "").replace("]", "");
		Assert.assertEquals(isSuccessful,CheckoutConstants.IS_SUCCESSFUL_FLAG,"Successful flag is false");
  }
     
   public void setDelMap(String cartResponse){
	   String[] subDetailsDelCharge=parseJson(cartResponse, "$.data..rendering_details["+map.get("positive_delivery_charges")+"].sub_details..key")
               .split(",");
        for(int i=0;i<subDetailsDelCharge.length;i++){
           delMap.put(subDetailsDelCharge[i], i); 
         }
	   
   }
   
   public void setResMap(String cartResponse){
	   String[] subDetailsResCharge=parseJson(cartResponse, "$.data..rendering_details["+map.get("restaurant_total")+"].sub_details..key")
               .split(",");
      for(int i=0;i<subDetailsResCharge.length;i++){
         resMap.put(subDetailsResCharge[i], i); 
       }
	   
   }
   
 public void verifyDelFeeTitleText(String cartResponse){
   String actualTittle="Passed on to delivery fleet for their time and effort";
   validateSubRenderingData(cartResponse,
					map.get("positive_delivery_charges"),delMap.get("delivery_fee_title_v2"),"display_text",actualTittle,"delivery_fee_title_v2");
 }
}
