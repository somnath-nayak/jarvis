
package com.swiggy.api.sf.checkout.tests.sanityTests;


import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.rng.helper.RngHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import com.swiggy.api.sf.checkout.dp.RegularCartDP;
import org.testng.asserts.SoftAssert;

public class RegularCartTest extends RegularCartDP {

    CheckoutHelper helper= new CheckoutHelper();
    RngHelper rngHelper = new RngHelper();
    SoftAssert softAssertion= new SoftAssert();
    String tid;
    String token;

    String mobile= System.getenv("mobile");
    String password= System.getenv("password");

    @BeforeTest
    public void loginHere(){
        if (mobile == null || mobile.isEmpty()){
            mobile=CheckoutConstants.mobile;}

        if (password == null ||password.isEmpty()){
            password=CheckoutConstants.password;}

        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        tid=hashMap.get("Tid");
        token=hashMap.get("Token");
    }

   @Test(dataProvider="cart", groups = {"Regression","Smoke","Chandan"}, description = "Create a Regular Cart")
    public void CreateRegularCart(String userAgent,String versionCode,Cart cartPayload,int index,
                                                String couponCode,String tdType) throws IOException{
       String cartResponse;
        if(couponCode!=null && !couponCode.isEmpty()){
            cartPayload.setCouponCode(couponCode);
        }
        if(tdType.equalsIgnoreCase("noTd")){
            cartResponse=createCart(userAgent,versionCode,cartPayload);
        } else {
            createTD(tdType,cartPayload.getRestaurantId());
            cartResponse=createCart(userAgent,versionCode,cartPayload);
            validateTDOnCartResponse(cartResponse);
            disableTD(cartPayload.getRestaurantId());
        }
        helper.validateDataFromResponseNew(cartResponse,cartPayload.getRestaurantId(),
                            cartPayload.getCartItems().get(index-1).getMenuItemId(),index);
    }


    @Test(dataProvider="cart", groups = {"Regression","Smoke","Chandan"}, description = "Create & Get Regular Cart")
    public void getRegularCart(String userAgent,String versionCode,Cart cartPayload,int index,
                                                String couponCode,String tdType) throws IOException{
        if(couponCode!=null && !couponCode.isEmpty()){
            cartPayload.setCouponCode(couponCode);
        }
        if(tdType.equalsIgnoreCase("noTd")){
            createCart(userAgent,versionCode,cartPayload);
        } else {
            createTD(tdType,cartPayload.getRestaurantId());
            createCart(userAgent,versionCode,cartPayload);
            disableTD(cartPayload.getRestaurantId());
        }
        String getCartRes=getCart(userAgent,versionCode);
        helper.validateDataFromResponseNew(getCartRes,cartPayload.getRestaurantId(),
                                    cartPayload.getCartItems().get(index-1).getMenuItemId(),index);
    }

    @Test(dataProvider="cart", groups = {"Regression","Smoke","Chandan"}, description = "Create & del Regular cart")
    public void deleteRegularCart(String userAgent,String versionCode,Cart cartPayload,int index,
                                        String couponCode,String tdType) throws IOException {
        if(couponCode!=null && !couponCode.isEmpty()){
            cartPayload.setCouponCode(couponCode);
        }
        if(tdType.equalsIgnoreCase("noTd")){
            createCart(userAgent,versionCode,cartPayload);
        } else {
            createTD(tdType,cartPayload.getRestaurantId());
            createCart(userAgent,versionCode,cartPayload);
            disableTD(cartPayload.getRestaurantId());
        }
        deleteCart(userAgent,versionCode);
        String getCartAfterDelete=getCart(userAgent,versionCode);
        String dataNode= JsonPath.read(getCartAfterDelete, "$.data");
        Assert.assertNull(dataNode);
    }

    @Test(dataProvider="deleteCouponOnCart", groups = {"Regression","Sanity","Chandan"}, description = "deleteCouponOnCart")
    public void deleteCouponOnCart(String userAgent,String versionCode,Cart cartPayload,int index,
                                                    String couponCode,String tdType) throws IOException {
        String cartResponse;
        if(couponCode!=null && !couponCode.isEmpty()){
            cartPayload.setCouponCode(couponCode);
            if(tdType.equalsIgnoreCase("noTd")){
                cartResponse=createCart(userAgent,versionCode,cartPayload);
            } else {
                createTD(tdType,cartPayload.getRestaurantId());
                cartResponse=createCart(userAgent,versionCode,cartPayload);
                disableTD(cartPayload.getRestaurantId());
            }
        String deleteCouponCartRes=deleteCouponOnCart(userAgent,versionCode,couponCode);
        validateDeleteCouponCartResponse(cartResponse,deleteCouponCartRes);
        }
    }


    @Test(dataProvider="cart", groups = {"Regression","Smoke","Chandan"}, description = "Create a Regular Cart")
    public void CreateMinimalCart(String userAgent,String versionCode,Cart cartPayload,int index,
                                  String couponCode,String tdType) throws IOException{
        String cartResponse;
        if(couponCode!=null && !couponCode.isEmpty()){
            cartPayload.setCouponCode(couponCode);
        }
        if(tdType.equalsIgnoreCase("noTd")){
            cartResponse=updateMinimalCart(userAgent,versionCode,cartPayload);
        } else {
            createTD(tdType,cartPayload.getRestaurantId());
            cartResponse=updateMinimalCart(userAgent,versionCode,cartPayload);
            validateTDOnCartResponse(cartResponse);
            disableTD(cartPayload.getRestaurantId());
        }
    }

    @Test(dataProvider="cart", groups = {"Regression","Smoke","Chandan"}, description = "Create & Get Regular Cart")
    public void getMinimalCart(String userAgent,String versionCode,Cart cartPayload,int index,
                               String couponCode,String tdType) throws IOException{
        if(couponCode!=null && !couponCode.isEmpty()){
            cartPayload.setCouponCode(couponCode);
        }
        if(tdType.equalsIgnoreCase("noTd")){
            updateMinimalCart(userAgent,versionCode,cartPayload);
        } else {
            createTD(tdType,cartPayload.getRestaurantId());
            updateMinimalCart(userAgent,versionCode,cartPayload);
            disableTD(cartPayload.getRestaurantId());
        }
        String getCartRes=getCart(userAgent,versionCode);
        helper.validateDataFromResponseNew(getCartRes,cartPayload.getRestaurantId(),
                cartPayload.getCartItems().get(index-1).getMenuItemId(),index);
    }

    @Test(dataProvider="cart", groups = {"Regression","Smoke","Chandan"}, description = "Create & del Regular cart")
    public void deleteMinimalCart(String userAgent,String versionCode,Cart cartPayload,int index,
                                  String couponCode,String tdType) throws IOException {
        if(couponCode!=null && !couponCode.isEmpty()){
            cartPayload.setCouponCode(couponCode);
        }
        if(tdType.equalsIgnoreCase("noTd")){
            updateMinimalCart(userAgent,versionCode,cartPayload);
        } else {
            createTD(tdType,cartPayload.getRestaurantId());
            updateMinimalCart(userAgent,versionCode,cartPayload);
            disableTD(cartPayload.getRestaurantId());
        }
        deleteCart(userAgent,versionCode);
        String getCartAfterDelete=getCart(userAgent,versionCode);
        String dataNode= JsonPath.read(getCartAfterDelete, "$.data");
        Assert.assertNull(dataNode);
    }

    @Test(dataProvider="deleteCouponOnCart", groups = {"Regression","Sanity","Chandan"}, description = "deleteCouponOnCart")
    public void deleteCouponOnMinimalCart(String userAgent,String versionCode,Cart cartPayload,int index,
                                   String couponCode,String tdType) throws IOException {
        String cartResponse;
        if(couponCode!=null && !couponCode.isEmpty()){
            cartPayload.setCouponCode(couponCode);
            if(tdType.equalsIgnoreCase("noTd")){
                cartResponse=updateMinimalCart(userAgent,versionCode,cartPayload);
            } else {
                createTD(tdType,cartPayload.getRestaurantId());
                cartResponse=updateMinimalCart(userAgent,versionCode,cartPayload);
                disableTD(cartPayload.getRestaurantId());
            }
            String deleteCouponCartRes=deleteCouponOnCart(userAgent,versionCode,couponCode);
            helper.validateDataFromResponseNew(deleteCouponCartRes,cartPayload.getRestaurantId(),
                    cartPayload.getCartItems().get(index-1).getMenuItemId(),index);
            validateDeleteCouponCartResponse(cartResponse,deleteCouponCartRes);

        }
    }

    @Test(dataProvider="cartCheckTotal", groups = {"Regression","Sanity","Chandan"}, description = "cartCheckTotal")
    public void cartCheckTotal(String userAgent,String versionCode,Cart cartPayload,int index,
                               String couponCode,String tdType) {
        String response=createCartCheckTotals(userAgent,versionCode,cartPayload);
        helper.validateDataFromResponseNew(response,cartPayload.getRestaurantId(),
                cartPayload.getCartItems().get(index-1).getMenuItemId(),index);
    }

    @Test(dataProvider="cartCheckTotal", groups = {"Regression","Sanity","Chandan"}, description = "cartCheckTotal")
    public void deleteCartCheckTotal(String userAgent,String versionCode,Cart cartPayload,int index,
                               String couponCode,String tdType) {
        String response=createCartCheckTotals(userAgent,versionCode,cartPayload);
        deleteCart(userAgent,versionCode);
        helper.validateDataFromResponseNew(response,cartPayload.getRestaurantId(),
                cartPayload.getCartItems().get(index-1).getMenuItemId(),index);
    }

    public String createCart(String userAgent,String versionCode,Cart cartPayload){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String cartResponse=helper.createCart(header,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cartResponse);
        return cartResponse;
    }


    public String getCart(String userAgent,String versionCode){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String getCartRes=helper.getCart(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getCartRes);
        return getCartRes;
    }

    public String deleteCart(String userAgent,String versionCode){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String delCartRes=helper.deleteCart(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(delCartRes);
        return delCartRes;
    }

    public String deleteCouponOnCart(String userAgent,String versionCode,String couponCode){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String delCouponCartRes=helper.deleteCouponOnCart(header,couponCode).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(delCouponCartRes);
        return delCouponCartRes;
    }

    public String createCartCheckTotals(String userAgent,String versionCode,Cart cartPayload){
        HashMap<String, String> header=helper.setCompleteHeader("", "", userAgent, versionCode);
        String cartResponse=helper.createCartCheckTotals(header,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cartResponse);
        return cartResponse;
    }

    public String updateMinimalCart(String userAgent,String versionCode,Cart cartPayload){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String minimalCartResponse=helper.updateMinimalCart(header,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(minimalCartResponse);
        return minimalCartResponse;
    }

    public void createTD(String tdType,String restID) throws IOException{
        if(tdType.equalsIgnoreCase("flatTD")){
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("0",restID,"1");
        } else if(tdType.equalsIgnoreCase("FreeDelTD")) {
            rngHelper.createFreeDeliveryTD(restID, false, false, false, false);
        }
    }

    public void disableTD(String restID){
        rngHelper.disabledActiveTD(restID);
    }

    public void validateTDOnCartResponse(String response){
        String tdCampaign= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders")
                .toString().replace("[", "").replace("]", "");
        String trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount")
                .toString().replace("[", "").replace("]", "");
        Double tdTotal = JsonPath.read(response, "$.data.trade_discount_total");
        softAssertion.assertNotNull(tdCampaign);
        softAssertion.assertNotEquals(trade_SubTotal,0);
        }

    public void validateDeleteCouponCartResponse(String cartRes,String deleteCartRes){

        Double cartTotal = Double.parseDouble(JsonPath.read(cartRes, "$.data.cart_subtotal_without_packing")
                .toString().replace("[", "").replace("]", ""));
        Double couponDiscount = Double.parseDouble(JsonPath.read(cartRes, "$.data.coupon_discount_total")
                .toString().replace("[", "").replace("]", ""));

        Double cartTotalNoCoupon = Double.parseDouble(JsonPath.read(deleteCartRes, "$.data.cart_subtotal_without_packing")
                .toString().replace("[", "").replace("]", ""));

        Double expectedTotal= cartTotal-couponDiscount;
        softAssertion.assertNotEquals(expectedTotal,cartTotalNoCoupon,"Mismatch in cart total after removing coupon");
    }

  // TODO: 11/10/18
//Create a normal cart for a closed restaurant
//create cart with item outofstock")
//create cart with negativeQuantity

}