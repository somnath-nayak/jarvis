package com.swiggy.api.sf.rng.constants;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.constants
 **/
public interface MultiTDConstants {


//    String[] mobile = {"9008738199", "7406734416", "9886373389"};
//    String[] password = {"swiggy","rkonowhere","passw0rd"};
    String[] mobile = {"6362191626", "9035592077", "9886373389","7737049198","7760677755","8660475180"};
    String[] password = {"swiggy","swiggy","swiggy","swiggy","swiggy","swiggy"};
    int statusZero = 0;
    int statusOne = 1;
    int status400 = 400;
    String androidUserAgent = "Swiggy-Android";
    String iosUserAgent = "Swiggy-iOS";
    String webUserAgent = "web";
    String statusMessageOK = "OK";
    String statusMessageNull = "null";
    String statusMessageSuccess = "success";
    String lat = "28.459";
    String lng = "77.026";
    String NUC_restID = "3456";
    String NUC_userID = "9876";
    String itemID = "56789";
}
