package com.swiggy.api.sf.rng.dp.coupondp;

import com.swiggy.api.sf.rng.helper.CouponHelper;
import com.swiggy.api.sf.rng.pojo.GlobalOffsetCreateMultiApplyCouponPOJO;
import org.testng.annotations.DataProvider;

import java.util.HashMap;

public class CouponGlobalOffsetDP {


    @DataProvider(name = "createMultiApplyCouponForSingleTransaction")
    public static Object[][] createMultiApplyCouponForSingleTransaction() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        GlobalOffsetCreateMultiApplyCouponPOJO globalOffsetCreateMultiApplyCouponPOJO = new CouponHelper().createMultiApplyCouponGlobalOffsetSingleTransaction(30,false,-1,1,20);
        data.put("GlobalOffsetCreateMultiApplyCouponPOJO",globalOffsetCreateMultiApplyCouponPOJO);
        return new Object[][]{{data}};
    }


    @DataProvider(name = "createMultiApplyCouponForMultipleTransaction")
    public static Object[][] createMultiApplyCouponForMultipleTransaction() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        GlobalOffsetCreateMultiApplyCouponPOJO globalOffsetCreateMultiApplyCouponPOJO = new CouponHelper().createMultiApplyCouponGlobalOffsetMultiple60thTransaction(30,false,-1,2,20);
        data.put("GlobalOffsetCreateMultiApplyCouponPOJO",globalOffsetCreateMultiApplyCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createMultiApplyCouponForMultipleTransactionGlobalOffsetGreaterThanValidTill")
    public static Object[][] createMultiApplyCouponForMultipleTransactionGlobalOffsetGreaterThanValidTill() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        GlobalOffsetCreateMultiApplyCouponPOJO globalOffsetCreateMultiApplyCouponPOJO = new CouponHelper().createMultiApplyCouponGlobalOffsetMultiple60thTransaction(150,false,-1,2,20);
        data.put("GlobalOffsetCreateMultiApplyCouponPOJO",globalOffsetCreateMultiApplyCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createMultiApplyCouponForMultipleTransactionWith60GlobalOffset")
    public static Object[][] createMultiApplyCouponForMultipleTransactionWith60GlobalOffset() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        GlobalOffsetCreateMultiApplyCouponPOJO globalOffsetCreateMultiApplyCouponPOJO = new CouponHelper().createMultiApplyCouponGlobalOffsetMultiple60thTransaction(60,false,-1,2,20);
        data.put("GlobalOffsetCreateMultiApplyCouponPOJO",globalOffsetCreateMultiApplyCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createMultiApplyCouponForMultipleTransactionWithinGlobalOffset")
    public static Object[][] createMultiApplyCouponForMultipleTransactionWithinGlobalOffset() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        GlobalOffsetCreateMultiApplyCouponPOJO globalOffsetCreateMultiApplyCouponPOJO = new CouponHelper().createMultiApplyCouponGlobalOffsetMultipleTransactionWithinGlobalOffset(60,false,-1,3,20);
        data.put("GlobalOffsetCreateMultiApplyCouponPOJO",globalOffsetCreateMultiApplyCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createMultiApplyCouponForMultipleTransactionOffSetvalidityWithMultipleTransactionGlobalOffset")
    public static Object[][] createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffset() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        GlobalOffsetCreateMultiApplyCouponPOJO globalOffsetCreateMultiApplyCouponPOJO = new CouponHelper().createMultiApplyCouponGlobalOffsetMultipleTransactionWithinGlobalOffset(60,false,-1,3,20);
        data.put("GlobalOffsetCreateMultiApplyCouponPOJO",globalOffsetCreateMultiApplyCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffsetDisable")
    public static Object[][] createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffsetDisable() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        GlobalOffsetCreateMultiApplyCouponPOJO globalOffsetCreateMultiApplyCouponPOJO = new CouponHelper().createMultiApplyCouponGlobalOffsetMultipleTransactionWithinGlobalOffset(-1,false,-1,3,20);
        data.put("GlobalOffsetCreateMultiApplyCouponPOJO",globalOffsetCreateMultiApplyCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffSetWithZeroOffSet")
    public static Object[][] createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffSetWithZeroOffSet() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        GlobalOffsetCreateMultiApplyCouponPOJO globalOffsetCreateMultiApplyCouponPOJO = new CouponHelper().createMultiApplyCouponGlobalOffsetMultipleTransactionWithinGlobalOffset(0,false,-1,3,20);
        data.put("GlobalOffsetCreateMultiApplyCouponPOJO",globalOffsetCreateMultiApplyCouponPOJO);
        return new Object[][]{{data}};
    }
}
