package com.swiggy.api.sf.snd.pojo.UpdateCarousel;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import java.util.List;
import com.swiggy.api.sf.rng.helper.RngHelper;
import java.util.ArrayList;

public class UpdateCarouselPOJO
{
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public UpdateCarouselPOJO withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("startDateTime")
    private String startDateTime;

    @JsonProperty("endDateTime")
    private String endDateTime;

    @JsonProperty("priority")
    private String priority;

    @JsonProperty("channel")
    private String channel;

    @JsonProperty("enabled")
    private Boolean enabled;

    @JsonProperty("updatedBy")
    private String updatedBy;

    public UpdateCarouselPOJO withUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    @JsonProperty("timeSlots")
    private List<TimeSlots> timeSlots;

    @JsonProperty("startDateTime")
    public String getStartDateTime() {
        return startDateTime;
    }

    @JsonProperty("startDateTime")
    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public UpdateCarouselPOJO withStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
        return this;
    }

    @JsonProperty("endDateTime")
    public String getEndDateTime() {
        return endDateTime;
    }

    @JsonProperty("endDateTime")
    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public UpdateCarouselPOJO withEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
        return this;
    }

    @JsonProperty("priority")
    public String getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(String priority) {
        this.priority = priority;
    }

    public UpdateCarouselPOJO withPriority(String priority) {
        this.priority = priority;
        return this;
    }

    @JsonProperty("channel")
    public String getChannel() {
        return channel;
    }

    @JsonProperty("channel")
    public void setChannel(String channel) {
        this.channel = channel;
    }

    public UpdateCarouselPOJO withChannel(String channel) {
        this.channel = channel;
        return this;
    }


    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public UpdateCarouselPOJO withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("timeSlots")
    public List<TimeSlots> getTimeSlots() {
        return timeSlots;
    }

    @JsonProperty("timeSlots")
    public void setTimeSlots(List<TimeSlots> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public UpdateCarouselPOJO withTimeSlots(List<TimeSlots> timeSlots) {
        this.timeSlots = timeSlots;
        return this;
    }

    public UpdateCarouselPOJO setDefault(){
        return this.withPriority("22")
                .withChannel("All")
                .withStartDateTime(RngHelper.getCurrentDate("yyyy-MM-dd HH:mm:ss"))
                .withEndDateTime(RngHelper.getFutureDate("yyyy-MM-dd HH:mm:ss"))
                .withEnabled(true)
                .withTimeSlots(getAllDaysTimeSlot());
    }

    @JsonIgnore
    public List<TimeSlots> getAllDaysTimeSlot(){
        timeSlots = new ArrayList<>();
        TimeSlots timeSlot ;
        String[] days = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
        for(String day: days) {
            timeSlot = new TimeSlots();
            timeSlots.add(timeSlot.withCloseTime(2359).withStartTime(0).withDay(day));
        }
        return timeSlots;
    }
}