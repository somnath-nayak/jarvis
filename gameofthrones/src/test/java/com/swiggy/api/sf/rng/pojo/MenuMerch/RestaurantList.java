package com.swiggy.api.sf.rng.pojo.MenuMerch;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class RestaurantList {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("categories")
    private List<Categories> categories;

    @JsonProperty("id")
    public String getId ()
    {
        return id;
    }
    @JsonProperty("id")
    public void setId (String id)
    {
        this.id = id;
    }

    public RestaurantList withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName ()
    {
        return name;
    }
    @JsonProperty("name")
    public void setName (String name)
    {
        this.name = name;
    }

    public RestaurantList withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("categories")
    public List<Categories> getCategories ()
    {
        return categories;
    }
    @JsonProperty("categories")
    public void setCategories (List<Categories> categories)
    {
        this.categories = categories;
    }

    public RestaurantList withCategories(List<Categories> categories) {
        this.categories = categories;
        return this;
    }

    public RestaurantList categories(String restId){
        return this.withId(restId)
                .withName("Rest Name")
                .withCategories(categories);
    }

    public RestaurantList setDefault(){
        return this.withId("12345")
                .withName("Restaurant")
                .withCategories(getCategories());
    }

    public RestaurantList(String restId,String name,List<Categories> categories){
        this.id=restId;
        this.name= "Restaurant";
        this.categories=categories;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("categories",categories).toString();
    }
}
