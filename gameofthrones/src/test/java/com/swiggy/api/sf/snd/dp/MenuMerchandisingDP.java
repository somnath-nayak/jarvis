package com.swiggy.api.sf.snd.dp;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.rng.pojo.MenuMerch.*;
import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import com.swiggy.api.sf.snd.constants.MenuMerchandisingConstants;
import com.swiggy.api.sf.snd.helper.EDVOHelper;
import com.swiggy.api.sf.snd.helper.MenuMerchandisingHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.PropertiesHandler;
import framework.gameofthrones.JonSnow.Processor;
import net.minidev.json.JSONArray;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

import static com.swiggy.api.sf.snd.constants.MenuMerchandisingConstants.high_touch;
import static com.swiggy.api.sf.snd.constants.MenuMerchandisingConstants.normal;
import static java.lang.Integer.valueOf;

public class MenuMerchandisingDP  {

    EDVOHelper edvoHelper = new EDVOHelper();
    com.swiggy.api.sf.rng.helper.EDVOHelper edvoHelperRng = new com.swiggy.api.sf.rng.helper.EDVOHelper();

    MenuMerchandisingHelper menuMerchandisingHelper = new MenuMerchandisingHelper();
    RuleDiscount ruleDiscount = new RuleDiscount();
    CMSHelper cmsHelper = new CMSHelper();

    String Env;
    static String restId;
    static String lat;
    static String lng;
    static String cityId;
    static String categoryId;
    static String subCategoryId;
    static String itemId;

    public static String getCityId() {
        return cityId;
    }

    public static void setCityId(String cityId) {
        MenuMerchandisingDP.cityId = cityId;
    }

    public static JSONArray getItemIdArray() {
        return itemIdArray;
    }

    public static void setItemIdArray(JSONArray itemIdArray) {
        MenuMerchandisingDP.itemIdArray = itemIdArray;
    }

    static JSONArray itemIdArray;

    SnDHelper sndHelper = new SnDHelper();

    public static void setRestId(String restId) {
        MenuMerchandisingDP.restId = restId;
    }

    public static String getLat() {
        return lat;
    }

    public static void setLat() {
        MenuMerchandisingDP.lat = MenuMerchandisingConstants.lat;
    }

    public static String getLng() {
        return lng;
    }

    public static void setLng() {
        MenuMerchandisingDP.lng = MenuMerchandisingConstants.lng;
    }

    public void getRestId() {
        setLat();
        setLng();

        //pick the rest ID
        Processor processor = sndHelper.getAggregatorDetails(lat, lng);
        restId = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.sortedRestaurants[0]..restaurantId").toString().replace("[\"", "").replace("\"]", "");
        setRestId(restId);

        cityId = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants.[?(@.id=='"+restId+"')].cityId");
        setCityId(cityId);

        Processor menuV4Response = edvoHelper.getMenuV4(restId, lat, lng);
        itemIdArray = menuV4Response.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.items[*].id");
        setItemIdArray(itemIdArray);
        itemId = itemIdArray.get(0).toString();

        subCategoryId = cmsHelper.getSubCategoryIdForItem(itemIdArray.get(0).toString());
        categoryId = cmsHelper.getCategoryIdForItem(itemIdArray.get(0).toString());

        //delete existing campaigns
        com.swiggy.api.sf.rng.helper.EDVOHelper.deleteUserByMobile(restId);

    }

    public PropertiesHandler properties = new PropertiesHandler();

    public MenuMerchandisingDP() {
        if (System.getenv("ENVIRONMENT") == null)
            Env = properties.propertiesMap.get("environment");
        else
            Env = System.getenv("ENVIRONMENT");
    }

    @DataProvider(name = "getRestId" )
    public Object[][] getRestIdData() {
        getRestId();
        return new Object[][] {
                {high_touch,restId}
        };
    }


    @DataProvider(name = "menuWithHighTouchValid" )
    public Object[][] getRestIdWithHighTouchEnabled() {
        getRestId();
        return new Object[][] {
                {high_touch,restId,lat,lng}
        };
    }

    @DataProvider(name = "menuWithLayoutTransition" )
    public Object[][] getRestIdmenuWithLayoutTransition() {
        getRestId();
        return new Object[][] {
                {high_touch,normal,restId,lat,lng}
        };
    }

    @DataProvider(name = "menuWithNormalLayout" )
    public Object[][] getRestIdmenuWithNormalLayout() {
        getRestId();
        return new Object[][] {
                {normal,restId,lat,lng}
        };
    }

    @DataProvider(name="createCarouselWithEnabled")
    public Object[][] createCarouselWithEnabled(){
        getRestId();
        return new Object[][]{
                { high_touch,restId, lat, lng, new CarouselPOJO()
                        .setDefault()
                        .setColor()
                        .withDescription("Test Description")
                        .withType("item")
                        .withMetatypes("TopCarousel")
                        .withChannel("All")
                        .withRedirectLink(itemId)
                        .withRestaurantId(valueOf(restId))
                        .withEnabled(true)}
        };
    }

    @DataProvider(name="createCarouselWithDisabled")
    public Object[][] createCarouselWithDisabled(){
        getRestId();
        return new Object[][]{
                { high_touch,restId, lat, lng, new CarouselPOJO()
                        .setDefault()
                        .setColor()
                        .withDescription("Test Description")
                        .withType("item")
                        .withMetatypes("TopCarousel")
                        .withChannel("All")
                        .withRedirectLink(itemIdArray.get(0).toString())
                        .withRestaurantId(valueOf(restId))
                        .withEnabled(false)}
        };
    }

    @DataProvider(name="createCarouselOutOfSlot")
    public Object[][] createCarouselOutOfSlot(){
        getRestId();
        return new Object[][]{
                { high_touch ,restId, lat, lng, new CarouselPOJO()
                        .setDefault()
                        .setColor()
                        .withDescription("Test Description")
                        .withType("item")
                        .withMetatypes("TopCarousel")
                        .withChannel("All")
                        .withRedirectLink(itemIdArray.get(0).toString())
                        .withRestaurantId(valueOf(restId))
                        .withEnabled(true)
                }
        };
    }

    @DataProvider(name="createCarouselOutOfStock")
    public Object[][] createCarouselOutOfStock(){
        getRestId();
        return new Object[][]{
                { high_touch,restId, lat, lng,itemId, new CarouselPOJO()
                        .setDefault()
                        .setColor()
                        .withDescription("Test Description")
                        .withType("item")
                        .withMetatypes("TopCarousel")
                        .withChannel("All")
                        .withRedirectLink(itemId)
                        .withRestaurantId(valueOf(restId))
                        .withEnabled(true)
                }
        };
    }

    @DataProvider(name="serviceableWithBanner")
    public Object[][] serviceableWithBanner(){
        getRestId();
        return new Object[][]{
                { high_touch,restId, lat, lng,cityId, new CarouselPOJO()
                        .setDefault()
                        .setColor()
                        .withDescription("Test Description")
                        .withType("item")
                        .withMetatypes("TopCarousel")
                        .withChannel("All")
                        .withRedirectLink(itemId)
                        .withRestaurantId(valueOf(restId))
                        .withEnabled(true)
                }
        };
    }

    public List<RestaurantList> buildRestaurantList(String restId, String categoryId, String subCategoryId, String itemId){

        List<RestaurantList> restaurantList = new ArrayList<>();
        List<Menu> itemList = new ArrayList<>();
        itemList.add(new Menu(itemId,"test Item"));
        List<SubCategories> subCategoriesList = new ArrayList<>();
        subCategoriesList.add(new SubCategories(subCategoryId,"Test subcategory",itemList));
        List<Categories> categories = new ArrayList<>();
        categories.add(new Categories(categoryId,"Test Category",subCategoriesList));
        restaurantList.add(new RestaurantList(restId,"Test Rest",categories));

        return restaurantList;
    }
    public List<Slots> buildSlotList(){
        CreateFlatTDItemLevelPOJO createFlatTDItemLevelPOJO = new CreateFlatTDItemLevelPOJO();
        List<Slots> allDaysTimeSlot = createFlatTDItemLevelPOJO.getAllDaysTimeSlot();
        return allDaysTimeSlot;
    }
    @DataProvider(name="createCarouselItemTDEnabled")
    public Object[][] createCarouselItemTDEnabled(){
        getRestId();
        menuMerchandisingHelper.buildStartTime();
        menuMerchandisingHelper.buildEndTime();
        menuMerchandisingHelper.getCurrentDay();
        List<RestaurantList> restaurantList = buildRestaurantList(restId,categoryId,categoryId,itemId);
        ruleDiscount = this.ruleDiscount.setDefault();
        List<Slots> slots = buildSlotList();

        return new Object[][]{
                { high_touch,restId,lat,lng, new CarouselPOJO()
                        .setDefault()
                        .setColor()
                        .withDescription("Test Description")
                        .withType("item")
                        .withMetatypes("TopCarousel")
                        .withChannel("All")
                        .withRedirectLink(itemId)
                        .withRestaurantId(valueOf(restId))
                        .withEnabled(true),

                        new CreateFlatTDItemLevelPOJO()
                        .setDefault()
                        .withTimeSlotRestriction(false)
                        .withEnabled(true)
                        .withRuleDiscount(ruleDiscount)
                        .withRestaurantList(restaurantList)
                        .withSlots(slots)

                }
        };
    }

    @DataProvider(name="createCarouselItemTDOutsideSlot")
    public Object[][] createCarouselItemTDOutsideSlot(){
        getRestId();
        menuMerchandisingHelper.buildStartTime();
        menuMerchandisingHelper.buildEndTime();
        menuMerchandisingHelper.getCurrentDay();
        List<RestaurantList> restaurantList = buildRestaurantList(restId,categoryId,subCategoryId,itemId);
        ruleDiscount = this.ruleDiscount.setDefault();
        List<Slots> slots = buildSlotList();

        return new Object[][]{
                { high_touch,restId,lat, lng, new CarouselPOJO()
                        .setDefault()
                        .setColor()
                        .withDescription("Test Description")
                        .withType("item")
                        .withMetatypes("TopCarousel")
                        .withChannel("All")
                        .withRedirectLink(itemId)
                        .withRestaurantId(valueOf(restId))
                        .withEnabled(true),

                        new CreateFlatTDItemLevelPOJO()
                                .setDefault()
                                .withTimeSlotRestriction(false)
                                .withEnabled(true)
                                .withRuleDiscount(ruleDiscount)
                                .withRestaurantList(restaurantList)
                                .withSlots(slots)

                }
        };
    }

    @DataProvider(name = "readLayoutType")
    public Object[][] readLayoutType() {
        return new Object[][]{{"12.936515", "77.605567"}};}

    @DataProvider(name = "sectionConfig")
    public Object[][] sectionConfig() {
        String[] layout = new String[3];
        layout[0]= "high_touch";
        return new Object[][]{{"automation", "Top Items","1","6","2","true",layout}};}

    @DataProvider(name = "sectionConfigInvalid")
    public Object[][] sectionConfigInvalid() {
        String[] layout = new String[3];
        layout[0]= "high_touch";
        return new Object[][]{{"", "Top Items","1","6","2","true",layout},
                {"automation","","1","6","2","true",layout},
                {"automation", "Top Items","","6","2","true",layout},
                {"automation", "Top Items","1","","2","true",layout},
                {"automation", "Top Items","1","6","","true",layout},
                {"automation", "Top Items","1","6","2","",layout}};}

    @DataProvider(name = "setLayoutType")
    public Object[][] setLayoutType() {
        String[] layout = new String[3];
        layout[0]= "high_touch";
        return new Object[][]{{"12.936515", "77.605567",layout,"nevia"}};}

    @DataProvider(name = "sectionConfigLayoutType")
    public Object[][] sectionConfigLayoutType() {
        return new Object[][]{{"high_touch"}};}

    @DataProvider(name = "disableLayout")
    public Object[][] disableLayout() {
        return new Object[][]{{"high_touch","9999"}};}

    @DataProvider(name = "disableLayoutMapping")
    public Object[][] disableLayoutMapping() {
        return new Object[][]{{"12.936515", "77.605567", "high_touch","nevia","NULL","9999","db","automation","1","6","2","1"}};}

    @DataProvider(name = "layoutRefresh")
    public Object[][] layoutRefresh() {
        return new Object[][]{{"12.936515", "77.605567"}};}

    @DataProvider(name = "layoutRefresht")
    public Object[][] layoutRefresht() {
        return new Object[][]{{"12.936515", "77.605567", "high_touch","nevia","NULL"}};}


    @DataProvider(name = "sectionConfigLayoutTypeRedis")
    public Object[][] sectionConfigLayoutTypeRedis() {
        return new Object[][]{{"high_touch","99999","db","automation","1","6","2","1"}};}

}

