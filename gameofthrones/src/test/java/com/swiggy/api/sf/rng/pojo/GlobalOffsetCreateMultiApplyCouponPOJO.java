package com.swiggy.api.sf.rng.pojo;

import com.swiggy.api.sf.rng.helper.Utility;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class GlobalOffsetCreateMultiApplyCouponPOJO {

    @JsonProperty("code")
    private String code;
    @JsonProperty("name")
    private String name;
    @JsonProperty("bucket")
    private String bucket;
    @JsonProperty("description")
    private String description;
    @JsonProperty("totalPerUser")
    private int totalPerUser;
    @JsonProperty("totalAvailable")
    private long totalAvailable;
    @JsonProperty("customerRestriction")
    private boolean customerRestriction;
    @JsonProperty("cityRestriction")
    private boolean cityRestriction;
    @JsonProperty("areaRestriction")
    private boolean areaRestriction;
    @JsonProperty("cuisineRestriction")
    private boolean cuisineRestriction;
    @JsonProperty("restaurantRestriction")
    private boolean restaurantRestriction;
    @JsonProperty("categoryRestriction")
    private boolean categoryRestriction;
    @JsonProperty("itemRestriction")
    private boolean itemRestriction;
    @JsonProperty("slotRestriction")
    private boolean slotRestriction;
    @JsonProperty("applicableWithSwiggyMoney")
    private boolean applicableWithSwiggyMoney;
    @JsonProperty("applicableInReferralCouponPresence")
    private boolean applicableInReferralCouponPresence;
    @JsonProperty("autoApply")
    private boolean autoApply;
    @JsonProperty("validFrom")
    private String validFrom;
    @JsonProperty("validTill")
    private String validTill;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("createdOn")
    private String createdOn;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("updatedOn")
    private String updatedOn;
    @JsonProperty("image")
    private String image;
    @JsonProperty("supportedAndroidVersion")
    private Integer supportedAndroidVersion;
    @JsonProperty("supportedIosVersion")
    private Integer supportedIosVersion;
    @JsonProperty("expiryOffset")
    private long expiryOffset;
    @JsonProperty("freeGifts")
    private boolean freeGifts;
    @JsonProperty("vendor")
    private int vendor;
    @JsonProperty("isBankDiscount")
    private boolean isBankDiscount;
    @JsonProperty("refundSource")
    private String refundSource;
    @JsonProperty("pgMessage")
    private String pgMessage;
    @JsonProperty("slots")
    private String slots;
    @JsonProperty("isPrivate")
    private boolean isPrivate;
    @JsonProperty("metadata")
    private List<CouponMeta> couponMetaList;
    @JsonProperty("coupon_user_type_id")
    private int couponUserTypeId;
    @JsonProperty("globalOffsetDays")
    private int globalOffsetDays;


    public GlobalOffsetCreateMultiApplyCouponPOJO withName(String name) {
        this.name=name;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withSupportedIosVersion(int supportedIosVersion) {
        this.supportedIosVersion=supportedIosVersion;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withCouponUserTypeId(Integer couponUserTypeId) {
        this.couponUserTypeId=couponUserTypeId;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withBucket(String bucket) {
        this.bucket=bucket;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withDescription(String description) {
        this.description=description;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withTotalPerUser(int totalPerUser) {
        this.totalPerUser=totalPerUser;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withTotalAvailable(int totalAvailable) {
        this.totalAvailable=totalAvailable;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withCityRestriction(boolean cityRestriction) {
        this.cityRestriction=cityRestriction;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withCuisineRestriction(boolean cuisineRestriction) {
        this.cuisineRestriction=cuisineRestriction;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withRestaurantRestriction(boolean restaurantRestriction) {
        this.restaurantRestriction=restaurantRestriction;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withAreaRestreiction(boolean areaRestriction) {
        this.areaRestriction=areaRestriction;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withCategoryRestriction(boolean categoryRestriction) {
        this.categoryRestriction=categoryRestriction;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withItemRestriction(boolean itemRestriction) {
        this.itemRestriction=itemRestriction;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withSlotRestriction(boolean slotRestriction) {
        this.slotRestriction=slotRestriction;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withApplicableWithSwiggyMoney(boolean applicableWithSwiggyMoney) {
        this.applicableWithSwiggyMoney=applicableWithSwiggyMoney;
        return this;
    }


    public GlobalOffsetCreateMultiApplyCouponPOJO withApplicableInReferralCouponPresence(boolean applicableInReferralCouponPresence) {
        this.applicableInReferralCouponPresence=applicableInReferralCouponPresence;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withAutoApply(boolean autoApply) {
        this.autoApply=autoApply;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withUpdatedBy(String updatedBy) {
        this.updatedBy=updatedBy;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withCreatedOn(String createdOn) {
        this.createdOn=createdOn;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withCreatedBy(String createdBy) {
        this.createdBy=createdBy;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withUpdatedOn(String updatedOn) {
        this.updatedOn=updatedOn;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withImage(String image) {
        this.image=image;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withSupportedAndroidVersion(Integer supportedAndroidVersion) {
        this.supportedAndroidVersion=supportedAndroidVersion;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withExpiryOffset(Integer expiryOffset) {
        this.expiryOffset=expiryOffset;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withFreeGifts(boolean freeGifts) {
        this.freeGifts=freeGifts;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withVendor(Integer vendor) {
        this.vendor=vendor;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withIsBankDiscount(boolean isBankDiscount) {
        this.isBankDiscount=isBankDiscount;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withRefundSource(String refundSource) {
        this.refundSource=refundSource;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withPgMessage(String pgMessage) {
        this.pgMessage=pgMessage;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withMetaList(List<CouponMeta> list) {
        this.couponMetaList = list;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withcouponUserTypeId(int couponUserTypeId) {
        this.couponUserTypeId = couponUserTypeId;
        return this;
    }

    public static List<CouponMeta> getMetaList(int numberOfTransactions, int discountAmount, boolean freeShipping, int offset) {
        List<CouponMeta> couponMetaList = new ArrayList<>();
        for (int i = 0; i < numberOfTransactions; i++) {
            CouponMeta couponMeta = new CouponMeta().withDefaultValues().withDiscountAmount(discountAmount).withOffset(offset)
                    .withFreeShipping(freeShipping).withUsageCount(i + 1);
            couponMetaList.add(couponMeta);
        }
        return couponMetaList;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withCode(String code) {
        this.code = code;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withValidFrom(String validFrom) {
        this.validFrom = validFrom;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withValidTill(String validTill) {
        this.validTill = validTill;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withSlots(String slots) {
        this.slots = slots;
        return this;
    }

    public GlobalOffsetCreateMultiApplyCouponPOJO withGlobalOffsetDays(int globalOffsetDays) {
        this.globalOffsetDays = globalOffsetDays;
        return this;
    }
    public GlobalOffsetCreateMultiApplyCouponPOJO withCustomerRestriction(boolean customerRestriction) {
        this.customerRestriction = customerRestriction;
        return this;
    }


    public GlobalOffsetCreateMultiApplyCouponPOJO setDefaultData() {

        return this.withCode("MULTI" +  Utility.getRandomPostfix())
                .withName("MultiCoupon")
                .withBucket("Internal")
                .withDescription("TestMultiApplyGlobalOffset")
                .withTotalPerUser(2)
                .withTotalAvailable(100)
                .withCityRestriction(false)
                .withAreaRestreiction(false)
                .withCuisineRestriction(false)
                .withRestaurantRestriction(false)
                .withCategoryRestriction(false)
                .withItemRestriction(false)
                .withSlotRestriction(false)
                .withApplicableWithSwiggyMoney(false)
                .withApplicableInReferralCouponPresence(false)
                .withAutoApply(false)
                .withUpdatedBy("MutiApply-Automation")
                .withCreatedOn(Utility.getCurrentDate())
                .withCreatedBy("MutiApply-Automation")
                .withUpdatedOn(Utility.getCurrentDate())
                .withImage(null)
                .withSupportedAndroidVersion(245)
                .withSupportedIosVersion(245)
                .withExpiryOffset(-1)
                .withFreeGifts(false)
                .withVendor(0)
                .withIsBankDiscount(false)
                .withRefundSource(null)
                .withPgMessage(null)
                .withSlots(null)
                .withIsPrivate(false)
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withcouponUserTypeId(0)
                .withCustomerRestriction(false)
                .withMetaList(getMetaList( 2,  20,  false,30))
                .withCouponUserTypeId(0)
                .withGlobalOffsetDays(-1);



    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTotalPerUser() {
        return totalPerUser;
    }

    public void setTotalPerUser(int totalPerUser) {
        this.totalPerUser = totalPerUser;
    }

    public long getTotalAvailable() {
        return totalAvailable;
    }

    public void setTotalAvailable(long totalAvailable) {
        this.totalAvailable = totalAvailable;
    }

    public boolean isCustomerRestriction() {
        return customerRestriction;
    }

    public void setCustomerRestriction(boolean customerRestriction) {
        this.customerRestriction = customerRestriction;
    }

    public boolean isCityRestriction() {
        return cityRestriction;
    }

    public void setCityRestriction(boolean cityRestriction) {
        this.cityRestriction = cityRestriction;
    }

    public boolean isAreaRestriction() {
        return areaRestriction;
    }

    public void setAreaRestriction(boolean areaRestriction) {
        this.areaRestriction = areaRestriction;
    }

    public boolean isCuisineRestriction() {
        return cuisineRestriction;
    }

    public void setCuisineRestriction(boolean cuisineRestriction) {
        this.cuisineRestriction = cuisineRestriction;
    }

    public boolean isRestaurantRestriction() {
        return restaurantRestriction;
    }

    public void setRestaurantRestriction(boolean restaurantRestriction) {
        this.restaurantRestriction = restaurantRestriction;
    }

    public boolean isCategoryRestriction() {
        return categoryRestriction;
    }

    public void setCategoryRestriction(boolean categoryRestriction) {
        this.categoryRestriction = categoryRestriction;
    }

    public boolean isItemRestriction() {
        return itemRestriction;
    }

    public void setItemRestriction(boolean itemRestriction) {
        this.itemRestriction = itemRestriction;
    }

    public boolean isSlotRestriction() {
        return slotRestriction;
    }

    public void setSlotRestriction(boolean slotRestriction) {
        this.slotRestriction = slotRestriction;
    }

    public boolean isApplicableWithSwiggyMoney() {
        return applicableWithSwiggyMoney;
    }

    public void setApplicableWithSwiggyMoney(boolean applicableWithSwiggyMoney) {
        this.applicableWithSwiggyMoney = applicableWithSwiggyMoney;
    }

    public boolean isApplicableInReferralCouponPresence() {
        return applicableInReferralCouponPresence;
    }

    public void setApplicableInReferralCouponPresence(boolean applicableInReferralCouponPresence) {
        this.applicableInReferralCouponPresence = applicableInReferralCouponPresence;
    }

    public boolean isAutoApply() {
        return autoApply;
    }

    public void setAutoApply(boolean autoApply) {
        this.autoApply = autoApply;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getSupportedAndroidVersion() {
        return supportedAndroidVersion;
    }

    public void setSupportedAndroidVersion(Integer supportedAndroidVersion) {
        this.supportedAndroidVersion = supportedAndroidVersion;
    }

    public long getSupportedIosVersion() {
        return supportedIosVersion;
    }

    public void setSupportedIosVersion(Integer supportedIosVersion) {
        this.supportedIosVersion = supportedIosVersion;
    }

    public long getExpiryOffset() {
        return expiryOffset;
    }

    public void setExpiryOffset(long expiryOffset) {
        this.expiryOffset = expiryOffset;
    }

    public boolean isFreeGifts() {
        return freeGifts;
    }

    public void setFreeGifts(boolean freeGifts) {
        this.freeGifts = freeGifts;
    }

    public int getVendor() {
        return vendor;
    }

    public void setVendor(int vendor) {
        this.vendor = vendor;
    }

    public boolean isBankDiscount() {
        return isBankDiscount;
    }

    public void setBankDiscount(boolean bankDiscount) {
        isBankDiscount = bankDiscount;
    }

    public String getRefundSource() {
        return refundSource;
    }

    public void setRefundSource(String refundSource) {
        this.refundSource = refundSource;
    }

    public String getPgMessage() {
        return pgMessage;
    }

    public void setPgMessage(String pgMessage) {
        this.pgMessage = pgMessage;
    }

    public String getSlots() {
        return slots;
    }

    public void setSlots(String slots) {
        this.slots = slots;
    }


    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public List<CouponMeta> getCouponMetaList() {
        return couponMetaList;
    }

    public void setCouponMetaList(List<CouponMeta> couponMetaList) {
        this.couponMetaList = couponMetaList;
    }


}
