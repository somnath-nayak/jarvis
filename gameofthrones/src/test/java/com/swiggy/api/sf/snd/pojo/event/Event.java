package com.swiggy.api.sf.snd.pojo.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

public class Event {

    @JsonInclude(JsonInclude.Include.NON_NULL)

    @JsonProperty("event_type")
    private String eventType;
    @JsonProperty("data")
    private Data data;
    @JsonProperty("meta")
    private Object meta;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("op_type")
    private String opType;
    @JsonProperty("version")
    private String version;
    @JsonProperty("event_timestamp")
    private Integer eventTimestamp;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Object getMeta() {
        return meta;
    }

    public void setMeta(Object meta) {
        this.meta = meta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getEventTimestamp() {
        return eventTimestamp;
    }

    public void setEventTimestamp(Integer eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("event_type", eventType).append("data", data).append("meta", meta).append("id", id).append("op_type", opType).append("version", version).append("event_timestamp", eventTimestamp).toString();
    }

}