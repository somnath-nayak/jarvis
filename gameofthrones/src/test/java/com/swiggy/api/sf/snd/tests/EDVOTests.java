package com.swiggy.api.sf.snd.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.gson.JsonArray;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.EDVOCronHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Item;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.EDVODataProvider;
import com.swiggy.api.sf.snd.helper.EDVOHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import framework.gameofthrones.Tyrion.WireMockHelper;
import io.advantageous.boon.core.Str;
import io.advantageous.boon.core.Sys;
import net.minidev.json.JSONArray;
import org.hibernate.secure.spi.IntegrationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;

import static org.testng.Assert.*;

public class EDVOTests extends EDVODataProvider {

    EDVOHelper edvoHelper= new EDVOHelper();
    EDVOCronHelper mealHelper= new EDVOCronHelper();
    RedisHelper redisHelper = new RedisHelper();
    RngHelper rngHelper = new RngHelper();
    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();
    SnDHelper sndHelper = new SnDHelper();
    WireMockHelper wireMockHelper = new WireMockHelper();

    @BeforeClass()
    public void startWireMock(){
        wireMockHelper.startMockServer(6666);

    }


    @Test(dataProvider = "menuV4",groups = {"sanity","smoke","srishty","edvo","meal"},description = "Verify menu v4 response")
    public void getMenuV4(String Id,String lat, String lng) {
        Processor processor = edvoHelper.getMenuV4(Id,lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        assertEquals(statusCode, 0);
        assertEquals(statusMessage, "done successfully");
    }

    @Test(dataProvider = "menuV4",groups = {"sanity","smoke","srishty","edvo","meal"},description = "Verify the edvo widget changes in menu v4")
    public void getMenuV4CreateWidgetAndVerify(String Id,String lat, String lng) {

        List<Map<String, Object>> dominosRestIds = mealHelper.getDominosRestaurant(12);
        //mealUpdateHelper.updateMeals("0 * * * * ","");
        Processor processor = edvoHelper.getMenuV4(Id,lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        assertEquals(statusCode, 0);
        assertEquals(statusMessage, "done successfully");
        processor.ResponseValidator.DoesNodeExists("$..menu.widgets", resp);
    }


    @Test(dataProvider = "bulkMeal",groups = {"sanity","smoke","srishty","edvo","meal"},description = "Verify bulk meal request for valid and invalid meal Ids")
    public void getBulkMeal(String Id) {
        Processor processor = edvoHelper.getBulkMeals(Id);
        String resp = processor.ResponseValidator.GetBodyAsText();
        assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        processor.ResponseValidator.DoesNodeExists("$.mealResponses", resp);
    }

    @Test(dataProvider = "mealAvailable",groups = {"sanity","smoke","srishty","edvo","meal"},description = "Verify the get meal Available API")
    public void getMealAvailable(String itemIds,String restId) {
        Processor processor = edvoHelper.mealAvailable(itemIds,restId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        processor.ResponseValidator.DoesNodeExists("$.mealInfoList", resp);
        processor.ResponseValidator.DoesNodeExists("$.mealInfoList..id", resp);
        processor.ResponseValidator.DoesNodeExists("$.mealInfoList..name", resp);
        processor.ResponseValidator.DoesNodeExists("$.mealInfoList..text", resp);
        processor.ResponseValidator.DoesNodeExists("$.mealInfoList..subText", resp);
        processor.ResponseValidator.DoesNodeExists("$.mealInfoList..imageId", resp);

    }

    @Test(dataProvider = "menuV4DataValidation",groups = {"functional","srishty","edvo","meal"},description = "Validate widgets in menu V4")
    public void validateWidgetsInMenuV4(String Id,String lat, String lng) {
        Processor processor = edvoHelper.getMenuV4(Id, lat, lng);
        String resp = processor.ResponseValidator.GetBodyAsText();

        boolean b = processor.ResponseValidator.DoesNodeExists("$..menu.widgets", resp);
        Assert.assertEquals(b, true);
    }

    @Test(dataProvider = "menuV4MealsValidation",groups = {"functional","srishty","edvo","meal"}, description = "validate meal group entity in menu v4")
    public void validateMealsInMenuV4(String Id,String lat, String lng,long campaignId) {

        Processor processor = edvoHelper.getMenuV4(Id,lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();

        JSONArray widgetTypes = JsonPath.read(resp,"$..menu.widgets..entities..type");
        boolean isMealFound = widgetTypes.contains("meal");
        if (isMealFound == true) {

            JSONArray mealIdArray = JsonPath.read(resp, "$..menu.widgets..entities..[?(@.type==\"meal\")].id");
            String mealId = mealIdArray.get(0).toString();


            //Verifying EDVO widget in response
            boolean b = processor.ResponseValidator.DoesNodeExists("$..menu.widgets", resp);
            assertEquals(b, true);

            //Verifying the type of EDVO widget
            JSONArray widgetType = JsonPath.read(resp, "$..menu.widgets[0].type");
            assertEquals(widgetType.get(0), "carousel");

            //Verifying the mealds against DB
             JSONArray mealIdByAPI = JsonPath.read(resp, "$..menu.widgets..entities..[?(@.type=='meal')].id");
            Set<Object> mealIdByAPIset = new HashSet<>();
            for (int i = 0; i < mealIdByAPI.size(); i++) {
                mealIdByAPIset.add(mealIdByAPI.get(i));
            }
            List<Map<String, Object>> mealIdsForDominosRestaurant = mealHelper.getEDVOMealIds(Id);
            Set<Object> mealIdSet = new HashSet<>();
            for (int i = 0; i < mealIdsForDominosRestaurant.size(); i++) {
                mealIdSet.add(mealIdsForDominosRestaurant.get(i).get("id"));
            }
            boolean flagMealId = mealIdSet.containsAll(mealIdByAPIset);
            Assert.assertEquals(flagMealId, true);

            //Verifying the EDVO images against DB

            String imageIdDB = edvoHelper.getEDVOImage(mealId);

            JSONArray imageIds = JsonPath.read(resp, "$..menu.widgets[0]..entities[0]..imageId");
            String imageIdAPI = imageIds.get(0).toString();
            Assert.assertEquals(imageIdAPI, imageIdDB);

            //Verifying the EDVO text against DB
            String operation_meta = edvoHelper.getEDVOText(campaignId);
            String textDB = JsonPath.read(operation_meta, "$.mealHeader");
            JSONArray texts = JsonPath.read(resp, "$..menu.widgets[0]..entities[0]..text");
            String textAPI = texts.get(0).toString();
            Assert.assertEquals(textAPI, textDB);

            //Verifying the EDVO subtext against DB
            String subTextDB = JsonPath.read(operation_meta, "$.mealDescription");
            JSONArray subTexts = JsonPath.read(resp, "$..menu.widgets[0]..entities[0]..subText");
            String subTextAPI = subTexts.get(0).toString();
            Assert.assertEquals(subTextAPI, subTextDB);

            //Verifying the meal group type for a given mealId against DB
            JSONArray mealGroupType = JsonPath.read(resp, "$..menu.widgets[0]..entities[0]..type");
            String entityMealGroupType = mealGroupType.get(0).toString();
            assertEquals(entityMealGroupType, "meal");
        }
        else
            {
            Assert.fail(("meal is not found"));
            }

    }

    @Test(dataProvider = "menuV4DataValidationNonEDVO",groups = {"functional","srishty","edvo","meal"},description = "verify menu v4 response for non edvo restaurant")
    public void validateNonEDVORestInMenuV4(String Id,String lat, String lng) {
        Processor processor = edvoHelper.getMenuV4(Id,lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();

        JSONArray entityType = JsonPath.read(resp,"$..menu.widgets..entities..type");
        Set<Object> entityTypeSet = new HashSet<>();
        for (int i=0; i<entityType.size(); i++){
            entityTypeSet.add(entityType.get(i));
        }
        boolean flag = entityTypeSet.contains("meal");
        Assert.assertEquals(flag, false);

    }

    @Test(dataProvider = "EDVOImageUpdate",groups = {"functional","srishty","edvo","meal"},description = "Verify update edvo Image")
    public void validateUpdateEDVOMealImage(String restId, String lat, String lng, String updatedImageId) throws InterruptedException {
        Processor processor = edvoHelper.getMenuV4(restId,lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();
        JSONArray widgetTypes = JsonPath.read(resp,"$..menu.widgets..entities..type");
        boolean isMealFound = widgetTypes.contains("meal");
        if (isMealFound == true) {
            JSONArray mealIdArray = JsonPath.read(resp, "$..menu.widgets[?(@.type == 'carousel')]..entities[?(@.type == 'meal')]..id");
            String mealId = mealIdArray.get(0).toString();

            edvoHelper.setEDVOImage(updatedImageId, mealId);
            String edvoImage = edvoHelper.getEDVOImage(mealId);
            System.out.println(edvoImage);

            edvoHelper.clearEDVORedis(restId, mealId);

            Processor processorNew = edvoHelper.getMenuV4(restId, lat, lng);
            String respNew = processorNew.ResponseValidator.GetBodyAsText();
            JSONArray imageIds = JsonPath.read(respNew, "$..menu.widgets[0]..entities[0]..imageId");
            String imageIdAPI = imageIds.get(0).toString();
            Assert.assertEquals(imageIdAPI, updatedImageId);
        }
        else
        {
            Assert.fail("meal is not Found");
        }

    }

    @Test(dataProvider = "EDVOTextUpdate",groups = {"functional","srishty","edvo","meal"},description = "verify updating edvo text")
    public void validateUpdateEDVOMealText(String restId, String lat, String lng, String updatedText,long campaignId) throws InterruptedException, JSONException {

        Processor processor = edvoHelper.getMenuV4(restId,lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();
        JSONArray widgetTypes = JsonPath.read(resp,"$..menu.widgets..entities..type");
        boolean isMealFound = widgetTypes.contains("meal");

        if (isMealFound == true) {

            JSONArray mealIdArray = JsonPath.read(resp, "$..menu.widgets[?(@.type=='carousel')]..entities[?(@.type=='meal')]..id");
            String mealId = mealIdArray.get(0).toString();
            String metaDatabefore = rngHelper.getOperationMetaData(campaignId);

            JSONObject jsonObject;
            jsonObject = new JSONObject(metaDatabefore);

            String headerBefore = JsonPath.read(metaDatabefore, "$.mealHeader");
            jsonObject.remove("mealHeader");
            jsonObject.put("mealHeader",updatedText);

            String updatedData = jsonObject.toString();

            rngHelper.setOperationMetaData(campaignId, updatedData);

            edvoHelper.clearEDVORedis(restId, mealId);

            Processor processorNew = edvoHelper.getMenuV4(restId, lat, lng);
            String respNew = processorNew.ResponseValidator.GetBodyAsText();

            JSONArray texts = JsonPath.read(respNew, "$..menu.widgets[?(@.type==\"carousel\")]..entities[?(@.type==\"meal\")]..text");
            String textAPI = texts.get(0).toString();
            Assert.assertEquals(textAPI, updatedText);

            //reverting changes
            rngHelper.setOperationMetaData(campaignId, metaDatabefore);

            //clearing cache
            edvoHelper.clearEDVORedis(restId, mealId);
        }
        else
        {
            Assert.fail("Meal is not found");
        }
    }

    @Test(dataProvider = "EDVOSubTextUpdate",groups = {"functional","srishty","edvo","meal"},description = "Verify updating edvo sub text")
    public void validateUpdateEDVOMealSubText(String restId, String lat, String lng, String updatedSubText,long campaignId) throws InterruptedException, JSONException {

        Processor processor = edvoHelper.getMenuV4(restId,lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();

        JSONArray widgetTypes = JsonPath.read(resp,"$..menu.widgets..entities..type");
        boolean isMealFound = widgetTypes.contains("meal");
        if (isMealFound == true) {

            JSONArray mealIdArray = JsonPath.read(resp, "$..menu.widgets[?(@.type ==\"carousel\")]..entities[?(@.type== \"meal\")]..id");
            String mealId = mealIdArray.get(0).toString();
            String metaDatabefore = rngHelper.getOperationMetaData(campaignId);

            JSONObject jsonObject;
            jsonObject = new JSONObject(metaDatabefore);

            String headerBefore = JsonPath.read(metaDatabefore, "$.mealDescription");
            jsonObject.remove("mealDescription");
            jsonObject.put("mealDescription",updatedSubText);

            String updatedData = jsonObject.toString();

            rngHelper.setOperationMetaData(campaignId,updatedData );
           // edvoHelper.setEDVOSubText(updatedSubText, mealId);

            edvoHelper.clearEDVORedis(restId, mealId);

            Processor processorNew = edvoHelper.getMenuV4(restId, lat, lng);
            String respNew = processorNew.ResponseValidator.GetBodyAsText();

            JSONArray subTexts = JsonPath.read(respNew, "$..menu.widgets[?(@.type == \"carousel\")]..entities[?(@.type == \"meal\")]..subText");
            String subTextAPI = subTexts.get(0).toString();
            Assert.assertEquals(subTextAPI, updatedSubText);
        }
        else
        {
            Assert.fail("Meal is not found");
        }

    }

    @Test(dataProvider = "minTotal",groups = {"functional","srishty","edvo","meal"},description = "validating min total for meal group")
    public void validateMinTotal(String groupId1, String itemId1, String quantity1,String groupId2, String itemId2, String quantity2,String mealId) throws InterruptedException{

            //clearing the cached values
            edvoHelper.clearEDVORedisWithoutRestId(mealId);

            //calling menu validate v4 for testing
            Processor processor = edvoHelper.validateMeal(groupId1,itemId1,quantity1,groupId2,itemId2,quantity2,mealId);
            String respFinal = processor.ResponseValidator.GetBodyAsText();
            JSONArray reasonArray = JsonPath.read(respFinal, "$.mealValidations..reason");
            String reason = reasonArray.get(0).toString();
            JSONArray validArray = JsonPath.read(respFinal, "$.mealValidations..valid");
            String valid = validArray.get(0).toString();
            String expectedReason = "group validation failed";
            String expectedValid = "false";

            Assert.assertEquals(reason, expectedReason);
            Assert.assertEquals(valid,expectedValid);


    }

    @Test(dataProvider = "maxTotal",groups = {"functional","srishty","edvo","meal"}, description = "verify max_total for meal group")
    public void validateMaxTotal(String groupId1, String itemId1, String quantity1,String groupId2, String itemId2, String quantity2,String mealId) throws InterruptedException{


        //clearing the cached values
        edvoHelper.clearEDVORedisWithoutRestId(mealId);

        //calling menu validate v4 for testing
        Processor processor = edvoHelper.validateMeal(groupId1,itemId1,quantity1,groupId2,itemId2,quantity2,mealId);
        String respFinal = processor.ResponseValidator.GetBodyAsText();
        JSONArray reasonArray = JsonPath.read(respFinal, "$.mealValidations..reason");
        String reason = reasonArray.get(0).toString();
        JSONArray validArray = JsonPath.read(respFinal, "$.mealValidations..valid");
        String valid = validArray.get(0).toString();
        String expectedReason = "group validation failed";
        String expectedValid = "false";

        Assert.assertEquals(reason, expectedReason);
        Assert.assertEquals(valid,expectedValid);

    }

    @Test(dataProvider = "screenOrder",groups = {"functional","srishty","edvo","meal"},description = "verify the screen sequence flow")
    public void verifyScreenOrder(String mealId, String itemId) throws InterruptedException{
        Processor processor = edvoHelper.getMealById( mealId,itemId);
        String resp = processor.ResponseValidator.GetBodyAsText();

        JSONArray groupIdArray1 = JsonPath.read(resp, "$.screens..group.id");
        String groupId1 = groupIdArray1.get(0).toString();
        String groupId2  = groupIdArray1.get(1).toString();
        edvoHelper.setScreenNumber(groupId1,1);
        edvoHelper.setScreenNumber(groupId2,0);

        edvoHelper.clearEDVORedisWithoutRestId(mealId);

        Processor processorNew = edvoHelper.getMealById( mealId,itemId);
        String respNew = processorNew.ResponseValidator.GetBodyAsText();
        JSONArray groupIdArray2 = JsonPath.read(respNew, "$.screens..group.id");
        String groupId1New = groupIdArray2.get(0).toString();
        String groupId2New  = groupIdArray2.get(1).toString();
        Assert.assertEquals(groupId1New,groupId2);
        Assert.assertEquals(groupId2New,groupId1);

    }

    @Test(dataProvider = "exitPageUpdateMainText",groups = {"functional","srishty","edvo","meal"},description = "verify the exit screen and updating the Main text")
    public void verifyUpdateExitScreenMainText(String mealId, String itemId, String mainText) throws InterruptedException{

        Processor processor = edvoHelper.getMealById( mealId,itemId);
        String resp = processor.ResponseValidator.GetBodyAsText();

        String mainTextAPI  = JsonPath.read(resp, "$.exitPage.mainText");
        String mainTextDB =edvoHelper.getExitMainText(mealId);
        assertEquals(mainTextAPI,mainTextDB);

        mealHelper.setExitMainText(mainText,mealId);

        edvoHelper.clearEDVORedisWithoutRestId(mealId);

        Processor processorNew = edvoHelper.getMealById( mealId,itemId);
        String respNew = processorNew.ResponseValidator.GetBodyAsText();

        String mainTextAPINew  = JsonPath.read(respNew, "$.exitPage.mainText");
        assertEquals(mainTextAPINew,mainText);

    }

    @Test(dataProvider = "exitPageUpdateSubText",groups = {"functional","srishty","edvo","meal"},description = "verify the exit screen and updating the Sub text")
    public void verifyUpdateExitScreenSubText(String mealId, String itemId, String subText) throws InterruptedException{

        Processor processor = edvoHelper.getMealById( mealId,itemId);
        String resp = processor.ResponseValidator.GetBodyAsText();

        String subTextAPI  = JsonPath.read(resp, "$.exitPage.subText");
        String subTextDB =mealHelper.getExitSubText(mealId);
        assertEquals(subTextAPI,subTextDB);

        mealHelper.setExitSubText(subText,mealId);

        edvoHelper.clearEDVORedisWithoutRestId(mealId);

        Processor processorNew = edvoHelper.getMealById( mealId,itemId);
        String respNew = processorNew.ResponseValidator.GetBodyAsText();

        String subTextAPINew  = JsonPath.read(respNew, "$.exitPage.subText");
        assertEquals(subTextAPINew,subText);
    }

    @Test(dataProvider = "launchPageUpdateMainText",groups = {"functional","srishty","edvo","meal"},description = "verify the launch screen and updating the Main text")
    public void verifyUpdateLaunchScreenMainText(String mealId, String itemId, String mainText) throws InterruptedException {
        Processor processor = edvoHelper.getMealById( mealId,itemId);
        String resp = processor.ResponseValidator.GetBodyAsText();

        String mainTextAPI  = JsonPath.read(resp, "$.launchPage.mainText");
        String mainTextDB =mealHelper.getLaunchMainText(mealId);
        assertEquals(mainTextAPI,mainTextDB);

        mealHelper.setLaunchMainText(mainText,mealId);

        edvoHelper.clearEDVORedisWithoutRestId(mealId);

        Processor processorNew = edvoHelper.getMealById( mealId,itemId);
        String respNew = processorNew.ResponseValidator.GetBodyAsText();

        String mainTextAPINew  = JsonPath.read(respNew, "$.launchPage.mainText");
        assertEquals(mainTextAPINew,mainText);

    }

    @Test(dataProvider = "launchPageUpdateSubText",groups = {"functional","srishty","edvo","meal"},description = "verify the launch screen and updating the Sub text")
    public void verifyUpdateLaunchScreenSubText(String mealId, String itemId, String subText) throws InterruptedException {
        Processor processor = edvoHelper.getMealById( mealId,itemId);
        String resp = processor.ResponseValidator.GetBodyAsText();

        String subTextAPI  = JsonPath.read(resp, "$.launchPage.subText");
        String subTextDB =mealHelper.getLaunchSubText(mealId);
        assertEquals(subTextAPI,subTextDB);

        mealHelper.setLaunchSubText(subText,mealId);

        edvoHelper.clearEDVORedisWithoutRestId(mealId);

        Processor processorNew = edvoHelper.getMealById( mealId,itemId);
        String respNew = processorNew.ResponseValidator.GetBodyAsText();

        String subTextAPINew  = JsonPath.read(respNew, "$.launchPage.subText");
        assertEquals(subTextAPINew,subText);

    }

    @Test(dataProvider = "addMeal",groups = {"functional","srishty","edvo","meal"},description = "verify adding a valid meal")
    public void addMealValidRequest(String restId, String item1, String item2, String item3, String item4, String item5){
        Processor processor = edvoHelper.addMeal(restId,item1,item2,item3, item4,item5);
        String resp = processor.ResponseValidator.GetBodyAsText();
        assertEquals(processor.ResponseValidator.GetResponseCode(),200);
    }

    @Test(dataProvider = "getMealById",groups = {"functional","srishty","edvo","meal"},description = "verify getting a meal information by ID")
    public void getMealById(String mealId,String ItemId){
        Processor processor = edvoHelper.getMealById( mealId,ItemId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        assertEquals(processor.ResponseValidator.GetResponseCode(),200);
    }

    @Test(dataProvider = "validateMeals",groups = {"functional","srishty","edvo","meal"},description = "validate a valid meal request")
    public void validateMealValidRequest(String group1, String item1, String quantity1,String group2, String item2, String quantity2,String mealId){
        Processor processor = edvoHelper.validateMeal(group1,item1,quantity1,group2,item2,quantity2,mealId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        JSONArray reasonArray = JsonPath.read(resp, "$.mealValidations..reason");
        String reason = reasonArray.get(0).toString();
        assertEquals(reason,"Success");
        JSONArray validArray = JsonPath.read(resp, "$.mealValidations..valid");
        String valid = validArray.get(0).toString();
        assertEquals(valid,"true");
    }

    @Test(dataProvider = "validateMealsNegative",groups = {"functional","srishty","edvo","meal"},description = "verify an invalid meal request")
    public void validateMealinvalidRequest(String group1, String item1, String quantity1, String group2, String item2, String quantity2, String mealId){
        Processor processor = edvoHelper.validateMeal(group1,item1,quantity1,group2,item2,quantity2,mealId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        JSONArray reasonArray = JsonPath.read(resp, "$.mealValidations..reason");
        String reason = reasonArray.get(0).toString();
        assertEquals(reason,"group validation failed");
        JSONArray validArray = JsonPath.read(resp, "$.mealValidations..valid");
        String valid = validArray.get(0).toString();
        assertEquals(valid,"false");
    }

    @Test (dataProvider= "menuV4", groups= {"schemaValidation","srishty","edvo","meal"}, description="Verify schema check for menuV4 API")
    public void menuV4JSONSchemavalidation(String Id,String lat, String lng) throws IOException, ProcessingException {

        Processor processor = edvoHelper.getMenuV4(Id,lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/menuV4.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For Menu V4 API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");


    }

    @Test (dataProvider= "addMeal", groups= {"schemaValidation","srishty","edvo","meal"}, description="Verify schema check for add Meal API")
    public void addMealJSONSchemavalidation(String restId,String item1, String item2, String item3, String item4, String item5) throws IOException, ProcessingException {

        Processor processor = edvoHelper.addMeal(restId,item1,item2,item3,item4,item5);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/addMeal.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For Add Meal API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");


    }

    @Test (dataProvider= "validateMeals", groups= {"schemaValidation","srishty","edvo","meal"}, description="Verify schema check for validate Meal API")
    public void validateMealJSONSchemavalidation(String group1, String item1, String quantity1,String group2, String item2, String quantity2,String mealId) throws IOException, ProcessingException {

        Processor processor = edvoHelper.validateMeal(group1,item1,quantity1,group2,item2,quantity2,mealId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/validateMeal.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For Validate Meal API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");


    }

    @Test (dataProvider= "bulkMealSchema", groups= {"schemaValidation","srishty","edvo","meal"}, description="Verify schema check for bulk Meal API")
    public void getBulkMealJSONSchemavalidation(String id) throws IOException, ProcessingException {

        Processor processor = edvoHelper.getBulkMeals(id);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/getBulkMeals.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For Bulk Meal API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test (dataProvider= "getMealById", groups= {"schemaValidation","srishty","edvo","meal"}, description="Verify schema check for get Meal by Id API")
    public void getMealByIdJSONSchemavalidation(String mealId, String itemId) throws IOException, ProcessingException {

        Processor processor = edvoHelper.getMealById( mealId,itemId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/getMealById.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For get Meal by Id API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test (dataProvider= "validateMinChoiceForGroup", groups= {"srishty","edvo","meal"}, description="Verify validate meal for min choices")
    public void validateMaxMinChoiceForGroup(String group1, String item1, String quantity1,String item2, String quantity2, String group2, String item3, String quantity3,String mealId) throws InterruptedException {

        edvoHelper.clearEDVORedisWithoutRestId(mealId);

        Processor processorNew = edvoHelper.validateMealMinMax(group1,item1,quantity1,item2,quantity2,group2, item3,quantity3,mealId);
        String respNew = processorNew.ResponseValidator.GetBodyAsText();
        assertEquals(processorNew.ResponseValidator.GetResponseCode(),200);
        JSONArray reasonArrayNew = JsonPath.read(respNew, "$.mealValidations..reason");
        String reasonNew = reasonArrayNew.get(0).toString();
        assertEquals(reasonNew,"Success");
        JSONArray validArrayNew = JsonPath.read(respNew, "$.mealValidations..valid");
        String validNew = validArrayNew.get(0).toString();
        assertEquals(validNew,"true");

        edvoHelper.clearEDVORedisWithoutRestId(mealId);

    }

    @Test (dataProvider= "validateMaxQuantityForGroup", groups= {"srishty","edvo","functional","meal"}, description="Verify validate meal for max item quantity")
    public void validateItemQuantity(String group1, String item1, String quantity1,String group2,String item2, String quantity2,String mealId) throws InterruptedException {


        edvoHelper.clearEDVORedisWithoutRestId(mealId);

        Processor processorNew = edvoHelper.validateMeal(group1,item1,quantity1,group2,item2,quantity2,mealId);
        String respNew = processorNew.ResponseValidator.GetBodyAsText();
        assertEquals(processorNew.ResponseValidator.GetResponseCode(),200);
        JSONArray reasonArrayNew = JsonPath.read(respNew, "$.mealValidations..reason");
        String reasonNew = reasonArrayNew.get(0).toString();
        assertEquals(reasonNew,"Success");
        JSONArray validArrayNew = JsonPath.read(respNew, "$.mealValidations..valid");
        String validNew = validArrayNew.get(0).toString();
        assertEquals(validNew,"true");

        edvoHelper.clearEDVORedisWithoutRestId(mealId);

    }

    @Test(dataProvider = "mealAvailableNegative",groups = {"sanity","smoke","srishty","edvo","meal"},description = "Verify the get meal Available API for invalid rest ID and item Ids")
    public void negativeMealCases(String itemIds,String restId){
        Processor processor = edvoHelper.mealAvailable(itemIds,restId.toString());
        String resp = processor.ResponseValidator.GetBodyAsText();
        assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        JSONArray mealInfoList = JsonPath.read(resp,"$.mealInfoList");
        boolean b1 =mealInfoList.isEmpty();
        Assert.assertEquals(b1,true);
    }

    @Test(dataProvider = "mockedServiceability",groups = {"mock","srishty","edvo","meal"},description = "Verify that meal does not show for unserviceable restaurant")
    public void mockUnservicebaleRestForMeal(String unserviceable,String restId, String lat,String lng) throws IOException {

        edvoHelper.mockServiceabilityforRest(unserviceable);
        Processor processor = edvoHelper.getMenuV4(restId,lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();
        JSONArray widgetsType = JsonPath.read(resp,"$.data.menu.widgets..type");
        boolean isMealPresent = widgetsType.contains("meal");
        Assert.assertEquals(isMealPresent,false);
    }

    @AfterClass()
    public void stopWireMock(){
        wireMockHelper.stopMockServer();

    }

//    @Test
//    public void restAndItemAvailability() throws InterruptedException{
//        System.out.println("Starting");
//       /* String lat=System.getenv("lat");
//        String lng= System.getenv("lng");*/
//        //String response=helper.aggregator(new String[]{"12.933196" , "77.602513"}).ResponseValidator.GetBodyAsText();
//        //List<String> restIds = Arrays.asList(helper.JsonString(response, "$.data.restaurants[*].id").split(","));
//        //System.out.println(restIds);
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
//        //for(String restid: restIds)
//        String restId="5394";
//        // {
//        //AV Menu
//        String menu_query="select menu_id from restaurant_menu_map where restaurant_id='"+restId+"';";
//        System.out.println(menu_query);
//        List<Map<String, Object>> lists = sqlTemplate.queryForList(menu_query);
//        for (int i = 0; i < lists.size(); i++) {
//            String menu_id=lists.get(i).get("menu_id").toString();
//            System.out.println("Menu Id===="+menu_id);
//            redisHelper.setValueJson("sandredisstage",0,"AV_MENU_"+menu_id,"{\"status\":true,\"nextChangeTime\":1636364740000}");
//
//        }
//        //AV Items
//        String item_query="select item_id from item_menu_map where menu_id in(select id from menu_taxonomy where parent in(select id from menu_taxonomy where parent in(select menu_id from restaurant_menu_map where restaurant_id ="+restId+") and active=1) and active=1) and active=1;";
//        List<Map<String, Object>> itemLists = sqlTemplate.queryForList(item_query);
//        for (int i = 0; i < itemLists.size(); i++) {
//            String item_id=itemLists.get(i).get("item_id").toString();
//            System.out.println("Item Id===="+item_id);
//            redisHelper.setValueJson("sandredisstage",0,"AV_ITEM_"+item_id,"{\"status\":true,\"nextChangeTime\":1636364740000}");
//        }
//        // }
//
//    }

}