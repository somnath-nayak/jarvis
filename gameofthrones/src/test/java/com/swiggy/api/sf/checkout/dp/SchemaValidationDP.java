package com.swiggy.api.sf.checkout.dp;

import com.swiggy.api.sf.checkout.constants.CancellationFeeConstants;
import com.swiggy.api.sf.checkout.constants.SchemaValidationConstants;
import org.testng.annotations.DataProvider;

public class SchemaValidationDP {

    @DataProvider(name = "updateCartJsonSchemaTestdata")
    public static Object[][] updateCartJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", SchemaValidationConstants.REST_ID, true, 1, false, false, 0, false, null, SchemaValidationConstants.CART_TYPE_REGULAR, SchemaValidationConstants.UPDATE_CART_V2_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", SchemaValidationConstants.REST_ID, true, 1, false, false, 0, false, null, SchemaValidationConstants.CART_TYPE_REGULAR, SchemaValidationConstants.UPDATE_CART_V2_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", SchemaValidationConstants.REST_ID, true, 1, false, false, 0, false, null, SchemaValidationConstants.CART_TYPE_REGULAR, SchemaValidationConstants.UPDATE_CART_V2_RESPONSE_IOS_SCHEMA_01}
        };
    }

    @DataProvider(name = "checkTotalsJsonSchemaTestdata")
    public static Object[][] checkTotalsJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", SchemaValidationConstants.REST_ID, true, 1, false, false, 0, false, null, SchemaValidationConstants.CART_TYPE_REGULAR, SchemaValidationConstants.CHECKTOTALS_V2_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", SchemaValidationConstants.REST_ID, true, 1, false, false, 0, false, null, SchemaValidationConstants.CART_TYPE_REGULAR, SchemaValidationConstants.CHECKTOTALS_V2_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", SchemaValidationConstants.REST_ID, true, 1, false, false, 0, false, null, SchemaValidationConstants.CART_TYPE_REGULAR, SchemaValidationConstants.CHECKTOTALS_V2_RESPONSE_IOS_SCHEMA_01}
        };
    }


    @DataProvider(name = "checkBankListAllJsonSchemaTestdata")
    public static Object[][] checkBankListAllJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", SchemaValidationConstants.CHECKBANKLISTALL_V1_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", SchemaValidationConstants.CHECKBANKLISTALL_V1_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", SchemaValidationConstants.CHECKBANKLISTALL_V1_RESPONSE_IOS_SCHEMA_01}
        };
    }

    @DataProvider(name = "checkCardsJuspayJsonSchemaTestdata")
    public static Object[][] checkCardsJuspayJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", SchemaValidationConstants.CHECKCARDSJUSPAY_V1_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", SchemaValidationConstants.CHECKCARDSJUSPAY_V1_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", SchemaValidationConstants.CHECKCARDSJUSPAY_V1_RESPONSE_IOS_SCHEMA_01}
        };
    }

    @DataProvider(name = "checkApplyCouponJsonSchemaTestdata")
    public static Object[][] checkApplyCouponJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", "ABCDE", SchemaValidationConstants.CHECKAPPLYCOUPON_V1_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", "ABCDE",SchemaValidationConstants.CHECKAPPLYCOUPON_V1_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", "ABCDE",SchemaValidationConstants.CHECKAPPLYCOUPON_V1_RESPONSE_IOS_SCHEMA_01}
        };
    }

    @DataProvider(name = "checkRemoveCouponJsonSchemaTestdata")
    public static Object[][] checkRemoveCouponJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", "ABCDE",SchemaValidationConstants.CHECKREMOVECOUPON_V1_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", "ABCDE",SchemaValidationConstants.CHECKREMOVECOUPON_V1_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", "ABCDE",SchemaValidationConstants.CHECKREMOVECOUPON_V1_RESPONSE_IOS_SCHEMA_01}
        };
    }

    @DataProvider(name = "checkCancellationFeeRevertJsonSchemaTestdata")
    public static Object[][] checkCancellationFeeRevertJsonSchemaTestdata() {
        return new Object[][]{
                {CancellationFeeConstants.RESTAURANT_ID, SchemaValidationConstants.CHECKCANCELLATIONFEEREVERT_V1_RESPONSE_SCHEMA_01}
        };
    }

    @DataProvider(name = "checkSettingsWalletEmailUpdateURLJsonSchemaTestdata")
    public static Object[][] checkSettingsWalletEmailUpdateURLJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", SchemaValidationConstants.CHECKSETTINGSWALLETEMAILUPDATEURL_V1_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", SchemaValidationConstants.CHECKSETTINGSWALLETEMAILUPDATEURL_V1_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", SchemaValidationConstants.CHECKSETTINGSWALLETEMAILUPDATEURL_V1_RESPONSE_IOS_SCHEMA_01}
        };
    }

    @DataProvider(name = "checkGetAllPaymentJsonSchemaTestdata")
    public static Object[][] checkGetAllPaymentJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", SchemaValidationConstants.CHECKGETALLPAYMENT_V1_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", SchemaValidationConstants.CHECKGETALLPAYMENT_V1_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", SchemaValidationConstants.CHECKGETALLPAYMENT_V1_RESPONSE_IOS_SCHEMA_01}
        };
    }

    @DataProvider(name = "checkGetInventoryJsonSchemaTestdata")
    public static Object[][] checkGetInventoryJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", SchemaValidationConstants.CHECKGETINVENTORY_V1_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", SchemaValidationConstants.CHECKGETINVENTORY_V1_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", SchemaValidationConstants.CHECKGETINVENTORY_V1_RESPONSE_IOS_SCHEMA_01}
        };
    }

    @DataProvider(name = "checkCafeUpdateCartJsonSchemaTestdata")
    public static Object[][] checkCafeUpdateCartJsonSchemaTestdata() {
        return new Object[][]{
                {"Swiggy-Android", "300", "Testing", "5748269","1","5271","CAFE", SchemaValidationConstants.CHECKCAFEUPDATECART_V2_RESPONSE_ANDROID_SCHEMA_01},
                {"Web", "300", "Testing", "5748269","1","5271","CAFE", SchemaValidationConstants.CHECKCAFEUPDATECART_V2_RESPONSE_WEB_SCHEMA_01},
                {"Swiggy-iOS", "300", "Testing", "5748269","1","5271","CAFE", SchemaValidationConstants.CHECKCAFEUPDATECART_V2_RESPONSE_IOS_SCHEMA_01}
        };
    }
}
