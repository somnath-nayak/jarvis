package com.swiggy.api.sf.checkout.tests;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.RTSHelper;
import com.swiggy.api.sf.checkout.constants.RTSConstants;
import com.swiggy.api.sf.checkout.dp.RTSDP;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartFromRedis.CartFromRedis;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.checkout.helper.rts.helper.CartHelper;
import com.swiggy.api.sf.checkout.helper.rts.helper.DeliveryHelper;
import com.swiggy.api.sf.checkout.helper.rts.validator.GenericValidator;
import com.swiggy.api.sf.rng.helper.RngHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;

public class RTSTest extends RTSDP {
    CartHelper cartHelper = new CartHelper();
    DeliveryHelper deliveryHelper = new DeliveryHelper();
    GenericValidator genericValidator = new GenericValidator();
    RngHelper rngHelper = new RngHelper();
    String lat = null;
    String lng = null;
    Long orderIdOld = null;
    Long orderIdNew = null;
    Integer cartIdOld = null;
    Integer cartIdNew = null;
    List<Long> orderIds = new ArrayList<>();


    @Test(dataProvider = "testData", groups = {"sanity"}, description = "Test create cart for reserve cart...")
    public void createCartTest(String mobile,
                             String password,
                             String restaurantId,
                             boolean isCartItemRequired,
                             int cartItemQuantity,
                             boolean isAddonRequiredForCartItems,
                             boolean isMealItemRequired,
                             int mealItemQuantity,
                             boolean isAddonRequiredForMealItems,
                             String[] mealIds,
                             String cartType,
                             String deId,
                             String cityId,
                             String zoneId,
                             String enableDE,
                             String tdType) {

        //Creates or update TD & for no TD, desables the existing TD
        cartHelper.createOrUpdateTradeDiscountForRTS(restaurantId, tdType);
        CartV2Response cartV2Response = cartHelper.updateCart(mobile,
                password,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_CARTV2_SUCCESS,RTSConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                cartV2Response);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        deId = deliveryHelper.createNewDE(Integer.parseInt(zoneId));
        System.out.println("DE_ID : " + deId);
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);

        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_CARTV2_SUCCESS,RTSConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                cartV2Response);
        cartIdOld = cartHelper.getCartIdFromRedisCart(mobile);
        Reporter.log("[CART_ID] ::::::::::::::::::::::::::::: " + cartIdOld, true);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);
        Processor reserveCart = cartHelper.reserveCart(cartV2Response);
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_RESERVE_CART_SUCCESS, RTSConstants.STATUS_MESSAGE_RESERVE_CART_SUCCESS,
                reserveCart, "Reserve Cart Failed...");
        orderIdOld = cartHelper.getOrderIdFromRedisCart(mobile);
        deliveryHelper.unreserveDE(orderIdOld);
        Reporter.log("[ORDER_ID] ::::::::::::::::::::::::::::: " + orderIdOld, true);
        orderIds.add(orderIdOld);

        Reporter.log("[LIST_OF_ORDER_ID'S]::::::::::::::::::::::::: " + orderIds, true);

        Reporter.log("[CART_ID] '"+ cartIdOld + "'", true);
        Reporter.log("[ORDER_ID] '"+ orderIdOld + "'", true);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotNull(cartIdOld, "Cart ID is null...");
        softAssert.assertNotNull(orderIdOld, "Order ID is null...");
        softAssert.assertAll();
    }

    @Test(dataProvider = "testData", groups = {"sanity"}, description = "Test Edit Cart for Reserve Cart...")
    public void editCartTest(String mobile,
                                String password,
                                String restaurantId,
                                boolean isCartItemRequired,
                                int cartItemQuantity,
                                boolean isAddonRequiredForCartItems,
                                boolean isMealItemRequired,
                                int mealItemQuantity,
                                boolean isAddonRequiredForMealItems,
                                String[] mealIds,
                                String cartType,
                                String deId,
                                String cityId,
                                String zoneId,
                                String enableDE,
                                String tdType) {

        //Creates or update TD & for no TD, desables the existing TD
        cartHelper.createOrUpdateTradeDiscountForRTS(restaurantId, tdType);
        CartV2Response cartV2Response = cartHelper.updateCart(mobile,
                password,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        deId = deliveryHelper.createNewDE(Integer.parseInt(zoneId));
        System.out.println("DE_ID : " + deId);
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);

        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_CARTV2_SUCCESS,RTSConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                cartV2Response);
        cartIdOld = cartHelper.getCartIdFromRedisCart(mobile);
        Reporter.log("[OLD_CART_ID] ::::::::::::::::::::::::::::: " + cartIdOld, true);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);
        Processor reserveCart = cartHelper.reserveCart(cartV2Response);
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_RESERVE_CART_SUCCESS, RTSConstants.STATUS_MESSAGE_RESERVE_CART_SUCCESS,
                reserveCart, "Reserve Cart Failed...");
        orderIdOld = cartHelper.getOrderIdFromRedisCart(mobile);
        deliveryHelper.unreserveDE(orderIdOld);
        Reporter.log("[OLD_ORDER_ID] ::::::::::::::::::::::::::::: " + orderIdOld, true);
        orderIds.add(orderIdOld);

        cartV2Response = cartHelper.updateCart(mobile,
                password,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);

        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_CARTV2_SUCCESS,RTSConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                cartV2Response);
        cartIdNew = cartHelper.getCartIdFromRedisCart(mobile);
        Reporter.log("NEW_CART_ID ::::::::::::::::::::::::::::: " + cartIdNew, true);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);
        reserveCart = cartHelper.reserveCart(cartV2Response);
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_RESERVE_CART_SUCCESS, RTSConstants.STATUS_MESSAGE_RESERVE_CART_SUCCESS,
                reserveCart, "Reserve Cart Failed...");
        orderIdNew = cartHelper.getOrderIdFromRedisCart(mobile);
        deliveryHelper.unreserveDE(orderIdNew);
        Reporter.log("[NEW_ORDER_ID] ::::::::::::::::::::::::::::: " + orderIdNew, true);
        orderIds.add(orderIdNew);

        Reporter.log("[LIST_OF_ORDER_ID'S]::::::::::::::::::::::::: " + orderIds, true);
        Reporter.log("Old Cart Id '"+ cartIdOld +"' != " + "New Cart Id '" + cartIdNew + "'", true);
        Reporter.log("Old Order Id '"+ orderIdOld +"' != " + "New Order Id '" + orderIdNew + "'", true);
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(cartIdOld, cartIdNew, "Cart Id did not changed after hitting the update cart api twice...");
        softAssert.assertNotEquals(orderIdOld, orderIdNew, "Order Id did not changed after hitting the reserve cart api...");
        softAssert.assertAll();
    }

    @Test(dataProvider = "testData", groups = {"sanity"}, description = "Test Flush Cart for Reserve Cart...")
    public void flushCartTest(String mobile,
                               String password,
                               String restaurantId,
                               boolean isCartItemRequired,
                               int cartItemQuantity,
                               boolean isAddonRequiredForCartItems,
                               boolean isMealItemRequired,
                               int mealItemQuantity,
                               boolean isAddonRequiredForMealItems,
                               String[] mealIds,
                               String cartType,
                               String deId,
                               String cityId,
                               String zoneId,
                               String enableDE,
                               String tdType) {

        //Creates or update TD & for no TD, desables the existing TD
        cartHelper.createOrUpdateTradeDiscountForRTS(restaurantId, tdType);
        CartV2Response cartV2Response = cartHelper.updateCart(mobile,
                password,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        deliveryHelper.createNewDE(Integer.parseInt(zoneId));
        System.out.println("[DE_ID] : " + deId);
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);

        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_CARTV2_SUCCESS,RTSConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                cartV2Response);
        cartIdOld = cartHelper.getCartIdFromRedisCart(mobile);
        Reporter.log("[CART_ID] ::::::::::::::::::::::::::::: " + cartIdOld, true);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        cartHelper.flushCart(); //Delete Cart
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);
        Processor reserveCart = cartHelper.reserveCart(cartV2Response);
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_RESERVE_CART_NULL_CART, RTSConstants.STATUS_MESSAGE_RESERVE_CART_NULL_CART,
                reserveCart, "Reserve Cart Failed for Empty(Null) Cart...");
    }

    @Test (dataProvider = "testData", groups = {"sanity"}, description = "Test unreserve cart...")
    public void unreserveCartTest(String mobile,
                                  String password,
                                  String restaurantId,
                                  boolean isCartItemRequired,
                                  int cartItemQuantity,
                                  boolean isAddonRequiredForCartItems,
                                  boolean isMealItemRequired,
                                  int mealItemQuantity,
                                  boolean isAddonRequiredForMealItems,
                                  String[] mealIds,
                                  String cartType,
                                  String deId,
                                  String cityId,
                                  String zoneId,
                                  String enableDE,
                                  String tdType){

        cartHelper.createOrUpdateTradeDiscountForRTS(restaurantId, tdType);
        CartV2Response cartV2Response = cartHelper.updateCart(mobile,
                password,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_CARTV2_SUCCESS,RTSConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                cartV2Response);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        deId = deliveryHelper.createNewDE(Integer.parseInt(zoneId));
        System.out.println("DE_ID : " + deId);
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);

        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_CARTV2_SUCCESS,RTSConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                cartV2Response);
        cartIdOld = cartHelper.getCartIdFromRedisCart(mobile);
        Reporter.log("[CART_ID] ::::::::::::::::::::::::::::: " + cartIdOld, true);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);
        Processor reserveCart = cartHelper.reserveCart(cartV2Response);
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_RESERVE_CART_SUCCESS, RTSConstants.STATUS_MESSAGE_RESERVE_CART_SUCCESS,
                reserveCart, "Reserve Cart Failed...");
        orderIdOld = cartHelper.getOrderIdFromRedisCart(mobile);
        Processor unreserveCart = cartHelper.unreserverCart();
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_UNRESERVE_CART, RTSConstants.STATUS_MESSAGE_UNRESERVE_CART, unreserveCart);
        Long newOrderId = cartHelper.getOrderIdFromRedisCart(mobile);

        Assert.assertNull(newOrderId, "Order Id not deleted from Redis Cart. Hence, un-reserve Cart failed...!!");
    }

    @Test (dataProvider = "reserveMealCartTestData", groups = {"sanity"}, description = "Test reserve cart for meals...")
    public void reserveMealCartTest(String mobile,
                                    String password,
                                    String restaurantId,
                                    boolean isCartItemRequired,
                                    int cartItemQuantity,
                                    boolean isAddonRequiredForCartItems,
                                    boolean isMealItemRequired,
                                    int mealItemQuantity,
                                    boolean isAddonRequiredForMealItems,
                                    String[] mealIds,
                                    String cartType,
                                    String deId,
                                    String cityId,
                                    String zoneId,
                                    String enableDE,
                                    String tdType){

        cartHelper.createOrUpdateMealTD(restaurantId, mealIds[0], tdType);
        CartV2Response cartV2Response = cartHelper.updateCart(mobile,
                password,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_CARTV2_SUCCESS,RTSConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                cartV2Response);
        lat = cartV2Response.getData().getRestaurantDetails().getLat();
        lng = cartV2Response.getData().getRestaurantDetails().getLng();
        deId = deliveryHelper.createNewDE(Integer.parseInt(zoneId));
        System.out.println("DE_ID : " + deId);
        deliveryHelper.enableDE(deId, lat, lng, zoneId, cityId, enableDE);
        Processor reserveCart = cartHelper.reserveCart(cartV2Response);
        genericValidator.smokeCheck(RTSConstants.STATUS_CODE_RESERVE_CART_SUCCESS, RTSConstants.STATUS_MESSAGE_RESERVE_CART_SUCCESS,
                reserveCart, "Reserve Cart Failed...");
        cartIdOld = cartHelper.getCartIdFromRedisCart(mobile);
        orderIdOld = cartHelper.getOrderIdFromRedisCart(mobile);
        deliveryHelper.unreserveDE(orderIdOld);
        Reporter.log("[ORDER_ID] ::::::::::::::::::::::::::::: " + orderIdOld, true);
        orderIds.add(orderIdOld);
        Reporter.log("[LIST_OF_ORDER_ID'S]::::::::::::::::::::::::: " + orderIds, true);
        Reporter.log("[CART_ID] '"+ cartIdOld + "'", true);
        Reporter.log("[ORDER_ID] '"+ orderIdOld + "'", true);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotNull(cartIdOld, "Cart ID is null...");
        softAssert.assertNotNull(orderIdOld, "Order ID is null...");
        softAssert.assertAll();
    }

    @BeforeMethod
    public void disableTD(){
        rngHelper.disabledActiveTD(RTSConstants.RESTAURANT_ID_1);
    }

    @AfterMethod
    public void callToUnreserve(){
        deliveryHelper.unreserveDE(orderIds);
    }

}
