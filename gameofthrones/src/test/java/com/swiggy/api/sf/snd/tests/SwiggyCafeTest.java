package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import com.swiggy.api.sf.snd.helper.SwiggyCafeHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import net.minidev.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SwiggyCafeTest extends  SnDDp {
        SnDHelper sDHelper = new SnDHelper();
        SANDHelper helper = new SANDHelper();
        CMSHelper cmsHelper = new CMSHelper();
        SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();
        CheckoutHelper checkoutHelper=new CheckoutHelper();
        SwiggyCafeHelper cafeHelper = new SwiggyCafeHelper();

    @Test(dataProvider= "accessibleCorporates", groups= {"functional","regression","srishty","cafe"}, description="Verify accessible corporates for a mapped user")
    public void getAccessibleCorporates(String userId) {
        Processor processor = cafeHelper.getAccessibleCorporates(userId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        JSONArray corporateIds= JsonPath.read(resp, "$.data[*]");
        boolean temp = corporateIds.isEmpty();
        Assert.assertEquals(temp,false);

    }

    @Test(dataProvider= "accessibleCorporatesNegative", groups= {"functional","regression","srishty","cafe"}, description="Verify accessible corporates for a mapped user")
    public void getAccessibleCorporatesNegative(String userId) {
        Processor processor = cafeHelper.getAccessibleCorporates(userId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        JSONArray corporateIds= JsonPath.read(resp, "$.data[*]");
        boolean temp = corporateIds.isEmpty();
        Assert.assertEquals(temp,true);
    }

    @Test(dataProvider= "accessibleCorporates", groups= {"functional","regression","srishty","cafe"}, description="Verify latest accessible corporates for a mapped user")
    public void getAccessibleLatestCorporates(String userId) {
        Processor processor = cafeHelper.accessibleLatestCorporates(userId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Integer corporateIds= JsonPath.read(resp, "$.data");
        String temp = corporateIds.toString();
        Assert.assertNotNull(temp);

    }

    @Test(dataProvider= "accessibleCorporatesNegative", groups= {"functional","regression","srishty","cafe"}, description="Verify latest accessible corporates for a mapped user")
    public void getAccessibleLatestCorporatesNegative(String userId) {
        Processor processor = cafeHelper.accessibleLatestCorporates(userId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        String corporateIds= JsonPath.read(resp, "$.data");
        Assert.assertNull(corporateIds);

    }

    @Test(dataProvider= "authenticate", groups= {"functional","regression","srishty","cafe"}, description="Verify authenticate corporates positive combinations")
    public void authenticate(String corporateId, String passcode, String userId) {
        Processor processor = cafeHelper.authenticate(corporateId,passcode,userId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "success");
        boolean data= JsonPath.read(resp,"$.data");
        Assert.assertEquals(data,true);
    }

    @Test(dataProvider= "authenticateNegative", groups= {"functional","regression","srishty","cafe"}, description="Verify authenticate corporate for invalid combinations")
    public void authenticateNegative(String corporateId, String passcode, String userId) {
        Processor processor = cafeHelper.authenticate(corporateId,passcode,userId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "success");
        boolean data= JsonPath.read(resp,"$.data");
        Assert.assertEquals(data,false);
    }

    @Test(dataProvider= "accessToCafe", groups= {"functional","regression","srishty","cafe"}, description="Verify access to cafe for positive combinations")
    public void accessToCafe(String cafeId, String userId, String passcode) {
        Processor processor = cafeHelper.accessToCafe(cafeId,userId,passcode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "success");
        boolean data= JsonPath.read(resp,"$.data");
        Assert.assertEquals(data,true);
    }

    @Test(dataProvider= "accessToCafeNegative", groups= {"functional","regression","srishty","cafe"}, description="Verify access to cafe  for invalid combinations")
    public void accessToCafeNegative(String cafeId, String userId, String passcode) {
        Processor processor = cafeHelper.accessToCafe(cafeId,userId,passcode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "success");
        boolean data= JsonPath.read(resp,"$.data");
        Assert.assertEquals(data,false);
    }

    @Test(dataProvider= "accessToCorporate", groups= {"functional","regression","srishty","cafe"}, description="Verify access to  corporate for positive combinations")
    public void  accessToCorporate(String corporateId, String userId, String passcode) {
        Processor processor = cafeHelper.accessToCorporate(corporateId,userId,passcode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "success");
        boolean data= JsonPath.read(resp,"$.data");
        Assert.assertEquals(data,true);
    }

    @Test(dataProvider= "accessToCorporateNegative", groups= {"functional","regression","srishty","cafe"}, description="Verify access to  corporate for invalid combinations")
    public void accessToCorporateNegative(String corporateId, String userId, String passcode) {
        Processor processor = cafeHelper.accessToCorporate(corporateId,userId,passcode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "success");
        boolean data= JsonPath.read(resp,"$.data");
        Assert.assertEquals(data,false);
    }

    @Test(dataProvider= "accessToRestaurant", groups= {"functional","regression","srishty","cafe"}, description="Verify access to restaurant positive combinations")
    public void  accessToRestaurant(String corporateId, String userId, String passcode) {
        Processor processor = cafeHelper.accessToRestaurant(corporateId,userId,passcode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "success");
        boolean data= JsonPath.read(resp,"$.data");
        Assert.assertEquals(data,true);
    }

    @Test(dataProvider= "accessToRestaurantNegative", groups= {"functional","regression","srishty","cafe"}, description="Verify access to restaurant  for invalid combinations")
    public void accessToRestaurantNegative(String corporateId, String userId, String passcode) {
        Processor processor = cafeHelper.accessToRestaurant(corporateId,userId,passcode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "success");
        boolean data= JsonPath.read(resp,"$.data");
        Assert.assertEquals(data,false);
    }

    @Test(dataProvider= "getCafeListing", groups= {"functional","regression","srishty","cafe"}, description="Verify cafe listing for positive combinations")
    public void  getCafeListing(String corporateId, String userId, String passcode) {
        Processor processor = cafeHelper.getCafes(corporateId,userId,passcode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
    }

    @Test(dataProvider= "getCorporateByLatLng", groups= {"functional","regression","srishty","cafe"}, description="Verify corporate listing by lat long for positive combinations")
    public void  getCorporatesList(String latLng)  {
        Processor processor = cafeHelper.getCorporatesList(latLng) ;
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
    }

    @Test(dataProvider= "getRestaurantList", groups= {"functional","regression","srishty","cafe"}, description="Verify corporate listing by lat long for positive combinations")
    public void  getRestaurantList(String cafeId, String userId, String passcode)  {
        Processor processor = cafeHelper.getRestaurantList(cafeId,  userId,  passcode) ;
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
    }


}
