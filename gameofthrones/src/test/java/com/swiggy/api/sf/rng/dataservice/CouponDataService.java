package com.swiggy.api.sf.rng.dataservice;

/*
This class is used to create all comobination of data in two dimension array which will help to provide the data to our
dataprovider method. This will save lots of time to prepare your dataset creation.
 */

public class CouponDataService {

    /*
    The below method returns all the possible combination of data
     */
   // public Object[][] createCouponDataService(){}
//    {
//        List<String> couponType = new ArrayList<String>();
//        couponType.add("Discount");
//        couponType.add("NonDiscount");
//
//        List<String> name = new ArrayList<String>();
//        name.add("Test"+RngHelper.getRandomPostfix());
//
//        List<String> code = new ArrayList<String>();
//        code.add("Auto"+RngHelper.getRandomPostfix());
//
//        List<String> description = new ArrayList<String>();
//        description.add("The description "+RngHelper.getRandomPostfix());
//
//        List<String> validFrom = new ArrayList<String>();
//        validFrom.add(RngHelper.getCurrentDate());
//
//        List<String> validTill = new ArrayList<String>();
//        validTill.add(RngHelper.getFutureDate());
//
//
//        List<String> preferredPaymentMethod = new ArrayList<String>();
//        preferredPaymentMethod.add("Juspay-NB");
//
//        List<String> createdOn = new ArrayList<String>();
//        createdOn.add("");
//
//        List<String> image = new ArrayList<String>();
//        image.add("NULL");
//
//        List<String> couponBucket = new ArrayList<String>();
//        couponBucket.add("Internal");
////        couponBucket.add("External");
//
//        List<Integer> cuisineRestriction = new ArrayList<Integer>();
//        cuisineRestriction.add(0);
//
//        List<String> pgMessage = new ArrayList<String>();
//        pgMessage.add("");
//
//
//        List<String> createdBy = new ArrayList<String>();
//        createdBy.add("user@swiggy.in");
//
//        List<String> title = new ArrayList<String>();
//        title.add("automation");
//
//        List<String> logo = new ArrayList<String>();
//        logo.add("lbx02iaashsnhea5fpwa");
//
//        List<Integer> isPrivate = new ArrayList<Integer>();
//        isPrivate.add(0);
////        isPrivate.add(1);
//
//        List<String> refundSource = new ArrayList<String>();
//        refundSource.add("");
//
//        List<Integer> totalAvailable = new ArrayList<Integer>();
//        totalAvailable.add(3);
//
//        List<Integer> totalPerUser = new ArrayList<Integer>();
//        totalPerUser.add(3);
////        totalPerUser.add(1);
//
//        List<Integer> customerRestriction = new ArrayList<Integer>();
//        customerRestriction.add(1);
//
//
//        List<Integer> cityRestriction = new ArrayList<Integer>();
//        cityRestriction.add(0);
//
//        List<Integer> areaRestriction = new ArrayList<Integer>();
//        areaRestriction.add(0);
//
//        List<Integer> restaurantRestriction = new ArrayList<Integer>();
//        restaurantRestriction.add(0);
//
//        List<Integer> categoryRestriction = new ArrayList<Integer>();
//        categoryRestriction.add(0);
//
//        List<Integer> itemRestriction = new ArrayList<Integer>();
//        itemRestriction.add(0);
//
//        List<Integer> discountPercentage = new ArrayList<Integer>();
//        discountPercentage.add(14);
////        discountPercentage.add(50);
//
//        List<Integer> discountAmount = new ArrayList<Integer>();
//        discountAmount.add(50);
////        discountAmount.add(200);
//
//        List<Integer> freeShipping = new ArrayList<Integer>();
//        freeShipping.add(0);
//
//        List<Integer> freeGifts = new ArrayList<Integer>();
//        freeGifts.add(0);
//
//
//        List<Integer> firstOrderRestriction = new ArrayList<Integer>();
//
//        firstOrderRestriction.add(0);
//
//        List<Integer> userClient = new ArrayList<Integer>();
//        userClient.add(0);
//
//        List<Integer> slotRestriction = new ArrayList<Integer>();
//        slotRestriction.add(0);
//
//        List<Integer> discountItem = new ArrayList<Integer>();
//        discountItem.add(-1);
//
//        List<Integer> usageCount = new ArrayList<Integer>();
//        usageCount.add(0);
//
//        List<Integer> expiryOffset = new ArrayList<Integer>();
//        expiryOffset.add(1);
//
//        List<Integer> isBankDiscount = new ArrayList<Integer>();
//        isBankDiscount.add(1);
//
//        List<Integer> supportedIosVersion = new ArrayList<Integer>();
//        supportedIosVersion.add(1);
//
//        List<Boolean> applicableWithSwiggyMoney = new ArrayList<Boolean>();
//        applicableWithSwiggyMoney.add(true);
////        applicableWithSwiggyMoney.add(false);
//
//        List<MinAmountObject> minAmountObject=new ArrayList<MinAmountObject>();
//        MinAmountObject minamtObject=new MinAmountObject();
//        minamtObject.setCart(0);
//        minAmountObject.add(minamtObject);
//
//        List<MinQuantityObject> minQuantityObject=new ArrayList<MinQuantityObject>();
//
//        MinQuantityObject minQnobject=new MinQuantityObject();
//        minQnobject.setCart(0);
//        minQuantityObject.add(minQnobject);
//
//        List<String[]> tnc=new ArrayList<String[]>();
//        tnc.add(new String[]{"testng","test"});
//
//        DataGenerator generator = new DataGenerator();
//
//        return generator.generatevariants2(couponType,name,code,description,isPrivate,validFrom,validTill,totalAvailable,totalPerUser,customerRestriction,cityRestriction,areaRestriction,restaurantRestriction,categoryRestriction,itemRestriction,discountPercentage,discountAmount,freeShipping,freeGifts,firstOrderRestriction,preferredPaymentMethod,userClient,slotRestriction,createdOn,image,couponBucket,cuisineRestriction,discountItem,usageCount,expiryOffset,applicableWithSwiggyMoney,isBankDiscount,refundSource,pgMessage,supportedIosVersion,createdBy,title,tnc,logo,minAmountObject,minQuantityObject);
//    }

}
