package com.swiggy.api.sf.checkout.dp;

import java.io.IOException;

import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.checkout.constants.EditTimeConstants;

public class WalletDP {
	
	@DataProvider(name = "freeChargeWallet")
    public static Object[][] walletCheck() throws IOException{
	
        return new Object[][]{
        	{EditTimeConstants.restInitSource,"1257724","1","226","1257725","2",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE}
        
        };
	}
}
        