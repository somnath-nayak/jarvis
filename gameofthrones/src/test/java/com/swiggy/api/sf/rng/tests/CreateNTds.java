package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.freebie.FreebieRestaurantList;
import com.swiggy.api.sf.rng.pojo.freedelivery.FreeDeliveryRestaurantList;

public class CreateNTds {

	
	CreatePercentageTDWithRestrictions cr;
	CreateFlatTDWithRestrictions cf;
	CreateFreebieTDWithRestrictions cfb ;
	CreateFreedeliveryTDWithRestrictions cfd;
	public List createPercentageTD(int n, List al,CreatePercentageTDWithRestrictions cr) throws IOException
	{
       for (int i = 0; i < n; i++) {
			
			al.add(cr.getPercentageTD("SFO", "Restaurant"));
			al.add( cr.getPercentageTD("RFO", "Restaurant"));
			al.add( cr.getPercentageTD("NR", "Restaurant"));
			al.add( cr.getPercentageTD("UR", "Restaurant"));
			al.add( cr.getPercentageTD("DU", "Restaurant"));
			al.add( cr.getPercentageTD("TR", "Restaurant"));
			al.add(cr.getPercentageTD("SFO", "Category"));
			al.add( cr.getPercentageTD("RFO", "Category"));
			al.add( cr.getPercentageTD("NR", "Category"));
			al.add( cr.getPercentageTD("UR", "Category"));
			al.add( cr.getPercentageTD("DU", "Category"));
			al.add( cr.getPercentageTD("TR", "Category"));
			al.add(cr.getPercentageTD("SFO", "Subcategory"));
			al.add( cr.getPercentageTD("RFO", "Subcategory"));
			al.add( cr.getPercentageTD("NR", "Subcategory"));
			al.add( cr.getPercentageTD("UR", "Subcategory"));
			al.add( cr.getPercentageTD("DU", "Subcategory"));
			al.add( cr.getPercentageTD("TR", "Subcategory"));
			al.add(cr.getPercentageTD("SFO", "Item"));
			al.add( cr.getPercentageTD("RFO", "Item"));
			al.add( cr.getPercentageTD("NR", "Item"));
			al.add( cr.getPercentageTD("UR", "Item"));
			al.add( cr.getPercentageTD("DU", "Item"));
			al.add( cr.getPercentageTD("TR", "Item"));
       }
       return al;
	}
	public List createFlatTD(int n,List al,CreateFlatTDWithRestrictions cf) throws IOException
	{
		 for (int i = 0; i < n; i++) {
		al.add(cf.getFlatTD("SFO", "Restaurant"));
		al.add(cf.getFlatTD("RFO", "Restaurant"));
		al.add(cf.getFlatTD("NR", "Restaurant"));
		al.add(cf.getFlatTD("UR", "Restaurant"));
		al.add(cf.getFlatTD("DU", "Restaurant"));
		al.add(cf.getFlatTD("TR", "Restaurant"));
		al.add(cf.getFlatTD("SFO", "Category"));
		al.add(cf.getFlatTD("RFO", "Category"));
		al.add(cf.getFlatTD("NR", "Category"));
		al.add(cf.getFlatTD("UR", "Category"));
		al.add(cf.getFlatTD("DU", "Category"));
		al.add(cf.getFlatTD("TR", "Category"));
		al.add(cf.getFlatTD("SFO", "Subcategory"));
		al.add(cf.getFlatTD("RFO", "Subcategory"));
		al.add(cf.getFlatTD("NR", "Subcategory"));
		al.add(cf.getFlatTD("UR", "Subcategory"));
		al.add(cf.getFlatTD("DU", "Subcategory"));
		al.add(cf.getFlatTD("TR", "Subcategory"));
		al.add(cf.getFlatTD("SFO", "Item"));
		al.add(cf.getFlatTD("RFO", "Item"));
		al.add(cf.getFlatTD("NR", "Item"));
		al.add(cf.getFlatTD("UR", "Item"));
		al.add(cf.getFlatTD("DU", "Item"));
		al.add(cf.getFlatTD("TR", "Item"));
	    }
		 
		 return al;
	}
	public List createFreebieTD(int n,List al, CreateFreebieTDWithRestrictions cfb) throws IOException
	{
		 for (int i = 0; i < n; i++) {
		al.add(cfb.getFreebieTD("SFO", "Restaurant"));
		al.add(cfb.getFreebieTD("RFO", "Restaurant"));
		al.add(cfb.getFreebieTD("NR", "Restaurant"));
		al.add(cfb.getFreebieTD("UR", "Restaurant"));
		al.add(cfb.getFreebieTD("DU", "Restaurant"));
		al.add(cfb.getFreebieTD("TR", "Restaurant"));
		 }
		 return al;
		
	}
	public List createFreedeliveryTD(int n,List al, CreateFreedeliveryTDWithRestrictions cfd) throws IOException
	{
		 for (int i = 0; i < n; i++) {
		al.add(cfd.getFreeDevliveryTD("SFO", "Restaurant"));
		al.add(cfd.getFreeDevliveryTD("RFO", "Restaurant"));
		al.add(cfd.getFreeDevliveryTD("NR", "Restaurant"));
		al.add(cfd.getFreeDevliveryTD("UR", "Restaurant"));
		al.add(cfd.getFreeDevliveryTD("DU", "Restaurant"));
		al.add(cfd.getFreeDevliveryTD("TR", "Restaurant"));
		 }
	return al;
	
	}

	public List createLevelBasedTD(int n, List al,CreatePercentageTDWithRestrictions cr,CreateFlatTDWithRestrictions cf,CreateFreebieTDWithRestrictions cfb,CreateFreedeliveryTDWithRestrictions cfd,String level) throws IOException
	{
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
    	  
			al.add(cr.getPercentageTD("SFO", "Restaurant"));
			al.add( cr.getPercentageTD("RFO", "Restaurant"));
			al.add( cr.getPercentageTD("NR", "Restaurant"));
			al.add( cr.getPercentageTD("UR", "Restaurant"));
			al.add( cr.getPercentageTD("DU", "Restaurant"));
			al.add( cr.getPercentageTD("TR", "Restaurant"));
			al.add(cf.getFlatTD("SFO", "Restaurant"));
			al.add(cf.getFlatTD("RFO", "Restaurant"));
			al.add(cf.getFlatTD("NR", "Restaurant"));
			al.add(cf.getFlatTD("UR", "Restaurant"));
			al.add(cf.getFlatTD("DU", "Restaurant"));
			al.add(cf.getFlatTD("TR", "Restaurant"));
			al.add(cfb.getFreebieTD("SFO", "Restaurant"));
			al.add(cfb.getFreebieTD("RFO", "Restaurant"));
			al.add(cfb.getFreebieTD("NR", "Restaurant"));
			al.add(cfb.getFreebieTD("UR", "Restaurant"));
			al.add(cfb.getFreebieTD("DU", "Restaurant"));
			al.add(cfb.getFreebieTD("TR", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("SFO", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("RFO", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("NR", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("UR", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("DU", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("TR", "Restaurant"));
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	    al.add(cr.getPercentageTD("SFO", "Category"));
			al.add( cr.getPercentageTD("RFO", "Category"));
			al.add( cr.getPercentageTD("NR", "Category"));
			al.add( cr.getPercentageTD("UR", "Category"));
			al.add( cr.getPercentageTD("DU", "Category"));
			al.add( cr.getPercentageTD("TR", "Category"));
			al.add(cf.getFlatTD("SFO", "Category"));
			al.add(cf.getFlatTD("RFO", "Category"));
			al.add(cf.getFlatTD("NR", "Category"));
			al.add(cf.getFlatTD("UR", "Category"));
			al.add(cf.getFlatTD("DU", "Category"));
			al.add(cf.getFlatTD("TR", "Category"));
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	    al.add(cr.getPercentageTD("SFO", "Subcategory"));
			al.add( cr.getPercentageTD("RFO", "Subcategory"));
			al.add( cr.getPercentageTD("NR", "Subcategory"));
			al.add( cr.getPercentageTD("UR", "Subcategory"));
			al.add( cr.getPercentageTD("DU", "Subcategory"));
			al.add( cr.getPercentageTD("TR", "Subcategory"));
			al.add(cf.getFlatTD("SFO", "Subcategory"));
			al.add(cf.getFlatTD("RFO", "Subcategory"));
			al.add(cf.getFlatTD("NR", "Subcategory"));
			al.add(cf.getFlatTD("UR", "Subcategory"));
			al.add(cf.getFlatTD("DU", "Subcategory"));
			al.add(cf.getFlatTD("TR", "Subcategory"));
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	    al.add(cr.getPercentageTD("SFO", "Item"));
			al.add( cr.getPercentageTD("RFO", "Item"));
			al.add( cr.getPercentageTD("NR", "Item"));
			al.add( cr.getPercentageTD("UR", "Item"));
			al.add( cr.getPercentageTD("DU", "Item"));
			al.add( cr.getPercentageTD("TR", "Item"));
			al.add(cf.getFlatTD("SFO", "Item"));
			al.add(cf.getFlatTD("RFO", "Item"));
			al.add(cf.getFlatTD("NR", "Item"));
			al.add(cf.getFlatTD("UR", "Item"));
			al.add(cf.getFlatTD("DU", "Item"));
			al.add(cf.getFlatTD("TR", "Item"));
       }
       
       }
		
		
		
       return al;
	}

	public List createLevelBasedTD(int n, List al,CreatePercentageTDWithRestrictions cr,CreateFlatTDWithRestrictions cf,CreateFreebieTDWithRestrictions cfb,CreateFreedeliveryTDWithRestrictions cfd,String level,boolean isMutiTD) throws IOException
	{
		getObject go = new getObject();
		List<RestaurantList> restaurantList = new ArrayList<>();
		List<RestaurantList> restaurantList2 = new ArrayList<>();
		List<FreebieRestaurantList> restaurantList3 = new ArrayList<>();
		List<FreeDeliveryRestaurantList> restaurantList4 = new ArrayList<>();
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
    	   restaurantList.add(go.getRestaurantObject());
    	   restaurantList2.add(go.getRestaurantObject());
    	   restaurantList3.add(go.getFreebieRestaurantObject());
    	   restaurantList4.add(go.getFreedeliveryRestaurantObject());
			al.add(cr.getPercentageTD("SFO", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("RFO", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("NR", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("UR", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("DU", "Restaurant",restaurantList));
			al.add(cf.getFlatTD("SFO", "Restaurant",restaurantList2));
			al.add(cf.getFlatTD("RFO", "Restaurant",restaurantList2));
			al.add(cf.getFlatTD("NR", "Restaurant",restaurantList2));
			al.add(cf.getFlatTD("UR", "Restaurant",restaurantList2));
			al.add(cf.getFlatTD("DU", "Restaurant",restaurantList2));
			al.add(cfb.getFreebieTD("SFO", "Restaurant",restaurantList3));
			al.add(cfb.getFreebieTD("RFO", "Restaurant",restaurantList3));
			al.add(cfb.getFreebieTD("NR", "Restaurant",restaurantList3));
			al.add(cfb.getFreebieTD("UR", "Restaurant",restaurantList3));
			al.add(cfb.getFreebieTD("DU", "Restaurant",restaurantList3));
			al.add(cfd.getFreeDevliveryTD("SFO", "Restaurant",restaurantList4));
			al.add(cfd.getFreeDevliveryTD("RFO", "Restaurant",restaurantList4));
			al.add(cfd.getFreeDevliveryTD("NR", "Restaurant",restaurantList4));
			al.add(cfd.getFreeDevliveryTD("UR", "Restaurant",restaurantList4));
			al.add(cfd.getFreeDevliveryTD("DU", "Restaurant",restaurantList4));
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	   restaurantList.add(go.getCategoryObject());
    	   restaurantList2.add(go.getCategoryObject());
    	   
    	    al.add(cr.getPercentageTD("SFO", "Category",restaurantList));
			al.add( cr.getPercentageTD("RFO", "Category",restaurantList));
			al.add( cr.getPercentageTD("NR", "Category",restaurantList));
			al.add( cr.getPercentageTD("UR", "Category",restaurantList));
			al.add( cr.getPercentageTD("DU", "Category",restaurantList));
			al.add(cf.getFlatTD("SFO", "Category",restaurantList2));
			al.add(cf.getFlatTD("RFO", "Category",restaurantList2));
			al.add(cf.getFlatTD("NR", "Category",restaurantList2));
			al.add(cf.getFlatTD("UR", "Category",restaurantList2));
			al.add(cf.getFlatTD("DU", "Category",restaurantList2));
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	   restaurantList.add(go.getsubCategoryObject());
    	   restaurantList2.add(go.getsubCategoryObject());
    	    al.add(cr.getPercentageTD("SFO", "Subcategory",restaurantList));
			al.add( cr.getPercentageTD("RFO", "Subcategory",restaurantList));
			al.add( cr.getPercentageTD("NR", "Subcategory",restaurantList));
			al.add( cr.getPercentageTD("UR", "Subcategory",restaurantList));
			al.add( cr.getPercentageTD("DU", "Subcategory",restaurantList));
			al.add(cf.getFlatTD("SFO", "Subcategory",restaurantList2));
			al.add(cf.getFlatTD("RFO", "Subcategory",restaurantList2));
			al.add(cf.getFlatTD("NR", "Subcategory",restaurantList2));
			al.add(cf.getFlatTD("UR", "Subcategory",restaurantList2));
			al.add(cf.getFlatTD("DU", "Subcategory",restaurantList2));
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	   restaurantList.add(go.getItemObject());
    	   restaurantList2.add(go.getItemObject());
    	    al.add(cr.getPercentageTD("SFO", "Item",restaurantList));
			al.add( cr.getPercentageTD("RFO", "Item",restaurantList));
			al.add( cr.getPercentageTD("NR", "Item",restaurantList));
			al.add( cr.getPercentageTD("UR", "Item",restaurantList));
			al.add( cr.getPercentageTD("DU", "Item",restaurantList));
			al.add(cf.getFlatTD("SFO", "Item",restaurantList2));
			al.add(cf.getFlatTD("RFO", "Item",restaurantList2));
			al.add(cf.getFlatTD("NR", "Item",restaurantList2));
			al.add(cf.getFlatTD("UR", "Item",restaurantList2));
			al.add(cf.getFlatTD("DU", "Item",restaurantList2));
       }
       
       }
		
		
		
       return al;
	}

	public List createPercentageTD(int n, List al,CreatePercentageTDWithRestrictions cr,boolean isMultiTD) throws IOException
	{
		getObject go = new getObject();
       for (int i = 0; i < n; i++) {
    	List<RestaurantList> restaurantList = new ArrayList<>();
   		List<RestaurantList> restaurantList2 = new ArrayList<>();
   		List<RestaurantList> restaurantList3 = new ArrayList<>();
   		List<RestaurantList> restaurantList4 = new ArrayList<>();
   		restaurantList.add(go.getRestaurantObject());
   		restaurantList2.add(go.getCategoryObject());
   		restaurantList3.add(go.getsubCategoryObject());
   		restaurantList4.add(go.getItemObject());
   		
   		
			al.add(cr.getPercentageTD("SFO", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("RFO", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("NR", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("UR", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("DU", "Restaurant",restaurantList));
			al.add(cr.getPercentageTD("SFO", "Category",restaurantList2));
			al.add( cr.getPercentageTD("RFO", "Category",restaurantList2));
			al.add( cr.getPercentageTD("NR", "Category",restaurantList2));
			al.add( cr.getPercentageTD("UR", "Category",restaurantList2));
			al.add( cr.getPercentageTD("DU", "Category",restaurantList2));
			al.add(cr.getPercentageTD("SFO", "Subcategory",restaurantList3));
			al.add( cr.getPercentageTD("RFO", "Subcategory",restaurantList3));
			al.add( cr.getPercentageTD("NR", "Subcategory",restaurantList3));
			al.add( cr.getPercentageTD("UR", "Subcategory",restaurantList3));
			al.add( cr.getPercentageTD("DU", "Subcategory",restaurantList3));
			al.add(cr.getPercentageTD("SFO", "Item",restaurantList4));
			al.add( cr.getPercentageTD("RFO", "Item",restaurantList4));
			al.add( cr.getPercentageTD("NR", "Item",restaurantList4));
			al.add( cr.getPercentageTD("UR", "Item",restaurantList4));
			al.add( cr.getPercentageTD("DU", "Item",restaurantList4));
       }
       return al;
	}


	public List createTypeLevelBasedTD(int n, List al, CreatePercentageTDWithRestrictions cr,String level) throws IOException
	{
		
	
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
			
			al.add(cr.getPercentageTD("SFO", "Restaurant"));
			al.add( cr.getPercentageTD("RFO", "Restaurant"));
			al.add( cr.getPercentageTD("NR", "Restaurant"));
			al.add( cr.getPercentageTD("UR", "Restaurant"));
			al.add( cr.getPercentageTD("DU", "Restaurant"));
			al.add( cr.getPercentageTD("TR", "Restaurant"));
			
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	    al.add(cr.getPercentageTD("SFO", "Category"));
			al.add( cr.getPercentageTD("RFO", "Category"));
			al.add( cr.getPercentageTD("NR", "Category"));
			al.add( cr.getPercentageTD("UR", "Category"));
			al.add( cr.getPercentageTD("DU", "Category"));
			al.add( cr.getPercentageTD("TR", "Category"));
		
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	    al.add(cr.getPercentageTD("SFO", "Subcategory"));
			al.add( cr.getPercentageTD("RFO", "Subcategory"));
			al.add( cr.getPercentageTD("NR", "Subcategory"));
			al.add( cr.getPercentageTD("UR", "Subcategory"));
			al.add( cr.getPercentageTD("DU", "Subcategory"));
			al.add( cr.getPercentageTD("TR", "Subcategory"));
			
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	    al.add(cr.getPercentageTD("SFO", "Item"));
			al.add( cr.getPercentageTD("RFO", "Item"));
			al.add( cr.getPercentageTD("NR", "Item"));
			al.add( cr.getPercentageTD("UR", "Item"));
			al.add( cr.getPercentageTD("DU", "Item"));
			al.add( cr.getPercentageTD("TR", "Item"));
			
       }
       
       }
		
		
		
       return al;
	}

	

	public List createTypeLevelBasedTD(int n, List al, CreateFlatTDWithRestrictions cf,String level) throws IOException
	{
		
	
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
			
			al.add(cf.getFlatTD("SFO", "Restaurant"));
			al.add( cf.getFlatTD("RFO", "Restaurant"));
			al.add( cf.getFlatTD("NR", "Restaurant"));
			al.add( cf.getFlatTD("UR", "Restaurant"));
			al.add( cf.getFlatTD("DU", "Restaurant"));
			al.add( cf.getFlatTD("TR", "Restaurant"));
			
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	    al.add(cf.getFlatTD("SFO", "Category"));
			al.add( cf.getFlatTD("RFO", "Category"));
			al.add( cf.getFlatTD("NR", "Category"));
			al.add( cf.getFlatTD("UR", "Category"));
			al.add( cf.getFlatTD("DU", "Category"));
			al.add( cf.getFlatTD("TR", "Category"));
		
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	    al.add(cf.getFlatTD("SFO", "Subcategory"));
			al.add( cf.getFlatTD("RFO", "Subcategory"));
			al.add( cf.getFlatTD("NR", "Subcategory"));
			al.add( cf.getFlatTD("UR", "Subcategory"));
			al.add( cf.getFlatTD("DU", "Subcategory"));
			al.add( cf.getFlatTD("TR", "Subcategory"));
			
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	    al.add(cf.getFlatTD("SFO", "Item"));
			al.add( cf.getFlatTD("RFO", "Item"));
			al.add( cf.getFlatTD("NR", "Item"));
			al.add( cf.getFlatTD("UR", "Item"));
			al.add( cf.getFlatTD("DU", "Item"));
			al.add( cf.getFlatTD("TR", "Item"));
			
       }
       
       }
		
		
		
       return al;
	}


	public List createTypeLevelBasedTD(int n, List al, CreateFreebieTDWithRestrictions cfb,String level) throws IOException
	{
		
	
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
			
			al.add(cfb.getFreebieTD("SFO", "Restaurant"));
			al.add(cfb.getFreebieTD("RFO", "Restaurant"));
			al.add(cfb.getFreebieTD("NR", "Restaurant"));
			al.add(cfb.getFreebieTD("UR", "Restaurant"));
			al.add(cfb.getFreebieTD("DU", "Restaurant"));
			al.add(cfb.getFreebieTD("TR", "Restaurant"));
			
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	    al.add(cfb.getFreebieTD("SFO", "Category"));
			al.add( cfb.getFreebieTD("RFO", "Category"));
			al.add( cfb.getFreebieTD("NR", "Category"));
			al.add( cfb.getFreebieTD("UR", "Category"));
			al.add( cfb.getFreebieTD("DU", "Category"));
			al.add( cfb.getFreebieTD("TR", "Category"));
		
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	    al.add(cfb.getFreebieTD("SFO", "Subcategory"));
			al.add(cfb.getFreebieTD("RFO", "Subcategory"));
			al.add(cfb.getFreebieTD("NR", "Subcategory"));
			al.add(cfb.getFreebieTD("UR", "Subcategory"));
			al.add(cfb.getFreebieTD("DU", "Subcategory"));
			al.add(cfb.getFreebieTD("TR", "Subcategory"));
			
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	    al.add(cfb.getFreebieTD("SFO", "Item"));
			al.add(cfb.getFreebieTD("RFO", "Item"));
			al.add(cfb.getFreebieTD("NR", "Item"));
			al.add(cfb.getFreebieTD("UR", "Item"));
			al.add(cfb.getFreebieTD("DU", "Item"));
			al.add(cfb.getFreebieTD("TR", "Item"));
			
       }
       
       }
		
		
		
       return al;
	}


	public List createTypeLevelBasedTD(int n, List al, CreateFreedeliveryTDWithRestrictions cfd,String level) throws IOException
	{
		
	
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
			
			al.add(cfd.getFreeDevliveryTD("SFO", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("RFO", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("NR", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("UR", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("DU", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("TR", "Restaurant"));
			
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	    al.add(cfd.getFreeDevliveryTD("SFO", "Category"));
			al.add(cfd.getFreeDevliveryTD("RFO", "Category"));
			al.add(cfd.getFreeDevliveryTD("NR", "Category"));
			al.add(cfd.getFreeDevliveryTD("UR", "Category"));
			al.add(cfd.getFreeDevliveryTD("DU", "Category"));
			al.add(cfd.getFreeDevliveryTD("TR", "Category"));
		
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	    al.add(cfd.getFreeDevliveryTD("SFO", "Subcategory"));
			al.add(cfd.getFreeDevliveryTD("RFO", "Subcategory"));
			al.add(cfd.getFreeDevliveryTD("NR", "Subcategory"));
			al.add(cfd.getFreeDevliveryTD("UR", "Subcategory"));
			al.add(cfd.getFreeDevliveryTD("DU", "Subcategory"));
			al.add(cfd.getFreeDevliveryTD("TR", "Subcategory"));
			
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	    al.add(cfd.getFreeDevliveryTD("SFO", "Item"));
			al.add(cfd.getFreeDevliveryTD("RFO", "Item"));
			al.add(cfd.getFreeDevliveryTD("NR", "Item"));
			al.add(cfd.getFreeDevliveryTD("UR", "Item"));
			al.add(cfd.getFreeDevliveryTD("DU", "Item"));
			al.add(cfd.getFreeDevliveryTD("TR", "Item"));
			
       }
       
       }
		
		
		
       return al;
	}

	public List createFlatTD(int n,List al,CreateFlatTDWithRestrictions cf,boolean isMultiTD) throws IOException
	{
		 for (int i = 0; i < n; i++) {
			 getObject go = new getObject();
			 List<RestaurantList> restaurantList = new ArrayList<>();
		   	 List<RestaurantList> restaurantList2 = new ArrayList<>();
		   	 List<RestaurantList> restaurantList3 = new ArrayList<>();
		   	 List<RestaurantList> restaurantList4 = new ArrayList<>();
		   	    restaurantList.add(go.getRestaurantObject());
		   		restaurantList2.add(go.getCategoryObject());
		   		restaurantList3.add(go.getsubCategoryObject());
		   		restaurantList4.add(go.getItemObject());
		al.add(cf.getFlatTD("SFO", "Restaurant",restaurantList));
		al.add(cf.getFlatTD("RFO", "Restaurant",restaurantList));
		al.add(cf.getFlatTD("NR", "Restaurant",restaurantList));
		al.add(cf.getFlatTD("UR", "Restaurant",restaurantList));
		al.add(cf.getFlatTD("DU", "Restaurant",restaurantList));
		al.add(cf.getFlatTD("SFO", "Category",restaurantList2));
		al.add(cf.getFlatTD("RFO", "Category",restaurantList2));
		al.add(cf.getFlatTD("NR", "Category",restaurantList2));
		al.add(cf.getFlatTD("UR", "Category",restaurantList2));
		al.add(cf.getFlatTD("DU", "Category",restaurantList2));
		al.add(cf.getFlatTD("SFO", "Subcategory",restaurantList3));
		al.add(cf.getFlatTD("RFO", "Subcategory",restaurantList3));
		al.add(cf.getFlatTD("NR", "Subcategory",restaurantList3));
		al.add(cf.getFlatTD("UR", "Subcategory",restaurantList3));
		al.add(cf.getFlatTD("DU", "Subcategory",restaurantList3));
		al.add(cf.getFlatTD("SFO", "Item",restaurantList4));
		al.add(cf.getFlatTD("RFO", "Item",restaurantList4));
		al.add(cf.getFlatTD("NR", "Item",restaurantList4));
		al.add(cf.getFlatTD("UR", "Item",restaurantList4));
		al.add(cf.getFlatTD("DU", "Item",restaurantList4));
	    }
		 
		 return al;
	}

	public List createFreebieTD(int n,List al, CreateFreebieTDWithRestrictions cfb,boolean isMultiTD) throws IOException
	{
		 for (int i = 0; i < n; i++) {
			 getObject go = new getObject();
			 List<FreebieRestaurantList> restaurantList = new ArrayList<>();
			 restaurantList.add(go.getFreebieRestaurantObject());
		al.add(cfb.getFreebieTD("SFO", "Restaurant",restaurantList));
		al.add(cfb.getFreebieTD("RFO", "Restaurant",restaurantList));
		al.add(cfb.getFreebieTD("NR", "Restaurant",restaurantList));
		al.add(cfb.getFreebieTD("UR", "Restaurant",restaurantList));
		al.add(cfb.getFreebieTD("DU", "Restaurant",restaurantList));
		 }
		 return al;
		
	}

	public List createFreedeliveryTD(int n,List al, CreateFreedeliveryTDWithRestrictions cfd,boolean isMultiTD) throws IOException
	{
		 for (int i = 0; i < n; i++) {
			 getObject go = new getObject();
			 List<FreeDeliveryRestaurantList> restaurantList = new ArrayList<>();
			 restaurantList.add(go.getFreedeliveryRestaurantObject());
		al.add(cfd.getFreeDevliveryTD("SFO", "Restaurant",restaurantList));
		al.add(cfd.getFreeDevliveryTD("RFO", "Restaurant",restaurantList));
		al.add(cfd.getFreeDevliveryTD("NR", "Restaurant",restaurantList));
		al.add(cfd.getFreeDevliveryTD("UR", "Restaurant",restaurantList));
		al.add(cfd.getFreeDevliveryTD("DU", "Restaurant",restaurantList));
		 }
	return al;
	
	}


	public List createTypeLevelBasedTD(int n, List al, CreatePercentageTDWithRestrictions cr,String level,boolean isMutiTD) throws IOException
	{
		getObject go = new getObject();
		List<RestaurantList> restaurantList = new ArrayList<>();
   		List<RestaurantList> restaurantList2 = new ArrayList<>();
   		List<RestaurantList> restaurantList3 = new ArrayList<>();
   		List<RestaurantList> restaurantList4 = new ArrayList<>();
   		restaurantList.add(go.getRestaurantObject());
   		restaurantList2.add(go.getCategoryObject());
   		restaurantList3.add(go.getsubCategoryObject());
   		restaurantList4.add(go.getItemObject());
	
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
			
			al.add(cr.getPercentageTD("SFO", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("RFO", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("NR", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("UR", "Restaurant",restaurantList));
			al.add( cr.getPercentageTD("DU", "Restaurant",restaurantList));
			
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	    al.add(cr.getPercentageTD("SFO", "Category",restaurantList2));
			al.add( cr.getPercentageTD("RFO", "Category",restaurantList2));
			al.add( cr.getPercentageTD("NR", "Category",restaurantList2));
			al.add( cr.getPercentageTD("UR", "Category",restaurantList2));
			al.add( cr.getPercentageTD("DU", "Category",restaurantList2));
		
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	    al.add(cr.getPercentageTD("SFO", "Subcategory",restaurantList3));
			al.add( cr.getPercentageTD("RFO", "Subcategory",restaurantList3));
			al.add( cr.getPercentageTD("NR", "Subcategory",restaurantList3));
			al.add( cr.getPercentageTD("UR", "Subcategory",restaurantList3));
			al.add( cr.getPercentageTD("DU", "Subcategory",restaurantList3));
			
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	    al.add(cr.getPercentageTD("SFO", "Item",restaurantList4));
			al.add( cr.getPercentageTD("RFO", "Item",restaurantList4));
			al.add( cr.getPercentageTD("NR", "Item",restaurantList4));
			al.add( cr.getPercentageTD("UR", "Item",restaurantList4));
			al.add( cr.getPercentageTD("DU", "Item",restaurantList4));
			
       }
       
       }
		
		
		
       return al;
	}

	public List createTypeLevelBasedTD(int n, List al, CreateFlatTDWithRestrictions cf,String level,boolean isMultiTD) throws IOException
	{
		getObject go = new getObject();
		List<RestaurantList> restaurantList = new ArrayList<>();
   		List<RestaurantList> restaurantList2 = new ArrayList<>();
   		List<RestaurantList> restaurantList3 = new ArrayList<>();
   		List<RestaurantList> restaurantList4 = new ArrayList<>();
   		restaurantList.add(go.getRestaurantObject());
   		restaurantList2.add(go.getCategoryObject());
   		restaurantList3.add(go.getsubCategoryObject());
   		restaurantList4.add(go.getItemObject());
	
	
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
			
			al.add(cf.getFlatTD("SFO", "Restaurant",restaurantList));
			al.add( cf.getFlatTD("RFO", "Restaurant",restaurantList));
			al.add( cf.getFlatTD("NR", "Restaurant",restaurantList));
			al.add( cf.getFlatTD("UR", "Restaurant",restaurantList));
			al.add( cf.getFlatTD("DU", "Restaurant",restaurantList));
			
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	    al.add(cf.getFlatTD("SFO","Category",restaurantList2));
			al.add( cf.getFlatTD("RFO","Category",restaurantList2));
			al.add( cf.getFlatTD("NR", "Category",restaurantList2));
			al.add( cf.getFlatTD("UR", "Category",restaurantList2));
			al.add( cf.getFlatTD("DU", "Category",restaurantList2));
		
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	    al.add(cf.getFlatTD("SFO", "Subcategory",restaurantList3));
			al.add( cf.getFlatTD("RFO", "Subcategory",restaurantList3));
			al.add( cf.getFlatTD("NR", "Subcategory",restaurantList3));
			al.add( cf.getFlatTD("UR", "Subcategory",restaurantList3));
			al.add( cf.getFlatTD("DU", "Subcategory",restaurantList3));
			
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	    al.add(cf.getFlatTD("SFO", "Item",restaurantList4));
			al.add( cf.getFlatTD("RFO", "Item",restaurantList4));
			al.add( cf.getFlatTD("NR", "Item",restaurantList4));
			al.add( cf.getFlatTD("UR", "Item",restaurantList4));
			al.add( cf.getFlatTD("DU", "Item",restaurantList4));
			
       }
       
       }
		
		
		
       return al;
	}

	public List createTypeLevelBasedTD(int n, List al, CreateFreebieTDWithRestrictions cfb,String level,boolean isMultiTD) throws IOException
	{
		getObject go = new getObject();
		 List<FreebieRestaurantList> restaurantList = new ArrayList<>();
		 restaurantList.add(go.getFreebieRestaurantObject());
	
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
			
			al.add(cfb.getFreebieTD("SFO", "Restaurant",restaurantList));
			al.add(cfb.getFreebieTD("RFO", "Restaurant",restaurantList));
			al.add(cfb.getFreebieTD("NR", "Restaurant",restaurantList));
			al.add(cfb.getFreebieTD("UR", "Restaurant",restaurantList));
			al.add(cfb.getFreebieTD("DU", "Restaurant",restaurantList));
			
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	    al.add(cfb.getFreebieTD("SFO", "Category",restaurantList));
			al.add( cfb.getFreebieTD("RFO", "Category",restaurantList));
			al.add( cfb.getFreebieTD("NR", "Category",restaurantList));
			al.add( cfb.getFreebieTD("UR", "Category",restaurantList));
			al.add( cfb.getFreebieTD("DU", "Category",restaurantList));
		
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	    al.add(cfb.getFreebieTD("SFO", "Subcategory",restaurantList));
			al.add(cfb.getFreebieTD("RFO", "Subcategory",restaurantList));
			al.add(cfb.getFreebieTD("NR", "Subcategory",restaurantList));
			al.add(cfb.getFreebieTD("UR", "Subcategory",restaurantList));
			al.add(cfb.getFreebieTD("DU", "Subcategory",restaurantList));
			
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	    al.add(cfb.getFreebieTD("SFO", "Item",restaurantList));
			al.add(cfb.getFreebieTD("RFO", "Item",restaurantList));
			al.add(cfb.getFreebieTD("NR", "Item",restaurantList));
			al.add(cfb.getFreebieTD("UR", "Item",restaurantList));
			al.add(cfb.getFreebieTD("DU", "Item",restaurantList));
			
       }
       
       }
		
		
		
       return al;
	}

	public List createTypeLevelBasedTD(int n, List al, CreateFreedeliveryTDWithRestrictions cfd,String level,boolean isMultiTD) throws IOException
	{
		 getObject go = new getObject();
		 List<FreeDeliveryRestaurantList> restaurantList = new ArrayList<>();
		 restaurantList.add(go.getFreedeliveryRestaurantObject());
	
       switch(level)
       {
       case "Restaurant" : for (int i = 0; i < n; i++) {
			
			al.add(cfd.getFreeDevliveryTD("SFO", "Restaurant",restaurantList));
			al.add(cfd.getFreeDevliveryTD("RFO", "Restaurant",restaurantList));
			al.add(cfd.getFreeDevliveryTD("NR", "Restaurant",restaurantList));
			al.add(cfd.getFreeDevliveryTD("UR", "Restaurant",restaurantList));
			al.add(cfd.getFreeDevliveryTD("DU", "Restaurant",restaurantList));
			
       }
           break;
			
			
       case "Category" : for (int i = 0; i < n; i++) {
    	    al.add(cfd.getFreeDevliveryTD("SFO", "Category",restaurantList));
			al.add(cfd.getFreeDevliveryTD("RFO", "Category",restaurantList));
			al.add(cfd.getFreeDevliveryTD("NR", "Category",restaurantList));
			al.add(cfd.getFreeDevliveryTD("UR", "Category",restaurantList));
			al.add(cfd.getFreeDevliveryTD("DU", "Category",restaurantList));
		
       }
           break;
       
       
       case "Subcategory" :for (int i = 0; i < n; i++) {
    	    al.add(cfd.getFreeDevliveryTD("SFO", "Subcategory",restaurantList));
			al.add(cfd.getFreeDevliveryTD("RFO", "Subcategory",restaurantList));
			al.add(cfd.getFreeDevliveryTD("NR", "Subcategory",restaurantList));
			al.add(cfd.getFreeDevliveryTD("UR", "Subcategory",restaurantList));
			al.add(cfd.getFreeDevliveryTD("DU", "Subcategory",restaurantList));
			
       }
           break;
       
       case "Item" : for (int i = 0; i < n; i++) {
    	    al.add(cfd.getFreeDevliveryTD("SFO", "Item",restaurantList));
			al.add(cfd.getFreeDevliveryTD("RFO", "Item",restaurantList));
			al.add(cfd.getFreeDevliveryTD("NR", "Item",restaurantList));
			al.add(cfd.getFreeDevliveryTD("UR", "Item",restaurantList));
			al.add(cfd.getFreeDevliveryTD("DU", "Item",restaurantList));
			
       }
       
       }
		
		
		
       return al;
	}


}
