package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CafeConstant;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.Validator;
import org.testng.Assert;

import java.util.HashMap;

public class  CafeHelper {
    Initialize gameofthrones = new Initialize();
    CheckoutPricingHelper pricingHelper= new CheckoutPricingHelper();
    int count =CafeConstant.DEFAULT_COUNT;
    Validator responseValidator;

    public Processor updateCartCafe(String tid, String token, String menu_item_id,String quantity,String restId,String cart_type){
        GameOfThronesService service = new GameOfThronesService("checkout", "cafeCart", gameofthrones);
        String[] payloadparams = {menu_item_id,quantity,restId,cart_type};
        HashMap<String, String> requestheaders=createReqHeader(tid, token);
        requestheaders.put("version-code", CafeConstant.VERSION_CODE);
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor trackCafeOrder(String tid, String token,String orderID)
    {
        GameOfThronesService service = new GameOfThronesService("checkout", "trackCafeOrder", gameofthrones);
        String[] orderId= {orderID};
        Processor processor = new Processor(service, createReqHeader(tid, token), null, orderId);
        return processor;
      }

        public Processor cafeRedeem(String tid, String token, String orderID){
        GameOfThronesService service = new GameOfThronesService("checkout", "cafeRedeem", gameofthrones);
        String[] payloadparams = {orderID};
        Processor processor = new Processor(service, createReqHeader(tid, token), payloadparams);
        return processor;
    }
        
        public Processor cafePlaceOrder(String tid, String token,String paymentMethod,String orderComment,String cartType){
        GameOfThronesService service = new GameOfThronesService("checkout", "placeSpecialOrder", gameofthrones);
        String[] payloadparams = {paymentMethod,orderComment,cartType};
        Processor processor = new Processor(service, createReqHeader(tid, token), payloadparams);
        return processor;
    }
        
        public void validateCafeOrderData(String response,String expectedCafeOrderType,String expectedCafeRestType){
        	String actualOrderType = JsonPath.read(response, "$.data..order_type").toString().replace("[", "").replace("]", "").replace("\"", "");
        	Assert.assertEquals(actualOrderType, expectedCafeOrderType, "Cafe order type mismatched");
        	String actualCafeRestType = JsonPath.read(response, "$.data..cafe_data.restaurant_type").toString().replace("[", "").replace("]", "").replace("\"", "");
        	Assert.assertEquals(actualCafeRestType, expectedCafeRestType, "Rest type mismatched");
        }
        
        public void validateTrackCafeOrderData(String response){
        	String cafeTrackHeader = JsonPath.read(response, "$.data.cafe_messages.redeem_message").toString().replace("[", "").replace("]", "").replace("\"", "");
        	Assert.assertNotNull(cafeTrackHeader, "Cafe Header is missing,Not cafe order");
        	}
        
        public void validateTrackOrderMessage(String response,String expectedMessage){
        	String actualTrackMessage = JsonPath.read(response, "$.data.message").toString().replace("[", "").replace("]", "").replace("\"", "");
        	Assert.assertEquals(actualTrackMessage,expectedMessage);
        	}
        
        public HashMap<String, String> createReqHeader(String tid, String token){
            HashMap<String, String> requestheaders = new HashMap<String, String>();
            requestheaders.put("content-type", "application/json");
            requestheaders.put("Tid", tid);
            requestheaders.put("token", token);
            return requestheaders;
        }
        
        public void updateCart(String tid,String token,String menu_item_id,String quantity, String restId,String cart_type){
        	while(count >0){
        			responseValidator= updateCartCafe(tid,token, menu_item_id, quantity, restId, cart_type).ResponseValidator;
        			pricingHelper.validateSucessStatusCode(responseValidator.GetResponseCode());
        	String getStatusCode = JsonPath.read(responseValidator.GetBodyAsText(), "$.statusCode").toString().replace("[", "").replace("]", "");
        			if (!getStatusCode.equals(PricingConstants.VALID_STATUS_CODE)){
        				count--;
            			updateCart(tid,token,menu_item_id,quantity,restId,cart_type);
        			} else {
        				pricingHelper.validateApiResData(responseValidator.GetBodyAsText(),CafeConstant.CART_UPDATED_SUCCESSFULLY);
        				break;
        				}
        			break;
        			}
        		}
}