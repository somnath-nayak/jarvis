package com.swiggy.api.sf.rng.dp;

import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.SuperMultiTDHelper;
import com.swiggy.api.sf.rng.pojo.MultiTD.CreateSuperCampaignV2.CreateSuperCampaignV2;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.CreateBenefits;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.util.*;


/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.dp
 * Test Case Sheet : https://docs.google.com/spreadsheets/d/1nACo-7DvP2DefND79gjtALRGmb2gufJT1CfcpUtGxQM/edit#gid=482800156
 **/
public class NudgeDP {

    private SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();
    private RngHelper rngHelper = new RngHelper();
    private BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    public String[] discountType = {"FREE_DELIVERY","Freebie", "Flat", "Percentage", "FinalPrice", "BXGY", "Combo"};
    public String[] discountLevel = {"SuperCartRestaurant", "SuperCart", "Restaurant","Category", "Group", "Meal", "Subcategory", "Item"};
    public String[] benefitType = {"FREE_DELIVERY", "Freebie", "CASHBACK", "COUPON", "NONE"};
    public int restID;

    //public String userID = superMultiTDHelper.loginAndReturnUserId(MultiTDConstants.mobile,MultiTDConstants.password);
    public String planID;
    public HashMap<String, String> benefitID = new HashMap<>();
    List<Integer> restIDs = Arrays.asList(281,1725,1889, 217, 289, 319, 1262);
    List<Integer> restIDsNegetive = Arrays.asList(-281,-1725,-1889, -217, -289, -319, -1262);

    String superMsgOnAllOrder = "Free delight on orders above \\u20B999 only for SUPERs";
   // String mapMsg = "Free delight only for SUPERs";






    @DataProvider(name = "nudgecaseForMappedUser")
    public Iterator<Object[]> nudgecaseForMappedUser () {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String,HashMap<String,List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> data = new HashMap<>();

        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // eval.put("super", hm_s);
        // eval.put("not_super", hm);
        //1 case
        System.out.println("Case"+ (i=i+1));
        List<String> list = new ArrayList<>(Arrays.asList("P" , "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P" , "S", "S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P" , "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("Freebie")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));


        eval.put("map", data);
        //obj.add(new Object[] { cases, actionAssert, eval, valid,"RFD,SFD,SFB"});

        // 2
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("RFO","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("Freebie")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        eval.put("map", data);
        //obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 3 case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("Freebie")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 4 case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("Freebie")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 5 case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("Freebie")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 6
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("RFO","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("FREE_DELIVERY")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 7
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("CUS","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("FREE_DELIVERY")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});



        // 8
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("RFO","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("RFO","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 9
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("CUS","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("CUS","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});



        // 10
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 12
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("RFO","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 13
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("CUS","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 14
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 15
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 16
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("FREE_DELIVERY")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);

        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 17
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("RFO","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("FREE_DELIVERY")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);

        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 18
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("CUS","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("FREE_DELIVERY")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);

        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 19
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("CUS","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("FREE_DELIVERY")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);

        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});



        // 20
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P","R","R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("CUS","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("FREE_DELIVERY")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 21
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        //obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 22
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("RFO","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 23
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("CUS","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 24
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});



        // 25
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P","R","R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","R","R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P","S","S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("map", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }

    @DataProvider(name = "nudgeCaseForSuperUser")
    public Iterator<Object[]> nudgeCaseForSuperUser () {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> data = new HashMap<>();

        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // eval.put("not_super", hm);
        //1 case
        System.out.println("Case" + (i = i + 1));
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        eval.put("super", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid,"N/A"});

        // 2
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("super", data);
        //obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 3
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("super", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 5
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        eval.put("super", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 6
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList(superMsgOnAllOrder)));
        eval.put("super", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});





        return obj.iterator();
    }

    @DataProvider(name = "nudgeCaseForNonSuperUser")
    public Iterator<Object[]> nudgeCaseForNonSuperUser () {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> data = new HashMap<>();

        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // eval.put("not_super", hm);
        //1 case
        System.out.println("Case" + (i = i + 1));
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("not_super", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid,""});

        // 2
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("not_super", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});




        // 3
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("not_super", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});


        // 5
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("not_super", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 6
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        data = new HashMap<>();

        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        data.put("EVAL_CART", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("N/A")));
        data.put("EVAL_MENU", new ArrayList<>(Arrays.asList("N/A")));
        eval.put("not_super", data);
        obj.add(new Object[] { cases, actionAssert, eval, valid, ""});





        return obj.iterator();
    }



}
