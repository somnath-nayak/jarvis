package com.swiggy.api.sf.rng.tests;

import java.util.PrimitiveIterator;
import java.util.Random;

public class RandomNumber {

	    private PrimitiveIterator.OfInt randomIterator;

	    public static void main(String[] args)
	    {
	    	System.out.println(new RandomNumber(100,1000000).nextInt());
	    }
	    public RandomNumber(int min, int max) {
	        randomIterator = new Random().ints(min, max + 1).iterator();
	    }
	    public int nextInt() {
	        return randomIterator.nextInt();
	    }
	
	  
}
