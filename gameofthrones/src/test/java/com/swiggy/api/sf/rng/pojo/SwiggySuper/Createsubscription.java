package com.swiggy.api.sf.rng.pojo.SwiggySuper;

import io.advantageous.boon.json.annotations.JsonProperty;

public class Createsubscription {

	@JsonProperty("")
	private String user_id;
	@JsonProperty("")
	private String plan_id;
	@JsonProperty("")
	private String order_id;

	public Createsubscription() {

	}

	public Createsubscription(String plan_id, String user_id, String order_id) {
     
	this.plan_id= plan_id;
	this.user_id= user_id;
	this.order_id= order_id;
	}
	
	public String getPlanId() {
		return this.plan_id;
	}
	public void setplanId(String plan_id) {
		this.plan_id= plan_id;
	}
	public String getUserId()
	{
		return this.user_id;
	}	
	public void setUserId() {
		this.user_id= user_id;
	}
	public String getOrderId() {
		return this.order_id;
	}
	public void setOrderId(String order_id) {
		this.order_id= order_id;
	}
	
	@Override
	
	public String toString() {
		
		return "{"
				+ "\"user_id\"" + ":" + user_id + ","
				+ "\"plan_id\"" + ":" + plan_id + ","
				+ "\"order_id\"" + ":" + "\""+ order_id + "\""
				
				+ "}"
				;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}