package com.swiggy.api.sf.rng.tests.growthPack;

import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.dp.coupondp.RestaurantCouponVisibilityDP;
import com.swiggy.api.sf.rng.dp.growthPackDP.GrowthPackDP;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.growthPack.CreateTypePOJO;
import com.swiggy.api.sf.rng.pojo.growthPack.TypeMetaPOJO;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.HashMap;


public class GrowthPack extends DP {


    RngHelper rngHelper = new RngHelper();

    /*=============================================Growth-Pack:DashBoard-Type(Type Create)=============================================*/


    @Test(dataProvider = "createGrowthPackType", dataProviderClass = GrowthPackDP.class, description = "type creation")
    public void createGrowthPackType(HashMap<String, Object> data) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(statusCode, 1, "Status Code is Zero");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createGrowthPackTypeWithEmptyStringName", dataProviderClass = GrowthPackDP.class, description = "name as empty string")
    public void createGrowthPackTypeWithEmptyStringAsName(HashMap<String, Object> data) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCode, 0, "Status Code is 1");
        softAssert.assertEquals(statusMessage, "name is Invalid", "Empty String is not passed");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createGrowthPackTypeWithSpecialCharAsName", dataProviderClass = GrowthPackDP.class, description = "name as Special Char")
    public void createGrowthPackTypeWithSpecialCharAsName(HashMap<String, Object> data) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCode, 0, "Status Code is 1");
        softAssert.assertEquals(statusMessage, "name is Invalid", "Special Char is not passed");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createGrowthPackTypeIconNullCheck", dataProviderClass = GrowthPackDP.class, description = "Icon Null Check")
    public void createGrowthPackTypeIconNullCheck(HashMap<String, Object> data) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCode, 0, "Icon should not be null");
        softAssert.assertEquals(statusMessage, "icon is Invalid", "icon is passed as null");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createGrowthPackTypeDescriptionNullCheck", dataProviderClass = GrowthPackDP.class, description = "description Null Check")
    public void createGrowthPackTypeDescriptionNullCheck(HashMap<String, Object> data) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCode, 0, "description should not be null");
        softAssert.assertEquals(statusMessage, "description is Invalid", "description is passed as null");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createGrowthPackTypeHeaderNullCheck", dataProviderClass = GrowthPackDP.class, description = "header Null Check")
    public void createGrowthPackTypeHeaderNullCheck(HashMap<String, Object> data) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCode, 0, "header should not be null");
        softAssert.assertEquals(statusMessage, "header is Invalid", "header is passed as null");
        softAssert.assertAll();
    }

    @Test(dataProvider = "growthPackTypeDBValidation", dataProviderClass = GrowthPackDP.class, description = "growth_pack_type table check")
    public void growthPackTypeDBValidation(HashMap<String, Object> data) throws IOException, JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Boolean statusMessage = createResponse.ResponseValidator.GetNodeValueAsBool("$.statusMessage");
        String typeName = createResponse.RequestValidator.GetNodeValue("$.name");
        String createdBy = createResponse.RequestValidator.GetNodeValue("$.created_by");
        String metaIcon = createResponse.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.icon");
        String metaDescription = createResponse.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.description");
        String metaHeader = createResponse.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.header");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCode, 1, "type is not created");
        softAssert.assertNull(statusMessage, "type is not created");
        softAssert.assertEquals(typeName, rngHelper.getGrowthPackTypeName(dataValue), "type name is not present under table growth_pack_type");
        softAssert.assertEquals(createdBy, rngHelper.getGrowthPackTypeCreatedBy(dataValue), "data is missing under createdBy column");
        softAssert.assertEquals(metaIcon, rngHelper.getGrowthPackTypeMetaIcon(dataValue), "data is missing under meta icon column");
        softAssert.assertEquals(metaDescription, rngHelper.getGrowthPackTypeMetaDescription(dataValue), "data is missing under meta description column");
        softAssert.assertEquals(metaHeader, rngHelper.getGrowthPackTypeMetaHeader(dataValue), "data is missing under meta header column");
        softAssert.assertAll();
    }

    @Test(dataProvider = "growthPackTypeDBValidation", dataProviderClass = GrowthPackDP.class, description = "growth_pack_type_aud table check")
    public void growthPackTypeAudDBValidation(HashMap<String, Object> data) throws IOException, JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Boolean statusMessage = createResponse.ResponseValidator.GetNodeValueAsBool("$.statusMessage");
        String typeName = createResponse.RequestValidator.GetNodeValue("$.name");
        String createdBy = createResponse.RequestValidator.GetNodeValue("$.created_by");
        String metaIcon = createResponse.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.icon");
        String metaDescription = createResponse.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.description");
        String metaHeader = createResponse.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.header");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCode, 1, "type is not created");
        softAssert.assertNull(statusMessage, "type is not created");
        softAssert.assertEquals(typeName, rngHelper.getGrowthPackTypeAudName(dataValue), "type name is not present under table growth_pack_type_aud");
        softAssert.assertEquals(createdBy, rngHelper.getGrowthPackTypeAudCreatedBy(dataValue), "data is missing under createdBy column");
        softAssert.assertEquals(metaIcon, rngHelper.getGrowthPackTypeAudMetaIcon(dataValue), "data is missing under meta icon column");
        softAssert.assertEquals(metaDescription, rngHelper.getGrowthPackTypeAudMetaDescription(dataValue), "data is missing under meta description column");
        softAssert.assertEquals(metaHeader, rngHelper.getGrowthPackTypeAudMetaHeader(dataValue), "data is missing under meta header column");
        softAssert.assertEquals("0", rngHelper.getGrowthPackTypeAudRevType(dataValue), "revtype is not inserted with 0 since type is created");
        softAssert.assertAll();
    }

    /*=============================================Growth-Pack:DashBoard-Type(Type Update)=============================================*/


    @Test(dataProvider = "createGrowthPackType", dataProviderClass = GrowthPackDP.class, description = "type data to update")
    public void updateGrowthPackType(HashMap<String, Object> data) throws IOException,JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCodeCreateType = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCodeCreateType, 1, "Status Code is Zero");
        Processor processor = rngHelper.updateGrowthPackType(String.valueOf(dataValue),"NameUpdated"+ Utility.getRandomPostfix(),new TypeMetaPOJO().withIcon("IconValue"+Utility.getRandomPostfix()).withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader("TURBO"+Utility.getRandomPostfix()),"binit@swiggy.in");
        int statusCodeUpdateType = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(statusCodeUpdateType, 1, "type is not updated");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createGrowthPackType", dataProviderClass = GrowthPackDP.class, description = "Name null Check ")
    public void updateGrowthPackTypeNameNullCheck(HashMap<String, Object> data) throws IOException,JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCodeCreateType = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCodeCreateType, 1, "Status Code is Zero");
        Processor processor = rngHelper.updateGrowthPackType(String.valueOf(dataValue),"NameUpdated"+ Utility.getRandomPostfix(),new TypeMetaPOJO().withIcon("IconValue"+Utility.getRandomPostfix()).withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader("TURBO"+Utility.getRandomPostfix()),"binit@swiggy.in");
        int statusCodeUpdateType = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCodeUpdateType, 0, "type is not updated");
        softAssert.assertEquals(statusMessage,"name is Invalid","name field is empty");
        softAssert.assertAll();
    }
    @Test(dataProvider = "createGrowthPackType", dataProviderClass = GrowthPackDP.class, description = "Icon Data null check")
    public void updateGrowthPackTypeIconNullCheck(HashMap<String, Object> data) throws IOException,JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCodeCreateType = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCodeCreateType, 1, "Status Code is Zero");
        Processor processor = rngHelper.updateGrowthPackType(String.valueOf(dataValue),"NameUpdated"+ Utility.getRandomPostfix(),new TypeMetaPOJO().withIcon("").withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader("TURBO"+Utility.getRandomPostfix()),"binit@swiggy.in");
        int statusCodeUpdateType = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCodeUpdateType, 0, "type is not updated");
        softAssert.assertEquals(statusMessage,"icon is Invalid","icon field is empty");
        softAssert.assertAll();
    }
    @Test(dataProvider = "createGrowthPackType", dataProviderClass = GrowthPackDP.class, description = "Description Data null check")
    public void updateGrowthPackTypeDescriptionNullCheck(HashMap<String, Object> data) throws IOException,JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCodeCreateType = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCodeCreateType, 1, "Status Code is Zero");
        Processor processor = rngHelper.updateGrowthPackType(String.valueOf(dataValue),"NameUpdated"+ Utility.getRandomPostfix(),new TypeMetaPOJO().withIcon("IconValue"+Utility.getRandomPostfix()).withDescription("").withHeader("TURBO"+Utility.getRandomPostfix()),"binit@swiggy.in");
        int statusCodeUpdateType = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCodeUpdateType, 0, "type is not updated");
        softAssert.assertEquals(statusMessage,"description is Invalid","description field is empty");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createGrowthPackType", dataProviderClass = GrowthPackDP.class, description = "Header Data null check")
    public void updateGrowthPackTypeHeaderNullCheck(HashMap<String, Object> data) throws IOException,JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCodeCreateType = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCodeCreateType, 1, "Status Code is Zero");
        Processor processor = rngHelper.updateGrowthPackType(String.valueOf(dataValue),"NameUpdated"+ Utility.getRandomPostfix(),new TypeMetaPOJO().withIcon("IconValue"+Utility.getRandomPostfix()).withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader(""),"binit@swiggy.in");
        int statusCodeUpdateType = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCodeUpdateType, 0, "type is not updated");
        softAssert.assertEquals(statusMessage,"header is Invalid","header field is empty");
        softAssert.assertAll();
    }
    @Test(dataProvider = "createGrowthPackType", dataProviderClass = GrowthPackDP.class, description = "Updated by Data null check")
    public void updateGrowthPackTypeUpdatedByNullCheck(HashMap<String, Object> data) throws IOException,JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCodeCreateType = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCodeCreateType, 1, "Status Code is Zero");
        Processor processor = rngHelper.updateGrowthPackType(String.valueOf(dataValue),"NameUpdated"+ Utility.getRandomPostfix(),new TypeMetaPOJO().withIcon("IconValue"+Utility.getRandomPostfix()).withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader("TURBO"+Utility.getRandomPostfix()),"");
        int statusCodeUpdateType = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusCodeUpdateType, 0, "type is not updated");
        softAssert.assertEquals(statusMessage,"updated_by is Invalid","updated by field is empty");
        softAssert.assertAll();
    }
    @Test(dataProvider = "createGrowthPackType", dataProviderClass = GrowthPackDP.class, description = "growth_pack_type table check")
    public void updateGrowthPackTypeDBValidation(HashMap<String, Object> data) throws IOException,JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCodeCreateType = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCodeCreateType, 1, "Status Code is Zero");
        Processor processor = rngHelper.updateGrowthPackType(String.valueOf(dataValue),"NameUpdated"+ Utility.getRandomPostfix(),new TypeMetaPOJO().withIcon("IconValue"+Utility.getRandomPostfix()).withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader("TURBO"+Utility.getRandomPostfix()),"binit@swiggy.in");
        int statusCodeUpdateType = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String typeName = processor.RequestValidator.GetNodeValue("$.name");
        String updatedBy = processor.RequestValidator.GetNodeValue("$.updated_by");
        String metaIcon = processor.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.icon");
        String metaDescription = processor.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.description");
        String metaHeader = processor.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.header");
        softAssert.assertEquals(statusCodeUpdateType, 1, "type is not updated");softAssert.assertEquals(typeName, rngHelper.getGrowthPackTypeName(dataValue), "type name is not present under table growth_pack_type");
        softAssert.assertEquals(updatedBy, rngHelper.getGrowthPackTypeUpdatedBy(dataValue), "data is not updating under updatedBy column");
        softAssert.assertEquals(metaIcon, rngHelper.getGrowthPackTypeMetaIcon(dataValue), "data is not updating under meta icon column");
        softAssert.assertEquals(metaDescription, rngHelper.getGrowthPackTypeMetaDescription(dataValue), " data is not updating  under meta description column");
        softAssert.assertEquals(metaHeader, rngHelper.getGrowthPackTypeMetaHeader(dataValue), " data is not updating under meta header column");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createGrowthPackType", dataProviderClass = GrowthPackDP.class, description = "growth_pack_type_aud table check")
    public void updateGrowthPackTypeAudDBValidation(HashMap<String, Object> data) throws IOException,JSONException {
        SoftAssert softAssert = new SoftAssert();
        CreateTypePOJO payload = (CreateTypePOJO) data.get("createTypePOJO");
        Processor createResponse = rngHelper.createGrowthPackType(payload);
        int statusCodeCreateType = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int dataValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCodeCreateType, 1, "Status Code is Zero");
        Processor processor = rngHelper.updateGrowthPackType(String.valueOf(dataValue),"NameUpdated"+ Utility.getRandomPostfix(),new TypeMetaPOJO().withIcon("IconValue"+Utility.getRandomPostfix()).withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader("TURBO"+Utility.getRandomPostfix()),"binit@swiggy.in");
        int statusCodeUpdateType = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String typeName = processor.RequestValidator.GetNodeValue("$.name");
        String updatedBy = processor.RequestValidator.GetNodeValue("$.updated_by");
        String metaIcon = processor.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.icon");
        String metaDescription = processor.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.description");
        String metaHeader = processor.RequestValidator.GetNodeValueAsStringFromJsonArray("$.meta.header");
        softAssert.assertEquals(statusCodeUpdateType, 1, "type is not updated");softAssert.assertEquals(typeName, rngHelper.getGrowthPackTypeName(dataValue), "type name is not present under table growth_pack_type");
        softAssert.assertEquals(typeName, rngHelper.getGrowthPackTypeAudNameUpdated(dataValue), "type name is not present under table growth_pack_type_aud");
        softAssert.assertEquals(updatedBy, rngHelper.getGrowthPackTypeAudUpdatedByUpdated(dataValue), "data is missing under updatedBy column");
        softAssert.assertEquals(metaIcon, rngHelper.getGrowthPackTypeAudMetaIconUpdated(dataValue), "data is missing under meta icon column");
        softAssert.assertEquals(metaDescription, rngHelper.getGrowthPackTypeAudMetaDescriptionUpdated(dataValue), "data is missing under meta description column");
        softAssert.assertEquals(metaHeader, rngHelper.getGrowthPackTypeAudMetaHeaderUpdated(dataValue), "data is missing under meta header column");
        softAssert.assertEquals("1", rngHelper.getGrowthPackTypeAudRevTypeUpdated(dataValue), "revtype is not inserted with 1 since type is updated");
        softAssert.assertAll();
    }

}
