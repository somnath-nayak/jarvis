package com.swiggy.api.sf.rng.pojo.freedelivery;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "active",
        "addon_description",
        "addon_free_limit",
        "addon_limit",
        "category_id",
        "created_by",
        "description",
        "eligible_for_long_distance",
        "enabled",
        "in_stock",
        "is_discoverable",
        "is_perishable",
        "is_spicy",
        "is_veg",
        "name",
        "order",
        "packing_charges",
        "packing_slab_count",
        "preparation_style",
        "price",
        "recommended",
        "restaurant_id",
        "serves_how_many",
        "service_charges",
        "service_tax",
        "sub_category_id",
        "third_party_id",
        "type",
        "updated_at",
        "updated_by",
        "updated_on",
        "variant_description",
        "vat",
        "image_id",
        "gst_details"
})

public class Coupon {
}
