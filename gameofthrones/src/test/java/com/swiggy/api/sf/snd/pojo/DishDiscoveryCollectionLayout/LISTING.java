package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class LISTING {

    private MENULET mENULET;
    private HORIZONTALLIST hORIZONTALLIST;
    private VERTICALGRID vERTICALGRID;

    /**
     * No args constructor for use in serialization
     *
     */
    public LISTING() {
    }

    /**
     *
     * @param hORIZONTALLIST
     * @param vERTICALGRID
     * @param mENULET
     */
    public LISTING(MENULET mENULET, HORIZONTALLIST hORIZONTALLIST, VERTICALGRID vERTICALGRID) {
        super();
        this.mENULET = mENULET;
        this.hORIZONTALLIST = hORIZONTALLIST;
        this.vERTICALGRID = vERTICALGRID;
    }

    public MENULET getMENULET() {
        return mENULET;
    }

    public void setMENULET(MENULET mENULET) {
        this.mENULET = mENULET;
    }

    public HORIZONTALLIST getHORIZONTALLIST() {
        return hORIZONTALLIST;
    }

    public void setHORIZONTALLIST(HORIZONTALLIST hORIZONTALLIST) {
        this.hORIZONTALLIST = hORIZONTALLIST;
    }

    public VERTICALGRID getVERTICALGRID() {
        return vERTICALGRID;
    }

    public void setVERTICALGRID(VERTICALGRID vERTICALGRID) {
        this.vERTICALGRID = vERTICALGRID;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mENULET", mENULET).append("hORIZONTALLIST", hORIZONTALLIST).append("vERTICALGRID", vERTICALGRID).toString();
    }

}