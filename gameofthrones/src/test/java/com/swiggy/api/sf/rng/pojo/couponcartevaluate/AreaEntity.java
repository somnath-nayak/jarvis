package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class AreaEntity {
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}
	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}
	public AreaEntity withId(Integer id) {
		this.id = id;
		return this;
	}

}
