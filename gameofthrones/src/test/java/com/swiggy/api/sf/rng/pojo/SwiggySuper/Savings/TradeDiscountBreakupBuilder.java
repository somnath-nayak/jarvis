package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;

public class TradeDiscountBreakupBuilder {



    public TradeDiscountBreakup tradeDiscountBreakup;
     public TradeDiscountBreakupBuilder() {
        tradeDiscountBreakup = new TradeDiscountBreakup();
    }



    public SeedingInfoBuilder seedingInfo;

    public TradeDiscountBreakupBuilder withCampaignId(Integer campaignId) {
        tradeDiscountBreakup.setCampaignId( campaignId);
        return this;
    }

    public TradeDiscountBreakupBuilder withRewardType(String rewardType) {
        tradeDiscountBreakup.setRewardType(rewardType);
        return this;
    }

    public TradeDiscountBreakupBuilder withDiscountAmount(Integer discountAmount) {
        tradeDiscountBreakup.setDiscountAmount(discountAmount);
        return this;
    }

    public TradeDiscountBreakupBuilder withIsSuper(Boolean isSuper) {
        tradeDiscountBreakup.setIsSuper(isSuper);
        return this;
    }

    public TradeDiscountBreakupBuilder withDiscountBreakup(Object discountBreakup) {
        tradeDiscountBreakup.setDiscountBreakup(discountBreakup);
        return this;
    }

    public TradeDiscountBreakupBuilder withSeedingInfo(SeedingInfo seedingInfo) {
        tradeDiscountBreakup.setSeedingInfo(seedingInfo);
        return this;
    }

    public TradeDiscountBreakup build() {
        getDefaultValue();
        return tradeDiscountBreakup;
    }

    private void getDefaultValue() {
        if (tradeDiscountBreakup.getCampaignId() == null) {
            tradeDiscountBreakup.setCampaignId(9);
        }
        if (tradeDiscountBreakup.getRewardType() == null) {
            tradeDiscountBreakup.setRewardType("Flat");
        }
        if (tradeDiscountBreakup.getDiscountAmount() == null) {
            tradeDiscountBreakup.setDiscountAmount(50);
        }
        if (tradeDiscountBreakup.getIsSuper() == null) {
            tradeDiscountBreakup.setIsSuper(Boolean.FALSE);
        }
        if (tradeDiscountBreakup.getDiscountBreakup() == null) {
            tradeDiscountBreakup.setDiscountBreakup(new TDBreakupBuilder().build());
        }
        if (seedingInfo == null) {
            tradeDiscountBreakup.setSeedingInfo(new SeedingInfoBuilder().build());
        }
    }



}
