package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import com.swiggy.api.sf.checkout.pojo.AddressPOJO;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class  AddressHelper {
   // Initialize gameofthrones = new Initialize();
   Initialize gameofthrones =Initializer.getInitializer();
    CheckoutHelper helper= new CheckoutHelper();
    JsonHelper jsonHelper= new JsonHelper();

    public Processor NewAddress(String tid, String token, String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation)
    {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Tid", tid);
        requestheaders.put("token", token);
        GameOfThronesService service = new GameOfThronesService("checkout", "newaddress", gameofthrones);
        String[] payloadparams = {name, mobile,address, landmark,area, lat, lng, flat_no,city,annotation};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor GetAllAdress(String tid, String token)
    {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Tid", tid);
        requestheaders.put("token", token);
        GameOfThronesService service = new GameOfThronesService("checkout", "getalladdress", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null, null);
        return processor;
    }

    @Test
    public void clearAddress()
    {
        CheckoutHelper c= new CheckoutHelper();
        HashMap<String,String> h=c.TokenData("7406734416", "rkonowhere");
        String resp= GetAllAdress(h.get("Tid"), h.get("Token")).ResponseValidator.GetBodyAsText();
        String deli= JsonPath.read(resp, "$.data.addresses..delivery_valid").toString().replace("[","").replace("]","");
        String[] del= deli.split(",");
        String de= JsonPath.read(resp, "$.data.addresses..id").toString().replace("[","").replace("]","");
        String[] d= de.split(",");
        for(int i=0; i<del.length; i++)
        {
            if(del[i].equalsIgnoreCase("0"))
            {
                DeleteAddress(h.get("Tid"), h.get("Token"),d[i] ).ResponseValidator.GetBodyAsText();
            }
        }

    }

    public Processor DeleteAddress(String tid, String token, String addressId)
    {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Tid", tid);
        requestheaders.put("token", token);
        GameOfThronesService service = new GameOfThronesService("checkout", "deleteaddress", gameofthrones);
        String[] payloadparams = {addressId};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor UpdateAddress(String tid, String token, String addressId, String name, String mobile, String address, String landmark1, String area, String lat, String lng, String flat_no, String city, String annotation)
    {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Tid", tid);
        requestheaders.put("token", token);
        GameOfThronesService service = new GameOfThronesService("checkout", "updateaddress", gameofthrones);
        String[] payloadparams = {addressId,name,mobile,address, landmark1,area, lat,lng,flat_no,city,annotation};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor Addresservicable(String tid, String token, String lat, String lng) {
        HashMap<String, String> requestheaders = CheckoutHelper.requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkout", "addressservicable", gameofthrones);
        String[] latlng = {lat, lng};
        Processor processor = new Processor(service, requestheaders, null, latlng);
        return processor;
    }


    public String deliveryValid(String tid, String token)
    {
        String response = GetAllAdress(tid, token).ResponseValidator.GetBodyAsText();
        String AddressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String[] AddressIdArray = (AddressId.split(","));
        String deliveryValid = JsonPath.read(response, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]", "");
        String[] delivery = (deliveryValid.split(","));
        //List<String> latitudeList= new ArrayList<>();
        List<String> addressIdList = new ArrayList<>();
        for (int i = 0; i < delivery.length; i++) {
            if (delivery[i].equalsIgnoreCase("1"))
            { addressIdList.add(AddressIdArray[i]);
                }
            }
        return addressIdList.get(0);
    }

    public String createCartAddress(CreateMenuEntry payload) throws IOException
    {
        String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap = helper.createLogin(payload.getpassword(), payload.getmobile());
        String response = helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String addressResponse= NewAddress(hashMap.get("Tid"), hashMap.get("Token"),payload.getName(),payload.getMobile1(),payload.getAddress(), payload.getLandmark(),payload.getArea(),payload.getLat(),payload.getLng(),payload.getflat_no(),payload.getCity(),payload.getAnnotation()).ResponseValidator.GetBodyAsText();
        String AddressId= JsonPath.read(addressResponse, "$.data..address_id").toString().replace("[", "").replace("]","");
        return AddressId;
    }

    public Processor addNewAddress(String tid, String token, AddressPOJO addressPayload)
    {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Tid", tid);
        requestheaders.put("token", token);
        GameOfThronesService service = new GameOfThronesService("checkout", "newaddressPOJO", gameofthrones);
		try {
			return new Processor(service, requestheaders, new String[] {jsonHelper.getObjectToJSON(addressPayload)});
		} catch (IOException e) {
			e.printStackTrace();
		}
        return null;
    }

    public Processor GetAllAdress(HashMap<String, String> requestheaders)
    {

        GameOfThronesService service = new GameOfThronesService("checkout", "getalladdress", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null, null);
        return processor;
    }

    public Processor NewAddress(HashMap<String, String> requestheaders, String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation)
    {

        GameOfThronesService service = new GameOfThronesService("checkout", "newaddress", gameofthrones);
        String[] payloadparams = {name, mobile,address, landmark,area, lat, lng, flat_no,city,annotation};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }


    public Processor Addresservicable(HashMap<String, String> requestheaders, String lat, String lng)
    {
        GameOfThronesService service = new GameOfThronesService("checkout", "addressservicable", gameofthrones);
        return new Processor(service, requestheaders, null, new String[]{lat,lng});
    }

    public Processor DeleteAddress(HashMap<String, String> requestheaders, String addressId)
    {

        GameOfThronesService service = new GameOfThronesService("checkout", "deleteaddress", gameofthrones);
        String[] payloadparams = {addressId};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor UpdateAddress(HashMap<String, String> requestheaders, String addressId, String name, String mobile, String address, String landmark1, String area, String lat, String lng, String flat_no, String city, String annotation)
    {

        GameOfThronesService service = new GameOfThronesService("checkout", "updateaddress", gameofthrones);
        String[] payloadparams = {addressId,name,mobile,address, landmark1,area, lat,lng,flat_no,city,annotation};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public static HashMap<String, String> setCompleteHeader(String tid, String token,
                                                     String userAgent,String versionCode) {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Authorization", CheckoutConstants.authorization);
        requestheaders.put("tid", tid);
        requestheaders.put("token", token);
        requestheaders.put("User-Agent", userAgent);
        requestheaders.put("Version-Code", versionCode);
        return requestheaders;
    }

    public Processor newAddress(String tid, String token,String payload) {
        HashMap<String, String> requestheaders =  CheckoutHelper.requestHeader(tid,token);
        GameOfThronesService service = new GameOfThronesService("checkout", "newaddress", gameofthrones);
        String[] payloadparams = {payload};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor newAddress(HashMap<String, String> requestheaders,String payload) {
        GameOfThronesService service = new GameOfThronesService("checkout", "newaddressPOJO", gameofthrones);
        String[] payloadparams = {payload};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor getAllAddress(HashMap<String, String> requestheaders) {
        GameOfThronesService service = new GameOfThronesService("checkout", "getalladdress", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null, null);
        return processor;
    }

    public Processor deleteAddress(HashMap<String, String> requestHeaders, String addressId) {
        GameOfThronesService service = new GameOfThronesService("checkout", "deleteaddress", gameofthrones);
        String[] payloadParams = {addressId};
        Processor processor = new Processor(service, requestHeaders, payloadParams);
        return processor;
    }

    public static void validateApiResStatusData(String response) {
        String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getStatusCode, PricingConstants.VALID_STATUS_CODE,"HTTPS response is not 0");
        String isSuccessful = JsonPath.read(response, "$.successful").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(isSuccessful,CheckoutConstants.IS_SUCCESSFUL_FLAG,"Successful flag is false");
    }
}
