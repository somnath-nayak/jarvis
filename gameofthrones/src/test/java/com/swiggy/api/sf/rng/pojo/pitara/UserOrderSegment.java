
package com.swiggy.api.sf.rng.pojo.pitara;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "orderCount",
    "lastOrderCityId",
    "firstOrderCityId",
    "lastOrderTime"
})
public class UserOrderSegment {

    @JsonProperty("orderCount")
    private Integer orderCount;
    @JsonProperty("lastOrderCityId")
    private Integer lastOrderCityId;
    @JsonProperty("firstOrderCityId")
    private Integer firstOrderCityId;
    @JsonProperty("lastOrderTime")
    private Integer lastOrderTime;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UserOrderSegment() {
    }

    /**
     * 
     * @param lastOrderTime
     * @param lastOrderCityId
     * @param firstOrderCityId
     * @param orderCount
     */
    public UserOrderSegment(Integer orderCount, Integer lastOrderCityId, Integer firstOrderCityId, Integer lastOrderTime) {
        super();
        this.orderCount = orderCount;
        this.lastOrderCityId = lastOrderCityId;
        this.firstOrderCityId = firstOrderCityId;
        this.lastOrderTime = lastOrderTime;
    }

    @JsonProperty("orderCount")
    public Integer getOrderCount() {
        return orderCount;
    }

    @JsonProperty("orderCount")
    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public UserOrderSegment withOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
        return this;
    }

    @JsonProperty("lastOrderCityId")
    public Integer getLastOrderCityId() {
        return lastOrderCityId;
    }

    @JsonProperty("lastOrderCityId")
    public void setLastOrderCityId(Integer lastOrderCityId) {
        this.lastOrderCityId = lastOrderCityId;
    }

    public UserOrderSegment withLastOrderCityId(Integer lastOrderCityId) {
        this.lastOrderCityId = lastOrderCityId;
        return this;
    }

    @JsonProperty("firstOrderCityId")
    public Integer getFirstOrderCityId() {
        return firstOrderCityId;
    }

    @JsonProperty("firstOrderCityId")
    public void setFirstOrderCityId(Integer firstOrderCityId) {
        this.firstOrderCityId = firstOrderCityId;
    }

    public UserOrderSegment withFirstOrderCityId(Integer firstOrderCityId) {
        this.firstOrderCityId = firstOrderCityId;
        return this;
    }

    @JsonProperty("lastOrderTime")
    public Integer getLastOrderTime() {
        return lastOrderTime;
    }

    @JsonProperty("lastOrderTime")
    public void setLastOrderTime(Integer lastOrderTime) {
        this.lastOrderTime = lastOrderTime;
    }

    public UserOrderSegment withLastOrderTime(Integer lastOrderTime) {
        this.lastOrderTime = lastOrderTime;
        return this;
    }

}
