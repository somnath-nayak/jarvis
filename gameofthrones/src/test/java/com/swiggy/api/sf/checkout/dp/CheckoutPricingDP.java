package com.swiggy.api.sf.checkout.dp;


import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.*;
import com.swiggy.api.sf.rng.helper.RngHelper;

import java.io.IOException;
import java.util.*;

import com.swiggy.api.sf.rng.pojo.*;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import org.testng.annotations.Test;


public class CheckoutPricingDP {
    CheckoutHelper checkoutHelper= new CheckoutHelper();

    RngHelper rngHelper = new RngHelper();

    static int order_id = -1;

    @BeforeClass
    public void GenertaeRandomOrderID() {
        UUID orderid = UUID.randomUUID();
        order_id = Math.abs(orderid.hashCode());

    }
    
    @DataProvider(name = "thresholdFee")
    public static Object[][] thresholdFee() {
        return new Object[][]{
               //Bangalore
     	       {"1","12.9279","77.6271","0000","2300","11.0","0","0","5000","12.0"},
     	       {"1","12.9279","77.6271","0000","2300","13.0","0","0","5000","14.0"},
     	       {"1","12.9279","77.6271","0000","2300","0.0","15.0","0","5000","16.0"},
     	       {"1","12.9279","77.6271","0000","2300","17.0","18.0","0","5000","19.0"},
     	       {"1","12.9279","77.6271","0000","2300","0.0","20.0","0","5000","0.0"},
               //Delhi
               {"1","28.55437","77.194","0000","2300","0.0","0","0","1000","49.0"},
               //Mumbai
               {"1","19.0592","72.8295","0000","2300","0.0","0","0","1000","49.0"},
        };
    }
 
    @DataProvider(name = "thresholdFeeCity")
    public static Object[][] thresholdFeeCity() {
        return new Object[][]{
        	//Bangalore restaurants
                {"1","12.9279", "77.6271","0000","2300","1","0.0","0","0","1000","33.0"},
                {"1","12.9279", "77.6271","0000","2300","1","37.0","0","0","1000","33.0"},
                {"1","28.55437","77.194","0000","2300","1","0.0","31.0","0","1000","34.0"},
                {"1","19.0592","72.8295","0000","2300","1","35.0","0","0","1000","0.0"},
                {"1","19.0592","72.8295","0000","2300","1","35.0","0","0","1000","36.0"},
           //Mumbai restaurants
                {"1","19.1094923926", "72.85317819960005","0000","2300","5","0.0","0","0","1000","33.0"},
                {"1","19.1094923926", "72.85317819960005","0000","2300","5","37.0","0","0","1000","33.0"},
                {"1","19.1094923926","72.85317819960005","0000","2300","5","0.0","31.0","0","1000","34.0"},
                {"1","19.1094923926","72.85317819960005","0000","2300","5","35.0","0","0","1000","0.0"},
                {"1","19.1094923926","72.85317819960005","0000","2300","5","35.0","0","0","1000","36.0"},
           //Delhi restaurants
                {"1","28.55437", "77.19436700000006","0000","2300","4","0.0","0","0","1000","33.0"},
                {"1","28.55437", "77.19436700000006","0000","2300","4","37.0","0","0","1000","33.0"},
                {"1","28.55437","77.19436700000006","0000","2300","4","0.0","31.0","0","1000","34.0"},
                {"1","28.55437","77.19436700000006","0000","2300","4","35.0","0","0","1000","0.0"},
                {"1","28.55437","77.19436700000006","0000","2300","4","35.0","0","0","1000","36.0"},
           
        };
    }
  
    @DataProvider(name = "distanceFee")
    public static Object[][] distanceFee() {
        return new Object[][]{
                {"1","12.9279", "77.6271","0000","2359","0","0","1000","41.0"},
                {"1","12.9279", "77.6271","0000","2359","42.0","0","1000","43.0"},
                {"1","12.9279", "77.6271","0000","2359","0","0","1000","0.0"}
        };
    }
    
    @DataProvider(name = "distanceFeeCity")
    public static Object[][] distanceFeeCity() {
        return new Object[][]{
        	//Bangalore city
                {"1","12.9279", "77.6271","1","0000","2359","0","0","1000","51.0"},
                {"1","12.9279", "77.6271","1","0000","2359","52.0","0","1000","53.0"},
                {"1","12.9279", "77.6271","1","0000","2359","0","0","1000","0.0"},
            //Mumbai city
                {"1","19.1094923926", "72.85317819960005","5","0000","2359","0","0","1000","51.0"},
                {"1","19.1094923926", "72.85317819960005","5","0000","2359","52.0","0","1000","53.0"},
                {"1","19.1094923926", "72.85317819960005","5","0000","2359","0","0","1000","0.0"},
            //Delhi city
                {"1","28.55437","77.19436700000006","4","0000","2359","0","0","1000","51.0"},
                {"1","28.55437","77.19436700000006","4","0000","2359","52.0","0","1000","53.0"},
                {"1","28.55437","77.19436700000006","4","0000","2359","0","0","1000","0.0"},
        };
    }
   
    @DataProvider(name = "timeFee")
    public static Object[][] timeFee() {
        return new Object[][]{
                {"1","12.9279","77.6271","0000","2359","61.0"},
                {"1","12.9279","77.6271","0000","2359","0.0"}
        };
    }
    
    @DataProvider(name = "timeFeeCity")
    public static Object[][] timeFeeCity() {
        return new Object[][]{
        	//Bangalore
                {"1","12.9279","77.6271","1","0000","2359","71.0"},
                {"1","12.9279","77.6271","1","0000","2359","0.0"},
            //Mumbai
                {"1","19.1094923926","72.85317819960005","5","0000","2359","71.0"},
                {"1","19.1094923926","72.85317819960005","5","0000","2359","0.0"},
            //Delhi
                {"1","28.55437","77.19436700000006","4","0000","2359","71.0"},
                {"1","28.55437","77.19436700000006","4","0000","2359","0.0"},
        };
    }
    
    @DataProvider(name = "specialFeeCity")
    public static Object[][] specialFeeCity() {
        return new Object[][]{
        	//Bangalore
                {"1","12.9279","77.6271","1","false","55.0","1","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","12.9279","77.6271","1","false","55.0","1","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","12.9279","77.6271","1","true","55.0","1","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","12.9279","77.6271","1","true","55.0","1","2018-03-01 00:00","2018-12-31 12:12"},
            //Mumbai
                {"1","19.1094923926","72.85317819960005","1","false","55.0","5","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","19.1094923926","72.85317819960005","1","false","55.0","5","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","19.1094923926","72.85317819960005","1","true","55.0","5","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","19.1094923926","72.85317819960005","1","true","55.0","5","2018-03-01 00:00","2018-12-31 12:12"},
            //Delhi
                {"1","28.55437","77.19436700000006","1","false","55.0","4","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","28.55437","77.19436700000006","1","false","55.0","4","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","28.55437","77.19436700000006","1","true","55.0","4","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","28.55437","77.19436700000006","1","true","55.0","4","2018-03-01 00:00","2018-12-31 12:12"}
        };
    }
    
    @DataProvider(name = "specialFee")
    public static Object[][] specialFee() {
        return new Object[][]{
                {"1","12.9279","77.6271","false","56.0","1","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","12.9279","77.6271","false","0.0","1","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","12.9279","77.6271","true","56.0","1","2018-03-01 00:00","2018-12-31 12:12"},
                {"1","12.9279","77.6271","true","0.0","1","2018-03-01 00:00","2018-12-31 12:12"}
                };
    }
   
    @DataProvider(name = "updatePricingMessage")
    public static Object[][] updatePricingMessage() {
        return new Object[][]{
                {"12.9279", "77.6271","Update###Automation###Test###message"}
                };
    }
    
    @DataProvider(name = "fetchPricing")
    public static Object[][] fetchPricing() {
        return new Object[][]{
                {"12.9279","77.6271","1","5","0"}
                };
    }
}


