package com.swiggy.api.sf.rng.pojo.TDTemplate;


import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "templateName",
        "name",
        "header",
        "description",
        "shortDescription",
        "validFrom",
        "validTill",
        "campaignType",
        "timeSpanType",
        "timeSpanLength",
        "rewardValue",
        "createdBy",
        "firstOrder",
        "minCartAmount",
        "discountCap",
        "userRestriction",
        "timeSlotRestriction",
        "commissionOnFullBill",
        "taxesOnDiscountedBill",
        "dormantUserType",
        "restaurantFirstOrder"
})


public class TemplateCreation {

    @JsonProperty("templateName")
    private String templateName;
    @JsonProperty("name")
    private String name;
    @JsonProperty("header")
    private String header;
    @JsonProperty("description")
    private String description;
    @JsonProperty("shortDescription")
    private String shortDescription;
    @JsonProperty("validFrom")
    private Long validFrom;
    @JsonProperty("validTill")
    private Long validTill;
    @JsonProperty("campaignType")
    private String campaignType;
    @JsonProperty("timeSpanType")
    private String timeSpanType;
    @JsonProperty("timeSpanLength")
    private Integer timeSpanLength;
    @JsonProperty("rewardValue")
    private String rewardValue;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("firstOrder")
    private Boolean firstOrder;
    @JsonProperty("minCartAmount")
    private Integer minCartAmount;
    @JsonProperty("discountCap")
    private Integer discountCap;
    @JsonProperty("userRestriction")
    private Boolean userRestriction;
    @JsonProperty("timeSlotRestriction")
    private Boolean timeSlotRestriction;
    @JsonProperty("commissionOnFullBill")
    private Boolean commissionOnFullBill;
    @JsonProperty("taxesOnDiscountedBill")
    private Boolean taxesOnDiscountedBill;

    @JsonProperty("restaurantFirstOrder")
    private Boolean restaurantFirstOrder;

    @JsonProperty("templateName")
    public String getTemplateName() {
        return templateName;
    }

    @JsonProperty("templateName")
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }


    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }


    @JsonProperty("header")
    public String getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(String header) {
        this.header = header;
    }


    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }


    @JsonProperty("shortDescription")
    public String getShortDescription() {
        return shortDescription;
    }

    @JsonProperty("shortDescription")
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }


    @JsonProperty("validFrom")
    public Long getValidFrom() {
        return validFrom;
    }

    @JsonProperty("validFrom")
    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }


    @JsonProperty("validTill")
    public Long getValidTill() {
        return validTill;
    }

    @JsonProperty("validTill")
    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }


    @JsonProperty("campaignType")
    public String getCampaignType() {
        return campaignType;
    }

    @JsonProperty("campaignType")
    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }


    @JsonProperty("timeSpanType")
    public String getTimeSpanType() {
        return timeSpanType;
    }

    @JsonProperty("timeSpanType")
    public void setTimeSpanType(String timeSpanType) {
        this.timeSpanType = timeSpanType;
    }


    @JsonProperty("timeSpanLength")
    public Integer getTimeSpanLength() {
        return timeSpanLength;
    }

    @JsonProperty("timeSpanLength")
    public void setTimeSpanLength(Integer timeSpanLength) {
        this.timeSpanLength = timeSpanLength;
    }


    @JsonProperty("rewardValue")
    public String getRewardValue() {
        return rewardValue;
    }

    @JsonProperty("rewardValue")
    public void setRewardValue(String rewardValue) {
        this.rewardValue = rewardValue;
    }


    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    @JsonProperty("firstOrder")
    public Boolean getIsFirstOrder() {
        return firstOrder;
    }

    @JsonProperty("firstOrder")
    public void setIsFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
    }


    @JsonProperty("restaurantFirstOrder")
    public Boolean getIsRestaurantFirstOrder() {
        return restaurantFirstOrder;
    }

    @JsonProperty("restaurantFirstOrder")
    public void setIsRestaurantFirstOrder(Boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
    }



    @JsonProperty("minCartAmount")
    public Integer getMinCartAmount() {
        return minCartAmount;
    }

    @JsonProperty("minCartAmount")
    public void setMinCartAmount(Integer minCartAmount) {
        this.minCartAmount = minCartAmount;
    }


    @JsonProperty("discountCap")
    public Integer getDiscountCap() {
        return discountCap;
    }

    @JsonProperty("discountCap")
    public void setDiscountCap(Integer discountCap) {
        this.discountCap = discountCap;
    }


    @JsonProperty("userRestriction")
    public Boolean getUserRestriction() {
        return userRestriction;
    }

    @JsonProperty("userRestriction")
    public void setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
    }


    @JsonProperty("timeSlotRestriction")
    public Boolean getTimeSlotRestriction() {
        return timeSlotRestriction;
    }

    @JsonProperty("timeSlotRestriction")
    public void setTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
    }


    @JsonProperty("commissionOnFullBill")
    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    @JsonProperty("commissionOnFullBill")
    public void setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
    }


    @JsonProperty("taxesOnDiscountedBill")
    public Boolean getTaxesOnDiscountedBill() {
        return taxesOnDiscountedBill;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public void setTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
    }



    @JsonProperty("dormantUserType")
    private String dormantUserType;

    @JsonProperty("dormantUserType")
    public String getDormantUserType() {
        return dormantUserType;
    }

    @JsonProperty("dormantUserType")
    public void setDormantUserType(String dormantUserType) {
        this.dormantUserType = dormantUserType;
    }


}



