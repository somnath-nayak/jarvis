package com.swiggy.api.sf.snd.pojo.event;

import com.squareup.moshi.Json;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class Variation_ {

    @JsonProperty("group_id")
    private String groupId;
    @JsonProperty("variation_id")
    private String variationId;
    @JsonProperty("third_party_groupId")
    private String thirdPartyGroupId;
    @JsonProperty("third_party_variation_id")
    private String thirdPartyVariationId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getVariationId() {
        return variationId;
    }

    public void setVariationId(String variationId) {
        this.variationId = variationId;
    }

    public String getThirdPartyGroupId() {
        return thirdPartyGroupId;
    }

    public void setThirdPartyGroupId(String thirdPartyGroupId) {
        this.thirdPartyGroupId = thirdPartyGroupId;
    }

    public String getThirdPartyVariationId() {
        return thirdPartyVariationId;
    }

    public void setThirdPartyVariationId(String thirdPartyVariationId) {
        this.thirdPartyVariationId = thirdPartyVariationId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("group_id", groupId).append("variation_id", variationId).append("third_party_groupId", thirdPartyGroupId).append("third_party_variation_id", thirdPartyVariationId).toString();
    }

}
