package com.swiggy.api.sf.snd.tests;

import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class RestAvailability {

    SANDHelper helper=new SANDHelper();
    RedisHelper redisHelper=new RedisHelper();

    @Test
    public void restAndItemAvailability() {
        System.out.println("Starting");
        //String lat=System.getenv("lat");
        //String lng= System.getenv("lng");
        String response = helper.aggregator(new String[]{"12.9326", "77.6036"}).ResponseValidator.GetBodyAsText();
        List<String> restIds = Arrays.asList(helper.JsonString(response, "$.data.restaurants[*].id").split(","));
        System.out.println(restIds);
        //SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
        for (String restid : restIds) {
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
            //String restid="30827";
            //AV Menu
            String menu_query = "select menu_id from restaurant_menu_map where restaurant_id='" + restid + "';";
            System.out.println(menu_query);
            List<Map<String, Object>> lists = sqlTemplate.queryForList(menu_query);
            for (int i = 0; i < lists.size(); i++) {
                String menu_id = lists.get(i).get("menu_id").toString();
                System.out.println("Menu Id====" + menu_id);
                redisHelper.setValueJson("sandredisstage", 0, "AV_MENU_" + menu_id, "{\"status\":true,\"nextChangeTime\":1636364740000}");
            }
            //AV Items
            String item_query = "select item_id from item_menu_map where menu_id in(select id from menu_taxonomy where parent in(select id from menu_taxonomy where parent in(select menu_id from restaurant_menu_map where restaurant_id =" + restid + ") and active=1) and active=1) and active=1;";
            List<Map<String, Object>> itemLists = sqlTemplate.queryForList(item_query);
            for (int i = 0; i < itemLists.size(); i++) {
                String item_id = itemLists.get(i).get("item_id").toString();
                System.out.println("Item Id====" + item_id);
                redisHelper.setValueJson("sandredisstage", 0, "AV_ITEM_" + item_id, "{\"status\":true,\"nextChangeTime\":1636364740000}");
            }
        }
    }


}
