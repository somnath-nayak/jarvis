package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "endTime",
        "startTime"
})
public class Slot {

    @JsonProperty("endTime")
    private Long endTime;
    @JsonProperty("startTime")
    private Long startTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public Slot() {
    }

    /**
     *
     * @param startTime
     * @param endTime
     */
    public Slot(Long endTime, Long startTime) {
        super();
        this.endTime = endTime;
        this.startTime = startTime;
    }

    @JsonProperty("endTime")
    public Long getEndTime() {
        return endTime;
    }

    @JsonProperty("endTime")
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @JsonProperty("startTime")
    public Long getStartTime() {
        return startTime;
    }

    @JsonProperty("startTime")
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

}