package com.swiggy.api.sf.checkout.helper;

import java.util.LinkedHashMap;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import com.swiggy.api.sf.rng.helper.RngHelper;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.RedisHelper;

public class FriedManV0Validator {
	
SoftAssert softAssertion= new SoftAssert();
    
public void validateV0CartDetails(String cartResponse){
		
    LinkedHashMap<String, Integer> map=new LinkedHashMap<String, Integer>();
	String[] renderingDetails=parseJson(cartResponse, "$.data.rendering_details[*].key").split(",");
		 
  for(int i=0;i<renderingDetails.length;i++){
		     map.put(renderingDetails[i], i);
  }		
  Reporter.log("Old rendering_details Keys:-->"+map,true);
  
  String isCouponValid=parseJson(cartResponse, "$.data..is_coupon_valid");
		if(isCouponValid.equals("0")){
			if (map.containsKey("apply_coupon")){
				validateRenderingData(cartResponse,map.get("apply_coupon"),"value","APPLY COUPON","apply_coupon");
				validateRenderingData(cartResponse,map.get("apply_coupon"),"display_text","Coupon Discount","apply_coupon");
			} else{
				softAssertion.assertEquals(true, false,"apply_coupon is not present in v0 rendering details");
			}
		}      
		
 Double billTotal=Double.parseDouble(parseJson(cartResponse, "$.data..bill_total"));
	   if (map.containsKey("bill_total")){
		     softAssertion.assertEquals(billTotal, "","value of bill Total is not equal");	
		} else{
			softAssertion.assertEquals(true, false,"bill_total is not present in cart response"); 	
		}

		
//!("cafe").equalsIgnoreCase(#cart_type) && ((#delivery_charges > #threshold_fee && #delivery_charges > #distance_fee 
		   //&& #delivery_charges > #time_fee) || #delivery_charges == 0) && (!(#trade_discount_reward_list).contains("FREE_DELIVERY"))
		
		String cartType=parseJson(cartResponse, "$.data..cart_type");
		Double delCharges=Double.parseDouble(parseJson(cartResponse, "$.data..delivery_charges"));
		Double conFee=Double.parseDouble(parseJson(cartResponse, "$.data..convenience_fee"));
		String isSuperTD=parseJson(cartResponse, "$.data..is_super_trade_discount");
		
		Double thresholdFee=Double.parseDouble(parseJson(cartResponse, "$.data..threshold_fee"));
		Double distanceFee=Double.parseDouble(parseJson(cartResponse, "$.data..distance_fee"));
		Double timeFee=Double.parseDouble(parseJson(cartResponse, "$.data..time_fee"));
		String thresholdFeeMes=parseJson(cartResponse, "$.data..threshold_fee_message");
		String convenienceFeeInfo=parseJson(cartResponse, "$.data..convenience_fee_info");
		String TDrewardList=parseJson(cartResponse, "$.data..trade_discount_reward_list");
		
		
		if (!cartType.equals("CAFE") && delCharges >thresholdFee && delCharges >distanceFee && delCharges>timeFee || delCharges==0 && 
				!TDrewardList.contains("FREE_DELIVERY")){
			if (map.containsKey("delivery_charges")){
				validateRenderingData(cartResponse,map.get("delivery_charges"),"value",delCharges,"coupon_discount");
			} else{
				softAssertion.assertEquals(true, false,"delivery_charges is not present in V0 rendering details");
			}
		}
		
		
		if (conFee>0){
			if (map.containsKey("surge_fee")){
				validateRenderingData(cartResponse,map.get("surge_fee"),"value",conFee,"surge_fee");
				validateRenderingData(cartResponse,map.get("surge_fee"),"info_text",convenienceFeeInfo,"convenience_fee_info");
			} else{
				softAssertion.assertEquals(true, false,"surge_fee is not present in V0 rendering details");
			}
		}
		
		
		Double canFee=Double.parseDouble(parseJson(cartResponse, "$.data..cancellation_fee"));
		Double swiggyMoney=Double.parseDouble(parseJson(cartResponse, "$.data..swiggy_money"));
		if (swiggyMoney>0){
			if (map.containsKey("swiggy_money")){
					validateRenderingData(cartResponse,map.get("swiggy_money"),"value",swiggyMoney,"swiggy_money");
				} else{
					softAssertion.assertEquals(true, false,"swiggy_money is not present in rendering details");
				}
		}
		

		if (map.containsKey("grand_total")){
			double exValue=Double.parseDouble(parseJson(cartResponse, "$.data..order_total"));
			validateRenderingData(cartResponse,map.get("grand_total"),"value",exValue,"grand_total");
			validateRenderingData(cartResponse,map.get("grand_total"),"display_text","To Pay","grand_total");
		} else{
			softAssertion.assertEquals(true, false,"grand_total is not present in V0 rendering details");
	    }
		
		
		String cancellationFeeInfo=parseJson(cartResponse, "$.data..cancellation_fee_info");
		if (canFee>0){
             if (map.containsKey("cancellation_fee")){
					validateRenderingData(cartResponse,map.get("cancellation_fee"),"value",canFee,"cancellation_fee");
					validateRenderingData(cartResponse,map.get("cancellation_fee"),"info_text",cancellationFeeInfo,"cancellation_fee");
				} else{
					softAssertion.assertEquals(true, false,"cancellation_fee is not present in rendering details");
				}   
		}
		
		if (isCouponValid.equals("1")){
			Double discount=Double.parseDouble(parseJson(cartResponse, "$.data..coupon_discount_total"));
			String couponCode=parseJson(cartResponse, "$.data..coupon_code");
				if (map.containsKey("coupon_discount")){
					validateRenderingData(cartResponse,map.get("coupon_discount"),"value",discount,"coupon_discount");
					validateRenderingData(cartResponse,map.get("coupon_discount"),"intermediateText",couponCode,"coupon_code");
				} else{
					softAssertion.assertEquals(true, false,"coupon_discount is not present in V0 rendering details");
				}
		}
		
		//delivery_charges_threshold
	if (thresholdFee>0 && distanceFee==0 && timeFee==0){
		if (map.containsKey("delivery_charges_threshold")){
			 validateRenderingData(cartResponse,map.get("delivery_charges_threshold"),"value",delCharges,"delivery_charges_threshold");
			 validateRenderingData(cartResponse,map.get("delivery_charges_threshold"),"info_text",thresholdFeeMes,"threshold_fee_message");
		} else{
			softAssertion.assertEquals(true, false,"delivery_charges_threshold is not present in rendering details");
			}
	}
		

	String distanceFeeMes=parseJson(cartResponse, "$.data..distance_fee_message");
	
	if (distanceFee>0 && thresholdFee==0 && timeFee==0){
		if (map.containsKey("delivery_charges_distance")){
			validateRenderingData(cartResponse,map.get("delivery_charges_distance"),"value",delCharges,"delivery_charges_threshold");
			validateRenderingData(cartResponse,map.get("delivery_charges_distance"),"info_text",distanceFeeMes,"threshold_fee_message");
		} else{
			softAssertion.assertEquals(true, false,"delivery_charges_distance is not present in rendering details");
		}
	}
		
	String timeFeeMes=parseJson(cartResponse, "$.data..time_fee_message");

	if (timeFee>0 && thresholdFee==0 && distanceFee==0){
		if (map.containsKey("delivery_charges_time")){
			validateRenderingData(cartResponse,map.get("delivery_charges_time"),"value",delCharges,"delivery_charges_time");
			validateRenderingData(cartResponse,map.get("delivery_charges_time"),"info_text",timeFeeMes,"delivery_charges_time info_text");
		} else{
			softAssertion.assertEquals(true, false,"delivery_charges_time is not present in V0 rendering details");
		}
	}
	
  Double subsTotal=Double.parseDouble(parseJson(cartResponse, "$.data.subscription_total"));
	if (subsTotal>0){
		if (map.containsKey("subscription_total")){
				validateRenderingData(cartResponse,map.get("subscription_total"),"value",subsTotal,"subscription_total");
				validateRenderingData(cartResponse,map.get("subscription_total"),"display_text","SUPER Membership","subscription_total display_text");
		} else{
				softAssertion.assertEquals(true, false,"subscription_total is not present in V0 rendering details");
			}
		}

		
	//((#trade_discount_reward_list).contains("FREE_DELIVERY")) && !((("ANDROID").equalsIgnoreCase(#userAgent) 
		//&& (#versionCode >= 226)) || (("IOS").equalsIgnoreCase(#userAgent) && (#versionCode > 204)))
			
		String delText="Free Delivery for this order";
			
	if (TDrewardList.contains("FREE_DELIVERY")){
		validateRenderingData(cartResponse,map.get("delivery_charges"),"value","FREE","delivery_charges");
		validateRenderingData(cartResponse,map.get("delivery_charges"),"info_text",delText,"delivery_charges");
	} else{
		softAssertion.assertEquals(true, false,"delivery_charges is not present in V0 rendering details"); 	
			}		
		

	if (map.containsKey("item_total")){
		double actualValue=Double.parseDouble(parseJson(cartResponse, "$.data.item_total"));
		validateRenderingData(cartResponse,map.get("item_total"),"value",actualValue,"item_total");
	} else{
		softAssertion.assertEquals(true, false,"item_total is not present in V0 rendering details");
	}	
	
	
	Double TDTotal=Double.parseDouble(parseJson(cartResponse, "$.data..trade_discount_without_freebie"));
	if (TDTotal>0.0){
			if (map.containsKey("offers_discount")){
				validateRenderingData(cartResponse,map.get("offers_discount"),"value",TDTotal,"offers_discount");
			} else{
				softAssertion.assertEquals(true, false,"offers_discount is not present in V0 rendering details");
			}
	}	
		
	
	double packingCharge = Double.parseDouble(parseJson(cartResponse, "$.data.total_packing_charges"));
	double sGST = Double.parseDouble(parseJson(cartResponse, "$.data.GST_details.cart_SGST"));
	double cGST = Double.parseDouble(parseJson(cartResponse, "$.data.GST_details.cart_CGST"));
	double iGST = Double.parseDouble(parseJson(cartResponse, "$.data.GST_details.cart_IGST"));
	double exGST =Double.parseDouble(parseJson(cartResponse, "$.data.GST_details.external_GST"));
	double actualGSTTotal=sGST+cGST+iGST+exGST;
	
	if (actualGSTTotal>0.0){
		if (map.containsKey("GST")){
			validateRenderingData(cartResponse,map.get("GST"),"value",actualGSTTotal,"GST");
			validateRenderingData(cartResponse,map.get("GST"),"display_text","GST","display_text of GST");
		} else{
			softAssertion.assertEquals(true, false,"GST is not present in V0 rendering details");
		}
}	
		
		
	String showSuperNudge=parseJson(cartResponse, "$.data..show_super_nudge");
	if (showSuperNudge.equals("true")){
			if (map.containsKey("super_nudge_button")){
				int i=map.get("super_nudge_button");
				String nudgeCTA=parseJson(cartResponse, "$.data..subscription_nudge..subscription_nudge_cta");
				String nudgeTittle=parseJson(cartResponse, "$.data..subscription_nudge..subscription_nudge_title");
				String nudgeSubTittle=parseJson(cartResponse, "$.data..subscription_nudge..subscription_nudge_subtitle");
				softAssertion.assertNotNull(nudgeCTA,"nudgeCTA is not present in cart");
				softAssertion.assertNotNull(nudgeTittle,"nudgeTittle is not present in cart");
				softAssertion.assertNotNull(nudgeSubTittle,"nudgeSubTittle is not present in cart");
			} else{
				softAssertion.assertEquals(true, false,"super_nudge_button is not present in rendering details");
			}
	}
	

	String couponValid=parseJson(cartResponse, "$.data..is_coupon_valid");
	Double couponDis=Double.parseDouble(parseJson(cartResponse, "$.data..coupon_discount_total"));
	if (couponValid.equals("1")){
		String couponCode=parseJson(cartResponse, "$.data..coupon_code");
         if (map.containsKey("remove_coupon_button")){
				validateRenderingData(cartResponse,map.get("remove_coupon_button"),"value",couponDis,"remove_coupon_button");
				validateRenderingData(cartResponse,map.get("remove_coupon_button"),"intermediateText",couponCode,"coupon_code");
			} else{
				softAssertion.assertEquals(true, false,"remove_coupon_button is not present in rendering details");
			}
	}
}

		
	public String getData(String cartResponse,int n,String key){
		String data=JsonPath.read(cartResponse, "$.data..rendering_details["+n+"]."+key+"")
                .toString().replace("[","").replace("]","").replace("\"","");
		return data;
	}
	
	public String getSubData(String cartResponse,int index,int subIndex,String key){
		String data=JsonPath.read(cartResponse, "$.data..rendering_details["+index+"].sub_details["+subIndex+"].."+key+"")
                .toString().replace("[","").replace("]","").replace("\"","");
		return data;
	}
	
	public String parseJson(String cartResponse,String key){
		return JsonPath.read(cartResponse, key).toString().replace("[","").replace("]","").replace("\"","");
	}
		
	public void validateRenderingData(String cartResponse,int index,String search,Double actualValue,String assertMes){
		Double value=Double.parseDouble(getData(cartResponse,index,search));
		softAssertion.assertEquals(value, actualValue, "Value mismatched for "+assertMes+" in V0 rendering details");
	}
	
	public void validateRenderingData(String cartResponse,int index,String search,String exValue,String assertMes){
		String value=getData(cartResponse,index,search);
		softAssertion.assertEquals(value,exValue, "Value mismatched for "+assertMes+" in V0 rendering details");
	} 
}
