package com.swiggy.api.sf.snd.helper;

import com.swiggy.api.sf.rng.constants.RngConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;
import java.util.Map;

public class ListingTroubleShooterHelper {

    Initialize gameofthrones = Initializer.getInitializer();

    static GameOfThronesService service;
    Processor processor;

    public Processor listingWithCheckout(String lat, String lng, String pageType) {

        HashMap headers = new HashMap<String, String>();
        headers.put("version-code", RngConstants.VersionCode229);
        service = new GameOfThronesService("checkout", "listingpage", gameofthrones);
        String[] urlparams = new String[] { lat, lng, pageType };
        processor = new Processor(service, headers, null, urlparams);
        return processor;

    }

    public Processor listingWithGandalf(String lat, String lng, String pageType) {

        HashMap headers = new HashMap<String, String>();
        headers.put("version-code", RngConstants.VersionCode229);
        service = new GameOfThronesService("dweblisting", "listingpage", gameofthrones);
        String[] urlparams = new String[] { lat, lng, pageType };
        processor = new Processor(service, headers, null, urlparams);
        return processor;

    }


    public Processor aggregator(String[] payload) {

        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("Content-Type", "application/json");

        GameOfThronesService gots = new GameOfThronesService("sand", "aggregator", gameofthrones);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor aggregatorWithCheckout(String[] payload) {

        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("Content-Type", "application/json");

        GameOfThronesService gots = new GameOfThronesService("checkout", "aggregator", gameofthrones);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor deliveryListingDebug(String[] payload)
    {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Cache-Control", "no-cache");
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","listingdebug",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, requestheaders, payload);
        return processor;
    }

}
