package com.swiggy.api.sf.snd.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class POPHelper {
	Initialize gameofthrones = new Initialize();
	WireMockHelper wireMockHelper = new WireMockHelper();
	
	/**
	 * Assertion method for Smoke Testing
	 * @param statusCode
	 * @param statusMessage
	 * @param processor
	 */
	public void smokeCheck(int statusCode, String statusMessage, Processor processor){
		Reporter.log("Expected StatusCode = '" + statusCode + "' <==> Actual StatusCode = '" + processor.ResponseValidator.GetNodeValueAsInt("statusCode") + "'", true);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode, "StatusCode mismatch");
		Reporter.log("Expected StatusMessage = '" + statusMessage + "' <==> Actual StatusMessage = '" + processor.ResponseValidator.GetNodeValue("statusMessage") + "'", true);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage, "StatusMessage mismatch");
	}
	
	/**
	 * Returns full JSON response as Processor instance
	 * @param payload
	 * @return Processor instance
	 */
	public Processor popForDelivery(String[] payload){
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		requestHeader.put("User-Agent", "Swiggy-iOS");
		GameOfThronesService gots = new GameOfThronesService("sand", "popfordelivery", gameofthrones);
		Processor processor = new Processor(gots, requestHeader,payload);
		return processor;
	}
	
	/**
	 * Returns full JSON response as Processor instance
	 * @param lat
	 * @param lng
	 * @return Processor instance
	 */
	public Processor popListing(String lat, String lng){
		String[] queryParam = {lat,lng};
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService gots = new GameOfThronesService("sand", "poplisting", gameofthrones);
		Processor processor = new Processor(gots, requestHeader, null, queryParam);
		return processor;
	}
	
	/**
	 * Returns full JSON response as Processor instance
	 * @param itemID
	 * @param restID
	 * @return Processor instance
	 */
	public Processor popItemMenu(String itemID, String restID){
		String[] queryParam = {itemID,restID};
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService gots = new GameOfThronesService("sand", "popitemmenu", gameofthrones);
		Processor processor = new Processor(gots, requestHeader, null, queryParam);
		return processor;
	}
	
	/**
	 * Returns full JSON response as Processor instance
	 * @param latlng
	 * @return Processor instance
	 */
	public Processor popAggregator(String[] latlng){
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService gots = new GameOfThronesService("sand", "popaggregator", gameofthrones);
		Processor processor = new Processor(gots, requestHeader, latlng);
		return processor;
	}

	public void stubFetchServiceability(String restaurantId) throws IOException
	{
		File file = new File("../Data/MockAPI/ResponseBody/sand/ServiceableMockResponse");
		String body = FileUtils.readFileToString(file);
		body=body.replace("${restaurantId}",restaurantId);
		System.out.println(body);
		wireMockHelper.setupStub("/api/delivery/listing_pop", 200, "application/json", body,0);
	}

	public void stubFetchUnserviceable(String restaurantId) throws IOException
	{
		File file = new File("../Data/MockAPI/ResponseBody/sand/UnServiceableMockResponse");
		String body = FileUtils.readFileToString(file);
		body=body.replace("${restaurantId}",restaurantId);
		System.out.println(body);
		wireMockHelper.setupStub("/api/delivery/listing_pop", 200, "application/json", body,0);
	}

	public void stubFetchServiceableWithBanner(String restaurantId) throws IOException
	{
		File file = new File("../Data/MockAPI/ResponseBody/sand/ServiceableWithBannerMockresponse");
		String body = FileUtils.readFileToString(file);
		body=body.replace("${restaurantId}",restaurantId);
		System.out.println(body);
		wireMockHelper.setupStub("/api/delivery/listing_pop", 200, "application/json", body,0);
	}


	public Processor toCheckTheDeliverySystemForServiceability(String lat_lon){
		String[] queryParam = {lat_lon};
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		requestHeader.put("Postman-Token","8915449b-4827-4f74-95e2-29199eef4c50");
		GameOfThronesService gots = new GameOfThronesService("serviceabilitycheckfromdeliveryforpop", "serviceabilitycheckforpop", gameofthrones);
		Processor processor = new Processor(gots, requestHeader, null, queryParam);
		return processor;
	}
}
