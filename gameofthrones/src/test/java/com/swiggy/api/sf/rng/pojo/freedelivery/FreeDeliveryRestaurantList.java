/**
 * @author manu.chadha
 */

package com.swiggy.api.sf.rng.pojo.freedelivery;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class FreeDeliveryRestaurantList {

    @JsonProperty("id")
    private String id;

    /**
     * No args constructor for use in serialization
     *
     */
    public FreeDeliveryRestaurantList() {
    }

    /**
     *
     * @param id
     */
    public FreeDeliveryRestaurantList(String id) {
        super();
        this.id = id;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).toString();
    }

}