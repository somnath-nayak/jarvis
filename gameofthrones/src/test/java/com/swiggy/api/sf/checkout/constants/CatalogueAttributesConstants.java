package com.swiggy.api.sf.checkout.constants;

public interface CatalogueAttributesConstants {

    String REST_ID_01 = "10582";
    String ITEM_ID_I_01 = "7978301"; //Item with attributes at Item Level
    String ITEM_ID_V_01 = "7978301"; //Item with attributes at Variant Level
    String ITEM_ID_I_V_01 = "7978301"; //Item with attributes at Item Level and Variant Level

    //Cart Types
    String CART_TYPE_REGULAR = "REGULAR";
    String CART_TYPE_PREORDER = "PREORDER";
    String CART_TYPE_POP = "POP";

    //Validation Constants
    int STATUS_CODE_CARTV2_SUCCESS = 0;
    String STATUS_MESSAGE_CARTV2_SUCCESS = "CART_UPDATED_SUCCESSFULLY";
    int STATUS_CODE_ORDER_SUCCESS = 0;
    String STATUS_MESSAGE_ORDER_SUCCESS = "done successfully";

    //Login Credentials
    String MOBILE = "8884292249";
    String PASSWORD = "welcome123";

    //TD Constants
    String MIN_CART_AMOUNT = "1";
    boolean FIRST_ORDER_RESTRICTION = false;
    boolean RESTAURANT_FIRST_ORDER = false;
    boolean USER_RESTRICTION = false;
    String DORMANT_USER_TYPE = "ZERO_DAYS_DORMANT";
    boolean SURGE = true;
    String FLAT_TD_AMOUNT = "20";
    String PERCENTAGE_TD_AMOUNT = "10";

    //TD Type
    String FREE_DELIVERY = "FREE_DELIVERY";
    String FREEBIE = "FREEBIE";
    String NO_TD = "NO_TD"; // Without any Trade Discount
    String FLAT_TD = "FLAT_TD";
    String PERCENTAGE_TD = "PERCENTAGE_TD";

    //Delay required to reflect Freebie TD on cart after creation
    int FREEBIE_CACHE_DELAY = 20000; //unit is in milliseconds

    String PAYMENT_METHOD = "Cash";
    String ORDER_COMMENT = "Test Automation Order";
}
