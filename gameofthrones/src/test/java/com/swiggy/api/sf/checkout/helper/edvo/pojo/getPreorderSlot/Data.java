package com.swiggy.api.sf.checkout.helper.edvo.pojo.getPreorderSlot;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Slot;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "date",
        "slots"
})
public class Data {

    @JsonProperty("date")
    private Long date;
    @JsonProperty("slots")
    private List<Slot> slots = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    /**
     *
     * @param slots
     * @param date
     */
    public Data(Long date, List<Slot> slots) {
        super();
        this.date = date;
        this.slots = slots;
    }

    @JsonProperty("date")
    public Long getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Long date) {
        this.date = date;
    }

    @JsonProperty("slots")
    public List<Slot> getSlots() {
        return slots;
    }

    @JsonProperty("slots")
    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

}