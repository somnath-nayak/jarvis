package com.swiggy.api.sf.rng.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.util.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.pojo.ProxyServerPOJO;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.dp.ListingDP;
import com.swiggy.api.sf.rng.helper.RngHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Optional;
import org.testng.asserts.SoftAssert;

public class ListingV5 extends ListingDP {

	Processor processor;
	RngHelper rngHelper = new RngHelper();
	SANDHelper sndHelper = new SANDHelper();
	SoftAssert softAssert = new SoftAssert();
	DeliveryDataHelper ddh = new DeliveryDataHelper();
//	WireMockHelper wireMockHelper;

//	static String contextId = "";
	static String polygonID = "3214";
	boolean redirectToMock;
	boolean doMock;
    ProxyServerPOJO proxyServerPOJO;

	@BeforeClass(groups = { "sanity", "regression" })
	@Parameters({ "redirectToMock"})
	public void layoutCreation(ITestContext iTestContext, @Optional("false") boolean mock) {
		System.out.println(iTestContext.getExcludedGroups());
		redirectToMock = mock;
		System.out.println("Mocked :: " + redirectToMock);
//		contextId = rngHelper.createLayoutContextMap("Testing1", "CITY", "1", "VIEW_DOMAIN", "RESTAURANT_LISTING", "PLATFORM", "Swiggy-Android", "true", "5000");
//		polygonID = rngHelper.createAndMapPolygon();

//		createOpenFilterWithoutLinkTestData();
		for (int i = 0; i < 4; i++) {
//			createCollection(RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent);
		}

//		wireMockHelper = new WireMockHelper();
//		wireMockHelper.startMockServer(6666);
		redirectToMock = mock;
		doMock = true;
		proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");

        addMockEndPoint(proxyServerPOJO);
        updateMockEndPoint(proxyServerPOJO);

		if (doMock) {
			setMockRequest("api/v1/aggregator/", "AggregatorWithOpenFilters");
		}


	}

	@AfterClass(groups = { "sanity", "regression" })
	public void disableContext() {
//		rngHelper.disableContext(contextId);
//		wireMockHelper.stopMockServer();
        proxyServerPOJO = new ProxyServerPOJO()
                .withApiEndPoint("/api/v1/aggregator/")
                .withDoMock("0");
        updateMockEndPoint(proxyServerPOJO);
        proxyServerPOJO = new ProxyServerPOJO()
                .withApiEndPoint("/api/v1/pop/aggregator")
                .withDoMock("0");
        updateMockEndPoint(proxyServerPOJO);
        proxyServerPOJO = new ProxyServerPOJO()
                .withApiEndPoint("/api/v1/restaurants/area/btm")
                .withDoMock("0");
        updateMockEndPoint(proxyServerPOJO);

	}

	@Test(dataProvider = "listingv5fieldLevelValidatation", description = "RestaurantListing Field Level Validation", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void listingFieldLevelValidation(String lat, String lng, String page, String collection,
			String popupBannerIds, boolean check) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);
		processor = rngHelper.getListingProcessor(lat, lng, page, collection, popupBannerIds);
		String listingResponse = processor.ResponseValidator.GetBodyAsText();
		if (check) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.statusMessage"), "done successfully");
		} else {
			if (page.equals("0") && collection.equals("123")) {
				Assert.assertEquals(JsonPath.read(listingResponse, "$.statusMessage").toString(),
						"Invalid latitude/longitude");

			} else {
				if (page.equalsIgnoreCase("-1")) {
                    Assert.assertTrue((int)JsonPath.read(listingResponse, "$.statusCode")==1,
                            "Assertion Failed: for invalid page number should also get happy response");
				}
				else {
                    Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 400);
                }
			}
		}

	}

	@Test(dataProvider = "dwebFieldLevelValidation", description = "DwebListing Field Level Validation", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void dwebFieldLevelValidation(String lat, String lng, String pageType, boolean isCheck) {
		processor = rngHelper.getDwebListingProcessor(lat, lng, pageType);
		String listingResponse = processor.ResponseValidator.GetBodyAsText();
		if (lat.equals(" ") || "null".equals(lat)) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.statusMessage").toString(),
					"Invalid latitude/longitude");
			return;
		}
		if (lng.equals(" ") | lng.equals("null")) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.statusMessage").toString(),
					"Invalid latitude/longitude");
			return;
		}

		if (pageType.equals(" ") || pageType.equals(null)) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.error").toString(), "Internal Server Error");
			return;
		}
		if (isCheck) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.statusMessage"), "done successfully");

		}

	}

	@Test(dataProvider = "seeAllValidation", description = "SeeAll Field Level Validation", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void seeAllValidation(String lat, String lng, String pageType, String offset, String pageLimit,
			boolean check) {
		processor = rngHelper.getSeeAllProcessor(lat, lng, pageType, offset, pageLimit);
		String listingResponse = processor.ResponseValidator.GetBodyAsText();

//		if(offset.equalsIgnoreCase(" "))
//
//		{
//			Assert.assertTrue(false,"offset is null");
//		}

		if (lat.equals("") || "null".equals(lat) || lng.equals("") || "null".equals(lng)) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.statusMessage").toString(),
					"Invalid latitude/longitude");
			return;
		}

		if (offset.equals("-1") || pageLimit.equals("-1")) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.statusMessage"),
					"offset and page_limit should be not null and greater than equals to 0");
			return;
		}

		if (offset.equals("null") || pageLimit.equals("null")) {
		    try{
                Assert.assertEquals(JsonPath.read(listingResponse, "$.error"), "Bad Request");
            }catch (PathNotFoundException e){
                Assert.assertTrue(listingResponse.contains("Whitelabel Error Page"));
        }
        return;

		}

		if (check) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.statusMessage"), "done successfully");

		}

		int i = 0;
		if (pageType.equals("null") || pageType.equals("test")) {
            try{
			    Assert.assertEquals(JsonPath.read(listingResponse, "$.error"), "Internal Server Error");
            }catch (PathNotFoundException e){
                Assert.assertTrue(listingResponse.contains("Whitelabel Error Page"));
            }
			return;
		}

		if (check) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.statusMessage"), "done successfully");

		}

		if (i > Integer.parseInt(pageLimit)) {

			Assert.assertTrue(false, "No of cards are more than the given page limit");
		}
		if (offset.equalsIgnoreCase("4")) {
			Assert.assertEquals(JsonPath.read(listingResponse, "$.data.currentOffset").toString(), offset,
					"Offset is coming different in response");

		}

	}

	@Test(dataProvider = "SeeAll_tidSidDeviceIdValidation", description = "SeeAll TID,SID,DeviceID Validation on Each Page", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void SeeAll_tidSidDeviceIdValidation(String lat, String lng, String pageType) {

		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		String offset = "0";
		String pageLimit = "4";
		processor = rngHelper.getSeeAllProcessor(lat, lng, pageType, offset, pageLimit, tid, token);
		String listingResponse = processor.ResponseValidator.GetBodyAsText();
		String tid1 = JsonPath.read(listingResponse, "$.tid");
		String sid1 = JsonPath.read(listingResponse, "$.sid");
//		String deviceId = JsonPath.read(listingResponse,"$.deviceId");

		offset = "2";
		pageLimit = "4";
		processor = rngHelper.getSeeAllProcessor(lat, lng, pageType, offset, pageLimit, tid, token);
		String listingResponse2 = processor.ResponseValidator.GetBodyAsText();
		String tid2 = JsonPath.read(listingResponse2, "$.tid");
		String sid2 = JsonPath.read(listingResponse2, "$.sid");

		softAssert.assertEquals(tid1, tid2, "Wrong tid is going to response");
		softAssert.assertEquals(sid1, sid2, "Wrong sid is going to response");

		softAssert.assertAll();
	}

	@Test(dataProvider = "seeAll_repeatedRestaurantValidation", description = "SeeAll Validation for Repeated Restaurants", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void seeAll_repeatedRestaurantValidation(String lat, String lng) {

		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		String offset = "0";
		String pageLimit = "2";
		String pageType = "DESKTOP_SEE_ALL_LISTING";
		String response = rngHelper.getSeeAllProcessor(lat, lng, pageType, offset, pageLimit, tid,
				token).ResponseValidator.GetBodyAsText();
		List<String> restids = JsonPath.read(response, "$.data..id");
		HashSet<String> hs = new HashSet<>();
		for (String restId : restids) {
			hs.add(restId);
		}

		offset = "2";
		pageLimit = "4";
		pageType = "DESKTOP_SEE_ALL_LISTING";
		response = rngHelper.getSeeAllProcessor(lat, lng, pageType, offset, pageLimit, tid, token).ResponseValidator
				.GetBodyAsText();
		List<String> restids2 = JsonPath.read(response, "$.data..id");
		for (String restId : restids2) {
			if (!hs.add(restId)) {
				Assert.assertTrue(false);
			}
		}

	}

	@Test(dataProvider = "seeAll_filterValidations", description = "SeeAll with Filter Validation ", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void seeAll_filterValidations(String lat, String lng, String pageType, String offset, String pagelimit,
			String filter) {

		String response = rngHelper.getSeeAllFilterProcessor(lat, lng, pageType, offset, pagelimit,
				filter).ResponseValidator.GetBodyAsText();
		List cardType = JsonPath.read(response, "$.data..cardType");
		Iterator it = cardType.iterator();
		while (it.hasNext()) {

			if (it.next().toString().equals("restaurant")) {
				switch (filter) {
				case "SHOW_RESTAURANTS_WITH:Offers":
					Assert.assertNotNull(JsonPath.read(response, "$.data..tradeCampaignHeaders"));
					break;
				case "SHOW_RESTAURANTS_WITH:PureVeg":
					Assert.assertNotNull(JsonPath.read(response, "$.data.cards..veg"));
					break;
				case "CUISINES:NorthIndian":
					Assert.assertEquals(JsonPath.read(response, "$.data.cards[2]..cuisines").toString(),
							"[\"North Indian\"]");
					break;
				case "SHOW_RESTAURANTS_WITH:Swiggy+Assured":
					Assert.assertEquals(JsonPath.read(response, "$.data.cards[2]..select").toString(), "[true]");
					break;
				case "SHOW_RESTAURANTS_WITH:Pure Veg;Swiggy Assured":
					twoFilterValiator(response, "or");

				}
			} else {
				System.out.println("restaurantCard Not Fount");
			}
		}

	}

	@Test(dataProvider = "seeAll_sortValidations", description = "SeeAll with Sort Validation ", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void seeAll_sortValidations(String lat, String lng, String pageType, String offset, String pagelimit,
			String sort) {

		String response = rngHelper.getSeeAllSortProcessor(lat, lng, pageType, offset, pagelimit,
				sort).ResponseValidator.GetBodyAsText();

		List cardType = JsonPath.read(response, "$.data..cardType");
		Iterator it = cardType.iterator();
		while (it.hasNext()) {
			if (it.next().toString().equals("restaurant")) {
				switch (sort) {
				case "RELEVANCY":
					Assert.assertNotNull(JsonPath.read(response, "$.data.cards[2]..id"));
					break;
				case "DELIVERY_TIME":
					deliveryTime(response);
					break;
				case "RATING":
					rating(response);
					break;
				case "COST_FOR_TWO":
					costForTwo(response);
					break;
				}
			} else {
				System.out.println("restaurantCard Not Found");
			}
		}

	}

	@Test(dataProvider = "seeMoreValidation", description = "SeeMore Field Level Validation ", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void seeMoreValidation(String lat, String lng, String pageType, String collectionId, String offset,
			String pageLimit, boolean isCheck, boolean pCheck, boolean tcheck) {
		proxyServerPOJO = new ProxyServerPOJO()
				.withIpAddress("127.0.0.1")
				.withApiEndPoint("/api/v1/aggregator/")
				.withMockUrl("http://10.0.6.59:6666/")
				.withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

		updateMockEndPoint(proxyServerPOJO);

		Processor processor = rngHelper.seeMore(lat, lng, pageType, collectionId, offset, pageLimit);
		String response = processor.ResponseValidator.GetBodyAsText();

		if (lat.equals("") || "null".equals(lat) || lng.equals("") || "null".equals(lng)) {
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "Invalid latitude/longitude");
			return;
		}

		if (collectionId.equals("")) {
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage"),
					"Missing param 'collection' for page type 'DESKTOP_COLLECTION_LISTING'");
			return;
		}
		if (offset.equals("-1") || pageLimit.equals("-1") || offset.equals(" ") || pageLimit.equals(" ")) {
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage"),
					"offset and page_limit should be not null and greater than equals to 0");
			return;
		}
		if (pageType.equals(" ")) {
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage"), "done successfully");
			return;

		}

		if (isCheck) {
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage"), "done successfully");
		} else {
			if (pCheck) {
                try{
                    Assert.assertEquals(JsonPath.read(response, "$.error"), "Internal Server Error");
                }catch(PathNotFoundException e){
			          Assert.assertTrue(response.contains("Whitelabel Error Page"));
			    }
			}else {

				if (tcheck) {
				    try{
					    Assert.assertEquals(JsonPath.read(response, "$.error"), "Bad Request");
                     }catch(PathNotFoundException e){
                         Assert.assertTrue(response.contains("Whitelabel Error Page"));
                    }
				} else {

					Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(),
							"Invalid latitude/longitude");
				}
			}

		}

	}

	@Test(dataProvider = "seeMoreRestaurantRepeatValidation", description = "SeeMore Validation for Repeat Restaurants ", groups = {
			"sanity", "Gaurav" })
	public void seeMoreRestaurantRepeatValidation(String lat, String lng, String userAgent) {
		String pageType = "DESKTOP_COLLECTION_LISTING";
		String offset = "0";
		String pageLimit = "2";
		String collectionId = rngHelper.createCollection(lat, lng, userAgent, 1);
		String response = rngHelper.seeMore(lat, lng, pageType, collectionId, offset, pageLimit).ResponseValidator
				.GetBodyAsText();
		List restids = JsonPath.read(response, "$.data..id");
		HashSet<Object> hs = new HashSet<>();
		for (Object restId : restids) {
			hs.add(restId);
		}
		offset = "2";
		pageLimit = "4";
		response = rngHelper.seeMore(lat, lng, pageType, collectionId, offset, pageLimit).ResponseValidator
				.GetBodyAsText();

		List<Object> restids2 = JsonPath.read(response, "$.data..id");
		for (Object restId : restids2) {
			if (!hs.add(restId)) {
				Assert.assertTrue(false);
			}

		}
	}

	@Test(dataProvider = "seeMoreRestaurantRepeatValidation", description = "SeeMore validation for Restaurant Count ", groups = {
			"sanity", "Gaurav" })
	public void seeMoreRestaurantValidtion(String lat, String lng, String userAgent) {
		String pageType = "DESKTOP_COLLECTION_LISTING";
		String offset = "0";
		String pageLimit = "100";
		String collectionId = rngHelper.createCollection(lat, lng, userAgent, 1);
		String response = rngHelper.seeMore(lat, lng, pageType, collectionId, offset, pageLimit).ResponseValidator
				.GetBodyAsText();

		List al = rngHelper.getRestaurantsList(response);
		if (al.size() == 0) {
			Assert.assertTrue(false, "No card found in collection");
		}

		int restcount = 0;
		List count = new ArrayList();
		int topCollectionCount = 0;
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		for (int i = 0; i < cardTypes.size(); i++) {
			if ((cardTypes.get(i).toString().equalsIgnoreCase("topCollection"))) {
				count = JsonPath.read(response, ".data.cards[" + i + "].data.data.count");
				topCollectionCount = (int) count.get(0);
			}
			if ((cardTypes.get(i).toString().equalsIgnoreCase("restaurant"))) {
				restcount++;
			}
		}

		Assert.assertEquals(count + "", "[" + restcount + "]", "Count is not correct");

	}

	@Test(dataProvider = "seeMore_filterValidations", description = "SeeMore with Filter Validation ", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void seeMoreFilterValidations(String lat, String lng, String pageType, String collectionId, String offset,
			String pageLimit, String filter) {

        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);

		List<String> allRest = rngHelper.getAllResturants(lat, lng, "0");
		rngHelper.addOfferOnGivenResturant(allRest);

		// TODO Create colletion with all given restids.

		String id = rngHelper.createCollection(lat, lng, RngConstants.webUserAgent, 1);

		String response = rngHelper.seeMore(lat, lng, pageType, id, offset, pageLimit, filter).ResponseValidator
				.GetBodyAsText();

		List al = rngHelper.getRestaurantsList(response);

		if (al.size() == 0) {
			Assert.assertTrue(false, "No card is found");
		}

		// int count = (Integer) al.get(0);
		int count = al.size();
		int restcount = 0;
		List<String> cartTypes = JsonPath.read(response, "$.data..cardType");

		for (String cartType : cartTypes) {
			if ((cartType.equalsIgnoreCase("restaurant"))) {
				restcount++;
			}
		}
		Assert.assertEquals(count, restcount, "Count is not correct");

		if (filter.equalsIgnoreCase("SHOW_RESTAURANTS_WITH:Offers")) {
			Assert.assertNotNull(JsonPath.read(response, "$.data..tradeCampaignHeaders"));
		}
	}

	@Test(dataProvider = "seeMoreSortValidations", description = "SeeMore with Sort Validation ", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void seeMoreSortValidations(String lat, String lng, String pageType, String collectionId, String offset,
			String pageLimit, String sort) {

		List<String> allRest = rngHelper.getAllResturants(lat, lng, "0");
		rngHelper.addOfferOnGivenResturant(allRest);

		String id = rngHelper.createCollection(lat, lng, RngConstants.webUserAgent, 1);

		String response = rngHelper.seeMoresort(lat, lng, pageType, id, offset, pageLimit, sort).ResponseValidator
				.GetBodyAsText();

		List al = rngHelper.getRestaurantsList(response);
		if (al.size() == 0) {
			Assert.assertTrue(false, "No card found");
		}
		int count = al.size();
		int restcount = 0;
		List<String> cartTypes = JsonPath.read(response, "$.data.cards..cardType");

		for (String cartType : cartTypes) {
			if ((cartType.equalsIgnoreCase("restaurant"))) {
				restcount++;
			}
		}
		Assert.assertEquals(count, restcount, "Count is not correct");
		if (sort.equalsIgnoreCase("DELIVERY_TIME")) {

			deliveryTime(response);

		}

	}

	@Test(dataProvider = "SeeMore_tidSidDeviceIdValidation", description = "SeeMore Validation for Tid,Sid,DeviceID for each Page ", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void SeeMore_tidSidDeviceIdValidation(String lat, String lng, String pageType, String collectionId) {

		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		String offset = "0";
		String pageLimit = "4";
		processor = rngHelper.seeMore(lat, lng, pageType, collectionId, offset, pageLimit, tid, token);
		String listingResponse = processor.ResponseValidator.GetBodyAsText();
		String tid1 = JsonPath.read(listingResponse, "$.tid");
		String sid1 = JsonPath.read(listingResponse, "$.sid");
//		String deviceId = JsonPath.read(listingResponse,"$.deviceId");

		offset = "2";
		pageLimit = "4";
		processor = rngHelper.seeMore(lat, lng, pageType, collectionId, offset, pageLimit, tid, token);
		String listingResponse2 = processor.ResponseValidator.GetBodyAsText();
		String tid2 = JsonPath.read(listingResponse2, "$.tid");
		String sid2 = JsonPath.read(listingResponse2, "$.sid");

		softAssert.assertEquals(tid1, tid2, "Wrong tid is going to response");
		softAssert.assertEquals(sid1, sid2, "Wrong sid is going to response");
		softAssert.assertAll();

	}

	@Test(dataProvider = "userAgent_layoutValidation", description = "Layout Validations", groups = { "regression",
			"Gaurav" })
	public void layoutValidation(String lat, String lng, String userAgent) {

        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);

		switch (userAgent) {
		case "Swiggy-Android":
			rngHelper.createCarousel(userAgent, "static", "TopCarousel", "N/A", polygonID);
			break;
		case "Swiggy-iOS":
			rngHelper.createCarousel(userAgent, "static", "TopCarousel", "N/A", polygonID);
			break;
		case "web":
			rngHelper.collectionRedis(rngHelper.createCollection(lat, lng, userAgent, 1));
			break;

		}
		processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, redirectToMock);
		String response = processor.ResponseValidator.GetBodyAsText();
		switch (userAgent) {
		case "Swiggy-Android":
			Assert.assertEquals(JsonPath.read(response, "$.data.cards[0].data.subtype"), "topCarousel");
			break;
		case "Swiggy-iOS":

			Assert.assertEquals(JsonPath.read(response, "$.data.cards[1].data.subtype"), "openFilter");
			break;
		default:
			Assert.assertEquals(JsonPath.read(response, "$.data.cards[0].data.type"), "collection");
			break;
		}
	}

	@Test(dataProvider = "createCollection", description = "Collection Creation ", groups = { "regression", "Gaurav" })
	public void createCollection(String lat, String lng, String userAgent) {
		rngHelper.collectionRedis(rngHelper.createCollection(lat, lng, userAgent, 1));
	}

	@Test(dataProvider = "createCollection", description = "Collection Creation for Merchantise Collection", groups = {
			"regression", "Gaurav" })
	public void createMerchantiseCollection(String lat, String lng, String userAgent) {
		rngHelper.collectionRedis(rngHelper.createCollection(lat, lng, userAgent, 2));

	}

	@Test(dataProvider = "collectionValidation", description = "Collection Validations", groups = { "regression",
			"Gaurav" })
	public void collectionValidation(String lat, String lng, String userAgent) {
		String collectionId = rngHelper.createCollection(lat, lng, userAgent, 1);
		rngHelper.collectionRedis(collectionId);
		processor = rngHelper.getCollectionLisitngProcessor(lat, lng, collectionId);
		String response = processor.ResponseValidator.GetBodyAsText();
		softAssert.assertEquals(JsonPath.read(response, "$.data.cards[0].cardType"), "topCollection");
		softAssert.assertNotNull(JsonPath.read(response, "$.data..count"));
		List<String> cartTypes = JsonPath.read(response, "$.data..cardType");
		int rest = 0;
		for (String cartType : cartTypes) {
			if (cartType.equalsIgnoreCase("TopCarousel")) {
				Assert.assertTrue(false);
			}
			if (cartType.equalsIgnoreCase("restaurant")) {
				rest++;
			}
		}

		softAssert.assertAll();

	}

	@Test(dataProvider = "collectionRestaurantValidation", description = "Collection Restaurants Validations", groups = {
			"regression", "Gaurav" })
	public void collectionRestaurantValidation(String lat, String lng, String userAgent) {

		String collectionId = rngHelper.createCollection(lat, lng, userAgent, 1);
		rngHelper.collectionRedis(collectionId);
		String[] payload = new String[] { lat, lng };
		int aggreRestCount = 0;
		String aggreResponse = sndHelper.aggregator(payload).ResponseValidator.GetBodyAsText();

		List<Object> cids = JsonPath.read(aggreResponse, "$.data.collections..id");
		for (int i = 0; i < cids.size(); i++) {
			if (cids.get(i).toString().equalsIgnoreCase(collectionId)) {
				List<String> rest1 = JsonPath.read(aggreResponse, "$.data.collections[" + i + "].restaurantIds");
				aggreRestCount = rest1.size();
			}
		}
		processor = rngHelper.getCollectionLisitngProcessor(lat, lng, collectionId);
		String response = processor.ResponseValidator.GetBodyAsText();
		List cardType = JsonPath.read(response, "$.data..cardType");
		String rcount = JsonPath.read(response, "$.data..count").toString();
		Iterator it = cardType.iterator();
		int i = 0;
		while (it.hasNext()) {
			if (it.next().toString().equals("restaurant")) {
				i++;
			}
		}
		if (rcount.equalsIgnoreCase("[]")) {
			Assert.assertTrue(false, "No card is found");
		} else {
			softAssert.assertEquals(Integer.parseInt(rcount.substring(1, rcount.length() - 1)), i);

			softAssert.assertEquals(aggreRestCount, i);

			softAssert.assertAll();
		}

	}

	@Test(dataProvider = "collectionRestaurantOrder", description = "Collection Restaurant Order Validation", groups = {
			"regression", "Gaurav" })
	public void collectionRestaurantOrder(String lat, String lng, String userAgent) {

		String collectionId = rngHelper.createCollection(lat, lng, userAgent, 1);
		rngHelper.collectionRedis(collectionId);

		processor = rngHelper.getCollectionLisitngProcessor(lat, lng, collectionId);
		String collectionResponse = processor.ResponseValidator.GetBodyAsText();

		List<String> cardTypes = JsonPath.read(collectionResponse, "$.data..cardType");
		List<String> collecitonRestIds = new ArrayList();

		for (int i = 0; i < cardTypes.size(); i++) {
			if (cardTypes.get(i).toString().equalsIgnoreCase("restaurant")) {
				collecitonRestIds.add(JsonPath.read(collectionResponse, "$.data.cards[" + i + "].data.data.id"));
			}
		}

		String[] payload = new String[] { lat, lng };
		int aggreRestCount = 0;
		rngHelper.errorCodeHandler(sndHelper.aggregator(payload), "Aggregator");
		String aggreResponse = sndHelper.aggregator(payload).ResponseValidator.GetBodyAsText();

		List<String> restArray = JsonPath.read(aggreResponse, "$.data.sortedRestaurants..restaurantId");

		for (int i = 0; i < collecitonRestIds.size() - 1; i++) {
			System.out.println(collecitonRestIds.get(i).toString() + "===============");
			int ind1 = restArray.indexOf(collecitonRestIds.get(i).toString());
			int ind2 = restArray.indexOf(collecitonRestIds.get(i + 1).toString());

			if (ind1 > ind2) {
				Assert.assertTrue(false, "order is different");
			}
		}

	}

	@Test(dataProvider = "collectionValidationOnListing", description = "CollectionOnListing Validation", groups = {
			"regression", "Gaurav" })
	public void collectionValidationOnListing(String lat, String lng, String userAgent) {
		if (doMock) {
            setMockRequest("api/v1/aggregator/", "Aggregator");
		    proxyServerPOJO = new ProxyServerPOJO()
                    .withIpAddress("127.0.0.1")
                    .withApiEndPoint("/api/v1/aggregator/")
                    .withMockUrl("http://10.0.6.59:6666/")
                    .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

            updateMockEndPoint(proxyServerPOJO);


		}
        processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, doMock);
		String response = processor.ResponseValidator.GetBodyAsText();

		int i = 0;
		List<String> al = JsonPath.read(response, "$.data..cardType");
		for (String cartType : al) {

			if (cartType.equalsIgnoreCase("collection")) {
				Assert.assertNotNull(JsonPath.read(response, "$.data.cards[" + i + "].data.data.id"));
				return;
			}
			i++;
		}

        updateMockEndPoint(proxyServerPOJO.withDoMock("0"));

		Assert.assertTrue(false, "Collection card is not found");


	}

//	@Test(dataProvider = "collectionTypeValidationOnListing",description="Collection Type Validation", groups={"regression","Gaurav"})
//	public void collectionTypeValidationOnListing(String lat, String lng, String userAgent, String type) {
//		processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent);
//		String response = processor.ResponseValidator.GetBodyAsText();
//		if (type.equals("personalized")) {
//			createCollection(lat, lng, userAgent);
//			Assert.assertEquals(JsonPath.read(response, "$.data.cards[0].data.subtype"), "personalized");
//		}
//		if (type.equalsIgnoreCase("merchandise")) {
//			createMerchantiseCollection(lat, lng, userAgent);
//			Assert.assertEquals(JsonPath.read(response, "$.data.cards[0].data.subtype"), "merchandise");
//		}
//
//
//	}

	@Test(dataProvider = "carouselValidation", description = "Carousel Validation on listing", groups = { "regression",
			"Gaurav" })
	public void carouselValidation(String lat, String lng, String userAgent, String polygonId) {
		String id = rngHelper.createCarousel(userAgent, "static", "TopCarousel", "N/A", polygonID);
		processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, redirectToMock);
		String response = processor.ResponseValidator.GetBodyAsText();
		List<String> cardTypes1 = JsonPath.read(response, "$.data..cardType");
		int k = 0;
		for (String cardType : cardTypes1) {

			if (cardType.equals("carousel")) {
				softAssert.assertEquals(JsonPath.read(response, "$.data.cards[" + k + "].data.subtype"), "topCarousel");

				softAssert.assertTrue(
						JsonPath.read(response, "$.data.cards[" + k + "]..bannerId").toString().contains(id),
						"banner Id is not found in carousel");
				softAssert.assertAll();
			}
			k++;
		}
	}

	@Test(dataProvider = "restaurantValidation", description = "Restaurnat Validation on Listing", groups = {
			"regression", "prodSanity", "Gaurav" })
	public void restaurantValidation(String lat, String lng, String userAgent) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);
	    processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, redirectToMock);
		String response = processor.ResponseValidator.GetBodyAsText();
		rngHelper.errorCodeHandler(processor, "Restaurant Listing");
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		for (int i = 0; i < cardTypes.size(); i++) {
			if (cardTypes.get(i).toString().equalsIgnoreCase("restaurant")) {
				softAssert.assertEquals(JsonPath.read(response, "$.data.cards[" + i + "].data.type"), "restaurant",
						"SecondCard is not Restaurant Card");
				softAssert.assertNotNull(JsonPath.read(response, "$.data.cards[" + i + "].data..id"),
						"restaurant have Null ImageId");
				softAssert.assertNotNull(JsonPath.read(response, "$.data.cards[" + i + "].data..cloudinaryImageId"),
						"restaurant does not have cloudanaryImageId");
				softAssert.assertNotNull(JsonPath.read(response, "$.data.cards[" + i + "].data..cityState"),
						"city is not  coming for reataurant");

			}
		}

		softAssert.assertAll();

	}

	@Test(dataProvider = "restaurantUnserviceabilityValidtion", description = "Validation for Unserviceable Restaurants", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void restaurantUnserviceabilityValidtion(String lat, String lng, String userAgent) {
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		if(doMock){
            setMockRequest("api/v1/aggregator/", "Aggregator");
		    proxyServerPOJO = new ProxyServerPOJO()
                    .withIpAddress("127.0.0.1")
                    .withApiEndPoint("/api/v1/aggregator/")
                    .withMockUrl("http://10.0.6.59:6666/")
                    .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");

            updateMockEndPoint(proxyServerPOJO);


		}
		processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, doMock);
		String response = processor.ResponseValidator.GetBodyAsText();
		List<String> cardTypes = JsonPath.read(response, "$.data..subtype");
		List<String> fullMessage = null;
		for (int i1 = 0; i1 < cardTypes.size(); i1++) {
			if (cardTypes.get(i1).toString().equalsIgnoreCase("sortedRestaurantCount")) {
				fullMessage = JsonPath.read(response, "$.data.cards[" + i1 + "]..message");
				return;
			}
		}
		if (fullMessage.size() == 0) {
			Assert.assertTrue(false, "Count Message cards is not coming, All restaurant are unserviceable");
		} else {
			String[] message = fullMessage.get(0).toString().split(" ");
			String count = message[0];
			int totalCards = Integer.parseInt(count);
			List<String> al;
			HashSet hs = new HashSet();
			int restCard = 0;
			int pages = JsonPath.read(response, "$.data.pages");
			for (int i1 = 0; i1 <= pages - 1; i1++) {
				processor = rngHelper.getListing(lat, lng, new Integer(i1).toString(), tid, token);
				response = processor.ResponseValidator.GetBodyAsText();
				rngHelper.errorCodeHandler(processor, "Restaurant Listing");
				int j = -1;
				al = JsonPath.read(response, "$.data..cardType");
				for (String cardType : al) {
					j++;
					if (cardType.equalsIgnoreCase("restaurant")) {

						restCard++;
					}
					if (restCard == totalCards + 1) {

						List restStatus = JsonPath.read(response, "$.data.cards[" + j + "]..unserviceable");

						System.out.println(restStatus.get(0).toString());

						Assert.assertEquals(restStatus.get(0).toString(), "true");

					}

				}
				al.clear();
			}

		}
        updateMockEndPoint(proxyServerPOJO.withDoMock("0"));
	}

	@Test(dataProvider = "FYIBanner_Validation", description = "FYI Banner Validation on listing", groups = {
			"regression", "Gaurav" })
	public void fYIBannerValidation(String lat, String lng, String userAgent, String areaId, String versionCode,
			boolean check) throws IOException {

		if(doMock) {
			System.out.println(RngConstants.redisFYIPOPUPIndiaLevel);
			new RedisHelper().setValue("sandredisstage", 0, "POPUPV2_LISTING_INDIA", RngConstants.redisFYIPOPUPIndiaLevel);
		}
		processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, versionCode);
		String response = processor.ResponseValidator.GetBodyAsText();
		rngHelper.errorCodeHandler(processor, "Restaurant Listing");
		if (userAgent.equalsIgnoreCase("Swiggy-Android")) {
			if (check) {
				Assert.assertEquals(
						(JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.popup.type").toString()),
						"popup");
			} else {
				Assert.assertFalse(response.contains("popup"));
			}
		}
	}

	@Test(dataProvider = "favoritelistingValidation", description = "Favourite Listing field Level Validation", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void favoritelistingValidation(String lat, String lng, String userAgent) {

        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);

		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, redirectToMock);
		String response = processor.ResponseValidator.GetBodyAsText();

		List<String> al = rngHelper.getRestaurantsList(response);
		String restId = al.get(0).toString();
		Processor favoriteProcessor = rngHelper.getFavoriteProcessor(lat, lng, restId);
		String favoriteResponse = favoriteProcessor.ResponseValidator.GetBodyAsText();
		if (JsonPath.read(favoriteResponse, "$.statusMessage").toString().contains("Oops")) {
			Assert.assertTrue(false, "favorite APi is not working fine");
		} else {
			List restid = JsonPath.read(favoriteResponse, "$.data.cards..id");

			if (restid.size() == 0) {
				Assert.assertTrue(false, "No restaurant card is found in Favorite Lising");
			} else {
				Assert.assertTrue(restid.contains(restId));
			}
			int rest = 0;
			List<String> cartTypes = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..cardType");
			if (cartTypes.size() == 0) {
				Assert.assertTrue(false, "No card is found in Favorite Lising");
			} else {

				for (String cartType : cartTypes) {
					if (cartType.equalsIgnoreCase("TopCarusel")) {
						Assert.assertTrue(false);
					}
					if (cartType.equalsIgnoreCase("restaurant")) {
						rest++;
					}
				}
			}
		}
	}

	@Test(dataProvider = "favoriteListingWithWrongtid", description = "Favourite Listing Validation for wrong Tid", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void favoriteListingWithWrongtid(String lat, String lng, boolean inValidTid) {

		if (inValidTid) {
			String tid = "test";
			String token = "test";
			Processor processor = rngHelper.favoriteListingWithWrongtid(lat, lng, tid, token);
			String response = processor.ResponseValidator.GetBodyAsText();

			Assert.assertEquals(JsonPath.read(response, "$.statusCode").toString(), "0", "Invalid status code");

		}

	}

	@Test(dataProvider = "pageIndexValidation", description = "Page Index Validations ", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void pageIndexValidation(String lat, String lng) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);
	    int i = 0;
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		processor = rngHelper.getListing(lat, lng, new Integer(i).toString(), tid, token);
		softAssert.assertEquals(
				JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.pageIndex").toString(),
				new Integer(i).toString());
		i = i + 1;
		processor = rngHelper.getListing(lat, lng, new Integer(i).toString(), tid, token);
		softAssert.assertEquals(
				JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.pageIndex").toString(),
				new Integer(i).toString());
		softAssert.assertAll();
	}

	@Test(dataProvider = "filterValidations", description = "Apply Filter Validations ", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void filterValidations(String lat, String lng, String filter) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		Processor processor = rngHelper.getFilterProcessor(filter, lat, lng, "0", tid, token);
		String response = processor.ResponseValidator.GetBodyAsText();
		List cardType = JsonPath.read(response, "$.data..cardType");
		Iterator it = cardType.iterator();
		while (it.hasNext()) {

			if (it.next().toString().equals("restaurant")) {
				switch (filter) {
				case "SHOW_RESTAURANTS_WITH:Offers":
					Assert.assertNotNull(JsonPath.read(response, "$.data..tradeCampaignHeaders"));
					break;
				case "SHOW_RESTAURANTS_WITH:PureVeg":
					Assert.assertNotNull(JsonPath.read(response, "$.data.cards..veg"));
					break;
				case "CUISINES:NorthIndian":
					Assert.assertEquals(JsonPath.read(response, "$.data.cards[2]..cuisines").toString(),
							"[\"North Indian\"]");
					break;
				case "SHOW_RESTAURANTS_WITH:Swiggy+Assured":
					Assert.assertEquals(JsonPath.read(response, "$.data.cards[2]..select").toString(), "[true]");
					break;
				case "SHOW_RESTAURANTS_WITH:Pure Veg;Swiggy Assured":
					twoFilterValiator(response, "or");

				}
			} else {
				System.out.println("restaurantCard Not Fount");
			}
		}

	}

	public void twoFilterValiator(String response, String orAnd) {
		if (orAnd.equals("or")) {
			if (JsonPath.read(response, "$.data.cards[2]..veg").equals("true")
					|| JsonPath.read(response, "$.data.cards[2]..select").equals("true")) {
				Assert.assertTrue(true);
			} else {
				Assert.assertTrue(false);
			}

		}
		if (orAnd.equalsIgnoreCase("and")) {
			if (JsonPath.read(response, "$.data.cards[2]..veg").equals("true")
					|| JsonPath.read(response, "$.data.cards[2]..select").equals("true")) {
				Assert.assertTrue(true);
			} else {
				Assert.assertTrue(false);
			}
		}

	}

	@Test(dataProvider = "sortValidations", description = "Apply Sort Validations", groups = { "sanity", "prodSanity",
			"Gaurav" })
	public void sortValidations(String lat, String lng, String sort) {
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		Processor processor = rngHelper.getSortProcessor(sort, lat, lng, "0", tid, token);
		String response = processor.ResponseValidator.GetBodyAsText();
		List cardType = JsonPath.read(response, "$.data..cardType");
		Iterator it = cardType.iterator();
		while (it.hasNext()) {
			if (it.next().toString().equals("restaurant")) {
				switch (sort) {
				case "RELEVANCY":
					Assert.assertNotNull(JsonPath.read(response, "$.data.cards[2]..id"));
					break;
				case "DELIVERY_TIME":
					deliveryTime(response);
					break;
				case "RATING":
					rating(response);
					break;
				case "COST_FOR_TWO":
					costForTwo(response);
					break;
				}
			} else {
				System.out.println("restaurantCard Not Found");
			}
		}

	}

	public void costForTwoOld(String response) {
		String rest1 = JsonPath.read(response, "$.data.cards[2]..costForTwo").toString();
		String rest2 = JsonPath.read(response, "$.data.cards[3]..costForTwo").toString();

		float costForTwo_rest1 = Float.parseFloat(rest1.substring(1, rest1.length() - 1));

		float costForTwo_rest2 = Float.parseFloat(rest2.substring(1, rest2.length() - 1));
		if (costForTwo_rest1 <= costForTwo_rest2) {
			Assert.assertTrue(true);
		} else {
			Assert.assertTrue(false);
		}

	}

	public void costForTwo(String response) {
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		List<Float> actualData = new ArrayList<Float>();
		List<Float> expectedData;
		int i = 0;
		String rest1;
		for (String card : cardTypes) {

			if (card.equals("restaurant")) {
				rest1 = JsonPath.read(response, "$.data.cards[" + i + "].data.data.costForTwo").toString();
				float deliveryTime_rest1 = Float.parseFloat(rest1);
				actualData.add(deliveryTime_rest1);
			}
			i++;
		}
		expectedData = actualData;
		Collections.sort(expectedData);
		Assert.assertEquals(actualData, expectedData, "The Sorting does not happen. Please check");

	}

	public void deliveryTimeOld(String response) {
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		int i = 0;
		String rest1, rest2;
		for (String card : cardTypes) {
			i++;
			if (card.equals("restaurant"))

			{
				rest1 = JsonPath.read(response, "$.data.cards[" + i + "].data.data.deliveryTime").toString();
				rest2 = JsonPath.read(response, "$.data.cards[" + (i + 1) + "].data.data.deliveryTime").toString();
				float deliveryTime_rest1 = Float.parseFloat(rest1);

				float deliveryTime_rest2 = Float.parseFloat(rest2);
				if (deliveryTime_rest1 <= deliveryTime_rest2) {
					Assert.assertTrue(true);
					break;
				} else {
					Assert.assertTrue(false);
					break;
				}

			}
		}

	}

	public void deliveryTime(String response) {
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		List<Float> actualData = new ArrayList<Float>();
		List<Float> expectedData;
		int i = 0;
		String rest1, rest2;
		for (String card : cardTypes) {

			if (card.equals("restaurant")) {
				rest1 = JsonPath.read(response, "$.data.cards[" + i + "].data.data.deliveryTime").toString();
				float deliveryTime_rest1 = Float.parseFloat(rest1);
				actualData.add(deliveryTime_rest1);
			}
			i++;
		}
		expectedData = actualData;
		Collections.sort(expectedData);
		Assert.assertEquals(actualData, expectedData, "The Sorting does not happen. Please check");
	}

	public void ratingOld(String response) {
		String rest1 = JsonPath.read(response, "$.data.cards[2].data.data.avgRating").toString();
		String rest2 = JsonPath.read(response, "$.data.cards[3].data.data.avgRating").toString();

		float rating_rest1 = Float.parseFloat(rest1);

		float rating_rest2 = Float.parseFloat(rest2);
		if (rating_rest1 >= rating_rest2) {
			Assert.assertTrue(true);
		} else {
			Assert.assertTrue(false);
		}

	}

	public void rating(String response) {

		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		List<Float> actualData = new ArrayList<Float>();
		List<Float> expectedData;
		int i = 0;
		String rest1;
		for (String card : cardTypes) {

			if (card.equals("restaurant")) {
				rest1 = JsonPath.read(response, "$.data.cards[" + i + "].data.data.avgRating").toString();
				float deliveryTime_rest1 = Float.parseFloat(rest1);
				actualData.add(deliveryTime_rest1);
			}
			i++;
		}
		expectedData = actualData;
		Collections.sort(expectedData);
		Assert.assertEquals(actualData, expectedData, "The Sorting does not happen. Please check");

	}

	@Test(dataProvider = "dwebPagination", description = "Dweb Offset Validations", groups = { "sanity", "prodSanity",
			"Gaurav" })
	public void dwebPagination(String lat, String lng, String pageType, String offset, String pagelimit) {
		int i = 0;
//        updateMockEndPoint(proxyServerPOJO.withDoMock("0"));
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);
		HashMap<String, String> headers = rngHelper.getHeader();
		String tid = headers.get("Tid");
		String token = headers.get("Token");
		processor = rngHelper.getSeeAllProcessor(lat, lng, pageType, offset, pagelimit, tid, token);
		String response = processor.ResponseValidator.GetBodyAsText();
		rngHelper.errorCodeHandler(processor, "See All");
		softAssert.assertEquals(JsonPath.read(response, "$.data..currentOffset").toString(), "[0]");
		offset = "1";
		processor = rngHelper.getSeeAllProcessor(lat, lng, pageType, offset, pagelimit, tid, token);
		softAssert.assertEquals(
				JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..currentOffset").toString(), "[1]");
		softAssert.assertAll();
	}

	@Test(dataProvider = "dwebPagination", description = "Dweb card type Validaiton ", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void dwebValidations(String lat, String lng, String pageType, String offset, String pagelimit) {
		int i = 0;
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		processor = rngHelper.getSeeAllProcessor(lat, lng, pageType, offset, pagelimit);
		String response = processor.ResponseValidator.GetBodyAsText();
		List<String> cardTypes = JsonPath.read(response, "$.data.cards..cardType");
//		for (String cardType : cardTypes) {
//			if (cardType.equalsIgnoreCase("carousel") || cardType.equalsIgnoreCase("seeAllRestaurants")) {
//				Assert.assertTrue(true);
//
//			} else {
//				Assert.assertTrue(false);
//			}
//		}

        Assert.assertTrue(cardTypes.contains("carousel") || cardTypes.contains("seeAllRestaurants"),"Assertion Failed :: carousels and sellAllRest cards not found.");
		Assert.assertEquals(JsonPath.read(response, "$.data.pages").toString(), "1", "Pagination is happening in DWEB");

	}

	@Test(dataProvider = "dwebWithFilter", description = "Dweb with filter", groups = { "sanity", "prodSanity",
			"Gaurav" })
	public void dwebPaginationwithFilter(String lat, String lng, String pageType, String offset, String pagelimit,
			String filter) throws InterruptedException {
        setMockRequest("api/v1/aggregator/", "Aggregator");
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);

		Thread.sleep(1000);
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		Processor processor = rngHelper.getDwebProcessorWithFilter(lat, lng, pageType, offset, pagelimit, filter);
		String response = processor.ResponseValidator.GetBodyAsText();
		List cardType = JsonPath.read(response, "$.data..cardType");
		Iterator it = cardType.iterator();
		while (it.hasNext()) {

			if (it.next().toString().equals("restaurant")) {
				switch (filter) {
				case "SHOW_RESTAURANTS_WITH:Offers":
					Assert.assertNotNull(JsonPath.read(response, "$.data..tradeCampaignHeaders"));
					break;
				case "SHOW_RESTAURANTS_WITH:PureVeg":
					Assert.assertNotNull(JsonPath.read(response, "$.data.cards..veg"));
					break;
				case "CUISINES:NorthIndian":
					Assert.assertEquals(JsonPath.read(response, "$.data.cards[2]..cuisines").toString(),
							"[\"North Indian\"]");
					break;
				case "SHOW_RESTAURANTS_WITH:Swiggy+Assured":
					Assert.assertEquals(JsonPath.read(response, "$.data.cards[2]..select").toString(), "[true]");
					break;
				case "SHOW_RESTAURANTS_WITH:Pure Veg;Swiggy Assured":
					twoFilterValiator(response, "or");

				}
			} else {
				System.out.println("restaurantCard Not Fount");
			}
		}
	}

	@Test(dataProvider = "dwebWithSort", description = "DWEB With Sort Validation", groups = { "sanity", "prodSanity",
			"Gaurav" })
	public void dwebPaginationwithSort(String lat, String lng, String pageType, String offset, String pagelimit,
			String sort) {
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		Processor processor = rngHelper.getDwebProcessorWithSort(lat, lng, pageType, offset, pagelimit, sort);
		String response = processor.ResponseValidator.GetBodyAsText();
		List cardType = JsonPath.read(response, "$.data..cardType");
		Iterator it = cardType.iterator();
		while (it.hasNext()) {
			if (it.next().toString().equals("restaurant")) {
				switch (sort) {
				case "RELEVANCY":
					Assert.assertNotNull(JsonPath.read(response, "$.data.cards[2]..id"));
					break;
				case "DELIVERY_TIME":
					deliveryTime(response);
					break;
				case "RATING":
					rating(response);
					break;
				case "COST_FOR_TWO":
					costForTwo(response);
					break;
				}
			} else {
				System.out.println("restaurantCard Not Found");
			}
		}

	}

	@Test(dataProvider = "dwebCollectionwithFilter", description = "Dweb Colleciton with Filter Validation", groups = {
			"sanity", "Gaurav" })
	public void dwebCollectionwithFilter(String lat, String lng, String pageType, String offset, String pagelimit,
			String filter) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);

		String collectionId = rngHelper.createCollection(lat, lng, "web", 1);
		processor = rngHelper.getSeeAllProcessorwithFilter(lat, lng, pageType, collectionId, offset, pagelimit, filter);
		String response = processor.ResponseValidator.GetBodyAsText();
		List cardType = JsonPath.read(response, "$.data..cardType");
		Iterator it = cardType.iterator();
		while (it.hasNext()) {

			if (it.next().toString().equals("restaurant")) {
				switch (filter) {
				case "SHOW_RESTAURANTS_WITH:Offers":
					Assert.assertNotNull(JsonPath.read(response, "$.data..tradeCampaignHeaders"));
					break;
				case "SHOW_RESTAURANTS_WITH:PureVeg":
					Assert.assertNotNull(JsonPath.read(response, "$.data.cards..veg"));
					break;
				case "CUISINES:NorthIndian":
					Assert.assertEquals(JsonPath.read(response, "$.data.cards[2]..cuisines").toString(),
							"[\"North Indian\"]");
					break;
				case "SHOW_RESTAURANTS_WITH:Swiggy+Assured":
					Assert.assertEquals(JsonPath.read(response, "$.data.cards[2]..select").toString(), "[true]");
					break;
				case "SHOW_RESTAURANTS_WITH:Pure Veg;Swiggy Assured":
					twoFilterValiator(response, "or");

				}
			} else {
				System.out.println("restaurantCard Not Fount");
			}
		}
	}

	@Test(dataProvider = "dwebWithSort", description = "Dweb With Sort Validation", groups = { "sanity", "Gaurav" })
	public void dwebCollectionwithSort(String lat, String lng, String pageType, String offset, String pagelimit,
			String sort) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);
		HashMap hm = rngHelper.getHeader();
		Processor processor = rngHelper.getDwebProcessorWithSort(lat, lng, pageType, offset, pagelimit, sort);
		String response = processor.ResponseValidator.GetBodyAsText();
		List cardType = JsonPath.read(response, "$.data..cardType");
		Iterator it = cardType.iterator();
		while (it.hasNext()) {
			if (it.next().toString().equals("restaurant")) {
				switch (sort) {
				case "RELEVANCY":
					Assert.assertNotNull(JsonPath.read(response, "$.data.cards[2]..id"));
					break;
				case "DELIVERY_TIME":
					deliveryTime(response);
					break;
				case "RATING":
					rating(response);
					break;
				case "COST_FOR_TWO":
					costForTwo(response);
					break;
				}
			} else {
				System.out.println("restaurantCard Not Found");
			}
		}

	}

	@Test(dataProvider = "RestaurantDuplicatValidation", description = "Restaurant Dublication Validation", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void RestaurantDuplicatValidation(String lat, String lng) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);
	    int i1 = 0;
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		processor = rngHelper.getListing(lat,  lng, new Integer(i1).toString(), tid, token);
		String response = processor.ResponseValidator.GetBodyAsText();
		HashSet hs = new HashSet();
		ArrayList al = new ArrayList();
		int pages = JsonPath.read(response, "$.data.pages");
		for (int i = 0; i <= pages - 1; i++) {
			processor = rngHelper.getListing(lat, lng, new Integer(i).toString(), tid, token);
			response = processor.ResponseValidator.GetBodyAsText();

			al = JsonPath.read(response, "$.data..cardType");

			for (int j = 1; j < al.size(); j++) {
				String cardType = JsonPath.read(response, "$.data.cards[" + j + "].cardType").toString();
				if (cardType.equals("restaurant")) {
					System.out.println(j + "************");
					String foundRestID = JsonPath.read(response, "$.data.cards[" + j + "]..id").toString();
					System.out.println(foundRestID);
					boolean b = hs.add(JsonPath.read(response, "$.data.cards[" + j + "]..id"));
					System.out.println(b);
					if (b == false) {

						Assert.assertTrue(false);
					}
				}

			}
			al.clear();
		}

	}

	@Test(dataProvider = "tIDSIDDeviceIDValidation", description = "Tid Sid and Device Id validaiton on Each page", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void tIDSIDDeviceIDValidation(String lat, String lng) {
		int i1 = 0;
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		processor = rngHelper.getListing(lat, lng, new Integer(i1).toString(), tid, token);
		String response = processor.ResponseValidator.GetBodyAsText();
		int pages = JsonPath.read(response, "$.data.pages");
		String tID_page0 = JsonPath.read(response, "$.tid");
		String sid = JsonPath.read(response, "$.sid");
		String deviceId = JsonPath.read(response, "$.deviceId");

		if (!tid.equalsIgnoreCase(tID_page0)) {
			Assert.assertTrue(false, "Login TID is not same as page 0 tid");
		} else {

			for (int i = 1; i <= pages - 1; i++) {

				processor = rngHelper.getListing(lat, lng, new Integer(i).toString(), tid, token);
				response = processor.ResponseValidator.GetBodyAsText();
				String tid_page = JsonPath.read(response, "$.tid");
				String sid_page = JsonPath.read(response, "$.sid");
				String deviceId_page = JsonPath.read(response, "$.deviceId");
				System.out.println(tid_page + "===================++++++++++++++====================" + tid);

				if (!tid_page.equalsIgnoreCase(tid)) {
					Assert.assertTrue(false);
				}
				System.out.println(sid_page + "===================++++++++++++++====================" + sid);

				if (!sid_page.equalsIgnoreCase(sid)) {
					Assert.assertTrue(false);
				}

				System.out.println(deviceId_page + "===================++++++++++++++====================" + deviceId);

				if (!deviceId_page.equalsIgnoreCase(deviceId)) {
					Assert.assertTrue(false);
				}

			}
		}

	}

	@Test(dataProvider = "offerListingValidation", description = "Offer Listing validations", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void offerListingValiations(String lat, String lng, String source_page, String tid) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);

		if (tid.equalsIgnoreCase("valid")) {
			HashMap hm = rngHelper.getHeader();
			tid = hm.get("Tid").toString();
			processor = rngHelper.getOfferListingProcessor(lat, lng, source_page, tid);
			String response = processor.ResponseValidator.GetBodyAsText();

			if (lat.equals("") || "null".equals(lat) || lat.equals("test") || lng.equals("") || "null".equals(lng)
					|| lng.equals("test")) {
				Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(),
						"Invalid latitude/longitude");
				return;
			}

			List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
			if (source_page.equalsIgnoreCase("cart")) {
				if (!cardTypes.get(0).toString().equalsIgnoreCase("couponApplyWidget")) {
					Assert.assertTrue(false, "First card is not couponApplyWidget");
				}

			}
			for (int i = 1; i < cardTypes.size(); i++) {
				System.out.println(cardTypes.get(i).toString() + "==========================");
				if (!cardTypes.get(i).toString().equalsIgnoreCase("messageCard")
						&& !cardTypes.get(i).toString().equalsIgnoreCase("couponCard")) {
					Assert.assertTrue(false);
				}
			}
			String response_tid = JsonPath.read(response, "$.tid");
			if (!tid.equalsIgnoreCase(response_tid)) {
				Assert.assertTrue(false, "requested tid is different than response tid");
			}

		} else {
			tid = "f48a08f3-224b-4261-b824-0765b7cf06";
			processor = rngHelper.getOfferListingProcessor(lat, lng, source_page, tid);
			String response = processor.ResponseValidator.GetBodyAsText();
			List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
			for (String s : cardTypes) {
				if (s.equalsIgnoreCase("couponCard")) {
					Assert.assertTrue(false, "there is coupon card for invalid tid also on offer listing");
				}
			}

			String response_tid = JsonPath.read(response, "$.tid");
			if (!tid.equalsIgnoreCase(response_tid)) {
				Assert.assertTrue(false, "requested tid is different than response tid");
			}

		}

	}

	@Test(dataProvider = "OfferListingFieldValidation", description = "Offer Listing Field level validations", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void OfferListingFieldValidation(String lat, String lng, String source_page) {

		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		processor = rngHelper.getOfferListingProcessor(lat, lng, source_page, tid);
		String response = processor.ResponseValidator.GetBodyAsText();

		if (lat.equalsIgnoreCase("") || lat.equalsIgnoreCase(null) || lat.equalsIgnoreCase("test")
				|| lng.equalsIgnoreCase("") || lng.equalsIgnoreCase(null) || lng.equalsIgnoreCase("test")
				|| source_page.equalsIgnoreCase(null)) {
			int code = JsonPath.read(response, "$.statusCode");
			if ((code != 0) && ((List) JsonPath.read(response, "$.data.cards")).size() != 0) {
				Assert.assertTrue(false, " in case of Invalid lat long, Proper message is not displaying");
			}
		}

	}

	@Test(dataProvider = "slugListing", description = "Slug Listing validations", groups = { "sanity", "prodSanity",
			"Gaurav" })
	public void slugListing(String area) {
        if(area.equalsIgnoreCase("btm")) {
            setMockGetRequest("api/v1/restaurants/area/btm", "SlugListingBTM");
        }
		processor = rngHelper.getSlugListingProcessor(area);
		String response = processor.ResponseValidator.GetBodyAsText();
		int responseCode = JsonPath.read(response, "$.statusCode");
		if (area.equalsIgnoreCase("null") || area.equalsIgnoreCase(" ") || area.equalsIgnoreCase("test")
				|| area.equalsIgnoreCase("2.2")) {
			Assert.assertEquals(responseCode, 0, "for invalid areaid, status code is different");
		} else {
			List<String> cardTypes = JsonPath.read(response, "$.data.cards..cardType");
			for (String cartType : cardTypes) {
				Assert.assertEquals(cartType, "restaurant", "All Restaurants are not restaurant type");
			}

		}
	}

//	deprecated test case, not valid now as there are lot of nux cards that are visible to new and old user
//	@Test(dataProvider = "nuxCardValidation", description = "Nux Card validations", groups = { "sanity", "Gaurav" })
	public void nuxCardValidation(String lat, String lng, boolean firstUser) {
		String tid;
		String token;
		int i1 = 0;
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);

		if (firstUser == true) {
			tid = "322470f0-44f9-af9a-fffef9c46454";
			token = "bf5722b4-94f7-480b-8a4c-9e7bf9c662a8883f5f7c-f25e-47f1-a0cd-b1f1d3a1dc20";
			processor = rngHelper.getListing(lat, lng, new Integer(i1).toString(), tid, token);
			String response = processor.ResponseValidator.GetBodyAsText();

			List cardTypes = JsonPath.read(response, "$.data..cardType");
			if (cardTypes.contains("nuxCard")) {
				Assert.assertTrue(true);
			}
		} else {

			HashMap hm = rngHelper.getHeader();
			tid = hm.get("Tid").toString();
			token = hm.get("Token").toString();
			processor = rngHelper.getListing(lat, lng, new Integer(i1).toString(), tid, token);
			int i = 20;
			processor = rngHelper.getListing(lat, lng, new Integer(i1).toString(), tid, token);

			String response = processor.ResponseValidator.GetBodyAsText();

			List cardTypes = JsonPath.read(response, "$.data..cardType");
			for (int j = 0; j < cardTypes.size(); j++) {
				if (cardTypes.get(j).toString().equalsIgnoreCase("nuxCard")) {
					Assert.assertTrue(false, "NuxCard is coming for existing user also");
				}
			}
		}
	}

	@Test(dataProvider = "popListing", description = "Pop Listing validations", groups = {"sanity","prodSanity", "Gaurav"})
	public void popListing(String lat, String lng, String pageType) {
		String uri = "api/v1/pop/aggregator", responseFile = "POPListingMockResponse";
		try {
			if (doMock) {
                setMockRequest(uri, responseFile);
			    proxyServerPOJO = new ProxyServerPOJO()
                        .withIpAddress("127.0.0.1")
                        .withApiEndPoint("/"+uri)
                        .withMockUrl("http://10.0.6.59:6666/")
                        .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");

                updateMockEndPoint(proxyServerPOJO);

			}

			Processor processor = rngHelper.getPopListingProcessor(lat, lng, pageType, doMock);
			String response = processor.ResponseValidator.GetBodyAsText();
			String statusCode = JsonPath.read(response, "$.statusCode").toString();
			if (statusCode.equalsIgnoreCase("0")) {
				Assert.assertTrue(false, "PopListing request is failed");
			} else {
				Assert.assertTrue(JsonPath.read(response, "$.data.configs.popMeta.launchMessage.type").toString()
						.equalsIgnoreCase("PopMetaConfig"), "Assertion failed, pop meta doesn't exists");
				Assert.assertNotNull(JsonPath.read(response, "$.data.configs.popMeta.launchMessage.imageUrl"),
						"Assertion failed, pop meta image url is null");
				Assert.assertFalse(
						JsonPath.read(response, "$.data.configs.popMeta.launchMessage.imageUrl").toString().isEmpty(),
						"Assertion failed, pop meta image url is empty");
				List cardTypes = JsonPath.read(response, "$.data..cardType");
				Assert.assertTrue(cardTypes.contains("messageWithImageCard"), "Swiggy pop unavailable");
				Assert.assertTrue(cardTypes.contains("popItem"), "Swiggy pop items unavailable");
				List cardsData = JsonPath.read(response, "$.data.cards.*.data");
				Assert.assertTrue(cardTypes.size() == cardsData.size());
				JsonArray cards = new JsonParser().parse(response).getAsJsonObject().get("data").getAsJsonObject()
						.get("cards").getAsJsonArray();
				for (int i = 0; i < cards.size(); i++) {
					JsonObject card = cards.get(i).getAsJsonObject();
					if (card.get("cardType").getAsString().equalsIgnoreCase("popItem")) {
						JsonObject data = card.get("data").getAsJsonObject().get("data").getAsJsonObject();
						Assert.assertNotNull(data.get("id").getAsString(), "POP item id not available");
						Assert.assertNotNull(data.get("name").getAsString(), "POP item name not available");
						Assert.assertNotNull(data.get("description").getAsString(),
								"POP item description not available");
						Assert.assertNotNull(data.get("price").getAsString(), "POP item price not available");
					}
				}
			}
		} catch (Exception e) {

			Assert.assertTrue(false, "API is down or not deployed");
		}finally {
            updateMockEndPoint(proxyServerPOJO.withDoMock("0"));
        }
	}

	@Test(dataProvider = "popListing", description = "Pop Listing validations", groups = { "sanity", "prodSanity",
			"Gaurav" })
	public void popListingExtendedMessageCard(String lat, String lng, String pageType) {
		String uri = "api/v1/pop/aggregator", responseFile = "POPListingExtendedCardMockResponse";
		try {
			if (doMock) {
				setMockRequest(uri, responseFile);
				proxyServerPOJO = new ProxyServerPOJO()
						.withIpAddress("127.0.0.1")
						.withApiEndPoint("/"+uri)
						.withMockUrl("http://10.0.6.59:6666/")
						.withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");

				updateMockEndPoint(proxyServerPOJO);

			}
			Processor processor = rngHelper.getPopListingProcessor(lat, lng, pageType, doMock);
			String response = processor.ResponseValidator.GetBodyAsText();
			String statusCode = JsonPath.read(response, "$.statusCode").toString();
			if (statusCode.equalsIgnoreCase("0")) {
				Assert.assertTrue(false, "PopListing request is failed");
			} else {
				List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
				if (!cardTypes.contains("extendedMessageCard")) {
					Assert.assertTrue(false, "Pop listing does not have extendedMessageCard");
				} else {
					for (int i = 0; i < cardTypes.size(); i++) {
						if (cardTypes.get(i).toString().equalsIgnoreCase("extendedMessageCard")) {
							Assert.assertNotNull(JsonPath.read(response, "$.data.cards[0].data.data"),
									"ExtendMessage card should not null");
						}
					}
				}
			}
		} catch (Exception e) {
			Assert.assertTrue(false, "API is down or not deployed");
		}finally {
            updateMockEndPoint(proxyServerPOJO.withDoMock("0"));
        }
	}

	@Test(dataProvider = "popListingValidation", description = "Pop Listing Field level validation", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void popListingValidation(String lat, String lng, String pageType) {
		String uri = "api/v1/pop/aggregator", responseFile = "POPListingMockResponse";

		if (doMock) {
            setMockRequest(uri, responseFile);
		    proxyServerPOJO = new ProxyServerPOJO()
                    .withIpAddress("127.0.0.1")
                    .withApiEndPoint("/"+uri)
                    .withMockUrl("http://10.0.6.59:6666/")
                    .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");

            updateMockEndPoint(proxyServerPOJO);

		}
		Processor processor = rngHelper.getPopListingProcessor(lat, lng, pageType, doMock);
		String response = processor.ResponseValidator.GetBodyAsText();
		if (pageType.equalsIgnoreCase("")){
		    Assert.assertTrue((int) JsonPath.read(response, "$.statusCode") == 1, "Assertion Failed, response should come when page_type is empty");
        } else if(pageType.equalsIgnoreCase("test") || pageType.equalsIgnoreCase("null")) {
		    try {
                Assert.assertTrue((int) JsonPath.read(response, "$.status") == 500, "Expecting internal server error, since page_type is a enum");
            }catch (PathNotFoundException e) {
                Assert.assertTrue((int) JsonPath.read(response, "$.statusCode") == 0, "Assertion failed, expecting status code as 0 and message as invalid page type");
            }
		}

		if (lat.equalsIgnoreCase("null") || lat.equalsIgnoreCase("") || lat.equalsIgnoreCase("test")
				|| lng.equalsIgnoreCase("null") || lng.equalsIgnoreCase("") || lng.equalsIgnoreCase("test")) {
			try {
				int i = JsonPath.read(response, "$.statusCode");

				System.out.println(i + "=============");
				if (i != 0) {
					Assert.assertTrue(false, "Invalid Status code when lat,lng,pagetype is wrong");
				}
			} catch (Exception e) {
				Assert.assertTrue(false, "Pop Api is down");
			}
		}
        updateMockEndPoint(proxyServerPOJO.withDoMock("0"));
	}

	@Test(dataProvider = "dishDiscoveryValidation", description = "Dish Discovery validations", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void dishDiscoveryValidation(String lat, String lng) {
		int i1 = 0;
		String tid = "Test";
		String token = "Test";
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");

        updateMockEndPoint(proxyServerPOJO);
		processor = rngHelper.getListing(lat, lng, new Integer(i1).toString(), tid, token);
		String response = processor.ResponseValidator.GetBodyAsText();

		List al = JsonPath.read(response, "$.data..cardType");

		for (int j = 0; j < al.size(); j++) {
			String cardType = JsonPath.read(response, "$.data.cards[" + j + "].cardType").toString();
			if (cardType.equals("storyCollection")) {
				Assert.assertNotNull(JsonPath.read(response, "$.data.cards[" + j + "]..cards").toString(),
						"Cards in Story Collection is Empty");
				Assert.assertEquals(JsonPath.read(response, "$.data.cards[4].data.type"), "storyCollection");
			}

		}

	}

	@Test(dataProvider = "dishDiscoveryFieldLevelValidations", description = "Dish Discovery field level validation", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void dishDiscoveryFieldLevelValidations(String lat, String lng, String type, String parentCollectionId,
			String collecitonId) {
		Processor processor = rngHelper.getDishDiscoveryCollectionProcessor(lat, lng, type, parentCollectionId,
				collecitonId);
		String response = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		if (lat.equalsIgnoreCase(null) || lat.equalsIgnoreCase("") || lat.equalsIgnoreCase("test")
				|| lng.equalsIgnoreCase(null) || lng.equalsIgnoreCase("") || lng.equalsIgnoreCase("test")
				|| collecitonId.equalsIgnoreCase(null) || collecitonId.equalsIgnoreCase("")
				|| collecitonId.equalsIgnoreCase("test") || parentCollectionId.equalsIgnoreCase(null)
				|| parentCollectionId.equalsIgnoreCase("") || parentCollectionId.equalsIgnoreCase("test")
				|| type.equalsIgnoreCase(null) || type.equalsIgnoreCase("") || type.equalsIgnoreCase("test")) {
			if (statusCode == 0) {
				Assert.assertTrue(true);
			} else {
				List cards = JsonPath.read(response, "$.data.cards");
				if (cards.size() == 0)
					Assert.assertTrue(true);
				else
					Assert.assertFalse(true, "Cards shouldn't come in response for invalida lat long");
			}
		}

	}

	@Test(dataProvider = "cacheValidation", description = "Cache validation", groups = { "sanity", "prodSanity",
			"Gaurav" })
	public void cacheValidtion(String lat, String lng, String filter) {
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		Processor processor = rngHelper.getFilterProcessor(filter, lat, lng, "0", tid, token);
		String response = processor.ResponseValidator.GetBodyAsText();
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		for (String cartType : cardTypes) {
			if (cartType.equals("TopCarousel")) {
				Assert.assertTrue(false);
			}
		}

	}

	@Test(dataProvider = "messageCardValidation", description = "Message Cards and Restaurant validation", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void messageCardValidation(String lat, String lng, String userAgent) {
		int i = 0;
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		if(doMock){
            setMockRequest("api/v1/aggregator/", "Aggregator");
		    proxyServerPOJO = new ProxyServerPOJO()
                    .withIpAddress("127.0.0.1")
                    .withApiEndPoint("/api/v1/aggregator/")
                    .withMockUrl("http://10.0.6.59:6666/")
                    .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");

            updateMockEndPoint(proxyServerPOJO);

		}
		processor = rngHelper.getListing(lat, lng, new Integer(i).toString(), userAgent, tid, token, doMock);
		String response = processor.ResponseValidator.GetBodyAsText();
		List<String> cardTypes = JsonPath.read(response, "$.data..subtype");
		List<String> fullMessage = null;
		for (int i1 = 0; i1 < cardTypes.size(); i1++) {
			if (cardTypes.get(i1).toString().equalsIgnoreCase("sortedRestaurantCount")) {
				fullMessage = JsonPath.read(response, "$.data.cards[" + i1 + "]..message");
				return;
			}
		}
		if (fullMessage.size() == 0) {
			Assert.assertTrue(false, "Count Message cards is not coming, All restaurant are unserviceable");
		} else {
			String[] message = fullMessage.get(0).toString().split(" ");
			String count = message[0];
			int totalCards = Integer.parseInt(count);
			List<String> al;
			HashSet hs = new HashSet();
			int restCard = 0;
			int pages = JsonPath.read(response, "$.data.pages");
			for (int i1 = 0; i1 <= pages - 1; i1++) {
				processor = rngHelper.getListing(lat, lng, new Integer(i1).toString(), tid, token);
				response = processor.ResponseValidator.GetBodyAsText();
				int j = -1;
				al = JsonPath.read(response, "$.data..cardType");
				for (String cardType : al) {
					j++;
					if (cardType.equalsIgnoreCase("restaurant")) {

						restCard++;
					}
					if (restCard == totalCards + 1) {

						List restStatus = JsonPath.read(response, "$.data.cards[" + j + "]..unserviceable");
						Assert.assertEquals(restStatus.get(0).toString(), "true",
								"Serviceable restaurants are more than the Message card count");

					}

				}
				al.clear();
			}
		}
        updateMockEndPoint(proxyServerPOJO.withDoMock("0"));
	}

	@Test(dataProvider = "bigCardUpload", description = "Big Card validation", groups = { "sanity",
			"Gaurav" }, enabled = false)
	public void bigCardUpload(String file_url, String lat, String lng, String restId, String type, boolean isBigCards) {
		String response = rngHelper.getBigCardProcessor(file_url).ResponseValidator.GetBodyAsText();
		Assert.assertEquals(JsonPath.read(response, "$.statusCode").toString(), "0", "Status Code is differnt");
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		processor = rngHelper.getListing(lat, lng, "0", tid, token);
		String listingResponse = processor.ResponseValidator.GetBodyAsText();
		int i = 0;
		List<String> cardTypes = JsonPath.read(listingResponse, "$.data.cards..cardType");
		for (String cardType : cardTypes) {
			i++;
			if (cardType.equalsIgnoreCase("restaurant")) {
				if (JsonPath.read(listingResponse, "$.data.cards[" + i + "].data..id").toString()
						.equalsIgnoreCase(restId)) {
					if (isBigCards) {
						Assert.assertEquals(JsonPath.read(listingResponse, "$.data.cards[11]..subtype"), type,
								"restaurat is not enriched");
					} else {
						Assert.assertEquals(JsonPath.read(listingResponse, "$.data.cards[11]..subtype"), "basic",
								"restaurat is not enriched");
					}

				}
			}
		}

	}

	public Map<Object, Object> getPreOrderTimeSlot(String lat, String lng) {

		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		Map<Object, Object> dates = null;
		String response = rngHelper.getListing(lat, lng, "0", tid, token).ResponseValidator.GetBodyAsText();
		List<String> preOrderList = JsonPath.read(response, "$.data.preorderSlots");
		if (preOrderList.size() == 0) {
			Assert.assertTrue(false, "PreOrder is not configure on this area");
		} else {
			dates = JsonPath.read(response, "$.data.preorderSlots[1].slots[3]");
		}
		return dates;

	}

	public Map<Object, Object> getPreOrderTimeSlot(String lat, String lng, boolean redirectToMock) {
        redirectToMock= false;
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		Map<Object, Object> dates = null;
        String response;
		if(redirectToMock){
            response = rngHelper.getListing(lat, lng, "0", tid, token, redirectToMock).ResponseValidator.GetBodyAsText();
        } else {
            response = rngHelper.getListing(lat, lng, "0", tid, token).ResponseValidator.GetBodyAsText();
        }
		List<String> preOrderList = JsonPath.read(response, "$.data.preorderSlots");
		if (preOrderList.size() == 0) {
			Assert.assertTrue(false, "PreOrder is not configure on this area");
		} else {
			dates = JsonPath.read(response, "$.data.preorderSlots[0].slots[3]");
		}
		return dates;

	}

	@Test(dataProvider = "preOrderValidations", description = "Pre Order validations", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void preOrderValidation(String lat, String lng, boolean preOrderEnabled) {
		if (preOrderEnabled) {
		    if(doMock){
                setMockRequest("api/v1/aggregator/","Aggregator");
		        proxyServerPOJO = new ProxyServerPOJO()
                        .withIpAddress("127.0.0.1")
                        .withApiEndPoint("/api/v1/aggregator/")
                        .withMockUrl("http://10.0.6.59:6666/")
                        .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");

                updateMockEndPoint(proxyServerPOJO);

            }
			Map<Object, Object> dates = getPreOrderTimeSlot(lat, lng, doMock);
			String start_time = dates.get("startTime").toString();
			String end_time = dates.get("endTime").toString();
			String response = rngHelper.getPreOrderProcessor(lat, lng, start_time, end_time, doMock).ResponseValidator
					.GetBodyAsText();
			List cards = JsonPath.read(response, "$.data.cards");
			if (cards.size() == 0) {
				Assert.assertTrue(false);
			} else {
				Assert.assertTrue(true);
			}

		} else {
			HashMap hm = rngHelper.getHeader();
			String tid = hm.get("Tid").toString();
			String token = hm.get("Token").toString();
			String response = rngHelper.getListing(lat, lng, "0", tid, token).ResponseValidator.GetBodyAsText();
			Assert.assertEquals(JsonPath.read(response, "$.data..preorderSlots"), "[]", "testing");

		}
        updateMockEndPoint(proxyServerPOJO.withDoMock("0"));
	}

	@Test(dataProvider = "preOrderFieldValidations", description = "Pre Order Field level validations", groups = {
			"sanity", "prodSanity", "Gaurav" })
	public void preOrderFieldValidations(String lat, String lng, String start_time, String end_time) {
		Processor processor = rngHelper.getPreOrderProcessor(lat, lng, start_time, end_time, false);
		String responseCode = "" + processor.ResponseValidator.GetResponseCode();
		// String response = processor.ResponseValidator.GetBodyAsText();

		if (start_time == "null" || end_time == "null" || start_time == "test" || end_time == "test")
			Assert.assertEquals(responseCode, "400");

		else if (lat.equalsIgnoreCase("null") || lat.equalsIgnoreCase("test") || lng.equalsIgnoreCase("null")
				|| lng.equalsIgnoreCase("test")) {

			String statusMessage = processor.ResponseValidator.GetNodeValue("$.statusMessage");
			Assert.assertEquals(statusMessage, "Invalid latitude/longitude");
			Assert.assertEquals(responseCode, "200", "Valid data failed");

		} else {
			Assert.assertEquals(responseCode, "200", "Valid data failed");
		}

//		if (responseCode == 400) {
//			Assert.assertTrue(false, "Invalid StartDate and EndTime");
//		} else {
//
//			if (lat.equalsIgnoreCase("null") || lat.equalsIgnoreCase("test") ||
//					lng.equalsIgnoreCase("null") || lng.equalsIgnoreCase("test")) {
//
//				if(JsonPath.read(response, "$.statusCode").toString().equalsIgnoreCase("1"))
//				{
//					Assert.assertTrue(false,"working fine for invalid lat and lng");
//				}else {
//					Assert.assertEquals(JsonPath.read(response, "$.errorCode").toString(), "321", "working fine for invalid lat and long");
//				}
//
//			}
//		}
	}

	@Test(dataProvider = "dedoopingValidation", description = "Restaurant Dedooping validations", groups = { "sanity",
			"prodSanity", "Gaurav" })
	public void dedoopingValidation(String lat, String lng) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);

		String[] payload = new String[] { lat, lng };
		String aggreResponse = sndHelper.aggregator(payload).ResponseValidator.GetBodyAsText();
		List<String> restIds = JsonPath.read(aggreResponse, "$.data.sortedRestaurants..restaurantId");
		List<Object> parentIds = new ArrayList<>();
		for (int i = 0; i < restIds.size(); i++) {
			Object o = JsonPath.using(Configuration.defaultConfiguration().setOptions(Option.SUPPRESS_EXCEPTIONS))
					.parse(aggreResponse).read("$.data.restaurants." + restIds.get(i) + ".parentId");
			parentIds.add(o);
		}

		HashMap<String, List<String>> hmap = new HashMap<>();
		for (int i = 0; i < restIds.size(); i++) {
			if (parentIds.get(i) != null) {
				if (hmap.containsKey(parentIds.get(i).toString())) {
					List<String> temp = new ArrayList<String>();
					temp = hmap.get(parentIds.get(i).toString());
					temp.add(restIds.get(i).toString());
					hmap.put(parentIds.get(i).toString(), temp);
				} else {
					List<String> temp2 = new ArrayList<String>();
					temp2.add(restIds.get(i).toString());
					hmap.put(parentIds.get(i).toString(), temp2);
				}
			}
		}
		System.out.println(hmap);
		HashMap<String, List<String>> SameParentRestIds = new HashMap<>();
		for (Map.Entry<String, List<String>> entry : hmap.entrySet()) {
			List<String> s = entry.getValue();
			if (s.size() > 1) {
				SameParentRestIds.put(entry.getKey(), entry.getValue());
			}
		}
		System.out.println(SameParentRestIds);
		for (Map.Entry<String, List<String>> entry : SameParentRestIds.entrySet()) {
			List<String> restId = entry.getValue();
			if (restId.size() == 2) {
				String searchKey = restId.get(0);
				HashMap hm = rngHelper.getHeader();
				String tid = hm.get("Tid").toString();
				String token = hm.get("Token").toString();
				String response = rngHelper.getListing(lat, lng, "0", tid, token).ResponseValidator.GetBodyAsText();
				ArrayList<String> al = new ArrayList();
				int pages = JsonPath.read(response, "$.data.pages");
				for (int i1 = 0; i1 <= pages - 1; i1++) {
					processor = rngHelper.getListing(lat, lng, new Integer(i1).toString(), tid, token);
					response = processor.ResponseValidator.GetBodyAsText();
					int j = -1;
					al = JsonPath.read(response, "$.data..cardType");
					for (String cardType : al) {
						j++;
						if (cardType.equalsIgnoreCase("restaurant")) {

							String id = JsonPath.read(response, "$.data.cards[" + j + "].data.data.id");
							if (id.equalsIgnoreCase(searchKey)) {
								List<String> childIds = JsonPath.read(response,
										"$.data.cards[" + j + "].data.data.chain..id");
								if (childIds.contains(restId.get(1).toString())) {
									Assert.assertTrue(true);
									return;
								} else {
									Assert.assertTrue(false);
								}
							}
						}

					}

				}
				Assert.assertTrue(false, "dedooping is not foud");

			}
		}

	}

	@Test(dataProvider = "carouselRestaurantValidation", description = "Restaurant based carousel validations", groups = {"sanity", "Gaurav" })
	public void carouselRestaurantValidation(String lat, String lng, String userAgent, String polygonId) {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		String response = rngHelper.getListing(lat, lng, "0", tid, token).ResponseValidator.GetBodyAsText();
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");

		String restIds = "";
		String restId1 = "";
		String restId2 = "";
		for (int i = 0; i < cardTypes.size(); i++) {

			if (cardTypes.get(i).toString().equals("restaurant")) {
				restId1 = JsonPath.read(response, "$.data.cards[" + i + "].data.data.id");
				restId2 = JsonPath.read(response, "$.data.cards[" + (i + 1) + "].data.data.id");
				restIds = restId1 + "," + restId2;
				break;
			}
		}
		String id = rngHelper.createCarousel(userAgent, "restaurant", "TopCarousel", restIds, polygonId);
		processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, redirectToMock);
		String carouselResponse = processor.ResponseValidator.GetBodyAsText();
		List<String> cardTypes1 = JsonPath.read(carouselResponse, "$.data..cardType");

		int k = 0;
		for (String cardType : cardTypes1) {

			if (cardType.equals("carousel")) {
				softAssert.assertTrue(
						JsonPath.read(carouselResponse, "$.data.cards[" + k + "]..bannerId").toString().contains(id),
						"banner Id is not found in carousel");
				List<String> test1 = JsonPath.read(carouselResponse, "$.data.cards[" + k + "]..cards[0]..link");

				softAssert.assertEquals(test1.get(0).toString(), restId1);
				softAssert.assertAll();
			}
			k++;
		}

		System.out.println("------------------->>>>>>>>>>>>>>>>>>>>>"
				+ JsonPath.read(carouselResponse, "$.data.cards[1]..bannerId").toString());

	}

//	@Test(dataProvider = "carouselAggreagatorValidation", description = "carousle to restuarant validation", groups = {"sanity", "Gaurav" })
	public void carouselAggregatorValidation(String lat, String lng, String userAgent, String polygonId) {
        String restId1 = "";
	    HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		String response = rngHelper.getListing(lat, lng, "0", tid, token).ResponseValidator.GetBodyAsText();
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		for (int i = 0; i < cardTypes.size(); i++) {

			if (cardTypes.get(i).toString().equals("restaurant")) {
				restId1 = JsonPath.read(response, "$.data.cards[" + i + "].data.data.id");
				break;
			}
		}
		String id = rngHelper.createCarousel(userAgent, "restaurant", "TopCarousel", restId1, polygonId);
		processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, redirectToMock);

		String[] payload = new String[] { lat, lng };
		int aggreRestCount = 0;
		rngHelper.errorCodeHandler(sndHelper.aggregator(payload), "Aggregator");
		String aggreResponse = sndHelper.aggregator(payload).ResponseValidator.GetBodyAsText();

		List<Integer> aggreBannerId = JsonPath.read(aggreResponse, "$.data.carousels..bannerId");
		Assert.assertTrue(aggreBannerId.contains(Integer.parseInt(id)));

	}

    @Test(dataProvider = "carouselAggreagatorValidation", description = "carousle to restuarant validation", groups = {"sanity", "Gaurav" })
    public void carouselAggregatorValidationMock(String lat, String lng, String userAgent, String polygonId) {
        String aggreResponse;
        String[] payload = new String[]{lat, lng};
        if (doMock){
			proxyServerPOJO = new ProxyServerPOJO()
					.withIpAddress("127.0.0.1")
					.withApiEndPoint("/api/v1/aggregator/")
					.withMockUrl("http://10.0.6.59:6666/")
					.withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");
			updateMockEndPoint(proxyServerPOJO);
			aggreResponse = sndHelper.aggregatorMocked(payload).ResponseValidator.GetBodyAsText();
        } else{
			proxyServerPOJO = new ProxyServerPOJO()
					.withIpAddress("127.0.0.1")
					.withApiEndPoint("/api/v1/aggregator/")
					.withMockUrl("http://10.0.6.59:6666/")
					.withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");
			updateMockEndPoint(proxyServerPOJO);
            aggreResponse = sndHelper.aggregator(payload).ResponseValidator.GetBodyAsText();
        }
        List<Integer> aggreBannerId = JsonPath.read(aggreResponse, "$.data.carousels..bannerId");

        processor = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, doMock);
        List<Integer> listingBannerId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data..bannerId");
        int matched = 0;
        for(int i: aggreBannerId){
            if(listingBannerId.contains(i)){
                matched++;
            }
        }
        System.out.println("Matched banner id's count :: "+matched);
        Assert.assertTrue(matched>=10,"Assertion Failed, minimum 10 banners should match in listing response");

    }

	@Test(dataProvider = "collectionPagination", description = "Colleciton Listing Pagination", groups = { "sanity",
			"Gaurav" })
	public void collectionPagination(String lat, String lng, String userAgent) {
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();

		String collectionId = rngHelper.createCollection(lat, lng, userAgent, 1);
		rngHelper.collectionRedis(collectionId);
		String offset = "2";
		processor = rngHelper.getCollectionLisitngPaginationProcessor(lat, lng, collectionId, offset, tid, token);

		processor = rngHelper.getCollectionLisitngPaginationProcessor(lat, lng, collectionId, offset, tid, token);
		String collectionResponse = processor.ResponseValidator.GetBodyAsText();
		List<Object> co = JsonPath.read(collectionResponse, "$.data..currentOffset");
		Assert.assertEquals(co.toString(), "[" + offset + "]", "given offset and current offset is different");

	}

	public void serviceabilityWithBannerValidation(String restId, String lat, String lng, String tid, String token) {
		int i = 0;
		processor = rngHelper.getListing(lat, lng, new Integer(i).toString(), tid, token);
		String response = processor.ResponseValidator.GetBodyAsText();
		int pages = JsonPath.read(response, "$.data.pages");
		for (int i1 = 0; i1 <= pages - 1; i1++) {
			processor = rngHelper.getListing(lat, lng, new Integer(i1).toString(), tid, token);
			response = processor.ResponseValidator.GetBodyAsText();
			List<Object> restIds = JsonPath.read(response, "$.data..id");
			int j = 0;
			for (Object rest : restIds) {

				if (rest.toString().equals(restId)) {

					System.out.println(JsonPath.read(response, "$.data.cards[" + j + "]..unserviceable").toString());
				}
				j++;
			}

		}

	}

//	@Test(dataProvider = "contextMatchValidation")
//	public void contextMatchValidation(String lat, String lng, String id) {
//
//
//		String contextId = rngHelper.createLayoutContextMap("Testing1", "CITY", "1", "VIEW_DOMAIN", "RESTAURANT_LISTING", "PLATFORM", "Swiggy-Android", "true", "425");
//		Processor processor = rngHelper.getContextMatchProcessor(lat, lng, id);
//		String response = processor.ResponseValidator.GetBodyAsText();
//		List<Object> contextIds = JsonPath.read(response, "$.data..id");
//		Assert.assertEquals(JsonPath.read(response, "$.statusCode").toString(), "0", "CreateContext api is failed");
//
//
//	}

	@Test(dataProvider = "OpenFilterRestaurantWidgetVaildation")
	public void OpenFilterRestaurantWidgetVaildation(String lat, String lng, String userAgent) {
        if(doMock){
            setMockRequest("api/v1/aggregator/","AggregatorWithOpenFilters");
            proxyServerPOJO = new ProxyServerPOJO()
                    .withIpAddress("127.0.0.1")
                    .withApiEndPoint("/api/v1/aggregator/")
                    .withMockUrl("http://10.0.6.59:6666/")
                    .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("1");

            updateMockEndPoint(proxyServerPOJO);

        }

		String response = rngHelper.getUserAgentRelatedAgent(lat,lng, userAgent, doMock).ResponseValidator
				.GetBodyAsText();
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		boolean openFilterflag = false;

		String carouselType = "";
		for (int i = 0; i < cardTypes.size(); i++) {
			if (cardTypes.get(i).equalsIgnoreCase("carousel")) {
				carouselType = JsonPath.read(response, "$.data.cards[" + i + "].data.subtype");
				if (carouselType.equalsIgnoreCase("openFilter"))
					openFilterflag = true;
			}

		}
        updateMockEndPoint(proxyServerPOJO.withDoMock("0"));
		Assert.assertTrue(openFilterflag, "the open filter flag is not visible.");

	}

	@Test(dataProvider = "OpenFilterWidgetVaildation")
	public void OpenFilterWidgetVaildation(String lat, String lng, String userAgent) {

		rngHelper.createCarousel("Swiggy-Android", "static", "OpenFilter", "N/A", polygonID);

		String response = rngHelper.getUserAgentRelatedAgent(lat, lng, userAgent, doMock).ResponseValidator
				.GetBodyAsText();
		List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
		int i1 = 0;
		ArrayList a = new ArrayList();
		if (cardTypes.size() == 0) {
			Assert.assertTrue(false);
		} else
			for (int i = 0; i < cardTypes.size(); i++) {

				if (cardTypes.get(i).equalsIgnoreCase("carousel")) {
					a.add(i);
				}
			}
//		if (a.size() < 3) {
//			Assert.assertTrue(false, "No Carousel is found");
//		} else {

		int j = (int) a.get(1);
		System.out.println("++++++++" + j);
		Assert.assertEquals(JsonPath.read(response, "$.data.cards[" + j + "].cardType"), "carousel",
				"cards is different");
		Assert.assertEquals(JsonPath.read(response, "$.data.cards[" + j + "].data.subtype"), "openFilter",
				"Card have different SubType");
		List<Object> bannerIds = JsonPath.read(response, "$.data.cards[" + j + "].data..cards[0]..bannerId");
		List<String> cTypes = JsonPath.read(response, "$.data.cards[" + j + "].data..data.type");
		for (String cType : cTypes) {
			if (cType.equalsIgnoreCase("restarrant")) {
				Assert.assertTrue(false, "found OPenFilter is not Restaurant based carousels");
			}
		}

//		}
	}

	@Test(dataProvider = "croutonValidation", description = "Croutons Validation on Listing", groups = { "regression",
			"Gaurav" })
	public void croutonValidtion(String lat, String lng, String userAgent, String type, boolean check)
			throws Exception {
        proxyServerPOJO = new ProxyServerPOJO()
                .withIpAddress("127.0.0.1")
                .withApiEndPoint("/api/v1/aggregator/")
                .withMockUrl("http://10.0.6.59:6666/")
                .withServiceUrl("http://sand-u-cuat-03.swiggyops.de/").withDoMock("0");

        updateMockEndPoint(proxyServerPOJO);
		int i = 0;
		HashMap hm = rngHelper.getHeader();
		String tid = hm.get("Tid").toString();
		String token = hm.get("Token").toString();
		processor = rngHelper.getListing(lat, lng, new Integer(i).toString(), tid, token);
		String response = processor.ResponseValidator.GetBodyAsText();

		List ids = JsonPath.read(response, "$.data..id");
		String restId = ids.get(0).toString();
		Iterator it = ids.iterator();
		if (check) {

			switch (type) {
			case "SERVICEABLE_WITH_BANNER": {
				int zoneId = rngHelper.enableRestaurantAsServiceableWithBanner(restId);
				serviceabilityWithBannerValidation(restId, lat, lng, tid, token);
				ddh.zoneOpenCloseSwitch(zoneId);
			}
				break;

			case "SPECIAL": {
				Assert.assertNotNull(JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
						"$.data.cards[10]..feeDetails.fees[3].fee"));
			}
				break;

			case "RAIN":
				Assert.assertNotNull(JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
						"$.data.cards[10]..feeDetails.fees[3].fee"));
				break;
			}
		} else {
			System.out.println("cruton not found");
		}
	}

	public void createOpenFilterWithoutLinkTestData() {

		Processor processor = rngHelper.getUserAgentRelatedAgent(RngConstants.iBCLat, RngConstants.iBCLng,
				RngConstants.androidUserAgent, redirectToMock);
		rngHelper.errorCodeHandler(processor, "Restaurant Listing");
		List<String> al = rngHelper.getRestaurantsList(processor.ResponseValidator.GetBodyAsText());
		String restId = "";
		if (al.size() == 0) {
			Assert.assertTrue(false, "cards are empty");
		} else {
			for (int i = 0; i < 4; i++) {
				rngHelper.createCarousel(RngConstants.androidUserAgent, "restaurant", "OpenFilter",
						al.get(i).toString(), polygonID);
				rngHelper.createCarousel(RngConstants.androidUserAgent, "static", "OpenFilter", "N/A", polygonID);
				rngHelper.createCarousel(RngConstants.androidUserAgent, "static", "TopCarousel", "N/A", polygonID);

			}

		}

	}

	public void setMockRequest(String uri, String fileName) {
		File file = new File("../Data/MockAPI/ResponseBody/rng/gandalf/" + fileName);
		String body = null;
		try {
			body = FileUtils.readFileToString(file);
//			wireMockHelper.setupStubPost(uri,200,"application/json",body,0);
			Processor processor = rngHelper.addMapping("/" + uri, "POST", body);
			System.out.println("mocking uri ::" + uri + "\n with this response body :: " + fileName);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setMockGetRequest(String uri, String fileName) {
		File file = new File("../Data/MockAPI/ResponseBody/rng/gandalf/" + fileName);
		String body = null;
		try {
			body = FileUtils.readFileToString(file);
//			wireMockHelper.setupStub(uri, 200, "application/json", body, 0);
			Processor processor = rngHelper.addMapping("/" + uri, "GET", body);
			System.out.println("mocking uri ::" + uri + "\n with this response body :: " + fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void addMockEndPoint(ProxyServerPOJO request) {
        try {
            rngHelper.addMockEndPoint(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateMockEndPoint(ProxyServerPOJO request) {
        try {
//            String systemipaddress = "52.221.8.23";
//            String systemipaddress = "182.75.181.110"; //jenkin box
//			String systemipaddress = "111.93.189.238"; //local
			String systemipaddress = "127.0.0.1"; //local

//            URL url_name = new URL("http://bot.whatismyipaddress.com");
//            BufferedReader sc = new BufferedReader(new InputStreamReader(url_name.openStream()));
//                // reads system IPAddress
//            systemipaddress = sc.readLine().trim();
            proxyServerPOJO.setIpAddress(systemipaddress);

            rngHelper.updateMockEndPoint(request);

            Thread.sleep(1000);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
