package com.swiggy.api.sf.checkout.helper.edvo.pojo.getLastOrderResponse;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "statusCode",
        "statusMessage",
        "data",
        "tid",
        "sid",
        "deviceId"
})
public class GetLastOrderResponse {

    @JsonProperty("statusCode")
    private Integer statusCode;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("data")
    private Data data;
    @JsonProperty("tid")
    private String tid;
    @JsonProperty("sid")
    private String sid;
    @JsonProperty("deviceId")
    private String deviceId;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetLastOrderResponse() {
    }

    /**
     *
     * @param statusCode
     * @param sid
     * @param data
     * @param tid
     * @param deviceId
     * @param statusMessage
     */
    public GetLastOrderResponse(Integer statusCode, String statusMessage, Data data, String tid, String sid, String deviceId) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.data = data;
        this.tid = tid;
        this.sid = sid;
        this.deviceId = deviceId;
    }

    @JsonProperty("statusCode")
    public Integer getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonProperty("tid")
    public String getTid() {
        return tid;
    }

    @JsonProperty("tid")
    public void setTid(String tid) {
        this.tid = tid;
    }

    @JsonProperty("sid")
    public String getSid() {
        return sid;
    }

    @JsonProperty("sid")
    public void setSid(String sid) {
        this.sid = sid;
    }

    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("statusCode", statusCode).append("statusMessage", statusMessage).append("data", data).append("tid", tid).append("sid", sid).append("deviceId", deviceId).toString();
    }

}