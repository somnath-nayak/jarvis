package com.swiggy.api.sf.rng.constants;

/**
 * Created by kiran.j on 11/9/17.
 */

public interface RngConstants {

	String rngDB = "rng";
	String rng_couponDB = "rng_coupon";
	String cmsDB = "cms";
	String restId = "16042";
	String areaId = "1";
	String marketingDBPerf = "MarketingTdPerf";
	String marketingDB = "marketing";
	int statuscode = 0;
	int cs_statuscode = 1;
	String statusmessage = "success";
	String get_td_path = "/Data/SchemaSet/Json/RNG/gettd";
	String td_restid = "/Data/SchemaSet/Json/RNG/getTdByRestid";
	String basic_auth = "Basic R0dZU1dJOjIwMTVTVyFHR1k=";
	String mobile1 = "9742940110";
	String password1 = "Nimda1234";
	String mobile2 = "2765998589";
	String password2 = "Nimda1234";
	String versionCode229 = "229";
	String versionCode250 = "250";
	String versionCode150 = "150";
	String versionCode200 = "200";
	String versionCode260 = "260";
	String AndroidOS = "ANDROID";
	String IosOS = "IOS";
	String WebOS = "WEB";
	String segmentationDB ="segmentationDB";

	String androidUserAgent = "Swiggy-Android";
	String iosUserAgent = "Swiggy-iOS";
	String webUserAgent = "web";

	String catid = "cat_id";
	String catname = "cat_name";
	String itemid = "item_id";
	String itemname = "item_name";
	String subcatid = "subcat_id";
	String subcatname = "subcat_name";
	Object minCartAmount = "2000";
	String next_item = "next_item";
	String deactiveCampaignByRestId = "update campaign set active = 0 where id in (select campaign_id from campaign_restaurant_map where restaurant_id=%s)";
	String allCampIdByRestId = "select * from campaign_restaurant_map where restaurant_id=%s";
	String deleteAllRestById = "delete from campaign_restaurant_map where id = %s";
	String deactiveTID = "update campaign set active = 0,enabled=0 where id in (select id from ( select c.id as id from campaign c join campaign_restaurant_map cres on c.id = cres.campaign_id where enabled = 1 and valid_till > CURRENT_TIMESTAMP and valid_from < CURRENT_TIMESTAMP and cres.restaurant_id=";
	String DeactiveTIDwithCampaignType = "update campaign set active = 0,enabled=0 where id in (select id from (select distinct c.id as id from campaign c join campaign_restaurant_map cres on c.id = cres.campaign_id where c.campaign_type=";
	String end = ") as c)";
	String getActiveTDID = "select id from campaign where active =0 and id in (select campaign_id from campaign_restaurant_map where restaurant_id =";
	String statusCodeZero = "0";
	String valueZero = "0";
	String selectUser = "select * from wp_users where mobile='";
	String deleteUser = "delete from wp_users where mobile='";
	String referral = "rngReferralCoupon";
	String couponDB= "coupondb";
    String growthPackDB= "growthpackdb";
    String growthPackTypeDbData= "select * from growth_pack_type where id = ";
    String growthPackTypeAudDbData= "select * from growth_pack_type_aud where id = ";
	String checkRC2 = "select * from coupons where code like ";
	String checkRC1 = "select * from coupons where code like ";
	String del = "delivered";
	String endQuotes = "'";
	String selectActiveCamp = "select id from campaign where enabled=1 and campaign_type =4 and valid_from <= '";
	String endSelectActiveCamp = "' and campaign.valid_till>='";
	String selectActiveCampRest = "select restaurant_id from campaign_restaurant_map where campaign_id in (";
    String endStatement = ");";
	String selectPaymentCode = "select pcm.`payment_code` from payment_code_mapping pcm, payment_code_mapping_aud pcma where pcm.`coupon_code`=pcma.`coupon_code` and pcm.coupon_code = '%s';";
	String selectBrands = "select option_value from wp_options where option_name='valid_brands'";
	String okicici = "okicici";
	String okhdfc = "okhdfc";
	String Tez = "Tez";
	String Bhim = "Bhim";
	String paymentCode123456 = "123456";
	String paymentCode1223 = "1223";
	String paymentCode559404 = "559404";
	String UPI_HANDLE = "UPI_HANDLE";
	String UPI_VPA = "UPI_VPA";
	String UPI_BRAND = "UPI_BRAND";
	String BANK_DISCOUNT = "BANK_DISCOUNT";
	String paymentCode678901 = "678901";
	Integer is_private_0 = 0;
	Integer is_private_1 = 1;
	Integer customer_restriction = 0;
	String coupon_type_discount = "Discount";
	String couponTypeFreeDel = "FreeDel";
	Integer discount_percentage = 0;
	Integer discount_amount = 50;
	Integer coupon_user_type_id = 0;
	String preferred_method = "Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb";
	String description = "Test";
	String publicCoupon = "PUBLIC";
	String privateCoupon = "PRIVATE";
	String superCoupon = "SUPER";
	Integer user_client = 0;


	String collectionWithBackGroundCreation = "INSERT INTO `collection` (`channel`, `description`, `title`, `enabled`, `cloudinary_image`, `cloudinary_icon`, `web_collection_header`, `web_carousal_icon`,`priority`, `minimum_restaurant`, `maximum_restaurant`, `type`, `city_id`, `identifier`, `start_date`, `end_date`, `entity`, `minimum_count`,`maximum_count`, `background_image`, `logo`, `visible`, `web_mast_head`, `app_mast_head`, `template_style`, `name`, `sub_title`,`dweb_open_filter_title`, `dweb_open_filter_bg_color`, `dweb_open_filter_select_icon`, `dweb_open_filter_deselect_icon`) VALUES('ALL', 'List of trending restaurants in your area.', 'Testing', 1, 'ixnhuotzsfwvh3l5ftfc', 'zjpk1fphiytydurwxlbk', NULL, 'aop9e16bpkpccpet83xj', 5, 1, 10, 'STATIC', 2,'testing', '2017-05-18 01:33:32', '2019-12-31 23:59:59', 'RESTAURANT', 2, 10, NULL, NULL, 1, 're1wfsp6iiytxp4p1pig','re1wfsp6iiytxp4p1pig','WITH_BACKGROUND', 'Collection', 'Trending restaurants in your area', NULL, NULL, NULL, NULL)";

	String selectDataFromCouponsSlots = "select * from coupons_slots where code ='";
	String selectDataFromCouponsSlotsAud = "select * from coupons_slots_aud where code ='";
	String ending = ";";
	String collectionId = "select id from swiggy.collection order by 1 desc limit 1;";
	String collection_user = "insert into swiggy.collection_entity_map(collection_id,entity_id)values(";

	String fyiBanner = "insert into swiggy.pop_up(name,creative_id,start_date_time,end_date_time,enabled,screen,level,type,title,message,priority)values('Testing','Ippavsyy0Inigvfkawsm','2017-08-12','2018-08-12','1','LISTING','AREA','ONCE_ONLY','automation+testing','automation_Testing','1');";
	String fyiId = "select * from swiggy.pop_up order by 1 desc limit 1;";
	String pop_area_map = "insert into swiggy.pop_up_area(pop_up_id,area_id,enabled,created_by)values(";
	String endpart = ",'1','gaurav')";
	String pop_channel_map = "insert into swiggy.pop_up_channels(pop_up_id,channel,app_version_check,app_version,enabled,created_by)values(";
	String pop_channel_map_end = ",'Swiggy-Android','GREATER_THAN',14,1,'gaurav');";
	String crutonQuery = "update zone set is_open=0 where id=124";
	String crutonRainQuery = "update zone set rain_mode_type=1 where id=124";
    String customerOrders = "insert into customer_orders(id,customer_id,created_at,updated_at,order_count,first_order_city,last_order_city,last_order_time) values(";
    String customerDeviceMap = "insert into device_user_map(id,device_id,user_id,updated_on) values(";
	String getUsers = "select id, referral_code from wp_users limit 6";
	String tD_User = "insert into campaign_user_map(campaign_id,user_id,updated_at,updated_by,is_active) values(";
	String selectTdActiveItemIds = "select crm.restaurant_id, r.entity_id from `campaign_rule_map` as crm join rule as r where  r.id = crm.rule_id and r.level=3 and crm.restaurant_id is not null and crm.restaurant_id in (";
	String endPart = ")";
	String randomRestaurant = "796";
    String getOrderId="select id from customer_orders where customer_id =";
	String testUser = "1812152";
	String testUser2 = "1812153";
	String testUser3 = "1812151";
	String selectEDVOActiveCamp = "select id from campaign where campaign_type=\"4\" and enabled=\"1\";";
	String selectActiveCampRule_RestId = "select rule_id, restaurant_id from campaign_rule_map where campaign_id in (";
	String selectEDVOActiveRestIds = "select restaurant_id from campaign join campaign_restaurant_map where campaign.id=campaign_restaurant_map.campaign_id and campaign.enabled=1 and campaign_type=4 and campaign.valid_from <='";
	String endSelectEDVOActiveRestIds = "' and campaign.valid_till>='";

	String selectEDVOTdActiveGroupIds = "select crm.restaurant_id, r.entity_id from `campaign_rule_map` as crm join rule as r where  r.id = crm.rule_id and r.level=101 and crm.restaurant_id is not null and crm.restaurant_id in (";
	String selectRestId_OperationId = "select restaurant_id, operation_id from campaign join campaign_restaurant_map where\r\n" +
			"campaign.id= campaign_restaurant_map.campaign_id and campaign.enabled=1 and campaign_restaurant_map.operation_type=\"MEAL\" and campaign.valid_from <='";
	String endSelectRestId_OperationId = "and campaign.valid_till>=\'";
	String deleteCoupon = " delete from coupons where code='";
	String enddeleteCoupon1 = "and valid_from='";
	String enddeleteCoupon2 = "and valid_till='";
	String selectMultiApplyCoupon = "select * from coupons where code='";
	String selectMultiApplyCouponCode = "select * from coupons where id='";
	String getactiveCampaign = "select id from `campaign` where id IN (select campaign_id from `campaign_restaurant_map` where operation_id = ";
	String getactiveCampaign1 = ") and enabled=1 ";
	String getOperationMeta = "select operation_meta from trade_discount.campaign_restaurant_map where campaign_id = ";
	String updateOperationMeta = "update `campaign_restaurant_map` set operation_meta = ";
	String updateOperationMeta1 = " where campaign_id = ";
	String insertMultiApplyCouponUsageCount= "insert into coupons_used (coupon_id,user_id, code) values (" ;
	String commaSeparation=",";
	String deleteEDVOCampaign="delete from campaign where id= '";
	String deleteCampaign="delete from campaign where id = %s;";
	String iosAgent = "Swiggy-iOS";
	String VersionCode229 = "229";
	String constantTid = "dcd1be13-9ec9-4713-afaf-2b92e42909b6";
	String constantToken = "8a12c95f-4740-4acb-ab98-feb0e2156492be84552c-8efc-4b60-ba17-e75b4d117f55";
	String iBCLat = "12.9326";
	String iBCLng = "77.6036";
	String city = "1";
	String delhiLat = "28.7041";
	String delhiLng = "77.1025";
	String bangalorePolygons = "13.187278 77.603430, 12.881567 77.898680, 12.728905 77.596556, 12.980613 77.309539, 13.134458 77.376830, 13.187278 77.603430 ";
	String updateMultiApplyCreationDate = "update `coupons_used` set created_at ='";
	String updateCustomerOrderCount = "update `customer_orders` set order_count ='";
	String getCartMinValue = "select min_amount from coupons where code=";
	String getRestaurantID = "select restaurant_id from coupons_restaurant_map where code=";
	String pitaraDBName = "pitara";

	String context = "INSERT INTO `context` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `dimension`, `name`, `priority`)\n" +
			"VALUES\n" +
			"\t(%s, 1, '2017-11-13 07:51:03', 'gaurav', '2018-06-12 12:23:24', NULL, 1, 'GANDALF_RESTAURANT_CARDS_LISTING', 1);";

	String tackcontext = "INSERT INTO `context` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `dimension`, `name`, `priority`)\n" +
			"VALUES\n" +
			"\t(%s, 1, '2017-11-13 07:51:03', 'gaurav', '2018-06-12 12:23:24', NULL, 1, 'TRACK_CARDS_LISTING', 1);";


	String nuxCard = "INSERT INTO `cards` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `type`, `creative_id`, `cta_text`, `cta_type`, `icon_creative_id`, `priority`, `subtitle`, `title`, `valid_from`, `valid_till`, `name`, `cta_link`, `cta_subtext`)" + "VALUES" + "(%s, 1, '2018-02-14 14:39:18', '', '2018-06-04 18:12:12', '', 'NUX_LISTING', NULL, 'OK, GOT IT', 'DISMISS', 'Free_delivery_icon_mpiz6n', 4, 'Use code FREEDEL2 to get free delivery on your order. Limited period offer. Order now!', 'Get free delivery on your order', '2018-02-13 00:00:00', '2028-05-01 00:00:00', 'LISTING_COUPON_NUX_1', NULL, NULL);";

	String nuxCard2 = "INSERT INTO `cards` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `type`, `creative_id`, `cta_text`, `cta_type`, `icon_creative_id`, `priority`, `subtitle`, `title`, `valid_from`, `valid_till`, `name`, `cta_link`, `cta_subtext`)" + "VALUES" + "(%s, 1, '2018-02-14 14:39:18', '', '2018-06-04 18:12:12', '', 'NUX_LISTING', NULL, 'OK, GOT IT', 'DISMISS', 'Free_delivery_icon_mpiz6n', 3, 'Use code FREEDEL2 to get free delivery on your order. Limited period offer. Order now!', 'Get free delivery on your order', '2018-02-13 00:00:00', '2028-05-01 00:00:00', 'LISTING_COUPON_NUX_1', NULL, NULL);";

	String contextKeyMap = "INSERT INTO `context_key_map` (`id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `context_id`, `context_key`)\n" +
			"VALUES\n" +
			"\t(%s, '2017-08-31 11:38:08', NULL, NULL, NULL, %s, 'pageType');";


	String collectionCreation1 = "INSERT INTO `collection` (`channel`, `description`, `title`, `enabled`, `cloudinary_image`, `cloudinary_icon`, `web_collection_header`, `web_carousal_icon`,`priority`, `minimum_restaurant`, `maximum_restaurant`, `type`, `city_id`, `identifier`, `start_date`, `end_date`, `entity`, `minimum_count`,`maximum_count`, `background_image`, `logo`, `visible`, `web_mast_head`, `app_mast_head`, `template_style`, `name`, `sub_title`,`dweb_open_filter_title`, `dweb_open_filter_bg_color`, `dweb_open_filter_select_icon`, `dweb_open_filter_deselect_icon`) VALUES('ALL', 'List of trending restaurants in your area.', 'Testing', 1, 'ixnhuotzsfwvh3l5ftfc', 'zjpk1fphiytydurwxlbk', NULL, 'aop9e16bpkpccpet83xj', 5, 1, 10, 'STATIC', 1,'testing', '2017-05-18 01:33:32', '2019-12-31 23:59:59', 'RESTAURANT', 1, 10, NULL, NULL, 1, 're1wfsp6iiytxp4p1pig','re1wfsp6iiytxp4p1pig','WITHOUT_BACKGROUND', 'Collection', 'Trending restaurants in your area', NULL, NULL, NULL, NULL)";

	String collectionCreation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)\n" +
			"VALUES\n" +
			"\t(%s, 1, '2018-02-14 14:39:18', '', '2018-06-11 15:38:06', '', '{\\\"validationType\\\":\\\"COLLECTION\\\",\\\"link\\\":\\\"566\\\"}', 'COLLECTION');\n";

	String couponCreation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-01-21 21:40:52', '', '2028-06-21 21:40:52', '', '{\\\"validationType\\\":\\\"COUPON\\\",\\\"couponCode\\\":\\\"%s\\\"}', 'COUPON');";

	String cardContextMap = "INSERT INTO `context_cards_map` (`id`, `context_id`, `card_id`)\n" +
			"VALUES\n" +
			"\t(%s, %s, %s);";

	String cardValidationMap = "INSERT INTO `cards_validation_map` (`id`, `card_id`, `card_validation_id`)\n" +
			"VALUES\n" +
			"\t(%s, %s, %s);";

	String contextValue = "INSERT INTO `context_value` ( `created_at`, `created_by`, `updated_at`, `updated_by`, `value`, `context_key_id`, `active`)\n" +
			"VALUES\n" +
			"\t('2017-11-13 07:54:36', NULL, NULL, NULL, 'RESTAURANT_LISTING', %s, 1);\n";
	String trackContextValue = "INSERT INTO `context_value` ( `created_at`, `created_by`, `updated_at`, `updated_by`, `value`, `context_key_id`, `active`)\n" +
			"VALUES\n" +
			"\t('2017-11-13 07:54:36', NULL, NULL, NULL, 'TRACK_CARDS_LISTING', %s, 1);\n";


	String sCard = "INSERT INTO cards ( id,active, created_at, created_by, updated_at, updated_by, type, creative_id, cta_text, cta_type, icon_creative_id, priority, subtitle, title, valid_from, valid_till, name, cta_link, cta_subtext)VALUES(%s, 1, '2018-04-30 13:00:22', 'sandeep.sahu@swiggy.in', '2018-06-26 16:37:25', 'sandeep.sahu@swiggy.in', 'S_CARD', 'rate_app_xpgd0j', 'RATE THE APP', 'STATIC', 'iOS-AppStore', 1, 'Give us 5 stars on AppStore', 'Loved the App?', '2017-04-18 00:00:00', '2029-04-18 00:00:00', 'Rate APP on AppStore', 'RateApp', NULL);";
	String lCard = "INSERT INTO `cards` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `type`, `creative_id`, `cta_text`, `cta_type`, `icon_creative_id`, `priority`, `subtitle`, `title`, `valid_from`, `valid_till`, `name`, `cta_link`, `cta_subtext`)VALUES(%s, 1, '2018-04-30 13:07:38', 'sandeep.sahu@swiggy.in', '2018-06-12 14:07:10', 'sandeep.sahu@swiggy.in', 'L_CARD', 'Swiggy_assured_image_lbdiah', NULL, 'STATIC', 'Swiggy_Assured_icon_jzq6u0', 1, 'Timely delivery or cashback! Tap the card to know more', 'Swiggy Assured Restaurants', '2017-04-18 00:00:00', '2020-04-18 00:00:00', 'SwiggyAssured Card', 'SwiggyAssured',NULL);";
	String xlCard = "INSERT INTO `cards` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `type`, `creative_id`, `cta_text`, `cta_type`, `icon_creative_id`, `priority`, `subtitle`, `title`, `valid_from`, `valid_till`, `name`, `cta_link`, `cta_subtext`)VALUES(%s, 1, '2018-04-30 13:15:43', 'sandeep.sahu@swiggy.in', '2017-07-02 09:37:02', 'sandeep.sahu@swiggy.in', 'XL_CARD', 'Long_distance_image_obbnba', NULL, NULL, 'long_distance_icon_spuaad', 1, NULL, 'Now order from far away restaurants', '2018-04-18 00:00:00', '2019-04-18 00:00:00', 'Long Distance Card', NULL, NULL);";
	String launchCard = "INSERT INTO `cards` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `type`, `creative_id`, `cta_text`, `cta_type`, `icon_creative_id`, `priority`, `subtitle`, `title`, `valid_from`, `valid_till`, `name`, `cta_link`, `cta_subtext`)VALUES(%s, 1, '2018-05-17 09:57:19', '', '2018-06-08 07:32:30', NULL, 'LAUNCH_CARD', NULL, NULL, NULL, 'food_court', 1, 'Skip queues. Order & pick up when ready', 'Your office food court is here!', '2017-05-01 00:00:00', '2028-12-31 23:59:00', 'Cafe Launch Card', NULL,NULL);";
	String rainCard = "INSERT INTO cards(id, active, created_at, created_by, updated_at, updated_by, type, creative_id, cta_text, cta_type, icon_creative_id, priority, subtitle, title, valid_from, valid_till, name, cta_link, cta_subtext)VALUES(%s, 1, '2018-04-18 15:36:10', '', '2018-06-26 16:37:25', '', 'RAIN_NUDGE_CARD', NULL, 'ORDER NOW', 'POP', 'cloud_taqqq5', 1, ' delicious, FREE delivery meals are up for grabs on Swiggy POP', 'Raining delicious meals at Swiggy POP!', '2017-04-18 00:00:00', '2029-04-18 00:00:00', 'RAIN_POP_NUDGE_EXTREME', NULL, NULL);";
	String smallNudgeCard = "INSERT INTO cards (active, created_at, created_by, updated_at, updated_by, type, creative_id, cta_text, cta_type, icon_creative_id, priority, subtitle, title, valid_from, valid_till, name, cta_link, cta_subtext, bg_color, bg_image, default_font_color)VALUES(1, 2018-05-04 18:04:31, NULL, 2018-05-25 11:46:45, , SMALL_NUDGE_CARD, NULL, NULL, NULL, Super.Mark.CART_3x_f6qgii, 5, Get Super Again, Your Super Expired, 2018-05-04 00:00:00, 2028-07-01 00:00:00, NUX_AND_2_ORDER_OLD_CITY_COUPON, NULL, NULL, #0c5260, NULL, #ffffff);";
	String bannerCard = "INSERT INTO cards (active, created_at, created_by, updated_at, updated_by, type, creative_id, cta_text, cta_type, icon_creative_id, priority, subtitle, title, valid_from, valid_till, name, cta_link, cta_subtext, bg_color, bg_image, default_font_color)VALUES(1, 2018-05-04 18:04:31, NULL, 2018-05-29 06:51:17, , BANNER_CARD, NULL, NULL, NULL, Super.Mark.CART_3x_f6qgii, 5, validFrom - validTill,  Super Days Left, 2018-05-04 00:00:00, 2028-07-01 00:00:00, NUX_AND_2_ORDER_OLD_CITY_COUPON, NULL, NULL, #0c5260, NULL, #ffffff);";
	String sCardV2 = "INSERT INTO `cards` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `type`, `creative_id`, `cta_text`, `cta_type`, `icon_creative_id`, `priority`, `title`, `valid_from`, `valid_till`, `name`, `cta_link`, `cta_subtext`, `bg_color`, `bg_image`, `default_font_color`) VALUES (%s, 1, '2018-04-30 13:00:22', 'sandeep.sahu@swiggy.in', '2018-06-06 11:49:57', 'sandeep.sahu@swiggy.in', 'S_CARD_V2', NULL, 'STAY SUPER', 'HALFCARD', 'Super.Mark.CART_3x_f6qgii', 1, 'Super Savings on this Order!', '2018-04-18 00:00:00', '2029-04-18 00:00:00', 'SUPER_TRACK', 'PWA', NULL, '#0c5260', 'super_nudge_bg_taztah', '#ffffff');";
	String superCard = "INSERT INTO `cards` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `type`, `creative_id`, `cta_text`, `cta_type`, `icon_creative_id`, `priority`, `subtitle`, `title`, `valid_from`, `valid_till`, `name`, `cta_link`, `cta_subtext`, `bg_color`, `bg_image`, `default_font_color`)VALUES(%s, 1, '2018-06-11 19:25:36', NULL, '2018-07-05 16:22:55', '', 'SUPER_LISTING_CARD', NULL, 'GET SUPER NOW', 'SWIGGY_SUPER', 'SuperNewLogo_loz2q9', 7, 'Buy SUPER to experience Unlimited Free Delivery, No Surge fee (EVER) and surprises just for you.', 'Swiggy SUPER. Free Delivery anytime, anywhere!', '2018-01-01 00:00:00', '2019-06-30 23:59:00', 'LISTING_NEW', NULL, NULL, '#0c5260', 'Nudge_Card_Background_Asset_3x_h59xvi', '#ffffff');";
	String popCard = "INSERT INTO `cards` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `type`, `creative_id`, `cta_text`, `cta_type`, `icon_creative_id`, `priority`, `subtitle`, `title`, `valid_from`, `valid_till`, `name`, `cta_link`, `cta_subtext`, `bg_color`, `bg_image`, `default_font_color`)\n" +
			"VALUES\n" +
			"\t(%s, 0, '2018-05-07 11:54:21', NULL, '2018-08-08 16:06:55', NULL, 'POP_LISTING', NULL, 'Try Pop now!', 'POP', 'swiggy_pop_3x_fz2a63', 1, 'Enjoy tasty single-serve meals, starting at Rs 99 and delivered for free! Forget your meal woes. Order from Pop.', 'Meals starting Rs 99 on POP', '2018-05-24 00:00:00', '2018-05-25 00:00:00', 'POP_NUDGE', NULL, NULL, NULL, NULL, NULL);";

	String disableContext = "update context set active=0 where id>=0";
	String disableCards = "update cards set active=0";
	String disableCard = "update cards set active=0 where id=%s";
	String collectionValidation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(10, 1, '2018-02-14 14:39:18', '', '2018-02-14 14:39:18', '', '{\"validationType\":\"COLLECTION\",\"link\":\"";
	String endCollectionValidation = "\"}', 'COLLECTION');";
	String couponValidation = "INSERT INTO `cards_validation` (`active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(1, '2018-02-14 14:39:18', '', '2018-02-14 14:39:18', '', '{\"validationType\":\"COUPON\",\"";
	String endCouponValidation = "\":\"SWIGGY33\"}', 'COUPON');";

	String test = "aa";

	String showRateValidation = "INSERT INTO cards_validation (id, active, created_at, created_by, updated_at, updated_by, validation_json, validation_type)VALUES(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"SHOW_RATE_APP_CARD\\\",\\\"value\\\": %s}', 'SHOW_RATE_APP_CARD');\n";
	String endShowRateValidation = "}', 'SHOW_RATE_APP_CARD');";

	String isPreOrderEnabledValidation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(50, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\"validationType\":\"IS_PRE_ORDER_ENABLED\",\"value\":";
	String endIsPreOrderEnabledValidaiton = "}', 'IS_PRE_ORDER_ENABLED');";
	String hasPlacePreOrderValidation = "INSERT INTO `cards_validation` (`active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(1, '2018-05-03 13:46:33', '', '2018-05-03 14:13:19', '', '{\"validationType\":\"HAS_PLACED_PRE_ORDER\",\"value\":";
	String endHasPlacePreOrerValidtion = "}', 'HAS_PLACED_PRE_ORDER');";
	String isPreOrderValidation = "INSERT INTO `cards_validation` (`active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\"validationType\":\"IS_PRE_ORDER\",\"value\":";
	String endIsPreOrdeValiation = "}', 'IS_PRE_ORDER');";
	String isLongDistanceOrder = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(45, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\"validationType\":\"IS_LONG_DISTANCE_ORDER\",\"value\":";
	String endIsLongDistanceOrder = "}', 'IS_LONG_DISTANCE_ORDER');";
	String hasPlacedLongDistanceOrder = "INSERT INTO `cards_validation` (`active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(1, '2018-05-03 13:46:33', '', '2018-05-03 14:12:01', '', '{\"validationType\":\"HAS_PLACED_LONG_DISTANCE_ORDER\",\"value\":";
	String endHasPlacedLongDistanceOrder = "}', 'HAS_PLACED_LONG_DISTANCE_ORDER');";
	String cafeAvailableValidation = "INSERT INTO `cards_validation` (`active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(1, '2018-05-17 09:58:35', '', '2018-06-05 14:37:28', NULL, '{\"validationType\":\"CAFE_AVAILABLE\",\"value\":";
	String endCafeAvailableValidation = "}', 'CAFE_AVAILABLE');";


	String rainCardValidation = "INSERT INTO `cards_validation` (id, active, created_at, created_by, updated_at, updated_by, validation_json, validation_type)VALUES(%s, 1, '2018-04-18 15:36:10', '', '2018-06-12 14:47:15', '', '{\\\"validationType\\\":\\\"RAIN_STRESS\\\",\\\"type\\\":\\\"RAIN\\\",\\\"value\\\":\\\"%s\\\"}', 'RAIN_STRESS');";

	String lcardValiadation_isPreOrderEnabled = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"IS_PRE_ORDER_ENABLED\\\",\\\"value\\\":%s}', 'IS_PRE_ORDER_ENABLED');";
	String lcardValiadation_hasPreOrder = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"HAS_PLACED_PRE_ORDER\\\",\\\"value\\\":%s}', 'HAS_PLACED_PRE_ORDER');";
	String lcardValiadation_isPreOrder = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"IS_PRE_ORDER\\\",\\\"value\\\":%s}', 'IS_PRE_ORDER');";

	String xlcardValiadation_IS_LONG_DISTANCE_ORDER = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"IS_LONG_DISTANCE_ORDER\\\",\\\"value\\\":%s}', 'IS_LONG_DISTANCE_ORDER');";
	String xlcardValiadation_HAS_PLACED_LONG_DISTANCE_ORDER = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\"validationType\":\"HAS_PLACED_LONG_DISTANCE_ORDER\",\"value\":%s}', 'HAS_PLACED_LONG_DISTANCE_ORDER');";

	String launchCardValiadation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-17 09:58:35', '', NULL, NULL, '{\\\"validationType\\\":\\\"CAFE_AVAILABLE\\\",\\\"value\\\":%s}', 'CAFE_AVAILABLE');";
	String launchCorporateCafeCardValiadation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-17 09:58:35', '', NULL, NULL, '{\\\"validationType\\\":\\\"CAFE_CORPORATE_AVAILABLE\\\",\\\"value\\\":%s}', 'CAFE_CORPORATE_AVAILABLE');";
	String superCardValidation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-15 18:24:57', '', '2018-05-29 19:38:18', NULL, '{\\\"validationType\\\":\\\"SUPER\\\",\\\"min\\\":0}', 'SUPER');";
	String superCardValidationWithMinAsTen = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-15 18:24:57', '', '2018-05-29 19:38:18', NULL, '{\\\"validationType\\\":\\\"SUPER\\\",\\\"min\\\":10}', 'SUPER');";
	String superCardValidationWithMaxAsTen = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-15 18:24:57', '', '2018-05-29 19:38:18', NULL, '{\\\"validationType\\\":\\\"SUPER\\\",\\\"min\\\":0,\\\"max\\\":10}', 'SUPER');";
	String isSuperOrderCardValidation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)VALUES(%s, 1, '2018-05-15 18:24:57', '', '2018-05-29 19:38:18', NULL, '{\\\"validationType\\\":\\\"IS_SUPER_ORDER\\\",\\\"value\\\":%s}', 'IS_SUPER_ORDER');";
	String couponUsermap = "INSERT INTO `coupons_user_map` (code,`coupon_id`,`created_by`,`created_at`,`user_id`)VALUES('%s', '%s', 'Gaurav','2016-11-23 08:03:44','%s');";

	String orderCountValidation = "INSERT INTO `cards_validation` (`id`,`active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)\n" +
			"VALUES\n" +
			"\t(%s,1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"ORDER_COUNT\\\",\\\"min\\\":1}', 'ORDER_COUNT');\n";
	String orderCountValidationWithMinAsTen = "INSERT INTO `cards_validation` (`id`,`active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)\n" +
			"VALUES\n" +
			"\t(%s,1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"ORDER_COUNT\\\",\\\"min\\\":10}', 'ORDER_COUNT');\n";

	String seenCountValidation  = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)\n" +
		"VALUES\n" +
		"\t(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"SEEN_COUNT\\\",\\\"min\\\":1, \\\"max\\\":2}', 'SEEN_COUNT');\n";

	String seenCountValidationWithMinMax  = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)\n" +
			"VALUES\n" +
			"\t(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"SEEN_COUNT\\\",\\\"min\\\":12, \\\"max\\\":15}', 'SEEN_COUNT');\n";

	String clickCountValidation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)\n" +
		"VALUES\n" +
		"\t(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:55:52', '', '{\\\"validationType\\\":\\\"CLICK_COUNT\\\",\\\"max\\\":1}', 'CLICK_COUNT');\n";

	String isSwiggyAssuredValidation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)\n" +
			"VALUES\n" +
			"\t(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 13:46:33', '', '{\\\"validationType\\\":\\\"IS_SWIGGY_ASSURED_ORDER\\\",\\\"value\\\":%s}', 'IS_SWIGGY_ASSURED_ORDER');\n";

	String hasSwiggyAssuredValidation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)\n" +
			"VALUES\n" +
			"\t(%s, 1, '2018-05-03 13:46:33', '', '2018-05-03 14:12:41', '', '{\\\"validationType\\\":\\\"HAS_PLACED_SWIGGY_ASSURED_ORDER\\\",\\\"value\\\":%s}', 'HAS_PLACED_SWIGGY_ASSURED_ORDER');\n";


	String isSuperOrderValidation = "INSERT INTO `cards_validation` (`id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `validation_json`, `validation_type`)\n" +
			"VALUES\n" +
			"\t(%s, 1, '2018-07-16 14:07:12', 'sanket.shah@swiggy.in', NULL, NULL, '{\\\"validationType\\\":\\\"IS_SUPER_ORDER\\\",\\\"value\\\":%s}', 'IS_SUPER_ORDER');\n";
//	"3074533";

	String userId = "6115986";
	String deleteContext = "delete from context_cards_map where card_id=%s";
	String deleteValidation = "delete from cards_validation_map where card_id=%s";
	String deleteCard = "delete from cards where id = %s";

	String areaSchedule = "INSERT INTO `area_schedule` (`slot_type`, `area_id`, `open_time`, `close_time`, `day`, `type`, `created_on`, `updated_on`, `created_by`, `updated_by`)\n" +
			"VALUES\n" +
			"\t('PREORDER', 1, 1000, 2200, 'Thu', 'PRE_ORDER', '2017-10-23 19:34:52', '2017-10-23 19:34:52', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"abhishek.pr@swiggy.in\\\"}}', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"abhishek.pr@swiggy.in\\\"}}'),\n" +
			"\t('PREORDER', 1, 0, 2360, 'Fri', 'PRE_ORDER', '2017-10-23 19:34:52', '2017-10-23 19:34:52', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"abhishek.pr@swiggy.in\\\"}}', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"abhishek.pr@swiggy.in\\\"}}'),\n" +
			"\t('PREORDER_SRISHTY', 1, 1100, 2330, 'Tue', 'PRE_ORDER', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),\n" +
			"\t('test', 1, 30, 100, 'Mon', 'PRE_ORDER', '2017-11-21 16:24:24', '2017-11-21 16:24:24', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"hemamalini.g@swiggy.in\\\"}}', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"hemamalini.g@swiggy.in\\\"}}'),\n" +
			"\t('PREORDER_SRISHTY', 1, 1100, 2330, 'Wed', 'PRE_ORDER', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),\n" +
			"\t('PRE_ORDER', 1, 1000, 2359, 'Sat', 'PRE_ORDER', '2017-10-23 19:34:52', '2017-10-23 19:34:52', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"abhishek.pr@swiggy.in\\\"}}', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"abhishek.pr@swiggy.in\\\"}}'),\n" +
			"\t('PRE_ORDER', 1, 1000, 2359, 'Sun', 'PRE_ORDER', '2017-10-23 19:34:52', '2017-10-23 19:34:52', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"abhishek.pr@swiggy.in\\\"}}', '{\\\"source\\\":\\\"CMS Dashboard\\\",\\\"meta\\\":{\\\"user\\\":\\\"abhishek.pr@swiggy.in\\\"}}');\n";

	String restauratSchedule = "INSERT INTO `restaurants_schedule` (`rest_id`, `day`, `open_time`, `close_time`, `type`, `created_on`, `updated_on`, `updated_by`, `created_by`)\n" +
			"VALUES\n" +
			"\t(%s, 'Fri', 1000, 2330, 'PREORDER', '2018-07-13 18:14:02', '2018-07-13 18:14:02', 'hemamalini.g@swiggy.in', 'hemamalini.g@swiggy.in'),\n" +
			"\t(%s, 'Tue', 1000, 2330, 'PREORDER', '2018-07-13 16:08:09', '2018-07-13 16:08:09', 'hemamalini.g@swiggy.in', 'hemamalini.g@swiggy.in'),\n" +
			"\t(%s, 'Mon', 1000, 2330, 'PREORDER', '2018-07-13 16:08:09', '2018-07-13 16:08:09', 'hemamalini.g@swiggy.in', 'hemamalini.g@swiggy.in'),\n" +
			"\t(%s, 'Wed', 1000, 2330, 'PREORDER', '2018-07-13 16:08:09', '2018-07-13 16:08:09', 'hemamalini.g@swiggy.in', 'hemamalini.g@swiggy.in'),\n" +
			"\t(%s, 'Thu', 1000, 2330, 'PREORDER', '2018-07-13 16:08:09', '2018-07-13 16:08:09', 'hemamalini.g@swiggy.in', 'hemamalini.g@swiggy.in'),\n" +
			"\t(%s, 'Sat', 1000, 2330, 'PREORDER', '2018-07-13 16:08:09', '2018-07-13 16:08:09', 'hemamalini.g@swiggy.in', 'hemamalini.g@swiggy.in'),\n" +
			"\t(%s, 'Sun', 1000, 2330, 'PREORDER', '2018-07-13 16:08:09', '2018-07-13 16:08:09', 'hemamalini.g@swiggy.in', 'hemamalini.g@swiggy.in');\n";

	String cardMetaData = "INSERT INTO `cards_metadata` (`active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `config_json`, `card_id`)\n" +
			"VALUES\n" +
			"    (1, '2018-07-16 13:36:22', 'sanket.shah@swiggy.in', '2018-07-27 11:44:22', '', '{\\\"type\\\":\\\"S_CARD_V2\\\",\\\"cardType\\\":\\\"S_CARD_V2\\\",\\\"savingsText\\\":\\\"Total Savings of \\\",\\\"freeDelText\\\":\\\"Free Delivery\\\",\\\"noSurgeText\\\":\\\"No surge fees\\\",\\\"freebieText\\\":\\\"Free Delights\\\"}', %s);";

	String deleteCardMetaData = "delete from `cards_metadata` where `card_id` = %s";

	String getAutoIncrementId = "SELECT `AUTO_INCREMENT`\n" +
			"FROM  INFORMATION_SCHEMA.TABLES\n" +
			"WHERE TABLE_SCHEMA = '%s'\n" +
			"AND   TABLE_NAME   = '%s';";

	String redisFYIPOPUPIndiaLevel = "[{\"id\":4003,\"name\":\"A\",\"creativeId\":\"jh0l2da1wksojrzetwfx\",\"startDateTime\":\"Mar 23, 2016 12:30:00 AM\",\"endDateTime\":\"Mar 23, 2016 7:00:00 AM\",\"enabled\":0,\"screen\":\"LISTING\",\"type\":\"EVERY_TIME\",\"title\":\"B\",\"message\":\"C\",\"callToActionJson\":\"\",\"level\":\"INDIA\",\"priority\":1,\"createdAt\":\"Mar 23, 2016 1:01:15 PM\",\"updatedAt\":\"Mar 23, 2016 1:07:14 PM\",\"createdBy\":\"ramzi@swiggy.in\",\"updatedBy\":\"ramzi@swiggy.in\",\"timeSlots\":[],\"areaEntities\":[],\"channelsEntities\":[{\"id\":477,\"popUpId\":4003,\"channel\":\"Web\",\"enabled\":1,\"createdAt\":\"Mar 23, 2016 1:01:15 PM\",\"createdBy\":\"ramzi@swiggy.in\"}],\"cityEntities\":[]},{\"id\":4075,\"name\":\"COD DIsabled\",\"creativeId\":\"wwqxn79wiau9li6ff7jn\",\"startDateTime\":\"Nov 9, 2016 7:20:00 AM\",\"endDateTime\":\"Nov 14, 2016 11:59:00 PM\",\"enabled\":0,\"screen\":\"LISTING\",\"type\":\"ONCE_ONLY\",\"title\":\"Please Note\",\"message\":\"\\u0027Change\\u0027 is great. Except for today. Use any one of our many payment options to place your order. COD has been paused temporarily.\",\"callToActionJson\":\"\",\"level\":\"INDIA\",\"priority\":1,\"createdAt\":\"Nov 9, 2016 8:07:48 AM\",\"updatedAt\":\"Nov 17, 2016 4:15:23 PM\",\"createdBy\":\"ankit.jindal@swiggy.in\",\"updatedBy\":\"ankit.jindal@swiggy.in\",\"timeSlots\":[],\"areaEntities\":[],\"channelsEntities\":[{\"id\":696,\"popUpId\":4075,\"channel\":\"Swiggy-Android\",\"enabled\":1,\"createdAt\":\"Nov 9, 2016 8:07:48 AM\",\"createdBy\":\"ankit.jindal@swiggy.in\"},{\"id\":697,\"popUpId\":4075,\"channel\":\"Web\",\"enabled\":1,\"createdAt\":\"Nov 9, 2016 8:07:48 AM\",\"createdBy\":\"ankit.jindal@swiggy.in\"},{\"id\":695,\"popUpId\":4075,\"channel\":\"Swiggy-iOS\",\"enabled\":1,\"createdAt\":\"Nov 9, 2016 8:07:48 AM\",\"createdBy\":\"ankit.jindal@swiggy.in\"}],\"cityEntities\":[]},{\"id\":4209,\"name\":\"scheduled_maintance_13july\",\"creativeId\":\"mve98wyjnql2hid0woic\",\"startDateTime\":\"Jul 12, 2017 7:29:00 AM\",\"endDateTime\":\"Jul 13, 2017 9:30:00 AM\",\"enabled\":0,\"screen\":\"LISTING\",\"type\":\"ONCE_ONLY\",\"title\":\"Scheduled Maintenance\",\"message\":\"Scheduled Maintenance We\\u0027ll be briefly offline from 1:30 AM to 7:30 AM. You won\\u0027t be able to place any orders during this time. Sorry about that.\",\"callToActionJson\":\"\",\"level\":\"INDIA\",\"priority\":1,\"createdAt\":\"Jul 12, 2017 10:10:35 PM\",\"updatedAt\":\"Jul 13, 2017 7:05:37 AM\",\"createdBy\":\"ankit.jindal@swiggy.in\",\"updatedBy\":\"ankit.jindal@swiggy.in\",\"timeSlots\":[{\"id\":2129,\"popUpId\":4209,\"day\":\"Thu\",\"startTime\":1,\"endTime\":130,\"enabled\":1,\"createdAt\":\"Jul 12, 2017 10:10:35 PM\",\"createdBy\":\"ankit.jindal@swiggy.in\"}],\"areaEntities\":[],\"channelsEntities\":[{\"id\":1136,\"popUpId\":4209,\"channel\":\"Swiggy-iOS\",\"appVersionCheck\":\"GREATER_THAN\",\"appVersion\":199,\"enabled\":1,\"createdAt\":\"Jul 12, 2017 10:10:35 PM\",\"createdBy\":\"ankit.jindal@swiggy.in\"},{\"id\":1138,\"popUpId\":4209,\"channel\":\"Web\",\"enabled\":1,\"createdAt\":\"Jul 12, 2017 10:10:35 PM\",\"createdBy\":\"ankit.jindal@swiggy.in\"},{\"id\":1137,\"popUpId\":4209,\"channel\":\"Swiggy-Android\",\"appVersionCheck\":\"GREATER_THAN\",\"appVersion\":199,\"enabled\":1,\"createdAt\":\"Jul 12, 2017 10:10:35 PM\",\"createdBy\":\"ankit.jindal@swiggy.in\"}],\"cityEntities\":[]}]";

	String NO_DATA_FOUND = "No data found in DB";

	String DATABASE = "db";
	String REDIS = "redis";
	String GUAVA = "guava";

	String updateEnableDisableTD0 = "update campaign set enabled=";
	String updateEnableDisableTD1 = "where id= ";

	String adsDB = "ads";
	String getPricingRule = "select `id` from `pricing_rule` where `date` = '%s' and area_id = %s and city_id = %s and `instrument_position` = %s and `instrument_type` = 'HIGH_PRIORITY' and `start_time_minutes` = %s and `end_time_minutes` = %s;";
	String getPricingRuleUnitMap = "select * from `pricing_rule_unit_map` where `rule_id` = %s;";
	String deletePricingRuleUnitMap = "delete from `pricing_rule_unit_map` where `id` = %s;";
	String deletePriceUnit = "delete from `price_unit` where id = %s;";
	String deletePriceRule = "delete from `pricing_rule` where `id` = %s;";
	String deletePricingException = "delete from `pricing_exception` where `restaurant_id` = %s and date = '%s' and `start_time_minutes` = %s and `end_time_minutes` = %s;";

	String getSoldInventoryIDs = "select * from inventory where `id` in( select `inventory_id` from `inventory_instruments_map` where `instruments_id` in (select `id` from `ads_instruments` where `rest_id`= %s and `type` = 'HIGH_PRIORITY')) and `date` ='%s' and `sold_quantity` =1;";
	String setSoldQuantityToZero = "update `inventory` set `sold_quantity` = 0 where `id` in (%s);";

	String adsTempFileDir = "./Data/temp/";


	String RMQ_USERNAME ="guest";
	String RMQ_PASSWORD = "guest";

	String TD_REST_ALREADY_HAS_ACTIVE_CAMPAIGN = "Restaurant already has an active campaign ";
	String TD_SUCCESS_CREATION_STATUS="done";



	String FREE_DELIVERY = "FREE_DELIVERY";
	String FREEBIE = "Freebie";
	String PERCENTAGE = "Percentage";
	String FLAT = "Flat";
	String ZERO_DAYS_DORMANT = "ZERO_DAYS_DORMANT";
	String THIRTY_DAYS_DORMANT = "THIRTY_DAYS_DORMANT";
	String SIXTY_DAYS_DORMANT = "SIXTY_DAYS_DORMANT";
	String NINETY_DAYS_DORMANT = "NINETY_DAYS_DORMANT";

	String CREATED_BY= "AUTOMATION";

	//DP constants
	String CAMPAIGN_TYPE = "CAMPAIGN_TYPE";
	String VALID_FROM = "VALID_FROM";
	String VALID_TILL = "VALID_TILL";
	String MIN_CART_AMOUNT = "MIN_CART_AMOUNT";
	String FIRST_ORDER = "FIRST_ORDER";
	String DORMANT_USER_TYPE = "DORMANT_USER_TYPE";
	String USER_RESTRICTION = "USER_RESTRICTION";


	//Header
	String CONTENT_TYPE= "Content-Type";
	String APPLICATION_JSON= "application/json";

	//TEMPLATE QUERIES
	String SELECT_TEMPLATE_BY_ID="select * from template where id =";
	String SELECT_RANDOM_TEMPLATE_ID="select id from `template` ORDER BY RAND() limit 1;";
	String SELECT_RANDOM_ENABLED_TEMPLATE_ID="select id from `template` where enabled=1  ORDER BY RAND() limit 1;";
	String SELECT_ENABLED_FROM_TEMPLATE_DB= "select enabled from `template`  where id =";
	String SELECT_TEMPLATE_FOR_GIVEN_ENABLED= "select count(*) from `template` where enabled=" ;
	String SELECT_COUNT_OF_ALL_TEMPLATE= "select count(*) from `template` ;" ;
	String DELETE_ALL_AUTOMATION_TEMPLATE="delete from template where created_by =\"AUTOMATION\"";
	String UPDATE_ALL_CAMPAIGNS_TO_DISABLE= "update `campaign` set enabled=0;";
	String SELECT_CAMPAIGN_BY_ID="select * from campaign where  id =";
	String SELECT_RESTAURANT_FROM_CAMPAIGN_RESTAURANT_MAP="select `restaurant_id` from `campaign_restaurant_map` where `campaign_id` = ";
	String  INGESTIONSOURCE_DASHBOARD="DASHBOARD";
	String  INGESTIONSOURCE_VENDOR_APP="VENDOR_APP";
	String  INGESTIONSOURCE_GROWTH_PACK= "GROWTH_PACK";
	String  INGESTIONSOURCE_BULK_SYSTEM="BULK_SYSTEM";

	//TEMPLATE
	String TD_TEMPLATE_TIMESPANTYPE_STATIC="STATIC";
	String TD_TEMPLATE_TIMESPANTYPE_SLIDING="SLIDING";
	String TD_TEMPLATE_TIMESPANTYPE_SUGGESTED="SUGGESTED";






}



