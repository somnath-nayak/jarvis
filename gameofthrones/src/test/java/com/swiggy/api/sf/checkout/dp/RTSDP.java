package com.swiggy.api.sf.checkout.dp;

import com.swiggy.api.sf.checkout.constants.RTSConstants;
import org.testng.annotations.DataProvider;

public class RTSDP {
    @DataProvider(name="testData")
    public static Object[][] getUpdateCartForOneMealTestData(){
        return new Object[][]{
                //mobile, password, restaurantId, isCartItemRequired, cartItemQuantity, isAddonRequiredForCartItems,
                // isMealItemRequired, mealItemQuantity, isAddonRequiredForMealItems, mealIds, cartType, tdType
                //City ID, Zone Id, Enable DE, TD Type
                {RTSConstants.MOBILE_1, RTSConstants.PASSWORD_1, RTSConstants.RESTAURANT_ID_1, true, 1, false,
                        false, 0, false, new String[]{""}, RTSConstants.CART_TYPE_REGULAR, RTSConstants.DE_ID_1,
                        RTSConstants.CITY_ID, RTSConstants.ZONE_ID, RTSConstants.ENABLE_DE, RTSConstants.NO_TD},
                {RTSConstants.MOBILE_1, RTSConstants.PASSWORD_1, RTSConstants.RESTAURANT_ID_1, true, 1, false,
                        false, 0, false, new String[]{""}, RTSConstants.CART_TYPE_REGULAR, RTSConstants.DE_ID_1,
                        RTSConstants.CITY_ID, RTSConstants.ZONE_ID, RTSConstants.ENABLE_DE, RTSConstants.FREE_DELIVERY},
                {RTSConstants.MOBILE_1, RTSConstants.PASSWORD_1, RTSConstants.RESTAURANT_ID_1, true, 1, false,
                        false, 0, false, new String[]{""}, RTSConstants.CART_TYPE_REGULAR, RTSConstants.DE_ID_1,
                        RTSConstants.CITY_ID, RTSConstants.ZONE_ID, RTSConstants.ENABLE_DE, RTSConstants.FREEBIE}
        };
    }

    @DataProvider(name="reserveMealCartTestData")
    public static Object[][] reserveMealCartTestData(){
        return new Object[][]{
                //mobile, password, restaurantId, isCartItemRequired, cartItemQuantity, isAddonRequiredForCartItems,
                // isMealItemRequired, mealItemQuantity, isAddonRequiredForMealItems, mealIds, cartType, tdType
                //City ID, Zone Id, Enable DE, TD Type
                {RTSConstants.MOBILE_1, RTSConstants.PASSWORD_1, RTSConstants.RESTAURANT_ID_1, true, 1, false,
                        true, 0, false, new String[]{RTSConstants.MEAL_ID}, RTSConstants.CART_TYPE_REGULAR, RTSConstants.DE_ID_1,
                        RTSConstants.CITY_ID, RTSConstants.ZONE_ID, RTSConstants.ENABLE_DE, RTSConstants.MEAL_TD_TYPE_03},
//                {RTSConstants.MOBILE_1, RTSConstants.PASSWORD_1, RTSConstants.RESTAURANT_ID_1, true, 1, false,
//                        true, 0, false, RTSConstants.MEAL_TD_TYPE_07, RTSConstants.CART_TYPE_REGULAR, RTSConstants.DE_ID_1,
//                        RTSConstants.CITY_ID, RTSConstants.ZONE_ID, RTSConstants.ENABLE_DE, RTSConstants.MEAL_TD_TYPE_07},
//                {RTSConstants.MOBILE_1, RTSConstants.MEAL_TD_TYPE_07, RTSConstants.RESTAURANT_ID_1, true, 1, false,
//                        true, 0, false, RTSConstants.MEAL_TD_TYPE_07, RTSConstants.CART_TYPE_REGULAR, RTSConstants.DE_ID_1,
//                        RTSConstants.CITY_ID, RTSConstants.ZONE_ID, RTSConstants.ENABLE_DE, RTSConstants.MEAL_TD_TYPE_07}
        };
    }
}
