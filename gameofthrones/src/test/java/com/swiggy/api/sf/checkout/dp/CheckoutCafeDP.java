package com.swiggy.api.sf.checkout.dp;

import org.testng.annotations.DataProvider;

public class CheckoutCafeDP {
    
    @DataProvider(name = "CafeFlow")
    public static Object[][] fetchPricing() {
        return new Object[][]{
        	    {"5748269","1","5271","CAFE"}
                };
    }
}