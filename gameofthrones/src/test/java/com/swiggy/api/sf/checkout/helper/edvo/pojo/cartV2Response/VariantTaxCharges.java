package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "GST"
})
public class VariantTaxCharges {

    @JsonProperty("GST")
    private String gST;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public VariantTaxCharges() {
    }

    /**
     *
     * @param gST
     */
    public VariantTaxCharges(String gST) {
        super();
        this.gST = gST;
    }

    @JsonProperty("GST")
    public String getGST() {
        return gST;
    }

    @JsonProperty("GST")
    public void setGST(String gST) {
        this.gST = gST;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gST", gST).append("additionalProperties", additionalProperties).toString();
    }

}