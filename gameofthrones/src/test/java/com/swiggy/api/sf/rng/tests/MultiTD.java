package com.swiggy.api.sf.rng.tests;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.dp.MultiTDDP;
import com.swiggy.api.sf.rng.helper.RngHelper;

import framework.gameofthrones.JonSnow.Processor;

public class MultiTD extends MultiTDDP {

	HashMap<String, String> tds = new HashMap<String,String>();
	String response = null;
	Processor processor = null;
	RngHelper rngHelper = new RngHelper();
	
	public MultiTD()
	{
		
	}

	
	@Test(dataProvider = "createTDRestaurantLevel",priority=1)
	public void CreateMultiTD(String tdType, String body) {
		rngHelper = new RngHelper();
		switch (tdType) {
	
		case "firstOrderRestriction":
			processor = rngHelper.createTD(body);
			response = processor.ResponseValidator.GetBodyAsText();
			rngHelper.TDstatusChecker(response);
			tds.put("firstOrderRestriction", JsonPath.read(response, "$.data").toString());
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "done");
			Assert.assertNotNull(JsonPath.read(response, "$.data").toString(), "td id is null");
			
			break;
		case "restaurantFirstOrder":
			processor = rngHelper.createTD(body);
			response = processor.ResponseValidator.GetBodyAsText();
			rngHelper.TDstatusChecker(response);
			tds.put("restaurantFirstOrder", JsonPath.read(response, "$.data").toString());
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "done");
			Assert.assertNotNull(JsonPath.read(response, "$.data").toString(), "td id is null");
			break;
		case "userRestriction":
			processor = rngHelper.createTD(body);
			response = processor.ResponseValidator.GetBodyAsText();
			rngHelper.TDstatusChecker(response);
			tds.put("userRestriction", JsonPath.read(response, "$.data").toString());
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "done");
			Assert.assertNotNull(JsonPath.read(response, "$.data").toString(), "td id is null");
			break;
		case "dormant_user_type":
			processor = rngHelper.createTD(body);
			response = processor.ResponseValidator.GetBodyAsText();
			rngHelper.TDstatusChecker(response);
			tds.put("dormant_user_type", JsonPath.read(response, "$.data").toString());
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "done");
			Assert.assertNotNull(JsonPath.read(response, "$.data").toString(), "td id is null");
			break;
			
		case "NoRestriction":
			processor = rngHelper.createTD(body);
			response = processor.ResponseValidator.GetBodyAsText();
			rngHelper.TDstatusChecker(response);
			tds.put("firstOrderRestriction", JsonPath.read(response, "$.data").toString());
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "done");
			Assert.assertNotNull(JsonPath.read(response, "$.data").toString(), "td id is null");
	
		}
	}
    
	@Test(priority = 2, dataProvider = "userSegmentData")
	public void verifyUserSegmentHasHigherPriorityThanRestaurantAndFirstOrderFRestriction(String userId) {
		try {
			    rngHelper.userMappingTD(userId, tds.get("userRestriction"));
			    System.out.println("================================="+tds.get("userRestriction"));
			} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test(priority = 3, dataProvider = "ListingEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder",description="Listing_UserSegment_V/S_RestaurantFirstOrder_V/S_FirstOrder")
	public void ListingEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder(String stringRestaurant,String firstOrder,String userId,String osType,String versionCode) {
		
		

		Processor processor = rngHelper.listEvalulate(stringRestaurant, firstOrder, userId,osType, versionCode);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(Integer.parseInt(JsonPath.read(response, "$.statusCode").toString()), 0);
		Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "done");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].header").toString(),"[\"MultiTDCampaign1Header\"]", "header Check");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].description").toString(),"[\"Default Description\"]", "description Check");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(),"[\"Percentage\"]", "discountType Check");

	}

	@Test(priority = 4, dataProvider = "MenuEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder",description="Menu_UserSegment_V/S_RestaurantFirstOrder_V/S_FirstOrder")
	public void MenuEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder(String minCartAmount, String restaurantIds, String firstOrder,String userId, String userAgent, String versionCode) {

		Processor processor = rngHelper.menuEvaluate(minCartAmount, restaurantIds, firstOrder, userId, userAgent,
				versionCode);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(),
				"[\"Percentage\"]", "TD is not percentage");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].header").toString(),
				"[\"MultiTDCampaign1Header\"]", "TD is not percentage");
	}
	
	
	@Test(priority = 5, dataProvider = "CartEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder",description="Cart_UserSegment_V/S_RestaurantFirstOrder_V/S_FirstOrder")
	public void CartEvaluation_UserSegment_RestaurantFirstOrder_FirstOrder(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String minCartAmount,  String userId, String firstOrder, String userAgent, String versionCode)
 {


		Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId,firstOrder, userAgent, versionCode);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].header").toString(),
				"[\"MultiTDCampaign1Header\"]", "TD is not percentage");
		Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "20.0", "TD is not applied");

	}
	
	@Test(priority = 6, dataProvider = "ListingEvaluation_RestaurantFirstOrder_FirstOrder",description="List_RestaurantFirstOrder_V/S_FirstOrder")
	public void ListingEvaluation_RestaurantFirstOrder_FirstOrder(String stringRestaurant,String firstOrder,String userId,String osType,String versionCode) {
		

		Processor processor = rngHelper.listEvalulate(stringRestaurant, firstOrder, userId,osType, versionCode);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(Integer.parseInt(JsonPath.read(response, "$.statusCode").toString()), 0);
		Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "done");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].header").toString(),"[\"MultiTDCampaign2Header\"]", "header Check");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].description").toString(),"[\"Default Description\"]", "description Check");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(),"[\"Percentage\"]", "discountType Check");

	}
	
	@Test(priority = 7, dataProvider = "MenuEvaluation_RestaurantFirstOrder_FirstOrder",description="Menu_RestaurantFirstOrder_V/S_FirstOrder")
	public void MenuEvaluation_RestaurantFirstOrder_FirstOrder(String minCartAmount, String restaurantIds, String firstOrder,
			String userId, String userAgent, String versionCode) {

		Processor processor = rngHelper.menuEvaluate(minCartAmount, restaurantIds, firstOrder, userId, userAgent,
				versionCode);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(),"[\"Percentage\"]", "TD is not percentage");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].header").toString(),"[\"MultiTDCampaign2Header\"]", "TD is not percentage");
	}
		
	
	@Test(priority = 8, dataProvider = "CartEvaluation_RestaurantFirstOrder_FirstOrder",description="Cart_RestaurantFirstOrder_V/S_FirstOrder")
	public void CartEvaluation_RestaurantFirstOrder_FirstOrder(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String minCartAmount,  String userId, String firstOrder, String userAgent, String versionCode)
 {

		Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId,firstOrder, userAgent, versionCode);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
		Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].header").toString(),"[\"MultiTDCampaign2Header\"]", "TD is not percentage");
		Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "20.0", "TD is not applied");

	}
	
	@Test(priority=9,dataProvider="dormantwithrestaurant")
	public void dormantUser(String stringRestaurant,String userId)
	{

		rngHelper.DormantUser(userId, stringRestaurant, "30");
	}

		@Test(priority = 10, dataProvider = "ListingEvaluation_PublicTD_ThirtyDaysDormant",description="List_PublicTD_V/S_ThirtyDaysDormant")
		public void ListingEvaluation_PublicTD_ThirtyDaysDormant(String stringRestaurant,String firstOrder,String userId,String osType,String versionCode) {
			
			
			Processor processor = rngHelper.listEvalulate(stringRestaurant, firstOrder, userId,osType, versionCode);
			String resp = processor.ResponseValidator.GetBodyAsText();
			Assert.assertEquals(Integer.parseInt(JsonPath.read(response, "$.statusCode").toString()), 0);
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "done");
			Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].header").toString(),"[\"MultiTDCampaign4Header\"]", "header Check");
			Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].description").toString(),"[\"Default Description\"]", "description Check");
			Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(),"[\"Percentage\"]", "discountType Check");

		}

		@Test(priority = 11, dataProvider = "MenuEvaluation_PublicTD_ThirtyDaysDormant",description="Menu_PublicTD_V/S_ThirtyDaysDormant")
		public void MenuEvaluation_PublicTD_ThirtyDaysDormant(String minCartAmount, String restaurantIds, String firstOrder,String userId, String userAgent, String versionCode) {
			
			Processor processor = rngHelper.menuEvaluate(minCartAmount, restaurantIds, firstOrder, userId, userAgent,
					versionCode);
			String resp = processor.ResponseValidator.GetBodyAsText();
			Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(),"[\"Percentage\"]", "TD is not percentage");
			Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].header").toString(),"[\"MultiTDCampaign4Header\"]", "TD is not percentage");
		}
		
		@Test(priority = 12, dataProvider = "CartEvaluation_PublicTD_ThirtyDaysDormant",description="Cart_PublicTD_V/S_ThirtyDaysDormant")
		public void CartEvaluation_PublicTD_ThirtyDaysDormant(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String minCartAmount,  String userId, String firstOrder, String userAgent, String versionCode)
	 {
			
			Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,minCartAmount, userId,firstOrder, userAgent, versionCode);
			String resp = processor.ResponseValidator.GetBodyAsText();
			Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].header").toString(),"[\"MultiTDCampaign4Header\"]", "TD is not percentage");
			Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "20.0", "TD is not applied");

		}



	
		}
		
		
