package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.snd.dp.AddressDP;
import com.swiggy.api.sf.snd.helper.AddressHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class AddressTest extends AddressDP{
    AddressHelper addressHelper= new AddressHelper();
    SANDHelper sandHelper= new SANDHelper();
    Random random= new Random();

    @Test(dataProvider = "addAddress", description = "add address for a user")
    public void addAddress(String lat, String lng, String name, String password, String msg, String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(sandHelper.JsonString(resp, "$.data"), "address not added");
    }


    @Test(dataProvider = "invalidUser", description = "add address for a non existing user")
    public void addAddressForNonExistingUser(String lat, String lng, String name, String password, String msg,String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(random.nextInt(99999999), mobile, lat, lng,annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNull(JsonPath.read(resp, "$.data"), "address added successfully");
    }

    @Test(dataProvider = "sameAnnotation", description = "adding same annotation for the same user ")
    public void addAddressForSame(String lat, String lng, String name, String password, String msg, String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(JsonPath.read(resp, "$.data"), "address added successfully");
        String getAddressUser= addressHelper.getaddressByUserId(hMap.get("userId")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getAddressUser, "$.data..annotation"),annotation, "address not added");
        String payloadHome=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String response=addressHelper.addAddress(payloadHome).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(response, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(JsonPath.read(response, "$.data"), "address added successfully");
        String getAddressUser2= addressHelper.getaddressByUserId(hMap.get("userId")).ResponseValidator.GetBodyAsText();
        List<String> noOf= Arrays.asList(sandHelper.JsonString(getAddressUser2, "$.data..annotation").split(","));
        for(String data: noOf){
            Assert.assertEquals(data,annotation, "address not added");
        }
    }

    @Test(dataProvider = "addAddress", description = "add address for a user and check it using get address by userid")
    public void getAddressById(String lat, String lng, String name, String password, String msg, String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(sandHelper.JsonString(resp, "$.data"), "address not added");
        String getAddressUser2= addressHelper.getaddressByUserId(hMap.get("userId")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getAddressUser2, "$.data..annotation"),annotation, "address not added");
    }

    @Test(dataProvider = "addAddress", description = "check address id for a non existing userId")
    public void getAddressByInvalidUser(String lat, String lng, String name, String password, String msg, String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(sandHelper.JsonString(resp, "$.data"), "address not added");
        String getAddressUser2= addressHelper.getaddressByUserId(String.valueOf(random.nextInt(999999999))).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getAddressUser2, "$.data"),"", "address not added");
    }

    @Test(dataProvider = "addAddress", description = "update address for a user")
    public void updateAddress(String lat, String lng, String name, String password, String msg, String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(sandHelper.JsonString(resp, "$.data"), "address not added");
        String id=sandHelper.JsonString(resp, "$.data");
        String updateString= addressHelper.addressUpdate(Integer.parseInt(id), Integer.parseInt(hMap.get("userId")),sandHelper.getMobile(), lat, lng, annotation);
        String updateResponse= addressHelper.addressUpdateById(updateString).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(updateResponse, "$.statusMessage"),msg, "address not updated");
        String getAddressUser= addressHelper.getaddressByUserId(hMap.get("userId")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getAddressUser, "$.data..annotation"),annotation, "address not added");

    }

    @Test(dataProvider = "nullExisting", description = "update address for a user")
    public void updateAddressByNull(String lat, String lng, String name, String password, String msg, String annotation, String notFound){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(sandHelper.JsonString(resp, "$.data"), "address not added");

        String updateString= addressHelper.addressUpdate(null, Integer.parseInt(hMap.get("userId")),sandHelper.getMobile(), lat, lng, annotation);
        String updateResponse= addressHelper.addressUpdateById(updateString).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(updateResponse, "$.statusMessage"),notFound, "address not updated");
        String getAddressUser= addressHelper.getaddressByUserId(hMap.get("userId")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getAddressUser, "$.data..annotation"),annotation, "address not added");
    }

    @Test(dataProvider = "addAddress", description = "find address by passing address Id")
    public void findAddressByAddressId(String lat, String lng, String name, String password, String msg, String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(sandHelper.JsonString(resp, "$.data"), "address not added");
        String id=sandHelper.JsonString(resp, "$.data");
        String getAddressById=addressHelper.addressById(id).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getAddressById, "$.data..annotation"),annotation, "address not added");
    }

    @Test(dataProvider = "nonExistingAddress", description = "find address by passing address Id for non existing user")
    public void findAddressByAddressIdNonExisting(String lat, String lng, String name, String password, String msg, String annotation, String notFound ){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(sandHelper.JsonString(resp, "$.data"), "address not added");
        String getAddressById=addressHelper.addressById(String.valueOf(random.nextInt(99999999))).ResponseValidator.GetBodyAsText();

        Assert.assertEquals(sandHelper.JsonString(getAddressById, "$.statusMessage"),notFound,"address id found" );
    }

    @Test(dataProvider = "addAddress", description = "get default address")
    public void addressGetDefault(String lat, String lng, String name, String password, String msg, String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(sandHelper.JsonString(resp, "$.data"), "address not added");

        String addressDefault= addressHelper.addressDefault(hMap.get("userId"), annotation).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(addressDefault, "$.statusMessage"),msg, "address not added");
        Assert.assertEquals(sandHelper.JsonString(resp, "$.data"), sandHelper.JsonString(addressDefault, "$.data.id"), "Address ids are not same");
    }

    @Test(dataProvider = "addAddress", description = "get default address")
    public void addressGetDefaultNonExisting(String lat, String lng, String name, String password, String msg, String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        Assert.assertNotNull(sandHelper.JsonString(resp, "$.data"), "address not added");

        String addressDefault= addressHelper.addressDefault(String.valueOf(random.nextInt(999999999)), annotation).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(addressDefault, "$.statusMessage"),msg, "address not added");
        Assert.assertNull(JsonPath.read(addressDefault, "$.data") ,"Address ids are not same");
    }

    @Test(dataProvider = "addAddress", description = "add and delete address")
    public void deleteAddressByAddressId(String lat, String lng, String name, String password, String msg, String annotation){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        String addressId = sandHelper.JsonString(resp, "$.data");
        Assert.assertNotNull(addressId, "address not added");
        String deleteAddress=addressHelper.deleteAddress(addressId).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(deleteAddress, "$.statusMessage"),msg, "address not deleted");
        //String getAddressById=addressHelper.addressById(addressId).ResponseValidator.GetBodyAsText();
    }

    @Test(dataProvider = "nonExistingAddress", description = "delete address")
    public void deleteAddressByNonExistingAddressId(String lat, String lng, String name, String password, String msg, String annotation,String notFound){
        String mobile=sandHelper.getMobile();
        HashMap<String, String> hMap=sandHelper.createUser(name, mobile, sandHelper.getemailString(), password);
        String payload=addressHelper.address(Integer.parseInt(hMap.get("userId")), mobile, lat, lng, annotation);
        String resp=addressHelper.addAddress(payload).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(resp, "$.statusMessage"),msg, "address not added");
        String addressId = sandHelper.JsonString(resp, "$.data");
        Assert.assertNotNull(addressId, "address not added");
        String deleteAddress=addressHelper.deleteAddress(String.valueOf(random.nextInt(999999999))).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(deleteAddress, "$.statusMessage"),notFound, "address not deleted");
        //String getAddressById=addressHelper.addressById(addressId).ResponseValidator.GetBodyAsText();
    }


}
