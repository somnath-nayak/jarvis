package com.swiggy.api.sf.rng.dp.coupondp;

import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.OrderCountByPaymentMethodPOJO;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CouponPaymentTransactionDP {

	RngHelper rngHelper = new RngHelper();

	@DataProvider(name = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneData")
	public static Object[][] createPublicDiscountTypeCouponWithNumTxnPaymentTypeOne() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("AmazonPay")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
			        .withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneForSuperUserData")
	public static Object[][] createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneForSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneForNonSuperUserData")
	public static Object[][] createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneData")
	public static Object[][] createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOne() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("AmazonPay")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneForSuperuserData")
	public static Object[][] createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneForSuperuser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("AmazonPay")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneForNonSuperuserData")
	public static Object[][] createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneForNonSuperuser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("AmazonPay")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneData")
	public static Object[][] createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOne() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("AmazonPay")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneForSuperUserData")
	public static Object[][] createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneForSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("AmazonPay")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneForNonSuperUserData")
	public static Object[][] createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("AmazonPay")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneData")
	public static Object[][] createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOne() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("AmazonPayWallet")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneForSuperUserData")
	public static Object[][] createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneForSuperUse() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("AmazonPayWallet")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneForNonSuperUserData")
	public static Object[][] createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("AmazonPayWallet")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsData")
	public static Object[][] createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethods() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserData")
	public static Object[][] createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserData")
	public static Object[][] createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsData")
	public static Object[][] createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethods() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserData")
	public static Object[][] createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserData")
	public static Object[][] createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsData")
	public static Object[][] createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethods() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserData")
	public static Object[][] createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserData")
	public static Object[][] createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsData")
	public static Object[][] createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethods() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserData")
	public static Object[][] createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserData")
	public static Object[][] createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Coupon-NumTxnPaymentType-TestCases")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicFreeShippingCouponWithNumTxnPaymentTypeAsZeroData")
	public static Object[][] createPublicFreeShippingCouponWithNumTxnPaymentTypeAsZero() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("Sodexo")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(0);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createPublicDiscountCouponWithNumTxnPaymentTypeAsZeroData")
	public static Object[][] createPublicDiscountCouponWithNumTxnPaymentTypeAsZero() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("Sodexo")
					.withDescription("Test")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(0);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createMappedUserDiscountCouponWithNumTxnPaymentTypeAsZeroData")
	public static Object[][] createMappedUserDiscountCouponWithNumTxnPaymentTypeAsZero() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("Sodexo")
					.withDescription("NumT")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(0);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createMappedUserFreeShippingCouponWithNumTxnPaymentTypeAsZeroData")
	public static Object[][] createMappedUserFreeShippingCouponWithNumTxnPaymentTypeAsZero() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("Sodexo")
					.withDescription("NumT")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(0);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createCouponWithNumTxnPaymentTypeAsNullData")
	public static Object[][] createCouponWithNumTxnPaymentTypeAsNull() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withFreeShipping(1)
					.withPreferredPaymentMethod("Sodexo")
					.withDescription("NumT")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(null);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name = "createCouponWithNumTxnPaymentTypeAsOneAndPreferredPaymentMethodAsEmptyStringData")
	public static Object[][] createCouponWithNumTxnPaymentTypeAsOneAndPreferredPaymentMethodAsEmptyString() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withFreeShipping(0)
					.withPreferredPaymentMethod("")
					.withDescription("NumT")
					.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
					.withNumTxnPaymentType(1);
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	@DataProvider(name= "getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneData")
	public static Object[][] getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOne(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("Sodexo")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("Sodexo",0).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}

	@DataProvider(name= "getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentCountIsOneData")
	public static Object[][] getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentCountIsOne(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("Sodexo")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("Sodexo",1).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}
	@DataProvider(name= "getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZeroData")
	public static Object[][] getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZero(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("Sodexo")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(0);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("Sodexo",10).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}
	@DataProvider(name= "getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForNewUserData")
	public static Object[][] getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForNewUser(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("Sodexo")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

//		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
//		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("Sodexo",10).withOrderCountByCartType("Pre",1);

		//payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		//data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}

	@DataProvider(name= "getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneData")
	public static Object[][] getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOne(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("Sodexo")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("Sodexo",0).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}

	@DataProvider(name= "getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOneData")
	public static Object[][] getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOne(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("Sodexo")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("Sodexo",1).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}


	@DataProvider(name= "getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZeroData")
	public static Object[][] getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZero(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("PayLater_Lazypay")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(0);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("PayLater_Lazypay",20).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}
	@DataProvider(name= "getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForNewUserData")
	public static Object[][] getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForNewUser(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("PayLater_Lazypay")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

//		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
//		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("PayLater_Lazypay",20).withOrderCountByCartType("Pre",1);

		//payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		//data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}






























	@DataProvider(name= "applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneData")
	public static Object[][] applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOne(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("AmazonPay")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("AmazonPay",0).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}
	@DataProvider(name= "applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOneData")
	public static Object[][] applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOne(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("AmazonPay")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("AmazonPay",1).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}
	@DataProvider(name= "applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZeroData")
	public static Object[][] applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZero(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("AmazonPay")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(0);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("AmazonPay",11).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}
	@DataProvider(name= "applyUserMappedCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOneData")
	public static Object[][] applyUserMappedCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOne(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(1)
				.withCustomerRestriction(1)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("AmazonPay")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("AmazonPay",1).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}
	@DataProvider(name= "applyPublicCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForFirstOrderUserData")
	public static Object[][] applyPublicCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForFirstOrderUser(){
		HashMap<String,Object> data = new HashMap<String, Object>();
		CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withIsPrivate(0)
				.withCustomerRestriction(0)
				.withCouponType("Discount")
				.withDiscountPercentage(0)
				.withDiscountAmount(20)
				.withCouponUserTypeId(0)
				.withFreeShipping(0)
				.withPreferredPaymentMethod("AmazonPay")
				.withDescription("NumT")
				.withCode("NUMTXNPAY" + Utility.getRandomPostfix())
				.withNumTxnPaymentType(1);

		List<OrderCountByPaymentMethodPOJO> payLoadObject = new ArrayList<>();
		OrderCountByPaymentMethodPOJO orderCountByPaymentMethodPOJO = new OrderCountByPaymentMethodPOJO().setDefaultData().withOrderCountByPaymentMethod("AmazonPay",0).withOrderCountByCartType("Pre",1);

		payLoadObject.add(orderCountByPaymentMethodPOJO);
		data.put("createCouponPOJO", createCouponPOJO);
		data.put("orderCountByPaymentMethodPOJO",payLoadObject);

		return new Object[][] {{data}};
	}





















}


