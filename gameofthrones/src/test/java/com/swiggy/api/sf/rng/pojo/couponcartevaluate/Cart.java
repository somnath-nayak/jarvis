package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import com.swiggy.api.sf.rng.pojo.OrderRequest;
import org.codehaus.jackson.annotate.JsonProperty;

public class Cart {
	@JsonProperty("restaurantListing")
	private RestaurantListing restaurantListing;
	@JsonProperty("cartBlob")
	private CartBlob cartBlob;
	@JsonProperty("cartUser")
	private CartUser cartUser;
	@JsonProperty("cartPrice")
	private CartPrice cartPrice;
	@JsonProperty("superUser")
	private Boolean superUser;
	@JsonProperty("typeOfPartner")
	private Object typeOfPartner;
	@JsonProperty("swiggyMoneyApplicable")
	private Boolean swiggyMoneyApplicable;
	@JsonProperty("orderRequest")
	private OrderRequest orderRequest;

	@JsonProperty("restaurantListing")
	public RestaurantListing getRestaurantListing() {
		return restaurantListing;
	}

	@JsonProperty("restaurantListing")
	public void setRestaurantListing(RestaurantListing restaurantListing) {
		this.restaurantListing = restaurantListing;
	}

	public Cart withRestaurantListing(RestaurantListing restaurantListing) {
		this.restaurantListing = restaurantListing;
		return this;
	}

	@JsonProperty("cartBlob")
	public CartBlob getCartBlob() {
		return cartBlob;
	}

	@JsonProperty("cartBlob")
	public void setCartBlob(CartBlob cartBlob) {
		this.cartBlob = cartBlob;
	}

	public Cart withCartBlob(CartBlob cartBlob) {
		this.cartBlob = cartBlob;
		return this;
	}

	@JsonProperty("cartUser")
	public CartUser getCartUser() {
		return cartUser;
	}

	@JsonProperty("cartUser")
	public void setCartUser(CartUser cartUser) {
		this.cartUser = cartUser;
	}

	public Cart withCartUser(CartUser cartUser) {
		this.cartUser = cartUser;
		return this;
	}

	@JsonProperty("cartPrice")
	public CartPrice getCartPrice() {
		return cartPrice;
	}

	@JsonProperty("cartPrice")
	public void setCartPrice(CartPrice cartPrice) {
		this.cartPrice = cartPrice;
	}

	public Cart withCartPrice(CartPrice cartPrice) {
		this.cartPrice = cartPrice;
		return this;
	}

	@JsonProperty("superUser")
	public Boolean getSuperUser() {
		return superUser;
	}

	@JsonProperty("superUser")
	public void setSuperUser(Boolean superUser) {
		this.superUser = superUser;
	}

	public Cart withSuperUser(Boolean superUser) {
		this.superUser = superUser;
		return this;
	}

	@JsonProperty("typeOfPartner")
	public Object getTypeOfPartner() {
		return typeOfPartner;
	}

	@JsonProperty("typeOfPartner")
	public void setTypeOfPartner(Object typeOfPartner) {
		this.typeOfPartner = typeOfPartner;
	}

	public Cart withTypeOfPartner(Object typeOfPartner) {
		this.typeOfPartner = typeOfPartner;
		return this;
	}

	@JsonProperty("swiggyMoneyApplicable")
	public Boolean getSwiggyMoneyApplicable() {
		return swiggyMoneyApplicable;
	}

	@JsonProperty("swiggyMoneyApplicable")
	public void setSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
		this.swiggyMoneyApplicable = swiggyMoneyApplicable;
	}

	public Cart withSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
		this.swiggyMoneyApplicable = swiggyMoneyApplicable;
		return this;
	}

	@JsonProperty("orderRequest")
	public OrderRequest getOrderRequest() {
		return orderRequest;
	}

	@JsonProperty("orderRequest")
	public void setOrderRequest(OrderRequest orderRequest) {
		this.orderRequest = orderRequest;
	}

	public Cart withOrderRequest(OrderRequest orderRequest) {
		this.orderRequest = orderRequest;
		return this;
	}

	public Cart setDefaultData(Boolean firstOrder, Integer editInitSource, Integer menuItemID, String restID,
			Integer userID, Double cartTotal) {
		return this.withRestaurantListing(null).withCartBlob(new CartBlob().setDefaultData(menuItemID, restID))
				.withCartUser(new CartUser().withUserId(userID).withSwiggyMoney(0.0).withFirstOrder(firstOrder))
				.withCartPrice(
						new CartPrice().withQuantity(1).withCartTotal(cartTotal).withDefaultPreferredPaymentMethod()
								.withReferralPresence(true).withSwiggyMoneyApplicable(false).withItemLevelPrice(null))
				.withSuperUser(false).withTypeOfPartner(null).withSwiggyMoneyApplicable(false)
				.withOrderRequest(new OrderRequest().withOrderEdited(true).withOrderEditedSource(editInitSource));
	}

	public Cart setAreaAndCityEntityID(Integer areaEntityID, Integer cityEntityID) {
		return this.withRestaurantListing(
				new RestaurantListing().withArea(new Area().withAreaEntity(new AreaEntity().withId(areaEntityID)))
						.withCity(new City().withCityEntity(new CityEntity().withId(cityEntityID))));
	}
}
