package com.swiggy.api.sf.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class COLLECTION {

    private String latlng;

    /**
     * No args constructor for use in serialization
     *
     */
    public COLLECTION() {
    }

    /**
     *
     * @param latlng
     */
    public COLLECTION(String latlng) {
        super();
        this.latlng = latlng;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("latlng", latlng).toString();
    }
}
