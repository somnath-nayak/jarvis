package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "variation_id",
        "group_id",
        "name",
        "price"
})
public class Variant {

    @JsonProperty("variation_id")
    private Integer variationId;
    @JsonProperty("group_id")
    private Integer groupId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private Integer price;

    /**
     * No args constructor for use in serialization
     *
     */
    public Variant() {
    }

    /**
     *
     * @param groupId
     * @param variationId
     * @param price
     * @param name
     */
    public Variant(Integer variationId, Integer groupId, String name, Integer price) {
        super();
        this.variationId = variationId;
        this.groupId = groupId;
        this.name = name;
        this.price = price;
    }

    @JsonProperty("variation_id")
    public Integer getVariationId() {
        return variationId;
    }

    @JsonProperty("variation_id")
    public void setVariationId(Integer variationId) {
        this.variationId = variationId;
    }

    public Variant withVariationId(Integer variationId) {
        this.variationId = variationId;
        return this;
    }

    @JsonProperty("group_id")
    public Integer getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Variant withGroupId(Integer groupId) {
        this.groupId = groupId;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Variant withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    public Variant withPrice(Integer price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("variationId", variationId).append("groupId", groupId).append("name", name).append("price", price).toString();
    }

}