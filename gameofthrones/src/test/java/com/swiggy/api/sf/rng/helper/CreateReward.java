package com.swiggy.api.sf.rng.helper;

import org.codehaus.jackson.annotate.JsonProperty;

public class  CreateReward{
	
	@JsonProperty("rewardFunction")
	private String rewardFunction;
	@JsonProperty("rewardType")
	private String rewardType;
	@JsonProperty("rewardValue")
	private String rewardValue;
	@JsonProperty("count")
	private String count; 
	
	public CreateReward() {
		this.rewardFunction = null;
		this.rewardType = null;
		this.rewardValue = null;
		this.count = null;
		
	}
	
	public CreateReward(String rewardFunction,String  rewardType, String rewardValue, String count) {
		
		this.rewardFunction = rewardFunction;
		this.rewardType = rewardType;
		this.rewardValue = rewardValue;
		this.count = count;
		
	}
	
	public String getRewardFunction() {
		
		return this.rewardFunction;
	}
	
	public void setRewardFunction(String rewardFunction) {
		this.rewardFunction = rewardFunction;
	}
	
	public String getrewardType() {
		
		return this.rewardType;
	}
	
	public void setRewardType(String rewardType)
	{
		this.rewardType = rewardType;
	}
	
	public String getRewardValue()
	{
		return this.rewardValue;
	}
	
	public void setRewardValue(String rewardValue) {
		this.rewardValue = rewardValue;
	}
	
	public String getCount() {
		return this.count;
	}
	
	public void setCount(String count) {
		this.count = count;
	}
	
	@Override
	public String toString() {
		
		if(!(rewardFunction==null)) {
			
		return " \"rewards\"" + ":" + "[{" 
				+  " \"rewardFunction\"" + ":" + "\""+ rewardFunction  + "\"" +","+
				" \"rewardType\"" + ":" + "\"" + rewardType + "\"" + ","+
				" \"rewardValue\"" + ":"  + rewardValue  + ","+
				" \"count\"" + ":"  + count  +
				" }]";
	}
		return" \"rewards\""+":" +"null";
		
	}

}
	
	
	
	
	
	
	
