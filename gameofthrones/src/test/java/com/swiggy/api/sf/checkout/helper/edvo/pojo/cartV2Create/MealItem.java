package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "mealId",
        "quantity",
        "groups"
})
public class MealItem {

    @JsonProperty("mealId")
    private Integer mealId;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("groups")
    private List<Group> groups = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public MealItem() {
    }

    /**
     *
     * @param mealId
     * @param quantity
     * @param groups
     */
    public MealItem(Integer mealId, Integer quantity, List<Group> groups) {
        super();
        this.mealId = mealId;
        this.quantity = quantity;
        this.groups = groups;
    }

    @JsonProperty("mealId")
    public Integer getMealId() {
        return mealId;
    }

    @JsonProperty("mealId")
    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    public MealItem withMealId(Integer mealId) {
        this.mealId = mealId;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public MealItem withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("groups")
    public List<Group> getGroups() {
        return groups;
    }

    @JsonProperty("groups")
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public MealItem withGroups(List<Group> groups) {
        this.groups = groups;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mealId", mealId).append("quantity", quantity).append("groups", groups).toString();
    }

}