package com.swiggy.api.sf.snd.pojo.DD;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class Slot {

    private String day;
    @JsonProperty("open_time")
    private Integer openTime;
    @JsonProperty("close_time")
    private Integer closeTime;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Integer openTime) {
        this.openTime = openTime;
    }

    public Integer getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("day", day).append("openTime", openTime).append("closeTime", closeTime).toString();
    }

}
