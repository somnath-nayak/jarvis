package com.swiggy.api.sf.rng.constants;

public interface CouponConstants {

	String  invalidCouponUserTyeId="invalid coupon user type id";
	String invalidPaymentMethod="Coupon must have only one payment method";
	String insertCustomerOrders="insert into customer_orders (customer_id,created_at,updated_at, order_count,first_order_city,last_order_city,last_order_time) values (\"#1\",\"#2\",\"#3\",\"#4\",\"#5\",\"#6\",\"#7\");";
	String segmentation_DB = "segmentationDB";
	String couponNotApplicableForUser = "E_COUPON_NOT_APPLICABLE_FOR_USER";

}
