package com.swiggy.api.sf.snd.pojo.Super;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DefaultValue {

    private String superValue;
    private Boolean testBoolean;
    private Integer testInt;
    private Double testFloat;

    public String getSuperValue() {
        return superValue;
    }

    public void setSuperValue(String superValue) {
        this.superValue = superValue;
    }

    public Boolean getTestBoolean() {
        return testBoolean;
    }

    public void setTestBoolean(Boolean testBoolean) {
        this.testBoolean = testBoolean;
    }

    public Integer getTestInt() {
        return testInt;
    }

    public void setTestInt(Integer testInt) {
        this.testInt = testInt;
    }

    public Double getTestFloat() {
        return testFloat;
    }

    public void setTestFloat(Double testFloat) {
        this.testFloat = testFloat;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("superValue", superValue).append("testBoolean", testBoolean).append("testInt", testInt).append("testFloat", testFloat).toString();
    }
}
