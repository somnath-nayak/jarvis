package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "choice_id",
        "group_id",
        "name",
        "price",
        "addon_tax_charges"
})
public class Addon {

    @JsonProperty("choice_id")
    private String choiceId;
    @JsonProperty("group_id")
    private String groupId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("addon_tax_charges")
    private AddonTaxCharges addonTaxCharges;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Addon() {
    }

    /**
     *
     * @param addonTaxCharges
     * @param groupId
     * @param price
     * @param choiceId
     * @param name
     */
    public Addon(String choiceId, String groupId, String name, Integer price, AddonTaxCharges addonTaxCharges) {
        super();
        this.choiceId = choiceId;
        this.groupId = groupId;
        this.name = name;
        this.price = price;
        this.addonTaxCharges = addonTaxCharges;
    }

    @JsonProperty("choice_id")
    public String getChoiceId() {
        return choiceId;
    }

    @JsonProperty("choice_id")
    public void setChoiceId(String choiceId) {
        this.choiceId = choiceId;
    }

    @JsonProperty("group_id")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("addon_tax_charges")
    public AddonTaxCharges getAddonTaxCharges() {
        return addonTaxCharges;
    }

    @JsonProperty("addon_tax_charges")
    public void setAddonTaxCharges(AddonTaxCharges addonTaxCharges) {
        this.addonTaxCharges = addonTaxCharges;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("choiceId", choiceId).append("groupId", groupId).append("name", name).append("price", price).append("addonTaxCharges", addonTaxCharges).append("additionalProperties", additionalProperties).toString();
    }

}