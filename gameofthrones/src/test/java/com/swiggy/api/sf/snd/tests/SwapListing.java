package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutOrderHelper;
import com.swiggy.api.daily.cms.helper.CMSCommonHelper;
import com.swiggy.api.daily.promotions.helper.PromotionConstants;
import com.swiggy.api.daily.promotions.helper.PromotionsHelper;
import com.swiggy.api.sf.snd.constants.DailySandConstants;
import com.swiggy.api.sf.snd.dp.DailyListingDP;
import com.swiggy.api.sf.snd.helper.DailyListingHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SwapListing extends DailyListingDP {
    SANDHelper sandHelper = new SANDHelper();
    DailyListingHelper dailyListingHelper = new DailyListingHelper();
    DateHelper dateHelper = new DateHelper();
    RedisHelper redisHelper = new RedisHelper();
    DailyCheckoutOrderHelper dailyCheckoutOrderHelper = new DailyCheckoutOrderHelper();
    PromotionsHelper promotionsHelper = new PromotionsHelper();
    CMSCommonHelper cmsCommonHelper = new CMSCommonHelper();


    double totalAmountOfSwappableMeal;

    @BeforeTest
    public void LoginAndGetTidSid() throws InterruptedException {
        //redisHelper.flushRedisDB(DailySandConstants.sandRedis,0);
        Processor p = dailyListingHelper.login(DailySandConstants.mobile, DailySandConstants.password);
        int status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(status, DailySandConstants.statusZero, "Login api status is not 0");
        tid = p.ResponseValidator.GetNodeValue("$.tid").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        sid = p.ResponseValidator.GetNodeValue("$.sid").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        //Thread.sleep(2000);
        Reporter.log("-- TID ::: " + tid);
        Reporter.log("-- SID ::: " + sid);
    }




    @Test(dataProvider = "latlong")
    public void compareTheNumberOfOffsets(String lat, String lon) throws Exception {

        int totalOffsetOfMealListing = 0;
        int totalOffsetOfSwapListing = 0;


        /*Making a meal listing call to get the number of items or offsets*/
        /*Here we are sending Dinner as slot type hardcoded. we can change whenever we want */

        String mealId = "";

        String orderId = dailyCheckoutOrderHelper.dailyCreateOrder(DailySandConstants.mobile, DailySandConstants.password, "75", "39534", "PLAN");
        System.out.println(orderId);

        /*Now calling subscription details call to get the next meal details and meal id*/
        String responseOfSubscriptionDetails = dailyListingHelper.getMealId(orderId).ResponseValidator.GetBodyAsText();
        System.out.println("Subscriptiion details response : " + responseOfSubscriptionDetails);

        int statusCodeOfSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.statusCode");

        if (statusCodeOfSubscriptionDetails == 1) {
            mealId = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.subscriptionMealId");

        }


        String responseOfMealListing = dailyListingHelper.mealListingWithSlot(tid, sid, lat, lon, "DINNER").ResponseValidator.GetBodyAsText();
        System.out.println(responseOfMealListing);
        int statusCodeOfMealListing = JsonPath.read(responseOfMealListing, "$.statusCode");
        if (statusCodeOfMealListing == 0) {
            totalOffsetOfMealListing = JsonPath.read(responseOfMealListing, "$.data.totalOffset");
        }

        /*Making swap listing call and check the offset*/
        System.out.println("tid :" + tid + "meal id :" + mealId);
        String responseOfSwapListing = dailyListingHelper.swapListingCall(tid, mealId).ResponseValidator.GetBodyAsText();
        int statusCodeOfSwapListing = JsonPath.read(responseOfSwapListing, "$.statusCode");
        if (statusCodeOfSwapListing == 0) {
            totalOffsetOfSwapListing = JsonPath.read(responseOfMealListing, "$.data.totalOffset");
        }

        if (totalOffsetOfMealListing == (totalOffsetOfSwapListing + 1)) {
            Assert.assertTrue(true);
        }

    }


    @Test(dataProvider = "latlong" , description = "Newer price is more than the older meal price even after the discount on newer price and customer pays it")
    public void customerPaysFinalAmountInSwapListingWhenThereIsDiscount(String lat, String lon) throws Exception {

        String mealId = "";

        /* For this test case we need to compare the swappable meal price and the new price with discount and if the newer price is more, then we have to pass
        1. Create a subscription where store id and city id are hard coded*/

        String orderId = dailyCheckoutOrderHelper.dailyCreateOrder(DailySandConstants.mobile, DailySandConstants.password, "75", "39534", "PLAN");
        System.out.println(orderId);

        /*Now calling subscription details call to get the next meal details and meal id*/

        String responseOfSubscriptionDetails = dailyListingHelper.getMealId(orderId).ResponseValidator.GetBodyAsText();
        System.out.println("Subscriptiion details response : " + responseOfSubscriptionDetails);

        int statusCodeOfSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.statusCode");
        String spinIdFromSubscriptionDetails = "";

        if (statusCodeOfSubscriptionDetails == 1) {
            mealId = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.subscriptionMealId");
            spinIdFromSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.mealId");
        }

        /*For a given meal id which can be swapped, we have to get the price and for this we are hitting a meta call as below.*/

        Processor processor = dailyListingHelper.metaValuesForSwappableMealId("true", "SWAP", "true", mealId);
        String responseOfSwappableMealId = processor.ResponseValidator.GetBodyAsText();
        System.out.println(responseOfSwappableMealId);

        int statusCodeOfSwappableResponse = JsonPath.read(responseOfSwappableMealId, "$.statusCode");

        String spinId = "";
        String storeId = "";

        double totalAmountOfSwappableMeal = 0.0;

        if (statusCodeOfSwappableResponse == 1) {
            totalAmountOfSwappableMeal = JsonPath.read(responseOfSwappableMealId, "$.data.itemDetails.itemBill.total");
            storeId = JsonPath.read(responseOfSwappableMealId, "$.data.storeSpin.storeId");
            System.out.println("totalAmountOfSwappableMeal = " + totalAmountOfSwappableMeal + "  ;  " + "Spin ID = " + spinId + "  ;  " + "Store Id  = " + storeId);
        }


        List<String> listOfSpinIds = new ArrayList();
        listOfSpinIds.add("SF2PN50UYS");
        listOfSpinIds.add("MQY3WG3CQW");
        listOfSpinIds.add("T5T669MEWX");
        listOfSpinIds.add("3I9E8S151C");
        listOfSpinIds.add("NRPYH48166");
        listOfSpinIds.add("RBQFP2X7JD");

        String newSpinId = "";
        for (String spinIdFromDetails : listOfSpinIds) {
            if (!spinIdFromSubscriptionDetails.equalsIgnoreCase(spinIdFromDetails)) {
                newSpinId = spinIdFromDetails;
                break;
            }
        }

         /*Setting the price for the new spin. This is done because of below test case
         - After the discount if customer has to pay for the new meal, then the new meal price has to be higher even after discount*/

        cmsCommonHelper.setPrice(storeId, newSpinId, ((int) totalAmountOfSwappableMeal + 5), "MEAL");

        /*Now calling the promotion helper to create the promotion for the new spin where we have set the price*/

        String promotionDiscountValue = "200";
        HashMap<String, String> mapOfPromotionDetails = promotionsHelper.createPromotion(PromotionConstants.actionType[0], promotionDiscountValue, "39534", newSpinId);
        System.out.println(mapOfPromotionDetails);

        /* We are making swap listing call to get check the price and discounted price. This is simplified by connecting to a redis and
        getting the whole response then parsing the spin and its price and discounted values.*/

        String get_redis_key_swap_meal = dailyListingHelper.redisKeyGeneratorForSwapMeal(tid, mealId, "SWAP_MEAL", lat, lon);

        String responseOfSwapListing = dailyListingHelper.swapListingCall(tid, mealId).ResponseValidator.GetBodyAsText();
        int statusCodeOfSwapListing = JsonPath.read(responseOfSwapListing, "$.statusCode");

        if (statusCodeOfSwapListing == 0) {

            Map<String, String> mapOfRedisKey = redisHelper.hKeysValues(DailySandConstants.sandRedis, 0, get_redis_key_swap_meal);
            String swapMealDataFromRedis = mapOfRedisKey.get("0");
            System.out.println("Redis value of swap listing :" + swapMealDataFromRedis);

            HashMap<String, Object> mapOfSpinStoreAndPrice = dailyListingHelper.getSpinPriceForASpinId(newSpinId, swapMealDataFromRedis);
            System.out.println(mapOfSpinStoreAndPrice);

            /*Here we are taking below values
            1. New price = mapOfSpinStoreAndPrice.get("price")
            2. New discounted Price = mapOfSpinStoreAndPrice.get("discountedPrice")
            */

            double newPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("price").toString());
            double newDiscountedPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("discountedPrice").toString());
            totalAmountOfSwappableMeal = totalAmountOfSwappableMeal * 100;
            double promotionDiscount = Double.parseDouble(promotionDiscountValue.toString());
            if ((newPrice - totalAmountOfSwappableMeal - promotionDiscount) == newDiscountedPrice) {
                Assert.assertTrue(true);

            } else {
                Assert.assertTrue(false);
            }

        }


    }


    @Test(dataProvider = "latlong")
    public void refundToCustomerInSwapListingWhenThereIsDiscount(String lat, String lon) throws Exception {

        String mealId = "";

        /* For this test case we need to compare the swappable meal price and the new price with discount and if the newer price is more, then we have to pass
        1. Create a subscription where store id and city id are hard coded*/

        String orderId = dailyCheckoutOrderHelper.dailyCreateOrder(DailySandConstants.mobile, DailySandConstants.password, "75", "39534", "PLAN");
        System.out.println(orderId);

        /*Now calling subscription details call to get the next meal details and meal id*/

        String responseOfSubscriptionDetails = dailyListingHelper.getMealId(orderId).ResponseValidator.GetBodyAsText();
        System.out.println("Subscriptiion details response : " + responseOfSubscriptionDetails);

        int statusCodeOfSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.statusCode");
        String spinIdFromSubscriptionDetails = "";

        if (statusCodeOfSubscriptionDetails == 1) {
            mealId = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.subscriptionMealId");
            spinIdFromSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.mealId");
        }

        /*For a given meal id which can be swapped, we have to get the price and for this we are hitting a meta call as below.*/

        Processor processor = dailyListingHelper.metaValuesForSwappableMealId("true", "SWAP", "true", mealId);
        String responseOfSwappableMealId = processor.ResponseValidator.GetBodyAsText();
        System.out.println(responseOfSwappableMealId);

        int statusCodeOfSwappableResponse = JsonPath.read(responseOfSwappableMealId, "$.statusCode");

        String spinId = "";
        String storeId = "";

        double totalAmountOfSwappableMeal = 0.0;

        if (statusCodeOfSwappableResponse == 1) {
            totalAmountOfSwappableMeal = JsonPath.read(responseOfSwappableMealId, "$.data.itemDetails.itemBill.total");
            storeId = JsonPath.read(responseOfSwappableMealId, "$.data.storeSpin.storeId");
            System.out.println("totalAmountOfSwappableMeal = " + totalAmountOfSwappableMeal + "  ;  " + "Spin ID = " + spinId + "  ;  " + "Store Id  = " + storeId);
        }


        List<String> listOfSpinIds = new ArrayList();
        listOfSpinIds.add("SF2PN50UYS");
        listOfSpinIds.add("MQY3WG3CQW");
        listOfSpinIds.add("T5T669MEWX");
        listOfSpinIds.add("3I9E8S151C");
        listOfSpinIds.add("NRPYH48166");
        listOfSpinIds.add("RBQFP2X7JD");

        String newSpinId = "";
        for (String spinIdFromDetails : listOfSpinIds) {
            if (!spinIdFromSubscriptionDetails.equalsIgnoreCase(spinIdFromDetails)) {
                newSpinId = spinIdFromDetails;
                break;
            }
        }

         /*Setting the price for the new spin. This is done because of below test case
         - After the discount if customer has to pay for the new meal, then the new meal price has to be higher even after discount*/

        cmsCommonHelper.setPrice(storeId, newSpinId, ((int) totalAmountOfSwappableMeal ), "MEAL");

        /*Now calling the promotion helper to create the promotion for the new spin where we have set the price*/

        String promotionDiscountValue = "100";
        HashMap<String, String> mapOfPromotionDetails = promotionsHelper.createPromotion(PromotionConstants.actionType[0], promotionDiscountValue, "39534", newSpinId);
        System.out.println(mapOfPromotionDetails);

        /* We are making swap listing call to get check the price and discounted price. This is simplified by connecting to a redis and
        getting the whole response then parsing the spin and its price and discounted values.*/

        String get_redis_key_swap_meal = dailyListingHelper.redisKeyGeneratorForSwapMeal(tid, mealId, "SWAP_MEAL", lat, lon);

        String responseOfSwapListing = dailyListingHelper.swapListingCall(tid, mealId).ResponseValidator.GetBodyAsText();
        int statusCodeOfSwapListing = JsonPath.read(responseOfSwapListing, "$.statusCode");

        if (statusCodeOfSwapListing == 0) {

            Map<String, String> mapOfRedisKey = redisHelper.hKeysValues(DailySandConstants.sandRedis, 0, get_redis_key_swap_meal);
            String swapMealDataFromRedis = mapOfRedisKey.get("0");
            System.out.println("Redis value of swap listing :" + swapMealDataFromRedis);

            HashMap<String, Object> mapOfSpinStoreAndPrice = dailyListingHelper.getSpinPriceForASpinId(newSpinId, swapMealDataFromRedis);
            System.out.println(mapOfSpinStoreAndPrice);

            /*Here we are taking below values
            1. New price = mapOfSpinStoreAndPrice.get("price")
            2. New discounted Price = mapOfSpinStoreAndPrice.get("discountedPrice")
            */

            double newPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("price").toString());
            double newDiscountedPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("discountedPrice").toString());
            totalAmountOfSwappableMeal = totalAmountOfSwappableMeal * 100;
            double promotionDiscount = Double.parseDouble(promotionDiscountValue);

            if (((newPrice - totalAmountOfSwappableMeal) - promotionDiscount ) == newDiscountedPrice) {
                Assert.assertTrue(true);

            } else {
                Assert.assertTrue(false);
            }

        }
    }


    @Test(dataProvider = "latlong")
    public void customerPaysFinalAmountInSwapListingWhenNoDiscount(String lat, String lon) throws Exception{

        String mealId = "";

        /* For this test case we need to compare the swappable meal price and the new price with discount and if the newer price is more, then we have to pass
        1. Create a subscription where store id and city id are hard coded*/

        String orderId = dailyCheckoutOrderHelper.dailyCreateOrder(DailySandConstants.mobile, DailySandConstants.password, "75", "39534", "PLAN");
        System.out.println(orderId);

        /*Now calling subscription details call to get the next meal details and meal id*/

        String responseOfSubscriptionDetails = dailyListingHelper.getMealId(orderId).ResponseValidator.GetBodyAsText();
        System.out.println("Subscriptiion details response : " + responseOfSubscriptionDetails);

        int statusCodeOfSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.statusCode");
        String spinIdFromSubscriptionDetails = "";

        if (statusCodeOfSubscriptionDetails == 1) {
            mealId = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.subscriptionMealId");
            spinIdFromSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.mealId");
        }

        /*For a given meal id which can be swapped, we have to get the price and for this we are hitting a meta call as below.*/

        Processor processor = dailyListingHelper.metaValuesForSwappableMealId("true", "SWAP", "true", mealId);
        String responseOfSwappableMealId = processor.ResponseValidator.GetBodyAsText();
        System.out.println(responseOfSwappableMealId);

        int statusCodeOfSwappableResponse = JsonPath.read(responseOfSwappableMealId, "$.statusCode");

        String spinId = "";
        String storeId = "";

        double totalAmountOfSwappableMeal = 0.0;

        if (statusCodeOfSwappableResponse == 1) {
            totalAmountOfSwappableMeal = JsonPath.read(responseOfSwappableMealId, "$.data.itemDetails.itemBill.total");
            storeId = JsonPath.read(responseOfSwappableMealId, "$.data.storeSpin.storeId");
            System.out.println("totalAmountOfSwappableMeal = " + totalAmountOfSwappableMeal + "  ;  " + "Spin ID = " + spinId + "  ;  " + "Store Id  = " + storeId);
        }


        List<String> listOfSpinIds = new ArrayList();
        listOfSpinIds.add("SF2PN50UYS");
        listOfSpinIds.add("MQY3WG3CQW");
        listOfSpinIds.add("T5T669MEWX");
        listOfSpinIds.add("3I9E8S151C");
        listOfSpinIds.add("NRPYH48166");
        listOfSpinIds.add("RBQFP2X7JD");

        String newSpinId = "";
        for (String spinIdFromDetails : listOfSpinIds) {
            if (!spinIdFromSubscriptionDetails.equalsIgnoreCase(spinIdFromDetails)) {
                newSpinId = spinIdFromDetails;
                break;
            }
        }

         /*Setting the price for the new spin. This is done because of below test case
         - After the discount if customer has to pay for the new meal, then the new meal price has to be higher even after discount*/

        cmsCommonHelper.setPrice(storeId, newSpinId, ((int) totalAmountOfSwappableMeal + 5), "MEAL");


        /* We are making swap listing call to get check the price and discounted price. This is simplified by connecting to a redis and
        getting the whole response then parsing the spin and its price and discounted values.*/

        String get_redis_key_swap_meal = dailyListingHelper.redisKeyGeneratorForSwapMeal(tid, mealId, "SWAP_MEAL", lat, lon);

        String responseOfSwapListing = dailyListingHelper.swapListingCall(tid, mealId).ResponseValidator.GetBodyAsText();
        int statusCodeOfSwapListing = JsonPath.read(responseOfSwapListing, "$.statusCode");

        if (statusCodeOfSwapListing == 0) {

            Map<String, String> mapOfRedisKey = redisHelper.hKeysValues(DailySandConstants.sandRedis, 0, get_redis_key_swap_meal);
            String swapMealDataFromRedis = mapOfRedisKey.get("0");
            System.out.println("Redis value of swap listing :" + swapMealDataFromRedis);

            HashMap<String, Object> mapOfSpinStoreAndPrice = dailyListingHelper.getSpinPriceForASpinId(newSpinId, swapMealDataFromRedis);
            System.out.println(mapOfSpinStoreAndPrice);

            /*Here we are taking below values
            1. New price = mapOfSpinStoreAndPrice.get("price")
            2. New discounted Price = mapOfSpinStoreAndPrice.get("discountedPrice")
            */

            double newPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("price").toString());
            double newDiscountedPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("discountedPrice").toString());
            totalAmountOfSwappableMeal = totalAmountOfSwappableMeal * 100;

            if ((newPrice - totalAmountOfSwappableMeal) == newDiscountedPrice) {
                Assert.assertTrue(true);

            } else {
                Assert.assertTrue(false);
            }

        }


    }

    @Test(dataProvider = "latlong")
    public void refundToCustomerInSwapListingWhenNoDiscount(String lat, String lon) throws Exception {

        String mealId = "";

        /* For this test case we need to compare the swappable meal price and the new price with discount and if the newer price is more, then we have to pass
        1. Create a subscription where store id and city id are hard coded*/

        String orderId = dailyCheckoutOrderHelper.dailyCreateOrder(DailySandConstants.mobile, DailySandConstants.password, "75", "39534", "PLAN");
        System.out.println(orderId);

        /*Now calling subscription details call to get the next meal details and meal id*/

        String responseOfSubscriptionDetails = dailyListingHelper.getMealId(orderId).ResponseValidator.GetBodyAsText();
        System.out.println("Subscriptiion details response : " + responseOfSubscriptionDetails);

        int statusCodeOfSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.statusCode");
        String spinIdFromSubscriptionDetails = "";

        if (statusCodeOfSubscriptionDetails == 1) {
            mealId = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.subscriptionMealId");
            spinIdFromSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.mealId");
        }

        /*For a given meal id which can be swapped, we have to get the price and for this we are hitting a meta call as below.*/

        Processor processor = dailyListingHelper.metaValuesForSwappableMealId("true", "SWAP", "true", mealId);
        String responseOfSwappableMealId = processor.ResponseValidator.GetBodyAsText();
        System.out.println(responseOfSwappableMealId);

        int statusCodeOfSwappableResponse = JsonPath.read(responseOfSwappableMealId, "$.statusCode");

        String spinId = "";
        String storeId = "";

        double totalAmountOfSwappableMeal = 0.0;

        if (statusCodeOfSwappableResponse == 1) {
            totalAmountOfSwappableMeal = JsonPath.read(responseOfSwappableMealId, "$.data.itemDetails.itemBill.total");
            storeId = JsonPath.read(responseOfSwappableMealId, "$.data.storeSpin.storeId");
            System.out.println("totalAmountOfSwappableMeal = " + totalAmountOfSwappableMeal + "  ;  " + "Spin ID = " + spinId + "  ;  " + "Store Id  = " + storeId);
        }


        List<String> listOfSpinIds = new ArrayList();
        listOfSpinIds.add("SF2PN50UYS");
        listOfSpinIds.add("MQY3WG3CQW");
        listOfSpinIds.add("T5T669MEWX");
        listOfSpinIds.add("3I9E8S151C");
        listOfSpinIds.add("NRPYH48166");
        listOfSpinIds.add("RBQFP2X7JD");

        String newSpinId = "";
        for (String spinIdFromDetails : listOfSpinIds) {
            if (!spinIdFromSubscriptionDetails.equalsIgnoreCase(spinIdFromDetails)) {
                newSpinId = spinIdFromDetails;
                break;
            }
        }

         /*Setting the price for the new spin. This is done because of below test case
         - After the discount if customer has to pay for the new meal, then the new meal price has to be higher even after discount*/

        cmsCommonHelper.setPrice(storeId, newSpinId, ((int) totalAmountOfSwappableMeal -1), "MEAL");


        /* We are making swap listing call to get check the price and discounted price. This is simplified by connecting to a redis and
        getting the whole response then parsing the spin and its price and discounted values.*/

        String get_redis_key_swap_meal = dailyListingHelper.redisKeyGeneratorForSwapMeal(tid, mealId, "SWAP_MEAL", lat, lon);

        String responseOfSwapListing = dailyListingHelper.swapListingCall(tid, mealId).ResponseValidator.GetBodyAsText();
        int statusCodeOfSwapListing = JsonPath.read(responseOfSwapListing, "$.statusCode");

        if (statusCodeOfSwapListing == 0) {

            Map<String, String> mapOfRedisKey = redisHelper.hKeysValues(DailySandConstants.sandRedis, 0, get_redis_key_swap_meal);
            String swapMealDataFromRedis = mapOfRedisKey.get("0");
            System.out.println("Redis value of swap listing :" + swapMealDataFromRedis);

            HashMap<String, Object> mapOfSpinStoreAndPrice = dailyListingHelper.getSpinPriceForASpinId(newSpinId, swapMealDataFromRedis);
            System.out.println(mapOfSpinStoreAndPrice);

            /*Here we are taking below values
            1. New price = mapOfSpinStoreAndPrice.get("price")
            2. New discounted Price = mapOfSpinStoreAndPrice.get("discountedPrice")
            */

            double newPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("price").toString());
            double newDiscountedPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("discountedPrice").toString());
            totalAmountOfSwappableMeal = totalAmountOfSwappableMeal * 100;


            if ((newPrice - totalAmountOfSwappableMeal) == newDiscountedPrice) {
                Assert.assertTrue(true);

            } else {
                Assert.assertTrue(false);
            }

        }

    }


    @Test(dataProvider = "latlong")
    public void customerPaysNothingInSwapListingWhenNoDiscount(String lat, String lon) throws Exception{

        String mealId = "";

        /* For this test case we need to compare the swappable meal price and the new price with discount and if the newer price is more, then we have to pass
        1. Create a subscription where store id and city id are hard coded*/

        String orderId = dailyCheckoutOrderHelper.dailyCreateOrder(DailySandConstants.mobile, DailySandConstants.password, "75", "39534", "PLAN");
        System.out.println(orderId);

        /*Now calling subscription details call to get the next meal details and meal id*/

        String responseOfSubscriptionDetails = dailyListingHelper.getMealId(orderId).ResponseValidator.GetBodyAsText();
        System.out.println("Subscriptiion details response : " + responseOfSubscriptionDetails);

        int statusCodeOfSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.statusCode");
        String spinIdFromSubscriptionDetails = "";

        if (statusCodeOfSubscriptionDetails == 1) {
            mealId = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.subscriptionMealId");
            spinIdFromSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.mealId");
        }

        /*For a given meal id which can be swapped, we have to get the price and for this we are hitting a meta call as below.*/

        Processor processor = dailyListingHelper.metaValuesForSwappableMealId("true", "SWAP", "true", mealId);
        String responseOfSwappableMealId = processor.ResponseValidator.GetBodyAsText();
        System.out.println(responseOfSwappableMealId);

        int statusCodeOfSwappableResponse = JsonPath.read(responseOfSwappableMealId, "$.statusCode");

        String spinId = "";
        String storeId = "";

        double totalAmountOfSwappableMeal = 0.0;

        if (statusCodeOfSwappableResponse == 1) {
            totalAmountOfSwappableMeal = JsonPath.read(responseOfSwappableMealId, "$.data.itemDetails.itemBill.total");
            storeId = JsonPath.read(responseOfSwappableMealId, "$.data.storeSpin.storeId");
            System.out.println("totalAmountOfSwappableMeal = " + totalAmountOfSwappableMeal + "  ;  " + "Spin ID = " + spinId + "  ;  " + "Store Id  = " + storeId);
        }


        List<String> listOfSpinIds = new ArrayList();
        listOfSpinIds.add("SF2PN50UYS");
        listOfSpinIds.add("MQY3WG3CQW");
        listOfSpinIds.add("T5T669MEWX");
        listOfSpinIds.add("3I9E8S151C");
        listOfSpinIds.add("NRPYH48166");
        listOfSpinIds.add("RBQFP2X7JD");

        String newSpinId = "";
        for (String spinIdFromDetails : listOfSpinIds) {
            if (!spinIdFromSubscriptionDetails.equalsIgnoreCase(spinIdFromDetails)) {
                newSpinId = spinIdFromDetails;
                break;
            }
        }

         /*Setting the price for the new spin. This is done because of below test case
         - After the discount if customer has to pay for the new meal, then the new meal price has to be higher even after discount*/

        cmsCommonHelper.setPrice(storeId, newSpinId, ((int) totalAmountOfSwappableMeal), "MEAL");


        /*Now calling the promotion helper to create the promotion for the new spin where we have set the price*/

        String promotionDiscountValue = "000";
        HashMap<String, String> mapOfPromotionDetails = promotionsHelper.createPromotion(PromotionConstants.actionType[0], promotionDiscountValue, "39534", newSpinId);
        System.out.println(mapOfPromotionDetails);


        /* We are making swap listing call to get check the price and discounted price. This is simplified by connecting to a redis and
        getting the whole response then parsing the spin and its price and discounted values.*/

        String get_redis_key_swap_meal = dailyListingHelper.redisKeyGeneratorForSwapMeal(tid, mealId, "SWAP_MEAL", lat, lon);

        String responseOfSwapListing = dailyListingHelper.swapListingCall(tid, mealId).ResponseValidator.GetBodyAsText();
        int statusCodeOfSwapListing = JsonPath.read(responseOfSwapListing, "$.statusCode");

        if (statusCodeOfSwapListing == 0) {

            Map<String, String> mapOfRedisKey = redisHelper.hKeysValues(DailySandConstants.sandRedis, 0, get_redis_key_swap_meal);
            String swapMealDataFromRedis = mapOfRedisKey.get("0");
            System.out.println("Redis value of swap listing :" + swapMealDataFromRedis);

            HashMap<String, Object> mapOfSpinStoreAndPrice = dailyListingHelper.getSpinPriceForASpinId(newSpinId, swapMealDataFromRedis);
            System.out.println(mapOfSpinStoreAndPrice);

            /*Here we are taking below values
            1. New price = mapOfSpinStoreAndPrice.get("price")
            2. New discounted Price = mapOfSpinStoreAndPrice.get("discountedPrice")
            */

            double newPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("price").toString());
            double newDiscountedPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("discountedPrice").toString());
            totalAmountOfSwappableMeal = totalAmountOfSwappableMeal * 100;


            if ((newPrice - totalAmountOfSwappableMeal) == newDiscountedPrice) {
                Assert.assertTrue(true);

            } else {
                Assert.assertTrue(false);
            }

        }
    }

    @Test(dataProvider = "latlong")
    public void customerPaysNothingInSwapListingWhenThereIsDiscount(String lat, String lon) throws Exception {

        String mealId = "";

        /* For this test case we need to compare the swappable meal price and the new price with discount and if the newer price is more, then we have to pass
        1. Create a subscription where store id and city id are hard coded*/

        String orderId = dailyCheckoutOrderHelper.dailyCreateOrder(DailySandConstants.mobile, DailySandConstants.password, "75", "39534", "PLAN");
        System.out.println(orderId);

        /*Now calling subscription details call to get the next meal details and meal id*/

        String responseOfSubscriptionDetails = dailyListingHelper.getMealId(orderId).ResponseValidator.GetBodyAsText();
        System.out.println("Subscriptiion details response : " + responseOfSubscriptionDetails);

        int statusCodeOfSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.statusCode");
        String spinIdFromSubscriptionDetails = "";

        if (statusCodeOfSubscriptionDetails == 1) {
            mealId = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.subscriptionMealId");
            spinIdFromSubscriptionDetails = JsonPath.read(responseOfSubscriptionDetails, "$.data.nextMeal.mealDetails.mealId");
        }

        /*For a given meal id which can be swapped, we have to get the price and for this we are hitting a meta call as below.*/

        Processor processor = dailyListingHelper.metaValuesForSwappableMealId("true", "SWAP", "true", mealId);
        String responseOfSwappableMealId = processor.ResponseValidator.GetBodyAsText();
        System.out.println(responseOfSwappableMealId);

        int statusCodeOfSwappableResponse = JsonPath.read(responseOfSwappableMealId, "$.statusCode");

        String spinId = "";
        String storeId = "";

        double totalAmountOfSwappableMeal = 0.0;

        if (statusCodeOfSwappableResponse == 1) {
            totalAmountOfSwappableMeal = JsonPath.read(responseOfSwappableMealId, "$.data.itemDetails.itemBill.total");
            storeId = JsonPath.read(responseOfSwappableMealId, "$.data.storeSpin.storeId");
            System.out.println("totalAmountOfSwappableMeal = " + totalAmountOfSwappableMeal + "  ;  " + "Spin ID = " + spinId + "  ;  " + "Store Id  = " + storeId);
        }


        List<String> listOfSpinIds = new ArrayList();
        listOfSpinIds.add("SF2PN50UYS");
        listOfSpinIds.add("MQY3WG3CQW");
        listOfSpinIds.add("T5T669MEWX");
        listOfSpinIds.add("3I9E8S151C");
        listOfSpinIds.add("NRPYH48166");
        listOfSpinIds.add("RBQFP2X7JD");

        String newSpinId = "";
        for (String spinIdFromDetails : listOfSpinIds) {
            if (!spinIdFromSubscriptionDetails.equalsIgnoreCase(spinIdFromDetails)) {
                newSpinId = spinIdFromDetails;
                break;
            }
        }

         /*Setting the price for the new spin. This is done because of below test case
         - After the discount if customer has to pay for the new meal, then the new meal price has to be higher even after discount*/

        cmsCommonHelper.setPrice(storeId, newSpinId, ((int) totalAmountOfSwappableMeal + 3), "MEAL");

        /*Now calling the promotion helper to create the promotion for the new spin where we have set the price*/

        String promotionDiscountValue = Double.toString((totalAmountOfSwappableMeal+2)*100);
        HashMap<String, String> mapOfPromotionDetails = promotionsHelper.createPromotion(PromotionConstants.actionType[0], promotionDiscountValue, "39534", newSpinId);
        System.out.println(mapOfPromotionDetails);

        /* We are making swap listing call to check the price and discounted price. This is simplified by connecting to a redis and
        getting the whole response then parsing the spin and its price and discounted values.*/

        String get_redis_key_swap_meal = dailyListingHelper.redisKeyGeneratorForSwapMeal(tid, mealId, "SWAP_MEAL", lat, lon);

        String responseOfSwapListing = dailyListingHelper.swapListingCall(tid, mealId).ResponseValidator.GetBodyAsText();
        int statusCodeOfSwapListing = JsonPath.read(responseOfSwapListing, "$.statusCode");

        if (statusCodeOfSwapListing == 0) {

            Map<String, String> mapOfRedisKey = redisHelper.hKeysValues(DailySandConstants.sandRedis, 0, get_redis_key_swap_meal);
            String swapMealDataFromRedis = mapOfRedisKey.get("0");
            System.out.println("Redis value of swap listing :" + swapMealDataFromRedis);

            HashMap<String, Object> mapOfSpinStoreAndPrice = dailyListingHelper.getSpinPriceForASpinId(newSpinId, swapMealDataFromRedis);
            System.out.println(mapOfSpinStoreAndPrice);

            /*Here we are taking below values
            1. New price = mapOfSpinStoreAndPrice.get("price")
            2. New discounted Price = mapOfSpinStoreAndPrice.get("discountedPrice")
            */

            double newPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("price").toString());
            double newDiscountedPrice = Double.parseDouble(mapOfSpinStoreAndPrice.get("discountedPrice").toString());
            totalAmountOfSwappableMeal = totalAmountOfSwappableMeal * 100;
            double promotionDiscount = Double.parseDouble(promotionDiscountValue);

            if ((newPrice - totalAmountOfSwappableMeal - promotionDiscount) == newDiscountedPrice) {
                Assert.assertTrue(true);

            } else {
                Assert.assertTrue(false);
            }

        }


    }

    @AfterMethod
    public void closeRedisConnections() {


    }


}
