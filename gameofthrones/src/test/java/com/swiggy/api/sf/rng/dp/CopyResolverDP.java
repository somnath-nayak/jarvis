package com.swiggy.api.sf.rng.dp;

import com.swiggy.api.sf.rng.constants.CopyConstants;
import com.swiggy.api.sf.rng.helper.CopyResolverHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.SuperHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.ItemRequest;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateListingV3.EvaluateListingV3;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu.EvaluateMenu;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu.ItemRequests;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class CopyResolverDP {

    //CopyResolverHelper copyResolverHelper = new CopyResolverHelper();
    EvaluateListingV3 evaluateListingV3 = new EvaluateListingV3();

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRestMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRestMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRestMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRestMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNoMOVAndRestMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNoMOVAndRestMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(0, false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNoMOVAndRestMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithNoMOVAndRestMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(0, false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercentMaxDiscountMOVNRFreebieMOVWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercentMaxDiscountMOVNRFreebieMOVWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercentMaxDiscountMOVNRFreebieMOVWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercentMaxDiscountMOVNRFreebieMOVWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithMOVofRPercRFreedelRfeebieNRPercentMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithMOVofRPercRFreedelRfeebieNRPercentMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithMOVofRPercRFreedelRfeebieNRPercentMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithMOVofRPercRFreedelRfeebieNRPercentMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNNoMOVNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNNoMOVNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNNoMOVNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNNoMOVNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // TD @Category level
    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyFalseNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }


    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }



    // Freebie Copy Over ridden is false  in Restaurant level

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }


    // Freebie Copy Over ridden is false  in Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

// Freebie Copy Over ridden is false  in Sub-Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // Freebie Copy Over ridden is false  in Item level

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // Super-Freedel Copy Over ridden is false  in Restaurant level

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDp")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // Super-Freedel Copy Over ridden is false  in Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }
    // Super-Freedel Copy Over ridden is false  in Sub-Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // Super-Freedel Copy Over ridden is false  in Item level

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and RFreedel Copy Over ridden is false  in Restaurant level


    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and RFreedel Copy Over ridden is false  in Category level


    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and RFreedel Copy Over ridden is false  in Sub-Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and RFreedel Copy Over ridden is false  in Item level

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and RFreebie Copy Over ridden is false  in Restaurant level
    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and RFreebie Copy Over ridden is false  in Category level


    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and RFreebie Copy Over ridden is false  in Sub-Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and RFreebie Copy Over ridden is false  in Item level

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and SFreedel Copy Over ridden is false  in Restaurant level
    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and SFreedel Copy Over ridden is false  in Category level


    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and SFreedel Copy Over ridden is false  in Sub-Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent and SFreedel Copy Over ridden is false  in Item level

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentSFreedelCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent RFreedel and RFeebie Copy Over ridden is false  in Restaurant level
    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreedelCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreedelCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFeebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFeebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RFreedel and RFeebie Copy Over ridden is false  in Category level


    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RFreedel and RFeebie Copy Over ridden is false  in Sub-Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RFreedel and RFeebie Copy Over ridden is false  in Item level

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithRPercentRFreedelRFreebieCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent RFreedel RFreebie and SFreedel Copy Over ridden is false  in Restaurant level

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignCopyFalseNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent RFreedel RFreebie and SFreedel Copy Over ridden is false in Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent RFreedel RFreebie and SFreedel Copy Over ridden is false in Sub-Category level

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    // RPercent RFreedel RFreebie and SFreedel Copy Over ridden is false in Item level

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDB")
    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRFlat")
    public Object[][] evaluateListingMenuForRFlat() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }
    // RFlat RFreedel RFreebie and SFreedel Copy Over ridden is true  in Restaurant level

    @DataProvider(name = "evaluateListingMenuForRFlatAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignCopyTrueNWithNoMOVWithNonSuperUserDP")
    public Object[][] evaluateListingMenuForRFlatAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignCopyTrueNWithNoMOVWithNonSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }

    @DataProvider(name = "evaluateListingMenuForRFlatAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignCopyTrueNWithNoMOVWithSuperUserDP")
    public Object[][] evaluateListingMenuForRFlatAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignCopyTrueNWithNoMOVWithSuperUser() {
        List<String> allRestid = new ArrayList<>();
        List<String> CategoryId = new ArrayList<>();
        List<List<String>> allCategoryId = new ArrayList<>();
        List<String> allSubCategoryId = new ArrayList<>();
        List<String> allMenuId = new ArrayList<>();
        List<Integer> listingRestIdList = new ArrayList<>();
        List<ItemRequests> menuItemRequests = new ArrayList<>();
        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
        allCategoryId.add(CategoryId);
        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));


        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercCopyTrueNWithMOVAndNoMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsFalseRPercCopyTrueNWithMOVAndMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtRestaurantLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    // RFlat RFreedel RFreebie and SFreedel Copy Over ridden is true in Category level
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtCateogryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPerNWithMOVAndNoMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    // RFlat RFreedel RFreebie and SFreedel Copy Over ridden is true in Sub-Category level
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtSubCateogryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtSubCategoryLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    // RFlat RFreedel RFreebie and SFreedel Copy Over ridden is true in Item level
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithNoMOVAndNoMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndNoMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUserDP")
//    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithNonSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.nonSuperUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.nonSuperUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUserDB")
//    public Object[][] evaluateListingMenuForRPercentAtItemLevelRFreedelRFreebieSFreeDelTdWithAllCampaignsCopyFalseRPercNWithMOVAndMaxDiscountWithSuperUser() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
//
//    @DataProvider(name = "evaluateListingMenuForRFlat")
//    public Object[][] evaluateListingMenuForRFlat() {
//        List<String> allRestid = new ArrayList<>();
//        List<String> CategoryId = new ArrayList<>();
//        List<List<String>> allCategoryId = new ArrayList<>();
//        List<String> allSubCategoryId = new ArrayList<>();
//        List<String> allMenuId = new ArrayList<>();
//        List<Integer> listingRestIdList = new ArrayList<>();
//        List<ItemRequests> menuItemRequests = new ArrayList<>();
//        allRestid.add(Integer.toString(Utility.getRandom(100000000, 900000000)));
//        CategoryId.add(Integer.toString(Utility.getRandom(200000, 70000000)));
//        allCategoryId.add(CategoryId);
//        allSubCategoryId.add(Integer.toString(Utility.getRandom(30000000, 99900000)));
//        allMenuId.add(Integer.toString(Utility.getRandom(10000000, 88000000)));
//        listingRestIdList.add(Integer.parseInt(allRestid.get(0)));
//
//
//        EvaluateMenu evaluateMenu = new EvaluateMenu(Integer.parseInt(CopyConstants.minCartAmount), false, Integer.parseInt(CopyConstants.superUserId), CopyConstants.userAgent[0], CopyConstants.userVersion, menuItemRequests);
//        menuItemRequests.add(0, new ItemRequests(Integer.parseInt(allSubCategoryId.get(0)), Integer.parseInt(allMenuId.get(0)), Integer.parseInt(allRestid.get(0)), 500, 2, Integer.parseInt(CategoryId.get(0))));
//        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(CopyConstants.superUserId), false, listingRestIdList);
//        return new Object[][]{{allRestid, allCategoryId, allSubCategoryId, allMenuId, listingPayload, evaluateMenu}};
//    }
}
