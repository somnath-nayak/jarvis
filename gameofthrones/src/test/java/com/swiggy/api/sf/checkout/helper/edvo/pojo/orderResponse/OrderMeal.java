package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "rewardType",
        "mealId",
        "name",
        "mealDescription",
        "mealType",
        "quantity",
        "subtotal",
        "subtotal_trade_discount",
        "packing_charge",
        "total",
        "is_veg",
        "in_stock",
        "groups"
})
public class OrderMeal {

    @JsonProperty("rewardType")
    private String rewardType;
    @JsonProperty("mealId")
    private Integer mealId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("mealDescription")
    private String mealDescription;
    @JsonProperty("mealType")
    private String mealType;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("subtotal")
    private String subtotal;
    @JsonProperty("subtotal_trade_discount")
    private String subtotalTradeDiscount;
    @JsonProperty("packing_charge")
    private String packingCharge;
    @JsonProperty("total")
    private String total;
    @JsonProperty("is_veg")
    private String isVeg;
    @JsonProperty("in_stock")
    private Integer inStock;
    @JsonProperty("groups")
    private List<Group> groups = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public OrderMeal() {
    }

    /**
     *
     * @param total
     * @param packingCharge
     * @param mealId
     * @param mealDescription
     * @param subtotal
     * @param rewardType
     * @param isVeg
     * @param name
     * @param quantity
     * @param inStock
     * @param groups
     * @param mealType
     * @param subtotalTradeDiscount
     */
    public OrderMeal(String rewardType, Integer mealId, String name, String mealDescription, String mealType, Integer quantity, String subtotal, String subtotalTradeDiscount, String packingCharge, String total, String isVeg, Integer inStock, List<Group> groups) {
        super();
        this.rewardType = rewardType;
        this.mealId = mealId;
        this.name = name;
        this.mealDescription = mealDescription;
        this.mealType = mealType;
        this.quantity = quantity;
        this.subtotal = subtotal;
        this.subtotalTradeDiscount = subtotalTradeDiscount;
        this.packingCharge = packingCharge;
        this.total = total;
        this.isVeg = isVeg;
        this.inStock = inStock;
        this.groups = groups;
    }

    @JsonProperty("rewardType")
    public String getRewardType() {
        return rewardType;
    }

    @JsonProperty("rewardType")
    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    @JsonProperty("mealId")
    public Integer getMealId() {
        return mealId;
    }

    @JsonProperty("mealId")
    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("mealDescription")
    public String getMealDescription() {
        return mealDescription;
    }

    @JsonProperty("mealDescription")
    public void setMealDescription(String mealDescription) {
        this.mealDescription = mealDescription;
    }

    @JsonProperty("mealType")
    public String getMealType() {
        return mealType;
    }

    @JsonProperty("mealType")
    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("subtotal")
    public String getSubtotal() {
        return subtotal;
    }

    @JsonProperty("subtotal")
    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    @JsonProperty("subtotal_trade_discount")
    public String getSubtotalTradeDiscount() {
        return subtotalTradeDiscount;
    }

    @JsonProperty("subtotal_trade_discount")
    public void setSubtotalTradeDiscount(String subtotalTradeDiscount) {
        this.subtotalTradeDiscount = subtotalTradeDiscount;
    }

    @JsonProperty("packing_charge")
    public String getPackingCharge() {
        return packingCharge;
    }

    @JsonProperty("packing_charge")
    public void setPackingCharge(String packingCharge) {
        this.packingCharge = packingCharge;
    }

    @JsonProperty("total")
    public String getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(String total) {
        this.total = total;
    }

    @JsonProperty("is_veg")
    public String getIsVeg() {
        return isVeg;
    }

    @JsonProperty("is_veg")
    public void setIsVeg(String isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("in_stock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("in_stock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("groups")
    public List<Group> getGroups() {
        return groups;
    }

    @JsonProperty("groups")
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("rewardType", rewardType).append("mealId", mealId).append("name", name).append("mealDescription", mealDescription).append("mealType", mealType).append("quantity", quantity).append("subtotal", subtotal).append("subtotalTradeDiscount", subtotalTradeDiscount).append("packingCharge", packingCharge).append("total", total).append("isVeg", isVeg).append("inStock", inStock).append("groups", groups).toString();
    }

}