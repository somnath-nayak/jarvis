package com.swiggy.api.sf.rng.pojo.freebie;

import org.apache.commons.lang.builder.ToStringBuilder;

public class FreebieRuleDiscount {

	private String type;
	private String discountLevel;
	private String minCartAmount;
	private String itemId;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public FreebieRuleDiscount() {
	}

	/**
	 * 
	 * @param minCartAmount
	 * @param itemId
	 * @param discountLevel
	 * @param type
	 */
	public FreebieRuleDiscount(String type, String discountLevel, String minCartAmount, String itemId) {
		super();
		this.type = type;
		this.discountLevel = discountLevel;
		this.minCartAmount = minCartAmount;
		this.itemId = itemId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDiscountLevel() {
		return discountLevel;
	}

	public void setDiscountLevel(String discountLevel) {
		this.discountLevel = discountLevel;
	}

	public String getMinCartAmount() {
		return minCartAmount;
	}

	public void setMinCartAmount(String minCartAmount) {
		this.minCartAmount = minCartAmount;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("type", type).append("discountLevel", discountLevel)
				.append("minCartAmount", minCartAmount).append("itemId", itemId).toString();
	}

}