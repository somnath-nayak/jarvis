package com.swiggy.api.sf.rng.pojo.tdcaching;

public class CacheConfig {

    public static final boolean guavaEnabled = true;
    public static final boolean campaignGuavaEnabled = true;
    public static final boolean restaurantGuavaEnabled = true;
    public static final boolean userGuavaEnabled = true;
    public static final boolean ruleGuavaEnabled = true;


    public static final boolean redisEnabled = true;
    public static final boolean campaignRedisEnabled = true;
    public static final boolean restaurantRedisEnable = true;
    public static final boolean userRedisEnabled = true;
    public static final boolean ruleRedisEnabled = true;

    public static final boolean tdEnabledRestaurant = true;
}
