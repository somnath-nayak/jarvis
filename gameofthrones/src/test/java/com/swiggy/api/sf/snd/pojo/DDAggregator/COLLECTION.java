package com.swiggy.api.sf.snd.pojo.DDAggregator;

import org.apache.commons.lang.builder.ToStringBuilder;

public class COLLECTION {

    private String latlng;

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("latlng", latlng).toString();
    }

}