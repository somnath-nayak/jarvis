package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Addon;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.AddonBuilder;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartBuilder;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartItem;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartItemBuilder;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.PreorderSlot;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Slot;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.SubscriptionItem;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.SubscriptionItemBuilder;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Variant;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.VariantBuilder;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import java.util.*;

public class CartPayloadHelper {
	    
		Initialize gameofthrones =Initializer.getInitializer();
		Cart cartPayload;
		
	    public String getCartPayloadRegular(String lat,String lng,boolean isAddonRequired) {
	    	String[] latLog=new String[]{lat,lng,"0"};
			
			String listingRes= getListingRes(latLog);
			String[] restList=JsonPath.read(listingRes, "$.data..id").toString()
					                  .replace("[","").replace("]","").replace("\"","").split(",");
			String[] unServiceabilityList=JsonPath.read(listingRes, "$.data..unserviceable").toString()
	                							  .replace("[","").replace("]","").replace("\"","").split(",");
			String[] isOpenedList=JsonPath.read(listingRes, "$.data..availability..opened").toString()
	                                      .replace("[","").replace("]","").replace("\"","").split(",");
			
			for(int i=0;i<isOpenedList.length;i++){
			   if (isOpenedList[i].equalsIgnoreCase("true") && unServiceabilityList[i].equalsIgnoreCase("false")) {
				   String restId=restList[i];
					       String statuscode=cartValidation(restId,isAddonRequired);
					          if (statuscode.equals("0")){
					              break;}
				            }
			          }
			
			return Utility.jsonEncode(cartPayload);
	    }
		
	    
		public String cartValidation(String restId,boolean isAddonRequired){
			String getStatusCode;
			try{
			cartPayload=cartPayload(restId,isAddonRequired);
			String response=createCartCheckTotals(Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
			getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
			}catch (Exception e){
				return "97";
			}
			return getStatusCode;
		}


	    
			public Cart cartPayload(String restaurantId,boolean isAddonRequired){
	        
	        Processor menuV4 = getRestaurantMenu(restaurantId);
	        CartBuilder cartBuilder = new CartBuilder()
	                .cartItems(getCartItems(menuV4, 1, isAddonRequired))
	                .cartType("REGULAR")
	                .restaurantId(restaurantId);
	        
	        return cartBuilder.build();
	    }

			
	    
   public String getCartPayloadSuper(String restaurantId, boolean isAddonRequired,String couponCode,Integer plan_id){
                Processor menuV4 = getRestaurantMenu(restaurantId);
                CartBuilder cartBuilder = new CartBuilder()
                   .cartItems(getCartItems(menuV4, 1, isAddonRequired))
                   .cartType("REGULAR")
                   .restaurantId(restaurantId)
                   .couponCode(couponCode)
                   .subscriptionItems(getSubscriptionItems(plan_id));
                
        return Utility.jsonEncode(cartBuilder.build());
 }


   public String getCartPayloadPreOrder(String restaurantId, boolean isAddonRequired,String couponCode){
          Processor menuV4 = getRestaurantMenu(restaurantId);  
          String menuRes=menuV4.ResponseValidator.GetBodyAsText();
          String preorderable=JsonPath.read(menuRes, "$.data.preorderable").toString().replace("[", "").replace("]", "");
               if (preorderable.equals("false")){
                   return null;
                   }
          Processor preOrderSlotListing = getPreOrderSlot(restaurantId);
          CartBuilder cartBuilder = new CartBuilder()
        		  .cartItems(getCartItems(menuV4, 1, isAddonRequired))
        		  .cartType("PREORDER")
        		  .preorderSlot(getPreorderSlot(preOrderSlotListing))
        		  .couponCode(couponCode)
        		  .restaurantId(restaurantId);
          return Utility.jsonEncode(cartBuilder.build());
 }

   		public String getCartPayloadCafe(String restaurantId, boolean isAddonRequired,String couponCode){
	 		Processor menuV4 = getRestaurantMenu(restaurantId);
	 		CartBuilder cartBuilder = new CartBuilder()
	 									.cartItems(getCartItems(menuV4, 1, isAddonRequired))
	 									.cartType("CAFE")
	 									.restaurantId(restaurantId)
	 									.couponCode(couponCode);
         return Utility.jsonEncode(cartBuilder.build());
   		}

   		
   		public String getCartPayloadPop(String restaurantId, boolean isAddonRequired,boolean isCartItemRequired){
   					Processor menuV4 = getRestaurantMenu(restaurantId);
   					CartBuilder cartBuilder = new CartBuilder()
   										.cartItems(getCartItems(menuV4, 1, isAddonRequired))
   										.cartType("POP")
   										.restaurantId(restaurantId);
   					return Utility.jsonEncode(cartBuilder.build());
   		}


   			public PreorderSlot getPreorderSlot(Processor preOrderSlotListing){

   				if(Utility.doesJsonPathExist(preOrderSlotListing, "$.data.[0].date")){
   					String response=preOrderSlotListing.ResponseValidator.GetBodyAsText();
   					Long date = Long.parseLong(JsonPath.read(response,"$.data.[0].date").toString().replace("[","")
   														.toString().replace("]",""));
   					PreorderSlot preorderSlot=new PreorderSlot();
   					Slot slot=new Slot();

   					int lastIndex = (JsonPath.read(response,"$.data[0]..slots..startTime").toString().replace("[","")
   											 .toString().replace("]","").split(",").length)-1;
   					Long startTime = Long.parseLong(JsonPath.read(response,"$.data[0].slots["+lastIndex+"].startTime")
   															.toString().replace("[","").toString().replace("]",""));
   					Long endTime = Long.parseLong(JsonPath.read(response,"$.data.[0].date").toString().replace("[","")
   														  .toString().replace("]",""));

   						slot.setStartTime(startTime);
   						slot.setEndTime(endTime);		   
   						preorderSlot.setDate(date);
   						preorderSlot.setSlot(slot);

   						return preorderSlot;
   				}
   					return null;  
   			}


	public int getPreOrderSlotLength(Processor preOrderSlotListing){

		if(Utility.doesJsonPathExist(preOrderSlotListing, "$.data.[0].date")){
			String response=preOrderSlotListing.ResponseValidator.GetBodyAsText();
			Long date = Long.parseLong(JsonPath.read(response,"$.data.[0].date").toString().replace("[","")
					.toString().replace("]",""));
			int length = (JsonPath.read(response,"$.data[0]..slots..startTime").toString().replace("[","")
					.toString().replace("]","").split(",").length);
			return length;
		}
		return 0;
	}


	public PreorderSlot getPreOrderSlot(Processor preOrderSlotListing,int index){

		if(Utility.doesJsonPathExist(preOrderSlotListing, "$.data.[0].date")){
			String response=preOrderSlotListing.ResponseValidator.GetBodyAsText();
			Long date = Long.parseLong(JsonPath.read(response,"$.data.[0].date").toString().replace("[","")
					.toString().replace("]",""));
			PreorderSlot preorderSlot=new PreorderSlot();
			Slot slot=new Slot();
			Long startTime = Long.parseLong(JsonPath.read(response,"$.data[0].slots["+index+"].startTime")
					.toString().replace("[","").toString().replace("]",""));
			Long endTime = Long.parseLong(JsonPath.read(response,"$.data[0].slots["+index+"].endTime")
					.toString().replace("[","").toString().replace("]",""));

			slot.setStartTime(startTime);
			slot.setEndTime(endTime);
			preorderSlot.setDate(date);
			preorderSlot.setSlot(slot);

			return preorderSlot;
		}
		return null;
	}
	   
	    public List<CartItem> getCartItems(Processor processor, Integer quantity, boolean isAddonRequired){
	        List<CartItem> cartItems = new ArrayList<>();
	        List<Integer> inStocks = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.inStock");
	        List<Integer> itemIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.id");

	        for(int i=0;i<inStocks.size();i++) {
	            CartItem cartItem = null;
	            if (inStocks.get(i).intValue() == 1) {
	                if (isAddonRequired && Utility.doesJsonPathExist(processor, "data.menu.items." + itemIds.get(i).intValue() + ".addons")) {
	                        cartItem = new CartItemBuilder()
	                                .menuItemId(String.valueOf(itemIds.get(i).intValue()))
	                                .variants(getVariantsForNormalItem(processor, i, 1, itemIds.get(i).intValue()))
	                                .quantity(quantity)
	                                .addons(getAddonsForCartMenuItem(processor, itemIds.get(i), isAddonRequired))
	                                .build();
	                        cartItems.add(cartItem);
	                        break;
	                } else {
	                    cartItem = new CartItemBuilder()
	                            .menuItemId(String.valueOf(itemIds.get(i).intValue()))
	                            .variants(getVariantsForNormalItem(processor, i, 1, itemIds.get(i).intValue()))
	                            .quantity(quantity)
	                            .addons(getAddonsForCartMenuItem(processor, itemIds.get(i), isAddonRequired))
	                            .build();
	                    cartItems.add(cartItem);
	                    break;
	                }
	            }
	        }
	        return cartItems;
	    }
	    
	    
	    public List<Variant> getVariantsForNormalItem(Processor processor, int itemPosition, int variationPosition, int itemId){
	        List<Variant> variants = new ArrayList<>();
	        if(!processor.ResponseValidator.GetNodeValue("$.data.name").contains("Domino")){
	            if(!Utility.doesJsonPathExist(processor, "data.menu.items." + itemId + ".variants_new")){
	                return variants;
	            }
	            Variant variant = null;
	            List<String> groupIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
	                    "$.data.menu.items." + itemId + ".variants_new.variant_groups.[*].group_id");

	            for(int j=0; j<groupIds.size();j++) {
	                List<String> variationIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
	                        "$.data.menu.items." + itemId + ".variants_new.variant_groups.[" + j + "].variations.[*].id");
	                variant = new VariantBuilder()
	                        .variationId(Integer.parseInt(variationIds.get(0)))
	                        .groupId(Integer.parseInt(groupIds.get(j)))
	                        .build();
	                variants.add(variant);
	            }
	        }
	        else {
	            if (Utility.doesJsonPathExist(processor, "data.menu.items." + itemId + ".variantsV2")){
	                Variant variant = null;
	                List<String> groupIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
	                        "$.data.menu.items." + itemId + ".variantsV2.pricing_models.[0].variations.[*].group_id");
	                List<String> variationIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
	                        "$.data.menu.items." + itemId + ".variantsV2.pricing_models.[0].variations.[*].variation_id");

	                for(int i=0;i<groupIds.size();i++){
	                    variant = new VariantBuilder()
	                            .variationId(Integer.parseInt(variationIds.get(i)))
	                            .groupId(Integer.parseInt(groupIds.get(i)))
	                            .build();
	                    variants.add(variant);
	                }
	            }
	        }
	        return variants;
	    }
	    
	    
	    public List<Addon> getAddonsForCartMenuItem(Processor processor, Integer itemId, boolean isAddonRequired){
	        List<Addon> addons = new ArrayList<>();
	        if(!isAddonRequired){
	            return addons;
	        }
	        if (Utility.doesJsonPathExist(processor, "$.data.menu.items." + itemId + ".addons.[*].group_id")) {
	            List<Integer> addonGroupIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
	                    "$.data.menu.items." + itemId + ".addons.[*].group_id");
	            for (int i = 0; i < addonGroupIds.size(); i++) {
	                List<Integer> choiceIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
	                        "$.data.menu.items." + itemId + ".addons.[" + i + "].choices.[*].id");
	                List<Integer> inStocks = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
	                        "$.data.menu.items." + itemId + ".addons.[" + i + "].choices.[*].inStock");
	                for (int j = 0; j < choiceIds.size(); j++) {
	                    System.out.println(i + "   " + j);
	                    if (inStocks.get(j).intValue() > 0) {
	                        Addon addon = new AddonBuilder()
	                                .groupId(String.valueOf(addonGroupIds.get(i).intValue()))
	                                .choiceId(String.valueOf(choiceIds.get(j).intValue()))
	                                .build();
	                        addons.add(addon);
	                        break;
	                    }
	                }
	            }
	        }
	        return addons;
	    }
	    
	    
	    public List<SubscriptionItem> getSubscriptionItems(Integer plan_id){
	        List<SubscriptionItem> subscriptionItems = new ArrayList<>();
	        if(plan_id==0){
	            return subscriptionItems;
	        }
	        SubscriptionItem subscriptionItem = new SubscriptionItemBuilder()
	        		                                .plan_id(plan_id)
	        		                                .quantity(1).build();
	        subscriptionItems.add(subscriptionItem);
	        return subscriptionItems;
	    }
	
	    public Processor getRestList(String[] latLngPage) {
	    	HashMap<String, String> requestHeader = new HashMap<String, String>();
	        requestHeader.put("Content-Type", "application/json");
	    	 GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listingv5", gameofthrones);
	    	 Processor processor = new Processor(service, requestHeader, null, latLngPage);
	    	 return processor;
	    }
	    
	    public Processor createCartCheckTotals(String cartPayload) {
			HashMap<String, String> requestheaders = new HashMap<>();
			requestheaders.put("content-type", "application/json");
			GameOfThronesService service = new GameOfThronesService("checkout", "checktotalv2edvo", gameofthrones);
			Processor processor = new Processor(service, requestheaders, new String[]{cartPayload});
			return processor;
		}
	  
	    public Processor getRestaurantMenu(String restID) {
	        String[] queryParam = {restID};
	        HashMap<String, String> requestHeader = new HashMap<String, String>();
	        requestHeader.put("Content-Type", "application/json");
	        GameOfThronesService gots = new GameOfThronesService("sand", "menuv4withoutlatlng", gameofthrones);
	        Processor processor = new Processor(gots, requestHeader, null, queryParam);
	        return processor;
	    }
	    
	    public Processor getPreOrderSlot(String restID) {
	        String[] queryParam = {restID};
	        HashMap<String, String> requestHeader = new HashMap<String, String>();
	        requestHeader.put("Content-Type", "application/json");
	        GameOfThronesService gots = new GameOfThronesService("sand", "preOrderSlot", gameofthrones);
	        Processor processor = new Processor(gots, requestHeader, null, queryParam);
	        return processor;
	    }
	    
	    public Processor getMenuv4(String restaurantId){
	        String[] queryParam = {restaurantId};
	        HashMap<String, String> requestHeader = new HashMap<String, String>();
	        requestHeader.put("Content-Type", "application/json");
	        GameOfThronesService gots = new GameOfThronesService("sand", "menuv4withoutlatlng", gameofthrones);
	        Processor processor = new Processor(gots, requestHeader, null, queryParam);
	        return processor;
	    }


	public Cart getObjectCartPayloadRegular(String lat,String lng,boolean isAddonRequired) {
		String[] latLog=new String[]{lat,lng,"0"};

		String listingRes= getListingRes(latLog,5);

		String[] restList=JsonPath.read(listingRes, "$.data..id").toString()
				.replace("[","").replace("]","").replace("\"","").split(",");
		String[] unServiceabilityList=JsonPath.read(listingRes, "$.data..unserviceable").toString()
				.replace("[","").replace("]","").replace("\"","").split(",");
		String[] isOpenedList=JsonPath.read(listingRes, "$.data..availability..opened").toString()
				.replace("[","").replace("]","").replace("\"","").split(",");

		for(int i=0;i<isOpenedList.length;i++){
			if (isOpenedList[i].equalsIgnoreCase("true") && unServiceabilityList[i].equalsIgnoreCase("false")) {
				String restId=restList[i];
				String statuscode=cartValidation(restId,isAddonRequired);
				if (statuscode.equals("0")){
					break;}
			}
		}

		return cartPayload;
	}

	public String getListingRes(String[] latLog) {
	    return getListingRes(latLog, 5);
	}


	private String getListingRes(String[] latLog, Integer maxRetires) {
	    for(int i=0; i<maxRetires;i++) {
	    	try {
				return getListingResInternal(latLog);
			}catch (Exception e){
	    		System.out.println("Error in getting response");
			}
		}
	    throw new RuntimeException("Error in getting Gandalf Listing API's response after " + maxRetires +" retries.");
	}

	private String getListingResInternal(String[] latLog){
		String response = getRestList(latLog).ResponseValidator.GetBodyAsText();
		JsonPath.read(response, "$.data..id").toString();
		return response;

	}

	public String getStatusCode(String cartResponse){
		String getStatusCode= JsonPath.read(cartResponse, "$.statusCode").toString().replace("[", "")
				.replace("]", "");
		return getStatusCode;
	}

	}