package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.GSTDetails;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "rewardType",
        "added_by_user_id",
        "added_by_username",
        "group_user_item_map",
        "item_id",
        "external_item_id",
        "name",
        "is_veg",
        "variants",
        "addons",
        "image_id",
        "quantity",
        "total",
        "subtotal",
        "packing_charges",
        "category_details",
        "global_main_category",
        "global_sub_category",
        "item_charges",
        "item_tax_expressions",
        "item_swiggy_discount_hit",
        "item_restaurant_discount_hit",
        "GST_details",
        "item_type",
        "meal_id",
        "meal_name",
        "meal_quantity",
        "single_variant"
})
public class Item {

    @JsonProperty("rewardType")
    private String rewardType;
    @JsonProperty("added_by_user_id")
    private Integer addedByUserId;
    @JsonProperty("added_by_username")
    private String addedByUsername;
    @JsonProperty("group_user_item_map")
    private GroupUserItemMap groupUserItemMap;
    @JsonProperty("item_id")
    private String itemId;
    @JsonProperty("external_item_id")
    private String externalItemId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("is_veg")
    private String isVeg;
    @JsonProperty("variants")
    private List<Object> variants = null;
    @JsonProperty("addons")
    private List<Object> addons = null;
    @JsonProperty("image_id")
    private String imageId;
    @JsonProperty("quantity")
    private String quantity;
    @JsonProperty("total")
    private String total;
    @JsonProperty("subtotal")
    private String subtotal;
    @JsonProperty("packing_charges")
    private String packingCharges;
    @JsonProperty("category_details")
    private CategoryDetails categoryDetails;
    @JsonProperty("global_main_category")
    private String globalMainCategory;
    @JsonProperty("global_sub_category")
    private String globalSubCategory;
    @JsonProperty("item_charges")
    private ItemCharges itemCharges;
    @JsonProperty("item_tax_expressions")
    private ItemTaxExpressions itemTaxExpressions;
    @JsonProperty("item_swiggy_discount_hit")
    private Integer itemSwiggyDiscountHit;
    @JsonProperty("item_restaurant_discount_hit")
    private Double itemRestaurantDiscountHit;
    @JsonProperty("GST_details")
    private GSTDetails gSTDetails;
    @JsonProperty("item_type")
    private String itemType;
    @JsonProperty("meal_id")
    private String mealId;
    @JsonProperty("meal_name")
    private String mealName;
    @JsonProperty("meal_quantity")
    private String mealQuantity;
    @JsonProperty("single_variant")
    private Boolean singleVariant;

    @JsonProperty("rewardType")
    public String getRewardType() {
        return rewardType;
    }

    @JsonProperty("rewardType")
    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    @JsonProperty("added_by_user_id")
    public Integer getAddedByUserId() {
        return addedByUserId;
    }

    @JsonProperty("added_by_user_id")
    public void setAddedByUserId(Integer addedByUserId) {
        this.addedByUserId = addedByUserId;
    }

    @JsonProperty("added_by_username")
    public String getAddedByUsername() {
        return addedByUsername;
    }

    @JsonProperty("added_by_username")
    public void setAddedByUsername(String addedByUsername) {
        this.addedByUsername = addedByUsername;
    }

    @JsonProperty("group_user_item_map")
    public GroupUserItemMap getGroupUserItemMap() {
        return groupUserItemMap;
    }

    @JsonProperty("group_user_item_map")
    public void setGroupUserItemMap(GroupUserItemMap groupUserItemMap) {
        this.groupUserItemMap = groupUserItemMap;
    }

    @JsonProperty("item_id")
    public String getItemId() {
        return itemId;
    }

    @JsonProperty("item_id")
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    @JsonProperty("external_item_id")
    public String getExternalItemId() {
        return externalItemId;
    }

    @JsonProperty("external_item_id")
    public void setExternalItemId(String externalItemId) {
        this.externalItemId = externalItemId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("is_veg")
    public String getIsVeg() {
        return isVeg;
    }

    @JsonProperty("is_veg")
    public void setIsVeg(String isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("variants")
    public List<Object> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Object> variants) {
        this.variants = variants;
    }

    @JsonProperty("addons")
    public List<Object> getAddons() {
        return addons;
    }

    @JsonProperty("addons")
    public void setAddons(List<Object> addons) {
        this.addons = addons;
    }

    @JsonProperty("image_id")
    public String getImageId() {
        return imageId;
    }

    @JsonProperty("image_id")
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    @JsonProperty("quantity")
    public String getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("total")
    public String getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(String total) {
        this.total = total;
    }

    @JsonProperty("subtotal")
    public String getSubtotal() {
        return subtotal;
    }

    @JsonProperty("subtotal")
    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    @JsonProperty("packing_charges")
    public String getPackingCharges() {
        return packingCharges;
    }

    @JsonProperty("packing_charges")
    public void setPackingCharges(String packingCharges) {
        this.packingCharges = packingCharges;
    }

    @JsonProperty("category_details")
    public CategoryDetails getCategoryDetails() {
        return categoryDetails;
    }

    @JsonProperty("category_details")
    public void setCategoryDetails(CategoryDetails categoryDetails) {
        this.categoryDetails = categoryDetails;
    }

    @JsonProperty("global_main_category")
    public String getGlobalMainCategory() {
        return globalMainCategory;
    }

    @JsonProperty("global_main_category")
    public void setGlobalMainCategory(String globalMainCategory) {
        this.globalMainCategory = globalMainCategory;
    }

    @JsonProperty("global_sub_category")
    public String getGlobalSubCategory() {
        return globalSubCategory;
    }

    @JsonProperty("global_sub_category")
    public void setGlobalSubCategory(String globalSubCategory) {
        this.globalSubCategory = globalSubCategory;
    }

    @JsonProperty("item_charges")
    public ItemCharges getItemCharges() {
        return itemCharges;
    }

    @JsonProperty("item_charges")
    public void setItemCharges(ItemCharges itemCharges) {
        this.itemCharges = itemCharges;
    }

    @JsonProperty("item_tax_expressions")
    public ItemTaxExpressions getItemTaxExpressions() {
        return itemTaxExpressions;
    }

    @JsonProperty("item_tax_expressions")
    public void setItemTaxExpressions(ItemTaxExpressions itemTaxExpressions) {
        this.itemTaxExpressions = itemTaxExpressions;
    }

    @JsonProperty("item_swiggy_discount_hit")
    public Integer getItemSwiggyDiscountHit() {
        return itemSwiggyDiscountHit;
    }

    @JsonProperty("item_swiggy_discount_hit")
    public void setItemSwiggyDiscountHit(Integer itemSwiggyDiscountHit) {
        this.itemSwiggyDiscountHit = itemSwiggyDiscountHit;
    }

    @JsonProperty("item_restaurant_discount_hit")
    public Double getItemRestaurantDiscountHit() {
        return this.itemRestaurantDiscountHit;
    }

    @JsonProperty("item_restaurant_discount_hit")
    public void setItemRestaurantDiscountHit(Double itemRestaurantDiscountHit) {
        this.itemRestaurantDiscountHit = itemRestaurantDiscountHit;
    }

    @JsonProperty("GST_details")
    public GSTDetails getGSTDetails() {
        return gSTDetails;
    }

    @JsonProperty("GST_details")
    public void setGSTDetails(GSTDetails gSTDetails) {
        this.gSTDetails = gSTDetails;
    }

    @JsonProperty("item_type")
    public String getItemType() {
        return itemType;
    }

    @JsonProperty("item_type")
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    @JsonProperty("meal_id")
    public String getMealId() {
        return mealId;
    }

    @JsonProperty("meal_id")
    public void setMealId(String mealId) {
        this.mealId = mealId;
    }

    @JsonProperty("meal_name")
    public String getMealName() {
        return mealName;
    }

    @JsonProperty("meal_name")
    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    @JsonProperty("meal_quantity")
    public String getMealQuantity() {
        return mealQuantity;
    }

    @JsonProperty("meal_quantity")
    public void setMealQuantity(String mealQuantity) {
        this.mealQuantity = mealQuantity;
    }

    @JsonProperty("single_variant")
    public Boolean getSingleVariant() {
        return singleVariant;
    }

    @JsonProperty("single_variant")
    public void setSingleVariant(Boolean singleVariant) {
        this.singleVariant = singleVariant;
    }

}