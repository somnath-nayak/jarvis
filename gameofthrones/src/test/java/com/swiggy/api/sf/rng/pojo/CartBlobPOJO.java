package com.swiggy.api.sf.rng.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "swiggyTradeDiscount",
        "restaurantTradeDiscount",
        "cartItems",
        "restaurantId",
        "shared",
        "cart_type"
})
public class CartBlobPOJO {

    @JsonProperty("swiggyTradeDiscount")
    private Integer swiggyTradeDiscount;
    @JsonProperty("restaurantTradeDiscount")
    private Integer restaurantTradeDiscount;
    @JsonProperty("cartItems")
    private List<CartItemPOJO> cartItems = null;
    @JsonProperty("restaurantId")
    private String restaurantId;
    @JsonProperty("shared")
    private Boolean shared;
    @JsonProperty("cart_type")
    private String cartType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public CartBlobPOJO() {
    }

    /**
     *
     * @param shared
     * @param cartItems
     * @param restaurantTradeDiscount
     * @param swiggyTradeDiscount
     * @param cartType
     * @param restaurantId
     */
    public CartBlobPOJO(Integer swiggyTradeDiscount, Integer restaurantTradeDiscount, List<CartItemPOJO> cartItems, String restaurantId, Boolean shared, String cartType) {
        super();
        this.swiggyTradeDiscount = swiggyTradeDiscount;
        this.restaurantTradeDiscount = restaurantTradeDiscount;
        this.cartItems = cartItems;
        this.restaurantId = restaurantId;
        this.shared = shared;
        this.cartType = cartType;
    }

    @JsonProperty("swiggyTradeDiscount")
    public Integer getSwiggyTradeDiscount() {
        return swiggyTradeDiscount;
    }

    @JsonProperty("swiggyTradeDiscount")
    public void setSwiggyTradeDiscount(Integer swiggyTradeDiscount) {
        this.swiggyTradeDiscount = swiggyTradeDiscount;
    }

    public CartBlobPOJO withSwiggyTradeDiscount(Integer swiggyTradeDiscount) {
        this.swiggyTradeDiscount = swiggyTradeDiscount;
        return this;
    }

    @JsonProperty("restaurantTradeDiscount")
    public Integer getRestaurantTradeDiscount() {
        return restaurantTradeDiscount;
    }

    @JsonProperty("restaurantTradeDiscount")
    public void setRestaurantTradeDiscount(Integer restaurantTradeDiscount) {
        this.restaurantTradeDiscount = restaurantTradeDiscount;
    }

    public CartBlobPOJO withRestaurantTradeDiscount(Integer restaurantTradeDiscount) {
        this.restaurantTradeDiscount = restaurantTradeDiscount;
        return this;
    }

    @JsonProperty("cartItems")
    public List<CartItemPOJO> getCartItems() {
        return cartItems;
    }

    @JsonProperty("cartItems")
    public void setCartItems(List<CartItemPOJO> cartItems) {
        this.cartItems = cartItems;
    }

    public CartBlobPOJO withCartItems(List<CartItemPOJO> cartItems) {
        this.cartItems = cartItems;
        return this;
    }

    @JsonProperty("restaurantId")
    public String getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurantId")
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public CartBlobPOJO withRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

    @JsonProperty("shared")
    public Boolean getShared() {
        return shared;
    }

    @JsonProperty("shared")
    public void setShared(Boolean shared) {
        this.shared = shared;
    }

    public CartBlobPOJO withShared(Boolean shared) {
        this.shared = shared;
        return this;
    }

    @JsonProperty("cart_type")
    public String getCartType() {
        return cartType;
    }

    @JsonProperty("cart_type")
    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public CartBlobPOJO withCartType(String cartType) {
        this.cartType = cartType;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CartBlobPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public CartBlobPOJO setDefault() {

        CartItemPOJO cartItemPOJO = new CartItemPOJO().setDefault();
        List<CartItemPOJO> cartItems = new ArrayList<>();
        cartItems.add(cartItemPOJO);
        return this.withSwiggyTradeDiscount(0)
                .withRestaurantTradeDiscount(0)
                .withCartItems(cartItems)
                .withRestaurantId("26211")
                .withShared(false)
                .withCartType("REGULAR");
    }

}
