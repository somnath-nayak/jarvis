package com.swiggy.api.sf.snd.tests;

import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.POP_Area_Schedule_Helper;
import com.swiggy.api.erp.cms.helper.Popv2Helper;
import com.swiggy.api.erp.cms.pojo.BaseServiceItems.Item;
import com.swiggy.api.sf.snd.constants.POPConstants;
import com.swiggy.api.sf.snd.dp.POPRegressionDP;
import com.swiggy.api.sf.snd.helper.POPRegressionHelper;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.tests
 **/
public class POPRegressionTest extends POPRegressionDP {

    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    POPRegressionHelper helper = new POPRegressionHelper();
    DateHelper dateHelper = new DateHelper();
    POP_Area_Schedule_Helper pop_area_schedule_helper = new POP_Area_Schedule_Helper();
    private static Logger log = LoggerFactory.getLogger(POPRegressionTest.class);
    String day = helper.getCurrentDay();
    Popv2Helper popv2Helper = new Popv2Helper();
    CMSHelper cmsHelper = new CMSHelper();
    public static String restID, itemID, item_name, item_schedule_mapping_id, inventory;
    public static int area_schedule_id;


    @BeforeClass
    public void dataCreation() throws IOException, JSONException {
        //restID = String.valueOf(baseServiceHelper.createRestaurantAndReturnID());
        restID = POPConstants.restId;
        boolean map_rest_zone;
        itemID = helper.createPOPItemOfPOPTypeForRestId(restID,"POP_ONLY");
        //itemID="20366190";
        Processor processor = baseServiceHelper.getItemByIdHelper(itemID);
        item_name = processor.ResponseValidator.GetNodeValue("$.data.name");
        log.info("--RestID ::: "+POPConstants.restId);
        log.info("--ItemID ::: "+itemID);
        log.info("--ItemName ::: "+item_name);
        log.info("--Day ::: "+day);
        boolean enable_rest =  helper.enableRestaurantPostCreateInDB(restID);
        //String[] polygon_zone = helper.getPolygonAndZoneIdFromSolr();
        if(!helper.checkPolygonRestaurantMapping(POPConstants.polygonId, restID)) {
            map_rest_zone = helper.mapPolygonWithRestaurant(restID, POPConstants.polygonId);
            Assert.assertTrue(map_rest_zone, "map_rest_zone not true");
        }
        Assert.assertTrue(enable_rest,"enable_rest is not true");
        List<Map<String, Object>> list = helper.getExistingAreaScheduleIdOpenCloseTimeDB(day,POPConstants.area_code);
        if (list.size() > 0) {
            String[] ids = new String[list.size()];
            for (int i = 0; i < list.size() ; i++) {
                ids[i] = list.get(i).get("id").toString();
            }
            helper.deleteConflictingAreaScheduleByIdDB(ids,POPConstants.area_code,day);
        }
        Processor p = pop_area_schedule_helper.createArea_schedule_helper(POPConstants.slot_type, Integer.parseInt(POPConstants.area_code),POPConstants.openTime,POPConstants.closeTime,day,POPConstants.menu_type);
        area_schedule_id = p.ResponseValidator.GetNodeValueAsInt("$.data.id");
        int status_create_area_schedule = p.ResponseValidator.GetNodeValueAsInt("$.status");
        String msg_create_area_schedule = p.ResponseValidator.GetNodeValue("$.code");
        System.out.println("--AreaScheduleId ::: "+area_schedule_id);
        Assert.assertEquals(status_create_area_schedule,POPConstants.status1, "status_create_area_schedule not 1");
        Assert.assertEquals(msg_create_area_schedule,POPConstants.msg_success, "msg_create_area_schedule  not SUCCESS");
        Processor p1 = popv2Helper.ItemSchedule(Integer.parseInt(itemID));
        item_schedule_mapping_id = String.valueOf(p1.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        inventory = String.valueOf(p1.ResponseValidator.GetNodeValueAsInt("$.data.inventory"));
        System.out.println("--ItemScheduleMappingId ::: "+item_schedule_mapping_id);
        Processor p2 = helper.createRestaurantSlotHelper(restID,day,String.valueOf(POPConstants.closeTime),String.valueOf(POPConstants.openTime));
    }

    @Test(groups = {"POP", "Integration", "SAND"},description = "Verify POP listing response when serviceable", enabled = true)
    public void verifyPOPListingResponse() throws JSONException, InterruptedException, IOException {
        SoftAssert softAssert = new SoftAssert();
        Thread.sleep(3000);
        Processor p2 = helper.popAggregatorHelper(POPConstants.lat,POPConstants.lng);
        String[] get_item_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items.[*].menuItem.itemId").replace("[","").replace("]","").split(",");
        String get_status_msg = p2.ResponseValidator.GetNodeValue("$.statusMessage");
        String[] get_rest_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities.[*].restaurantId").replace("[","").replace("]","").replace("\"","").split(",");
        String get_item_enabled_status = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.enabled");
        String get_item_instock_status = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.inStock");
        String get_item_order = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.itemOrder");
        String get_pop_usage_type = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.popUsageType");
        String get_restId_in_Item = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].restId");
        String get_rest_serviceable = p2.ResponseValidator.GetNodeValue("$.data.restaurantServiceabilities."+restID+".serviceability").replace("[","").replace("]","");
        int get_status = p2.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(get_status,POPConstants.status0, "status not 0");
        softAssert.assertEquals(get_status_msg,POPConstants.msg_doneSuccess, "msg not similar msg_doneSuccess");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id,itemID),true, "Item Id not found in aggregator response");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_rest_id,restID),true, "Rest id is not there in restaurantServiceabilities");
        softAssert.assertEquals(get_rest_serviceable,POPConstants.serviceable_str, "restaurant not SERVICEABLE");
        softAssert.assertEquals(get_item_enabled_status,"1", "item enabled is not 1");
        softAssert.assertEquals(get_item_instock_status,"true", "in stock is not true");
        softAssert.assertEquals(get_item_order,"0", "order is not 0");
        softAssert.assertEquals(get_pop_usage_type,"POP_ONLY", "get_pop_usage_type is not POP_ONLY");
        softAssert.assertEquals(get_restId_in_Item,restID, "rest id is not same within item object");

        softAssert.assertAll();
    }

    @Test(groups = {"POP", "Integration", "SAND"},description = "Verify POP listing response when unserviceable and serviceable", enabled = true)
    public void verifyPOPListingResponseWhenRestUnserviceableWithBanner() throws JSONException, InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        helper.enablePopServiceabilityBanner(restID,false);
        Thread.sleep(8000);
        Processor p2 = helper.popAggregatorHelper(POPConstants.lat, POPConstants.lng);
        String[] get_item_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items.[*].menuItem.itemId").replace("[","").replace("]","").split(",");
        String get_status_msg = p2.ResponseValidator.GetNodeValue("$.statusMessage");
        String[] get_rest_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities.[*].restaurantId").replace("[","").replace("]","").split(",");
        int get_status = p2.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String get_rest_serviceable_with_banner = p2.ResponseValidator.GetNodeValue("$.data.restaurantServiceabilities."+restID+".serviceability").replace("[","").replace("]","");
        //debug
        for (String s: get_item_id)
        System.out.println("========get_item_id " +s);
        for (String s1: get_rest_id)
        System.out.println("==============get_rest_serviciability "+s1);
        softAssert.assertEquals(get_status,POPConstants.status0, "status not 0");
        softAssert.assertEquals(get_status_msg,POPConstants.msg_doneSuccess, "msg not similar msg_doneSuccess");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id,itemID),true, "Item Id not found in aggregator response");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_rest_id,restID),true, "Rest id is not there in restaurantServiceabilities");
        softAssert.assertEquals(get_rest_serviceable_with_banner,POPConstants.serviceable_with_banner_str, "restaurant not SERVICEABLE_WITH_BANNER");
        //serviceable
        helper.enablePopServiceabilityBanner(restID,true);
        Thread.sleep(8000);
        Processor p1 = helper.popAggregatorHelper(POPConstants.lat, POPConstants.lng);
        String get_rest_serviceable = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities."+restID+".serviceability").replace("[","").replace("]","");
        softAssert.assertEquals(get_rest_serviceable, POPConstants.serviceable_str, "restaurant not found in json objects that are SERVICEABLE");

        softAssert.assertAll();
    }

    @Test(groups = {"POP", "Integration", "SAND"},description = "Verify POP listing response for enabled disabled restaurant", enabled = true)
    public void verifyPOPListingForEnabledDisabledRestaurant() {
        SoftAssert softAssert = new SoftAssert();
        //disable restaurant
       // cmsHelper.setRestaurantEnabled(restID,0);
        // TODO: Disable restaurant and then hit aggregator, then enable restaurant and hit again and validate.
        // TODO: Disable restaurant from delivery is still impending automation. Will check with preetesh once back

        softAssert.assertAll();
    }


    @Test(groups = {"POP", "Integration", "SAND"},description = "Verify POP listing response for area slot", enabled = true)
    public void validatePOPListingForAreaSlot() {
        SoftAssert softAssert = new SoftAssert();
        String slotId, open_time, close_time;
        HashMap<String, String> hm = helper.getExistingAreaSlotIdFromDB(day,POPConstants.area_code);
        if(hm.size() > 0) {
            slotId = hm.get("id");
            open_time = hm.get("open_time");
            close_time = hm.get("close_time");
            System.out.println("===> existing area slot: "+slotId);
            Processor p = helper.deleteAreaSlotsHelper(slotId);
            int status1 = p.ResponseValidator.GetNodeValueAsInt("$.status");
            String statusMsg1 = p.ResponseValidator.GetNodeValue("$.code");
            softAssert.assertEquals(status1,POPConstants.status1, "delete area slot status not 1");
            softAssert.assertEquals(statusMsg1,POPConstants.msg_success, "delete area slot status msg not success");
        } else {
            open_time = "0";
            close_time = "2350";
        }
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Processor p1 = helper.popAggregatorHelper(POPConstants.lat, POPConstants.lng);
        String[] get_item_id = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items.[*].menuItem.itemId").replace("[","").replace("]","").split(",");
        String get_status_msg = p1.ResponseValidator.GetNodeValue("$.statusMessage");
        String[] get_rest_id = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities.[*].restaurantId").replace("[","").replace("]","").replace("\"","").split(",");
        int get_status = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String[] get_rest_serviceable_with_banner = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities.[?(@.serviceability == 'SERVICEABLE_WITH_BANNER')].restaurantId").replace("[","").replace("]","").split(",");
        softAssert.assertEquals(get_status,POPConstants.status0, "status not 0");
        softAssert.assertEquals(get_status_msg,POPConstants.msg_doneSuccess, "msg not similar msg_doneSuccess");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id,itemID),false, "Item Id not found in aggregator response");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_rest_id,restID),false, "Rest id is not there in restaurantServiceabilities");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_rest_serviceable_with_banner,restID),false, "restaurant not SERVICEABLE_WITH_BANNER");
        for (String s: get_item_id)
            System.out.println("========get_item_id " +s);
        for (String s1: get_rest_id)
            System.out.println("==============rest_id "+s1);
        Processor p2 =  helper.createAreaSlotsHelper(POPConstants.area_code,day,close_time,open_time);
        int status1 = p2.ResponseValidator.GetNodeValueAsInt("$.status");
        String statusMsg1 = p2.ResponseValidator.GetNodeValue("$.code");
        softAssert.assertEquals(status1,POPConstants.status1, "create area slot status not 1");
        softAssert.assertEquals(statusMsg1,POPConstants.msg_success, "create area slot status msg not success");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HashMap<String, String> hm1 = helper.getExistingAreaSlotIdFromDB(day,POPConstants.area_code);
        if(hm1.size()>0)
        System.out.println("====>slot_id: " +hm1.get("id"));
       //softAssert.assertEquals();
        softAssert.assertAll();
    }

    @Test(groups = {"POP", "Integration", "SAND"},description = "Verify POP items not visible in search aggregator", enabled = true)
    public void validateSearchV2ForPOPItems() {
        SoftAssert softAssert = new SoftAssert();
        Processor p = helper.sndSearchV2_DishHelper(POPConstants.lat,POPConstants.lng,POPConstants.search_str_item);
        String[] get_item_id = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items").replace("[","").replace("]","").split(",");
        String get_status_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
        String[] get_rest_id = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[*].restaurants[*].id").replace("[","").replace("]","").replace("\"","").split(",");
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(get_status,POPConstants.status0, "status not 0");
        softAssert.assertEquals(get_status_msg,POPConstants.msg_doneSuccess, "msg not similar msg_doneSuccess");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id,itemID),false, "pop item found in the searchv2 dish response");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_rest_id,restID),false, "pop restaurant found in the search v2 response");
        softAssert.assertAll();
    }

//    @Test(groups = {"POP", "Regression", "SAND"},description = "Verify POP items quantity post item inventory update", enabled = true)
//    public void validatePOPItemInventoryAfterInventoryUpdate() {
//        SoftAssert softAssert = new SoftAssert();
//
//    }

    @Test(groups = {"POP", "Mocked", "SAND"},description = "Verify POP items not visible in POP aggregator", enabled = true)
    public void validatePOPAgreggatorForCreatedItem() {
        SoftAssert softAssert = new SoftAssert();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Processor p2 = helper.popAggregatorHelper(POPConstants.lat,POPConstants.lng);
        String[] get_item_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items.[*].menuItem.itemId").replace("[","").replace("]","").split(",");
        String get_status_msg = p2.ResponseValidator.GetNodeValue("$.statusMessage");
        String[] get_rest_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities.[*].restaurantId").replace("[","").replace("]","").replace("\"","").split(",");
        String get_item_enabled_status = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.enabled");
        String get_item_instock_status = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.inStock");
        String get_item_order = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.itemOrder");
        String get_pop_usage_type = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.popUsageType");
        String get_restId_in_Item = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].restId");
        String get_rest_serviceable = p2.ResponseValidator.GetNodeValue("$.data.restaurantServiceabilities."+restID+".serviceability").replace("[","").replace("]","");
        int get_status = p2.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(get_status,POPConstants.status0, "status not 0");
        softAssert.assertEquals(get_status_msg,POPConstants.msg_doneSuccess, "msg not similar msg_doneSuccess");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id,itemID),true, "Item Id not found in aggregator response");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_rest_id,restID),true, "Rest id is not there in restaurantServiceabilities");
        softAssert.assertEquals(get_rest_serviceable,POPConstants.serviceable_str, "restaurant not SERVICEABLE");
        softAssert.assertEquals(get_item_enabled_status,"1", "item enabled is not 1");
        softAssert.assertEquals(get_item_instock_status,"true", "in stock is not true");
        softAssert.assertEquals(get_item_order,"0", "order is not 0");
        softAssert.assertEquals(get_pop_usage_type,"POP_ONLY", "get_pop_usage_type is not POP_ONLY");
        softAssert.assertEquals(get_restId_in_Item,restID, "rest id is not same within item object");
        softAssert.assertAll();
    }

    @Test(groups = {"POP", "Mocked", "SAND"},description = "Verify POP items when inventory is updated inventory", enabled = true)
    public void validatePOPAggregatorForUpdatedInventory() throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        String current_item_inv;
        Processor p1 = helper.popAggregatorHelper(POPConstants.lat,POPConstants.lng);
        String[] get_item_id_before_update = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items.[*].menuItem.itemId").replace("[","").replace("]","").split(",");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id_before_update,itemID),true, "Item Id not found in aggregator response before updating inventory");
        List<Map<String, Object>> list = helper.getItemScheduleMappingDetailsDB(itemID);
        if (list.size()>0) {
            current_item_inv = list.get(0).get("inventory").toString();
            item_schedule_mapping_id = list.get(0).get("id").toString();
            System.out.println("--current_item_inv ::: "+current_item_inv);
        }
        else {
            current_item_inv = "0";
            softAssert.assertTrue(false, "current_item_inv could not be found from item_schedule_mapping table");
        }
        System.out.println("--item_schedule_mapping_id ::: "+item_schedule_mapping_id);
        String new_env = String.valueOf((Integer.parseInt(current_item_inv) - 5));
        helper.update_checkout_inventory_redis(new_env,item_schedule_mapping_id);
        Processor p2 = helper.popAggregatorHelper(POPConstants.lat,POPConstants.lng);
        Thread.sleep(3000);
        String[] get_item_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items.[*].menuItem.itemId").replace("[","").replace("]","").split(",");
        String get_status_msg = p2.ResponseValidator.GetNodeValue("$.statusMessage");
        String[] get_rest_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities.[*].restaurantId").replace("[","").replace("]","").replace("\"","").split(",");
        String get_item_enabled_status = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.enabled");
        String get_item_instock_status = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.inStock");
        String get_rest_serviceable = p2.ResponseValidator.GetNodeValue("$.data.restaurantServiceabilities."+restID+".serviceability").replace("[","").replace("]","");
        //DEBUG
        for(String s : get_rest_id)
            System.out.println("------restIds ::: "+s);
        int get_status = p2.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String get_stock_count = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].stockCount").replace("[","").replace("]","");
        softAssert.assertEquals(get_stock_count,new_env,"stock count is not the updated value");
        softAssert.assertEquals(get_status,POPConstants.status0, "status not 0");
        softAssert.assertEquals(get_status_msg,POPConstants.msg_doneSuccess, "msg not similar msg_doneSuccess");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id,itemID),true, "Item Id not found in aggregator response");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_rest_id,restID),true, "Rest id is not there in restaurantServiceabilities");
        softAssert.assertEquals(get_rest_serviceable,POPConstants.serviceable_str, "restaurant not SERVICEABLE");
        softAssert.assertEquals(get_item_enabled_status,"1", "item enabled is not 1");
        softAssert.assertEquals(get_item_instock_status,"true", "in stock is not true");
        softAssert.assertAll();
    }

    @Test(groups = {"POP", "Mocked", "SAND"},description = "Verify POP items when inventory updated 0", enabled = true)
    public void validatePOPAggregatorForInventoryZero() throws InterruptedException {
        //String itemID = "20366188";
        SoftAssert softAssert = new SoftAssert();
        String inv_zero = "0";
        Thread.sleep(3000);
        List<Map<String, Object>> list = helper.getItemScheduleMappingDetailsDB(itemID);
        if (list.size()>0) {
            item_schedule_mapping_id = list.get(0).get("id").toString();
            System.out.println("--item_schedule_mapping_id ::: "+item_schedule_mapping_id);
        }
        else {
            softAssert.assertTrue(false, "item_schedule_mapping_id could not be found from item_schedule_mapping table");
        }
        helper.update_checkout_inventory_redis(inv_zero,item_schedule_mapping_id);
        Thread.sleep(20000);
        Processor p2 = helper.popAggregatorHelper(POPConstants.lat,POPConstants.lng);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String[] get_item_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items.[*].menuItem.itemId").replace("[","").replace("]","").split(",");
        String get_status_msg = p2.ResponseValidator.GetNodeValue("$.statusMessage");
        String[] get_rest_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities.[*].restaurantId").replace("[","").replace("]","").replace("\"","").split(",");
        String get_item_enabled_status = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.enabled");
        String get_item_instock_status = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].menuItem.inStock");
        String get_stock_count = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items['"+itemID+"'].stockCount").replace("[","").replace("]","");
        int get_status = p2.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(get_status,POPConstants.status0, "status not 0");
        softAssert.assertEquals(get_status_msg,POPConstants.msg_doneSuccess, "msg not similar msg_doneSuccess");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id,itemID),true, "Item Id not found in aggregator response");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_rest_id,restID),true, "Rest id is not there in restaurantServiceabilities");
        softAssert.assertEquals(get_item_enabled_status,"1", "item enabled is not 1");
        softAssert.assertEquals(get_item_instock_status,"true", "in stock is not true");
        softAssert.assertEquals(get_stock_count,"0","stock count is not 0");
        helper.update_checkout_inventory_redis("200",item_schedule_mapping_id);
        Thread.sleep(3000);
        softAssert.assertAll();
    }

    @Test(groups = {"POP", "Mocked", "SAND"},description = "Verify POP items after updating area schedule and making it unavailable", enabled = false)
    public void validatePOPAggregatorForNoAreaSchedule() throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();

        //TODO: there is a caching of 5 min. in prod. vipul will make it env variable in later push when we can set it up in uat. Once done will enable

        List<Map<String, Object>> list = helper.getExistingAreaScheduleIdOpenCloseTimeDB(day,POPConstants.area_code);
        Processor p1 = helper.popAggregatorHelper(POPConstants.lat,POPConstants.lng);
        String[] get_item_id_before_update = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items.[*].menuItem.itemId").replace("[","").replace("]","").split(",");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id_before_update,itemID),true, "Item Id not found in aggregator response");
        String area_schedule_id = list.get(0).get("id").toString();
        String getCurrentTime = dateHelper.getCurrentTimeInHundredHours();
        String openTime = dateHelper.getMinutesPlusMinusCurrentTimeInHundredHours(-20);
        String closeTime = dateHelper.getMinutesPlusMinusCurrentTimeInHundredHours(-10);
        System.out.println("validatePOPAggregatorForNoAreaSchedule - OpenTime ::: "+openTime+"  CloseTime ::: "+closeTime);
        helper.avail_area_schedule_redis(area_schedule_id,day,openTime,closeTime,POPConstants.area_code);
        helper.avail_item_schedule_redis_set_empty("[]",itemID);
        Thread.sleep(20000);
        Processor p2 = helper.popAggregatorHelper(POPConstants.lat,POPConstants.lng);
        String[] get_item_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.items.[*].menuItem.itemId").replace("[","").replace("]","").split(",");
        String get_status_msg = p2.ResponseValidator.GetNodeValue("$.statusMessage");
        String[] get_rest_id = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities.[*].restaurantId").replace("[","").replace("]","").replace("\"","").split(",");
        String get_rest_serviceable = p2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantServiceabilities["+restID+"].serviceability").replace("[","").replace("]","");
        int get_status = p2.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(get_status,POPConstants.status0, "status not 0");
        softAssert.assertEquals(get_status_msg,POPConstants.msg_doneSuccess, "msg not similar msg_doneSuccess");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_item_id,itemID),true, "Item Id not found in aggregator response");
        softAssert.assertEquals(helper.searchIdFromResponseArray(get_rest_id,restID),true, "Rest id is not there in restaurantServiceabilities");
        softAssert.assertEquals(get_rest_serviceable,POPConstants.serviceable_str, "restaurant not SERVICEABLE");
    }







}
