package com.swiggy.api.sf.checkout.tests;

import com.swiggy.api.erp.delivery.helper.OrderStatus;
import com.swiggy.api.sf.checkout.dp.TrackOrderDP;
import com.swiggy.api.sf.checkout.helper.TrackOrderHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.trackOrderResponse.TrackOrderResponse;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TrackOrderTest extends TrackOrderDP {
    private TrackOrderHelper trackOrderHelper = new TrackOrderHelper();
    private HashMap<String, String> headers;
    private TrackOrderResponse trackOrderResponse = new TrackOrderResponse();
    private String orderId;
    private String orderKey;
    private String deID;
    private List<TrackOrderResponse> trackOrderResponses = new ArrayList<>();

    @BeforeClass
    public void login(){
        headers = trackOrderHelper.loginV2(mobile, password);
        deID = trackOrderHelper.createDE(zoneId);
        trackOrderHelper.updateDELocationInRedis(deID);
        trackOrderHelper.makeDEFree(deID);
    }

    @Test(dataProvider = "trackOrderTestData", description = "Test Track Order API")
    public void trackOrderTest(String restaurantId,
                               String userAgent,
                               String versionCode,
                               String paymentMethod,
                               String orderComments,
                               String orderState,
                               int statusCode,
                               String statusMessage){
        if(orderId == null || !(headers.get("User-Agent").equals(userAgent))) {
            trackOrderHelper.makeDEFree(deID);
            headers.put("User-Agent", userAgent);
            headers.put("version-code", versionCode);
            orderId = trackOrderHelper.getNewOrderId(restaurantId, headers, paymentMethod, orderComments);
        }

        if(orderState.equals(OrderStatus.ASSIGNED)){
            trackOrderHelper.assignDE(String.valueOf(orderId), deID);
        }

        if(!(orderState.equals(OrderStatus.ASSIGNED) || orderState.equals(TrackOrderHelper.UNASSIGNED))){
            trackOrderHelper.changeOrderStatus(String.valueOf(orderId), orderState);
        }
        trackOrderResponse = trackOrderHelper.trackOrderByOrderId(orderId, headers);
        trackOrderResponses.add(trackOrderResponse);
        trackOrderHelper.smokeCheck(statusCode, statusMessage, trackOrderResponse);
        trackOrderHelper.validateTrackOrder(trackOrderResponse, orderState);
    }

    @Test(dataProvider = "trackOrderMinimalTestData", description = "Test Track Minimal Order API", priority = 1)
    public void trackOrderMinimalTest(String restaurantId,
                                      String userAgent,
                                      String versionCode,
                                      String paymentMethod,
                                      String orderComments,
                                      String orderState,
                                      int statusCode,
                                      String statusMessage){

        if(orderKey == null || !(headers.get("User-Agent").equals(userAgent))) {
            headers.put("User-Agent", userAgent);
            headers.put("version-code", versionCode);
            String[] orderData = trackOrderHelper.getNewOrderKeyWithOrderID(restaurantId, headers, paymentMethod, orderComments);
            orderKey = orderData[0];
            orderId = orderData[1];
        }

        if(orderState.equals(OrderStatus.ASSIGNED)) trackOrderHelper.assignDE(String.valueOf(orderId), deID);

        if(!(orderState.equals(OrderStatus.ASSIGNED) || orderState.equals(TrackOrderHelper.UNASSIGNED)))
            trackOrderHelper.changeOrderStatus(String.valueOf(orderId), orderState);
        trackOrderResponse = trackOrderHelper.trackOrderMinimal(orderKey);
        trackOrderResponses.add(trackOrderResponse);
        trackOrderHelper.smokeCheck(statusCode, statusMessage, trackOrderResponse);
        trackOrderHelper.validateTrackOrder(trackOrderResponse, orderState);
    }

    @AfterClass
    public void printOrderId(){
        for (int i=0;i<trackOrderResponses.size(); i++){
            Reporter.log("[MESSAGE] " + trackOrderResponses.get(i).getData().getOrderId(), true);
        }
    }
}
