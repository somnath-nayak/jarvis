package com.swiggy.api.sf.rng.pojo;


import com.swiggy.api.sf.rng.helper.Utility;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
public class

CreateCouponPOJO {

	@JsonProperty("coupon_type")
	private String couponType;
	@JsonProperty("name")
	private String name;
	@JsonProperty("code")
	private String code;
	@JsonProperty("description")
	private String description;
	@JsonProperty("is_private")
	private Integer isPrivate;
	@JsonProperty("valid_from")
	private String validFrom;
	@JsonProperty("valid_till")
	private String validTill;
	@JsonProperty("total_available")
	private Integer totalAvailable;
	@JsonProperty("total_per_user")
	private Integer totalPerUser;
	@JsonProperty("min_amount")
	private MinAmount minAmount;
	@JsonProperty("customer_restriction")
	private Integer customerRestriction;
	@JsonProperty("city_restriction")
	private Integer cityRestriction;
	@JsonProperty("area_restriction")
	private Integer areaRestriction;
	@JsonProperty("restaurant_restriction")
	private Integer restaurantRestriction;
	@JsonProperty("category_restriction")
	private Integer categoryRestriction;
	@JsonProperty("item_restriction")
	private Integer itemRestriction;
	@JsonProperty("discount_percentage")
	private Integer discountPercentage;
	@JsonProperty("discount_amount")
	private Integer discountAmount;
	@JsonProperty("free_shipping")
	private Integer freeShipping;
	@JsonProperty("free_gifts")
	private Integer freeGifts;
	@JsonProperty("first_order_restriction")
	private Integer firstOrderRestriction;
	@JsonProperty("preferred_payment_method")
	private String preferredPaymentMethod;
	@JsonProperty("user_client")
	private Integer userClient;
	@JsonProperty("slot_restriction")
	private Integer slotRestriction;
	@JsonProperty("timeSlot")
	private List<TimeSlot> timeSlot = null;
	@JsonProperty("created_on")
	private String createdOn;
	@JsonProperty("image")
	private String image;
	@JsonProperty("coupon_bucket")
	private String couponBucket;
	@JsonProperty("min_quantity")
	private MinQuantity minQuantity;
	@JsonProperty("cuisine_restriction")
	private Integer cuisineRestriction;
	@JsonProperty("discount_item")
	private String discountItem;
	@JsonProperty("usage_count")
	private Integer usageCount;
	@JsonProperty("expiry_offset")
	private Integer expiryOffset;
	@JsonProperty("applicable_with_swiggy_money")
	private Boolean applicableWithSwiggyMoney;
	@JsonProperty("is_bank_discount")
	private Integer isBankDiscount;
	@JsonProperty("refund_source")
	private String refundSource;
	@JsonProperty("pg_message")
	private String pgMessage;
	@JsonProperty("supported_ios_version")
	private Integer supportedIosVersion;
	@JsonProperty("created_by")
	private String createdBy;
	@JsonProperty("title")
	private String title;
	@JsonProperty("tnc")
	private List<String> tnc = null;
	@JsonProperty("logo_id")
	private String logoId;
	@JsonProperty("coupon_user_type_id")
	private Integer couponUserTypeId=0;
	@JsonProperty("coupon_cooling_type_id")
	private Integer couponCoolingTypeId=0;
	@JsonProperty("num_txn_payment_type")
	private Integer numTxnPaymentType=0;
	@JsonProperty("capping_percentage")
	private Integer capping_percentage;
	@JsonProperty("updated_on")
	private String updatedOn;
	@JsonProperty("global_offset_days")
	private Integer global_offset_days;

	public Integer getGlobal_offset_days() {
		return global_offset_days;
	}
	public void setGlobal_offset_days(Integer global_offset_days) {
		this.global_offset_days = global_offset_days;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	public void setCapping_percentage(Integer capping_percentage) {
		this.capping_percentage = capping_percentage;
	}

	public Integer getUser_transaction() {
		return user_transaction;
	}

	public void setUser_transaction(Integer user_transaction) {
		this.user_transaction = user_transaction;
	}

	@JsonProperty("user_transaction")
	private Integer user_transaction;

	@JsonProperty("coupon_type")
	public String getCouponType() {
		return couponType;
	}

	@JsonProperty("coupon_type")
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public CreateCouponPOJO withCouponType(String couponType) {
		this.couponType = couponType;
		return this;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	public CreateCouponPOJO withName(String name) {
		this.name = name;
		return this;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	@JsonProperty("code")
	public void setCode(String code) {
		this.code = code;
	}

	public CreateCouponPOJO withCode(String code) {
		this.code = code;
		return this;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public CreateCouponPOJO withDescription(String description) {
		this.description = description;
		return this;
	}

	@JsonProperty("is_private")
	public Integer getIsPrivate() {
		return isPrivate;
	}

	@JsonProperty("is_private")
	public void setIsPrivate(Integer isPrivate) {
		this.isPrivate = isPrivate;
	}

	public CreateCouponPOJO withIsPrivate(Integer isPrivate) {
		this.isPrivate = isPrivate;
		return this;
	}

	@JsonProperty("valid_from")
	public String getValidFrom() {
		return validFrom;
	}

	@JsonProperty("valid_from")
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public CreateCouponPOJO withValidFrom(String validFrom) {
		this.validFrom = validFrom;
		return this;
	}

	@JsonProperty("valid_till")
	public String getValidTill() {
		return validTill;
	}

	@JsonProperty("valid_till")
	public void setValidTill(String validTill) {
		this.validTill = validTill;
	}

	public CreateCouponPOJO withValidTill(String validTill) {
		this.validTill = validTill;
		return this;
	}

	@JsonProperty("total_available")
	public Integer getTotalAvailable() {
		return totalAvailable;
	}

	@JsonProperty("total_available")
	public void setTotalAvailable(Integer totalAvailable) {
		this.totalAvailable = totalAvailable;
	}

	public CreateCouponPOJO withTotalAvailable(Integer totalAvailable) {
		this.totalAvailable = totalAvailable;
		return this;
	}

	@JsonProperty("total_per_user")
	public Integer getTotalPerUser() {
		return totalPerUser;
	}

	@JsonProperty("total_per_user")
	public void setTotalPerUser(Integer totalPerUser) {
		this.totalPerUser = totalPerUser;
	}

	public CreateCouponPOJO withTotalPerUser(Integer totalPerUser) {
		this.totalPerUser = totalPerUser;
		return this;
	}

	@JsonProperty("min_amount")
	public MinAmount getMinAmount() {
		return minAmount;
	}

	@JsonProperty("min_amount")
	public void setMinAmount(MinAmount minAmount) {
		this.minAmount = minAmount;
	}

	public CreateCouponPOJO withMinAmount(MinAmount minAmount) {
		this.minAmount = minAmount;
		return this;
	}

	@JsonProperty("customer_restriction")
	public Integer getCustomerRestriction() {
		return customerRestriction;
	}

	@JsonProperty("customer_restriction")
	public void setCustomerRestriction(Integer customerRestriction) {
		this.customerRestriction = customerRestriction;
	}

	public CreateCouponPOJO withCustomerRestriction(Integer customerRestriction) {
		this.customerRestriction = customerRestriction;
		return this;
	}

	@JsonProperty("city_restriction")
	public Integer getCityRestriction() {
		return cityRestriction;
	}

	@JsonProperty("city_restriction")
	public void setCityRestriction(Integer cityRestriction) {
		this.cityRestriction = cityRestriction;
	}

	public CreateCouponPOJO withCityRestriction(Integer cityRestriction) {
		this.cityRestriction = cityRestriction;
		return this;
	}

	@JsonProperty("area_restriction")
	public Integer getAreaRestriction() {
		return areaRestriction;
	}

	@JsonProperty("area_restriction")
	public void setAreaRestriction(Integer areaRestriction) {
		this.areaRestriction = areaRestriction;
	}

	public CreateCouponPOJO withAreaRestriction(Integer areaRestriction) {
		this.areaRestriction = areaRestriction;
		return this;
	}

	@JsonProperty("restaurant_restriction")
	public Integer getRestaurantRestriction() {
		return restaurantRestriction;
	}

	@JsonProperty("restaurant_restriction")
	public void setRestaurantRestriction(Integer restaurantRestriction) {
		this.restaurantRestriction = restaurantRestriction;
	}

	public CreateCouponPOJO withRestaurantRestriction(Integer restaurantRestriction) {
		this.restaurantRestriction = restaurantRestriction;
		return this;
	}

	@JsonProperty("category_restriction")
	public Integer getCategoryRestriction() {
		return categoryRestriction;
	}

	@JsonProperty("category_restriction")
	public void setCategoryRestriction(Integer categoryRestriction) {
		this.categoryRestriction = categoryRestriction;
	}

	public CreateCouponPOJO withCategoryRestriction(Integer categoryRestriction) {
		this.categoryRestriction = categoryRestriction;
		return this;
	}

	@JsonProperty("item_restriction")
	public Integer getItemRestriction() {
		return itemRestriction;
	}

	@JsonProperty("item_restriction")
	public void setItemRestriction(Integer itemRestriction) {
		this.itemRestriction = itemRestriction;
	}

	public CreateCouponPOJO withItemRestriction(Integer itemRestriction) {
		this.itemRestriction = itemRestriction;
		return this;
	}

	@JsonProperty("discount_percentage")
	public Integer getDiscountPercentage() {
		return discountPercentage;
	}

	@JsonProperty("discount_percentage")
	public void setDiscountPercentage(Integer discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public CreateCouponPOJO withDiscountPercentage(Integer discountPercentage) {
		this.discountPercentage = discountPercentage;
		return this;
	}

	@JsonProperty("discount_amount")
	public Integer getDiscountAmount() {
		return discountAmount;
	}

	@JsonProperty("discount_amount")
	public void setDiscountAmount(Integer discountAmount) {
		this.discountAmount = discountAmount;
	}

	public CreateCouponPOJO withDiscountAmount(Integer discountAmount) {
		this.discountAmount = discountAmount;
		return this;
	}

	@JsonProperty("free_shipping")
	public Integer getFreeShipping() {
		return freeShipping;
	}

	@JsonProperty("free_shipping")
	public void setFreeShipping(Integer freeShipping) {
		this.freeShipping = freeShipping;
	}

	public CreateCouponPOJO withFreeShipping(Integer freeShipping) {
		this.freeShipping = freeShipping;
		return this;
	}

	@JsonProperty("free_gifts")
	public Integer getFreeGifts() {
		return freeGifts;
	}

	@JsonProperty("free_gifts")
	public void setFreeGifts(Integer freeGifts) {
		this.freeGifts = freeGifts;
	}

	public CreateCouponPOJO withFreeGifts(Integer freeGifts) {
		this.freeGifts = freeGifts;
		return this;
	}

	@JsonProperty("first_order_restriction")
	public Integer getFirstOrderRestriction() {
		return firstOrderRestriction;
	}

	@JsonProperty("first_order_restriction")
	public void setFirstOrderRestriction(Integer firstOrderRestriction) {
		this.firstOrderRestriction = firstOrderRestriction;
	}

	public CreateCouponPOJO withFirstOrderRestriction(Integer firstOrderRestriction) {
		this.firstOrderRestriction = firstOrderRestriction;
		return this;
	}

	@JsonProperty("preferred_payment_method")
	public String getPreferredPaymentMethod() {
		return preferredPaymentMethod;
	}

	@JsonProperty("preferred_payment_method")
	public void setPreferredPaymentMethod(String preferredPaymentMethod) {
		this.preferredPaymentMethod = preferredPaymentMethod;
	}

	public CreateCouponPOJO withPreferredPaymentMethod(String preferredPaymentMethod) {
		this.preferredPaymentMethod = preferredPaymentMethod;
		return this;
	}

	@JsonProperty("user_client")
	public Integer getUserClient() {
		return userClient;
	}

	@JsonProperty("user_client")
	public void setUserClient(Integer userClient) {
		this.userClient = userClient;
	}

	public CreateCouponPOJO withUserClient(Integer userClient) {
		this.userClient = userClient;
		return this;
	}

	@JsonProperty("slot_restriction")
	public Integer getSlotRestriction() {
		return slotRestriction;
	}

	@JsonProperty("slot_restriction")
	public void setSlotRestriction(Integer slotRestriction) {
		this.slotRestriction = slotRestriction;
	}

	public CreateCouponPOJO withSlotRestriction(Integer slotRestriction) {
		this.slotRestriction = slotRestriction;
		return this;
	}

	@JsonProperty("timeSlot")
	public List<TimeSlot> getTimeSlot() {
		return timeSlot;
	}

	@JsonProperty("timeSlot")
	public void setTimeSlot(List<TimeSlot> timeSlot) {
		this.timeSlot = timeSlot;
	}

	public CreateCouponPOJO withTimeSlot(List<TimeSlot> timeSlot) {
		this.timeSlot = timeSlot;
		return this;
	}

	@JsonProperty("createdOn")
	public String getCreatedOn() {
		return createdOn;
	}

	@JsonProperty("createdOn")
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public CreateCouponPOJO withCreatedOn(String createdOn) {
		this.createdOn = createdOn;
		return this;
	}

	@JsonProperty("image")
	public String getImage() {
		return image;
	}

	@JsonProperty("image")
	public void setImage(String image) {
		this.image = image;
	}

	public CreateCouponPOJO withImage(String image) {
		this.image = image;
		return this;
	}

	@JsonProperty("coupon_bucket")
	public String getCouponBucket() {
		return couponBucket;
	}

	@JsonProperty("coupon_bucket")
	public void setCouponBucket(String couponBucket) {
		this.couponBucket = couponBucket;
	}

	public CreateCouponPOJO withCouponBucket(String couponBucket) {
		this.couponBucket = couponBucket;
		return this;
	}

	@JsonProperty("min_quantity")
	public MinQuantity getMinQuantity() {
		return minQuantity;
	}

	@JsonProperty("min_quantity")
	public void setMinQuantity(MinQuantity minQuantity) {
		this.minQuantity = minQuantity;
	}

	public CreateCouponPOJO withMinQuantity(MinQuantity minQuantity) {
		this.minQuantity = minQuantity;
		return this;
	}

	@JsonProperty("cuisine_restriction")
	public Integer getCuisineRestriction() {
		return cuisineRestriction;
	}

	@JsonProperty("cuisine_restriction")
	public void setCuisineRestriction(Integer cuisineRestriction) {
		this.cuisineRestriction = cuisineRestriction;
	}

	public CreateCouponPOJO withCuisineRestriction(Integer cuisineRestriction) {
		this.cuisineRestriction = cuisineRestriction;
		return this;
	}

	@JsonProperty("discount_item")
	public String getDiscountItem() {
		return discountItem;
	}

	@JsonProperty("discount_item")
	public void setDiscountItem(String discountItem) {
		this.discountItem = discountItem;
	}

	public CreateCouponPOJO withDiscountItem(String discountItem) {
		this.discountItem = discountItem;
		return this;
	}

	@JsonProperty("usage_count")
	public Integer getUsageCount() {
		return usageCount;
	}

	@JsonProperty("usage_count")
	public void setUsageCount(Integer usageCount) {
		this.usageCount = usageCount;
	}

	public CreateCouponPOJO withUsageCount(Integer usageCount) {
		this.usageCount = usageCount;
		return this;
	}

	@JsonProperty("expiry_offset")
	public Integer getExpiryOffset() {
		return expiryOffset;
	}

	@JsonProperty("expiry_offset")
	public void setExpiryOffset(Integer expiryOffset) {
		this.expiryOffset = expiryOffset;
	}

	public CreateCouponPOJO withExpiryOffset(Integer expiryOffset) {
		this.expiryOffset = expiryOffset;
		return this;
	}

	@JsonProperty("applicable_with_swiggy_money")
	public Boolean getApplicableWithSwiggyMoney() {
		return applicableWithSwiggyMoney;
	}

	@JsonProperty("applicable_with_swiggy_money")
	public void setApplicableWithSwiggyMoney(Boolean applicableWithSwiggyMoney) {
		this.applicableWithSwiggyMoney = applicableWithSwiggyMoney;
	}

	public CreateCouponPOJO withApplicableWithSwiggyMoney(Boolean applicableWithSwiggyMoney) {
		this.applicableWithSwiggyMoney = applicableWithSwiggyMoney;
		return this;
	}

	@JsonProperty("is_bank_discount")
	public Integer getIsBankDiscount() {
		return isBankDiscount;
	}

	@JsonProperty("is_bank_discount")
	public void setIsBankDiscount(Integer isBankDiscount) {
		this.isBankDiscount = isBankDiscount;
	}

	public CreateCouponPOJO withIsBankDiscount(Integer isBankDiscount) {
		this.isBankDiscount = isBankDiscount;
		return this;
	}

	@JsonProperty("refund_source")
	public String getRefundSource() {
		return refundSource;
	}

	@JsonProperty("refund_source")
	public void setRefundSource(String refundSource) {
		this.refundSource = refundSource;
	}

	public CreateCouponPOJO withRefundSource(String refundSource) {
		this.refundSource = refundSource;
		return this;
	}

	@JsonProperty("pg_message")
	public String getPgMessage() {
		return pgMessage;
	}

	@JsonProperty("pg_message")
	public void setPgMessage(String pgMessage) {
		this.pgMessage = pgMessage;
	}

	public CreateCouponPOJO withPgMessage(String pgMessage) {
		this.pgMessage = pgMessage;
		return this;
	}

	@JsonProperty("supported_ios_version")
	public Integer getSupportedIosVersion() {
		return supportedIosVersion;
	}

	@JsonProperty("supported_ios_version")
	public void setSupportedIosVersion(Integer supportedIosVersion) {
		this.supportedIosVersion = supportedIosVersion;
	}

	public CreateCouponPOJO withSupportedIosVersion(Integer supportedIosVersion) {
		this.supportedIosVersion = supportedIosVersion;
		return this;
	}

	@JsonProperty("created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	@JsonProperty("created_by")
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public CreateCouponPOJO withCreatedBy(String createdBy) {
		this.createdBy = createdBy;
		return this;
	}

	public Integer getCapping_percentage() {
		return capping_percentage;
	}



	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	public CreateCouponPOJO withTitle(String title) {
		this.title = title;
		return this;
	}

	@JsonProperty("tnc")
	public List<String> getTnc() {
		return tnc;
	}

	@JsonProperty("tnc")
	public void setTnc(List<String> tnc) {
		this.tnc = tnc;
	}

	public CreateCouponPOJO withTnc(List<String> tnc) {
		this.tnc = tnc;
		return this;
	}

	@JsonProperty("logo_id")
	public String getLogoId() {
		return logoId;
	}

	@JsonProperty("logo_id")
	public void setLogoId(String logoId) {
		this.logoId = logoId;
	}

	public CreateCouponPOJO withLogoId(String logoId) {
		this.logoId = logoId;
		return this;
	}
	@JsonProperty("coupon_user_type_id")
	public Integer getCouponUserTypeId(){
		return this.couponUserTypeId;
	}
	@JsonProperty("coupon_user_type_id")
	public void setCouponUserTypeId(Integer couponUserTypeId){
		this.couponUserTypeId = couponUserTypeId;
	}
	@JsonProperty("coupon_user_type_id")
	public CreateCouponPOJO withCouponUserTypeId(Integer couponUserTypeId){
		this.couponUserTypeId = couponUserTypeId;
		return this;
	}
	@JsonProperty("coupon_cooling_type_id")
	public Integer getCouponCoolingTypeId(){return this.couponCoolingTypeId;}

	@JsonProperty("coupon_cooling_type_id")
	public  void setCouponCoolingTypeId(Integer couponCoolingType_Id){
		this.couponCoolingTypeId = couponCoolingTypeId;
	}
	@JsonProperty("coupon_cooling_type_id")
	public CreateCouponPOJO withCouponCoolingTypeId(Integer couponCoolingTypeId){
		this.couponCoolingTypeId = couponCoolingTypeId;
		return this;
	}
	@JsonProperty("num_txn_payment_type")
	public Integer getNumTxnPaymentType(){
		return this.numTxnPaymentType;
	}
	@JsonProperty("num_txn_payment_type")
	public void setNumTxnPaymentType(Integer numTxnPaymentType){
		this.numTxnPaymentType = numTxnPaymentType ;
	}
	@JsonProperty("num_txn_payment_type")
	public CreateCouponPOJO withNumTxnPaymentType(Integer numTxnPaymentType){
		this.numTxnPaymentType = numTxnPaymentType;
		return this;
	}
	public CreateCouponPOJO withCapping_percentage(Integer capping_percentage){
		this.capping_percentage = capping_percentage;
		return this;
	}
	public CreateCouponPOJO withUser_transaction(Integer user_transaction){
		this.user_transaction = user_transaction;
		return this;
	}

	public CreateCouponPOJO withUpdatedOn(String updatedOn) {
		this.updatedOn= updatedOn;
		return this;
	}

	public CreateCouponPOJO withGlobalOffsetDays(Integer globalOffsetDays) {
		this.global_offset_days= globalOffsetDays;
		return this;
	}

	public CreateCouponPOJO setDefaultData() {
		return this.withCouponType("Discount")
		.withName("TestAutomation")
		.withCode("OrderEditTest"+String.valueOf(new Random().nextInt(1000)))
		.withCouponType("Discount")
		.withDescription("TestAutomation coupon dev")
		.withIsPrivate(0)
		.withTotalAvailable(3)
		.withTotalPerUser(1)
		.withMinAmount(new MinAmount()
				.withCart(100))
		.withMinQuantity(new MinQuantity()
				.withCart(1))
		.withCustomerRestriction(0)
		.withCityRestriction(0)
		.withAreaRestriction(0)
		.withRestaurantRestriction(0)
		.withCategoryRestriction(0)
		.withItemRestriction(0)
		.withDiscountPercentage(0)
		.withDiscountAmount(50)
		.withFreeShipping(0)
		.withFreeGifts(0)
		.withFirstOrderRestriction(0)
		.withPreferredPaymentMethod("Cash")
		.withUserClient(0)		
		.withTimeSlot(new ArrayList<TimeSlot>() {{
			add(new TimeSlot().setDefaultData());}})
		.withSlotRestriction(0)
		.withCreatedOn(Utility.getCurrentDate())
		.withUpdatedOn(Utility.getCurrentDate())
		.withImage("NULL")
		.withCouponBucket("IGCC")
		.withCuisineRestriction(0)
		.withDiscountItem("0")
		.withUsageCount(0)
		.withExpiryOffset(-1)
		.withApplicableWithSwiggyMoney(false)
		.withIsBankDiscount(0)
		.withRefundSource("")
		.withPgMessage("")
		.withSupportedIosVersion(1)
		.withCreatedBy("user@swiggy.in")
		.withTitle("testing")
		.withTnc(new ArrayList<String>() {{
			add("testTNC");
			add("test");
		}})		
		.withLogoId("lbx02iaashsnhea5fpwa")
		.withUser_transaction(0)
		.withGlobalOffsetDays(0)
		.withCapping_percentage(-1);
	}
}