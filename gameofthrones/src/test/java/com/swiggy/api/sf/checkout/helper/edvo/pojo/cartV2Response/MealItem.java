package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "rewardType",
        "mealId",
        "name",
        "mealDescription",
        "mealType",
        "quantity",
        "subtotal",
        "subtotal_trade_discount",
        "packing_charge",
        "total",
        "final_price",
        "is_veg",
        "in_stock",
        "groups"
})
public class MealItem {

    @JsonProperty("rewardType")
    private Object rewardType;
    @JsonProperty("mealId")
    private Integer mealId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("mealDescription")
    private String mealDescription;
    @JsonProperty("mealType")
    private String mealType;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("subtotal")
    private Integer subtotal;
    @JsonProperty("subtotal_trade_discount")
    private Integer subtotalTradeDiscount;
    @JsonProperty("packing_charge")
    private Double packingCharge;
    @JsonProperty("total")
    private Double total;
    @JsonProperty("final_price")
    private Double finalPrice;
    @JsonProperty("is_veg")
    private String isVeg;
    @JsonProperty("in_stock")
    private Integer inStock;
    @JsonProperty("groups")
    private List<Group> groups = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("rewardType")
    public Object getRewardType() {
        return rewardType;
    }

    @JsonProperty("rewardType")
    public void setRewardType(Object rewardType) {
        this.rewardType = rewardType;
    }

    @JsonProperty("mealId")
    public Integer getMealId() {
        return mealId;
    }

    @JsonProperty("mealId")
    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("mealDescription")
    public String getMealDescription() {
        return mealDescription;
    }

    @JsonProperty("mealDescription")
    public void setMealDescription(String mealDescription) {
        this.mealDescription = mealDescription;
    }

    @JsonProperty("mealType")
    public String getMealType() {
        return mealType;
    }

    @JsonProperty("mealType")
    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("subtotal")
    public Integer getSubtotal() {
        return subtotal;
    }

    @JsonProperty("subtotal")
    public void setSubtotal(Integer subtotal) {
        this.subtotal = subtotal;
    }

    @JsonProperty("subtotal_trade_discount")
    public Integer getSubtotalTradeDiscount() {
        return subtotalTradeDiscount;
    }

    @JsonProperty("subtotal_trade_discount")
    public void setSubtotalTradeDiscount(Integer subtotalTradeDiscount) {
        this.subtotalTradeDiscount = subtotalTradeDiscount;
    }

    @JsonProperty("packing_charge")
    public Double getPackingCharge() {
        return packingCharge;
    }

    @JsonProperty("packing_charge")
    public void setPackingCharge(Double packingCharge) {
        this.packingCharge = packingCharge;
    }

    @JsonProperty("total")
    public Double getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Double total) {
        this.total = total;
    }

    @JsonProperty("final_price")
    public Double getFinalPrice() {
        return finalPrice;
    }

    @JsonProperty("final_price")
    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    @JsonProperty("is_veg")
    public String getIsVeg() {
        return isVeg;
    }

    @JsonProperty("is_veg")
    public void setIsVeg(String isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("in_stock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("in_stock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("groups")
    public List<Group> getGroups() {
        return groups;
    }

    @JsonProperty("groups")
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}