package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.HeaderHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class CatalogAttributes extends SnDDp {
    SANDHelper helper= new SANDHelper();
    HeaderHelper headerHelper= new HeaderHelper();

    @Test(dataProvider = "catalogAttributesVariants", description = "Verify variants having catalog attributes fields")
    public void catalogAttributes(String restId, String lat,String lng, String data){
        String response= helper.menuV4RestId(restId,lat,lng).ResponseValidator.GetBodyAsText();
        String[] itemIds= headerHelper.JsonString(response, "$.data.menu.items[*].id").split(",");
        String menuId= helper.getMenuId(restId, SANDConstants.regularMenu);
        List<String> listRedis= helper.keys(SANDConstants.database, SANDConstants.menuKey+menuId);
            List<String> list = new ArrayList<>();
            for (int i = 0; i < itemIds.length; i++) {
                List<String> variantId = Arrays.asList(headerHelper.JsonString(response, "$.data.menu.items."+ Integer.parseInt(itemIds[i]) + "..variant_groups..variations..id").split(","));
                for (int j = 0; j < variantId.size(); j++) {
                    String attributeFields = "$.data.menu.items." + Integer.parseInt(itemIds[i]) + "..variant_groups..variations.[" + j + "]..attributes";
                    if (!headerHelper.JsonString(response, "$.data.menu.items." + Integer.parseInt(itemIds[i]) + "..variant_groups..variations.[" + j + "]..attributes").isEmpty()) {
                        list.add(variantId.get(j));
                        String e=itemIds[i];
                        List<String> result = listRedis.stream()
                                .filter(a -> a.contains(data+e))
                                .collect(Collectors.toList());
                        String redisData = helper.catalogAttributes(SANDConstants.database, SANDConstants.menuKey + menuId, result.get(0));
                        result.clear();
                        String redisVariant = JsonPath.read(redisData, "$.variants").toString();
                        List<String> redisIdList = Arrays.asList(headerHelper.JsonString(redisVariant, "$.variant_groups..variations..id").split(","));
                        if(redisIdList.contains(variantId.get(j))) {
                            String catalogAttributesRedis ="$.variant_groups..variations..catalog_attributes";
                           Assert.assertEquals(headerHelper.JsonString(response, attributeFields + ".spiceLevel"), headerHelper.JsonString(redisVariant,catalogAttributesRedis+".spice_level"));

                            Assert.assertEquals(headerHelper.JsonString(response, attributeFields + ".vegClassifier"), headerHelper.JsonString(redisVariant,catalogAttributesRedis+".veg_classifier"));

                            Assert.assertEquals(headerHelper.JsonString(response, attributeFields + ".portionSize"), headerHelper.JsonString(redisVariant,catalogAttributesRedis+"..value")+" "+headerHelper.JsonString(redisVariant,catalogAttributesRedis+"..unit"));

                            Assert.assertTrue(headerHelper.JsonString(response, attributeFields + ".accompaniments").contains(headerHelper.JsonString(redisVariant,catalogAttributesRedis+".accompaniments")));
                        }
                    }
                    else{
                        Reporter.log("No Catalog attributes on Variant level");
                }
                }
            }
    }

    @Test(dataProvider = "catalogAttributesRest", description = "restaurant level attributes fields")
    public void catalogAttributesRest(String restId, String lat,String lng){
        String response= helper.menuV4RestId(restId,lat,lng).ResponseValidator.GetBodyAsText();
       String[] attributeRestLevel= headerHelper.JsonString(response, "$.data.restaurantAttributes..type").split(",");
        System.out.println(attributeRestLevel);
        List<String> listRedis= helper.keys(SANDConstants.database, SANDConstants.restKey+restId);
        for(int i=0; i<listRedis.size(); i++){
            if(listRedis.get(i).contains("ATTRIBUTES_V3")) {
                String attributesRedis = helper.redisHget(SANDConstants.database2, SANDConstants.restKey + restId, listRedis.get(i));
                Assert.assertTrue(attributeRestLevel[i].contains(headerHelper.JsonString(attributesRedis, "$..halalClassifier")));
                Assert.assertTrue(attributeRestLevel[i+1].contains(headerHelper.JsonString(attributesRedis, "$..qualityPackaging")));
            }
            if(listRedis.get(i).contains("TAGS_V3")) {
                String attributesRedis = helper.redisHget(SANDConstants.database, SANDConstants.restKey + restId, listRedis.get(i));
                Assert.assertNotNull(headerHelper.JsonString(attributesRedis, "$..tag_group"));
                Assert.assertNotNull(headerHelper.JsonString(attributesRedis, "$..tag_value"));
            }
        }
    }


    @Test(dataProvider = "catalogAttributesItem", description = "Item level attributes fields")
    public void catalogAttributesItem(String restId, String lat,String lng, String data) {
        Processor p= helper.menuV4RestId(restId,lat,lng);
       String response= p.ResponseValidator.GetBodyAsText();
         String[] itemIds= headerHelper.JsonString(response, "$.data.menu.items[*].id").split(",");
        System.out.println(itemIds[3]);
        String menuId= helper.getMenuId(restId, SANDConstants.regularMenu);
        List<String> listRedis= helper.keys(SANDConstants.database, SANDConstants.menuKey+menuId);

            for (int i=0; i<itemIds.length; i++){
                String attributeFields = "$..menu.items."+itemIds[i] +".attributes";
                if (!attributeFields.isEmpty()) {
                    System.out.println(itemIds[i]);
                    String attributeFiel = "$..menu.items."+itemIds[i] +".attributes";
                    System.out.println(attributeFields);
                    String e=itemIds[i];
                    List<String> result = listRedis.stream()
                            .filter(a -> a.contains(data+e))
                            .collect(Collectors.toList());
                    String redisData = helper.catalogAttributes(SANDConstants.database, SANDConstants.menuKey + menuId, result.get(0));
                    result.clear();
                    headerHelper.JsonString(response, attributeFields + ".portionSize");
                    headerHelper.JsonString(response, attributeFields + ".accompaniments");
                    Assert.assertEquals(headerHelper.JsonString(response, attributeFields + ".spiceLevel"), headerHelper.JsonString(redisData,"$.catalogAttributes.spice_level"));

                    Assert.assertEquals(headerHelper.JsonString(response, attributeFields + ".vegClassifier"), headerHelper.JsonString(redisData,"$.catalogAttributes.veg_classifier"));

                    Assert.assertEquals(headerHelper.JsonString(response, attributeFields + ".portionSize"), headerHelper.JsonString(redisData,"$.catalogAttributes..value")+" "+headerHelper.JsonString(redisData,"$.catalogAttributes..unit"));

                    Assert.assertTrue(headerHelper.JsonString(response, attributeFields + ".accompaniments").contains(headerHelper.JsonString(redisData,"$.catalogAttributes.accompaniments")));
                }
        }
    }

   /* @Test(dataProvider = "catalogAttributes", description = "addons level attributes fields")
    public void catalogAttributesAddons(String restId, String lat,String lng) {
        String response = helper.menuV4RestId(restId, lat, lng).ResponseValidator.GetBodyAsText();
        List<String> itemIds = Arrays.asList(headerHelper.JsonString(response, "$.data.menu.items[*].id").split(","));
        String menuId = helper.getMenuId(restId, SANDConstants.regularMenu);
        List<String> listRedis = helper.keys(SANDConstants.database, SANDConstants.menuKey + menuId);
        for (int i = 0; i < itemIds.size(); i++) {
                //String redisData = helper.catalogAttributes(SANDConstants.database, SANDConstants.menuKey + menuId, listRedis.get(k));

                for (String key : itemIds)
                    if (!headerHelper.JsonString(response, "$.data.menu.items." + Integer.parseInt(key) + ".attributes").isEmpty()) {
                        String attributeFields = "$.data.menu.items.attributes";
                        headerHelper.JsonString(response, attributeFields + ".portionSize");
                        headerHelper.JsonString(response, attributeFields + ".accompaniments");
                    }
            }
        }*/


}
