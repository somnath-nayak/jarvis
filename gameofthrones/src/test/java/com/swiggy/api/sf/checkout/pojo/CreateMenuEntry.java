package com.swiggy.api.sf.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class CreateMenuEntry {

    @JsonProperty("cartItems")
    private List<Cart> cartItems = null;
    @JsonProperty("restaurantId")
    private Integer restaurantId;
    @JsonProperty("address_id")
    private Integer address_id;
    @JsonProperty("payment_cod_method")
    private String payment_cod_method;
    @JsonProperty("order_comments")
    private String order_comments;
    @JsonProperty("password")
    private String password;
    @JsonProperty("mobile")
    private Long mobile;
    @JsonProperty("name")
    private String name;
    @JsonProperty("mobile")
    private String mobile1;
    @JsonProperty("address")
    private String address;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("area")
    private String area;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("flat_no")
    private String flat_no;
    @JsonProperty("city")
    private String city;
    @JsonProperty("annotation")
    private String annotation;
    @JsonProperty("carttype")
    private String carttype;
    @JsonProperty("ordertype")
    private String ordertype;

    @JsonProperty("RESTAURANT_LISTING")
    private RestaurantListing rESTAURANTLISTING;

    @JsonProperty("COLLECTION")
    private COLLECTION cOLLECTION;

    @JsonProperty("CAROUSEL")
    private CAROUSEL cAROUSEL;


    /**
     * No args constructor for use in serialization
     *
     */
    public CreateMenuEntry() {
    }

    /**
     *
     * @param cartItems
     * @param restaurantId
     */
    public CreateMenuEntry(List<Cart> cartItems, Integer restaurantId, Integer address_id, String payment_cod_method, String order_comments, String password, Long mobile, String name, String mobile1, String address, String landmark, String area, String lat, String lng, String flat_no, String city, String annotation, String cart_type,String order_type, RestaurantListing RestaurantListing, COLLECTION COLLECTION, CAROUSEL CAROUSEL) {
        super();
        this.cartItems = cartItems;
        this.restaurantId = restaurantId;
        this.address_id= address_id;
        this.payment_cod_method=payment_cod_method;
        this.order_comments=order_comments;
        this.password=password;
        this.mobile=mobile;
        this.name = name;
        this.mobile1 = mobile1;
        this.address = address;
        this.landmark = landmark;
        this.area = area;
        this.lat = lat;
        this.lng = lng;
        this.flat_no = flat_no;
        this.city = city;
        this.annotation = annotation;
        this.carttype = cart_type;
        this.ordertype = order_type;
        this.rESTAURANTLISTING = RestaurantListing;
        this.cOLLECTION = COLLECTION;
        this.cAROUSEL = CAROUSEL;
    }


    public List<Cart> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<Cart> cartItems) {
        this.cartItems = cartItems;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Integer getaddress_id() {
        return address_id;
    }

    public void setaddress_id(Integer address_id) {
        this.address_id = address_id;
    }

    public String getpayment_cod_method() {
        return payment_cod_method;
    }

    public void setpayment_cod_method(String payment_cod_method) {
        this.payment_cod_method = payment_cod_method;
    }
    
    public String getcart_type() {

        return carttype; }

    public void setcart_type(String cart_type) {

        this.carttype = cart_type;

    }

     public String getorder_type() {

        return ordertype;

    }
      public void setorder_type(String order_type) {

        this.ordertype = order_type;

    }

    public String getorder_comments() {
        return order_comments;
    }

    public void setorder_comments(String order_comments) {
        this.order_comments = order_comments;
    }

    public String getpassword() {
        return password;
    }

    public void setpassword(String password) {
        this.password = password;
    }

    public Long getmobile() {
        return mobile;
    }

    public void setmobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile1() {
        return mobile1;
    }

    public void setMobile1(String mobile1) {
        this.mobile1 = mobile1;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getflat_no() {
        return flat_no;
    }

    public void setflat_no(String flat_no) {
        this.flat_no = flat_no;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }
    @JsonProperty("RESTAURANT_LISTING")
    public RestaurantListing getRESTAURANTLISTING() {
        return rESTAURANTLISTING;
    }

    @JsonProperty("RESTAURANT_LISTING")
    public void setRESTAURANTLISTING(RestaurantListing rESTAURANTLISTING) {
        this.rESTAURANTLISTING = rESTAURANTLISTING;
    }

    @JsonProperty("COLLECTION")
    public COLLECTION getCOLLECTION() {
        return cOLLECTION;
    }

    @JsonProperty("COLLECTION")
    public void setCOLLECTION(COLLECTION cOLLECTION) {
        this.cOLLECTION = cOLLECTION;
    }

    @JsonProperty("CAROUSEL")
    public CAROUSEL getCAROUSEL() {
        return cAROUSEL;
    }

    @JsonProperty("CAROUSEL")
    public void setCAROUSEL(CAROUSEL cAROUSEL) {
        this.cAROUSEL = cAROUSEL;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cartItems", cartItems).append("restaurantId", restaurantId).append("address_id", address_id).append("payment_cod_method", payment_cod_method).append("order_comments",order_comments).append("mobile", mobile).append("password", password).append("name", name).append("mobile", mobile).append("address", address).append("landmark", landmark).append("area", area).append("lat", lat).append("lng", lng).append("flat_no", flat_no).append("city", city).append("annotation", annotation).append("RESTAURANT_LISTING", rESTAURANTLISTING).append("COLLECTION", cOLLECTION).append("CAROUSEL", cAROUSEL).toString();
    }

    }