package com.swiggy.api.sf.checkout.helper.paas;

import com.swiggy.api.sf.checkout.constants.PaasConstant;
import com.swiggy.api.sf.checkout.pojo.ValidatePhonepeOtp;
import com.swiggy.api.sf.checkout.pojo.mySms.MySmsLogin;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;

public class PaasHelper {

    Initialize gameofthrones=new Initialize();
    JsonHelper jsonHelper=new JsonHelper();


    public static HashMap<String,String> requestHeader(String tid,String token){
        HashMap<String, String> headers = new HashMap<>();
        headers.put(PaasConstant.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put("tid",tid);
        headers.put("token",token);
        headers.put("deviceId",PaasConstant.DEVICE_ID);
        return headers;
    }

    public Processor loginFromCheckout(String mobile, String password){
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "logincheckout", gameofthrones);
        return new Processor(gameOfThronesService, headers, new String[]{mobile, password});
    }

    //Mobikwik API

    public Processor mobikwikCheckBalance(String tid, String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "mobikwikcheckbalance", gameofthrones);
        //GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "mobikwikCheckBalance", gameofthrones);
        Processor processor = new Processor(service, requestHeaders);
        return processor;
    }

    public Processor mobikwikDelink(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "mobikwikDelink", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }

    public Processor mobikwikLink(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "mobikwikLink", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }

    public Processor mobikwikValidateOtp(String tid,String token,String otp) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "mobikwikValidateOtp", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null, new String[]{otp});
        return processor;
    }

    public Processor mobikwikHasToken(String tid, String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "mobikwikHasToken", gameofthrones);
        Processor processor = new Processor(service, requestHeaders);
        return processor;
    }

    //freeCharge API

    public Processor freechargeDelink(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "freechargeDelink", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }

    public Processor freechargeCheckBalance(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "freechargeCheckBalance", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }

    public Processor freechargeLink(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "freechargeLink", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }

    public Processor freechargeValidateOtp(String tid,String token,String otpID,String otp) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "freechargeValidateOtp", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null, new String[]{otpID,otp});
        return processor;
    }

    public Processor freechargeHasToken(String tid, String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "freechargeHasToken", gameofthrones);
        Processor processor = new Processor(service, requestHeaders);
        return processor;
    }

    public Processor freechargeResendOtp(String tid,String token,String otpID) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "freechargeResendOtp", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null, new String[]{otpID});
        return processor;
    }

    //paytm API

    public Processor paytmCheckBalance(String tid, String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "paytmCheckBalance", gameofthrones);
        Processor processor = new Processor(service, requestHeaders);
        return processor;
    }

    public Processor paytmDelink(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "paytmDelink", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }

    public Processor paytmLink(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "paytmLink", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }

    public Processor paytmValidateOtp(String tid,String token,String state,String otp) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "paytmValidateOtp", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null, new String[]{state,otp});
        return processor;
    }

    public Processor paytmHasToken(String tid, String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "paytmHasToken", gameofthrones);
        Processor processor = new Processor(service, requestHeaders);
        return processor;
    }

    //phonepe API

    public Processor phonepeCheckBalance(String tid, String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "phonepeCheckBalance", gameofthrones);
        Processor processor = new Processor(service, requestHeaders);
        return processor;
    }

    public Processor phonepeDelink(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "phonepeDelink", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }

    public Processor phonepeIsLink(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "phonepeIsLink", gameofthrones);
        Processor processor = new Processor(service, requestHeaders);
        return processor;
    }

    public Processor phonepeValidateOtp(String tid,String token,String otp) {
        Processor processor=null;
      try{
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "phonepeValidateOtp", gameofthrones);
        ValidatePhonepeOtp validatePhonepeOtp = new ValidatePhonepeOtp(otp);
        String payload=jsonHelper.getObjectToJSON(validatePhonepeOtp);
        processor = new Processor(service, requestHeaders,payload,0);
        }
      catch(Exception e){
          return null;
      }
      return processor;
    }

    public Processor phonepeSendOtp(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "phonepeGetOtp", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }

    public Processor phonepeResendOtp(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "phonepeResendOtp", gameofthrones);
        Processor processor = new Processor(service, requestHeaders);
        return processor;
    }

    public Processor phonepeProfileChecksum(String tid,String token) {
        HashMap<String, String> requestHeaders = requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "phonepeProfileChecksum", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null);
        return processor;
    }
}
