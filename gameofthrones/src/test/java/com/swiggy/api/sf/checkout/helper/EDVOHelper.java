package com.swiggy.api.sf.checkout.helper;

import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class EDVOHelper {
    Initialize gameofthrones = new Initialize();
    SANDHelper sandHelper = new SANDHelper();


    public void createCart(){
        GameOfThronesService gots = new GameOfThronesService("checkout", "createcartv2", gameofthrones);
        HashMap<String, String> headers = this.getHeaders();
        String[] payload = new String[]{""};
        Processor processor = new Processor(gots, headers, payload);
    }

    public Processor login(String mobile, String password){
        return sandHelper.login(new String[]{mobile,password});
    }

    public HashMap<String,String> getHeaders(){
        Processor processor = this.login("8884292249","welcome123");
        String contentType = "application/json";
        String tid = processor.ResponseValidator.GetNodeValue("$.data.token");
        String token = processor.ResponseValidator.GetNodeValue("$.tid");
        String versionCode = "36";
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", contentType);
        requestHeader.put("Tid", tid);
        requestHeader.put("token", token);
        requestHeader.put("version-code", versionCode);
        return requestHeader;
    }
}
