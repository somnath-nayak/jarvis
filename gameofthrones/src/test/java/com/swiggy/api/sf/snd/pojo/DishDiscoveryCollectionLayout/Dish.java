package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class Dish {

    private String type;
    private List<DataString> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Dish() {
    }

    /**
     *
     * @param data
     * @param type
     */
    public Dish(String type, List<DataString> data) {
        super();
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<DataString> getData() {
        return data;
    }

    public void setData(List<DataString> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("data", data).toString();
    }

}