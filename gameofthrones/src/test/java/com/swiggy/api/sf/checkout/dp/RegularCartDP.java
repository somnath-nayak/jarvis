package com.swiggy.api.sf.checkout.dp;


import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.MinAmount;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.*;

import java.io.IOException;
import java.util.*;
import com.swiggy.api.sf.checkout.helper.CartPayloadHelper;

public class RegularCartDP {


    CartPayloadHelper CartPayloadHelper=new CartPayloadHelper();
    RngHelper rngHelper = new RngHelper();


    static Cart cartpayloadNoAddons;
    static Cart cartpayloadAddons;
    static String couponCode;


    @BeforeClass
    public void setCartPayload() throws IOException {
        cartpayloadNoAddons=CartPayloadHelper.getObjectCartPayloadRegular("12.9279", "77.6271",false);
        try{
            cartpayloadAddons=CartPayloadHelper.getObjectCartPayloadRegular("12.9279", "77.6271",true);
        couponCode=getCoupon();
        } catch (Exception e){
            e.printStackTrace();
            //cartpayloadAddons=CartPayloadHelper.getObjectCartPayloadRegular("12.9279", "77.6271",true);
        }

    }


    public String getCoupon() throws IOException{
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO();
        createCouponPOJO.setDefaultData().withMinAmount(new MinAmount().withCart(0))
                .withValidFrom(CheckoutHelper.getCurrentDate())
                .withValidTill(CheckoutHelper.getFutureDate())
                .withApplicableWithSwiggyMoney(true);
        String couponCode=createCouponPOJO.getCode();
        rngHelper.createCoupon(createCouponPOJO);
        return couponCode;
    }

//test case=264
    @DataProvider(name = "cart")
    public static Object[][] CartData() {
        return new Object[][]{
//Parameter->1.User Agent,2. version-code,3.cart payload,4.index,5.coupon code

 //create Cart Without Addons &  no coupon & no TD
                {"Swiggy-Android", "310",cartpayloadNoAddons,1,null,"noTD"},
                {"Web", "309",cartpayloadNoAddons,1,null,"noTD"},
                {"Swiggy-iOS", "234",cartpayloadNoAddons,1,null,"noTD"},
 //create Cart Without Addons & no coupon & freeDelTD
                 {"Swiggy-Android", "310",cartpayloadNoAddons,1,null,"FreeDelTD"},
                 {"Web", "309",cartpayloadNoAddons,1,null,"FreeDelTD"},
                 {"Swiggy-iOS", "234",cartpayloadNoAddons,1,null,"FreeDelTD"},
 //create Cart Without Addons & no coupon & flatTD
                 {"Swiggy-Android", "310",cartpayloadNoAddons,1,null,"flatTD"},
                 {"Web", "309",cartpayloadNoAddons,1,null,"flatTD"},
                 {"Swiggy-iOS", "234",cartpayloadNoAddons,1,null,"flatTD"},
 //create Cart Without Addons & with Coupon & no TD
                 {"Swiggy-Android", "310",cartpayloadNoAddons,1,couponCode,"noTD"},
                 {"Web", "309",cartpayloadNoAddons,1,couponCode,"noTD"},
                 {"Swiggy-iOS", "234",cartpayloadNoAddons,1,couponCode,"noTD"},
 //create Cart Without Addons & with Coupon & flatTD
                {"Swiggy-Android", "310",cartpayloadNoAddons,1,couponCode,"flatTD"},
                {"Web", "309",cartpayloadNoAddons,1,couponCode,"flatTD"},
                {"Swiggy-iOS", "234",cartpayloadNoAddons,1,couponCode,"flatTD"},
//create Cart Without Addons & with Coupon & freeDelTD
                {"Swiggy-Android", "310",cartpayloadNoAddons,1,couponCode,"freeDelTD"},
                {"Web", "309",cartpayloadNoAddons,1,couponCode,"freeDelTD"},
                {"Swiggy-iOS", "234",cartpayloadNoAddons,1,couponCode,"freeDelTD"},
 //create Cart With Addons & no coupon & no TD
                {"Swiggy-Android", "310",cartpayloadAddons,1,null,"noTD"},
                {"Web", "309",cartpayloadAddons,1,null,"noTD"},
                {"Swiggy-iOS", "234",cartpayloadAddons,1,null,"noTD"},
 //create Cart With Addons & no coupon & freeDelTD
                {"Swiggy-Android", "310",cartpayloadAddons,1,null,"freeDelTD"},
                {"Web", "309",cartpayloadAddons,1,null,"freeDelTD"},
                {"Swiggy-iOS", "234",cartpayloadAddons,1,null,"freeDelTD"},
 //create Cart With Addons & no coupon & flatTD
                {"Swiggy-Android", "310",cartpayloadAddons,1,null,"flatTD"},
                {"Web", "309",cartpayloadAddons,1,null,"flatTD"},
                {"Swiggy-iOS", "234",cartpayloadAddons,1,null,"flatTD"},
 //create Cart With Addons & Coupon & noTD
                {"Swiggy-Android", "310",cartpayloadAddons,1,couponCode,"noTD"},
                {"Web", "309",cartpayloadAddons,1,couponCode,"noTD"},
                {"Swiggy-iOS", "234",cartpayloadAddons,1,couponCode,"noTD"},
//create Cart With Addons & Coupon & freeDelTD
                {"Swiggy-Android", "310",cartpayloadAddons,1,couponCode,"freeDelTD"},
                {"Web", "309",cartpayloadAddons,1,couponCode,"freeDelTD"},
                {"Swiggy-iOS", "234",cartpayloadAddons,1,couponCode,"freeDelTD"},
//create Cart With Addons & Coupon & flatTD
                {"Swiggy-Android", "310",cartpayloadAddons,1,couponCode,"flatTD"},
                {"Web", "309",cartpayloadAddons,1,couponCode,"flatTD"},
                {"Swiggy-iOS", "234",cartpayloadAddons,1,couponCode,"flatTD"}
        };
    }


    @DataProvider(name = "deleteCouponOnCart")
    public static Object[][] deleteCouponOnCart() {
        return new Object[][]{
//Parameter->1.User Agent,2. version-code,3.cart payload,4.index,5.coupon code

   //create Cart Without Addons & with Coupon & noTD
                {"Swiggy-Android", "310",cartpayloadNoAddons,1,couponCode,"noTD"},
                {"Web", "309",cartpayloadNoAddons,1,couponCode,"noTD"},
                {"Swiggy-iOS", "234",cartpayloadNoAddons,1,couponCode,"noTD"},
 //create Cart Without Addons & with Coupon & freeDelTD
                {"Swiggy-Android", "310",cartpayloadNoAddons,1,couponCode,"freeDelTD"},
                {"Web", "309",cartpayloadNoAddons,1,couponCode,"freeDelTD"},
                {"Swiggy-iOS", "234",cartpayloadNoAddons,1,couponCode,"freeDelTD"},
 //create Cart Without Addons & with Coupon & flatTD
                {"Swiggy-Android", "310",cartpayloadNoAddons,1,couponCode,"flatTD"},
                {"Web", "309",cartpayloadNoAddons,1,couponCode,"flatTD"},
                {"Swiggy-iOS", "234",cartpayloadNoAddons,1,couponCode,"flatTD"},
  //create Cart With Addons & Coupon & noTD
                {"Swiggy-Android", "310",cartpayloadAddons,1,couponCode,"noTD"},
                {"Web", "309",cartpayloadAddons,1,couponCode,"noTD"},
                {"Swiggy-iOS", "234",cartpayloadAddons,1,couponCode,"noTD"},
 //create Cart With Addons & Coupon & freeDelTD
                {"Swiggy-Android", "310",cartpayloadAddons,1,couponCode,"freeDelTD"},
                {"Web", "309",cartpayloadAddons,1,couponCode,"freeDelTD"},
                {"Swiggy-iOS", "234",cartpayloadAddons,1,couponCode,"freeDelTD"},
 //create Cart With Addons & Coupon & flatTD
                {"Swiggy-Android", "310",cartpayloadAddons,1,couponCode,"flatTD"},
                {"Web", "309",cartpayloadAddons,1,couponCode,"flatTD"},
                {"Swiggy-iOS", "234",cartpayloadAddons,1,couponCode,"flatTD"}
        };
    }


        @DataProvider(name = "cartCheckTotal")
        public static Object[][] cartCheckTotal() {
                            return new Object[][]{
 //Parameter->1.User Agent,2. version-code,3.cart payload,4.index,5.coupon code

      //create Cart Without Addons & No Coupon & noTD
            {"Swiggy-Android", "310",cartpayloadNoAddons,1,null,"noTD"},
            {"Web", "309",cartpayloadNoAddons,1,null,"noTD"},
            {"Swiggy-iOS", "234",cartpayloadNoAddons,1,null,"noTD"},
      //create Cart With Addons & No Coupon & noTD
           {"Swiggy-Android", "310",cartpayloadAddons,1,null,"noTD"},
           {"Web", "309",cartpayloadAddons,1,null,"noTD"},
           {"Swiggy-iOS", "234",cartpayloadAddons,1,null,"noTD"}
        };
    }
}