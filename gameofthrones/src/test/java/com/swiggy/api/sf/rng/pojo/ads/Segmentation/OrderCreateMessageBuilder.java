package com.swiggy.api.sf.rng.pojo.ads.Segmentation;


import com.swiggy.api.sf.rng.constants.SegmentationConstants;

public class OrderCreateMessageBuilder {
    
    public OrderCreateMessage orderCreateMessage = new OrderCreateMessage();
    
    public OrderCreateMessageBuilder() {
        new OrderCreateMessage();
    }
    
    public OrderCreateMessageBuilder withCityId(String cityId) {
        orderCreateMessage.setCityId(1);
        return this;
    }
    
    public OrderCreateMessageBuilder withAreaaId(String areaId) {
        orderCreateMessage.setAreaId(Integer.parseInt(areaId));
        return this;
    }
    
    public OrderCreateMessageBuilder withCustomerId(String customerId) {
        orderCreateMessage.setCustomerId(Long.parseLong(customerId));
        return this;
    }
    
    public OrderCreateMessageBuilder withPaymentTxnStatus(String paymentTxnStatus) {
        orderCreateMessage.setPaymentTxnStatus(paymentTxnStatus);
        return this;
    }
    
    public OrderCreateMessageBuilder withOrderType(String orderType) {
        orderCreateMessage.setOrderType(orderType);
        return this;
    }
    
    public OrderCreateMessageBuilder withSharedOrder(boolean sharedOrder) {
        orderCreateMessage.setSharedOrder(sharedOrder);
        return this;
    }
    
    public OrderCreateMessageBuilder withOrderId(String orderId) {
        orderCreateMessage.setOrderId(orderId);
        return this;
    }
    
    public OrderCreateMessageBuilder withPaymentMethod(String paymentMethod) {
        orderCreateMessage.setPaymentMethod(paymentMethod);
        return this;
    }
    
    public OrderCreateMessageBuilder withOrderStatus(String order_status) {
        orderCreateMessage.setStatus(order_status);
        return this;
    }
    
    public OrderCreateMessageBuilder withRestaurantid(String restaurantId) {
        orderCreateMessage.setRestaurantId(Long.parseLong(restaurantId));
        return this;
    }
    
    public OrderCreateMessageBuilder withOrderTime(String orderTime) {
        //yyyy-MM-dd HH:mm:ss
        orderCreateMessage.setOrderTime(orderTime);
        return this;
    }
    
    public OrderCreateMessage orderCreateMessageByCartTypeUserId(String customerId, String cartType, String orderId, String restaurantid, String orderTime) {
        OrderCreateMessage orderCreateMessage = new OrderCreateMessage();
        orderCreateMessage.setCityId(1);
        orderCreateMessage.setAreaId(1);
        orderCreateMessage.setCustomerId(Long.parseLong(customerId));
        orderCreateMessage.setOrderId(orderId);
        orderCreateMessage.setOrderType(cartType);
        orderCreateMessage.setPaymentTxnStatus(SegmentationConstants.paymentTxnStatus[0]);
        orderCreateMessage.setSharedOrder(false);
        orderCreateMessage.setStatus(SegmentationConstants.orderStatus[0]);
        orderCreateMessage.setRestaurantId(Long.parseLong(restaurantid));
        orderCreateMessage.setOrderTime(orderTime);
        orderCreateMessage.setPaymentMethod(SegmentationConstants.paymentMethod[0]);
        return orderCreateMessage;
        
    }
    
    public OrderCreateMessage orderCancelledMessageByCartTypeUserId(String customerId, String cartType, String orderId, String restaurantid, String orderTime) {
        OrderCreateMessage orderCreateMessage = new OrderCreateMessage();
        orderCreateMessage.setCityId(1);
        orderCreateMessage.setAreaId(1);
        orderCreateMessage.setCustomerId(Long.parseLong(customerId));
        orderCreateMessage.setOrderId(orderId);
        orderCreateMessage.setOrderType(cartType);
        orderCreateMessage.setPaymentTxnStatus("refund-initiated");
        orderCreateMessage.setSharedOrder(false);
        orderCreateMessage.setStatus("cancelled");
        orderCreateMessage.setRestaurantId(Long.parseLong(restaurantid));
        orderCreateMessage.setOrderTime(orderTime);
        orderCreateMessage.setPaymentMethod(SegmentationConstants.paymentMethod[0]);
        return orderCreateMessage;
        
    }
    
    public OrderCreateMessage orderCreateMessageByPaymentMethodUserId(String customerId, String paymentMethod, String orderId, String restaurantid, String orderTime) {
        
        OrderCreateMessage orderCreateMessage = new OrderCreateMessage();
        orderCreateMessage.setCityId(1);
        orderCreateMessage.setAreaId(1);
        orderCreateMessage.setCustomerId(Long.parseLong(customerId));
        orderCreateMessage.setOrderId(orderId);
        orderCreateMessage.setOrderType(SegmentationConstants.cartType[3]);
        orderCreateMessage.setPaymentTxnStatus(SegmentationConstants.paymentTxnStatus[0]);
        orderCreateMessage.setSharedOrder(false);
        orderCreateMessage.setStatus(SegmentationConstants.orderStatus[0]);
        orderCreateMessage.setRestaurantId(Long.parseLong(restaurantid));
        orderCreateMessage.setOrderTime(orderTime);
        orderCreateMessage.setPaymentMethod(paymentMethod);
        
        return orderCreateMessage;
    }
    
}
