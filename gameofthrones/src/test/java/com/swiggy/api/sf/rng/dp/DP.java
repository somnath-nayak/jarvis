
package com.swiggy.api.sf.rng.dp;

import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Random;

import com.swiggy.api.sf.rng.helper.SuperMultiTDHelper;
import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.DataProvider;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import com.swiggy.api.sf.checkout.pojo.AddressPOJO;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.CartEvaluateRequest;
import com.swiggy.api.sf.rng.pojo.Category;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.CreateFlatTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateFlatTdEntry;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;
import com.swiggy.api.sf.rng.pojo.ItemRequest;
import com.swiggy.api.sf.rng.pojo.Menu;
import com.swiggy.api.sf.rng.pojo.MenuEvaluateRequest;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.Slot;
import com.swiggy.api.sf.rng.pojo.SubCategory;
import com.swiggy.api.sf.rng.pojo.MinAmount;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.Slot;
import com.swiggy.api.sf.rng.pojo.SubCategory;
import com.swiggy.api.sf.rng.pojo.TimeSlot;
import com.swiggy.api.sf.rng.tests.RandomNumber;
import com.swiggy.api.sf.snd.helper.SANDHelper;

import edu.emory.mathcs.backport.java.util.Arrays;
import framework.gameofthrones.Aegon.PropertiesHandler;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class DP {

	RngHelper rngHelper = new RngHelper();
	CMSHelper cmsHelper = new CMSHelper();
	RngConstants rngConstants;
	CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
	CheckoutHelper checkoutHelper = new CheckoutHelper();
	RandomNumber rm = new RandomNumber(2000, 3000);

	String Env;
	public PropertiesHandler properties = new PropertiesHandler();

	public DP() {
		if (System.getenv("ENVIRONMENT") == null)
			Env = properties.propertiesMap.get("environment");
		else
			Env = System.getenv("ENVIRONMENT");
	}

	@DataProvider(name = "TDCreation")
	public Object[][] TDCreation() {
		Object[][] data = new Object[1][25];
		data[0][0] = "AutomationScript";
		data[0][1] = "Automation_TradeDiscount";
		data[0][2] = "Automation_TradeDiscount_Description";
		data[0][3] = "Automation_TradeDiscount_ShortDescription";
		data[0][4] = "1507616669000";
		data[0][5] = String.valueOf(DateUtils.addHours(new Date(), 24).toInstant().getEpochSecond() * 1000);
		data[0][6] = "Freebie";
		data[0][7] = "ZERO_DAYS_DORMANT";
		data[0][8] = "100";
		data[0][9] = "0";
		data[0][10] = "Restaurant";
		data[0][11] = "6120";
		data[0][12] = "Freebie";
		data[0][13] = "100";
		data[0][14] = "172260";
		data[0][15] = "true";
		data[0][16] = "GauravKumar_Automation";
		data[0][17] = "GauravKumar_Automation";
		data[0][18] = "false";
		data[0][19] = "false";
		data[0][20] = "";
		data[0][21] = "false";
		data[0][22] = "false";
		data[0][23] = "false";
		data[0][24] = "false";
		return data;

	}

	@DataProvider(name = "listEvalutaion")
	public Object[][] listEvalutaion() {
		Object[][] data = new Object[1][29];

		data[0][0] = "false";
		data[0][1] = "1122343";
		data[0][2] = "ANDROID";
		data[0][3] = "229";
		data[0][4] = "AutomationScript";
		data[0][5] = "Automation_TradeDiscount";
		data[0][6] = "Automation_TradeDiscount_Description";
		data[0][7] = "Automation_TradeDiscount_ShortDescription";
		data[0][8] = "1507616669000";
		data[0][9] = String.valueOf(DateUtils.addHours(new Date(), 24).toInstant().getEpochSecond() * 1000);
		data[0][10] = "Freebie";
		data[0][11] = "ZERO_DAYS_DORMANT";
		data[0][12] = "100";
		data[0][13] = "0";
		data[0][14] = "Restaurant";
		data[0][15] = "6120";
		data[0][16] = "Freebie";
		data[0][17] = "100";
		data[0][18] = "172260";
		data[0][19] = "true";
		data[0][20] = "GauravKumar_Automation";
		data[0][21] = "GauravKumar_Automation";
		data[0][22] = "false";
		data[0][23] = "false";
		data[0][24] = "";
		data[0][25] = "false";
		data[0][26] = "false";
		data[0][27] = "false";
		data[0][28] = "false";
		return data;

	}

	@DataProvider(name = "menuEvaluation")
	public Object[][] menuEvaluation() {
		Object[][] data = new Object[1][34];
		data[0][0] = "AutomationScript";
		data[0][1] = "Automation_TradeDiscount";
		data[0][2] = "Automation_TradeDiscount_Description";
		data[0][3] = "Automation_TradeDiscount_ShortDescription";
		data[0][4] = "1507616669000";
		data[0][5] = String.valueOf(DateUtils.addHours(new Date(), 24).toInstant().getEpochSecond() * 1000);
		data[0][6] = "Freebie";
		data[0][7] = "ZERO_DAYS_DORMANT";
		data[0][8] = "100";
		data[0][9] = "0";
		data[0][10] = "Restaurant";
		data[0][11] = "6120";
		data[0][12] = "Freebie";
		data[0][13] = "100";
		data[0][14] = "172260";
		data[0][15] = "true";
		data[0][16] = "GauravKumar_Automation";
		data[0][17] = "GauravKumar_Automation";
		data[0][18] = "false";
		data[0][19] = "false";
		data[0][20] = "";
		data[0][21] = "false";
		data[0][22] = "false";
		data[0][23] = "false";
		data[0][24] = "false";
		data[0][25] = "false";
		data[0][26] = "1122343";
		data[0][27] = "ANDROID";
		data[0][28] = "229";
		data[0][29] = "100";
		data[0][30] = "5000";
		data[0][31] = "1";
		data[0][32] = "34716";
		data[0][33] = "712465";
		return data;
	}

	@DataProvider(name = "getTd")
	public Object[][] getTD() {
		return new Object[][] { { "25646" } };
	}

	@DataProvider(name = "tdbyrestid")
	public static Object[][] tdbyrestid() {
		return new Object[][] { { "6120" } };
	}

	@DataProvider(name = "createPercentageTD")
	public Object[][] createPercentageTD() throws IOException {

		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));

		List rlist = rngHelper.getRestaurantListing("12.9719", "77.6412", "0");

		// Creating Menus
		List<Menu> menus = new ArrayList<>();
		menus.add(new Menu("132563", "Paneer Butter Masala"));

		// Creating SubCategorys
		List<SubCategory> subCategorys = new ArrayList<>();
		subCategorys.add(new SubCategory("14018", "Chicken Cutlets", menus));

		// Creating RestaurantLists
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(rlist.get(0).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists2 = new ArrayList<>();
		restaurantLists2.add(new RestaurantList(rlist.get(1).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists3 = new ArrayList<>();
		restaurantLists3.add(new RestaurantList(rlist.get(2).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists4 = new ArrayList<>();
		restaurantLists4.add(new RestaurantList(rlist.get(3).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists5 = new ArrayList<>();
		restaurantLists5.add(new RestaurantList(rlist.get(4).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists6 = new ArrayList<>();
		restaurantLists6.add(new RestaurantList(rlist.get(6).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists7 = new ArrayList<>();
		restaurantLists7.add(new RestaurantList(rlist.get(7).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists8 = new ArrayList<>();
		restaurantLists8.add(new RestaurantList(rlist.get(8).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists9 = new ArrayList<>();
		restaurantLists9.add(new RestaurantList(rlist.get(9).toString(), "Test", new ArrayList<>()));

		rngHelper.disabledActiveTD(rlist.get(0).toString());
		rngHelper.disabledActiveTD(rlist.get(1).toString());
		rngHelper.disabledActiveTD(rlist.get(2).toString());
		rngHelper.disabledActiveTD(rlist.get(3).toString());
		rngHelper.disabledActiveTD(rlist.get(4).toString());
		rngHelper.disabledActiveTD(rlist.get(5).toString());
		rngHelper.disabledActiveTD(rlist.get(6).toString());
		rngHelper.disabledActiveTD(rlist.get(7).toString());
		rngHelper.disabledActiveTD(rlist.get(8).toString());
		rngHelper.disabledActiveTD(rlist.get(9).toString());

		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateTdEntry percetageTradeDiscount2 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists2)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateTdEntry percetageTradeDiscount3 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists3)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateTdEntry percetageTradeDiscount4 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists4)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateTdEntry percetageTradeDiscount5 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists5)
				// .slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateTdEntry percetageTradeDiscount6 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists6).slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateTdEntry percetageTradeDiscount7 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists7).slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount8 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists8).slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount9 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists9).slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		List allList = RngHelper.getList(cmsHelper.getRestListTD());

		ListIterator it = allList.listIterator();
		while (it.hasNext()) {
			rngHelper.disabledActiveTD(it.next().toString());
		}
		while (it.hasPrevious()) {
			it.previous();
		}
		CreateTdEntry percetageTradeDiscount10 = new CreateTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				/** .slots(slots) **/
				.ruleDiscount("Percentage", "Category", "10", "0", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateTdEntry percetageTradeDiscount11 = new CreateTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateTdEntry percetageTradeDiscount12 = new CreateTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateTdEntry percetageTradeDiscount13 = new CreateTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateTdEntry percetageTradeDiscount14 = new CreateTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateTdEntry percetageTradeDiscount15 = new CreateTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				.slots(slots).ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateTdEntry percetageTradeDiscount16 = new CreateTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				.slots(slots).ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount17 = new CreateTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				.slots(slots).ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount18 = new CreateTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				.slots(slots).ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount19 = new CreateTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				/** .slots(slots) **/
				.ruleDiscount("Percentage", "Subcategory", "10", "0", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateTdEntry percetageTradeDiscount20 = new CreateTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Subcategory", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateTdEntry percetageTradeDiscount21 = new CreateTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Subcategory", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateTdEntry percetageTradeDiscount22 = new CreateTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Subcategory", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateTdEntry percetageTradeDiscount23 = new CreateTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Subcategory", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateTdEntry percetageTradeDiscount24 = new CreateTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				.slots(slots).ruleDiscount("Percentage", "Subcategory", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateTdEntry percetageTradeDiscount25 = new CreateTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				.slots(slots).ruleDiscount("Percentage", "Subcategory", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount26 = new CreateTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				.slots(slots).ruleDiscount("Percentage", "Subcategory", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount27 = new CreateTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				.slots(slots).ruleDiscount("Percentage", "Subcategory", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount28 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				/** .slots(slots) **/
				.ruleDiscount("Percentage", "Item", "10", "0", "2000").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateTdEntry percetageTradeDiscount29 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateTdEntry percetageTradeDiscount30 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateTdEntry percetageTradeDiscount31 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateTdEntry percetageTradeDiscount32 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				// .slots(slots)
				.ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateTdEntry percetageTradeDiscount33 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				.slots(slots).ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateTdEntry percetageTradeDiscount34 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				.slots(slots).ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount35 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				.slots(slots).ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateTdEntry percetageTradeDiscount36 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				.slots(slots).ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		return new Object[][] { { jsonHelper.getObjectToJSON(percetageTradeDiscount1) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount2) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount3) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount4) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount5) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount6) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount7) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount8) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount9) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount10) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount11) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount12) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount13) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount14) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount15) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount16) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount17) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount18) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount19) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount20) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount21) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount22) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount23) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount24) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount25) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount26) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount27) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount28) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount29) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount30) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount31) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount32) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount33) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount34) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount35) },
				{ jsonHelper.getObjectToJSON(percetageTradeDiscount36) }, };
	}

	@DataProvider(name = "PercentageTD_EvaluationWithNoMinCartAtRestaurantLevel")
	public Object[][] PercentageTD_EvaluationWithNoMinCartAtRestaurantLevel() throws IOException {
		HashMap<String, String> Keys = rngHelper.createPercentageWithNoMinAmountAtRestaurantLevel();
		String restId = Keys.get("restId").toString();
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TID").toString();
		String count = "1";

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(RngConstants.AndroidOS);
		a1.add(RngConstants.versionCode200);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(RngConstants.AndroidOS);
		a2.add(RngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(RngConstants.AndroidOS);
		a3.add(RngConstants.versionCode200);

  	return new Object[][] {{ "list", a1,tid,true },  { "menu", a2,tid,true } ,{ "cart", a3,tid,true }

		};
	}

	@DataProvider(name = "PercentageTD_EvaluationWithMinCartAtRestaurantLevel")
	public Object[][] PercentageTD_EvaluationWithMinCartAtRestaurantLevel() throws IOException {
		HashMap<String, String> Keys = rngHelper.createPercentageWithMinAmountAtRestaurantLevel();
		String restId = Keys.get("restId");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		ArrayList a3 = new ArrayList();

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add("50");
		a15.add("50");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);


		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add("500");
		a16.add("500");
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid,true }, { "list", a2,tid,true }, { "menu", a8,tid,true }, { "menu", a9,tid,true }, { "cart", a15,tid,false },
				{ "cart", a16,tid,true } };
	}

	@DataProvider(name = "PercentageTD_EvaluationWithSwiggyFirstOrderRestaurantLevel")
	public Object[][] PercentageTD_EvaluationWithSwiggyFirstOrderRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithSwiggyFirstOrderAtRestaurantLevel();
		String restId = (String) Keys.get("restId");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add("500");
		a15.add("500");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add("500");
		a16.add("500");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, true }, { "menu", a8,tid, false },
				{ "menu", a9,tid, true }, { "cart", a15,tid, false }, { "cart", a16,tid, true } };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithRestaurantFirstOrderRestaurantLevel")
	public Object[][] PercentageTD_EvaluationWithRestaurantFirstOrderRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithSwiggyRestaurantOrderAtRestaurantLevel();
		String restId = (String) Keys.get("restId");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add(500);
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId,"10");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("true");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("true"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * 
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add(500);
		 * a6.add("500"); a6.add(userId); a6.add("true");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tid, true }, { "menu", a2,tid, true },
				{ "cart", a3,tid, true },/* { "list", a4,false }, { "menu", a5,false },{ "cart", a6,false } */ };
	}

	@DataProvider(name = "PercentageTD_EvaluationWithUserRestrictionRestaurantLevel")
	public Object[][] PercentageTD_EvaluationWithUserRestrictionRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithUserRestrictionAtRestaurantLevel();
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("true");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();
		rngHelper.userMappingTD(userId2, tid);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId2);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId2);
		a5.add(rngConstants.AndroidOS);
		a5.add(rngConstants.versionCode229);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add(categoryId);
		a6.add(subCategoryId);
		a6.add(itemId);
		a6.add(count);
		a6.add("500");
		a6.add("500");
		a6.add(userId2);
		a6.add("true");
		a6.add(rngConstants.AndroidOS);
		a6.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, false }, { "menu", a2,tid, false }, { "cart", a3,tid, false },
				{ "list", a4,tid, true }, { "menu", a5,tid, true }, { "cart", a6,tid, true } };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithTimeSlotRestrictionRestaurantLevel")
	public Object[][] PercentageTD_EvaluationWithTimeSlotRestrictionRestaurantLevel()
			throws IOException, InterruptedException {
		HashMap Keys = rngHelper.createPercentageWithTimeSlotRestrictionAtRestaurantLevel();
		String restId = (String) Keys.get("restId");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);
		/*
		 * Thread.sleep(20000);
		 * 
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("true");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */
		return new Object[][] { { "list", a1,tid, true }, { "menu", a2,tid, true }, /*
																			 * { "cart", a3,true },{ "list", a4,false },
																			 * { "menu", a5,false }, { "cart", a6,false}
																			 */ };

	}

	@DataProvider(name = "createFlatTDLevel")
	public Object[][] createFlatTD() throws IOException {

		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));

		List rlist = rngHelper.getRestaurantListing("12.9719", "77.6412", "0");

		// Creating Slots

		// Creating Menus
		List<Menu> menus = new ArrayList<>();
		menus.add(new Menu("132563", "Paneer Butter Masala"));

		// Creating SubCategorys
		List<SubCategory> subCategorys = new ArrayList<>();
		subCategorys.add(new SubCategory("14018", "Chicken Cutlets", menus));

		// Creating RestaurantLists
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(rlist.get(0).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists2 = new ArrayList<>();
		restaurantLists2.add(new RestaurantList(rlist.get(1).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists3 = new ArrayList<>();
		restaurantLists3.add(new RestaurantList(rlist.get(2).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists4 = new ArrayList<>();
		restaurantLists4.add(new RestaurantList(rlist.get(3).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists5 = new ArrayList<>();
		restaurantLists5.add(new RestaurantList(rlist.get(4).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists6 = new ArrayList<>();
		restaurantLists6.add(new RestaurantList(rlist.get(6).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists7 = new ArrayList<>();
		restaurantLists7.add(new RestaurantList(rlist.get(7).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists8 = new ArrayList<>();
		restaurantLists8.add(new RestaurantList(rlist.get(8).toString(), "Test", new ArrayList<>()));

		List<RestaurantList> restaurantLists9 = new ArrayList<>();
		restaurantLists9.add(new RestaurantList(rlist.get(9).toString(), "Test", new ArrayList<>()));

		rngHelper.disabledActiveTD(rlist.get(0).toString());
		rngHelper.disabledActiveTD(rlist.get(1).toString());
		rngHelper.disabledActiveTD(rlist.get(2).toString());
		rngHelper.disabledActiveTD(rlist.get(3).toString());
		rngHelper.disabledActiveTD(rlist.get(4).toString());
		rngHelper.disabledActiveTD(rlist.get(5).toString());
		rngHelper.disabledActiveTD(rlist.get(6).toString());
		rngHelper.disabledActiveTD(rlist.get(7).toString());
		rngHelper.disabledActiveTD(rlist.get(8).toString());
		rngHelper.disabledActiveTD(rlist.get(9).toString());

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "100", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateFlatTdEntry flatTradeDiscount2 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists2)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateFlatTdEntry flatTradeDiscount3 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists3)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(true).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateFlatTdEntry flatTradeDiscount4 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists4)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateFlatTdEntry flatTradeDiscount5 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists5)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateFlatTdEntry flatTradeDiscount6 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists6).slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(true)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateFlatTdEntry flatTradeDiscount7 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists7).slots(slots)
				.ruleDiscount("Flat", "Restaurant", "10", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateFlatTdEntry flatTradeDiscount8 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists8).slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(true)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateFlatTdEntry flatTradeDiscount9 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Restaurant").restaurantList(restaurantLists9).slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("NINETY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		List allList = RngHelper.getList(cmsHelper.getRestListTD());

		ListIterator it = allList.listIterator();
		while (it.hasNext()) {
			rngHelper.disabledActiveTD(it.next().toString());
		}
		while (it.hasPrevious()) {
			it.previous();
		}
		CreateFlatTdEntry flatTradeDiscount10 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				/** .slots(slots) **/
				.ruleDiscount("Flat", "Category", "200", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateFlatTdEntry flatTradeDiscount11 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Category", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateFlatTdEntry flatTradeDiscount12 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Category", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(true).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateFlatTdEntry flatTradeDiscount13 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Category", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateFlatTdEntry flatTradeDiscount14 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Category", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateFlatTdEntry flatTradeDiscount15 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				.slots(slots).ruleDiscount("Flat", "Category", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateFlatTdEntry flatTradeDiscount16 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				.slots(slots).ruleDiscount("Flat", "Category", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateFlatTdEntry flatTradeDiscount17 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				.slots(slots).ruleDiscount("Flat", "Category", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateFlatTdEntry flatTradeDiscount18 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Category")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				.slots(slots).ruleDiscount("Flat", "Category", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateFlatTdEntry flatTradeDiscount19 = new CreateFlatTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				/** .slots(slots) **/
				.ruleDiscount("Flat", "Subcategory", "200", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateFlatTdEntry flatTradeDiscount20 = new CreateFlatTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Subcategory", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateFlatTdEntry flatTradeDiscount21 = new CreateFlatTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Subcategory", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(true).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateFlatTdEntry flatTradeDiscount22 = new CreateFlatTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Subcategory", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateFlatTdEntry flatTradeDiscount23 = new CreateFlatTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Subcategory", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateFlatTdEntry flatTradeDiscount24 = new CreateFlatTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				.slots(slots).ruleDiscount("Flat", "Subcategory", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateFlatTdEntry flatTradeDiscount25 = new CreateFlatTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				.slots(slots).ruleDiscount("Flat", "Subcategory", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateFlatTdEntry flatTradeDiscount26 = new CreateFlatTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				.slots(slots).ruleDiscount("Flat", "Subcategory", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateFlatTdEntry flatTradeDiscount27 = new CreateFlatTdBuilder().nameSpace("Subcategory").header("Subcategory")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Subcategory")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, false, 2))
				.slots(slots).ruleDiscount("Flat", "Subcategory", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateFlatTdEntry flatTradeDiscount28 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				/** .slots(slots) **/
				.ruleDiscount("Flat", "Item", "200", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateFlatTdEntry flatTradeDiscount29 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Item", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateFlatTdEntry flatTradeDiscount30 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Item", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(true).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		CreateFlatTdEntry flatTradeDiscount31 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Category", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateFlatTdEntry flatTradeDiscount32 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				// .slots(slots)
				.ruleDiscount("Flat", "Item", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		CreateFlatTdEntry flatTradeDiscount33 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				.slots(slots).ruleDiscount("Flat", "Item", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		CreateFlatTdEntry flatTradeDiscount34 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				.slots(slots).ruleDiscount("Flat", "Item", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateFlatTdEntry flatTradeDiscount35 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				.slots(slots).ruleDiscount("Flat", "Item", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		CreateFlatTdEntry flatTradeDiscount36 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item")
				.restaurantList(rngHelper.createRandomRestaurantList(it.next().toString(), true, 2, true, 2, true, 2))
				.slots(slots).ruleDiscount("Flat", "Item", "200", "100").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		return new Object[][] { { jsonHelper.getObjectToJSON(flatTradeDiscount1) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount2) }, { jsonHelper.getObjectToJSON(flatTradeDiscount3) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount4) }, { jsonHelper.getObjectToJSON(flatTradeDiscount5) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount6) }, { jsonHelper.getObjectToJSON(flatTradeDiscount7) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount8) }, { jsonHelper.getObjectToJSON(flatTradeDiscount9) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount10) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount11) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount12) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount13) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount14) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount15) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount16) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount17) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount18) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount19) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount20) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount21) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount22) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount23) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount24) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount25) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount26) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount27) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount28) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount29) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount30) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount31) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount32) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount33) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount34) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount35) },
				{ jsonHelper.getObjectToJSON(flatTradeDiscount36) }, };
	}

	@DataProvider(name = "PercentageTD_EvaluationWithThirtyDaysDormantRestaurantLevel")
	public Object[][] PercentageTD_EvaluationWithThirtyDaysDormantRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithThirtyDaysDormantAtRestaurantLevel();
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId, "30");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("false");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tid, false }, { "menu", a2,tid, false },
				{ "cart", a3,tid, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false } */ };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithSixtyDaysDormantRestaurantLevel")
	public Object[][] PercentageTD_EvaluationWithSixtyDaysDormantRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithSixtyDaysDormantAtRestaurantLevel();
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId, "30");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("false");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tid, false }, { "menu", a2,tid, false },
				{ "cart", a3,tid, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false } */ };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithNinetyDaysDormantRestaurantLevel")
	public Object[][] PercentageTD_EvaluationWithNinetyDaysDormantRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithNinetyDaysDormantAtRestaurantLevel();
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId, "30");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("false");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tid, false }, { "menu", a2,tid, false },
				{ "cart", a3,tid, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false } */ };

	}

	@DataProvider(name = "menuEvaluationWithPercentageAtRestaurant")
	public Object[][] menuEvaluationWithPercentageAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		System.out.println(processor.ResponseValidator.GetBodyAsText());
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ rngConstants.minCartAmount, randomRestaurant, "false", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.AndroidOS,
						rngConstants.versionCode200 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.IosOS,
						rngConstants.versionCode200 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.IosOS,
						rngConstants.versionCode250 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.WebOS,
						rngConstants.versionCode250 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.WebOS,
						rngConstants.versionCode200 } };
	}

	@DataProvider(name = "menuEvaluationWithPercentageMinLimitAtRestaurant")
	public Object[][] menuEvaluationWithPercentageMinLimitAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		System.out.println(processor.ResponseValidator.GetBodyAsText());
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ rngConstants.minCartAmount, randomRestaurant, "false", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.AndroidOS,
						rngConstants.versionCode200 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.IosOS,
						rngConstants.versionCode200 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.IosOS,
						rngConstants.versionCode250 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.WebOS,
						rngConstants.versionCode250 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.WebOS,
						rngConstants.versionCode200 } };
	}

	@DataProvider(name = "menuEvaluationWithPercentageMinLimitSwiggyFirstAtRestaurant")
	public Object[][] menuEvaluationWithPercentageMinLimitSwiggyFirstAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").firstOrderRestriction(true).build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		System.out.println(processor.ResponseValidator.GetBodyAsText());
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229, true },
				{ rngConstants.minCartAmount, randomRestaurant, "false", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229, false },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.IosOS,
						rngConstants.versionCode200, true },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.WebOS,
						rngConstants.versionCode200, true } };
	}

	@DataProvider(name = "menuEvaluationWithPercentageMinLimitRestaurantFirstAtRestaurant")
	public Object[][] menuEvaluationWithPercentageMinLimitRestaurantFirstAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").restaurantFirstOrder(true).build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		System.out.println(processor.ResponseValidator.GetBodyAsText());
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "false", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.IosOS,
						rngConstants.versionCode200 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.WebOS,
						rngConstants.versionCode200 } };
	}

	@DataProvider(name = "menuEvaluationWithPercentageMinLimitWithUserRestrictiontAtRestaurant")
	public Object[][] menuEvaluationWithPercentageMinLimitWithUserRestrictiontAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(true).build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229, createdTD_Id },
				{ rngConstants.minCartAmount, randomRestaurant, "false", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229, createdTD_Id },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.IosOS,
						rngConstants.versionCode200, createdTD_Id },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.WebOS,
						rngConstants.versionCode200, createdTD_Id } };
	}

	@DataProvider(name = "menuEvaluationWithPercentageMinLimitWithTimeSlotRestrictionAtRestaurant")
	public Object[][] menuEvaluationWithPercentageMinLimitWithTimeSlotRestrictionAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		List<Slot> slots = new ArrayList<>();
		/* slots.add(new Slot("1559", "ALL","1305")); */
		System.out.println(new SimpleDateFormat("HHmm").format(new Date()));
		slots.add(new Slot(new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), -2))));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").slots(slots)
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").timeSlotRestriction(true).build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "false", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.IosOS,
						rngConstants.versionCode200 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.WebOS,
						rngConstants.versionCode200 } };
	}

	@DataProvider(name = "menuEvalutateWithPercentageAtRestaurantWithDormantUser")
	public Object[][] menuEvalutateWithPercentageAtRestaurantWithDormantUser() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		List<Slot> slots = new ArrayList<>();
		System.out.println(new SimpleDateFormat("HHmm").format(new Date()));
		slots.add(new Slot(new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), -2))));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").slots(slots)
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").timeSlotRestriction(false)
				.dormant_user_type("THIRTY_DAYS_DORMANT").build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		System.out
				.println("CreatTD ID " + "**************************************************************************");
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "false", userId, rngConstants.AndroidOS,
						rngConstants.versionCode229 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.IosOS,
						rngConstants.versionCode200 },
				{ rngConstants.minCartAmount, randomRestaurant, "true", userId, rngConstants.WebOS,
						rngConstants.versionCode200 } };
	}

	@DataProvider(name = "cartEvaluationWithPercentageAtRestaurant")
	public Object[][] cartEvaluationWithPercentageAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List rlist = rngHelper.getRestaurantListing("12.9751", "77.6451", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		System.out.println(processor.ResponseValidator.GetBodyAsText());

		List<Map<String, Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);

		String itemId = itemDetails.get(0).get("item_id").toString();
		String categoryId = itemDetails.get(0).get("cat_id").toString();
		String subCategoryId = itemDetails.get(0).get("subcat_id").toString();
		String count = "1";
		String price = "200";
		String minCartAmount = price;
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, "0", userId, "false",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "false",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, "10", userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "false",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.IosOS, rngConstants.versionCode200 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.WebOS, rngConstants.versionCode200 } };
	}

	@DataProvider(name = "FreebieTD_EvaluationforVersionCheck")
	public Object[][] FreebieTD_EvaluationforVersionCheck() throws IOException {

		String randomRestaurant = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326", "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(randomRestaurant);
		HashMap Keys = rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel("0", randomRestaurant, freebieitemId);

		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
        String tid = Keys.get("TDID").toString();
		String isCheck = "true"; /* Keys.get("isCheck").toString(); */
		String tdEnables = "true";/* Keys.get("tdEnables").toString(); */

		ArrayList a1 = new ArrayList();

		a1.add(randomRestaurant);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(randomRestaurant);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();

		a4.add(randomRestaurant);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(randomRestaurant);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode150);

		ArrayList a6 = new ArrayList();
		a6.add(randomRestaurant);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(randomRestaurant);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.WebOS);
		a7.add(rngConstants.versionCode250);
		a7.add(isCheck);
		a7.add(tdEnables);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(randomRestaurant);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(randomRestaurant);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(randomRestaurant);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);

		a12.add(randomRestaurant);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode150);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);

		a13.add(randomRestaurant);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(randomRestaurant);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.WebOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();

		a15.add(randomRestaurant);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add("100");
		a15.add("100");
		a15.add(userId);
		a15.add("true");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a18 = new ArrayList();
		a18.add(randomRestaurant);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add("100");
		a18.add("100");
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);

		ArrayList a19 = new ArrayList();

		a19.add(randomRestaurant);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add("100");
		a19.add("100");
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);

		ArrayList a20 = new ArrayList();
		a20.add(randomRestaurant);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add("100");
		a20.add("100");
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode150);

		ArrayList a21 = new ArrayList();
		a21.add(randomRestaurant);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add("100");
		a21.add("100");
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);

		ArrayList a22 = new ArrayList();
		a22.add(randomRestaurant);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add("100");
		a22.add("100");
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);

		return new Object[][] { { "list", a1,tid,freebieitemId, true }, { "list", a3,tid,freebieitemId, false }, { "list", a4,tid,freebieitemId, true },
				{ "list", a5,tid,freebieitemId, false }, { "list", a6,tid,freebieitemId, true }, { "list", a7,tid,freebieitemId, true }, { "menu", a9,tid,freebieitemId, true },
				{ "menu", a10,tid,freebieitemId, false }, { "menu", a11,tid,freebieitemId, true }, { "menu", a12,tid,freebieitemId, false }, { "menu", a13,tid,freebieitemId, true },
				{ "menu", a14,tid,freebieitemId, true }, { "cart", a15,tid,freebieitemId, true }, { "cart", a18,tid,freebieitemId, false }, { "cart", a19,tid,freebieitemId, true },
				{ "cart", a20,tid,freebieitemId, false }, { "cart", a21,tid,freebieitemId, true }, { "cart", a22,tid,freebieitemId, true } };
	}

	@DataProvider(name = "FreebieTD_EvaluationWithNoMinCartAtRestaurantLevel")
	public Object[][] FreebieTD_EvaluationWithNoMinCartAtRestaurantLevel() throws IOException {

		String randomRestaurant = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326",
																		// "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(randomRestaurant);
		HashMap Keys = rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel("0", randomRestaurant, freebieitemId);
		String tid = Keys.get("TDID").toString();
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";

		String isCheck = "true"; /* Keys.get("isCheck").toString(); */
		String tdEnables = "true";/* Keys.get("tdEnables").toString(); */

		ArrayList a1 = new ArrayList();
		a1.add(randomRestaurant);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(randomRestaurant);
		a8.add("true");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a15 = new ArrayList();
		a15.add(randomRestaurant);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add("100");
		a15.add("100");
		a15.add(userId);
		a15.add("true");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid,freebieitemId,true }, { "menu", a8,tid,freebieitemId,true }, { "cart", a15,tid,freebieitemId,true }, };
	}

	@DataProvider(name = "FreebieTD_EvaluationWithMinCartAtRestaurantLevel")
	public Object[][] FreebieTD_EvaluationWithMinCartAtRestaurantLevel() throws Exception {

		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326", "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(restId);
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		HashMap Keys = rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel("100", restId, freebieitemId);
		String isCheck = "true";
		String tdEnables = "true";
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("true");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add("10");
		a15.add("10");
		a15.add(userId);
		a15.add("true");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add("100");
		a16.add("100");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid,freebieitemId, true }, { "menu", a8,tid,freebieitemId, true }, { "cart", a15,tid,freebieitemId, false },
				{ "cart", a16,tid,freebieitemId, true } };
	}

	@DataProvider(name = "FreebieTD_EvaluationWithSwiggyFirstOrderRestaurantLevel")
	public Object[][] FreebieTD_EvaluationWithSwiggyFirstOrderRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326", "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(restId);
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		HashMap Keys = rngHelper.createFeebieTDWithFirstOrderRestrictionAtRestaurantLevel("0", restId, freebieitemId);
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add("100");
		a15.add("100");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add("100");
		a16.add("100");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid,freebieitemId, false }, { "list", a2,tid,freebieitemId, true }, { "menu", a8,tid,freebieitemId, false },
				{ "menu", a9,tid,freebieitemId,true }, { "cart", a15,tid,freebieitemId, false }, { "cart", a16,tid,freebieitemId, true } };

	}

	@DataProvider(name = "FreebieTD_EvaluationWithRestaurantFirstOrderRestaurantLevel")
	public Object[][] FreebieTD_EvaluationWithRestaurantFirstOrderRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326", "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(restId);
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		HashMap Keys = rngHelper.createFeebieTDWithRestaurantFirstOrderAtRestaurantLevel("0", restId, freebieitemId);
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add("100");
		a15.add("100");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId,"10");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("true");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("true"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * 
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add(500);
		 * a6.add("500"); a6.add(userId); a6.add("true");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tid,freebieitemId, true }, { "menu", a8,tid,freebieitemId, true },
				{ "cart", a15,tid,freebieitemId, true }/* , { "list", a4,true }, { "menu", a5,true }, { "cart", a6,true } */ };

	}

	@DataProvider(name = "FreebieTD_EvaluationWithTimeSlotRestrictionAtRestaurantLevel")
	public Object[][] FreebieTD_EvaluationWithTimeSlotRestrictionAtRestaurantLevel()
			throws IOException, InterruptedException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326", "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(restId);
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		HashMap Keys = rngHelper.createFeebieTDWithTimeSlotRestrictionAtRestaurantLevel("0", restId, freebieitemId);
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

//		Thread.sleep(20000);
//
//		ArrayList a4 = new ArrayList();
//		a4.add(restId);
//		a4.add("true");
//		a4.add(userId);
//		a4.add(rngConstants.AndroidOS);
//		a4.add(rngConstants.versionCode229);
//
//		ArrayList a5 = new ArrayList();
//		a5.add(rngConstants.minCartAmount);
//		a5.add(restId);
//		a5.add("false");
//		a5.add(userId);
//		a5.add(rngConstants.AndroidOS);
//		a5.add(rngConstants.versionCode229);
//
//		ArrayList a6 = new ArrayList();
//		a6.add(restId);
//		a6.add(categoryId);
//		a6.add(subCategoryId);
//		a6.add(itemId);
//		a6.add(count);
//		a6.add("500");
//		a6.add("500");
//		a6.add(userId);
//		a6.add("false");
//		a6.add(rngConstants.AndroidOS);
//		a6.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid,freebieitemId, true }, { "menu", a2,tid,freebieitemId, true },
				{ "cart", a3,tid,freebieitemId, true }/* ,{ "list", a4,false }, { "menu", a5,false }, { "cart", a6,false} */ };

	}

	@DataProvider(name = "FreebieTD_EvaluationWithUserRestrictionAtRestaurantLevel")
	public Object[][] FreebieTD_EvaluationWithUserRestrictionAtRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326", "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(restId);
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		HashMap Keys = rngHelper.createFeebieTDWithUserRestrictionAtRestaurantLevel("0", restId, freebieitemId);
		String tid = Keys.get("TDID").toString();


		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("100");
		a3.add("100");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();
		rngHelper.userMappingTD(userId2, tid);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId2);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId2);
		a5.add(rngConstants.AndroidOS);
		a5.add(rngConstants.versionCode229);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add(categoryId);
		a6.add(subCategoryId);
		a6.add(itemId);
		a6.add(count);
		a6.add("100");
		a6.add("100");
		a6.add(userId2);
		a6.add("true");
		a6.add(rngConstants.AndroidOS);
		a6.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid,freebieitemId, false }, { "menu", a2,tid,freebieitemId, false }, { "cart", a3,tid,freebieitemId, false },
				{ "list", a4,tid,freebieitemId, true }, { "menu", a5,tid,freebieitemId, true }, { "cart", a6,tid,freebieitemId, true } };

	}

	@DataProvider(name = "FreebieTD_EvaluationWithThirtyDayDormantAtRestaurantLevel")
	public Object[][] FreebieTD_EvaluationWithThirtyDayDormantAtRestaurantLevel() throws Exception {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326", "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(restId);
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		HashMap Keys = rngHelper.createFeebieTDWithThirtyDayDormantAtRestaurantLevel("0", restId, freebieitemId);
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("true");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId, "30");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("false");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tid,freebieitemId, false }, { "menu", a2,tid,freebieitemId, false },
				{ "cart", a3,tid,freebieitemId, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false } */ };

	}

	@DataProvider(name = "FreebieTD_EvaluationWithSixtyDayDormantAtRestaurantLevel")
	public Object[][] FreebieTD_EvaluationWithSixtyDayDormantAtRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326", "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(restId);
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		HashMap Keys = rngHelper.createFeebieTDWithSixtyDayDormantAtRestaurantLevel("0", restId, freebieitemId);
        String tid = Keys.get("TDID").toString();
		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("true");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId, "30");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("false");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tid,freebieitemId, false }, { "menu", a2,tid,freebieitemId, false },
				{ "cart", a3,tid,freebieitemId, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false } */ };

	}

	@DataProvider(name = "FreebieTD_EvaluationWithNinetyDayDormantAtRestaurantLevel")
	public Object[][] FreebieTD_EvaluationWithNinetyDayDormantAtRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9326", "77.6036");
		String freebieitemId = new Integer(rm.nextInt()).toString();
		rngHelper.disabledActiveTD(restId);
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		HashMap Keys = rngHelper.createFeebieTDWithNinetyDayDormantAtRestaurantLevel("0", restId, freebieitemId);
String tid = Keys.get("TDID").toString();
		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("true");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId, "30");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("false");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tid,freebieitemId, false }, { "menu", a2,tid,freebieitemId, false },
				{ "cart", a3,tid,freebieitemId, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false } */ };

	}

	@DataProvider(name = "cartEvaluationWithPercentageMinLimitAtRestaurant")
	public Object[][] cartEvaluationWithPercentageMinLimitAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List rlist = rngHelper.getRestaurantListing("12.9751", "77.6451", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		List<Map<String, Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);

		String itemId = itemDetails.get(0).get("item_id").toString();
		String categoryId = itemDetails.get(0).get("cat_id").toString();
		String subCategoryId = itemDetails.get(0).get("subcat_id").toString();
		String count = "1";
		String price = "200";
		String cartAmount = price;
		String minCartAmount = "100";
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false",
						rngConstants.AndroidOS, rngConstants.versionCode229, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, "30", userId, "false",
						rngConstants.AndroidOS, rngConstants.versionCode229, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode200, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.IosOS, rngConstants.versionCode200, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.IosOS, rngConstants.versionCode250, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.WebOS, rngConstants.versionCode250, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.WebOS, rngConstants.versionCode200, minCartAmount } };
	}

	@DataProvider(name = "cartEvaluationWithPercentageMinLimitSwiggyFirstAtRestaurant")
	public Object[][] cartEvaluationWithPercentageMinLimitSwiggyFirstAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").firstOrderRestriction(true).build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		List<Map<String, Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);

		String itemId = itemDetails.get(0).get("item_id").toString();
		String categoryId = itemDetails.get(0).get("cat_id").toString();
		String subCategoryId = itemDetails.get(0).get("subcat_id").toString();
		String count = "1";
		String price = "200";
		String cartAmount = price;
		String minCartAmount = "100";

		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, "10", userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false",
						rngConstants.AndroidOS, rngConstants.versionCode229, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.IosOS, rngConstants.versionCode200, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.WebOS, rngConstants.versionCode200, minCartAmount } };
	}

	@DataProvider(name = "cartEvaluationWithPercentageMinLimitRestaurantFirstAtRestaurant")
	public Object[][] cartEvaluationWithPercentageMinLimitRestaurantFirstAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").restaurantFirstOrder(true).build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		List<Map<String, Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);
		String itemId = itemDetails.get(0).get("item_id").toString();
		String categoryId = itemDetails.get(0).get("cat_id").toString();
		String subCategoryId = itemDetails.get(0).get("subcat_id").toString();
		String count = "1";
		String price = "200";
		String minCartAmount = price;

		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, "10", userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.IosOS, rngConstants.versionCode200 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.WebOS, rngConstants.versionCode200 } };
	}

	@DataProvider(name = "cartEvaluationWithPercentageMinLimitWithUserRestrictiontAtRestaurant")
	public Object[][] cartEvaluationWithPercentageMinLimitWithUserRestrictiontAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").userRestriction(true).build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		List<Map<String, Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);
		String itemId = itemDetails.get(0).get("item_id").toString();
		String categoryId = itemDetails.get(0).get("cat_id").toString();
		String subCategoryId = itemDetails.get(0).get("subcat_id").toString();
		String count = "1";
		String price = "200";
		String cartAmount = price;
		String minCartAmount = "100";
		return new Object[][] {
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229, createdTD_Id, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, "10", userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229, createdTD_Id, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false",
						rngConstants.AndroidOS, rngConstants.versionCode229, createdTD_Id, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.IosOS, rngConstants.versionCode200, createdTD_Id, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true",
						rngConstants.WebOS, rngConstants.versionCode200, createdTD_Id, minCartAmount } };
	}

	@DataProvider(name = "cartEvaluationWithPercentageMinLimitWithTimeSlotRestrictionAtRestaurant")
	public Object[][] cartEvaluationWithPercentageMinLimitWithTimeSlotRestrictionAtRestaurant() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		List<Slot> slots = new ArrayList<>();
		System.out.println(new SimpleDateFormat("HHmm").format(new Date()));
		slots.add(new Slot(new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), -2))));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").slots(slots)
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").timeSlotRestriction(true).build();
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		List<Map<String, Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);
		String itemId = itemDetails.get(0).get("item_id").toString();
		String categoryId = itemDetails.get(0).get("cat_id").toString();
		String subCategoryId = itemDetails.get(0).get("subcat_id").toString();
		String count = "1";
		String price = "200";
		String minCartAmount = price;
		return new Object[][] {
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, "10", userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "false",
						rngConstants.AndroidOS, rngConstants.versionCode229 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.IosOS, rngConstants.versionCode200 },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "true",
						rngConstants.WebOS, rngConstants.versionCode200 } };
	}

	@DataProvider(name = "cartEvalutateWithPercentageAtRestaurantWithDormantUser")
	public Object[][] cartEvalutateWithPercentageAtRestaurantWithDormantUser() throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		List<String> rlist = rngHelper.getRestaurantListing("12.9721", "77.6421", "0");
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		List<Slot> slots = new ArrayList<>();
		System.out.println(new SimpleDateFormat("HHmm").format(new Date()));
		slots.add(new Slot(new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), -2))));
		rngHelper.disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").slots(slots)
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").timeSlotRestriction(false)
				.dormant_user_type("THIRTY_DAYS_DORMANT").build();
		List<Map<String, Object>> itemDetails = cmsHelper.getItemDetails(randomRestaurant);
		String itemId = itemDetails.get(0).get("item_id").toString();
		String categoryId = itemDetails.get(0).get("cat_id").toString();
		String subCategoryId = itemDetails.get(0).get("subcat_id").toString();
		String count = "1";
		String price = "200";
		String carAmount = price;
		String minCartAmount = "100";
		Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		System.out
				.println("CreatTD ID " + "**************************************************************************");
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();
		return new Object[][] {
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, carAmount, userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, "10", userId, "true",
						rngConstants.AndroidOS, rngConstants.versionCode229, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, carAmount, userId, "false",
						rngConstants.AndroidOS, rngConstants.versionCode229, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, carAmount, userId, "true",
						rngConstants.IosOS, rngConstants.versionCode200, minCartAmount },
				{ randomRestaurant, categoryId, subCategoryId, itemId, count, price, carAmount, userId, "true",
						rngConstants.WebOS, rngConstants.versionCode200, minCartAmount } };
	}

	@DataProvider(name = "FlatTD_EvaluationWithNoMinCartAtRestaurantLevel")
	public Object[][] FlatTD_EvaluationWithNoMinCartAtRestaurantLevel() throws IOException {

		HashMap Keys = rngHelper.createFlatWithNoMinAmountAtRestaurantLevel("0", "200");
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(RngConstants.AndroidOS);
		a1.add(RngConstants.versionCode200);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(RngConstants.AndroidOS);
		a2.add(RngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("200");
		a3.add("200");
		a3.add(userId);
		a3.add("false");
		a3.add(RngConstants.AndroidOS);
		a3.add(RngConstants.versionCode200);

		return new Object[][] { { "list", a1,tid,true }, { "menu", a2,tid,true  }, { "cart", a3,tid,true  } };
	}

	@DataProvider(name = "FlatTD_EvaluationWithMinCartAtRestaurantLevel")
	public Object[][] FlatTD_EvaluationWithMinCartAtRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("500", "50");
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		ArrayList a3 = new ArrayList();

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add("200");
		a15.add("200");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add("500");
		a16.add("500");
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid,true }, { "list", a2,tid,true }, { "menu", a8,tid,true }, { "menu", a9,tid,true }, { "cart", a15,tid,false },
				{ "cart", a16,tid,true } };

	}

	@DataProvider(name = "FlatTD_EvaluationWithSwiggyFirstOrderRestaurantLevel")
	public Object[][] FlatTD_EvaluationWithSwiggyFirstOrderRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel("500", "50");
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add("500");
		a15.add("500");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add("500");
		a16.add("500");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, true }, { "menu", a8,tid, false },
				{ "menu", a9,tid, true }, { "cart", a15,tid, false }, { "cart", a16,tid, true } };

	}

	@DataProvider(name = "FlatTD_EvaluationWithRestaurantFirstOrderRestaurantLevel")
	public Object[][] FlatTD_EvaluationWithRestaurantFirstOrderRestaurantLevel() throws Exception {
		HashMap Keys = rngHelper.createFlatWithRestaurantFirstOrderRestrictionAtRestaurantLevel("500", "50");
		String restId = (String) Keys.get("restId");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();
		String tid = (String) Keys.get("TDID");

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add(500);
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();
		rngHelper.DormantUser(userId2, restId, "10");

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId2);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId2);
		a5.add(rngConstants.AndroidOS);
		a5.add(rngConstants.versionCode229);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add(categoryId);
		a6.add(subCategoryId);
		a6.add(itemId);
		a6.add(count);
		a6.add(500);
		a6.add("500");
		a6.add(userId2);
		a6.add("true");
		a6.add(rngConstants.AndroidOS);
		a6.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, true }, { "menu", a2,tid, true }, { "cart", a3,tid, true }, { "list", a4,tid, false },
				{ "menu", a5,tid, false }, { "cart", a6,tid, false } };

	}

	@DataProvider(name = "FlatTD_EvaluationWithTimeSlotRestrictionAtRestaurantLevel")
	public Object[][] FlatTD_EvaluationWithTimeSlotRestrictionAtRestaurantLevel()
			throws IOException, InterruptedException {
		HashMap Keys = rngHelper.createFlatWithTimeSlotRestrictionAtRestaurantLevel("500", "50");
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);
		/*
		 * Thread.sleep(20000);
		 * 
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("true");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */
		return new Object[][] { { "list", a1,tid, false }, { "menu", a2,tid, false },
				{ "cart", a3,tid,false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false} */ };

	}

	@DataProvider(name = "FlatTD_EvaluationWithUserRestrictionAtRestaurantLevel")
	public Object[][] FlatTD_EvaluationWithUserRestrictionAtRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithUserRestrictionAtRestaurantLevel("500", "50");
		String restId = (String) Keys.get("restId");
		String tId = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("true");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();
		rngHelper.userMappingTD(userId2, tId);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId2);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId2);
		a5.add(rngConstants.AndroidOS);
		a5.add(rngConstants.versionCode229);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add(categoryId);
		a6.add(subCategoryId);
		a6.add(itemId);
		a6.add(count);
		a6.add("500");
		a6.add("500");
		a6.add(userId2);
		a6.add("true");
		a6.add(rngConstants.AndroidOS);
		a6.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tId, false }, { "menu", a2,tId, false }, { "cart", a3,tId, false },
				{ "list", a4,tId, true }, { "menu", a5,tId, true }, { "cart", a6,tId, true } };

	}

	@DataProvider(name = "FlatTD_EvaluationWithThirtyDayDormantAtRestaurantLevel")
	public Object[][] FlatTD_EvaluationWithThirtyDayDormantAtRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithThirtyDayDormantAtRestaurantLevel("500", "50");
		String restId = (String) Keys.get("restId");
		String tId = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId, "30");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("false");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tId, false }, { "menu", a2,tId, false },
				{ "cart", a3,tId, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false } */ };

	}

	@DataProvider(name = "FlatTD_EvaluationWithSixtyDayDormantAtRestaurantLevel")
	public Object[][] FlatTD_EvaluationWithSixtyDayDormantAtRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithSixtyDayDormantAtRestaurantLevel("500", "50");
		String restId = (String) Keys.get("restId");
		String tId = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId, "60");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("false");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tId, false }, { "menu", a2,tId, false },
				{ "cart", a3,tId, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false } */ };

	}

	@DataProvider(name = "FlatTD_EvaluationWithNinetyDayDormantAtRestaurantLevel")
	public Object[][] FlatTD_EvaluationWithNinetyDayDormantAtRestaurantLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithNinetyDayDormantAtRestaurantLevel("500", "50");
		String restId = (String) Keys.get("restId");
		String tId = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		/*
		 * rngHelper.DormantUser(userId, restId, "60");
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("false");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */

		return new Object[][] { { "list", a1,tId, false }, { "menu", a2,tId, false },
				{ "cart", a3,tId, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false } */ };

	}

	@DataProvider(name = "EvaluationForSubCategoryLevel")
	public Object[][] EvaluationForSubCategoryLevel() throws IOException {
		String minCartAmount = "100";
		String maxDiscountAmount = "50.0";

		String randomRestaurant = new Integer(rm.nextInt()).toString();
//		List<Map<String, Object>> categoriesList = rngHelper.getCategoryForRestId(randomRestaurant);
//		String categoryId1 = categoriesList.get(1).get("cat_id").toString();
//		// List<Map<String, Object>> subCategoryList =
		// rngHelper.getSubCategoryForRestId(randomRestaurant, categoryId1);
		// if (subCategoryList.isEmpty() && subCategoryList.size() < 2) {
		// Assert.fail("Not able to find the Sub Categories " + subCategoryList);
		// return null;
		// }
		String categoryId1 = new Integer(rm.nextInt()).toString();
		String subCategoryId1 = new Integer(rm.nextInt()).toString();
		String subCategoryId2 = new Integer(rm.nextInt()).toString();

		List<Category> categories = new ArrayList<>();
		Category category = new Category();

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(new SubCategory(subCategoryId1, "test", new ArrayList<>()));

		category.setSubCategories(new ArrayList<>(subCategories));
		category.setId(categoryId1);
		category.setName("Testdsds");
		categories.add(category);
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test SubCategory", categories));
		rngHelper.disabledActiveTD(randomRestaurant);

		String tdId = rngHelper.createPercentageTDWithNoMinAmountAtSubCategoryLevel(restaurantLists1, minCartAmount, maxDiscountAmount);

		String itemDetailsTD = new Integer(rm.nextInt()).toString();
		String itemDetailsNonTD = new Integer(rm.nextInt()).toString();
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(randomRestaurant);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);


		List<ItemRequest> menulist1 = new ArrayList<>();
		menulist1.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", minCartAmount));

		List<ItemRequest> menulist2 = new ArrayList<>();
		menulist2.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", "50"));


		List<ItemRequest> menulist3 = new ArrayList<>();
		menulist3.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId2, itemDetailsTD, "1", minCartAmount));

		List<ItemRequest> menulist4 = new ArrayList<>();
		menulist4.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", minCartAmount));
		menulist4.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId2, itemDetailsTD, "1", minCartAmount));

		List<ItemRequest> menulist5 = new ArrayList<>();
		menulist5.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", "5000"));


MenuEvaluateRequest withTDItem_samePrice = new MenuEvaluateRequest(minCartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, menulist1);
MenuEvaluateRequest withTDItem_lowPrice = new MenuEvaluateRequest("20", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, menulist2);
MenuEvaluateRequest withTDItem_OtherCategory = new MenuEvaluateRequest("100", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, menulist3);
MenuEvaluateRequest withTDItem_MixedCategory = new MenuEvaluateRequest("200", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, menulist4);
MenuEvaluateRequest withTDItem_UpperCap = new MenuEvaluateRequest("5000", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, menulist5);




		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(randomRestaurant);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		List<ItemRequest> cartList1 = new ArrayList<>();
		cartList1.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", minCartAmount));
		CartEvaluateRequest cart_MinCartAmount = new CartEvaluateRequest(minCartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, cartList1);

		List<ItemRequest> cartList2 = new ArrayList<>();
		cartList2.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", "50"));
		CartEvaluateRequest cart_LowCartAmount = new CartEvaluateRequest("50", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, cartList2);

		List<ItemRequest> cartList3 = new ArrayList<>();
		cartList3.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", "200"));
		CartEvaluateRequest cart_HighCartAmount = new CartEvaluateRequest("200", userId, "true", rngConstants.AndroidOS, rngConstants.versionCode229, cartList3);

		List<ItemRequest> cartList4 = new ArrayList<>();
		cartList4.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "2", "50"));
		CartEvaluateRequest cart_MultiItems = new CartEvaluateRequest("100", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, cartList4);

		List<ItemRequest> cartList5 = new ArrayList<>();
		cartList5.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "6", "100"));
		CartEvaluateRequest cart_withCapping = new CartEvaluateRequest("600", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, cartList5);

		List<ItemRequest> cartList6 = new ArrayList<>();
		cartList6.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId2, itemDetailsNonTD, "1", minCartAmount));
		cartList6.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId2, itemDetailsNonTD, "1", minCartAmount));

		CartEvaluateRequest cart_withOtherCategory = new CartEvaluateRequest("200", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, cartList6);

		List<ItemRequest> cartList7 = new ArrayList<>();
		cartList7.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", "100"));
		cartList7.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId2, itemDetailsNonTD, "1", "200"));

		CartEvaluateRequest cartList7_mixedRequest = new CartEvaluateRequest("300", userId, "true", rngConstants.AndroidOS, rngConstants.versionCode229, cartList7);

		return new Object[][] {

				{ "list", a1, true, "20",tdId,true, true },

				{ "menu", withTDItem_samePrice, true, maxDiscountAmount,tdId,true, false },
				{ "menu", withTDItem_lowPrice, false, maxDiscountAmount,tdId,true, false },
				{ "menu", withTDItem_OtherCategory, true, maxDiscountAmount,tdId,false, false },
				{ "menu", withTDItem_MixedCategory, true, maxDiscountAmount,tdId, true,false },
				{ "menu", withTDItem_UpperCap, true, maxDiscountAmount,tdId,true, true },

                { "cart", cart_MinCartAmount, true, maxDiscountAmount,tdId,true, false },
				{ "cart", cart_LowCartAmount, false, maxDiscountAmount,tdId,true, false },
				{ "cart", cart_MultiItems, true, maxDiscountAmount,tdId,true, false },
				{ "cart", cart_withCapping, true, maxDiscountAmount,tdId,true, true },
				{ "cart", cart_withOtherCategory, true, maxDiscountAmount,tdId,false, false },
				{ "cart", cartList7_mixedRequest, true, maxDiscountAmount,tdId,true, false }


		};
	}

	@DataProvider(name = "FlatTDEvalutionAtSubCategoryLevel")
	public Object[][] FlatTDEvalutionAtSubCategoryLevel() throws IOException {
		String minCartAmount = "100";
		String flatTDAmount = "20";
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		String randomRestaurant = new Integer(rm.nextInt()).toString();
        rngHelper.disabledActiveTD(randomRestaurant);
		String categoryId1 = new Integer(rm.nextInt()).toString();

		String subCategoryId1 = new Integer(rm.nextInt()).toString();
		String subCategoryId2 = new Integer(rm.nextInt()).toString();

		List<Category> categories = new ArrayList<>();
		Category category = new Category();
		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(new SubCategory(subCategoryId1, new Integer(rm.nextInt()).toString(), new ArrayList<>()));
		category.setSubCategories(new ArrayList<>(subCategories));
		category.setId(categoryId1);
		category.setName("Testdsds");
		categories.add(category);

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test SubCategory", categories));
		String TdId = rngHelper.createFlatWithNoMinAmountAtSubCategoryLevel(restaurantLists1, minCartAmount,
				flatTDAmount);
		System.out.println("TD ID: " + TdId);
		// List<Map<String, Object>> itemDetailsTD = cmsHelper.getItem(randomRestaurant,
		// categoryId1, subCategoryId1);
		// List<Map<String, Object>> itemDetailsNonTD =
		// cmsHelper.getItem(randomRestaurant, categoryId1, subCategoryId2);
		//
		String userId = new Integer(rm.nextInt()).toString();
		String itemDetailsTD = new Integer(rm.nextInt()).toString();

		String itemDetailsNonTD = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(randomRestaurant);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(randomRestaurant);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		List<ItemRequest> list = new ArrayList<>();
		list.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", minCartAmount));
		CartEvaluateRequest cartEvaluateRequestMin = new CartEvaluateRequest(minCartAmount, userId, "false",
				rngConstants.AndroidOS, rngConstants.versionCode229, list);

		List<ItemRequest> list1 = new ArrayList<>();
		list1.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", "50"));
		CartEvaluateRequest cartEvaluatewithmin = new CartEvaluateRequest("50", userId, "false", rngConstants.AndroidOS,
				rngConstants.versionCode229, list1);

		List<ItemRequest> list2 = new ArrayList<>();
		list2.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "2", "4000"));
		list2.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId2, itemDetailsTD, "2", "4000"));

		CartEvaluateRequest cartEvaluateRequestMultiItem = new CartEvaluateRequest("4000", userId, "false",
				rngConstants.AndroidOS, rngConstants.versionCode229, list2);

		List<ItemRequest> list3 = new ArrayList<>();
		list3.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId2, itemDetailsNonTD, "2", minCartAmount));
		list3.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId2, itemDetailsNonTD, "2", minCartAmount));
		CartEvaluateRequest cartEvaluateRequestNonSubCategory = new CartEvaluateRequest("200", userId, "false",
				rngConstants.AndroidOS, rngConstants.versionCode229, list3);

		return new Object[][] { { "list", a1, true, flatTDAmount, true }, { "menu", a2, true, flatTDAmount, true },
				{ "cart", cartEvaluateRequestMin, true, flatTDAmount, true },
				{ "cart", cartEvaluatewithmin, false, flatTDAmount, true },
				{ "cart", cartEvaluateRequestMultiItem, true, flatTDAmount, true },
				{ "cart", cartEvaluateRequestNonSubCategory, true, flatTDAmount, false } };
	}

	@DataProvider(name = "FreeDeliveryTD_EvaluationWithNoMinCartAtRestaurantLevel")
	public Object[][] FreeDeliveryTD_EvaluationWithNoMinCartAtRestaurantLevel() throws IOException {

		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, false, false,
				"ZERO_DAYS_DORMANT",false);
		// String minCartAmount = Keys.get("minCartAmount").toString();
		String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,
																// rngConstants.password1).get("userId").toString().replace("\"",
																// "").trim();
		String tid = (String) Keys.get("TDID");

		String price = "500";
		String cartAmount = "500";
		String minCartAmount = "500";

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);
		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("99");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(cartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add(cartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);
		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(cartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);
		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(cartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);
		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(cartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);
		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(cartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);

		return new Object[][] { { "list", a1 ,tid,true}, { "list", a2,tid,true}, { "list", a3,tid,true }, { "list", a4,tid,true }, { "list", a5,tid,true },
				{ "list", a6,tid,true }, { "list", a7,tid,true }, { "menu", a8,tid,true }, { "menu", a9,tid,true }, { "menu", a10,tid,true }, { "menu", a11,tid,true },
				{ "menu", a12,tid,true }, { "menu", a13,tid,true }, { "menu", a14,tid,true }, { "cart", a15,tid,true }, { "cart", a16,tid,true }, { "cart", a17,tid,true },
				{ "cart", a18,tid,true }, { "cart", a19,tid,true }, { "cart", a20,tid,true }, { "cart", a21,tid,true }, { "cart", a22,tid,true } };
	}

	@DataProvider(name = "FreeDeliveryTD_EvaluationWithMinCartAtRestaurantLevel")
	public Object[][] FreeDeliveryTD_EvaluationWithMinCartAtRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,
																// rngConstants.password1).get("userId").toString().replace("\"",
																// "").trim();
		String price = "500";
		String cartAmount = "500";
		String minCartAmount = "500";
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99", restId, false, false,
				false, "ZERO_DAYS_DORMANT",false);
		String tid = (String) Keys.get("TDID");

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("0");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(cartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add(cartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(cartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);

		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(cartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);

		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(cartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);

		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(cartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);

		return new Object[][] { { "list", a1,tid, true }, { "list", a2,tid, true }, { "list", a3,tid, true }, { "list", a4,tid, true },
				{ "list", a5,tid, true }, { "list", a6,tid, true }, { "list", a7,tid, true }, { "menu", a8,tid, true },
				{ "menu", a9,tid, true }, { "menu", a10,tid, true }, { "menu", a11,tid, true }, { "menu", a12,tid, true },
				{ "menu", a13,tid, true }, { "menu", a14,tid, true }, { "cart", a15,tid, false }, { "cart", a16,tid, true },
				{ "cart", a17,tid, true }, { "cart", a18,tid, true }, { "cart", a19,tid, true }, { "cart", a20,tid, true },
				{ "cart", a21,tid, true }, { "cart", a22,tid, true } };

	}

	@DataProvider(name = "FreeDeliveryTD_EvaluationWithMinCartSwiggyFirstOrderRestaurantLevel")
	public Object[][] FreeDeliveryTD_EvaluationWithMinCartSwiggyFirstOrderRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,
																// rngConstants.password1).get("userId").toString().replace("\"",
																// "").trim();
		String price = "500";
		String CartAmount = "500";
		String minCartAmount = "500";
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, true, false, false,
				"ZERO_DAYS_DORMANT",false);
		String tid = (String) Keys.get("TDID");

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("0");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);

		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(CartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);

		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(CartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);

		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(CartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);

		return new Object[][] {
				{ "list", a1,tid, false }, { "list", a2,tid, true }, { "list", a3,tid, true }, { "list", a4,tid, true },
				{ "list", a5,tid, true }, { "list", a6,tid, true }, { "list", a7,tid, true },
				{ "menu", a8,tid, false },{ "menu", a9,tid, true }, { "menu", a10,tid, true }, { "menu", a11,tid, true }, { "menu", a12,tid, true },
				{ "menu", a13,tid, true }, { "menu", a14,tid, true },
				{ "cart", a15,tid, false }, { "cart", a16,tid, true },{ "cart", a19,tid, true }, { "cart", a20,tid, true }, { "cart", a21,tid, true }, { "cart", a22,tid, true } };

	}

	@DataProvider(name = "FreeDeliveryTD_EvaluationWithMinCartRestaurantFirstOrderRestaurantLevel")
	public Object[][] FreeDeliveryTD_EvaluationWithMinCartRestaurantFirstOrderRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,
																// rngConstants.password1).get("userId").toString().replace("\"",
																// "").trim();
		String userId2 = new Integer(rm.nextInt()).toString();
		rngHelper.DormantUser(userId2, restId, "10");
		String price = "500";
		String CartAmount = "500";
		String minCartAmount = "500";
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, true, false,
				"ZERO_DAYS_DORMANT",false);
		String tid = (String) Keys.get("TDID");

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add(price);
		a3.add("100");
		a3.add(userId);
		a3.add("true");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId2);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId2);
		a5.add(rngConstants.AndroidOS);
		a5.add(rngConstants.versionCode229);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add(categoryId);
		a6.add(subCategoryId);
		a6.add(itemId);
		a6.add(count);
		a6.add(price);
		a6.add("100");
		a6.add(userId2);
		a6.add("true");
		a6.add(rngConstants.AndroidOS);
		a6.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, true }, { "menu", a2,tid, true }, { "cart", a3,tid, true }, { "list", a4,tid, false },
				{ "menu", a5,tid, false }, { "cart", a6,tid, false }, };
	}

	@DataProvider(name = "FreeDeliveryTD_EvaluationWithMinCartWithUserRestrictionRestaurantLevel")
	public Object[][] FreeDeliveryTD_EvaluationWithMinCartWithUserRestrictionRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,
																// rngConstants.password1).get("userId").toString().replace("\"",
																// "").trim();

		String price = "500";
		String CartAmount = "500";
		String minCartAmount = "500";
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, false, true,
				"ZERO_DAYS_DORMANT",false);
		String userId2 = new Integer(rm.nextInt()).toString();
	String tid = 	Keys.get("TDID").toString();
		rngHelper.userMappingTD(userId2, tid);

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.WebOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("false");
		a5.add(userId);
		a5.add(rngConstants.AndroidOS);
		a5.add(rngConstants.versionCode229);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.AndroidOS);
		a6.add(rngConstants.versionCode229);

		ArrayList a7 = new ArrayList();
		a7.add(rngConstants.minCartAmount);
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add(categoryId);
		a8.add(subCategoryId);
		a8.add(itemId);
		a8.add(count);
		a8.add(price);
		a8.add("100");
		a8.add(userId);
		a8.add("true");
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add(categoryId);
		a9.add(subCategoryId);
		a9.add(itemId);
		a9.add(count);
		a9.add(price);
		a9.add(minCartAmount);
		a9.add(userId);
		a9.add("false");
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add(categoryId);
		a10.add(subCategoryId);
		a10.add(itemId);
		a10.add(count);
		a10.add(price);
		a10.add(CartAmount);
		a10.add(userId);
		a10.add("true");
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(restId);
		a11.add(categoryId);
		a11.add(subCategoryId);
		a11.add(itemId);
		a11.add(count);
		a11.add(price);
		a11.add(CartAmount);
		a11.add(userId);
		a11.add("true");
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode250);

		ArrayList a12 = new ArrayList();
		a12.add(restId);
		a12.add(categoryId);
		a12.add(subCategoryId);
		a12.add(itemId);
		a12.add(count);
		a12.add(price);
		a12.add(CartAmount);
		a12.add(userId);
		a12.add("true");
		a12.add(rngConstants.WebOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(restId);
		a13.add("false");
		a13.add(userId2);
		a13.add(rngConstants.AndroidOS);
		a13.add(rngConstants.versionCode229);

		ArrayList a14 = new ArrayList();
		a14.add(restId);
		a14.add("true");
		a14.add(userId2);
		a14.add(rngConstants.AndroidOS);
		a14.add(rngConstants.versionCode229);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add("true");
		a15.add(userId2);
		a15.add(rngConstants.IosOS);
		a15.add(rngConstants.versionCode250);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add("true");
		a16.add(userId2);
		a16.add(rngConstants.WebOS);
		a16.add(rngConstants.versionCode200);

		ArrayList a17 = new ArrayList();
		a17.add(rngConstants.minCartAmount);
		a17.add(restId);
		a17.add("false");
		a17.add(userId2);
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);

		ArrayList a18 = new ArrayList();
		a18.add(rngConstants.minCartAmount);
		a18.add(restId);
		a18.add("true");
		a18.add(userId2);
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(rngConstants.minCartAmount);
		a19.add(restId);
		a19.add("true");
		a19.add(userId2);
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode250);

		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add("100");
		a20.add(userId2);
		a20.add("false");
		a20.add(rngConstants.AndroidOS);
		a20.add(rngConstants.versionCode229);

		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(minCartAmount);
		a21.add(userId2);
		a21.add("false");
		a21.add(rngConstants.AndroidOS);
		a21.add(rngConstants.versionCode229);

		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(CartAmount);
		a22.add(userId2);
		a22.add("true");
		a22.add(rngConstants.AndroidOS);
		a22.add(rngConstants.versionCode200);

		ArrayList a23 = new ArrayList();
		a23.add(restId);
		a23.add(categoryId);
		a23.add(subCategoryId);
		a23.add(itemId);
		a23.add(count);
		a23.add(price);
		a23.add(CartAmount);
		a23.add(userId2);
		a23.add("true");
		a23.add(rngConstants.IosOS);
		a23.add(rngConstants.versionCode250);

		ArrayList a24 = new ArrayList();
		a24.add(restId);
		a24.add(categoryId);
		a24.add(subCategoryId);
		a24.add(itemId);
		a24.add(count);
		a24.add(price);
		a24.add(CartAmount);
		a24.add(userId2);
		a24.add("true");
		a24.add(rngConstants.WebOS);
		a24.add(rngConstants.versionCode250);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, false }, { "list", a3,tid, false },
				{ "list", a4,tid, false }, { "menu", a5,tid, false }, { "menu", a6,tid, false }, { "menu", a7,tid, false },
				{ "cart", a8,tid, false }, { "cart", a9,tid, false }, { "cart", a10,tid, false }, { "cart", a11,tid, false },
				{ "cart", a12,tid, false }, { "list", a13,tid, true }, { "list", a14,tid, true }, { "list", a15,tid, true },
				{ "list", a16,tid, true }, { "menu", a17,tid, true }, { "menu", a18,tid, true }, { "menu", a19,tid, true },
				{ "cart", a20,tid, true }, { "cart", a21,tid, true }, { "cart", a22,tid, true }, { "cart", a23,tid, true },
				{ "cart", a24,tid, true } };
	}

	@DataProvider(name = "FreeDeliveryTD_EvaluationWithMinCartWithThirtyDaysDormantRestaurantLevel")
	public Object[][] FreeDeliveryTD_EvaluationWithMinCartWithThirtyDaysDormantRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,
																// rngConstants.password1).get("userId").toString().replace("\"",
																// "").trim();
		String userId2 = new Integer(rm.nextInt()).toString();
		rngHelper.DormantUser(userId2, restId, "30");

		String price = "500";
		String CartAmount = "500";
		String minCartAmount = "500";
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, false, false,
				"THIRTY_DAYS_DORMANT",false);
		String tid = (String) Keys.get("TDID");

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);
		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);
		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);
		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);
		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);
		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);
		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);
		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);
		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);
		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);
		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);
		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("10");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);
		a15.add(minCartAmount);
		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		a16.add(minCartAmount);
		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		a17.add(minCartAmount);
		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add(CartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);
		a18.add(minCartAmount);
		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);
		a19.add(minCartAmount);
		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(CartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);
		a20.add(minCartAmount);
		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(CartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);
		a21.add(minCartAmount);
		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(CartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);
		a22.add(minCartAmount);

		rngHelper.DormantUser(userId2,restId,"30");

		ArrayList a23 = new ArrayList();
		a23.add(restId);
		a23.add("false");
		a23.add(userId2);
		a23.add(rngConstants.AndroidOS);
		a23.add(rngConstants.versionCode229);
		ArrayList a24 = new ArrayList();
		a24.add(restId);
		a24.add("true");
		a24.add(userId2);
		a24.add(rngConstants.AndroidOS);
		a24.add(rngConstants.versionCode229);
		ArrayList a25 = new ArrayList();
		a25.add(restId);
		a25.add("true");
		a25.add(userId2);
		a25.add(rngConstants.AndroidOS);
		a25.add(rngConstants.versionCode200);
		ArrayList a26 = new ArrayList();
		a26.add(restId);
		a26.add("true");
		a26.add(userId2);
		a26.add(rngConstants.IosOS);
		a26.add(rngConstants.versionCode200);
		ArrayList a27 = new ArrayList();
		a27.add(restId);
		a27.add("true");
		a27.add(userId2);
		a27.add(rngConstants.IosOS);
		a27.add(rngConstants.versionCode250);
		ArrayList a28 = new ArrayList();
		a28.add(restId);
		a28.add("true");
		a28.add(userId2);
		a28.add(rngConstants.WebOS);
		a28.add(rngConstants.versionCode200);
		ArrayList a29 = new ArrayList();
		a29.add(restId);
		a29.add("true");
		a29.add(userId2);
		a29.add(rngConstants.IosOS);
		a29.add(rngConstants.versionCode250);

		ArrayList a30 = new ArrayList();
		a30.add(rngConstants.minCartAmount);
		a30.add(restId);
		a30.add("false");
		a30.add(userId2);
		a30.add(rngConstants.AndroidOS);
		a30.add(rngConstants.versionCode229);
		ArrayList a31 = new ArrayList();
		a31.add(rngConstants.minCartAmount);
		a31.add(restId);
		a31.add("true");
		a31.add(userId2);
		a31.add(rngConstants.AndroidOS);
		a31.add(rngConstants.versionCode229);
		ArrayList a32 = new ArrayList();
		a32.add(rngConstants.minCartAmount);
		a32.add(restId);
		a32.add("true");
		a32.add(userId2);
		a32.add(rngConstants.AndroidOS);
		a32.add(rngConstants.versionCode200);
		ArrayList a33 = new ArrayList();
		a33.add(rngConstants.minCartAmount);
		a33.add(restId);
		a33.add("true");
		a33.add(userId2);
		a33.add(rngConstants.IosOS);
		a33.add(rngConstants.versionCode200);
		ArrayList a34 = new ArrayList();
		a34.add(rngConstants.minCartAmount);
		a34.add(restId);
		a34.add("true");
		a34.add(userId2);
		a34.add(rngConstants.IosOS);
		a34.add(rngConstants.versionCode250);
		ArrayList a35 = new ArrayList();
		a35.add(rngConstants.minCartAmount);
		a35.add(restId);
		a35.add("true");
		a35.add(userId2);
		a35.add(rngConstants.WebOS);
		a35.add(rngConstants.versionCode200);
		ArrayList a36 = new ArrayList();
		a36.add(rngConstants.minCartAmount);
		a36.add(restId);
		a36.add("true");
		a36.add(userId2);
		a36.add(rngConstants.IosOS);
		a36.add(rngConstants.versionCode250);

		ArrayList a37 = new ArrayList();
		a37.add(restId);
		a37.add(categoryId);
		a37.add(subCategoryId);
		a37.add(itemId);
		a37.add(count);
		a37.add(price);
		a37.add("100");
		a37.add(userId2);
		a37.add("true");
		a37.add(rngConstants.AndroidOS);
		a37.add(rngConstants.versionCode229);
		a37.add(minCartAmount);
		ArrayList a38 = new ArrayList();
		a38.add(restId);
		a38.add(categoryId);
		a38.add(subCategoryId);
		a38.add(itemId);
		a38.add(count);
		a38.add(price);
		a38.add(minCartAmount);
		a38.add(userId2);
		a38.add("false");
		a38.add(rngConstants.AndroidOS);
		a38.add(rngConstants.versionCode229);
		a38.add(minCartAmount);
		ArrayList a39 = new ArrayList();
		a39.add(restId);
		a39.add(categoryId);
		a39.add(subCategoryId);
		a39.add(itemId);
		a39.add(count);
		a39.add(price);
		a39.add(CartAmount);
		a39.add(userId2);
		a39.add("true");
		a39.add(rngConstants.AndroidOS);
		a39.add(rngConstants.versionCode229);
		a39.add(minCartAmount);
		ArrayList a40 = new ArrayList();
		a40.add(restId);
		a40.add(categoryId);
		a40.add(subCategoryId);
		a40.add(itemId);
		a40.add(count);
		a40.add(price);
		a40.add(CartAmount);
		a40.add(userId2);
		a40.add("true");
		a40.add(rngConstants.AndroidOS);
		a40.add(rngConstants.versionCode200);
		a40.add(minCartAmount);
		ArrayList a41 = new ArrayList();
		a41.add(restId);
		a41.add(categoryId);
		a41.add(subCategoryId);
		a41.add(itemId);
		a41.add(count);
		a41.add(price);
		a41.add(CartAmount);
		a41.add(userId2);
		a41.add("true");
		a41.add(rngConstants.IosOS);
		a41.add(rngConstants.versionCode200);
		a41.add(minCartAmount);
		ArrayList a42 = new ArrayList();
		a42.add(restId);
		a42.add(categoryId);
		a42.add(subCategoryId);
		a42.add(itemId);
		a42.add(count);
		a42.add(price);
		a42.add(CartAmount);
		a42.add(userId2);
		a42.add("true");
		a42.add(rngConstants.IosOS);
		a42.add(rngConstants.versionCode250);
		a42.add(minCartAmount);
		ArrayList a43 = new ArrayList();
		a43.add(restId);
		a43.add(categoryId);
		a43.add(subCategoryId);
		a43.add(itemId);
		a43.add(count);
		a43.add(price);
		a43.add(CartAmount);
		a43.add(userId2);
		a43.add("true");
		a43.add(rngConstants.WebOS);
		a43.add(rngConstants.versionCode250);
		a43.add(minCartAmount);
		ArrayList a44 = new ArrayList();
		a44.add(restId);
		a44.add(categoryId);
		a44.add(subCategoryId);
		a44.add(itemId);
		a44.add(count);
		a44.add(price);
		a44.add(CartAmount);
		a44.add(userId2);
		a44.add("true");
		a44.add(rngConstants.WebOS);
		a44.add(rngConstants.versionCode200);
		a44.add(minCartAmount);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, false }, { "list", a3,tid, false },
				{ "list", a4,tid, false }, { "list", a5,tid, false }, { "list", a6,tid, false }, { "list", a7,tid, false },
				{ "menu", a8,tid, false }, { "menu", a9,tid, false }, { "menu", a10,tid, false }, { "menu", a11,tid, false },
				{ "menu", a12,tid, false }, { "menu", a13,tid, false }, { "menu", a14,tid, false }, { "cart", a15,tid, false },
				{ "cart", a16,tid, false }, { "cart", a17,tid, false }, { "cart", a18,tid, false }, { "cart", a19,tid, false },
				{ "cart", a20,tid, false }, { "cart", a21,tid, false }, { "cart", a22,tid, false }, { "list", a23,tid, true },
				{ "list", a24,tid, true }, { "list", a25,tid, true }, { "list", a26,tid, true }, { "list", a27,tid, true },
				{ "list", a28,tid, true }, { "list", a29,tid, true }, { "menu", a30,tid, true }, { "menu", a31,tid, true },
				{ "menu", a32,tid, true }, { "menu", a33,tid, true }, { "menu", a34,tid, true }, { "menu", a35,tid, true },
				{ "menu", a36,tid, true }, { "cart", a37,tid, true }, { "cart", a38,tid, true }, { "cart", a39,tid, true },
				{ "cart", a40,tid, true }, { "cart", a41,tid, true }, { "cart", a42,tid, true }, { "cart", a43,tid, true },
				{ "cart", a44,tid, true } };
	}

	@DataProvider(name = "FreeDeliveryTD_RestaurantBillAmount")
	public Object[][] FreeDeliveryTD_RestaurantBillAmount() throws IOException {

		SuperMultiTDHelper helper =new SuperMultiTDHelper();
		String restId = helper.getFreshRestId();
		String freedelMinCart = "99.0";
      	HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(freedelMinCart, restId, false, false, false,
				"ZERO_DAYS_DORMANT",false);
		String tid = (String) Keys.get("TDID");

        double freeDelMincartAmtLongVal = Double.parseDouble(freedelMinCart);
        int freeDelMincartAmt = (int) (double) freeDelMincartAmtLongVal;
		return new Object[][] {
				{ Integer.parseInt(restId),freeDelMincartAmt,freeDelMincartAmt-1, freeDelMincartAmt},
				{ Integer.parseInt(restId),freeDelMincartAmt,freeDelMincartAmt,freeDelMincartAmt },
				{ Integer.parseInt(restId),freeDelMincartAmt,freeDelMincartAmt+1,freeDelMincartAmt },
				{ Integer.parseInt(restId),freeDelMincartAmt+1,freeDelMincartAmt-2,freeDelMincartAmt },
				{ Integer.parseInt(restId),freeDelMincartAmt+1,freeDelMincartAmt+1,freeDelMincartAmt }};

	}

	@DataProvider(name = "FreeBie_RestaurantBillAmount")
	public Object[][] FreeBie_RestaurantBillAmount() throws IOException {
		SuperMultiTDHelper helper =new SuperMultiTDHelper();
		String restId = helper.getFreshRestId();
		String freedbieMinCart = "99.0";
		rngHelper.createFreebieTDWithMinAmountAtRestaurantLevel(freedbieMinCart,restId,"123");

		double freeBieMincartAmtLongVal = Double.parseDouble(freedbieMinCart);
		int freeBieMincartAmt = (int) (double) freeBieMincartAmtLongVal;
		return new Object[][] {
				{ Integer.parseInt(restId),freeBieMincartAmt,freeBieMincartAmt-1, freeBieMincartAmt},
				{ Integer.parseInt(restId),freeBieMincartAmt,freeBieMincartAmt,freeBieMincartAmt },
				{ Integer.parseInt(restId),freeBieMincartAmt,freeBieMincartAmt+1,freeBieMincartAmt },
				{ Integer.parseInt(restId),freeBieMincartAmt+1,freeBieMincartAmt-2,freeBieMincartAmt },
				{ Integer.parseInt(restId),freeBieMincartAmt+1,freeBieMincartAmt+1,freeBieMincartAmt }};

	}


		@DataProvider(name = "FreeDeliveryTD_EvaluationWithMinCartWithSixtyDaysDormantRestaurantLevel")
	public Object[][] FreeDeliveryTD_EvaluationWithMinCartWithSixtyDaysDormantRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,
																// rngConstants.password1).get("userId").toString().replace("\"",
																// "").trim();
		String userId2 = new Integer(rm.nextInt()).toString();
		rngHelper.DormantUser(userId2, restId, "60");

		String price = "500";
		String CartAmount = "500";
		String minCartAmount = "500";
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, false, false,
				"SIXTY_DAYS_DORMANT",false);
		String tid = (String) Keys.get("TDID");

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);
		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);
		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);
		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);
		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);
		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);
		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);
		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);
		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);
		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);
		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);
		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("10");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);
		a15.add(minCartAmount);
		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		a16.add(minCartAmount);
		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		a17.add(minCartAmount);
		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add(CartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);
		a18.add(minCartAmount);
		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);
		a19.add(minCartAmount);
		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(CartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);
		a20.add(minCartAmount);
		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(CartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);
		a21.add(minCartAmount);
		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(CartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);
		a22.add(minCartAmount);

		rngHelper.DormantUser(userId2,restId,"60");

		ArrayList a23 = new ArrayList();
		a23.add(restId);
		a23.add("false");
		a23.add(userId2);
		a23.add(rngConstants.AndroidOS);
		a23.add(rngConstants.versionCode229);
		ArrayList a24 = new ArrayList();
		a24.add(restId);
		a24.add("true");
		a24.add(userId2);
		a24.add(rngConstants.AndroidOS);
		a24.add(rngConstants.versionCode229);
		ArrayList a25 = new ArrayList();
		a25.add(restId);
		a25.add("true");
		a25.add(userId2);
		a25.add(rngConstants.AndroidOS);
		a25.add(rngConstants.versionCode200);
		ArrayList a26 = new ArrayList();
		a26.add(restId);
		a26.add("true");
		a26.add(userId2);
		a26.add(rngConstants.IosOS);
		a26.add(rngConstants.versionCode200);
		ArrayList a27 = new ArrayList();
		a27.add(restId);
		a27.add("true");
		a27.add(userId2);
		a27.add(rngConstants.IosOS);
		a27.add(rngConstants.versionCode250);
		ArrayList a28 = new ArrayList();
		a28.add(restId);
		a28.add("true");
		a28.add(userId2);
		a28.add(rngConstants.WebOS);
		a28.add(rngConstants.versionCode200);
		ArrayList a29 = new ArrayList();
		a29.add(restId);
		a29.add("true");
		a29.add(userId2);
		a29.add(rngConstants.IosOS);
		a29.add(rngConstants.versionCode250);

		ArrayList a30 = new ArrayList();
		a30.add(rngConstants.minCartAmount);
		a30.add(restId);
		a30.add("false");
		a30.add(userId2);
		a30.add(rngConstants.AndroidOS);
		a30.add(rngConstants.versionCode229);
		ArrayList a31 = new ArrayList();
		a31.add(rngConstants.minCartAmount);
		a31.add(restId);
		a31.add("true");
		a31.add(userId2);
		a31.add(rngConstants.AndroidOS);
		a31.add(rngConstants.versionCode229);
		ArrayList a32 = new ArrayList();
		a32.add(rngConstants.minCartAmount);
		a32.add(restId);
		a32.add("true");
		a32.add(userId2);
		a32.add(rngConstants.AndroidOS);
		a32.add(rngConstants.versionCode200);
		ArrayList a33 = new ArrayList();
		a33.add(rngConstants.minCartAmount);
		a33.add(restId);
		a33.add("true");
		a33.add(userId2);
		a33.add(rngConstants.IosOS);
		a33.add(rngConstants.versionCode200);
		ArrayList a34 = new ArrayList();
		a34.add(rngConstants.minCartAmount);
		a34.add(restId);
		a34.add("true");
		a34.add(userId2);
		a34.add(rngConstants.IosOS);
		a34.add(rngConstants.versionCode250);
		ArrayList a35 = new ArrayList();
		a35.add(rngConstants.minCartAmount);
		a35.add(restId);
		a35.add("true");
		a35.add(userId2);
		a35.add(rngConstants.WebOS);
		a35.add(rngConstants.versionCode200);
		ArrayList a36 = new ArrayList();
		a36.add(rngConstants.minCartAmount);
		a36.add(restId);
		a36.add("true");
		a36.add(userId2);
		a36.add(rngConstants.IosOS);
		a36.add(rngConstants.versionCode250);

		ArrayList a37 = new ArrayList();
		a37.add(restId);
		a37.add(categoryId);
		a37.add(subCategoryId);
		a37.add(itemId);
		a37.add(count);
		a37.add(price);
		a37.add("100");
		a37.add(userId2);
		a37.add("false");
		a37.add(rngConstants.AndroidOS);
		a37.add(rngConstants.versionCode229);
		a37.add(minCartAmount);
		ArrayList a38 = new ArrayList();
		a38.add(restId);
		a38.add(categoryId);
		a38.add(subCategoryId);
		a38.add(itemId);
		a38.add(count);
		a38.add(price);
		a38.add(minCartAmount);
		a38.add(userId2);
		a38.add("false");
		a38.add(rngConstants.AndroidOS);
		a38.add(rngConstants.versionCode229);
		a38.add(minCartAmount);
		ArrayList a39 = new ArrayList();
		a39.add(restId);
		a39.add(categoryId);
		a39.add(subCategoryId);
		a39.add(itemId);
		a39.add(count);
		a39.add(price);
		a39.add(CartAmount);
		a39.add(userId2);
		a39.add("true");
		a39.add(rngConstants.AndroidOS);
		a39.add(rngConstants.versionCode229);
		a39.add(minCartAmount);
		ArrayList a40 = new ArrayList();
		a40.add(restId);
		a40.add(categoryId);
		a40.add(subCategoryId);
		a40.add(itemId);
		a40.add(count);
		a40.add(price);
		a40.add(CartAmount);
		a40.add(userId2);
		a40.add("true");
		a40.add(rngConstants.AndroidOS);
		a40.add(rngConstants.versionCode200);
		a40.add(minCartAmount);
		ArrayList a41 = new ArrayList();
		a41.add(restId);
		a41.add(categoryId);
		a41.add(subCategoryId);
		a41.add(itemId);
		a41.add(count);
		a41.add(price);
		a41.add(CartAmount);
		a41.add(userId2);
		a41.add("true");
		a41.add(rngConstants.IosOS);
		a41.add(rngConstants.versionCode200);
		a41.add(minCartAmount);
		ArrayList a42 = new ArrayList();
		a42.add(restId);
		a42.add(categoryId);
		a42.add(subCategoryId);
		a42.add(itemId);
		a42.add(count);
		a42.add(price);
		a42.add(CartAmount);
		a42.add(userId2);
		a42.add("true");
		a42.add(rngConstants.IosOS);
		a42.add(rngConstants.versionCode250);
		a42.add(minCartAmount);
		ArrayList a43 = new ArrayList();
		a43.add(restId);
		a43.add(categoryId);
		a43.add(subCategoryId);
		a43.add(itemId);
		a43.add(count);
		a43.add(price);
		a43.add(CartAmount);
		a43.add(userId2);
		a43.add("true");
		a43.add(rngConstants.WebOS);
		a43.add(rngConstants.versionCode250);
		a43.add(minCartAmount);
		ArrayList a44 = new ArrayList();
		a44.add(restId);
		a44.add(categoryId);
		a44.add(subCategoryId);
		a44.add(itemId);
		a44.add(count);
		a44.add(price);
		a44.add(CartAmount);
		a44.add(userId2);
		a44.add("true");
		a44.add(rngConstants.WebOS);
		a44.add(rngConstants.versionCode200);
		a44.add(minCartAmount);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, false }, { "list", a3,tid, false },
				{ "list", a4,tid, false }, { "list", a5,tid, false }, { "list", a6,tid, false }, { "list", a7,tid, false },
				{ "menu", a8,tid, false }, { "menu", a9,tid, false }, { "menu", a10,tid, false }, { "menu", a11,tid, false },
				{ "menu", a12,tid, false }, { "menu", a13,tid, false }, { "menu", a14,tid, false }, { "cart", a15,tid, false },
				{ "cart", a16,tid, false }, { "cart", a17,tid, false }, { "cart", a18,tid, false }, { "cart", a19,tid, false },
				{ "cart", a20,tid, false }, { "cart", a21,tid, false }, { "cart", a22,tid, false }, { "list", a23,tid, true },
				{ "list", a24,tid, true }, { "list", a25,tid, true }, { "list", a26,tid, true }, { "list", a27,tid, true },
				{ "list", a28,tid, true }, { "list", a29,tid, true }, { "menu", a30,tid, true }, { "menu", a31,tid, true },
				{ "menu", a32,tid, true }, { "menu", a33,tid, true }, { "menu", a34,tid, true }, { "menu", a35,tid, true },
				{ "menu", a36,tid, true }, { "cart", a37,tid, true }, { "cart", a38,tid, true }, { "cart", a39,tid, true },
				{ "cart", a40,tid, true }, { "cart", a41,tid, true }, { "cart", a42,tid, true }, { "cart", a43,tid, true },
				{ "cart", a44,tid, true } };
	}

	@DataProvider(name = "FreeDeliveryTD_EvaluationWithMinCartWithNINETYDaysDormantRestaurantLevel")
	public Object[][] FreeDeliveryTD_EvaluationWithMinCartWithNINETYDaysDormantRestaurantLevel() throws IOException {
		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,
																// rngConstants.password1).get("userId").toString().replace("\"",
																// "").trim();
		String userId2 = new Integer(rm.nextInt()).toString();
		rngHelper.DormantUser(userId2, restId, "90");

		String price = "500";
		String CartAmount = "500";
		String minCartAmount = "500";
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("0", restId, false, false, false,
				"NINETY_DAYS_DORMANT",false);
		String createTdId = (String) Keys.get("TDID");

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);
		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);
		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);
		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);
		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);
		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);
		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);
		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);
		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);
		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);
		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);
		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("100");
		a15.add(userId);
		a15.add("true");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);
		a15.add(minCartAmount);
		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		a16.add(minCartAmount);
		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		a17.add(minCartAmount);
		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add(CartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);
		a18.add(minCartAmount);
		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);
		a19.add(minCartAmount);
		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(CartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);
		a20.add(minCartAmount);
		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(CartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);
		a21.add(minCartAmount);
		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(CartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);
		a22.add(minCartAmount);

		rngHelper.DormantUser(userId2,restId,"90");

		ArrayList a23 = new ArrayList();
		a23.add(restId);
		a23.add("false");
		a23.add(userId2);
		a23.add(rngConstants.AndroidOS);
		a23.add(rngConstants.versionCode229);
		ArrayList a24 = new ArrayList();
		a24.add(restId);
		a24.add("true");
		a24.add(userId2);
		a24.add(rngConstants.AndroidOS);
		a24.add(rngConstants.versionCode229);
		ArrayList a25 = new ArrayList();
		a25.add(restId);
		a25.add("true");
		a25.add(userId2);
		a25.add(rngConstants.AndroidOS);
		a25.add(rngConstants.versionCode200);
		ArrayList a26 = new ArrayList();
		a26.add(restId);
		a26.add("true");
		a26.add(userId2);
		a26.add(rngConstants.IosOS);
		a26.add(rngConstants.versionCode200);
		ArrayList a27 = new ArrayList();
		a27.add(restId);
		a27.add("true");
		a27.add(userId2);
		a27.add(rngConstants.IosOS);
		a27.add(rngConstants.versionCode250);
		ArrayList a28 = new ArrayList();
		a28.add(restId);
		a28.add("true");
		a28.add(userId2);
		a28.add(rngConstants.WebOS);
		a28.add(rngConstants.versionCode200);
		ArrayList a29 = new ArrayList();
		a29.add(restId);
		a29.add("true");
		a29.add(userId2);
		a29.add(rngConstants.IosOS);
		a29.add(rngConstants.versionCode250);

		ArrayList a30 = new ArrayList();
		a30.add(rngConstants.minCartAmount);
		a30.add(restId);
		a30.add("false");
		a30.add(userId2);
		a30.add(rngConstants.AndroidOS);
		a30.add(rngConstants.versionCode229);
		ArrayList a31 = new ArrayList();
		a31.add(rngConstants.minCartAmount);
		a31.add(restId);
		a31.add("true");
		a31.add(userId2);
		a31.add(rngConstants.AndroidOS);
		a31.add(rngConstants.versionCode229);
		ArrayList a32 = new ArrayList();
		a32.add(rngConstants.minCartAmount);
		a32.add(restId);
		a32.add("true");
		a32.add(userId2);
		a32.add(rngConstants.AndroidOS);
		a32.add(rngConstants.versionCode200);
		ArrayList a33 = new ArrayList();
		a33.add(rngConstants.minCartAmount);
		a33.add(restId);
		a33.add("true");
		a33.add(userId2);
		a33.add(rngConstants.IosOS);
		a33.add(rngConstants.versionCode200);
		ArrayList a34 = new ArrayList();
		a34.add(rngConstants.minCartAmount);
		a34.add(restId);
		a34.add("true");
		a34.add(userId2);
		a34.add(rngConstants.IosOS);
		a34.add(rngConstants.versionCode250);
		ArrayList a35 = new ArrayList();
		a35.add(rngConstants.minCartAmount);
		a35.add(restId);
		a35.add("true");
		a35.add(userId2);
		a35.add(rngConstants.WebOS);
		a35.add(rngConstants.versionCode200);
		ArrayList a36 = new ArrayList();
		a36.add(rngConstants.minCartAmount);
		a36.add(restId);
		a36.add("true");
		a36.add(userId2);
		a36.add(rngConstants.IosOS);
		a36.add(rngConstants.versionCode250);

		ArrayList a37 = new ArrayList();
		a37.add(restId);
		a37.add(categoryId);
		a37.add(subCategoryId);
		a37.add(itemId);
		a37.add(count);
		a37.add(price);
		a37.add("100");
		a37.add(userId2);
		a37.add("false");
		a37.add(rngConstants.AndroidOS);
		a37.add(rngConstants.versionCode229);
		a37.add(minCartAmount);
		ArrayList a38 = new ArrayList();
		a38.add(restId);
		a38.add(categoryId);
		a38.add(subCategoryId);
		a38.add(itemId);
		a38.add(count);
		a38.add(price);
		a38.add(minCartAmount);
		a38.add(userId2);
		a38.add("false");
		a38.add(rngConstants.AndroidOS);
		a38.add(rngConstants.versionCode229);
		a38.add(minCartAmount);
		ArrayList a39 = new ArrayList();
		a39.add(restId);
		a39.add(categoryId);
		a39.add(subCategoryId);
		a39.add(itemId);
		a39.add(count);
		a39.add(price);
		a39.add(CartAmount);
		a39.add(userId2);
		a39.add("true");
		a39.add(rngConstants.AndroidOS);
		a39.add(rngConstants.versionCode229);
		a39.add(minCartAmount);
		ArrayList a40 = new ArrayList();
		a40.add(restId);
		a40.add(categoryId);
		a40.add(subCategoryId);
		a40.add(itemId);
		a40.add(count);
		a40.add(price);
		a40.add(CartAmount);
		a40.add(userId2);
		a40.add("true");
		a40.add(rngConstants.AndroidOS);
		a40.add(rngConstants.versionCode200);
		a40.add(minCartAmount);
		ArrayList a41 = new ArrayList();
		a41.add(restId);
		a41.add(categoryId);
		a41.add(subCategoryId);
		a41.add(itemId);
		a41.add(count);
		a41.add(price);
		a41.add(CartAmount);
		a41.add(userId2);
		a41.add("true");
		a41.add(rngConstants.IosOS);
		a41.add(rngConstants.versionCode200);
		a41.add(minCartAmount);
		ArrayList a42 = new ArrayList();
		a42.add(restId);
		a42.add(categoryId);
		a42.add(subCategoryId);
		a42.add(itemId);
		a42.add(count);
		a42.add(price);
		a42.add(CartAmount);
		a42.add(userId2);
		a42.add("true");
		a42.add(rngConstants.IosOS);
		a42.add(rngConstants.versionCode250);
		a42.add(minCartAmount);
		ArrayList a43 = new ArrayList();
		a43.add(restId);
		a43.add(categoryId);
		a43.add(subCategoryId);
		a43.add(itemId);
		a43.add(count);
		a43.add(price);
		a43.add(CartAmount);
		a43.add(userId2);
		a43.add("true");
		a43.add(rngConstants.WebOS);
		a43.add(rngConstants.versionCode250);
		a43.add(minCartAmount);
		ArrayList a44 = new ArrayList();
		a44.add(restId);
		a44.add(categoryId);
		a44.add(subCategoryId);
		a44.add(itemId);
		a44.add(count);
		a44.add(price);
		a44.add(CartAmount);
		a44.add(userId2);
		a44.add("true");
		a44.add(rngConstants.WebOS);
		a44.add(rngConstants.versionCode200);
		a44.add(minCartAmount);

		return new Object[][] { { "list", a1,createTdId, false }, { "list", a2,createTdId, false }, { "list", a3,createTdId, false },
				{ "list", a4,createTdId, false }, { "list", a5,createTdId, false }, { "list", a6,createTdId, false }, { "list", a7,createTdId, false },
				{ "menu", a8,createTdId, false }, { "menu", a9,createTdId, false }, { "menu", a10,createTdId, false }, { "menu", a11,createTdId, false },
				{ "menu", a12, createTdId,false }, { "menu", a13,createTdId, false }, { "menu", a14,createTdId, false }, { "cart", a15,createTdId, false },
				{ "cart", a16, createTdId,false }, { "cart", a17,createTdId, false }, { "cart", a18,createTdId, false }, { "cart", a19,createTdId, false },
				{ "cart", a20,createTdId, false }, { "cart", a21,createTdId, false }, { "cart", a22,createTdId, false }, { "list", a23,createTdId, true },
				{ "list", a24,createTdId, true }, { "list", a25,createTdId, true }, { "list", a26,createTdId, true }, { "list", a27,createTdId, true },
				{ "list", a28,createTdId, true }, { "list", a29,createTdId, true }, { "menu", a30,createTdId, true }, { "menu", a31,createTdId, true },
				{ "menu", a32,createTdId, true }, { "menu", a33,createTdId, true }, { "menu", a34,createTdId, true }, { "menu", a35,createTdId, true },
				{ "menu", a36,createTdId, true }, { "cart", a37,createTdId, true }, { "cart", a38,createTdId, true }, { "cart", a39,createTdId, true },
				{ "cart", a40,createTdId, true }, { "cart", a41,createTdId, true }, { "cart", a42,createTdId, true }, { "cart", a43,createTdId, true },
				{ "cart", a44,createTdId, true } };
	}

	// @DataProvider(name =
	// "FreeDeliveryTD_EvaluationWithMinCartWithTimeSlotRestrictionRestaurantLevel")
	// public Object[][]
	// FreeDeliveryTD_EvaluationWithMinCartWithTimeSlotRestrictionRestaurantLevel()
	// throws IOException {
	// String restId = new
	// Integer(rm.nextInt()).toString();//rngHelper.getRandomRestaurant("12.9719",
	// "77.6412");
	// String itemId = new
	// Integer(rm.nextInt()).toString();//rngHelper.getItemDetails(restId).get(0).toString();
	// String categoryId = new
	// Integer(rm.nextInt()).toString();//rngHelper.getItemDetails(restId).get(1).toString();
	// String subCategoryId = new
	// Integer(rm.nextInt()).toString();//rngHelper.getItemDetails(restId).get(2).toString();
	// String count = "1";//rngHelper.getItemDetails(restId).get(3).toString();
	// String userId = new
	// Integer(rm.nextInt()).toString();//checkoutHelper.TokenData(rngConstants.mobile1,
	// rngConstants.password1).get("userId").toString().replace("\"", "").trim();
	// String userId2 = new Integer(rm.nextInt()).toString();
	// rngHelper.DormantUser(userId2,restId,"90");
	//
	// String price = "500";
	// String CartAmount = "500";
	// String minCartAmount = "500";
	// HashMap Keys =
	// rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("0", restId,
	// false, false,false, "ZERO_DAYS_DORMANT");
	// String createTdId = (String) Keys.get("TDID");
	//
	// ArrayList a1 = new ArrayList();
	// a1.add(restId);
	// a1.add("false");
	// a1.add(userId);
	// a1.add(rngConstants.AndroidOS);
	// a1.add(rngConstants.versionCode229);
	// ArrayList a2 = new ArrayList();
	// a2.add(restId);
	// a2.add("true");
	// a2.add(userId);
	// a2.add(rngConstants.AndroidOS);
	// a2.add(rngConstants.versionCode229);
	// ArrayList a3 = new ArrayList();
	// a3.add(restId);
	// a3.add("true");
	// a3.add(userId);
	// a3.add(rngConstants.AndroidOS);
	// a3.add(rngConstants.versionCode200);
	// ArrayList a4 = new ArrayList();
	// a4.add(restId);
	// a4.add("true");
	// a4.add(userId);
	// a4.add(rngConstants.IosOS);
	// a4.add(rngConstants.versionCode200);
	// ArrayList a5 = new ArrayList();
	// a5.add(restId);
	// a5.add("true");
	// a5.add(userId);
	// a5.add(rngConstants.IosOS);
	// a5.add(rngConstants.versionCode250);
	// ArrayList a6 = new ArrayList();
	// a6.add(restId);
	// a6.add("true");
	// a6.add(userId);
	// a6.add(rngConstants.WebOS);
	// a6.add(rngConstants.versionCode200);
	// ArrayList a7 = new ArrayList();
	// a7.add(restId);
	// a7.add("true");
	// a7.add(userId);
	// a7.add(rngConstants.IosOS);
	// a7.add(rngConstants.versionCode250);
	//
	// ArrayList a8 = new ArrayList();
	// a8.add(rngConstants.minCartAmount);
	// a8.add(restId);
	// a8.add("false");
	// a8.add(userId);
	// a8.add(rngConstants.AndroidOS);
	// a8.add(rngConstants.versionCode229);
	// ArrayList a9 = new ArrayList();
	// a9.add(rngConstants.minCartAmount);
	// a9.add(restId);
	// a9.add("true");
	// a9.add(userId);
	// a9.add(rngConstants.AndroidOS);
	// a9.add(rngConstants.versionCode229);
	// ArrayList a10 = new ArrayList();
	// a10.add(rngConstants.minCartAmount);
	// a10.add(restId);
	// a10.add("true");
	// a10.add(userId);
	// a10.add(rngConstants.AndroidOS);
	// a10.add(rngConstants.versionCode200);
	// ArrayList a11 = new ArrayList();
	// a11.add(rngConstants.minCartAmount);
	// a11.add(restId);
	// a11.add("true");
	// a11.add(userId);
	// a11.add(rngConstants.IosOS);
	// a11.add(rngConstants.versionCode200);
	// ArrayList a12 = new ArrayList();
	// a12.add(rngConstants.minCartAmount);
	// a12.add(restId);
	// a12.add("true");
	// a12.add(userId);
	// a12.add(rngConstants.IosOS);
	// a12.add(rngConstants.versionCode250);
	// ArrayList a13 = new ArrayList();
	// a13.add(rngConstants.minCartAmount);
	// a13.add(restId);
	// a13.add("true");
	// a13.add(userId);
	// a13.add(rngConstants.WebOS);
	// a13.add(rngConstants.versionCode200);
	// ArrayList a14 = new ArrayList();
	// a14.add(rngConstants.minCartAmount);
	// a14.add(restId);
	// a14.add("true");
	// a14.add(userId);
	// a14.add(rngConstants.IosOS);
	// a14.add(rngConstants.versionCode250);
	//
	// ArrayList a15 = new ArrayList();
	// a15.add(restId);
	// a15.add(categoryId);
	// a15.add(subCategoryId);
	// a15.add(itemId);
	// a15.add(count);
	// a15.add(price);
	// a15.add("10");
	// a15.add(userId);
	// a15.add("false");
	// a15.add(rngConstants.AndroidOS);
	// a15.add(rngConstants.versionCode229);
	// a15.add(minCartAmount);
	// ArrayList a16 = new ArrayList();
	// a16.add(restId);
	// a16.add(categoryId);
	// a16.add(subCategoryId);
	// a16.add(itemId);
	// a16.add(count);
	// a16.add(price);
	// a16.add(minCartAmount);
	// a16.add(userId);
	// a16.add("false");
	// a16.add(rngConstants.AndroidOS);
	// a16.add(rngConstants.versionCode229);
	// a16.add(minCartAmount);
	// ArrayList a17 = new ArrayList();
	// a17.add(restId);
	// a17.add(categoryId);
	// a17.add(subCategoryId);
	// a17.add(itemId);
	// a17.add(count);
	// a17.add(price);
	// a17.add(CartAmount);
	// a17.add(userId);
	// a17.add("true");
	// a17.add(rngConstants.AndroidOS);
	// a17.add(rngConstants.versionCode229);
	// a17.add(minCartAmount);
	// ArrayList a18 = new ArrayList();
	// a18.add(restId);
	// a18.add(categoryId);
	// a18.add(subCategoryId);
	// a18.add(itemId);
	// a18.add(count);
	// a18.add(price);
	// a18.add(CartAmount);
	// a18.add(userId);
	// a18.add("true");
	// a18.add(rngConstants.AndroidOS);
	// a18.add(rngConstants.versionCode200);
	// a18.add(minCartAmount);
	// ArrayList a19 = new ArrayList();
	// a19.add(restId);
	// a19.add(categoryId);
	// a19.add(subCategoryId);
	// a19.add(itemId);
	// a19.add(count);
	// a19.add(price);
	// a19.add(CartAmount);
	// a19.add(userId);
	// a19.add("true");
	// a19.add(rngConstants.IosOS);
	// a19.add(rngConstants.versionCode200);
	// a19.add(minCartAmount);
	// ArrayList a20 = new ArrayList();
	// a20.add(restId);
	// a20.add(categoryId);
	// a20.add(subCategoryId);
	// a20.add(itemId);
	// a20.add(count);
	// a20.add(price);
	// a20.add(CartAmount);
	// a20.add(userId);
	// a20.add("true");
	// a20.add(rngConstants.IosOS);
	// a20.add(rngConstants.versionCode250);
	// a20.add(minCartAmount);
	// ArrayList a21 = new ArrayList();
	// a21.add(restId);
	// a21.add(categoryId);
	// a21.add(subCategoryId);
	// a21.add(itemId);
	// a21.add(count);
	// a21.add(price);
	// a21.add(CartAmount);
	// a21.add(userId);
	// a21.add("true");
	// a21.add(rngConstants.WebOS);
	// a21.add(rngConstants.versionCode250);
	// a21.add(minCartAmount);
	// ArrayList a22 = new ArrayList();
	// a22.add(restId);
	// a22.add(categoryId);
	// a22.add(subCategoryId);
	// a22.add(itemId);
	// a22.add(count);
	// a22.add(price);
	// a22.add(CartAmount);
	// a22.add(userId);
	// a22.add("true");
	// a22.add(rngConstants.WebOS);
	// a22.add(rngConstants.versionCode200);
	// a22.add(minCartAmount);
	//
	// ArrayList a23 = new ArrayList();
	// a23.add(restId);
	// a23.add("false");
	// a23.add(userId2);
	// a23.add(rngConstants.AndroidOS);
	// a23.add(rngConstants.versionCode229);
	// ArrayList a24 = new ArrayList();
	// a24.add(restId);
	// a24.add("true");
	// a24.add(userId2);
	// a24.add(rngConstants.AndroidOS);
	// a24.add(rngConstants.versionCode229);
	// ArrayList a25 = new ArrayList();
	// a25.add(restId);
	// a25.add("true");
	// a25.add(userId2);
	// a25.add(rngConstants.AndroidOS);
	// a25.add(rngConstants.versionCode200);
	// ArrayList a26 = new ArrayList();
	// a26.add(restId);
	// a26.add("true");
	// a26.add(userId2);
	// a26.add(rngConstants.IosOS);
	// a26.add(rngConstants.versionCode200);
	// ArrayList a27 = new ArrayList();
	// a27.add(restId);
	// a27.add("true");
	// a27.add(userId2);
	// a27.add(rngConstants.IosOS);
	// a27.add(rngConstants.versionCode250);
	// ArrayList a28 = new ArrayList();
	// a28.add(restId);
	// a28.add("true");
	// a28.add(userId2);
	// a28.add(rngConstants.WebOS);
	// a28.add(rngConstants.versionCode200);
	// ArrayList a29 = new ArrayList();
	// a29.add(restId);
	// a29.add("true");
	// a29.add(userId2);
	// a29.add(rngConstants.IosOS);
	// a29.add(rngConstants.versionCode250);
	//
	// ArrayList a30 = new ArrayList();
	// a30.add(rngConstants.minCartAmount);
	// a30.add(restId);
	// a30.add("false");
	// a30.add(userId2);
	// a30.add(rngConstants.AndroidOS);
	// a30.add(rngConstants.versionCode229);
	// ArrayList a31 = new ArrayList();
	// a31.add(rngConstants.minCartAmount);
	// a31.add(restId);
	// a31.add("true");
	// a31.add(userId2);
	// a31.add(rngConstants.AndroidOS);
	// a31.add(rngConstants.versionCode229);
	// ArrayList a32 = new ArrayList();
	// a32.add(rngConstants.minCartAmount);
	// a32.add(restId);
	// a32.add("true");
	// a32.add(userId2);
	// a32.add(rngConstants.AndroidOS);
	// a32.add(rngConstants.versionCode200);
	// ArrayList a33 = new ArrayList();
	// a33.add(rngConstants.minCartAmount);
	// a33.add(restId);
	// a33.add("true");
	// a33.add(userId2);
	// a33.add(rngConstants.IosOS);
	// a33.add(rngConstants.versionCode200);
	// ArrayList a34 = new ArrayList();
	// a34.add(rngConstants.minCartAmount);
	// a34.add(restId);
	// a34.add("true");
	// a34.add(userId2);
	// a34.add(rngConstants.IosOS);
	// a34.add(rngConstants.versionCode250);
	// ArrayList a35 = new ArrayList();
	// a35.add(rngConstants.minCartAmount);
	// a35.add(restId);
	// a35.add("true");
	// a35.add(userId2);
	// a35.add(rngConstants.WebOS);
	// a35.add(rngConstants.versionCode200);
	// ArrayList a36 = new ArrayList();
	// a36.add(rngConstants.minCartAmount);
	// a36.add(restId);
	// a36.add("true");
	// a36.add(userId2);
	// a36.add(rngConstants.IosOS);
	// a36.add(rngConstants.versionCode250);
	//
	// ArrayList a37 = new ArrayList();
	// a37.add(restId);
	// a37.add(categoryId);
	// a37.add(subCategoryId);
	// a37.add(itemId);
	// a37.add(count);
	// a37.add(price);
	// a37.add("10");
	// a37.add(userId2);
	// a37.add("false");
	// a37.add(rngConstants.AndroidOS);
	// a37.add(rngConstants.versionCode229);
	// a37.add(minCartAmount);
	// ArrayList a38 = new ArrayList();
	// a38.add(restId);
	// a38.add(categoryId);
	// a38.add(subCategoryId);
	// a38.add(itemId);
	// a38.add(count);
	// a38.add(price);
	// a38.add(minCartAmount);
	// a38.add(userId2);
	// a38.add("false");
	// a38.add(rngConstants.AndroidOS);
	// a38.add(rngConstants.versionCode229);
	// a38.add(minCartAmount);
	// ArrayList a39 = new ArrayList();
	// a39.add(restId);
	// a39.add(categoryId);
	// a39.add(subCategoryId);
	// a39.add(itemId);
	// a39.add(count);
	// a39.add(price);
	// a39.add(CartAmount);
	// a39.add(userId2);
	// a39.add("true");
	// a39.add(rngConstants.AndroidOS);
	// a39.add(rngConstants.versionCode229);
	// a39.add(minCartAmount);
	// ArrayList a40 = new ArrayList();
	// a40.add(restId);
	// a40.add(categoryId);
	// a40.add(subCategoryId);
	// a40.add(itemId);
	// a40.add(count);
	// a40.add(price);
	// a40.add(CartAmount);
	// a40.add(userId2);
	// a40.add("true");
	// a40.add(rngConstants.AndroidOS);
	// a40.add(rngConstants.versionCode200);
	// a40.add(minCartAmount);
	// ArrayList a41 = new ArrayList();
	// a41.add(restId);
	// a41.add(categoryId);
	// a41.add(subCategoryId);
	// a41.add(itemId);
	// a41.add(count);
	// a41.add(price);
	// a41.add(CartAmount);
	// a41.add(userId2);
	// a41.add("true");
	// a41.add(rngConstants.IosOS);
	// a41.add(rngConstants.versionCode200);
	// a41.add(minCartAmount);
	// ArrayList a42 = new ArrayList();
	// a42.add(restId);
	// a42.add(categoryId);
	// a42.add(subCategoryId);
	// a42.add(itemId);
	// a42.add(count);
	// a42.add(price);
	// a42.add(CartAmount);
	// a42.add(userId2);
	// a42.add("true");
	// a42.add(rngConstants.IosOS);
	// a42.add(rngConstants.versionCode250);
	// a42.add(minCartAmount);
	// ArrayList a43 = new ArrayList();
	// a43.add(restId);
	// a43.add(categoryId);
	// a43.add(subCategoryId);
	// a43.add(itemId);
	// a43.add(count);
	// a43.add(price);
	// a43.add(CartAmount);
	// a43.add(userId2);
	// a43.add("true");
	// a43.add(rngConstants.WebOS);
	// a43.add(rngConstants.versionCode250);
	// a43.add(minCartAmount);
	// ArrayList a44 = new ArrayList();
	// a44.add(restId);
	// a44.add(categoryId);
	// a44.add(subCategoryId);
	// a44.add(itemId);
	// a44.add(count);
	// a44.add(price);
	// a44.add(CartAmount);
	// a44.add(userId2);
	// a44.add("true");
	// a44.add(rngConstants.WebOS);
	// a44.add(rngConstants.versionCode200);
	// a44.add(minCartAmount);
	//
	//
	// return new Object[][] { { "list", a1,false }, { "list", a2,false }, { "list",
	// a3,false }, { "list", a4,false }, { "list", a5,false },
	// { "list", a6,false }, { "list", a7,false }, { "menu", a8,false }, { "menu",
	// a9,false }, { "menu", a10,false }, { "menu", a11,false },
	// { "menu", a12,false }, { "menu", a13,false }, { "menu", a14,false }, {
	// "cart", a15,false }, { "cart", a16,false }, { "cart", a17,false },
	// { "cart", a18,false }, { "cart", a19,false }, { "cart", a20,false }, {
	// "cart", a21,false }, { "cart", a22,false }, { "list", a23,true }, { "list",
	// a24,true }, { "list", a25,true }, { "list", a26,true }, { "list", a27,true },
	// { "list", a28,true }, { "list", a29,true }, { "menu", a30,true }, { "menu",
	// a31,true }, { "menu", a32,true }, { "menu", a33,true },
	// { "menu", a34,true }, { "menu", a35,true }, { "menu", a36,true }, { "cart",
	// a37,true }, { "cart", a38,true }, { "cart", a39,true },
	// { "cart", a40,true }, { "cart", a41,true }, { "cart", a42,true }, { "cart",
	// a43,true }, { "cart", a44,true } };
	// }
	//

	// @DataProvider(name = "FlatTD_EvaluationWithNoMinCartAtItemLevel")
	// public Object[][] FlatTD_EvaluationWithNoMinCartAtItemLevel() throws
	// IOException {
	// HashMap Keys = rngHelper.createFlatWithNoMinAmountAtItemLevel("0", "200");
	// String restId = (String) Keys.get("restId");
	//
	// String itemId = Keys.get(RngConstants.itemid).toString();
	// String categoryId = Keys.get(RngConstants.catid).toString();
	// String subCategoryId = Keys.get(RngConstants.subcatid).toString();
	// String next_item = Keys.get(RngConstants.next_item).toString();
	// String count = "1";
	// String price = "500";
	// String minCartAmount = price;
	// String userId = checkoutHelper.TokenData(rngConstants.mobile1,
	// rngConstants.password1).get("userId").toString()
	// .replace("\"", "").trim();
	//
	// String createTdId = (String) Keys.get("TDID");
	// rngHelper.disabledActiveTD(restId);
	//
	// ArrayList a1 = new ArrayList();
	// a1.add(restId);
	// a1.add("false");
	// a1.add(userId);
	// a1.add(rngConstants.AndroidOS);
	// a1.add(rngConstants.versionCode229);
	// ArrayList a2 = new ArrayList();
	// a2.add(restId);
	// a2.add("true");
	// a2.add(userId);
	// a2.add(rngConstants.AndroidOS);
	// a2.add(rngConstants.versionCode229);
	// ArrayList a3 = new ArrayList();
	// a3.add(restId);
	// a3.add("true");
	// a3.add(userId);
	// a3.add(rngConstants.AndroidOS);
	// a3.add(rngConstants.versionCode200);
	// ArrayList a4 = new ArrayList();
	// a4.add(restId);
	// a4.add("true");
	// a4.add(userId);
	// a4.add(rngConstants.IosOS);
	// a4.add(rngConstants.versionCode200);
	// ArrayList a5 = new ArrayList();
	// a5.add(restId);
	// a5.add("true");
	// a5.add(userId);
	// a5.add(rngConstants.IosOS);
	// a5.add(rngConstants.versionCode250);
	// ArrayList a6 = new ArrayList();
	// a6.add(restId);
	// a6.add("true");
	// a6.add(userId);
	// a6.add(rngConstants.WebOS);
	// a6.add(rngConstants.versionCode200);
	// ArrayList a7 = new ArrayList();
	// a7.add(restId);
	// a7.add("true");
	// a7.add(userId);
	// a7.add(rngConstants.IosOS);
	// a7.add(rngConstants.versionCode250);
	//
	// ArrayList a8 = new ArrayList();
	// a8.add(rngConstants.minCartAmount);
	// a8.add(restId);
	// a8.add("false");
	// a8.add(userId);
	// a8.add(rngConstants.AndroidOS);
	// a8.add(rngConstants.versionCode229);
	// ArrayList a9 = new ArrayList();
	// a9.add(rngConstants.minCartAmount);
	// a9.add(restId);
	// a9.add("true");
	// a9.add(userId);
	// a9.add(rngConstants.AndroidOS);
	// a9.add(rngConstants.versionCode229);
	// ArrayList a10 = new ArrayList();
	// a10.add(rngConstants.minCartAmount);
	// a10.add(restId);
	// a10.add("true");
	// a10.add(userId);
	// a10.add(rngConstants.AndroidOS);
	// a10.add(rngConstants.versionCode200);
	// ArrayList a11 = new ArrayList();
	// a11.add(rngConstants.minCartAmount);
	// a11.add(restId);
	// a11.add("true");
	// a11.add(userId);
	// a11.add(rngConstants.IosOS);
	// a11.add(rngConstants.versionCode200);
	// ArrayList a12 = new ArrayList();
	// a12.add(rngConstants.minCartAmount);
	// a12.add(restId);
	// a12.add("true");
	// a12.add(userId);
	// a12.add(rngConstants.IosOS);
	// a12.add(rngConstants.versionCode250);
	// ArrayList a13 = new ArrayList();
	// a13.add(rngConstants.minCartAmount);
	// a13.add(restId);
	// a13.add("true");
	// a13.add(userId);
	// a13.add(rngConstants.WebOS);
	// a13.add(rngConstants.versionCode200);
	// ArrayList a14 = new ArrayList();
	// a14.add(rngConstants.minCartAmount);
	// a14.add(restId);
	// a14.add("true");
	// a14.add(userId);
	// a14.add(rngConstants.IosOS);
	// a14.add(rngConstants.versionCode250);
	//
	// ArrayList a15 = new ArrayList();
	// a15.add(restId);
	// a15.add(categoryId);
	// a15.add(subCategoryId);
	// a15.add(itemId);
	// a15.add(count);
	// a15.add(price);
	// a15.add("0");
	// a15.add(userId);
	// a15.add("false");
	// a15.add(rngConstants.AndroidOS);
	// a15.add(rngConstants.versionCode229);
	// a15.add(next_item);
	// ArrayList a16 = new ArrayList();
	// a16.add(restId);
	// a16.add(categoryId);
	// a16.add(subCategoryId);
	// a16.add(itemId);
	// a16.add(count);
	// a16.add(price);
	// a16.add(minCartAmount);
	// a16.add(userId);
	// a16.add("false");
	// a16.add(rngConstants.AndroidOS);
	// a16.add(rngConstants.versionCode229);
	// a16.add(next_item);
	// ArrayList a17 = new ArrayList();
	// a17.add(restId);
	// a17.add(categoryId);
	// a17.add(subCategoryId);
	// a17.add(itemId);
	// a17.add(count);
	// a17.add(price);
	// a17.add(minCartAmount);
	// a17.add(userId);
	// a17.add("true");
	// a17.add(rngConstants.AndroidOS);
	// a17.add(rngConstants.versionCode229);
	// a17.add(next_item);
	// ArrayList a18 = new ArrayList();
	// a18.add(restId);
	// a18.add(categoryId);
	// a18.add(subCategoryId);
	// a18.add(itemId);
	// a18.add(count);
	// a18.add(price);
	// a18.add(minCartAmount);
	// a18.add(userId);
	// a18.add("true");
	// a18.add(rngConstants.AndroidOS);
	// a18.add(rngConstants.versionCode200);
	// a18.add(next_item);
	// ArrayList a19 = new ArrayList();
	// a19.add(restId);
	// a19.add(categoryId);
	// a19.add(subCategoryId);
	// a19.add(itemId);
	// a19.add(count);
	// a19.add(price);
	// a19.add(minCartAmount);
	// a19.add(userId);
	// a19.add("true");
	// a19.add(rngConstants.IosOS);
	// a19.add(rngConstants.versionCode200);
	// a19.add(next_item);
	// ArrayList a20 = new ArrayList();
	// a20.add(restId);
	// a20.add(categoryId);
	// a20.add(subCategoryId);
	// a20.add(itemId);
	// a20.add(count);
	// a20.add(price);
	// a20.add(minCartAmount);
	// a20.add(userId);
	// a20.add("true");
	// a20.add(rngConstants.IosOS);
	// a20.add(rngConstants.versionCode250);
	// a20.add(next_item);
	// ArrayList a21 = new ArrayList();
	// a21.add(restId);
	// a21.add(categoryId);
	// a21.add(subCategoryId);
	// a21.add(itemId);
	// a21.add(count);
	// a21.add(price);
	// a21.add(minCartAmount);
	// a21.add(userId);
	// a21.add("true");
	// a21.add(rngConstants.WebOS);
	// a21.add(rngConstants.versionCode250);
	// a21.add(next_item);
	// ArrayList a22 = new ArrayList();
	// a22.add(restId);
	// a22.add(categoryId);
	// a22.add(subCategoryId);
	// a22.add(itemId);
	// a22.add(count);
	// a22.add(price);
	// a22.add(minCartAmount);
	// a22.add(userId);
	// a22.add("true");
	// a22.add(rngConstants.WebOS);
	// a22.add(rngConstants.versionCode200);
	// a22.add(next_item);
	//
	// return new Object[][] { { "list", a1 }, { "list", a2 }, { "list", a3 }, {
	// "list", a4 }, { "list", a5 },
	// { "list", a6 }, { "list", a7 }, { "menu", a8 }, { "menu", a9 }, { "menu", a10
	// }, { "menu", a11 },
	// { "menu", a12 }, { "menu", a13 }, { "menu", a14 }, { "cart", a15 }, { "cart",
	// a16 }, { "cart", a17 },
	// { "cart", a18 }, { "cart", a19 }, { "cart", a20 }, { "cart", a21 }, { "cart",
	// a22 } };
	// }
	//
	// @DataProvider(name = "FlatTD_EvaluationWithMinCartAtItemLevel")
	// public Object[][] FlatTD_EvaluationWithMinCartAtItemLevel() throws
	// IOException {
	// HashMap Keys = rngHelper.createFlatWithMinCartAmountAtItemLevel("100",
	// "200");
	// String restId = (String) Keys.get("restId");
	//
	// String itemId = Keys.get(RngConstants.itemid).toString();
	// String categoryId = Keys.get(RngConstants.catid).toString();
	// String subCategoryId = Keys.get(RngConstants.subcatid).toString();
	// String count = "1";
	// String price = "200";
	// String CartAmount = price;
	// String minCartAmount = Keys.get("minCartAmount").toString();
	// String userId = checkoutHelper.TokenData(rngConstants.mobile1,
	// rngConstants.password1).get("userId").toString()
	// .replace("\"", "").trim();
	//
	// ArrayList a1 = new ArrayList();
	// a1.add(restId);
	// a1.add("false");
	// a1.add(userId);
	// a1.add(rngConstants.AndroidOS);
	// a1.add(rngConstants.versionCode229);
	// ArrayList a2 = new ArrayList();
	// a2.add(restId);
	// a2.add("true");
	// a2.add(userId);
	// a2.add(rngConstants.AndroidOS);
	// a2.add(rngConstants.versionCode229);
	// ArrayList a3 = new ArrayList();
	// a3.add(restId);
	// a3.add("true");
	// a3.add(userId);
	// a3.add(rngConstants.AndroidOS);
	// a3.add(rngConstants.versionCode200);
	// ArrayList a4 = new ArrayList();
	// a4.add(restId);
	// a4.add("true");
	// a4.add(userId);
	// a4.add(rngConstants.IosOS);
	// a4.add(rngConstants.versionCode200);
	// ArrayList a5 = new ArrayList();
	// a5.add(restId);
	// a5.add("true");
	// a5.add(userId);
	// a5.add(rngConstants.IosOS);
	// a5.add(rngConstants.versionCode250);
	// ArrayList a6 = new ArrayList();
	// a6.add(restId);
	// a6.add("true");
	// a6.add(userId);
	// a6.add(rngConstants.WebOS);
	// a6.add(rngConstants.versionCode200);
	// ArrayList a7 = new ArrayList();
	// a7.add(restId);
	// a7.add("true");
	// a7.add(userId);
	// a7.add(rngConstants.IosOS);
	// a7.add(rngConstants.versionCode250);
	//
	// ArrayList a8 = new ArrayList();
	// a8.add(rngConstants.minCartAmount);
	// a8.add(restId);
	// a8.add("false");
	// a8.add(userId);
	// a8.add(rngConstants.AndroidOS);
	// a8.add(rngConstants.versionCode229);
	// ArrayList a9 = new ArrayList();
	// a9.add(rngConstants.minCartAmount);
	// a9.add(restId);
	// a9.add("true");
	// a9.add(userId);
	// a9.add(rngConstants.AndroidOS);
	// a9.add(rngConstants.versionCode229);
	// ArrayList a10 = new ArrayList();
	// a10.add(rngConstants.minCartAmount);
	// a10.add(restId);
	// a10.add("true");
	// a10.add(userId);
	// a10.add(rngConstants.AndroidOS);
	// a10.add(rngConstants.versionCode200);
	// ArrayList a11 = new ArrayList();
	// a11.add(rngConstants.minCartAmount);
	// a11.add(restId);
	// a11.add("true");
	// a11.add(userId);
	// a11.add(rngConstants.IosOS);
	// a11.add(rngConstants.versionCode200);
	// ArrayList a12 = new ArrayList();
	// a12.add(rngConstants.minCartAmount);
	// a12.add(restId);
	// a12.add("true");
	// a12.add(userId);
	// a12.add(rngConstants.IosOS);
	// a12.add(rngConstants.versionCode250);
	// ArrayList a13 = new ArrayList();
	// a13.add(rngConstants.minCartAmount);
	// a13.add(restId);
	// a13.add("true");
	// a13.add(userId);
	// a13.add(rngConstants.WebOS);
	// a13.add(rngConstants.versionCode200);
	// ArrayList a14 = new ArrayList();
	// a14.add(rngConstants.minCartAmount);
	// a14.add(restId);
	// a14.add("true");
	// a14.add(userId);
	// a14.add(rngConstants.IosOS);
	// a14.add(rngConstants.versionCode250);
	//
	// ArrayList a15 = new ArrayList();
	// a15.add(restId);
	// a15.add(categoryId);
	// a15.add(subCategoryId);
	// a15.add(itemId);
	// a15.add(count);
	// a15.add(price);
	// a15.add("0");
	// a15.add(userId);
	// a15.add("false");
	// a15.add(rngConstants.AndroidOS);
	// a15.add(rngConstants.versionCode229);
	// a15.add(minCartAmount);
	// ArrayList a16 = new ArrayList();
	// a16.add(restId);
	// a16.add(categoryId);
	// a16.add(subCategoryId);
	// a16.add(itemId);
	// a16.add(count);
	// a16.add(price);
	// a16.add(minCartAmount);
	// a16.add(userId);
	// a16.add("false");
	// a16.add(rngConstants.AndroidOS);
	// a16.add(rngConstants.versionCode229);
	// a16.add(minCartAmount);
	// ArrayList a17 = new ArrayList();
	// a17.add(restId);
	// a17.add(categoryId);
	// a17.add(subCategoryId);
	// a17.add(itemId);
	// a17.add(count);
	// a17.add(price);
	// a17.add(CartAmount);
	// a17.add(userId);
	// a17.add("true");
	// a17.add(rngConstants.AndroidOS);
	// a17.add(rngConstants.versionCode229);
	// a17.add(minCartAmount);
	// ArrayList a18 = new ArrayList();
	// a18.add(restId);
	// a18.add(categoryId);
	// a18.add(subCategoryId);
	// a18.add(itemId);
	// a18.add(count);
	// a18.add(price);
	// a18.add(CartAmount);
	// a18.add(userId);
	// a18.add("true");
	// a18.add(rngConstants.AndroidOS);
	// a18.add(rngConstants.versionCode200);
	// a18.add(minCartAmount);
	// ArrayList a19 = new ArrayList();
	// a19.add(restId);
	// a19.add(categoryId);
	// a19.add(subCategoryId);
	// a19.add(itemId);
	// a19.add(count);
	// a19.add(price);
	// a19.add(CartAmount);
	// a19.add(userId);
	// a19.add("true");
	// a19.add(rngConstants.IosOS);
	// a19.add(rngConstants.versionCode200);
	// a19.add(minCartAmount);
	// ArrayList a20 = new ArrayList();
	// a20.add(restId);
	// a20.add(categoryId);
	// a20.add(subCategoryId);
	// a20.add(itemId);
	// a20.add(count);
	// a20.add(price);
	// a20.add(CartAmount);
	// a20.add(userId);
	// a20.add("true");
	// a20.add(rngConstants.IosOS);
	// a20.add(rngConstants.versionCode250);
	// a20.add(minCartAmount);
	// ArrayList a21 = new ArrayList();
	// a21.add(restId);
	// a21.add(categoryId);
	// a21.add(subCategoryId);
	// a21.add(itemId);
	// a21.add(count);
	// a21.add(price);
	// a21.add(CartAmount);
	// a21.add(userId);
	// a21.add("true");
	// a21.add(rngConstants.WebOS);
	// a21.add(rngConstants.versionCode250);
	// a21.add(minCartAmount);
	// ArrayList a22 = new ArrayList();
	// a22.add(restId);
	// a22.add(categoryId);
	// a22.add(subCategoryId);
	// a22.add(itemId);
	// a22.add(count);
	// a22.add(price);
	// a22.add(CartAmount);
	// a22.add(userId);
	// a22.add("true");
	// a22.add(rngConstants.WebOS);
	// a22.add(rngConstants.versionCode200);
	// a22.add(minCartAmount);
	//
	// return new Object[][] { { "list", a1 }, { "list", a2 }, { "list", a3 }, {
	// "list", a4 }, { "list", a5 },
	// { "list", a6 }, { "list", a7 }, { "menu", a8 }, { "menu", a9 }, { "menu", a10
	// }, { "menu", a11 },
	// { "menu", a12 }, { "menu", a13 }, { "menu", a14 }, { "cart", a15 }, { "cart",
	// a16 }, { "cart", a17 },
	// { "cart", a18 }, { "cart", a19 }, { "cart", a20 }, { "cart", a21 }, { "cart",
	// a22 } };
	//
	// }

	@DataProvider(name = "FlatTD_EvaluationWithSwiggyFirstOrderItemLevel")
	public Object[][] FlatTD_EvaluationWithSwiggyFirstOrderItemLevel() throws IOException {

		HashMap Keys = rngHelper.createFlatWithFirstOrderRestrictionAtItemLevel("0", "50");

		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add(CartAmount);
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, true }, { "list", a3,tid, true }, { "list", a4,tid, true },
				{ "list", a5,tid, true }, { "list", a6,tid, true }, { "list", a7,tid, true }, { "menu", a8,tid, false },
				{ "menu", a9,tid, true }, { "menu", a10,tid, true }, { "menu", a11,tid, true }, { "menu", a12,tid, true },
				{ "menu", a13,tid, true }, { "menu", a14,tid, true }, { "cart", a15,tid, false }, { "cart", a17,tid, true } };

	}

	@DataProvider(name = "FlatTD_EvaluationWithNoMinCartItemLevel")
	public Object[][] FlatTD_EvaluationWithNoMinCartItemLevel() throws IOException {

		HashMap Keys = rngHelper.createFlatWithNoMinCartAtItemLevel("0", "20");

		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TDID");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId2 = new Integer(rm.nextInt()).toString();
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add(CartAmount);
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId2);
		a18.add(count);
		a18.add(price);
		a18.add(CartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, true }, { "list", a2,tid, true }, { "list", a3,tid, true }, { "list", a4,tid, true },
				{ "list", a5,tid, true }, { "list", a6,tid, true }, { "list", a7,tid, true }, { "menu", a8,tid, true },
				{ "menu", a9,tid, true }, { "menu", a10,tid, true }, { "menu", a11,tid, true }, { "menu", a12,tid, true },
				{ "menu", a13,tid, true }, { "menu", a14,tid, true }, { "cart", a15,tid, true },
				{ "cart", a17,tid, true },/* { "cart2", a18,false } */
		};

	}

	@DataProvider(name = "FlatTD_EvaluationWithMinCartItemLevel")
	public Object[][] FlatTD_EvaluationWithMinCartItemLevel() throws IOException {

		HashMap Keys = rngHelper.createFlatWithMinCartAtItemLevel("100", "20");

		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TDID");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("10");
		a15.add(userId);
		a15.add("true");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, true }, { "list", a2,tid, true }, { "list", a3,tid, true }, { "list", a4,tid, true },
				{ "list", a5,tid, true }, { "list", a6,tid, true }, { "list", a7,tid, true }, { "menu", a8,tid, true },
				{ "menu", a9,tid, true }, { "menu", a10,tid, true }, { "menu", a11,tid, true }, { "menu", a12,tid, true },
				{ "menu", a13,tid, true }, { "menu", a14,tid, true }, { "cart", a15,tid, false }, { "cart", a17,tid, true } };

	}

	@DataProvider(name = "FlatTD_EvaluationWithRestaurantFirstOrderItemLevel")
	public Object[][] FlatTD_EvaluationWithRestaurantFirstOrderItemLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithRestaurantFirstOrderRestrictionAtItemLevel("0", "50");

		String restId = (String) Keys.get("restId");
		String tid = (String)Keys.get("TDID");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.WebOS);
		a2.add(rngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(rngConstants.minCartAmount);
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add("10");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.WebOS);
		a17.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();

		rngHelper.DormantUser(userId2, restId, "10");

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("true");
		a8.add(userId2);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("true");
		a9.add(userId2);
		a9.add(rngConstants.WebOS);
		a9.add(rngConstants.versionCode200);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add("true");
		a10.add(userId2);
		a10.add(rngConstants.IosOS);
		a10.add(rngConstants.versionCode250);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add("10");
		a18.add(userId2);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("true");
		a19.add(rngConstants.WebOS);
		a19.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, true }, { "list", a2,tid, true }, { "list", a3,tid, true }, { "menu", a4,tid, true },
				{ "menu", a5,tid, true }, { "menu", a6,tid, true }, { "cart", a17,tid, true }, { "cart", a16,tid, true },
				{ "list", a8,tid, false }, { "list", a9,tid, false }, { "list", a10,tid, false }, { "menu", a11,tid, false },
				{ "menu", a12,tid, false }, { "menu", a13,tid, false }, { "cart", a18,tid, false }, { "cart", a19,tid, false }, };

	}

	public Object[][] FlatTD_EvaluationWithTimeSlotRestrictionAtItemLevel_Set1() throws IOException {
		HashMap Keys = rngHelper.createFlatWithTimeSlotRestrictionAtItemLevel("100", "200", 0, 15);
		String restId = (String) Keys.get("restId");
		String isValidforCurrentTime = (String) Keys.get("isValidforCurrentTime");

		String itemId = Keys.get(RngConstants.itemid).toString();
		String categoryId = Keys.get(RngConstants.catid).toString();
		String subCategoryId = Keys.get(RngConstants.subcatid).toString();
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String minCartAmount = Keys.get("minCartAmount").toString();
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);
		a1.add(isValidforCurrentTime);
		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		a2.add(isValidforCurrentTime);
		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);
		a3.add(isValidforCurrentTime);
		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);
		a4.add(isValidforCurrentTime);
		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);
		a5.add(isValidforCurrentTime);
		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);
		a6.add(isValidforCurrentTime);
		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);
		a7.add(isValidforCurrentTime);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);
		a8.add(isValidforCurrentTime);
		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);
		a9.add(isValidforCurrentTime);
		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);
		a10.add(isValidforCurrentTime);
		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);
		a11.add(isValidforCurrentTime);
		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);
		a12.add(isValidforCurrentTime);
		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);
		a13.add(isValidforCurrentTime);
		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);
		a14.add(isValidforCurrentTime);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("10");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);
		a15.add(minCartAmount);
		a15.add(isValidforCurrentTime);
		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		a16.add(minCartAmount);
		a16.add(isValidforCurrentTime);
		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		a17.add(minCartAmount);
		a17.add(isValidforCurrentTime);
		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add(CartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);
		a18.add(minCartAmount);
		a18.add(isValidforCurrentTime);
		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);
		a19.add(minCartAmount);
		a19.add(isValidforCurrentTime);
		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(CartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);
		a20.add(minCartAmount);
		a20.add(isValidforCurrentTime);
		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(CartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);
		a21.add(minCartAmount);
		a21.add(isValidforCurrentTime);
		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(CartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);
		a22.add(minCartAmount);
		a22.add(isValidforCurrentTime);

		Object[][] obj1 = new Object[][] { { "list", a1 }, { "list", a2 }, { "list", a3 }, { "list", a4 },
				{ "list", a5 }, { "list", a6 }, { "list", a7 }, { "menu", a8 }, { "menu", a9 }, { "menu", a10 },
				{ "menu", a11 }, { "menu", a12 }, { "menu", a13 }, { "menu", a14 }, { "cart", a15 }, { "cart", a16 },
				{ "cart", a17 }, { "cart", a18 }, { "cart", a19 }, { "cart", a20 }, { "cart", a21 }, { "cart", a22 } };
		return obj1;
	}

	public Object[][] FlatTD_EvaluationWithTimeSlotRestrictionAtItemLevel_Set2() throws IOException {
		HashMap Keys = rngHelper.createFlatWithTimeSlotRestrictionAtItemLevel("100", "200", 1, 15);
		String restId = (String) Keys.get("restId");
		String isValidforCurrentTime = (String) Keys.get("isValidforCurrentTime");
		String itemId = Keys.get(RngConstants.itemid).toString();
		String categoryId = Keys.get(RngConstants.catid).toString();
		String subCategoryId = Keys.get(RngConstants.subcatid).toString();
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String minCartAmount = Keys.get("minCartAmount").toString();
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);
		a1.add(isValidforCurrentTime);
		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		a2.add(isValidforCurrentTime);
		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);
		a3.add(isValidforCurrentTime);
		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);
		a4.add(isValidforCurrentTime);
		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);
		a5.add(isValidforCurrentTime);
		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);
		a6.add(isValidforCurrentTime);
		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);
		a7.add(isValidforCurrentTime);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);
		a8.add(isValidforCurrentTime);
		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);
		a9.add(isValidforCurrentTime);
		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);
		a10.add(isValidforCurrentTime);
		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);
		a11.add(isValidforCurrentTime);
		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);
		a12.add(isValidforCurrentTime);
		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);
		a13.add(isValidforCurrentTime);
		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);
		a14.add(isValidforCurrentTime);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("10");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);
		a15.add(minCartAmount);
		a15.add(isValidforCurrentTime);
		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		a16.add(minCartAmount);
		a16.add(isValidforCurrentTime);
		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		a17.add(minCartAmount);
		a17.add(isValidforCurrentTime);
		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add(CartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);
		a18.add(minCartAmount);
		a18.add(isValidforCurrentTime);
		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);
		a19.add(minCartAmount);
		a19.add(isValidforCurrentTime);
		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(CartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);
		a20.add(minCartAmount);
		a20.add(isValidforCurrentTime);
		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(CartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);
		a21.add(minCartAmount);
		a21.add(isValidforCurrentTime);
		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(CartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);
		a22.add(minCartAmount);
		a22.add(isValidforCurrentTime);

		Object[][] obj2 = new Object[][] { { "list", a1 }, { "list", a2 }, { "list", a3 }, { "list", a4 },
				{ "list", a5 }, { "list", a6 }, { "list", a7 }, { "menu", a8 }, { "menu", a9 }, { "menu", a10 },
				{ "menu", a11 }, { "menu", a12 }, { "menu", a13 }, { "menu", a14 }, { "cart", a15 }, { "cart", a16 },
				{ "cart", a17 }, { "cart", a18 }, { "cart", a19 }, { "cart", a20 }, { "cart", a21 }, { "cart", a22 } };
		return obj2;
	}

	// @DataProvider(name = "FlatTD_EvaluationWithTimeSlotRestrictionAtItemLevel")
	// public Object[][] FlatTD_EvaluationWithTimeSlotRestrictionAtItemLevel()
	// throws IOException {
	//
	// Object[][] b1 = FlatTD_EvaluationWithTimeSlotRestrictionAtItemLevel_Set1();
	// Object[][] b2 = FlatTD_EvaluationWithTimeSlotRestrictionAtItemLevel_Set2();
	// List<Object[]> result = new ArrayList();
	//
	// result.addAll(Arrays.asList(b1));
	// result.addAll(Arrays.asList(b2));
	//
	// return result.toArray(new Object[result.size()][]);
	//
	// }

	@DataProvider(name = "FlatTD_EvaluationWithUserRestrictionAtItemLevel")
	public Object[][] FlatTD_EvaluationWithUserRestrictionAtItemLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithUserRestrictionAtItemLevel("0", "20");

		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.WebOS);
		a2.add(rngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(rngConstants.minCartAmount);
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add("10");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.WebOS);
		a17.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();

		rngHelper.userMappingTD(userId2, tid);

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("true");
		a8.add(userId2);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("true");
		a9.add(userId2);
		a9.add(rngConstants.WebOS);
		a9.add(rngConstants.versionCode200);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add("true");
		a10.add(userId2);
		a10.add(rngConstants.IosOS);
		a10.add(rngConstants.versionCode250);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add("10");
		a18.add(userId2);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("true");
		a19.add(rngConstants.WebOS);
		a19.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, false }, { "list", a3,tid, false },
				{ "menu", a4,tid, false }, { "menu", a5,tid, false }, { "menu", a6,tid, false }, { "cart", a17,tid, false },
				{ "cart", a16,tid, false }, { "list", a8,tid, true }, { "list", a9,tid, true }, { "list", a10,tid, true },
				{ "menu", a11,tid, true }, { "menu", a12,tid, true }, { "menu", a13,tid, true }, { "cart", a18,tid, true },
				{ "cart", a19,tid, true }, };

	}

	@DataProvider(name = "FlatTD_EvaluationWithThirtyDayDormantAtItemLevel")
	public Object[][] FlatTD_EvaluationWithThirtyDayDormantAtItemLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithThirtyDayDormantAtItemLevel("0", "20");

		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.WebOS);
		a2.add(rngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(rngConstants.minCartAmount);
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add("10");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.WebOS);
		a17.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();

		rngHelper.DormantUser(userId2, restId, "30");

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("true");
		a8.add(userId2);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("true");
		a9.add(userId2);
		a9.add(rngConstants.WebOS);
		a9.add(rngConstants.versionCode200);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add("true");
		a10.add(userId2);
		a10.add(rngConstants.IosOS);
		a10.add(rngConstants.versionCode250);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add("10");
		a18.add(userId2);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("true");
		a19.add(rngConstants.WebOS);
		a19.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, false }, { "list", a3,tid, false },
				{ "menu", a4,tid, false }, { "menu", a5,tid, false }, { "menu", a6,tid, false }, { "cart", a17,tid, false },
				{ "cart", a16,tid, false }, { "list", a8,tid, true }, { "list", a9,tid, true }, { "list", a10,tid, true },
				{ "menu", a11,tid, true }, { "menu", a12,tid, true }, { "menu", a13,tid, true }, { "cart", a18,tid, true },
				{ "cart", a19,tid, true }, };

	}

	@DataProvider(name = "FlatTD_EvaluationWithSixtyDayDormantAtItemLevel")
	public Object[][] FlatTD_EvaluationWithSixtyDayDormantAtItemLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithSixtyDayDormantAtItemLevel("100", "200");

		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.WebOS);
		a2.add(rngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(rngConstants.minCartAmount);
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add("10");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.WebOS);
		a17.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();

		rngHelper.DormantUser(userId2, restId, "60");

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("true");
		a8.add(userId2);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("true");
		a9.add(userId2);
		a9.add(rngConstants.WebOS);
		a9.add(rngConstants.versionCode200);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add("true");
		a10.add(userId2);
		a10.add(rngConstants.IosOS);
		a10.add(rngConstants.versionCode250);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add("10");
		a18.add(userId2);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("true");
		a19.add(rngConstants.WebOS);
		a19.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, false }, { "list", a3,tid, false },
				{ "menu", a4,tid, false }, { "menu", a5,tid, false }, { "menu", a6,tid, false }, { "cart", a17,tid, false },
				{ "cart", a16,tid, false }, { "list", a8,tid, true }, { "list", a9,tid, true }, { "list", a10,tid, true },
				{ "menu", a11,tid, true }, { "menu", a12,tid, true }, { "menu", a13,tid, true }, { "cart", a18,tid, true },
				{ "cart", a19,tid, true }, };

	}

	@DataProvider(name = "FlatTD_EvaluationWithNinetyDayDormantAtItemLevel")
	public Object[][] FlatTD_EvaluationWithNinetyDayDormantAtItemLevel() throws IOException {
		HashMap Keys = rngHelper.createFlatWithNinetyDayDormantAtItemLevel("100", "200");
		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.WebOS);
		a2.add(rngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(rngConstants.minCartAmount);
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add("10");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.WebOS);
		a17.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();

		rngHelper.DormantUser(userId2, restId, "90");

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("true");
		a8.add(userId2);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("true");
		a9.add(userId2);
		a9.add(rngConstants.WebOS);
		a9.add(rngConstants.versionCode200);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add("true");
		a10.add(userId2);
		a10.add(rngConstants.IosOS);
		a10.add(rngConstants.versionCode250);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add("10");
		a18.add(userId2);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("true");
		a19.add(rngConstants.WebOS);
		a19.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1,tid, false }, { "list", a2,tid, false }, { "list", a3,tid, false },
				{ "menu", a4,tid, false }, { "menu", a5,tid, false }, { "menu", a6,tid, false }, { "cart", a17,tid, false },
				{ "cart", a16,tid, false }, { "list", a8,tid, true }, { "list", a9,tid, true }, { "list", a10,tid, true },
				{ "menu", a11,tid, true }, { "menu", a12,tid, true }, { "menu", a13, tid,true }, { "cart", a18,tid, true },
				{ "cart", a19,tid, true }, };

	}

	@DataProvider(name = "PercentageTD_EvaluateCategoryLevelTest")
	public Object[][] PercentageTD_EvaluateCategoryLevelTest() throws IOException {

		String maxDiscountAmount = "400";
		String minCartAmount = "100";
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		String randomRestaurant = new Integer(rm.nextInt()).toString();

		rngHelper.disabledActiveTD(randomRestaurant);
		List<Map<String, Object>> categoriesList = rngHelper.getCategoryForRestId(randomRestaurant);

		String subCategoryId1 = new Integer(rm.nextInt()).toString();
		String categoryId1 = new Integer(rm.nextInt()).toString();
		String categoryId2 = new Integer(rm.nextInt()).toString();
		List<Category> categories = new ArrayList<>();

		Category category = new Category();
		category.setSubCategories(new ArrayList());
		category.setId(categoryId1);
		category.setName("Testdsds");

		categories.add(category);

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test Category", categories));

		String tdId = rngHelper.createPercentageTDWithNoMinAmountAtCategoryLevel(restaurantLists1, minCartAmount,
				maxDiscountAmount);

		System.out.println("TD ID : " + tdId);
		String itemDetailsTD = new Integer(rm.nextInt()).toString();

		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(randomRestaurant);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(randomRestaurant);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		List<ItemRequest> list = new ArrayList<>();
		list.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", minCartAmount));
		CartEvaluateRequest minCartAmountCart = new CartEvaluateRequest(minCartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list);

		List<ItemRequest> list1 = new ArrayList<>();
		list1.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", "5000"));
		CartEvaluateRequest maxDiscountCart = new CartEvaluateRequest("5000", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list1);

		List<ItemRequest> list2 = new ArrayList<>();
		list2.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "2", "5000"));
		list2.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "2", "5000"));
		CartEvaluateRequest multiItemCart = new CartEvaluateRequest("10000", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list2);

		List<ItemRequest> list3 = new ArrayList<>();
		list3.add(new ItemRequest(randomRestaurant, categoryId2, subCategoryId1, itemDetailsTD, "1", "500"));
		CartEvaluateRequest NoDiscountCart = new CartEvaluateRequest("500", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list3);

		return new Object[][] { { "list", a1, true, "20", true }, { "menu", a2, true, "20", true },
				{ "cart", minCartAmountCart, true, "20.0", true }, { "cart", maxDiscountCart, true, "400.0", true },
				{ "cart", multiItemCart, true, "400.0", true }, { "cart", NoDiscountCart, true, "80", false },
				// {"cart", nonTDCart, false, "0"},
				// {"cart", tdNonTDCart, true, "40"}
		};
	}

	@DataProvider(name = "FlatTDEvaluationAtCategoryLevelDP")
	public Object[][] FlatTDEvaluationAtCategoryLevelDP() throws IOException {
		String minCartAmount = "500";
		String flatTDAmount = "200";
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		String subCategoryId1 = new Integer(rm.nextInt()).toString();
		String categoryId1 = new Integer(rm.nextInt()).toString();
		String categoryId2 = new Integer(rm.nextInt()).toString();
		List<Category> categories = new ArrayList<>();

		Category category = new Category();
		category.setSubCategories(new ArrayList());
		category.setId(categoryId1);
		category.setName("Testdsds");

		categories.add(category);

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test SubCategory", categories));
		rngHelper.disabledActiveTD(randomRestaurant);
		String TdId = rngHelper.createFlatWithNoMinAmountAtCategoryLevel(restaurantLists1, minCartAmount, flatTDAmount);
		System.out.println("TD ID: " + TdId);
		String itemDetailsTD = new Integer(rm.nextInt()).toString(); // cmsHelper.getItem(randomRestaurant, categoryId1,
																		// subCategoryId1);
		String itemDetailsNonTD = new Integer(rm.nextInt()).toString(); // cmsHelper.getItem(randomRestaurant,
																		// categoryId1, subCategoryId2);

		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(randomRestaurant);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(randomRestaurant);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		List<ItemRequest> list = new ArrayList<>();
		list.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", minCartAmount));
		CartEvaluateRequest cartEvaluateRequestMin = new CartEvaluateRequest(minCartAmount, userId, "false",
				rngConstants.AndroidOS, rngConstants.versionCode229, list);

		List<ItemRequest> list1 = new ArrayList<>();
		list1.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "1", "50"));
		CartEvaluateRequest cartEvaluatewithmin = new CartEvaluateRequest("50", userId, "false", rngConstants.AndroidOS,
				rngConstants.versionCode229, list1);

		List<ItemRequest> list2 = new ArrayList<>();
		list2.add(new ItemRequest(randomRestaurant, categoryId1, subCategoryId1, itemDetailsTD, "2", "4000"));
		list2.add(new ItemRequest(randomRestaurant, categoryId2, subCategoryId1, itemDetailsTD, "2", "4000"));

		CartEvaluateRequest cartEvaluateRequestMultiItem = new CartEvaluateRequest("4000", userId, "false",
				rngConstants.AndroidOS, rngConstants.versionCode229, list2);

		List<ItemRequest> list3 = new ArrayList<>();
		list3.add(new ItemRequest(randomRestaurant, categoryId2, subCategoryId1, itemDetailsNonTD, "2", minCartAmount));
		list3.add(new ItemRequest(randomRestaurant, categoryId2, subCategoryId1, itemDetailsNonTD, "2", minCartAmount));
		CartEvaluateRequest cartEvaluateRequestNonCategory = new CartEvaluateRequest("500", userId, "false",
				rngConstants.AndroidOS, rngConstants.versionCode229, list3);

		return new Object[][] { { "list", a1, true, flatTDAmount, true }, { "menu", a2, true, flatTDAmount, true },
				{ "cart", cartEvaluateRequestMin, true, flatTDAmount, true },
				{ "cart", cartEvaluatewithmin, false, flatTDAmount, true },
				{ "cart", cartEvaluateRequestMultiItem, true, flatTDAmount, true },
				{ "cart", cartEvaluateRequestNonCategory, true, flatTDAmount, false } };
	}

	@DataProvider(name = "FreeDeliveryTD_EvaluationWithSpecialFee")
	public Object[][] FreeDeliveryTD_EvaluationWithSpecialFee() throws IOException {

		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		String price = "500";
		String cartAmount = price;
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("0", restId, false, false, false,
				"ZERO_DAYS_DORMANT",true);

		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);
		a1.add("20.0");
		a1.add("0.0");
		a1.add("0.0");
		a1.add("0.0");
		a1.add("0.0");
		a1.add("20.0");

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		a2.add("0.0");
		a2.add("20.0");
		a2.add("0.0");
		a2.add("0.0");
		a2.add("0.0");
		a2.add("20.0");

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("false");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);
		a3.add("0.0");
		a3.add("0.0");
		a3.add("20.0");
		a3.add("0.0");
		a3.add("0.0");
		a3.add("20.0");



		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("false");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);
		a4.add("0.0");
		a4.add("0.0");
		a4.add("0.0");
		a4.add("20.0");
		a4.add("0.0");
		a4.add("20.0");



		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("false");
		a5.add(userId);
		a5.add(rngConstants.AndroidOS);
		a5.add(rngConstants.versionCode229);
		a5.add("0.0");
		a5.add("0.0");
		a5.add("0.0");
		a5.add("0.0");
		a5.add("20.0");
		a5.add("20.0");


		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("false");
		a6.add(userId);
		a6.add(rngConstants.AndroidOS);
		a6.add(rngConstants.versionCode229);
		a6.add("20.0");
		a6.add("20.0");
		a6.add("0.0");
		a6.add("0.0");
		a6.add("0.0");
		a6.add("40.0");



		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("false");
		a7.add(userId);
		a7.add(rngConstants.AndroidOS);
		a7.add(rngConstants.versionCode229);
		a7.add("0.0");
		a7.add("20.0");
		a7.add("20.0");
		a7.add("0.0");
		a7.add("0.0");
		a7.add("40.0");


		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);
		a8.add("0.0");
		a8.add("0.0");
		a8.add("20.0");
		a8.add("20.0");
		a8.add("0.0");
		a8.add("40.0");


		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("false");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);
		a9.add("0.0");
		a9.add("0.0");
		a9.add("20.0");
		a9.add("20.0");
		a9.add("0.0");
		a9.add("40.0");


	//	ArrayList a3 = new ArrayList();
//		a1.add(restId);
//		a1.add("false");
//		a1.add(userId);
//		a1.add(rngConstants.AndroidOS);
//		a1.add(rngConstants.versionCode229);
//		a1.add("0.0");
//		a1.add("20.0");
//		a1.add("0.0");
//		a1.add("0.0");
//		a1.add("0.0");
//		a1.add("20.0");


//		ArrayList a3 = new ArrayList();
//		a3.add(restId);
//		a3.add("false");
//		a3.add(userId);
//		a3.add(rngConstants.AndroidOS);
//		a3.add(rngConstants.versionCode229);
//		a3.add("20.0");
//		a3.add("20.0");
//		ArrayList a4 = new ArrayList();
//		a4.add(restId);
//		a4.add("false");
//		a4.add(userId);
//		a4.add(rngConstants.AndroidOS);
//		a4.add(rngConstants.versionCode229);
//		a4.add("0.0");
//		a4.add("0.0");

//		ArrayList a8 = new ArrayList();
//		a8.add(rngConstants.minCartAmount);
//		a8.add(restId);
//		a8.add("false");
//		a8.add(userId);
//		a8.add(rngConstants.AndroidOS);
//		a8.add(rngConstants.versionCode229);
//		a8.add("20.0");
//		a8.add("0.0");
//
//		ArrayList a9 = new ArrayList();
//		a9.add(rngConstants.minCartAmount);
//		a9.add(restId);
//		a9.add("false");
//		a9.add(userId);
//		a9.add(rngConstants.AndroidOS);
//		a9.add(rngConstants.versionCode229);
//		a9.add("0.0");
//		a9.add("20.0");

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("false");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode229);
		a10.add("20.0");
		a10.add("0.0");
		a10.add("0.0");
		a10.add("0.0");
		a10.add("0.0");
		a10.add("20.0");



		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("false");
		a11.add(userId);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);
		a11.add("0.0");
		a11.add("20.0");
		a11.add("0.0");
		a11.add("0.0");
		a11.add("0.0");
		a11.add("20.0");
		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("false");
		a12.add(userId);
		a12.add(rngConstants.AndroidOS);
		a12.add(rngConstants.versionCode229);
		a12.add("0.0");
		a12.add("0.0");
		a12.add("20.0");
		a12.add("0.0");
		a12.add("0.0");
		a12.add("20.0");

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("false");
		a13.add(userId);
		a13.add(rngConstants.AndroidOS);
		a13.add(rngConstants.versionCode229);
		a13.add("0.0");
		a13.add("0.0");
		a13.add("0.0");
		a13.add("20.0");
		a13.add("0.0");
		a13.add("20.0");

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("false");
		a14.add(userId);
		a14.add(rngConstants.AndroidOS);
		a14.add(rngConstants.versionCode229);
		a14.add("0.0");
		a14.add("0.0");
		a14.add("0.0");
		a14.add("0.0");
		a14.add("20.0");
		a14.add("20.0");

		ArrayList a15 = new ArrayList();
		a15.add(rngConstants.minCartAmount);
		a15.add(restId);
		a15.add("false");
		a15.add(userId);
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);
		a15.add("20.0");
		a15.add("20.0");
		a15.add("0.0");
		a15.add("0.0");
		a15.add("0.0");
		a15.add("40.0");


		ArrayList a16 = new ArrayList();
		a16.add(rngConstants.minCartAmount);
		a16.add(restId);
		a16.add("false");
		a16.add(userId);
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		a16.add("0.0");
		a16.add("20.0");
		a16.add("20.0");
		a16.add("0.0");
		a16.add("0.0");
		a16.add("40.0");


		ArrayList a17 = new ArrayList();
		a17.add(rngConstants.minCartAmount);
		a17.add(restId);
		a17.add("false");
		a17.add(userId);
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		a17.add("0.0");
		a17.add("0.0");
		a17.add("20.0");
		a17.add("20.0");
		a17.add("0.0");
		a17.add("40.0");


		ArrayList a18 = new ArrayList();
		a18.add(rngConstants.minCartAmount);
		a18.add(restId);
		a18.add("false");
		a18.add(userId);
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);
		a18.add("0.0");
		a18.add("0.0");
		a18.add("0.0");
		a18.add("20.0");
		a18.add("20.0");
		a18.add("40.0");

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add("100");
		a19.add(userId);
		a19.add("false");
		a19.add(rngConstants.AndroidOS);
		a19.add(rngConstants.versionCode229);
		a19.add("20.0");
		a19.add("0.0");
		a19.add("0.0");
		a19.add("0.0");
		a19.add("0.0");
		a19.add("20.0");


		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add("100");
		a20.add(userId);
		a20.add("false");
		a20.add(rngConstants.AndroidOS);
		a20.add(rngConstants.versionCode229);
		a20.add("0.0");
		a20.add("20.0");
		a20.add("0.0");
		a20.add("0.0");
		a20.add("0.0");
		a20.add("20.0");


		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add("100");
		a21.add(userId);
		a21.add("false");
		a21.add(rngConstants.AndroidOS);
		a21.add(rngConstants.versionCode229);
		a21.add("0.0");
		a21.add("0.0");
		a21.add("20.0");
		a21.add("0.0");
		a21.add("0.0");
		a21.add("20.0");

		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add("100");
		a22.add(userId);
		a22.add("false");
		a22.add(rngConstants.AndroidOS);
		a22.add(rngConstants.versionCode229);
		a22.add("0.0");
		a22.add("0.0");
		a22.add("0.0");
		a22.add("20.0");
		a22.add("0.0");
		a22.add("20.0");


		ArrayList a23 = new ArrayList();
		a23.add(restId);
		a23.add(categoryId);
		a23.add(subCategoryId);
		a23.add(itemId);
		a23.add(count);
		a23.add(price);
		a23.add("100");
		a23.add(userId);
		a23.add("false");
		a23.add(rngConstants.AndroidOS);
		a23.add(rngConstants.versionCode229);
		a23.add("0.0");
		a23.add("0.0");
		a23.add("0.0");
		a23.add("0.0");
		a23.add("20.0");
		a23.add("20.0");


		ArrayList a24 = new ArrayList();
		a24.add(restId);
		a24.add(categoryId);
		a24.add(subCategoryId);
		a24.add(itemId);
		a24.add(count);
		a24.add(price);
		a24.add("100");
		a24.add(userId);
		a24.add("false");
		a24.add(rngConstants.AndroidOS);
		a24.add(rngConstants.versionCode229);
		a24.add("20.0");
		a24.add("20.0");
		a24.add("0.0");
		a24.add("0.0");
		a24.add("0.0");
		a24.add("40.0");


		ArrayList a25 = new ArrayList();
		a25.add(restId);
		a25.add(categoryId);
		a25.add(subCategoryId);
		a25.add(itemId);
		a25.add(count);
		a25.add(price);
		a25.add("100");
		a25.add(userId);
		a25.add("false");
		a25.add(rngConstants.AndroidOS);
		a25.add(rngConstants.versionCode229);
		a25.add("0.0");
		a25.add("20.0");
		a25.add("20.0");
		a25.add("0.0");
		a25.add("0.0");
		a25.add("40.0");



		ArrayList a26 = new ArrayList();
		a26.add(restId);
		a26.add(categoryId);
		a26.add(subCategoryId);
		a26.add(itemId);
		a26.add(count);
		a26.add(price);
		a26.add("100");
		a26.add(userId);
		a26.add("false");
		a26.add(rngConstants.AndroidOS);
		a26.add(rngConstants.versionCode229);
		a26.add("0.0");
		a26.add("0.0");
		a26.add("20.0");
		a26.add("20.0");
		a26.add("0.0");
		a26.add("40.0");



		ArrayList a27 = new ArrayList();
		a27.add(restId);
		a27.add(categoryId);
		a27.add(subCategoryId);
		a27.add(itemId);
		a27.add(count);
		a27.add(price);
		a27.add("100");
		a27.add(userId);
		a27.add("false");
		a27.add(rngConstants.AndroidOS);
		a27.add(rngConstants.versionCode229);
		a27.add("0.0");
		a27.add("0.0");
		a27.add("0.0");
		a27.add("20.0");
		a27.add("20.0");
		a27.add("40.0");
//		ArrayList a16 = new ArrayList();
//		a16.add(restId);
//		a16.add(categoryId);
//		a16.add(subCategoryId);
//		a16.add(itemId);
//		a16.add(count);
//		a16.add(price);
//		a16.add("100");
//		a16.add(userId);
//		a16.add("false");
//		a16.add(rngConstants.AndroidOS);
//		a16.add(rngConstants.versionCode229);
//		a16.add("0.0");
//		a16.add("20.0");
//		ArrayList a17 = new ArrayList();
//		a17.add(restId);
//		a17.add(categoryId);
//		a17.add(subCategoryId);
//		a17.add(itemId);
//		a17.add(count);
//		a17.add(price);
//		a17.add("100");
//		a17.add(userId);
//		a17.add("true");
//		a17.add(rngConstants.AndroidOS);
//		a17.add(rngConstants.versionCode229);
//		a17.add("20.0");
//		a17.add("20.0");
//		ArrayList a18 = new ArrayList();
//		a18.add(restId);
//		a18.add(categoryId);
//		a18.add(subCategoryId);
//		a18.add(itemId);
//		a18.add(count);
//		a18.add(price);
//		a18.add("100");
//		a18.add(userId);
//		a18.add("true");
//		a18.add(rngConstants.AndroidOS);
//		a18.add(rngConstants.versionCode200);
//		a18.add("0.0");
//		a18.add("0.0");

		return new Object[][] { { "list", a1,true }, { "list", a2,true }, { "list", a3,true }, { "list", a4,true },{ "list", a5,true },{ "list", a6,true }, { "list", a7,true }, { "list", a8,true }, { "list", a9,true }, { "menu", a10,true },
                { "menu", a11,true }, { "menu", a12,true }, { "menu", a13,true }, { "menu", a14,true }, { "menu", a15,true }, { "menu", a16,true },{ "menu", a17,true }, { "menu", a18,true }

				, { "cart", a19,true }, { "cart", a20,true }, { "cart", a21,true }, { "cart", a22,true }, { "cart", a23,true }, { "cart", a24,true }, { "cart", a25,true }, { "cart", a26,true }, { "cart", a27,true }
		};
	}



	@DataProvider(name = "FreeDeliveryTD_EvaluationWithSpecialFeeWithoutSURGEFEE")
	public Object[][] FreeDeliveryTD_EvaluationWithSpecialFeeWithoutSURGEFEE() throws IOException {

		String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
		String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
		String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
		String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
		String price = "500";
		String cartAmount = price;
		HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("0", restId, false, false, false,
				"ZERO_DAYS_DORMANT",false);

		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);
		a1.add("20.0");
		a1.add("0.0");
		a1.add("0.0");
		a1.add("0.0");
		a1.add("0.0");
		a1.add("20.0");

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		a2.add("0.0");
		a2.add("20.0");
		a2.add("0.0");
		a2.add("0.0");
		a2.add("0.0");
		a2.add("20.0");

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("false");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);
		a3.add("0.0");
		a3.add("0.0");
		a3.add("20.0");
		a3.add("0.0");
		a3.add("0.0");
		a3.add("20.0");



		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("false");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);
		a4.add("0.0");
		a4.add("0.0");
		a4.add("0.0");
		a4.add("20.0");
		a4.add("0.0");
		a4.add("20.0");



		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("false");
		a5.add(userId);
		a5.add(rngConstants.AndroidOS);
		a5.add(rngConstants.versionCode229);
		a5.add("0.0");
		a5.add("0.0");
		a5.add("0.0");
		a5.add("0.0");
		a5.add("20.0");
		a5.add("20.0");


		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("false");
		a6.add(userId);
		a6.add(rngConstants.AndroidOS);
		a6.add(rngConstants.versionCode229);
		a6.add("20.0");
		a6.add("20.0");
		a6.add("0.0");
		a6.add("0.0");
		a6.add("0.0");
		a6.add("40.0");



		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("false");
		a7.add(userId);
		a7.add(rngConstants.AndroidOS);
		a7.add(rngConstants.versionCode229);
		a7.add("0.0");
		a7.add("20.0");
		a7.add("20.0");
		a7.add("0.0");
		a7.add("0.0");
		a7.add("40.0");


		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);
		a8.add("0.0");
		a8.add("0.0");
		a8.add("20.0");
		a8.add("20.0");
		a8.add("0.0");
		a8.add("40.0");


		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("false");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);
		a9.add("0.0");
		a9.add("0.0");
		a9.add("20.0");
		a9.add("20.0");
		a9.add("0.0");
		a9.add("40.0");


		//	ArrayList a3 = new ArrayList();
//		a1.add(restId);
//		a1.add("false");
//		a1.add(userId);
//		a1.add(rngConstants.AndroidOS);
//		a1.add(rngConstants.versionCode229);
//		a1.add("0.0");
//		a1.add("20.0");
//		a1.add("0.0");
//		a1.add("0.0");
//		a1.add("0.0");
//		a1.add("20.0");


//		ArrayList a3 = new ArrayList();
//		a3.add(restId);
//		a3.add("false");
//		a3.add(userId);
//		a3.add(rngConstants.AndroidOS);
//		a3.add(rngConstants.versionCode229);
//		a3.add("20.0");
//		a3.add("20.0");
//		ArrayList a4 = new ArrayList();
//		a4.add(restId);
//		a4.add("false");
//		a4.add(userId);
//		a4.add(rngConstants.AndroidOS);
//		a4.add(rngConstants.versionCode229);
//		a4.add("0.0");
//		a4.add("0.0");

//		ArrayList a8 = new ArrayList();
//		a8.add(rngConstants.minCartAmount);
//		a8.add(restId);
//		a8.add("false");
//		a8.add(userId);
//		a8.add(rngConstants.AndroidOS);
//		a8.add(rngConstants.versionCode229);
//		a8.add("20.0");
//		a8.add("0.0");
//
//		ArrayList a9 = new ArrayList();
//		a9.add(rngConstants.minCartAmount);
//		a9.add(restId);
//		a9.add("false");
//		a9.add(userId);
//		a9.add(rngConstants.AndroidOS);
//		a9.add(rngConstants.versionCode229);
//		a9.add("0.0");
//		a9.add("20.0");

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("false");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode229);
		a10.add("20.0");
		a10.add("0.0");
		a10.add("0.0");
		a10.add("0.0");
		a10.add("0.0");
		a10.add("20.0");



		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("false");
		a11.add(userId);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);
		a11.add("0.0");
		a11.add("20.0");
		a11.add("0.0");
		a11.add("0.0");
		a11.add("0.0");
		a11.add("20.0");
		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("false");
		a12.add(userId);
		a12.add(rngConstants.AndroidOS);
		a12.add(rngConstants.versionCode229);
		a12.add("0.0");
		a12.add("0.0");
		a12.add("20.0");
		a12.add("0.0");
		a12.add("0.0");
		a12.add("20.0");

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("false");
		a13.add(userId);
		a13.add(rngConstants.AndroidOS);
		a13.add(rngConstants.versionCode229);
		a13.add("0.0");
		a13.add("0.0");
		a13.add("0.0");
		a13.add("20.0");
		a13.add("0.0");
		a13.add("20.0");

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("false");
		a14.add(userId);
		a14.add(rngConstants.AndroidOS);
		a14.add(rngConstants.versionCode229);
		a14.add("0.0");
		a14.add("0.0");
		a14.add("0.0");
		a14.add("0.0");
		a14.add("20.0");
		a14.add("20.0");

		ArrayList a15 = new ArrayList();
		a15.add(rngConstants.minCartAmount);
		a15.add(restId);
		a15.add("false");
		a15.add(userId);
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);
		a15.add("20.0");
		a15.add("20.0");
		a15.add("0.0");
		a15.add("0.0");
		a15.add("0.0");
		a15.add("40.0");


		ArrayList a16 = new ArrayList();
		a16.add(rngConstants.minCartAmount);
		a16.add(restId);
		a16.add("false");
		a16.add(userId);
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		a16.add("0.0");
		a16.add("20.0");
		a16.add("20.0");
		a16.add("0.0");
		a16.add("0.0");
		a16.add("40.0");


		ArrayList a17 = new ArrayList();
		a17.add(rngConstants.minCartAmount);
		a17.add(restId);
		a17.add("false");
		a17.add(userId);
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		a17.add("0.0");
		a17.add("0.0");
		a17.add("20.0");
		a17.add("20.0");
		a17.add("0.0");
		a17.add("40.0");


		ArrayList a18 = new ArrayList();
		a18.add(rngConstants.minCartAmount);
		a18.add(restId);
		a18.add("false");
		a18.add(userId);
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);
		a18.add("0.0");
		a18.add("0.0");
		a18.add("0.0");
		a18.add("20.0");
		a18.add("20.0");
		a18.add("40.0");

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add("100");
		a19.add(userId);
		a19.add("false");
		a19.add(rngConstants.AndroidOS);
		a19.add(rngConstants.versionCode229);
		a19.add("20.0");
		a19.add("0.0");
		a19.add("0.0");
		a19.add("0.0");
		a19.add("0.0");
		a19.add("20.0");


		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add("100");
		a20.add(userId);
		a20.add("false");
		a20.add(rngConstants.AndroidOS);
		a20.add(rngConstants.versionCode229);
		a20.add("0.0");
		a20.add("20.0");
		a20.add("0.0");
		a20.add("0.0");
		a20.add("0.0");
		a20.add("20.0");


		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add("100");
		a21.add(userId);
		a21.add("false");
		a21.add(rngConstants.AndroidOS);
		a21.add(rngConstants.versionCode229);
		a21.add("0.0");
		a21.add("0.0");
		a21.add("20.0");
		a21.add("0.0");
		a21.add("0.0");
		a21.add("20.0");

		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add("100");
		a22.add(userId);
		a22.add("false");
		a22.add(rngConstants.AndroidOS);
		a22.add(rngConstants.versionCode229);
		a22.add("0.0");
		a22.add("0.0");
		a22.add("0.0");
		a22.add("20.0");
		a22.add("0.0");
		a22.add("20.0");


		ArrayList a23 = new ArrayList();
		a23.add(restId);
		a23.add(categoryId);
		a23.add(subCategoryId);
		a23.add(itemId);
		a23.add(count);
		a23.add(price);
		a23.add("100");
		a23.add(userId);
		a23.add("false");
		a23.add(rngConstants.AndroidOS);
		a23.add(rngConstants.versionCode229);
		a23.add("0.0");
		a23.add("0.0");
		a23.add("0.0");
		a23.add("0.0");
		a23.add("20.0");
		a23.add("20.0");


		ArrayList a24 = new ArrayList();
		a24.add(restId);
		a24.add(categoryId);
		a24.add(subCategoryId);
		a24.add(itemId);
		a24.add(count);
		a24.add(price);
		a24.add("100");
		a24.add(userId);
		a24.add("false");
		a24.add(rngConstants.AndroidOS);
		a24.add(rngConstants.versionCode229);
		a24.add("20.0");
		a24.add("20.0");
		a24.add("0.0");
		a24.add("0.0");
		a24.add("0.0");
		a24.add("40.0");


		ArrayList a25 = new ArrayList();
		a25.add(restId);
		a25.add(categoryId);
		a25.add(subCategoryId);
		a25.add(itemId);
		a25.add(count);
		a25.add(price);
		a25.add("100");
		a25.add(userId);
		a25.add("false");
		a25.add(rngConstants.AndroidOS);
		a25.add(rngConstants.versionCode229);
		a25.add("0.0");
		a25.add("20.0");
		a25.add("20.0");
		a25.add("0.0");
		a25.add("0.0");
		a25.add("40.0");



		ArrayList a26 = new ArrayList();
		a26.add(restId);
		a26.add(categoryId);
		a26.add(subCategoryId);
		a26.add(itemId);
		a26.add(count);
		a26.add(price);
		a26.add("100");
		a26.add(userId);
		a26.add("false");
		a26.add(rngConstants.AndroidOS);
		a26.add(rngConstants.versionCode229);
		a26.add("0.0");
		a26.add("0.0");
		a26.add("20.0");
		a26.add("20.0");
		a26.add("0.0");
		a26.add("40.0");



		ArrayList a27 = new ArrayList();
		a27.add(restId);
		a27.add(categoryId);
		a27.add(subCategoryId);
		a27.add(itemId);
		a27.add(count);
		a27.add(price);
		a27.add("100");
		a27.add(userId);
		a27.add("false");
		a27.add(rngConstants.AndroidOS);
		a27.add(rngConstants.versionCode229);
		a27.add("0.0");
		a27.add("0.0");
		a27.add("0.0");
		a27.add("20.0");
		a27.add("20.0");
		a27.add("40.0");
//		ArrayList a16 = new ArrayList();
//		a16.add(restId);
//		a16.add(categoryId);
//		a16.add(subCategoryId);
//		a16.add(itemId);
//		a16.add(count);
//		a16.add(price);
//		a16.add("100");
//		a16.add(userId);
//		a16.add("false");
//		a16.add(rngConstants.AndroidOS);
//		a16.add(rngConstants.versionCode229);
//		a16.add("0.0");
//		a16.add("20.0");
//		ArrayList a17 = new ArrayList();
//		a17.add(restId);
//		a17.add(categoryId);
//		a17.add(subCategoryId);
//		a17.add(itemId);
//		a17.add(count);
//		a17.add(price);
//		a17.add("100");
//		a17.add(userId);
//		a17.add("true");
//		a17.add(rngConstants.AndroidOS);
//		a17.add(rngConstants.versionCode229);
//		a17.add("20.0");
//		a17.add("20.0");
//		ArrayList a18 = new ArrayList();
//		a18.add(restId);
//		a18.add(categoryId);
//		a18.add(subCategoryId);
//		a18.add(itemId);
//		a18.add(count);
//		a18.add(price);
//		a18.add("100");
//		a18.add(userId);
//		a18.add("true");
//		a18.add(rngConstants.AndroidOS);
//		a18.add(rngConstants.versionCode200);
//		a18.add("0.0");
//		a18.add("0.0");

		return new Object[][] { { "list", a1,false }, { "list", a2,false }, { "list", a3,true }, { "list", a4,true },{ "list", a5,true },{ "list", a6,false }, { "list", a7,false }, { "list", a8,true }, { "list", a9,true }, { "menu", a10,false },
				{ "menu", a11,false }, { "menu", a12,true }, { "menu", a13,true }, { "menu", a14,true }, { "menu", a15,false }, { "menu", a16,false },{ "menu", a17,true }, { "menu", a18,true }

				, { "cart", a19,false }, { "cart", a20,false }, { "cart", a21,true }, { "cart", a22,true }, { "cart", a23,true }, { "cart", a24,false }, { "cart", a25,false }, { "cart", a26,true }, { "cart", a27,true }
		};
	}























	@DataProvider(name = "PercentageTD_EvaluationWithSwiggyFirstOrderItemLevel")
	public Object[][] PercentageTD_EvaluationWithSwiggyFirstOrderItemLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithFirstOrderRestrictionAtItemLevel();

		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add(CartAmount);
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1, false }, { "list", a2, true }, { "list", a3, true }, { "list", a4, true },
				{ "list", a5, true }, { "list", a6, true }, { "list", a7, true }, { "menu", a8, false },
				{ "menu", a9, true }, { "menu", a10, true }, { "menu", a11, true }, { "menu", a12, true },
				{ "menu", a13, true }, { "menu", a14, true }, { "cart", a15, false }, { "cart", a17, true } };
	}

	@DataProvider(name = "PercentageTD_EvaluationWithRestaurantFirstOrderItemLevel")
	public Object[][] PercentageTD_EvaluationWithRestaurantFirstOrderItemLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithRestaurantFirstOrderRestrictionAtItemLevel();
		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.WebOS);
		a2.add(rngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(rngConstants.minCartAmount);
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add("10");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.WebOS);
		a17.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();

		rngHelper.DormantUser(userId2, restId, "10");

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("true");
		a8.add(userId2);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("true");
		a9.add(userId2);
		a9.add(rngConstants.WebOS);
		a9.add(rngConstants.versionCode200);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add("true");
		a10.add(userId2);
		a10.add(rngConstants.IosOS);
		a10.add(rngConstants.versionCode250);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add("10");
		a18.add(userId2);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("true");
		a19.add(rngConstants.WebOS);
		a19.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1, true }, { "list", a2, true }, { "list", a3, true }, { "menu", a4, true },
				{ "menu", a5, true }, { "menu", a6, true }, { "cart", a17, true }, { "cart", a16, true },
				{ "list", a8, false }, { "list", a9, false }, { "list", a10, false }, { "menu", a11, false },
				{ "menu", a12, false }, { "menu", a13, false }, { "cart", a18, false }, { "cart", a19, false }, };

	}

	public Object[][] PercentageTD_EvaluationWithTimeSlotRestrictionAtItemLevel_Set1() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithTimeSlotRestrictionAtItemLevel("100", "200", 0, 15);
		String restId = (String) Keys.get("restId");
		String isValidforCurrentTime = (String) Keys.get("isValidforCurrentTime");

		String itemId = Keys.get(RngConstants.itemid).toString();
		String categoryId = Keys.get(RngConstants.catid).toString();
		String subCategoryId = Keys.get(RngConstants.subcatid).toString();
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String minCartAmount = Keys.get("minCartAmount").toString();
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);
		a1.add(isValidforCurrentTime);
		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		a2.add(isValidforCurrentTime);
		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);
		a3.add(isValidforCurrentTime);
		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);
		a4.add(isValidforCurrentTime);
		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);
		a5.add(isValidforCurrentTime);
		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);
		a6.add(isValidforCurrentTime);
		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);
		a7.add(isValidforCurrentTime);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);
		a8.add(isValidforCurrentTime);
		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);
		a9.add(isValidforCurrentTime);
		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);
		a10.add(isValidforCurrentTime);
		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);
		a11.add(isValidforCurrentTime);
		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);
		a12.add(isValidforCurrentTime);
		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);
		a13.add(isValidforCurrentTime);
		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);
		a14.add(isValidforCurrentTime);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("10");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);
		a15.add(minCartAmount);
		a15.add(isValidforCurrentTime);
		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		a16.add(minCartAmount);
		a16.add(isValidforCurrentTime);
		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		a17.add(minCartAmount);
		a17.add(isValidforCurrentTime);
		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add(CartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);
		a18.add(minCartAmount);
		a18.add(isValidforCurrentTime);
		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);
		a19.add(minCartAmount);
		a19.add(isValidforCurrentTime);
		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(CartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);
		a20.add(minCartAmount);
		a20.add(isValidforCurrentTime);
		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(CartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);
		a21.add(minCartAmount);
		a21.add(isValidforCurrentTime);
		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(CartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);
		a22.add(minCartAmount);
		a22.add(isValidforCurrentTime);

		Object[][] obj1 = new Object[][] { { "list", a1 }, { "list", a2 }, { "list", a3 }, { "list", a4 },
				{ "list", a5 }, { "list", a6 }, { "list", a7 }, { "menu", a8 }, { "menu", a9 }, { "menu", a10 },
				{ "menu", a11 }, { "menu", a12 }, { "menu", a13 }, { "menu", a14 }, { "cart", a15 }, { "cart", a16 },
				{ "cart", a17 }, { "cart", a18 }, { "cart", a19 }, { "cart", a20 }, { "cart", a21 }, { "cart", a22 } };
		return obj1;
	}

	public Object[][] PercentageTD_EvaluationWithTimeSlotRestrictionAtItemLevel_Set2() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithTimeSlotRestrictionAtItemLevel("100", "200", 1, 15);
		String restId = (String) Keys.get("restId");
		String isValidforCurrentTime = (String) Keys.get("isValidforCurrentTime");
		String itemId = Keys.get(RngConstants.itemid).toString();
		String categoryId = Keys.get(RngConstants.catid).toString();
		String subCategoryId = Keys.get(RngConstants.subcatid).toString();
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String minCartAmount = Keys.get("minCartAmount").toString();
		String userId = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("userId").toString()
				.replace("\"", "").trim();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);
		a1.add(isValidforCurrentTime);
		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);
		a2.add(isValidforCurrentTime);
		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);
		a3.add(isValidforCurrentTime);
		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);
		a4.add(isValidforCurrentTime);
		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);
		a5.add(isValidforCurrentTime);
		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);
		a6.add(isValidforCurrentTime);
		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);
		a7.add(isValidforCurrentTime);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);
		a8.add(isValidforCurrentTime);
		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);
		a9.add(isValidforCurrentTime);
		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);
		a10.add(isValidforCurrentTime);
		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);
		a11.add(isValidforCurrentTime);
		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);
		a12.add(isValidforCurrentTime);
		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);
		a13.add(isValidforCurrentTime);
		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);
		a14.add(isValidforCurrentTime);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("10");
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);
		a15.add(minCartAmount);
		a15.add(isValidforCurrentTime);
		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add(minCartAmount);
		a16.add(userId);
		a16.add("false");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);
		a16.add(minCartAmount);
		a16.add(isValidforCurrentTime);
		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);
		a17.add(minCartAmount);
		a17.add(isValidforCurrentTime);
		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add(CartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode200);
		a18.add(minCartAmount);
		a18.add(isValidforCurrentTime);
		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId);
		a19.add("true");
		a19.add(rngConstants.IosOS);
		a19.add(rngConstants.versionCode200);
		a19.add(minCartAmount);
		a19.add(isValidforCurrentTime);
		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(CartAmount);
		a20.add(userId);
		a20.add("true");
		a20.add(rngConstants.IosOS);
		a20.add(rngConstants.versionCode250);
		a20.add(minCartAmount);
		a20.add(isValidforCurrentTime);
		ArrayList a21 = new ArrayList();
		a21.add(restId);
		a21.add(categoryId);
		a21.add(subCategoryId);
		a21.add(itemId);
		a21.add(count);
		a21.add(price);
		a21.add(CartAmount);
		a21.add(userId);
		a21.add("true");
		a21.add(rngConstants.WebOS);
		a21.add(rngConstants.versionCode250);
		a21.add(minCartAmount);
		a21.add(isValidforCurrentTime);
		ArrayList a22 = new ArrayList();
		a22.add(restId);
		a22.add(categoryId);
		a22.add(subCategoryId);
		a22.add(itemId);
		a22.add(count);
		a22.add(price);
		a22.add(CartAmount);
		a22.add(userId);
		a22.add("true");
		a22.add(rngConstants.WebOS);
		a22.add(rngConstants.versionCode200);
		a22.add(minCartAmount);
		a22.add(isValidforCurrentTime);

		Object[][] obj2 = new Object[][] { { "list", a1 }, { "list", a2 }, { "list", a3 }, { "list", a4 },
				{ "list", a5 }, { "list", a6 }, { "list", a7 }, { "menu", a8 }, { "menu", a9 }, { "menu", a10 },
				{ "menu", a11 }, { "menu", a12 }, { "menu", a13 }, { "menu", a14 }, { "cart", a15 }, { "cart", a16 },
				{ "cart", a17 }, { "cart", a18 }, { "cart", a19 }, { "cart", a20 }, { "cart", a21 }, { "cart", a22 } };
		return obj2;
	}

	@DataProvider(name = "PercentageTD_EvaluationWithTimeSlotRestrictionAtItemLevel")
	public Object[][] PercentageTD_EvaluationWithTimeSlotRestrictionAtItemLevel() throws IOException {

		Object[][] b1 = PercentageTD_EvaluationWithTimeSlotRestrictionAtItemLevel_Set1();
		Object[][] b2 = PercentageTD_EvaluationWithTimeSlotRestrictionAtItemLevel_Set2();
		List<Object[]> result = new ArrayList();

		result.addAll(Arrays.asList(b1));
		result.addAll(Arrays.asList(b2));

		return result.toArray(new Object[result.size()][]);

	}

	@DataProvider(name = "PercentageTD_EvaluationWithUserRestrictionAtItemLevel")
	public Object[][] PercentageTD_EvaluationWithUserRestrictionAtItemLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithUserRestrictionAtItemLevel();

		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String userId2 = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("false");
		a5.add(userId);
		a5.add(rngConstants.AndroidOS);
		a5.add(rngConstants.versionCode229);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.AndroidOS);
		a6.add(rngConstants.versionCode229);

		ArrayList a7 = new ArrayList();
		a7.add(rngConstants.minCartAmount);
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.AndroidOS);
		a7.add(rngConstants.versionCode200);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("true");
		a8.add(userId);
		a8.add(rngConstants.IosOS);
		a8.add(rngConstants.versionCode200);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add(categoryId);
		a9.add(subCategoryId);
		a9.add(itemId);
		a9.add(count);
		a9.add(price);
		a9.add(CartAmount);
		a9.add(userId);
		a9.add("false");
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add(categoryId);
		a10.add(subCategoryId);
		a10.add(itemId);
		a10.add(count);
		a10.add(price);
		a10.add(CartAmount);
		a10.add(userId);
		a10.add("false");
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode229);

		rngHelper.userMappingTD(userId2, tid);

		ArrayList a11 = new ArrayList();
		a11.add(restId);
		a11.add("false");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.AndroidOS);
		a12.add(rngConstants.versionCode229);

		ArrayList a13 = new ArrayList();
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.AndroidOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(restId);
		a14.add("true");
		a14.add(userId2);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode200);

		ArrayList a15 = new ArrayList();
		a15.add(rngConstants.minCartAmount);
		a15.add(restId);
		a15.add("false");
		a15.add(userId2);
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a16 = new ArrayList();
		a16.add(rngConstants.minCartAmount);
		a16.add(restId);
		a16.add("true");
		a16.add(userId2);
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(rngConstants.minCartAmount);
		a17.add(restId);
		a17.add("true");
		a17.add(userId2);
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(rngConstants.minCartAmount);
		a18.add(restId);
		a18.add("true");
		a18.add(userId2);
		a18.add(rngConstants.IosOS);
		a18.add(rngConstants.versionCode200);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("false");
		a19.add(rngConstants.AndroidOS);
		a19.add(rngConstants.versionCode229);

		ArrayList a20 = new ArrayList();
		a20.add(restId);
		a20.add(categoryId);
		a20.add(subCategoryId);
		a20.add(itemId);
		a20.add(count);
		a20.add(price);
		a20.add(CartAmount);
		a20.add(userId2);
		a20.add("false");
		a20.add(rngConstants.AndroidOS);
		a20.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1, false }, { "list", a2, false }, { "list", a3, false },
				{ "list", a4, false }, { "menu", a5, false }, { "menu", a6, false }, { "menu", a7, false },
				{ "menu", a8, false }, { "cart", a9, false }, { "cart", a10, false }, { "list", a11, true },
				{ "list", a12, true }, { "list", a13, true }, { "list", a14, true }, { "menu", a15, true },
				{ "menu", a16, true }, { "menu", a17, true }, { "menu", a18, true }, { "cart", a19, true },
				{ "cart", a20, true } };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithThirtyDayDormantAtItemLevel")
	public Object[][] PercentageTD_EvaluationWithThirtyDayDormantAtItemLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithThirtyDayDormantAtItemLevel();
		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.WebOS);
		a2.add(rngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(rngConstants.minCartAmount);
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add("10");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.WebOS);
		a17.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();

		rngHelper.DormantUser(userId2, restId, "30");

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("true");
		a8.add(userId2);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("true");
		a9.add(userId2);
		a9.add(rngConstants.WebOS);
		a9.add(rngConstants.versionCode200);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add("true");
		a10.add(userId2);
		a10.add(rngConstants.IosOS);
		a10.add(rngConstants.versionCode250);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add("10");
		a18.add(userId2);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("true");
		a19.add(rngConstants.WebOS);
		a19.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1, false }, { "list", a2, false }, { "list", a3, false },
				{ "menu", a4, false }, { "menu", a5, false }, { "menu", a6, false }, { "cart", a17, false },
				{ "cart", a16, false }, { "list", a8, true }, { "list", a9, true }, { "list", a10, true },
				{ "menu", a11, true }, { "menu", a12, true }, { "menu", a13, true }, { "cart", a18, true },
				{ "cart", a19, true }, };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithSixtyDayDormantAtItemLevel")
	public Object[][] PercentageTD_EvaluationWithSixtyDayDormantAtItemLevel() throws IOException {
		HashMap Keys = rngHelper.createPercentageWithSixtyDayDormantAtItemLevel();
		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.WebOS);
		a2.add(rngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(rngConstants.minCartAmount);
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add("10");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.WebOS);
		a17.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();

		rngHelper.DormantUser(userId2, restId, "60");

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("true");
		a8.add(userId2);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("true");
		a9.add(userId2);
		a9.add(rngConstants.WebOS);
		a9.add(rngConstants.versionCode200);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add("true");
		a10.add(userId2);
		a10.add(rngConstants.IosOS);
		a10.add(rngConstants.versionCode250);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add("10");
		a18.add(userId2);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("true");
		a19.add(rngConstants.WebOS);
		a19.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1, false }, { "list", a2, false }, { "list", a3, false },
				{ "menu", a4, false }, { "menu", a5, false }, { "menu", a6, false }, { "cart", a17, false },
				{ "cart", a16, false }, { "list", a8, true }, { "list", a9, true }, { "list", a10, true },
				{ "menu", a11, true }, { "menu", a12, true }, { "menu", a13, true }, { "cart", a18, true },
				{ "cart", a19, true }, };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithNinetyDayDormantAtItemLevel")
	public Object[][] PercentageTD_EvaluationWithNinetyDayDormantAtItemLevel() throws IOException {

		HashMap Keys = rngHelper.createPercentageWithNinetyDayDormantAtItemLevel();
		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();
		String tid = Keys.get("TDID").toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.WebOS);
		a2.add(rngConstants.versionCode200);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.IosOS);
		a3.add(rngConstants.versionCode250);

		ArrayList a4 = new ArrayList();
		a4.add(rngConstants.minCartAmount);
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.AndroidOS);
		a4.add(rngConstants.versionCode229);

		ArrayList a5 = new ArrayList();
		a5.add(rngConstants.minCartAmount);
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(rngConstants.minCartAmount);
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a16 = new ArrayList();
		a16.add(restId);
		a16.add(categoryId);
		a16.add(subCategoryId);
		a16.add(itemId);
		a16.add(count);
		a16.add(price);
		a16.add("10");
		a16.add(userId);
		a16.add("true");
		a16.add(rngConstants.AndroidOS);
		a16.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.WebOS);
		a17.add(rngConstants.versionCode229);

		String userId2 = new Integer(rm.nextInt()).toString();

		rngHelper.DormantUser(userId2, restId, "90");

		ArrayList a8 = new ArrayList();
		a8.add(restId);
		a8.add("true");
		a8.add(userId2);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(restId);
		a9.add("true");
		a9.add(userId2);
		a9.add(rngConstants.WebOS);
		a9.add(rngConstants.versionCode200);

		ArrayList a10 = new ArrayList();
		a10.add(restId);
		a10.add("true");
		a10.add(userId2);
		a10.add(rngConstants.IosOS);
		a10.add(rngConstants.versionCode250);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId2);
		a11.add(rngConstants.AndroidOS);
		a11.add(rngConstants.versionCode229);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId2);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId2);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId);
		a18.add(count);
		a18.add(price);
		a18.add("10");
		a18.add(userId2);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		ArrayList a19 = new ArrayList();
		a19.add(restId);
		a19.add(categoryId);
		a19.add(subCategoryId);
		a19.add(itemId);
		a19.add(count);
		a19.add(price);
		a19.add(CartAmount);
		a19.add(userId2);
		a19.add("true");
		a19.add(rngConstants.WebOS);
		a19.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1, false }, { "list", a2, false }, { "list", a3, false },
				{ "menu", a4, false }, { "menu", a5, false }, { "menu", a6, false }, { "cart", a17, false },
				{ "cart", a16, false }, { "list", a8, true }, { "list", a9, true }, { "list", a10, true },
				{ "menu", a11, true }, { "menu", a12, true }, { "menu", a13, true }, { "cart", a18, true },
				{ "cart", a19, true }, };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithNoMinCartItemLevel")
	public Object[][] PercentageTD_EvaluationWithNoMinCartItemLevel() throws IOException {

		HashMap Keys = rngHelper.createPercentageWithNoMinCartAtItemLevel();

		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId2 = new Integer(rm.nextInt()).toString();
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add(CartAmount);
		a15.add(userId);
		a15.add("false");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);

		ArrayList a18 = new ArrayList();
		a18.add(restId);
		a18.add(categoryId);
		a18.add(subCategoryId);
		a18.add(itemId2);
		a18.add(count);
		a18.add(price);
		a18.add(CartAmount);
		a18.add(userId);
		a18.add("true");
		a18.add(rngConstants.AndroidOS);
		a18.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1, true }, { "list", a2, true }, { "list", a3, true }, { "list", a4, true },
				{ "list", a5, true }, { "list", a6, true }, { "list", a7, true }, { "menu", a8, true },
				{ "menu", a9, true }, { "menu", a10, true }, { "menu", a11, true }, { "menu", a12, true },
				{ "menu", a13, true }, { "menu", a14, true }, { "cart", a15, true }, { "cart", a17, true },
				{ "cart2", a18, false } };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithMinCartItemLevel")
	public Object[][] PercentageTD_EvaluationWithMinCartItemLevel() throws IOException {

		HashMap Keys = rngHelper.createPercentageWithMinCartAtItemLevel();

		String restId = (String) Keys.get("restId");
		String categoryId = (String) Keys.get("catId");
		String subCategoryId = (String) Keys.get("subCatId");
		String itemId = (String) Keys.get("itemId");
		String count = "1";
		String price = "200";
		String CartAmount = price;
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("false");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(restId);
		a2.add("true");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add("true");
		a3.add(userId);
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode200);

		ArrayList a4 = new ArrayList();
		a4.add(restId);
		a4.add("true");
		a4.add(userId);
		a4.add(rngConstants.IosOS);
		a4.add(rngConstants.versionCode200);

		ArrayList a5 = new ArrayList();
		a5.add(restId);
		a5.add("true");
		a5.add(userId);
		a5.add(rngConstants.IosOS);
		a5.add(rngConstants.versionCode250);

		ArrayList a6 = new ArrayList();
		a6.add(restId);
		a6.add("true");
		a6.add(userId);
		a6.add(rngConstants.WebOS);
		a6.add(rngConstants.versionCode200);

		ArrayList a7 = new ArrayList();
		a7.add(restId);
		a7.add("true");
		a7.add(userId);
		a7.add(rngConstants.IosOS);
		a7.add(rngConstants.versionCode250);

		ArrayList a8 = new ArrayList();
		a8.add(rngConstants.minCartAmount);
		a8.add(restId);
		a8.add("false");
		a8.add(userId);
		a8.add(rngConstants.AndroidOS);
		a8.add(rngConstants.versionCode229);

		ArrayList a9 = new ArrayList();
		a9.add(rngConstants.minCartAmount);
		a9.add(restId);
		a9.add("true");
		a9.add(userId);
		a9.add(rngConstants.AndroidOS);
		a9.add(rngConstants.versionCode229);

		ArrayList a10 = new ArrayList();
		a10.add(rngConstants.minCartAmount);
		a10.add(restId);
		a10.add("true");
		a10.add(userId);
		a10.add(rngConstants.AndroidOS);
		a10.add(rngConstants.versionCode200);

		ArrayList a11 = new ArrayList();
		a11.add(rngConstants.minCartAmount);
		a11.add(restId);
		a11.add("true");
		a11.add(userId);
		a11.add(rngConstants.IosOS);
		a11.add(rngConstants.versionCode200);

		ArrayList a12 = new ArrayList();
		a12.add(rngConstants.minCartAmount);
		a12.add(restId);
		a12.add("true");
		a12.add(userId);
		a12.add(rngConstants.IosOS);
		a12.add(rngConstants.versionCode250);

		ArrayList a13 = new ArrayList();
		a13.add(rngConstants.minCartAmount);
		a13.add(restId);
		a13.add("true");
		a13.add(userId);
		a13.add(rngConstants.WebOS);
		a13.add(rngConstants.versionCode200);

		ArrayList a14 = new ArrayList();
		a14.add(rngConstants.minCartAmount);
		a14.add(restId);
		a14.add("true");
		a14.add(userId);
		a14.add(rngConstants.IosOS);
		a14.add(rngConstants.versionCode250);

		ArrayList a15 = new ArrayList();
		a15.add(restId);
		a15.add(categoryId);
		a15.add(subCategoryId);
		a15.add(itemId);
		a15.add(count);
		a15.add(price);
		a15.add("10");
		a15.add(userId);
		a15.add("true");
		a15.add(rngConstants.AndroidOS);
		a15.add(rngConstants.versionCode229);

		ArrayList a17 = new ArrayList();
		a17.add(restId);
		a17.add(categoryId);
		a17.add(subCategoryId);
		a17.add(itemId);
		a17.add(count);
		a17.add(price);
		a17.add(CartAmount);
		a17.add(userId);
		a17.add("true");
		a17.add(rngConstants.AndroidOS);
		a17.add(rngConstants.versionCode229);

		return new Object[][] { { "list", a1, true }, { "list", a2, true }, { "list", a3, true }, { "list", a4, true },
				{ "list", a5, true }, { "list", a6, true }, { "list", a7, true }, { "menu", a8, true },
				{ "menu", a9, true }, { "menu", a10, true }, { "menu", a11, true }, { "menu", a12, true },
				{ "menu", a13, true }, { "menu", a14, true }, { "cart", a15, false }, { "cart", a17, true } };

	}

	@DataProvider(name = "PercentageTD_EvaluationWithTimeSlotRestrictionItemLevel")
	public Object[][] PercentageTD_EvaluationWithTimeSlotRestrictionItemLevel()
			throws IOException, InterruptedException {
		HashMap Keys = rngHelper.createPercentageWithTimeSlotRestrictionAtItemLevel();
		String restId = (String) Keys.get("restId");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);
		/*
		 * Thread.sleep(20000);
		 * 
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("true");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */
		return new Object[][] { { "list", a1, true }, { "menu", a2, true }, /*
																			 * { "cart", a3,true },{ "list", a4,false },
																			 * { "menu", a5,false }, { "cart", a6,false}
																			 */ };

	}

	@DataProvider(name = "FlatTD_EvaluationWithTimeSlotRestrictionItemLevel")
	public Object[][] FlatTD_EvaluationWithTimeSlotRestrictionItemLevel() throws IOException, InterruptedException {
		HashMap Keys = rngHelper.createFlatWithTimeSlotRestrictionAtItemLevel("0", "50");
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);
		/*
		 * Thread.sleep(20000);
		 * 
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("true");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */
		return new Object[][] { { "list", a1,tid, true }, { "menu", a2,tid, true },
				{ "cart", a3,tid, true },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false} */ };

	}

	@DataProvider(name = "FlatTD_EvaluationWithDisabledTdItemLevel")
	public Object[][] FlatTD_EvaluationWithDisabledTdItemLevel() throws IOException, InterruptedException {
		HashMap Keys = rngHelper.createFlatWithDisabledTDAtItemLevel("0", "20");
		String restId = (String) Keys.get("restId");
		String tid = (String) Keys.get("TDID");
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();
		String count = "1";
		String userId = new Integer(rm.nextInt()).toString();

		ArrayList a1 = new ArrayList();
		a1.add(restId);
		a1.add("true");
		a1.add(userId);
		a1.add(rngConstants.AndroidOS);
		a1.add(rngConstants.versionCode229);

		ArrayList a2 = new ArrayList();
		a2.add(rngConstants.minCartAmount);
		a2.add(restId);
		a2.add("false");
		a2.add(userId);
		a2.add(rngConstants.AndroidOS);
		a2.add(rngConstants.versionCode229);

		ArrayList a3 = new ArrayList();
		a3.add(restId);
		a3.add(categoryId);
		a3.add(subCategoryId);
		a3.add(itemId);
		a3.add(count);
		a3.add("500");
		a3.add("500");
		a3.add(userId);
		a3.add("false");
		a3.add(rngConstants.AndroidOS);
		a3.add(rngConstants.versionCode229);
		/*
		 * Thread.sleep(20000);
		 * 
		 * 
		 * ArrayList a4 = new ArrayList(); a4.add(restId); a4.add("true");
		 * a4.add(userId); a4.add(rngConstants.AndroidOS);
		 * a4.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a5 = new ArrayList(); a5.add(rngConstants.minCartAmount);
		 * a5.add(restId); a5.add("false"); a5.add(userId);
		 * a5.add(rngConstants.AndroidOS); a5.add(rngConstants.versionCode229);
		 * 
		 * 
		 * ArrayList a6 = new ArrayList(); a6.add(restId); a6.add(categoryId);
		 * a6.add(subCategoryId); a6.add(itemId); a6.add(count); a6.add("500");
		 * a6.add("500"); a6.add(userId); a6.add("false");
		 * a6.add(rngConstants.AndroidOS); a6.add(rngConstants.versionCode229);
		 */
		return new Object[][] { { "list", a1,tid, false }, { "menu", a2,tid, false },
				{ "cart", a3,tid, false },/* { "list", a4,false }, { "menu", a5,false }, { "cart", a6,false} */ };

	}

	@DataProvider(name = "createCouponData")
	public static Object[][] getCreateCouponData() {
		return new Object[][] {

				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "null", "TestAutomation", "AUTOMAT1", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"\"", "TestAutomation", "AUTOMAT2", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"test\"", "TestAutomation", "AUTOMAT22", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "", "AUTOMAT3", "Test Automation coupon Ankita", "0", "2017-12-19T17:12:00.000Z",
						"2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0", "0", "0", "14", "50", "0", "0",
						"0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0", "-1", "0", "1", "true", "0",
						"", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "Automation12", "", "0", "2017-12-19T17:12:00.000Z",
						"2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0", "0", "0", "14", "50", "0", "0",
						"0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0", "-1", "0", "1", "true", "0",
						"", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "-1",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "0", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "2", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "0", "0", "0", "0", "0", "0",
						"0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "-1", "0", "0", "0", "0", "0",
						"0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "100", "1", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "100", "-1", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "1", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "-1", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "1", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "-1", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "1",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "-1",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"1", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"-1", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "1", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "-1", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "null", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "0", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "1", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "100", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "0", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "1", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "-1", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "1", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "-1", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "1", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "1", "-1", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "changed", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"PayTM\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Cash\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"PayTM-SSO\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Mobikwik\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Mobikwik-SSO\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Freecharge\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Freecharge-SSO\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal",
						"1", "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Sodexo\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "null", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0", "-1",
						"0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0", "-1",
						"0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"test\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "2", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "3", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "4", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "1", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "1", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "0", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" }

		};
	}

	@DataProvider(name = "createCouponDataDiscountAmountNullCheck")
	public static Object[][] createCouponDataDiscountAmountNullCheck() {
		return new Object[][]{
				{"\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "null", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa"}
		};
	}

	@DataProvider(name = "createCouponDataWithNegativeScenario")
	public static Object[][] getCreateCouponDataWithNegativeScenario() {
		return new Object[][]{

				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Internal", "1", "0", "-1", "0", "1", "false", "0", "", "test test", "1", "user@swiggy.in",
						"testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Internal", "1", "0", "", "0", "1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Internal", "1", "0", "0", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL", "", "1",
						"0", "1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL", "Other",
						"1", "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Complete Trade", "1", "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in",
						"testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Partial Trade", "1", "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in",
						"testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Affiliate", "1", "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Alliances", "1", "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Referral", "1", "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1", "", "", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "-1", "0", "0", "0", "\"Juspay-NB\"", "0", "0","", "", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "-1", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","", "", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation couponAnkita", "1",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "-1", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","", "", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Internal", "1", "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay\"", "0", "1","1514374396501", "1514374396501", "NULL",
						"Internal", "0", "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" }

		};
		}

	@DataProvider(name = "createExpiredCouponData")
	public static Object[][] getcreateExpiredCouponData() {
		return new Object[][] { { "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
				"2017-12-28T08:12:00.000Z", "2017-12-28T06:22:00.000Z", "3", "3", "200", "1", "0", "0", "0", "0", "0",
				"14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "", "NULL", "Internal", "1", "0", "-1", "0", "-1",
				"true", "0", "", "", "1", "user@swiggy.in" } };
	}

	@DataProvider(name = "createDuplicateCouponData")
	public static Object[][] getcreateDuplicateCouponData() {
		return new Object[][] { { "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
				"2017-12-28T08:12:00.000Z", "2017-12-31T06:22:00.000Z", "3", "3", "200", "1", "0", "0", "0", "0", "0",
				"14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0", "-1", "0", "-1",
				"true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
				"lbx02iaashsnhea5fpwa" } };
	}

	@DataProvider(name = "createCouponUserMapData")
	public static Object[][] getCreateCouponUserMapData() {
		String[] a = { "1", "2" };
		String b = "";
		for (String c : a)
			b += "\"" + c;

		// "\"testTNC\",\"test\""
		return new Object[][] { { "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
				"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "1", "0", "0", "0", "0", "0",
				"14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0", "-1", "0", "1",
				"true", "0", "", "", "1", "user@swiggy.in", "21213", "testing", "\"testTNC\",\"test\"",
				"lbx02iaashsnhea5fpwa" } };

	}

	@DataProvider(name = "createCouponCityMapData")
	public static Object[][] getCreateCouponCityMapData() {
		return new Object[][] { { "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
				"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "1", "0", "0", "0", "0", "0",
				"14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0", "-1", "0", "1",
				"true", "0", "", "", "1", "user@swiggy.in", "1", "testing", "\"testTNC\",\"test\"",
				"lbx02iaashsnhea5fpwa" } };
	}

	@DataProvider(name = "createCouponItemMapData")
	public static Object[][] getCreateCouponItemMapData() {
		return new Object[][] { { "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
				"2017-12-19T17:12:00.000Z", "2017-12-22T06:31:00.000Z", "3", "3", "200", "1", "0", "0", "0", "0", "0",
				"14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0", "-1", "0", "1",
				"true", "0", "", "", "1", "user@swiggy.in", "2", "testing", "\"testTNC\",\"test\"",
				"lbx02iaashsnhea5fpwa", } };
	}

	@DataProvider(name = "createMultipleCouponItemMapData")
	public static Object[][] getCreateMultipleCouponItemMapData() {
		return new Object[][] {
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "2", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "10", "70", "0", "0", "0", "\"Juspay\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "2", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" } };
	}

	@DataProvider(name = "createMultipleCouponCityMapData")
	public static Object[][] getCreateMultipleCouponCityMapData() {
		return new Object[][] {
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "1", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "10", "70", "0", "0", "0", "\"Juspay\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "1", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" } };
	}

	@DataProvider(name = "createMultipleCouponUserMapData")
	public static Object[][] getCreateMultipleCouponUserMapData() {
		return new Object[][] {
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "21213", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "10", "70", "0", "0", "0", "\"Juspay\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "21214", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" } };
	}

	@DataProvider(name = "createMultipleCouponRestaurantMapData")
	public static Object[][] getCreateMultipleCouponRestaurantMapData() {
		return new Object[][] {
				{ "\"Discount\"", "TestAutomation", "AUTOMAT2", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "212", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT3", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "10", "70", "0", "0", "0", "\"Juspay\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "212", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" } };
	}

	@DataProvider(name = "createMultipleCouponAreaMapData")
	public static Object[][] getCreateMultipleCouponAreaMapData() {
		return new Object[][] {
				{ "\"Discount\"", "TestAutomation", "AUTOMAT3", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "2", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT22", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2017-12-29T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "10", "70", "0", "0", "0", "\"Juspay\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0",
						"-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "2", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" } };
	}

	@DataProvider(name = "couponUserCity")
	public static Object[][] getcouponUserCity() {
		return new Object[][] {
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2018-01-31T06:31:00.000Z", "3", "3", "200", "1", "1", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "3249402", "1", "both",
						"testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				// only City restriction is true but in get API not getting this coupon
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2018-01-31T06:31:00.000Z", "3", "3", "200", "0", "1", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "3249402", "1", "city",
						"testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2018-01-31T06:31:00.000Z", "3", "3", "200", "1", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "3249402", "1", "user",
						"testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" } };
	}

	@DataProvider(name = "getCouponUserCityInvalidData")
	public static Object[][] getCouponUserCityInvalidData() {
		return new Object[][] {
				// both- city and user
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2018-01-31T06:31:00.000Z", "3", "3", "200", "1", "1", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "1", "3249402", "both",
						"testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				// only City
				// { "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita
				// Ankita", "1",
				// "2017-12-19T17:12:00.000Z", "2018-01-31T06:31:00.000Z", "3", "3", "200", "0",
				// "1", "0", "0",
				// "0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "", "NULL",
				// "Internal", "1",
				// "0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in","1",
				// "3249402", "city","testing","\"testTNC\",\"test\"","lbx02iaashsnhea5fpwa" },
				// //only user
				// { "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita
				// Ankita", "1",
				// "2017-12-19T17:12:00.000Z", "2018-01-31T06:31:00.000Z", "3", "3", "200", "1",
				// "0", "0", "0",
				// "0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "", "NULL",
				// "Internal", "1",
				// "0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in","1",
				// "3249402", "user","testing","\"testTNC\",\"test\"","lbx02iaashsnhea5fpwa" }
		};
	}

	@DataProvider(name = "publicCouponUserCity")
	public static Object[][] getpublicCouponUserCity() {
		return new Object[][] { { "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita",
				"0", "2017-12-19T17:12:00.000Z", "2018-01-31T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0", "0",
				"0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1", "0", "-1", "0",
				"1", "true", "0", "", "", "1", "user@swiggy.in", "3249402", "1", "testing", "\"testTNC\",\"test\"",
				"lbx02iaashsnhea5fpwa" } };

	}

	@DataProvider(name = "couponUserData")
	public static Object[][] getcouponUserData() {
		return new Object[][] {
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "1",
						"2017-12-19T17:12:00.000Z", "2018-01-10T23:59:00.000Z", "3", "3", "200", "1", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "3249402", "user", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2018-01-10T23:59:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "3249402", "public", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa" }

		};
	}

	@DataProvider(name = "getcouponUserInvalidData")
	public static Object[][] getcouponUserInvalidData() {
		return new Object[][] {
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2018-01-31T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "null", "null" },
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita Ankita", "0",
						"2017-12-19T17:12:00.000Z", "2018-01-31T06:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "", "NULL", "Internal", "1",
						"0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "0", "zero'" } };
	}

	@DataProvider(name = "createAllTypebulkCouponData")
	public static Object[][] createAllTypebulkCouponData() {
		return new Object[][] {
				{ "i", "4", "2", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa", "800", "2159", "", "createAllTypebulkCouponData" },
				{ "t", "4", "2", "\"CashBack\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" },
				{ "l", "4", "2", "\"marketing\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" },
				{ "m", "4", "2", "\"paytm\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0",
						"uspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,Sodexo",
						"0", "0", "", "NULL", "other", "1", "0", "-1", "0", "-1", "true", "1", "RefundSource", "", "1",
						"user@swiggy.in", "testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" } };
	}

	@DataProvider(name = "bulkCouponCode_prefixData")
	public static Object[][] bulkCouponCode_prefixData() {
		return new Object[][] {
				{ "\"\"", "4", "2", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa", "800", "2159", "", "EmptyString" },
				{ "null", "4", "2", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa", "800", "2159", "", "bulkCouponCode_prefixData" },
				{ "\"ABCDEFGHIJKLMNOP\"", "47", "2", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita",
						"0", "2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0",
						"0", "0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"bulkCouponCode_prefixData" }, };
	}

	@DataProvider(name = "bulkCouponCode_repeatPrefixData")
	public static Object[][] bulkCouponCodeRepeatprefixData() {
		return new Object[][] { { "\"REP\"", "4", "2", "\"Discount\"", "TestAutomation",
				"Test Automation coupon Ankita", "0", "2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2",
				"200", "0", "0", "0", "0", "0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL",
				"Internal", "1", "0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
				"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "", "Repeat" }, };
	}

	@DataProvider(name = "bulkCouponCode_suffixData")
	public static Object[][] bulkCouponCode_suffixData() {
		return new Object[][] {
				{ "i", "4", "2", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa", "800", "2159", "", "createAllTypebulkCouponData" },
				{ "t", "3", "2", "\"CashBack\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" },
				{ "l", "0", "2", "\"marketing\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" }, };
	}

	@DataProvider(name = "bulkCouponCode_TotalCouponData")
	public static Object[][] bulkCouponCode_TotalCouponData() {
		return new Object[][] {
				{ "i", "4", "100", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa", "800", "2159", "", "createAllTypebulkCouponData" },
				{ "t", "4", "2", "\"CashBack\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" },
				{ "l", "4", "0", "\"marketing\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" },
				{ "m", "4", "null", "\"paytm\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0",
						"uspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,Sodexo",
						"0", "0", "", "NULL", "other", "1", "0", "-1", "0", "-1", "true", "1", "RefundSource", "", "1",
						"user@swiggy.in", "testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" } };
	}

	@DataProvider(name = "bulkCouponCode_timeSlotData")
	public static Object[][] bulkCouponCode_timeSlotData() {
		return new Object[][] {
				{ "i", "4", "2", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "1", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa", "800", "2159", "all", "createAllTypebulkCouponData" },
				// from-till is same date
				{ "t", "4", "2", "\"CashBack\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-10T06:06:00.000Z", "2018-01-10T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Cash", "0", "1", "", "NULL", "Internal", "1", "0", "-1",
						"0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "1400", "all",
						"createAllTypebulkCouponData" },
				{ "l", "4", "2", "\"marketing\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Sodexo", "0", "1", "", "NULL", "Internal", "1", "0", "-1",
						"0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "1100", "1459", "mon",
						"createAllTypebulkCouponData" },
				{ "m", "4", "2", "\"paytm\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0",
						"uspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,Sodexo",
						"0", "0", "", "NULL", "other", "1", "0", "-1", "0", "-1", "true", "1", "RefundSource", "", "1",
						"user@swiggy.in", "testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159",
						"all", "createAllTypebulkCouponData" },
				{ "m", "4", "2", "\"paytm\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "PayTM,Sodexo", "0", "1", "", "NULL", "other", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "0", "0", "", "createAllTypebulkCouponData" } };
	}

	@DataProvider(name = "getBulkCoupon")
	public static Object[][] getBulkCoupon() {
		return new Object[][] { { "REP", "4", "2", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita",
				"0", "2018-01-16T06:06:00.000Z", "2018-01-20T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0", "0",
				"0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0", "-1", "0",
				"-1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
				"lbx02iaashsnhea5fpwa", "800", "2159", "all", "createAllTypebulkCouponData" }, };
	}

	@DataProvider(name = "getBulkCouponAll")
	public static Object[][] getBulkCouponAll() {
		return new Object[][] {
				{ "i", "4", "2", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa", "800", "2159", "", "createAllTypebulkCouponData" },
				{ "t", "4", "2", "\"CashBack\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" },
				{ "l", "4", "2", "\"marketing\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" },
				{ "m", "4", "2", "\"paytm\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0",
						"uspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,Sodexo",
						"0", "0", "", "NULL", "other", "1", "0", "-1", "0", "-1", "true", "1", "RefundSource", "", "1",
						"user@swiggy.in", "testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" } };
	}

	@DataProvider(name = "checkTotalCouponsInBulk")
	public static Object[][] checkTotalCouponsInBulk() {
		return new Object[][] {
				{ "i", "4", "5", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"",
						"lbx02iaashsnhea5fpwa", "800", "2159", "", "createAllTypebulkCouponData" },
				{ "t", "4", "100", "\"CashBack\"", "TestAutomation", "Test Automation coupon Ankita", "0",
						"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0",
						"-1", "0", "-1", "true", "1", "RefundSource", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "",
						"createAllTypebulkCouponData" } };
	}

	@DataProvider(name = "checkBulkCouponStatus")
	public static Object[][] checkBulkCouponStatus() {
		return new Object[][] { { "i", "4", "2", "\"Discount\"", "TestAutomation", "Test Automation coupon Ankita", "0",
				"2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2", "200", "0", "0", "0", "0", "0", "0",
				"14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL", "Internal", "1", "0", "-1", "0", "-1",
				"true", "0", "", "", "1", "user@swiggy.in", "testing", "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa",
				"800", "2159", "", "createAllTypebulkCouponData" } };
	}

	@DataProvider(name = "checkBulkCouponCodePrefix")
	public static Object[][] checkBulkCouponCodePrefix() {
		return new Object[][] { { "CODEPREFIX", "4", "2", "\"Discount\"", "TestAutomation",
				"Test Automation coupon Ankita", "0", "2018-01-09T06:06:00.000Z", "2018-01-22T23:57:00.000Z", "2", "2",
				"200", "0", "0", "0", "0", "0", "0", "14", "50", "0", "0", "0", "Juspay-NB", "0", "0", "", "NULL",
				"Internal", "1", "0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
				"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "800", "2159", "", "Repeat" } };
	}

	@DataProvider(name = "applyCoupon")
	public static Object[][] applyCoupon() {
		return new Object[][] {
				// public
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"" },
				// user-mapped
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "1", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "userPrivate",
						"3249402", "", "", "" },
				// restaurant mapped
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "1",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "restaurantPrivate",
						"", "222", "", "" },
				// area mapped
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "0", "0", "1", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "1", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "areaPrivate", "",
						"", "1", "" },
				// city mapped
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "0", "1", "0", "0",
						"0", "1", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "1", "1", "0.0", "0.0", "null", "22894", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "cityPrivate", "",
						"", "", "1" } };
	}

	@DataProvider(name = "applyCouponInvalidData")
	public static Object[][] applyCouponInvalidData() {
		return new Object[][] {
				// user-mapped
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "1", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "userPrivate",
						"97133", "", "", "" },
				// restaurant mapped
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "1",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "restaurantPrivate",
						"", "223", "", "" },
				// area mapped
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "0", "0", "1", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "1", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "areaPrivate", "",
						"", "4", "" },

				// city mapped
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "1",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "0", "1", "0", "0",
						"0", "1", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "1", "1", "0.0", "0.0", "null", "22894", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "cityPrivate", "",
						"", "", "6" } };
	}

	@DataProvider(name = "applyCouponUsageCountInvalid")
	public static Object[][] applyCouponUsageCountInvalid() {
		return new Object[][] {
				// totalPreUser
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "2", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" }, };
	}

	@DataProvider(name = "applyCouponAndUsageCount")
	public static Object[][] applyCouponAndUsageCount() {
		return new Object[][] {
				// total available Zero & totalPreUser Zero
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "0", "0", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" },
				// total available x & totalPreUser Zero
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "10", "0", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" },
				// total available 0 & totalPreUser x
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "0", "10", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "10" }, };
	}

	@DataProvider(name = "applyCouponMultipleUser")
	public static Object[][] applyCouponMultipleUser() {
		return new Object[][] {
				// total available y, totalPreUser lesser than y -multiple users
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2", "354990" }, };
	}

	@DataProvider(name = "applyCouponApplicableWithSwiggyMoneyInvalid")
	public static Object[][] applyCouponApplicableWithSwiggyMoneyInvalid() {
		return new Object[][] {
				// coupon applicable with swiggy money is false & apply API set true
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "100.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "true", "false", "0", "true", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" }, };
	}

	@DataProvider(name = "applyCouponApplicableWithSwiggyMoney")
	public static Object[][] applyCouponApplicableWithSwiggyMoney() {
		return new Object[][] {
				// coupon applicable with swiggy money is false & apply API set false
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" }, };
	}

	@DataProvider(name = "applyCouponApplicableWithSwiggyMoneyTrue")
	public static Object[][] applyCouponApplicableWithSwiggyMoneyTrue() {
		return new Object[][] {
				// coupon applicable with swiggy money is true & apply API set true
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "100.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "true", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" },
				// coupon applicable with swiggy money is true & apply API set false
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" }, };
	}

	@DataProvider(name = "applyCouponApplicableUserClient")
	public static Object[][] applyCouponApplicableUserClient() {
		return new Object[][] {
				// coupon applicable with user agent ALL & apply API set ALL
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "100.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "true", "245", "ALL", "null", "null", "public", "", "", "", "",
						"5743457", "2" },
				// coupon applicable with user agent WEB & apply API set WEB
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "1", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "WEB", "null", "null", "public", "", "", "", "",
						"5743457", "2" },
				// coupon applicable with user agent ANDROID & apply API set ANDROID
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "3", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" },
				// coupon applicable with user agent IOS & apply API set IOS
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "4", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "IOS", "null", "null", "public", "", "", "", "",
						"5743457", "2" },
				// coupon applicable with user agent APP & apply API set IOS
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "2", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "IOS", "null", "null", "public", "", "", "", "",
						"5743457", "2" },
				// coupon applicable with user agent APP & apply API set ANDROID
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "2", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" }, };
	}

	@DataProvider(name = "applyCouponApplicableUserClientInvalid")
	public static Object[][] applyCouponApplicableUserClientInvalid() {
		return new Object[][] {
				// coupon applicable with user agent WEB & apply API set IOS
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "1", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "100.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "true", "245", "IOS", "null", "null", "public", "", "", "", "",
						"5743457", "2" },
				// coupon applicable with user agent WEB & apply API set ANDROID
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "1", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" },
				// coupon applicable with user agent ANDROID & apply API set IOS
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "3", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "IOS", "null", "null", "public", "", "", "", "",
						"5743457", "2" },
				// coupon applicable with user agent ANDROID & apply API set WEB
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "3", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "1", "false", "245", "IOS", "null", "null", "public", "", "", "", "",
						"5743457", "2" },
				// coupon applicable with user agent IOS & apply API set ANDROID
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "4", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ANDROID", "null", "null", "public", "", "", "",
						"", "5743457", "2" },
				// coupon applicable with user agent IOS & apply API set WEB
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "12", "9", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "4", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"222", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "WEB", "null", "null", "public", "", "", "", "",
						"5743457", "2" }, };
	}

	@DataProvider(name = "applyCouponMinCartAmount")
	public static Object[][] applyCouponMinCartAmount() {
		return new Object[][] {
				//// min cart amount of Create coupon & apply coupon is same
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" },
				// min cart amount of Create coupon & apply coupon is not same
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" } };
	}

	@DataProvider(name = "applyCouponMinQuantity")
	public static Object[][] applyCouponMinQuantity() {
		return new Object[][] {
				// min quantity of Create coupon & apply coupon is same
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "2",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "2", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" },
				// apply coupon is min quabtity is greater
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "3",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "5", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" },
				// min quantity of Create coupon & apply coupon is not same
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "200", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "2",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "1", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" } };
	}

	@DataProvider(name = "applyCouponFirstOrder")
	public static Object[][] applyCouponFirstOrder() {
		return new Object[][] {
				// in coupon 1st order is true & in apply coupon is 1st order is true
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "true", "1", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" },
				// in coupon 1st order is true & in apply coupon is 1st order is false
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-17T06:12:00.000Z", "2018-01-22T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "false", "1", "200", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" } };
	}

	@DataProvider(name = "applyCouponPreOrderExpired")
	public static Object[][] applyCouponPreOrderExpired() {
		return new Object[][] {
				// preOrder out of the coupon validity
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-21T06:12:00.000Z", "2018-01-27T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "true", "1", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "1517117400000", "public", "",
						"", "", "" }, };
	}

	@DataProvider(name = "applyCouponPreOrder")
	public static Object[][] applyCouponPreOrder() {
		return new Object[][] {
				// preOrder with in coupon validity
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-21T06:12:00.000Z", "2018-01-27T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "true", "1", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "1516944600000", "public", "",
						"", "", "" }, };
	}

	@DataProvider(name = "applyCouponVendorDominos")
	public static Object[][] applyCouponVendorDominos() {
		return new Object[][] {
				// conpon for vendor Dominos(12) and in apply API typeOfPartner is Dominos(12)
				// only
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-21T06:12:00.000Z", "2018-01-27T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "12", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "true", "1", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "12", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" },
				// conpon for vendor Dominos(12) and in apply API typeOfPartner is Swiggy(0)
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-21T06:12:00.000Z", "2018-01-27T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "12", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "true", "1", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" }, };
	}

	@DataProvider(name = "applyCouponVendorSwiggy")
	public static Object[][] applyCouponVendorSwiggy() {
		return new Object[][] {
				// conpon for vendor Swiggy(1) and in apply API typeOfPartner is Swiggy(0) only
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-21T06:12:00.000Z", "2018-01-27T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "true", "1", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" },
				// conpon for vendor Swiggy(0) and in apply API typeOfPartner is Dominos(12)
				// only
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-21T06:12:00.000Z", "2018-01-27T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "true", "1", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "12", "false", "245", "ALL", "null", "null", "public", "", "", "",
						"" }, };
	}

	@DataProvider(name = "applyCouponOutOfTheTimeSlot")
	public static Object[][] applyCouponOutOfTheTimeSlot() {
		return new Object[][] {
				//
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-21T06:12:00.000Z", "2018-01-27T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "1", "", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "true", "1", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "null", "public", "", "", "", "",
						"mon", "800", "1200", "4348341", "ankita", "ANKITA", "2018-01-24T23:57:00.000Z",
						"2018-01-21T06:12:00.000Z" }, };
	}

	@DataProvider(name = "applyCouponPreOrderWithInValidity")
	public static Object[][] applyCouponPreOrderWithInValidity() {
		return new Object[][] {
				// Coupon apply in case of PreOrder
				{ "\"Discount\"", "TestAutomation", "AUTOMAT", "Test Automation coupon Ankita", "0",
						"2018-01-21T06:12:00.000Z", "2018-01-27T23:31:00.000Z", "3", "3", "100", "0", "0", "0", "0",
						"0", "0", "14", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0","1514374396501", "1514374396501", "NULL", "Internal", "1",
						"0", "-1", "0", "-1", "false", "0", "0", "", "", "1", "user@swiggy.in", "testing",
						"\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa", "2", "1", "0.0", "0.0", "null", "5282696", "1",
						"28157", "3249402", "0.0", "true", "1", "100", "5282696_0_variants_addons", "1", "120",
						"Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo",
						"false", "false", "false", "0", "false", "245", "ALL", "null", "1516597200000", "public", "",
						"", "", "", "wed", "800", "2100", "4348341", "ankita", "ANKITA", "2018-01-24T23:57:00.000Z",
						"2018-01-21T06:12:00.000Z" }, };
	}

	@DataProvider(name = "referralFlowData")
	public static Object[][] referralFlowData() {
		return new Object[][] {
				//
				{ "ANKITA", "7987663422", "Test1234566268@gmail.com", "swiggy" } };

	}

	@DataProvider(name = "referralUserIdGetNPostData")
	public static Object[][] referralUserIdGetNPostData() {
		return new Object[][] {
				//
				{ "3", "1512613697083", "ANKITA", "7987663455", "Testttr1234566658@swiggy.in", "swiggy" } };
	}

	@DataProvider(name = "getReferralUserIdData")
	public static Object[][] getReferralUserIdData() {
		return new Object[][] {
				// userId as null
				{ "ANKITA", "7987663409", "Testtre1234566uuu@swiggy.in", "swiggy", "null" },
				// //userId as 0
				// {"ANKITA", "7987663409", "Test123456668@swiggy.in", "swiggy","0" } ,
				// //new signup userId TC
				{ "ANKITA", "9984565678", "Testttt12345666876zzz@swiggy.in", "swiggy", "" } };

	}

	@DataProvider(name = "postReferralUserIdData")
	public static Object[][] postReferralUserIdData() {
		return new Object[][] {
				// userId as null
				{ "ANKITA", "7987663475", "Test1234567758@swiggy.in", "swiggy", "", "8", "1512613697083" },
				{ "ANKITA", "9981325957", "Test1234536yr3@swiggy.in", "swiggy", "null", "8", "1512613697083" }, };
	}
	@DataProvider(name = "createMultiApplyCouponData")
	public static Object[][] createMultiApplyCouponData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType="false";
		
			hashMap.put("0", "ALPHACODE"); 
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1525046400000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap,  duplicateType} };

		}}
	@DataProvider(name = "createDuplicateMultiApplyCouponData")
	public static Object[][] createDuplicateMultiApplyCoupon() {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			ArrayList a1 = new ArrayList();
			String duplicateType="true";
			// String.valueOf(a1).toString();
			hashMap.put("0", "ALPHACODE002"); 
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap,  duplicateType}};

		}
	
	@DataProvider(name = "createMultiApplyCouponTransactionsData")
	public static Object[][] createMultiApplyCouponTransactionsData() {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			ArrayList a1 = new ArrayList();
			String duplicateType="false";
			String totalPerUser="lesser";
			String totalPerUser1="equal";
			String totalPerUser2="greater";

			hashMap.put("0", "ALPHACODE002"); 
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "0");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			
			hashMap1.put("0", "ALPHACODE002"); 
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");
			
			hashMap2.put("0", "ALPHACODE002"); 
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "3");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "10");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");
			return new Object[][] { { hashMap,  duplicateType,totalPerUser},
				{ hashMap1,  duplicateType,totalPerUser1},
				{ hashMap2,  duplicateType,totalPerUser2}};

		}
	
	@DataProvider(name = "createMultiApplyCouponUsageCountData")
	public static Object[][] createMultiApplyCouponUsageCountData() {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			ArrayList a1 = new ArrayList();
			String duplicateType="false";

			hashMap.put("0", "ALPHACODE002"); 
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "0");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "0");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			
			hashMap1.put("0", "ALPHACODE002"); 
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "0");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "1");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");
			
			hashMap2.put("0", "ALPHACODE002"); 
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "null");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "10");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "null");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");
			return new Object[][] { { hashMap,  duplicateType},
				{ hashMap1,  duplicateType},
				{ hashMap2,  duplicateType}};

		}
	
	@DataProvider(name = "createMultiApplyTypeOfCouponsData")
	public static Object[][] createMultiApplyTypeOfCouponsData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();
			HashMap<String, String> hashMap4 = new HashMap<String, String>();
			HashMap<String, String> hashMap5 = new HashMap<String, String>();
			HashMap<String, String> hashMap6 = new HashMap<String, String>();
			HashMap<String, String> hashMap7 = new HashMap<String, String>();
		
			String duplicateType="false";
			// String.valueOf(a1).toString();
			hashMap.put("0", "ALPHACODE"); 
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "CashBack");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			
			
			hashMap1.put("0", "ALPHACODE"); 
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "CashBack");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Marketing");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");
			
			hashMap2.put("0", "ALPHACODE"); 
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "PayTM");
			hashMap2.put("34", "10");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Marketing");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");
			
			
			hashMap3.put("0", "ALPHACODE"); 
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1519279920000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "10");
			hashMap3.put("35", "1");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Marketing");
			hashMap3.put("52", "10");
			hashMap3.put("53", "1");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");
			
			
			
			hashMap4.put("0", "ALPHACODE"); 
			hashMap4.put("1", "MULTICOUPON");
			hashMap4.put("2", "Internal");
			hashMap4.put("3", "AUTOMATION");
			hashMap4.put("4", "2");
			hashMap4.put("5", "10");
			hashMap4.put("6", "false");
			hashMap4.put("7", "false");
			hashMap4.put("8", "false");
			hashMap4.put("9", "false");
			hashMap4.put("10", "false");
			hashMap4.put("11", "false");
			hashMap4.put("12", "false");
			hashMap4.put("13", "false");
			hashMap4.put("14", "false");
			hashMap4.put("15", "false");
			hashMap4.put("16", "false");
			hashMap4.put("17", "1518934320000");
			hashMap4.put("18", "1519279920000");
			hashMap4.put("19", "ankita.yadav@swiggy.in");
			hashMap4.put("20", "1518934320000");
			hashMap4.put("21", "1518934320000");
			hashMap4.put("22", "null");
			hashMap4.put("23", "null");
			hashMap4.put("24", "null");
			hashMap4.put("25", "-1");
			hashMap4.put("26", "false");
			hashMap4.put("27", "0");
			hashMap4.put("28", "null");
			hashMap4.put("29", "null");
			hashMap4.put("30", "null");
			hashMap4.put("31", "null");
			hashMap4.put("32", "1");
			hashMap4.put("33", "Discount");
			hashMap4.put("34", "10");
			hashMap4.put("35", "1");
			hashMap4.put("36", "50");
			hashMap4.put("37", "10");
			hashMap4.put("38", "90");
			hashMap4.put("39", "false");
			hashMap4.put("40", "0");
			hashMap4.put("41", "-1");
			hashMap4.put("42", "1st transaction desc.");
			hashMap4.put("43", "1st transaction");
			hashMap4.put("44", "\"com.swiggy.poc.test1\"");
			hashMap4.put("45", "w2fh4c46d0bszhl89voy");
			hashMap4.put("46", "false");
			hashMap4.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap4.put("48", "ANDROID");
			hashMap4.put("49", "2");
			hashMap4.put("50", "2");
			hashMap4.put("51", "PayTM");
			hashMap4.put("52", "10");
			hashMap4.put("53", "1");
			hashMap4.put("54", "50");
			hashMap4.put("55", "10");
			hashMap4.put("56", "90");
			hashMap4.put("57", "false");
			hashMap4.put("58", "0");
			hashMap4.put("59", "-1");
			hashMap4.put("60", "1st transaction desc.");
			hashMap4.put("61", "1st transaction");
			hashMap4.put("62", "\"com.swiggy.poc.test1\"");
			hashMap4.put("63", "w2fh4c46d0bszhl89voy");
			hashMap4.put("64", "false");
			hashMap4.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap4.put("66", "ANDROID");
			hashMap4.put("67", "2");
			hashMap4.put("68", "false");
			hashMap4.put("69", "false");
			hashMap4.put("70", "false");
			
			hashMap5.put("0", "ALPHACODE"); 
			hashMap5.put("1", "MULTICOUPON");
			hashMap5.put("2", "Internal");
			hashMap5.put("3", "AUTOMATION");
			hashMap5.put("4", "2");
			hashMap5.put("5", "10");
			hashMap5.put("6", "false");
			hashMap5.put("7", "false");
			hashMap5.put("8", "false");
			hashMap5.put("9", "false");
			hashMap5.put("10", "false");
			hashMap5.put("11", "false");
			hashMap5.put("12", "false");
			hashMap5.put("13", "false");
			hashMap5.put("14", "false");
			hashMap5.put("15", "false");
			hashMap5.put("16", "false");
			hashMap5.put("17", "1518934320000");
			hashMap5.put("18", "1519279920000");
			hashMap5.put("19", "ankita.yadav@swiggy.in");
			hashMap5.put("20", "1518934320000");
			hashMap5.put("21", "1518934320000");
			hashMap5.put("22", "null");
			hashMap5.put("23", "null");
			hashMap5.put("24", "null");
			hashMap5.put("25", "-1");
			hashMap5.put("26", "false");
			hashMap5.put("27", "0");
			hashMap5.put("28", "null");
			hashMap5.put("29", "null");
			hashMap5.put("30", "null");
			hashMap5.put("31", "null");
			hashMap5.put("32", "1");
			hashMap5.put("33", "CashBack");
			hashMap5.put("34", "10");
			hashMap5.put("35", "1");
			hashMap5.put("36", "50");
			hashMap5.put("37", "10");
			hashMap5.put("38", "90");
			hashMap5.put("39", "false");
			hashMap5.put("40", "0");
			hashMap5.put("41", "-1");
			hashMap5.put("42", "1st transaction desc.");
			hashMap5.put("43", "1st transaction");
			hashMap5.put("44", "\"com.swiggy.poc.test1\"");
			hashMap5.put("46", "false");
			hashMap5.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap5.put("48", "ANDROID");
			hashMap5.put("49", "2");
			hashMap5.put("50", "2");
			hashMap5.put("51", "PayTM");
			hashMap5.put("52", "10");
			hashMap5.put("53", "1");
			hashMap5.put("54", "50");
			hashMap5.put("55", "10");
			hashMap5.put("56", "90");
			hashMap5.put("57", "false");
			hashMap5.put("58", "0");
			hashMap5.put("59", "-1");
			hashMap5.put("60", "1st transaction desc.");
			hashMap5.put("61", "1st transaction");
			hashMap5.put("62", "\"com.swiggy.poc.test1\"");
			hashMap5.put("63", "w2fh4c46d0bszhl89voy");
			hashMap5.put("64", "false");
			hashMap5.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap5.put("66", "ANDROID");
			hashMap5.put("67", "2");
			hashMap5.put("68", "false");
			hashMap5.put("69", "false");
			hashMap5.put("70", "false");
			
			hashMap6.put("0", "ALPHACODE"); 
			hashMap6.put("1", "MULTICOUPON");
			hashMap6.put("2", "Internal");
			hashMap6.put("3", "AUTOMATION");
			hashMap6.put("4", "2");
			hashMap6.put("5", "10");
			hashMap6.put("6", "false");
			hashMap6.put("7", "false");
			hashMap6.put("8", "false");
			hashMap6.put("9", "false");
			hashMap6.put("10", "false");
			hashMap6.put("11", "false");
			hashMap6.put("12", "false");
			hashMap6.put("13", "false");
			hashMap6.put("14", "false");
			hashMap6.put("15", "false");
			hashMap6.put("16", "false");
			hashMap6.put("17", "1518934320000");
			hashMap6.put("18", "1519279920000");
			hashMap6.put("19", "ankita.yadav@swiggy.in");
			hashMap6.put("20", "1518934320000");
			hashMap6.put("21", "1518934320000");
			hashMap6.put("22", "null");
			hashMap6.put("23", "null");
			hashMap6.put("24", "null");
			hashMap6.put("25", "-1");
			hashMap6.put("26", "false");
			hashMap6.put("27", "0");
			hashMap6.put("28", "null");
			hashMap6.put("29", "null");
			hashMap6.put("30", "null");
			hashMap6.put("31", "null");
			hashMap6.put("32", "1");
			hashMap6.put("33", "");
			hashMap6.put("34", "10");
			hashMap6.put("35", "1");
			hashMap6.put("36", "50");
			hashMap6.put("37", "10");
			hashMap6.put("38", "90");
			hashMap6.put("39", "false");
			hashMap6.put("40", "0");
			hashMap6.put("41", "-1");
			hashMap6.put("42", "1st transaction desc.");
			hashMap6.put("43", "1st transaction");
			hashMap6.put("44", "\"com.swiggy.poc.test1\"");
			hashMap6.put("45", "w2fh4c46d0bszhl89voy");
			hashMap6.put("46", "false");
			hashMap6.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap6.put("48", "ANDROID");
			hashMap6.put("49", "2");
			hashMap6.put("50", "2");
			hashMap6.put("51", "");
			hashMap6.put("52", "10");
			hashMap6.put("53", "1");
			hashMap6.put("54", "50");
			hashMap6.put("55", "10");
			hashMap6.put("56", "90");
			hashMap6.put("57", "false");
			hashMap6.put("58", "0");
			hashMap6.put("59", "-1");
			hashMap6.put("60", "1st transaction desc.");
			hashMap6.put("61", "1st transaction");
			hashMap6.put("62", "\"com.swiggy.poc.test1\"");
			hashMap6.put("63", "w2fh4c46d0bszhl89voy");
			hashMap6.put("64", "false");
			hashMap6.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap6.put("66", "ANDROID");
			hashMap6.put("67", "2");
			hashMap6.put("68", "false");
			hashMap6.put("69", "false");
			hashMap6.put("70", "false");
			
			hashMap7.put("0", "ALPHACODE"); 
			hashMap7.put("1", "MULTICOUPON");
			hashMap7.put("2", "Internal");
			hashMap7.put("3", "AUTOMATION");
			hashMap7.put("4", "2");
			hashMap7.put("5", "10");
			hashMap7.put("6", "false");
			hashMap7.put("7", "false");
			hashMap7.put("8", "false");
			hashMap7.put("9", "false");
			hashMap7.put("10", "false");
			hashMap7.put("11", "false");
			hashMap7.put("12", "false");
			hashMap7.put("13", "false");
			hashMap7.put("14", "false");
			hashMap7.put("15", "false");
			hashMap7.put("16", "false");
			hashMap7.put("17", "1518934320000");
			hashMap7.put("18", "1519279920000");
			hashMap7.put("19", "ankita.yadav@swiggy.in");
			hashMap7.put("20", "1518934320000");
			hashMap7.put("21", "1518934320000");
			hashMap7.put("22", "null");
			hashMap7.put("23", "null");
			hashMap7.put("24", "null");
			hashMap7.put("25", "-1");
			hashMap7.put("26", "false");
			hashMap7.put("27", "0");
			hashMap7.put("28", "null");
			hashMap7.put("29", "null");
			hashMap7.put("30", "null");
			hashMap7.put("31", "null");
			hashMap7.put("32", "1");
			hashMap7.put("33", "test123");
			hashMap7.put("34", "10");
			hashMap7.put("35", "1");
			hashMap7.put("36", "50");
			hashMap7.put("37", "10");
			hashMap7.put("38", "90");
			hashMap7.put("39", "false");
			hashMap7.put("40", "0");
			hashMap7.put("41", "-1");
			hashMap7.put("42", "1st transaction desc.");
			hashMap7.put("43", "1st transaction");
			hashMap7.put("44", "\"com.swiggy.poc.test1\"");
			hashMap7.put("45", "w2fh4c46d0bszhl89voy");
			hashMap7.put("46", "false");
			hashMap7.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap7.put("48", "ANDROID");
			hashMap7.put("49", "2");
			hashMap7.put("50", "2");
			hashMap7.put("51", "com.swiggy.poc.test1");
			hashMap7.put("52", "10");
			hashMap7.put("53", "1");
			hashMap7.put("54", "50");
			hashMap7.put("55", "10");
			hashMap7.put("56", "90");
			hashMap7.put("57", "false");
			hashMap7.put("58", "0");
			hashMap7.put("59", "-1");
			hashMap7.put("60", "1st transaction desc.");
			hashMap7.put("61", "1st transaction");
			hashMap7.put("62", "\"com.swiggy.poc.test1\"");
			hashMap7.put("63", "w2fh4c46d0bszhl89voy");
			hashMap7.put("64", "false");
			hashMap7.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap7.put("66", "ANDROID");
			hashMap7.put("67", "2");
			hashMap7.put("68", "false");
			hashMap7.put("69", "false");
			hashMap7.put("70", "false");
			
			return new Object[][] { { hashMap,  duplicateType},
				{hashMap1,  duplicateType},{hashMap2,  duplicateType},{hashMap3,  duplicateType},
				{hashMap4,  duplicateType},{hashMap5,  duplicateType},
				
				};

		}}
	
	@DataProvider(name = "createInvalidMultiApplyTypeOfCouponsData")
	public static Object[][] createInvalidMultiApplyTypeOfCouponsData() {
		{
			HashMap<String, String> hashMap6 = new HashMap<String, String>();
			HashMap<String, String> hashMap7 = new HashMap<String, String>();
			String duplicateType="false";
			
			hashMap6.put("0", "ALPHACODE"); 
			hashMap6.put("1", "MULTICOUPON");
			hashMap6.put("2", "Internal");
			hashMap6.put("3", "AUTOMATION");
			hashMap6.put("4", "2");
			hashMap6.put("5", "10");
			hashMap6.put("6", "false");
			hashMap6.put("7", "false");
			hashMap6.put("8", "false");
			hashMap6.put("9", "false");
			hashMap6.put("10", "false");
			hashMap6.put("11", "false");
			hashMap6.put("12", "false");
			hashMap6.put("13", "false");
			hashMap6.put("14", "false");
			hashMap6.put("15", "false");
			hashMap6.put("16", "false");
			hashMap6.put("17", "1518934320000");
			hashMap6.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap6.put("19", "ankita.yadav@swiggy.in");
			hashMap6.put("20", "1518934320000");
			hashMap6.put("21", "1518934320000");
			hashMap6.put("22", "null");
			hashMap6.put("23", "null");
			hashMap6.put("24", "null");
			hashMap6.put("25", "-1");
			hashMap6.put("26", "false");
			hashMap6.put("27", "0");
			hashMap6.put("28", "null");
			hashMap6.put("29", "null");
			hashMap6.put("30", "null");
			hashMap6.put("31", "null");
			hashMap6.put("32", "1");
			hashMap6.put("33", "");
			hashMap6.put("34", "10");
			hashMap6.put("35", "1");
			hashMap6.put("36", "50");
			hashMap6.put("37", "10");
			hashMap6.put("38", "90");
			hashMap6.put("39", "false");
			hashMap6.put("40", "0");
			hashMap6.put("41", "-1");
			hashMap6.put("42", "1st transaction desc.");
			hashMap6.put("43", "1st transaction");
			hashMap6.put("44", "\"com.swiggy.poc.test1\"");
			hashMap6.put("45", "w2fh4c46d0bszhl89voy");
			hashMap6.put("46", "false");
			hashMap6.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap6.put("48", "ANDROID");
			hashMap6.put("49", "2");
			hashMap6.put("50", "2");
			hashMap6.put("51", "");
			hashMap6.put("52", "10");
			hashMap6.put("53", "1");
			hashMap6.put("54", "50");
			hashMap6.put("55", "10");
			hashMap6.put("56", "90");
			hashMap6.put("57", "false");
			hashMap6.put("58", "0");
			hashMap6.put("59", "-1");
			hashMap6.put("60", "1st transaction desc.");
			hashMap6.put("61", "1st transaction");
			hashMap6.put("62", "\"com.swiggy.poc.test1\"");
			hashMap6.put("63", "w2fh4c46d0bszhl89voy");
			hashMap6.put("64", "false");
			hashMap6.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap6.put("66", "ANDROID");
			hashMap6.put("67", "2");
			hashMap6.put("68", "false");
			hashMap6.put("69", "false");
			hashMap6.put("70", "false");
			
			hashMap7.put("0", "ALPHACODE"); 
			hashMap7.put("1", "MULTICOUPON");
			hashMap7.put("2", "Internal");
			hashMap7.put("3", "AUTOMATION");
			hashMap7.put("4", "2");
			hashMap7.put("5", "10");
			hashMap7.put("6", "false");
			hashMap7.put("7", "false");
			hashMap7.put("8", "false");
			hashMap7.put("9", "false");
			hashMap7.put("10", "false");
			hashMap7.put("11", "false");
			hashMap7.put("12", "false");
			hashMap7.put("13", "false");
			hashMap7.put("14", "false");
			hashMap7.put("15", "false");
			hashMap7.put("16", "false");
			hashMap7.put("17", "1518934320000");
			hashMap7.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap7.put("19", "ankita.yadav@swiggy.in");
			hashMap7.put("20", "1518934320000");
			hashMap7.put("21", "1518934320000");
			hashMap7.put("22", "null");
			hashMap7.put("23", "null");
			hashMap7.put("24", "null");
			hashMap7.put("25", "-1");
			hashMap7.put("26", "false");
			hashMap7.put("27", "0");
			hashMap7.put("28", "null");
			hashMap7.put("29", "null");
			hashMap7.put("30", "null");
			hashMap7.put("31", "null");
			hashMap7.put("32", "1");
			hashMap7.put("33", "test123");
			hashMap7.put("34", "10");
			hashMap7.put("35", "1");
			hashMap7.put("36", "50");
			hashMap7.put("37", "10");
			hashMap7.put("38", "90");
			hashMap7.put("39", "false");
			hashMap7.put("40", "0");
			hashMap7.put("41", "-1");
			hashMap7.put("42", "1st transaction desc.");
			hashMap7.put("43", "1st transaction");
			hashMap7.put("44", "\"com.swiggy.poc.test1\"");
			hashMap7.put("45", "w2fh4c46d0bszhl89voy");
			hashMap7.put("46", "false");
			hashMap7.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap7.put("48", "ANDROID");
			hashMap7.put("49", "2");
			hashMap7.put("50", "2");
			hashMap7.put("51", "com.swiggy.poc.test1");
			hashMap7.put("52", "10");
			hashMap7.put("53", "1");
			hashMap7.put("54", "50");
			hashMap7.put("55", "10");
			hashMap7.put("56", "90");
			hashMap7.put("57", "false");
			hashMap7.put("58", "0");
			hashMap7.put("59", "-1");
			hashMap7.put("60", "1st transaction desc.");
			hashMap7.put("61", "1st transaction");
			hashMap7.put("62", "\"com.swiggy.poc.test1\"");
			hashMap7.put("63", "w2fh4c46d0bszhl89voy");
			hashMap7.put("64", "false");
			hashMap7.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap7.put("66", "ANDROID");
			hashMap7.put("67", "2");
			hashMap7.put("68", "false");
			hashMap7.put("69", "false");
			hashMap7.put("70", "false");
			
			return new Object[][] { { hashMap6,  duplicateType},
				{hashMap7,  duplicateType}
				
				};}}
	
	@DataProvider(name = "createMultiApplyCouponCartMinAmountData")
	public static Object[][] createMultiApplyCouponCartMinAmountData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();			
			String duplicateType="false";
			// String.valueOf(a1).toString();
			hashMap.put("0", "ALPHACODE"); 
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "1");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "1");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			
			hashMap1.put("0", "ALPHACODE"); 
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "0");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "0");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");
			
			hashMap1.put("0", "ALPHACODE"); 
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "1");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "0");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			hashMap2.put("0", "ALPHACODE"); 
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "0");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "1");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");
			
			hashMap3.put("0", "ALPHACODE"); 
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1519279920000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "500");
			hashMap3.put("35", "1");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Discount");
			hashMap3.put("52", "100");
			hashMap3.put("53", "1");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");
			
			return new Object[][] { { hashMap,  duplicateType},{ hashMap1,  duplicateType},{ hashMap2,  duplicateType},{ hashMap3,  duplicateType} };

		}}

	
	@DataProvider(name = "createMultiApplyCouponInvalidCartMinAmountData")
	public static Object[][] createMultiApplyCouponInvalidCartMinAmountData() {
		{
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			ArrayList a1 = new ArrayList();
			String duplicateType="false";
			// String.valueOf(a1).toString();
			
			
			hashMap1.put("0", "ALPHACODE"); 
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "null");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "null");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");
			
			hashMap2.put("0", "ALPHACODE"); 
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "null");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "null");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");
			return new Object[][] { { hashMap1,  duplicateType},{ hashMap2,  duplicateType} };

		}}
	
	@DataProvider(name = "createMultiApplyCouponCartMinQtyData")
	public static Object[][] createMultiApplyCouponCartMinQtyData() {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();
			
			ArrayList a1 = new ArrayList();
			String duplicateType="false";
			
			hashMap.put("0", "ALPHACODE002"); 
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			
			hashMap1.put("0", "ALPHACODE002"); 
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "0");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");
			
			
			hashMap2.put("0", "ALPHACODE002"); 
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "10");
			hashMap2.put("35", "0");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");
			
			
			hashMap3.put("0", "ALPHACODE002"); 
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1519279920000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "10");
			hashMap3.put("35", "0");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Discount");
			hashMap3.put("52", "10");
			hashMap3.put("53", "0");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");
			
			return new Object[][] { { hashMap,  duplicateType},{ hashMap1,  duplicateType},{ hashMap2,  duplicateType},{ hashMap3,  duplicateType},};

		}
	
	@DataProvider(name = "createMultiApplyCouponInvalidCartMinQtyData")
	public static Object[][] createMultiApplyCouponInvalidCartMinQtyData() {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();
			
			ArrayList a1 = new ArrayList();
			String duplicateType="false";
			
			hashMap.put("0", "ALPHACODE002"); 
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "null");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "null");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			
			hashMap1.put("0", "ALPHACODE002"); 
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "null");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "0");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");
			
			
			hashMap2.put("0", "ALPHACODE002"); 
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "10");
			hashMap2.put("35", "null");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");
			
			
			hashMap3.put("0", "ALPHACODE002"); 
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1525046400000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "10");
			hashMap3.put("35", "0");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Discount");
			hashMap3.put("52", "10");
			hashMap3.put("53", "null");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");
			
			return new Object[][] { { hashMap,  duplicateType},{ hashMap1,  duplicateType},{ hashMap2,  duplicateType},{ hashMap3,  duplicateType},};

		}


	@DataProvider(name = "PercentageTD_UpperCappingChanges")
	public Object[][] PercentageTD_UpperCappingChanges() throws IOException {
		HashMap<String, String> Keys = rngHelper.createTDWithUpperCapping();
		String restId = Keys.get("restId").toString();
		String userId = new Integer(rm.nextInt()).toString();
		String itemId = new Integer(rm.nextInt()).toString();
		String categoryId = new Integer(rm.nextInt()).toString();
		String subCategoryId = new Integer(rm.nextInt()).toString();

		String itemId2 = new Integer(rm.nextInt()).toString();
		String categoryId2 = new Integer(rm.nextInt()).toString();
		String subCategoryId2 = new Integer(rm.nextInt()).toString();

		String tid = Keys.get("TID").toString();
		String count = "1";


		List<ItemRequest> list1 = new ArrayList<>();
		list1.add(new ItemRequest(restId, categoryId, subCategoryId, itemId, "1", "600"));
		CartEvaluateRequest cartWithUpperCapping = new CartEvaluateRequest("600", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list1);

		List<ItemRequest> list2 = new ArrayList<>();
		list2.add(new ItemRequest(restId, categoryId, subCategoryId, itemId, "1", "400"));
		CartEvaluateRequest cartWithNoUpperCapping = new CartEvaluateRequest("400", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list2);

		List<ItemRequest> list3 = new ArrayList<>();
		list3.add(new ItemRequest(restId, categoryId, subCategoryId, itemId, "1", "300"));
		list3.add(new ItemRequest(restId, categoryId2, subCategoryId2, itemId2, "1", "300"));
		CartEvaluateRequest cartCappingWithEqualWeightageItems = new CartEvaluateRequest("600", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list3);

		List<ItemRequest> list4 = new ArrayList<>();
		list4.add(new ItemRequest(restId, categoryId, subCategoryId, itemId, "2", "200"));
		list4.add(new ItemRequest(restId, categoryId2, subCategoryId2, itemId2, "1", "200"));
		CartEvaluateRequest cartCappingWithDifferentWeightageItems = new CartEvaluateRequest("600", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list4);

		List<ItemRequest> list5 = new ArrayList<>();
		list5.add(new ItemRequest(restId, categoryId, subCategoryId, itemId, "1", "200"));
		list5.add(new ItemRequest(restId, categoryId2, subCategoryId2, itemId2, "1", "200"));
		CartEvaluateRequest cartWithEqualWeightageItems2 = new CartEvaluateRequest("400", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list5);

		List<ItemRequest> list6 = new ArrayList<>();
		list6.add(new ItemRequest(restId, categoryId, subCategoryId, itemId, "2", "100"));
		list6.add(new ItemRequest(restId, categoryId2, subCategoryId2, itemId2, "1", "200"));
		CartEvaluateRequest cartWithDifferentlWeightageItems2 = new CartEvaluateRequest("400", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229, list6);
		return new Object[][] {
				               { "cart", cartWithUpperCapping,false,"50.0","null","50.0" },
				               { "cart", cartWithNoUpperCapping,false,"40.0","null" ,"40.0"},
				               { "cart", cartCappingWithEqualWeightageItems,true,"25.0","25.0","50.0" },
				               { "cart", cartCappingWithDifferentWeightageItems,true,"33.33333333333333","16.666666666666664","50.0" },
				               { "cart", cartWithEqualWeightageItems2,true,"20.0","20.0","40.0" },
				               { "cart", cartWithDifferentlWeightageItems2,true,"20.0","20.0","40.0" }};
	}
	@DataProvider(name="orderEdit")
	public Object[][] orderEdit(){
		SANDHelper sandHelper = new SANDHelper();
		CreateCouponPOJO  createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(RngHelper.getCurrentDate())
				.withValidTill(RngHelper.getFutureDate());
		HashMap<String, Object> hm = new HashMap<String, Object>() {
			{	
				put("name", "dev");
				put("mobile", sandHelper.getMobile());
				put("password", "123456789");
				put("email",sandHelper.getemailString());
				put("createCouponPayload",createCouponPOJO);
				put("address",new AddressPOJO().setDefaultData());
			}
		};
		return new Object[][] {{hm}};
	}
	@DataProvider(name="orderEditCouponPercentage")
	public Object[][] orderEditCouponPercentage(){
		SANDHelper sandHelper = new SANDHelper();



		CreateCouponPOJO  createCouponPOJO = new CreateCouponPOJO()
				.setDefaultData()
				.withValidFrom(RngHelper.getCurrentDate())
				.withValidTill(RngHelper.getFutureDate())
				.withDiscountPercentage(20)
				.withDiscountAmount(90);
		HashMap<String, Object> hm = new HashMap<String, Object>() {
			{	
				put("name", "dev");
				put("mobile", sandHelper.getMobile());
				put("password", "123456789");
				put("email",sandHelper.getemailString());
				put("createCouponPayload",createCouponPOJO);
				put("address",new AddressPOJO().setDefaultData());
			}
		};
		return new Object[][] {{hm}};
	}
	
	@DataProvider(name="orderEditWithFlatTD")
	public Object[][] orderEditWithFlatTD(){
		SANDHelper sandHelper = new SANDHelper();
		
		HashMap<String, Object> hm = new HashMap<String, Object>() {
			{	
				put("name", "dev");
				put("mobile", sandHelper.getMobile());
				put("password", "123456789");
				put("email",sandHelper.getemailString());
				put("address",new AddressPOJO().setDefaultData());
				put("flat","111");
				put("minCart","200");
				put("editOrderQuantity","3");
				put("initSource","2");
				
			}
		};
		return new Object[][] {{hm}};
	}
	@DataProvider(name="orderEditWithPercentageTD")
	public Object[][] orderEditWithPercentageTD(){
SANDHelper sandHelper = new SANDHelper();
		HashMap<String, Object> hm = new HashMap<String, Object>() {
			{	
				put("name", "dev");
				put("mobile", sandHelper.getMobile());
				put("password", "123456789");
				put("email",sandHelper.getemailString());
				put("address",new AddressPOJO().setDefaultData());
				put("percentage","20");
				put("minCart","200");
				put("discountCap","200");
				put("editOrderQuantity","3");
				put("initSource","2");
			}
		};
		return new Object[][] {{hm}};
	}
	@DataProvider(name="orderEditPercentageWithDiscountCapTD")
	public Object[][] orderEditPercentageWithDiscountCapTD(){
SANDHelper sandHelper = new SANDHelper();
		HashMap<String, Object> hm = new HashMap<String, Object>() {
			{	
				put("name", "dev");
				put("mobile", sandHelper.getMobile());
				put("password", "123456789");
				put("email",sandHelper.getemailString());
				put("address",new AddressPOJO().setDefaultData());
				put("percentage","50");
				put("minCart","200");
				put("discountCap","90");
				put("editOrderQuantity","3");
				put("initSource","2");
			}
		};
		return new Object[][] {{hm}};
	}

}
