package com.swiggy.api.sf.rng.pojo.edvo;

import io.advantageous.boon.json.annotations.JsonProperty;

public class EDVOTimeSlot {

	@JsonProperty("OpenTime")
	private String openTime;
	@JsonProperty("closeTime")
	private String closeTime;
	@JsonProperty("day")
	private String day;

	public EDVOTimeSlot() {
		this.openTime = null;
		this.closeTime = null;
		this.day = null;
	}

	public EDVOTimeSlot(String day, int openTime, int closeTime) {
		this.day = day;
		this.openTime = Integer.toString(openTime);
		this.closeTime = Integer.toString(closeTime);
	}

	public String getOpenTime() {

		return this.openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime;

	}

	public String getCloseTime() {

		return this.closeTime;
	}

	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime;
	}

	public String getDay() {
		return this.day;
	}

	@Override
	public String toString() {

		if (!(day == null)) {
			return "[{" + "\"openTime\"" + ":" + openTime + "," + "\"closeTime\"" + ":" + closeTime + "," + "\"day\""
					+ ":" + "\"" + day + "\"" + "}]";
		} else {
			return "null";
		}
	}
	
	
	public String toPojo() {

		if (!(day == null)) {
			return "{" + "\"openTime\"" + ":" + openTime + "," + "\"closeTime\"" + ":" + closeTime + "," + "\"day\""
					+ ":" + "\"" + day + "\"" + "}";
		} else {
			return "null";
		}
	}

}