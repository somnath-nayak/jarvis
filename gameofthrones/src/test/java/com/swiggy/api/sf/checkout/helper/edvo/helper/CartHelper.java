package com.swiggy.api.sf.checkout.helper.edvo.helper;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.*;

import java.util.List;

public class CartHelper {
    public Cart getCart(List<CartItem> cartItems, List<MealItem> mealItems,
                        String restaurantId, String cartType, Integer addressId){
        return new CartBuilder()
            .cartItems(cartItems)
            .mealItems(mealItems)
            .restaurantId(restaurantId)
            .cartType(cartType)
            .build();

    }

    public CartItem getCartItem(List<Addon> addons, List<Variant> variants, String menuItemId, Integer quantity){
        return new CartItemBuilder()
                .addons(addons)
                .variants(variants)
                .menuItemId(menuItemId)
                .quantity(quantity)
                .build();
    }

    public MealItem getMealItem(List<Group> groups, Integer mealId, Integer quantity){
        return new MealItemBuilder()
                .groups(groups)
                .mealId(mealId)
                .quantity(quantity)
                .build();
    }

    public Group getGroup(List<Item> items, Integer groupId){
        return new GroupBuilder()
                .items(items)
                .groupId(groupId)
                .build();
    }

    public Item getItem(Integer menuItemId, Integer quantity, List<Addon> addons, List<Variant> variants){
        return new ItemBuilder()
                .menuItemId(menuItemId)
                .quantity(quantity)
                .addons(addons)
                .variants(variants)
                .build();
    }

    public Addon getAddon(String choiceId, String groupId, String name, Integer price){
        return new AddonBuilder()
                .choiceId(choiceId)
                .groupId(groupId)
                .build();
    }

    public Variant getVariant(Integer variationId, Integer groupId, String name, Integer price){
        return new VariantBuilder()
               .variationId(variationId)
                .groupId(groupId)
                .build();
    }
}
