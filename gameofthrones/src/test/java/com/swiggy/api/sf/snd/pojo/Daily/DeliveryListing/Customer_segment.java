package com.swiggy.api.sf.snd.pojo.Daily.DeliveryListing;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.pojo.Daily.DeliveryListing
 **/
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "unique_id",
        "user_id",
        "value_segment",
        "premium_segment",
        "high_eng_segment",
        "contact_affinity",
        "cancellation_probability",
        "churn_probability",
        "marketing_segment",
        "new_user",
        "customer_zone_id"
})
public class Customer_segment {

    @JsonProperty("unique_id")
    private Integer unique_id;
    @JsonProperty("user_id")
    private Integer user_id;
    @JsonProperty("value_segment")
    private String value_segment;
    @JsonProperty("premium_segment")
    private Integer premium_segment;
    @JsonProperty("high_eng_segment")
    private Integer high_eng_segment;
    @JsonProperty("contact_affinity")
    private Double contact_affinity;
    @JsonProperty("cancellation_probability")
    private Double cancellation_probability;
    @JsonProperty("churn_probability")
    private Double churn_probability;
    @JsonProperty("marketing_segment")
    private String marketing_segment;
    @JsonProperty("new_user")
    private Integer new_user;
    @JsonProperty("customer_zone_id")
    private Integer customer_zone_id;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("unique_id")
    public Integer getUnique_id() {
        return unique_id;
    }

    @JsonProperty("unique_id")
    public void setUnique_id(Integer unique_id) {
        this.unique_id = unique_id;
    }

    @JsonProperty("user_id")
    public Integer getUser_id() {
        return user_id;
    }

    @JsonProperty("user_id")
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    @JsonProperty("value_segment")
    public String getValue_segment() {
        return value_segment;
    }

    @JsonProperty("value_segment")
    public void setValue_segment(String value_segment) {
        this.value_segment = value_segment;
    }

    @JsonProperty("premium_segment")
    public Integer getPremium_segment() {
        return premium_segment;
    }

    @JsonProperty("premium_segment")
    public void setPremium_segment(Integer premium_segment) {
        this.premium_segment = premium_segment;
    }

    @JsonProperty("high_eng_segment")
    public Integer getHigh_eng_segment() {
        return high_eng_segment;
    }

    @JsonProperty("high_eng_segment")
    public void setHigh_eng_segment(Integer high_eng_segment) {
        this.high_eng_segment = high_eng_segment;
    }

    @JsonProperty("contact_affinity")
    public Double getContact_affinity() {
        return contact_affinity;
    }

    @JsonProperty("contact_affinity")
    public void setContact_affinity(Double contact_affinity) {
        this.contact_affinity = contact_affinity;
    }

    @JsonProperty("cancellation_probability")
    public Double getCancellation_probability() {
        return cancellation_probability;
    }

    @JsonProperty("cancellation_probability")
    public void setCancellation_probability(Double cancellation_probability) {
        this.cancellation_probability = cancellation_probability;
    }

    @JsonProperty("churn_probability")
    public Double getChurn_probability() {
        return churn_probability;
    }

    @JsonProperty("churn_probability")
    public void setChurn_probability(Double churn_probability) {
        this.churn_probability = churn_probability;
    }

    @JsonProperty("marketing_segment")
    public String getMarketing_segment() {
        return marketing_segment;
    }

    @JsonProperty("marketing_segment")
    public void setMarketing_segment(String marketing_segment) {
        this.marketing_segment = marketing_segment;
    }

    @JsonProperty("new_user")
    public Integer getNew_user() {
        return new_user;
    }

    @JsonProperty("new_user")
    public void setNew_user(Integer new_user) {
        this.new_user = new_user;
    }

    @JsonProperty("customer_zone_id")
    public Integer getCustomer_zone_id() {
        return customer_zone_id;
    }

    @JsonProperty("customer_zone_id")
    public void setCustomer_zone_id(Integer customer_zone_id) {
        this.customer_zone_id = customer_zone_id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("unique_id", unique_id).append("user_id", user_id).append("value_segment", value_segment).append("premium_segment", premium_segment).append("high_eng_segment", high_eng_segment).append("contact_affinity", contact_affinity).append("cancellation_probability", cancellation_probability).append("churn_probability", churn_probability).append("marketing_segment", marketing_segment).append("new_user", new_user).append("customer_zone_id", customer_zone_id).append("additionalProperties", additionalProperties).toString();
    }

}
