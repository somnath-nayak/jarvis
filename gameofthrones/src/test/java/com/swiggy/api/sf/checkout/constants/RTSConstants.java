package com.swiggy.api.sf.checkout.constants;

public interface RTSConstants {

    String RESTAURANT_ID_1 = "330";
    String MOBILE_1 = "8884292249";
    String PASSWORD_1 = "welcome123";
    String MOBILE_2 = "8085798950";
    String PASSWORD_2 = "welcome123";

    //Delivery helper related constants
    String DE_ID_1 = "60016";
    String DE_ID_2 = "60017";
    String CITY_ID = "1";
    String ZONE_ID = "4";
    String ENABLE_DE = "true";

    //Headers Constants
    String TID = "9c1c3296-392f-4e13-b6b3-fede612f7bca";
    String TOKEN = "3dcc12ab-2cde-47f4-9021-aea87e90aa6d48183cae-c6e2-4760-b887-b9bd67bd1e13";
    String AUTHORIZATION = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
    String PAYMENT_METHOD_CASH = "Cash";
    String ORDER_COMMENTS = "Automation Test Order";
    String VERSION_CODE = "300";
    String USER_AGENT = "Swiggy-Android";
    String SWUID = "Shaswat";
    String CONTENT_TYPE = "application/json";

    String CART_TYPE_REGULAR = "REGULAR";
    String CART_TYPE_PREORDER = "PREORDER";
    String CART_TYPE_POP = "POP";
    String CART_TYPE_CAFE = "CAFE";
    Integer ADDRESS_ID = 11126798;
    boolean SURGE = true;
    //Validation Constants
    int STATUS_CODE_CARTV2_SUCCESS = 0;
    String STATUS_MESSAGE_CARTV2_SUCCESS = "CART_UPDATED_SUCCESSFULLY";
    int STATUS_CODE_RESERVE_CART_SUCCESS = 0;
    String STATUS_MESSAGE_RESERVE_CART_SUCCESS = "Done Successfully";
    int STATUS_CODE_RESERVE_CART_NULL_CART = 5;
    String STATUS_MESSAGE_RESERVE_CART_NULL_CART = "Null cart for user";
    int STATUS_CODE_UNRESERVE_CART = 0;
    String STATUS_MESSAGE_UNRESERVE_CART = "Done Successfully";

    //TD Constants
    String MIN_CART_AMOUNT = "1";
    boolean FIRST_ORDER_RESTRICTION = false;
    boolean RESTAURANT_FIRST_ORDER = false;
    boolean USER_RESTRICTION = false;
    String DORMANT_USER_TYPE = "ZERO_DAYS_DORMANT";

    //TD Type
    String FREE_DELIVERY = "FREE_DELIVERY";
    String FREEBIE = "FREEBIE";
    String NO_TD = "NO_TD"; // Without any Trade Discount
    String MEAL_TD = "MEAL_TD";

    //Meal TD Type
    String MEAL_TD_TYPE_03 = "MEAL_TD_TYPE_03";
    String MEAL_ID = "5";

    //Delay required to reflect Freebie TD on cart after creation
    int FREEBIE_CACHE_DELAY = 20000; //unit is in milliseconds

}
