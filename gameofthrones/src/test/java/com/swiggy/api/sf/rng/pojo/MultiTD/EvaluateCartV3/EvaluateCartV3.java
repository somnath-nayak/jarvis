package com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.MultiTD
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "firstOrder",
        "itemRequests",
        "mealItemRequests",
        "minCartAmount",
        "planIds",
        "restaurantFirstOrder",
        "unit",
        "userAgent",
        "userId",
        "versionCode"
})
public class EvaluateCartV3 {

    @JsonProperty("firstOrder")
    private Boolean firstOrder;
    @JsonProperty("itemRequests")
    private List<ItemRequest> itemRequests = null;
    @JsonProperty("mealItemRequests")
    private List<MealItemRequest> mealItemRequests = null;
    @JsonProperty("minCartAmount")
    private Integer minCartAmount;
    @JsonProperty("restaurantBillAmount")
    private Integer restaurantBillAmount;
    @JsonProperty("planIds")
    private List<Integer> planIds = null;
    @JsonProperty("restaurantFirstOrder")
    private Boolean restaurantFirstOrder;
    @JsonProperty("unit")
    private String unit;
    @JsonProperty("userAgent")
    private String userAgent;
    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("versionCode")
    private Integer versionCode;
    @JsonProperty("firstOrder")
    public Boolean getFirstOrder() {
        return firstOrder;
    }

    @JsonProperty("firstOrder")
    public void setFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
    }

    @JsonProperty("itemRequests")
    public List<ItemRequest> getItemRequests() {
        return itemRequests;
    }

    @JsonProperty("itemRequests")
    public void setItemRequests(List<ItemRequest> itemRequests) {
        this.itemRequests = itemRequests;
    }

    @JsonProperty("mealItemRequests")
    public List<MealItemRequest> getMealItemRequests() {
        return mealItemRequests;
    }

    @JsonProperty("mealItemRequests")
    public void setMealItemRequests(List<MealItemRequest> mealItemRequests) {
        this.mealItemRequests = mealItemRequests;
    }

    @JsonProperty("minCartAmount")
    public Integer getMinCartAmount() {
        return minCartAmount;
    }

    @JsonProperty("minCartAmount")
    public void setMinCartAmount(Integer minCartAmount) {
        this.minCartAmount = minCartAmount;
    }

    @JsonProperty("planIds")
    public List<Integer> getPlanIds() {
        return planIds;
    }

    @JsonProperty("planIds")
    public void setPlanIds(List<Integer> planIds) {
        this.planIds = planIds;
    }

    @JsonProperty("restaurantFirstOrder")
    public Boolean getRestaurantFirstOrder() {
        return restaurantFirstOrder;
    }

    @JsonProperty("restaurantFirstOrder")
    public void setRestaurantFirstOrder(Boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
    }

    @JsonProperty("unit")
    public String getUnit() {
        return unit;
    }

    @JsonProperty("unit")
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @JsonProperty("userAgent")
    public String getUserAgent() {
        return userAgent;
    }

    @JsonProperty("userAgent")
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @JsonProperty("userId")
    public Integer getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @JsonProperty("versionCode")
    public Integer getVersionCode() {
        return versionCode;
    }

    @JsonProperty("versionCode")
    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public Integer getRestaurantBillAmount() {
        return restaurantBillAmount;
    }

    public void setRestaurantBillAmount(Integer restaurantBillAmount) {
        this.restaurantBillAmount = restaurantBillAmount;
    }

    private void setDefaultValues(Integer userID, boolean firstOrder, boolean restFirstOrder) {
//        List<Integer> plans =  new ArrayList<>();
//        plans.add(planID);
        if (this.getFirstOrder() == null)
            this.setFirstOrder(firstOrder);
        if (this.getItemRequests() == null)
            this.setItemRequests(new ArrayList<>());
        if (this.getRestaurantFirstOrder() == null)
            this.setRestaurantFirstOrder(restFirstOrder);
        if (this.getMinCartAmount() == null)
            this.setMinCartAmount(100);
//        if (this.getPlanIds() == null)
//            this.setPlanIds(plans);
        if (this.getUnit() == null)
            this.setUnit("RUPEES");
        if (this.getUserAgent() == null)
            this.setUserAgent("WEB");
        if (this.getUserId() == null)
            this.setUserId(userID);
        if (this.getVersionCode() == null)
            this.setVersionCode(0);
    }

    public EvaluateCartV3 build(Integer userID, boolean firstOrder, boolean restFirstOrder) {
        setDefaultValues(userID,firstOrder,restFirstOrder);
        return this;
    }



    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstOrder", firstOrder).append("itemRequests", itemRequests).append("mealItemRequests", mealItemRequests).append("minCartAmount", minCartAmount).append("planIds", planIds).append("restaurantFirstOrder", restaurantFirstOrder).append("unit", unit).append("userAgent", userAgent).append("userId", userId).append("versionCode", versionCode).toString();
    }

}
