package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.swiggy.api.sf.checkout.helper.CheckoutPricingHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;


import java.io.IOException;
import java.util.function.Predicate;

import com.swiggy.api.sf.snd.constants.SANDConstants;

import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.OrderPlace;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.pojo.DD.DD;
import com.swiggy.api.sf.snd.pojo.DD.DDBuilder;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.apache.commons.lang.time.DateUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DataServiceTest extends SnDDp{
    SANDHelper sandHelper= new SANDHelper();
    OrderPlace orderPlace= new OrderPlace();
    RngHelper rngHelper = new RngHelper();
    CheckoutPricingHelper checkoutPricingHelper= new CheckoutPricingHelper();
    Random random = new Random();




    //NEED to check with different discounts on item level

   /* @Test(dataProvider = "flatMenu", description = "apply flat discount at item level and verify the same on menu" )
    public void menuFlatItemLevel(String lat, String lng, String flat){
        List<String> restIds = orderPlace.Aggregator(new String[]{lat, lng}, null, null);
        try {
            //rngHelper.createFlatWithNoMinCartAtItemLevel()
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=sandHelper.menuV4RestId(restIds.get(1),lat, lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "no flat on this restaurant");
    }

    @Test(dataProvider = "freebieMenu", description = "apply free bie and verify the same on menu" )
    public void menuFreeBieItemLevel(String lat, String lng, String flat){
        List<String> restIds = orderPlace.Aggregator(new String[]{lat, lng}, null, null);
        try {
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", restIds.get(1),"10");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=sandHelper.menuV4RestId(restIds.get(1),lat, lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "no flat on this restaurant");
    }*/
/*
    @Test(dataProvider = "searchLatLng")
    public void searchV2(String name , String lat, String lng){
        String response = sandHelper.searchV2(name, lat,lng, null, null).ResponseValidator.GetBodyAsText();


    }*/


    @Test(dataProvider = "flatAggregator", description = "apply flat and verify the same on aggregator for a rest Id" )
    public void aggregatorFlat(String lat, String lng, String flat, String restaurant){
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        HashMap<String, String> hMap;
        try {
            hMap=rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", restIds.get(4),"10");
            String getTid= RngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), flat, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        System.out.println(restIds.get(2));
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(response, "$.data.." + restIds.get(4) + ".tradeDiscountInfo..discountType").contains(flat), "not flat restaurant");
       // Assert.assertTrue(sandHelper.JsonString(response, "$.data.." + restIds.get(2) + ".tradeDiscountInfo..operationType").contains(restaurant), "not flat restaurant");
    }

    @Test(dataProvider = "percentageAggregator", description = "apply percentage and verify the same on aggregator for a rest Id" )
    public void aggregatorPercentage(String lat, String lng, String percentage, String restaurant){
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        HashMap<String, String> hMap;
        try {
           hMap= rngHelper.createPercentageWithNoMinAmountAtRestaurantLevel(restIds.get(5));
            String getTid= RngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), percentage, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        System.out.println(restIds.get(2));
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(response, "$.data.." + restIds.get(5) + ".tradeDiscountInfo..discountType").contains(percentage), "not percentage restaurant");
       // Assert.assertTrue(sandHelper.JsonString(response, "$.data.." + restIds.get(2) + ".tradeDiscountInfo..operationType").contains(restaurant), "not percentage restaurant");
    }

    @Test(dataProvider = "freebieAggregator", description = "apply freebie and verify the same on aggregator for a rest Id" )
    public void aggregatorFreebie(String lat, String lng, String freebie) throws InterruptedException{
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        HashMap<String, String> hMap;
        sandHelper.TMenu(restIds.get(3));
        String item=orderPlace.getItemFromMenu(restIds.get(3),null, null);

        try {
            hMap=rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel("10", restIds.get(3), item);
            Thread.sleep(2000);
            String getTid= RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        System.out.println(restIds.get(3));
        Assert.assertTrue(sandHelper.JsonString(response, "$.data.."+restIds.get(3)+".tradeDiscountInfo..discountType").contains(freebie), "not freebie restaurant");
    }

    @Test(dataProvider = "freeDelAggregator", description = "apply free del and verify the same on aggregator for a rest Id" )
    public void aggregatorFreeDel(String lat, String lng, String freeDel, String restaurant){
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        HashMap<String, String>hMap;
        try {
            hMap=rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("10.0", restIds.get(7), false, false, false, "ZERO_DAYS_DORMANT", false);
            String getTid= RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), freeDel, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(response, "$.data.."+restIds.get(7)+".tradeDiscountInfo..discountType").contains(freeDel), "not free del restaurant");
      //  Assert.assertTrue(sandHelper.JsonString(response, "$.data.." + restIds.get(0) + ".tradeDiscountInfo..operationType").contains(restaurant), "not free del restaurant");
    }

    @Test(dataProvider = "rainMode", description = "apply a rain mode and verify the rain mode on aggregator")
    public void aggregatorRainMode(String lat, String lng,String rainMode, String removeRainMode, String heavy) throws InterruptedException{
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..id").split(","));
        List<String> zoneId = Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..areaId").split(","));
        sandHelper.rainMode(rainMode, zoneId.get(0));
        List<Integer> list = new ArrayList<>();
        for(int i=0; i<zoneId.size(); i++){
            if(zoneId.get(0).equals(zoneId.get(i))){
                list.add(i);
            }
        }
        Thread.sleep(10000);
        String response1=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(response1, "$.data.restaurants."+restIds.get(list.get(0))+"..rainMode"), heavy, "rain mode is not heavy");
        sandHelper.rainMode(removeRainMode, zoneId.get(0));
    }



    @Test(dataProvider = "rainMode", description = "apply a rain mode and verify the rain mode on aggregator")
    public void searchRainMode(String lat, String lng,String rainMode, String removeRainMode, String heavy) throws InterruptedException{
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..id").split(","));
        List<String> zoneId = Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..areaId").split(","));
        sandHelper.rainMode(rainMode, zoneId.get(0));
        List<Integer> list = new ArrayList<>();
        for(int i=0; i<zoneId.size(); i++){
            if(zoneId.get(0).equals(zoneId.get(i))){
                list.add(i);
            }
        }
        Thread.sleep(1000);
        String re= sandHelper.menuV4RestId(restIds.get(list.get(3)),lat,lng).ResponseValidator.GetBodyAsText();
        String name=sandHelper.JsonString(re,"$.data.name").replace(" ","+");
        sandHelper.solrIndexing(restIds.get(list.get(3)));
        String searchResponse=sandHelper.searchV2(name,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        try{
            Assert.assertEquals(sandHelper.JsonString(searchResponse, "$.data..rainMode"), heavy, "rain mode is not heavy");
        }catch (Exception e){
            sandHelper.rainMode(removeRainMode, zoneId.get(0));
            Assert.assertTrue(false, "rain mode is not applied");
        }

    }

    @Test(dataProvider = "slug", description = "verify the slug on aggregator")
    public void slugAggregator(String lat, String lng){
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..id").split(","));
        for(int i=0; i<10; i++) {
            String slug = sandHelper.JsonString(response, "$.data.restaurants."+restIds.get(i)+".slugs.restaurant");
            Map<String, Object> hMap=sandHelper.getSlug(restIds.get(i));
            Assert.assertEquals(slug, hMap.get("new_slug"), "slug is not present");
        }
    }

    @Test(dataProvider = "slug", description = "Verify cost for two on aggregator")
    public void costForTwoAggregator(String lat, String lng) {
        String response = sandHelper.aggregator(new String[]{lat, lng}).ResponseValidator.GetBodyAsText();
        List<String> restIds = Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..id").split(","));
        for(int i=0; i<10; i++) {
            String costForTwo = sandHelper.JsonString(response, "$.data.restaurants." + restIds.get(i) + ".costForTwo");
            Map<String, Object> hMap = sandHelper.getSlug(restIds.get(i));
            Assert.assertEquals(Integer.parseInt(costForTwo), (int) (Double.parseDouble(hMap.get("cost_for_two").toString())) * 100, "cost For two is not present");
        }
    }

        @Test(dataProvider = "slug", description = "verify slug on menu")
        public void slugMenu(String lat, String lng){
            String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
            List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..id").split(","));
            for(int i=0; i<50; i++) {
                String menuResponse = sandHelper.menuV4RestId(restIds.get(i), lat,lng).ResponseValidator.GetBodyAsText();
                String slug = sandHelper.JsonString(menuResponse, "$.data.restaurantSlug.restaurant");
                Map<String, Object> hMap=sandHelper.getSlug(restIds.get(i));
                Assert.assertEquals(slug, hMap.get("new_slug"), "slug is not present");
            }
        }



    @Test(dataProvider = "favourite",description = "get favorites for a user")
    public void setFavorite(String name, String password, String markFavorite, String lat, String lng) {
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        String mobileNo=sandHelper.getMobile();
        sandHelper.createUser(name, mobileNo, sandHelper.getemailString(),password);
        List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants.[*].id").split(","));
        List<String> favoriteRest=restIds.stream().limit(5).collect(Collectors.toList());
        favoriteRest.forEach(restId -> sandHelper.setFavorite(mobileNo, password,new String[]{markFavorite, "1", restId}));

        String getFavorite = sandHelper.favorites(lat, lng, sandHelper.loginData(mobileNo,password).get("userId")).ResponseValidator.GetBodyAsText();
        List<String> listFavorite= Arrays.asList(sandHelper.JsonString(getFavorite, "$.data.restaurants.[*].id").split(","));
        for(int i= 0; i<listFavorite.size(); i++){
            Assert.assertEquals(listFavorite.size(), favoriteRest.size(), "no. of rest ids are not equal");
            Assert.assertTrue(restIds.contains(listFavorite.get(i)), "rest ids are not same");
        }
    }

    @Test(dataProvider = "favouritePercentage",description = "get favorites for a user")
    public void setFavoritePercentage(String name, String password, String markFavorite, String lat, String lng, String percentage) {
        List<String> restIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        String mobileNo=sandHelper.getMobile();
        sandHelper.createUser(name, mobileNo, sandHelper.getemailString(),password);
        //List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants.[*].id").split(","));
        List<String> favoriteRest=restIds.stream().limit(1).collect(Collectors.toList());
        percentage(favoriteRest,percentage);
        favoriteRest.forEach(restId -> sandHelper.setFavorite(mobileNo, password,new String[]{markFavorite, "1", restId}));
        String getFavorite = sandHelper.favorites(lat, lng, sandHelper.loginData(mobileNo,password).get("userId")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getFavorite, "$.data..tradeDiscountInfo..discountType"),percentage, "NO discounts available on that  restaurant");
        List<String> listFavorite= Arrays.asList(sandHelper.JsonString(getFavorite, "$.data.restaurants.[*].id").split(","));
        for(int i= 0; i<listFavorite.size(); i++){
            Assert.assertEquals(listFavorite.size(), favoriteRest.size(), "no. of rest ids are not equal");
            Assert.assertTrue(restIds.contains(listFavorite.get(i)), "rest ids are not same");
        }
    }


    @Test(dataProvider = "favouriteFlat",description = "get favorites for a user and check discount on the rest id")
    public void setFavoriteFlat(String name, String password, String markFavorite, String lat, String lng, String flat) {
        List<String> restIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        String mobileNo=sandHelper.getMobile();
        sandHelper.createUser(name, mobileNo, sandHelper.getemailString(),password);
        //List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants.[*].id").split(","));
        List<String> favoriteRest=restIds.stream().limit(1).collect(Collectors.toList());
        flat(favoriteRest,flat);
        favoriteRest.forEach(restId -> sandHelper.setFavorite(mobileNo, password,new String[]{markFavorite, "1", restId}));
        String getFavorite = sandHelper.favorites(lat, lng, sandHelper.loginData(mobileNo,password).get("userId")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getFavorite, "$.data..tradeDiscountInfo..discountType"),flat, "NO discounts available on that restaurant");
        List<String> listFavorite= Arrays.asList(sandHelper.JsonString(getFavorite, "$.data.restaurants.[*].id").split(","));
        for(int i= 0; i<listFavorite.size(); i++){
            Assert.assertEquals(listFavorite.size(), favoriteRest.size(), "no. of rest ids are not equal");
            Assert.assertTrue(restIds.contains(listFavorite.get(i)), "rest ids are not same");
        }
    }

    @Test(dataProvider = "favouriteFreeDel",description = "get favorites for a user and check freeDel on the rest id")
    public void setFavoriteFreeDel(String name, String password, String markFavorite, String lat, String lng, String freeDelivery, String minAmount) {
        List<String> restIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        String mobileNo=sandHelper.getMobile();
        sandHelper.createUser(name, mobileNo, sandHelper.getemailString(),password);
        List<String> favoriteRest=restIds.stream().limit(1).collect(Collectors.toList());
        favoriteRest.forEach(restId -> sandHelper.setFavorite(mobileNo, password,new String[]{markFavorite, "1", restId}));
        freeDelivery(favoriteRest, minAmount, freeDelivery);
        String getFavorite = sandHelper.favorites(lat, lng, sandHelper.loginData(mobileNo,password).get("userId")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getFavorite, "$.data..tradeDiscountInfo..discountType"),freeDelivery, "NO discounts available on that restaurant");
        List<String> listFavorite= Arrays.asList(sandHelper.JsonString(getFavorite, "$.data.restaurants.[*].id").split(","));
        for(int i= 0; i<listFavorite.size(); i++){
            Assert.assertEquals(listFavorite.size(), favoriteRest.size(), "no. of rest ids are not equal");
            Assert.assertTrue(restIds.contains(listFavorite.get(i)), "rest ids are not same");
        }
    }

    @Test(dataProvider = "favouriteFreeBie",description = "get favorites for a user and check freeBie on the rest id")
    public void setFavoriteFreeBie(String name, String password, String markFavorite, String lat, String lng, String freeBie, String minAmount) {
        List<String> t= new ArrayList<>();
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        String mobileNo=sandHelper.getMobile();
        sandHelper.createUser(name, mobileNo, sandHelper.getemailString(),password);
        List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants.[*].id").split(","));         List<String> restTds=Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..tradeDiscountInfo").split(","));
        for(int i=0; i<restIds.size(); i++) {
            if(restTds.get(i).equals("null"))
            {
             t.add(restIds.get(i));
            }
        }
        List<String> favoriteRest=t.stream().limit(1).collect(Collectors.toList());
        favoriteRest.forEach(restId -> sandHelper.setFavorite(mobileNo, password,new String[]{markFavorite, "1", restId}));
        freeBie(favoriteRest, minAmount, freeBie);
        String getFavorite = sandHelper.favorites(lat, lng, sandHelper.loginData(mobileNo,password).get("userId")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(sandHelper.JsonString(getFavorite, "$.data..tradeDiscountInfo..discountType"),freeBie, "NO discounts available on that restaurant");
        List<String> listFavorite= Arrays.asList(sandHelper.JsonString(getFavorite, "$.data.restaurants.[*].id").split(","));
        for(int i= 0; i<listFavorite.size(); i++){
            Assert.assertEquals(listFavorite.size(), favoriteRest.size(), "no. of rest ids are not equal");
            Assert.assertTrue(restIds.contains(listFavorite.get(i)), "rest ids are not same");
        }
    }

    @Test(dataProvider = "unFavourite",description = "remove favorites for a user")
    public void removeFavorite(String name, String password, String markFavorite,String unMark, String lat, String lng) throws InterruptedException {
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        String mobileNo=sandHelper.getMobile();
        sandHelper.createUser(name, mobileNo, sandHelper.getemailString(),password);
        List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants.[*].id").split(","));
        //List<String> restNames= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants.[*].name").split(","));
        List<String> favoriteRest=restIds.stream().limit(5).collect(Collectors.toList());
        favoriteRest.forEach(restId -> sandHelper.setFavorite(mobileNo, password,new String[]{markFavorite, "1", restId}));

        String getFavorite = sandHelper.favorites(lat, lng, sandHelper.loginData(mobileNo,password).get("userId")).ResponseValidator.GetBodyAsText();
        List<String> listFavorite= Arrays.asList(sandHelper.JsonString(getFavorite, "$.data.restaurants.[*].id").split(","));
        for(int i= 0; i<listFavorite.size(); i++){
            Assert.assertEquals(listFavorite.size(), favoriteRest.size(), "no. of rest ids are not equal");
            Assert.assertTrue(restIds.contains(listFavorite.get(i)), "rest ids are not same");
        }
        favoriteRest.forEach(restId -> sandHelper.setFavorite(mobileNo, password,new String[]{unMark, "1", restId}));
        String notFavorite = sandHelper.favorites(lat, lng, sandHelper.loginData(mobileNo,password).get("userId")).ResponseValidator.GetBodyAsText();

        Thread.sleep(5000);
        List<String> listUnFavorite= Arrays.asList(sandHelper.JsonString(getFavorite, "$.data.restaurants.[*].id").split(","));
        for(int i= 0; i<favoriteRest.size(); i++){
            Assert.assertTrue(listUnFavorite.isEmpty(), "rest ids are not same");
        }
    }


    @Test(dataProvider = "searchV2Dish",description ="Search by dish name and verify in items", groups = "Search")
    public void searchV2ByDishName(String lat, String lng, String dish){
        String searchResponse= sandHelper.SearchV2(lat,lng,dish).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.items").split(",")).stream().anyMatch((s) -> s.matches(dish)), "items does not contain dish Item");
        }

    /*@Test(dataProvider = "searchV2RestName",description ="Search by rest name and verify in response")
    public void searchV2ByRestName(String lat, String lng){
        List<String> restIds=orderPlace.aggregator(new String[]{lat,lng}, null, null);
        String restName= sandHelper.restName(restIds.get(3)).replace(" ", "+");
        String searchResponse= sandHelper.SearchV2(lat,lng,restName).ResponseValidator.GetBodyAsText();
        //if rest name is not in rest List , check items list
       Assert.assertTrue(Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..name").split(",")).stream().anyMatch((s) -> s.matches(restName.replace("+", " "))), "items does not contain dish Item");
    }*/

    @Test(dataProvider = "searchV2Page",description ="Search by dishItem and verify in response", groups = "Search")
    public void searchV2Page(String lat, String lng, String dishItem, String page, String item){
        String searchResponse= sandHelper.SearchV2(lat,lng, dishItem,page, item).ResponseValidator.GetBodyAsText();
        List<String> t=Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..menuItems..name").split(","));
        List<String> dI= new ArrayList<>();
        for(String s:t){
            if(s.contains(dishItem)){
                dI.add(s);
            }
        }
        Assert.assertTrue(dI.stream().anyMatch((s) -> s.contains(dishItem)), "item is present in menu Items of the rest");
    }

    @Test(dataProvider = "searchV2FreeDeliver",description ="Verify free delivery in restaurant" ,groups = "Search" )
    public void searchV2FreeDelivery(String lat, String lng, String freeDel){
        List<String> restIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        String name= sandHelper.restName(restIds.get(1)).replace(" ","+");
        HashMap<String, String> hMap;
        try {
            hMap = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restIds.get(1),false, false, false, "ZERO_DAYS_DORMANT", false);
            String getTid= RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        System.out.println(restIds.get(1));
        String searchResponse= sandHelper.SearchV2(lat,lng, name).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchResponse,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }

    @Test(dataProvider = "SearchV2DiscountAtRestaurantLevel", description = "Verify discount at restaurant level", groups = "Search")
    public void searchV2Percentage(String lat,String lng, String percentage) {
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        String name= sandHelper.restName(restIds.get(0)).replace(" ","+");
        HashMap<String, String> hMap;
        try {
                hMap = rngHelper.createPercentageWithNoMinAmountAtRestaurantLevel(restIds.get(0));
                String getTid = RngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
                Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), percentage, "No discounts present or different type of discount is there");

            } catch (Exception e) {
                Assert.assertTrue(false, "No discounts present or different type of discount is there");
            }
            String response = sandHelper.searchV2(lat, lng,name).ResponseValidator.GetBodyAsText();
            Assert.assertTrue(sandHelper.JsonString(response, "$.data..tradeCampaignHeaders..discountType").contains(percentage), "No off on the product");
        }

    public void percentage(List<String> restIds, String percentage){
        HashMap<String, String> hMap;
        try {
            rngHelper.createPercentageTD(restIds.get(0));
            //String getTid=rngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
           // Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), percentage, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
    }

    public void flat(List<String> restIds, String flat){
        HashMap<String, String> hMap;
        try {
            hMap=rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", restIds.get(0),"10");
            String getTid= RngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), flat, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
    }

    public void flatTd(String restId, String flat){

        HashMap<String, String> hMap;
        try {
            hMap=rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", restId,"10");
            String getTid= RngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), flat, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
    }

    public void freeDelivery(List<String> restIds, String minAmount, String freeDel){
        HashMap<String, String> hMap;
        try {
            hMap = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(minAmount, restIds.get(0), false, false, false, zeroDormant, false);
            String getTid= RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), freeDel, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
    }


    public void freeBie(List<String> restIds,String minAmount, String freebie){
        HashMap<String, String> hMap;
        sandHelper.TMenu(restIds.get(0));
        String item = orderPlace.getItemFromMenu(restIds.get(0), null, null);
        try {
            hMap = rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel(minAmount, restIds.get(0), item);
            String getTid = RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), freebie, "No discounts present or different type of discount is there");
        } catch (Exception e) {
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
    }

    @Test(dataProvider = "SearchFlatDiscount", description = "Verify if any flat discount at restaurant level", groups = "Search")
    public void searchFlatRestaurant(String lat , String lng, String flat){
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        String name= sandHelper.restName(restIds.get(1)).replace(" ","+");
        flat(restIds,flat);
        String resp1=sandHelper.searchV2( lat,lng,name).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(resp1,"$.data..tradeCampaignHeaders..discountType").contains(flat), "No flat discount on the product");
    }

    @Test(dataProvider = "searchFreeBie", description = "Verify freebie at restaurant level", groups = "Search")
    public void searchV2FreebieTdRestaurantLevel(String keyword, String lat, String lng, String freeBie){

        String resp=sandHelper.searchV2( lat,lng,keyword).ResponseValidator.GetBodyAsText();

        List<String> list=Arrays.asList(sandHelper.JsonString(resp,"$.data.restaurants..restaurants..id").split(","))
                .stream()
                .limit(4)
                .collect(Collectors.toList());
        list.forEach(restId -> freeBie(list, minAmount, freeBie));
        String resp1=sandHelper.searchV2( lat,lng,keyword).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(resp1,"$.data..tradeCampaignHeaders..discountType").contains(freeBie), "No flat discount on the product");
    }

    @Test(dataProvider = "LatLong", description = "Verify cuisine of restaurants with db", groups = "Search")
    public void cuisine(String lat, String lng) {
        String resp = sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(statusCode, 0, "status code is 1");
        Assert.assertEquals(statusMessage, "done successfully");
        List<String> restIds = Arrays.asList(sandHelper.JsonString(resp, "$.data.restaurants[*].id").split(","));
        HashMap<String, List<String>> restaurantCuisine = new HashMap<>();
        for (int i = 0; i < restIds.size(); i++) {
            List<String> cuisine = Arrays.asList(sandHelper.JsonString(resp, "$.data.restaurants." + restIds.get(i) + ".cuisines"));
            restaurantCuisine.put(restIds.get(i), cuisine);
            List<Map<String, Object>> l = sandHelper.cuisine(restIds.get(i));
            Assert.assertEquals(restaurantCuisine.get(restIds.get(i)).toString().replaceAll("\\]|\\[", ""), l.get(0).get("cuisine"), "Response and db does not matches");
        }
    }


    @Test(dataProvider = "latLongDel", description = "creating free del collection")
    public void freeDelColOnAggregator(String lat ,String lng, String identifier) throws InterruptedException{
        List<String> l=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            List<String> firstNElementsList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", l.get(i), false, false, false, "ZERO_DAYS_DORMANT", false);
            }
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        Thread.sleep(4000);
        String aggregatorAfter=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggregatorAfter, "$.data.collections[*].id").split(","));
        sandHelper.JsonString(aggregatorAfter, "$.data.collections[*].id");
        System.out.println("collection id"+ id);
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }

    @Test(dataProvider = "percentageCol", description = "creating TD collection and verify it will work super user")
    public void percentageColOnAggregator(String lat ,String lng, String identifier) throws InterruptedException {
        List<String> l=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            List<String> restList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<restList.size(); i++) {
                rngHelper.createPercentageTD(restList.get(i));
            }
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id).ResponseValidator.GetBodyAsText();
        Thread.sleep(2000);
        String aggregatorAfterColl=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggregatorAfterColl, "$.data.collections[*].id").split(","));
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }

    @Test(dataProvider = "percentGreaterFirstCol", description = "creating TD collection and verify it will work super user")
    public void percentageColFirst(String lat ,String lng, String identifier) throws InterruptedException {
        List<String> l=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            List<String> firstNElementsList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createPercentageTD(firstNElementsList.get(i));
            }
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id).ResponseValidator.GetBodyAsText();
        Thread.sleep(1000);
        String aggregatorAfterColl=sandHelper.aggregator(new String[]{lat,lng},null,null).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggregatorAfterColl, "$.data.collections[*].id").split(","));
       // sandHelper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id");
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }

    @Test(dataProvider = "percentageLessThanCol", description = "creating TD collection and verify it will work super user")
    public void percentageLessThanCol(String lat ,String lng, String identifier) throws InterruptedException {
        List<String> l=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            List<String> firstNElementsList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createPercentageTD(firstNElementsList.get(i));
            }
            System.out.println("list of rest ids"+firstNElementsList);
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id).ResponseValidator.GetBodyAsText();
        Thread.sleep(1000);
        String aggregatorAfter=sandHelper.aggregator(new String[]{lat,lng},null,null).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggregatorAfter, "$.data.collections[*].id").split(","));
        // sandHelper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id");
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }



    @Test(dataProvider = "slaCollection", description = "creating sla collection and verify it will work super user")
    public void slaCollection(String lat ,String lng, String identifier) throws InterruptedException {
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id).ResponseValidator.GetBodyAsText();
        Thread.sleep(4000);
        String aggregatorSla=sandHelper.aggregator(new String[]{lat,lng}, null,null).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggregatorSla, "$.data.collections[*].id").split(","));
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }


    @Test(dataProvider = "longDistance", description = "creating long distance collection and verify it will work super user")
    public void longDistanceColOnAggregator(String lat ,String lng, String identifier) throws InterruptedException{
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        Thread.sleep(4000);
        String aggAfterCollection=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id").split(","));
        sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id");
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }


    @Test(dataProvider = "offers", description = "apply offer and verify in aggregator")
    public void offerOnAggregator(String lat ,String lng, String identifier){
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        String aggAfterCollection=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id").split(","));
        sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id");
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }

    @Test(dataProvider = "trending", description = "create a trending collection and verify in aggregator")
    public void trendingCollection(String lat ,String lng, String identifier){
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        String aggAfterCollection=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id").split(","));
        sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id");
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }


    @Test(dataProvider = "vegOnly", description = "create a veg only collection and verify in aggregator")
    public void vegOnlyCollection(String lat ,String lng, String identifier){
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        String aggAfterCollection=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id").split(","));
        sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id");
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }

    @Test(dataProvider = "ratings", description = "create a ratings collection and verify in aggregator")
    public void ratingsCollection(String lat ,String lng, String identifier){
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        String aggAfterCollection=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id").split(","));
        sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id");
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }

    @Test(dataProvider = "costForTwo", description = "create a cost for two collection and verify in aggregator")
    public void costForTwoCollection(String lat ,String lng, String identifier) throws InterruptedException{
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        Thread.sleep(2000);
        System.out.println("coll id"+id);
        String aggAfterCollection=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id").split(","));
        sandHelper.JsonString(aggAfterCollection, "$.data.collections[*].id");

        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }



    @Test(dataProvider = "distanceFee", description = "apply distance fee and verify in aggregator")
    public void distanceFeeDetails(String lat ,String lng, String startTime,String endTime,String defaultFee,String distanceFrom,String distanceTo,String cartFee) throws InterruptedException{
        List<String> l=orderPlace.aggregator(new String[]{lat,lng}, null, null);
        checkoutPricingHelper.setDistanceFee(l.get(0),startTime,endTime,defaultFee,distanceFrom,distanceTo,cartFee);
        Thread.sleep(10000);
        sandHelper.deleteCheckoutPricingCache(l.get(0));
        String aggregator=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(Integer.parseInt(sandHelper.JsonString(aggregator, "$.data.."+l.get(0)+".feeDetails.fees.[1].fee"))/100, cartFee);
    }

    @Test(dataProvider = "timeFee", description = "apply time fee and verify in aggregator")
    public void timeFeeDetails(String lat ,String lng, String startTime,String endTime,String cartFee){
        List<String> l=orderPlace.aggregator(new String[]{lat,lng}, null, null);
        checkoutPricingHelper.setTimeFee(l.get(1),startTime,endTime,cartFee);
        String aggregator=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(Integer.parseInt(sandHelper.JsonString(aggregator, "$.data.."+l.get(1)+".feeDetails.fees.[2].fee"))/100, Integer.parseInt(cartFee), "time fee is not applied");

    }

    @Test(dataProvider = "specialFee", description = "apply special fee and verify in aggregator")
    public void specialFeeDetails(String lat ,String lng, String rainMode,String cartFee,String priority, int fromDate, int toDate){
        List<String> l=orderPlace.aggregator(new String[]{lat,lng}, null, null);
        checkoutPricingHelper.setSpecialFee(l.get(1),rainMode,cartFee,priority,sandHelper.dateTime(fromDate),sandHelper.dateTime(toDate));
        String aggregator=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(Integer.parseInt(sandHelper.JsonString(aggregator, "$.data.."+l.get(1)+".feeDetails.fees.[3].fee"))/100, Integer.parseInt(cartFee), "time fee is not applied");

    }

    @Test(dataProvider = "timeFee", description = "apply time fee and verify in menu")
    public void timeFeeDetailsMenu(String lat ,String lng, String startTime,String endTime,String cartFee){
        List<String> l=orderPlace.aggregator(new String[]{lat,lng}, null, null);
        checkoutPricingHelper.setTimeFee(l.get(1),startTime,endTime,cartFee);
        String menu=sandHelper.menuV4RestId(l.get(1),lat,lng).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(Integer.parseInt(sandHelper.JsonString(menu, "$.data..feeDetails.fees.[2].fee"))/100, Integer.parseInt(cartFee), "time fee is not applied");
    }



    @Test(dataProvider = "multi", description ="apply percentage and free del on a rest id and verify the same on aggregator" )
    public void multiTdAggregator(String lat ,String lng) throws InterruptedException{
        List<String> listOfRestIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            System.out.println("ids"+listOfRestIds.get(1));
                rngHelper.createPercentageWithMinAmountAtRestaurantLevel(listOfRestIds.get(1), false);
                rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(1), false, false, false, "ZERO_DAYS_DORMANT", false,false);
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        Thread.sleep(4000);
        String aggAfterDiscount=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        System.out.println("idddd"+sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(1)+".tradeDiscountInfo..discountType"));
        System.out.println("rest id"+listOfRestIds.get(1));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(1)+".tradeDiscountInfo..discountType").contains(percent));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(1)+".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel));
    }

    @Test(dataProvider = "multi", description ="apply percentage and free del on a rest id and verify the same on menu" )
    public void multiTdMenu(String lat ,String lng) throws InterruptedException{
        List<String> listOfRestIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        tdPercentFreeDel(listOfRestIds);
        Thread.sleep(4000);
        String response=sandHelper.menuV4RestId(listOfRestIds.get(0),lat, lng).ResponseValidator.GetBodyAsText();
        System.out.println("777"+sandHelper.JsonString(response,"$.data.aggregatedDiscountInfo.descriptionList..discountType"));
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(response,"$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(percent), "no free bie on this restaurant");
    }

    public void tdPercentFreeDel(List<String> listOfRestIds){
        try {
            System.out.println("ids"+listOfRestIds.get(0));
            rngHelper.createPercentageTD(listOfRestIds.get(0));
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(0), false, false, false, "ZERO_DAYS_DORMANT", false);

        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
    }

    /*@Test(dataProvider = "multi", description ="apply percentage and free del on a rest id and verify the same on search" )
    public void multiTdSearch(String lat ,String lng){
        List<String> listOfRestIds=orderPlace.aggregator(new String[]{lat,lng}, null, null);
       tdPercentFreeDel(listOfRestIds);
        String searchAfterDiscount=sandHelper.searchV2(lat,lng,sandHelper.restName(listOfRestIds.get(0)).replace(" ", "+"), null, null).ResponseValidator.GetBodyAsText();


    }*/


    @Test(dataProvider = "multi", description ="apply flat and free del on a rest id and verify the same on aggregator" )
    public void multiTdAggregatorFlat(String lat ,String lng) throws InterruptedException{
        List<String> listOfRestIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", listOfRestIds.get(3),"10", false);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(3), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(3));
        String aggAfterDiscount=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(3)+".tradeDiscountInfo..discountType").contains(flat));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(3)+".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel));
    }


    @Test(dataProvider = "multi", description ="apply flat and free del on a rest id and verify the same on menu" )
    public void multiTdMenuFlat(String lat ,String lng) throws InterruptedException{
        List<String> listOfRestIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            System.out.println("ids"+listOfRestIds.get(4));
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", listOfRestIds.get(4),"10", false);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(4), false, false, false, "ZERO_DAYS_DORMANT", false, false);

        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }

        String menuAfterDiscount=sandHelper.menuV4RestId(listOfRestIds.get(4),lat,lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount,"$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "no free bie on this restaurant");
    }

    @Test(dataProvider = "multi", description ="apply flat and free del on a rest id and verify the same on menu" )
    public void multiTdSearchFlat(String lat ,String lng){
        List<String> listOfRestIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        String restName= sandHelper.restName(listOfRestIds.get(2)).replace(" ", "+");
        try {
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", listOfRestIds.get(2),"10", false);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(2), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        String searchAfterDiscount=sandHelper.searchV2(restName,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }


    @Test(dataProvider = "searchByCuisine",description ="Search by cuisine name and verify in response")
    public void searchByCuisine(String lat, String lng, String cuisine){
        //if rest name is not in cuisine List , check items list
        List<Map<String, Object>> l =sandHelper.cuisineList();
        for(Map<String, Object> map:l){
            String arr[]=map.get("cuisine").toString().split(",");
            for(String a:arr){
            String searchResponse= sandHelper.SearchV2(lat,lng,a.replace(" ", "+")).ResponseValidator.GetBodyAsText();
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..cuisine").split(",")).stream().anyMatch((s) -> s.matches(a)), "cuisine does not contain dish Item");
        }}
    }

    @Test(dataProvider = "searchByCuisine",description ="verify all nodes in search response")
    public void searchNodes(String lat, String lng, String cuisine){
        String searchResponse= sandHelper.SearchV2(lat,lng,"pizza").ResponseValidator.GetBodyAsText();
        List<String> op=Arrays.asList(SANDConstants.nodes);
        List<String> type= Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..type").split(","));
        List<String> restaurantNodes= Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..restaurants").split(","));
       // System.out.println(op.size()+"jjj"+restaurantNodes.size());
        for (int i=0; i<type.size(); i++) {
            Assert.assertTrue(op.contains(type.get(i)), "type is not present");
            //Assert.assertTrue(Optional.of(restaurantNodes.get(i)).isPresent(), "restaurant node is not present");
        }
    }


    @Test(dataProvider = "searchByCuisine",description ="verify all nodes in search response")
    public void searchParamInItemList(String lat, String lng, String cuisine) {
        String searchResponse = sandHelper.SearchV2(lat, lng, "pizza").ResponseValidator.GetBodyAsText();
        String items =sandHelper.JsonString(searchResponse, "$.data.items");
        Pattern pattern = Pattern.compile(",");
        List<String> list = pattern.splitAsStream(items)
                .map(String::valueOf)
                .collect(Collectors.toList());
            Assert.assertTrue(list.contains("pizza"), "item is not found");
    }


    @Test(dataProvider = "searchByCuisine",description ="verify dish sugesstion with 2 or more words in search response")
    public void searchParam2Words(String lat, String lng, String cuisine) {
        String searchResponse = sandHelper.SearchV2(lat, lng, "panner+paratha").ResponseValidator.GetBodyAsText();
        String items = sandHelper.JsonString(searchResponse, "$.data.items");
        Pattern pattern = Pattern.compile(",");
        List<String> list=pattern.splitAsStream(items)
                .map(String::valueOf)
                .collect(Collectors.toList());
        //Assert.assertEquals();
    }

    @Test(dataProvider = "searchV2Page",description ="Search by dishItem and verify in response")
    public void searchNoDishSuggestion(String lat, String lng, String dishItem, String page, String item){
        String searchResponse= sandHelper.SearchV2(lat,lng, dishItem,page, item).ResponseValidator.GetBodyAsText();
        List<String> list=Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.items").split(","));
        Assert.assertTrue(list.contains(null) || list.contains(""), "item is present in menu Items of the rest");
    }

    @Test
    public void tes() throws IOException{
        DD dd = new DDBuilder().build();
        JsonHelper jsonHelper= new JsonHelper();
        String s=jsonHelper.getObjectToJSON(dd);
        String r= sandHelper.dDCollection(s).ResponseValidator.GetBodyAsText();
    }

    @Test(dataProvider = "freebie", description = "creating freebie collection and verify it works for super user")
    public void freeCollectionSuper(String lat ,String lng, String identifier){
        List<String> restIds = orderPlace.aggregator(new String[]{lat, lng}, null, null);
        HashMap<String, String> hMap;
        String item=orderPlace.getItemFromMenu(restIds.get(4),null, null);
        try {
            System.out.println("restId="+restIds.get(4));
            hMap=rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel("10", restIds.get(4), item);
            String getTid=rngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), freebie, "No freebie present");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present");
        }
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        String aggregatorAfter=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggregatorAfter, "$.data.collections[*].id").split(","));
        sandHelper.JsonString(aggregatorAfter, "$.data.collections[*].id");
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");

    }

    @Test(dataProvider = "free_del_discount_percent_greater_than", description = "creating TD collection of free_del_discount_percent_greater_than")
    public void free_del_discount_percent_greater_than(String lat ,String lng, String identifier) {
        List<String> l=orderPlace.aggregator(new String[]{lat,lng}, null, null);
        try {
            List<String> restList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<restList.size(); i++) {
                rngHelper.createPercentageTD(restList.get(i));
            }
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }

        HashMap<String, String>hMap;
        try {
            List<String> restList = l.stream().limit(5).collect(Collectors.toList());
            for (int i = 0; i < restList.size(); i++) {
                hMap = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("10.0", restList.get(i), false, false, false, "ZERO_DAYS_DORMANT", false);
                String getTid = rngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
                Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), freeDel, "No discounts present or different type of discount is there");
            }
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id).ResponseValidator.GetBodyAsText();
        String aggregatorAfterColl=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggregatorAfterColl, "$.data.collections[*].id").split(","));
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }

}







