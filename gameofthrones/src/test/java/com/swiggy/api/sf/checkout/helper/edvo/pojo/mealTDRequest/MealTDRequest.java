package com.swiggy.api.sf.checkout.helper.edvo.pojo.mealTDRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "id",
        "namespace",
        "header",
        "description",
        "valid_from",
        "valid_till",
        "campaign_type",
        "restaurant_hit",
        "enabled",
        "swiggy_hit",
        "createdBy",
        "discountCap",
        "discounts",
        "slots",
        "commissionOnFullBill",
        "taxesOnDiscountedBill",
        "firstOrderRestriction",
        "timeSlotRestriction",
        "userRestriction"
})
public class MealTDRequest {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("namespace")
    private String namespace;
    @JsonProperty("header")
    private String header;
    @JsonProperty("description")
    private String description;
    @JsonProperty("valid_from")
    private String validFrom;
    @JsonProperty("valid_till")
    private String validTill;
    @JsonProperty("campaign_type")
    private String campaignType;
    @JsonProperty("restaurant_hit")
    private String restaurantHit;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("swiggy_hit")
    private String swiggyHit;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("discountCap")
    private Integer discountCap;
    @JsonProperty("discounts")
    private List<Discount> discounts = null;
    @JsonProperty("slots")
    private List<Object> slots = null;
    @JsonProperty("commissionOnFullBill")
    private Boolean commissionOnFullBill;
    @JsonProperty("taxesOnDiscountedBill")
    private Boolean taxesOnDiscountedBill;
    @JsonProperty("firstOrderRestriction")
    private Boolean firstOrderRestriction;
    @JsonProperty("timeSlotRestriction")
    private Boolean timeSlotRestriction;
    @JsonProperty("userRestriction")
    private Boolean userRestriction;

    /**
     * No args constructor for use in serialization
     *
     */
    public MealTDRequest() {
    }

    /**
     *
     * @param timeSlotRestriction
     * @param enabled
     * @param campaignType
     * @param commissionOnFullBill
     * @param userRestriction
     * @param swiggyHit
     * @param discountCap
     * @param restaurantHit
     * @param validTill
     * @param header
     * @param namespace
     * @param id
     * @param firstOrderRestriction
     * @param createdBy
     * @param slots
     * @param description
     * @param discounts
     * @param validFrom
     * @param taxesOnDiscountedBill
     */
    public MealTDRequest(Integer id, String namespace, String header, String description, String validFrom, String validTill, String campaignType, String restaurantHit, Boolean enabled, String swiggyHit, String createdBy, Integer discountCap, List<Discount> discounts, List<Object> slots, Boolean commissionOnFullBill, Boolean taxesOnDiscountedBill, Boolean firstOrderRestriction, Boolean timeSlotRestriction, Boolean userRestriction) {
        super();
        this.id = id;
        this.namespace = namespace;
        this.header = header;
        this.description = description;
        this.validFrom = validFrom;
        this.validTill = validTill;
        this.campaignType = campaignType;
        this.restaurantHit = restaurantHit;
        this.enabled = enabled;
        this.swiggyHit = swiggyHit;
        this.createdBy = createdBy;
        this.discountCap = discountCap;
        this.discounts = discounts;
        this.slots = slots;
        this.commissionOnFullBill = commissionOnFullBill;
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        this.firstOrderRestriction = firstOrderRestriction;
        this.timeSlotRestriction = timeSlotRestriction;
        this.userRestriction = userRestriction;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("namespace")
    public String getNamespace() {
        return namespace;
    }

    @JsonProperty("namespace")
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    @JsonProperty("header")
    public String getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(String header) {
        this.header = header;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("valid_from")
    public String getValidFrom() {
        return validFrom;
    }

    @JsonProperty("valid_from")
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    @JsonProperty("valid_till")
    public String getValidTill() {
        return validTill;
    }

    @JsonProperty("valid_till")
    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    @JsonProperty("campaign_type")
    public String getCampaignType() {
        return campaignType;
    }

    @JsonProperty("campaign_type")
    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    @JsonProperty("restaurant_hit")
    public String getRestaurantHit() {
        return restaurantHit;
    }

    @JsonProperty("restaurant_hit")
    public void setRestaurantHit(String restaurantHit) {
        this.restaurantHit = restaurantHit;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("swiggy_hit")
    public String getSwiggyHit() {
        return swiggyHit;
    }

    @JsonProperty("swiggy_hit")
    public void setSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @JsonProperty("discountCap")
    public Integer getDiscountCap() {
        return discountCap;
    }

    @JsonProperty("discountCap")
    public void setDiscountCap(Integer discountCap) {
        this.discountCap = discountCap;
    }

    @JsonProperty("discounts")
    public List<Discount> getDiscounts() {
        return discounts;
    }

    @JsonProperty("discounts")
    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }

    @JsonProperty("slots")
    public List<Object> getSlots() {
        return slots;
    }

    @JsonProperty("slots")
    public void setSlots(List<Object> slots) {
        this.slots = slots;
    }

    @JsonProperty("commissionOnFullBill")
    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    @JsonProperty("commissionOnFullBill")
    public void setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public Boolean getTaxesOnDiscountedBill() {
        return taxesOnDiscountedBill;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public void setTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
    }

    @JsonProperty("firstOrderRestriction")
    public Boolean getFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    @JsonProperty("firstOrderRestriction")
    public void setFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
    }

    @JsonProperty("timeSlotRestriction")
    public Boolean getTimeSlotRestriction() {
        return timeSlotRestriction;
    }

    @JsonProperty("timeSlotRestriction")
    public void setTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
    }

    @JsonProperty("userRestriction")
    public Boolean getUserRestriction() {
        return userRestriction;
    }

    @JsonProperty("userRestriction")
    public void setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("namespace", namespace).append("header", header).append("description", description).append("validFrom", validFrom).append("validTill", validTill).append("campaignType", campaignType).append("restaurantHit", restaurantHit).append("enabled", enabled).append("swiggyHit", swiggyHit).append("createdBy", createdBy).append("discountCap", discountCap).append("discounts", discounts).append("slots", slots).append("commissionOnFullBill", commissionOnFullBill).append("taxesOnDiscountedBill", taxesOnDiscountedBill).append("firstOrderRestriction", firstOrderRestriction).append("timeSlotRestriction", timeSlotRestriction).append("userRestriction", userRestriction).toString();
    }

}