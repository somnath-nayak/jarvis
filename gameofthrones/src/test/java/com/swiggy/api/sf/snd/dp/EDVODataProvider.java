package com.swiggy.api.sf.snd.dp;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.snd.constants.EDVOConstants;
import com.swiggy.api.sf.snd.helper.EDVOHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.PropertiesHandler;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import framework.gameofthrones.JonSnow.Processor;

import java.util.List;
import java.util.Map;


public class EDVODataProvider implements EDVOConstants{

    String Env;

    SnDHelper sndHelper = new SnDHelper();
    EDVOHelper edvoHelper= new EDVOHelper();
    com.swiggy.api.sf.rng.helper.EDVOHelper rngEDVOHelper = new com.swiggy.api.sf.rng.helper.EDVOHelper();

    static boolean flag=false;

    static String lat ;
    static String lng ;
    static String restId;
    static List itemIds;
    static List edvoGroupIds;
    static long campaignId;

    public static long getCampaignId() {
        return campaignId;
    }

    public static void setCampaignId(long campaignId) {
        EDVODataProvider.campaignId = campaignId;
    }

    public static String getMealId() {
        return mealId;
    }

    public static void setMealId(String mealId) {
        EDVODataProvider.mealId = mealId;
    }

    static String mealId ;

    public static String getRestId() {
        return restId;
    }

    public static void setRestId(String restId) {
        EDVODataProvider.restId = restId;
    }

    public static String getLat() {
        return lat;
    }

    public static void setLat() {
        EDVODataProvider.lat = EDVOConstants.lat;
    }

    public static String getLng() {
        return lng;
    }

    public static void setLng() {
        EDVODataProvider.lng = EDVOConstants.lng;
    }

    public static List getItemIds() {
        return itemIds;
    }

    public static void setItemIds(List itemIds) {
        EDVODataProvider.itemIds = itemIds;
    }

    public static void setGroupIds(List edvoGroupIds){
        EDVODataProvider.edvoGroupIds=edvoGroupIds;
    }

    public static List getGroupIds() {
        return edvoGroupIds;
    }

    public void createMealData() throws InterruptedException {

         if (flag == false) {
             setLat();
             setLng();

             //pick the rest ID
             Processor processor = sndHelper.getAggregatorDetails(lat, lng);
             String restId = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.sortedRestaurants[0]..restaurantId").toString().replace("[\"", "").replace("\"]", "");
             setRestId(restId);


             // get the itemIds
             Processor menuV4Response = edvoHelper.getMenuV4(restId, lat, lng);
             List<Integer> itemIds = JsonPath.read(menuV4Response.ResponseValidator.GetBodyAsText(), "$.data.menu.items[*].id");
             //String itemIds = menuV4Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.menu.items[*].id");
             setItemIds(itemIds);

             if (itemIds.size() == 0) {
                 Assert.assertNotNull(itemIds, "Items list is not empty..!!!");
             }

             String itemId1 = itemIds.get(0).toString();
             String itemId2 = itemIds.get(1).toString();
             String itemId3 = itemIds.get(2).toString();
             String itemId4 = itemIds.get(3).toString();
             String itemId5 = itemIds.get(4).toString();

             edvoHelper.deleteMealData(restId);

             // add the meal
             Processor processor1 = edvoHelper.addMeal(restId, itemId1, itemId2, itemId3, itemId4, itemId5);
//             System.out.println("------------------------------------"+processor1.ResponseValidator.GetNodeValue("$.id"));
             String mealId = String.valueOf(processor1.ResponseValidator.GetNodeValueAsInt("$.id"));
             setMealId(mealId);

             List<Map<String, Object>> edvoGroupIds = edvoHelper.getEDVOGroupIds(mealId);

             // create a campaign
             setCampaignId(rngEDVOHelper.createEDVOCamp(getRestId(), getMealId()));

             //clear redis
             try {
                 edvoHelper.clearEDVORedis(restId,mealId);
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }

             flag = true;
         }

    }

    public PropertiesHandler properties = new PropertiesHandler();

    public EDVODataProvider() {
        if (System.getenv("ENVIRONMENT") == null)
            Env = properties.propertiesMap.get("environment");
        else
            Env = System.getenv("ENVIRONMENT");
    }

    @DataProvider(name = "menuV4" )
    public Object[][] getMenuV4() throws InterruptedException {
        createMealData();
        return new Object[][] {
//                {"23798","12.933","77.601"}
                {restId,lat,lng}
        };
    }

    @DataProvider(name = "bulkMeal" )
    public Object[][] getBulkMeal() throws InterruptedException {
        createMealData();
        return new Object[][] {
                {"12"},
                {"1759,42,23"},
                {mealId},
                {""}
        };
    }

    @DataProvider(name = "bulkMealSchema" )
    public Object[][] getBulkMealSchema() throws InterruptedException {
        createMealData();
        return new Object[][] {
                {mealId},
        };
    }
    @DataProvider(name = "mealAvailable" )
    public  Object[][] getMealAvailable() throws InterruptedException {
        createMealData();
        String items = itemIds.get(0).toString() + "," + itemIds.get(1).toString();
        return new Object[][] {
                // {"675213,565614","23786"},
                {items,restId}
        };
    }

    @DataProvider(name = "mealAvailableNegative" )
    public Object[][] getMealAvailableNegative() throws InterruptedException {
       // createMealData();
        String invalidRestId = String.valueOf(edvoHelper.createNegativeMealData()+10L);
        return new Object[][] {
                {"650215,650252",invalidRestId},

        };
    }

    @DataProvider(name = "menuV4DataValidation")
    public Object[][] validateMenuV4Widgets() throws InterruptedException{
        createMealData();
        return new Object[][] {
                {restId,lat,lng}
        };
    }


    @DataProvider(name = "menuV4MealsValidation" )
    public Object[][] validateMealsDataInMenuV4() throws InterruptedException {
        createMealData();
        return new Object[][] {
                {restId,lat,lng,campaignId}
        };
    }

    @DataProvider(name = "menuV4DataValidationNonEDVO" )
    public Object[][] validateWidgetsNonEDVOInMenuV4() throws InterruptedException {
        createMealData();
        return new Object[][] {
                {"276","12.933","77.601"}
        };
    }

    @DataProvider(name = "EDVOImageUpdate" )
    public  Object[][] EDVOImageUpdate() throws InterruptedException {
        createMealData();
        return new Object[][] {
                {restId,lat,lng,"newtafsghjkjhsads"}
        };
    }

    @DataProvider(name = "EDVOTextUpdate" )
    public  Object[][] EDVOTextUpdate() throws InterruptedException {
        createMealData();
        return new Object[][] {
                {restId,lat,lng,"Updated the text",campaignId}
        };
    }

    @DataProvider(name = "EDVOSubTextUpdate" )
    public Object[][] EDVOSubTextUpdate()throws InterruptedException  {
        createMealData();
        return new Object[][] {
                {restId,lat,lng,"Updated the Sub-text",campaignId}
        };
    }

    @DataProvider(name="addMeal")
    public  Object[][] AddMeal() throws InterruptedException{
        createMealData();
        return new Object[][]{
                {restId,itemIds.get(0).toString(),itemIds.get(1).toString(),itemIds.get(2).toString(),itemIds.get(3).toString(),itemIds.get(4).toString()}
        };
    }

    @DataProvider(name="getMealById")
    public Object[][] getMealById()throws InterruptedException {
        createMealData();
        return new Object[][]{
                {mealId,itemIds.get(0).toString()}
        };
    }

    @DataProvider(name="validateMeals")
    public  Object[][] getMealByIdValidData() throws InterruptedException{
        createMealData();
        String group1 = edvoGroupIds.get(0).toString();
        String group2 = edvoGroupIds.get(1).toString();

        return new Object[][]{
                {group1,itemIds.get(0).toString(),"1",group2,itemIds.get(0).toString(),"1",mealId}
        };
    }

    @DataProvider(name="validateMealsNegative")
    public  Object[][] getMealByIdInvalidData()throws InterruptedException {
        createMealData();
        return new Object[][]{
                {"2090","10456","1","2086","10432","1","1035"}
        };
    }

    @DataProvider(name="validateMinChoiceForGroup")
    public  Object[][] validateMinChoiceForGroup() throws InterruptedException{
        createMealData();

        String group1 = edvoGroupIds.get(0).toString();
        String group2 = edvoGroupIds.get(1).toString();
        String maxChoice="2";
        edvoHelper.setMaxChoice(group1,maxChoice);
        edvoHelper.setMaxTotal(group1,maxChoice);

        String quantity2 = edvoHelper.getMaxTotal(group2) ;

        return new Object[][]{
                {group1,itemIds.get(0).toString(),"1",itemIds.get(1).toString(),"1",group2,itemIds.get(0).toString(),"1",mealId,"2"}
//                {"4697","1064227","1","1064228","1","4698","1064228","1","2360"}
        };
    }

    @DataProvider(name="validateMaxQuantityForGroup")
    public  Object[][] validateMaxQuantityForGroup() throws InterruptedException{
        createMealData();

        String group1 = edvoGroupIds.get(0).toString();
        String group2 = edvoGroupIds.get(1).toString();
        String quantity1 = edvoHelper.getMaxTotal(group1) ;
        String quantity2 = edvoHelper.getMaxTotal(group2) ;

        String updatedMaxTotal = quantity1+2;
        edvoHelper.setMaxTotal(group1,updatedMaxTotal);

        return new Object[][]{
                {group1,itemIds.get(0).toString(),updatedMaxTotal,group2,itemIds.get(1).toString(),quantity2,mealId}
        };
    }

    @DataProvider(name="launchPageUpdateSubText")
    public  Object[][] launchPageUpdateSubText() throws InterruptedException{
        createMealData();
        return new Object[][]{
                {mealId,itemIds.get(0).toString(),"launchPageUpdateSubText"}
        };
    }

    @DataProvider(name="launchPageUpdateMainText")
    public  Object[][] launchPageUpdateMainText() throws InterruptedException{
        createMealData();
        return new Object[][]{
                {mealId,itemIds.get(0).toString(),"launchPageUpdateMainText"}
        };
    }
    @DataProvider(name="exitPageUpdateSubText")
    public  Object[][] exitPageUpdateSubText() throws InterruptedException{
        createMealData();
        return new Object[][]{
                {mealId,itemIds.get(0).toString(),"exitPageUpdateSubText"}
        };
    }

    @DataProvider(name="exitPageUpdateMainText")
    public  Object[][] exitPageUpdateMainText() throws InterruptedException {
        createMealData();
        return new Object[][]{
                {mealId,itemIds.get(0).toString(),"exitPageUpdateMainText"}
        };
    }

    @DataProvider(name="screenOrder")
    public  Object[][] screenOrder() throws InterruptedException{
        createMealData();
        return new Object[][]{
                {mealId,itemIds.get(0).toString()}
        };
    }

    @DataProvider(name="minTotal")
    public  Object[][] minTotal() throws InterruptedException{
        createMealData();

        String group1 = edvoGroupIds.get(0).toString();
        String group2 = edvoGroupIds.get(1).toString();
        String quantity1 = edvoHelper.getMinTotal(group1) ;
        String quantity2 = edvoHelper.getMinTotal(group2) ;
        int newMinQuantity = Integer.valueOf(quantity1) -  1;

        return new Object[][]{
                {group1,itemIds.get(0).toString(),newMinQuantity,group2,itemIds.get(0).toString(),quantity2,mealId}
                //{"4697","1064227","0","4698","1064228","1","2360"}
        };
    }

    @DataProvider(name="maxTotal")
    public  Object[][] maxTotal() throws InterruptedException{
        createMealData();

        String group1 = edvoGroupIds.get(0).toString();
        String group2 = edvoGroupIds.get(1).toString();
        String quantity1 = edvoHelper.getMinTotal(group1) ;
        String quantity2 = edvoHelper.getMinTotal(group2) ;
        int newMaxQuantity = Integer.valueOf(quantity1) +  1;

        return new Object[][]{
                {group1,itemIds.get(0).toString(),newMaxQuantity,group2,itemIds.get(0).toString(),quantity2,mealId}
               // {"4697","1064227","20","4698","1064228","1","2360"}
        };
    }

    @DataProvider(name="mockedServiceability")
    public Object[][] getmockedServiceability() throws InterruptedException{
        createMealData();
        return new Object[][]{
                {"1",restId,lat,lng},
                {"0",restId,lat,lng}
        };
    }

}
