package com.swiggy.api.sf.rng.dp.coupondp;

import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.helper.*;
import com.swiggy.api.sf.rng.pojo.*;
import org.json.JSONException;
import org.testng.annotations.DataProvider;

import java.util.HashMap;

public class CouponSuperDP {

	RngHelper rngHelper = new RngHelper();


	@DataProvider(name = "createPublicDiscountTypeCouponForUserTypeSuperData")
	public static Object[][] createPublicDiscountTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createUserMapDiscountTypeCouponForUserTypeSuperData")
	public static Object[][] createUserMapDiscountTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

//	============================================MAP and DELETE Coupons - UPI Handle====================================================

	@DataProvider(name = "UPICouponMapAndDeletePublic")
	public Object[][] mapCouponForUserType() throws JSONException {
		{
			CreateCouponPOJO createCouponPOJO =  new CreateCouponPOJO ()
					.setDefaultData()
					.withIsPrivate(RngConstants.is_private_0)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType(RngConstants.couponTypeFreeDel)
					.withDiscountPercentage(RngConstants.discount_percentage)
					.withDiscountAmount(RngConstants.discount_amount)
					.withCouponUserTypeId(RngConstants.coupon_user_type_id)
					.withPreferredPaymentMethod(RngConstants.preferred_method)
					.withDescription(RngConstants.description)
					.withCode(RngConstants.publicCoupon + Utility.getRandomPostfix());
			UPIPaymentContractPOJO upiPaymentContractPOJO = new UPIPaymentContractPOJO()
					.setDefault(createCouponPOJO.getCode(),RngConstants.okicici,RngConstants.UPI_HANDLE);
			return new Object[][] {{createCouponPOJO, upiPaymentContractPOJO}};
		}
	}

	@DataProvider(name = "UPICouponMapAndDeletePrivate")
	public Object[][] mapCouponForUserPrivateType() throws JSONException {
		{
			CreateCouponPOJO createCouponPOJO =  new CreateCouponPOJO ()
					.setDefaultData()
					.withIsPrivate(RngConstants.is_private_1)
					.withCustomerRestriction(RngConstants.customer_restriction)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType(RngConstants.coupon_type_discount)
					.withDiscountPercentage(RngConstants.discount_percentage)
					.withDiscountAmount(RngConstants.discount_amount)
					.withCouponUserTypeId(RngConstants.coupon_user_type_id)
					.withPreferredPaymentMethod(RngConstants.preferred_method)
					.withDescription(RngConstants.description)
					.withCode(RngConstants.privateCoupon + Utility.getRandomPostfix());
			UPIPaymentContractPOJO upiPaymentContractPOJO = new UPIPaymentContractPOJO()
					.setDefault(createCouponPOJO.getCode(),RngConstants.okicici,RngConstants.UPI_HANDLE);
			return new Object[][] {{createCouponPOJO, upiPaymentContractPOJO}};
		}
	}

	@DataProvider(name = "UPICouponMapAndDeleteSuper")
	public Object[][] mapCouponForUserSuperType() throws JSONException {
		{
			CreateCouponPOJO createCouponPOJO =  new CreateCouponPOJO ()
					.setDefaultData()
					.withIsPrivate(RngConstants.is_private_1)
					.withCustomerRestriction(RngConstants.customer_restriction)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType(RngConstants.coupon_type_discount)
					.withDiscountPercentage(RngConstants.discount_percentage)
					.withDiscountAmount(RngConstants.discount_amount)
					.withCouponUserTypeId(RngConstants.coupon_user_type_id)
					.withPreferredPaymentMethod(RngConstants.preferred_method)
					.withDescription(RngConstants.description)
					.withCode(RngConstants.superCoupon + Utility.getRandomPostfix());
			UPIPaymentContractPOJO upiPaymentContractPOJO = new UPIPaymentContractPOJO()
					.setDefault(createCouponPOJO.getCode(),RngConstants.okicici,RngConstants.UPI_HANDLE);
			return new Object[][] {{createCouponPOJO, upiPaymentContractPOJO}};
		}
	}

	@DataProvider(name = "UPIApplyPublic")
	public Object[][] Type() throws JSONException {
		{
			CreateCouponPOJO createCouponPOJO =  new CreateCouponPOJO ()
					.setDefaultData()
					.withIsPrivate(RngConstants.is_private_0)
					.withCustomerRestriction(RngConstants.customer_restriction)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType(RngConstants.coupon_type_discount)
					.withDiscountPercentage(RngConstants.discount_percentage)
					.withDiscountAmount(RngConstants.discount_amount)
					.withCouponUserTypeId(RngConstants.coupon_user_type_id)
					.withPreferredPaymentMethod(RngConstants.preferred_method)
					.withDescription(RngConstants.description)
					.withCouponUserTypeId(RngConstants.coupon_user_type_id)
					.withUserClient(RngConstants.user_client)
					.withCode(Utility.getRandomPostfix());
			UPIPaymentContractPOJO upiPaymentContractPOJO = new UPIPaymentContractPOJO()
					.setDefault(createCouponPOJO.getCode(),RngConstants.okicici,RngConstants.UPI_HANDLE);
			CouponApplyPOJO couponApplyPOJO = new CouponApplyPOJO().setDefault(Utility.getRandomCodePrefix());
			return new Object[][] {{createCouponPOJO, upiPaymentContractPOJO, couponApplyPOJO}};
		}
	}

	@DataProvider(name = "GetDetailsPublic")
	public Object[][] getDetailsType() {

		HashMap<String, Object> data = new HashMap<String, Object>();
		return new Object[][] {{data}};
	}


//	====================================================================================================================

	@DataProvider(name = "createPublicFreeDelTypeCouponForUserTypeSuperData")
	public static Object[][] createPublicFreeDelTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test123")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createUserMapFreeDelTypeCouponForUserTypeSuperData")
	public static Object[][] createUserMapFreeDelTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

//=========================================

	@DataProvider(name = "createPublicDiscountTypeCouponForUserTypeNonSuperData")
	public static Object[][] createPublicDiscountTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createUserMapDiscountTypeCouponForUserTypeNonSuperTest")
	public static Object[][] createUserMapDiscountTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createPublicFreeDelTypeCouponForUserTypeNonSuperTest")
	public static Object[][] createPublicFreeDelTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createUserMapFreeDelTypeCouponForUserTypeNonSuperTest")
	public static Object[][] createUserMapFreeDelTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	//=========================================

	@DataProvider(name = "createPublicDiscountTypeCouponForUserTypeALLData")
	public static Object[][] createPublicDiscountTypeCouponForUserTypeALL() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createUserMapDiscountTypeCouponForUserTypeALLData")
	public static Object[][] createUserMapDiscountTypeCouponForUserTypeALL() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createPublicFreeDelTypeCouponForUserTypeALLData")
	public static Object[][] createPublicFreeDelTypeCouponForUserTypeALL() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createUserMapFreeDelTypeCouponForUserTypeALLData")
	public static Object[][] createUserMapFreeDelTypeCouponForUserTypeALL() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	//=========================================

	@DataProvider(name = "createPublicDiscountTypeCouponForInvalidUserTypeIdData")
	public static Object[][] createPublicDiscountTypeCouponForInvalidUserTypeId() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(3)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createUserMapDiscountTypeCouponForInvalidUserTypeIdData")
	public static Object[][] createUserMapDiscountTypeCouponForInvalidUserTypeId() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(3)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createPublicFreeDelTypeCouponForInvalidUserTypeIdData")
	public static Object[][] createPublicFreeDelTypeCouponForInvalidUserTypeId() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(3)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "createUserMapFreeDelTypeCouponForInvalidUserTypeIdData")
	public static Object[][] createUserMapFreeDelTypeCouponForInvalidUserTypeId() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(3)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	//===============  Get by user id and city id for Super user =================

	@DataProvider(name = "getByUserIdCityIdPublicDiscountTypeCouponForUserTypeSuperData")
	public static Object[][] getByUserIdCityIdPublicDiscountTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityIdUserMapDiscountTypeCouponForUserTypeSuperData")
	public static Object[][] getByUserIdCityIdUserMapDiscountTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdCityIdPublicFreeDelTypeCouponForUserTypeSuperData")
	public static Object[][] GetByUserIdCityIdPublicFreeDelTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityUserMapFreeDelTypeCouponForUserTypeSuperData")
	public static Object[][] getByUserIdCityUserMapFreeDelTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}


	//===============  Get by user id and city id super coupon for non Super user =================

	@DataProvider(name = "getByUserIdCityIdPublicDiscountTypeSuperCouponForUserTypeNonSuperData")
	public static Object[][] getByUserIdCityIdPublicDiscountTypeSuperCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityIdUserMapDiscountTypeSuperCouponForUserTypeNonSuperData")
	public static Object[][] getByUserIdCityIdUserMapDiscountTypeSuperCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdCityIdPublicFreeDelTypeSuperCouponForUserTypeNonSuperData")
	public static Object[][] GetByUserIdCityIdPublicFreeDelTypeSuperCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityUserMapFreeDelTypeSuperCouponForUserTypeNonSuperData")
	public static Object[][] getByUserIdCityUserMapFreeDelTypeSuperCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	//===============  Get by user id and city id non super coupon for non Super user =================

	@DataProvider(name = "getByUserIdCityIdPublicDiscountTypeNonSuperCouponForUserTypeNonSuperData")
	public static Object[][] getByUserIdCityIdPublicDiscountTypeNonSuperCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeNonSuperData")
	public static Object[][] getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdCityIdPublicFreeDelTypeNonSuperCouponForUserTypeNonSuperData")
	public static Object[][] GetByUserIdCityIdPublicFreeDelTypeNonSuperCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityIdUserMapFreeDelTypeNonSuperCouponForUserTypeNonSuperData")
	public static Object[][] getByUserIdCityIdUserMapFreeDelTypeNonSuperCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	// ========== Get by user id city id , when no mapping for user ==============

	@DataProvider(name = "getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeNonSuperWhenUserNotMappedData")
	public static Object[][] getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeNonSuperWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenUserNotMappedData")
	public static Object[][] getByUserIdCityIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityUserMapFreeDelTypeSuperCouponForUserTypeSuperDataWhenUserNotMappedData")
	public static Object[][] getByUserIdCityUserMapFreeDelTypeSuperCouponForUserTypeSuperDataWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}


	@DataProvider(name = "getByUserIdCityUserMapFreeDelTypeNonSuperCouponForUserTypeNonSuperDataWhenUserNotMappedData")
	public static Object[][] getByUserIdCityUserMapFreeDelTypeNonSuperCouponForUserTypeNonSuperDataWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}


	@DataProvider(name = "getByUserIdCityIdUserMapDiscountTypeAllCouponForUserTypeALLWhenUserNotMappedData")
	public static Object[][] getByUserIdCityIdUserMapDiscountTypeAllCouponForUserTypeALLWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(0)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityUserMapFreeDelTypeALLCouponForUserTypeALLDataWhenUserNotMappedData")
	public static Object[][] getByUserIdCityUserMapFreeDelTypeALLCouponForUserTypeALLDataWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}


	//===========================================Get by user id-city id  non-Super type coupon for Super user  ========================

	@DataProvider(name = "getByUserIdCityIdPublicDiscountTypeNonSuperCouponForUserTypeSuperData")
	public static Object[][] getByUserIdCityIdPublicDiscountTypeNonSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeSuperData")
	public static Object[][] getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdCityIdPublicFreeDelTypeNonSuperCouponForUserTypeSuperData")
	public static Object[][] GetByUserIdCityIdPublicFreeDelTypeNonSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityIdUserMapFreeDelTypeNonSuperCouponForUserTypeSuperData")
	public static Object[][] getByUserIdCityIdUserMapFreeDelTypeNonSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	//===========================================Get by user id-city id  ALL type coupon  ========================

	@DataProvider(name = "getByUserIdCityIdPublicDiscountALLTypeCouponForBothUsersSuperNnonSuperData")
	public static Object[][] getByUserIdCityIdPublicDiscountALLTypeCouponForBothUsersSuperNnonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityIdUserMapDiscountALLTypeCouponForBothUserSuperNnonSuperData")
	public static Object[][] getByUserIdCityIdUserMapDiscountALLTypeCouponForBothUserSuperNnonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdCityIdPublicFreeDelALLTypeCouponForBothUserSuperNnonSuperData")
	public static Object[][] GetByUserIdCityIdPublicFreeDelALLTypeCouponForBothUserSuperNnonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdCityIdUserMapFreeDelALLTypeCouponForBothUserSuperNnonSuperData")
	public static Object[][] getByUserIdCityIdUserMapFreeDelALLTypeCouponForBothUserSuperNnonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("NOTSUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	// ====== Get by user id Super type coupon for Super user   ========================================

	@DataProvider(name = "getByUserIdPublicDiscountTypeSuperCouponForUserTypeSuperData")
	public static Object[][] getByUserIdPublicDiscountTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperData")
	public static Object[][] getByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdPublicFreeDelTypeSuperCouponForUserTypeSuperData")
	public static Object[][] GetByUserIdPublicFreeDelTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperData")
	public static Object[][] getByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

// ====== Get by user id Super type coupon for Non-Super user   ========================================

	@DataProvider(name = "getByUserIdPublicDiscountTypeSuperCouponForUserNonSuperData")
	public static Object[][] getByUserIdPublicDiscountTypeSuperCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapDiscountTypeSuperCouponForUserNonSuperData")
	public static Object[][] getByUserIdUserMapDiscountTypeSuperCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdPublicFreeDelTypeSuperCouponForUserNonSuperData")
	public static Object[][] GetByUserIdPublicFreeDelTypeSuperCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapFreeDelTypeSuperCouponForUserNonSuperData")
	public static Object[][] getByUserIdUserMapFreeDelTypeSuperCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	// ====== Get by user id Non-Super type coupon for Non-Super user   ========================================

	@DataProvider(name = "getByUserIdPublicDiscountTypeNonSuperCouponForUserNonSuperData")
	public static Object[][] getByUserIdPublicDiscountTypeNonSuperCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapDiscountTypeNonSuperCouponForUserNonSuperData")
	public static Object[][] getByUserIdUserMapDiscountTypeNonSuperCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdPublicFreeDelTypeNonSuperCouponForUserNonSuperData")
	public static Object[][] GetByUserIdPublicFreeDelTypeNonSuperCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapFreeDelTypeNonSuperCouponForUserNonSuperData")
	public static Object[][] getByUserIdUserMapFreeDelTypeNonSuperCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	// ====== Get by user id Non-Super type coupon for Super user   ========================================

	@DataProvider(name = "getByUserIdPublicDiscountTypeNonSuperCouponForUserSuperData")
	public static Object[][] getByUserIdPublicDiscountTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapDiscountTypeNonSuperCouponForUserSuperData")
	public static Object[][] getByUserIdUserMapDiscountTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdPublicFreeDelTypeNonSuperCouponForUserSuperData")
	public static Object[][] GetByUserIdPublicFreeDelTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapFreeDelTypeNonSuperCouponForUserSuperData")
	public static Object[][] getByUserIdUserMapFreeDelTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}


	// ====== Get by user id ALL type coupon for both - Super & Non-Super users   ========================================

	@DataProvider(name = "getByUserIdPublicDiscountALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] getByUserIdPublicDiscountALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] getByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "GetByUserIdPublicFreeDelALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] GetByUserIdPublicFreeDelALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("AmazonPay,Sodexo")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] getByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}
// ====== Get by user id  when no user mapping   ========================================

	@DataProvider(name = "getByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] getByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapFreeDelTypeNonSuperCouponFoNonSuperUserData")
	public static Object[][] getByUserIdUserMapFreeDelTypeNonSuperCouponFoNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenUserNotMappedData")
	public static Object[][] getByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperDataWhenUserNotMappedData")
	public static Object[][] getByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperDataWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapDiscountTypeAllCouponForUserTypeALLWhenUserNotMappedData")
	public static Object[][] getByUserIdUserMapDiscountTypeAllCouponForUserTypeALLWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "getByUserIdUserMapFreeDelTypeALLCouponForUserTypeALLDataWhenUserNotMappedData")
	public static Object[][] getByUserIdUserMapFreeDelTypeALLCouponForUserTypeALLDataWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withFreeShipping(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	// ============== apply Super user coupon  as Super user ======================

	@DataProvider(name = "applyPublicDiscountTypeSuperCouponForUserTypeSuperData")
	public static Object[][] applyPublicDiscountTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapDiscountTypeSuperCouponForUserTypeSuperData")
	public static Object[][] applyUserMapDiscountTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyPublicFreeDelTypeSuperCouponForUserTypeSuperData")
	public static Object[][] applyPublicFreeDelTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapFreeDelTypeSuperCouponForUserTypeSuperData")
	public static Object[][] applyUserMapFreeDelTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	// ============== apply non-Super user coupon  as Super user ======================

	@DataProvider(name = "applyPublicDiscountTypeNonSuperCouponForUserSuperData")
	public static Object[][] applyPublicDiscountTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapDiscountTypeNonSuperCouponForUserSuperData")
	public static Object[][] applyUserMapDiscountTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyPublicFreeDelTypeNonSuperCouponForUserSuperData")
	public static Object[][] applyPublicFreeDelTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapFreeDelTypeNonSuperCouponForUserSuperData")
	public static Object[][] applyUserMapFreeDelTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	// ============== apply non-Super user coupon  as non-Super user ======================

	@DataProvider(name = "applyPublicDiscountTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] applyPublicDiscountTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapDiscountTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] applyUserMapDiscountTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyPublicFreeDelTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] applyPublicFreeDelTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapFreeDelTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] applyUserMapFreeDelTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	// ============== apply Super user coupon  as non-Super user ======================

	@DataProvider(name = "applyPublicDiscountTypeSuperCouponForNonSuperUserData")
	public static Object[][] applyPublicDiscountTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapDiscountTypeSuperCouponForNonSuperUserData")
	public static Object[][] applyUserMapDiscountTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyPublicFreeDelTypeSuperCouponForNonSuperUserData")
	public static Object[][] applyPublicFreeDelTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapFreeDelTypeSuperCouponForNonSuperUserData")
	public static Object[][] applyUserMapFreeDelTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	// ============== apply ALL type coupon  as both- Super & Non-Super user ======================

	@DataProvider(name = "applyPublicDiscountTypeALLCouponForBothSuperNnonSuperUserData")
	public static Object[][] applyPublicDiscountTypeALLCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(0)
					.withCustomerRestriction(0)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] applyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyPublicFreeDelALLTypeCouponForSuperNnonSuperUserData")
	public static Object[][] applyPublicFreeDelALLTypeCouponForSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] applyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	@DataProvider(name = "applyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedData")
	public static Object[][] applyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	@DataProvider(name = "applyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedData")
	public static Object[][] applyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(0)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}

//============================== Apply Super type when no user mapping  =======================================

	@DataProvider(name = "applyUserMapDiscountSuperTypeCouponForSuperUserWhenUserNotMappedData")
	public static Object[][] applyUserMapDiscountSuperTypeCouponForSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	@DataProvider(name = "applyUserMapFreeDelSuperTypeCouponForSuperUserWhenUserNotMappedData")
	public static Object[][] applyUserMapFreeDelSuperTypeCouponForSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(1)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	//============================== Apply Non-Super type when no user mapping  =======================================

	@DataProvider(name = "applyUserMapDiscountNonSuperTypeCouponForNonSuperUserWhenUserNotMappedData")
	public static Object[][] applyUserMapDiscountNonSuperTypeCouponForNonSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(20)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	@DataProvider(name = "applyUserMapFreeDelNonSuperTypeCouponForNonSuperUserWhenUserNotMappedData")
	public static Object[][] applyUserMapFreeDelNonSuperTypeCouponForNonSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
					.setDefaultData()
					.withIsPrivate(1)
					.withCustomerRestriction(1)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withCouponType("Discount")
					.withDiscountPercentage(0)
					.withDiscountAmount(0)
					.withFreeShipping(1)
					.withCouponUserTypeId(2)
					.withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
					.withDescription("Test")
					.withCode("SUPER" + Utility.getRandomPostfix());
			data.put("createCouponPOJO", createCouponPOJO);
			return new Object[][]{{data}};


		}
	}
	// =========== Create multiApply coupon for User type  super =================
	@DataProvider(name = "createMultiApplyPublicDiscountTypeCouponForUserTypeSuperData")
	public static Object[][] createMultiApplyPublicDiscountTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	@DataProvider(name = "createMultiAPplyUserMapDiscountTypeCouponForUserTypeSuperData")
	public static Object[][] createMultiAPplyUserMapDiscountTypeCouponForUserTypeSuperTest() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	@DataProvider(name = "createMultiApplyPublicFreeDelTypeCouponForUserTypeSuperData")
	public static Object[][] createMultiApplyPublicFreeDelTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	@DataProvider(name = "createMultiApplyUserMapFreeDelTypeCouponForUserTypeSuperData")
	public static Object[][] createMultiApplyUserMapFreeDelTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	// =========== Create multiApply coupon for User type  non super =================
	@DataProvider(name = "createMultiApplyPublicDiscountTypeCouponForUserTypeNonSuperData")
	public static Object[][] createMultiApplyPublicDiscountTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "createMultiAPplyUserMapDiscountTypeCouponForUserTypeNonSuperData")
	public static Object[][] createMultiAPplyUserMapDiscountTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	@DataProvider(name = "createMultiApplyPublicFreeDelTypeCouponForUserTypeNonSuperData")
	public static Object[][] createMultiApplyPublicFreeDelTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};


		}
	}

	@DataProvider(name = "createMultiApplyUserMapFreeDelTypeCouponForUserTypeNonSuperData")
	public static Object[][] createMultiApplyUserMapFreeDelTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};


		}
	}


	// =========== Create multiApply coupon for User type  ALL =================
	@DataProvider(name = "createMultiApplyPublicDiscountTypeCouponForUserTypeALLData")
	public static Object[][] createMultiApplyPublicDiscountTypeCouponForUserTypeALL() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "createMultiAPplyUserMapDiscountTypeCouponForUserTypeALLData")
	public static Object[][] createMultiAPplyUserMapDiscountTypeCouponForUserTypeALL() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "createMultiApplyPublicFreeDelTypeCouponForUserTypeALLData")
	public static Object[][] createMultiApplyPublicFreeDelTypeCouponForUserTypeALL() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "createMultiApplyUserMapFreeDelTypeCouponForUserTypeALLData")
	public static Object[][] createMultiApplyUserMapFreeDelTypeCouponForUserTypeALL() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// =========== Create multiApply coupon for Invalid User type  =================

	@DataProvider(name = "createMultiApplyPublicDiscountTypeCouponForInvalidUserTypeData")
	public static Object[][] createMultiApplyPublicDiscountTypeCouponForInvalidUserType() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(3)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "createMultiAPplyUserMapDiscountTypeCouponForInvalidUserTypeData")
	public static Object[][] createMultiAPplyUserMapDiscountTypeCouponForInvalidUserType() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(3)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "createMultiApplyPublicFreeDelTypeCouponForUserInvalidTypeData")
	public static Object[][] createMultiApplyPublicFreeDelTypeCouponForUserInvalidType() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(3)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "createMultiApplyUserMapFreeDelTypeCouponForInvalidUserTypeData")
	public static Object[][] createMultiApplyUserMapFreeDelTypeCouponForInvalidUserType() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(3)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ===========================================Get by user id-city id  for Super user  ========================

	@DataProvider(name = "getMultiApplyByUserIdCityIdPublicDiscountTypeCouponForUserTypeSuperData")
	public static Object[][] getMultiApplyByUserIdCityIdPublicDiscountTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdCityIdUserMapDiscountTypeCouponForUserTypeSuperData")
	public static Object[][] getMultiApplyByUserIdCityIdUserMapDiscountTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "GetMultiApplyByUserIdCityIdPublicFreeDelTypeCouponForUserTypeSuperData")
	public static Object[][] GetMultiApplyByUserIdCityIdPublicFreeDelTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdCityUserMapFreeDelTypeCouponForUserTypeSuperData")
	public static Object[][] getMultiApplyByUserIdCityUserMapFreeDelTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}
// ===========================================Get by user id-city id by Non Super user id  ========================

	@DataProvider(name = "getMultiApplyByUserIdCityIdPublicDiscountSuperTypeCouponForUserNonSuperData")
	public static Object[][] getMultiApplyByUserIdCityIdPublicDiscountSuperTypeCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdCityIdUserMapDiscountSuperTypeCouponForUserNonSuperData")
	public static Object[][] getMultiApplyByUserIdCityIdUserMapDiscountSuperTypeCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "GetMultiApplyByUserIdCityIdPublicFreeDelSuperTypeCouponForUserNonSuperData")
	public static Object[][] GetMultiApplyByUserIdCityIdPublicFreeDelSuperTypeCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdCityUserMapFreeDelSuperTypeCouponForUserTypeNonSuperData")
	public static Object[][] getMultiApplyByUserIdCityUserMapFreeDelSuperTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}
// ===========================================Get by user id-city id  non super type of coupon for Non Super user id  ========================

@DataProvider(name = "getMultiApplyByUserIdCityIdPublicDiscountNonSuperTypeCouponForUserNonSuperData")
public static Object[][] getMultiApplyByUserIdCityIdPublicDiscountNonSuperTypeCouponForUserNonSuper() {
	{
		HashMap<String, Object> data = new HashMap<String, Object>();
		CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
				.setDefaultData()
				.withIsPrivate(false)
				.withValidFrom(Utility.getCurrentDate())
				.withValidTill(Utility.getFutureDate())
				.withcouponUserTypeId(2)
				.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

		data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
		return new Object[][]{{data}};
	}
}

	@DataProvider(name = "getMultiApplyByUserIdCityIdUserMapDiscountNonSuperTypeCouponForUserNonSuperData")
	public static Object[][] getMultiApplyByUserIdCityIdUserMapDiscountNonSuperTypeCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "GetMultiApplyByUserIdCityIdPublicFreeDelNonSuperTypeCouponForUserNonSuperData")
	public static Object[][] GetMultiApplyByUserIdCityIdPublicFreeDelNonSuperTypeCouponForUserNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdCityUserMapFreeDelNonSuperTypeCouponForUserTypeNonSuperData")
	public static Object[][] getMultiApplyByUserIdCityUserMapFreeDelNonSuperTypeCouponForUserTypeNonSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ===========================================Get by user id-city id  non-super type coupon for Super user  ========================

	@DataProvider(name = "getMultiApplyByUserIdCityIdPublicDiscountNonSuperTypeCouponForUserTypeSuperData")
	public static Object[][] getMultiApplyByUserIdCityIdPublicDiscountNonSuperTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdCityIdUserMapDiscountNonSuperTypeCouponForUserTypeSuperData")
	public static Object[][] getMultiApplyByUserIdCityIdUserMapDiscountNonSuperTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "GetMultiApplyByUserIdCityIdPublicFreeDelNonSuperTypeCouponForUserTypeSuperData")
	public static Object[][] GetMultiApplyByUserIdCityIdPublicFreeDelNonSuperTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdCityUserMapFreeDelNonSuperTypeCouponForUserTypeSuperData")
	public static Object[][] getMultiApplyByUserIdCityUserMapFreeDelNonSuperTypeCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ====== Get by user id Super type coupon for Super user   ========================================

	@DataProvider(name = "getMultiApplyByUserIdPublicDiscountTypeSuperCouponForUserTypeSuperData")
	public static Object[][] getMultiApplyByUserIdPublicDiscountTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperData")
	public static Object[][] getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "GetMultiApplyByUserIdPublicFreeDelTypeSuperCouponForUserTypeSuperData")
	public static Object[][] GetMultiApplyByUserIdPublicFreeDelTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperData")
	public static Object[][] getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ====== Get by user id Super type coupon for non Super user   ========================================


	@DataProvider(name = "getMultiApplyByUserIdPublicDiscountTypeSuperCouponForNonSuperUserData")
	public static Object[][] getMultiApplyByUserIdPublicDiscountTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForNonSuperUserData")
	public static Object[][] getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "GetMultiApplyByUserIdPublicFreeDelTypeSuperCouponForNonSuperUserData")
	public static Object[][] GetMultiApplyByUserIdPublicFreeDelTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForNonSuperUserData")
	public static Object[][] getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ====== Get by user id Non Super type coupon for Super user   ========================================

	@DataProvider(name = "getMultiApplyByUserIdPublicDiscountTypeNonSuperCouponForUserSuperData")
	public static Object[][] getMultiApplyByUserIdPublicDiscountTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForUserSuperData")
	public static Object[][] getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "GetMultiApplyByUserIdPublicFreeDelTypeNonSuperCouponForUserSuperData")
	public static Object[][] GetMultiApplyByUserIdPublicFreeDelTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForUserSuperData")
	public static Object[][] getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}
	// ====== Get by user id Non-Super type coupon for Non-Super user   ========================================

	@DataProvider(name = "getMultiApplyByUserIdPublicDiscountTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] getMultiApplyByUserIdPublicDiscountTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperTypeData")
	public static Object[][] getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperType() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "GetMultiApplyByUserIdPublicFreeDelTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] GetMultiApplyByUserIdPublicFreeDelTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ====== Get by user id ALL type coupon for both Super and Non-Super user   ========================================
	@DataProvider(name = "getMultiApplyByUserIdPublicDiscountALLTypeCouponForBothUserData")
	public static Object[][] getMultiApplyByUserIdPublicDiscountALLTypeCouponForBothUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperTypeData")
	public static Object[][] getMultiApplyByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperType() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "GetMultiApplyByUserIdPublicFreeDelALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] GetMultiApplyByUserIdPublicFreeDelALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] getMultiApplyByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ====== Get by user id ALL type coupon for both Super and Non-Super user   ========================================

	@DataProvider(name = "getMultiApplyByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperTypeWhenNoUserMappedData")
	public static Object[][] getMultiApplyByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperTypeWhenNoUserMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenNoUserMappedData")
	public static Object[][] getMultiApplyByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenNoUserMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ====== Get by user id Non-Super type coupon for Non-Super user when no user mapping   ========================================

	@DataProvider(name = "getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperTypeWhenNoUserMappedData")
	public static Object[][] getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperTypeWhenNoUserMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForNonSuperUserWhenNoUserMappedData")
	public static Object[][] getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForNonSuperUserWhenNoUserMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ====== Get by user id Super type coupon for Super user when no user mapped========================================

	@DataProvider(name = "getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenNoUserMappedData")
	public static Object[][] getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenNoUserMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperWhenNoUserMappedData")
	public static Object[][] getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperWhenNoUserMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	//========================= APPLY =================================
	// ============== apply Super user coupon  as Super user ======================

	@DataProvider(name = "applyMultiApplyPublicDiscountTypeSuperCouponForUserTypeSuperData")
	public static Object[][] applyMultiApplyPublicDiscountTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyUserMapDiscountTypeSuperCouponForUserTypeSuperData")
	public static Object[][] applyMultiApplyUserMapDiscountTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyPublicFreeDelTypeSuperCouponForUserTypeSuperData")
	public static Object[][] applyMultiApplyPublicFreeDelTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyUserMapFreeDelTypeSuperCouponForUserTypeSuperData")
	public static Object[][] applyMultiApplyUserMapFreeDelTypeSuperCouponForUserTypeSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ============== apply non-Super user coupon  as Super user ======================

	@DataProvider(name = "applyMultiApplyPublicDiscountTypeNonSuperCouponForUserSuperData")
	public static Object[][] applyMultiApplyPublicDiscountTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "applyMultiApplyUserMapDiscountTypeNonSuperCouponForUserSuperData")
	public static Object[][] applyMultiApplyUserMapDiscountTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyPublicFreeDelTypeNonSuperCouponForUserSuperData")
	public static Object[][] applyMultiApplyPublicFreeDelTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "applyMultiApplyUserMapFreeDelTypeNonSuperCouponForUserSuperData")
	public static Object[][] applyMultiApplyUserMapFreeDelTypeNonSuperCouponForUserSuper() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	// ============== apply non-Super user coupon  as non-Super user ======================

	@DataProvider(name = "applyMultiAPplyPublicDiscountTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] applyMultiAPplyPublicDiscountTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "applyMultiApplyUserMapDiscountTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] applyMultiApplyUserMapDiscountTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyPublicFreeDelTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] applyMultiApplyPublicFreeDelTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyUserMapFreeDelTypeNonSuperCouponForNonSuperUserData")
	public static Object[][] applyMultiApplyUserMapFreeDelTypeNonSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ============== apply Super user coupon  as non-Super user ======================

	@DataProvider(name = "applyMultiApplyPublicDiscountTypeSuperCouponForNonSuperUserData")
	public static Object[][] applyMultiApplyPublicDiscountTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyUserMapDiscountTypeSuperCouponForNonSuperUserData")
	public static Object[][] applyMultiApplyUserMapDiscountTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyPublicFreeDelTypeSuperCouponForNonSuperUserData")
	public static Object[][] applyMultiApplyPublicFreeDelTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "applyMultiApplyUserMapFreeDelTypeSuperCouponForNonSuperUserData")
	public static Object[][] applyMultiApplyUserMapFreeDelTypeSuperCouponForNonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	// ============== apply ALL type coupon  as both- Super & Non-Super user ======================

	@DataProvider(name = "applyMultiApplyPublicDiscountTypeALLCouponForBothSuperNnonSuperUserData")
	public static Object[][] applyMultiApplyPublicDiscountTypeALLCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "applyMultiApplyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] applyMultiApplyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyPublicFreeDelALLTypeCouponForSuperNnonSuperUserData")
	public static Object[][] applyMultiApplyPublicFreeDelALLTypeCouponForSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(false)
					.withCustomerRestriction(false)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserData")
	public static Object[][] applyMultiApplyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUser() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "applyMultiApplyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedData")
	public static Object[][] applyMultiApplyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

	@DataProvider(name = "applyMultiApplyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedData")
	public static Object[][] applyMultiApplyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(0)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};
		}
	}

//============================== Apply Super type when no user mapping  =======================================

	@DataProvider(name = "applyMultiApplyUserMapDiscountSuperTypeCouponForSuperUserWhenUserNotMappedData")
	public static Object[][] applyMultiApplyUserMapDiscountSuperTypeCouponForSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyUserMapFreeDelSuperTypeCouponForSuperUserWhenUserNotMappedData")
	public static Object[][] applyMultiApplyUserMapFreeDelSuperTypeCouponForSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(1)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	//============================== Apply Non-Super type when no user mapping  =======================================

	@DataProvider(name = "applyMultiApplyUserMapDiscountNonSuperTypeCouponForNonSuperUserWhenUserNotMappedData")
	public static Object[][] applyMultiApplyUserMapDiscountNonSuperTypeCouponForNonSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  20,  false));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

	@DataProvider(name = "applyMultiApplyUserMapFreeDelNonSuperTypeCouponForNonSuperUserWhenUserNotMappedData")
	public static Object[][] applyMultiApplyUserMapFreeDelNonSuperTypeCouponForNonSuperUserWhenUserNotMapped() {
		{
			HashMap<String, Object> data = new HashMap<String, Object>();
			CreateMultiApplyCouponPOJO createMultiApplyCouponPOJO = new CreateMultiApplyCouponPOJO()
					.setDefaultData()
					.withIsPrivate(true)
					.withCustomerRestriction(true)
					.withValidFrom(Utility.getCurrentDate())
					.withValidTill(Utility.getFutureDate())
					.withcouponUserTypeId(2)
					.withMetaList(CreateMultiApplyCouponPOJO.getMetaList( 2,  0,  true));

			data.put("createMultiApplyCouponPOJO", createMultiApplyCouponPOJO);
			return new Object[][]{{data}};

		}
	}

}