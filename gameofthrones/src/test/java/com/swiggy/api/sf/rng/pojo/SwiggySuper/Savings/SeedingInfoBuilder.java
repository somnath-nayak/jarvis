package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;


public class SeedingInfoBuilder {
    public SeedingInfo seedingInfo;
    public SeedingInfoBuilder() {
        seedingInfo = new SeedingInfo();
    }


    public SeedingInfoBuilder withMerchantType(String merchantType) {
        seedingInfo.setMerchantType( merchantType);
        return this;
    }

    public SeedingInfoBuilder withCommissionLevy(Integer commissionLevy) {
       seedingInfo.setCommissionLevy( commissionLevy);
        return this;
    }

    public SeedingInfoBuilder withShareType(String shareType) {
        seedingInfo.setShareType(shareType);
        return this;
    }

    public SeedingInfoBuilder withShareValue(Integer shareValue) {
        seedingInfo.setShareValue(shareValue);
        return this;
    }

    public SeedingInfo build() {
        getDefaultValue();
        return seedingInfo;
    }

    private void getDefaultValue() {


        if (seedingInfo.getMerchantType() == null) {
            seedingInfo.setMerchantType("RESTAURANT");
        }
        if (seedingInfo.getCommissionLevy() == null) {
            seedingInfo.setCommissionLevy(0);
        }
        if (seedingInfo.getShareType() == null) {
            seedingInfo.setShareType("Percentage");
        }
        if (seedingInfo.getShareValue() == null) {
            seedingInfo.setShareValue(100);
        }


    }

}