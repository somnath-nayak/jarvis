package com.swiggy.api.sf.checkout.helper.edvo.pojo.editOrderRequest;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ser.std.ToStringSerializer;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class EditOrderRequest extends Cart {

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("swiggy_money")
    private double swiggyMoney;

    @JsonProperty("identifier")
    private String identifier;

    /**
     * Order edit initiation source.
     * 1 - Restaurant Initiated (default)
     * 2 - Customer Initiated
     */
    @JsonProperty("initiation_source")
    private Integer initiationSource = 1;

    @JsonProperty("order_spending")
    private String orderSpending;

    @JsonProperty("order_incoming")
    private String orderIncoming;

    @JsonProperty("swiggy_money_applicable")
    private String swiggyMoneyApplicable;

    @JsonProperty("reason_for_cloning")
    private String reasonForCloning;

    @JsonProperty("rest_bear_amount")
    private String restaurantBearAmount;

    @JsonProperty("order_comments")
    private String orderComments;

    @JsonProperty("validate_item_availability")
    private boolean validateItemAvailability;

    @JsonProperty("validate_restaurant_availability")
    private boolean validateRestaurantAvailability;

    @JsonProperty("validate_serviceability")
    private boolean validateServiceability;

    @JsonProperty("copy_subscription_from_old_cart")
    private boolean copySubscriptionFromOldCart;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getSwiggyMoney() {
        return swiggyMoney;
    }

    public void setSwiggyMoney(double swiggyMoney) {
        this.swiggyMoney = swiggyMoney;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Integer getInitiationSource() {
        return initiationSource;
    }

    public void setInitiationSource(Integer initiationSource) {
        this.initiationSource = initiationSource;
    }

    public String getOrderSpending() {
        return orderSpending;
    }

    public void setOrderSpending(String orderSpending) {
        this.orderSpending = orderSpending;
    }

    public String getOrderIncoming() {
        return orderIncoming;
    }

    public void setOrderIncoming(String orderIncoming) {
        this.orderIncoming = orderIncoming;
    }

    public String getSwiggyMoneyApplicable() {
        return swiggyMoneyApplicable;
    }

    public void setSwiggyMoneyApplicable(String swiggyMoneyApplicable) {
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
    }

    public String getReasonForCloning() {
        return reasonForCloning;
    }

    public void setReasonForCloning(String reasonForCloning) {
        this.reasonForCloning = reasonForCloning;
    }

    public String getRestaurantBearAmount() {
        return restaurantBearAmount;
    }

    public void setRestaurantBearAmount(String restaurantBearAmount) {
        this.restaurantBearAmount = restaurantBearAmount;
    }

    public String getOrderComments() {
        return orderComments;
    }

    public void setOrderComments(String orderComments) {
        this.orderComments = orderComments;
    }

    public boolean isValidateItemAvailability() {
        return validateItemAvailability;
    }

    public void setValidateItemAvailability(boolean validateItemAvailability) {
        this.validateItemAvailability = validateItemAvailability;
    }

    public boolean isValidateRestaurantAvailability() {
        return validateRestaurantAvailability;
    }

    public void setValidateRestaurantAvailability(boolean validateRestaurantAvailability) {
        this.validateRestaurantAvailability = validateRestaurantAvailability;
    }

    public boolean isValidateServiceability() {
        return validateServiceability;
    }

    public void setValidateServiceability(boolean validateServiceability) {
        this.validateServiceability = validateServiceability;
    }

    public boolean isCopySubscriptionFromOldCart() {
        return copySubscriptionFromOldCart;
    }

    public void setCopySubscriptionFromOldCart(boolean copySubscriptionFromOldCart) {
        this.copySubscriptionFromOldCart = copySubscriptionFromOldCart;
    }
}