package com.swiggy.api.sf.checkout.helper.edvo.pojo.getLastOrderResponse;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "last_order",
        "has_multiple"
})
public class Data {

    @JsonProperty("last_order")
    private com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data lastOrder;
    @JsonProperty("has_multiple")
    private Boolean hasMultiple;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    /**
     *
     * @param hasMultiple
     * @param lastOrder
     */
    public Data(com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data lastOrder, Boolean hasMultiple) {
        super();
        this.lastOrder = lastOrder;
        this.hasMultiple = hasMultiple;
    }

    @JsonProperty("last_order")
    public com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data getLastOrder() {
        return lastOrder;
    }

    @JsonProperty("last_order")
    public void setLastOrder(com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data lastOrder) {
        this.lastOrder = lastOrder;
    }

    @JsonProperty("has_multiple")
    public Boolean getHasMultiple() {
        return hasMultiple;
    }

    @JsonProperty("has_multiple")
    public void setHasMultiple(Boolean hasMultiple) {
        this.hasMultiple = hasMultiple;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("lastOrder", lastOrder).append("hasMultiple", hasMultiple).toString();
    }

}