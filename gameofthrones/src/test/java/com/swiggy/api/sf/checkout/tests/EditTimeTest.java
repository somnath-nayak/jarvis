package com.swiggy.api.sf.checkout.tests;


import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.EditTimeConstants;
import com.swiggy.api.sf.checkout.dp.EditTimeDP;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.EditTimeHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.CartValidator;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.OrderValidator;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.JonSnow.Validator;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EditTimeTest extends EditTimeDP {

    private Initialize gameofthrones = new Initialize();
    CheckoutHelper helper= new CheckoutHelper();
    private EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
    private EditTimeHelper editHelper = new EditTimeHelper();
    private String orderID=null;
    private String init_source=null;
    String order=null;
    String tid;
    String token;
    HashMap<String, String> headers;
    int counter = 0;

    @BeforeClass
    public void login(){
        headers = helper.getTokenDataCheckout(EditTimeConstants.mobile1, EditTimeConstants.password1);
        tid = headers.get("Tid");
        token = headers.get("Token");
        itemIds = editHelper.getItemsWithoutVariant(EditTimeConstants.REST_ID);
    }

    @Test(dataProvider="editTimeCartUpdate",groups = {"Smoke","Sangeetha"}, description = "Edit Service - Rest Init / Cust Init / Other Init")
    public void editTimeRestUpdate(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
        /* Edit order confirm*/
        Validator editConfirmOrderValidator=editHelper.editOrderConfirm(tid, token, type, itemId, quantity, restId, newItemId, newquantity);
        editHelper.orderResCheck(editConfirmOrderValidator.GetBodyAsText(),cartConfirmStatusCode, cartConfirmMessage);
        if(cartEditStatusCode.equals(EditTimeConstants.STATUS_CODE)) {
            init_source=JsonPath.read(editConfirmOrderValidator.GetBodyAsText(), "$.data.initiation_source").toString().replace("[", "").replace("]", "");
            System.out.println(init_source);
            editHelper.initiationCheck(editConfirmOrderValidator.GetBodyAsText(), type);
        }
    }

    @Test(dataProvider="editTimeCartUpdate",groups = {"Smoke","Sanity","Regression","Sangeetha"}, description = " 1.place an order 2.Do Edit order 3.Verify initiation source on get Single Order by id")

    public void editTimeGetSingleOrderId(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
        Validator  getSingleOrder=editHelper.getSingleOrder(tid, token, type, itemId, quantity, restId, newItemId, newquantity, cartEditStatusCode, cartEditmessage, cartConfirmStatusCode, cartConfirmMessage);
        editHelper.orderInitiationCheck(getSingleOrder.GetBodyAsText(), type);
    }

    @Test(dataProvider="editTimeCartUpdate",groups = {"Smoke","Sanity","Regression","Sangeetha"},description = " 1.place an order 2.Do Edit order 3.Verify initiation source on get Single Order by key")
    public void editTimeGetSingleOrderKey(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
        Validator  getSingleOrderKey=editHelper.getSingleOrderKey(tid, token, type, itemId, quantity, restId, newItemId, newquantity, cartEditStatusCode, cartEditmessage, cartConfirmStatusCode, cartConfirmMessage);
        editHelper.orderInitiationCheck(getSingleOrderKey.GetBodyAsText(), type);
    }


    @Test(dataProvider="editTimeCartUpdate",groups = {"Smoke","Sanity","Regression","Sangeetha"},description = " 1.place an order 2.Do Edit order 3.Verify initiation source on get last order")
    public void editTimeLastOrder(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
        Validator lastOrderValidator = editHelper.getLastOrderET(tid, token, type, itemId, quantity, restId, newItemId, newquantity, cartEditStatusCode, cartEditmessage, cartConfirmStatusCode, cartConfirmMessage);
        editHelper.lastOrderInitiationCheck(lastOrderValidator.GetBodyAsText(), type);
    }


    @Test(dataProvider="editTimeCartUpdate",groups = {"Smoke","Sanity","Regression","Sangeetha"},description = " 1.place an order 2.Do Edit order 3.Verify initiation source on get order")

    public void editTimeGetOrder(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
        String getOrderValidator = editHelper.getOrderET(tid, token, type, itemId, quantity, restId, newItemId, newquantity, cartEditStatusCode, cartEditmessage, cartConfirmStatusCode, cartConfirmMessage);
        editHelper.getOrderInitiationCheck(getOrderValidator, type);
    }


    @Test(dataProvider="editTimeCartUpdate",groups = {"Smoke","Sanity","Regression","Sangeetha"},priority = 1, description = "Edit Service - Rest Init / Cust Init / Other Init")
    public void editTimeTradeDiscountCheck(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage) throws IOException {

        //Apply TD @restaurant level for given rest id
        RngHelper rngHelper = new RngHelper();
        rngHelper.createPercentageTD(restId);

        /* Login */
        HashMap<String, String> hashMap=helper.getTokenDataCheckout(EditTimeConstants.mobile1, EditTimeConstants.password1);
        tid=hashMap.get("Tid");
        token=hashMap.get("Token");
        /* Create Cart */
        Validator cartValidator=editHelper.updateCartET(tid, token, itemId, quantity, restId).ResponseValidator;

        editHelper.statusCodeCheck(cartValidator.GetResponseCode());
        editHelper.cartResCheck(cartValidator.GetBodyAsText(), EditTimeConstants.CART_UPDATED_SUCCESSFULLY);
        /* Place order */
        String addressID=editHelper.getAddressIDfromCartResponse(tid, token,cartValidator);
        System.out.println(addressID);
        Validator orderValidator=editHelper.orderPlace(tid, token, addressID, CheckoutConstants.paymentMethod, CheckoutConstants.orderComments).ResponseValidator;
        editHelper.statusCodeCheck(orderValidator.GetResponseCode());
        editHelper.orderResCheck(orderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        orderID=JsonPath.read(orderValidator.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
        System.out.println(orderID);
        String tradeDiscountValue = editHelper.tradeDiscountCheck(orderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
        System.out.println(tradeDiscountValue);

        /* Edit order check*/
        Validator editCheckOrderValidator=editHelper.editOrderCheckET(Long.valueOf(orderID),type,newItemId,newquantity,restId).ResponseValidator;
        System.out.println(editCheckOrderValidator.GetBodyAsText());
        editHelper.statusCodeCheck(editCheckOrderValidator.GetResponseCode());
        editHelper.cartResCheck(editCheckOrderValidator.GetBodyAsText(),cartEditStatusCode,cartEditmessage);
        CartV2Response editOrderCheckResponse = Utility.jsonDecode(editCheckOrderValidator.GetBodyAsText(), CartV2Response.class);
        assert editOrderCheckResponse != null;
        String tradeDiscountEditValue = String.valueOf(editOrderCheckResponse.getData().getTradeDiscountTotal());
        System.out.println(tradeDiscountEditValue);

        System.out.println("Cart min amount check validated after edit " +tradeDiscountEditValue );
        Assert.assertTrue((tradeDiscountValue != "0") && (tradeDiscountValue != tradeDiscountEditValue), "Cart min amount check validateed after edit");

        /* Edit order confirm*/
        Validator editConfirmOrderValidator=editHelper.editOrderCheckConfirm(Long.valueOf(orderID),type,newItemId,newquantity, restId).ResponseValidator;
        System.out.println(editConfirmOrderValidator.GetBodyAsText());
        editHelper.statusCodeCheck(editConfirmOrderValidator.GetResponseCode());
        editHelper.orderResCheck(editConfirmOrderValidator.GetBodyAsText(),cartConfirmStatusCode, cartConfirmMessage);
        if(cartEditStatusCode.equals(EditTimeConstants.STATUS_CODE)) {
            init_source=JsonPath.read(editConfirmOrderValidator.GetBodyAsText(), "$.data.initiation_source").toString().replace("[", "").replace("]", "");
            System.out.println(init_source);
            editHelper.initiationCheck(editConfirmOrderValidator.GetBodyAsText(), type);

        }
    }

    @AfterMethod
    public void cancelAllOrders(){
        counter++;
        if(!headers.isEmpty() && counter >= 9){
            edvoCartHelper.cancelAllOrders(headers);
            counter = 0;
        }
    }
}
   
    

