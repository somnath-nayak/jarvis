package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

public class TimeSlot {

	@JsonProperty("openTime")
	private Integer openTime;
	@JsonProperty("closeTime")
	private Integer closeTime;
	@JsonProperty("day")
	private String day;

	@JsonProperty("openTime")
	public Integer getOpenTime() {
		return openTime;
	}

	@JsonProperty("openTime")
	public void setOpenTime(Integer openTime) {
		this.openTime = openTime;
	}

	public TimeSlot withOpenTime(Integer openTime) {
		this.openTime = openTime;
		return this;
	}

	@JsonProperty("closeTime")
	public Integer getCloseTime() {
		return closeTime;
	}

	@JsonProperty("closeTime")
	public void setCloseTime(Integer closeTime) {
		this.closeTime = closeTime;
	}

	public TimeSlot withCloseTime(Integer closeTime) {
		this.closeTime = closeTime;
		return this;
	}

	@JsonProperty("day")
	public String getDay() {
		return day;
	}

	@JsonProperty("day")
	public void setDay(String day) {
		this.day = day;
	}

	public TimeSlot withDay(String day) {
		this.day = day;
		return this;
	}
	public TimeSlot setDefaultData() {
		return this.withOpenTime(1000)
		.withCloseTime(2359)
		.withDay("ALL");
	}
}