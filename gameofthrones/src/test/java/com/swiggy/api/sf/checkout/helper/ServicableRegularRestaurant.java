package com.swiggy.api.sf.checkout.helper;

import java.io.IOException;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.SuperHelper;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.MinAmount;
import com.swiggy.poc.RuleEngine.main;


public class ServicableRegularRestaurant {
	
	RngHelper rngHelper=new RngHelper();
	SuperHelper rngSuperHelper = new SuperHelper();
		
	Cart cartPayload;
	String restId;
	String[] latLog;
	String planId="0";


 public Cart getRegularRestData(String cartType,boolean isCouponApplied,CheckoutHelper helper,
		                 EDVOCartHelper eDVOCartHelper,String userID) throws IOException {
	  
	latLog=new String[]{"12.935215600000001","77.6199608","0"};
	
	String listingRes=helper.getRestList(latLog).ResponseValidator.GetBodyAsText();
	String[] restList=JsonPath.read(listingRes, "$.data..id").toString().replace("[","")
			                                   .replace("]","").replace("\"","").split(",");
	String[] unServiceabilityList=JsonPath.read(listingRes, "$.data..unserviceable").toString()
			                              .replace("[","").replace("]","").replace("\"","").split(",");
	String[] isOpenedList=JsonPath.read(listingRes, "$.data..availability..opened").toString()
	                                      .replace("[","").replace("]","").replace("\"","").split(",");
			
	for(int i=0;i<restList.length;i++){
			if (isOpenedList[i].equalsIgnoreCase("true") && unServiceabilityList[i].equalsIgnoreCase("false")) {
			      	       restId=restList[i];
			      	       Cart cartPayload1=eDVOCartHelper.getCartPayload1(null,restId, false, false, true);
					       String statuscode=cartValidation(helper,cartPayload1);
					          if (statuscode.equals("0")){
					              break;
					              }
				            }
			          }
	
	String couponCode = null;
	if (isCouponApplied==true){
	     couponCode=getCoupon();
	}
	if(cartType.equalsIgnoreCase("regular")){
		planId="0";
		cartPayload=eDVOCartHelper.getCartPayloadSuper(null,restId, false, false, true,couponCode,Integer.parseInt(planId));
    	} else if(cartType.equalsIgnoreCase("superSec") || cartType.equalsIgnoreCase("asSuper")){
    	   getSuperPlan(userID);
    	   if(Integer.parseInt(planId)>0){
    	   cartPayload=eDVOCartHelper.getCartPayloadSuper(null,restId, false, false, true,couponCode,Integer.parseInt(planId));
    	   }
    	}
	
	return cartPayload;
	}

		
    public String cartValidation(CheckoutHelper helper,Cart cartPayload){
			String getStatusCode;
			try{
			String response=helper.createCartCheckTotals(Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
			getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
			}catch (Exception e){
				return "97";
			}
			return getStatusCode;
		}
    
    public Cart getCartPayload(String cartType,boolean isCouponApplied,EDVOCartHelper eDVOCartHelper,String restId,String userID) throws IOException{
    	
    	cartPayload=eDVOCartHelper.getCartPayload1(null,restId, false, false, true);
		return cartPayload;
	}
	
    
    public void createTD(String tdDiscount,String restID) throws IOException{
  	  rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("0",restID,tdDiscount);
    }
    
     public String getCoupon() throws IOException{
  	  CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO();
  	  createCouponPOJO.setDefaultData().withMinAmount(new MinAmount().withCart(0))
  	                                    .withValidFrom(CheckoutHelper.getCurrentDate())
  	                                    .withValidTill(CheckoutHelper.getFutureDate())
  	                                    .withApplicableWithSwiggyMoney(true);
  	  String couponCode=createCouponPOJO.getCode();
  	  rngHelper.createCoupon(createCouponPOJO);
  	  return couponCode;
    }
    
     public void createFreeDelTD(String restId,boolean is_surge) {
  	  rngHelper.createFreeDeliveryTD(restId, is_surge, false, false,false);
      }
     
     public void disabledActiveTD(String restId){
    	 rngHelper.disabledActiveTD(restId);
     }
     
     public String getSuperPlan(String userId){ 
    	 try{
    		  String planResp = rngSuperHelper.getPlansByUserId(userId, "true").ResponseValidator.GetBodyAsText();
    		  String superPlanId=JsonPath.read(planResp, "$.data.plan_list[0]..plan_id")
    			    	                      .toString().replace("[","").replace("]","");
    		         planId=superPlanId;
    		   }catch(Exception e){
    		         planId="0";
    		   }
    	   return planId;
    	 }
}
