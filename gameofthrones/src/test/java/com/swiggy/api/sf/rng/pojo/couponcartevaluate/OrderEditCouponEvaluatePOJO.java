package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class OrderEditCouponEvaluatePOJO {
	@JsonProperty("code")
	private String code;
	@JsonProperty("cart")
	private Cart cart;
	@JsonProperty("headers")
	private Headers headers;
	@JsonProperty("paymentMethods")
	private PaymentMethods paymentMethods;
	@JsonProperty("time")
	private Object time;
	@JsonProperty("code")
	public String getCode() {
		return code;
	}
	@JsonProperty("code")
	public void setCode(String code) {
		this.code = code;
	}
	public OrderEditCouponEvaluatePOJO withCode(String code) {
		this.code = code;
		return this;
	}
	@JsonProperty("cart")
	public Cart getCart() {
		return cart;
	}
	@JsonProperty("cart")
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	public OrderEditCouponEvaluatePOJO withCart(Cart cart) {
		this.cart = cart;
		return this;
	}
	@JsonProperty("headers")
	public Headers getHeaders() {
		return headers;
	}
	@JsonProperty("headers")
	public void setHeaders(Headers headers) {
		this.headers = headers;
	}
	public OrderEditCouponEvaluatePOJO withHeaders(Headers headers) {
		this.headers = headers;
		return this;
	}
	@JsonProperty("paymentMethods")
	public PaymentMethods getPaymentMethods() {
		return paymentMethods;
	}
	@JsonProperty("paymentMethods")
	public void setPaymentMethods(PaymentMethods paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	public OrderEditCouponEvaluatePOJO withPaymentMethods(PaymentMethods paymentMethods) {
		this.paymentMethods = paymentMethods;
		return this;
	}
	@JsonProperty("time")
	public Object getTime() {
		return time;
	}
	@JsonProperty("time")
	public void setTime(Object time) {
		this.time = time;
	}
	public OrderEditCouponEvaluatePOJO withTime(Object time) {
		this.time = time;
		return this;
	}
	public OrderEditCouponEvaluatePOJO setDefaultData(String couponCode, Integer menuItemID, String restID, Integer userID, Boolean firstOrder, Integer editInitSource, Double cartTotal) {
		return this
			.withCode(couponCode)
			.withHeaders(new Headers().setDefaultData())
			.withPaymentMethods(new PaymentMethods().setDefaultData())
			.withTime(null)
			.withCart(new Cart().setDefaultData(firstOrder, editInitSource, menuItemID, restID, userID, cartTotal));
	}
}
