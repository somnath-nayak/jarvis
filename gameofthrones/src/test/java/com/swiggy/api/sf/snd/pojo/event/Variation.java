package com.swiggy.api.sf.snd.pojo.event;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class Variation {

    private Integer id;
    private String name;
    private Float price;
    private Integer order;
    @JsonProperty("unique_id")
    private String uniqueId;
    @JsonProperty("default")
    private Integer _default;
    @JsonProperty("variant_group_id")
    private Integer variantGroupId;
    @JsonProperty("in_stock")
    private Integer inStock;
    @JsonProperty("is_veg")
    private Integer isVeg;
    @JsonProperty("external_variant_id")
    private String externalVariantId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Integer getDefault() {
        return _default;
    }

    public void setDefault(Integer _default) {
        this._default = _default;
    }

    public Integer getVariantGroupId() {
        return variantGroupId;
    }

    public void setVariantGroupId(Integer variantGroupId) {
        this.variantGroupId = variantGroupId;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public String getExternalVariantId() {
        return externalVariantId;
    }

    public void setExternalVariantId(String externalVariantId) {
        this.externalVariantId = externalVariantId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("price", price).append("order", order).append("uniqueId", uniqueId).append("_default", _default).append("variantGroupId", variantGroupId).append("inStock", inStock).append("isVeg", isVeg).append("externalVariantId", externalVariantId).toString();
    }

}