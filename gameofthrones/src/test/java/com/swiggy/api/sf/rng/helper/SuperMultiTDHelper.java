package com.swiggy.api.sf.rng.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.projectX.grouporder.helper.GroupOrderHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartBuilder;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartItem;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.rng.constants.MultiTDConstants;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.pojo.MultiTD.CreateSuperCampaignV2.CreateSuperCampaignV2;
import com.swiggy.api.sf.rng.pojo.MultiTD.CreateSuperCampaignV2.RemoveFreebie;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3.EvaluateCartV3;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3.ItemRequest;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3.MealItemRequest;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateListingV3.EvaluateListingV3;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu.EvaluateMenu;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.*;
import com.swiggy.api.sf.rng.pojo.freebie.CreateFreebieTDEntryNewPojo;
import com.swiggy.api.sf.rng.pojo.freebie.RestIdList;
import com.swiggy.api.sf.rng.pojo.freebie.RuleDiscount;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import edu.emory.mathcs.backport.java.util.Arrays;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.lang.time.DateUtils;

import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.helper
 **/
public class SuperMultiTDHelper {

    Initialize gameofthrones = new Initialize();
    RngHelper rngHelper = new RngHelper();
    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    EDVOHelper edvoHelper = new EDVOHelper();
    GroupOrderHelper groupOrderHelper = new GroupOrderHelper();
    String[] userIDs = new String[MultiTDConstants.mobile.length];
    String benefit_freedel, benefit_freebie, plan_freedel, plan_freebie, plan_freedel_freebie;
    String user_freedel, user_freebie, user_freedel_freebie, user_not_super, user_no_userCut, user_map_fd_fb;
    ArrayList<String> token = new ArrayList<>();
    ArrayList<String> tid = new ArrayList<>();
    String camp_freedel, camp_freebie;
    String restID_freebie;
    String   freebie_itemID;
    String restIdMulti1, restIdMulti2;
    int freebie_catID, freebie_subCatID;
    List<String> super_campaignID;
    List<String> restaurant_campaignID;
    List<String> edvo_campaignID;
    String mealid, groupid;
    HashMap<String, String> restmealidmap = new HashMap<>();
    SoftAssert softAssert = new SoftAssert();

    private HashMap<String, String> getDefaultHeader() {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        return requestHeader;
    }

    public Long getCurrentEpochTimeInMillis() {
        return Instant.now().toEpochMilli();

    }

    public String[] getRandomUserID() {
        String[] args = new String[]{"9" + generateRandom(), "9" + generateRandom(), "9" + generateRandom(),
                "9" + generateRandom(), "9" + generateRandom(), "9" + generateRandom()};
        return args;
    }

    public void deactiveAllSubscription() {
        SuperHelper superHelper = new SuperHelper();
        System.out.println("deactve sub");
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("subscription");
        for (String useid : userIDs) {
            String query = "update subscription set enabled=0 where user_id=" + useid + "";
            System.out.println(query + "===============================>");
            sqlTemplate.update(query);
            System.out.println("deactvated sub");
//            String cancelSubsByOrderPayload = superHelper.populateCancelSubscriptionIdPayload(useid);
//            superHelper.cancelSubscription(cancelSubsByOrderPayload);
        }
    }

    public void disableSuperSubscription() {
        userIDs = loginAllUsersAndGetUserID();
        SuperHelper superHelper = new SuperHelper();
        for (String useid : userIDs) {
            superHelper.cancelSubscriptionOfUser(useid);
        }
    }


    //delete from plan_benefit_mapping where id>0
    public void deactiveAllMaping() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("subscription");
        String query = "delete from plan_benefit_mapping where id>0";
        System.out.println(query + "===============================>");
        sqlTemplate.update(query);
    }

    public void deactiveAllFreeDelBenifits() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("subscription");
        if (verifyFreeDelBenefits() == "" && verifyFreeBieBenefits() == "") {
            String query = "delete from benefits where id>=0";
            System.out.println(query + "===============================>");
            sqlTemplate.update(query);
        }
    }

    public static String verifyFreeDelBenefits() {
        String benefitId = "";
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("subscription");
        List<Map<String, Object>> list = sqlTemplate
                .queryForList("select id from benefits where type='FREE_DELIVERY'");
        for (Map<String, Object> map : list) {
            System.out.println(map.get("id").toString());
            benefitId = map.get("id").toString();
        }
        return benefitId;

    }

    public static String verifyFreeBieBenefits() {
        String benefitId = "";
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("subscription");
        List<Map<String, Object>> list = sqlTemplate
                .queryForList("select id from benefits where type='Freebie'");
        for (Map<String, Object> map : list) {
            System.out.println(map.get("id").toString());
            benefitId = map.get("id").toString();
        }
        return benefitId;
    }


    public void deactiveAllCampagin() {
//        System.out.println("deleting camp");
//        SqlTemplate sqlTemplate1 = SystemConfigProvider.getTemplate("trade_discount");
//        String query ="delete from campaign where id>0";
//        sqlTemplate1.update(query);
//        SqlTemplate sqlTemplate2 = SystemConfigProvider.getTemplate("trade_discount");
//        String query2 ="delete from campaign_restaurant_map where id>=0";
//        sqlTemplate2.update(query2);
//        System.out.println("deleted camp");


    }

    public void disableCampaignByRestId(String restaurant_id) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("trade_discount");

        String query = String.format(RngConstants.allCampIdByRestId, restaurant_id);
        System.out.println(query);
        List<Map<String, Object>> lists = sqlTemplate.queryForList(query);

        List<String> data = new ArrayList<String>();

        int size = lists.size();
        for (int i = 0; i < size; i++) {
            data.add(lists.get(i).get("id").toString());
            //lists.get(0).get("referral_code").toString()) : "RC1 is not created in DB";
        }
        for (String id : data) {
            //String newQuery= String.format(RngConstants.deleteAllRestById,id);
            // sqlTemplate.update(newQuery);
        }

    }


    public Long getDaysAddedToCurrentEpochInMillis(Integer days) {
        return Instant.now().plus(days, DAYS).toEpochMilli();
    }

    public String loginAndReturnUserId(String mobile, String password) throws Exception {
        String[] payload = {mobile, password};
        GameOfThronesService gots = new GameOfThronesService("sand9005", "loginV22", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        String[] user = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..customer_id").replace("[", "").replace("]", "").replace("\"", "").trim().split(",");
        String tid_user = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..tid").replace("[", "").replace("]", "").replace("\"", "").trim();
        String token_user = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..token").replace("[", "").replace("]", "").replace("\"", "").trim();
        String userID = user[0];
        if (userID.equals("")) {
            throw new Exception("Could not login and get user ID of user: " + mobile);
        } else {
            tid.add(tid_user);
            token.add(token_user);
        }
        return userID;
    }

    public String[] loginAllUsersAndGetUserID() {
        String[] userIds = new String[MultiTDConstants.mobile.length];

        for (int i = 0; i < MultiTDConstants.mobile.length; i++) {
            try {
                userIds[i] = loginAndReturnUserId(MultiTDConstants.mobile[i], MultiTDConstants.password[i]);
            } catch (Exception e) {
                try {
                    SANDHelper helper = new SANDHelper();

                    helper.createUser(SANDConstants.name, MultiTDConstants.mobile[i], generateRandom() + generateRandom() + "@gmail.com", MultiTDConstants.password[i]);

                    userIds[i] = loginAndReturnUserId(MultiTDConstants.mobile[i], MultiTDConstants.password[i]);
                } catch (Exception e1) {

                    // Mocking code has to be here...
                    System.out.println("Could not login and get user ID of user: " + MultiTDConstants.mobile[i]);
                    e.printStackTrace();
                }
            }
            System.out.println("TID --> " + tid);
            System.out.println("TOKEN --> " + token);
            System.out.println("userID -- " + i + " --> " + userIds[i]);
        }
        return userIds;
    }

    public String createPlanAndReturnPlanId(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("super", "createplan", gameofthrones);
        String body = new Processor(gots, getDefaultHeader(), payload).ResponseValidator.GetBodyAsText();
        String planID = JsonPath.read(body, "$..data").toString().replace("[", "").replace("]", "").replace("\"", "");
        return planID;
    }

    public String createBenefitAndReturnBenefitId(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("super", "createbenefit", gameofthrones);
        String body = new Processor(gots, getDefaultHeader(), payload).ResponseValidator.GetBodyAsText();
        String benefitId = JsonPath.read(body, "$..data").toString().replace("[", "").replace("]", "").replace("\"", "");
        return benefitId;
    }

    public Processor createSuperCampaign(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("td", "createSuperCampaignV2", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }


    public Processor evaluateCartHelper(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("td", "cartV3EvaluatePOJO", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public Processor evaluateCartHelperPojo(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("td", "cartV3EvaluatePOJO", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public Processor evaluateListingHelper(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("td", "evaluateListingV3", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public Processor evaluateMenuHelper(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("td", "evaluateMenuV3", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public Processor evaluateMealHelper(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("td", "evaluateMealV3", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public Processor createPlanBenefitMapping(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("super", "createplanbenefitmapping", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public Processor createPlanUserIncentiveMapping(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("super", "createplanuserincentivemapping", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public Processor createSubscription(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("super", "createsubscription", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public Processor removeCamp(String json) {

        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("td", "removecampaign", gameofthrones);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;

    }

    public Processor createCartHelper(String tid, String token, String restId) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid);
        headers.put("token", token);

        //String[] menu = groupOrderHelper.getMenuItems(restId);

        CartItem item = new CartItem();
        item.setMenuItemId("121");
        item.setQuantity(2);
        List<CartItem> items = new ArrayList<CartItem>();
        items.add(item);
        Cart cart = new CartBuilder()
                .cartItems(items)
                .restaurantId(restId)
                .build();


        String payload = Utility.jsonEncode(cart);

        //System.out.println("Menu Size for the restaurant: " + menu.length);
        GameOfThronesService got = new GameOfThronesService("checkout", "createcartv2edvo", gameofthrones);
        String[] payloadNew = {payload};
        Processor processor = new Processor(got, headers, payloadNew);
        return processor;
    }

    public Processor deleteCartHelper(String tid, String token) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid);
        headers.put("token", token);
        GameOfThronesService got = new GameOfThronesService("checkout", "deletecart", gameofthrones);
        Processor processor = new Processor(got, headers);
        return processor;
    }

    public HashMap<String, String> createEDVOCampaignReturnHashMap(String restID) {
        HashMap<String, String> hm;
        HashMap<String, String> returnVals = new HashMap<>();
        mealid = String.valueOf(generateRandom());
        groupid = String.valueOf(generateRandom());
        hm = edvoHelper.createDormantTypeEDVOCampaign("0", restID, mealid, groupid);
        // hm = edvoHelper.createDormantTypeEDVOCampaign("0", restID, String.valueOf(generateRandom()), String.valueOf(generateRandom()));
        Processor processor = edvoHelper.createEDVOTradeDiscount(hm);
        int edvo_campaignID = processor.ResponseValidator.GetNodeValueAsInt("$.data");
        int edvo_campaign_status = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if (edvo_campaign_status == 0) {
            returnVals.put("TDID", "" + edvo_campaignID);
            returnVals.put("status", "success");
            restmealidmap.put(restID + "_" + "mealid", mealid);
            restmealidmap.put(restID + "_" + "groupid", groupid);


            System.out.println("EDVO Campaign created --> " + edvo_campaignID);
        } else {
            returnVals.put("status", "failed");
            System.out.println("-- EDVO Campaign creation Failed");
        }
        return returnVals;
    }

    public HashMap<String, String> createSuperCampaignOfGivenType(List<Integer> restIDs, String benefitType, boolean disableAllActiveTD) throws IOException {
        System.out.println(" -- IN CREATE-SUPER-CAMPAIGN-OF-GIVEN-TYPE");
        HashMap<String, String> returnHM = new HashMap<>();
        JsonHelper jsonHelper = new JsonHelper();
        if (disableAllActiveTD)
            disableCampaignByRestId("" + restIDs.get(0));

        String[] discountLevel = {"SuperCart", "SuperCartRestaurant"};
        CreateSuperCampaignV2 createSuperCampaignV2 = new CreateSuperCampaignV2();
        if (benefitType.equals("FREE_DELIVERY")) {
            createSuperCampaignV2 = new CreateSuperCampaignV2();
            List<Integer> restId = new ArrayList<>();
            restId.add(-1);
            createSuperCampaignV2.build(benefit_freedel, benefitType, discountLevel[0], restId);
            Processor processor = createSuperCampaign(jsonHelper.getObjectToJSON(createSuperCampaignV2));
            camp_freedel = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            int camp_freedel_status = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            if (!camp_freedel.equals("null") && !camp_freedel.equals("") && camp_freedel_status == 0) {
                returnHM.put("TDID", camp_freedel);
                returnHM.put("benefitID", benefit_freedel);
                returnHM.put("status", "success");
            } else
                returnHM.put("status", "failed");
        } else if (benefitType.equals("Freebie")) {
            String restId = restIDs.get(0) + "";
            List<RestIdList> al = new ArrayList();
            al.add(new RestIdList(restId));
            SuperMultiTDHelper mhelper = new SuperMultiTDHelper();

            CreateFreebieTDEntryNewPojo freebieTradeDiscount1 = new CreateFreebieTDEntryNewPojo();

            RuleDiscount discount = new RuleDiscount();
            discount.withDiscountLevel("Restaurant").withType("Freebie").withMinCartAmount("99").withItemId("1283271");


            freebieTradeDiscount1.withNamespace("RestaurantLevel").withHeader("RestaurantLevelDiscount").withDescription("created by automation").withEnabled(true)
                    .withDiscountLevel("Restaurant").withCreatedBy("Ram").withCommissionOnFullBill(false).withValidFrom(rngHelper.getMidNightTimeOfCurrentDay())
                    .withValidTill(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
                    .withRestaurantList(al)
                    .withRuleDiscount(discount)
                    .withRestaurantHit("100")
                    .withFirstOrderRestriction(false)
                    .withIsSurge(true)
                    .withSlots(new ArrayList<>())
                    .withSurgeApplicable(true)
                    .withTaxesOnDiscountedBill(false)
                    .withTimeSlotRestriction(false)
                    .withUserRestriction(false)
                    .withIsSuper(true)
                    .withRestaurantFirstOrder(false).withDormantUserType("ZERO_DAYS_DORMANT").withCampaignType("Freebie").withSwiggyHit("0");

            System.out.println(jsonHelper.getObjectToJSON(freebieTradeDiscount1));

            Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(freebieTradeDiscount1));


            camp_freebie = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            int camp_freebie_status = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            if (!camp_freebie.equals("null") && !camp_freebie.equals("") && camp_freebie_status == 0) {
                returnHM.put("TDID", camp_freebie);
                returnHM.put("benefitID", benefit_freebie);
                returnHM.put("status", "success");
            } else {
                returnHM.put("status", "failed");
            }
        }
        System.out.println("Super campaign created:--> " + returnHM.get("TDID") + " BenefitID: -->" + returnHM.get("benefitID"));
        return returnHM;
    }


    public String createItemForRandomRestaurant() throws InterruptedException {
//        System.out.println(" -- IN CREATE-ITEM-FOR-RANDOM-RESTAURANT");
//        Thread.sleep(2000);
//        restID_freebie = rngHelper.getRandomRestaurant(MultiTDConstants.lat,MultiTDConstants.lng);
//        //Thread.sleep(2000);
//        System.out.println("Random RestID ---> "+restID_freebie);
//        freebie_catID = baseServiceHelper.createCategoryIdAndReturnInt(Integer.parseInt(restID_freebie));
//        freebie_subCatID = baseServiceHelper.createSubCategoryAndReturnInt(Integer.parseInt(restID_freebie),freebie_catID);
//        String freebie_itemID = baseServiceHelper.createItemIdAndReturnString(Integer.parseInt(restID_freebie),freebie_catID,freebie_subCatID);
//        System.out.println("freebie_itemID ---> "+freebie_itemID);
//        return freebie_itemID;
        restID_freebie = "" + generateRandom();
        return "" + generateRandom();

    }

    public Long getMonthsAddedToCurrentEpochInMillis(Integer months) {
        return ZonedDateTime.now().plusMonths(months).toInstant().toEpochMilli();
    }


    public void createPlanBenifitWithMapingSuper() throws IOException, InterruptedException {
        System.out.println(" -- IN CREATE-SUPER-BENEFIT-PLANS-AND-USER-MAPPINGS");
        JsonHelper jsonHelper = new JsonHelper();
        SoftAssert softAssert = new SoftAssert();
        String[] benefitType = {"FREE_DELIVERY", "Freebie"};
        // userIDs = loginAllUsersAndGetUserID();
        userIDs = getRandomUserID();
        //Assigning user to readable variable to ease of understanding cases
        user_freebie = userIDs[0];
        user_freedel = userIDs[1];
        user_freedel_freebie = userIDs[2];
        user_not_super = userIDs[3];
        user_no_userCut = userIDs[4];
        user_map_fd_fb = userIDs[5];
//        restID2_freebie_freedel = rngHelper.getRandomRestaurant(MultiTDConstants.lat,MultiTDConstants.lng);
        CreateBenefits createBenefits = new CreateBenefits();
        createBenefits.build(benefitType[0]);
        if (verifyFreeDelBenefits() != "")
            benefit_freedel = verifyFreeDelBenefits();
        else
            benefit_freedel = createBenefitAndReturnBenefitId(jsonHelper.getObjectToJSON(createBenefits));

        createBenefits = new CreateBenefits();
        createBenefits.build(benefitType[1]);
        if (verifyFreeBieBenefits() != "")
            benefit_freebie = verifyFreeBieBenefits();
        else
            benefit_freebie = createBenefitAndReturnBenefitId(jsonHelper.getObjectToJSON(createBenefits));

        CreatePlanV2 createPlan = new CreatePlanV2();
        createPlan.build();
        plan_freedel = createPlanAndReturnPlanId(jsonHelper.getObjectToJSON(createPlan));
        plan_freebie = createPlanAndReturnPlanId(jsonHelper.getObjectToJSON(createPlan));
        plan_freedel_freebie = createPlanAndReturnPlanId(jsonHelper.getObjectToJSON(createPlan));
        freebie_itemID = createItemForRandomRestaurant();
        //debug
        System.out.println("plan_freebie --> " + plan_freebie);
        System.out.println("plan_freedel --> " + plan_freedel);
        System.out.println("plan_freedel_freebie --> " + plan_freedel_freebie);
        System.out.println("benefit_freebie --> " + benefit_freebie);
        System.out.println("benefit_freedel --> " + benefit_freedel);
        //Thread.sleep(10000);
        //check in case of empty values to prevent type conversion error
        if (!plan_freebie.equals("") && !plan_freedel.equals("") && !benefit_freebie.equals("") && !benefit_freedel.equals("") && !plan_freedel_freebie.equals("")) {
            //mapping freedel
            PlanBenefitMapping planBenefitMapping = new PlanBenefitMapping();
            planBenefitMapping.build(Integer.parseInt(plan_freebie), Integer.parseInt(benefit_freebie));
            String json = jsonHelper.getObjectToJSON(planBenefitMapping);
            int status1 = createPlanBenefitMapping("[" + json + "]").ResponseValidator.GetNodeValueAsInt("$.statusCode");
            //mapping freebie
            planBenefitMapping = new PlanBenefitMapping();
            planBenefitMapping.build(Integer.parseInt(plan_freedel), Integer.parseInt(benefit_freedel));
            json = jsonHelper.getObjectToJSON(planBenefitMapping);
            int status2 = createPlanBenefitMapping("[" + json + "]").ResponseValidator.GetNodeValueAsInt("$.statusCode");
            List<PlanBenefitMapping> pbMapping = new ArrayList<>();
            //mapping freebie,freedel
            planBenefitMapping = new PlanBenefitMapping();
            planBenefitMapping.build(Integer.parseInt(plan_freedel_freebie), Integer.parseInt(benefit_freedel));
            pbMapping.add(planBenefitMapping);

            planBenefitMapping = new PlanBenefitMapping();
            planBenefitMapping.build(Integer.parseInt(plan_freedel_freebie), Integer.parseInt(benefit_freebie));
            pbMapping.add(planBenefitMapping);
            int status3 = createPlanBenefitMapping(jsonHelper.getObjectToJSON(pbMapping)).ResponseValidator.GetNodeValueAsInt("$.statusCode");
            //create plan-user-incentive mapping
            PlanUserIncentiveMapping planUserIncentiveMapping = new PlanUserIncentiveMapping();
            planUserIncentiveMapping.build(Integer.parseInt(plan_freebie), Integer.parseInt(user_freebie));
            json = jsonHelper.getObjectToJSON(planUserIncentiveMapping);
            int status4 = createPlanUserIncentiveMapping("[" + json + "]").ResponseValidator.GetNodeValueAsInt("$.statusCode");
            planUserIncentiveMapping = new PlanUserIncentiveMapping();
            planUserIncentiveMapping.build(Integer.parseInt(plan_freedel), Integer.parseInt(user_freedel));
            json = jsonHelper.getObjectToJSON(planUserIncentiveMapping);
            int status5 = createPlanUserIncentiveMapping("[" + json + "]").ResponseValidator.GetNodeValueAsInt("$.statusCode");
            planUserIncentiveMapping = new PlanUserIncentiveMapping();
            planUserIncentiveMapping.build(Integer.parseInt(plan_freedel_freebie), Integer.parseInt(user_freedel_freebie));
            json = jsonHelper.getObjectToJSON(planUserIncentiveMapping);
            int status6 = createPlanUserIncentiveMapping("[" + json + "]").ResponseValidator.GetNodeValueAsInt("$.statusCode");

            planUserIncentiveMapping = new PlanUserIncentiveMapping();
            planUserIncentiveMapping.build(Integer.parseInt(plan_freedel_freebie), Integer.parseInt(user_map_fd_fb));
            json = jsonHelper.getObjectToJSON(planUserIncentiveMapping);
            int status7 = createPlanUserIncentiveMapping("[" + json + "]").ResponseValidator.GetNodeValueAsInt("$.statusCode");


            softAssert.assertEquals(status1, MultiTDConstants.statusOne, "freebie mapping failed with status other than 1");
            softAssert.assertEquals(status2, MultiTDConstants.statusOne, "freedel mapping failed with status other than 1");
            softAssert.assertEquals(status3, MultiTDConstants.statusOne, "freebie mapping failed with status other than 1");
            softAssert.assertEquals(status4, MultiTDConstants.statusOne, "freebie plan-user-incentive mapping failed with status other than 1");
            softAssert.assertEquals(status5, MultiTDConstants.statusOne, "freedel plan-user-incentive mapping failed with status other than 1");
            softAssert.assertEquals(status6, MultiTDConstants.statusOne, "freedel_freebie plan-user-incentive mapping failed with status other than 1");
            softAssert.assertEquals(status7, MultiTDConstants.statusOne, "freedel_freebie plan-user-incentive mapping failed with status other than 1");
            softAssert.assertAll();
        }
    }

    public void createSubscription() throws IOException {


        // The below line code has been disabled due to super incomplete redis entry, This will be done once super cancel subscription API call devs.
//        Calendar calender = Calendar.getInstance();
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        calender.add(Calendar.DATE, -15);
//        String date = dateFormat.format(calender.getTime());
//        SuperDbHelper db = new SuperDbHelper();
//        db.updateValidTillByUserId(date,user_freebie);
//        db.updateValidTillByUserId(date,user_freedel_freebie);
//        db.updateValidTillByUserId(date,user_freedel);
//
//        SANDHelper sandh = new SANDHelper();
//        sandh.superCacheSet(user_freebie,"superCacheSet","");


        JsonHelper jsonHelper = new JsonHelper();
        SoftAssert softAssert = new SoftAssert();
        //create user plan subscription
        CreateSubscriptionV2 subscription = new CreateSubscriptionV2();
        subscription.build(Integer.parseInt(plan_freebie), Integer.parseInt(user_freebie));
        int status7 = createSubscription(jsonHelper.getObjectToJSON(subscription)).ResponseValidator.GetNodeValueAsInt("$.statusCode");
        subscription = new CreateSubscriptionV2();
        subscription.build(Integer.parseInt(plan_freedel), Integer.parseInt(user_freedel));
        int status8 = createSubscription(jsonHelper.getObjectToJSON(subscription)).ResponseValidator.GetNodeValueAsInt("$.statusCode");
        subscription = new CreateSubscriptionV2();
        Createsubscription subv1 = new Createsubscription();


        subscription.build(Integer.parseInt(plan_freedel_freebie), Integer.parseInt(user_freedel_freebie));
        int status9 = createSubscription(jsonHelper.getObjectToJSON(subscription)).ResponseValidator.GetNodeValueAsInt("$.statusCode");
        //assertions
        softAssert.assertEquals(status7, MultiTDConstants.statusOne, "freebie user subscription failed with status other than 1");
        softAssert.assertEquals(status8, MultiTDConstants.statusOne, "freedel user subscription failed with status other than 1");
        softAssert.assertEquals(status9, MultiTDConstants.statusOne, "freedel_freebie user subscription failed with status other than 1");
        softAssert.assertAll();
    }

    public static List<String> disableCampByCampID(String camp_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("trade_discount");
        List<Map<String, Object>> list = sqlTemplate

                //select c.id from campaign c join campaign_restaurant_map cres on c.id = cres.campaign_id where c.enabled = 1 and cres.restaurant_id = -1
                .queryForList("select c.id from campaign c join campaign_restaurant_map cres on c.id = cres.campaign_id where c.enabled = 1 and cres.restaurant_id = " + camp_id + "");
        //select campaign_id from campaign_restaurant_map where restaurant_id = "+camp_id+"

        List<String> data = new ArrayList<String>();
        for (Map<String, Object> map : list) {
            System.out.println(map.get("id").toString());

            data.add(map.get("id").toString());
        }
        return data;
    }

    public String getFreshRestId() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        String restId = "" + generateRandom();
        List<String> disableCamp = disableCampByCampID(restID_freebie);
        List<String> disableFreedelCamp = disableCampByCampID("-1");

        RemoveFreebie rm = new RemoveFreebie();
        for (String campId : disableCamp) {
            rm.withEnabled(false).withId(Integer.parseInt(campId)).withUpdatedBy("Automation");
            removeCamp(jsonHelper.getObjectToJSON(rm));
        }
        for (String campId : disableFreedelCamp) {
            rm.withEnabled(false).withId(Integer.parseInt(campId)).withUpdatedBy("Automation");

            removeCamp(jsonHelper.getObjectToJSON(rm));
        }
        return restId;
    }


    public boolean caseHandler(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType) throws Exception {

        restID_freebie = getFreshRestId();
        System.out.println(" -- IN CASE-HANDLER");
        int casesLen = cases.size();
        boolean flag = false;
        String userCut = "public";
        List<Integer> restID = new ArrayList<>();
        restID.add(Integer.parseInt(restID_freebie));


        for (int i = 0; i < casesLen; i++) {
            if (cases.get(i).get(0).equalsIgnoreCase("CUS")) {
                userCut = "dorm30";
            } else if (cases.get(i).get(0).equalsIgnoreCase("RFO")) {
                userCut = "restaurantFirstOrder";
            } else if (cases.get(i).get(0).equalsIgnoreCase("SFO")) {
                userCut = "firstOrder";
            }
            if (i == 0) {
                super_campaignID = new ArrayList<>();
                restaurant_campaignID = new ArrayList<>();
                edvo_campaignID = new ArrayList<>();
                List<String> case1 = cases.get(i);
                flag = caseResolver(case1, actionAssert.get(i), restID, true);
                if (!flag)
                    return flag;
            } else {
                flag = caseResolver(cases.get(i), actionAssert.get(i), restID, false);
                if (!flag)
                    return flag;
            }
        }
        if (caseType.equalsIgnoreCase("VALID") && !(eval.get("super").get("EVAL_CART").get(0).equals("NA"))) {
            flag = evaluator(userCut, eval, restID, user_freedel_freebie, user_not_super);
            if (!flag)
                return flag;
        } else if (caseType.equalsIgnoreCase("INVALID") && !(eval.get("super").get("EVAL_CART").get(0).equals("NA"))) {
            //Non UserCut user/rest ID
            flag = evaluator(userCut, eval, restID, user_freedel_freebie, user_no_userCut);
            if (!flag)
                return flag;
        }
        System.out.println("CaseHandler did its job: Flag -->" + flag);
        return flag;
    }


    public boolean caseHandlerMultiRest(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, int totalRest) throws Exception {
        List<Integer> restID = new ArrayList<>();
        restID_freebie = getFreshRestId();
        restIdMulti1 = getFreshRestId();

        restID.add(Integer.parseInt(restID_freebie));
        restID.add(Integer.parseInt(restIdMulti1));
        if (totalRest > 2) {
            restIdMulti2 = getFreshRestId();
            restID.add(Integer.parseInt(restIdMulti2));
        }
        HashMap<String, String> users = new HashMap<String, String>();
        users.put("visited", user_not_super);
        users.put("notvisited", user_no_userCut);
        System.out.println(" -- IN CASE-HANDLER");
        int casesLen = cases.size();
        boolean flag = false;
        String userCut = "public";

        for (int i = 0; i < casesLen; i++) {
            if (cases.get(i).get(0).equalsIgnoreCase("CUS")) {
                userCut = "dorm30";
            } else if (cases.get(i).get(0).equalsIgnoreCase("RFO")) {
                userCut = "restaurantFirstOrder";
            } else if (cases.get(i).get(0).equalsIgnoreCase("SFO")) {
                userCut = "firstOrder";
            }
            if (i == 0) {
                super_campaignID = new ArrayList<>();
                restaurant_campaignID = new ArrayList<>();
                edvo_campaignID = new ArrayList<>();
                List<String> case1 = cases.get(i);

                flag = caseResolver(case1, actionAssert.get(i), restID, true);
                if (!flag)
                    return flag;
            } else {
                flag = caseResolver(cases.get(i), actionAssert.get(i), restID, false);
                if (!flag)
                    return flag;
            }
        }


        flag = evaluatorMultiRest(userCut, eval, restID, users, totalRest);

        if (!flag)
            return flag;

        System.out.println("CaseHandler did its job: Flag -->" + flag);
        return flag;
    }

    public boolean caseHandlerForNudge(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType) throws Exception {

        restID_freebie = getFreshRestId();
        System.out.println(" -- IN CASE-HANDLER");
        int casesLen = cases.size();
        boolean flag = false;
        String userCut = "public";
        List<Integer> restID = new ArrayList<>();
        restID.add(Integer.parseInt(restID_freebie));

        for (int i = 0; i < casesLen; i++) {
            if (cases.get(i).get(0).equalsIgnoreCase("CUS")) {
                userCut = "dorm30";
            } else if (cases.get(i).get(0).equalsIgnoreCase("RFO")) {
                userCut = "restaurantFirstOrder";
            } else if (cases.get(i).get(0).equalsIgnoreCase("SFO")) {
                userCut = "firstOrder";
            }
            if (i == 0) {
                super_campaignID = new ArrayList<>();
                restaurant_campaignID = new ArrayList<>();
                edvo_campaignID = new ArrayList<>();
                List<String> case1 = cases.get(i);
                flag = caseResolver(case1, actionAssert.get(i), restID, true);
                if (!flag)
                    return flag;
            } else {
                flag = caseResolver(cases.get(i), actionAssert.get(i), restID, false);
                if (!flag)
                    return flag;
            }
        }
        String userTypes;
        if (eval.containsKey("map"))
            userTypes = "map";
        else if (eval.containsKey("super"))
            userTypes = "super";
        else
            userTypes = "not_super";

        flag = evaluatorForNudge(userCut, eval, restID, user_freedel_freebie, user_not_super, user_map_fd_fb, userTypes);

        System.out.println("CaseHandler did its job: Flag -->" + flag);
        return flag;
    }

    public boolean caseHandlerForCreate(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, String restId) throws Exception {

        JsonHelper jsonHelper = new JsonHelper();
        List<String> disableCamp = disableCampByCampID(restId);
        List<String> disableFreedelCamp = disableCampByCampID("-1");
        RemoveFreebie rm = new RemoveFreebie();
        for (String campId : disableCamp) {
            rm.withEnabled(false).withId(Integer.parseInt(campId)).withUpdatedBy("Automation");
            removeCamp(jsonHelper.getObjectToJSON(rm));
        }
        for (String campId : disableFreedelCamp) {
            rm.withEnabled(false).withId(Integer.parseInt(campId)).withUpdatedBy("Automation");

            removeCamp(jsonHelper.getObjectToJSON(rm));
        }
        int casesLen = cases.size();
        boolean flag = false;
        List<Integer> restID = new ArrayList<>();
        restID.add(Integer.parseInt(restID_freebie));

        for (int i = 0; i < casesLen; i++) {
            if (i == 0) {
                super_campaignID = new ArrayList<>();
                restaurant_campaignID = new ArrayList<>();
                edvo_campaignID = new ArrayList<>();
                List<String> case1 = cases.get(i);
                flag = caseResolver(case1, actionAssert.get(i), restID, true);
                if (!flag)
                    return flag;
            } else {
                flag = caseResolver(cases.get(i), actionAssert.get(i), restID, false);
                if (!flag)
                    return flag;
            }
        }
        return flag;
    }

    public boolean caseResolver(List<String> cases, Boolean actionAssert, List<Integer> restID, Boolean disableAllActiveTD) throws Exception {
        System.out.println("restID" + restID.size());

        boolean flag = false;
        System.out.println(" -- IN CASE-RESOLVER");

        HashMap<String, String> receiver;
        if (cases.get(0).equalsIgnoreCase("P") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FD")) {
            receiver = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restID, false, false, false, "ZERO_DAYS_DORMANT", true, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("P") && cases.get(1).equalsIgnoreCase("S") && cases.get(2).equalsIgnoreCase("S_FD")) {
            receiver = createSuperCampaignOfGivenType(restID, "FREE_DELIVERY", disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                super_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("P") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FB")) {
            receiver = rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel("99.0", restID, freebie_itemID, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("P") && cases.get(1).equalsIgnoreCase("S") && cases.get(2).equalsIgnoreCase("S_FB")) {
            receiver = createSuperCampaignOfGivenType(restID, "Freebie", disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                super_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("P") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_PERCENTAGE")) {
            receiver = rngHelper.createPercentageWithMinAmountAtRestaurantLevel(restID, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("P") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FLAT")) {
            receiver = rngHelper.createFlatWithMinCartAmountAtRestaurantLevel(restID, "100", "50", disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("CUS") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_PERCENTAGE")) {
            receiver = rngHelper.createPercentageWithThirtyDaysDormantAtRestaurantLevel(restID, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("CUS") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FLAT")) {
            receiver = rngHelper.createFlatWithThirtyDayDormantAtRestaurantLevel(restID, "100", "50", disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("CUS") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FB")) {
            receiver = rngHelper.createFeebieTDWithThirtyDayDormantAtRestaurantLevel("100", restID, freebie_itemID, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("CUS") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FD")) {
            receiver = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevelThrityDayDormant("100.0", restID, false, false, false, "THIRTY_DAYS_DORMANT", true, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("SFO") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_PERCENTAGE")) {
            receiver = rngHelper.createPercentageWithSwiggyFirstOrderAtRestaurantLevel(restID, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("SFO") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FLAT")) {
            receiver = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel(restID, "100", "50", disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("SFO") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FB")) {
            receiver = rngHelper.createFeebieTDWithFirstOrderRestrictionAtRestaurantLevel(restID, String.valueOf(restID.get(0)), freebie_itemID, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("SFO") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FD")) {
            receiver = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("100", restID, true, false, false, "ZERO_DAYS_DORMANT", true, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("RFO") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_PERCENTAGE")) {
            receiver = rngHelper.createPercentageWithSwiggyRestaurantOrderAtRestaurantLevel(restID, disableAllActiveTD);
            //TODO: confirm if valid method ^^
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("RFO") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FLAT")) {
            receiver = rngHelper.createFlatWithRestaurantFirstOrderRestrictionAtRestaurantLevel("100", "50", restID);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("RFO") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FB")) {
            receiver = rngHelper.createFeebieTDWithRestaurantFirstOrderAtRestaurantLevel("100", restID, freebie_itemID, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("RFO") && cases.get(1).equalsIgnoreCase("R") && cases.get(2).equalsIgnoreCase("R_FD")) {
            receiver = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("100", restID, false, true, false, "ZERO_DAYS_DORMANT", true, disableAllActiveTD);
            flag = validateAssert(receiver, actionAssert);
            if (receiver.containsKey("TDID"))
                restaurant_campaignID.add(receiver.get("TDID"));
            return flag;

        } else if ((cases.get(0).equalsIgnoreCase("P") && cases.get(1).equalsIgnoreCase("M") && cases.get(2).equalsIgnoreCase("BXGY"))) {
            if (!edvo_campaignID.isEmpty()) {
                for (int i = 0; i < edvo_campaignID.size(); i++)
                    edvoHelper.deleteEDVOCampaignFromDB(edvo_campaignID.get(i));
            }
            for (int i = 0; i < restID.size(); i++) {
                receiver = createEDVOCampaignReturnHashMap(String.valueOf(restID.get(i)));
                flag = validateAssert(receiver, actionAssert);
                if (receiver.containsKey("TDID"))
                    edvo_campaignID.add(receiver.get("TDID"));
            }
            System.out.println("FLAG----" + flag);
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("RFO") && cases.get(1).equalsIgnoreCase("M") && cases.get(2).equalsIgnoreCase("BXGY")) {
            if (!edvo_campaignID.isEmpty()) {
                for (int i = 0; i < edvo_campaignID.size(); i++)
                    edvoHelper.deleteEDVOCampaignFromDB(edvo_campaignID.get(i));
            }
            for (int i = 0; i < restID.size(); i++) {
//                test
                receiver = createRestaurantFirstOrderEdvo(String.valueOf(restID.get(i)));
                flag = validateAssert(receiver, actionAssert);
                if (receiver.containsKey("TDID"))
                    edvo_campaignID.add(receiver.get("TDID"));
            }
            System.out.println("FLAG----" + flag);
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("SFO") && cases.get(1).equalsIgnoreCase("M") && cases.get(2).equalsIgnoreCase("BXGY")) {
            if (!edvo_campaignID.isEmpty()) {
                for (int i = 0; i < edvo_campaignID.size(); i++)
                    edvoHelper.deleteEDVOCampaignFromDB(edvo_campaignID.get(i));
            }
            for (int i = 0; i < restID.size(); i++) {
//                test
                receiver = createSwiggyFirstOrderEdvo(String.valueOf(restID.get(i)));
                flag = validateAssert(receiver, actionAssert);
                if (receiver.containsKey("TDID"))
                    edvo_campaignID.add(receiver.get("TDID"));
            }
            System.out.println("FLAG----" + flag);
            return flag;
        } else if (cases.get(0).equalsIgnoreCase("CUS") && cases.get(1).equalsIgnoreCase("M") && cases.get(2).equalsIgnoreCase("BXGY")) {
            if (!edvo_campaignID.isEmpty()) {
                for (int i = 0; i < edvo_campaignID.size(); i++)
                    edvoHelper.deleteEDVOCampaignFromDB(edvo_campaignID.get(i));
            }
            for (int i = 0; i < restID.size(); i++) {
//                test
                receiver = createDormantUserEdvo(String.valueOf(restID.get(i)));
                flag = validateAssert(receiver, actionAssert);
                if (receiver.containsKey("TDID"))
                    edvo_campaignID.add(receiver.get("TDID"));
            }
            System.out.println("FLAG----" + flag);
            return flag;
        }


        return false;
    }


    private boolean validateAssert(HashMap<String, String> hm, Boolean actionAssert) {
        System.out.println(" -- IN VALIDATE-ASSERT");
        boolean val = false;
        if (hm.get("status").equalsIgnoreCase("success") && !hm.get("TDID").equals("") && actionAssert) {
            val = true;
        } else if (hm.get("status").equalsIgnoreCase("failed") && !actionAssert) {
            val = true;
        }
        System.out.println("DP actionAssert --> " + actionAssert);
        System.out.println("API creation status --> " + hm.get("status"));
        System.out.println("Overall ValidateAssert VAL --> " + val);
        return val;
    }


    private boolean evaluator(String userCut, HashMap<String, HashMap<String, List<String>>> eval, List<Integer> restID, String super_userID, String not_super_userID) throws IOException {
        System.out.println(" -- IN EVALUATOR");
        HashMap<String, List<String>> hm;
        HashMap<String, List<String>> hm_s;
        hm = eval.get("not_super");
        hm_s = eval.get("super");
        System.out.println("SUPER_CAMPAIGNS --> " + super_campaignID);
        System.out.println("RESTAURANT_CAMPAIGNS --> " + restaurant_campaignID);
        if (userCut.equalsIgnoreCase("dorm30")) {
            setDormantUser(not_super_userID, restID_freebie, "30");
            setDormantUser(super_userID, restID_freebie, "30");
        } else if (userCut.equalsIgnoreCase("restaurantFirstOrder")) {
            deleteUserInRedis(not_super_userID);
            deleteUserInRedis(super_userID);
        } else {
//                setDormantUser(not_super_userID,restID_freebie,"1");
//                setDormantUser(super_userID,restID_freebie,"1");
            System.out.println("nothing has selected");
        }

        boolean flag = false, flag_s = false;
        if (!hm.get("EVAL_CART").get(0).equalsIgnoreCase("NA"))
            flag = evaluateNotSuper(userCut, hm, restID, not_super_userID);
        else
            flag = true;
        if (!hm_s.get("EVAL_CART").get(0).equalsIgnoreCase("NA"))
            flag_s = evaluateSuper(userCut, hm_s, restID, super_userID);
        else
            flag_s = true;
        return flag && flag_s;
    }


    private boolean evaluatorMultiRest(String userCut, HashMap<String, HashMap<String, List<String>>> eval, List<Integer> restID, HashMap<String, String> user, int totalRest) throws IOException {
        System.out.println(" -- IN EVALUATOR");
        HashMap<String, List<String>> hm;
        HashMap<String, List<String>> hm_s;
        hm = eval.get("not_super");
        System.out.println("SUPER_CAMPAIGNS --> " + super_campaignID);
        System.out.println("RESTAURANT_CAMPAIGNS --> " + restaurant_campaignID);

        setDormantUser(user.get("notvisited"), restID_freebie, "30");
        setDormantUser(user.get("notvisited"), restIdMulti1, "30");
        setDormantUser(user.get("visited"), restID_freebie, "1");
        setDormantUser(user.get("visited"), restIdMulti1, "1");

        if (userCut.equalsIgnoreCase("restaurantFirstOrder")) {
            deleteUserInRedis(user.get("notvisited"));
        }
        boolean flag = false;
        if ((hm.containsKey("EVAL_CART_P")) && (hm.containsKey("EVAL_LISTING_P")) && (hm.containsKey("EVAL_MENU_P"))) {
            setDormantUser(user.get("visited"), restID_freebie, "1");
            setDormantUser(user.get("visited"), restIdMulti1, "1");
            setDormantUser(user.get("notvisited"), restID_freebie, "30");
            setDormantUser(user.get("notvisited"), restIdMulti1, "30");
            if (totalRest != 1)
                flag = evaluateNotSuperMultiRest(userCut, hm.get("EVAL_CART_P"), hm.get("EVAL_LISTING_P"), hm.get("EVAL_MENU_P"), restID, user.get("visited"), false);
            else {
                flag = evaluateNotSuperMultiRestIndividual(userCut, hm.get("EVAL_CART_P"), hm.get("EVAL_LISTING_P"), hm.get("EVAL_MENU_P"), restID_freebie, restIdMulti1, user.get("visited"), user.get("notvisited"));
            }
        }
        if (((hm.containsKey("EVAL_CART_RFO")) && (hm.containsKey("EVAL_LISTING_RFO")) && (hm.containsKey("EVAL_MENU_RFO")))) {
            deleteUserInRedis(user.get("notvisited"));
            //deleteUserInRedis(user.get("notvisited"),restID_freebie);
            if (totalRest != 1)
                flag = evaluateNotSuperMultiRest(userCut, hm.get("EVAL_CART_RFO"), hm.get("EVAL_LISTING_RFO"), hm.get("EVAL_MENU_RFO"), restID, user.get("notvisited"), false);
            else {
                flag = evaluateNotSuperMultiRestIndividual(userCut, hm.get("EVAL_CART_RFO"), hm.get("EVAL_LISTING_RFO"), hm.get("EVAL_MENU_RFO"), restID_freebie, restIdMulti1, user.get("visited"), user.get("notvisited"));
            }
        }

        if (((hm.containsKey("EVAL_CART_SFO")) && (hm.containsKey("EVAL_LISTING_SFO")) && (hm.containsKey("EVAL_MENU_SFO")))) {
            deleteUserInRedis(user.get("notvisited"));
            if (totalRest != 1)
                flag = evaluateNotSuperMultiRest(userCut, hm.get("EVAL_CART_SFO"), hm.get("EVAL_LISTING_SFO"), hm.get("EVAL_MENU_SFO"), restID, user.get("notvisited"), true);
            else {
                flag = evaluateNotSuperMultiRestIndividual(userCut, hm.get("EVAL_CART_SFO"), hm.get("EVAL_LISTING_SFO"), hm.get("EVAL_MENU_SFO"), restID_freebie, restIdMulti1, user.get("visited"), user.get("notvisited"));
            }
        }

        if (((hm.containsKey("EVAL_CART_CUS"))
                && (hm.containsKey("EVAL_LISTING_CUS"))
                && (hm.containsKey("EVAL_MENU_CUS")))) {
            setDormantUser(user.get("notvisited"), restID_freebie, "30");
            setDormantUser(user.get("notvisited"), restIdMulti1, "30");
            setDormantUser(user.get("visited"), restID_freebie, "1");
            setDormantUser(user.get("visited"), restIdMulti1, "1");
            if (totalRest != 1)
                flag = evaluateNotSuperMultiRest(userCut, hm.get("EVAL_CART_CUS"), hm.get("EVAL_LISTING_CUS"), hm.get("EVAL_MENU_CUS"), restID, user.get("notvisited"), false);
            else
                flag = evaluateNotSuperMultiRestIndividual(userCut, hm.get("EVAL_CART_CUS"), hm.get("EVAL_LISTING_CUS"), hm.get("EVAL_MENU_CUS"), restID_freebie, restIdMulti1, user.get("visited"), user.get("notvisited"));
        }
        if (flag)
            return true;
        else
            return false;
    }

    private boolean evaluatorForNudge(String userCut, HashMap<String, HashMap<String, List<String>>> eval, List<Integer> restID, String super_userID, String not_super_userID, String map_user_id, String usertype) throws IOException {
        System.out.println(" -- IN EVALUATOR");
        boolean flag = false;
        HashMap<String, List<String>> data;
        if (usertype.equalsIgnoreCase("super"))
            data = eval.get("super");
        else if (usertype.equalsIgnoreCase("map"))
            data = eval.get("map");
        else
            data = eval.get("not_super");

        System.out.println("SUPER_CAMPAIGNS --> " + super_campaignID);
        System.out.println("RESTAURANT_CAMPAIGNS --> " + restaurant_campaignID);
        if (userCut.equalsIgnoreCase("dorm30")) {
            setDormantUser(not_super_userID, restID_freebie, "30");
            setDormantUser(super_userID, restID_freebie, "30");
            setDormantUser(map_user_id, restID_freebie, "30");
        } else if (userCut.equalsIgnoreCase("restaurantFirstOrder")) {
            deleteUserInRedis(not_super_userID);
            deleteUserInRedis(super_userID);
            deleteUserInRedis(map_user_id);
        } else {
            System.out.println("nothing has selected");
        }
        boolean cartFlag = false, listingFlag = false, menuFlag = false;
        if (usertype.equalsIgnoreCase("super")) {
            cartFlag = cartEvaluationForNudge(userCut, data.get("EVAL_CART"), restID, super_userID, usertype);
            listingFlag = listingEvaluationForNudge(userCut, data.get("EVAL_LISTING"), super_userID, restID, usertype);
            //listingFlag = true;
            menuFlag = menuEvaluationForNudge(userCut, data.get("EVAL_MENU"), super_userID, restID.get(0), usertype);
        } else if (usertype.equalsIgnoreCase("not_super")) {
            cartFlag = cartEvaluationForNudge(userCut, data.get("EVAL_CART"), restID, not_super_userID, usertype);
            listingFlag = listingEvaluationForNudge(userCut, data.get("EVAL_LISTING"), not_super_userID, restID, usertype);
            //listingFlag = true;
            menuFlag = menuEvaluationForNudge(userCut, data.get("EVAL_MENU"), map_user_id, restID.get(0), usertype);
        } else {

            cartFlag = cartEvaluationForNudge(userCut, data.get("EVAL_CART"), restID, map_user_id, usertype);
            listingFlag = listingEvaluationForNudge(userCut, data.get("EVAL_LISTING"), map_user_id, restID, usertype);
            //listingFlag = true;
            menuFlag = menuEvaluationForNudge(userCut, data.get("EVAL_MENU"), map_user_id, restID.get(0), usertype);

        }

        if (cartFlag && listingFlag && menuFlag)
            flag = true;

        return flag;
    }

    public boolean evaluateSuper(String userCut, HashMap<String, List<String>> hm, List<Integer> restIDs, String userID) throws IOException {
        System.out.println(" -- IN EVALUATE-SUPER");
        boolean flag = false, cartFlag = false, listingFlag = false, menuFlag = false;
        //since using first user from userIDs

        for (Integer restID : restIDs) {
            cartFlag = cartEvaluation(userCut, hm.get("EVAL_CART"), restID, userID, true);
            softAssert.assertTrue(cartFlag, "Super Cart Failing");
            System.out.println("Cart eval:" + cartFlag);
            listingFlag = listingEvaluation(userCut, hm.get("EVAL_LISTING"), userID, restIDs);
            System.out.println("List eval:" + cartFlag);
            softAssert.assertTrue(cartFlag, "Super Listing Failing");
            menuFlag = menuEvaluation(userCut, hm.get("EVAL_MENU"), userID, restID);
            softAssert.assertTrue(cartFlag, "Super Menu Failing");
            softAssert.assertAll();


        }
        System.out.println("Menu eval:" + cartFlag);
        if (cartFlag && listingFlag && menuFlag) {
            flag = true;
            System.out.println("cartFlag && listingFlag --> " + flag);
        } else
            System.out.println("cartFlag val -- " + cartFlag + " -- listingFlag  either are not true");
        return flag;
    }

    public boolean evaluateNotSuper(String userCut, HashMap<String, List<String>> hm, List<Integer> restIDs, String userID) throws IOException {
        System.out.println(" -- IN EVALUATE-NOT-SUPER");
        boolean flag = false, cartFlag = false, listingFlag = false, menuFlag = false;

        for (int restID : restIDs) {
            cartFlag = cartEvaluation(userCut, hm.get("EVAL_CART"), restID, userID, false);
            softAssert.assertTrue(cartFlag, "Cart has not fount" + hm.get("EVAL_CART") + " on restid: " + restID);
            menuFlag = menuEvaluation(userCut, hm.get("EVAL_MENU"), userID, restID);
            softAssert.assertTrue(menuFlag, "Menu has not fount" + hm.get("EVAL_MENU") + " on restid: " + restID);
        }
        listingFlag = listingEvaluation(userCut, hm.get("EVAL_LISTING"), userID, restIDs);
        softAssert.assertTrue(listingFlag, "Listing has not fount" + hm.get("EVAL_LISTING"));
        softAssert.assertAll();
        if (!(cartFlag && listingFlag && menuFlag)) {
            return false;
        }
        return true;
    }

    public boolean evaluateNotSuperMultiRest(String userCut, List<String> cart, List<String> list, List<String> menu, List<Integer> restIDs, String userID, Boolean firstOrder) throws IOException {
        System.out.println(" -- IN EVALUATE-NOT-SUPER");
        boolean flag = false, cartFlag = false, listingFlag = false, menuFlag = false;

        for (int restID : restIDs) {

            cartFlag = cartEvaluationForMultiRest(userCut, cart, restID, userID, firstOrder);
            softAssert.assertTrue(cartFlag, "Cart " + cart.get(0) + " user has not shown the TD");
            menuFlag = menuEvaluationForMultiRest(userCut, menu, userID, restID, firstOrder);
            softAssert.assertTrue(menuFlag, "Cart " + menu.get(0) + " user has not shown the TD");
            if (!(cartFlag && menuFlag)) {
                return false;
            }
        }
        listingFlag = listingEvaluationForMultiRest(userCut, list, userID, restIDs, firstOrder);
        softAssert.assertTrue(cartFlag, "Cart " + list.get(0) + " user has not shown the TD");
        softAssert.assertAll();
        if (!listingFlag)
            return false;

        if (cartFlag && listingFlag && menuFlag) {
            flag = true;
            System.out.println("cartFlag && listingFlag --> " + flag);
        } else
            System.out.println("cartFlag val -- " + cartFlag + " -- listingFlag -- either are not true");
        return flag;
    }

    public boolean evaluateNotSuperMultiRestIndividual(String userCut, List<String> cart, List<String> list, List<String> menu, String visitedRest, String notvisitedRest, String visitedUser, String notvisitedUser) throws IOException {
        System.out.println(" -- IN EVALUATE-NOT-SUPER");
        boolean flag = false, cartFlag = false, listingFlag = false, menuFlag = false;

        List<Integer> restIDs = new ArrayList<Integer>();

        cartFlag = cartEvaluationForMultiRest(userCut, cart, Integer.parseInt(visitedRest), visitedUser, false);
        softAssert.assertFalse(cartFlag, "Cart " + cart.get(0) + " user has not shown the TD");
        menuFlag = menuEvaluationForMultiRest(userCut, menu, visitedUser, Integer.parseInt(visitedRest), false);
        softAssert.assertFalse(menuFlag, "Cart " + menu.get(0) + " user has not shown the TD");


//        Assert.assertFalse(listingFlag, "Cart "+ list.get(0)+ " user has not shown the TD");

        cartFlag = cartEvaluationForMultiRest(userCut, cart, Integer.parseInt(notvisitedRest), notvisitedUser, false);
        softAssert.assertTrue(cartFlag, "Cart " + cart.get(0) + " user has not shown the TD");
        menuFlag = menuEvaluationForMultiRest(userCut, menu, notvisitedUser, Integer.parseInt(notvisitedRest), false);
        softAssert.assertTrue(menuFlag, "Cart " + menu.get(0) + " user has not shown the TD");

        if (userCut.equalsIgnoreCase("dorm30")) {
            setDormantUser(notvisitedUser, visitedRest, "1");
            setDormantUser(notvisitedUser, notvisitedRest, "30");

        } else if (userCut.equalsIgnoreCase("restaurantFirstOrder"))
            setDormantUser(notvisitedUser, visitedRest, "1");

        listingFlag = listingEvaluationForMultiRestIndividual(userCut, list, visitedRest, notvisitedRest, visitedUser, notvisitedUser);
        softAssert.assertTrue(listingFlag, "Listing " + list.get(0) + " user has not shown the TD");
        softAssert.assertAll();

        return true;
    }

    private boolean cartEvaluation(String userCut, List<String> cartVal, int restID, String userID, Boolean isSuper) throws IOException {
        System.out.println(" -- IN CART-EVALUATION");
        JsonHelper jsonHelper = new JsonHelper();
        boolean flag = false;
        boolean firstOrder = false, restaurantFirstOrder = false, isPublic = false;
        String dorm_days = "0";
        if (userCut.equalsIgnoreCase("firstOrder"))
            firstOrder = true;
        else if (userCut.equalsIgnoreCase("dorm30"))
            dorm_days = "30"; // ignoring for time being.. later on it has to implement...
        else if (userCut.equalsIgnoreCase("restaurantFirstOrder"))
            restaurantFirstOrder = true;
        else
            isPublic = true;

        EvaluateCartV3 evaluateCartV3 = new EvaluateCartV3();

        evaluateCartV3.build(Integer.parseInt(userID), firstOrder, restaurantFirstOrder);

        ItemRequest itemRequest = new ItemRequest();
        itemRequest.build(restID, freebie_catID, freebie_subCatID, Integer.parseInt(freebie_itemID));
        List<ItemRequest> list = new ArrayList<>();
        list.add(itemRequest);
        evaluateCartV3.setItemRequests(list);
        Processor processor = evaluateCartHelper(jsonHelper.getObjectToJSON(evaluateCartV3));

        if (isSuper) {
            flag = false;
            String super_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..super..tradeDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String[] super_campaignId = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..super..tradeDiscountInfo..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
            String get_rest_isPublic = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..super..tradeDiscountInfo..public").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String[] edvo_campaigns = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..meals[*].campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
            String edvo_rewardType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..meals[*].rewardType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();

            String rest_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..restaurants..tradeDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();


            System.out.println(rest_discountType);
            String[] rest_tds = rest_discountType.split(",");

            boolean campflag = false, discountflag = false;
            String[] super_tds = super_discountType.split(",");
            String[] meal_tds = edvo_rewardType.split(",");
            for (String s : cartVal) {
                String[] ss = s.split(",");
                for (String td : ss) {
                    if (td.substring(0, 1).equalsIgnoreCase("R")) {
                        String convertedValue = getActualValue(td);
                        if (Arrays.asList(rest_tds).contains(convertedValue)) {
                            campflag = true;

                        } else {
                            campflag = false;
//                            Assert.fail("Restaurant " +convertedValue+" TD has not found");
                            break;
                        }

                    } else if (td.substring(0, 1).equalsIgnoreCase("S")) {
                        String convertedValue = getActualValue(td);
                        if (Arrays.asList(super_tds).contains(convertedValue)) {
                            campflag = true;

                        } else {
                            campflag = false;
//                            Assert.fail("Super " +convertedValue+" TD has not found");
                            break;
                        }

                    } else if (td.equalsIgnoreCase("BXGY")) {
                        String actualValue = getActualValue(td);
                        if (Arrays.asList(meal_tds).contains(actualValue)) {
                            System.out.println("Could not find dp discountType in super discount type. Could not find --" + s + " in the response");
                            campflag = false;

                        } else {
                            campflag = false;
                            break;
                        }
                    } else {
                        campflag = false;
                        System.out.println("Not found..");
                    }
                }
                if (campflag == false)
                    break;
            }

            if (!super_campaignID.isEmpty()) {
                System.out.println("super campaign id");
                for (int i = 0; i < super_campaignID.size(); i++) {
                    // if (super_campaignID.contains(super_campaignID.get(i)))

                    //super_campaignId
                    if (Arrays.asList(super_campaignId).contains(super_campaignID.get(i))) {
                        discountflag = true;

                    } else {
                        discountflag = false;
                        softAssert.fail("Super campaign ID has not found");
                        break;
                    }
                }

            } else
                discountflag = true;
//                if (!edvo_campaignID.isEmpty()) {
//                    System.out.println("edvo campaign id");
//                    for (int i = 0; i < edvo_campaignID.size(); i++) {
//                        if (!(Arrays.binarySearch(edvo_campaigns, edvo_campaignID.get(i)) >= 0)) {
//                            System.out.println("Could not find campaignID --> " + super_campaignID.get(i) + " in API response");
//                            flag = false;
//                        }
//                    }
//                }

            flag = campflag && discountflag;
        } else {

            String rest_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restaurants..tradeDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();

            String[] rest_campaignID = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..restaurants..tradeDiscountInfo..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");

            boolean campflag = false, discountFlag = false;

            for (String s : restaurant_campaignID) {
                if ((Arrays.asList(rest_campaignID).contains(s))) {
                    campflag = true;
                } else {
                    campflag = false;
//                    Assert.fail("Restaurant campaign ID:" +s+" has not found");
                    break;
                }

            }

            for (String s : cartVal) {
                String convertedValue = getActualValue(s);
                if (Arrays.asList(rest_discountType.split(",")).contains(convertedValue)) {
                    discountFlag = true;
                } else {
                    discountFlag = false;
//                    Assert.fail("Restaurant " +convertedValue+" TD has not found");
                    break;
                }
            }
            if (!(campflag && discountFlag)) {
                flag = false;
                System.out.println("Could not find dp discountType/restaurant_campaignID in restaurant discount type. Could not find -- in the response");
            } else
                flag = true;

        }
        softAssert.assertAll();
        return flag;
    }


    private boolean listingEvaluation(String userCut, List<String> listVal, String userID, List<Integer> restIds) throws IOException {
        System.out.println(" -- IN LIST-EVALUATION");
        boolean flag = false;
        JsonHelper jsonHelper = new JsonHelper();
        boolean firstOrder = false;
        if (userCut.equalsIgnoreCase("firstOrder"))
            firstOrder = true;

        EvaluateListingV3 evaluateListingV3 = new EvaluateListingV3();
        evaluateListingV3.build(Integer.parseInt(userID), firstOrder, restIds);

        Processor processor = evaluateListingHelper(jsonHelper.getObjectToJSON(evaluateListingV3));
        String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
        String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
        boolean flagTDIL = evaluateTDIL(userCut, listVal.get(0), get_tdil_discountType, get_tdil_campaignIds);

        if (flagTDIL) {
            System.out.println("listingEvaluation was successful with ADI & TDIL == true");
            flag = true;
        } else {
            System.out.println("flagTDIL --> " + flagTDIL + "; flagADI --> " + flagTDIL + "; either have failed");
        }
        return flag;
    }


    public boolean menuEvaluation(String userCut, List<String> listVal, String userID, int restId) throws IOException {
        System.out.println(" -- IN MENU-EVALUATION");
        boolean flag = false;
        JsonHelper jsonHelper = new JsonHelper();
        boolean firstOrder = false;
        if (userCut.equalsIgnoreCase("firstOrder"))
            firstOrder = true;

        EvaluateMenu evaluateMenu = new EvaluateMenu();
        evaluateMenu.setFirstOrder(firstOrder);
        evaluateMenu.build(Integer.parseInt(userID), restId, firstOrder);

        Processor processor = evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..tradeDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
        // String get_ldi_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..legacyDiscountInfo..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..tradeDiscountInfo..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
        // String[] get_ldi_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..legacyDiscountInfo..campaignId").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim().split(",");

        boolean flagTDIL = evaluateTDIL(userCut, listVal.get(0), get_tdil_discountType, get_tdil_campaignIds);
        // boolean flagADI = evaluateLDI(userCut, listVal.get(0),get_ldi_discountType,get_ldi_campaignIds);
        if (flagTDIL) {
            System.out.println("listingEvaluation was successful with ADI & TDIL == true");
            flag = true;
        } else
            System.out.println("flagTDIL --> " + flagTDIL + "; flagADI -->  either have failed");
        return flag;
    }

    private boolean cartEvaluationForNudge(String userCut, List<String> cartVal, List<Integer> restID, String userID, String usertype) throws IOException {
        System.out.println(" -- IN CART-EVALUATION");
        JsonHelper jsonHelper = new JsonHelper();
        boolean flag = false;
        boolean firstOrder = false, restaurantFirstOrder = false, isPublic = false;
        String dorm_days = "0";
        if (userCut.equalsIgnoreCase("firstOrder"))
            firstOrder = true;
        else if (userCut.equalsIgnoreCase("dorm30"))
            dorm_days = "30"; // ignoring for time being.. later on it has to implement...
        else if (userCut.equalsIgnoreCase("restaurantFirstOrder"))
            restaurantFirstOrder = true;
        else
            isPublic = true;

        EvaluateCartV3 evaluateCartV3 = new EvaluateCartV3();

        evaluateCartV3.build(Integer.parseInt(userID), firstOrder, restaurantFirstOrder);

        ItemRequest itemRequest = new ItemRequest();
        itemRequest.build(restID.get(0), freebie_catID, freebie_subCatID, Integer.parseInt(freebie_itemID));
        List<ItemRequest> list = new ArrayList<>();
        list.add(itemRequest);
        evaluateCartV3.setItemRequests(list);
        Processor processor = evaluateCartHelper(jsonHelper.getObjectToJSON(evaluateCartV3));

        String rest_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..nudges[*]").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();

        if (usertype.equalsIgnoreCase("map") && !cartVal.contains("N/A")) {
            String tds[] = rest_discountType.split(",");
            for (int i = 0; i < tds.length; i++) {
                if (cartVal.contains(tds[i]))
                    flag = true;
                else {
                    softAssert.fail("Cart:  nudge has not found");
                    return false;
                }

            }
        } else {
            System.out.println("Empty validation for CART_V");
            flag = (rest_discountType.equals(""));
        }
        if (!flag)
            softAssert.fail("Listing: nudge has not found");
        softAssert.assertAll();
        return flag;
    }


    private boolean listingEvaluationForNudge(String userCut, List<String> listVal, String userID, List<Integer> restId, String usertype) throws IOException {
        System.out.println(" -- IN LIST-EVALUATION");
        boolean flag = false;
        JsonHelper jsonHelper = new JsonHelper();
        boolean firstOrder = false;
        if (userCut.equalsIgnoreCase("firstOrder"))
            firstOrder = true;
        EvaluateListingV3 evaluateListingV3 = new EvaluateListingV3();
        evaluateListingV3.build(Integer.parseInt(userID), firstOrder, restId);
        Processor processor = evaluateListingHelper(jsonHelper.getObjectToJSON(evaluateListingV3));
        //$.data..discountList..aggregatedDiscountInfo..header

        String[] sdMeta = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..aggregatedDiscountInfo..shortDescriptionList..meta").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
        String[] fd_meta = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..aggregatedDiscountInfo..descriptionList..meta").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");

        String[] get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..description").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");

        if (usertype.equalsIgnoreCase("map") && !listVal.contains("N/A")) {
            if (Arrays.asList(sdMeta).contains("Free delight only for SUPERs") && Arrays.asList(fd_meta).contains(listVal.get(0))) {
                return !Arrays.asList(get_tdil_discountType).contains(listVal.get(0));
            } else {
                flag = false;
            }
        } else if (usertype.equalsIgnoreCase("super")) {
            if (!listVal.contains("N/A"))
                return (Arrays.asList(get_tdil_discountType).contains(listVal.get(0))) &&
                        (Arrays.asList(sdMeta).contains("Free delight only for SUPERs")) && (Arrays.asList(fd_meta).contains(listVal.get(0)));
            else
                return !(Arrays.asList(sdMeta).contains("Free delight only for SUPERs")) && !(Arrays.asList(fd_meta).contains(listVal.get(0)));

        } else {
            if (Arrays.asList(sdMeta).contains("Free delight only for SUPERs") || Arrays.asList(fd_meta).contains("Free delight only for SUPERs")) {
                {
                    softAssert.fail("Listing: nudge has not found");
                    return false;
                }
            } else
                flag = true;
        }
        if (!flag)
            softAssert.fail("Listing: nudge has not found");
        softAssert.assertAll();
        return flag;
    }


    public boolean menuEvaluationForNudge(String userCut, List<String> listVal, String userID, int restId, String usertype) throws IOException {
        System.out.println(" -- IN MENU-EVALUATION");
        boolean flag = false;
        JsonHelper jsonHelper = new JsonHelper();
        boolean firstOrder = false;
        if (userCut.equalsIgnoreCase("firstOrder"))
            firstOrder = true;

        EvaluateMenu evaluateMenu = new EvaluateMenu();
        evaluateMenu.setFirstOrder(firstOrder);
        evaluateMenu.build(Integer.parseInt(userID), restId, firstOrder);

        Processor processor = evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String[] sdMeta = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..aggregatedDiscountInfo..shortDescriptionList..meta").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
        String[] fd_meta = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..aggregatedDiscountInfo..descriptionList..meta").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
        String[] get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..tradeDiscountInfo..description").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");

        if (usertype.equalsIgnoreCase("map") && !listVal.contains("N/A")) {
            if (Arrays.asList(sdMeta).contains("Free delight only for SUPERs") && Arrays.asList(fd_meta).contains(listVal.get(0))) {
                return !Arrays.asList(get_tdil_discountType).contains(listVal.get(0));
            } else {
                softAssert.fail("Menu: Super nudge has not found");
                flag = false;
            }
        } else if (usertype.equalsIgnoreCase("super")) {
            if (!listVal.contains("N/A"))
                return (Arrays.asList(get_tdil_discountType).contains(listVal.get(0))) &&
                        (Arrays.asList(sdMeta).contains("Free delight only for SUPERs")) && (Arrays.asList(fd_meta).contains(listVal.get(0)));
            else
                return !(Arrays.asList(sdMeta).contains("Free delight only for SUPERs")) && !(Arrays.asList(fd_meta).contains(listVal.get(0)));

        } else {
            if (Arrays.asList(sdMeta).contains("Free delight only for SUPERs") || Arrays.asList(fd_meta).contains("Free delight only for SUPERs")) {
                softAssert.fail("Menu: Super freebie nudge has not found");
                return false;
            } else
                flag = true;
        }
        if (!flag)
            softAssert.fail("Listing: nudge has not found");
        softAssert.assertAll();
        return flag;
    }


    private boolean cartEvaluationForMultiRest(String userCut, List<String> cartVal, int restID, String userID, Boolean firstOrder) throws IOException {
        System.out.println(" -- IN CART-EVALUATION");
        System.out.println("restID" + restID);
        JsonHelper jsonHelper = new JsonHelper();
        boolean flag = false;
        boolean restaurantFirstOrder = false, isPublic = false;
        String dorm_days = "0";
        if (userCut.equalsIgnoreCase("firstOrder") && firstOrder)
            firstOrder = true;
        else if (userCut.equalsIgnoreCase("dorm30"))
            dorm_days = "30"; // ignoring for time being.. later on it has to implement...
        else if (userCut.equalsIgnoreCase("restaurantFirstOrder"))
            restaurantFirstOrder = true;
        else
            isPublic = true;

        Processor processor = null;
//        createEdvoCampaign()

        if (cartVal.contains("BXGY")) {

            EvaluateCartV3 evaluateCartV3 = new EvaluateCartV3();
            evaluateCartV3.build(Integer.parseInt(userID), firstOrder, restaurantFirstOrder);

            ItemRequest itemRequest = new ItemRequest();
            itemRequest.build(restID, freebie_catID, freebie_subCatID, Integer.parseInt(freebie_itemID));
            List<ItemRequest> list = new ArrayList<>();
            list.add(itemRequest);
            evaluateCartV3.setItemRequests(list);
            //Meal Item request - debug
            List<MealItemRequest> mealItemList = new ArrayList<>();
            MealItemRequest mealitemrequest = new MealItemRequest();

            if (mealid != null) {
                mealitemrequest.setRestaurantId(restID);

                System.out.println(restmealidmap.entrySet());
                System.out.println("restID" + restID);
                Integer group_id_from_map = Integer.valueOf(restmealidmap.get(String.valueOf(restID) + "_groupid"));
                Integer meal_id_from_map = Integer.valueOf(restmealidmap.get(String.valueOf(restID) + "_mealid"));

                System.out.println("group_id_from_map  " + group_id_from_map);
                System.out.println("meal_id_from_map  " + meal_id_from_map);

                mealitemrequest.setGroupId(group_id_from_map);
                mealitemrequest.setMealId(meal_id_from_map);
            }
            mealItemList.add(mealitemrequest);
            evaluateCartV3.setMealItemRequests(mealItemList);

            processor = evaluateCartHelper(jsonHelper.getObjectToJSON(evaluateCartV3));

        } else {


            EvaluateCartV3 evaluateCartV3 = new EvaluateCartV3();
            evaluateCartV3.build(Integer.parseInt(userID), firstOrder, restaurantFirstOrder);

            ItemRequest itemRequest = new ItemRequest();
            itemRequest.build(restID, freebie_catID, freebie_subCatID, Integer.parseInt(freebie_itemID));
            List<ItemRequest> list = new ArrayList<>();
            list.add(itemRequest);
            evaluateCartV3.setItemRequests(list);
            //Meal Item request
            List<MealItemRequest> mealItemList = new ArrayList<>();
            MealItemRequest mealitemrequest = new MealItemRequest();
            if (mealid != null) {
                mealitemrequest.setRestaurantId(restID);
                mealitemrequest.setMealId(Integer.valueOf(mealid));
                mealitemrequest.setGroupId(Integer.valueOf(groupid));
            }
            mealItemList.add(mealitemrequest);
            evaluateCartV3.setMealItemRequests(mealItemList);
            processor = evaluateCartHelper(jsonHelper.getObjectToJSON(evaluateCartV3));
        }

        String rest_discountType;

        if (cartVal.contains("BXGY")) {
            //for EDVO dicount_tyoe will be at meal level in the response JSON
            rest_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..meals..tradeDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();

        } else {
            rest_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restaurants..tradeDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String rest_discountType_all = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restaurants..tradeDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();

        }
        System.out.println("rest_discountType" + rest_discountType);


        boolean campflag = false, discountFlag = false;
        if (!cartVal.contains("N/A")) {

            for (String s : cartVal) {
                String convertedValue = getActualValue(s);
                System.out.println("convertedValue" + convertedValue);
                if (Arrays.asList(rest_discountType.split(",")).contains(convertedValue)) {
                    discountFlag = true;
                } else {
                    discountFlag = false;
//                    Assert.fail("Restaurant " +convertedValue+" TD has not found");
                    break;
                }
            }
            if (!discountFlag) {
                flag = false;
                System.out.println("Could not find dp discountType/restaurant_campaignID in restaurant discount type. Could not find -- in the response");
            } else
                flag = true;

        } else
            flag = rest_discountType.equalsIgnoreCase("") ? true : false;

        System.out.println("flag" + flag);
        return flag;
    }


    private boolean listingEvaluationForMultiRest(String userCut, List<String> listVal, String userID, List<Integer> restIds, boolean firstOrder) throws IOException {
        System.out.println(" -- IN LIST-EVALUATION");
        boolean flag = false;
        JsonHelper jsonHelper = new JsonHelper();
        if (userCut.equalsIgnoreCase("firstOrder") && firstOrder)
            firstOrder = true;

        EvaluateListingV3 evaluateListingV3 = new EvaluateListingV3();
        evaluateListingV3.build(Integer.parseInt(userID), firstOrder, restIds);

        Processor processor = evaluateListingHelper(jsonHelper.getObjectToJSON(evaluateListingV3));
        String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
        String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
        String[] dpVals = listVal.get(0).split(",");
        String[] all_tdil_td = get_tdil_discountType.split(",");
        List<String> allCampaigns = new ArrayList<>(restaurant_campaignID);
        allCampaigns.addAll(super_campaignID);
        allCampaigns.addAll(edvo_campaignID);
        String[] allCampaignIds = allCampaigns.toArray(new String[allCampaigns.size()]);
        boolean campflag = false, discountFlag = false;
        if (!listVal.contains("N/A")) {
            for (String s : dpVals) {
                if (s.substring(0, 1).equalsIgnoreCase("R") || s.equalsIgnoreCase("BXGY")) {
                    String convertedValue = getActualValue(s);
                    if (Arrays.asList(all_tdil_td).contains(convertedValue)) {
                        flag = true;

                    } else {
                        flag = false;
                        break;
                    }

                }
            }
        } else
            flag = get_tdil_discountType.equalsIgnoreCase("") ? true : false;
        return flag;
    }


    private boolean listingEvaluationForMultiRestIndividual(String userCut, List<String> listVal, String visitedRest, String notvisitedRest, String visitedUser, String notvisitedUser) throws IOException {
        System.out.println(" -- IN LIST-EVALUATION");
        boolean flag = false;
        List<Integer> restIds = new ArrayList<Integer>();
        restIds.add(Integer.parseInt(visitedRest));
        restIds.add(Integer.parseInt(notvisitedRest));
        JsonHelper jsonHelper = new JsonHelper();
        boolean firstOrder = false;
        if (userCut.equalsIgnoreCase("firstOrder"))
            firstOrder = true;

        EvaluateListingV3 evaluateListingV3 = new EvaluateListingV3();
        evaluateListingV3.build(Integer.parseInt(notvisitedUser), firstOrder, restIds);

        Processor processor = evaluateListingHelper(jsonHelper.getObjectToJSON(evaluateListingV3));
        String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
        String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
        String[] get_tdil_restids = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..restaurantId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
        String[] dpVals = listVal.get(0).split(",");
        String[] all_tdil_td = get_tdil_discountType.split(",");
        List<String> allCampaigns = new ArrayList<>(restaurant_campaignID);
        allCampaigns.addAll(super_campaignID);
        allCampaigns.addAll(edvo_campaignID);
        String[] allCampaignIds = allCampaigns.toArray(new String[allCampaigns.size()]);
        softAssert.assertTrue(get_tdil_restids.length == 1);
        softAssert.assertTrue(get_tdil_restids[0].equalsIgnoreCase(notvisitedRest));
        if (!listVal.contains("N/A")) {
            for (String s : dpVals) {
                if (s.substring(0, 1).equalsIgnoreCase("R")) {
                    String convertedValue = getActualValue(s);
                    if (Arrays.asList(all_tdil_td).contains(convertedValue)) {
                        flag = true;
                    } else {
                        flag = false;
                        break;
                    }

                }
            }
        } else
            flag = get_tdil_discountType.equalsIgnoreCase("") ? true : false;
        softAssert.assertAll();
        return flag;
    }


    public boolean menuEvaluationForMultiRest(String userCut, List<String> menueval, String userID, int restId, boolean firstOrder) throws IOException {
        System.out.println(" -- IN MENU-EVALUATION");
        boolean flag = false;
        JsonHelper jsonHelper = new JsonHelper();
        if (userCut.equalsIgnoreCase("firstOrder") && firstOrder)
            firstOrder = true;

        EvaluateMenu evaluateMenu = new EvaluateMenu();
        evaluateMenu.setFirstOrder(firstOrder);
        evaluateMenu.build(Integer.parseInt(userID), restId, firstOrder);

        Processor processor = evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..tradeDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
        // String get_ldi_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..legacyDiscountInfo..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..tradeDiscountInfo..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
        // String[] get_ldi_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..legacyDiscountInfo..campaignId").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim().split(",");
        String[] dpVals = menueval.get(0).split(",");
        String[] all_tdil_td = get_tdil_discountType.split(",");
        List<String> allCampaigns = new ArrayList<>(restaurant_campaignID);
        allCampaigns.addAll(super_campaignID);
        allCampaigns.addAll(edvo_campaignID);
        String[] allCampaignIds = allCampaigns.toArray(new String[allCampaigns.size()]);
        if (!menueval.contains("N/A")) {
            for (String s : dpVals) {
                if (s.substring(0, 1).equalsIgnoreCase("R") || s.equalsIgnoreCase("BXGY")) {
                    String convertedValue = getActualValue(s);
                    if (Arrays.asList(all_tdil_td).contains(convertedValue)) {
                        flag = true;

                    } else {
                        flag = false;
                        break;
                    }

                }
            }
        } else
            flag = get_tdil_discountType.equalsIgnoreCase("") ? true : false;
        return flag;
    }


    private boolean evaluateTDIL(String userCut, String toValidate, String get_tdil_discountType, String[] get_tdil_campaignIds) {
        System.out.println(" -- IN EVALUATE-TDIL");
        boolean flag = false;
        String[] dpVals = toValidate.split(",");
        String[] all_tdil_td = get_tdil_discountType.split(",");
        List<String> allCampaigns = new ArrayList<>(restaurant_campaignID);
        allCampaigns.addAll(super_campaignID);
        allCampaigns.addAll(edvo_campaignID);
        String[] allCampaignIds = allCampaigns.toArray(new String[allCampaigns.size()]);
        // String[] allCampaignIds = (String[]) allCampaigns.toArray();
        boolean campflag = false, discountFlag = false;


        for (String s : dpVals) {
            if (s.substring(0, 1).equalsIgnoreCase("R")) {
                String convertedValue = getActualValue(s);
                if (Arrays.asList(all_tdil_td).contains(convertedValue)) {
                    campflag = true;

                } else {
                    campflag = false;
//                    Assert.fail("Restaurant " +convertedValue+" TD has not found");
                    break;
                }

            } else if (s.substring(0, 1).equalsIgnoreCase("S")) {
                String convertedValue = getActualValue(s);
                if (Arrays.asList(all_tdil_td).contains(convertedValue)) {
                    campflag = true;

                } else {
                    campflag = false;
//                    Assert.fail("Super " +convertedValue+" TD has not found");
                    break;
                }

            }
        }
        for (String s : dpVals) {
            if (s.substring(0, 1).equalsIgnoreCase("R")) {

                for (int i = 0; i < restaurant_campaignID.size(); i++) {
                    // if (super_campaignID.contains(super_campaignID.get(i)))

                    //super_campaignId
                    if (allCampaigns.contains(get_tdil_campaignIds[i])) {
                        discountFlag = true;

                    } else {
                        discountFlag = false;
//                        Assert.fail("Restaurant Campaign ID of TD has not found");
                        break;
                    }
                }
            } else if (s.substring(0, 1).equalsIgnoreCase("S")) {
                for (int i = 0; i < super_campaignID.size(); i++) {
                    // if (super_campaignID.contains(super_campaignID.get(i)))

                    //super_campaignId
                    if (allCampaigns.contains(get_tdil_campaignIds[i])) {
                        discountFlag = true;

                    } else {
                        discountFlag = false;
//                            Assert.fail("Super Campaign ID of TD has not found");

                        break;
                    }
                }
            }

        }



        if (!(campflag && discountFlag)) {
            flag = false;
            System.out.println("Could not find dp discountType/restaurant_campaignID in restaurant discount type. Could not find -- in the response");
        } else
            flag = true;

        return flag;
    }

    private boolean evaluateLDI(String userCut, String toValidate, String get_ldi_discountType, String[] get_ldi_campaignIds) {
        System.out.println(" -- IN EVALUATE-ADI");

        System.out.println(" -- IN EVALUATE-TDIL");
        boolean flag = true;
        String[] dpVals = toValidate.split(",");
        String[] all_tdil_td = get_ldi_discountType.split(",");
        List<String> allCampaigns = new ArrayList<>(restaurant_campaignID);
        allCampaigns.addAll(super_campaignID);
        allCampaigns.addAll(edvo_campaignID);
        String[] allCampaignIds = allCampaigns.toArray(new String[allCampaigns.size()]);
        // String[] allCampaignIds = (String[]) allCampaigns.toArray();

        //checks if all the campaign IDs in response are there in the campaign IDs created for the test case
        for (int i = 0; i < get_ldi_campaignIds.length; i++) {
            if (!(Arrays.binarySearch(allCampaignIds, get_ldi_campaignIds[i]) >= 0)) {
                System.out.println("Could not find the campaignID --> " + get_ldi_campaignIds[i] + " in all the campaignIDs created");
                flag = false;
            }

        }
        //check if all the discountTypes as per DP are there in the response
        for (String s : dpVals) {
            String actualValue = getActualValue(s);
            if (!(Arrays.binarySearch(all_tdil_td, actualValue) >= 0)) {
                System.out.println("Could not find discountType --> \"" + s + "\" in the evaluateTDIL response");
                flag = false;
            }
        }
        return flag;
    }

    private boolean evaluateADI(String userCut, String toValidate, String get_adi_discountType, String[] get_adi_campaignIds) {
        System.out.println(" -- IN EVALUATE-ADI");

        System.out.println(" -- IN EVALUATE-TDIL");
        boolean flag = false;
        String[] dpVals = toValidate.split(",");
        String[] all_tdil_td = get_adi_discountType.split(",");
        List<String> allCampaigns = new ArrayList<>(restaurant_campaignID);
        allCampaigns.addAll(super_campaignID);
        allCampaigns.addAll(edvo_campaignID);
        String[] allCampaignIds = allCampaigns.toArray(new String[allCampaigns.size()]);
        // String[] allCampaignIds = (String[]) allCampaigns.toArray();

        //checks if all the campaign IDs in response are there in the campaign IDs created for the test case
//        for (int i = 0; i < get_adi_campaignIds.length; i++) {
//            if (!(Arrays.binarySearch(allCampaignIds,get_adi_campaignIds[i]) >= 0))  {
//                System.out.println("Could not find the campaignID --> "+get_adi_campaignIds[i]+" in all the campaignIDs created");
//                flag = false;
//            }
//
//        }
//        //check if all the discountTypes as per DP are there in the response
//        for (String s:dpVals) {
//            String actualValue = getActualValue(s);
//            if (!(Arrays.binarySearch(all_tdil_td,actualValue) >= 0)) {
//                System.out.println("Could not find discountType --> \"" + s + "\" in the evaluateTDIL response");
//                flag = false;
//            }
        for (String s : get_adi_campaignIds) {
            if (allCampaigns.contains(s)) {
                flag = true;
                break;
            }

        }

        //check if all the discountTypes as per DP are there in the response
        for (String s : get_adi_campaignIds) {
            String actualValue = getActualValue(s);
            if (Arrays.asList(all_tdil_td).contains(get_adi_discountType)) {
                flag = true;
                break;
            }

        }
        return flag;


//        boolean firstOrder = false,restaurantFirstOrder = false, isPublic = false;
//        String dorm_days = "0";
//        if (userCut.equalsIgnoreCase("SFO"))
//            firstOrder = true;
//        else if (userCut.equalsIgnoreCase("dorm30"))
//            dorm_days = "30";
//        else if (userCut.equalsIgnoreCase("RFO"))
//            restaurantFirstOrder = true;
//        else
//            isPublic = true;
//        String[] dpVals = toValidate.split(",");
//        String[] all_tdil_td = get_adi_discountType.split(",");
//        for (String s:dpVals) {
//            String convertedValue = getConvertedValue(s);
//            if (!(Arrays.binarySearch(all_tdil_td,convertedValue) >= 0)) {
//                System.out.println("Could not find dp Assert Values in TDIL discountType. Could not find --" + s + " in the response");
//                flag = false;
//            }
//        }

    }

    public int generateRandom() {
        Random random = new Random();
        int x = random.nextInt(999999) + 10000;
        return x;
    }

    private String getActualValue(String s) {
        String val = s;
        switch (s) {
            case "S_FD":
                return "FREE_DELIVERY";
            case "R_FD":
                return "FREE_DELIVERY";
            case "S_FB":
                return "Freebie";
            case "R_FB":
                return "Freebie";
            case "R_FLAT":
                return "Flat";
            case "R_PERCENTAGE":
                return "Percentage";
            case "BXGY":
                return "BXGY";
            default: {
                System.out.println("getConvertedValue() could not find the case for -- " + s);
                break;
            }
        }
        return val;
    }

    public void setDormantUser(String userId, String restId, String days) {

        RedisHelper redisHelper = new RedisHelper();
        //hget user_restaurant_last_order:7206851 987424
        System.out.println("hget user_restaurant_last_order:" + userId + " " + restId);
        System.out.println("UserID:" + userId + "RestID" + restId + "  Dormant days  " + days);
        Calendar calender = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        calender.add(Calendar.DATE, - (Integer.parseInt(days)));
        String date = dateFormat.format(calender.getTime());
        redisHelper.setHashValue("multitd", 0, "user_restaurant_last_order:" + userId, restId, date);
        System.out.println("Done redis connection...");
    }

    public void deleteUserInRedis(String userId) {

        RedisHelper redisHelper = new RedisHelper();
        System.out.println("UserID:" + userId);
        redisHelper.deleteKey("multitd", 0, "user_restaurant_last_order:" + userId);
        System.out.println("Deleted user in redis...");
    }

    public void deleteUserInRedis(String userId, String restId) {

        RedisHelper redisHelper = new RedisHelper();
        System.out.println("UserID:" + userId);
        System.out.println("user_restaurant_last_order:" + userId + " " + restId);
        redisHelper.deleteKey("multitd", 0, "user_restaurant_last_order:" + userId + " " + restId);
        System.out.println("Deleted user in redis...");
    }

    public void setAllMenuValue() {

        RedisHelper redisHelper = new RedisHelper();
        String key = "AV_MENU_355586";
        for (int i = 12135; i <= 500000; i++) {
            key = "AV_MENU_" + i;
            redisHelper.setValueJson("sandredisstage", 0, key, "{\"status\":true,\"nextChangeTime\":1636364740000}");
            System.out.println("Done For:" + key);
        }
    }


    //


    public void createRandomUser() throws InterruptedException {
        userIDs = getRandomUserID();
        //Assigning user to readable variable to ease of understanding cases
        user_freebie = userIDs[0];
        user_freedel = userIDs[1];
        user_freedel_freebie = userIDs[2];
        user_not_super = userIDs[3];
        user_no_userCut = userIDs[4];
        user_map_fd_fb = userIDs[5];
        freebie_itemID = createItemForRandomRestaurant();
    }


    public HashMap<String, String> createRestaurantFirstOrderEdvo(String restID) {
        String restId;
        String mealId;
        String groupId;
        ArrayList<String> listOfRestIds = new ArrayList();
        ArrayList<String> allMealIds = new ArrayList();
        ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
        HashMap<String, String> returnVals = new HashMap<>();
        mealid = String.valueOf(generateRandom());
        groupid = String.valueOf(generateRandom());
        // listOfAllRestaurantIds
        listOfRestIds.add(0, restId = restID);
        // list of All MealIds
        allMealIds.add(0, mealId = mealid);
        // list of groupIds in Meal
        allGroupIdsOfMeals.add(0, groupId = groupid);
        HashMap<String, String> createEDVOCampRestFirstOrder = new HashMap<String, String>();
        createEDVOCampRestFirstOrder = edvoHelper.createCampaignWithUserCut(false, false, true, "ZERO_DAYS_DORMANT",
                new CreateReward().toString(), new CreateReward("NONE", "FinalPrice", "120", "1").toString(),
                listOfRestIds.get(0), allMealIds.get(0), allGroupIdsOfMeals.get(0));

        Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
        int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
                .GetNodeValue("$.statusMessage");
        softAssert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success","Status message is not as expected... Actual status message is "+edvoCampRestFirstOrderResponseStatusMessage);
        int edvo_campaignID = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        int edvo_campaign_status = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if (edvo_campaign_status == 0) {
            returnVals.put("TDID", "" + edvo_campaignID);
            returnVals.put("status", "success");
            restmealidmap.put(restID + "_" + "mealid", mealid);
            restmealidmap.put(restID + "_" + "groupid", groupid);
        }
        softAssert.assertAll();
        return returnVals;
    }


    public HashMap<String, String> createSwiggyFirstOrderEdvo(String restID) {
        String mealId;
        HashMap<String, String> createEDVOCampWithSwiggyFirstOrder;
        ArrayList<String> allMealIds = new ArrayList();
        HashMap<String, String> returnVals = new HashMap<>();

        mealid = String.valueOf(generateRandom());


        // listOfAllMealIds
        allMealIds.add(0, mealId = mealid);


        createEDVOCampWithSwiggyFirstOrder = edvoHelper.createUserCutMultiTD(false, true, false,
                "ZERO_DAYS_DORMANT", restID, allMealIds.get(0));
        Processor CampWithSwiggyFirstOrder = edvoHelper.createEDVOTradeDiscount(createEDVOCampWithSwiggyFirstOrder);
        int statusCode2 = CampWithSwiggyFirstOrder.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage2 = CampWithSwiggyFirstOrder.ResponseValidator.GetNodeValue("$.statusMessage");
        int campaignId2 = CampWithSwiggyFirstOrder.ResponseValidator.GetNodeValueAsInt("$.data");
        softAssert.assertEquals(statusCode2, 0, "Status code is not as expected... Actual status code is "+statusCode2  );
        softAssert.assertEquals(statusMessage2, "success", "Status message is not as expected... Actual status message is "+statusMessage2 );

        HashMap<String, String> hm;
        int edvo_campaignID = CampWithSwiggyFirstOrder.ResponseValidator.GetNodeValueAsInt("$.data");
        int edvo_campaign_status = CampWithSwiggyFirstOrder.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if (edvo_campaign_status == 0) {
            returnVals.put("TDID", "" + edvo_campaignID);
            returnVals.put("status", "success");
            restmealidmap.put(restID + "_" + "mealid", mealid);
            restmealidmap.put(restID + "_" + "groupid", groupid);
        }
        softAssert.assertAll();
        return returnVals;
    }


    public HashMap<String, String> createDormantUserEdvo(String restID) {
        String mealId;
        String groupId;
        HashMap<String, String> createEDVOCampWitDormantUser;
        ArrayList<String> allMealIds = new ArrayList();
        HashMap<String, String> returnVals = new HashMap<>();
        String dormantDays = "30";
        ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();

        mealid = String.valueOf(generateRandom());
        groupid = String.valueOf(generateRandom());

        // listOfAllMealIds
        allMealIds.add(0, mealId = mealid);
        allGroupIdsOfMeals.add(0,groupId = groupid);

        HashMap<String, String> createEDVOCamp = edvoHelper.createDormantTypeEDVOCampaign("THIRTY_DAYS_DORMANT",restID,
                allMealIds.get(0), allGroupIdsOfMeals.get(0));
        Processor campResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCamp);
        int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertTrue(campStatusMessage.equalsIgnoreCase("success"), "Campaign creation failed.. Status Message is" +campStatusMessage);

        HashMap<String, String> hm;
        int edvo_campaignID = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        int edvo_campaign_status = campResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if (edvo_campaign_status == 0) {
            returnVals.put("TDID", "" + edvo_campaignID);
            returnVals.put("status", "success");
            restmealidmap.put(restID + "_" + "mealid", mealid);
            restmealidmap.put(restID + "_" + "groupid", groupid);
        }
        softAssert.assertAll();
        return returnVals;
    }





}