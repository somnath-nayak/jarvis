package com.swiggy.api.sf.rng.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.rng.constants.MultiTDConstants;
import com.swiggy.api.sf.rng.dp.SuperMultiTDDP;
import com.swiggy.api.sf.rng.helper.SuperMultiTDHelper;
import com.swiggy.api.sf.rng.pojo.MultiTD.CreateSuperCampaignV2.RemoveFreebie;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.UpdatePlan;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.tests
 **/
public class SuperMultiTDTest extends SuperMultiTDDP {

    SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();
    private static Logger log = LoggerFactory.getLogger(SuperMultiTDTest.class);
    int count = 1;
    @BeforeClass(alwaysRun = true)
    public void getBenefitPlanUserMapped () throws IOException, InterruptedException {

        superMultiTDHelper.deactiveAllFreeDelBenifits();
        //superMultiTDHelper.deactiveAllMaping();
        superMultiTDHelper.createPlanBenifitWithMapingSuper();
        superMultiTDHelper.deactiveAllSubscription();
        superMultiTDHelper.createSubscription();

    }



    @Test(dataProvider = "multitdSuperCase", priority = 2,groups = {"multitdRegression"},description = "MultiTD regression cases multitdSuperCase")
    public void multitdSuperBusinessCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String,HashMap<String,List<String>>> eval, String caseType,String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag= false;
        log.info("TESTCASE ---> "+description);
        log.info("TESTCASE NUMBER: "+count++);
        flag = superMultiTDHelper.caseHandler(cases, actionAssert, eval, caseType);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }
    @Test(dataProvider = "multitdNonSuperCase", priority = 2,groups = {"multitdRegression"},description = "MultiTD regression cases multitdNonSuperCase")
    public void multitdNonSuperBusinessCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String,HashMap<String,List<String>>> eval, String caseType,String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag= false;
        log.info("TESTCASE ---> "+description);
        log.info("TESTCASE NUMBER: "+count++);
        flag = superMultiTDHelper.caseHandler(cases, actionAssert, eval, caseType);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }

//    @Test(dataProvider = "multitdEdvoCase", priority = 2,groups = {"multitdRegression"},description = "MultiTD regression cases edvo case")
//    public void multitdEdvoCases(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String,HashMap<String,List<String>>> eval, String caseType,String description) throws Exception {
//        SoftAssert softAssert = new SoftAssert();
//        boolean flag= false;
//        log.info("TESTCASE ---> "+description);
//        log.info("TESTCASE NUMBER: "+count++);
//        flag = superMultiTDHelper.caseHandler(cases, actionAssert, eval, caseType);
//        softAssert.assertTrue(flag);
//        softAssert.assertAll();
//
//    }

//    @Test(dataProvider = "businessUseCases", priority = 2,groups = {"multitdRegression"},description = "MultiTD regression cases")
//    public void multiTDSuperBusinessCases(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String,HashMap<String,List<String>>> eval, String caseType,String description) throws Exception {
//        SoftAssert softAssert = new SoftAssert();
//        boolean flag= false;
//        log.info("TESTCASE ---> "+description);
//        log.info("TESTCASE NUMBER: "+count++);
//        flag = superMultiTDHelper.caseHandler(cases, actionAssert, eval, caseType);
//        softAssert.assertTrue(flag);
//        softAssert.assertAll();
//
//    }

}
