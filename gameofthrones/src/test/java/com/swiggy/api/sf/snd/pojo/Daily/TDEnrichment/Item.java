package com.swiggy.api.sf.snd.pojo.Daily.TDEnrichment;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.pojo.Daily.TDEnrichment
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "skuId",
        "price"
})
public class Item {

    @JsonProperty("skuId")
    private String skuId;
    @JsonProperty("price")
    private Integer price;

    @JsonProperty("skuId")
    public String getSkuId() {
        return skuId;
    }

    @JsonProperty("skuId")
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("skuId", skuId).append("price", price).toString();
    }

    private void setDefaultValues(String skuId, String price) {
        if (price.equals("") || price.equals(" ") || price.equals(null))
            price = "0";
        if(this.getPrice() == null)
            this.setPrice(Integer.parseInt(price));
        if (this.getSkuId() == null)
            this.setSkuId(skuId);
    }

    public Item build(String skuId, String price) {
        setDefaultValues(skuId,price);
        return this;
    }

}
