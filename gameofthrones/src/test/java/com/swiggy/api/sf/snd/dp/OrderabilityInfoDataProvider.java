package com.swiggy.api.sf.snd.dp;

import framework.gameofthrones.Aegon.PropertiesHandler;
import org.testng.annotations.DataProvider;

public class OrderabilityInfoDataProvider {

    String Env;

    public PropertiesHandler properties = new PropertiesHandler();

    public OrderabilityInfoDataProvider() {
        if (System.getenv("ENVIRONMENT") == null)
            Env = properties.propertiesMap.get("environment");
        else
            Env = System.getenv("ENVIRONMENT");
    }

    @DataProvider(name = "validLatLong" )
    public Object[][] getValidLatLng()  {
        return new Object[][] {
                {"8096548963","123456","12.933","77.601"}

        };
    }

    @DataProvider(name = "invalidLatLong" )
    public Object[][] getInValidLatLng() {
        return new Object[][] {
                {"8096548963","123456","112.933","77.601"},
                {"8096548963","123456","12.933","177.601"},
                {"8096548963","123456","112.933","177.601"}
        };
    }


    @DataProvider(name = "BlackZoneLatLong" )
    public Object[][] getBlackZoneLatLong() {
        return new Object[][] {
                {"28.737236842579726","77.08114081342774"}

        };
    }

    @DataProvider(name = "rainPopEnabledLatLong" )
    public Object[][] getRainPopEnabledLatLong() {
        return new Object[][] {
                {"12.925136240162718","77.63161632823949"}

        };
    }

    @DataProvider(name = "validLatLong2" )
    public Object[][] getvalidLatLong2()  {
        return new Object[][] {
                {"12.933","77.601"}

        };
    }

    @DataProvider(name = "CityClosedLatLong" )
    public Object[][] getCityClosedLatLong() {
        return new Object[][] {
                {"12.933","77.601"}

        };
    }


}
