package com.swiggy.api.sf.checkout.tests;

import com.swiggy.api.sf.checkout.constants.CancellationFeeConstants;
import com.swiggy.api.sf.checkout.dp.CancellationFeeDP;
import com.swiggy.api.sf.checkout.helper.CancellationFeeHelper;
import com.swiggy.api.sf.checkout.helper.rts.validator.GenericValidator;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CancellationFeeTest extends CancellationFeeDP{
    CancellationFeeHelper cancellationFeeHelper = new CancellationFeeHelper();
    GenericValidator genericValidator = new GenericValidator();
    List<String> orderIds = new ArrayList<>();
    String orderId = null;

    @Test (dataProvider = "smokeTestData", groups = {"smoke"}, description = "Smoke Test for OrderCancelCheck API...")
    public void orderCancelCheckSmokeTest(String restaurantId){
        Processor processor = cancellationFeeHelper.getOrderCancelCheck(restaurantId);
        genericValidator.smokeCheck(CancellationFeeConstants.STATUS_CODE, CancellationFeeConstants.STATUS_MESSAGE_ORDER_CANCEL_CHECK,
                processor, "[Order Cancel Check] Smoke Test Failed");
    }

    @Test (dataProvider = "smokeTestData", groups = {"smoke"}, description = "Smoke Test for RevertCancellationFee API...")
    public void revertCancellationFeeSmokeTest(String restaurantId){
        Processor processor = cancellationFeeHelper.getRevertCancellationFee(restaurantId);
        genericValidator.smokeCheck(CancellationFeeConstants.STATUS_CODE, CancellationFeeConstants.STATUS_MESSAGE_ORDER_CANCEL_CHECK,
                processor, "[REVERT CANCELLATION FEE] Smoke Test Failed");
    }

    @Test(dataProvider = "smokeTestData", groups = {"smoke"}, description = "Smoke Test for OrderCancel API...")
    public void orderCancelSmokeTest(String restaurantId){
        Processor processor = cancellationFeeHelper.getOrderCancel(restaurantId);
        genericValidator.smokeCheck(CancellationFeeConstants.STATUS_CODE, CancellationFeeConstants.STATUS_MESSAGE_ORDER_CANCEL
                , processor, "[Order Cancel] Smoke Test Failed");
    }

    @AfterMethod
    public void cancelAllOrders(){
        cancellationFeeHelper.cancelAllOrders();
    }
}
