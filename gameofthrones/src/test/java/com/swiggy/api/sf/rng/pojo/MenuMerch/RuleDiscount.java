package com.swiggy.api.sf.rng.pojo.MenuMerch;

import org.codehaus.jackson.annotate.JsonProperty;

public class RuleDiscount {

    @JsonProperty("minCartAmount")
    private String minCartAmount;
    @JsonProperty("discountLevel")
    private String discountLevel;
    @JsonProperty("type")
    private String type;
    @JsonProperty("flatDiscountAmount")
    private String flatDiscountAmount;

    @JsonProperty("minCartAmount")
    public String getMinCartAmount ()
    {
        return minCartAmount;
    }
    @JsonProperty("minCartAmount")
    public void setMinCartAmount (String minCartAmount)
    {
        this.minCartAmount = minCartAmount;
    }

    public RuleDiscount withMinCartAmount(String minCartAmount) {
        this.minCartAmount = minCartAmount;
        return this;
    }

    @JsonProperty("discountLevel")
    public String getDiscountLevel ()
    {
        return discountLevel;
    }
    @JsonProperty("discountLevel")
    public void setDiscountLevel (String discountLevel)
    {
        this.discountLevel = discountLevel;
    }

    public RuleDiscount withDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
        return this;
    }

    @JsonProperty("type")
    public String getType ()
    {
        return type;
    }
    @JsonProperty("type")
    public void setType (String type)
    {
        this.type = type;
    }

    public RuleDiscount withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("flatDiscountAmount")
    public String getFlatDiscountAmount ()
    {
        return flatDiscountAmount;
    }
    @JsonProperty("flatDiscountAmount")
    public void setFlatDiscountAmount (String flatDiscountAmount)
    {
        this.flatDiscountAmount = flatDiscountAmount;
    }

    public RuleDiscount withFlatDiscountAmount(String flatDiscountAmount) {
        this.flatDiscountAmount = flatDiscountAmount;
        return this;
    }

    public RuleDiscount setDefault(){
        return this.withDiscountLevel("Item")
                .withFlatDiscountAmount("10")
                .withMinCartAmount("0")
                .withType("Flat");
    }

    @Override
    public String toString()
    {
        return "ClassPojo [minCartAmount = "+minCartAmount+", discountLevel = "+discountLevel+", type = "+type+", flatDiscountAmount = "+flatDiscountAmount+"]";
    }
}
