package com.swiggy.api.sf.snd.pojo.Daily;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.pojo.Daily.DeliveryListing
 **/
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "store_id",
        "spins"
})
public class SearchBySpin {

    @JsonProperty("store_id")
    private Integer store_id;
    @JsonProperty("spins")
    private List<String> spins = null;

    @JsonProperty("store_id")
    public Integer getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(Integer store_id) {
        this.store_id = store_id;
    }

    @JsonProperty("spins")
    public List<String> getSpins() {
        return spins;
    }

    @JsonProperty("spins")
    public void setSpins(List<String> spins) {
        this.spins = spins;
    }

    private void setDefaultValues(Integer store_id, List<String> spins){
        if (this.getSpins() == null)
            this.setSpins(spins);
        if (this.getStore_id() == null)
            this.setStore_id(store_id);
    }

    public SearchBySpin build(String storeId, List<String> spin) {
        setDefaultValues(Integer.parseInt(storeId),spin);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("store_id", store_id).append("spins", spins).toString();
    }

}
