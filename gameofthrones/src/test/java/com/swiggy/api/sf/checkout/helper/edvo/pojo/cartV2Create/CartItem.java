package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "addons",
        "variants",
        "menu_item_id",
        "quantity"
})
public class CartItem {

    @JsonProperty("addons")
    private List<Addon> addons = null;
    @JsonProperty("variants")
    private List<Variant> variants = null;
    @JsonProperty("menu_item_id")
    private String menuItemId;
    @JsonProperty("quantity")
    private Integer quantity;

    /**
     * No args constructor for use in serialization
     *
     */
    public CartItem() {
    }

    /**
     *
     * @param menuItemId
     * @param addons
     * @param variants
     * @param quantity
     */
    public CartItem(List<Addon> addons, List<Variant> variants, String menuItemId, Integer quantity) {
        super();
        this.addons = addons;
        this.variants = variants;
        this.menuItemId = menuItemId;
        this.quantity = quantity;
    }

    @JsonProperty("addons")
    public List<Addon> getAddons() {
        return addons;
    }

    @JsonProperty("addons")
    public void setAddons(List<Addon> addons) {
        this.addons = addons;
    }

    public CartItem withAddons(List<Addon> addons) {
        this.addons = addons;
        return this;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    public CartItem withVariants(List<Variant> variants) {
        this.variants = variants;
        return this;
    }

    @JsonProperty("menu_item_id")
    public String getMenuItemId() {
        return menuItemId;
    }

    @JsonProperty("menu_item_id")
    public void setMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
    }

    public CartItem withMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public CartItem withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }
    public CartItem setData(String id, Integer  quan){
        this.withMenuItemId(id).withQuantity(quan);
        return this;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addons", addons).append("variants", variants).append("menuItemId", menuItemId).append("quantity", quantity).toString();
    }

}