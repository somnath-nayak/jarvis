package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;

import java.util.List;

public class TDBreakupBuilder {

    public TDBreakup tdBreakup;

    public TDBreakupBuilder() {
        tdBreakup = new TDBreakup();
    }

    public List<TradeDiscountBreakup> tradeDiscountBreakup = null;

    public TDBreakupBuilder withTradeDiscountBreakup(List<TradeDiscountBreakup> tradeDiscountBreakup) {
        tdBreakup.setTradeDiscountBreakup(tradeDiscountBreakup);
        return this;
    }

    public TDBreakup build() {
        getDefaultValue();
        return tdBreakup;
    }

    private void getDefaultValue() {

        if (tdBreakup.getTradeDiscountBreakup() == null) {
            tdBreakup.setTradeDiscountBreakup(null);
        }

    }
}
