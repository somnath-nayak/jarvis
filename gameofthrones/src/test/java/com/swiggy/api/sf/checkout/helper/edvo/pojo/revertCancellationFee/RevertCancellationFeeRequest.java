package com.swiggy.api.sf.checkout.helper.edvo.pojo.revertCancellationFee;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "order_id",
        "reason",
        "amount",
        "created_by"
})
public class RevertCancellationFeeRequest {

    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("reason")
    private String reason;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("created_by")
    private String createdBy;

    /**
     * No args constructor for use in serialization
     *
     */
    public RevertCancellationFeeRequest() {
    }

    /**
     *
     * @param amount
     * @param createdBy
     * @param reason
     * @param orderId
     */
    public RevertCancellationFeeRequest(String orderId, String reason, String amount, String createdBy) {
        super();
        this.orderId = orderId;
        this.reason = reason;
        this.amount = amount;
        this.createdBy = createdBy;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    @JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("created_by")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}