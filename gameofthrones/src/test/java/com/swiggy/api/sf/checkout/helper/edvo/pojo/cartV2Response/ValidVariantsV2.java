package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "variant_groups",
        "pricing_models"
})
public class ValidVariantsV2 {

    @JsonProperty("variant_groups")
    private List<VariantGroup> variantGroups = null;
    @JsonProperty("pricing_models")
    private List<PricingModel> pricingModels = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public ValidVariantsV2() {
    }

    /**
     *
     * @param pricingModels
     * @param variantGroups
     */
    public ValidVariantsV2(List<VariantGroup> variantGroups, List<PricingModel> pricingModels) {
        super();
        this.variantGroups = variantGroups;
        this.pricingModels = pricingModels;
    }

    @JsonProperty("variant_groups")
    public List<VariantGroup> getVariantGroups() {
        return variantGroups;
    }

    @JsonProperty("variant_groups")
    public void setVariantGroups(List<VariantGroup> variantGroups) {
        this.variantGroups = variantGroups;
    }

    @JsonProperty("pricing_models")
    public List<PricingModel> getPricingModels() {
        return pricingModels;
    }

    @JsonProperty("pricing_models")
    public void setPricingModels(List<PricingModel> pricingModels) {
        this.pricingModels = pricingModels;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("variantGroups", variantGroups).append("pricingModels", pricingModels).append("additionalProperties", additionalProperties).toString();
    }

}