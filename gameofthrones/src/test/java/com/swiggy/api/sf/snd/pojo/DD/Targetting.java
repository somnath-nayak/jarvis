package com.swiggy.api.sf.snd.pojo.DD;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class Targetting {

    @JsonProperty("relevance_group")
    private String relevanceGroup;
    @JsonProperty("relevance_value")
    private String relevanceValue;
    @JsonProperty("relevance_score")
    private Integer relevanceScore;
    private Boolean manual;

    public String getRelevanceGroup() {
        return relevanceGroup;
    }

    public void setRelevanceGroup(String relevanceGroup) {
        this.relevanceGroup = relevanceGroup;
    }

    public String getRelevanceValue() {
        return relevanceValue;
    }

    public void setRelevanceValue(String relevanceValue) {
        this.relevanceValue = relevanceValue;
    }

    public Integer getRelevanceScore() {
        return relevanceScore;
    }

    public void setRelevanceScore(Integer relevanceScore) {
        this.relevanceScore = relevanceScore;
    }

    public Boolean getManual() {
        return manual;
    }

    public void setManual(Boolean manual) {
        this.manual = manual;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("relevanceGroup", relevanceGroup).append("relevanceValue", relevanceValue).append("relevanceScore", relevanceScore).append("manual", manual).toString();
    }

}
