package com.swiggy.api.sf.checkout.helper.edvo.pojo.mealTDRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "groupId",
        "count",
        "rewards"
})
public class Group {

    @JsonProperty("groupId")
    private Integer groupId;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("rewards")
    private List<Reward> rewards = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Group() {
    }

    /**
     *
     * @param groupId
     * @param count
     * @param rewards
     */
    public Group(Integer groupId, Integer count, List<Reward> rewards) {
        super();
        this.groupId = groupId;
        this.count = count;
        this.rewards = rewards;
    }

    @JsonProperty("groupId")
    public Integer getGroupId() {
        return groupId;
    }

    @JsonProperty("groupId")
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("rewards")
    public List<Reward> getRewards() {
        return rewards;
    }

    @JsonProperty("rewards")
    public void setRewards(List<Reward> rewards) {
        this.rewards = rewards;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("groupId", groupId).append("count", count).append("rewards", rewards).toString();
    }

}