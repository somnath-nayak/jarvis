package com.swiggy.api.sf.rng.pojo.ads.Segmentation;

import com.swiggy.api.sf.rng.constants.SegmentationConstants;

public class OrderCancelMessageBuilder {
    OrderCancelMessage orderCancelMessage = new OrderCancelMessage();
    
    public OrderCancelMessageBuilder() {
        new OrderCancelMessage();
    }
    
    public OrderCancelMessageBuilder withOrderId(String orderId) {
        orderCancelMessage.setOrderId(orderId);
        return this;
    }
    
    public OrderCancelMessageBuilder withOrderStatus(String orderStatus) {
        orderCancelMessage.setStatus(orderStatus);
        return this;
    }
    
    public OrderCancelMessageBuilder withPaymentMethod(String paymentMethod) {
        orderCancelMessage.setPaymentMethod(paymentMethod);
        return this;
    }
    
    public OrderCancelMessageBuilder withCustomerId(String customerId) {
        orderCancelMessage.setCustomerId(Long.parseLong(customerId));
        return this;
    }
    
    public OrderCancelMessageBuilder withOrderType(String orderType) {
        orderCancelMessage.setOrderType(orderType);
        return this;
    }
    
    public OrderCancelMessageBuilder withSharedOrder(boolean sharedOrder) {
        orderCancelMessage.setSharedOrder(sharedOrder);
        return this;
    }
    
    public OrderCancelMessage orderCancelByCartTypeUserId(String customerId, String cartType, String orderId, String status, String paymentMethod,
                                                          boolean sharedOrder) {
        
        orderCancelMessage.setPaymentMethod(paymentMethod);
        orderCancelMessage.setSharedOrder(sharedOrder);
        orderCancelMessage.setStatus(SegmentationConstants.orderStatus[1]);
        orderCancelMessage.setCustomerId(Long.parseLong(customerId));
        orderCancelMessage.setOrderType(cartType);
        orderCancelMessage.setOrderId(orderId);
        
        return orderCancelMessage;
    }
    
    
}
