package com.swiggy.api.sf.rng.dp;

import com.swiggy.api.sf.rng.constants.RngConstants;
import org.testng.annotations.DataProvider;


public class ListingDP {

	@DataProvider(name = "listingv5fieldLevelValidatation")
	public Object[][] listingv5fieldLevelValidatation() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "0", "123", "456", true},
				{"null", RngConstants.iBCLng, "0", "123", "456", false},
				{"%20", RngConstants.iBCLng, "0", "123", "456", false},
				{RngConstants.iBCLat, "%20", "0", "123", "456", false},
//				{"0000000000.0000000000", RngConstants.iBCLng, "0", "123", "456", true},
				{RngConstants.iBCLat, "null", "0", "123", "456", false},
				{RngConstants.iBCLat, "", "0", "123", "456", false},
//				{RngConstants.iBCLat, "0000000000.000000001100000", "0", "123", "456", true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "null", "123", "456", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "Test", "123", "456", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "", "123", "456", true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "0.0", "123", "456", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "-1", "123", "456", false},

				{RngConstants.iBCLat, RngConstants.iBCLng, "50000000000000", "123", "456", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "0", "", "456", true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "0", "null", "456", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "0", "1.0", "456", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "0", "123", "null", true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "0", "123", "", true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "0", "123", "test", true}

		};
	}

	@DataProvider(name = "dwebFieldLevelValidation")
	public Object[][] dwebFieldLevelValidation() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_WEB_LISTING", true},
				{"null", RngConstants.iBCLng, "DESKTOP_WEB_LISTING", false},
				{"", RngConstants.iBCLng, "DESKTOP_WEB_LISTING", false},
				{"00000000000.0000000000", RngConstants.iBCLng, "DESKTOP_WEB_LISTING", false},
				{RngConstants.iBCLat, "null", "DESKTOP_WEB_LISTING", false},
				{RngConstants.iBCLat, "", "DESKTOP_WEB_LISTING", false},
				{RngConstants.iBCLat, "0000000000.000000001100000", "DESKTOP_WEB_LISTING", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "null", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "Test", false},
		};
	}


	@DataProvider(name = "seeAllValidation")
	public Object[][] seeAllValidation() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "4", true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "4", "4", true},
				{"null", RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "4", false},
				{"", RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING","0", "4", false},
				{"00000000000.0000000000", RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "4", true},
				{RngConstants.iBCLat, "null", "DESKTOP_SEE_ALL_LISTING", "0", "4", false},
				{RngConstants.iBCLat, "", "DESKTOP_SEE_ALL_LISTING", "0", "4", false},
				{RngConstants.iBCLat, "0000000000.000000001100000", "DESKTOP_SEE_ALL_LISTING", "0", "4", true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "null", "0", "4", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "", "0", "4", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "test", "0", "4", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "-1", "4", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "null", "4", false},
			//	{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "", "4", true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "-1", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "null", false},
			//	{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "-1", "-1", false},

		};
	}

	@DataProvider(name = "SeeAll_tidSidDeviceIdValidation")
	public Object[][] SeeAll_tidSidDeviceIdValidation() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING"},
				{RngConstants.delhiLat, RngConstants.delhiLng, "DESKTOP_SEE_ALL_LISTING"}

		};
	}


	@DataProvider(name = "seeMoreValidation")
	public Object[][] seeMoreValidation() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "2", "4", true, true, true},
				{"null", RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "0", "4", false, false, false},
				{"quot;&quot", RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "0", "4", false, false, false},
//				{"00000000000.0000000000", RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "0", "4", true, true, true},
				{RngConstants.iBCLat, "null", "DESKTOP_COLLECTION_LISTING", "32", "0", "4", false, false, false},
				{RngConstants.iBCLat, "quot;&quot", "DESKTOP_COLLECTION_LISTING", "32", "0", "4", false, false, false},
//				{RngConstants.iBCLat, "00000000000.0000000000", "DESKTOP_COLLECTION_LISTING", "32", "0", "4", true, true, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "", "32", "0", "4", true, true, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "null", "32", "0", "4", false, true, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "test", "32", "0", "4", false, true, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "", "0", "4", true, false, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "null", "0", "4", false, false, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "", "4", true, false, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "null", "4", false, false, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "0", "", true, false, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "0", "null", false, false, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "-1", "1", false, false, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "-1", "-1", false, false, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "1", "-1", false, false, true},

		};
	}


	@DataProvider(name = "SeeMore_tidSidDeviceIdValidation")
	public Object[][] SeeMore_tidSidDeviceIdValidation() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32"},
				{RngConstants.delhiLat, RngConstants.delhiLng, "DESKTOP_COLLECTION_LISTING", "32"}
		};
	}

	@DataProvider(name = "userAgent_layoutValidation")
	public Object[][] userAgent_layoutValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent},
//				{RngConstants.iBCLat, RngConstants.iBCLng, "Swiggy-iOS"},
//				{"28.4595", "77.0266", RngConstants.webUserAgent}
		};
	}

	@DataProvider(name = "carouselValidation")
	public Object[][] carouselValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.webUserAgent,"3204"},
		};
	}

	@DataProvider(name = "croutonValidation")
	public Object[][] croutonValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent, "SERVICEABLE_WITH_BANNER", true}, //{"12.8374445", "72.1725774", RngConstants.androidUserAgent, "SERVICEABLE_WITH_BANNER", false},
//				{"27.8374445", "76.1725774", RngConstants.webUserAgent, "SPECIAL", true}, {"12.8374445", "76.1725774", RngConstants.webUserAgent, "SPECIAL", false}, {"27.8374445", "76.1725774", RngConstants.webUserAgent, "RAIN", true}, {"12.8374445", "77.1725774", RngConstants.webUserAgent, "RAIN", false}
	};
	}

	@DataProvider(name = "restaurantUnserviceabilityValidtion")
	public Object[][] restaurantUnserviceabilityValidtion() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.webUserAgent},{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent},{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.iosAgent},
		};
	}

	@DataProvider(name = "FYIBanner_Validation")
	public Object[][] FYIBanner_Validation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent,"1", "260", true}, {RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.webUserAgent,"1", "260", false},
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent,"1", "10", false}, {RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent, "1","20", false}
		};
	}

	@DataProvider(name = "favoritelistingValidation")
	public Object[][] favoritelistingValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent},
		};
	}

	@DataProvider(name = "favoriteListingWithWrongtid")
	public Object[][] favoriteListingWithWrongtid() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, true},
		};
	}

	@DataProvider(name = "pageIndexValidation")
	public Object[][] pageIndexValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng,},{RngConstants.delhiLat, RngConstants.delhiLng,},
		};
	}


	@DataProvider(name = "filterValidations")
	public Object[][] filterValidations() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "SHOW_RESTAURANTS_WITH:Offers"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "SHOW_RESTAURANTS_WITH:PureVeg"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "CUISINES:NorthIndian"},

		};
	}

	@DataProvider(name = "sortValidations")
	public Object[][] sortValidations() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "RELEVANCY"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DELIVERY_TIME"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "RATING"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "COST_FOR_TWO"},

		};
	}


	@DataProvider(name = "createCollection")
	public Object[][] createCollection() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "ALL"}
		};
	}

	@DataProvider(name = "collectionValidationOnListing")
	public Object[][] collectionValidationOnListing() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent}
		};
	}

	@DataProvider(name = "collectionTypeValidationOnListing")
	public Object[][] collectionTypeValidationOnListing() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.webUserAgent, "personalized"}
		};
	}


	@DataProvider(name = "collectionValidation")
	public Object[][] collectionValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.webUserAgent}};
	}

	@DataProvider(name = "collectionRestaurantValidation")
	public Object[][] collectionRestaurantValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.webUserAgent}};
	}


	@DataProvider(name = "restaurantValidation")
	public Object[][] restaurantValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent},

		};
	}

	@DataProvider(name = "unserviceableRestaurants")
	public Object[][] unserviceableRestaurants() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent},{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.iosUserAgent}
		};
	}


	@DataProvider(name = "dwebPagination")
	public Object[][] dwebPagination() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1"}
		};
	}

	@DataProvider(name = "dwebWithFilter")
	public Object[][] dwebWithFilter() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "SHOW_RESTAURANTS_WITH:Offers"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "SHOW_RESTAURANTS_WITH:PureVeg"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "CUISINES:NorthIndian"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "SHOW_RESTAURANTS_WITH:Swiggy%2BAssured"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "SHOW_RESTAURANTS_WITH:PureVeg;Swiggy%2BAssured"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "SHOW_RESTAURANTS_WITH:Swiggy%2BAssured"},


		};
	}


	@DataProvider(name = "dwebWithSort")
	public Object[][] dwebWithSort() {
		return new Object[][]
				{


						{RngConstants.iBCLat,RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "RELEVANCY"},
						{RngConstants.iBCLat,RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "DELIVERY_TIME"},
						{RngConstants.iBCLat,RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "CUISINES:NorthIndian"},
						{RngConstants.iBCLat,RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "SHOW_RESTAURANTS_WITH:Swiggy%2BAssured"},
						{RngConstants.iBCLat,RngConstants.iBCLng, "DESKTOP_WEB_LISTING", "0", "1", "COST_FOR_TWO"}

				};
	}


	@DataProvider(name = "cacheValidation")
	public Object[][] cacheValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng,"SHOW_RESTAURANTS_WITH:Offers"}


		};
	}


	@DataProvider(name = "RestaurantDuplicatValidation")
	public Object[][] RestaurantDuplicatValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng}
//				,{RngConstants.delhiLat, RngConstants.delhiLng}


		};
	}


	@DataProvider(name = "messageCardValidation")
	public Object[][] messageCardValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng,RngConstants.androidUserAgent},{RngConstants.iBCLat, RngConstants.iBCLng,RngConstants.iosUserAgent},{RngConstants.iBCLat, RngConstants.iBCLng,RngConstants.webUserAgent}


		};
	}


	@DataProvider(name = "tIDSIDDeviceIDValidation")
	public Object[][] tIDSIDDeviceIDValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng},{RngConstants.delhiLat, RngConstants.delhiLng}


		};
	}


	@DataProvider(name = "offerListingValidation")
	public Object[][] offerListingValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "cart", "valid"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "cart", "invalid"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "account", "valid"},
				{RngConstants.iBCLat, RngConstants.iBCLng,"account", "invalid"},
				{"", RngConstants.iBCLng, "cart", "valid"},
				{"null", RngConstants.iBCLng, "cart", "valid"},
				{"test", RngConstants.iBCLng, "cart", "valid"},
				{RngConstants.iBCLat, "", "cart", "valid"},
				{RngConstants.iBCLat, "null", "cart", "valid"},
				{RngConstants.iBCLat, "test", "cart", "valid"},


		};
	}

	@DataProvider(name = "OfferListingFieldValidation")
	public Object[][] OfferListingFieldValidation() {
		return new Object[][]{
				{"", RngConstants.iBCLng, "cart"},
				{"null", RngConstants.iBCLng, "cart"},
				{"test", RngConstants.iBCLng, "cart"},
				{RngConstants.iBCLat, "", "cart"},
				{RngConstants.iBCLat, "null", "cart"},
				{RngConstants.iBCLat, "test", "cart"},
				{RngConstants.iBCLat, RngConstants.iBCLng, ""},
				{RngConstants.iBCLat, RngConstants.iBCLng, "null"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "test"},


		};
	}

	@DataProvider(name = "slugListing")
	public Object[][] slugListing() {
		return new Object[][]{

				{"null"}, {" "}, {"test"}, {"2.2"}, {"btm"}
		};

	}

	@DataProvider(name = "nuxCardValidation")
	public Object[][] nuxCardValidation() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, true},
				{RngConstants.iBCLat, RngConstants.iBCLng, false},


		};

	}


	@DataProvider(name = "popListing")
	public Object[][] popListing() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "POP_LISTING"},


		};

	}


	@DataProvider(name = "popListingValidation")
	public Object[][] popListingValidation() {
		return new Object[][]{
				{"", RngConstants.iBCLng, "POP_LISTING"},

				{"null", RngConstants.iBCLng, "POP_LISTING"},
				{"test", RngConstants.iBCLng, "POP_LISTING"},
				{RngConstants.iBCLat, "", "POP_LISTING"},
				{RngConstants.iBCLat, "null", "POP_LISTING"},
				{RngConstants.iBCLat, "test", "POP_LISTING"},
				{RngConstants.iBCLat, RngConstants.iBCLng, ""},
				{RngConstants.iBCLat, RngConstants.iBCLng, "test"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "null"},


		};

	}


	@DataProvider(name = "dishDiscoveryValidation")
	public Object[][] dishDiscoveryValidation() {
		return new Object[][]{

				{RngConstants.iBCLat, RngConstants.iBCLng},


		};

	}


	@DataProvider(name = "dishDiscoveryFieldLevelValidations")
	public Object[][] dishDiscoveryFieldLevelValidations() {
		return new Object[][]{

				{RngConstants.iBCLat, RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", "1", "127"},
				{"null", RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", "1", "127"},
				{"", RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", "1", "127"},
				{"test", RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", "1", "127"},
				{RngConstants.iBCLat, "null", "RESTAURANT_ITEM_GROUP", "1", "127"},
				{RngConstants.iBCLat, "", "RESTAURANT_ITEM_GROUP", "1", "127"},
				{RngConstants.iBCLat, "test", "RESTAURANT_ITEM_GROUP", "1", "127"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "null", "1", "127"},
				//{RngConstants.iBCLat, RngConstants.iBCLng, " ", "1", "127"},
     			{RngConstants.iBCLat, RngConstants.iBCLng, "test", "1", "127"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", "null", "127"},
				//{RngConstants.iBCLat, RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", " ", "127"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", "test", "127"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", "1", "null"},
				//{RngConstants.iBCLat, RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", "1", " "},
				{RngConstants.iBCLat, RngConstants.iBCLng, "RESTAURANT_ITEM_GROUP", "1", "test"},


		};

	}


	@DataProvider(name = "preOrderValidations")
	public Object[][] preOrderValidations() {
		return new Object[][]{

				{RngConstants.iBCLat, RngConstants.iBCLng, true},


		};
	}

	@DataProvider(name = "preOrderFieldValidations")
	public Object[][] preOrderFieldValidations() {
		return new Object[][]{

				{RngConstants.iBCLat, RngConstants.iBCLng, "1525747500000", "1525749300000"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "null", "1525749300000"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "test", "1525749300000"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "test", "1525749300000"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "1525747500000", "null"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "1525747500000", "test"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "1525747500000", "test"},
				{"test", RngConstants.iBCLng, "1525747500000", "1525749300000"},
				{"null", RngConstants.iBCLng, "1525747500000", "1525749300000"},
				{RngConstants.iBCLat, "null", "1525747500000", "1525749300000"},
				{RngConstants.iBCLat, "test", "1525747500000", "1525749300000"}


		};
	}

	@DataProvider(name = "seeAll_repeatedRestaurantValidation")
	public Object[][] seeAll_repeatedRestaurantValidation() {
		return new Object[][]{

				{RngConstants.iBCLat, RngConstants.iBCLng},


		};
	}


	@DataProvider(name = "seeMoreRestaurantRepeatValidation")
	public Object[][] seeMoreRestaurantRepeatValidation() {
		return new Object[][]{

				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.webUserAgent},


		};
	}


	@DataProvider(name = "seeMore_filterValidations")
	public Object[][] seeMore_filterValidations() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "0", "200", "SHOW_RESTAURANTS_WITH:Offers"},
		};

	}


	@DataProvider(name = "seeMoreSortValidations")
	public Object[][] seeMoreSortValidations() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "32", "0", "200", "DELIVERY_TIME"},
		};

	}



	@DataProvider(name = "seeAll_filterValidations")
	public Object[][] seeAll_filterValidations() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "4", "SHOW_RESTAURANTS_WITH:Offers"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "4", "SHOW_RESTAURANTS_WITH:PureVeg"},
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "4", "CUISINES:NorthIndian"},

		};

		}



	@DataProvider(name = "dedoopingValidation")
	public Object[][] dedoopingValidation() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng}
		};

	}


	@DataProvider(name = "seeAll_sortValidations")
	public Object[][] seeAll_sortValidations() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_SEE_ALL_LISTING", "0", "4", "RELEVANCY"},
				{RngConstants.iBCLat, RngConstants.iBCLng,"DESKTOP_SEE_ALL_LISTING", "0", "4", "DELIVERY_TIME"},
				{RngConstants.iBCLat, RngConstants.iBCLng,"DESKTOP_SEE_ALL_LISTING", "0", "4", "RATING"},
				{RngConstants.iBCLat, RngConstants.iBCLng,"DESKTOP_SEE_ALL_LISTING", "0", "4", "COST_FOR_TWO"},



		};
	}



	@DataProvider(name = "carouselRestaurantValidation")
	public Object[][] carouselRestaurantValidation() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng,RngConstants.webUserAgent,"3204"}
		};

	}


	@DataProvider(name = "collectionRestaurantOrder")
	public Object[][] collectionRestaurantOrder() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.webUserAgent}};
	}



	@DataProvider(name = "carouselAggreagatorValidation")
	public Object[][] carouselAggreagatorValidation() {

		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng,RngConstants.webUserAgent,"3204"}
		};

	}


	@DataProvider(name = "collectionPagination")
	public Object[][] collectionPagination() {
		return new Object[][]{
				{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.webUserAgent}};
	}

	@DataProvider(name="contextMatchValidation")
	public Object[][] contextMatchValidation()
	{
		return new Object[][]
				{
						{RngConstants.iBCLat, RngConstants.iBCLng, "1"}
				};
	}



	@DataProvider(name="OpenFilterRestaurantWidgetVaildation")
	public Object[][] OpenFilterRestaurantWidgetVaildation()
	{
		return new Object[][]
				{
						{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent}
				};
	}


	@DataProvider(name="OpenFilterWidgetVaildation")
	public Object[][] OpenFilterWidgetVaildation()
	{
		return new Object[][]
				{
						{RngConstants.iBCLat, RngConstants.iBCLng, RngConstants.androidUserAgent}
				};
	}



	@DataProvider(name="dwebCollectionwithFilter")
	public Object[][] dwebCollectionwithFilter()
	{
		return new Object[][]
				{
						{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "0", "2", "SHOW_RESTAURANTS_WITH:Offers"},

						{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "0", "4", "SHOW_RESTAURANTS_WITH:PureVeg"},
						{RngConstants.iBCLat, RngConstants.iBCLng, "DESKTOP_COLLECTION_LISTING", "0", "4", "CUISINES:NorthIndian"},

				};
	}


}
