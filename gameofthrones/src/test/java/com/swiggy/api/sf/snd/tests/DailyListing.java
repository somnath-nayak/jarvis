package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.pojo.AvailabilityListing;
import com.swiggy.api.daily.cms.pojo.AvailabilityMeta;
import com.swiggy.api.dash.Availability.models.Availability;
import com.swiggy.api.sf.snd.constants.DailySandConstants;
import com.swiggy.api.sf.snd.dp.DailyListingDP;
import com.swiggy.api.sf.snd.helper.DailyListingHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.pojo.Daily.DeliveryListing.DeliveryListing_Cerebro;
import com.swiggy.api.sf.snd.pojo.Daily.EnrichmentService;
import com.swiggy.api.sf.snd.pojo.Daily.SearchBySpin;
import com.swiggy.api.sf.snd.pojo.Daily.TDEnrichment.TDEnrichment;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.apache.commons.lang.ArrayUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.annotations.BeforeMethod;

import javax.xml.bind.SchemaOutputResolver;
import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.tests
 **/
public class DailyListing extends DailyListingDP {
    SANDHelper sandHelper = new SANDHelper();
    DailyListingHelper helper = new DailyListingHelper();
    DateHelper dateHelper = new DateHelper();
    RedisHelper redisHelper = new RedisHelper();
    DeliveryListing_Cerebro deliveryListing_cerebro =  new DeliveryListing_Cerebro();
    String[] serviceable_rest;
    JsonHelper jsonHelper = new JsonHelper();

    private static Logger log = LoggerFactory.getLogger(DailyListing.class);

    @BeforeTest
    public void LoginAndGetTidSid() throws InterruptedException {
        //redisHelper.flushRedisDB(DailySandConstants.sandRedis,0);
        Processor p = helper.login(DailySandConstants.mobile,DailySandConstants.password);
        int status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(status, DailySandConstants.statusZero, "Login api status is not 0");
        tid = p.ResponseValidator.GetNodeValue("$.tid").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        sid = p.ResponseValidator.GetNodeValue("$.sid").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        //Thread.sleep(2000);
        log.info("-- TID ::: "+tid);
        log.info("-- SID ::: "+sid);
    }

    @Test(dataProvider = "validateMealPlanSlotDP", groups = {"SAND", "DAILY", "LISTING", "MEAL-SLOT"}, description = "Validate Meal slot and plan slot as first call / second call, with and without offset and slots", enabled = true)
    public void validateMealPlanSlotData(String lat, String lng, String slot, String offset, String slotType) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        HashMap<String, String> hm =helper.getDefaultMealSlot(DailySandConstants.cityId,String.valueOf(dateHelper.getCurrentEpochTimeInMillis()));
        String daily_meal_slots = helper.getDailyMealSlotsHelper(DailySandConstants.cityId, String.valueOf(dateHelper.getCurrentEpochTimeInMillis())).ResponseValidator.GetBodyAsText();
        //List of array list containing slot-type, display name, start-time, end-time, day
        // Fist list entry is always default
        //TODO: identify a lat lng where there are no slots and pass in DP
        List<ArrayList<String>> al_mealslots = new ArrayList<>();
        int status_mealslots = JsonPath.read(daily_meal_slots, "$.statusCode");
        Assert.assertEquals(status_mealslots, DailySandConstants.statusOne, "Status is not one for meal_slot_Service");
        String[] slot_names = JsonPath.read(daily_meal_slots, "$.data..slot").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
        for (int i = 0; i < slot_names.length; i++) {
            String get_start_time = JsonPath.read(daily_meal_slots, "$.data.[?(@.slot=='" + slot_names[i] + "')].start_time").toString().replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String get_end_time = JsonPath.read(daily_meal_slots, "$.data.[?(@.slot=='" + slot_names[i] + "')].end_time").toString().replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            //String get_default = JsonPath.read(daily_meal_slots,"$.data.[?(@.slot=='"+slot_names[i]+"')].default").toString().replace("]","").replace("{","").replace("}","").replace("\"","");
            String get_slot_display_name = JsonPath.read(daily_meal_slots, "$.data.[?(@.slot=='" + slot_names[i] + "')].slot_display_name").toString().replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String get_day = JsonPath.read(daily_meal_slots, "$.data.[?(@.slot=='" + slot_names[i] + "')].day").toString().replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            ArrayList<String> list = new ArrayList<>();
            list.add(slot_names[i]);
            list.add(get_slot_display_name);
            list.add(get_start_time);
            list.add(get_end_time);
            list.add(get_day);
            al_mealslots.add(list);
        }
        String get_default_slot_from_meal_slots = al_mealslots.get(0).get(0);
        // Aggregator
        if(slotType.equals(DailySandConstants.slotType_meal)) {
            String meal_aggregator = helper.dailyMealAggregator(lat, lng, slot, offset,sid,tid).ResponseValidator.GetBodyAsText();
            int get_status_agg = JsonPath.read(meal_aggregator, "$.statusCode");
            if (!slot.equals("") && hm.get("slot").equals(slot)) {
                // String get_agg_slot_null = JsonPath.read(meal_aggregator, "$.data.slots").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "");
                softAssert.assertNull(JsonPath.read(meal_aggregator, "$.data.slots"), "slot not null when hitting with default slot");
            } else {
                String[] agg_slot_type = JsonPath.read(meal_aggregator, "$.data.slots[*]..type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                Assert.assertEquals(get_status_agg, DailySandConstants.statusZero, "status is not 1 in aggregator hit");
                List<ArrayList<String>> al_agg_mealslots = new ArrayList<>();
                // String[] all_time_ranges_agg = JsonPath.read(meal_aggregator,"$.data.slots[*].timeRanges..timeRangeId").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").split(",");
                for (int i = 0; i < agg_slot_type.length; i++) {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(agg_slot_type[i]);
                    String get_agg_slot_display_name = JsonPath.read(meal_aggregator, "$.data.slots[?(@.type=='" + agg_slot_type[i] + "')].slot_display_name").toString().replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String get_agg_mealslot_default = JsonPath.read(meal_aggregator, "$.data.slots[?(@.type=='" + agg_slot_type[i] + "')].default").toString().replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    list.add(get_agg_slot_display_name);
                    list.add(get_agg_mealslot_default);
                    al_agg_mealslots.add(list);
                }
                String get_default_slot_from_agg_slots = al_agg_mealslots.get(0).get(0);
                Assert.assertEquals(agg_slot_type.length, slot_names.length, "no. of slots type not same- in aggregator:" + agg_slot_type.length + "  in meal slot:" + slot_names.length);
                Assert.assertEquals(get_default_slot_from_agg_slots, get_default_slot_from_meal_slots, "get_default_slot_from_agg_slots: " + get_default_slot_from_agg_slots + "  get_default_slot_from_meal_slots:" + get_default_slot_from_meal_slots + "  not same");
                for (int i = 0; i < al_agg_mealslots.size(); i++) {
                    softAssert.assertEquals(al_agg_mealslots.get(i).get(0),al_mealslots.get(i).get(0), "slot name not same at i="+i+"  al_agg_mealslots:: "+al_agg_mealslots.get(i).get(0)+ "  al_mealslots:: "+al_mealslots.get(i).get(0));
                    softAssert.assertEquals(al_agg_mealslots.get(i).get(1),al_mealslots.get(i).get(1), "display name not same at i="+i+"  al_agg_mealslots:: "+al_agg_mealslots.get(i).get(1)+ "  al_mealslots:: "+al_mealslots.get(i).get(1));
                }
                if (!slot.equals("") && !offset.equals("")) {
                    String selected_slot = JsonPath.read(meal_aggregator,"$.data.slotsData.slotType").toString().replace("]", "").replace("{", "").replace("}", "").replace("[","");
                    softAssert.assertEquals(selected_slot,slot, "Given slot name::"+slot+ "  not similar to slotData.slotType::"+selected_slot);
                }
            } } else if (slotType.equals(DailySandConstants.slotType_plan)) {
            String plan_aggregator = helper.dailyPlanAggregator(lat, lng, slot, offset,sid,tid).ResponseValidator.GetBodyAsText();
            int get_status_agg = JsonPath.read(plan_aggregator, "$.statusCode");
            if (!slot.equals("") && hm.get("slot").equals(slot)) {
                softAssert.assertNull(JsonPath.read(plan_aggregator, "$.data.slots"), "slot not null when hitting with default slot");
            } else {
                String[] agg_slot_type = JsonPath.read(plan_aggregator, "$.data.slots[*]..type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                Assert.assertEquals(get_status_agg, DailySandConstants.statusZero, "status is not 1 in aggregator hit");
                List<ArrayList<String>> al_agg_planslots = new ArrayList<>();
                // String[] all_time_ranges_agg = JsonPath.read(meal_aggregator,"$.data.slots[*].timeRanges..timeRangeId").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").split(",");
                for (int i = 0; i < agg_slot_type.length; i++) {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(agg_slot_type[i]);
                    String get_agg_slot_display_name = JsonPath.read(plan_aggregator, "$.data.slots[?(@.type=='" + agg_slot_type[i] + "')].slot_display_name").toString().replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String get_agg_mealslot_default = JsonPath.read(plan_aggregator, "$.data.slots[?(@.type=='" + agg_slot_type[i] + "')].default").toString().replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    list.add(get_agg_slot_display_name);
                    list.add(get_agg_mealslot_default);
                    al_agg_planslots.add(list);
                }
                String get_default_slot_from_agg_slots = al_agg_planslots.get(0).get(0);
                Assert.assertEquals(agg_slot_type.length, slot_names.length, "no. of slots type not same- in aggregator:" + agg_slot_type.length + "  in meal slot:" + slot_names.length);
                Assert.assertEquals(get_default_slot_from_agg_slots, get_default_slot_from_meal_slots, "get_default_slot_from_agg_slots: " + get_default_slot_from_agg_slots + "  get_default_slot_from_meal_slots:" + get_default_slot_from_meal_slots + "  not same");
                for (int i = 0; i < al_agg_planslots.size(); i++) {
                    softAssert.assertEquals(al_agg_planslots.get(i).get(0),al_mealslots.get(i).get(0), "slot name not same at i="+i+"  al_agg_mealslots:: "+al_agg_planslots.get(i).get(0)+ "  al_mealslots:: "+al_mealslots.get(i).get(0));
                    softAssert.assertEquals(al_agg_planslots.get(i).get(1),al_mealslots.get(i).get(1), "display name not same at i="+i+"  al_agg_mealslots:: "+al_agg_planslots.get(i).get(1)+ "  al_mealslots:: "+al_mealslots.get(i).get(1));
                }
            }
        }

        softAssert.assertAll();
    }

//    @Test(groups = {"SAND", "DAILY", "LISTING", "MEAL-SLOT"}, description = "create multiple meal slots of same type", enabled = false)
//    public void validateMultipleMealSlotsOfSameType(String lat, String lng, String slot, String offset, String slotType){
//        SoftAssert softAssert = new SoftAssert();
//        if(slotType == DailySandConstants.slotType_meal) {
//            String startTime = String.valueOf(dateHelper.getCurrentTimeInHundredHours());
//            String endTime = String.valueOf(dateHelper.getMinutesPlusMinusCurrentTimeInHundredHours(240));
//            String create_new_slot1 = helper.createDailyMealSlotsHelper(DailySandConstants.cityId, "LUNCH", startTime, endTime).ResponseValidator.GetBodyAsText();
//            String create_new_slot2 = helper.createDailyMealSlotsHelper(DailySandConstants.cityId, "LUNCH", startTime, endTime).ResponseValidator.GetBodyAsText();
//            int status_slot1 = JsonPath.read(create_new_slot1, "$.statusCode");
//            int status_slot2 = JsonPath.read(create_new_slot2, "$.statusCode");
//            softAssert.assertEquals(status_slot1, DailySandConstants.statusOne, "status_slot1 not 1");
//            softAssert.assertEquals(status_slot2, DailySandConstants.statusOne, "status_slot2 not 1");
//            String meal_aggregator = helper.dailyMealAggregator(lat, lng, slot, offset).ResponseValidator.GetBodyAsText();
//            String[] get_slot_type = JsonPath.read(meal_aggregator, "$.data.slots[*].type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
//            int get_status_agg = JsonPath.read(meal_aggregator, "$.statusCode");
//            Assert.assertEquals(get_status_agg, DailySandConstants.statusOne, "status is not 1 in meal aggregator hit");
//
//        }
//    }
//
//    @Test(dataProvider = "validateMealSlotsGenericDP",groups = {"SAND", "DAILY", "LISTING", "MEAL-SLOT"}, description = "validate daily meal slot with single meal deault=0", enabled = false)
//    public void validateMealSlotWithSingleSlotDefaults(String lat, String lng, String slot, String offset, String slotType) {
//        SoftAssert softAssert = new SoftAssert();
//        if (slotType == DailySandConstants.slotType_meal) {
//            String get_default_slot_from_agg_slots = "";
//            String slot_type = "BREAKFAST";
//            String delete_existing_slots = helper.deleteMealSlotByCityId(DailySandConstants.cityId).ResponseValidator.GetBodyAsText();
//            int delete_slot_status = JsonPath.read(delete_existing_slots, "$.statusCode");
//            softAssert.assertEquals(delete_slot_status, DailySandConstants.statusOne, "delete_slot_status not 1");
//            String start_time = dateHelper.getMinutesPlusMinusCurrentTimeInHundredHours(-20);
//            String end_time = dateHelper.getMinutesPlusMinusCurrentTimeInHundredHours(240);
//            String create_new_slot = helper.createDailyMealSlotsHelper(DailySandConstants.cityId, slot_type, start_time, end_time).ResponseValidator.GetBodyAsText();
//            int create_slot_status = JsonPath.read(create_new_slot, "$.statusCode");
//            softAssert.assertEquals(create_slot_status, DailySandConstants.statusOne, "create_slot_status not 1");
//            String meal_aggregator = helper.dailyMealAggregator(lat, lng, slot, offset).ResponseValidator.GetBodyAsText();
//            String[] get_slot_type = JsonPath.read(meal_aggregator, "$.data.slots[*].type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
//            int get_status_agg = JsonPath.read(meal_aggregator, "$.statusCode");
//            List<ArrayList<String>> al_agg_mealslots = new ArrayList<>();
//            Assert.assertEquals(get_status_agg, DailySandConstants.statusOne, "status is not 1 in meal aggregator hit");
//            for (int i = 0; i < get_slot_type.length; i++) {
//                ArrayList<String> list = new ArrayList<>();
//                list.add(get_slot_type[i]);
//                String agg_mealslot_default = JsonPath.read(meal_aggregator, "$.data.slots[?(@.type=='" + get_slot_type[i] + "')].default");
//                if (agg_mealslot_default == "1")
//                    get_default_slot_from_agg_slots = get_slot_type[i];
//                list.add(agg_mealslot_default);
//                al_agg_mealslots.add(list);
//            }
//            Assert.assertEquals(get_default_slot_from_agg_slots, slot_type, "get_default_slot_from_agg_slots: " + get_default_slot_from_agg_slots + "  get_default_slot_from_meal_slots:" + slot_type + "  not same");
//
//        }
//    }

    //Since there is no city service - default is always city id 99, so these test cases will fail
    @Test(dataProvider = "validateEmptyMealPlanSlotDP",groups = {"SAND", "DAILY", "LISTING", "MEAL-SLOT"}, description = "validate meal slot and plan slot from lat lng that should have no slots", enabled = true)
    public void validateMealPlanSlotWithEmptySlots(String lat, String lng, String slot, String offset, String slotType) {
        if (slotType == DailySandConstants.slotType_meal) {
            //String deleteExistingMealSlots = helper.deleteMealSlotByCityId(DailySandConstants.cityId).ResponseValidator.GetBodyAsText();
            String meal_aggregator = helper.dailyMealAggregator(lat, lng, slot, offset,sid,tid).ResponseValidator.GetBodyAsText();
            String agg_slots_data = JsonPath.read(meal_aggregator, "$.data..slots").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            int get_status_agg = JsonPath.read(meal_aggregator, "$.statusCode");
            //int get_status_delete = JsonPath.read(deleteExistingMealSlots, "$.statusCode");
            //Assert.assertEquals(get_status_delete, DailySandConstants.statusOne, "delete api response not 1");
            Assert.assertEquals(get_status_agg, DailySandConstants.statusOne, "status is not 1 in aggregator hit");
            Assert.assertEquals(agg_slots_data, "","with no existing meals, agg_slots_data is not null");
        }
        else if (slotType == DailySandConstants.slotType_plan){
            String meal_aggregator = helper.dailyPlanAggregator(lat, lng, slot, offset,sid,tid).ResponseValidator.GetBodyAsText();
            String agg_slots_data = JsonPath.read(meal_aggregator, "$.data..slots").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            int get_status_agg = JsonPath.read(meal_aggregator, "$.statusCode");
            //int get_status_delete = JsonPath.read(deleteExistingMealSlots, "$.statusCode");
            //Assert.assertEquals(get_status_delete, DailySandConstants.statusOne, "delete api response not 1");
            Assert.assertEquals(get_status_agg, DailySandConstants.statusOne, "status is not 1 in aggregator hit");
            Assert.assertEquals(agg_slots_data,"", "with no existing meals, agg_slots_data is not null");
        }
    }

    @Test(dataProvider = "validateServiceabilityDP",groups = {"SAND", "DAILY", "LISTING", "SERVICEABILITY"}, description = "validate available items with serviceability of restaurant for lat lng and slot and logged out user", enabled = false)
    public void validateServiceabilityLoggedOutUser(String json,String lat, String lng, String slot, String offset, String slotType) {
        SoftAssert softAssert = new SoftAssert();
        log.info("JSON ---> "+json);
        Processor p = helper.deliveryListingHelper(json);
        int status_delivery = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String meal_aggregator = helper.dailyMealAggregator(lat, lng, slot, offset,sid,tid).ResponseValidator.GetBodyAsText();
        int status_meal_agg = JsonPath.read(meal_aggregator,"$.statusCode");
        Assert.assertEquals(status_delivery,DailySandConstants.statusOne, "status_delivery not 1");
        Assert.assertEquals(status_meal_agg,DailySandConstants.statusOne, "status_meal_agg not 1");
        //TODO: send data to cms availability of meals

        String[] get_agg_meal_id = JsonPath.read(meal_aggregator, "$.data.slotsData.items.[*]..mealId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
        //TODO: String[] get_cms_meal_ids from cms availability response
        String[] get_cms_meal_ids = {};
        Arrays.sort(get_agg_meal_id);
        softAssert.assertEquals(get_agg_meal_id.length,get_cms_meal_ids.length, "get_agg_meal_id len="+get_agg_meal_id.length+"  get_cms_meal_ids len="+get_cms_meal_ids.length + "  not same");
        for (int i = 0; i < get_agg_meal_id.length ; i++) {
            if(Arrays.binarySearch(get_cms_meal_ids,get_agg_meal_id[i]) >= 0)
                softAssert.assertTrue(true);
            else
                softAssert.assertTrue(false, "get_agg_meal_id: "+get_agg_meal_id+" at i="+i+" not found in get_cms_meal_ids");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "validateServiceabilityFromBlackZoneDP",groups = {"SAND", "DAILY", "LISTING", "SERVICEABILITY"}, description = "validate if there are no serviceable restaurants (hitting from blackzone", enabled = false)
    public void validateServiceabilityFromBlackZone(String startTime, String endTime, String slot, String offset, String slotType) {
        SoftAssert softAssert = new SoftAssert();
        if (startTime.equals("") || endTime.equals(""))
            Assert.assertTrue(false, "setting/updating blackzone slot time failed");
        Processor p = helper.dailyMealAggregator(DailySandConstants.blackzone_lat,DailySandConstants.blackzone_lng,slot,offset,sid,tid);
        int status_agg = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(status_agg,DailySandConstants.statusOne, "Daily Agg. hit failed");
        String get_agg_meal_id = p.ResponseValidator.GetNodeValueAsStringFromJsonArray(" $.data.slotsData.items").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        //since there is no data it should be empty after replacing all brackets
        softAssert.assertEquals(get_agg_meal_id,"", "get_agg_meal_id is not empty string when no serviceable rests.");
        softAssert.assertAll();
    }

    @Test(dataProvider = "validateDeliverySlotsDP",groups = {"SAND", "DAILY", "LISTING", "DELIVERYSLOTS"}, description = "validate delivery slots for slot", enabled = true)
    public void validateDeliverySlots(String lat, String lng, String slotName,String offset, String slotType) {
        SoftAssert softAssert = new SoftAssert();
        HashMap<String, String> hm;
        String currentEpoch = String.valueOf(dateHelper.getCurrentEpochTimeInMillis());

        if(slotName.equals("")) {
            hm = helper.getDefaultMealSlot(DailySandConstants.cityId, currentEpoch);
            Assert.assertTrue((hm.size() > 0), "Meal slot service status not one hence hashmap of default is size 0");
        } else {
            hm = helper.getSlotDetailOfGivenSlotIfExists(DailySandConstants.cityId,String.valueOf(dateHelper.getCurrentEpochTimeInMillis()),slotName);
        }
        if (hm.size() > 0) {
            Processor p = helper.getDeliverySlotsHelper(lat, lng, hm.get("start_time"), hm.get("delivery_start_time"), hm.get("end_time"));
            int status_del_slot = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(status_del_slot, DailySandConstants.statusZero, "deliverySlot status not 0");
            List<HashMap<String, Integer>> ls_del_slot = new ArrayList<>();
            String timeslotList = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.timeslotList").toString().replace("[", "").replace("]", "");
            Processor p1 = null;
            //debug
            System.out.println("timeslotList::: "+timeslotList);
            if(timeslotList.equals("")) {
                log.info("--NO Delivery Time Slot Data");
                p1 = helper.dailyMealAggregator(lat, lng, slotName, offset,sid,tid);
                String timeRanges = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.timeRanges").toString().replace("[", "").replace("]", "");
                softAssert.assertEquals(timeslotList,timeRanges , "No DeliverySLots - so data should be empty timeRanges:"+timeRanges+"   delivery timeslotList"+timeslotList );

            }
//                else if (lat.equals(DailySandConstants.blackzone_lat) && lng.equals(DailySandConstants.blackzone_lng) && !timeslotList.equals("") ){
//                    String[] get_enabled
//                }
            else {

                String[] del_enabled_start_time = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.timeslotList[?(@.enabled==1)].start_time").replace("[", "").replace("]", "").split(",");
                if (del_enabled_start_time.length <= 0) {
                    hm = helper.getNextDefaultMealSlot(DailySandConstants.cityId, currentEpoch);
                    if (hm.size() <= 0) {
                        Assert.assertTrue(false, "Next Meal slot data does not exist");
                    }
                    p = helper.getDeliverySlotsHelper(lat, lng, hm.get("start_time"), hm.get("delivery_start_time"), hm.get("end_time"));
                }
                String[] get_del_start_time = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.timeslotList[*]..start_time").toString().replace("[", "").replace("]", "").split(",");
                String[] get_del_end_time = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.timeslotList[*]..end_time").toString().replace("[", "").replace("]", "").split(",");


                //get all delivery slot data
                for (int i = 0; i < get_del_start_time.length; i++) {
                    HashMap<String, Integer> hm_del_slots = new HashMap<>();
                    String get_del_end = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.timeslotList[?(@.start_time==" + Integer.parseInt(get_del_start_time[i]) + ")].end_time").toString().replace("[", "").replace("]", "");
                    String get_del_enabled = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.timeslotList[?(@.start_time==" + Integer.parseInt(get_del_start_time[i]) + ")].enabled").toString().replace("[", "").replace("]", "");
                    hm_del_slots.put("start_time", Integer.parseInt(get_del_start_time[i]));
                    hm_del_slots.put("end_time", Integer.parseInt(get_del_end));
                    hm_del_slots.put("enabled", Integer.parseInt(get_del_enabled));
                    ls_del_slot.add(hm_del_slots);
                }
//                String[] get_del_end_time = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.timeslotList[*]..end_time").toString().replace("[", "").replace("]", "").split(",");
//                String[] get_del_enabled = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.timeslotList[*]..enabled").toString().replace("[", "").replace("]", "").split(",");
                p1 = helper.dailyMealAggregator(lat, lng, slotName, offset,sid,tid);
                int get_meal_Agg_status = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(get_meal_Agg_status, DailySandConstants.statusZero, "Meal Listing call failed, Status is not 0");
                //if there are enabled slots in delivery slots response - enable=1
                String[] del_enabled_end_time = new String[del_enabled_start_time.length];
                del_enabled_end_time = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.timeslotList[?(@.enabled==1)].end_time").replace("[", "").replace("]", "").split(",");
                System.out.println("----del_enabled_end_time "+Arrays.asList(del_enabled_end_time));
                System.out.println("-----del_enabled_start_time "+Arrays.asList(del_enabled_start_time));
                int[] del_start_time = Arrays.stream(del_enabled_start_time).mapToInt(Integer::parseInt).toArray();
                int[] del_end_time = Arrays.stream(del_enabled_end_time).mapToInt(Integer::parseInt).toArray();
                Arrays.sort(del_start_time);
                Arrays.sort(del_end_time);
                //aggregator delivery slots - active and default
                String agg_del_active_default_start_time = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.timeRanges[?(@.active==true && @.default==true)].startTime").replace("[", "").replace("]", "");
                String agg_del_active_default_end_time = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.timeRanges[?(@.active==true && @.default==true)].endTime").replace("[", "").replace("]", "");
                //validate default active slot
                softAssert.assertEquals(del_start_time[0], Integer.parseInt(agg_del_active_default_start_time), "Sorted Delivery SLot start time: Default is the first one which is active - in this case default does not match the active sorted delivery slot. Delivery Slot (sorted): " + del_start_time[0] + "   TimeRanges Default: " + agg_del_active_default_start_time);
                softAssert.assertEquals(del_end_time[0], Integer.parseInt(agg_del_active_default_end_time), "Sorted Delivery SLot end time: Default is the first one which is active - in this case default does not match the active sorted delivery slot. Delivery Slot (sorted): " + del_end_time[0] + "   TimeRanges Default: " + agg_del_active_default_end_time);

                String[] agg_del_slot_start_time = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.timeRanges[*].startTime").replace("[", "").replace("]", "").split(",");
                String[] agg_del_slot_end_time = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.timeRanges[*].endTime").replace("[", "").replace("]", "").split(",");
                int[] agg_start_time = Arrays.stream(agg_del_slot_start_time).mapToInt(Integer::parseInt).toArray();
                int[] agg_end_time = Arrays.stream(agg_del_slot_end_time).mapToInt(Integer::parseInt).toArray();
                softAssert.assertEquals(get_del_start_time.length, agg_start_time.length, "Delivery slot length:" + get_del_start_time.length + "Delivery SLot from listing length:" + agg_start_time.length + " are not same");
                if (get_del_start_time.length == agg_start_time.length) {
                    int[] all_del_start_time = Arrays.stream(get_del_start_time).mapToInt(Integer::parseInt).toArray();
                    int[] all_del_end_time = Arrays.stream(get_del_end_time).mapToInt(Integer::parseInt).toArray();
                    Arrays.sort(all_del_start_time);
                    Arrays.sort(all_del_end_time);
                    for (int i = 0; i < agg_start_time.length; i++) {
                        softAssert.assertEquals(all_del_start_time[i], agg_start_time[i], "all_del_start_time[i]:" + all_del_start_time[i] + "   agg_start_time[i]:" + agg_start_time[i] + "  not same at i=" + i);
                        softAssert.assertEquals(all_del_end_time[i], agg_end_time[i], "all_del_end_time[i]:" + all_del_end_time[i] + "   agg_end_time[i]:" + agg_end_time[i] + "  not same at i=" + i);
                    }
                }
            }
        }
        else {
            Assert.assertTrue(false, "GIven slot details do not exist");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "validatePaginationDP",groups = {"SAND", "DAILY", "LISTING", "PAGINATION"}, description = "Validate Pagination from Redis", enabled = true)
    public void validatePaginationFromRedis(String lat, String lng, String slotName, String offset, String slotType, boolean isFirstCall) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        String get_redis_data;
        HashMap<String, String> hm_slot;
        List<String> get_redis_Ids = new ArrayList<>();
        List<String> get_agg_Ids = new ArrayList<>();
        //ArrayList<String> al_redis_mealIds;
        Processor p = null;
        HashMap<String, List<String>> hm_filter = new HashMap<>();
        HashMap<String, String> hm_sort = new HashMap<>();
        String[] consul_props = helper.getPaginationConfigFromConsul(DailySandConstants.userAgent);
        // Thread.sleep(1000);
        if (helper.isSlotNameValid(slotName)) {
            if (slotType.equals(DailySandConstants.slotType_meal))
                p = helper.dailyMealAggregator(lat, lng, slotName, "0", sid, tid);
            else if (slotType.equals(DailySandConstants.slotType_plan))
                p = helper.dailyPlanAggregator(lat, lng, slotName, "0", sid, tid);
            else
                Assert.assertTrue(false, "Incorrect MEAL/PLAN slotType provided ::: " + slotType);
            Thread.sleep(3000);
            int status_agg = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            // Assert.assertEquals(status_agg,DailySandConstants.statusZero, "Listing status not 0");
            int currentOffset;
            if (offset.equals(""))
                currentOffset = 0;
            else
                currentOffset = Integer.parseInt(offset);
            String agg_total_offset;
            if (isFirstCall) {
                //agg_total_offset = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.totalOffset").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                String default_sorting = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.sort[?(@.key=='SCORE')].options[?(@.option=='High To Low')].selected").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                Assert.assertEquals(status_agg, DailySandConstants.statusZero, "status_listing not equal to 0");
                Assert.assertTrue(Boolean.parseBoolean(default_sorting), "Default sorting - SCORE: High To Low is not true");
            }
            //String[] get_agg_storeIds = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.slotsData.items[*]..storeId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
            //String[] get_agg_discounted_price = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.slotsData.items[*]..discountedPrice").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
            Arrays.sort(DailySandConstants.SlotKeys);
            if (slotName.equals("") || helper.isSlotNameValid(slotName)) {
                hm_slot = helper.getDefaultMealSlot(DailySandConstants.cityId, String.valueOf(dateHelper.getCurrentEpochTimeInMillis()));
                slotName = hm_slot.get("slot");
            }
            String get_redis_key_meal = helper.redisKeyGenerator(tid, slotType, slotName, lat, lng, hm_filter, hm_sort);
            agg_total_offset = String.valueOf(helper.totalLengthOfRedisDataInPagination( get_redis_key_meal,Integer.parseInt(consul_props[0])));

            // though this gives start and end - but we need only start redis block from this method
            ArrayList<Integer> al_redis_block_ids = helper.identifyRedisBlockId(offset, get_redis_key_meal);
            //put the last block as per the length of data in redis
            int endBlock = Math.floorDiv(Integer.parseInt(agg_total_offset), Integer.parseInt(consul_props[0]));
            if (al_redis_block_ids.size() > 0) {
                int startBlock = al_redis_block_ids.get(0);
                log.info("-- START BLOCK ::: "+startBlock);
                log.info("-- END BLOCK ::: "+endBlock);
                if (slotType.equals(DailySandConstants.slotType_meal)) {
                    while (currentOffset < Integer.parseInt(agg_total_offset)) {
                        log.info("-- Current Offset ::: " + currentOffset);
                        p = helper.dailyMealAggregator(lat, lng, slotName, String.valueOf(currentOffset), sid, tid);
                        currentOffset = helper.getNextOffset(String.valueOf(currentOffset), agg_total_offset, consul_props[1]);
                        log.info("-- Next Offset ::: " + currentOffset);
                        String[] get_agg_mealIds = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.slotsData.items[*]..mealId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                        get_agg_Ids.addAll(Arrays.asList(get_agg_mealIds));
                    }
                    if (startBlock == endBlock) {
                        get_redis_data = redisHelper.getHashValue(DailySandConstants.sandRedis, 0, get_redis_key_meal, String.valueOf(al_redis_block_ids.get(0)));
                        get_redis_Ids = Arrays.asList(JsonPath.read(get_redis_data, "$.records[*].mealId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(","));

                    } else {
                        for (int i = startBlock; i <= endBlock; i++) {
                            String temp = redisHelper.getHashValue(DailySandConstants.sandRedis, 0, get_redis_key_meal, String.valueOf(i));
                            String[] temp_get_redis_mealIds = JsonPath.read(temp, "$.records[*].mealId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                            get_redis_Ids.addAll(Arrays.asList(temp_get_redis_mealIds));
                        }
                    }
                    log.info("--REDIS MEAL IDs Arraylist [List Size:"+get_redis_Ids.size()+" ] :::" + get_redis_Ids);
                    Collections.sort(get_redis_Ids);
                    Thread.sleep(1000);
                    log.info("--Size of Meal Ids from Listing ::: "+get_agg_Ids.size());
                    for (String meal_id : get_agg_Ids) {
                        log.info("-- MEAL ID:" + meal_id);
                        if (!meal_id.equals(" ") && !meal_id.equals(""))

                            softAssert.assertTrue((Collections.binarySearch(get_redis_Ids, meal_id) >= 0), "-- MEAL ID:" + meal_id + "  from listing not found in get_redis_mealIds");
                    }
                }
                else if (slotType.equals(DailySandConstants.slotType_plan)) {
                    while (currentOffset < Integer.parseInt(agg_total_offset)) {
                        log.info("-- Current Offset ::: " + currentOffset);
                        p = helper.dailyPlanAggregator(lat, lng, slotName, String.valueOf(currentOffset), sid, tid);
                        currentOffset = helper.getNextOffset(String.valueOf(currentOffset), agg_total_offset, consul_props[1]);
                        log.info("-- Next Offset ::: " + currentOffset);
                        String[] get_agg_planIds = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.slotsData.items[*]..planId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                        get_agg_Ids.addAll(Arrays.asList(get_agg_planIds));
                    }
                    if (startBlock == endBlock) {
                        get_redis_data = redisHelper.getHashValue(DailySandConstants.sandRedis, 0, get_redis_key_meal, String.valueOf(al_redis_block_ids.get(0)));
                        get_redis_Ids = Arrays.asList(JsonPath.read(get_redis_data, "$.records[*].planId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(","));

                    } else {
                        for (int i = startBlock; i <= endBlock; i++) {
                            String temp = redisHelper.getHashValue(DailySandConstants.sandRedis, 0, get_redis_key_meal, String.valueOf(i));
                            String[] temp_get_redis_planIds = JsonPath.read(temp, "$.records[*].planId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                            get_redis_Ids.addAll(Arrays.asList(temp_get_redis_planIds));
                        }
                    }
                    log.info("--REDIS PLAN IDs Arraylist [ List Size:"+get_redis_Ids.size()+" ] :::" + get_redis_Ids);
                    Collections.sort(get_redis_Ids);
                    //Thread.sleep(1000);
                    log.info("--Size of Plan Ids from Listing ::: "+get_agg_Ids.size());
                    for (String plan_id : get_agg_Ids) {
                        log.info("-- PLAN ID:" + plan_id);
                        if (!plan_id.equals(" ") && !plan_id.equals(""))
                            //Thread.sleep(1000);
                            softAssert.assertTrue((Collections.binarySearch(get_redis_Ids, plan_id) >= 0), "-- PLAN ID:" + plan_id + "  from listing not found in get_redis_Ids");
                    }
                }
            }
            else {
                if (slotType.equals(DailySandConstants.slotType_meal))
                    p = helper.dailyMealAggregator(lat, lng, slotName, offset, sid, tid);
                else if (slotType.equals(DailySandConstants.slotType_plan))
                    p = helper.dailyPlanAggregator(lat, lng, slotName, offset, sid, tid);
                String get_agg_mealIds = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.slotsData.items").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                get_redis_data = "";
                softAssert.assertEquals(get_agg_mealIds, get_redis_data, "Since offset is more than redis data, itemId should be empty. This assert shows items are not empty");
            }
        } else {
            if (slotType.equals(DailySandConstants.slotType_meal))
                p = helper.dailyMealAggregator(lat, lng, slotName, offset,sid,tid);
            else if (slotType.equals(DailySandConstants.slotType_plan))
                p = helper.dailyPlanAggregator(lat, lng, slotName, offset,sid,tid);
            int status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(status,DailySandConstants.statusOne, "Invalid slotName - status should be 1");

        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "validateFilterSortDP",groups = {"SAND", "DAILY", "LISTING", "FILTER-SORTING"}, description = "Validate Filtering and Sorting from Redis", enabled = true)
    public void validateSortAndFilter(String lat, String lng, String slotType, String filter_sort, HashMap<String, List<String>>hm_filter, HashMap<String, String> hm_sort) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        Processor p = null;
        HashMap<String, String> param_hm = new HashMap<>();
        if(filter_sort.equals("FILTER") || filter_sort.equals("FILTER-SORT")) {
            param_hm.put("filters",helper.optionalFilterParamGenerator(hm_filter));
        }
        if (filter_sort.equals("SORT") || filter_sort.equals("FILTER-SORT")) {
            param_hm.put("sorts",helper.optionalSortParamGenerator(hm_sort));
        }

        if (slotType.equals(DailySandConstants.slotType_meal)){
            p = helper.dailyMealAggregatorWithOptionalParams(lat,lng,param_hm,sid,tid);
        } else {
            p = helper.dailyPlanAggregatorWithOptionalParams(lat,lng,param_hm,sid,tid);
        }
        // Thread.sleep(3000);
        if (filter_sort.equals("FILTER") || filter_sort.equals("FILTER-SORT")) {
            for (String filterKey:hm_filter.keySet()) {
                List<String> filter_ls = hm_filter.get(filterKey);
                for (String filterVal: filter_ls) {
                    String filter_selected = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.filters[?(@.key=='"+filterKey+"')].options[?(@.option=='"+filterVal+"')].selected").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    softAssert.assertTrue(Boolean.parseBoolean(filter_selected),"FILTER KEY: "+filterKey+"   FILTER VALUE: "+filterVal+"  should be true since its applied.");
                }
                if (filterKey.equals("VENDOR")) {
                    String[] vendor_filter = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[*].vendorName").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    // list of vendors that are in response but filter but not applied in filter
                    List<String> vendors_not_in_filters = helper.validateKeysEquatesAllValuesInArray(vendor_filter,(String[])filter_ls.toArray());
                    softAssert.assertTrue((vendors_not_in_filters.size() == 0), "--Following vendor names are beyond filters applied ::: "+vendors_not_in_filters);
                }
                else if (filterKey.equals("CUISINE")) {
                    String[] cuisine_filter = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[*].cuisine").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    List<String> cuisine_not_in_filters = helper.validateKeysEquatesAllValuesInArray(cuisine_filter,(String[])filter_ls.toArray());
                    softAssert.assertTrue((cuisine_not_in_filters.size() == 0), "--Following cuisines are beyond filters applied ::: "+cuisine_not_in_filters);
                }
            }
        }

        if (filter_sort.equals("SORT") || filter_sort.equals("FILTER-SORT")) {
            for (String sortKey: hm_sort.keySet()) {
                String sortVal = hm_sort.get(sortKey);
                String sort_selected = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.sort[?(@.key=='"+sortKey+"')].options[?(@.option=='"+sortVal+"')].selected").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                softAssert.assertTrue(Boolean.parseBoolean(sort_selected),"--SORT KEY: "+sortKey+"   SORT VALUE: "+sortVal+"  should be true since its applied.");

                if(sortKey.equals("PRICE") && slotType.equals(DailySandConstants.slotType_meal)) {
                    String[] get_discounted_price = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.slotsData.items[*].priceDetails.discountedPrice").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    softAssert.assertTrue(helper.isSorted(get_discounted_price,sortVal), "--For SORT KEY: "+sortKey+"  and SORT VALUE:"+sortVal+"  the data is not sorted as per the filter in MEAL. Data in response:"+Arrays.asList(get_discounted_price));
                }
                else if (sortKey.equals("PRICE") && slotType.equals(DailySandConstants.slotType_plan)) {
                    String[] get_discounted_price = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.slotsData.items[*].priceDetails[?(@.numberOfMeals=="+DailySandConstants.min_number_of_meals_for_plan+")].discountedPrice").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    softAssert.assertTrue(helper.isSorted(get_discounted_price,sortVal), "--For SORT KEY: "+sortKey+"  and SORT VALUE:"+sortVal+"  the data is not sorted as per the filter. Data in response:"+Arrays.asList(get_discounted_price));                }

                else if (sortKey.equals("RATING")) {
                    String[] get_ratings = p.ResponseValidator.GetNodeValueAsJsonArray("$.data.slotsData.items[*].ratings").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    softAssert.assertTrue(helper.isSorted(get_ratings,sortVal), "--For SORT KEY: "+sortKey+"  and SORT VALUE:"+sortVal+"  the data is not sorted as per the filter in PLAN. Data in response:"+Arrays.asList(get_ratings));
                }
            }
        }
    }

    @Test(dataProvider = "validateSubscriptionDP",groups = {"SAND", "DAILY", "LISTING", "SUBSCRIPTION"}, description = "Validate Current Subscription data on listing", enabled = true)
    public void validateSubscriptionWithMeta(String lat, String lng, String slotName, String offset, String customerId, String sid, String tid, String cityId) throws IOException, InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        List<AvailabilityMeta> ls_avail_meta;
        List<AvailabilityListing> ls_avail_list;
        String currentTime =  String.valueOf(dateHelper.getCurrentEpochTimeInMillis());
        String[] serviceable_rest;
        JsonHelper jsonHelper = new JsonHelper();
        DeliveryListing_Cerebro deliveryListing_cerebro =  new DeliveryListing_Cerebro();
        HashMap<String,String> hm = helper.getSlotDetailOfGivenSlotIfExists(DailySandConstants.cityId,currentTime,slotName);
        if (hm.size() == 0) {
            log.info("-- SENT SLOT KEY:"+slotName+" does not exist in meal slot service response, hence plan listing will return status=1");
            Processor p1 = helper.dailyPlanAggregator(lat, lng, slotName, offset, sid, tid);
            int status_plan_listing = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(status_plan_listing, DailySandConstants.statusOne, "Invalid slot is passed - status for PlanListing API should be 1");
        }
        else {
            String startTimeEpoch = dateHelper.convertCurrentDateTimeInHHMMToEpochMillis(hm.get("start_time"));
            String endTimeEpoch = dateHelper.convertCurrentDateTimeInHHMMToEpochMillis(hm.get("end_time"));
            Processor p = helper.dailySubscriptionListingHelper(customerId, startTimeEpoch, endTimeEpoch);
            int subscription_listing_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscription_listing_status, DailySandConstants.statusOne, "Status for subscription listing is not 1");
            HashMap<String, HashMap<String, String>> hm_hm = new HashMap<>();
            String check_subscription = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            //hit plan meta
            String startTime_epoch = dateHelper.convertCurrentDateTimeIndd_mm_yyyy_hh_mmToEpoch(hm.get("slot_date"), hm.get("start_time"));
            String endTime_epoch = dateHelper.convertCurrentDateTimeIndd_mm_yyyy_hh_mmToEpoch(hm.get("slot_date"), hm.get("end_time"));
            deliveryListing_cerebro.build(startTime_epoch, endTime_epoch, lat, lng, cityId);
            deliveryListing_cerebro.setPack_size(5);
            Processor p_cerebro = helper.deliveryListingHelper(jsonHelper.getObjectToJSON(deliveryListing_cerebro));
            serviceable_rest = helper.getServiceableRestaurantsArray(p_cerebro.ResponseValidator.GetBodyAsText());
            if (serviceable_rest.length != 0) {
                //serviceable_rest = p_cerebro.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.serviceableRestaurants[?(@.listingServiceabilityResponse.serviceability==2)].restaurantId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("--List of Serviceable Restaurants: "+Arrays.asList(serviceable_rest));

            } else {
                serviceable_rest = new String[0];
                log.info(" -- No Serviceable Restaurants found in serviceability response");
            }
            AvailabilityMeta availabilityMeta = new AvailabilityMeta();
            //AvailabilityListing availabilityListing =  new AvailabilityListing();
            //hit availability listing and then hit availability meta
            ls_avail_meta = new ArrayList<>();
            ls_avail_list =  new ArrayList<>();
            for (String store_id : serviceable_rest) {
                Integer st_id = Integer.parseInt(store_id);
                AvailabilityListing availabilityListing = new AvailabilityListing();
                availabilityListing.build(st_id);
                ls_avail_list.add(availabilityListing);
            }
            // store id and product id and variations
            SearchBySpin searchBySpin = new SearchBySpin();
            Processor p_avail_plan_listing = helper.availabilityPlanListingHelper(jsonHelper.getObjectToJSON(ls_avail_list),DailySandConstants.cms_listing_include_only,hm.get("slot_date"),hm.get("start_time"),hm.get("end_time"),DailySandConstants.cms_base_variant);
            String[] store_ids_avail_plan = p_avail_plan_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[*].store_id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
            String[] get_store_ids_avail_plan = helper.returnArrayWithUniqueData(store_ids_avail_plan);
            for (String storeId_plan_list : get_store_ids_avail_plan) {
                String[] get_product_ids_avail_plan = p_avail_plan_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.store_id==" + storeId_plan_list + ")].product_id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                HashMap<String, List<String>> temp_hm_ls = new HashMap<>();
                for (String productId : get_product_ids_avail_plan) {
                    log.info("--Plan Id in Plan Avail Listing ::: " + productId);
                    String[] get_product_variations = p_avail_plan_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[*].id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    List<String> temp_ls_variations = Arrays.asList(get_product_variations);
                    temp_hm_ls.put(productId, temp_ls_variations);
                }
                //for meal - meta pojo data
                availabilityMeta = new AvailabilityMeta();
                availabilityMeta.build(storeId_plan_list, Arrays.asList(get_product_ids_avail_plan));
                ls_avail_meta.add(availabilityMeta);
            }
            Processor p_avail_plan_meta = helper.availabilityMealMetaHelper(jsonHelper.getObjectToJSON(ls_avail_meta), DailySandConstants.cms_meta_include_only);
            Processor p_avail_meal_meta = helper.availabilityMealMetaHelper(jsonHelper.getObjectToJSON(ls_avail_meta), DailySandConstants.cms_meta_include_only);

            if (check_subscription.equals("")) {
                Processor p1 = helper.dailyPlanAggregator(lat, lng, slotName, offset,sid,tid);
                int status_plan_listing = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(status_plan_listing, DailySandConstants.statusZero, "Status for plan listing is not 0");
                String checkCurrentSubscription_agg = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                softAssert.assertEquals(checkCurrentSubscription_agg, check_subscription, "listing currentSubscription is \"\" but subcriptionListing API:" + check_subscription + " is not same");

            } else {
                String[] getSubscriptionIds = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[*].subscriptionId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                log.info("--List of SubscriptionIds from subscriptionListingAPI ::: " + Arrays.asList(getSubscriptionIds));
                for (String sub_id : getSubscriptionIds) {
                    List<SearchBySpin> ls_search_sku = new ArrayList<>();
                    HashMap<String, String> hm_meal_meta = new HashMap<>();
                    HashMap<String,String> hm_plan_meta =  new HashMap<>();
                    HashMap<String,String> hm_spin_meta_searchSku = new HashMap<>();
                    String storeId = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].planDetails.vendorId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String startDate = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].startDate").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String endDate = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].endDate").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String nextMealDate = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].nextMealDate").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String remainingMeals = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].remainingMeals").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String planId = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].planDetails.planId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String subscriptionStatus = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].subscriptionStatus").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String delivery_start_time = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].deliverySlot.delivery_start_time").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String delivery_end_time = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].deliverySlot.delivery_end_time").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    //String nextMealDetails_subscriptionMealId = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId=="+sub_id+")].nextMealDetails.subscriptionMealId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String nextMealDetails_mealId = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].nextMealDetails.mealId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String nextMealDetails_addOnCount = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.subscriptions[?(@.subscriptionId==" + sub_id + ")].nextMealDetails.addons[0].quantity").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    if (nextMealDetails_addOnCount.equals(""))
                        nextMealDetails_addOnCount = "0";
                    String planMetaJsonString = p_avail_plan_meta.ResponseValidator.GetBodyAsText();
                    String mealMetaJsonString = p_avail_meal_meta.ResponseValidator.GetBodyAsText();
                    log.info("-- VendorId:::"+storeId+"  PlanId:::"+planId+"  nextMealId:::"+nextMealDetails_mealId);
                    hm_plan_meta = helper.getMetaDetailsForAGivenSpin(planMetaJsonString,planId);
                    hm_meal_meta = helper.getMetaDetailsForAGivenSpin(mealMetaJsonString,nextMealDetails_mealId);
                    searchBySpin = new SearchBySpin();
                    searchBySpin.build(storeId,Arrays.asList(nextMealDetails_mealId));
                    ls_search_sku.add(searchBySpin);
                    Processor p_search_sku = helper.searchBySpinIdHelper(jsonHelper.getObjectToJSON(ls_search_sku));
                    String spinMeta_searchsku = p_search_sku.ResponseValidator.GetBodyAsText();
                    hm_spin_meta_searchSku = helper.getSpinMetaFromSearchSKU(spinMeta_searchsku,nextMealDetails_mealId);
                    HashMap<String, String> temp_hm = new HashMap<>();
                    temp_hm.put("subscriptionId", sub_id);
                    temp_hm.put("startDate", startDate);
                    temp_hm.put("endDate", endDate);
                    temp_hm.put("nextMealDate", nextMealDate);
                    temp_hm.put("remainingMeals", remainingMeals);
                    temp_hm.put("planId", planId);
                    temp_hm.put("name",hm_plan_meta.get("display_name"));
                    //temp_hm.put("veg",hm_plan_meta.get("is_veg"));
                    temp_hm.put("subscriptionStatus", subscriptionStatus);
                    temp_hm.put("delivery_start_time", delivery_start_time);
                    temp_hm.put("delivery_end_time", delivery_end_time);
                    //temp_hm.put("subscriptionMealId",nextMealDetails_subscriptionMealId);
                    temp_hm.put("nextMeal_mealId", nextMealDetails_mealId);
                    temp_hm.put("nextMeal_title",hm_spin_meta_searchSku.get("display_name"));
                    temp_hm.put("nextMeal_veg",hm_spin_meta_searchSku.get("is_veg"));
                    temp_hm.put("nextMeal_Images",hm_spin_meta_searchSku.get("images"));
                    temp_hm.put("addon_quantity", nextMealDetails_addOnCount);

                    hm_hm.put(sub_id, temp_hm);
                }
                Thread.sleep(1000);
                Processor p1 = helper.dailyPlanAggregator(lat, lng, slotName, offset,sid, tid);
                int status_plan_listing = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(status_plan_listing, DailySandConstants.statusZero, "Status for plan listing is not 0");
                String[] agg_subcription_ids = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[*].subscriptionId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                log.info("--List of SubscriptionIds from PlanListingAPI ::: " + Arrays.asList(agg_subcription_ids));
                softAssert.assertEquals(agg_subcription_ids.length, getSubscriptionIds.length, "subscription-listing: no. of subscription-" + getSubscriptionIds.length + "  plan-listing: no. of subscription-" + agg_subcription_ids.length);
                // Integer[] agg_subscriptionIds_to_Int = Stream.of(agg_subcription_ids).map(Integer::valueOf).toArray(Integer[]::new);
                for (String sub_id: getSubscriptionIds) {
                    //int sub_int = Integer.parseInt(sub_id);
                    softAssert.assertTrue(ArrayUtils.contains(agg_subcription_ids,sub_id), "SubscriptionID from subscriptionListingAPI: "+sub_id+"   not found in planListingAPI response");
                }
                HashMap<String, HashMap<String, String>> agg_hm_hm = new HashMap<>();

                for (String agg_sub_id : agg_subcription_ids) {
                    String startDate = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].startTime").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String endDate = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].endTime").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String nextMealDate = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].upcomingMeal.mealDate").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String remainingMeals = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].remainingMeals").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String planId = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].planId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String name = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].name").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String veg = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].upcomingMeal.veg").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String subscriptionStatus = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].currentStatus").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String delivery_start_time = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].deliverySlot.startTime").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String delivery_end_time = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].deliverySlot.endTime").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    //String nextMealDetails_subscriptionMealId = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId=="+agg_sub_id+")].planId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String nextMealDetails_mealId = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].upcomingMeal.mealId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String nextMealDetails_title = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].upcomingMeal.title").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String nextMealDetails_veg = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].upcomingMeal.veg").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String nextMealDetails_imageUrl = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].upcomingMeal.imageUrl").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String nextMealDetails_addOnCount = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.currentSubscriptions[?(@.subscriptionId==" + agg_sub_id + ")].upcomingMeal.addOnCount").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    HashMap<String, String> temp_hm = new HashMap<>();
                    temp_hm.put("subscriptionId", agg_sub_id);
                    temp_hm.put("startDate", startDate);
                    temp_hm.put("endDate", endDate);
                    temp_hm.put("nextMealDate", nextMealDate);
                    temp_hm.put("remainingMeals", remainingMeals);
                    temp_hm.put("planId", planId);
                    temp_hm.put("name",name);
                    temp_hm.put("veg",veg);
                    temp_hm.put("subscriptionStatus", subscriptionStatus);
                    temp_hm.put("delivery_start_time", delivery_start_time);
                    temp_hm.put("delivery_end_time", delivery_end_time);
                    //temp_hm.put("subscriptionMealId",nextMealDetails_subscriptionMealId);
                    temp_hm.put("nextMeal_mealId", nextMealDetails_mealId);
                    temp_hm.put("nextMealDetails_title",nextMealDetails_title);
                    temp_hm.put("nextMealDetails_veg",nextMealDetails_veg);
                    temp_hm.put("nextMealDetails_imageUrl",nextMealDetails_imageUrl);
                    temp_hm.put("addon_quantity", nextMealDetails_addOnCount);
                    agg_hm_hm.put(agg_sub_id, temp_hm);
                }

                for (String subscription_id : hm_hm.keySet()) {
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("subscriptionId"), agg_hm_hm.get(subscription_id).get("subscriptionId"), "subscription id not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("addon_quantity"), agg_hm_hm.get(subscription_id).get("addon_quantity"), "addon_quantity not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("nextMeal_mealId"), agg_hm_hm.get(subscription_id).get("nextMeal_mealId"), "nextMeal_mealId not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("delivery_start_time"), agg_hm_hm.get(subscription_id).get("delivery_start_time"), "delivery_start_time not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("delivery_end_time"), agg_hm_hm.get(subscription_id).get("delivery_end_time"), "delivery_end_time not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("subscriptionStatus"), agg_hm_hm.get(subscription_id).get("subscriptionStatus"), "subscriptionStatus not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("planId"), agg_hm_hm.get(subscription_id).get("planId"), "planId not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("remainingMeals"), agg_hm_hm.get(subscription_id).get("remainingMeals"), "remainingMeals not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("startDate"), agg_hm_hm.get(subscription_id).get("startDate"), "startTime not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("endDate"), agg_hm_hm.get(subscription_id).get("endDate"), "endTime not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("nextMealDate"), agg_hm_hm.get(subscription_id).get("nextMealDate"), "endTime not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("nextMeal_title"), agg_hm_hm.get(subscription_id).get("nextMealDetails_title"), "nextMealDetails_title not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("nextMeal_veg"), agg_hm_hm.get(subscription_id).get("nextMealDetails_veg"), "nextMealDetails_veg not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("nextMeal_Images"), agg_hm_hm.get(subscription_id).get("nextMealDetails_imageUrl"), "nextMealDetails_imageUrl not same for subscription:" + subscription_id);
                    softAssert.assertEquals(hm_hm.get(subscription_id).get("name"), agg_hm_hm.get(subscription_id).get("name"), "name not same for subscription:" + subscription_id);
                    //softAssert.assertEquals(hm_hm.get(subscription_id).get("veg"), agg_hm_hm.get(subscription_id).get("veg"), "veg not same for subscription:" + subscription_id);
                }
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "validateAvailabilityDP",groups = {"SAND", "DAILY", "LISTING", "AVAILABILITY", "SERVICEABILITY","CMS-META","ENRICHMENT"}, description = "Validate Serviceability, Availability data & CMS-Meta Enrichment in listing", enabled = true)
    public void validateServiceabilityAvailabilityAndCMSMeta(String lat, String lng,String cityId, String slotName, String offset, String meal_plan, Boolean hit_serv, String scenario) throws IOException, InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        JsonHelper jsonHelper = new JsonHelper();
        String[] serviceable_rest;
        List<AvailabilityListing> ls_avail_list;
        List<AvailabilityMeta> ls_avail_meta;
        String currentTime = String.valueOf(dateHelper.getCurrentEpochTimeInMillis());
        String[] consul_props = helper.getPaginationConfigFromConsul(DailySandConstants.userAgent);
        HashMap<String, String> hm = helper.getSlotDetailOfGivenSlotIfExists(DailySandConstants.cityId,currentTime,slotName);
        if (hm.size() <= 0)
            Assert.assertTrue(false, "Given Slot: "+slotName+" does not exist in the Meal Slot Response");
        //if hit serve is true from dp
        if (hit_serv) {
            DeliveryListing_Cerebro deliveryListing_cerebro = new DeliveryListing_Cerebro();
            String startTime_epoch = dateHelper.convertCurrentDateTimeIndd_mm_yyyy_hh_mmToEpoch(hm.get("slot_date"), hm.get("start_time"));
            String endTime_epoch = dateHelper.convertCurrentDateTimeIndd_mm_yyyy_hh_mmToEpoch(hm.get("slot_date"), hm.get("end_time"));
            deliveryListing_cerebro.build(startTime_epoch, endTime_epoch, lat, lng, cityId);
            if (meal_plan.equals(DailySandConstants.slotType_plan))
                deliveryListing_cerebro.setPack_size(5);
            Processor p_cerebro = helper.deliveryListingHelper(jsonHelper.getObjectToJSON(deliveryListing_cerebro));
             Thread.sleep(1000);
            //serviceable = p_cerebro.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.serviceableRestaurants").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            serviceable_rest = helper.getServiceableRestaurantsArray(p_cerebro.ResponseValidator.GetBodyAsText());
            ls_avail_list = new ArrayList<>();
            if (serviceable_rest.length > 0) {
                log.info("\n --List of Serviceable Restaurants ::: "+Arrays.asList(serviceable_rest));
                //log.info("\n --length of serviceable Rest::: "+serviceable_rest.length);
                for (String store_id : serviceable_rest) {
                    Integer st_id = Integer.parseInt(store_id);
                    AvailabilityListing availabilityListing = new AvailabilityListing();
                    availabilityListing.build(st_id);
                    ls_avail_list.add(availabilityListing);
                }
            }
        }
        else {
            ls_avail_list = new ArrayList<>();
            serviceable_rest = new String[0];
        }
        //*********MEAL Availability**********
        if (meal_plan.equals(DailySandConstants.slotType_meal)) {
            //availability meal listing
            String avail_meal_start_time = dateHelper.convertCurrentDateTimeIndd_mm_yyyy_hh_mmToEpochMillis(hm.get("slot_date"),hm.get("start_time"));
            String avail_meal_end_time = dateHelper.convertCurrentDateTimeIndd_mm_yyyy_hh_mmToEpochMillis(hm.get("slot_date"),hm.get("end_time"));
            Processor p_avail_meal_listing = helper.availabilityMealListingHelper(jsonHelper.getObjectToJSON(ls_avail_list),DailySandConstants.cms_listing_include_only,avail_meal_start_time,avail_meal_end_time);
            if (!hit_serv || serviceable_rest.length <= 0) {
               // String data = p_avail_meal_listing.ResponseValidator.GetNodeValue("$.data").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                softAssert.assertNull(p_avail_meal_listing.ResponseValidator.GetNodeValue("$.data"),"data should be empty in meal listing since no serviceable restaurants were given");
                if (lat.equals(DailySandConstants.blackzone_lat) && lng.equals(DailySandConstants.blackzone_lng)) {
                    String data_agg = JsonPath.read(helper.dailyMealAggregator(lat, lng, slotName, offset, sid, tid).ResponseValidator.GetBodyAsText(), "$.data.slotsData.items").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    softAssert.assertEquals(data_agg, "", "items in aggregator (listing) meal should be empty string as serviceability is empty, but its not");
                }
            }
            else {
                String[] all_store_ids_avail_meal = p_avail_meal_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[*].store_id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                String[] get_store_ids_avail_meal = helper.returnArrayWithUniqueData(all_store_ids_avail_meal);
                softAssert.assertTrue(get_store_ids_avail_meal.length <= serviceable_rest.length, "length of delivery serviceable restaurant::" + serviceable_rest.length + "  length of avail-meal-listing api storeIds::" + get_store_ids_avail_meal.length + " not equal");
                HashMap<String, List<String>> hm_store_product = new HashMap<>();
                //meta - meal
                AvailabilityMeta availabilityMeta = new AvailabilityMeta();
                ls_avail_meta = new ArrayList<>();
                // store id and product id
                HashMap<String, HashMap<String, String>> hm_meal_meta = new HashMap<>();
                for (String storeId_meal_list : get_store_ids_avail_meal) {
                    String[] get_product_ids_avail_meal = p_avail_meal_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.store_id==" + storeId_meal_list + ")].product_id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    String[] get_productIds = helper.returnArrayWithUniqueData(get_product_ids_avail_meal);
                    List<String> temp_ls = Arrays.asList(get_productIds);
                    hm_store_product.put(storeId_meal_list, temp_ls);
                    //for meal - meta
                    availabilityMeta = new AvailabilityMeta();
                    availabilityMeta.build(storeId_meal_list, temp_ls);
                    ls_avail_meta.add(availabilityMeta);

                }
                //hit meta - including inventory
                Processor p_avail_meal_meta = helper.availabilityMealMetaHelper(jsonHelper.getObjectToJSON(ls_avail_meta), DailySandConstants.cms_meta_include_only);
                //get meta data for each product id
                for (String store : hm_store_product.keySet()) {
                    for (String productId : hm_store_product.get(store)) {
                        String inventory = p_avail_meal_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[0].inventory.remaining").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                        String is_available = p_avail_meal_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[0].availability.is_avail").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        // hm_product_inventory.put(product_id, inventory);
                        HashMap<String, String> temp_hm = new HashMap<>();
                        String display_name = p_avail_meal_meta.ResponseValidator.GetNodeValueAsJsonArray("$.data[?(@.product_id=='" + productId + "')].meta.display_name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        String is_veg = p_avail_meal_meta.ResponseValidator.GetNodeValueAsJsonArray("$.data[?(@.product_id=='" + productId + "')].meta.is_veg").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        String cuisine = p_avail_meal_meta.ResponseValidator.GetNodeValueAsJsonArray("$.data[?(@.product_id=='" + productId + "')].meta.cuisine").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        String spin = p_avail_meal_meta.ResponseValidator.GetNodeValueAsJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[0].spin").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        String price = p_avail_meal_meta.ResponseValidator.GetNodeValueAsJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[0].price.price").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        String images = p_avail_meal_meta.ResponseValidator.GetNodeValueAsJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[0].images[0]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        temp_hm.put("display_name", display_name);
                        temp_hm.put("is_veg", is_veg);
                        temp_hm.put("cuisine", cuisine);
                        temp_hm.put("spin", spin);
                        temp_hm.put("price", price);
                        temp_hm.put("inventory", inventory);
                        temp_hm.put("is_available", is_available);
                        temp_hm.put("images", images);
                        //log.info("--First Spin Id from Meta :::" + spin + "  for ProductId::: " + productId);
                        hm_meal_meta.put(productId, temp_hm);
                        //Thread.sleep(500);
                    }
                }
                HashMap<String, List<String>> hm_agg_storeId_mealId_ls = new HashMap<>();
                int currentOffset;
                if (offset.equals(""))
                    currentOffset = 0;
                else
                    currentOffset = Integer.parseInt(offset);
                Processor p_agg_meal = helper.dailyMealAggregator(lat,lng,slotName,"0",sid,tid);
                String agg_total_offset = p_agg_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.totalOffset").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                //if total offset is null then get it from cache size
                if (agg_total_offset.equals("") || agg_total_offset.equals(null))
                    Thread.sleep(500);
                agg_total_offset = String.valueOf(helper.totalLengthOfRedisDataInPagination(helper.redisKeyGenerator(tid, "MEAL", slotName, lat, lng, new HashMap<String, List<String>>(), new HashMap<String, String>()), Integer.parseInt(consul_props[0])));
                HashMap<String, HashMap<String, String>> agg_meal_hm = new HashMap<>();

                //get all the agg. data looping to all the possible offsets
                while (currentOffset < Integer.parseInt(agg_total_offset)) {
                    log.info("-- Current Offset ::: " + currentOffset);
                    Processor p_agg_listing_meal = helper.dailyMealAggregator(lat, lng, slotName, String.valueOf(currentOffset), sid, tid);
                    currentOffset = helper.getNextOffset(String.valueOf(currentOffset), agg_total_offset,consul_props[1]);
                    log.info("-- Next Offset ::: " + currentOffset);
                    String[] agg_stores = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[*].storeId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    String[] agg_store_ids = helper.returnArrayWithUniqueData(agg_stores);
                    //assert storeid length
                    //softAssert.assertEquals(get_store_ids_avail_meal.length, agg_store_ids.length, "No. of store ids in availability:" + get_store_ids_avail_meal.length + "  no. of store ids in meal_agg:" + agg_store_ids.length);
                    //get meal details
                    for (String agg_store_id : agg_store_ids) {
                        String[] agg_productIds_meal = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.storeId=='" + agg_store_id + "')].mealId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                        List<String> agg_productId_meal_ls = new ArrayList<>();
                        //if that store is already there then append to the existing value and insert to hm
                        if (hm_agg_storeId_mealId_ls.containsKey(agg_store_id)) {
                            List<String> temp_temp = new ArrayList<>();
                            temp_temp.addAll(hm_agg_storeId_mealId_ls.get(agg_store_id));
                            temp_temp.addAll(Arrays.asList(agg_productIds_meal));
                            String[] temp= helper.returnArrayWithUniqueData(temp_temp.toArray(new String[0]));
                            agg_productId_meal_ls = Arrays.asList(temp);
                        } else
                            agg_productId_meal_ls.addAll(Arrays.asList(helper.returnArrayWithUniqueData(agg_productIds_meal)));

                        hm_agg_storeId_mealId_ls.put(agg_store_id, agg_productId_meal_ls);
                    //for each meal id - get data and meal meta
                        log.info("For storeId:" + agg_store_id + " no. of products ::: " + hm_agg_storeId_mealId_ls.get(agg_store_id).size());
                        for (String mealId : helper.returnArrayWithUniqueData(agg_productIds_meal)) {
                            //log.info("--Meal Id In Meal Listing (agg):" + mealId);
                            HashMap<String, String> temp_hm = new HashMap<>();
                            String available = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.mealId=='" + mealId + "')].available").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String outOfStock = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.mealId=='" + mealId + "')].outOfStock").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String veg = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.mealId=='" + mealId + "')].veg").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String cuisine = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.mealId=='" + mealId + "')].cuisine").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String price = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.mealId=='" + mealId + "')].priceDetails.price").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String spin = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.mealId=='" + mealId + "')].spin").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String title = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.mealId=='" + mealId + "')].title").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String imageUrl = p_agg_listing_meal.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.mealId=='" + mealId + "')].imageUrl").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            temp_hm.put("available", available);
                            temp_hm.put("outOfStock", outOfStock);
                            temp_hm.put("veg", veg);
                            temp_hm.put("cuisine", cuisine);
                            temp_hm.put("price", price);
                            temp_hm.put("spin", spin);
                            temp_hm.put("title", title);
                            temp_hm.put("imageUrl", imageUrl);
                            agg_meal_hm.put(mealId, temp_hm);
                            //System.out.println(agg_meal_hm);

                        }
                    }
                    //Thread.sleep(500);
                }
                //Asserts serviceability
                Assert.assertTrue(serviceable_rest.length >= hm_agg_storeId_mealId_ls.size(), "No. of all the serviceable restaurants is not greater than or equal to list of store ids in meal aggregator");
                if(serviceable_rest.length > 0) {
                    for (String agg_rest: hm_agg_storeId_mealId_ls.keySet()) {
                        softAssert.assertTrue(ArrayUtils.contains(serviceable_rest,agg_rest), "Restaurant(StoreId) :"+agg_rest+"  is not available in list of serviceable restaurant from cerebro");
                    }
                } else {
                    // if serviceable restaurant length is 0, then agg_store_ids should also be 0
                    softAssert.assertEquals(serviceable_rest.length, hm_agg_storeId_mealId_ls.size(), "since serviceable_rest is 0, then agg_store_ids should be equal to 0 as well");
                }
                //Asserts availability
                softAssert.assertEquals(hm_agg_storeId_mealId_ls.size(), hm_store_product.size(), "storeid-productid hm sizes not equal");
                for (String agg_storeIds : hm_agg_storeId_mealId_ls.keySet()) {
                    List<String> agg_meal_ids = hm_agg_storeId_mealId_ls.get(agg_storeIds);
                    Assert.assertTrue(hm_store_product.containsKey(agg_storeIds), "availability listing does not contain store id:" + agg_storeIds);
                    softAssert.assertEquals(hm_store_product.get(agg_storeIds).size(), hm_agg_storeId_mealId_ls.get(agg_storeIds).size(), "no. of product ids for storeId:" + agg_storeIds + "  not same");
                    for (String agg_meal_id : agg_meal_ids) {
                        //Debug - to check data for random entries
//                        System.out.println("******************** CMS Meta & Listing data*************");
//                        log.info("agg_meal_id "+agg_meal_id+"---------------");
//                        log.info("hm_meal_meta.get(agg_meal_id).get(\"cuisine\") "+hm_meal_meta.get(agg_meal_id).get("cuisine"));
//                        log.info("hm_meal_meta.get(agg_meal_id).get(\"display_name\") "+hm_meal_meta.get(agg_meal_id).get("display_name"));
//                        log.info("hm_meal_meta.get(agg_meal_id).get(\"spin\") "+hm_meal_meta.get(agg_meal_id).get("spin"));
//
//                        System.out.println("************************* SND Listing data *******************");
//                        log.info("agg_meal_hm.get(agg_meal_id).get(\"cuisine\") "+agg_meal_hm.get(agg_meal_id).get("cuisine"));
//                        log.info("agg_meal_hm.get(agg_meal_id).get(\"title\") "+agg_meal_hm.get(agg_meal_id).get("title"));
//                        log.info("agg_meal_hm.get(agg_meal_id).get(\"spin\") "+agg_meal_hm.get(agg_meal_id).get("spin"));
                        Assert.assertTrue(hm_store_product.get(agg_storeIds).contains(agg_meal_id), "Avail Response does not contain product Id: " + agg_meal_id);
                        softAssert.assertEquals(agg_meal_hm.get(agg_meal_id).get("cuisine"), hm_meal_meta.get(agg_meal_id).get("cuisine"), "cuisine not same for meal id:" + agg_meal_id);
                        softAssert.assertEquals(Boolean.parseBoolean(agg_meal_hm.get(agg_meal_id).get("outOfStock")), helper.isOutOfStock(hm_meal_meta.get(agg_meal_id).get("inventory")), "outOfStock not same for meal id:" + agg_meal_id);
                        softAssert.assertEquals(agg_meal_hm.get(agg_meal_id).get("title"), hm_meal_meta.get(agg_meal_id).get("display_name"), "title not same for meal id:" + agg_meal_id);
                        softAssert.assertEquals(agg_meal_hm.get(agg_meal_id).get("available"), hm_meal_meta.get(agg_meal_id).get("is_available"), "is_available not same for meal id:" + agg_meal_id);
                        softAssert.assertEquals(agg_meal_hm.get(agg_meal_id).get("price"), hm_meal_meta.get(agg_meal_id).get("price"), "price not same for meal id:" + agg_meal_id);
                        softAssert.assertEquals(agg_meal_hm.get(agg_meal_id).get("veg"), hm_meal_meta.get(agg_meal_id).get("is_veg"), "is_veg not same for meal id:" + agg_meal_id);
                        softAssert.assertEquals(agg_meal_hm.get(agg_meal_id).get("spin"), hm_meal_meta.get(agg_meal_id).get("spin"), "spin not same for meal id:" + agg_meal_id);
                        softAssert.assertEquals(agg_meal_hm.get(agg_meal_id).get("imageUrl"), hm_meal_meta.get(agg_meal_id).get("images"), "imageUrl not same for meal id:" + agg_meal_id);
                    }
                   // Thread.sleep(500);
                }
            }
        }

        else if (meal_plan.equals(DailySandConstants.slotType_plan)) {
            Processor p_avail_plan_listing = helper.availabilityPlanListingHelper(jsonHelper.getObjectToJSON(ls_avail_list),DailySandConstants.cms_listing_include_only,hm.get("slot_date"),hm.get("start_time"),hm.get("end_time"),DailySandConstants.cms_base_variant);
            if (!hit_serv || (serviceable_rest.length <= 0)) {
                String data = p_avail_plan_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                softAssert.assertEquals(data,"","data should be empty in meal listing since no serviceable restaurants were given");
                if (lat.equals(DailySandConstants.blackzone_lat) && lng.equals(DailySandConstants.blackzone_lng)) {
                    String data_agg = JsonPath.read(helper.dailyPlanAggregator(lat, lng, slotName, offset, sid, tid).ResponseValidator.GetBodyAsText(), "$.data.slotsData.items").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    softAssert.assertEquals(data_agg, "", "items in aggregator (listing) plan should be empty string as serviceability is empty, but its not");
                }
            }
            else {
                String[] store_ids_avail_plan = p_avail_plan_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[*].store_id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                String[] get_store_ids_avail_plan = helper.returnArrayWithUniqueData(store_ids_avail_plan);
                HashMap<String, HashMap<String, List<String>>> hm_avail_store_product = new HashMap<>();
                //meta - meal
                AvailabilityMeta availabilityMeta = new AvailabilityMeta();
                ls_avail_meta = new ArrayList<>();
                // store id and product id and variations

                for (String storeId_plan_list : get_store_ids_avail_plan) {
                    String[] get_product_ids_avail_plan = p_avail_plan_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.store_id==" + storeId_plan_list + ")].product_id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    HashMap<String, List<String>> temp_hm_ls = new HashMap<>();
                    for (String productId : get_product_ids_avail_plan) {
                        log.info("--Plan Id in Plan Avail Listing ::: "+productId);
                        String[] get_product_variations = p_avail_plan_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[*].id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                        List<String> temp_ls_variations = Arrays.asList(get_product_variations);
                        temp_hm_ls.put(productId, temp_ls_variations);
                    }
                    //for meal - meta pojo data
                    availabilityMeta = new AvailabilityMeta();
                    availabilityMeta.build(storeId_plan_list, Arrays.asList(get_product_ids_avail_plan));
                    ls_avail_meta.add(availabilityMeta);
                    hm_avail_store_product.put(storeId_plan_list, temp_hm_ls);
                }
                //hit meta - including inventory
                Processor p_avail_plan_meta = helper.availabilityPlanMetaHelper(jsonHelper.getObjectToJSON(ls_avail_meta), DailySandConstants.cms_meta_include_only,DailySandConstants.cms_base_variant);
                //get meta data for each product id
                HashMap<String, HashMap<String, List<String>>> hm_variation_meta = new HashMap<>();
                HashMap<String, HashMap<String, String>> hm_product_meta = new HashMap<>();
                for (String store : hm_avail_store_product.keySet()) {
                    HashMap<String, List<String>> productId_variations = hm_avail_store_product.get(store);
                    for (String productId : productId_variations.keySet()) {
                        HashMap<String, String> temp_hm = new HashMap<>();
                        //for inventory and out of stock calculation from availability listing - plan
                        String[] is_avail = p_avail_plan_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[*].availability.is_avail").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                        String[] inventory = p_avail_plan_listing.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[*].inventory.remaining").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");

                        String store_id = p_avail_plan_meta.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].store_id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        String display_name = p_avail_plan_meta.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].meta.display_name").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        String is_veg = p_avail_plan_meta.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].meta.is_veg").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        String cuisine = p_avail_plan_meta.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].meta.cuisine").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        String long_description = p_avail_plan_meta.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].meta.long_description").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                        temp_hm.put("store_id", store_id);
                        temp_hm.put("display_name", display_name);
                        temp_hm.put("is_veg", is_veg);
                        temp_hm.put("cuisine", cuisine);
                        temp_hm.put("long_description", long_description);
                        // will calculate and put boolean for isOutOfStock value and for isAvailable value
                        temp_hm.put("is_avail", String.valueOf(helper.isAvailable(is_avail)));
                        temp_hm.put("inventory", String.valueOf(helper.isOutOfStock(inventory)));
                        hm_product_meta.put(productId, temp_hm);
                        HashMap<String, List<String>> temp_hm_variations = new HashMap<>();
                        String[] price = p_avail_plan_meta.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[*].price.list_price").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                        String[] tenure = p_avail_plan_meta.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.product_id=='" + productId + "')].variations[*].meta.tenure").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                        temp_hm_variations.put("price", Arrays.asList(price));
                        temp_hm_variations.put("tenure", Arrays.asList(tenure));
                        hm_variation_meta.put(productId, temp_hm_variations);
                    }
                }
                //Plan Listing data
                //hit plan - including inventory
                int currentOffset;
                if (offset.equals(""))
                    currentOffset = 0;
                else
                    currentOffset = Integer.parseInt(offset);

                HashMap<String, List<String>> hm_agg_storeId_planId_ls = new HashMap<>();
                Processor p_agg_plan = helper.dailyPlanAggregator(lat, lng, slotName, offset,sid,tid);
                String agg_total_offset = p_agg_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.totalOffset").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                //if total offset is null then get it from cache size
                if (agg_total_offset.equals("") || agg_total_offset.equals(null))
                Thread.sleep(1000);
                agg_total_offset = String.valueOf(helper.totalLengthOfRedisDataInPagination(helper.redisKeyGenerator(tid, "PLAN", slotName, lat, lng, new HashMap<String, List<String>>(), new HashMap<String, String>()), Integer.parseInt(consul_props[0])));

                HashMap<String, HashMap<String, String>> agg_plan_hm = new HashMap<>();
                HashMap<String, HashMap<String, List<String>>> agg_tenure_price = new HashMap<>();
                while (currentOffset < Integer.parseInt(agg_total_offset)) {
                    log.info("-- Current Offset ::: " + currentOffset);
                    Processor p_agg_listing_plan = helper.dailyPlanAggregator(lat, lng, slotName, String.valueOf(currentOffset), sid, tid);
                    currentOffset = helper.getNextOffset(String.valueOf(currentOffset),agg_total_offset, consul_props[1]);
                    String[] agg_stores = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[*].storeId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(" ", "").split(",");
                    String[] agg_store_ids = helper.returnArrayWithUniqueData(agg_stores);
                    log.info("agg_store_ids :::" + Arrays.asList(agg_store_ids));
                    //assert storeid length
                    //softAssert.assertEquals(get_store_ids_avail_plan.length, agg_store_ids.length, "No. of store ids in availability:" + get_store_ids_avail_plan.length + "  no. of store ids in meal_agg:" + agg_store_ids.length);
                    for (String agg_store_id : agg_store_ids) {
                        String[] agg_productIds_plan = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.storeId=='" + agg_store_id + "')].planId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                        log.info("agg_productIds_plan :::" + Arrays.asList(agg_productIds_plan));
                        List<String> agg_productId_plan_ls = new ArrayList<>();
                        if (hm_agg_storeId_planId_ls.containsKey(agg_store_id)) {
                            List<String> temp_temp = new ArrayList<>();
                            temp_temp.addAll(hm_agg_storeId_planId_ls.get(agg_store_id));
                            temp_temp.addAll(Arrays.asList(agg_productIds_plan));
                            String[] temp = helper.returnArrayWithUniqueData(temp_temp.toArray(new String[0]));
                            agg_productId_plan_ls = Arrays.asList(temp);
                        } else
                            agg_productId_plan_ls.addAll(Arrays.asList(helper.returnArrayWithUniqueData(agg_productIds_plan)));
                        hm_agg_storeId_planId_ls.put(agg_store_id, agg_productId_plan_ls);

                        for (String planId : helper.returnArrayWithUniqueData(agg_productIds_plan)) {
                            //log.info("PlanId Agg: "+planId);
                            HashMap<String, String> temp_hm = new HashMap<>();
                            String available = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].available").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String outOfStock = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].outOfStock").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String veg = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].veg").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String cuisine = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].cuisine").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String title = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].title").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String imageUrl = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].imageUrl").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String itemDescription = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].itemDescription").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            String storeId = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].storeId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").replace(",","");
                            temp_hm.put("available", available);
                            temp_hm.put("outOfStock", outOfStock);
                            temp_hm.put("veg", veg);
                            temp_hm.put("cuisine", cuisine);
                            temp_hm.put("title", title);
                            temp_hm.put("imageUrl", imageUrl);
                            temp_hm.put("itemDescription", itemDescription);
                            temp_hm.put("storeId", storeId);
                            agg_plan_hm.put(planId, temp_hm);
                            //System.out.println(agg_plan_hm);
                            String[] price_details = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].priceDetails[*].price").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                            String[] numberOfMeals = p_agg_listing_plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotsData.items[?(@.planId=='" + planId + "')].priceDetails[*].numberOfMeals").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                            HashMap<String, List<String>> temp_hm_price = new HashMap<>();
                            temp_hm_price.put("price", Arrays.asList(price_details));
                            temp_hm_price.put("numberOfMeals", Arrays.asList(numberOfMeals));
                            agg_tenure_price.put(planId, temp_hm_price);
                        }
                   }
                }
                //Asserts Serviceability
                Assert.assertTrue(serviceable_rest.length >= hm_agg_storeId_planId_ls.size(), "No. of all the serviceable restaurants is not greater than or equal to list of store ids in meal aggregator");
                if(serviceable_rest.length > 0) {
                    for (String agg_rest: hm_agg_storeId_planId_ls.keySet()) {
                        softAssert.assertTrue(ArrayUtils.contains(serviceable_rest,agg_rest), "Restaurant(StoreId) :"+agg_rest+"  is not available in list of serviceable restaurant from cerebro");
                    }
                } else {
                    // if serviceable restaurant length is 0, then agg_store_ids should also be 0
                    softAssert.assertEquals(serviceable_rest.length, hm_agg_storeId_planId_ls.size(), "since serviceable_rest is 0, then agg_store_ids should be equal to 0 as well");
                }
                //asserts availability
                softAssert.assertEquals(hm_agg_storeId_planId_ls.size(), hm_avail_store_product.size(), "storeid-productid hm sizes not equal");
                for (String agg_storeIds : hm_agg_storeId_planId_ls.keySet()) {
                    List<String> agg_plan_ids = hm_agg_storeId_planId_ls.get(agg_storeIds);
                    Assert.assertTrue(hm_avail_store_product.containsKey(agg_storeIds), "availability listing does not contain store id:" + agg_storeIds);
                    for (String agg_plan_id : agg_plan_ids) {
                        //log.info("Plan Id In Plan Listing (agg): "+agg_plan_id);
                        //Debug - Plan random data from meta and listing
//                        System.out.println("***********************PLAN Agg***************");
//                        log.info("agg_plan_id "+agg_plan_id);
//                        log.info("agg_plan_hm.get(agg_plan_id).get(\"title\") "+agg_plan_hm.get(agg_plan_id).get("title"));
//                        log.info("agg_plan_hm.get(agg_plan_id).get(\"storeId\") "+agg_plan_hm.get(agg_plan_id).get("storeId"));
//                        System.out.println("**************************Plan Meta*************");
//                        log.info("hm_product_meta.get(agg_plan_id).get(\"display_name\") "+hm_product_meta.get(agg_plan_id).get("display_name"));
//                        log.info("hm_product_meta.get(agg_plan_id).get(\"is_veg\") "+hm_product_meta.get(agg_plan_id).get("is_veg"));
//                        log.info("hm_product_meta.get(agg_plan_id).get(\"store_id\") "+hm_product_meta.get(agg_plan_id).get("store_id"));
                        Assert.assertTrue(hm_variation_meta.containsKey(agg_plan_id), "availability listing does not contain plan id: " + agg_plan_id);
                        softAssert.assertEquals(agg_plan_hm.get(agg_plan_id).get("title"), hm_product_meta.get(agg_plan_id).get("display_name"), "title not same for plan id:" + agg_plan_id);
                        softAssert.assertEquals(agg_plan_hm.get(agg_plan_id).get("veg"), hm_product_meta.get(agg_plan_id).get("is_veg"), "is_veg not same for plan id:" + agg_plan_id);
                        softAssert.assertEquals(agg_plan_hm.get(agg_plan_id).get("itemDescription"), hm_product_meta.get(agg_plan_id).get("long_description"), "itemDescription not same for plan id:" + agg_plan_id);
                        softAssert.assertEquals(agg_plan_hm.get(agg_plan_id).get("storeId"), hm_product_meta.get(agg_plan_id).get("store_id"), "store_id not same for plan id:" + agg_plan_id);
                        softAssert.assertEquals(agg_plan_hm.get(agg_plan_id).get("cuisine"), hm_product_meta.get(agg_plan_id).get("cuisine"), "cuisine not same for plan id:" + agg_plan_id);
                        //check for oos, tenure
                        HashMap<String, List<String>> temp_hm_prod_price = agg_tenure_price.get(agg_plan_id);
                        softAssert.assertEquals(temp_hm_prod_price.size(), hm_variation_meta.get(agg_plan_id).size(), "variation length (no. of priceDetails entries) not same for planId: " + agg_plan_id);
                        softAssert.assertEquals(temp_hm_prod_price.get("price"), hm_variation_meta.get(agg_plan_id).get("price"), "variation length - price details (no. of priceDetails entries) not same for planId: " + agg_plan_id);
                        for (int i = 0; i < temp_hm_prod_price.get("price").size(); i++) {
                            softAssert.assertTrue(hm_variation_meta.get(agg_plan_id).get("price").contains(temp_hm_prod_price.get("price").get(i)), "Price Details entry:" + temp_hm_prod_price.get("price").get(i) + "  not found in variations, for planId:" + agg_plan_id);
                            softAssert.assertTrue(hm_variation_meta.get(agg_plan_id).get("tenure").contains(temp_hm_prod_price.get("numberOfMeals").get(i)), "numberOfMeals entry:" + temp_hm_prod_price.get("numberOfMeals").get(i) + "  not found in variations for planId:" + agg_plan_id);
                        }
                        softAssert.assertEquals(Boolean.parseBoolean(agg_plan_hm.get(agg_plan_id).get("available")), Boolean.parseBoolean(hm_product_meta.get(agg_plan_id).get("is_avail")), "available field is not correct for planId: " + agg_plan_id);
                        softAssert.assertEquals(Boolean.parseBoolean(agg_plan_hm.get(agg_plan_id).get("outOfStock")), Boolean.parseBoolean(hm_product_meta.get(agg_plan_id).get("inventory")), "outOfStock field is not correct for planId: " + agg_plan_id);
                    }
                }
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "validateTDAndVendorEnrichmentDP", groups = {"SAND", "DAILY", "LISTING", "AVAILABILITY", "SERVICEABILITY","CMS-META", "ENRICHMENT"}, description = "Validate TD & Vendor Enrichment with Asserting data from Enrichment and Listing API", enabled = true)
    public void validateTDAndVendorEnrichment(String lat, String lng, String slotName, String offset, String meal_plan, String sid, String tid ) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        //List<String>
        Processor p_listing;
        String agg_total_offset;
        JsonHelper jsonHelper = new JsonHelper();
        HashMap<String,List<String>> redis_storeId_productId = new HashMap<>();

        if (!helper.isSlotNameValid(slotName)) {
            p_listing = helper.dailyMealAggregator(lat, lng, slotName, "0", sid, tid);
            int status = p_listing.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(status, DailySandConstants.statusOne, "since invalid slotname status should be 1");
        }
        else {
        // TO populate data in redis
        if (meal_plan.equals(DailySandConstants.slotType_meal)) {
            p_listing = helper.dailyMealAggregator(lat, lng, slotName, "0", sid, tid);
            int status_meal_list = p_listing.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(status_meal_list, DailySandConstants.statusZero, "Meal Listing API status is not 0");

        } else if (meal_plan.equals(DailySandConstants.slotType_plan)) {
            p_listing = helper.dailyPlanAggregator(lat, lng, slotName, "0", sid, tid);
            int status_plan_list = p_listing.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(status_plan_list, DailySandConstants.statusZero, "Plan Listing API status is not 0");
        }
        String redisKey = helper.redisKeyGenerator(tid,meal_plan,slotName,lat,lng,new HashMap<>(),new HashMap<>());
        String[] consul_props= helper.getPaginationConfigFromConsul(DailySandConstants.userAgent);
        ArrayList<Integer> redis_block_id = helper.identifyRedisBlockId(offset,redisKey);
        agg_total_offset = String.valueOf(helper.totalLengthOfRedisDataInPagination(redisKey, Integer.parseInt(consul_props[0])));

        int endBlock = Math.floorDiv(Integer.parseInt(agg_total_offset), Integer.parseInt(consul_props[0]));
        if(redis_block_id.size() > 0) {
            int startBlock = redis_block_id.get(0);
            log.info("-- START BLOCK ID ::: " + startBlock);
            log.info("-- END BLOCK ID ::: " + endBlock);
            //get all storeIds from redis
            //Get all storeIds and associated ProductIds <MealId/PlanId> from redis
            for (int i = startBlock; i <= endBlock; i++) {
                String temp = redisHelper.getHashValue(DailySandConstants.sandRedis, 0, redisKey, String.valueOf(i));
                String[] temp_get_redis_storeIds= JsonPath.read(temp, "$.records[*].storeId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                //get_redis_storeIds.addAll(Arrays.asList(temp_get_redis_storeIds));
                for (String storeId:temp_get_redis_storeIds) {
                    List<String> temp_productIds =  new ArrayList<>();
                    //if meal then get meal Ids
                    if (meal_plan.equals(DailySandConstants.slotType_meal))
                        temp_productIds = Arrays.asList(JsonPath.read(temp,"$.records[?(@.storeId=='"+storeId+"')].mealId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(","));
                        //else its a plan and get plan ids
                    else
                        temp_productIds = Arrays.asList(JsonPath.read(temp,"$.records[?(@.storeId=='"+storeId+"')].planId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(","));
                    //if hashmap has entry then append the new list to existing else put
                    if(redis_storeId_productId.containsKey(storeId)) {
                        List<String> temp_temp_ls = new ArrayList<>();
                        temp_temp_ls.addAll(redis_storeId_productId.get(storeId));
                        temp_temp_ls.addAll(temp_productIds);
                        String[] temp_arr = helper.returnArrayWithUniqueData(temp_temp_ls.toArray(new String[0]));
                        redis_storeId_productId.put(storeId, Arrays.asList(temp_arr));
                    } else
                        redis_storeId_productId.put(storeId,temp_productIds);
                }
            }
        }
        //log.info
            log.info("\n-- StoreId-ProductIds :::\n"+redis_storeId_productId);
        // prepare data for avail-meta
        AvailabilityMeta availabilityMeta;
        List<AvailabilityMeta> ls_avail_meta = new ArrayList<>();
        for (String storeId:redis_storeId_productId.keySet()) {
            availabilityMeta = new AvailabilityMeta();
            availabilityMeta.build(storeId, redis_storeId_productId.get(storeId));
            ls_avail_meta.add(availabilityMeta);
        }
        // get spin and price corresponding to store Ids from cms meta
        HashMap<String, HashMap<String,String>> hm_store_spin_price_meta = new HashMap<>();
        //********MEAL*************
        if (meal_plan.equals(DailySandConstants.slotType_meal)) {
            Processor p_avail_meta = helper.availabilityMealMetaHelper(jsonHelper.getObjectToJSON(ls_avail_meta),DailySandConstants.cms_meta_include_only);
            for (String stores:redis_storeId_productId.keySet()) {
                HashMap<String, String> temp_hm = new HashMap<>();
                //get all spin for a restaurant for TD enrichment hit
                String[] temp_spinIds = p_avail_meta.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.store_id=='"+stores+"')].variations[*].spin").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                for (String spin: temp_spinIds) {
                    String get_price = p_avail_meta.ResponseValidator.GetNodeValueAsJsonArray("$.data[*].variations[?(@.spin=='"+spin+"')].price.price").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    temp_hm.put(spin,get_price);
                }
                hm_store_spin_price_meta.put(stores,temp_hm);
            }
            //same steps to get data for plan
            //************PLAN*********
        } else {
            Processor p_avail_meta = helper.availabilityPlanMetaHelper(jsonHelper.getObjectToJSON(ls_avail_meta),DailySandConstants.cms_meta_include_only,DailySandConstants.cms_base_variant);
            for (String stores:redis_storeId_productId.keySet()) {
                HashMap<String, String> temp_hm = new HashMap<>();
                //get all spin for a restaurant for TD enrichment hit
                String[] temp_spinIds = p_avail_meta.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.store_id=='"+stores+"')].variations[*].spin").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                //get hm of spin and price - in plan price taken should be list_price
                for (String spin: temp_spinIds) {
                    String get_price = p_avail_meta.ResponseValidator.GetNodeValueAsJsonArray("$.data[*].variations[?(@.spin=='"+spin+"')].price.list_price").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    temp_hm.put(spin,get_price);
                }
                hm_store_spin_price_meta.put(stores,temp_hm);
            }
        }
        // Hit TD enrichment with the above data
        HashMap<String, HashMap<String, String>> td_enrich_store_spin_price = new HashMap<>();
        TDEnrichment tdEnrichment = new TDEnrichment();
        tdEnrichment.build(hm_store_spin_price_meta);
        Processor p_td_enrich = helper.TDEnrichmentHelper(jsonHelper.getObjectToJSON(tdEnrichment),meal_plan);
        int status_td_enrich = p_td_enrich.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(status_td_enrich,DailySandConstants.statusOne, "TD Enrichment API status != 1");
        String data = p_td_enrich.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").toString().replace("[", "").replace("]", "").replace("[","").replace("{", "").replace("}", "").replace("\"", "");
        //if td applicable for any spin then get its data
        if (!data.equals("")) {
            String[] td_storeIds = p_td_enrich.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores[*].storeId").replace("]", "").replace("[","").replace("{", "").replace("}", "").replace("\"", "").split(",");
            for (String store:td_storeIds) {
                HashMap<String, String> temp_hm =  new HashMap<>();
                String[] td_spinIds = p_td_enrich.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores[?(@.storeId=='"+store+"')].items[*].skuId").replace("[","").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                for (String spinid: td_spinIds) {
                    String td_discountedPrice = p_td_enrich.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores[?(@.storeId=='"+store+"')].items[?(@.skuId=='"+spinid+"')].probableDiscountedPrice").replace("[","").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    temp_hm.put(spinid,td_discountedPrice);
                }
                td_enrich_store_spin_price.put(store,temp_hm);
            }
        } else {
            // scenario when td data is empty - no applicable tds for the given data
            td_enrich_store_spin_price = new HashMap<>();
        }
        log.info("\n*** --TD Enrichment Response Data *** ::: \n"+td_enrich_store_spin_price);
        // VENDOR Enrichment
        Processor p_vendor_enrich = helper.vendorEnrichment(jsonHelper.getObjectToJSON(redis_storeId_productId.keySet().toArray(new String[0])));
        int status_vendor_enrich = p_vendor_enrich.ResponseValidator.GetNodeValueAsInt("$.status");
        softAssert.assertEquals(status_vendor_enrich,DailySandConstants.statusOne, "VENDOR ENRICHMENT API gave status != 1");
        HashMap<String, HashMap<String,String>> hm_vendor_enrich = new HashMap<>();
        for (String store: redis_storeId_productId.keySet()) {
            HashMap<String,String> hm_temp = new HashMap<>();
            String vendorName = p_vendor_enrich.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id=="+store+")].name").replace("[","").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String vendorImage = p_vendor_enrich.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id=="+store+")].cloudinaryImageId").replace("[","").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String vendorDescription = p_vendor_enrich.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id=="+store+")].short_story").replace("[","").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            hm_temp.put("vendorName",vendorName);
            hm_temp.put("vendorImage",vendorImage);
            hm_temp.put("vendorDescription",vendorDescription);
            hm_vendor_enrich.put(store,hm_temp);
        }

        //Hit Enrichment internal API
        EnrichmentService enrichmentService;
        List<EnrichmentService> ls_enrichment_serv = new ArrayList<>();
        for (String storeId:redis_storeId_productId.keySet()) {
            enrichmentService = new EnrichmentService();
            enrichmentService.build(storeId,redis_storeId_productId.get(storeId));
            ls_enrichment_serv.add(enrichmentService);
        }
        Processor p_enrich_serv = helper.enrichmentServiceHelper(jsonHelper.getObjectToJSON(ls_enrichment_serv),meal_plan);
        int status_enrich_serv = p_enrich_serv.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(status_enrich_serv,DailySandConstants.statusZero, "Enrichment Service API call for "+meal_plan+" is not equal to 0");
        if (meal_plan.equals(DailySandConstants.slotType_meal)) {
            //Since meal response has spinId we can directly use it to filter data
            System.out.println("********MEAL Enrichment Asserts********");
            for (String storeId : hm_store_spin_price_meta.keySet()) {
                for (String spin : hm_store_spin_price_meta.get(storeId).keySet()) {
                    String get_price = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.spin=='" + spin + "')].priceDetails.price").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String get_discounted_price = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.spin=='" + spin + "')].priceDetails.discountedPrice").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String get_vendorName = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.spin=='" + spin + "')].vendorName").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String get_vendorImage = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.spin=='" + spin + "')].vendorImage").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String get_vendorDescription = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.spin=='" + spin + "')].vendorDescription").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    //vendor enrichment asserts
                    softAssert.assertEquals(get_vendorName, hm_vendor_enrich.get(storeId).get("vendorName"), "MEAL - Vendor Name not same for storeId:" + storeId);
                    softAssert.assertEquals(get_vendorDescription, hm_vendor_enrich.get(storeId).get("vendorDescription"), "MEAL - Vendor Description is not same for storeId:" + storeId);
                    softAssert.assertEquals(get_vendorImage, hm_vendor_enrich.get(storeId).get("vendorImage"), "MEAL - Vendor Image is not same for storeId:" + storeId);
                    //Price as compared to cms meta
                    softAssert.assertEquals(get_price, hm_store_spin_price_meta.get(storeId).get(spin), "MEAL - Price not same for storeId:" + storeId + "  and spinId:" + spin);
                    // check if there is a discounted price from td else validate it same to price
                    if (td_enrich_store_spin_price.size() > 0 && td_enrich_store_spin_price.containsKey(storeId)) {
                        if (td_enrich_store_spin_price.get(storeId).containsKey(spin))
                            softAssert.assertEquals(get_discounted_price, td_enrich_store_spin_price.get(storeId).get(spin), "MEAL - TD available - yet discounted price not equal to TD Enrichment response for spin: " + spin + "  & store:" + storeId + " . DiscountedPrice IN ENRICHMENT: " + get_discounted_price + " . Discounted Price from TD: " + td_enrich_store_spin_price.get(storeId).get(spin));
                    } //discounted price not in TD response so should be equal to price
                    else
                        softAssert.assertEquals(get_discounted_price, hm_store_spin_price_meta.get(storeId).get(spin), "MEAL - TD not available - discounted price should be similar to price for store Id:" + storeId + " & spin:" + spin);
                }
            }
        }
        else {
            System.out.println("********PLAN Enrichment Asserts********");
            //since no spinIds in plan we will get planIds from search-sku api for given spinIds
            SearchBySpin searchBySpin = new SearchBySpin();
            List<SearchBySpin> ls_searchBySpin = new ArrayList<>();
            for (String storeId : hm_store_spin_price_meta.keySet()) {
                searchBySpin = new SearchBySpin();
                searchBySpin.build(storeId, Arrays.asList(hm_store_spin_price_meta.get(storeId).keySet().toArray(new String[0])));
                ls_searchBySpin.add(searchBySpin);
            }
            Processor p_search_by_sku = helper.searchBySpinIdHelper(jsonHelper.getObjectToJSON(ls_searchBySpin));
            int status_searchBySku = p_search_by_sku.ResponseValidator.GetResponseCode();
            softAssert.assertEquals(status_searchBySku, 200, "response code for searchBySku not 200");
            HashMap<String, String> spinId_productId = new HashMap<>();
            for (String store : hm_store_spin_price_meta.keySet()) {
                for (String spinId : hm_store_spin_price_meta.get(store).keySet()) {
                    String planId = p_search_by_sku.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.spin=='" + spinId + "')].product_id").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    spinId_productId.put(spinId, planId);
                }
            }
            for (String storeId : hm_store_spin_price_meta.keySet()) {
                for (String spin : hm_store_spin_price_meta.get(storeId).keySet()) {
                    //validate price and discount
                    String planId = spinId_productId.get(spin);
                    String[] get_price = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.planId=='" + planId + "')].priceDetails[*].price").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");

                    String[] get_discounted_price = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.planId=='" + planId + "')].priceDetails.discountedPrice").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
                    String get_vendorName = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.planId=='" + spin + "')].vendorName").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String get_vendorImage = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.planId=='" + spin + "')].vendorImage").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    String get_vendorDescription = p_enrich_serv.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.planId=='" + spin + "')].vendorDescription").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
                    //vendor enrichment asserts
                    softAssert.assertEquals(get_vendorName, hm_vendor_enrich.get(storeId).get("vendorName"), "PLAN -Vendor Name not same for storeId:" + storeId);
                    softAssert.assertEquals(get_vendorDescription, hm_vendor_enrich.get(storeId).get("vendorDescription"), "PLAN -Vendor Description is not same for storeId:" + storeId);
                    softAssert.assertEquals(get_vendorImage, hm_vendor_enrich.get(storeId).get("vendorImage"), "PLAN -Vendor Image is not same for storeId:" + storeId);
                    //Price as compared to cms meta
                    softAssert.assertTrue(Arrays.asList(get_price).contains(hm_store_spin_price_meta.get(storeId).get(spin)), "PLAN - Price not contained in list of prices for storeId:" + storeId + "  and spinId:" + spin);
                    //check if there is a discounted price from td else validate it same to price
                    if (td_enrich_store_spin_price.size() > 0 && td_enrich_store_spin_price.containsKey(storeId)) {
                        if (td_enrich_store_spin_price.get(storeId).containsKey(spin))
                            softAssert.assertTrue(Arrays.asList(get_discounted_price).contains(td_enrich_store_spin_price.get(storeId).get(spin)), "PLAN - TD available - yet discounted price not contained in TD Enrichment response for spin: " + spin + "  & store:" + storeId + " . DiscountedPrice IN ENRICHMENT: " + get_discounted_price + " . Discounted Price from TD: " + td_enrich_store_spin_price.get(storeId).get(spin));
                    } //discounted price not in TD response so should be equal to price
                    else
                        softAssert.assertTrue(Arrays.asList(get_discounted_price).contains(hm_store_spin_price_meta.get(storeId).get(spin)), "PLAN - TD not available - discounted price should be similar to price for store Id:" + storeId + " & spin:" + spin);
                }
            }

        }
        }
        /**H
         * Store id from Redis
         * Get spin and price from cms meta
         * Hit TD with store, spin and price from meta
         * Hit search-sku api with store and spin Ids and get respective plan or meal Ids - storeId->PlanId
         * Hit vendor enrichment to get vendor details from unique store ids
         * Hit enrichment api with above data and validate response for those productIds whose discount is applicable & their vendor names
         */

    }

    @Test(dataProvider = "latlng", groups = {"SAND", "DAILY", "LISTING", "MEAL-SLOT"}, description = "Validate Meal slot api with cityServiceIntegration")
    public void getCityIdFromCityService(String lat, String lng) {
        String response = helper.getCityIdFromLatLng(lat, lng).ResponseValidator.GetBodyAsText();
        String cityId = JsonPath.read(response,"$.data.id").toString();
        String daily_meal_slots = helper.getDailyMealSlotsHelper(cityId, String.valueOf(dateHelper.getCurrentEpochTimeInMillis())).ResponseValidator.GetBodyAsText();
        String statusCode = JsonPath.read(daily_meal_slots,"$.statusCode").toString();
        Assert.assertEquals(statusCode,"1");
        }

    @Test(dataProvider = "latlngInvalid", groups = {"SAND", "DAILY", "LISTING", "MEAL-SLOT"}, description = "Validate Meal slot api with cityServiceIntegration for an unmapped city")
    public void getCityIdFromCityServiceDefault(String lat, String lng) {
        String response = helper.getCityIdFromLatLng(lat, lng).ResponseValidator.GetBodyAsText();
        String cityId = JsonPath.read(response,"$.data.id").toString();
        String daily_meal_slots = helper.getDailyMealSlotsHelper(cityId, String.valueOf(dateHelper.getCurrentEpochTimeInMillis())).ResponseValidator.GetBodyAsText();
        String statusCode = JsonPath.read(daily_meal_slots,"$.statusCode").toString();
        Assert.assertEquals(statusCode,"1");
    }

    @Test(dataProvider = "latlng", groups = {"SAND", "DAILY", "LISTING", "MEAL-SLOT"}, description = "Validate cerebro listing with cityServiceIntegration")
    public void getCityIdFromCityServiceCerebro(String lat, String lng) throws IOException {
        String response = helper.getCityIdFromLatLng(lat, lng).ResponseValidator.GetBodyAsText();
        String cityId = JsonPath.read(response,"$.data.id").toString();
        deliveryListing_cerebro.setPack_size(5);
        deliveryListing_cerebro.setCity_id(Integer.getInteger(cityId));
        Processor p_cerebro = helper.deliveryListingHelper(jsonHelper.getObjectToJSON(deliveryListing_cerebro));
        String cerebroListingResponse = p_cerebro.ResponseValidator.GetBodyAsText();
        String statusCode = JsonPath.read(cerebroListingResponse,"$.statusCode").toString();
        String statusMessage = JsonPath.read(cerebroListingResponse,"$.statusMessage");
        HashMap<String,String> data = JsonPath.read(cerebroListingResponse,"$.data");
        if(data.isEmpty()) {
            Assert.assertEquals(statusCode, "1");
            Assert.assertEquals(statusMessage, "Failed");
        }
        else {
            Assert.assertEquals(statusCode, "0");
            Assert.assertEquals(statusMessage, "SUCCESS");
        }
    }

    }