package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Variant;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cmsAttributes.Attributes;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({

        "added_by_user_id",
        "added_by_username",
        "item_id",
        "external_item_id",
        "name",
        "attributes",
        "is_veg",
        "variants",
        "addons",
        "image_id",
        "quantity",
        "total",
        "subtotal",
        "packing_charges",
        "category_details",
        "global_main_category",
        "global_sub_category",
        "item_charges",
        "meal_quantity",
        "rewardType",
        "item_type",
        "meal_id",
        "meal_name"
})
public class OrderItem {

    @JsonProperty("added_by_user_id")
    private Integer addedByUserId;
    @JsonProperty("added_by_username")
    private String addedByUsername;
    @JsonProperty("item_id")
    private String itemId;
    @JsonProperty("external_item_id")
    private String externalItemId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("attributes")
    private Attributes attributes;
    @JsonProperty("is_veg")
    private String isVeg;
    @JsonProperty("variants")
    private List<Variant> variants = null;
    @JsonProperty("addons")
    private List<Object> addons = null;
    @JsonProperty("image_id")
    private String imageId;
    @JsonProperty("quantity")
    private String quantity;
    @JsonProperty("total")
    private String total;
    @JsonProperty("subtotal")
    private String subtotal;
    @JsonProperty("packing_charges")
    private String packingCharges;
    @JsonProperty("category_details")
    private CategoryDetails categoryDetails;
    @JsonProperty("global_main_category")
    private String globalMainCategory;
    @JsonProperty("global_sub_category")
    private String globalSubCategory;
    @JsonProperty("item_charges")
    private ItemCharges itemCharges;
    @JsonProperty("meal_quantity")
    private String mealQuantity;
    @JsonProperty("rewardType")
    private String rewardType;
    @JsonProperty("item_type")
    private String itemType;
    @JsonProperty("meal_id")
    private String mealId;
    @JsonProperty("meal_name")
    private String mealName;

    /**
     * No args constructor for use in serialization
     *
     */
    public OrderItem() {
    }

    /**
     *
     * @param total
     * @param globalMainCategory
     * @param mealName
     * @param mealId
     * @param itemType
     * @param addons
     * @param itemCharges
     * @param externalItemId
     * @param itemId
     * @param subtotal
     * @param addedByUserId
     * @param rewardType
     * @param packingCharges
     * @param isVeg
     * @param categoryDetails
     * @param imageId
     * @param globalSubCategory
     * @param addedByUsername
     * @param name
     * @param attributes
     * @param variants
     * @param quantity
     * @param mealQuantity
     */
    public OrderItem(Integer addedByUserId, String addedByUsername, String itemId, String externalItemId, String name, Attributes attributes, String isVeg, List<Variant> variants, List<Object> addons, String imageId, String quantity, String total, String subtotal, String packingCharges, CategoryDetails categoryDetails, String globalMainCategory, String globalSubCategory, ItemCharges itemCharges, String mealQuantity, String rewardType, String itemType, String mealId, String mealName) {
        super();
        this.addedByUserId = addedByUserId;
        this.addedByUsername = addedByUsername;
        this.itemId = itemId;
        this.externalItemId = externalItemId;
        this.name = name;
        this.attributes = attributes;
        this.isVeg = isVeg;
        this.variants = variants;
        this.addons = addons;
        this.imageId = imageId;
        this.quantity = quantity;
        this.total = total;
        this.subtotal = subtotal;
        this.packingCharges = packingCharges;
        this.categoryDetails = categoryDetails;
        this.globalMainCategory = globalMainCategory;
        this.globalSubCategory = globalSubCategory;
        this.itemCharges = itemCharges;
        this.mealQuantity = mealQuantity;
        this.rewardType = rewardType;
        this.itemType = itemType;
        this.mealId = mealId;
        this.mealName = mealName;
    }

    @JsonProperty("added_by_user_id")
    public Integer getAddedByUserId() {
        return addedByUserId;
    }

    @JsonProperty("added_by_user_id")
    public void setAddedByUserId(Integer addedByUserId) {
        this.addedByUserId = addedByUserId;
    }

    @JsonProperty("added_by_username")
    public String getAddedByUsername() {
        return addedByUsername;
    }

    @JsonProperty("added_by_username")
    public void setAddedByUsername(String addedByUsername) {
        this.addedByUsername = addedByUsername;
    }

    @JsonProperty("item_id")
    public String getItemId() {
        return itemId;
    }

    @JsonProperty("item_id")
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    @JsonProperty("external_item_id")
    public String getExternalItemId() {
        return externalItemId;
    }

    @JsonProperty("external_item_id")
    public void setExternalItemId(String externalItemId) {
        this.externalItemId = externalItemId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("is_veg")
    public String getIsVeg() {
        return isVeg;
    }

    @JsonProperty("is_veg")
    public void setIsVeg(String isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    @JsonProperty("addons")
    public List<Object> getAddons() {
        return addons;
    }

    @JsonProperty("addons")
    public void setAddons(List<Object> addons) {
        this.addons = addons;
    }

    @JsonProperty("image_id")
    public String getImageId() {
        return imageId;
    }

    @JsonProperty("image_id")
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    @JsonProperty("quantity")
    public String getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("total")
    public String getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(String total) {
        this.total = total;
    }

    @JsonProperty("subtotal")
    public String getSubtotal() {
        return subtotal;
    }

    @JsonProperty("subtotal")
    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    @JsonProperty("packing_charges")
    public String getPackingCharges() {
        return packingCharges;
    }

    @JsonProperty("packing_charges")
    public void setPackingCharges(String packingCharges) {
        this.packingCharges = packingCharges;
    }

    @JsonProperty("category_details")
    public CategoryDetails getCategoryDetails() {
        return categoryDetails;
    }

    @JsonProperty("category_details")
    public void setCategoryDetails(CategoryDetails categoryDetails) {
        this.categoryDetails = categoryDetails;
    }

    @JsonProperty("global_main_category")
    public String getGlobalMainCategory() {
        return globalMainCategory;
    }

    @JsonProperty("global_main_category")
    public void setGlobalMainCategory(String globalMainCategory) {
        this.globalMainCategory = globalMainCategory;
    }

    @JsonProperty("global_sub_category")
    public String getGlobalSubCategory() {
        return globalSubCategory;
    }

    @JsonProperty("global_sub_category")
    public void setGlobalSubCategory(String globalSubCategory) {
        this.globalSubCategory = globalSubCategory;
    }

    @JsonProperty("item_charges")
    public ItemCharges getItemCharges() {
        return itemCharges;
    }

    @JsonProperty("item_charges")
    public void setItemCharges(ItemCharges itemCharges) {
        this.itemCharges = itemCharges;
    }

    @JsonProperty("meal_quantity")
    public String getMealQuantity() {
        return mealQuantity;
    }

    @JsonProperty("meal_quantity")
    public void setMealQuantity(String mealQuantity) {
        this.mealQuantity = mealQuantity;
    }

    @JsonProperty("rewardType")
    public String getRewardType() {
        return rewardType;
    }

    @JsonProperty("rewardType")
    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    @JsonProperty("item_type")
    public String getItemType() {
        return itemType;
    }

    @JsonProperty("item_type")
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    @JsonProperty("meal_id")
    public String getMealId() {
        return mealId;
    }

    @JsonProperty("meal_id")
    public void setMealId(String mealId) {
        this.mealId = mealId;
    }

    @JsonProperty("meal_name")
    public String getMealName() {
        return mealName;
    }

    @JsonProperty("meal_name")
    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addedByUserId", addedByUserId).append("addedByUsername", addedByUsername).append("itemId", itemId).append("externalItemId", externalItemId).append("name", name).append("attributes", attributes).append("").append("isVeg", isVeg).append("variants", variants).append("addons", addons).append("imageId", imageId).append("quantity", quantity).append("total", total).append("subtotal", subtotal).append("packingCharges", packingCharges).append("categoryDetails", categoryDetails).append("globalMainCategory", globalMainCategory).append("globalSubCategory", globalSubCategory).append("itemCharges", itemCharges).append("mealQuantity", mealQuantity).append("rewardType", rewardType).append("itemType", itemType).append("mealId", mealId).append("mealName", mealName).toString();
    }

}