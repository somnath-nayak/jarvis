package com.swiggy.api.sf.rng.helper;

import com.swiggy.api.sf.rng.pojo.*;

public class CouponHelper {

    GlobalOffsetCreateMultiApplyCouponPOJO globalOffsetCreateMultiApplyCouponPOJO = new GlobalOffsetCreateMultiApplyCouponPOJO();

    public  CreateCouponPOJO createCouponWithTimeSlotWithDayAllTrue(Integer isPrivate,Integer offset,Integer slotRestricstion) {

        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withCouponUserTypeId(0)
                .withSlotRestriction(1)
                .withSlotRestriction(slotRestricstion)
                .withIsPrivate(isPrivate)
                .withExpiryOffset(offset)
                .withValidFrom(Utility.getYesterDayDate())
                .withValidTill(Utility.getFutureDateThreeMonth())
                .withCode("AUTOMATE" + Utility.getRandomPostfix());
        return createCouponPOJO;

    }

    public CouponTimeSlotPOJO timeSlotMaping(String day,String openTime,String closeTime, String couponCode,Integer couponId) {

        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponTimeSlotPOJO().setDefaultData(couponCode,couponId)
                .withDay(day)
                .withCode(couponCode)
                .withCouponId(couponId)
                .withCreatedOn(Utility.getYesterDayDate())
                .withOpenTime(openTime)
                .withCloseTime(closeTime);
        return couponTimeSlotPOJO;

    }
    public CouponTimeSlotPOJO timeSlotMapingPreviousMonth(String day,String openTime,String closeTime, String couponCode,Integer couponId) {

        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponTimeSlotPOJO().setDefaultData(couponCode,couponId)
                .withDay(day)
                .withCode(couponCode)
                .withCouponId(couponId)
                .withCreatedOn(Utility.getPreviousDate())
                .withUpdatedOn(Utility.getFutureDate())
                .withOpenTime(openTime)
                .withCloseTime(closeTime);
        return couponTimeSlotPOJO;

    }

    public  CreateCouponPOJO createCouponWithValidFromPreviousMonthTrue(Integer isPrivate,Integer offset,Integer slotRestricstion) {

        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withCouponUserTypeId(0)
                .withSlotRestriction(1)
                .withSlotRestriction(slotRestricstion)
                .withIsPrivate(isPrivate)
                .withExpiryOffset(offset)
                .withValidFrom(Utility.getPreviousDate())
                .withValidTill(Utility.getFutureDateThreeMonth())
                .withCreatedOn(Utility.getPreviousDate())
                .withCode("AUTOMATE" + Utility.getRandomPostfix());
        return createCouponPOJO;

    }



    public ApplyCouponTimeSlotPOJO applyCoupon(String couponCode) {
        ApplyCouponTimeSlotPOJO applyCouponTimeSlotPOJO = new ApplyCouponTimeSlotPOJO().setDefaultData(couponCode);
        return applyCouponTimeSlotPOJO;
    }

    public ApplyCouponPOJO1 applyCoupon(String couponCode,String userID, String time) {
        ApplyCouponPOJO1 applyCouponPOJO1 = new ApplyCouponPOJO1()
                .setDefaultData(couponCode)
                .withTime(time)
                .withUserId(userID);
        return applyCouponPOJO1;
    }


    public  GlobalOffsetCreateMultiApplyCouponPOJO createMultiApplyCouponGlobalOffsetSingleTransaction(Integer globalOffset ,boolean isPrivate,Integer expiryOffset,int peruserCount, int totalAvailable) {

        GlobalOffsetCreateMultiApplyCouponPOJO GlobalOffsetCreateMultiApplyCouponPOJO = new GlobalOffsetCreateMultiApplyCouponPOJO()
                .setDefaultData()
                .withIsPrivate(isPrivate)
                .withExpiryOffset(expiryOffset)
                .withValidFrom(Utility.getYesterDayDate())
                .withGlobalOffsetDays(globalOffset)
                .withTotalPerUser(peruserCount)
                .withTotalAvailable(totalAvailable)
                .withValidTill(Utility.getFutureDateThreeMonth())
                .withMetaList(globalOffsetCreateMultiApplyCouponPOJO.getMetaList(1,20,false,30));
        return GlobalOffsetCreateMultiApplyCouponPOJO;

    }

    public  GlobalOffsetCreateMultiApplyCouponPOJO createMultiApplyCouponGlobalOffsetMultiple60thTransaction(Integer globalOffset ,boolean isPrivate,Integer expiryOffset,int peruserCount, int totalAvailable) {

        GlobalOffsetCreateMultiApplyCouponPOJO GlobalOffsetCreateMultiApplyCouponPOJO = new GlobalOffsetCreateMultiApplyCouponPOJO()
                .setDefaultData()
                .withIsPrivate(isPrivate)
                .withExpiryOffset(expiryOffset)
                .withValidFrom(Utility.getYesterDayDate())
                .withGlobalOffsetDays(globalOffset)
                .withTotalPerUser(peruserCount)
                .withTotalAvailable(totalAvailable)
                .withValidTill(Utility.getFutureDateThreeMonth())
                .withMetaList(globalOffsetCreateMultiApplyCouponPOJO.getMetaList(2,20,false, 30));
        return GlobalOffsetCreateMultiApplyCouponPOJO;

    }

    public  GlobalOffsetCreateMultiApplyCouponPOJO createMultiApplyCouponGlobalOffsetMultipleTransactionWithinGlobalOffset(Integer globalOffset ,boolean isPrivate,Integer expiryOffset,int peruserCount, int totalAvailable) {

        GlobalOffsetCreateMultiApplyCouponPOJO GlobalOffsetCreateMultiApplyCouponPOJO = new GlobalOffsetCreateMultiApplyCouponPOJO()
                .setDefaultData()
                .withIsPrivate(isPrivate)
                .withExpiryOffset(expiryOffset)
                .withValidFrom(Utility.getYesterDayDate())
                .withGlobalOffsetDays(globalOffset)
                .withTotalPerUser(peruserCount)
                .withTotalAvailable(totalAvailable)
                .withValidTill(Utility.getFutureDateThreeMonth())
                .withMetaList(globalOffsetCreateMultiApplyCouponPOJO.getMetaList(3,20,false, 30));
        return GlobalOffsetCreateMultiApplyCouponPOJO;

    }


    public  CreateCouponPOJO couponCappingWithTDAsZero(String couponType,Integer discount_Percentage,Integer discount_Amount,Integer capping_percentage) {

        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withCouponUserTypeId(0)
                .withSlotRestriction(0)
                .withCouponType(couponType)
                .withDiscountPercentage(discount_Percentage)
                .withDiscountAmount(discount_Amount)
                .withCapping_percentage(capping_percentage)
                .withValidFrom(Utility.getYesterDayDate())
                .withValidTill(Utility.getFutureDateThreeMonth())
                .withCode("AUTOMATE" + Utility.getRandomPostfix());
        return createCouponPOJO;

    }
}
