package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderCancelCheck;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "order_id",
        "cancellation_fee_applicability"
})
public class OrderCancelCheckRequest {

    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("cancellation_fee_applicability")
    private boolean cancellationFeeApplicability;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     */
    public OrderCancelCheckRequest() {
    }

    /**
     * @param cancellationFeeApplicability
     * @param orderId
     */
    public OrderCancelCheckRequest(String orderId, boolean cancellationFeeApplicability) {
        super();
        this.orderId = orderId;
        this.cancellationFeeApplicability = cancellationFeeApplicability;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("cancellation_fee_applicability")
    public boolean getCancellationFeeApplicability() {
        return cancellationFeeApplicability;
    }

    @JsonProperty("cancellation_fee_applicability")
    public void setCancellationFeeApplicability(boolean cancellationFeeApplicability) {
        this.cancellationFeeApplicability = cancellationFeeApplicability;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}