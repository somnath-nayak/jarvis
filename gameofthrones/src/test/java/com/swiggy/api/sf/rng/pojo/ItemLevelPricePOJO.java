package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "11122224_1_variants_addons",
        "11122223_0_variants_addons"
})
public class ItemLevelPricePOJO {

    @JsonProperty("11122224_1_variants_addons")
    private String VariantsAddons1;
    @JsonProperty("11122223_0_variants_addons")
    private String VariantsAddons2;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public ItemLevelPricePOJO() {
    }


    public ItemLevelPricePOJO(String VariantsAddons1, String VariantsAddons2) {
        super();
        this.VariantsAddons1= VariantsAddons1;
        this.VariantsAddons2 = VariantsAddons2;
    }

    @JsonProperty("11122224_1_variants_addons")
    public String getVariantsAddons1(String VariantsAddons1) {
        return VariantsAddons1;
    }

    @JsonProperty("11122224_1_variants_addons")
    public void setVariantsAddons1(String VariantsAddons1) {
        this.VariantsAddons1 = VariantsAddons1;
    }

    public ItemLevelPricePOJO withVariantsAddons1(String VariantsAddons1) {
        this.VariantsAddons1 = VariantsAddons1;
        return this;
    }

    @JsonProperty("11122223_0_variants_addons")
    public String getVariantsAddons2() {
        return VariantsAddons2;
    }

    @JsonProperty("11122223_0_variants_addons")
    public void setVariantsAddons2(String VariantsAddons2) {
        this.VariantsAddons2 = VariantsAddons2;
    }

    public ItemLevelPricePOJO withVariantsAddons2(String VariantsAddons2) {
        this.VariantsAddons2 = VariantsAddons2;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public ItemLevelPricePOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public ItemLevelPricePOJO setDefault() {
        return this.withVariantsAddons1("{\n" +
                "                \t\"quantity\": 1,\n" +
                "                \t\"subTotal\": 50\n" +
                "            \t},")
                .withVariantsAddons2("{\n" +
                        "                \t\"quantity\": 1,\n" +
                        "                \t\"subTotal\": 50\n" +
                        "            \t}\n");
    }

}
