package com.swiggy.api.sf.checkout.helper.edvo.pojo.trackOrderResponse;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ErrorPopup {
    @JsonProperty("show_popup")
    private boolean showPopup;

    @JsonProperty("route_to_conversation")
    private boolean routeToConversation;

    @JsonProperty("conversation_id")
    private String conversationId;

    @JsonProperty("display_title")
    private String displayTitle;

    @JsonProperty("display_message")
    private String displayMessage;

    public boolean isShowPopup() {
        return showPopup;
    }

    public void setShowPopup(boolean showPopup) {
        this.showPopup = showPopup;
    }

    public boolean isRouteToConversation() {
        return routeToConversation;
    }

    public void setRouteToConversation(boolean routeToConversation) {
        this.routeToConversation = routeToConversation;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public String getDisplayMessage() {
        return displayMessage;
    }

    public void setDisplayMessage(String displayMessage) {
        this.displayMessage = displayMessage;
    }
}
