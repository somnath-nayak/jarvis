package com.swiggy.api.sf.rng.pojo.SwiggySuper;

public class Createplanbenefitmapping {
	
	private String plan_id;
	private String benefit_id;
	private String created_by="By-Super-Automation";
	
	public Createplanbenefitmapping() {
		
	}
	
   public Createplanbenefitmapping(String plan_id, String benefit_id, String  created_by) {
	   this.plan_id= plan_id;
	   this.benefit_id =benefit_id;
	   this.created_by = created_by;
   }
   
   public String getPlanId() {
	   return this.plan_id;
   }
 public void setPlanId(String plan_id) {
	 this.plan_id= plan_id;
 }
 
 public String getBenefitId() {
	 return this.benefit_id;
 }
 public void setBenefitId(String benefit_id) {
	 this.benefit_id= benefit_id;
	 }
 public String getCreateBy() {
	 return this.created_by;
 }
 public void setCreatedBy(String created_id) {
	 this.created_by= created_id;
 }
 
 @Override
  public String toString() {
	 
	 return "{"
			 + "\"plan_id\"" + ":" + plan_id + ","
			 + "\"benefit_id\"" + ":" + benefit_id + ","
			 + "\"created_by\"" + ":"+ "\"" + created_by + "\""
			 + "}"
			 ;
 }
 
 
 
 
 
 
 
 
   
   
   
   
   
   

}

	