package com.swiggy.api.sf.rng.tests;
import com.swiggy.api.sf.rng.dp.CouponDP;
import com.swiggy.api.sf.rng.pojo.CouponPojo;
import com.swiggy.api.sf.rng.pojo.MinAmountObject;
import com.swiggy.api.sf.rng.pojo.MinQuantityObject;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.swiggy.api.sf.rng.helper.RngHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RedisHelper;

import java.io.IOException;

/*
Please dont add anything to this class. This is being written for further use of best practice notes.
 */

public class DontUseThis extends CouponDP {

    Processor processor;
    RngHelper rngHelper = new RngHelper();
    DBHelper dbHelper = new DBHelper();
    RedisHelper redisHelper = new RedisHelper();
    JsonHelper j = new JsonHelper();
    CouponPojo data;

//    @Test(description="Creation of coupon",dataProvider = "createCouponNew", dataProviderClass = CouponDP.class, priority = 1)
//    public void createCouponService(String couponType, String name, String code, String description, Integer isPrivate, String validFrom, String validTill, Integer totalAvailable, Integer totalPerUser, Integer customerRestriction, Integer cityRestriction, Integer areaRestriction, Integer restaurantRestriction, Integer categoryRestriction, Integer itemRestriction, Integer discountPercentage, Integer discountAmount, Integer freeShipping, Integer freeGifts, Integer firstOrderRestriction, String preferredPaymentMethod, Integer userClient, Integer slotRestriction, String createdOn, String image, String couponBucket, Integer cuisineRestriction, Integer discountItem, Integer usageCount, Integer expiryOffset, Boolean applicableWithSwiggyMoney, Integer isBankDiscount, String refundSource, String pgMessage, Integer supportedIosVersion, String createdBy, String title, String[] tnc, String logo, MinAmountObject minAmountObject, MinQuantityObject minQuantityObject) throws IOException {
//
//        data = new CouponPojo(couponType,name,code,description,isPrivate,validFrom,validTill,totalAvailable,totalPerUser,customerRestriction,cityRestriction,areaRestriction,restaurantRestriction,categoryRestriction,itemRestriction,discountPercentage,discountAmount,freeShipping,freeGifts,firstOrderRestriction,preferredPaymentMethod,userClient,slotRestriction,createdOn,image,couponBucket,cuisineRestriction,discountItem,usageCount,expiryOffset,applicableWithSwiggyMoney,isBankDiscount,refundSource,pgMessage,supportedIosVersion,createdBy,title,tnc,logo,minAmountObject,minQuantityObject);
//        data.setCode("AutoMate"+RngHelper.getRandomPostfix());
//        String body = j.getObjectToJSON(data);
//        processor = rngHelper.createCoupon(body);
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200,"The response code is not success");
//        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.code"), data.getCode(), "The coupon code has not created");
//
//    }
}