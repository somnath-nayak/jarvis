package com.swiggy.api.sf.rng.pojo.SwiggySuper;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.SwiggySuper
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "created_at",
        "created_by",
        "incentive_id",
        "plan_id",
        "user_id",
        "user_segment"
})
public class PlanUserIncentiveMapping {
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("incentive_id")
    private Integer incentive_id;
    @JsonProperty("plan_id")
    private Integer plan_id;
    @JsonProperty("user_id")
    private Integer user_id;
    @JsonProperty("user_segment")
    private String user_segment;

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("incentive_id")
    public Integer getIncentive_id() {
        return incentive_id;
    }

    @JsonProperty("incentive_id")
    public void setIncentive_id(Integer incentive_id) {
        this.incentive_id = incentive_id;
    }

    @JsonProperty("plan_id")
    public Integer getPlan_id() {
        return plan_id;
    }

    @JsonProperty("plan_id")
    public void setPlan_id(Integer plan_id) {
        this.plan_id = plan_id;
    }

    @JsonProperty("user_id")
    public Integer getUser_id() {
        return user_id;
    }

    @JsonProperty("user_id")
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    @JsonProperty("user_segment")
    public String getUser_segment() {
        return user_segment;
    }

    @JsonProperty("user_segment")
    public void setUser_segment(String user_segment) {
        this.user_segment = user_segment;
    }

    private void setDefaultValues(int planID, int userID) {
        if (getPlan_id() == null)
            this.setPlan_id(planID);
        if (getUser_id() == null)
            this.setUser_id(userID);
    }

    public PlanUserIncentiveMapping build(int planID, int userID) {
        setDefaultValues(planID,userID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("created_at", created_at).append("created_by", created_by).append("incentive_id", incentive_id).append("plan_id", plan_id).append("user_id", user_id).append("user_segment", user_segment).toString();
    }

}
