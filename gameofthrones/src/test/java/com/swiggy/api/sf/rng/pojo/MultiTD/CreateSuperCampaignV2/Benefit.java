package com.swiggy.api.sf.rng.pojo.MultiTD.CreateSuperCampaignV2;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.MultiTD
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "benefitId",
        "discountType",
        "level",
        "restaurantIds"
})
public class Benefit {

    @JsonProperty("benefitId")
    private Integer benefitId;
    @JsonProperty("discountType")
    private String discountType;
    @JsonProperty("level")
    private String level;
    @JsonProperty("restaurantIds")
    private List<Integer> restaurantIds = null;

    @JsonProperty("restaurantRewardMap")
    private Map<String, String> restaurantRewardMap = null;

    @JsonProperty("benefitId")
    public Integer getBenefitId() {
        return benefitId;
    }

    @JsonProperty("benefitId")
    public void setBenefitId(Integer benefitId) {
        this.benefitId = benefitId;
    }

    @JsonProperty("discountType")
    public String getDiscountType() {
        return discountType;
    }

    @JsonProperty("discountType")
    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    @JsonProperty("level")
    public String getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(String level) {
        this.level = level;
    }

    @JsonProperty("restaurantIds")
    public List<Integer> getRestaurantIds() {
        return restaurantIds;
    }

    @JsonProperty("restaurantIds")
    public void setRestaurantIds(List<Integer> restaurantIds) {
        this.restaurantIds = restaurantIds;
    }

    @JsonProperty("restaurantRewardMap")
    public Map<String, String> getRestaurantRewardMap() {
        return restaurantRewardMap;
    }

    @JsonProperty("restaurantRewardMap")
    public void setRestaurantRewardMap(Map<String, String> hm) {
        this.restaurantRewardMap = hm;
    }


    public void setDefaultValues(String benefitID, String type, String level, List<Integer> restID) {

        if (this.getBenefitId() == null)
            this.setBenefitId(Integer.parseInt(benefitID));
        if (this.getDiscountType() == null)
            this.setDiscountType(type);
        if (this.getLevel() == null)
            this.setLevel(level);
        if (this.getRestaurantIds() == null)
            this.setRestaurantIds(restID);
    }

    public Benefit build(String benefitID, String type, String level, List<Integer> restID) {
        setDefaultValues(benefitID,type,level,restID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("benefitId", benefitId).append("discountType", discountType).append("level", level).append("restaurantIds", restaurantIds).toString();
    }

}
