package com.swiggy.api.sf.checkout.pojo;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CartBuilder {
    private CreateMenuEntry createMenuEntry;

    public CartBuilder(){
        createMenuEntry= new CreateMenuEntry();
    }

    public CartBuilder cart(List<Cart> cartDetails) {
        ArrayList<Cart> cartEntries = new ArrayList<>();
        for (Cart cartEntry : cartDetails) {
            cartEntries.add(cartEntry);
        }
        createMenuEntry.setCartItems(cartEntries);
        return this;
    }

    public CartBuilder restaurant(Integer restaurant) {
        createMenuEntry.setRestaurantId(restaurant);
        return this;
    }

    public CreateMenuEntry build() throws IOException {
        return createMenuEntry;
    }


}
