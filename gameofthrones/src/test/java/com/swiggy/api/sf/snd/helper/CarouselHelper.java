package com.swiggy.api.sf.snd.helper;

import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import com.swiggy.api.sf.snd.pojo.UpdateCarousel.UpdateCarouselPOJO;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.json.JSONException;
import org.testng.Assert;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CarouselHelper {

    Initialize gameofthrones = new Initialize();

    JsonHelper jsonHelper = new JsonHelper();

    public Processor getCarouselJobById(String jobId)
    {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
        GameOfThronesService service = new GameOfThronesService("carousel", "getCarouselJobById", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] { jobId});
        errorCodeHandler(processor,"getCarouselJobById");
        return processor;
    }

    public Processor getAllCarouselJob()
    {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("carousel", "getCarouselJobAll", gameofthrones);
        Processor processor = new Processor(service, requestHeaders,null);
        errorCodeHandler(processor,"getCarouselJobAll");
        return processor;
    }

    public Processor deleteCarouselJobById(String jobId)
    {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("carousel", "deleteCarouselJobById", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] {jobId});
        errorCodeHandler(processor,"deleteCarouselJobById");
        return processor;
    }

    public Processor getCarouselById(String bannerId)
    {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("carousel", "getCarouselById", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] {bannerId});
        errorCodeHandler(processor,"getCarouselById");
        return processor;
    }

    public Processor getCarouselByPage(String pageNumber, String pageSize,String type)
    {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("carousel", "getCarouselByPage", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] {pageNumber,pageSize,type});
        errorCodeHandler(processor,"getCarouselByPage");
        return processor;
    }

    public Processor refreshCacheForRestId(String restId){
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("carousel", "refreshCarouselData", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] {restId});
        errorCodeHandler(processor,"refreshCarouselData");
        return processor;
    }

    public Processor createCarousel(CarouselPOJO carouselPOJO) throws IOException {

        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        hashmap.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
        GameOfThronesService got = new GameOfThronesService("carousel", "createCarousel", gameofthrones);
        Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(carouselPOJO)});
        errorCodeHandler(processor, "createCarousel API");
        return processor;
    }

    public Processor createCarouselWithBulkPayload(ArrayList<CarouselPOJO> carouselPOJOS) throws IOException {
        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        hashmap.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
        GameOfThronesService got = new GameOfThronesService("carousel", "createCarouselWithBulkPayload", gameofthrones);
        Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(carouselPOJOS)});
        errorCodeHandler(processor, "createCarouselWithBulkPayload API");
        return processor;
    }

    public Processor getAllCarousel(String type)
    {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("carousel", "getCarouselAll", gameofthrones);
        Processor processor = new Processor(service, requestHeaders,null,new String[]{type});
        errorCodeHandler(processor,"getCarouselAll");
        return processor;
    }

    public Processor updateCarousel(UpdateCarouselPOJO updateCarouselPOJO) throws IOException {

        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        hashmap.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
        GameOfThronesService got = new GameOfThronesService("carousel", "updateCarousel", gameofthrones);
        Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(updateCarouselPOJO)});
        errorCodeHandler(processor, "Update carousel API");
        return processor;
    }
    public Processor createCarouselWithBulkCSV(String filePath) throws JSONException {
        GameOfThronesService service = new GameOfThronesService("carousel", "createCarouselWithBulkCSV", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Accept", "application/json");

        HashMap<String, String> formData = new HashMap<>();
        formData.put("file", filePath);

        Processor processor =  new Processor(service, headers, null, null, formData);
        return processor;
    }

    public Processor disableCarouselBulk(ArrayList<Integer> bannerIds) throws IOException {

        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        hashmap.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
        GameOfThronesService got = new GameOfThronesService("carousel", "disableCarouselBulk", gameofthrones);
        Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(bannerIds)});
        errorCodeHandler(processor, "delete bulk carousel API");
        return processor;
    }

    public void errorCodeHandler(Processor processor, String api) {
        if (processor.ResponseValidator.GetResponseCode() == 500) {
            Assert.assertTrue(false, "Api is down=" + api);
        }
    }

}
