package com.swiggy.api.sf.rng.constants;


public class CacheConstants {
    public static final String CAMPAIGN_REDIS_KEY = "TD_CAMPAIGN_ID_";
    public static final String  RESTAURANT_REDIS_KEY= "TD_RESTAURANT_ID_";
    public  static final String TD_ENABLED_REDIS_KEY = "enableTDEnabledRestaurantCache";
    public static final String TD_OPERATION_REDIS_KEY ="TD_OPERATION_ID_";
    public static final String TD_USER_MAP_REDIS_KEY ="TD_USER_MAP_";
    public static final String RULE_REDIS_KEY = "TD_RULE_ID_";
    public static final String COUPON_USES_COUNT_REDIS_KEY ="COUPONS_USAGE_COUNT_";


}
