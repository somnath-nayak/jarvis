package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class VERTICALGRID {

    private String type;
    private String title;
    private Integer elementsPerRow;
    private Integer minElements;
    private Integer maxElements;

    /**
     * No args constructor for use in serialization
     *
     */
    public VERTICALGRID() {
    }

    /**
     *
     * @param title
     * @param elementsPerRow
     * @param maxElements
     * @param minElements
     * @param type
     */
    public VERTICALGRID(String type, String title, Integer elementsPerRow, Integer minElements, Integer maxElements) {
        super();
        this.type = type;
        this.title = title;
        this.elementsPerRow = elementsPerRow;
        this.minElements = minElements;
        this.maxElements = maxElements;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getElementsPerRow() {
        return elementsPerRow;
    }

    public void setElementsPerRow(Integer elementsPerRow) {
        this.elementsPerRow = elementsPerRow;
    }

    public Integer getMinElements() {
        return minElements;
    }

    public void setMinElements(Integer minElements) {
        this.minElements = minElements;
    }

    public Integer getMaxElements() {
        return maxElements;
    }

    public void setMaxElements(Integer maxElements) {
        this.maxElements = maxElements;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("title", title).append("elementsPerRow", elementsPerRow).append("minElements", minElements).append("maxElements", maxElements).toString();
    }

}