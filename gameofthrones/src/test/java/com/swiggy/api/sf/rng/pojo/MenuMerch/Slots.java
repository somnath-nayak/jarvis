package com.swiggy.api.sf.rng.pojo.MenuMerch;

import org.codehaus.jackson.annotate.JsonProperty;

public class Slots {

    @JsonProperty("closeTime")
    private String closeTime;
    @JsonProperty("openTime")
    private String openTime;
    @JsonProperty("day")
    private String day;

    @JsonProperty("closeTime")
    public String getCloseTime ()
    {
        return closeTime;
    }
    @JsonProperty("closeTime")
    public void setCloseTime (String closeTime)
    {
        this.closeTime = closeTime;
    }

    public Slots withCloseTime(String closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    @JsonProperty("openTime")
    public String getOpenTime ()
    {
        return openTime;
    }
    @JsonProperty("openTime")
    public void setOpenTime (String openTime)
    {
        this.openTime = openTime;
    }

    public Slots withOpenTime(String openTime) {
        this.openTime = openTime;
        return this;
    }

    @JsonProperty("day")
    public String getDay ()
    {
        return day;
    }
    @JsonProperty("day")
    public void setDay (String day)
    {
        this.day = day;
    }

    public Slots withDay(String day) {
        this.day = day;
        return this;
    }

    public Slots (){

    }
    public Slots(String openTime, String closeTime, String day ){
        super();
        this.closeTime = closeTime;
        this.day = day;
        this.openTime = openTime;
    }
    @Override
    public String toString()
    {
        return "ClassPojo [closeTime = "+closeTime+", openTime = "+openTime+", day = "+day+"]";
    }
}
