package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.SuperHelper;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.SuperDP;
import com.swiggy.api.sf.snd.helper.OrderPlace;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.pojo.Super.AddSc;
import com.swiggy.api.sf.snd.pojo.Super.AddSchemaBuilder;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SandSuperTest extends SuperDP {
    SANDHelper helper= new SANDHelper();
    OrderPlace orderPlace= new OrderPlace();
    JsonHelper jsonHelper= new JsonHelper();
    RngHelper rngHelper= new RngHelper();
    SuperHelper superHelper= new SuperHelper();

    @Test(dataProvider = "userProfileSuper", description = "verifying data for a super user")
    public void userProfileSignUp( String lat, String lng, String addRqd, String detailsRqd, String... optionalKey){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        String response=helper.userProfileSuperGet(hMap.get("tid"), hMap.get("token"),addRqd, detailsRqd,optionalKey).ResponseValidator.GetBodyAsText();
        verifyDetails(hMap.get("userId"),response,optionalKey);
    }


    @Test(dataProvider = "userProfileNonSuper", description = "verifying data for a non super user")
    public void userProfile(String name, String password, String addRqd, String detailsRqd, String... optionalKey){
        HashMap<String, String> hMap=helper.createUser(name, helper.getMobile(),helper.getemailString(), password);
        String response=helper.userProfileSuperGet(hMap.get("tid"), hMap.get("token"),addRqd, detailsRqd,optionalKey).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..superStatus"),"NOT_SUPER", "status are not same");
    }

    @Test(dataProvider = "userProfileSuper", description = "verifying data for a super user")
    public void userProfileOptional(String lat, String lng, String addRqd, String detailsRqd, String... optionalKey){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        String response=helper.userProfileSuperGet(hMap.get("tid"), hMap.get("token"),addRqd, detailsRqd,optionalKey).ResponseValidator.GetBodyAsText();
        verifyDetails(hMap.get("userId"),response,optionalKey);
    }


    @Test(dataProvider = "userProfileSuperWithoutToken", description = "verifying data for a non super user")
    public void userProfileWithoutToken(String name, String password, String addRqd, String detailsRqd, String... optionalKey){
        HashMap<String, String> hMap=helper.createUser(name, helper.getMobile(),helper.getemailString(), password);
        String response=helper.userProfileSuperGet(hMap.get("tid"), hMap.get("toke"),addRqd, detailsRqd,optionalKey).ResponseValidator.GetBodyAsText();
        try{
            Assert.assertEquals(helper.JsonString(response, "$.statusMessage"),"Session expired. Please login again.", "status are not same");
        }
        catch (NullPointerException e){
            Assert.assertEquals(helper.JsonString(response, "$.statusMessage"),"Session expired. Please login again.", "status are not same");
        }
    }


    @Test(dataProvider = "userProfileWithoutOptional", description = "verifying data for a super user without optional keys")
    public void userProfileWithoutOptionalKey(String lat, String lng, String addRqd, String detailsRqd){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        String response=helper.userProfileSuperGet(hMap.get("tid"), hMap.get("toke"),addRqd, detailsRqd).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(helper.JsonString(response, "$.statusMessage"),"Session expired. Please login again.", "status are not same");
        Assert.assertNull(JsonPath.read(response, "$.data"),"status not same");
    }

    @Test(dataProvider = "userProfileValidNNonValid", description = "verifying data for a super user with valid and non valid optional keys")
    public void userProfileWithValidAndNonValid(String lat, String lng, String addRqd, String detailsRqd,String msg, String... optionalKeys){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        String response=helper.userProfileSuperGet(hMap.get("tid"), hMap.get("token"),addRqd, detailsRqd).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(helper.JsonString(response, "$.statusMessage"),SANDConstants.msg, "status are not same");
        try{
            Assert.assertEquals(helper.JsonString(response, "$.data"),null, "status are not same");
            //data is not null here
        }
        catch (Exception e){
            e.printStackTrace();
        }

        Assert.assertNotNull(JsonPath.read(response, "$.data"),"status not same");

    }

    @Test(dataProvider = "userProfileAdminSuper", description = "verifying data for a super user in profile admin with multiple optional keys")
    public void userProfileAdmin(String lat, String lng,String param, String... optionalKey){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        String response=helper.userProfileAdminSuper(param, hMap.get("userId"),optionalKey).ResponseValidator.GetBodyAsText();
        for( String ke:optionalKey) {
            String cache = helper.superCache(hMap.get("userId"),ke);
            System.out.println(cache);
            if(ke.equalsIgnoreCase("IS_SUPER")) {
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..schemaVersion"), helper.JsonString(cache, "$.meta.schemaVersion"), "schema version are not same");
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..superStatus"), helper.JsonString(cache, "$.value.superStatus"), "status are not same");
            }
            if(ke.equalsIgnoreCase("SUPER_DETAILS")){
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.SUPER_DETAILS..schemaVersion"), helper.JsonString(cache, "$.meta.schemaVersion"), "schema version are not same");
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.SUPER_DETAILS..type"), helper.JsonString(cache, "$.value.benefits..type"), "status are not same");
            }
        }
    }

    @Test(dataProvider = "userProfileAdmin1Optional", description = "verifying data for a super user in profile admin only 1 optional keys")
    public void userProfileAdminWith1Optional(String lat, String lng,String param, String... optionalKey) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        String response = helper.userProfileAdminSuper(param, hMap.get("userId"), optionalKey).ResponseValidator.GetBodyAsText();
        for (String ke : optionalKey) {
            String cache = helper.superCache(hMap.get("userId"), ke);
            System.out.println(cache);
            if (ke.equalsIgnoreCase("IS_SUPER")) {
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..schemaVersion"), helper.JsonString(cache, "$.meta.schemaVersion"), "schema version are not same");
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..superStatus"), helper.JsonString(cache, "$.value.superStatus"), "status are not same");
            }
            if (ke.equalsIgnoreCase("SUPER_DETAILS")) {
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.SUPER_DETAILS..schemaVersion"), helper.JsonString(cache, "$.meta.schemaVersion"), "schema version are not same");
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.SUPER_DETAILS..type"), helper.JsonString(cache, "$.value.benefits..type"), "status are not same");
            }
        }
    }


    @Test(dataProvider = "userProfileAdminSuper", description = "verifying data for a non super user in profile admin")
    public void userProfileAdminWithoutSuper(String lat, String lng,String param, String... optionalKey) {
        HashMap<String, String> hMap = helper.createUser(SANDConstants.name, helper.getMobile(), helper.getemailString(), SANDConstants.password);
        String response = helper.userProfileAdminSuper(param, hMap.get("userId"), optionalKey).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..superStatus"),"NOT_SUPER", "status are not same");
    }




    @Test(dataProvider = "userProfileAdminSuper", description = "verifying data for a non super user in profile admin")
    public void userProfileAdminWithoutAuth(String lat, String lng,String param, String... optionalKey) {
        HashMap<String, String> hMap = helper.createUser(SANDConstants.name, helper.getMobile(), helper.getemailString(), SANDConstants.password);
        String response = helper.userProfileAdminSuperWithoutAuth(param, hMap.get("userId"), optionalKey).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(helper.JsonString(response, "$.status"), "401", "status is not 401");
        try {
            Assert.assertEquals(helper.JsonString(response, "$.error"), "Unauthorized", "status are not same");
        } catch (NullPointerException e) {
            Assert.assertEquals(helper.JsonString(response, "$.error"), "Unauthorized", "status are not same");
        }
    }


    @Test(dataProvider = "userProfileAdminSuperWithoutOptional", description = "verifying keys for a super user in profile,without optional keys")
    public void userProfileAdminWithoutOptionalKeys(String lat, String lng,String param){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        Processor p=helper.userProfileAdminSuper(param, hMap.get("userId"));
        try {
            Assert.assertFalse(p.ResponseValidator.DoesNodeExists("data..SUPER_DETAILS"), "node is present");
            Assert.assertFalse(p.ResponseValidator.DoesNodeExists("data..IS_SUPER"), "node is present");
        }
        catch (Exception e){
            Assert.assertFalse(p.ResponseValidator.DoesNodeExists("data..SUPER_DETAILS"), "node is present");
            Assert.assertFalse(p.ResponseValidator.DoesNodeExists("data..IS_SUPER"), "node is present");
        }
    }


    @Test(dataProvider = "userProfileInternal", description = "verifying data for a super user in user Internal")
    public void userProfileInternal(String lat, String lng,String param, String... optionalKey){
        String mobile=helper.getMobile();
        String email=helper.getemailString();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile,email, SANDConstants.password);
        superHelper.createSuperUserWithPublicPlan(hMap.get("userId"));
        if(param.equalsIgnoreCase("id")) {
            String response = helper.userInternalSuper(param, hMap.get("userId"), optionalKey).ResponseValidator.GetBodyAsText();
            verifyDetails(hMap.get("userId"),response,optionalKey);
        }
        if(param.equalsIgnoreCase("mobile")){
            String response = helper.userInternalSuper(param, mobile, optionalKey).ResponseValidator.GetBodyAsText();
            verifyDetails(hMap.get("userId"),response,optionalKey);
        }
        if(param.equalsIgnoreCase("email")){
            String response = helper.userInternalSuper(param,email, optionalKey).ResponseValidator.GetBodyAsText();
            verifyDetails(hMap.get("userId"),response,optionalKey);
        }
    }


    @Test(dataProvider = "userProfileInternal", description = "verifying data for a super user in user Internal but without optional key")
    public void userProfileInternalWithoutOptionalKey(String lat, String lng,String param, String... optionalKey){
        String mobile=helper.getMobile();
        String email=helper.getemailString();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile,email, SANDConstants.password);
        orderPlace.createOrderSuper(hMap.get("tid"), hMap.get("token"),lat, lng, SANDConstants.plan);
        if(param.equalsIgnoreCase("id")) {
            String response = helper.userInternalSuper(param, hMap.get("userId")).ResponseValidator.GetBodyAsText();
            Assert.assertTrue(helper.JsonString(response, "$.data.optional_map").replaceAll("\\}|\\{", "").isEmpty(),"optionalMap is not empty");
        }
        if(param.equalsIgnoreCase("mobile")){
            String response = helper.userInternalSuper(param, mobile).ResponseValidator.GetBodyAsText();
            Assert.assertTrue(helper.JsonString(response, "$.data.optional_map").replaceAll("\\}|\\{", "").isEmpty(),"optionalMap is not empty");
        }
        if(param.equalsIgnoreCase("email")){
            String response = helper.userInternalSuper(param,email).ResponseValidator.GetBodyAsText();
            Assert.assertTrue(helper.JsonString(response, "$.data.optional_map").replaceAll("\\}|\\{", "").isEmpty(),"optionalMap is not empty");
        }
    }

    @Test(dataProvider = "userProfileInternal", description = "verifying data for a super user iin user Internal")
    public void userProfileInternalNotSubscribed(String lat, String lng,String param, String... optionalKey){
        String mobile=helper.getMobile();
        String email=helper.getemailString();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile,email, SANDConstants.password);
        //orderPlace.createOrderSuper(hMap.get("tid"), hMap.get("token"),lat, lng, SANDConstants.plan);
        if(param.equalsIgnoreCase("id")) {
            String response = helper.userInternalSuper(param, hMap.get("userId"), optionalKey).ResponseValidator.GetBodyAsText();
            verifDetailsNotSuper(response,optionalKey);
        }
        if(param.equalsIgnoreCase("mobile")){
            String response = helper.userInternalSuper(param, mobile, optionalKey).ResponseValidator.GetBodyAsText();
            verifDetailsNotSuper(response,optionalKey);
        }
        if(param.equalsIgnoreCase("email")){
            String response = helper.userInternalSuper(param,email, optionalKey).ResponseValidator.GetBodyAsText();
            verifDetailsNotSuper(response,optionalKey);
        }

    }

    @Test(dataProvider = "userProfileInternalOptional", description = "verifying data for a super user in user Internal optional")
    public void userProfileInternalOptional(String lat, String lng ,String... optionalKey){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        String response=helper.userInternalOptional(hMap.get("userId"),optionalKey).ResponseValidator.GetBodyAsText();
        verifyDetails(hMap.get("userId"),response,optionalKey);
    }

    @Test(dataProvider = "userProfileInternalOptional", description = "verifying data for a super user in user Internal optional")
    public void userProfileInternalOptionalWithoutAuth(String lat, String lng ,String... optionalKey){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        String response=helper.userInternalOptional(hMap.get("userId"),optionalKey).ResponseValidator.GetBodyAsText();
        verifyDetails(hMap.get("userId"),response,optionalKey);
    }

    //check this
    @Test(dataProvider = "userProfileInternalOptional", description = "verifying data for a super user in user Login")
    public void superLogin(String lat, String lng ,String... optionalKey){
        String mobile=helper.getMobile();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile,helper.getemailString(), SANDConstants.password);
        orderPlace.createOrderSuper(hMap.get("tid"), hMap.get("token"),lat, lng, SANDConstants.plan);
        String response=helper.login1(mobile,SANDConstants.password).ResponseValidator.GetBodyAsText();
        verifyDetails(hMap.get("userId"),response,optionalKey);
    }

    @Test(dataProvider = "userProfileInternalOptional", description = "verifying data for a super user in user Login")
    public void nonSuperLogin(String lat, String lng ,String... optionalKey){
        String mobile=helper.getMobile();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile,helper.getemailString(), SANDConstants.password);
        String response=helper.login1(mobile,SANDConstants.password).ResponseValidator.GetBodyAsText();
        verifDetailsNotSuper(response,optionalKey);
    }


    @Test(dataProvider = "userInternalOptional", description = "verifying data for a super user in user Internal")
    public void userProfileOptionalData(String lat, String lng ,String... optionalKey){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        String response=helper.userOptional(hMap.get("tid"), hMap.get("token"),hMap.get("userId"),optionalKey).ResponseValidator.GetBodyAsText();
        verifyDetails(hMap.get("userId"),response,optionalKey);
    }

    @Test(dataProvider = "deleteCache", description = "verifying data for a super user in user Internal")
    public void deleteCache(String lat, String lng ,String... optionalKey){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> nonSuper=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(), SANDConstants.password);
        for(String option: optionalKey){
            String cacheData=helper.superCache(hMap.get("userId"),option);
            String response=helper.deleteCacheKey(hMap.get("userId"), option).ResponseValidator.GetBodyAsText();

            Assert.assertEquals(helper.JsonString(response,"$.statusMessage"), "Deleted successfully", "Not able to delete");

        }
    }


    @Test(dataProvider = "addSchema",description = "Validating ")
    public void addSchema(String name, String password){
        HashMap<String, String> hMap=helper.createUser(name, helper.getMobile(),helper.getemailString(), password);
        try{
            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/addSchema.txt");
            AddSc add = new AddSchemaBuilder().key("SUPER")
                    .schmeaVersion("1.1.1").jsonSchema(jsonschema).author("cc").defaultValue("{"+"superValue"+":"+"WAS_SUPER"+"}").build();
            helper.addSchema(hMap.get("tid"), hMap.get("token"),jsonHelper.getObjectToJSON(add));
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    @Test(dataProvider = "latLong1", description = "creating free del collection and verify it will not work super user")
    public void freeDelColOnAggregator(String lat ,String lng, String identifier){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        try {
            List<String> firstNElementsList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", l.get(i), false, false, false, "ZERO_DAYS_DORMANT", false);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        helper.collectionQuery(identifier);
        String id= helper.collectionId().get("id").toString();
        helper.collection(id);
        String aggregatorAfterSuper=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id").split(","));
        helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id");
        Assert.assertFalse(collectionIds.contains(id), "collection is present for super user");
    }




    @Test(dataProvider = "freeDelAgg",description = "check super user will not get any free del on aggregator and verify it will not work super user")
    public void superOnAggregator(String lat ,String lng, String identifier, String type, String su){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> nonSuper= helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        String aggregatorAfterSuper=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        String[] restIds = helper.JsonString(aggregatorAfterSuper, "$.data.restaurants[*].id").split(",");
        for(String restId : restIds) {
            Assert.assertFalse(helper.JsonString(aggregatorAfterSuper, "$.data.." + restId + ".tradeDiscountInfo..discountType").contains(type), "not free delivery restaurant");
            Assert.assertTrue(helper.JsonString(aggregatorAfterSuper, "$.data.." + restId + ".tradeDiscountInfo..operationType").contains(su) , "not a super user");
        }

        String aggregatorWithoutSuper=helper.aggregator(new String[]{lat,lng}, nonSuper.get("tid"), nonSuper.get("token")).ResponseValidator.GetBodyAsText();
        String[] restIdNoSuper = helper.JsonString(aggregatorWithoutSuper, "$.data.restaurants[*].id").split(",");
        for(String restId : restIdNoSuper) {
            Assert.assertFalse(helper.JsonString(aggregatorWithoutSuper, "$.data.." + restId + ".tradeDiscountInfo..operationType").contains(su) , "not a super user");
        }
    }


    @Test(dataProvider = "percentageAgg",description = "check super user will not get any free del on aggregator and verify it will not work super user")
    public void percentageOnAggregator(String lat ,String lng, String type, String su, String percentage){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> nonSuper= helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        String aggregatorAfterSuper=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        List<String> restIds = Arrays.asList(helper.JsonString(aggregatorAfterSuper, "$.data.restaurants[*].id").split(","));
        List<String> firstNRest = restIds.stream().limit(8).collect(Collectors.toList());
        try {
            rngHelper.createPercentageTD(firstNRest.get(5));
            String aggregatorAfterPercentage=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
            Assert.assertTrue(helper.JsonString(aggregatorAfterPercentage, "$.data.." + firstNRest.get(5) + ".tradeDiscountInfo..discountType").contains(percentage), "not percentage restaurant");
            Assert.assertFalse(helper.JsonString(aggregatorAfterPercentage, "$.data.." + firstNRest.get(5) + ".tradeDiscountInfo..operationType").contains(su) , "not a super user");
        }
        catch(Exception e){
            e.printStackTrace();
        }

        String aggWithoutSuper=helper.aggregator(new String[]{lat,lng}, nonSuper.get("tid"), nonSuper.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(aggWithoutSuper, "$.data.." + firstNRest.get(5) + ".tradeDiscountInfo..operationType").contains(su) , "not a super user");
    }

    @Test(dataProvider = "freeAgg", description = "creating free del TD and verify it will not work super user")
    public void freeDelOnAggregator(String lat ,String lng, String identifier, String freeDel, String su){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> nonSuper= helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        String aggregatorAfterSuper=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        List<String> restIds = Arrays.asList(helper.JsonString(aggregatorAfterSuper, "$.data.restaurants[*].id").split(","));
        List<String> firstNRest = restIds.stream().limit(8).collect(Collectors.toList());
        HashMap<String, String> discountResponse= new HashMap<>();
        try {
            discountResponse=rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", firstNRest.get(7), false, false, false, "ZERO_DAYS_DORMANT", false);
            String getTid= RngHelper.getTradeDiscount(discountResponse.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(helper.JsonString(getTid, "$.data.ruleDiscount.type"), freeDel, "No discounts present or different type of discount is there");
            System.out.println("data"+hMap+ " "+firstNRest.get(7));
            String aggregatorAfterFreeDel=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
            Assert.assertFalse(helper.JsonString(aggregatorAfterFreeDel, "$.data.." + firstNRest.get(7) + ".tradeDiscountInfo..discountType").contains(freeDel), "not free del restaurant");
            Assert.assertFalse(helper.JsonString(aggregatorAfterFreeDel, "$.data.." + firstNRest.get(7) + ".tradeDiscountInfo..operationType").contains(su) , "not a super user");
        }
        catch(Exception e){
            String getTid1= RngHelper.getTradeDiscount(discountResponse.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(helper.JsonString(getTid1, "$.data.ruleDiscount.type"), freeDel, "No discounts present or different type of discount is there");
        }
        String aggWithoutSuper=helper.aggregator(new String[]{lat,lng}, nonSuper.get("tid"), nonSuper.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(aggWithoutSuper, "$.data.." + firstNRest.get(7) + ".tradeDiscountInfo..operationType").contains(su) , "not a super user");
    }


    @Test(dataProvider = "freeBieAgg", description = "creating free bie on aggregator and verify it will not work super user")
    public void freeBieOnAggregator(String lat ,String lng,  String type, String su, String freeBie){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> nonSuper= helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        String aggResponse=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        List<String> restIds = Arrays.asList(helper.JsonString(aggResponse, "$.data.restaurants[*].id").split(","));
        List<String> firstNRest = restIds.stream().limit(7).collect(Collectors.toList());

        HashMap<String, String> discountResponse= new HashMap<>();
        try {
            String freebieItem=orderPlace.getRestaurantMenu(firstNRest.get(6),null, null);
            System.out.println("rest id"+firstNRest.get(6));
            discountResponse=rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel("10", firstNRest.get(6),freebieItem);
            String getTid= RngHelper.getTradeDiscount(discountResponse.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(helper.JsonString(getTid, "$.data.ruleDiscount.type"), freeBie, "No discounts present or different type of discount is there");
            String aggregatorAfterFreebie=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
            Assert.assertFalse(helper.JsonString(aggregatorAfterFreebie, "$.data.." + firstNRest.get(6) + ".tradeDiscountInfo..discountType").contains(freeBie), "not freebie restaurant");
            Assert.assertFalse(helper.JsonString(aggregatorAfterFreebie, "$.data.." + firstNRest.get(6) + ".tradeDiscountInfo..operationType").contains(su) , "not a super user");
        }
        catch(Exception e){
            String getTid1= RngHelper.getTradeDiscount(discountResponse.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(helper.JsonString(getTid1, "$.data.ruleDiscount.type"), freeBie, "No discounts present or different type of discount is there");
        }

        String aggWithoutSuper=helper.aggregator(new String[]{lat,lng}, nonSuper.get("tid"), nonSuper.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(aggWithoutSuper, "$.data.." + firstNRest.get(6) + ".tradeDiscountInfo..operationType").contains(su) , "not a super user");

    }

    @Test(dataProvider = "latLong1", description = "check and verify free del on menu for both it will not work super user")
    public void menuSuper(String lat ,String lng, String identifier){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> userNotSuper=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> restList = l.stream().limit(1).collect(Collectors.toList());
        String response=helper.menuV4RestId(restList.get(0),lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();

        Assert.assertFalse(helper.JsonString(response,"$.data.tradeCampaignHeaders..operationType").contains("SUPER"), "user is super");
        String response1=helper.menuV4RestId(restList.get(0),lat,lng, userNotSuper.get("tid"), userNotSuper.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(response1,"$.data.tradeCampaignHeaders..operationType").contains("SUPER"), "user is not super");
    }


    @Test(dataProvider = "freeDelMenu", description = "creating free del discount and verifying on menu for super nad non super user")
    public void freeDelOnMenu(String lat ,String lng, String freeDel, String su ,String rest){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
       HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(4).collect(Collectors.toList());
        try {
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", firstNElementsList.get(i), false, false, false, "ZERO_DAYS_DORMANT", false);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        String responseSuper=helper.menuV4RestId(firstNElementsList.get(3),lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(Arrays.asList(helper.JsonString(responseSuper,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(freeDel), "free delivery is there");
        Assert.assertFalse(Arrays.asList(helper.JsonString(responseSuper,"$.data.tradeCampaignHeaders..operationType").split(",")).contains(su), "free delivery is there");
        String response1=helper.menuV4RestId(firstNElementsList.get(3),lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(helper.JsonString(response1,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(freeDel), "not free delivery");
        Assert.assertTrue(Arrays.asList(helper.JsonString(response1,"$.data.tradeCampaignHeaders..operationType").split(",")).contains(rest), "not Restaurant type");
    }


    @Test(dataProvider = "percentageMenu", description = "creating % discount and verify on menu, it will not work super user")
    public void percentageOnMenu(String lat ,String lng, String freeDel, String percent, String restaurant) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(5).collect(Collectors.toList());
        try {
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createPercentageTD(firstNElementsList.get(i));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        String response=helper.menuV4RestId(firstNElementsList.get(3),lat,lng,hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(freeDel), "not free delivery");
        Assert.assertTrue(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(percent), "not free delivery");

        Assert.assertFalse(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..operationType").split(",")).contains("SUPER"), "not super");
        String response1=helper.menuV4RestId(firstNElementsList.get(3),lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();

        Assert.assertEquals(helper.JsonString(response1,"$.data.tradeCampaignHeaders..discountType"), percent, "not percentage discount");
        Assert.assertEquals(helper.JsonString(response1,"$.data.tradeCampaignHeaders..operationType"), restaurant, "operation is not RESTAURANT type");
    }




    @Test(dataProvider = "flatMenu", description = "creating flat discount and verify on menu, it will not work super user")
    public void flatOnMenu(String lat ,String lng, String freeDel, String flat, String rest){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(3).collect(Collectors.toList());
        try {
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", l.get(i),"10");
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.menuV4RestId(firstNElementsList.get(2),lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(freeDel), "not free delivery");
        Assert.assertTrue(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "not free delivery");
        String response1=helper.menuV4RestId(firstNElementsList.get(2),lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(helper.JsonString(response1,"$.data.tradeCampaignHeaders..operationType").split(",")).contains(rest), "not super");
        Assert.assertTrue(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "not free delivery");
    }


    @Test(dataProvider = "freebieMenu", description = "creating freebie discount and verify on menu, it will not work super user")
    public void freebieOnMenu(String lat ,String lng, String freeDel, String freebie, String opnType){
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(1).collect(Collectors.toList());
        try {
            String freebieItem=orderPlace.getRestaurantMenu(firstNElementsList.get(0),null, null);
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel("10", l.get(i),freebieItem);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.menuV4RestId(firstNElementsList.get(0),lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(freeDel), "not free delivery");
        Assert.assertTrue(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(freebie), "not free freebie");
        String response1=helper.menuV4RestId(firstNElementsList.get(0),lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(helper.JsonString(response1,"$.data.tradeCampaignHeaders..operationType").split(",")).contains(opnType), "not super");
        Assert.assertTrue(Arrays.asList(helper.JsonString(response1,"$.data.tradeCampaignHeaders..discountType").split(",")).contains("Freebie"), "not free delivery");
    }



    @Test(dataProvider = "freebieSearch", description = "creating freebie discount and verify on menu, it will not work super user")
    public void freebieOnSearch(String lat ,String lng, String freeDel, String freebie,String su, String opnType) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(1).collect(Collectors.toList());
        String re= helper.menuV4RestId(firstNElementsList.get(0),lat,lng).ResponseValidator.GetBodyAsText();
        String name=helper.JsonString(re,"$.data.name").replace(" ","%");
        try {
            String freebieItem=orderPlace.getRestaurantMenu(firstNElementsList.get(0),hMap.get("tid"),hMap.get("token"));
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel("10", l.get(i),freebieItem);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freebie), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        String menuWithoutSuper=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freebie), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
    }


    @Test(dataProvider = "freebieSearch", description = "creating freebie discount and verify on menu, it will not work super user")
    public void freebieOnSearchExpiry(String lat ,String lng, String freeDel, String freebie,String su, String opnType) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(1).collect(Collectors.toList());
        String re= helper.menuV4RestId(firstNElementsList.get(0),lat,lng).ResponseValidator.GetBodyAsText();
        String name=helper.JsonString(re,"$.data.name").replace(" ", "%");
        try {
            String freebieItem=orderPlace.getRestaurantMenu(firstNElementsList.get(0),hMap.get("tid"),hMap.get("token"));
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel("10", l.get(i),freebieItem);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freebie), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        String menuWithoutSuper=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freebie), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        try {
            Thread.sleep(61000);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String responseDiscountEnds=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(responseDiscountEnds,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(responseDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        String menuWithoutSuperDiscountEnds=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
    }



    @Test(dataProvider = "percentSearch", description = "creating percent discount and verify on search, it will not work super user")
    public void percentOnSearch(String lat ,String lng, String freeDel, String percent,String su, String opnType) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(1).collect(Collectors.toList());
        String re= helper.menuV4RestId(firstNElementsList.get(0),lat,lng).ResponseValidator.GetBodyAsText();
        String name=helper.JsonString(re,"$.data.name");
        try {
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createPercentageTD( l.get(i));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(percent), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        String menuWithoutSuper=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(percent), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
    }


    @Test(dataProvider = "percentSearch", description = "creating percent discount and verify on search, it will not work super user")
    public void percentOnSearchExpiry(String lat ,String lng, String freeDel, String percent,String su, String opnType) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(2).collect(Collectors.toList());
        String re= helper.menuV4RestId(firstNElementsList.get(1),lat,lng).ResponseValidator.GetBodyAsText();
        String name=helper.JsonString(re,"$.data.name").replace(" ", "%");
        try {
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createPercentageTD( l.get(i));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(percent), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        String menuWithoutSuper=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(percent), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        try {
            Thread.sleep(61000);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String responseDiscountEnds=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(responseDiscountEnds,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(responseDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        String menuWithoutSuperDiscountEnds=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds," $.data..tradeCampaignHeaders..discountType").contains(percent), "Not percentage discount");
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
    }



    @Test(dataProvider = "flatSearch", description = "creating falt discount and verify on search, it will not work super user")
    public void flatOnSearch(String lat ,String lng, String freeDel, String flat,String su, String opnType) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(1).collect(Collectors.toList());
        String re= helper.menuV4RestId(firstNElementsList.get(0),lat,lng).ResponseValidator.GetBodyAsText();
        String name=helper.JsonString(re,"$.data.name");
        try {
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", l.get(i),"50");
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(flat), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        String menuWithoutSuper=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(flat), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
    }


    @Test(dataProvider = "flatSearch", description = "creating falt discount and verify on search, it will not work super user")
    public void flatOnSearchExpiry(String lat ,String lng, String freeDel, String flat,String su, String opnType) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(1).collect(Collectors.toList());
        String re= helper.menuV4RestId(firstNElementsList.get(0),lat,lng).ResponseValidator.GetBodyAsText();
        String name=helper.JsonString(re,"$.data.name");
        try {
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", l.get(i),"50");
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(flat), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        String menuWithoutSuper=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(flat), "No free delivery on the product");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        try {
            Thread.sleep(61000);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String responseDiscountEnds=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(responseDiscountEnds,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(responseDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        String menuWithoutSuperDiscountEnds=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");

    }



    @Test(dataProvider = "freeDelSearch", description = "creating free del discount on rest and verify on search, it will not work super user")
    public void freeDelOnSearch(String lat ,String lng, String freeDel, String flat,String su, String opnType) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(1).collect(Collectors.toList());
        String re= helper.menuV4RestId(firstNElementsList.get(0),lat,lng).ResponseValidator.GetBodyAsText();
        String name=helper.JsonString(re,"$.data.name");
        try {
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", l.get(i+1), false, false, false, "ZERO_DAYS_DORMANT", false);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        String menuWithoutSuper=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");

    }


    @Test(dataProvider = "freeDelSearch", description = "creating free del discount on rest and verify on search, remove it for some time, it will not work super user")
    public void verifyFreeDelExpiryOnSearch(String lat ,String lng, String freeDel, String flat,String su, String opnType) {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        HashMap<String, String> hMap1=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        List<String> firstNElementsList = l.stream().limit(1).collect(Collectors.toList());
        String re= helper.menuV4RestId(firstNElementsList.get(0),lat,lng).ResponseValidator.GetBodyAsText();
        String name=helper.JsonString(re,"$.data.name");
        try {
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", l.get(i+1), false, false, false, "ZERO_DAYS_DORMANT", false);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String response=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(response,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(response," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        String menuWithoutSuper=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuper,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertTrue(helper.JsonString(menuWithoutSuper," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
        try {
            Thread.sleep(61000);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        String responseDiscountEnds=helper.searchV2(name, lat,lng, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(responseDiscountEnds,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(responseDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        String menuWithoutSuperDiscountEnds=helper.searchV2(name, lat,lng, hMap1.get("tid"), hMap1.get("token")).ResponseValidator.GetBodyAsText();
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(su), "User is not Super");
        Assert.assertFalse(helper.JsonString(menuWithoutSuperDiscountEnds," $.data..tradeCampaignHeaders..operationType").contains(opnType), "User is not Super");
    }


    @Test(dataProvider = "percentageCol", description = "creating TD collection and verify it will work super user")
    public void percentageColOnAggregator(String lat ,String lng, String identifier) throws InterruptedException {
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        orderPlace.createOrderSuper(hMap.get("tid"), hMap.get("token"),lat, lng, SANDConstants.plan);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        try {
            List<String> firstNElementsList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createPercentageTD(firstNElementsList.get(i));
            }
            System.out.println("list of rest ids"+firstNElementsList);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        helper.collectionQuery(identifier);
        String id= helper.collectionId().get("id").toString();
        helper.collection(id).ResponseValidator.GetBodyAsText();
        Thread.sleep(10000);
        String aggregatorAfterSuper=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id").split(","));
        helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id");
        Assert.assertNotNull(collectionIds.contains(id), "collection is not present");
    }



    @Test(dataProvider = "percentGreaterFirstCol", description = "creating TD collection and verify it will work super user")
    public void percentageColOnAggregatorFirst(String lat ,String lng, String identifier){
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        orderPlace.createOrderSuper(hMap.get("tid"), hMap.get("token"),lat, lng, SANDConstants.plan);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        try {
            List<String> firstNElementsList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createPercentageTD(firstNElementsList.get(i));
            }
            System.out.println("list of rest ids"+firstNElementsList);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        helper.collectionQuery(identifier);
        String id= helper.collectionId().get("id").toString();
        helper.collection(id).ResponseValidator.GetBodyAsText();
        String aggregatorAfterSuper=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id").split(","));
        helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id");
        Assert.assertNotNull(collectionIds.contains(id), "collection is not present");
    }



    @Test(dataProvider = "percentageDiscountLess", description = "creating TD collection and verify it will work super user")
    public void percentageColOnAggregatorDisLess(String lat ,String lng, String identifier) throws InterruptedException{
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, helper.getMobile(),helper.getemailString(),SANDConstants.password);
        orderPlace.createOrderSuper(hMap.get("tid"), hMap.get("token"),lat, lng, SANDConstants.plan);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        try {
            List<String> firstNElementsList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createPercentageTD(firstNElementsList.get(i));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        helper.collectionQuery(identifier);
        String id= helper.collectionId().get("id").toString();
        helper.collection(id).ResponseValidator.GetBodyAsText();
        Thread.sleep(4000);
        String aggregatorAfterSuper=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id").split(","));
        helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id");
        Assert.assertNotNull(collectionIds.contains(id), "collection is not present");
    }


    @Test(dataProvider = "slaCollection", description = "creating sla collection and verify it will work super user")
    public void slaCollection(String lat ,String lng, String identifier) throws InterruptedException {
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        helper.collectionQuery(identifier);
        String id= helper.collectionId().get("id").toString();
        helper.collection(id).ResponseValidator.GetBodyAsText();
        Thread.sleep(4000);
        String aggregatorAfterSuper=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id").split(","));
        helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id");
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }



    @Test(dataProvider = "longDistance", description = "creating long distance collection and verify it will work super user")
    public void longDistanceColOnAggregator(String lat ,String lng, String identifier) throws InterruptedException{
        HashMap<String, String> hMap=createSuperUser(lat,lng);
        List<String> l=orderPlace.Aggregator(new String[]{lat,lng}, null, null);
        helper.collectionQuery(identifier);
        String id= helper.collectionId().get("id").toString();
        helper.collection(id).ResponseValidator.GetBodyAsText();
        Thread.sleep(4000);
        String aggregatorAfterSuper=helper.aggregator(new String[]{lat,lng}, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id").split(","));
        helper.JsonString(aggregatorAfterSuper, "$.data.collections[*].id");
        Assert.assertNotNull(collectionIds.contains(id), "collection is not present");
    }


   /* @Test
    public void createSuperUsr(){
        String mobile= helper.getMobile();
        String pwd= SANDConstants.password;
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile,helper.getemailString(),pwd);


        String response=helper.login1(mobile,pwd).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..superStatus"), "SUPER", "status are not same");

    }*/

    public HashMap<String, String> createSuperUser(String lat, String lng){
        String mobile= helper.getMobile();
        String pwd= SANDConstants.password;
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile,helper.getemailString(),pwd);


        superHelper.createSuperUserWithPublicPlan(hMap.get("userId"));
        String response=helper.login1(mobile,pwd).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..superStatus"), "SUPER", "status are not same");
        return hMap;
    }



    public void verifDetailsNotSuper(String response, String... optionalKey){
        for (String ke : optionalKey) {
            if (ke.equalsIgnoreCase("IS_SUPER")) {
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..schemaVersion"),"1.0.0", "schema version are not same");
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..superStatus"), "NOT_SUPER", "status are not same");
            }
        }
    }


    public void verifyDetails(String userId,String response,String... optionalKey){
        for(String ke:optionalKey){
            String cache = helper.superCache(userId,ke);
            System.out.println(cache);
            if(ke.equalsIgnoreCase("IS_SUPER")) {
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..schemaVersion"), helper.JsonString(cache, "$.meta.schemaVersion"), "schema version are not same");
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.IS_SUPER..superStatus"), helper.JsonString(cache, "$.value.superStatus"), "status are not same");
            }
            if(ke.equalsIgnoreCase("SUPER_DETAILS")){
                Assert.assertEquals(helper.JsonString(response, "$.data.optional_map.SUPER_DETAILS..schemaVersion"), helper.JsonString(cache, "$.meta.schemaVersion"), "schema version are not same");
                Assert.assertEquals(helper.JsonString(response, "$.data..SUPER_DETAILS..benefits..type").split(","), helper.JsonString(cache, "$..benefits..type").split(","), "schema version are not same");

            }
        }
    }


}
