package com.swiggy.api.sf.rng.dp;

import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.SuperMultiTDHelper;
import org.testng.annotations.DataProvider;

import java.util.*;


/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.dp
 * Test Case Sheet : https://docs.google.com/spreadsheets/d/1nACo-7DvP2DefND79gjtALRGmb2gufJT1CfcpUtGxQM/edit#gid=482800156
 **/
public class SuperMultiTDDP {

    private SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();
    private RngHelper rngHelper = new RngHelper();
    private BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    public String[] discountType = {"FREE_DELIVERY", "Freebie", "Flat", "Percentage", "FinalPrice", "BXGY", "Combo"};
    public String[] discountLevel = {"SuperCartRestaurant", "SuperCart", "Restaurant", "Category", "Group", "Meal", "Subcategory", "Item"};
    public String[] benefitType = {"FREE_DELIVERY", "Freebie", "CASHBACK", "COUPON", "NONE"};
    public int restID;

    //public String userID = superMultiTDHelper.loginAndReturnUserId(MultiTDConstants.mobile,MultiTDConstants.password);
    public String planID;
    public HashMap<String, String> benefitID = new HashMap<>();
    List<Integer> restIDs = Arrays.asList(281, 1725, 1889, 217, 289, 319, 1262);
    List<Integer> restIDsNegetive = Arrays.asList(-281, -1725, -1889, -217, -289, -319, -1262);

//
//    @DataProvider(name = "createSuperTDDP")
//    public Iterator<Object[]> createSuperTDDP() throws Exception {
//=======
//>>>>>>> 870b861c8da398c106264256be616979d2d65f97


    @DataProvider(name = "multitdSuperCase")
    public Iterator<Object[]> multitdSuperCase() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;
        //1 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        //Listing/Menu - index 0: TDIL; index 1: ADI
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_FD --> TRUE \n P - S - S_FD --> TRUE"});


        //3
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD,R_FB", "R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD,R_FB", "R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD,R_FB", "R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB", "R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB", "R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_FB --> TRUE \n P - S - S_FB --> TRUE"});


        //4
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD,S_FB", "R_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FB,R_FD", "S_FB,R_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FB,R_FD", "S_FB,R_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FD", "R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FD", "R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_FD --> TRUE \n P - S - S_FB --> TRUE"});

        //5
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));

        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - S - S_FB --> TRUE \n P - S - S_FB --> FALSE"});


        //6
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - S - S_FD --> TRUE \n P - S - S_FD --> FALSE"});


        //7
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FB,S_FD", "S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FB,S_FD", "S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FB,S_FD", "S_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("NA")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("NA")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - S - S_FB --> TRUE \n P - S - S_FD --> TRUE"});

//12
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FB --> TRUE \n P - S - S_FB --> TRUE"});

        //13
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n SFO - R - R_FB --> TRUE \n P - S - S_FB --> TRUE"});

        //16
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n SFO - R - R_FB --> TRUE \n P - S - S_FB --> TRUE \n USERCUT INVALID"});


        //18 case 10
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FB --> TRUE \n P - S - S_FD --> TRUE"});

        //19
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n SFO - R - R_FB --> TRUE \n P - S - S_FD --> TRUE"});

        //20
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n RFO - R - R_FB --> TRUE \n P - S - S_FD --> TRUE"});


        //22
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n SFO - R - R_FB --> TRUE \n P - S - S_FD --> TRUE \n USERCUT INVALID"});


        //24 case 11
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FB --> TRUE \n P - S - S_FD --> TRUE"});

        //25
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n SFO - R - R_FB --> TRUE \n P - S - S_FD --> TRUE"});


        //27
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n CUS - R - R_FB --> TRUE \n P - S - S_FD --> TRUE \n USERCUT INVALID"});

        //28
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD,R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n SFO - R - R_FB --> TRUE \n P - S - S_FD --> TRUE \n USERCUT INVALID"});

        //30 case 12
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FD --> TRUE \n P - S - S_FD --> TRUE"});


        //31
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n SFO - R - R_FD --> TRUE \n P - S - S_FD --> TRUE"});

        //32
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n RFO - R - R_FD --> TRUE \n P - S - S_FD --> TRUE"});

        //33
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n CUS - R - R_FD --> TRUE \n P - S - S_FD --> TRUE \n USERCUT INVALID"});


        //35.1
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD,S_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FD,S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FD,S_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n RFO - R - R_FD --> TRUE \n P - S - S_FD --> TRUE \n USERCUT INVALID"});


//40 case 16
        System.out.println("Case" + (i = i + 1));
        System.out.println("Case 40");
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB", "S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,R_FB,S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,R_FB,S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n P - R - R_FLAT --> TRUE \n P - R - R_FB --> TRUE \n P - S - S_FD --> TRUE "});

        //41 case 16
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB", "S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FB,S_FD", "R_PERCENTAGE,R_FB,S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FB,S_FD", "R_PERCENTAGE,R_FB,S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FB", "R_PERCENTAGE,R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FB", "R_PERCENTAGE,R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n P - R - R_PERCENTAGE --> TRUE \n P - R - R_FB --> TRUE \n P - S - S_FD --> TRUE "});

        //42 case 17
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE,S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE,S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n P - R - R_PERCENTAGE --> TRUE \n P - R - R_FD --> TRUE \n P - S - S_FD --> TRUE "});

        //43 case 17
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,S_FD", "R_FLAT")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,S_FD", "R_FLAT")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,R_FD", "R_FLAT,R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,R_FD", "R_FLAT,R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n P - R - R_FLAT --> TRUE \n P - R - R_FD --> TRUE \n P - S - S_FD --> TRUE "});

        //44 case 18
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(false);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "R_FLAT")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,R_FB", "R_FLAT")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,R_FB", "R_FLAT")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,R_FB", "R_FLAT,R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,R_FB", "R_FLAT,R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n P - R - R_FLAT --> TRUE \n P - R - R_FB --> TRUE \n P - S - S_FB --> TRUE "});

        //45 case 18
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(false);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FB", "R_PERCENTAGE,R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE,R_FB", "R_PERCENTAGE,R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n P - R - R_PERCENTAGE --> TRUE \n P - R - R_FB --> TRUE \n P - S - S_FB --> FALSE "});

        //46 case 19
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "S_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE,S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE,S_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_PERCENTAGE")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_PERCENTAGE")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FB --> TRUE \n P - S - S_FD --> TRUE"});

        //47 case 19
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "S_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,S_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT", "R_FLAT")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT", "R_FLAT")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FB --> TRUE \n P - S - S_FD --> TRUE"});


        //48 case 19
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE,S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE,S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FB --> TRUE \n P - S - S_FD --> TRUE"});

        //48 case 19
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT", "R_FLAT")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT", "R_FLAT")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FB --> TRUE \n P - S - S_FD --> TRUE"});


        // 49 case 20
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "S_FD", "S_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,S_FD,S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,S_FD,S_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT", "R_FLAT")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT", "R_FLAT")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FLAT --> TRUE \n P - S - S_FD --> TRUE " +
                "\n P - S - S_FB --> TRUE"});

        // 50 case 20
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "S_FD,S_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE,S_FD,S_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE,S_FD,S_FB")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_PERCENTAGE")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_PERCENTAGE")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FLAT --> TRUE \n P - S - S_FD --> TRUE " +
                "\n P - S - S_FB --> TRUE"});


        // 51 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("S_FB", "S_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("S_FB,S_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("S_FB,S_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB", "R_FB")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FLAT --> TRUE \n P - S - S_FD --> TRUE " +
                "\n P - S - S_FB --> TRUE"});

        return obj.iterator();
    }


    @DataProvider(name = "multitdNonSuperCase")
    public Iterator<Object[]> multitdNonSuperCase() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;
        //1 case 1
        System.out.println("Case" + (i = i + 1));
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "S", "S_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));

        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FB")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FB")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FB")));


        eval.put("super", hm_s);
        eval.put("not_super", hm);

        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_FB --> TRUE \n P - S - S_FB --> FALSE"});


        //8
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_FD --> TRUE \n P - R - R_FD --> FALSE"});


        //8.1
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_FD --> TRUE \n P - R - R_FD --> FALSE"});


        //9
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_FLAT --> TRUE \n P - R - R_FLAT --> FALSE"});


        //9.1
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
//        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
//        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_FLAT --> TRUE \n P - R - R_FLAT --> FALSE"});


        //10
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_PERCENTAGE --> TRUE \n P - S - R_PERCENTAGE --> FALSE"});

        //10.1
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
//        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
//        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_PERCENTAGE --> TRUE \n P - S - R_PERCENTAGE --> FALSE"});


        //11 case 9
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n P - R - R_FB --> TRUE \n P - S - R_FB --> FALSE"});


        //36 case 13
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n P - R - R_PERCENTAGE --> TRUE \n P - R - R_FLAT --> FALSE"});


        //37 case 14
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("NA")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n P - R - R_FLAT --> TRUE \n P - R - R_PERCENTAGE --> FALSE"});


        //39 case 15
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB", "R_FD")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,R_FB,R_FD", "R_FLAT,R_FB,R_FD")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,R_FB,R_FD", "R_FLAT,R_FB,R_FD")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB", "R_FD")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT,R_FB,R_FD")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT,R_FB,R_FD")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, invalid, "\n P - R - R_FLAT --> TRUE \n P - R - R_FB --> TRUE \n P - R - R_FD --> TRUE "});


        // 39.1 case 15
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("R_FLAT")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        //obj.add(new Object[] { cases, actionAssert, eval, invalid, "\n P - R - R_FLAT --> TRUE \n P - R - R_FB --> TRUE \n P - R - R_FD --> TRUE "});


        return obj.iterator();
    }

    @DataProvider(name = "multitdEdvoCase")
    public Iterator<Object[]> multitdEdvoCase() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;


        // BXGY CASES STARTS HERE

        // 51 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("P", "M", "BXGY"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(false);
        hm_s.put("EVAL_CART", new ArrayList<>(Arrays.asList("BXGY", "BXGY")));
        hm_s.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("BXGY")));
        hm_s.put("EVAL_MENU", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_CART", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_LISTING", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_MENU", new ArrayList<>(Arrays.asList("BXGY", "BXGY")));
        eval.put("super", hm_s);
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, "\n CUS - R - R_FLAT --> TRUE \n P - S - S_FD --> TRUE " +
                "\n P - S - S_FB --> TRUE"});

        return obj.iterator();

    }

    @DataProvider(name = "multiRestFreedel")
    public Iterator<Object[]> multiRestFreedel() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FD")));

        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FD")));

        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FD")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 2 Case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FD")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 3 Case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FD")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 4 Case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("R_FD")));

        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("N/A")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));


        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }

    @DataProvider(name = "multiRestFreebie")
    public Iterator<Object[]> multiRestFreebie() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FB")));

        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FB")));

        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 2 Case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FB")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 3 Case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FB")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // 4 Case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("R_FB")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }

    @DataProvider(name = "multiRestPercentage")
    public Iterator<Object[]> multiRestPercentageCase() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case 21
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));

        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));

        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 2 Case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 3 Case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 4 Case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }

    @DataProvider(name = "multiRestFlat")
    public Iterator<Object[]> multiRestFlatCase() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case 21
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));

        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));

        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 2 Case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 3 Case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        //obj.add(new Object[] { cases, actionAssert, eval, valid, ""});

        // 4 Case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("R_FLAT")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));

        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }


    @DataProvider(name = "multiRestFlatVisited")
    public Iterator<Object[]> multiRestFlatVisited() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // 2 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }

    @DataProvider(name = "multiRestFreedelVisited")
    public Iterator<Object[]> multiRestFreedelVisited() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FD")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // 2 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }

    @DataProvider(name = "multiRestFreebieVisited")
    public Iterator<Object[]> multiRestFreebieVisited() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FB")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // 2 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FB")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }


    @DataProvider(name = "multiRestPercentageVisited")
    public Iterator<Object[]> multiRestPercentageVisited() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("RFO", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // 2 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();
    }

    @DataProvider(name = "multiRestCampaign")
    public Iterator<Object[]> multiRestCampaign() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT", "R_FB")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT", "R_FB")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT", "R_FB")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT", "R_FB")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT", "R_FB")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT", "R_FB")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // 2 case 21
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FD")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // 2 case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FD")));

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }

    @DataProvider(name = "multiRestCampaign2")
    public Iterator<Object[]> multiRestCampaign2() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FD", "R_FLAT")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 2 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FLAT", "R_FB")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 3 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FB")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 4 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FD")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FD")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FD")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FD")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FD")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE", "R_FD")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        ///// RFO...

        // 1 case
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FD")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FLAT")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 2 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FB")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FLAT")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 3 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FB")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 4 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FD")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // CUS...

        // 1 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FLAT")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FD")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 2 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FB")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FLAT")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 3 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_FB")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 4 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("P", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("R_FD")));


        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }


    @DataProvider(name = "multiRestFlatmultiUserCut")
    public Iterator<Object[]> multiRestFlatmultiUserCut() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();
        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        List<String> list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FD"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FLAT"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("R_FD")));


        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        // 2
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FB"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_PERCENTAGE"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("R_FB")));


        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // 3
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_FLAT"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FD"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("R_FLAT")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("R_FLAT")));


        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FD")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        // 2
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "R", "R_PERCENTAGE"));
        cases.add(list);
        list = new ArrayList<>(Arrays.asList("RFO", "R", "R_FB"));
        cases.add(list);
        actionAssert.add(true);
        actionAssert.add(true);
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("R_PERCENTAGE")));

        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("R_FB")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        return obj.iterator();

    }

    //

    @DataProvider(name = "multiRestEdvo")
    public Iterator<Object[]> multiRestEdvo() {

        List<Object[]> obj = new ArrayList<>();
        ArrayList<Boolean> actionAssert = new ArrayList<>();
        ArrayList<List<String>> cases = new ArrayList<>();

        HashMap<String, HashMap<String, List<String>>> eval = new HashMap<>();
        HashMap<String, List<String>> hm_s = new HashMap<>();
        HashMap<String, List<String>> hm = new HashMap<>();
        String valid = "VALID";
        String invalid = "INVALID";
        int i = 0;

        // 1 case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        hm_s = new HashMap<>();
        ArrayList<String> list = new ArrayList<>(Arrays.asList("P", "M", "BXGY"));
        cases.add(list);
        actionAssert.add(true);

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("BXGY")));

        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("BXGY")));

        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("BXGY")));
        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        //2 Case
        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("RFO", "M", "BXGY"));
        cases.add(list);
        actionAssert.add(true);

        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("BXGY")));

        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("BXGY")));

        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("BXGY")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        //3 Case

        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("SFO", "M", "BXGY"));
        cases.add(list);
        actionAssert.add(true);

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("N/A")));

        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("N/A")));


        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("BXGY")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("N/A")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});


        //4 CASE


        System.out.println("Case" + (i = i + 1));
        cases = new ArrayList<>();
        actionAssert = new ArrayList<>();
        eval = new HashMap<>();
        hm = new HashMap<>();
        list = new ArrayList<>(Arrays.asList("CUS", "M", "BXGY"));
        cases.add(list);
        actionAssert.add(true);

        hm.put("EVAL_CART_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_CART_SFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_CART_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_CART_CUS", new ArrayList<>(Arrays.asList("BXGY")));


        hm.put("EVAL_LISTING_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_SFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_LISTING_CUS", new ArrayList<>(Arrays.asList("BXGY")));

        hm.put("EVAL_MENU_P", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_SFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_RFO", new ArrayList<>(Arrays.asList("N/A")));
        hm.put("EVAL_MENU_CUS", new ArrayList<>(Arrays.asList("BXGY")));

        eval.put("not_super", hm);
        obj.add(new Object[]{cases, actionAssert, eval, valid, ""});

        return obj.iterator();

    }


}