package com.swiggy.api.sf.rng.pojo.edvo;


import org.codehaus.jackson.annotate.JsonProperty;

public class EDVOMeal {

    @JsonProperty("meal_id")
    private String mealId;

    @JsonProperty("restaurant_id")
    private String restaurantId;

    public EDVOMeal() { }

    public EDVOMeal(String mealId, String restaurantId) {
        this.mealId = mealId;
        this.restaurantId = restaurantId;
    }

    public EDVOMeal defaultData(String meal_id, String restaurant_id) {
        if (getMealId() == null) {
            setMealId(meal_id);
        }
        if (getRestaurantId() == null) {
            setRestaurantId(restaurant_id);
        }
        return this;
    }


    public String getMealId() {
        return this.mealId;
    }

    public void setMealId(String meal_id) {
        this.mealId = meal_id;
    }


    public String getRestaurantId() {
        return this.restaurantId;
    }

    public void setRestaurantId(String restaurant_id) {

        this.restaurantId = restaurant_id;
    }

    public EDVOMeal withMeal(String meal_id) {
        setMealId(meal_id);
        return this;

    }

    public EDVOMeal withRestaurantId(String restaurant_id) {
        setRestaurantId(restaurant_id);
        return this;
    }

    @Override
    public String toString() {
        return "{" + "\"" + "mealId" + "\"" + ":" + mealId + ","
                + "\"" + "restaurantId" + "\"" + ":" + restaurantId
                + "}";

    }

}