package com.swiggy.api.sf.rng.pojo.SwiggySuper;

import java.util.HashMap;

public class Createbenefit {
	
	private String title= "Automated_Benefit";
	   
	private String description= "Automated_Benefit_Description_Test";
	
	private String name= "Auto-Benefit_name";
	   
	private String logo_id="z1d3z4ws42ryer";
	   
	private String meta="null";
	   
	private String type="FREE_DELIVERY" ;
	   
	private String created_by= "SuperAutomationScript";
	   
	private String priority="10";
	
	public Createbenefit() {
		
	}
	
	public Createbenefit(String type, String priority) {
	 this.type= type;
	 this.priority= priority;
	}
	
	public Createbenefit(HashMap<String, String> benefitData) {
		 this.title= benefitData.get("0");
		 this.description= benefitData.get("1");
		 this.name= benefitData.get("2");
		 this.logo_id= benefitData.get("3");
		 this.meta= benefitData.get("4");
		 this.type= benefitData.get("5");
		 this.created_by= benefitData.get("6");
		 this.priority= benefitData.get("7");
	}
	
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title= title;
	}
	public String getDescription() {
		return this.description;
	}
	public void setDescription(String description) {
		this.description= description;
	}
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name= name;
	}
	public String getLogoId() {
		return this.logo_id;
	}
	public void setLogoId(String logoId) {
		this.logo_id= logoId;
	}
	public String getMeta() {
		return this.meta;
	}
	public void setMeta(String meta) {
		this.meta= meta;
	}
	public String getType() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type= type;
	}
	public String getCreatedBy() {
		return this.created_by;
	}
	public void setCreatedBy(String created_by) {
		this.created_by= created_by;
	}
	public String getPriority() {
		return this.priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	@Override
	public String toString() {
		return 
				"{"
				+ "\"title\"" + ":" + "\"" + title +"\"" + "," 
				+ "\"description\"" + ":" + "\"" + description +"\"" + ","  
				+ "\"name\"" + ":" + "\"" +name +"\"" + ","  
				+ "\"logo_id\"" + ":" + "\"" + logo_id +"\"" + ","  
				+ "\"meta\"" + ":" + meta + ","  
				+ "\"type\"" + ":" + "\"" + type +"\"" + ","
				+ "\"created_by\"" + ":" + "\"" + created_by +"\"" + ","
				+ "\"priority\"" + ":" + priority  
				+ "}"
				;
	}

	public String freebie() {
		return
				"{"
						+ "\"title\"" + ":" + "\"" + title +"\"" + ","
						+ "\"description\"" + ":" + "\"" + description +"\"" + ","
						+ "\"name\"" + ":" + "\"" +name +"\"" + ","
						+ "\"logo_id\"" + ":" + "\"" + logo_id +"\"" + ","
						+ "\"meta\"" + ":" + meta + ","
						+ "\"type\"" + ":" + "\"" + "Freebie" +"\"" + ","
						+ "\"created_by\"" + ":" + "\"" + created_by +"\"" + ","
						+ "\"priority\"" + ":" + priority
						+ "}"
				;
	}
	
	public String toStringWithTitleNull() {
		return 
				"{"
				+ "\"title\"" + ":" + title  + "," 
				+ "\"description\"" + ":" + "\"" + description +"\"" + ","  
				+ "\"name\"" + ":" + "\"" +name +"\"" + ","  
				+ "\"logo_id\"" + ":" + "\"" + logo_id +"\"" + ","  
				+ "\"meta\"" + ":" + meta + ","  
				+ "\"type\"" + ":" + "\"" + type +"\"" + ","
				+ "\"created_by\"" + ":" + "\"" + created_by +"\"" + ","
				+ "\"priority\"" + ":" + priority  
				+ "}"
				;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

	