package com.swiggy.api.sf.rng.helper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PopulateCartV3Data {
	
	private static final String filePath ="D:\\perf\\cartV3_perf.csv";
	private static int payLoadLimit = 1;
	private static final int mealPayLoadMinValue=3;
	private static final int mealPayLoadMaxValue=50;
	private static final int itemIdMinValue=2345543;
	private static final int itemIdMaxValue=99999999;
	
	
	String menuPayload="{\"minCartAmount\": 0,\"firstOrder\": false,\"itemRequests\": [],\"mealItemRequests\": [<mealPayload>],\"userId\": 9898}";
	String mealPayload="{\"mealId\": <meal_id>,\"itemId\": <item_id>,\"restaurantId\":<rest_id>,\"price\": 500,\"count\": 1,\"groupId\": <group_id>}";
	
	public void writeDataToFile(String filePath, Map<Integer, String> map) {

		File listingFile = new File(filePath);
		FileWriter fw;
		try {
			fw = new FileWriter(listingFile);

			for (int i = 0; i < map.size(); i++) {

				String payload = map.get(i);
				System.out.println("payload " + payload);
				fw.write(payload);
				fw.write("\n");

			}
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
	
	public void createCartPayload() {
		TDPerfTest tdPerfTest = new TDPerfTest();
		List<String> restIds = tdPerfTest.getEDVOEnableTdRestIds();
		for(int i=0;i<restIds.size(); i++) {
			System.out.println("rest. ids are .... "+restIds.get(i));
		}
		Map<String, List<String>> restIdMealIdMap = tdPerfTest.getEDVORestIdMealId();
		Map<String, List<String>> restIdGroupMap = TDPerfTest.getEDVOTdEnabledGroupIds(restIds);
		
		Map<Integer, String> restIdToCartMap = new HashMap();
		//List validRestIds = new ArrayList<>(restIdToItemMap.keySet());
		
		for(int i = 0; i < payLoadLimit; i++) {
			String restId1 = Utility.getRandomFromList(restIds);	
			String cartPayLoad = createItemRequest(restId1, restIdGroupMap,restIdMealIdMap);
			String cartV3PayLoad = menuPayload.replace("<mealPayload>", cartPayLoad);
			restIdToCartMap.put(i, cartV3PayLoad);
			
			
		}
		writeDataToFile(filePath,restIdToCartMap);
	}
	
	
	private String createItemRequest(String restId, Map<String, List<String>> restIdToGroupMap,Map<String, List<String>> restIdToMealMap ) {

		int mealLimit = Utility.getRandom(mealPayLoadMinValue, mealPayLoadMaxValue);
		List<String> mealList = new ArrayList<String>();
		List<String> groupList = new ArrayList<String>();
		
		for(int i = 0; i < mealLimit; i++) {
			String groupId = Utility.getRandomFromList(restIdToGroupMap.get(restId));
			String mealId = Utility.getRandomFromList(restIdToMealMap.get(restId));
			String itemId = String.valueOf(Utility.getRandom(itemIdMinValue, itemIdMaxValue));
			mealList.add(mealPayload.replace("<rest_id>", restId).replace("<meal_id>", mealId).replace("<item_id>", itemId).replace("<group_id>", groupId));
			//groupList.add(mealPayload.replace("<group_id>", groupId));
			
		
		}
		return String.join(",", mealList);
	}

}
