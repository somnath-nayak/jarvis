package com.swiggy.api.sf.rng.helper.TDHelper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.constants.SuperConstants;
import com.swiggy.api.sf.rng.helper.*;
import com.swiggy.api.sf.rng.pojo.*;
import com.swiggy.api.sf.rng.pojo.freedelivery.CreateFreeDeliveryEntry;
import com.swiggy.api.sf.rng.pojo.freedelivery.CreateFreeDeliveryTdBuilder;
import com.swiggy.api.sf.rng.pojo.freedelivery.FreeDeliveryRestaurantList;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.apache.commons.lang.time.DateUtils;
import org.mortbay.util.ajax.JSON;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.*;

public class SuperV2TdHelper {
    RngHelper rngHelper = new RngHelper();
    SuperHelper superHelper = new SuperHelper();
    SoftAssert softAssert = new SoftAssert();
    JsonHelper jsonHelper = new JsonHelper();
    RandomNumber rm = new RandomNumber(2000000, 3000000);

    public ArrayList getArrayList(String restId, String flag, String userId, String deviceos, String versionCode) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(restId);
        arrayList.add(flag);
        arrayList.add(userId);
        arrayList.add(deviceos);
        arrayList.add(versionCode);
        return arrayList;
    }

    public ArrayList getListForMenu(String mincartamount, String restId, String flag, String userId, String deviceos, String versionCode) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(mincartamount);
        arrayList.add(restId);
        arrayList.add(flag);
        arrayList.add(userId);
        arrayList.add(deviceos);
        arrayList.add(versionCode);
        return arrayList;
    }

    public ArrayList getListForCart(String restId, String categoryId, String subCategoryId, String itemId,
                                    String count, String price, String cartamount, String userId, String flag, String deviceos, String versioncode) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(restId);
        arrayList.add(categoryId);
        arrayList.add(subCategoryId);
        arrayList.add(itemId);
        arrayList.add(count);
        arrayList.add(price);
        arrayList.add(cartamount);
        arrayList.add(userId);
        arrayList.add(flag);
        arrayList.add(deviceos);
        arrayList.add(versioncode);
        return arrayList;
    }


    public HashMap<String, String> publicUserSubsCription(String planPayload, String numOfPlans,
                                                          String benefitPayload) {
        SuperDbHelper.deleteAllBenefit();
        Processor planResponse = superHelper.createPlan(planPayload);
        int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        // System.out.println( "palnid" +planId);
        Assert.assertEquals(statusCode, 1);
        Processor benefitResponse = superHelper.createBenefit(benefitPayload);
        int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(benefitStatusCode, 1);
        int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        String planBenefitMapPayload = superHelper
                .populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
        Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
        int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
        String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
        Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);
        String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
        String orderId = Integer.toString(Utility.getRandom(1, 1000000));

        String planUserIncentiveMapPayload = superHelper
                .populatePlanUserIncentiveMappingPayload(Integer.toString(planId), "USER", "null").toString();
        Processor planUserIncentiveMapResponse = superHelper
                .createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
        int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
                .GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(statusCodePlanUserIncentiveMap, 1);
        String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
        softAssert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);
        Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
        String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
                "$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
        softAssert.assertEquals(benefitDetails.isEmpty(), false);
        String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
        Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
        int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(verifyStatus, 1);
        String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
        softAssert.assertEquals(verifyMessage, SuperConstants.successCheck);

        boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
        softAssert.assertEquals(validPlanVerify, true);
        String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
                publicUserId, orderId);
        Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
        int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(statusCodeOfSubsResp, 1);
        String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
        int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
        softAssert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));
        Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");
        String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
                "$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
        softAssert.assertEquals(benefitDetailsAfterSubs, "[]");
        Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
        int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
        softAssert.assertEquals(subsUserPlanId, planId);
        String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
        softAssert.assertEquals(subsUserBenefitId.isEmpty(), false);
        Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
        List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
                ("$.data..subscription_id"));
        String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
                orderId);
        softAssert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
        for (String id : listOfSubsIds) {
            softAssert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
        }
        System.out.println("PLAN ID --" + planId);

        HashMap<String, String> hmap = new HashMap<>();
        hmap.put("userid", publicUserId);

        return hmap;
    }


    public HashMap<String, String> publicUserPlanBenefitsMapped(String planPayload, String numOfPlans,
                                                                String benefitPayload) {
        SuperDbHelper.deleteFreeDelBenefit();
        //Plan Creation
        Processor planResponse = superHelper.createPlan(planPayload);
        int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        Assert.assertEquals(statusCode, 1);
        //Benefits Creation
        Processor benefitResponse = superHelper.createBenefit(benefitPayload);
        int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(benefitStatusCode, 1);
        //Plan  Benefit Mapping
        int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
        String planBenefitMapPayload = superHelper
                .populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
        Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
        int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
        String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
        Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);
        String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
        String orderId = Integer.toString(Utility.getRandom(1, 1000000));

        String planUserIncentiveMapPayload = superHelper
                .populatePlanUserIncentiveMappingPayload(Integer.toString(planId), String.valueOf(publicUserId), "null").toString();
        Processor planUserIncentiveMapResponse = superHelper
                .createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
        int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
                .GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(statusCodePlanUserIncentiveMap, 1);
        String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
        softAssert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);
        Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
        String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
                "$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
        softAssert.assertEquals(benefitDetails.isEmpty(), false);
        String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
        Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
        int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(verifyStatus, 1);
        String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
        softAssert.assertEquals(verifyMessage, SuperConstants.successCheck);
        boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
        softAssert.assertEquals(validPlanVerify, true);

        HashMap<String, String> hmap = new HashMap<>();
        hmap.put("userid", publicUserId);

        return hmap;
    }


    public HashMap<String, String> createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(String minCartAmount,
                                                                                        String restId, boolean firstorderRestriction, boolean restaurantFirstOrder, boolean userRestriction,
                                                                                        String dormant_user_type, boolean is_surge) throws IOException {

        ArrayList al = new ArrayList();
        al.add(new FreeDeliveryRestaurantList(restId));
        CreateFreeDeliveryEntry FreeDeliveryDiscount1 = new CreateFreeDeliveryTdBuilder().nameSpace("RestaurantLevel")
                .header("RestaurantLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
                .campaign_type("FREE_DELIVERY").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
                .discountLevel("Restaurant").restaurantList(al).is_surge(is_surge)
                // .slots(slots)
                .ruleDiscount("FREE_DELIVERY", "Restaurant", minCartAmount).userRestriction(userRestriction)
                .timeSlotRestriction(false).firstOrderRestriction(firstorderRestriction).taxesOnDiscountedBill(false)
                .commissionOnFullBill(false).restaurantFirstOrder(restaurantFirstOrder)
                .dormant_user_type(dormant_user_type).build();
        Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(FreeDeliveryDiscount1));
        String createTDResponse = processor.ResponseValidator.GetBodyAsText();
        String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
        String statusCode = JsonPath.read(createTDResponse, "$.statusCode").toString();
        String statusMessage = JsonPath.read(createTDResponse, "$.statusMessage").toString();


        HashMap Keys = new HashMap();
        Keys.put("restId", restId);
        Keys.put("minCartAmount", minCartAmount);
        Keys.put("TDID", createdTD_Id);
        Keys.put("statusCode", statusCode);
        Keys.put("statusMessage", statusMessage);

        return Keys;
    }


    public String createFlat(List<RestaurantList> restaurantLists1) throws IOException {
        CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
                .campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
                .discountLevel("Restaurant").restaurantList(restaurantLists1)
                // .slots(slots)
                .ruleDiscount("Flat", "Restaurant", "100", "0").userRestriction(false).timeSlotRestriction(false)
                .firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT").build();
        return jsonHelper.getObjectToJSON(flatTradeDiscount1) ;
    }


    public String createPercentageWithMinCartAtItemLevel(String restid) throws IOException {

        Menu menu = new Menu();
        String itemId = new Integer(rm.nextInt()).toString();

        menu.setId(itemId);
        menu.setName("testItem");

        List<Menu> menus = new ArrayList<>();
        menus.add(menu);

        SubCategory subCategory = new SubCategory();
        String SubcategoryId = new Integer(rm.nextInt()).toString();
        subCategory.setId(SubcategoryId);
        subCategory.setName("test");
        subCategory.setMenu(menus);

        List<SubCategory> subCategories = new ArrayList<>();
        subCategories.add(subCategory);

        Category category = new Category();
        String categoryId = new Integer(rm.nextInt()).toString();
        category.setId(categoryId);
        category.setName("test");
        category.setSubCategories(subCategories);

        List<Category> categories = new ArrayList<>();
        categories.add(category);

        RestaurantList restaurant = new RestaurantList();

        restaurant.setId(restid);
        restaurant.setName("test");
        restaurant.setCategories(categories);

        List<RestaurantList> restaurantList = new ArrayList<>();
        restaurantList.add(restaurant);



        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
                .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
                .campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Test")
                .discountLevel("Item").restaurantList(restaurantList)
                .ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
                .timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
                .commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();
        Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
        String createTDResponse = processor.ResponseValidator.GetBodyAsText();
        return createTDResponse;
    }


    public String createPercentageWithMinCartAtItemLevelForSuperUser(String restid) throws IOException {

        Menu menu = new Menu();
        String itemId = new Integer(rm.nextInt()).toString();

        menu.setId(itemId);
        menu.setName("testItem");

        List<Menu> menus = new ArrayList<>();
        menus.add(menu);

        SubCategory subCategory = new SubCategory();
        String SubcategoryId = new Integer(rm.nextInt()).toString();
        subCategory.setId(SubcategoryId);
        subCategory.setName("test");
        subCategory.setMenu(menus);

        List<SubCategory> subCategories = new ArrayList<>();
        subCategories.add(subCategory);

        Category category = new Category();
        String categoryId = new Integer(rm.nextInt()).toString();
        category.setId(categoryId);
        category.setName("test");
        category.setSubCategories(subCategories);

        List<Category> categories = new ArrayList<>();
        categories.add(category);

        RestaurantList restaurant = new RestaurantList();

        restaurant.setId(restid);
        restaurant.setName("test");
        restaurant.setCategories(categories);

        List<RestaurantList> restaurantList = new ArrayList<>();
        restaurantList.add(restaurant);



        CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
                .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
                .campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Test")
                .discountLevel("Item").restaurantList(restaurantList)
                .ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
                .timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
                .commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).withIsSuper(Boolean.TRUE).build();
        Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
        String createTDResponse = processor.ResponseValidator.GetBodyAsText();
        return createTDResponse;
    }




    public void validationForSuperUser(String type, ArrayList al, String tid, String TDType, String minCartAmount, String cartTD, String rewardList, boolean check) {
        if (type.equals("list")) {
            Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            System.out.println(resp);
            if (check) {
                boolean cidFlag = false;

                String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                Assert.assertEquals(get_tdil_discountType, TDType, "" + TDType + " TD is not found");
                String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (String cid : get_tdil_campaignIds) {
                    if (tid != cid) {
                        cidFlag = true;
                        break;
                    }
                }
                Assert.assertTrue(cidFlag, "camp id is not visible");
                String nudgeinfo = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..nudgeDiscountInfo");
                Assert.assertEquals(nudgeinfo, "[[]]");
                String legacyDiscountInfo = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..legacyDiscountInfo");
                Assert.assertNotEquals(legacyDiscountInfo, "[null]");

                String get_tdil_mincartamt = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..minCartAmount").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                Assert.assertEquals(get_tdil_mincartamt, minCartAmount, "Mincart amout is not found");

            } else {
                Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
                //System.out.println(JsonPath.read(resp, "$..data..discountList[*]").toString());
            }


        }
        if (type.equals("listsuper")) {
            Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            System.out.println(resp);
            if (check) {
                boolean cidFlag = false;

                String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                Assert.assertTrue(get_tdil_discountType.toLowerCase().contains(TDType.toLowerCase()),"" + TDType + " TD is not found");
                String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (String cid : get_tdil_campaignIds) {
                    if (tid != cid) {
                        cidFlag = true;
                        break;
                    }
                }
                Assert.assertTrue(cidFlag, "camp id is not visible");
                String nudgeinfo = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..nudgeDiscountInfo");
                Assert.assertEquals(nudgeinfo, "[[]]");
                String legacyDiscountInfoType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..legacyDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();;
                Assert.assertTrue(legacyDiscountInfoType.equalsIgnoreCase( TDType), "" + TDType + " TD is not found");
                String[] legacyDiscountInfoTypecampaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..legacyDiscountInfo..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (String cid : legacyDiscountInfoTypecampaignIds) {
                    if (tid != cid) {
                        cidFlag = true;
                        break;
                    }

                    String get_tdil_mincartamt = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..minCartAmount").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                    Assert.assertEquals(get_tdil_mincartamt, minCartAmount, "Mincart amout is not found");

                }
            }else {
                Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
                //System.out.println(JsonPath.read(resp, "$..data..discountList[*]").toString());
            }


        }

        if (type.equals("list_superplanmapped")) //assertion for non super user --> mapped with SUPER Plan & Benefits
        {
            Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            System.out.println(resp);
            if (check) {
                boolean cidFlag = false;

                String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                Assert.assertEquals(get_tdil_discountType, TDType, "" + TDType + " TD is not found");

                String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (String cid : get_tdil_campaignIds) {
                    if (tid != cid) {
                        cidFlag = true;
                        break;
                    }
                }
                Assert.assertTrue(cidFlag, "camp id is not visible");

                String get_tddiscounttype_legacyDiscountInfo = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..legacyDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                Assert.assertEquals(get_tddiscounttype_legacyDiscountInfo, TDType, "" + TDType + " TD is not found");

                String[] get_tdid_legacyDiscountInfo = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..legacyDiscountInfo..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (String cid : get_tdil_campaignIds) {
                    if (tid != cid) {
                        cidFlag = true;
                        break;
                    }
                }
                Assert.assertTrue(cidFlag, "camp id is not visible");


                String nudgeinfo = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..nudgeDiscountInfo");
                Assert.assertEquals(nudgeinfo, "[[]]");

                String get_tdil_mincartamt = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..minCartAmount").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                Assert.assertEquals(get_tdil_mincartamt, minCartAmount, "Mincart amout is not found");

            } else {
                Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
                //System.out.println(JsonPath.read(resp, "$..data..discountList[*]").toString());
            }


        }
        if (type.equals("list_nudge")) //assertion for non super user --> mapped with SUPER Plan & Benefits
        {
            Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            System.out.println(resp);
            if (check) {
                boolean cidFlag = false;
                String nudgeinfo = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..nudgeDiscountInfo");
                Assert.assertNotEquals(nudgeinfo,"[]");
                String campaignId = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..nudgeDiscountInfo..campaignId");
                String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..nudgeDiscountInfo..campaignId").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (String cid : get_tdil_campaignIds) {
                    if (tid != cid) {
                        cidFlag = true;
                        break;
                    }
                }
                Assert.assertTrue(cidFlag, "camp id is not visible");


                String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..nudgeDiscountInfo..discountType").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                Assert.assertEquals(get_tdil_discountType, TDType, "" + TDType + " TD is not found");

            } else {
                Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
                //System.out.println(JsonPath.read(resp, "$..data..discountList[*]").toString());
            }


        }
        if (type.equals("menu")) {
            Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            if (check) {
                List<Integer> tdid_list= new ArrayList<>();
                tdid_list.addAll(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].campaignId"));
                int  index_td_id=tdid_list.indexOf(Integer.valueOf(tid));


                Assert.assertEquals(String.valueOf(tdid_list.get(index_td_id)),(tid) , "TD is not found");
                Assert.assertTrue(JsonPath.read(resp, "$.data.tradeDiscountInfo["+index_td_id+"].discountType").toString().toLowerCase().contains(  TDType.toLowerCase() ), "TD is not percentage");
                Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), cartTD, "TD is not percentage");
                Assert.assertTrue(JsonPath.read(resp, "$.data.tradeDiscountInfo["+index_td_id+"].minCartAmount").toString().contains( minCartAmount), "TD is not percentage");
                // Assert.assertEquals(JsonPath.read(resp, "$.data.rewardType").toString(), TDType, "TD is not percentage");
                Assert.assertEquals(JsonPath.read(resp, "$.data.rewardList").toString(), "[]", "TD is not percentage");


            } else {
                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
                Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "TD is not percentage");

            }
        }
        if (type.equals("cart")) {
            Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(), al.get(10).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();

            if (check) {
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..campaignId").toString(), "[" + tid + "]", "TD is not found");
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..discountType").toString(), "[" + "\"" + TDType + "\"" + "]", "TD is not percentage");
                Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), cartTD, "TD is not percentage");


            } else {
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
                Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "TD is not percentage");

            }
        }
        if (type.equals("cartsuper_freedel")) {
            Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(), al.get(10).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();

            if (check) {
                Assert.assertNotEquals(JsonPath.read(resp, "$.data..nudgeDetails").toString(), "[[]]", "Nudge details is not empty");
                Assert.assertEquals(JsonPath.read(resp, "$.data..nudgeDetails..tradeDiscountInfo..discountType").toString(), "[" + "\"" + TDType + "\"" + "]", "TD is not as expected" +TDType);

            }
        }

        if (type.equals("cartsuper")) {
            Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(), al.get(10).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();

            if (check) {
                Assert.assertEquals(JsonPath.read(resp, "$.data..super..tradeDiscountInfo..campaignId").toString(), "[" + tid + "]", "TD is not found");
                Assert.assertEquals(JsonPath.read(resp, "$.data..super..tradeDiscountInfo..discountType").toString().toLowerCase(), "[" + "\"" + TDType.toLowerCase() + "\"" + "]", "TD is not percentage");
                Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), cartTD, "TD is not percentage");


            } else {
                Assert.assertEquals(JsonPath.read(resp, "$.data..super..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
                Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "TD is not percentage");

            }
        }


        if (type.equals("cart2")) {
            Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(), al.get(10).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            if (check) {
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Flat\"]", "TD is not percentage");
            } else {
                Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "tradeDiscountInfo object has discount");
            }
        }

    }

}
