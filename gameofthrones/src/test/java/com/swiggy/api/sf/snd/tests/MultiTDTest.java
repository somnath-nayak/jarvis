package com.swiggy.api.sf.snd.tests;


import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.OrderPlace;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class MultiTDTest extends SnDDp {
    SANDHelper sandHelper= new SANDHelper();
    OrderPlace orderPlace= new OrderPlace();
    RngHelper rngHelper = new RngHelper();

    @Test(dataProvider = "multi", description ="apply percentage and free del on a rest id and verify the same on aggregator" )
    public void multiTdAggregator(String lat ,String lng) throws InterruptedException{
        List<String> listOfRestIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            System.out.println("ids"+listOfRestIds.get(3));
            rngHelper.createPercentageWithMinAmountAtRestaurantLevel(listOfRestIds.get(3), false);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(3), false, false, false, "ZERO_DAYS_DORMANT", false,false);
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        Thread.sleep(4000);
        String aggAfterDiscount=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        System.out.println("idddd"+sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(3)+".tradeDiscountInfo..discountType"));
        System.out.println("agg"+sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(3)+".aggregatedDiscountInfo.shortDescriptionList..discountType"));
        System.out.println("rest id"+listOfRestIds.get(3));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(3)+".tradeDiscountInfo..discountType").contains(freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(3)+".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel));

        //Menu call
        String response=sandHelper.menuV4RestId(listOfRestIds.get(3),lat, lng).ResponseValidator.GetBodyAsText();
        System.out.println("777"+sandHelper.JsonString(response,"$.data.aggregatedDiscountInfo.descriptionList..discountType"));
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(response,"$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        System.out.println("--->"+sandHelper.JsonString(response,"$.data.tradeCampaignHeaders..discountType"));
        if(!sandHelper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").contains(percent)){
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        }else
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(percent),"no percentage on restaurant");

        //search call
        String restName= sandHelper.restName(listOfRestIds.get(3)).replace(" ", "+");
        String searchAfterDiscount=sandHelper.searchV2(restName,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");

    }


    public void tdPercentFreeDel(List<String> listOfRestIds){
        try {
            System.out.println("ids"+listOfRestIds.get(1));
            rngHelper.createPercentageTD(listOfRestIds.get(1));
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(0), false, false, false, "ZERO_DAYS_DORMANT", false);

        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
    }


    @Test(dataProvider = "multi", description ="apply flat and free del on a rest id and verify the same on aggregator" )
    public void multiTdAggregatorFlat(String lat ,String lng) throws InterruptedException{
        List<String> listOfRestIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel(listOfRestIds.get(5),"100" ,"10", false);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(5), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(5));
        String aggAfterDiscount=sandHelper.aggregator(new String[]{lat,lng}, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(5)+".tradeDiscountInfo..discountType").contains(freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.."+listOfRestIds.get(5)+".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel));

        //menu call
        String menuAfterDiscount=sandHelper.menuV4RestId(listOfRestIds.get(5),lat,lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount,"$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        if(!sandHelper.JsonString(menuAfterDiscount,"$.data.tradeCampaignHeaders..discountType").contains(flat)){
            System.out.println(sandHelper.JsonString(menuAfterDiscount,"$.data.tradeCampaignHeaders..discountType"));
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        }
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "no free bie on this restaurant");

        //search call
        String restName= sandHelper.restName(listOfRestIds.get(5)).replace(" ", "+");
        String searchAfterDiscount=sandHelper.searchV2(restName,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");

    }

    @Test(dataProvider = "multi", description ="apply flat ,free del, freebie on a rest id and verify the same on aggregator" )
    public void multiTdFFF(String lat ,String lng) throws InterruptedException {
        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        try {
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel(listOfRestIds.get(6), "100", "10", false);
            freeBie(listOfRestIds.get(6), "100");
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(6), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(6));
        String aggAfterDiscount = sandHelper.aggregator(new String[]{lat, lng}, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(6) + ".tradeDiscountInfo..discountType").contains(freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(6) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel));

        //menu call
        String menuAfterDiscount = sandHelper.menuV4RestId(listOfRestIds.get(6), lat, lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "no free bie on this restaurant");
        //Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(freebie), "no free bie on this restaurant");

        //search call
        String restName= sandHelper.restName(listOfRestIds.get(6)).replace(" ", "+");
        String searchAfterDiscount=sandHelper.searchV2(restName,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }


    @Test(dataProvider = "multi", description ="apply free del, freebie on a rest id and verify the same on aggregator" )
    public void multiTdFF(String lat ,String lng) throws InterruptedException {
        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        try {
            freeBie(listOfRestIds.get(7), "100");
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(7), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(7));
        String aggAfterDiscount = sandHelper.aggregator(new String[]{lat, lng}, null, null).ResponseValidator.GetBodyAsText();
        System.out.println("000"+listOfRestIds.size());
        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(7) + ".tradeDiscountInfo..discountType"));
        if(!sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(7) + ".tradeDiscountInfo..discountType").contains(freeDel)){
            Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(7) + ".tradeDiscountInfo..discountType").contains(freebie));
        }else
            Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(7) + ".tradeDiscountInfo..discountType").contains(freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(7) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel));

        //menu call
        String menuAfterDiscount = sandHelper.menuV4RestId(listOfRestIds.get(7), lat, lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        //Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "no free bie on this restaurant");
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(freebie), "no free bie on this restaurant");

        //search call
        String restName= sandHelper.restName(listOfRestIds.get(7)).replace(" ", "+");
        String searchAfterDiscount=sandHelper.searchV2(restName,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }


    @Test(dataProvider = "multi", description ="apply Percentage ,free del, freebie on a rest id and verify the same on aggregator" )
    public void multiTdPFF(String lat ,String lng) throws InterruptedException {
        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        try {
            //rngHelper.createPercentageWithMinAmountAtRestaurantLevel(listOfRestIds.get(8), false);
            freeBie(listOfRestIds.get(0), "100");
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(0), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(0));
        String aggAfterDiscount = sandHelper.aggregator(new String[]{lat, lng}, null, null).ResponseValidator.GetBodyAsText();
        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(0) + ".tradeDiscountInfo..discountType"));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(0) + ".tradeDiscountInfo..discountType").contains(freeDel));
        if(!sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(0) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel)){
            Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(0) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freebie));
        }
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(0) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel));

        //menu call
        String menuAfterDiscount = sandHelper.menuV4RestId(listOfRestIds.get(0), lat, lng).ResponseValidator.GetBodyAsText();
        System.out.println("-->"+sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType"));
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        if(!sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(percent) && !sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(freebie))
            Assert.assertTrue(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(freeDel), "no free bie on this restaurant");
        else if(!sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(percent) && !sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(freeDel))
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(freebie), "no free bie on this restaurant");
        else if(!sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(freeDel) && !sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(freebie))
        {
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(percent), "no free bie on this restaurant");
        }

        //search call
        String restName= sandHelper.restName(listOfRestIds.get(0)).replace(" ", "+");
        String searchAfterDiscount=sandHelper.searchV2(restName,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }


    @Test(dataProvider = "multi", description ="apply Percentage ,free del  on a rest id , user is superand verify the same on aggregator" )
    public void multiTdSPF(String lat ,String lng) throws InterruptedException {

        HashMap<String, String>hMap=sandHelper.loginData(SANDConstants.mobile, SANDConstants.password);

        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, hMap.get("Tid"), hMap.get("Token"));
        System.out.println(hMap.get("Tid")+" - " +hMap.get("Token"));
        try {
            rngHelper.createPercentageWithMinAmountAtRestaurantLevel(listOfRestIds.get(9), false);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(9), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(9));
        String aggAfterDiscount = sandHelper.aggregator(new String[]{lat, lng}, hMap.get("Tid"), hMap.get("Token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(9) + ".tradeDiscountInfo..discountType").contains(freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(9) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel));

        //menu call
        String menuAfterDiscount = sandHelper.menuV4RestId(listOfRestIds.get(9), lat, lng,hMap.get("Tid"), hMap.get("Token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        if(!sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(percent))
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(freebie), "no free bie on this restaurant");
        else
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(percent), "no free bie on this restaurant");

        //search call
        String restName= sandHelper.restName(listOfRestIds.get(9)).replace(" ", "+");
        String searchAfterDiscount=sandHelper.searchV2(restName,lat,lng, hMap.get("Tid"), hMap.get("Token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }

    @Test(dataProvider = "multi", description ="apply flat ,free del on a rest id ,user is super and verify the same on aggregator" )
    public void multiTdSFlatF(String lat ,String lng) throws InterruptedException {

        HashMap<String, String>hMap=sandHelper.loginData(SANDConstants.mobile, SANDConstants.password);
        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, hMap.get("Tid"), hMap.get("Token"));
        System.out.println(hMap.get("Tid")+" - " +hMap.get("Token"));
        try {
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(10), false, false, false, "ZERO_DAYS_DORMANT", false, false);
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel(listOfRestIds.get(10), "100", "10", true);
           } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(10));
        String aggAfterDiscount = sandHelper.aggregator(new String[]{lat, lng}, hMap.get("Tid"), hMap.get("Token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(10) + ".tradeDiscountInfo..discountType").contains(flat));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(10) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(flat));

        //menu call
        String menuAfterDiscount = sandHelper.menuV4RestId(listOfRestIds.get(10), lat, lng,hMap.get("Tid"), hMap.get("Token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(flat), "no free bie on this restaurant");
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "no free bie on this restaurant");
        //Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(freebie), "no free bie on this restaurant");

        //search call
        String restName= sandHelper.restName(listOfRestIds.get(10)).replace(" ", "+");
        String searchAfterDiscount=sandHelper.searchV2(restName,lat,lng, hMap.get("Tid"), hMap.get("Token")).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(flat), "No free delivery on the product");
    }



    @Test(dataProvider = "multi", description ="apply Percentage ,free del, freebie on a rest id and verify the same on aggregator" )
    public void multiTdPFlatFreeDel(String lat ,String lng) throws InterruptedException {
        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        try {
            rngHelper.createPercentageWithMinAmountAtRestaurantLevel(listOfRestIds.get(11), false);
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel(listOfRestIds.get(11), "100", "10", true);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(11), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(11));
        String aggAfterDiscount = sandHelper.aggregator(new String[]{lat, lng}, null, null).ResponseValidator.GetBodyAsText();
        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(11) + ".tradeDiscountInfo..discountType").contains(freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(11) + ".tradeDiscountInfo..discountType").contains(freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(11) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(freeDel));

        //menu call
        String menuAfterDiscount = sandHelper.menuV4RestId(listOfRestIds.get(11), lat, lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");
        System.out.println(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType"));
        if(!sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(percent) && !sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(flat) )
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType")).contains(freeDel),"no freedel");
        else if(!sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(flat) && !sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").contains(freeDel) )
            Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType")).contains(percent), "no free del on this restaurant");
        else Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType")).contains(flat), "no free del on this restaurant");
        //Assert.assertTrue(Arrays.asList(sandHelper.JsonString(menuAfterDiscount, "$.data.tradeCampaignHeaders..discountType").split(",")).contains(freeDel), "no free bie on this restaurant");

        //search call
        String restName= sandHelper.restName(listOfRestIds.get(11)).replace(" ", "+");
        String searchAfterDiscount=sandHelper.searchV2(restName,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }

    @Test(dataProvider = "latLongDelPer", description = "creating free del and percentage collection")
    public void freeDelColOnAggregator(String lat ,String lng, String identifier, String identifier1) throws InterruptedException{
        List<String> l=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            List<String> firstNElementsList = l.stream().limit(5).collect(Collectors.toList());
            for(int i=0; i<firstNElementsList.size(); i++) {
                rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", l.get(i), false, false, false, "ZERO_DAYS_DORMANT", false);
                rngHelper.createPercentageTD(l.get(i));
            }
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        sandHelper.collectionQuery(identifier);
        String id= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        Thread.sleep(4000);

        sandHelper.collectionQuery(identifier1);
        String id1= sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id1).ResponseValidator.GetBodyAsText();

        String aggregatorAfter=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(sandHelper.JsonString(aggregatorAfter, "$.data.collections[*].id").split(","));
        System.out.println("collection id"+ id +id1);
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }



    @Test(dataProvider = "latLongDelPer", description = "creating free delcollection for super , which is not visible")
    public void freeDelColOnAggregatorS(String lat ,String lng, String identifier, String identifier1) throws InterruptedException {
        HashMap<String, String>hMap=sandHelper.loginData(SANDConstants.mobile, SANDConstants.password);

        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, hMap.get("Tid"), hMap.get("Token"));
        try {
            List<String> firstNElementsList = listOfRestIds.stream().limit(5).collect(Collectors.toList());
            for (int i = 0; i < firstNElementsList.size(); i++) {
                rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(i), false, false, false, "ZERO_DAYS_DORMANT", false);
                //rngHelper.createPercentageTD(l.get(i));
            }
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        sandHelper.collectionQuery(identifier);
        String id = sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        Thread.sleep(4000);

        String aggAfterDiscount = sandHelper.aggregator(new String[]{lat, lng}, hMap.get("Tid"), hMap.get("Token")).ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(sandHelper.JsonString(aggAfterDiscount, "$.data.collections[*].id").split(","));
        System.out.println("collection id" + id);
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }

    @Test(dataProvider = "latLongDelFlat", description = "creating free del and flat collection")
    public void freeDelColOnAggregatorFlat(String lat ,String lng, String identifier, String identifier1) throws InterruptedException{
        HashMap<String, String>hMap=sandHelper.loginData(SANDConstants.mobile, SANDConstants.password);

        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, hMap.get("Tid"), hMap.get("Token"));
        try {
            List<String> firstNElementsList = listOfRestIds.stream().limit(5).collect(Collectors.toList());
            for (int i = 0; i < firstNElementsList.size(); i++) {
                rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(i), false, false, false, "ZERO_DAYS_DORMANT", false);
                rngHelper.createFlatWithMinCartAmountAtRestaurantLevel(listOfRestIds.get(5),"100" ,"10", false);
            }
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        sandHelper.collectionQuery(identifier);
        String id = sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id);
        Thread.sleep(4000);
        sandHelper.collectionQuery(identifier);
        String id1 = sandHelper.collectionId().get("id").toString();
        sandHelper.collection(id1);

        String aggAfterDiscount = sandHelper.aggregator(new String[]{lat, lng}, hMap.get("Tid"), hMap.get("Token")).ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(sandHelper.JsonString(aggAfterDiscount, "$.data.collections[*].id").split(","));
        System.out.println("collection id" + id);
        Thread.sleep(9000);
        Assert.assertTrue(collectionIds.contains(id), "collection is not present");
    }





    public void freeBie(String restId,String minAmount){
        HashMap<String, String> hMap;
        sandHelper.TMenu(restId);
        String item = orderPlace.getItemFromMenu(restId, null, null);
        try {
            hMap = rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel(minAmount, restId, item);
            String getTid = RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), freebie, "No discounts present or different type of discount is there");
        } catch (Exception e) {
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
    }


}
