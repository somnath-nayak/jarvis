package com.swiggy.api.sf.rng.pojo.SwiggySuper;

public class Createplanuserincentivemapping {

	public String plan_id="";
	public String user_segment="USER";
	public String user_id="";
	public String incentive_id="null";
	public String created_by= "By-Super-Automation";
	
	public Createplanuserincentivemapping() {
		
	}
	
	public Createplanuserincentivemapping(String plan_id,String userType,String incentiveId) {
		this.plan_id= plan_id;
		if(!(userType.equalsIgnoreCase("USER")))		{		
		this.user_id= userType;
				}
		else {
			this.user_segment= userType;	
		}
		this.incentive_id= incentiveId;	
	}
	
	public String getPlanId() {
		return this.plan_id;
	}
	public void setPlanId(String plan_id) {
		this.plan_id= plan_id;
	}
	public String getIncentiveId() {
		return this.incentive_id;
	}
	public void setIncentiveId(String incentive_id) {
		this.incentive_id= incentive_id;
	}
	public String getUserId() {
		return this.user_id;
	}
	public void setUserid(String user_id) {
		this.user_id= user_id;
	}
	public String getUserSegment() {
		return this.user_segment;
	}
	public void setUserSegment(String user_segment) {
		this.user_segment= user_segment;
	}
	public String getCreateBy() {
		return this.created_by;
	}
	public void setCreateBy(String created_by) {
		this.created_by= created_by;
	}
	
	@Override
	public String toString() {

		return "{" 
		             + "\"plan_id\"" + ":" + plan_id + "," 
		              + "\"user_segment\"" + ":" + "\"" + user_segment + "\""+ ","
		              + "\"incentive_id\"" + ":" + incentive_id + ","
		               + "\"created_by\"" + ":" + "\"" + created_by + "\""
		              
		              + "}"
		            		             ;
		} 
		public String MappingUserId() {
			
			return  "{" 
        + "\"plan_id\"" + ":" + plan_id + "," 
        + "\"user_id\"" + ":" + "\"" + user_id + "\""+ ","
        + "\"incentive_id\"" + ":" + incentive_id + ","
         + "\"created_by\"" + ":" + "\"" + created_by + "\""
        
        + "}"
      		             ;
		}
}
		
	
	
	
	
	
	
	
	
	


	