package com.swiggy.api.sf.rng.tests;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.dp.PitaraDP;
import com.swiggy.api.sf.rng.helper.PitaraHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.pitara.*;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

public class Pitara extends DontUseThis {

    GameOfThronesService service;
    Processor processor;
    RngHelper rngHelper = new RngHelper();
    DBHelper dbHelper = new DBHelper();
    RedisHelper redisHelper = new RedisHelper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    JsonHelper j = new JsonHelper();
    PitaraHelper pitaraHelper = new PitaraHelper();
    public String contextId = null, contextId2 = null;

    CheckoutHelper ch = new CheckoutHelper();
    String CardContextMapId = null;
    String CardValidationMapId = null;
    String CardValidationMapId2 = null;
    String CardValidationMapId3 = null;
    String CardValidationMapId4 = null;
    String CardValidationMapId5 = null;
    String CardValidationMapId6 = null;
    String CardValidationMapId7 = null;


    DontUseThis coupon = null;
    String couponCode = null;
    String showRateCardEnabled = "1";
    String notShowRateEnabled = "0";
    String extremeRain = "1";
    String normalRain = "0";

    String collectionValidationId = null;
    String couponValidtionId = null, privateCouponValidationId = null, privateCouponId = null, showRateValidationId_false = null, showRateValidationId_true = null, extremeRainCardValidationId = null, normalRainCardValidationId = null, superValidationId = null;
    String isPreOrderValidationId_true = null, isPreOrderValidationId_false = null, hasPreOrderValidationId_true = null, hasPreOrderValidationId_false = null;
    String hasPreOrderValidationId = null, isPreOrderEnabledValidationId_true = null, isPreOrderEnabledValidationId_false = null, isLongDistanceOrderValidationId_true = null, isLongDistanceOrderValidationId_false = null, hasPlacedLongDistanceOrderValidationId_true = null, hasPlacedLongDistanceOrderValidationId_false = null;
    String isPreOrderEnabledValidationId = null, launchCardValiadation_false = null, launchCardValiadation_true = null, launchCorporateCafeValidation_true = null, launchCorporateCafeValidation_false = null;
    String isLongDistanceOrderValidationId = null;
    String hasPlacedLongDistanceOrderValidationId = null;
    String isCafeValidationEnabled_true = null;
    String isCafeValidationEnabled_false = null;
    String seenCountValidation = null, seenCountValidationMinMax = null;
    String orderCountValidation = null, orderCountValidationWithMin = null;
    String clickCountValidation = null;
    String isSuperOrderCardValidation_true = null, isSuperOrderCardValidation_false = null, isSwiggyAssuredTrue = null, isSwiggyAssuredFalse = null, hasSwiggyAssuredTrue = null, hasSwiggyAssuredFalse = null;
    String superUserValidation = null, superUserValidationWithMinAsTen = null, superUserValidationWithMaxAsTen = null;

    public String NUX_CollectionCard = null;
    public String NUX_CouponCard = null;
    public String sCard1 = null, sCard2 = null,sCardv2 = null;
    public String rainCard1 = null, rainCard2 = null;

    public String lCard = null;
    public String XlCard = null;
    public String XlCard2 = null;
    public String XlCard3 = null;
    public String XlCard4 = null;
    public String launchCard1 = null;
    public String launchCard2 = null;
    public String getSuperCard = null;
    public String tid, token, userId, tid2, token2, userId2;


    PitaraHelper helper = new PitaraHelper();
    WireMockHelper wireMockHelper;


    @BeforeClass
    public void beforeClass() {
        PitaraHelper helper = new PitaraHelper();
        helper.disableAllContext();
        helper.disableAllCards();


        contextId = helper.createContext();
        contextId2 = helper.createTrackContext();

        helper.createContextKeynMap(contextId);
        helper.createContextKeynMap(contextId2);

        String keymapId = helper.createContextKeynMap(contextId);
        String keymapId2 = helper.createContextKeynMap(contextId2);

        helper.createKeyValue(keymapId);
        helper.trackCreateKeyValue(keymapId2);

        isPreOrderValidationId_true = helper.createValidation(PitaraHelper.ValidationType.lcard_isPreOrder, "true");
        isPreOrderValidationId_false = helper.createValidation(PitaraHelper.ValidationType.lcard_isPreOrder, "false");
        hasPreOrderValidationId_true = helper.createValidation(PitaraHelper.ValidationType.lcard_hasPreOrder, "true");
        hasPreOrderValidationId_false = helper.createValidation(PitaraHelper.ValidationType.lcard_hasPreOrder, "false");

        superUserValidation = helper.createValidation(PitaraHelper.ValidationType.superCard);
        superUserValidationWithMinAsTen = helper.createValidation(PitaraHelper.ValidationType.superCardMin);
        superUserValidationWithMaxAsTen = helper.createValidation(PitaraHelper.ValidationType.superCardMax);

        isSuperOrderCardValidation_true = helper.createValidation(PitaraHelper.ValidationType.isSuperOrder, "true");
        isSuperOrderCardValidation_false = helper.createValidation(PitaraHelper.ValidationType.isSuperOrder, "false");
        isPreOrderEnabledValidationId_true = helper.createValidation(PitaraHelper.ValidationType.lcard_isPreOrderEnabled, "true");
        isPreOrderEnabledValidationId_false = helper.createValidation(PitaraHelper.ValidationType.lcard_isPreOrderEnabled, "false");


        isLongDistanceOrderValidationId_true = helper.createValidation(PitaraHelper.ValidationType.xlcard_isLongDistanceOrder, "true");
        isLongDistanceOrderValidationId_false = helper.createValidation(PitaraHelper.ValidationType.xlcard_isLongDistanceOrder, "false");
        hasPlacedLongDistanceOrderValidationId_true = helper.createValidation(PitaraHelper.ValidationType.xlcard_hasLongDistanceOrder, "true");
        hasPlacedLongDistanceOrderValidationId_false = helper.createValidation(PitaraHelper.ValidationType.xlcard_hasLongDistanceOrder, "false");
        launchCardValiadation_false = helper.createValidation(PitaraHelper.ValidationType.launchCard, "false");
        launchCardValiadation_true = helper.createValidation(PitaraHelper.ValidationType.launchCard, "true");
        launchCorporateCafeValidation_true =  helper.createValidation(PitaraHelper.ValidationType.launchCorporateCafeCard, "true");
        launchCorporateCafeValidation_false =  helper.createValidation(PitaraHelper.ValidationType.launchCorporateCafeCard, "false");

        seenCountValidation = helper.createValidation(PitaraHelper.ValidationType.seenCount);
        seenCountValidationMinMax = helper.createValidation(PitaraHelper.ValidationType.seenCountMinMax);
        orderCountValidation = helper.createValidation(PitaraHelper.ValidationType.orderCount);
        orderCountValidationWithMin = helper.createValidation(PitaraHelper.ValidationType.orderCountMin);
        clickCountValidation = helper.createValidation(PitaraHelper.ValidationType.clickCount);
        isSwiggyAssuredTrue = helper.createValidation(PitaraHelper.ValidationType.isSwiggyAssured, "true");
        isSwiggyAssuredFalse= helper.createValidation(PitaraHelper.ValidationType.isSwiggyAssured, "false");
        hasSwiggyAssuredTrue = helper.createValidation(PitaraHelper.ValidationType.hasSwiggyAssured,"true");
        hasSwiggyAssuredFalse = helper.createValidation(PitaraHelper.ValidationType.hasSwiggyAssured,"false");

//        isSuperOrderCardValidation = helper.createValidation(PitaraHelper.ValidationType.scardV2);

        SANDHelper sandHelper = new SANDHelper();
//        sandHelper.createUser("dev",RngConstants.mobile1,RngConstants.mobile1+"@gmail.com" ,RngConstants.password1);
        HashMap<String, String> hm = rngHelper.getHeader();
        tid = hm.get("Tid");
        token = hm.get("Token");
        userId = hm.get("CustomerId");
//        HashMap<String, String> hm2 = rngHelper.getHeader("user2");
//        tid2 = hm2.get("Tid");
//        token2 = hm2.get("Token");
//        userId2 = hm2.get("CustomerId");
//        wireMockHelper = new WireMockHelper();
//        wireMockHelper.startMockServer(5373);
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void fetchNuxcardValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {

        NUX_CollectionCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, NUX_CollectionCard);
        collectionValidationId = helper.createValidation(PitaraHelper.ValidationType.collection);
        helper.createCardValidationMap(NUX_CollectionCard, collectionValidationId);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    Assert.assertNotNull(JsonPath.read(response, "$.data.cards..link"));
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists("data.cards[0].cta.link"));
                }
            }
        }
        finally {
            helper.deleteCards(NUX_CollectionCard);
            helper.cacheEvict();
        }


    }

    @Test(dataProvider = "fetchScardCard", dataProviderClass = PitaraDP.class)
    public void sCardValidation_validationTrue(UserOrderSegment uso, UserSegment us, Payload pl, String showRateApp) throws IOException {


        showRateValidationId_true = helper.createValidation(PitaraHelper.ValidationType.showrate, "1");
        sCard1 = helper.createCard(PitaraHelper.CardType.scard);
        helper.createCardValidationMap(sCard1, showRateValidationId_true);
        helper.createCardContextMap(contextId2, sCard1);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, showRateApp);
        String response = processor.ResponseValidator.GetBodyAsText();
        List al = JsonPath.read(response, "$.data.cards");
        try {
            if (showRateApp.equals("1")) {
                if (al.size() == 0) {
                    Assert.assertTrue(false, "cards are coming empty");
                } else {
                    List al2 = JsonPath.read(response, "$.data.cards..type");

                    Assert.assertEquals(al2.get(0).toString(), "S_CARD");
                    Assert.assertEquals(JsonPath.read(response, "$.data.cards..id").toString(), "[" + sCard1 + "]");
                }

            } else {
                if (al.size() != 0) {
                    Assert.assertTrue(false, "cards are coming empty");
                } else {
                    Assert.assertTrue(true);
                }

            }

        } finally {
            helper.deleteCards(sCard1);
            helper.cacheEvict();
        }

    }

    @Test(dataProvider = "fetchScardCard", dataProviderClass = PitaraDP.class)
    public void sCardValidation_validationfalse(UserOrderSegment uso, UserSegment us, Payload pl, String showRateApp) throws IOException {

        showRateValidationId_false = helper.createValidation(PitaraHelper.ValidationType.showrate, "0");
        sCard2 = helper.createCard(PitaraHelper.CardType.scard);
        helper.createCardValidationMap(sCard2, showRateValidationId_false);
        helper.createCardContextMap(contextId2, sCard2);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, showRateApp);
        String response = processor.ResponseValidator.GetBodyAsText();
        List al = JsonPath.read(response, "$.data.cards");
        try {
            if (showRateApp.equals("1")) {
                if (al.size() != 0) {
                    Assert.assertTrue(false, "cards are coming empty");
                } else {
                    Assert.assertTrue(true);
                }
            } else {

                if (al.size() == 0) {
                    Assert.assertTrue(false, "cards are coming empty");
                } else {
                    List al2 = JsonPath.read(response, "$.data.cards..type");

                    Assert.assertEquals(al2.get(0).toString(), "S_CARD");
                    Assert.assertEquals(JsonPath.read(response, "$.data.cards..id").toString(), "[" + sCard2 + "]");
                }

            }
        } finally {
            helper.deleteCards(sCard2);
            helper.cacheEvict();
        }

    }


    @Test(dataProvider = "rainCard", dataProviderClass = PitaraDP.class)
    public void rainCardValidation_Extreme(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {

        rainCard1 = helper.createCard(PitaraHelper.CardType.rain);
        extremeRainCardValidationId = helper.createValidation(PitaraHelper.ValidationType.rain, "EXTREME");
        helper.createCardValidationMap(rainCard1, extremeRainCardValidationId);
        helper.createCardContextMap(contextId, rainCard1);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data.cards");
        try {
            if (pl.getRAIN_NUDGE_CARD().getModes().get(0).getValue().toString().equalsIgnoreCase("EXTREME")) {
                if (al.size() == 0) {
                    Assert.assertTrue(false, "cards are coming empty");
                } else {
                    List cards = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.cards..cardType");
                    List ids = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.cards..id");
                    Assert.assertEquals(cards.get(0).toString(), "RAIN_NUDGE_CARD");
                }
            } else {
                if (al.size() == 0) {
                    Assert.assertTrue(true, "cards are coming empty");
                } else {
                    Assert.assertTrue(false);
                }
            }
        } finally {

            helper.deleteCards(rainCard1);
            helper.cacheEvict();
        }

    }

    @Test(dataProvider = "rainCard", dataProviderClass = PitaraDP.class)
    public void rainCardValidation_Normal(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {


        rainCard2 = helper.createCard(PitaraHelper.CardType.rain);
        normalRainCardValidationId = helper.createValidation(PitaraHelper.ValidationType.rain, "NORMAL");
        helper.createCardValidationMap(rainCard2, normalRainCardValidationId);
        helper.createCardContextMap(contextId, rainCard2);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        try {
            List al = JsonPath.read(response, "$.data.cards");
            if (pl.getRAIN_NUDGE_CARD().getModes().get(0).getValue().toString().equalsIgnoreCase("EXTREME")) {

                if (al.size() == 0) {
                    Assert.assertTrue(true, "cards are coming empty");
                } else {
                    Assert.assertTrue(false);
                }
            } else {
                if (al.size() == 0) {
                    Assert.assertTrue(false, "cards are coming empty");
                } else {
                    List cards = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.cards..cardType");
                    List ids = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.cards..id");
                    Assert.assertEquals(cards.get(0).toString(), "RAIN_NUDGE_CARD");
                }
            }
        } finally {
            helper.deleteCards(rainCard2);
            helper.cacheEvict();
        }

    }

    @Test(dataProvider = "couponCard", dataProviderClass = PitaraDP.class, priority = 0)
    public void couponCardValidation_PublicCoupon(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        NUX_CouponCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, NUX_CouponCard);
        String couponId = pitaraHelper.createCopuonForPitara("0");
        couponValidtionId = helper.createValidation(PitaraHelper.ValidationType.coupon, couponId);
        helper.createCardValidationMap(NUX_CouponCard, couponValidtionId);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data.cards");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "cards are coming empty");
            } else {
                List cardIds = JsonPath.read(response, "$.data.cards..id");

                Assert.assertEquals(cardIds.get(0).toString(), NUX_CouponCard);
            }
        } finally {
            helper.deleteCards(NUX_CouponCard);
            helper.cacheEvict();

        }
    }

    @Test(dataProvider = "couponCard", dataProviderClass = PitaraDP.class, priority = 0)
    public void couponCardValidation_PrivateCoupon(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String userId = String.valueOf(new RandomNumber(1000, 899999).nextInt());
//        userId="7202587";
        NUX_CouponCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, NUX_CouponCard);
        privateCouponId = pitaraHelper.createCopuonForPitara("1",userId);
        privateCouponValidationId = helper.createValidation(PitaraHelper.ValidationType.coupon, privateCouponId);
        helper.createCardValidationMap(NUX_CouponCard, privateCouponValidationId);

        us.setUserId(Integer.valueOf(userId));

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data.cards");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "cards are coming empty");
            } else {
                    List cardIds = JsonPath.read(response, "$.data.cards..id");
                    Assert.assertEquals(cardIds.get(0).toString(), NUX_CouponCard);
            }
        } finally {

            helper.deleteCards(NUX_CouponCard);
            helper.cacheEvict();

        }
    }

    @Test(dataProvider = "couponCard", dataProviderClass = PitaraDP.class, priority = 0)
    public void couponCardValidationPrivateCouponInvalidUserId(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {

        String userId = String.valueOf(new RandomNumber(900000, 999999).nextInt());
        NUX_CouponCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        privateCouponId = pitaraHelper.createCopuonForPitara("1",userId);
        privateCouponValidationId = helper.createValidation(PitaraHelper.ValidationType.coupon, privateCouponId);
        helper.createCardContextMap(contextId, NUX_CouponCard);
        helper.createCardValidationMap(NUX_CouponCard, privateCouponValidationId);

        us.setUserId(Integer.valueOf(500));

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data.cards");
        try {
            Assert.assertFalse(al.isEmpty(),"Assertion Failed, for invalid user id, cards shouldn't come");
        } finally {

            helper.deleteCards(NUX_CouponCard);
            helper.cacheEvict();

        }
    }

    @Test(dataProvider = "superCard", dataProviderClass = PitaraDP.class, priority = 0)
    public void superCardValidation_Listing(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/plan/user/0", responseFile="SuperMockResponse";
        setMockGetRequest(uri,responseFile);
        userId = "6114643";
        String getSuperCard = helper.createCard(PitaraHelper.CardType.superCard);
        helper.createCardContextMap(contextId, getSuperCard);
        superValidationId = helper.createValidation(PitaraHelper.ValidationType.superCard);
        helper.createCardValidationMap(getSuperCard, superValidationId);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getSuperCard(body, RngConstants.userId);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(al.get(0).toString(), "SUPER_LISTING_CARD");
            }
        } finally {
            helper.deleteCards(getSuperCard);
            helper.cacheEvict();
        }


    }

    @Test(dataProvider = "superCardTrack", dataProviderClass = PitaraDP.class)
    public void superCardValidation_Track(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {

//        http://localhost:6666/api/v1/plan/user/0
        getSuperCard = helper.createCard(PitaraHelper.CardType.superCard);
        helper.createCardContextMap(contextId2, getSuperCard);
        superValidationId = helper.createValidation(PitaraHelper.ValidationType.superCard);
        helper.createCardValidationMap(getSuperCard, superValidationId);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackSuperCard(body, RngConstants.userId);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(al.get(0).toString(), "SUPER_LISTING_CARD");
            }
        } finally {

            helper.deleteCards(getSuperCard);
            helper.cacheEvict();

        }


    }

    @Test(dataProvider = "couponCard", dataProviderClass = PitaraDP.class)
    public void TwoCardValidation_Priority(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        NUX_CollectionCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, NUX_CollectionCard);
        collectionValidationId = helper.createValidation(PitaraHelper.ValidationType.collection);
        helper.createCardValidationMap(NUX_CollectionCard, collectionValidationId);


        NUX_CouponCard = helper.createCard(PitaraHelper.CardType.nuxcard2);
        helper.createCardContextMap(contextId, NUX_CouponCard);
        String couponId = pitaraHelper.createCopuonForPitara("0");
        couponValidtionId = helper.createValidation(PitaraHelper.ValidationType.coupon, couponId);
        helper.createCardValidationMap(NUX_CouponCard, couponValidtionId);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");

        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(JsonPath.read(response, "$.data.cards..id").toString(), "[" + NUX_CouponCard + "]");
            }
        } finally {
            helper.deleteCards(getSuperCard);
            helper.cacheEvict();
        }


    }

//    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
//    public void lCardValidation_withIspreOrder_Enable_HAS_PLACED_PRE_ORDER_false_IS_PRE_ORDER_true_Validation(UserOrderSegment uso, UserSegment us, Payload pl,String isPreOrder) throws IOException {
//
//
//        String orderId = getOrder(tid,token,true);
//        helper.preOrderEnableHelper(RngConstants.restId,RngConstants.areaId);
//
//
//        lCard = helper.createCard(PitaraHelper.CardType.lcard);
//        helper.createCardValidationMap(lCard, isPreOrderValidationId_true);
//        helper.createCardValidationMap(lCard, hasPreOrderValidationId_false);
//        helper.createCardValidationMap(lCard, isPreOrderEnabledValidationId_true);
//        helper.createCardContextMap(contextId2,lCard);
//
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body,userId,orderId,tid,token,isPreOrder);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//        List al = JsonPath.read(response, "$.data..cardType");
//       try
//       {
//           if(al.size()==0)
//           {
//               Assert.assertTrue(false,"NoCardFound");
//           }
//           else {
//               Assert.assertEquals(JsonPath.read(response, "$.data.cards..id").toString(), "[" + lCard + "]");
//           }
//       }
//        finally {
//           helper.deleteCards(lCard);
//           helper.cacheEvict();
//       }
//    }
//


//    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
//    public void lCardValidation_withIspreOrder_Enable_HAS_PLACED_PRE_ORDER_false_IS_PRE_ORDER_false_Validation(UserOrderSegment uso, UserSegment us, Payload pl,String isPreOrder) throws IOException {
//
//
//        String orderId = getOrder(tid2,token2,false);
//        helper.preOrderEnableHelper(RngConstants.restId,RngConstants.areaId);
//
//
//        lCard = helper.createCard(PitaraHelper.CardType.lcard);
//
//        helper.createCardValidationMap(lCard, isPreOrderValidationId_false);
//        helper.createCardValidationMap(lCard, hasPreOrderValidationId_false);
//        helper.createCardValidationMap(lCard, isPreOrderEnabledValidationId_true);
//        helper.createCardContextMap(contextId2,lCard);
//
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body,userId2,orderId,tid2,token2,isPreOrder);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//        List al = JsonPath.read(response, "$.data..cardType");
//        List al2 = null;
//        if(isPreOrder.equalsIgnoreCase("1")) {
//
//            if (al.size() == 0) {
//                Assert.assertTrue(false, "NoCardFound");
//
//            } else {
//                al2 = JsonPath.read(response, "$.data.cards..id");
//                System.out.println("======================================"+lCard);
//                boolean flag = false;
//                for(Object cardId: al2)
//                    if(cardId.toString().equalsIgnoreCase(lCard))
//                        flag = true;
//                Assert.assertTrue(flag, "The card id is not visible");
//
//            }
//        }else
//        {
//           Assert.assertEquals(al.size(),0,"Card should not be found");
//
//        }
//        helper.deleteCards(lCard);
//        helper.cacheEvict();
//    }
//

//    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lCardValidation_with_HAS_PLACED_PRE_ORDER_Validation(UserOrderSegment uso, UserSegment us, Payload pl, String urlparam) throws IOException {
        String orderId = getOrder(tid, token, true);
//        String orderId = "12345";

        helper.preOrderEnableHelper(RngConstants.restId, RngConstants.areaId);
        lCard = helper.createCard(PitaraHelper.CardType.lcard);
        if (urlparam.equalsIgnoreCase("0"))

        {
            helper.createCardValidationMap(lCard, hasPreOrderValidationId_false);
        } else {
            helper.createCardValidationMap(lCard, hasPreOrderValidationId_true);
        }
        helper.createCardContextMap(contextId2, lCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        try {
            List al = JsonPath.read(response, "$.data..cardType");
            List al2 = null;

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + lCard);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(lCard))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

//    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lCardValidation_with_HAS_PLACED_PRE_ORDER_True_Validation(UserOrderSegment uso, UserSegment us, Payload pl, String urlparam) throws IOException {
        String orderId = getOrder(tid, token, true);
        helper.preOrderEnableHelper(RngConstants.restId, RngConstants.areaId);
        lCard = helper.createCard(PitaraHelper.CardType.lcard);

        helper.createCardValidationMap(lCard, hasPreOrderValidationId_true);
        helper.createCardContextMap(contextId2, lCard);
        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;
        try {
            if (urlparam.equalsIgnoreCase("1")) {
                if (al.size() == 0) {
                    Assert.assertTrue(false, "NoCardFound");

                } else {
                    al2 = JsonPath.read(response, "$.data.cards..id");
                    System.out.println("======================================" + lCard);
                    boolean flag = false;
                    for (Object cardId : al2)
                        if (cardId.toString().equalsIgnoreCase(lCard))
                            flag = true;
                    Assert.assertTrue(flag, "The card id is not visible");

                }

            } else {
                Assert.assertEquals(al.size(), 0);
            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

//    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lCardValidation_with_Is_PLACED_PRE_ORDER_Validation(UserOrderSegment uso, UserSegment us, Payload pl, String urlparam) throws IOException {

        String orderId = getOrder(tid, token, true);
        helper.preOrderEnableHelper(RngConstants.restId, RngConstants.areaId);
        lCard = helper.createCard(PitaraHelper.CardType.lcard);
        if (urlparam.equalsIgnoreCase("0"))

        {
            helper.createCardValidationMap(lCard, isPreOrderValidationId_true);
        } else {
            helper.createCardValidationMap(lCard, isPreOrderValidationId_false);
        }
        helper.createCardContextMap(contextId2, lCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;
        try {
            if (urlparam.equalsIgnoreCase("0")) {
                if (al.size() == 0) {
                    Assert.assertTrue(false, "NoCardFound");

                } else {
                    al2 = JsonPath.read(response, "$.data.cards..id");
                    System.out.println("======================================" + lCard);
                    boolean flag = false;
                    for (Object cardId : al2)
                        if (cardId.toString().equalsIgnoreCase(lCard))
                            flag = true;
                    Assert.assertTrue(flag, "The card id is not visible");

                }

            } else {
                Assert.assertEquals(al.size(), 0);
            }
        } finally {

            helper.deleteCards(lCard);
            helper.cacheEvict();

        }
    }


    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lCardValidation_with_Is_PRE_ORDER_Enabled_True_Validation(UserOrderSegment uso, UserSegment us, Payload pl, String urlparam) throws IOException {

        String orderId = getOrder(tid, token, true);
        helper.preOrderEnableHelper(RngConstants.restId, RngConstants.areaId);
        lCard = helper.createCard(PitaraHelper.CardType.lcard);

        helper.createCardValidationMap(lCard, isPreOrderEnabledValidationId_true);

        helper.createCardContextMap(contextId2, lCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token, urlparam);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;
        try {
            if (urlparam.equalsIgnoreCase("1")) {
                if (al.size() == 0) {
                    Assert.assertTrue(false, "NoCardFound");

                } else {
                    al2 = JsonPath.read(response, "$.data.cards..id");
                    System.out.println("======================================" + lCard);
                    boolean flag = false;
                    for (Object cardId : al2)
                        if (cardId.toString().equalsIgnoreCase(lCard))
                            flag = true;
                    Assert.assertTrue(flag, "The card id is not visible");

                }

            } else {
                Assert.assertEquals(al.size(), 0);
            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }

    }


    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lCardValidation_with_Is_PRE_ORDER_Enabled_False_Validation(UserOrderSegment uso, UserSegment us, Payload pl, String urlparam) throws IOException {
        helper.cacheEvict();

        String orderId = getOrder(tid, token, true);
        helper.preOrderEnableHelper(RngConstants.restId, RngConstants.areaId);
        lCard = helper.createCard(PitaraHelper.CardType.lcard);

        helper.createCardValidationMap(lCard, isPreOrderEnabledValidationId_false);

        helper.createCardContextMap(contextId2, lCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token, urlparam);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;
        try {
            if (urlparam.equalsIgnoreCase("1")) {
                Assert.assertEquals(al.size(), 0);

            } else {


                if (al.size() == 0) {
                    Assert.assertTrue(false, "NoCardFound");

                } else {
                    al2 = JsonPath.read(response, "$.data.cards..id");
                    System.out.println("======================================" + lCard);
                    boolean flag = false;
                    for (Object cardId : al2)
                        if (cardId.toString().equalsIgnoreCase(lCard))
                            flag = true;
                    Assert.assertTrue(flag, "The card id is not visible");

                }
            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }

    }

//
//
//    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class, priority = 0)
//    public void lCardValidation_withIspreOrder_Enable_HAS_PLACED_PRE_ORDER_true_IS_PRE_ORDER_false_Validation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
//        helper.createCardValidationMap(lCard, isPreOrderValidationId_false);
//        helper.createCardValidationMap(lCard, hasPreOrderValidationId_true);
//        helper.createCardValidationMap(lCard, isPreOrderEnabledValidationId_true);
//
//
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getNuxCollectionCard(body);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//
//    }
//
//
//    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class, priority = 0)
//    public void lCardValidation_withIspreOrder_Enable_HAS_PLACED_PRE_ORDER_true_IS_PRE_ORDER_true_Validation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
//        helper.createCardValidationMap(lCard, isPreOrderValidationId_true);
//        helper.createCardValidationMap(lCard, hasPreOrderValidationId_true);
//        helper.createCardValidationMap(lCard, isPreOrderEnabledValidationId_true);
//
//
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getNuxCollectionCard(body);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//
//    }
//
//
//    @Test(dataProvider = "nuxCard", dataProviderClass = PitaraDP.class, priority = 0)
//    public void xlCardValidation_isLongDistanceOrderValidationId_true_hasPlacedLongDistanceOrderValidationId_true(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
//        helper.createCardValidationMap(XlCard1, isLongDistanceOrderValidationId_true);
//        helper.createCardValidationMap(XlCard1, hasPlacedLongDistanceOrderValidationId_true);
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getNuxCollectionCard(body);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//
//    }
//
//    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class, priority = 0)
//    public void xlCardValidation_isLongDistanceOrderValidationId_false_hasPlacedLongDistanceOrderValidationId_true(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
//        helper.createCardValidationMap(XlCard2, isLongDistanceOrderValidationId_false);
//        helper.createCardValidationMap(XlCard2, hasPlacedLongDistanceOrderValidationId_true);
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getNuxCollectionCard(body);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//
//    }

//    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class, priority = 0)
//    public void xlCardValidation_isLongDistanceOrderValidationId_true_hasPlacedLongDistanceOrderValidationId_false(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
//        helper.createCardValidationMap(XlCard3, isLongDistanceOrderValidationId_true);
//        helper.createCardValidationMap(XlCard3, hasPlacedLongDistanceOrderValidationId_false);
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getNuxCollectionCard(body);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//
//    }
//
//    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class, priority = 0)
//    public void xlCardValidation_isLongDistanceOrderValidationId_false_hasPlacedLongDistanceOrderValidationId_false(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
//        helper.createCardValidationMap(XlCard4, isLongDistanceOrderValidationId_false);
//        helper.createCardValidationMap(XlCard4, hasPlacedLongDistanceOrderValidationId_false);
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getNuxCollectionCard(body);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//
//    }
//
//    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class, priority = 0)
//    public void launchCardValidation_launchCardValiadation_false(UserOrderSegment uso, UserSegment us, Payload pl)throws IOException
//    {
//        helper.createCardValidationMap(launchCard1,launchCardValiadation_false);
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getNuxCollectionCard(body);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//
//    }
//    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class, priority = 0)
//    public void launchCardValidation_launchCardValiadation_true(UserOrderSegment uso, UserSegment us, Payload pl)throws IOException
//    {
//        helper.createCardValidationMap(launchCard2,launchCardValiadation_true);
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getNuxCollectionCard(body);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//
//
//
// }
//    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class, priority = 0)
//    public void launchCardValidation(UserOrderSegment uso, UserSegment us, Payload pl)throws IOException
//    {
//        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
//        String body = j.getObjectToJSON(nuxObj);
//        Processor processor = pitaraHelper.getNuxCollectionCard(body);
//        String response = processor.ResponseValidator.GetBodyAsText();
//        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
//
//    }

//    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class)
    public void xlCardValidation_with_longDistance_Enabled_False_Validation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {

        String orderId = getOrder(tid, token, true);

        XlCard = helper.createCard(PitaraHelper.CardType.xlcard);

        helper.createCardValidationMap(XlCard, isLongDistanceOrderValidationId_false);

        helper.createCardContextMap(contextId2, XlCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;

        try {

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + XlCard);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(XlCard))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }

    }


//    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class)
    public void xlCardValidation_with_longDistance_Enabled_True_Validation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String orderId = getOrder(tid, token, true);
        XlCard = helper.createCard(PitaraHelper.CardType.xlcard);

        helper.createCardValidationMap(XlCard, isLongDistanceOrderValidationId_true);

        helper.createCardContextMap(contextId2, XlCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;

        try {

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + XlCard);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(XlCard))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }

    }

    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class, priority = 0)
    public void launchCardValidation_with_lauchCard_Enabled_False_Validation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="feature/is-cafe-present.*", responseFile="CorporateCafeAvailableMockResponse";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        data.addProperty("available",false);
        json.add("data",data);
        setMockGetUrlPatternRequest(uri,json);

        String launchCard1 = helper.createCard(PitaraHelper.CardType.launchcard);
        helper.createCardValidationMap(launchCard1, launchCardValiadation_false);
        helper.createCardContextMap(contextId, launchCard1);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(launchCard1,10,10);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("XL_CARD", "LAUNCH_CARD");
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;

        try {

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + XlCard);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(launchCard1))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }

    }


//    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class)
    public void launchCardValidation_with_lauchCard_Enabled_True_Validation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {

        launchCard1 = helper.createCard(PitaraHelper.CardType.launchcard);

        helper.createCardValidationMap(launchCard1, launchCardValiadation_true);

        helper.createCardContextMap(contextId, launchCard1);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("XL_CARD", "LAUNCH_CARD");
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;

        try {

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + launchCard1);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(XlCard))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }

    }


    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void Nuxcard_With_clickCountValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {

        NUX_CollectionCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, NUX_CollectionCard);
        helper.createCardValidationMap(NUX_CollectionCard, clickCountValidation);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(NUX_CollectionCard,1,1);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    Assert.assertNotNull(JsonPath.read(response, "$.data.cards..link"));
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists("$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(NUX_CollectionCard);
            helper.cacheEvict();
        }


    }


    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void Nuxcard_With_orderCountValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {

        NUX_CollectionCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, NUX_CollectionCard);
        helper.createCardValidationMap(NUX_CollectionCard, orderCountValidation);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(NUX_CollectionCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    Assert.assertNotNull(JsonPath.read(response, "$.data.cards..link"));
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists("$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(NUX_CollectionCard);
            helper.cacheEvict();
        }


    }


    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void Nuxcard_With_seenCountValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {

        sCard1 = helper.createCard(PitaraHelper.CardType.scard);
        helper.createCardContextMap(contextId, sCard1);
        helper.createCardValidationMap(sCard1, seenCountValidation);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(sCard1,10,2);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("NUX_LISTING", "S_CARD");
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, "1");
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    Assert.assertNotNull(JsonPath.read(response, "$.data.cards..link"));
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists("$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(NUX_CollectionCard);
            helper.cacheEvict();
        }


    }

    public String getOrder(String tid, String token, boolean isPreOrder) {


        List al2 = null;
        processor = ch.getOrder(tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        if (isPreOrder) {
            List<String> al = JsonPath.read(response, "$.data..order_type");
            for (int i = 0; i < al.size(); i++) {
                if (al.get(i).toString().equalsIgnoreCase("pre_order")) {
                    al2 = JsonPath.read(response, "$.data..orders.[" + i + "]..order_id");
                }
            }
            if (al2.size() == 0) {
                Assert.assertTrue(false, "No PreOrder found for this user");
            }
        } else {
            List<String> al = JsonPath.read(response, "$.data..order_type");
            for (int i = 0; i < al.size(); i++) {
                if (al.get(i).toString().equalsIgnoreCase("regular")) {
                    al2 = JsonPath.read(response, "$.data..orders.[" + i + "]..order_id");
                }
            }
            if (al2.size() == 0) {
                Assert.assertTrue(false, "No PreOrder found for this user");
            }
        }


        return al2.get(0).toString();
    }


    /**
     *
     */
    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithClickCountValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        NUX_CollectionCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, NUX_CollectionCard);
        helper.createCardValidationMap(NUX_CollectionCard, clickCountValidation);
        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(NUX_CollectionCard,10,10);

        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                   Assert.assertTrue(cardsID.contains(Integer.valueOf(NUX_CollectionCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(NUX_CollectionCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithSeenCountValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String nuxCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId2, nuxCard);
        helper.createCardValidationMap(nuxCard, seenCountValidation);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(nuxCard,10,2);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, "1");
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertTrue(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }


    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithOrderCountValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="OrderCountMockResponse";

        setMockGetRequest(uri,responseFile);

        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, nuxCard);
        helper.createCardValidationMap(nuxCard, orderCountValidation);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(nuxCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertTrue(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }


    }

    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lcardWithIsPreOrderEnabled(UserOrderSegment uso, UserSegment us, Payload pl,String isPreOrder) throws IOException {
//        please use these orderid and userid as same are configured in mock server
        String uri ="api/v1/order/get", responseFile="IsPreorderMockResponse";
        setMockGetRequest(uri,responseFile);
        String orderId = "724040866";
        userId = "6114643";
        lCard = helper.createCard(PitaraHelper.CardType.lcard);
        helper.createCardValidationMap(lCard, isPreOrderValidationId_true);
        helper.createCardContextMap(contextId2,lCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body,userId,orderId,tid,token,isPreOrder);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
       try
       {
           if(al.size()==0)
           {
               Assert.assertTrue(false,"NoCardFound");
           }
           else {
               Assert.assertEquals(JsonPath.read(response, "$.data.cards..id").toString(), "[" + lCard + "]");
           }
       }
        finally {
           helper.deleteCards(lCard);
           helper.cacheEvict();
       }
    }

    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lcardWithHasPreOrderEnabled(UserOrderSegment uso, UserSegment us, Payload pl,String isPreOrder) throws IOException {
        String uri ="api/v1/order/get", responseFile="HasPreorderMockResponse";
        setMockGetRequest(uri,responseFile);
//        please use these orderid and userid as same are configured in mock server
        String orderId = "724040866";
        userId = "6114643";
        lCard = helper.createCard(PitaraHelper.CardType.lcard);
        helper.createCardValidationMap(lCard, hasPreOrderValidationId_true);
        helper.createCardContextMap(contextId2,lCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body,userId,orderId,tid,token,isPreOrder);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        try
        {
            if(al.size()==0)
            {
                Assert.assertTrue(false,"NoCardFound");
            }
            else {
                Assert.assertEquals(JsonPath.read(response, "$.data.cards..id").toString(), "[" + lCard + "]");
            }
        }
        finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "superCard", dataProviderClass = PitaraDP.class)
    public void sCardV2WithSuperValidationTrue(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/plan/user/0", responseFile="SuperMockResponse";
        setMockGetRequest(uri,responseFile);
        userId = "6114643";
        String getSuperUserCard = helper.createCard(PitaraHelper.CardType.scardv2);
        helper.setCardMetaDataSCardV2(getSuperUserCard);
        helper.createCardValidationMap(getSuperUserCard, superUserValidation);
        helper.createCardContextMap(contextId, getSuperUserCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(getSuperUserCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("NUX_LISTING", "S_CARD_V2");
        Processor processor = pitaraHelper.getSuperCard(body,userId);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List cards = JsonPath.read(response, "$.data..id");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(al.get(0).toString(), "S_CARD_V2");
                Assert.assertTrue(cards.contains(Integer.valueOf(getSuperUserCard)), "Assertion failed, didn't received same launch card");
            }
        } finally {
            helper.deleteCardMetaData(getSuperUserCard);
            helper.deleteCards(getSuperUserCard);
            helper.cacheEvict();
        }


    }


    @Test(dataProvider = "superCard", dataProviderClass = PitaraDP.class)
    public void sCardV2WithIsSuperOrderValidationTrue(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get_order_details", responseFile="IsSuperMockResponse";
        setMockGetRequest(uri,responseFile);

        userId = "6114643";
        getSuperCard = helper.createCard(PitaraHelper.CardType.scardv2);
        helper.setCardMetaDataSCardV2(getSuperCard);
        helper.createCardValidationMap(getSuperCard, isSuperOrderCardValidation_true);
        helper.createCardContextMap(contextId, getSuperCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(getSuperCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("NUX_LISTING", "S_CARD_V2");
        Processor processor = pitaraHelper.getSuperCard(body,userId);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List cards = JsonPath.read(response, "$.data..id");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertTrue(al.contains("S_CARD_V2"),"Assertion Failed, didn't received any S_CARD_V2");
                Assert.assertTrue(cards.contains(Integer.valueOf(getSuperCard)), "Assertion failed, didn't received same launch card");
            }
        } finally {
            helper.deleteCardMetaData(getSuperCard);
            helper.deleteCards(getSuperCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class)
    public void launchCardWithCafeAvailableValidation(UserOrderSegment uso, UserSegment us, Payload pl)throws IOException
    {
        String uri ="feature/is-cafe-present.*", responseFile="CafeAvailableMockResponse";

        setMockGetUrlPatternRequest(uri,responseFile);
        launchCard1 = helper.createCard(PitaraHelper.CardType.launchcard);
        helper.createCardValidationMap(launchCard1,launchCardValiadation_true);
        helper.createCardContextMap(contextId, launchCard1);
        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("XL_CARD", "LAUNCH_CARD");
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List cards = JsonPath.read(response, "$.data..id");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(al.get(0).toString(), "LAUNCH_CARD");
                Assert.assertTrue(cards.contains(Integer.valueOf(launchCard1)), "Assertion failed, didn't received same launch card");
            }
        } finally {
            helper.deleteCards(getSuperCard);
            helper.cacheEvict();
        }

 }

    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class)
    public void launchCardWithCorporateCafeAvailableValidation(UserOrderSegment uso, UserSegment us, Payload pl)throws IOException
    {
        String uri ="feature/is-cafe-present.*", responseFile="CorporateCafeAvailableMockResponse";
        setMockGetUrlPatternRequest(uri,responseFile);
        launchCard1 = helper.createCard(PitaraHelper.CardType.launchcard);
        helper.createCardValidationMap(launchCard1,launchCorporateCafeValidation_true);
        helper.createCardContextMap(contextId, launchCard1);
        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("XL_CARD", "LAUNCH_CARD");
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List cards = JsonPath.read(response, "$.data..id");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(al.get(0).toString(), "LAUNCH_CARD");
                Assert.assertTrue(cards.contains(Integer.valueOf(launchCard1)), "Assertion failed, didn't received same launch card");
            }
        } finally {
            helper.deleteCards(getSuperCard);
            helper.cacheEvict();
        }

    }

    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class)
    public void xlCardWithIsLongDistanceOrderValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="IsLongDistanceMockResponse";
        setMockGetRequest(uri,responseFile);
        String orderId = "724040866";
        userId = "6114643";
        XlCard = helper.createCard(PitaraHelper.CardType.xlcard);
        helper.createCardValidationMap(XlCard, isLongDistanceOrderValidationId_true);
        helper.createCardContextMap(contextId2, XlCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;

        try {

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + XlCard);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(XlCard))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class)
    public void xlCardWithHasPlacedLongDistanceOrderValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="HasPlacedLongDistanceMockResponse";
        setMockGetRequest(uri,responseFile);
        String orderId = "724040866";
        userId = "6114643";
        XlCard = helper.createCard(PitaraHelper.CardType.xlcard);
        helper.createCardValidationMap(XlCard, hasPlacedLongDistanceOrderValidationId_true);
        helper.createCardContextMap(contextId2, XlCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;

        try {

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + XlCard);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(XlCard))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithIsSwiggyAssuredValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="IsSwiggyAssuredMockResponse";
        setMockGetRequest(uri,responseFile);
        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardValidationMap(nuxCard, isSwiggyAssuredTrue);
        helper.createCardContextMap(contextId, nuxCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertTrue(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithHasSwiggyAssuredOrderValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="HasSwiggyAssuredMockResponse";
        setMockGetRequest(uri,responseFile);
        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardValidationMap(nuxCard, hasSwiggyAssuredTrue);
        helper.createCardContextMap(contextId, nuxCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertTrue(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithHasSwiggyAssuredOrderFalseValidationWhenOrdersIsNull(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="HasSwiggyAssuredMockResponse";
        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        data.add("orders", new JsonArray());
        json.add("data",data);
        setMockGetRequest(uri,json);

        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardValidationMap(nuxCard, hasSwiggyAssuredFalse);
        helper.createCardContextMap(contextId, nuxCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertTrue(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithHasSwiggyAssuredOrderFalseValidationWhenOrdersStatusIsNull(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="HasSwiggyAssuredMockResponse";
        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.add("order_status",null);
        orders = new JsonArray();
        orders.add(order);
        data.add("orders",orders);
        json.add("data",data);
        setMockGetRequest(uri,json);

        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardValidationMap(nuxCard, hasSwiggyAssuredFalse);
        helper.createCardContextMap(contextId, nuxCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertTrue(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithHasSwiggyAssuredOrderFalseValidationWhenIsAssuredIsZero(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="HasSwiggyAssuredMockResponse";
        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.addProperty("is_assured",0 );
        orders = new JsonArray();
        orders.add(order);
        data.add("orders",orders);
        json.add("data",data);
        setMockGetRequest(uri,json);

        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardValidationMap(nuxCard, hasSwiggyAssuredFalse);
        helper.createCardContextMap(contextId, nuxCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertTrue(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithIsSwiggyAssuredOrderFalseValidationWhenIsAssuredIsFalse(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="IsSwiggyAssuredMockResponse";
        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.addProperty("is_assured",false);
        setMockGetRequest(uri,json);

        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardValidationMap(nuxCard, isSwiggyAssuredFalse);
        helper.createCardContextMap(contextId, nuxCard);


        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertTrue(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithIsSwiggyAssuredOrderFalseValidationWhenOrderIdNotSame(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="IsSwiggyAssuredMockResponse";
        setMockGetRequest(uri,responseFile);
        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardValidationMap(nuxCard, isSwiggyAssuredFalse);
        helper.createCardContextMap(contextId, nuxCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCardWithOrderId(str, tid, token, "1122");
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
            Assert.assertFalse(cardsID.contains(nuxCard),"Assertion Failed, card shouldn't be present for invalid order id");
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class)
    public void xlCardWithHasPlacedLongDistanceOrderFalseValidationWhenLongDistanceIsFalse(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="HasPlacedLongDistanceMockResponse";
        String orderId = "724040866";
        userId = "6114643";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.addProperty("is_long_distance",false);
        orders = new JsonArray();
        orders.add(order);
        data.add("orders",orders);
        json.add("data",data);
        setMockGetRequest(uri,json);

        XlCard = helper.createCard(PitaraHelper.CardType.xlcard);
        helper.createCardValidationMap(XlCard, hasPlacedLongDistanceOrderValidationId_false);
        helper.createCardContextMap(contextId2, XlCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;

        try {

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + XlCard);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(XlCard))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class)
    public void xlCardWithHasPlacedLongDistanceOrderFalseValidationWhenLonDistanceIsFalse(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="HasPlacedLongDistanceMockResponse";
        String orderId = "724040866";
        userId = "6114643";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.addProperty("is_long_distance",false);
        orders = new JsonArray();
        orders.add(order);
        data.add("orders",orders);
        json.add("data",data);
        setMockGetRequest(uri,json);

        XlCard = helper.createCard(PitaraHelper.CardType.xlcard);
        helper.createCardValidationMap(XlCard, hasPlacedLongDistanceOrderValidationId_false);
        helper.createCardContextMap(contextId2, XlCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;

        try {

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + XlCard);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(XlCard))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class)
    public void xlCardWithIsLongDistanceOrderFalseValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="IsLongDistanceMockResponse";
        String orderId = "724040866";
        userId = "6114643";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.addProperty("is_long_distance",false);
        orders = new JsonArray();
        orders.add(order);
        data.add("orders",orders);
        json.add("data",data);
        setMockGetRequest(uri,json);

        XlCard = helper.createCard(PitaraHelper.CardType.xlcard);
        helper.createCardValidationMap(XlCard, isLongDistanceOrderValidationId_false);
        helper.createCardContextMap(contextId2, XlCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List al2 = null;

        try {

            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");

            } else {
                al2 = JsonPath.read(response, "$.data.cards..id");
                System.out.println("======================================" + XlCard);
                boolean flag = false;
                for (Object cardId : al2)
                    if (cardId.toString().equalsIgnoreCase(XlCard))
                        flag = true;
                Assert.assertTrue(flag, "The card id is not visible");

            }
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "xlCard", dataProviderClass = PitaraDP.class)
    public void xlCardWithIsLongDistanceOrderFalseValidationWhenInvalidOrderId(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="IsLongDistanceMockResponse";
        setMockGetRequest(uri,responseFile);
        String orderId = "1122334";
        userId = "6114643";
        XlCard = helper.createCard(PitaraHelper.CardType.xlcard);
        helper.createCardValidationMap(XlCard, isLongDistanceOrderValidationId_false);
        helper.createCardContextMap(contextId2, XlCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, userId, orderId, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data.cards..id");
        List al2 = null;

        try {
            Assert.assertFalse(al.contains(Integer.valueOf(XlCard)),"Assertion Failed, card shouldn't be present for invalid orderId");
        } finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class)
    public void launchCardWithCorporateCafeAvailableFalseValidationWhenAvailableIsFalse(UserOrderSegment uso, UserSegment us, Payload pl)throws IOException
    {
        String uri ="feature/is-cafe-present.*", responseFile="CorporateCafeAvailableMockResponse";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        data.addProperty("available",false);
        json.add("data",data);
        setMockGetUrlPatternRequest(uri,json);

        launchCard1 = helper.createCard(PitaraHelper.CardType.launchcard);
        helper.createCardValidationMap(launchCard1,launchCorporateCafeValidation_false);
        helper.createCardContextMap(contextId, launchCard1);
        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("XL_CARD", "LAUNCH_CARD");
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List cards = JsonPath.read(response, "$.data.cards..id");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(al.get(0).toString(), "LAUNCH_CARD");
                Assert.assertTrue(cards.contains(Integer.valueOf(launchCard1)), "Assertion failed, didn't received same launch card");
            }
        } finally {
            helper.deleteCards(getSuperCard);
            helper.cacheEvict();
        }

    }

    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class)
    public void launchCardWithCorporateCafeAvailableFalseValidationWhenCorpCafeIsNull(UserOrderSegment uso, UserSegment us, Payload pl)throws IOException
    {
        String uri ="feature/is-cafe-present.*", responseFile="CorporateCafeAvailableMockResponse";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        data.add("corporate", null);
        json.add("data",data);
        setMockGetUrlPatternRequest(uri,json);

        launchCard1 = helper.createCard(PitaraHelper.CardType.launchcard);
        helper.createCardValidationMap(launchCard1,launchCorporateCafeValidation_false);
        helper.createCardContextMap(contextId, launchCard1);
        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("XL_CARD", "LAUNCH_CARD");
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List cards = JsonPath.read(response, "$.data.cards..id");
        try {
                Assert.assertFalse(cards.contains(Integer.valueOf(launchCard1)), "Assertion failed, card should't be present when corporate data is null");

        } finally {
            helper.deleteCards(getSuperCard);
            helper.cacheEvict();
        }

    }

    @Test(dataProvider = "launchCard", dataProviderClass = PitaraDP.class)
    public void launchCardWithCafeAvailableFalseValidationWhenAvailableIsFalse(UserOrderSegment uso, UserSegment us, Payload pl)throws IOException
    {
        String uri ="feature/is-cafe-present.*", responseFile="CafeAvailableMockResponse";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        data.addProperty("available",false);
        json.add("data",data);
        setMockGetUrlPatternRequest(uri,json);

        launchCard1 = helper.createCard(PitaraHelper.CardType.launchcard);
        helper.createCardValidationMap(launchCard1,launchCardValiadation_false);
        helper.createCardContextMap(contextId, launchCard1);
        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("XL_CARD", "LAUNCH_CARD");
        Processor processor = pitaraHelper.getNuxCollectionCard(body, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List cards = JsonPath.read(response, "$.data..id");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(al.get(0).toString(), "LAUNCH_CARD");
                Assert.assertTrue(cards.contains(Integer.valueOf(launchCard1)), "Assertion failed, didn't received same launch card");
            }
        } finally {
            helper.deleteCards(getSuperCard);
            helper.cacheEvict();
        }

    }


// already known issue
    @Test(dataProvider = "superCard", dataProviderClass = PitaraDP.class, priority = 0, description = "already a known issue, for super false validation")
    public void sCardV2WithIsSuperOrderFalseValidation(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get_order_details", responseFile="IsSuperFalseMockResponse";
        setMockGetRequest(uri,responseFile);
        userId = "6114643";
        String getSuperCard = helper.createCard(PitaraHelper.CardType.scardv2);
        helper.setCardMetaDataSCardV2(getSuperCard);
        helper.createCardValidationMap(getSuperCard, isSuperOrderCardValidation_false);
        helper.createCardContextMap(contextId, getSuperCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(getSuperCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("NUX_LISTING", "S_CARD_V2");
        Processor processor = pitaraHelper.getSuperCard(body,userId);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List cards = JsonPath.read(response, "$.data..id");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(al.get(0).toString(), "S_CARD_V2");
                Assert.assertTrue(cards.contains(Integer.valueOf(getSuperCard)), "Assertion failed, didn't received same launch card");
            }
        } finally {
            helper.deleteCardMetaData(getSuperCard);
            helper.deleteCards(getSuperCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lcardWithHasPreOrderEnabledFalseValidationWhenPreOrderDataIsNull(UserOrderSegment uso, UserSegment us, Payload pl,String isPreOrder) throws IOException {
        String uri ="api/v1/order/get", responseFile="HasPreorderMockResponse";
//        please use these orderid and userid as same are configured in mock server
        String orderId = "724040866";
        userId = "6114643";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.add("preorder_data",null);
        orders = new JsonArray();
        orders.add(order);
        data.add("orders",orders);
        json.add("data",data);
        setMockGetRequest(uri,json);

        lCard = helper.createCard(PitaraHelper.CardType.lcard);
        helper.createCardValidationMap(lCard, hasPreOrderValidationId_false);
        helper.createCardContextMap(contextId2,lCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body,userId,orderId,tid,token,isPreOrder);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        try
        {
            if(al.size()==0)
            {
                Assert.assertTrue(false,"NoCardFound");
            }
            else {
                Assert.assertEquals(JsonPath.read(response, "$.data.cards..id").toString(), "[" + lCard + "]");
            }
        }
        finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

//    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lcardWithIsPreOrderEnabledFalseValidationWhenOrderTypeIsInvalid(UserOrderSegment uso, UserSegment us, Payload pl,String isPreOrder) throws IOException {
//        please use these orderid and userid as same are configured in mock server
        String uri ="api/v1/order/get", responseFile="IsPreorderMockResponse";
        String orderId = "724040866";
        userId = "6114643";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.add("preorder_data",null);
        orders = new JsonArray();
        orders.add(order);
        data.add("orders",orders);
        json.add("data",data);
        setMockGetRequest(uri,json);

        lCard = helper.createCard(PitaraHelper.CardType.lcard);
        helper.createCardValidationMap(lCard, isPreOrderValidationId_false);
        helper.createCardContextMap(contextId2,lCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body,userId,orderId,tid,token,isPreOrder);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List al = JsonPath.read(response, "$.data.cards..id");
            Assert.assertTrue(al.contains(Integer.valueOf(lCard)),"Assertion Failed, card should be present when validation is false");
        }
        finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lcardWithIsPreOrderEnabledFalseValidationWhenOrderStatusIsInvalid(UserOrderSegment uso, UserSegment us, Payload pl,String isPreOrder) throws IOException {
//        please use these orderid and userid as same are configured in mock server
        String uri ="api/v1/order/get", responseFile="IsPreorderMockResponse";
        String orderId = "724040866";
        userId = "6114643";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.addProperty("order_status","completed");
        orders = new JsonArray();
        orders.add(order);
        data.add("orders",orders);
        json.add("data",data);
        setMockGetRequest(uri,json);

        lCard = helper.createCard(PitaraHelper.CardType.lcard);
        helper.createCardValidationMap(lCard, isPreOrderValidationId_false);
        helper.createCardContextMap(contextId2,lCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body,userId,orderId,tid,token,isPreOrder);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        try {
            List al = JsonPath.read(response, "$.data.cards..id");
            Assert.assertFalse(al.contains(Integer.valueOf(lCard)),"Assertion Failed, card shouldn't be present when validation is false");
        }
        finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "lCard", dataProviderClass = PitaraDP.class)
    public void lcardWithIsPreOrderEnabledFalseValidationWhenPreorderDataIsNull(UserOrderSegment uso, UserSegment us, Payload pl,String isPreOrder) throws IOException {
//        please use these orderid and userid as same are configured in mock server
        String uri ="api/v1/order/get", responseFile="IsPreorderMockResponse";
        String orderId = "724040866";
        userId = "6114643";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray orders = data.get("orders").getAsJsonArray();
        JsonObject order = orders.get(0).getAsJsonObject();
        order.add("preorder_data",null);
        orders = new JsonArray();
        orders.add(order);
        data.add("orders",orders);
        json.add("data",data);
        setMockGetRequest(uri,json);

        lCard = helper.createCard(PitaraHelper.CardType.lcard);
        helper.createCardValidationMap(lCard, isPreOrderValidationId_false);
        helper.createCardContextMap(contextId2,lCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body,userId,orderId,tid,token,isPreOrder);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        try {
            List al = JsonPath.read(response, "$.data.cards..id");
            Assert.assertTrue(al.contains(Integer.valueOf(lCard)),"Assertion Failed, card should be present when validation is false");
        }
        finally {
            helper.deleteCards(lCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "superCard", dataProviderClass = PitaraDP.class)
    public void sCardV2WithSuperFalseValidationWithCountLTMin(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/plan/user/0", responseFile="SuperMockResponse";
        userId = "6114643";

        JsonObject json = getMockBody(responseFile);
        JsonObject data = json.get("data").getAsJsonObject();
        JsonArray plans = data.get("plan_list").getAsJsonArray();
        JsonObject plan = plans.get(0).getAsJsonObject();
        plan.addProperty("available_count",5);
        data.add("orders",plans);
        json.add("data",data);
        setMockRequest(uri,json);


        String getSuperUserCard = helper.createCard(PitaraHelper.CardType.scardv2);
        helper.setCardMetaDataSCardV2(getSuperUserCard);
        helper.createCardValidationMap(getSuperUserCard, superUserValidationWithMinAsTen);
        helper.createCardContextMap(contextId, getSuperUserCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(getSuperUserCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("NUX_LISTING", "S_CARD_V2");
        Processor processor = pitaraHelper.getSuperCard(body,userId);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List cards = JsonPath.read(response, "$.data.cards");
        try {
                Assert.assertTrue(cards.isEmpty(), "Assertion failed, shouldn't get cards when one of the plan avail count is less than min");

        } finally {
            helper.deleteCardMetaData(getSuperUserCard);
            helper.deleteCards(getSuperUserCard);
            helper.cacheEvict();
        }


    }

//    @Test(dataProvider = "superCard", dataProviderClass = PitaraDP.class)
    public void sCardV2WithSuperFalseValidationWithCountGTMax(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/plan/user/0", responseFile="SuperMockResponse";
        userId = "6114643";

        setMockRequest(uri,responseFile);


        String getSuperUserCard = helper.createCard(PitaraHelper.CardType.scardv2);
        helper.setCardMetaDataSCardV2(getSuperUserCard);
        helper.createCardValidationMap(getSuperUserCard, superUserValidationWithMaxAsTen);
        helper.createCardContextMap(contextId, getSuperUserCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(getSuperUserCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("NUX_LISTING", "S_CARD_V2");
        Processor processor = pitaraHelper.getSuperCard(body,userId);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List cards = JsonPath.read(response, "$.data.cards..id");
        try {
            Assert.assertFalse(cards.contains(getSuperUserCard), "Assertion failed, shouldn't get cards when one of the plan avail count is less than min");

        } finally {
            helper.deleteCardMetaData(getSuperUserCard);
            helper.deleteCards(getSuperUserCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithOrderCountFalseValidationWithMinCount(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/order/get", responseFile="OrderCountMockResponse";

        setMockGetRequest(uri,responseFile);

        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, nuxCard);
        helper.createCardValidationMap(nuxCard, orderCountValidationWithMin);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(nuxCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertFalse(cardsID.contains(nuxCard),"Assertion failed, card shouldn't be present in the list when order count< min.");
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithSeenCountFalseValidationWithMinCount(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String nuxCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId2, nuxCard);
        helper.createCardValidationMap(nuxCard, seenCountValidationMinMax);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(nuxCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, "1");
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertFalse(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card shouldn't be present in the list when seen count< min(12).");

        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithSeenCountFalseValidationWithMaxCount(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String nuxCard = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId2, nuxCard);
        helper.createCardValidationMap(nuxCard, seenCountValidationMinMax);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(nuxCard,10,20);
        String body = j.getObjectToJSON(nuxObj);
        Processor processor = pitaraHelper.getTrackScreenCardProcessor(body, "1");
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Integer> cardsID =JsonPath.read(response, "$.data.cards");
            Assert.assertFalse(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card shouldn't be present in the list when order count>max(15).");

        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }
    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithSeenAndOrderCountValidationTrue(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException{

    String uri ="api/v1/order/get", responseFile="OrderCountMockResponse";

        setMockGetRequest(uri,responseFile);

        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, nuxCard);
        helper.createCardValidationMap(nuxCard, orderCountValidation);
        helper.createCardValidationMap(nuxCard,seenCountValidation);
        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(nuxCard,10,2);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
            List<Collection> cls = pl.getNUXLISTING().getCollections();
            for (Collection c : cls) {
                if (c.getId() != null) {
                    List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
                    Assert.assertTrue(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card is not present in the list.");
                } else {
                    Assert.assertFalse(processor.ResponseValidator.DoesNodeExists( "$.data.cards[0].cta.link"));
                }
            }
        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }

    }

    @Test(dataProvider = "fetchNuxCardValidation", dataProviderClass = PitaraDP.class)
    public void nuxCardWithSeenAndOrderCountValidationTrueWhenSeenCountGTMax(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException{

        String uri ="api/v1/order/get", responseFile="OrderCountMockResponse";

        setMockGetRequest(uri,responseFile);

        String nuxCard  = helper.createCard(PitaraHelper.CardType.nuxcard);
        helper.createCardContextMap(contextId, nuxCard);
        helper.createCardValidationMap(nuxCard, orderCountValidation);
        helper.createCardValidationMap(nuxCard,seenCountValidation);
        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(nuxCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        String str = body.replace("nuxlisting", "NUX_LISTING");
        Processor processor = pitaraHelper.getNuxCollectionCard(str, tid, token);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");

        try {
             List<Integer> cardsID =JsonPath.read(response, "$.data.cards..id");
             Assert.assertFalse(cardsID.contains(Integer.valueOf(nuxCard)),"Assertion failed, card shouldn't be present in the list.");

        } finally {
            helper.deleteCards(nuxCard);
            helper.cacheEvict();
        }

    }

    @Test(dataProvider = "superCard", dataProviderClass = PitaraDP.class)
    public void sCardV2WithSuperAndOrderCountValidationTrue(UserOrderSegment uso, UserSegment us, Payload pl) throws IOException {
        String uri ="api/v1/plan/user/0", responseFile="SuperMockResponse";
        userId = "6114643";
        setMockGetRequest(uri,responseFile);

        uri ="api/v1/order/get"; responseFile="OrderCountMockResponse";
        setMockGetRequest(uri,responseFile);


        String getSuperUserCard = helper.createCard(PitaraHelper.CardType.scardv2);
        helper.setCardMetaDataSCardV2(getSuperUserCard);
        helper.createCardValidationMap(getSuperUserCard, superUserValidation);
        helper.createCardValidationMap(getSuperUserCard, orderCountValidation);
        helper.createCardContextMap(contextId, getSuperUserCard);

        PitaraNuxPojo nuxObj = new PitaraNuxPojo(uso, us, pl).withCardsMeta(getSuperUserCard,10,10);
        String body = j.getObjectToJSON(nuxObj);
        body = body.replace("NUX_LISTING", "S_CARD_V2");
        Processor processor = pitaraHelper.getSuperCard(body,userId);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "The response code is not");
        List al = JsonPath.read(response, "$.data..cardType");
        List cards = JsonPath.read(response, "$.data..id");
        try {
            if (al.size() == 0) {
                Assert.assertTrue(false, "NoCardFound");
            } else {
                Assert.assertEquals(al.get(0).toString(), "S_CARD_V2");
                Assert.assertTrue(cards.contains(Integer.valueOf(getSuperUserCard)), "Assertion failed, didn't received same launch card");
            }
        } finally {
            helper.deleteCardMetaData(getSuperUserCard);
            helper.deleteCards(getSuperUserCard);
            helper.cacheEvict();
        }
    }

    public void setMockGetRequest(String uri, String fileName) {
        File file = new File("../Data/MockAPI/ResponseBody/rng/pitara/" + fileName);
        String body = null;
        try {
            body = FileUtils.readFileToString(file);
//			wireMockHelper.setupStub(uri, 200, "application/json", body, 0);
            Processor processor = rngHelper.addMapping("/" + uri, "GET", body);
            System.out.println("mocking uri ::" + uri + "\n with this response body :: " + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setMockGetUrlPatternRequest(String uri, String fileName) {
        File file = new File("../Data/MockAPI/ResponseBody/rng/pitara/" + fileName);
        String body = null;
        try {
            body = FileUtils.readFileToString(file);
//			wireMockHelper.setupStub(uri, 200, "application/json", body, 0);
            Processor processor = rngHelper.addPatternMapping("/" + uri, "GET", body);
            System.out.println("mocking uri ::" + uri + "\n with this response body :: " + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JsonObject getMockBody(String fileName) throws IOException{
        File file = new File("../Data/MockAPI/ResponseBody/rng/pitara/"+fileName);
        String body = FileUtils.readFileToString(file);
        JsonObject jsonObject = new JsonParser().parse(body).getAsJsonObject();
        return jsonObject;
    }

    public void setMockRequest(String uri, String fileName) {
        File file = new File("../Data/MockAPI/ResponseBody/rng/pitara/" + fileName);
        String body = null;
        try {
            body = FileUtils.readFileToString(file);
//			wireMockHelper.setupStubPost(uri,200,"application/json",body,0);
            Processor processor = rngHelper.addMapping("/" + uri, "POST", body);
            System.out.println("mocking uri ::" + uri + "\n with this response body :: " + fileName);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setMockRequest(String uri, JsonObject payload) throws IOException {
        String body = payload.toString();
        Processor processor = rngHelper.addMapping("/" + uri, "POST", body);
        System.out.println("mocking uri ::"+uri+"\n with this response body :: "+body);
    }

    public void setMockGetUrlPatternRequest(String uri, JsonObject payload) {
        String body = payload.toString();
        Processor processor = rngHelper.addPatternMapping("/" + uri, "GET", body);
        System.out.println("mocking uri ::" + uri + "\n with this response body :: " + body);
    }

    public void setMockGetRequest(String uri, JsonObject payload) throws IOException {
        String body = payload.toString();
        Processor processor = rngHelper.addMapping("/" + uri, "GET", body);
//        try {
        System.out.println("mocking uri ::"+uri+"\n with this response body :: "+payload);
//            Thread.sleep(150000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

//    @AfterClass
//    public void stopMockServer(){
//        wireMockHelper.stopMockServer();
//
//    }
}


