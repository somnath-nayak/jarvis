package com.swiggy.api.sf.rng.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class FlatRuleDiscount {


    private String type;
    private String discountLevel;
    private String flatDiscountAmount;
    private String minCartAmount;

    /**
     * No args constructor for use in serialization
     *
     */
    public FlatRuleDiscount() {
    }

    /**
     *
     * @param minCartAmount
     * @param discountLevel
     * @param type
     * @param flatDiscountAmount
     */
    public FlatRuleDiscount(String type, String discountLevel, String flatDiscountAmount, String minCartAmount) {
        super();
        this.type = type;
        this.discountLevel = discountLevel;
        this.flatDiscountAmount = flatDiscountAmount;
        this.minCartAmount = minCartAmount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDiscountLevel() {
        return discountLevel;
    }

    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    public String getFlatDiscountAmount() {
        return flatDiscountAmount;
    }

    public void setFlatDiscountAmount(String flatDiscountAmount) {
        this.flatDiscountAmount = flatDiscountAmount;
    }

    public String getMinCartAmount() {
        return minCartAmount;
    }

    public void setMinCartAmount(String minCartAmount) {
        this.minCartAmount = minCartAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("discountLevel", discountLevel).append("flatDiscountAmount", flatDiscountAmount).append("minCartAmount", minCartAmount).toString();
    }

    }

