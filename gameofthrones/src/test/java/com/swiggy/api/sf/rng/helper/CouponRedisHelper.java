package com.swiggy.api.sf.rng.helper;

import framework.gameofthrones.Tyrion.RedisHelper;


public class CouponRedisHelper extends RedisHelper{
    RedisHelper redisHelper = new RedisHelper();

    public String getCouponUsageCountRedis(String couponCode) {
        System.out.println("Code:" + couponCode);
        System.out.println("---->"+(String)redisHelper.getValue("couponredis", 5, "COUPONS_USAGE_COUNT_"+couponCode));
       return (String)redisHelper.getValue("couponredis", 5, "COUPONS_USAGE_COUNT_"+couponCode);
    }

    public void flushPreviousDateFromRedis(String couponCode) {
        System.out.println("Code:" + couponCode);
       redisHelper.deleteKey("couponredis", 6, "COUPON_META_"+couponCode);
       redisHelper.deleteKey("couponredis",6,"COUPON_"+couponCode);
    }

    public void flushCouponUsedCacheFromRedis(String userID) {
        System.out.println("UserID:" + userID);
        redisHelper.deleteKey("couponredis", 8, "COUPON_USED_"+userID);
        redisHelper.deleteKey("couponredis", 8, "COUPON_USER_"+userID);
    }
    public void flushOrderRelatedDataFromRedis(String name) {
        System.out.println("name:" + name);
        redisHelper.flushAll(name);

    }

/*===========================Deleting user ID for particular restaurants visit===================================*/
    public void deleteDormantUserRestMapping(String userId) {
        String key = "user_restaurant_last_order:" + userId;
        redisHelper.deleteKey("rngredis", 5, key);
        System.out.println("Dormant key deleted");
    }

}
