package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.EndToEndHelp;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import io.advantageous.boon.core.Sys;
import io.gatling.recorder.har.Json;

public class ReferralCoupons extends DP {
	HashMap<String, String> requestHeaders = new HashMap<String, String>();
	GameOfThronesService service;
	Processor processor;
	RngHelper rngHelper = new RngHelper();
	DBHelper dbHelper = new DBHelper();
	RedisHelper redisHelper = new RedisHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();

	@Test(dataProvider = "referralFlowData", dataProviderClass = DP.class)
	public void referralFlow(String name, String mobile, String email, String password)
			throws Exception {

		String mboli_num = RngHelper.checkUserByMobile(mobile);
		if (mobile.equalsIgnoreCase(mboli_num)) {
			RngHelper.deleteUserByMobile(mobile);
		}
		String referralCode = RngHelper.getReferralCodeFromWpUsers();
		Processor tidProcessor = RngHelper.signUpWithReferral(name, mobile, email, password, referralCode);
		String tid = String.valueOf(tidProcessor.ResponseValidator.GetNodeValue("$.tid"));
		System.out.println("tid for signup is   " + tid);
		String otp = SnDHelper.getOTP(tid);
		SnDHelper.verifyOTP(otp, tid);
		// Sign in 1st time
		SnDHelper.consumerLogin(mobile, password);
		Processor P = SnDHelper.consumerLoginCheck(mobile);
		String login_verify = String.valueOf(P.ResponseValidator.GetNodeValue("$.statusMessage"));
		System.out.println("Verify login    " + login_verify);
		String refer_code = RngHelper.getReferralCode(mobile);
		System.out.println("1st time login user referral code is  " + refer_code);
		String RC1 = RngHelper.checkRC1Coupon("'%RC1-" + refer_code + "%'");
		System.out.println("RC1 for this user is     " + RC1);
		Assert.assertEquals(RC1, "RC1-" + refer_code);
		// get order placed and delivered
		System.out.println("referralCode used to signUp is  " +referralCode);
		rngHelper.delivered();
		System.out.println("RC1 for this user is     " + RC1);
		System.out.println("1st time login user referral code is  " + refer_code);
		String RC2 = RngHelper.checkRC2Coupon("'%RC2-" + refer_code + "%'");
		System.out.println("RC2 for this user is   " + RC2);
		Assert.assertEquals(RC2, "RC2-" + refer_code);

      System.out.println("referralCode used to signUp is  " +referralCode);
	}

	@Test(dataProvider = "referralUserIdGetNPostData", dataProviderClass = DP.class)
	public void referralUserIdGetNPost(String count, String updatedAt, String name, String mobile, String email,
			String password) throws Exception {

		String referralCode = RngHelper.getReferralCodeFromWpUsers();

		String mboli_num = RngHelper.checkUserByMobile(mobile);
		if (mobile.equalsIgnoreCase(mboli_num)) {
			RngHelper.deleteUserByMobile(mobile);
		}
		Processor tidProcessor = RngHelper.signUpWithReferral(name, mobile, email, password, referralCode);
		String tid = String.valueOf(tidProcessor.ResponseValidator.GetNodeValue("$.tid"));
		System.out.println("tid for signup is   " + tid);
		String otp = SnDHelper.getOTP(tid);
		SnDHelper.verifyOTP(otp, tid);
		// Sign in 1st time
		SnDHelper.consumerLogin(mobile, password);
		String userId = RngHelper.getUserByMobile(mobile);
		Processor P = SnDHelper.consumerLoginCheck(mobile);
		String login_verify = P.ResponseValidator.GetNodeValue("$.statusMessage");
		System.out.println("Verify login    " + login_verify);
		System.out.println("referralCode  used for signUp is    " + referralCode);
		Processor postResponse = RngHelper.postRefferalUserId(userId, count, updatedAt);
		Processor getResponse = RngHelper.getReferralUserId(userId);
		String check_count = String.valueOf(getResponse.ResponseValidator.GetNodeValueAsInt("$.data.count"));
		Assert.assertEquals(check_count, count);

	}

	}