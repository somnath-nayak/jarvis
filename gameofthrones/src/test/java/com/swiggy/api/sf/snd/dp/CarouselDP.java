package com.swiggy.api.sf.snd.dp;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.rng.pojo.MenuMerch.*;
import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import com.swiggy.api.sf.snd.constants.MenuMerchandisingConstants;
import com.swiggy.api.sf.snd.helper.EDVOHelper;
import com.swiggy.api.sf.snd.helper.MenuMerchandisingHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.PropertiesHandler;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CarouselDP {

    EDVOHelper edvoHelper = new EDVOHelper();
//    com.swiggy.api.sf.rng.helper.EDVOHelper edvoHelperRng = new com.swiggy.api.sf.rng.helper.EDVOHelper();

    MenuMerchandisingHelper menuMerchandisingHelper = new MenuMerchandisingHelper();
    RuleDiscount ruleDiscount = new RuleDiscount();
    CMSHelper cmsHelper = new CMSHelper();

    String Env;
    static String restId="227";
    static String lat;
    static String lng;
    static String itemId = "2555073";
    String tdId1 = "6198";
    String tdId2 = "6199";
    ArrayList<String> tdlist = new ArrayList();

    SnDHelper sndHelper = new SnDHelper();

    public static void setRestId(String restId) {
        MenuMerchandisingDP.restId = restId;
    }

    public static String getLat() {
        return lat;
    }

    public static void setLat() {
        MenuMerchandisingDP.lat = MenuMerchandisingConstants.lat;
    }

    public static String getLng() {
        return lng;
    }

    public static void setLng() {
        MenuMerchandisingDP.lng = MenuMerchandisingConstants.lng;
    }

    public void getRestId() {
        setLat();
        setLng();

        //pick the rest ID
        Processor processor = sndHelper.getAggregatorDetails(lat, lng);
        restId = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.sortedRestaurants[0]..restaurantId").toString().replace("[\"", "").replace("\"]", "");
        setRestId(restId);

        //delete existing campaigns
        com.swiggy.api.sf.rng.helper.EDVOHelper.deleteUserByMobile(restId);

    }

    public PropertiesHandler properties = new PropertiesHandler();

    public CarouselDP() {
        if (System.getenv("ENVIRONMENT") == null)
            Env = properties.propertiesMap.get("environment");
        else
            Env = System.getenv("ENVIRONMENT");
    }

    public List<RestaurantList> buildRestaurantList(String restId, String categoryId, String subCategoryId, String itemId) {

        List<RestaurantList> restaurantList = new ArrayList<>();
        List<Menu> itemList = new ArrayList<>();
        itemList.add(new Menu(itemId, "test Item"));
        List<SubCategories> subCategoriesList = new ArrayList<>();
        subCategoriesList.add(new SubCategories(subCategoryId, "Test subcategory", itemList));
        List<Categories> categories = new ArrayList<>();
        categories.add(new Categories(categoryId, "Test Category", subCategoriesList));
        restaurantList.add(new RestaurantList(restId, "Test Rest", categories));

        return restaurantList;
    }

    public List<Slots> buildSlotList() {
        CreateFlatTDItemLevelPOJO createFlatTDItemLevelPOJO = new CreateFlatTDItemLevelPOJO();
        List<Slots> allDaysTimeSlot = createFlatTDItemLevelPOJO.getAllDaysTimeSlot();
        return allDaysTimeSlot;
    }

    @DataProvider(name = "createCarouselValid")
    public Object[][] createCarouselValidRestItemMapping() {
        // getRestId();
        tdlist.clear();
        tdlist.add(tdId1);
        return new Object[][]{
                {new CarouselPOJO().setDefaultCarouselData()
                        .withRedirectLink("2555073")
                        .withType("item")
                        .withRestaurantId(227)
                        .withDescription("test-desc")
                        .withFontColor("#ffffff")
                        .withCarouselTradeDiscountInfoSet("Campaign", tdlist)
                }
        };
    }

    @DataProvider(name = "createCarouselInvalidRestItemMapping")
    public Object[][] createCarouselInvalidRestItemMapping() {
        // getRestId();
        tdlist.clear();
        tdlist.add(tdId1);
        return new Object[][]{
                {new CarouselPOJO().setDefaultCarouselData()
                        .withRedirectLink("23345")
                        .withType("item")
                        .withRestaurantId(227)
                        .withDescription("test-desc")
                        .withFontColor("#ffffff")
                        .withCarouselTradeDiscountInfoSet("Campaign", tdlist)
                }
        };
    }

    @DataProvider(name = "createCarouselInvalidTD")
    public Object[][] createCarouselInvalidTD() {
        // getRestId();
        tdlist.clear();
        tdlist.add("7878");
        return new Object[][]{
                {new CarouselPOJO().setDefaultCarouselData()
                        .withRedirectLink("2555073")
                        .withType("item")
                        .withRestaurantId(227)
                        .withDescription("test-desc")
                        .withFontColor("#ffffff")
                        .withCarouselTradeDiscountInfoSet("Campaign", tdlist)
                }
        };
    }
    @DataProvider(name = "createCarouselWithFlatAndFreeDelTD")
    public Object[][] createCarouselWithFlatAndFreeDelTD() {
        // getRestId();
        tdlist.clear();
        tdlist.add(tdId1);
        tdlist.add(tdId2);
        return new Object[][]{
                {new CarouselPOJO().setDefaultCarouselData()
                        .withRedirectLink("2555073")
                        .withType("item")
                        .withRestaurantId(227)
                        .withDescription("test-desc")
                        .withFontColor("#ffffff")
                        .withCarouselTradeDiscountInfoSet("Campaign", tdlist)
                }
        };
    }

    @DataProvider(name = "createCarouselWithBulkPayload")
    public Object[][] createCarouselWithBulkPayload() {
        ArrayList<String> tdList2 = new ArrayList<>();
        // getRestId();
        tdlist.clear();
        tdlist.add(tdId1);
        tdList2.add(tdId2);

        ArrayList<CarouselPOJO> carouselArray = new ArrayList<>();
        CarouselPOJO carouselPOJO1 = new CarouselPOJO();
        CarouselPOJO carouselPOJO2 = new CarouselPOJO();
        carouselArray.add(carouselPOJO1.setDefaultCarouselData()
                .withRedirectLink("2555073")
                .withType("item")
                .withRestaurantId(227)
                .withDescription("test-desc")
                .withFontColor("#ffffff")
                .withCarouselTradeDiscountInfoSet("Campaign", tdlist));
        carouselArray.add(carouselPOJO2.setDefaultCarouselData()
                .withRedirectLink("2555073")
                .withType("item")
                .withRestaurantId(227)
                .withDescription("test-desc")
                .withFontColor("#ffffff")
                .withCarouselTradeDiscountInfoSet("Campaign", tdList2));

        return new Object[][]{
                {carouselArray}
        };

    }

    @DataProvider(name = "getCarouselValidId")
    public Object[][] getCarouselValidId() {

        return new Object[][]{
                {"72859"}
        };
    }

    @DataProvider(name = "getCarouselInvalidId")
    public Object[][] getCarouselInvalidId() {

        return new Object[][]{
                {"728589898"}
        };
    }

    @DataProvider(name = "getCarouselAll")
    public Object[][] getCarouselAll() {

        return new Object[][]{
                {"static"},
                {"restaurant"},
                {"item"}
        };
    }

    @DataProvider(name = "refreshRest")
    public Object[][] refreshRest() {

        return new Object[][]{
                {"232"}
        };
    }

    @DataProvider(name = "getCarouselByPage")
    public Object[][] getCarouselByPage(){
        return new Object[][]{
                {"2","2","item"},
                {"0","5","static"},
                {"1","4","restaurant"}
        };
    }

    @DataProvider(name = "getCarouselByPageInvalidPageSize")
    public Object[][] getCarouselByPageInvalidPageSize(){
        return new Object[][]{
                {"2","2.5","item"},
                {"0","5","static"},
                {"1","4","restaurant"}
        };
    }

    @DataProvider(name = "createCarouselWithBulkCSVInvalid")
    public Object[][] createCarouselWithBulkCSVInvalid(){
        return new Object[][]{
                {System.getProperty("user.dir") + "/../Data/Payloads/FORMDATA/CarouselBukCSVTestData/carousel_tayyabsInvalid"}
        };
    }

    @DataProvider(name = "createCarouselWithBulkCSVValid")
    public Object[][] createCarouselWithBulkCSVValid() throws IOException, ProcessingException {
        return new Object[][]{
                {System.getProperty("user.dir") + "/../Data/Payloads/FORMDATA/CarouselBukCSVTestData/carousel_tayyabsValid"}
        };
    }

    @DataProvider(name = "createCarouselWithBulkCSVEmpty")
    public Object[][] createCarouselWithBulkCSVEmpty() throws IOException, ProcessingException {
        return new Object[][]{
                {System.getProperty("user.dir") + "/../Data/Payloads/FORMDATA/CarouselBukCSVTestData/carousel_tayyabsEmpty"}
        };
    }

    @DataProvider(name = "createCarouselWithBulkCSVInvalidFormatOfData")
    public Object[][] createCarouselWithBulkCSVInvalidFormatOfData() throws IOException, ProcessingException {
        return new Object[][]{
                {System.getProperty("user.dir") + "/../Data/Payloads/FORMDATA/CarouselBukCSVTestData/carousel_tayyabsInvalidFormatOfData"}
        };
    }


    @DataProvider(name = "createCarouselWithBulkCSVInvalidFileFormat")
    public Object[][] createCarouselWithBulkCSVInvalidFileFormat() throws IOException, ProcessingException {
        return new Object[][]{
                {System.getProperty("user.dir") + "/../Data/Payloads/FORMDATA/CarouselBukCSVTestData/carousel_tayyabsInvalidFileFormat"}
        };
    }

    @DataProvider(name = "createCarouselWithBulkCSVInvalidTDMapped")
    public Object[][] createCarouselWithBulkCSVInvalidTDMapped() throws IOException, ProcessingException {
        return new Object[][]{
                {System.getProperty("user.dir") + "/../Data/Payloads/FORMDATA/CarouselBukCSVTestData/carousel_tayyabsInvalidTDMapped"}
        };
    }

    @DataProvider(name = "createCarouselWithBulkCSVInvalidItemRestMapping")
    public Object[][] createCarouselWithBulkCSVInvalidItemRestMapping() throws IOException, ProcessingException {
        return new Object[][]{
                {System.getProperty("user.dir") + "/../Data/Payloads/FORMDATA/CarouselBukCSVTestData/carousel_tayyabsInvalidItemRestMapping"}
        };
    }

    @DataProvider(name = "deleteCarouselBulkValid")
    public Object[][] deleteCarouselBulkValid() throws IOException, ProcessingException {
        ArrayList<Integer> bannerIds = new ArrayList<Integer>();
        bannerIds.add(22);
        bannerIds.add(37);
        return new Object[][]{
                {bannerIds}
        };
    }
    @DataProvider(name = "deleteCarouselBulkInvalid")
    public Object[][] deleteCarouselBulkInvalid() throws IOException, ProcessingException {
        ArrayList<Integer> bannerIds = new ArrayList<Integer>();
        bannerIds.add(22989898);
        return new Object[][]{
                {bannerIds}
        };
    }
}