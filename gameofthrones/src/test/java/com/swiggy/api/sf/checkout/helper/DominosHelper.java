package com.swiggy.api.sf.checkout.helper;

import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class DominosHelper {

    Initialize gameofthrones = new Initialize();
    SnDHelper sndHelper = new SnDHelper();
    SANDHelper sandHelper = new SANDHelper();
    CheckoutHelper checkoutHelper= new CheckoutHelper();

    public Processor CreateCartAddon(String tid, String token, String cart, Integer rest)
    {
        HashMap<String, String> requestheaders = CheckoutHelper.requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2nitems", gameofthrones);
        String []payloadparams = {cart,String.valueOf(rest)};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }
}
