package com.swiggy.api.sf.rng.pojo.MenuMerch;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class Menu {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    public String getId ()
    {
        return id;
    }
    @JsonProperty("id")
    public void setId (String id)
    {
        this.id = id;
    }

    public Menu withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName ()
    {
        return name;
    }
    @JsonProperty("name")
    public void setName (String name)
    {
        this.name = name;
    }

    public Menu withName(String name) {
        this.name = name;
        return this;
    }

    public Menu(String itemId,String name){
        this.id=itemId;
        this.name=name;
    }
    public Menu setDefault(){
        return this.withId("12345")
                .withName("ItemId");
    }

    @Override
    public String toString()
    {
       return new ToStringBuilder(this).append("id", id).append("name", name).toString();
    }
}
