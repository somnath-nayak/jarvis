package com.swiggy.api.sf.rng.helper.TDHelper;

import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.pojo.TDTemplate.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.mortbay.util.ajax.JSON;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.*;

public class TDTemplateHelper {
    static Initialize gameofthrones = new Initialize();
    JsonHelper jsonHelper = new JsonHelper();
    static String tradeDiscountDB="trade_discount";

    /******CREATE TEMPLATE ID***********/

    //Create TEMPLATE  API call
    public Processor createTemplate(String campaignType, String validfrom, String validto, String minCartAmount,
                                    String firstOrder, String dormantUserType, String userRestriction) throws IOException {
        GameOfThronesService service = new GameOfThronesService("td", "createtemplate", gameofthrones);
        String[] payload = new String[]{jsonHelper.getObjectToJSON(getCreateTemplatePayload(campaignType, validfrom, validto, minCartAmount, firstOrder, dormantUserType, userRestriction))};
        Processor processor = new Processor(service, getHeadermap(), payload);
        return processor;
    }
    //Create TEMPLATE  API call - Dormant User as NULL
    public Processor createTemplateWithDormantUserNull(String campaignType, String validfrom, String validto, String minCartAmount,
                                    String firstOrder, String dormantUserType, String userRestriction) throws IOException {

        GameOfThronesService service = new GameOfThronesService("td", "createtemplate", gameofthrones);
        String[] payload = new String[]{jsonHelper.getObjectToJSON(getCreateTemplatePayloadWithDormantUserAsNull(campaignType, validfrom, validto, minCartAmount, firstOrder, dormantUserType, userRestriction))};
        Processor processor = new Processor(service, getHeadermap(), payload);
        return processor;
    }

    //Create TEMPLATE  API call with time span type
    public Processor createTemplate(String campaignType, String validfrom, String validto, String minCartAmount,
                                    String firstOrder, String dormantUserType, String userRestriction,String timespantype) throws IOException {
        GameOfThronesService service = new GameOfThronesService("td", "createtemplate", gameofthrones);
        String[] payload = new String[]{jsonHelper.getObjectToJSON(getCreateTemplatePayload(campaignType, validfrom, validto, minCartAmount, firstOrder, dormantUserType, userRestriction,timespantype))};
        Processor processor = new Processor(service, getHeadermap(), payload);
        return processor;
    }

    //Create TEMPLATE  API call with time span type
    public Processor createTemplate(String campaignType, String validfrom, String validto, String minCartAmount,
                                    String firstOrder, String dormantUserType, String userRestriction,String timespantype,String rfo) throws IOException {
        GameOfThronesService service = new GameOfThronesService("td", "createtemplate", gameofthrones);
        String[] payload = new String[]{jsonHelper.getObjectToJSON(getCreateTemplatePayload(campaignType, validfrom, validto, minCartAmount, firstOrder, dormantUserType, userRestriction, timespantype, rfo))};
        Processor processor = new Processor(service, getHeadermap(), payload);
        return processor;
    }

    /******CREATE TEMPLATE BUILDER*****/

    //Create Template API request builder
    private TemplateCreation getCreateTemplatePayload(String campaignType, String validfrom, String validto,
                                                      String minCartAmount, String firstOrder, String dormantUserType, String userRestriction) {

        TemplateCreation templateCreationBuilder = new TemplateCreationBuilder().withCampaignType(campaignType).withValidFrom(Long.parseLong(validfrom))
                .withValidTill(Long.parseLong(validto)).withMinCartAmount(Integer.valueOf(minCartAmount)).
                        withIsFirstOrder(Boolean.valueOf(firstOrder)).withDormantUserType(dormantUserType).withUserRestriction(Boolean.valueOf(userRestriction)).build();
        return templateCreationBuilder;
    }
    //Create Template API request builder
    private TemplateCreation getCreateTemplatePayload(String campaignType, String validfrom, String validto,
                                                      String minCartAmount, String firstOrder, String dormantUserType, String userRestriction,String timespantype) {

        TemplateCreation templateCreationBuilder = new TemplateCreationBuilder().withCampaignType(campaignType).withValidFrom(Long.parseLong(validfrom))
                .withValidTill(Long.parseLong(validto)).withMinCartAmount(Integer.valueOf(minCartAmount)).
                        withIsFirstOrder(Boolean.valueOf(firstOrder)).withDormantUserType(dormantUserType).withUserRestriction(Boolean.valueOf(userRestriction)).withTimeSpanType(timespantype).build();
        return templateCreationBuilder;
    }

    private TemplateCreation getCreateTemplatePayload(String campaignType, String validfrom, String validto,
                                                      String minCartAmount, String firstOrder, String dormantUserType, String userRestriction,String timespantype,String rfo) {

        TemplateCreation templateCreationBuilder = new TemplateCreationBuilder().withCampaignType(campaignType).withValidFrom(Long.parseLong(validfrom))
                .withValidTill(Long.parseLong(validto)).withMinCartAmount(Integer.valueOf(minCartAmount)).
                        withIsFirstOrder(Boolean.valueOf(firstOrder)).withDormantUserType(dormantUserType).withUserRestriction(Boolean.valueOf(userRestriction)).withTimeSpanType(timespantype).withRestaurantFirstOrder(Boolean.valueOf(rfo)).build();
        return templateCreationBuilder;
    }



    //Create Template API request builder ->  DormantUser as NULL
    private TemplateCreation getCreateTemplatePayloadWithDormantUserAsNull(String campaignType, String validfrom, String validto,
                                                      String minCartAmount, String firstOrder, String dormantUserType, String userRestriction) {

        TemplateCreation templateCreationBuilder =new TemplateCreationBuilder().withCampaignType(campaignType).withValidFrom(Long.parseLong(validfrom))
                .withValidTill(Long.parseLong(validto)).withMinCartAmount(Integer.valueOf(minCartAmount)).
                        withIsFirstOrder(Boolean.valueOf(firstOrder)).withDormantUserType(dormantUserType).withUserRestriction(Boolean.valueOf(userRestriction)).build();
        templateCreationBuilder.setDormantUserType(null);
        return templateCreationBuilder;
    }




    /******DISABLE TEMPLATE**********/

    //Disable TEMPLATE  API call
    public Processor disableTemplate( List<Integer> templateIds) throws IOException {
        GameOfThronesService service = new GameOfThronesService("td", "disabletemplate", gameofthrones);
        String[] payload= new String[]{jsonHelper.getObjectToJSON(getDisableTemplatePayload(templateIds))};
        Processor processor = new Processor(service, getHeadermap(), payload);
        return processor;
    }

    //Create Template API request builder
    private DisableTemplate getDisableTemplatePayload(List<Integer>  templateIds) {

        DisableTemplate disableTemplate = new DisableTemplateBuilder().withIds(templateIds).build();
        return disableTemplate;
    }



    /******GET TEMPLATE********/

    //GET TEMPLATE  API call
    public Processor getTemplateById( String templateId) throws IOException {
        String[] urlparams = new String[] { templateId };
        GameOfThronesService service = new GameOfThronesService("td", "gettemplatebyid", gameofthrones);

        Processor processor = new Processor(service, null, null,urlparams);
        return processor;
    }



    /******GET TEMPLATE LIST********/

    //GET TEMPLATE  API call
    public Processor getTemplateList( String enabled,String templateName) throws IOException {
        String[] urlparams = new String[] { enabled , templateName};
        GameOfThronesService service = new GameOfThronesService("td", "gettemplatelistbyname", gameofthrones);

        Processor processor = new Processor(service, null, null,urlparams);
        return processor;
    }

    //GET TEMPLATE  API call
    public Processor getTemplateList( String templateName) throws IOException {
        String[] urlparams = new String[] { templateName};
        GameOfThronesService service = new GameOfThronesService("td", "gettemplatelistbynameonlytname", gameofthrones);

        Processor processor = new Processor(service, null, null,urlparams);
        return processor;
    }

    public Processor getTemplateListOnlyEnabled( String enabled) throws IOException {
        String[] urlparams = new String[] { enabled };
        GameOfThronesService service = new GameOfThronesService("td", "gettemplatelistbynameonlyenabled", gameofthrones);

        Processor processor = new Processor(service, null, null,urlparams);
        return processor;
    }

    public Processor getTemplateListWithoutAnyParam( ) throws IOException {

        GameOfThronesService service = new GameOfThronesService("td", "gettemplatelistbynamewithoutparam", gameofthrones);

        Processor processor = new Processor(service, null, null,null);
        return processor;
    }


    /*******ONBOARDING TEMPLATE*********/

    //ONBOARDING TEMPLATE  API call
    public Processor onboaringTemplate( List<Integer> restIds, int templateId) throws IOException {
        GameOfThronesService service = new GameOfThronesService("td", "onboardtemplate", gameofthrones);
        String[] payload= new String[]{jsonHelper.getObjectToJSON(getOnboardTemplatePayload(restIds,templateId))};
        Processor processor = new Processor(service, getHeadermap(), payload);
        return processor;
    }
    //with optional params
    public Processor onboaringTemplate( List<Integer> restIds, int templateId,String duration, String startdate,String ingestionSource) throws IOException {
        GameOfThronesService service = new GameOfThronesService("td", "onboardtemplate", gameofthrones);
        String[] payload= new String[]{jsonHelper.getObjectToJSON(getOnboardTemplatePayload(restIds,templateId,duration,startdate,ingestionSource))};
        Processor processor = new Processor(service, getHeadermap(), payload);
        return processor;
    }

    //Onboarding Template API request builder
    private OnboardTemplate getOnboardTemplatePayload(List<Integer> restaurantIds , int templateId) {

        OnboardTemplate onboardTemplate = new OnboardTemplateBuilder().withRestaurantIds(restaurantIds).withTemplateId(templateId).build();
        return onboardTemplate;
    }

    //Optional Params request Builder
    private OnboardTemplate getOnboardTemplatePayload(List<Integer> restaurantIds , int templateId,String duration, String startdate,String ingestionSource ) {

        OnboardTemplateBuilder builder = new OnboardTemplateBuilder();
        OnboardTemplate onboardTemplate = builder.withRestaurantIds(restaurantIds).withTemplateId(templateId).build();

        if (duration!= null){
          onboardTemplate = builder.withDuration(Integer.valueOf(duration)).build();
        }
        if (startdate!= null){
            onboardTemplate =builder.withStartDate(Long.valueOf(startdate)).build();
        }
        if (ingestionSource!= null){
            onboardTemplate = builder.withIngestionSource(ingestionSource).build();

        }

        return onboardTemplate;
    }



    /************VALIDATE TEMPLATE*******************/
    public Processor validateTemplate( List<Integer> templateIds) throws IOException {
        GameOfThronesService service = new GameOfThronesService("td", "validatetemplate", gameofthrones);
        String[] payload= new String[]{jsonHelper.getObjectToJSON(getValidateTemplatePayload(templateIds))};
        Processor processor = new Processor(service, getHeadermap(), payload);
        return processor;
    }

    //Validate Template API request builder
    private TemplateValidation getValidateTemplatePayload(List<Integer> templateIds ) {

        TemplateValidation validateTemplate = new TemplateValidationBuilder().withTemplateIds(templateIds).build();
        return validateTemplate;
    }





    /*******Internal Methods required as Util*************/


    public String getFutureTimeForMonthsInMilliSecondsFromCurrentDate(int numberofmonths) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, numberofmonths);
        Long time =cal.getTimeInMillis();
        return  String.valueOf(time);
    }

    public String getFutureTimeForMinutesInMilliSecondsFromCurrentDate(int numberOfMinutes) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, numberOfMinutes);
        Long time =cal.getTimeInMillis();
        return String.valueOf(time);
    }

    //Header Hashmap
    private HashMap<String, String> getHeadermap() {
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(RngConstants.CONTENT_TYPE, RngConstants.APPLICATION_JSON);
        return headerMap;
    }


    public static Map<String, Object> getTemplateFromDB(String id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
       return  sqlTemplate.queryForMap(RngConstants.SELECT_TEMPLATE_BY_ID +id +";");

    }

    public static Map<String, Object> getRandomTemplateIdFromDB() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        return  sqlTemplate.queryForMap(RngConstants.SELECT_RANDOM_TEMPLATE_ID);

    }
    public static Map<String, Object> getRandomEnabledTemplateIdFromDB() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        return  sqlTemplate.queryForMap(RngConstants.SELECT_RANDOM_ENABLED_TEMPLATE_ID);

    }

    public static Map<String, Object> getEnabledForGivenTemplateIdFromDB(String id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        return  sqlTemplate.queryForMap(RngConstants.SELECT_ENABLED_FROM_TEMPLATE_DB +id +";");
    }

    public static Map<String, Object> getEnabledTemplate(String enabled,String templateName) {
        String enabledflag;
        String tname;
        if (enabled=="true"){
            enabledflag="1";
        }
        else{enabledflag="0";}

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        return  sqlTemplate.queryForMap(RngConstants.SELECT_TEMPLATE_FOR_GIVEN_ENABLED +enabledflag +" and `template_name` like '%"+templateName+"%';");

    }
    public static Map<String, Object> getAllTemplate() {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        return  sqlTemplate.queryForMap(RngConstants.SELECT_COUNT_OF_ALL_TEMPLATE);

    }

    public void deleteAllTemplate() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        sqlTemplate.execute(RngConstants.DELETE_ALL_AUTOMATION_TEMPLATE);
    }

    public static Map<String, Object> getEnabledTemplate(String enabled) {
        String enabledflag = "0";
        String tname;
        if (enabled == "true") {
            enabledflag = "1";
        } else if (enabled == "false" || enabled == "") {
            enabledflag = "0";
        }

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        return  sqlTemplate.queryForMap(RngConstants.SELECT_TEMPLATE_FOR_GIVEN_ENABLED +enabledflag +";");

    }


    public int disableAllCampaigns() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        return sqlTemplate.update(RngConstants.UPDATE_ALL_CAMPAIGNS_TO_DISABLE);
    }

    public Map<String, Object> getCampaignById(String id){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        return  sqlTemplate.queryForMap(RngConstants.SELECT_CAMPAIGN_BY_ID +id +";");
    }

    public List<String> getRestaurantIdByCampaignId(String id){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(tradeDiscountDB);
        return  sqlTemplate.queryForListFromSingleRow(RngConstants.SELECT_RESTAURANT_FROM_CAMPAIGN_RESTAURANT_MAP +id +";","restaurant_id");
    }




    /**********HELPER***********/

    public String createTemplate(String campaigntype ) throws IOException {
        String statuscode_jsonpath = "$.statusCode";
        String template_creation_id = "$.data.id";
        SoftAssert softAssert = new SoftAssert();

        String validfrom = String.valueOf(getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));


        HashMap<String , String > requestmap=createRequestMap(campaigntype, validfrom,validTill,"false","30",RngConstants.ZERO_DAYS_DORMANT,"false");

        Processor response = createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);
        softAssert.assertAll();
        return template_id;
    }


    public String createTemplateWithDormantUserNull(String campaigntype, String firstOrder, String mincartamount, String dormantuser, String userRestriction) throws IOException {

        String statuscode_jsonpath = "$.statusCode";
        String template_creation_id = "$.data.id";
        String doramantuser_type ="$.data.dormantUserType";

        SoftAssert softAssert = new SoftAssert();
        String validfrom = String.valueOf(getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));



        HashMap<String , String > requestmap=createRequestMap(campaigntype, validfrom,validTill,firstOrder,mincartamount,dormantuser,userRestriction);
        Processor response = createTemplateWithDormantUserNull(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode,"1");

        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertNotNull(template_id);

        String dormant_user_type =   JSON.toString(response.ResponseValidator.GetNodeValue(doramantuser_type));
        softAssert.assertTrue(dormant_user_type.contains(RngConstants.ZERO_DAYS_DORMANT));

        softAssert.assertAll();
        return template_id;
    }



    public String createTemplateWithAllParams(String campaigntype,  String valid_from,String valid_till,String firstOrder,String mincartamount,String dormantuser,String userRestriction,String timespantype) throws IOException {
        String statuscode_jsonpath = "$.statusCode";
        String template_creation_id = "$.data.id";
        SoftAssert softAssert = new SoftAssert();

        String validfrom = String.valueOf(getFutureTimeForMinutesInMilliSecondsFromCurrentDate(Integer.valueOf(valid_from)));
        String validTill = String.valueOf(getFutureTimeForMonthsInMilliSecondsFromCurrentDate(Integer.valueOf(valid_till)));


        HashMap<String , String > requestmap=createRequestMap(campaigntype, validfrom,validTill,firstOrder,mincartamount,dormantuser,userRestriction);

        Processor response = createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION),timespantype);

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);
        softAssert.assertAll();
        return template_id;
    }


    public String createTemplateWithAllParams(String campaigntype,  String valid_from,String valid_till,String firstOrder,String mincartamount,String dormantuser,String userRestriction,String timespantype,String rfo) throws IOException {
        String statuscode_jsonpath = "$.statusCode";
        String template_creation_id = "$.data.id";
        SoftAssert softAssert = new SoftAssert();

        String validfrom = String.valueOf(getFutureTimeForMinutesInMilliSecondsFromCurrentDate(Integer.valueOf(valid_from)));
        String validTill = String.valueOf(getFutureTimeForMonthsInMilliSecondsFromCurrentDate(Integer.valueOf(valid_till)));


        HashMap<String , String > requestmap=createRequestMap(campaigntype, validfrom,validTill,firstOrder,mincartamount,dormantuser,userRestriction);

        Processor response = createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION),timespantype,rfo);

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);
        softAssert.assertAll();
        return template_id;
    }









    private static HashMap<String, String> createRequestMap(String campaigntype, String validfrom, String validtill,String firstorder, String mincartamount, String dormantUserType, String userRestriction){

        HashMap<String , String > requestmap= new HashMap<>();
        requestmap.put(RngConstants.CAMPAIGN_TYPE,campaigntype);
        requestmap.put(RngConstants.VALID_FROM,validfrom);
        requestmap.put(RngConstants.VALID_TILL,validtill);
        requestmap.put(RngConstants.MIN_CART_AMOUNT,mincartamount);
        requestmap.put(RngConstants.FIRST_ORDER,firstorder);
        requestmap.put(RngConstants.DORMANT_USER_TYPE,dormantUserType);
        requestmap.put(RngConstants.USER_RESTRICTION,userRestriction);
        return requestmap;
    }






}
