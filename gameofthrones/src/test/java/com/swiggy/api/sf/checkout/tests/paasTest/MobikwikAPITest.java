package com.swiggy.api.sf.checkout.tests.paasTest;


import com.swiggy.api.sf.checkout.helper.paas.MySmsValidator;
import com.swiggy.api.sf.checkout.helper.paas.PaasWalletValidator;
import com.swiggy.api.sf.checkout.constants.PaasConstant;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

public class MobikwikAPITest {

    PaasWalletValidator paasWalletValidator = new PaasWalletValidator();
    MySmsValidator mySmsValidator = new MySmsValidator();

    String mobile = System.getenv("mobile");
    String password = System.getenv("password");

    String tid;
    String token;
    String authToken;

    @BeforeClass
    public void login() {
        if (mobile == null || mobile.isEmpty()) {
            mobile = PaasConstant.MOBILE;
        }
        if (password == null || password.isEmpty()) {
            password = PaasConstant.PASSWORD;
        }

        HashMap<String, String> hashMap = paasWalletValidator.loginTokenData(mobile, password);
        tid = hashMap.get("tid");
        token = hashMap.get("token");
        authToken = mySmsValidator.getAuthToken();
    }

    @Test(priority = 1)
    public void invokeMobikwikLinkApi() {

        int statusCode = paasWalletValidator.callMobikwikCheckBalance(tid, token);

        if (statusCode != 0) {
            mySmsValidator.deleteAllConversation(authToken);
            paasWalletValidator.validateMobikwikLink(tid, token);
        } else {
            paasWalletValidator.callMobikwikDelink(tid, token);
            invokeMobikwikLinkApi();
        }
    }

    @Test (dependsOnMethods="invokeMobikwikLinkApi",priority = 2)
    public void invokeMobikwikValidateOTPApi() {
        String otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.MOBIKWIK_ADDRESS);
        int retry = 0;

        while (otp==null && retry < PaasConstant.MAX_RETRY){
            invokeMobikwikLinkApi();
            otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.MOBIKWIK_ADDRESS);
            retry++;
        }
        paasWalletValidator.validatemobikwikOtp(tid, token, otp);
    }

    @Test (dependsOnMethods="invokeMobikwikValidateOTPApi",priority = 3)
    public void invokeMobikwikCheckBalanceApi() {

        paasWalletValidator.validateMobikwikCheckBalance(tid, token);
    }


    @Test (dependsOnMethods="invokeMobikwikValidateOTPApi",priority = 4)
    public void invokeMobikwikHasTokenApi() {

        paasWalletValidator.validateMobikwikHasToken(tid, token);
    }


    @Test (dependsOnMethods="invokeMobikwikValidateOTPApi",priority = 5)
    public void invokeMobikwikDeLinkApi() {

        paasWalletValidator.validateMobikwikDelink(tid, token);
    }


}