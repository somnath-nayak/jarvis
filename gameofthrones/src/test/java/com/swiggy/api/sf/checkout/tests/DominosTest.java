
package com.swiggy.api.sf.checkout.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.dp.CheckoutDP;
import com.swiggy.api.sf.checkout.dp.DominosDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;


public class DominosTest extends DominosDP {

    Initialize gameofthrones = new Initialize();
    JsonHelper jsonHelper= new JsonHelper();
    CheckoutHelper checkoutHelper= new CheckoutHelper();

    @Test(dataProvider = "userSingleOrder",groups = { "Regression", "Smoke" }, description = "")
    public void userSingleOrderPlace(CreateMenuEntry payload) throws IOException
    {
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap=checkoutHelper.createLogin(payload.getpassword(), payload.getmobile());
        String response= checkoutHelper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        checkoutHelper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
    }

    @Test(dataProvider = "SingleOrder",groups = { "Regression", "Smoke" }, description = "")
    public void SingleOrderPlace(CreateMenuEntry payload) throws IOException
    {
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap=checkoutHelper.createLogin(payload.getpassword(), payload.getmobile());
        String response= checkoutHelper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        checkoutHelper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
    }

}