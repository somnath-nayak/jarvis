package com.swiggy.api.sf.snd.tests.pop;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.SolvingForRainsDP;
import com.swiggy.api.erp.cms.helper.Popv2Helper;
import com.swiggy.api.sf.snd.helper.POPHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

/*This class creates, checks and validates POP data from SAND side. We did not have any helper methods
from delivery and hence we had to hard code the restaurant ids and write the test cases. Please make sure to add the restaurant id's */

public class PopCreationAndValidation extends SolvingForRainsDP {

    String itemId;
    Popv2Helper v2 = new Popv2Helper();
    POPHelper popHelper = new POPHelper();

    @Test(dataProvider = "PopItem", description = "Creating pop item using restaurant id")
    public void popItemCreation(String rest_id, String popUsageType)
            throws IOException, JSONException, InterruptedException {

        Processor p = v2.PopItemCreation(Integer.parseInt(rest_id), popUsageType);
        String response = p.ResponseValidator.GetBodyAsText();
        Thread.sleep(5000);
        int restId = Integer.parseInt(rest_id);
        itemId = JsonPath.read(response, "$.data.uniqueId").toString();
        Assert.assertEquals(1, p.ResponseValidator.GetNodeValueAsInt("statusCode"),"Unable to Create Item");
        Processor p1 = v2.ItemSchedule(Integer.parseInt(itemId));
        Assert.assertEquals(1, p1.ResponseValidator.GetNodeValueAsInt("statusCode"),"Unable to Item Schedule");
        System.out.println(
                "***********************************created item" + itemId + "***********************************");

    }

   @Test(dataProvider = "PopItem", description = "Verifying whether POP id created or not")
    public void verifyFromSandWhetherPopIsTrue(String rest_id, String popUsageType, String lat_lon) throws Exception{
        Processor p = v2.PopItemCreation(Integer.parseInt(rest_id), popUsageType);
        String response = p.ResponseValidator.GetBodyAsText();
        Thread.sleep(5000);
        int restId = Integer.parseInt(rest_id);
        itemId = JsonPath.read(response, "$.data.uniqueId").toString();
        Assert.assertEquals(1, p.ResponseValidator.GetNodeValueAsInt("statusCode"),"Unable to Create Item");
        Processor p1 = v2.ItemSchedule(Integer.parseInt(itemId));
        Assert.assertEquals(1, p1.ResponseValidator.GetNodeValueAsInt("statusCode"),"Unable to Item Schedule");
        System.out.println(
                "***********************************created item" + itemId + "***********************************");

        /* Below code is to check the serviceability from delivery */

   String assertingTheDeliveryForServiceability = popHelper.toCheckTheDeliverySystemForServiceability(lat_lon).ResponseValidator.GetBodyAsText();
   Assert.assertEquals(true,true);

   /*Next to get the restaurant with pop items*/

       String [] latLon = {lat_lon};
       String popAggregatorResponse = popHelper.popAggregator(latLon).ResponseValidator.GetBodyAsText();

    }
}
