package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "area",
        "city"
})
public class RestaurantListingPOJO {

    @JsonProperty("area")
    private AreaPOJO area;
    @JsonProperty("city")
    private CityPOJO city;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public RestaurantListingPOJO() {
    }

    /**
     *
     * @param area
     * @param city
     */
    public RestaurantListingPOJO(AreaPOJO area, CityPOJO city) {
        super();
        this.area = area;
        this.city = city;
    }

    @JsonProperty("area")
    public AreaPOJO getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(AreaPOJO area) {
        this.area = area;
    }

    public RestaurantListingPOJO withArea(AreaPOJO area) {
        this.area = area;
        return this;
    }

    @JsonProperty("city")
    public CityPOJO getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(CityPOJO city) {
        this.city = city;
    }

    public RestaurantListingPOJO withCity(CityPOJO city) {
        this.city = city;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public RestaurantListingPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public RestaurantListingPOJO setDefault() {
        AreaPOJO areaPOJO = new AreaPOJO().setDefault();
        CityPOJO cityPOJO = new CityPOJO().setDefault();
        return this.withArea(areaPOJO)
                .withCity(cityPOJO);
    }
}
