package com.swiggy.api.sf.checkout.dp;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.snd.helper.SANDHelper;

public class PayTMMockDP {
    
	EDVOCartHelper eDVOCartHelper=new EDVOCartHelper();
	SANDHelper sandHelper = new SANDHelper();
	static String restId;
	String[] latLog;
	static Cart cartPayload;
	
	@BeforeClass
    public void getRestData() {
	
	    latLog=new String[]{"12.935215600000001","77.6199608"};
		
		String aggregatorRes=sandHelper.aggregator(latLog).ResponseValidator.GetBodyAsText();
		String[] restList=JsonPath.read(aggregatorRes, "$.data.restaurants[*].id").toString().replace("[","").replace("]","").split(",");
		for(int i=0;i<restList.length;i++){
		String serviceability=JsonPath.read(aggregatorRes, "$.data.restaurants["+restList[i]+"]..sla..serviceability")
				                      .toString().replace("[","").replace("]","").replace("\"","");
		String isOpened=JsonPath.read(aggregatorRes, "$.data.restaurants["+restList[i]+"]..availability..opened")
				                .toString().replace("[","").replace("]","");
		if (isOpened.equalsIgnoreCase("true") && serviceability.equalsIgnoreCase("SERVICEABLE")) {
				restId=restList[i].replace("\"","");
				break;
			}}
		cartPayload=eDVOCartHelper.getCartPayload1(null, restId, false, false, true);
   }
	
    @DataProvider(name = "payTMWithdraw")
    public static Object[][] restData() {
    	   return new Object[][]{
    		   {cartPayload}};
    }
}