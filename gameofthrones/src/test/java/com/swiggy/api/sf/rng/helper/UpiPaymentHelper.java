package com.swiggy.api.sf.rng.helper;

import com.swiggy.api.sf.rng.pojo.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.util.ArrayList;
import java.util.List;

public class UpiPaymentHelper {
    RngHelper rngHelper = new RngHelper();
    JsonHelper jsonHelper=new JsonHelper();
    CartPOJO cartPOJO = new CartPOJO();
    HeadersPOJO headers = new HeadersPOJO();
    PaymentMethodsPOJO paymentMethods = new PaymentMethodsPOJO();
    static Initialize gameofthrones = new Initialize();

    public Processor mapCouponForUsers(UPIPaymentContractPOJO upiPaymentContractPOJO, String couponCode, String paymentCode, String type)throws Exception {

            PaymentCodeMappingPOJO paymentCodeMappingPOJO = new PaymentCodeMappingPOJO();
            paymentCodeMappingPOJO.withCouponCode(couponCode);
            paymentCodeMappingPOJO.withPaymentCode(paymentCode);
            paymentCodeMappingPOJO.withType(type);

            List<PaymentCodeMappingPOJO> listPaymentCode = new ArrayList<>();
            listPaymentCode.add(paymentCodeMappingPOJO);
            upiPaymentContractPOJO.setPaymentCodeMappings(listPaymentCode);
            upiPaymentContractPOJO.setCreatedBy("user@swiggy.in");

            Processor updateResponse = rngHelper.mapCoupon(upiPaymentContractPOJO);

        return updateResponse;
    }

    public Processor deletePaymentForUsers(UPIPaymentContractPOJO upiPaymentContractPOJO, String couponCode, String paymentCode, String type)throws Exception {

        PaymentCodeMappingPOJO paymentCodeMappingPOJO = new PaymentCodeMappingPOJO();
        paymentCodeMappingPOJO.withCouponCode(couponCode);
        paymentCodeMappingPOJO.withPaymentCode(paymentCode);
        paymentCodeMappingPOJO.withType(type);

        List<PaymentCodeMappingPOJO> listPaymentCode = new ArrayList<>();
        listPaymentCode.add(paymentCodeMappingPOJO);
        upiPaymentContractPOJO.setPaymentCodeMappings(listPaymentCode);
        upiPaymentContractPOJO.setCreatedBy("user@swiggy.in");

        Processor updateResponse = rngHelper.deletePayment(upiPaymentContractPOJO);

        return updateResponse;
    }

    public Processor applyCouponForUsers(CouponApplyPOJO couponApplyPOJO, String couponCode,Integer source) throws Exception {

        cartPOJO.setDefault();
        headers.setDefault();
        paymentMethods.setDefault();

        couponApplyPOJO.withCart(cartPOJO)
                .withCode(couponCode)
                .withHeaders(headers)
                .withPaymentMethods(paymentMethods);

        Processor updateResponse = rngHelper.couponApply001(couponApplyPOJO, couponCode);

        return updateResponse;

    }

}
