package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class CartBlob {
	@JsonProperty("swiggyTradeDiscount")
	private Double swiggyTradeDiscount;
	@JsonProperty("restaurantTradeDiscount")
	private Double restaurantTradeDiscount;
	@JsonProperty("cartItems")
	private List<CartItem> cartItems = null;
	@JsonProperty("restaurantId")
	private String restaurantId;
	@JsonProperty("shared")
	private Boolean shared;
	@JsonProperty("cart_type")
	private String cartType;

	@JsonProperty("swiggyTradeDiscount")
	public Double getSwiggyTradeDiscount() {
		return swiggyTradeDiscount;
	}

	@JsonProperty("swiggyTradeDiscount")
	public void setSwiggyTradeDiscount(Double swiggyTradeDiscount) {
		this.swiggyTradeDiscount = swiggyTradeDiscount;
	}

	public CartBlob withSwiggyTradeDiscount(Double swiggyTradeDiscount) {
		this.swiggyTradeDiscount = swiggyTradeDiscount;
		return this;
	}

	@JsonProperty("restaurantTradeDiscount")
	public Double getRestaurantTradeDiscount() {
		return restaurantTradeDiscount;
	}

	@JsonProperty("restaurantTradeDiscount")
	public void setRestaurantTradeDiscount(Double restaurantTradeDiscount) {
		this.restaurantTradeDiscount = restaurantTradeDiscount;
	}

	public CartBlob withRestaurantTradeDiscount(Double restaurantTradeDiscount) {
		this.restaurantTradeDiscount = restaurantTradeDiscount;
		return this;
	}

	@JsonProperty("cartItems")
	public List<CartItem> getCartItems() {
		return cartItems;
	}

	@JsonProperty("cartItems")
	public void setCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
	}

	public CartBlob withCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
		return this;
	}

	@JsonProperty("restaurantId")
	public String getRestaurantId() {
		return restaurantId;
	}

	@JsonProperty("restaurantId")
	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}

	public CartBlob withRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
		return this;
	}

	@JsonProperty("shared")
	public Boolean getShared() {
		return shared;
	}

	@JsonProperty("shared")
	public void setShared(Boolean shared) {
		this.shared = shared;
	}

	public CartBlob withShared(Boolean shared) {
		this.shared = shared;
		return this;
	}

	@JsonProperty("cart_type")
	public String getCartType() {
		return cartType;
	}

	@JsonProperty("cart_type")
	public void setCartType(String cartType) {
		this.cartType = cartType;
	}

	public CartBlob withCartType(String cartType) {
		this.cartType = cartType;
		return this;
	}
	public CartBlob setDefaultData(Integer menuItemID, String restID) {
		return this
				.withSwiggyTradeDiscount(0.0)
				.withRestaurantTradeDiscount(0.0)
				.withShared(false)
				.withCartType("Regular")
				.withRestaurantId(restID)
				.withCartItems(new ArrayList<CartItem>() 
				{{
					add(new CartItem().withItemKey(null).withMenuItemId(menuItemID).withQuantity(1));
					}}
				);
	}
}
