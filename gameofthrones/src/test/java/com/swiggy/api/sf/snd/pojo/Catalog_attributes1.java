package com.swiggy.api.sf.snd.pojo;


public class Catalog_attributes1
{
    private String[] accompaniments;

    private String packaging;

    private String cutlery;

    private String prep_style;

    private String veg_classifier;

    private String spice_level;

    private String serves_how_many;

    public String[] getAccompaniments ()
    {
        return accompaniments;
    }

    public void setAccompaniments (String[] accompaniments)
    {
        this.accompaniments = accompaniments;
    }

    public String getPackaging ()
    {
        return packaging;
    }

    public void setPackaging (String packaging)
    {
        this.packaging = packaging;
    }

    public String getCutlery ()
    {
        return cutlery;
    }

    public void setCutlery (String cutlery)
    {
        this.cutlery = cutlery;
    }

    public String getPrep_style ()
    {
        return prep_style;
    }

    public void setPrep_style (String prep_style)
    {
        this.prep_style = prep_style;
    }

    public String getVeg_classifier ()
    {
        return veg_classifier;
    }

    public void setVeg_classifier (String veg_classifier)
    {
        this.veg_classifier = veg_classifier;
    }

    public String getSpice_level ()
    {
        return spice_level;
    }

    public void setSpice_level (String spice_level)
    {
        this.spice_level = spice_level;
    }

    public String getServes_how_many ()
    {
        return serves_how_many;
    }

    public void setServes_how_many (String serves_how_many)
    {
        this.serves_how_many = serves_how_many;
    }

    public Catalog_attributes1 build(){
        setDefaultValues();
        return this;
    }

    public void setDefaultValues(){
        if(this.getAccompaniments()==null){
            this.setAccompaniments(new String[]{""});
        }
        if(this.getServes_how_many()==null){
            this.setServes_how_many("8");
        }
        if(this.getCutlery()==null){
            this.setCutlery("Spoon");

        }
        if(this.getPackaging()==null){
            this.setPackaging("Aluminium bag with airtight seal");
        }
        if(this.getPrep_style()==null){
            this.setPrep_style("Steaming");
        }
        if(this.getVeg_classifier()==null){
            this.setVeg_classifier("EGG");
        }

        if(this.getSpice_level()==null){
            this.setSpice_level("MEDIUMSPICY");
        }

    }

    @Override
    public String toString()
    {
        return "ClassPojo [accompaniments = "+accompaniments+", packaging = "+packaging+", cutlery = "+cutlery+", prep_style = "+prep_style+", veg_classifier = "+veg_classifier+", spice_level = "+spice_level+", serves_how_many = "+serves_how_many+"]";
    }
}
