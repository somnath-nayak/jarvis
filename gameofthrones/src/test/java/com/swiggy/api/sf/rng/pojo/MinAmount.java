package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

public class MinAmount {

	@JsonProperty("cart")
	private Integer cart;

	@JsonProperty("cart")
	public Integer getCart() {
		return cart;
	}

	@JsonProperty("cart")
	public void setCart(Integer cart) {
		this.cart = cart;
	}

	public MinAmount withCart(Integer cart) {
		this.cart = cart;
		return this;
	}
	
}