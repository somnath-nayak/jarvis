package com.swiggy.api.sf.snd.tests;


import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import org.json.simple.JSONValue;
import org.testng.annotations.Test;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBCheck {

    public String getJSONFromResultSet(ResultSet rs, String keyName) {
        Map json = new HashMap();
        System.out.println("__"+rs);
        List list = new ArrayList();
        if(rs!=null)
        {
            try {
                ResultSetMetaData metaData = rs.getMetaData();
                while(rs.next())
                {
                    Map<String,Object> columnMap = new HashMap<>();
                    for(int columnIndex=1;columnIndex<=metaData.getColumnCount();columnIndex++)
                    {
                        if(rs.getString(metaData.getColumnName(columnIndex))!=null)
                            columnMap.put(metaData.getColumnLabel(columnIndex),     rs.getString(metaData.getColumnName(columnIndex)));
                        else
                            columnMap.put(metaData.getColumnLabel(columnIndex), "");
                    }
                    list.add(columnMap);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            json.put(keyName, list);
            System.out.println(json.get(keyName));
        }
        return JSONValue.toJSONString(json);
        //return JSONValue.toJSONString(json);
    }

    public List<Map<String, Object>> dbs() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from swiggy.area");
        return list;
    }

    @Test
    public void t(){
        System.out.println(dbs());
    }



}
