package com.swiggy.api.sf.rng.dp.TradeDiscount;

import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.SuperHelper;
import com.swiggy.api.sf.rng.helper.TDHelper.SuperV2TdHelper;
import com.swiggy.api.sf.rng.tests.RandomNumber;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class SuperV2Dp {
    RngHelper rngHelper = new RngHelper();
    SuperV2TdHelper superV2TdHelper = new SuperV2TdHelper();
    static SuperHelper superhelper = new SuperHelper();
    JsonHelper jsonHelper = new JsonHelper();

    RngConstants rngConstants;
    RandomNumber rm = new RandomNumber(2000, 3000);


    @DataProvider(name = "FreeDeliveryTD_EvaluationAtRestaurantLevel")
    public Object[][] FreeDeliveryTD_EvaluationWithNoMinCartAtRestaurantLevel() throws IOException {

        String restId = new Integer(rm.nextInt()).toString();
        String itemId = new Integer(rm.nextInt()).toString();
        String categoryId = new Integer(rm.nextInt()).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
        String count = "1";
        HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, false, false,
                "ZERO_DAYS_DORMANT", false);

        String userId = new Integer(rm.nextInt()).toString();
        String tid = (String) Keys.get("TDID");
        String price = "500";
        String minCartAmount = "500";

        //LISTING
        ArrayList a1 = superV2TdHelper.getArrayList(restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a2 = superV2TdHelper.getArrayList(restId, "true", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        //MENU
        ArrayList a3 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a4 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "true", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        //CART
        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, "99", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a6 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);

        return new Object[][]{
                {"list", a1, tid, true}, {"list", a2, tid, true},
                {"menu", a3, tid, true}, {"menu", a4, tid, true},
                {"cart", a5, tid, true}, {"cart", a6, tid, true}


        };
    }


    @DataProvider(name = "superSubscriptionFreeDel")
    public Object[][] superSubscriptionFreeDel() throws IOException {

        //SUPER SUBSCRIPTION
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        HashMap<String, String> createBenefit = new HashMap<String, String>();
        String numOfPlans = "1";

        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays

        createPublicPlan.put("0", "0");
        createPublicPlan.put("1", "3");
        createPublicPlan.put("2", "1");
        createPublicPlan.put("3", "99");
        createPublicPlan.put("4", "5");
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        // type, priority
        createBenefit.put("0", "FREE_DELIVERY");
        createBenefit.put("1", "1");
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        //TD creation & evaluation
        String restId = new Integer(rm.nextInt()).toString();
        String itemId = new Integer(rm.nextInt()).toString();
        String categoryId = new Integer(rm.nextInt()).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();
        String count = "1";
        HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, false, false,
                "ZERO_DAYS_DORMANT", false);

        String userId = new Integer(rm.nextInt()).toString();
        String tid = (String) Keys.get("TDID");
        String price = "500";
        String minCartAmount = "500";

        //LISTING
        ArrayList a1 = superV2TdHelper.getArrayList(restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a2 = superV2TdHelper.getArrayList(restId, "true", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        //MENU
        ArrayList a3 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a4 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "true", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        //CART
        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, "99", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a6 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);


        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, "list", a1, tid, true}, {planPayload[0], numOfPlans, benefitPayload, "list", a1, tid, true},
                {planPayload[0], numOfPlans, benefitPayload, "menu", a3, tid, true}, {planPayload[0], numOfPlans, benefitPayload, "menu", a4, tid, true},
                {planPayload[0], numOfPlans, benefitPayload, "cart", a5, tid, true}, {planPayload[0], numOfPlans, benefitPayload, "cart", a6, tid, true}
        };

    }

    @DataProvider(name = "superPlanBenefitsFreeDelMapped")
    public Object[][] superPlanBenefitsFreeDelMapped() throws IOException {

        //SUPER SUBSCRIPTION
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        HashMap<String, String> createBenefit = new HashMap<String, String>();
        String numOfPlans = "1";

        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays

        createPublicPlan.put("0", "0");
        createPublicPlan.put("1", "3");
        createPublicPlan.put("2", "1");
        createPublicPlan.put("3", "99");
        createPublicPlan.put("4", "5");
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        // type, priority
        createBenefit.put("0", "FREE_DELIVERY");
        createBenefit.put("1", "1");
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        //TD creation & evaluation
        String restId = new Integer(rm.nextInt()).toString();
        String itemId = new Integer(rm.nextInt()).toString();
        String categoryId = new Integer(rm.nextInt()).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();
        String count = "1";
        HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, false, false,
                "ZERO_DAYS_DORMANT", false);

        String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,

        String tid = (String) Keys.get("TDID");

        String price = "500";
        String cartAmount = "500";
        String minCartAmount = "500";

        //LISTING
        ArrayList a1 = superV2TdHelper.getArrayList(restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a2 = superV2TdHelper.getArrayList(restId, "true", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        //MENU
        ArrayList a3 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a4 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "true", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        //CART
        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, "99", userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a6 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, minCartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, "list_superplanmapped", a1, tid, true}, {planPayload[0], numOfPlans, benefitPayload, "list_superplanmapped", a1, tid, true},
                {planPayload[0], numOfPlans, benefitPayload, "menu", a3, tid, true}, {planPayload[0], numOfPlans, benefitPayload, "menu", a4, tid, true},
                {planPayload[0], numOfPlans, benefitPayload, "cart", a5, tid, true}, {planPayload[0], numOfPlans, benefitPayload, "cart", a6, tid, true}
        };
    }





    @DataProvider(name = "superSubscriptionFlatDp")
    public Object[][] superSubscriptionFlat() throws IOException {

        //SUPER SUBSCRIPTION
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        HashMap<String, String> createBenefit = new HashMap<String, String>();
        String numOfPlans = "1";

        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays

        createPublicPlan.put("0", "0");
        createPublicPlan.put("1", "3");
        createPublicPlan.put("2", "1");
        createPublicPlan.put("3", "99");
        createPublicPlan.put("4", "5");
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        // type, priority
        createBenefit.put("0", "Flat");
        createBenefit.put("1", "1");
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload}};
    }


    @DataProvider(name = "superSubscriptionPercentageDp")
    public Object[][] superSubscriptionPercentage() throws IOException {

        //SUPER SUBSCRIPTION
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        HashMap<String, String> createBenefit = new HashMap<String, String>();
        String numOfPlans = "1";

        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays

        createPublicPlan.put("0", "0");
        createPublicPlan.put("1", "3");
        createPublicPlan.put("2", "1");
        createPublicPlan.put("3", "99");
        createPublicPlan.put("4", "5");
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        // type, priority
        createBenefit.put("0", "Percentage");
        createBenefit.put("1", "1");
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload}};
    }

    @DataProvider(name = "superSubscriptionFreebieDp")
    public Object[][] superSubscriptionFreebie() throws IOException {

        //SUPER SUBSCRIPTION
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        HashMap<String, String> createBenefit = new HashMap<String, String>();
        String numOfPlans = "1";

        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays

        createPublicPlan.put("0", "0");
        createPublicPlan.put("1", "3");
        createPublicPlan.put("2", "1");
        createPublicPlan.put("3", "99");
        createPublicPlan.put("4", "5");
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        // type, priority
        createBenefit.put("0", "Freebie");
        createBenefit.put("1", "1");
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload}};
    }


    @DataProvider(name = "superPlanBenefitsFreeDelMappedDp")
    public Object[][] superPlanBenefitsFreeDelMappedDp() throws IOException {
        //SUPER SUBSCRIPTION
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        HashMap<String, String> createBenefit = new HashMap<String, String>();
        String numOfPlans = "1";

        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays

        createPublicPlan.put("0", "0");
        createPublicPlan.put("1", "3");
        createPublicPlan.put("2", "1");
        createPublicPlan.put("3", "99");
        createPublicPlan.put("4", "5");
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        // type, priority
        createBenefit.put("0", "FREE_DELIVERY");
        createBenefit.put("1", "1");
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload}};
    }

    @DataProvider(name = "superPlanBenefitsFreeBieMappedDp")
    public Object[][] superPlanBenefitsFreeBieMappedDp() throws IOException {
        //SUPER SUBSCRIPTION
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        HashMap<String, String> createBenefit = new HashMap<String, String>();
        String numOfPlans = "1";

        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays

        createPublicPlan.put("0", "0");
        createPublicPlan.put("1", "3");
        createPublicPlan.put("2", "1");
        createPublicPlan.put("3", "99");
        createPublicPlan.put("4", "5");
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        // type, priority
        createBenefit.put("0", "Freebie");
        createBenefit.put("1", "1");
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload}};
    }


    @DataProvider(name = "superPlanBenefitsFlatMappedDp")
    public Object[][] superPlanBenefitsFlatMappedDp() throws IOException {
        //SUPER SUBSCRIPTION
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        HashMap<String, String> createBenefit = new HashMap<String, String>();
        String numOfPlans = "1";

        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays

        createPublicPlan.put("0", "0");
        createPublicPlan.put("1", "3");
        createPublicPlan.put("2", "1");
        createPublicPlan.put("3", "99");
        createPublicPlan.put("4", "5");
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        // type, priority
        createBenefit.put("0", "Flat");
        createBenefit.put("1", "1");
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload}};
    }


    @DataProvider(name = "superPlanBenefitsPercenatageMappedDp")
    public Object[][] superPlanBenefitsPercenatageMappedDp() throws IOException {
        //SUPER SUBSCRIPTION
        HashMap<String, String> createPublicPlan = new HashMap<String, String>();
        HashMap<String, String> createBenefit = new HashMap<String, String>();
        String numOfPlans = "1";

        // value for create plan-- > validFromDate , numOfMonthValidity , planTenure ,
        // planPrice , renewalOffsetDays

        createPublicPlan.put("0", "0");
        createPublicPlan.put("1", "3");
        createPublicPlan.put("2", "1");
        createPublicPlan.put("3", "99");
        createPublicPlan.put("4", "5");
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        // type, priority
        createBenefit.put("0", "Percentage");
        createBenefit.put("1", "1");
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload}};
    }





}
