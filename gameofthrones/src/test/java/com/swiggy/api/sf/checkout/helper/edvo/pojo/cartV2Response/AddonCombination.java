package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "group_id",
        "addon_id"
})
public class AddonCombination {

    @JsonProperty("group_id")
    private String groupId;
    @JsonProperty("addon_id")
    private String addonId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public AddonCombination() {
    }

    /**
     *
     * @param groupId
     * @param addonId
     */
    public AddonCombination(String groupId, String addonId) {
        super();
        this.groupId = groupId;
        this.addonId = addonId;
    }

    @JsonProperty("group_id")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("addon_id")
    public String getAddonId() {
        return addonId;
    }

    @JsonProperty("addon_id")
    public void setAddonId(String addonId) {
        this.addonId = addonId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("groupId", groupId).append("addonId", addonId).append("additionalProperties", additionalProperties).toString();
    }

}