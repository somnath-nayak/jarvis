package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "order_id",
        "customer_id",
        "order_time",
        "trade_discount_breakup",
        "order_type",
        "coupon_code",
        "coupon_discount",
        "order_tags"
})
public class CreateOrder {

    @JsonProperty("order_id")
    private Integer orderId;
    @JsonProperty("customer_id")
    private String customerId;
    @JsonProperty("order_time")
    private String orderTime;
    @JsonProperty("trade_discount_breakup")
    private List<TradeDiscountBreakup> tradeDiscountBreakup = null;
    @JsonProperty("order_type")
    private String orderType;
    @JsonProperty("coupon_code")
    private String couponCode;
    @JsonProperty("coupon_discount")
    private Integer couponDiscount;
    @JsonProperty("order_tags")
    private List<String> orderTags = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public CreateOrder() {
    }


    /**
     *
     * @param orderTags
     * @param customerId
     * @param orderType
     * @param couponCode
     * @param orderTime
     * @param couponDiscount
     * @param tradeDiscountBreakup
     * @param orderId
     */
    public CreateOrder(Integer orderId, String customerId, String orderTime, List<TradeDiscountBreakup> tradeDiscountBreakup, String orderType, String couponCode, Integer couponDiscount, List<String> orderTags) {
        super();
        this.orderId = orderId;
        this.customerId = customerId;
        this.orderTime = orderTime;
        this.tradeDiscountBreakup = tradeDiscountBreakup;
        this.orderType = orderType;
        this.couponCode = couponCode;
        this.couponDiscount = couponDiscount;
        this.orderTags = orderTags;
    }

    @JsonProperty("order_id")
    public Integer getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("customer_id")
    public String getCustomerId() {
        return customerId;
    }

    @JsonProperty("customer_id")
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @JsonProperty("order_time")
    public String getOrderTime() {
        return orderTime;
    }

    @JsonProperty("order_time")
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    @JsonProperty("trade_discount_breakup")
    public List<TradeDiscountBreakup> getTradeDiscountBreakup() {
        return tradeDiscountBreakup;
    }

    @JsonProperty("trade_discount_breakup")
    public void setTradeDiscountBreakup(List<TradeDiscountBreakup> tradeDiscountBreakup) {
        this.tradeDiscountBreakup = tradeDiscountBreakup;
    }

    @JsonProperty("order_type")
    public String getOrderType() {
        return orderType;
    }

    @JsonProperty("order_type")
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @JsonProperty("coupon_code")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("coupon_code")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    @JsonProperty("coupon_discount")
    public Integer getCouponDiscount() {
        return couponDiscount;
    }

    @JsonProperty("coupon_discount")
    public void setCouponDiscount(Integer couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    @JsonProperty("order_tags")
    public List<String> getOrderTags() {
        return orderTags;
    }

    @JsonProperty("order_tags")
    public void setOrderTags(List<String> orderTags) {
        this.orderTags = orderTags;
    }

}