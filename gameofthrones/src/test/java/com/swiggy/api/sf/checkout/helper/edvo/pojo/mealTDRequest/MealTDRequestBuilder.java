package com.swiggy.api.sf.checkout.helper.edvo.pojo.mealTDRequest;

import java.util.List;

public class MealTDRequestBuilder {
    private MealTDRequest mealTDRequest;
    public MealTDRequestBuilder(){ mealTDRequest = new MealTDRequest();}

    public MealTDRequestBuilder id(Integer id){
        mealTDRequest.setId(id);
        return this;
    }

    public MealTDRequestBuilder namespace(String namespace){
        mealTDRequest.setNamespace(namespace);
        return this;
    }

    public MealTDRequestBuilder header(String header){
        mealTDRequest.setHeader(header);
        return this;
    }

    public MealTDRequestBuilder description(String description){
        mealTDRequest.setDescription(description);
        return this;
    }

    public MealTDRequestBuilder validFrom(String validFrom){
        mealTDRequest.setValidFrom(validFrom);
        return this;
    }

    public MealTDRequestBuilder validTill(String validTill){
        mealTDRequest.setValidTill(validTill);
        return this;
    }

    public MealTDRequestBuilder campaignType(String campaignType){
        mealTDRequest.setCampaignType(campaignType);
        return this;
    }

    public MealTDRequestBuilder restaurantHit(String restaurantHit){
        mealTDRequest.setRestaurantHit(restaurantHit);
        return this;
    }

    public MealTDRequestBuilder enabled(Boolean enabled){
        mealTDRequest.setEnabled(enabled);
        return this;
    }

    public MealTDRequestBuilder swiggyHit(String swiggyHit){
        mealTDRequest.setSwiggyHit(swiggyHit);
        return this;
    }

    public MealTDRequestBuilder createdBy(String createdBy){
        mealTDRequest.setCreatedBy(createdBy);
        return this;
    }

    public MealTDRequestBuilder discountCap(Integer discountCap){
        mealTDRequest.setDiscountCap(discountCap);
        return this;
    }

    public MealTDRequestBuilder discounts(List<Discount> discounts){
        mealTDRequest.setDiscounts(discounts);
        return this;
    }

    public MealTDRequestBuilder slots(List<Object> slots){
        mealTDRequest.setSlots(slots);
        return this;
    }

    public MealTDRequestBuilder commissionOnFullBill(Boolean commissionOnFullBill){
        mealTDRequest.setCommissionOnFullBill(commissionOnFullBill);
        return this;
    }

    public MealTDRequestBuilder taxesOnDiscountedBill(Boolean taxesOnDiscountedBill){
        mealTDRequest.setTaxesOnDiscountedBill(taxesOnDiscountedBill);
        return this;
    }

    public MealTDRequestBuilder firstOrderRestriction(Boolean firstOrderRestriction){
        mealTDRequest.setFirstOrderRestriction(firstOrderRestriction);
        return this;
    }

    public MealTDRequestBuilder timeSlotRestriction(Boolean timeSlotRestriction){
        mealTDRequest.setTimeSlotRestriction(timeSlotRestriction);
        return this;
    }

    public MealTDRequestBuilder userRestriction(Boolean userRestriction){
        mealTDRequest.setUserRestriction(userRestriction);
        return this;
    }

    public MealTDRequest build(){
        return mealTDRequest;
    }
}
