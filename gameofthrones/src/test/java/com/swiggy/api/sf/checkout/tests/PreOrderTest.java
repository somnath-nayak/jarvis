
package com.swiggy.api.sf.checkout.tests;


import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.dp.PreOrderDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CartPayloadHelper;
import com.swiggy.api.sf.checkout.helper.PreOrderHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PreOrderTest extends PreOrderDP {

    CheckoutHelper helper= new CheckoutHelper();
    CartPayloadHelper cartPayloadHelper=new CartPayloadHelper();
    PreOrderHelper preOrderHelper=new PreOrderHelper();

    String tid;
    String token;
    String cartResponse;

    String mobile= System.getenv("mobile");
    String password= System.getenv("password");

    @BeforeTest
    public void login(){
        if (mobile == null || mobile.isEmpty()){
            mobile=CheckoutConstants.mobile;
        }

        if (password == null ||password.isEmpty()){
            password=CheckoutConstants.password;
        }

       HashMap<String, String> hashMap=helper.getTokenDataCheckout(mobile,password);
        tid=hashMap.get("Tid");
        token=hashMap.get("Token");
    }



    @Test(dataProvider="preorder",priority = 1)
    public void preOrderEnableCMSSetup(Cart cartPayload){
        String restId=cartPayload.getRestaurantId();
        String response=preOrderHelper.preOrderEnable(restId,"preorder_enabled","1")
                .ResponseValidator.GetBodyAsText();
        int status=JsonPath.read(response,"$.status");
        Assert.assertEquals(status,1);
    }


    @Test(dataProvider="preorder",dependsOnMethods="preOrderEnableCMSSetup",priority = 2)
    public void preOrderRestScheduleCMSSetup(Cart cartPayload){
        String restId=cartPayload.getRestaurantId();
        preOrderHelper.preorderSlots(restId,PreOrderHelper.getDay(),"0","2330");

        List<Map<String, Object>> data=preOrderHelper.getPreOrderRestSchedule(restId);
        Assert.assertNotEquals(data.size(),0,"");
    }

    @Test(dataProvider="preorder",dependsOnMethods="preOrderRestScheduleCMSSetup",priority = 3)
    public void preOrderItemScheduleCMSSetup(Cart cartPayload){
        String menuItemId=cartPayload.getCartItems().get(0).getMenuItemId();
        String day=preOrderHelper.getDay().toUpperCase();

        String response=preOrderHelper.setItemSechedule("0","2330",day,menuItemId)
                                      .ResponseValidator.GetBodyAsText();

        int status=JsonPath.read(response,"$.statusCode");
        Assert.assertEquals(status,1);
    }


    @Test(dataProvider="preorder",dependsOnMethods="preOrderItemScheduleCMSSetup", priority = 4)
    public void preOrderAreaScheduleCMSSetup(Cart cartPayload){
        String restId=cartPayload.getRestaurantId();
        String areaId=preOrderHelper.getAreaIDFromDB(restId)+"";
        String day=preOrderHelper.getDay().toUpperCase();

        String response=preOrderHelper.preOrderAreaScheduleCreate(areaId,day,"1","2330")
                                      .ResponseValidator.GetBodyAsText();

        int status=JsonPath.read(response,"$.status");
        Assert.assertEquals(status,1);

        List<Map<String, Object>> data=preOrderHelper.getPreOrderAreaSchedule(areaId);
        Assert.assertNotEquals(data.size(),0);
    }


    @Test(dataProvider="preorder",dependsOnMethods="preOrderAreaScheduleCMSSetup",priority = 5)
    public void preOrderDBOptionsWpOptionsSAND(Cart cartPayload) {
        String flag = preOrderHelper.getPreOrderDBConf();
        Assert.assertEquals(flag, "true");
    }


    @Test(dataProvider="preorder",dependsOnMethods="preOrderDBOptionsWpOptionsSAND",priority = 6,enabled=true)
    public void preOrderRedisConfigurationSAND(Cart cartPayload) throws InterruptedException {
        String restId=cartPayload.getRestaurantId();
        String cache=preOrderHelper.getResCache(restId).replace("{","").replace("}","");
        System.out.println("--------cache---------"+cache);
        if(cache.isEmpty() || cache==null){
            System.out.println("--------Writting Cache---------"+cache);
            preOrderHelper.setResCache(restId);
            //Thread.sleep(120000);
        }

        String[] cacheJson=preOrderHelper.getResCache(restId).replace("\"","")
                .replace("{","").replace("}","").split(",");
        String[] data=null;

        for (String key : cacheJson) {
            if(key.contains("preorder_enabled")){
                data=key.split(":");
                break;
            }
        }
        Assert.assertEquals(data[1],"true");
    }


    @Test(dataProvider="preorder",dependsOnMethods="preOrderRedisConfigurationSAND",priority = 7)
    public void getPreOrderableFromMenuSAND(Cart cartPayload){
        String restId=cartPayload.getRestaurantId();
        String menuRes = cartPayloadHelper.getRestaurantMenu(restId).ResponseValidator.GetBodyAsText();
        String preorderable= JsonPath.read(menuRes, "$.data.preorderable").toString()
                .replace("[", "").replace("]", "");
        Assert.assertEquals(preorderable,"true");
    }


    @Test(dataProvider="preorder", dependsOnMethods="getPreOrderableFromMenuSAND",priority = 8)
    public void getPreOrderSlotSAND(Cart cartPayload){
        String restId=cartPayload.getRestaurantId();
        String preOrderSlotRes=cartPayloadHelper.getPreOrderSlot(restId).ResponseValidator.GetBodyAsText();

        String data= JsonPath.read(preOrderSlotRes, "$.data").toString()
                .replace("[", "").replace("]", "");
        Assert.assertTrue(!data.isEmpty());
    }

   @Test(dataProvider="preorder", dependsOnMethods="getPreOrderSlotSAND",priority = 9)
    public void CreatePreOrderCartCHECKOUT(Cart cartPayload){

       String restId=cartPayload.getRestaurantId();
       Processor preOrderSlotListing = cartPayloadHelper.getPreOrderSlot(restId);

       int length=cartPayloadHelper.getPreOrderSlotLength(preOrderSlotListing);

       for (int index=0;index <length;index++){
           cartPayload.setPreorderSlot(cartPayloadHelper.getPreOrderSlot(preOrderSlotListing,index));
           cartPayload.setCartType("PREORDER");
           cartResponse=helper.invokeCreateCart(tid,token,Utility.jsonEncode(cartPayload));
           String statusCode=cartPayloadHelper.getStatusCode(cartResponse);
               if (statusCode.equals("0")){
                   break;
               }
       }
       CheckoutHelper.validateApiResSanity(cartResponse);
    }


    @Test(dataProvider="preorder",dependsOnMethods="CreatePreOrderCartCHECKOUT",priority = 10)
    public void preOrderIsCODEnabledDBConfigurationCHECKOUT(Cart cartPayload){
        String flag=preOrderHelper.getPreOrderCODEnabled();
        Assert.assertEquals(flag,"true");
    }


    @Test(dataProvider="preorder",dependsOnMethods="preOrderIsCODEnabledDBConfigurationCHECKOUT",priority =11)
    public void placePreOrderCHECKOUT(Cart cartPayload){

        String response=helper.invokePlaceOrder(tid,token,cartResponse);
    }

}