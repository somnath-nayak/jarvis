package com.swiggy.api.sf.snd.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.Popv2Helper;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.sf.snd.constants.POPConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.noggit.JSONUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.helper
 **/
public class POPRegressionHelper {

    public static Initialize gameofthrones = new Initialize();
    static RestaurantHelper restaurantHelper = new RestaurantHelper();
    static CMSHelper cmsHelper = new CMSHelper();
    Popv2Helper popv2Helper = new Popv2Helper();
    DateHelper dateHelper = new DateHelper();
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    DeliveryHelperMethods deliveryHelperMethods = new DeliveryHelperMethods();
    RedisHelper redisHelper = new RedisHelper();

    public HashMap<String, String> setHeaders() throws JSONException {
        HashMap<String, String> requestheader = new HashMap<String, String>();
        JSONObject json = new JSONObject();
        json.put("source", "automation");
        requestheader.put("Content-Type", "application/json");
        requestheader.put("user-meta", json.toString());
        return requestheader;
    }

    public HashMap<String, String> setHeadersSlot() throws JSONException {
        HashMap<String, String> requestheader = new HashMap<String, String>();
        JSONObject json = new JSONObject();
        json.put("source", "automation");
        json.put("user","Automation-Tester");
        requestheader.put("Content-Type", "application/json");
        requestheader.put("user_meta", json.toString());
        return requestheader;
    }

    public Boolean enableServiceabilityBanner(String restId, Boolean serviceableFlag) {
        Map<String, Object> map;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName);
        String q = "select z.id as zone_id from restaurant r inner join zone z inner join area a on z.id=a.zone_id and r.area_code=a.id where r.id=" + restId;
        System.out.println("--Query ::: "+ q);
        map = sqlTemplate.queryForMap(q);
        String zoneId = map.get("zone_id").toString();
        RedisHelper redisHelper = new RedisHelper();
        String upperBanner = "5";
        String lowerBanner = ".5";
        //Object getBanner = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zoneId);
        if (serviceableFlag==true) {
            redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zoneId, lowerBanner);
            return true;
        } else {
            redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zoneId, upperBanner);
            return true;
        }
    }


    public Boolean enablePopServiceabilityBanner(String restId, Boolean serviceableFlag) {
        Map<String, Object> map;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName);
        String q = "select z.id as zone_id from restaurant r inner join zone z inner join area a on z.id=a.zone_id and r.area_code=a.id where r.id=" + restId;
        System.out.println("--Query ::: "+ q);
        map = sqlTemplate.queryForMap(q);
        String zoneId = map.get("zone_id").toString();
        RedisHelper redisHelper = new RedisHelper();
        if (serviceableFlag==true) {
            redisHelper.setValue("deliveryredis", 1, "BL:POP:" + zoneId, "true");
            return true;
        } else {
            redisHelper.setValue("deliveryredis", 1, "BL:POP:" + zoneId, "false");
            return true;
        }
    }

    public String createPOPItemOfPOPTypeForRestId( String restID, String pop_type) {
       Processor p = null;
        try {
            p = popv2Helper.PopItemCreation(Integer.parseInt(restID),pop_type);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String itemId = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.uniqueId").toString();
        return itemId;
    }

    public Processor createRestaurantMenuMapPopHelper(String menu_type, String parent_category_id, String restId, String type, String name) throws JSONException {
        String[] payload = {menu_type, parent_category_id, restId, type, name};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "restaurantmenumapcreate1", gameofthrones);
        // System.out.println("test" + name);
        Processor p = new Processor(gots, setHeaders(),payload);
        return p;
    }


    public Processor popAggregatorHelper(String lat, String lng){
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        String[] payload = {lat, lng};
        GameOfThronesService gots = new GameOfThronesService("sand", "popaggregator", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public List<Map<String, Object>> getExistingAreaScheduleIdOpenCloseTimeDB(String day, String areaId) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(POPConstants.cmsDB);
        String q = POPConstants.get_area_schedule_id_open_close_time_db.replace("${AREA}",areaId).replace("${DAY}",day);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public boolean deleteConflictingAreaScheduleByIdDB(String[] id, String areaId, String day) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(POPConstants.cmsDB);
        for (int i = 0; i < id.length ; i++) {
            String q =POPConstants.delete_conflicting_area_schedule_db+id[i];
            System.out.println("--Query ::: " + q);
            sqlTemplate.execute(q);
        }
        List<Map<String, Object>> list;
        String q1 = POPConstants.get_area_schedule_by_id.replace("${AREA}",areaId).replace("${DAY}",day);
        System.out.println("--Query ::: " + q1);
        list = sqlTemplate.queryForList(q1);
        if(list.size() == 0)
            return true;
        return false;
    }

    public HashMap<String, String> getExistingAreaSlotIdFromDB(String day, String areaId) {
        List<Map<String, Object>> list;
        HashMap<String, String> hm = new HashMap<>();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(POPConstants.cmsDB);
        String q = POPConstants.get_area_slot_db.replace("${AREA}",areaId).replace("${DAY}",day);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size() == 0)
            return hm;
        hm.put("id", list.get(0).get("id").toString());
        hm.put("open_time", list.get(0).get("open_time").toString());
        hm.put("close_time", list.get(0).get("close_time").toString());
        return hm;
    }

    public boolean deleteConflictingAreaSlotByIdDB(String id) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(POPConstants.cmsDB);
        String q =POPConstants.delete_conflicting_area_slot_db+id;
        System.out.println("--Query ::: " + q);
        sqlTemplate.queryForList(q);
        String q1 = POPConstants.get_area_slot_by_id_db+id;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if(list.size() == 0)
            return true;
        return false;
    }

    public Processor createAreaSlotsHelper(String areaId,String day, String close_time, String open_time){

        String[] payload = {areaId,close_time,day,open_time};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "areaslots", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeadersSlot(), payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor deleteAreaSlotsHelper(String id) {
        String[] params = {id};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "delete_areaslots", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeadersSlot(), null,params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }


    public Processor createCitySlotsHelper(String cityId,String day, String close_time, String open_time){

        String[] payload = {cityId,close_time,day,open_time};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "cityslots", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeadersSlot(), payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor deleteCitySlotsHelper(String id) {
        String[] params = {id};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "delete_cityslots", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeaders(), null,params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public String getCurrentDay() {
        int day = dateHelper.getCurrentDayOfWeekInNumeric();
        return POPConstants.days[day-1];
    }

    public String[] getOpenCloseTime(int deltaMinutes) {
        String open_time = dateHelper.getCurrentTimeInHundredHours();
        String close_time = dateHelper.getMinutesPlusMinusCurrentTimeInHundredHours(deltaMinutes);
        String[] open_close_time = {open_time,close_time};
        return open_close_time;
    }

    public boolean validateConflictingSlotSchedule(HashMap<String, String> hm) {
        int currentTime = Integer.parseInt(dateHelper.getCurrentTimeInHundredHours());
        int closeTime =  Integer.parseInt(hm.get("close_time"));
        int openTime = Integer.parseInt(hm.get("open_time"));
        if(currentTime < closeTime && currentTime > openTime)
            return true;
        else
            return false;
    }

    public Processor createRestaurantSlotHelper(String restId,String day, String close_time, String open_time) {
        String[] payload = {restId,open_time,close_time,day};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "addtimeslots", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeadersSlot(), payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor deleteRestaurantSlotsHelper(String id) {
        String[] params = {id};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "deltimeslots", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeaders(), null,params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor updateAreaScheduleHelper(String areaId,String day, String close_time, String open_time){

        String[] payload = {POPConstants.slot_type,areaId,open_time,close_time,day, POPConstants.menu_type};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "areascheduleupdate", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeaders(), payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor updateAreaSlotHelper(String areaId,String day, String close_time, String open_time){

        String[] payload = {areaId,close_time,day, open_time};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "areascheduleupdate", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeaders(), payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor updateCityScheduleHelper(String cityId,String day, String close_time, String open_time){

        String[] payload = {POPConstants.slot_type,cityId,open_time,close_time,day, POPConstants.menu_type};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "cityscheduleupdate", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeaders(), payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor updateCitySlotHelper(String areaId,String day, String close_time, String open_time){
        String[] payload = {areaId,close_time,day, open_time};
        GameOfThronesService gots = new GameOfThronesService("cmsrestaurantservice", "updatecityslots", gameofthrones);
        Processor processor = null;
        try {
            processor = new Processor(gots, setHeaders(), payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public boolean enableRestaurantPostCreateInDB(String restID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(POPConstants.cmsDB);
        String q =POPConstants.enable_created_restaurant+"'"+restID+"'";
        System.out.println("--Query ::: " + q);
        System.out.println("--REST ID "+restID+" is now enabled");
        sqlTemplate.update(q);
        String q1 = POPConstants.get_rest_details+restID;
        System.out.println("--Query ::: " + q1);
        list = sqlTemplate.queryForList(q1);
        if(list.size() != 0) {
            if (list.get(0).get("enabled").toString().equals("1"))
                return true;
        }
        return false;
    }

    public String[] getPolygonAndZoneIdFromSolr() {
        String q = "polygon:\"Intersects("+POPConstants.lat+" "+ POPConstants.lng+")\"";
        String env = deliveryHelperMethods.getSolrUrl();
        String json = JSONUtil.toJSON(deliveryDataHelper.getSolrOutput(env,"polygon",q));
        System.out.println("--SOLR Response ::: "+ json);
        //$.[?(@.enabled=="true" && @.tags=="POP")].id
        //$.[?(@.enabled=="true" && @.tags=="POP")].id
        String get_polygon = JsonPath.read(json,"$.[?(@.enabled==\"true\" && @.tags[0]==\"POP\")].id").toString().replace("[","").replace("]","").replace("\"","");
        String get_zone = JsonPath.read(json,"$.[?(@.enabled==\"true\" && @.tags[0]==\"POP\")].pzone").toString().replace("[","").replace("]","").replace("\"","");
        System.out.println("--- Polygon and zone where POP is enabled ----");
        System.out.println("--PolygonID ::: "+get_polygon);
        System.out.println("--ZoneID ::: "+get_zone);
        String[] polygon_zone = {get_polygon,get_zone};
        return polygon_zone;
    }

    public boolean mapPolygonWithRestaurant(String restID, String polygonID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(POPConstants.deliveryDB);
        String q = POPConstants.map_restaurant_polygon.replace("${REST}",restID).replace("${POLYGON}",polygonID);
        System.out.println("--Query ::: " + q);
        sqlTemplate.execute(q);
        String q1 = POPConstants.get_data_restaurant_polygon_map.replace("${REST}",restID).replace("${POLYGON}",polygonID);
        System.out.println("--Query ::: " + q1);
        list = sqlTemplate.queryForList(q1);
        if(list.size() > 0)
            return true;
        return false;
    }

    public boolean checkPolygonRestaurantMapping(String polygonID, String restuarantID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(POPConstants.deliveryDB);
        String q = POPConstants.get_data_restaurant_polygon_map.replace("${REST}",restuarantID).replace("${POLYGON}",polygonID);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if(list.size() > 0)
            return true;
        return false;
    }

    public boolean searchIdFromResponseArray(String[] arr , String id) {
        Arrays.sort(arr);
        int i = Arrays.binarySearch(arr,id);
        if(i>=0) return true;
        else return false;
    }

    public Processor createRestaurantHolidaySlotsHelper(String restId, String fromTime, String toTime) {
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "addholidayslots", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        String[] payload = {restId,fromTime,toTime};
        headers.put("Content-Type", "application/json");
        //headers.put("Authorization", SANDConstants.bAuth);
        Processor processer = new Processor(service, headers, payload);
        return processer;
    }

    public Processor sndSearchV2_DishHelper(String lat, String lng, String str_search) {
        GameOfThronesService service = new GameOfThronesService("sand", "searchV2_item", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        String[] params = { lat, lng, str_search };
        headers.put("Content-Type", "application/json");
        Processor processer = new Processor(service, headers,null, params);
        return processer;
    }

    public void avail_item_schedule_redis (String item_schedule_mapping_id,String itemId, String area_schedule_id, String date) {
        String data = POPConstants.avail_item_schedule_item_redis.replace("${ID}",item_schedule_mapping_id).replace("${ITEMID}",itemId).replace("${AREASCHEDULE}",area_schedule_id).replace("${DATE}",date);
        System.out.println("--AVAILABILITY_ITEM_SCHEDULE_ITEM_ID_"+itemId+ " data ::: "+data);
        redisHelper.setValue("sandredisstage",POPConstants.redis_index_0,POPConstants.AVAILABILITY_ITEM_SCHEDULE_ITEM_ID+itemId,data);
    }

    public void avail_item_schedule_redis_set_empty (String data, String itemId ) {
        System.out.println("--AVAILABILITY_ITEM_SCHEDULE_ITEM_ID_"+itemId+ " data ::: "+data);
        redisHelper.setValue("sandredisstage",POPConstants.redis_index_0,POPConstants.AVAILABILITY_ITEM_SCHEDULE_ITEM_ID+itemId,data);
    }

    public void avail_area_schedule_redis (String area_schedule_id, String day, String openTime, String closeTime, String areaId) {
        String data = POPConstants.avail_area_schedule_redis.replace("${ID}",area_schedule_id).replace("${DAY}",day).replace("${OPEN}",openTime).replace("${CLOSE}",closeTime).replace("${AREAID}",areaId);
        System.out.println("AVAILABILITY_AREA_SCHEDULE_"+area_schedule_id+" data ::: "+data);
        redisHelper.setValue("sandredisstage",POPConstants.redis_index_0,POPConstants.AVAILABILITY_AREA_SCHEDULE+area_schedule_id,data);
    }

    public void restaurant_pop_menu_redis (String restId, String menuId) {
        String data = menuId;
        System.out.println("restaurant_pop_menu_redis data ::: "+data);
        redisHelper.setValue("sandredisstage",POPConstants.redis_index_0,"RESTAURANT_"+restId+"_POP_MENU",data);
    }

    public void update_checkout_inventory_redis (String data, String itemScheduleMappingId) {
        System.out.println("INV_"+itemScheduleMappingId+" ::: "+data);
        redisHelper.setValue("checkoutredis",POPConstants.redis_index_0,"INV_"+itemScheduleMappingId,data);
    }

    public List<Map<String, Object>> getItemScheduleMappingDetailsDB(String itemId) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(POPConstants.cmsDB);
        String q = POPConstants.get_item_schedule_mapping+itemId;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public String getMenuIdFromItemIdDB (String itemId) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(POPConstants.cmsDB);
        String q = POPConstants.get_menu_id_from_item_menu_map+itemId;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size()>0) return list.get(0).get("menu_id").toString();
        else return "na";
    }


}
