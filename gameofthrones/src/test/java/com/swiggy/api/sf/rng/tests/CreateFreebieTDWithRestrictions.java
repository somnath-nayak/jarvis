package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.rng.pojo.Category;
import com.swiggy.api.sf.rng.pojo.CreateFlatTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateFlatTdEntry;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;
import com.swiggy.api.sf.rng.pojo.Menu;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.Slot;
import com.swiggy.api.sf.rng.pojo.SubCategory;
import com.swiggy.api.sf.rng.pojo.freebie.CreateFreebieTDEntry;
import com.swiggy.api.sf.rng.pojo.freebie.CreateFreebieTdBuilder;
import com.swiggy.api.sf.rng.pojo.freebie.FreebieRestaurantList;
import com.swiggy.api.sf.rng.pojo.freebie.FreebieSlot;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class CreateFreebieTDWithRestrictions {
	
	getObject go = new getObject();
	static 	int i =0;
	
	public String getFreebieTD(String restrictionType,String level) throws IOException
	{
		i++;
	    List<FreebieRestaurantList> restaurantList = new ArrayList<>();
	    JsonHelper jsonHelper = new JsonHelper();
	    CreateFreebieTDEntry freebieTD =null ;
	    switch(level)
		{
		case "Restaurant"  : restaurantList.add(go.getFreebieRestaurantObject());break;
		}
		
		
	
	{
		switch(restrictionType)
		{
		
		    case "SFO": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId()).firstOrderRestriction("true")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;

		    case "RFO": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId()).restaurantFirstOrder("true")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;


		    case "NR": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId())
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;


		    case "UR": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId()).userRestriction("true")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;
					
		    case "DU": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId()).dormant_user_type("THIRTY_DAYS_DORMANT")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;
			        
			        
		    case "TR": List<FreebieSlot> slots = new ArrayList<>();
                       slots.add(new FreebieSlot("2350", "ALL", "0005"));
		    	     freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList).timeSlotRestriction("true").slots(slots)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId()).dormant_user_type("ZERO_DAYS_DORMANT")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;

		}     
		      return   jsonHelper.getObjectToJSON(freebieTD);
	}
	}

	public String getFreebieTD(String restrictionType,String level, List<FreebieRestaurantList> restaurantList) throws IOException
	{
		i++;
	
	    JsonHelper jsonHelper = new JsonHelper();
	    CreateFreebieTDEntry freebieTD =null ;
		
	
	{
		switch(restrictionType)
		{
		
		    case "SFO": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId()).firstOrderRestriction("true")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;

		    case "RFO": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId()).restaurantFirstOrder("true")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;


		    case "NR": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId())
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;


		    case "UR": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId()).userRestriction("true")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;
					
		    case "DU": freebieTD = new CreateFreebieTdBuilder()
					.campaign_type("Freebie")
					.discountLevel(level)
					.restaurantList(restaurantList)
					.ruleDiscount("Freebie", level,"0",go.FrebieItemId()).dormant_user_type("THIRTY_DAYS_DORMANT")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			        .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			        .build();break;

		}     
		      return   jsonHelper.getObjectToJSON(freebieTD);
	}
	}

	
	
}
