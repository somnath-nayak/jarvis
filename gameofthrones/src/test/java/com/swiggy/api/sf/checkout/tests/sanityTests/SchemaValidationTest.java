package com.swiggy.api.sf.checkout.tests.sanityTests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.SchemaValidationConstants;
import com.swiggy.api.sf.checkout.dp.SchemaValidationDP;
import com.swiggy.api.sf.checkout.helper.*;
import com.swiggy.api.sf.checkout.helper.sanityHelper.SanityHelper;
import com.swiggy.api.sf.checkout.tests.SuperTest;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class SchemaValidationTest extends SchemaValidationDP {
    SanityHelper sanityHelper = new SanityHelper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    String tid;
    String token;
    CancellationFeeHelper cancellationFeeHelper = new CancellationFeeHelper();
    SuperTest superhelper= new SuperTest();
    SuperHelper superHelper=new SuperHelper();
    CheckoutHelper helper= new CheckoutHelper();
    WalletHelper walletHelper= new WalletHelper();
    CafeHelper cafeHelper=new CafeHelper();

    @BeforeClass()
    public void setUp(){
        HashMap<String, String> headers = sanityHelper.login(SchemaValidationConstants.MOBILE, SchemaValidationConstants.PASSWORD);
        tid = headers.get("tid");
        token = headers.get("token");
        String cartResponse= helper.invokeCreateCartAPI(tid, token, "625924","1","238").ResponseValidator.GetBodyAsText();
    }

    @Test(dataProvider = "updateCartJsonSchemaTestdata", groups = {"shaswat", "sanity"}, description = "Update Cart v2 and validate the Json Schema...")
    public void updateCartV2SchemaTest(String userAgent,
                                       String versionCode,
                                       String swuid,
                                       String restaurantId,
                                       boolean isCartItemRequired,
                                       int cartItemQuantity,
                                       boolean isAddonRequiredForCartItems,
                                       boolean isMealItemRequired,
                                       int mealItemQuantity,
                                       boolean isAddonRequiredForMealItems,
                                       String[] mealIds,
                                       String cartType,
                                       String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders(tid, token, userAgent, versionCode, swuid);
        Processor processor = sanityHelper.cartUpdate(headers,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test(dataProvider = "updateCartJsonSchemaTestdata", groups = {"shaswat", "sanity"}, description = "Get Cart v2 and validate the Json Schema...")
    public void getCartV2SchemaTest(String userAgent,
                                    String versionCode,
                                    String swuid,
                                    String restaurantId,
                                    boolean isCartItemRequired,
                                    int cartItemQuantity,
                                    boolean isAddonRequiredForCartItems,
                                    boolean isMealItemRequired,
                                    int mealItemQuantity,
                                    boolean isAddonRequiredForMealItems,
                                    String[] mealIds,
                                    String cartType,
                                    String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders(tid, token, userAgent, versionCode, swuid);
        Processor cartUpdate = sanityHelper.cartUpdate(headers,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
        Processor processor = sanityHelper.getCart(headers);
        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test(dataProvider = "checkTotalsJsonSchemaTestdata", groups = {"shaswat", "sanity"}, description = "Update Cart v2 and validate the Json Schema...")
    public void checkTotalsSchemaTest(String userAgent,
                                      String versionCode,
                                      String swuid,
                                      String restaurantId,
                                      boolean isCartItemRequired,
                                      int cartItemQuantity,
                                      boolean isAddonRequiredForCartItems,
                                      boolean isMealItemRequired,
                                      int mealItemQuantity,
                                      boolean isAddonRequiredForMealItems,
                                      String[] mealIds,
                                      String cartType,
                                      String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders("", "", userAgent, versionCode, swuid);
        Processor processor = sanityHelper.checkTotals(headers,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test(dataProvider = "checkBankListAllJsonSchemaTestdata", groups = {"arup", "sanity"}, description = "Update Cart v2 and validate the Json Schema...")
    public void checkBankListAllSchemaTest(String userAgent,
                                           String versionCode,
                                           String swuid,
                                           String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders(tid, token, userAgent, versionCode, swuid);
        Processor processor = sanityHelper.checkBankListAll(headers);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test(dataProvider = "checkApplyCouponJsonSchemaTestdata", groups = {"arup", "sanity"}, description = "Update Cart v2 and validate the Json Schema...")
    public void checkApplyCouponSchemaTest(String userAgent,
                                           String versionCode,
                                           String swuid,
                                           String couponCode,
                                           String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders(tid, token, userAgent, versionCode, swuid);
        Processor processor = sanityHelper.checkApplyCoupon(headers, couponCode);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test(dataProvider = "checkRemoveCouponJsonSchemaTestdata", groups = {"arup", "sanity"}, description = "Update Cart v2 and validate the Json Schema...", dependsOnMethods={"checkApplyCouponSchemaTest"})
    public void checkRemoveCouponSchemaTest(String userAgent,
                                            String versionCode,
                                            String swuid,
                                            String couponCode,
                                            String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders(tid, token, userAgent, versionCode, swuid);
        Processor processor = sanityHelper.checkRemoveCoupon(headers, couponCode);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test(dataProvider = "checkCardsJuspayJsonSchemaTestdata", groups = {"arup", "sanity"}, description = "Update Cart v2 and validate the Json Schema...")
    public void checkCardsJuspaySchemaTest(String userAgent,
                                           String versionCode,
                                           String swuid,
                                           String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders(tid, token, userAgent, versionCode, swuid);
        Processor processor = sanityHelper.checkCardsJuspay(headers);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test (dataProvider = "checkCancellationFeeRevertJsonSchemaTestdata", groups = {"arup", "sanity"}, description = "Schem Test for RevertCancellationFee API...")
    public void checkCancellationFeeRevertSchemaTest(String restaurantId, String jsonSchemaFilePath) throws IOException, ProcessingException{
        Processor processor = cancellationFeeHelper.getRevertCancellationFee(restaurantId);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);

    }

    @Test(dataProvider = "checkSettingsWalletEmailUpdateURLJsonSchemaTestdata", groups = {"arup", "sanity"}, description = "Update Cart v2 and validate the Json Schema...")
    public void checkSettingsWalletEmailUpdateURLSchemaTest(String userAgent,
                                                            String versionCode,
                                                            String swuid,
                                                            String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders(tid, token, userAgent, versionCode, swuid);
        Processor processor = sanityHelper.checkSettingsWalletEmailUpdateURL(headers);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test(dataProvider = "checkGetAllPaymentJsonSchemaTestdata", groups = {"arup", "sanity"}, description = "Update Cart v2 and validate the Json Schema...")
    public void checkGetAllPaymentSchemaTest(String userAgent,
                                             String versionCode,
                                             String swuid,
                                             String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders(tid, token, userAgent, versionCode, swuid);
        Processor processor = sanityHelper.checkGetAllPayment(headers);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test(dataProvider = "checkGetInventoryJsonSchemaTestdata", groups = {"arup", "sanity"}, description = "Update Cart v2 and validate the Json Schema...")
    public void checkGetInventorySchemaTest(String userAgent,
                                            String versionCode,
                                            String swuid,
                                            String jsonSchemaFilePath) throws IOException, ProcessingException {

        HashMap<String, String> headers = sanityHelper.setHeaders(tid, token, userAgent, versionCode, swuid);
        headers.put("Authorization", CheckoutConstants.authorization);
        Processor processor = sanityHelper.checkGetInventory(headers);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }

    @Test(dataProvider = "checkCafeUpdateCartJsonSchemaTestdata", groups = {"arup", "sanity"}, description = "Update Cafe Cart v2 and validate the Json Schema...")
    public void checkCafeUpdateCartSchemaTest(String userAgent,
                                              String versionCode,
                                              String swuid,
                                              String menu_item_id,
                                              String quantity,
                                              String restId,
                                              String cart_type,
                                              String jsonSchemaFilePath) throws IOException, ProcessingException {

        Processor processor = sanityHelper.checkCafeUpdateCart(tid,token, menu_item_id, quantity, restId, cart_type);

        sanityHelper.JSONSchemaValidator(processor, jsonSchemaFilePath);
    }
}
