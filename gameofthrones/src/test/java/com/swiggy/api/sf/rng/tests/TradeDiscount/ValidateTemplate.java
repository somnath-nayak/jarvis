package com.swiggy.api.sf.rng.tests.TradeDiscount;

import com.swiggy.api.sf.rng.dp.TradeDiscount.TemplateCreationDp;
import com.swiggy.api.sf.rng.helper.TDHelper.TDTemplateHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.mortbay.util.ajax.JSON;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.List;

public class ValidateTemplate extends TemplateCreationDp {
    SoftAssert softAssert = new SoftAssert();
    TDTemplateHelper tdTemplateHelper = new TDTemplateHelper();

    String statuscode_jsonpath = "$.statusCode";
    String response_data ="$.data";



    @Test(dataProvider = "validateTemplateDp",description = "Request with empty Template ID list ")
    public void  emptyTemplateId(List<Integer> templateids ) throws IOException {
        Processor response = tdTemplateHelper.validateTemplate(templateids);
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String responsedata= JSON.toString(response.ResponseValidator.GetNodeValueAsBool(response_data));

        softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
        softAssert.assertEquals(responsedata, "true", "Response Data is not TRUE");
        softAssert.assertAll();
    }


    @Test(dataProvider = "singleValidTemplateIdDp",description = "Request with one valid Template ID list ")
    public void  singleValidTemplateId(List<Integer> templateids ) throws IOException {
        Processor response = tdTemplateHelper.validateTemplate(templateids);
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String responsedata= JSON.toString(response.ResponseValidator.GetNodeValueAsBool(response_data));

        softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
        softAssert.assertEquals(responsedata, "true", "Response Data is not TRUE");
        softAssert.assertAll();
    }


    @Test(dataProvider = "multipleValidTemplateIdDp",description = "Request with multiple valid /invalid Template ID list ")
    public void  multipleValidTemplateId(List<Integer> templateids,String validate ) throws IOException {
        Processor response = tdTemplateHelper.validateTemplate(templateids);
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String responsedata= JSON.toString(response.ResponseValidator.GetNodeValueAsBool(response_data));

        if (validate== "valid"){
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "true", "Response Data is not TRUE");
        }
        else {
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "false", "Response Data is not FALSE");
        }


        softAssert.assertAll();
    }


    @Test(dataProvider = "percentageAndFlatTemplateCoexistenseDp",description = "Request with Flat and Percentage template co-existence should fail ")
    public void  percentageAndFlatTemplateCoexistense(List<Integer> templateids ) throws IOException {
        Processor response = tdTemplateHelper.validateTemplate(templateids);
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String responsedata= JSON.toString(response.ResponseValidator.GetNodeValueAsBool(response_data));
        softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
        softAssert.assertEquals(responsedata, "false", "Response Data is not TRUE");
        softAssert.assertAll();
    }



    @Test(dataProvider = "multipleValidTemplateWithoutOverLappingDp",description = "Request with multiple valid Template ID list  - > STATIC/SLIDING should not overlap and SUGGESTED should overlap")
    public void  multipleValidTemplateWithoutOverLapping(List<Integer> templateids,String validate ) throws IOException {
        Processor response = tdTemplateHelper.validateTemplate(templateids);
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String responsedata= JSON.toString(response.ResponseValidator.GetNodeValueAsBool(response_data));

        if (validate== "valid"){
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "true", "Response Data is not TRUE");
        }
        else {
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "false", "Response Data is not FALSE");
        }

        softAssert.assertAll();
    }


    @Test(dataProvider = "multipleValidTemplateWithOverLappingDp",description = "Request with multiple valid Template ID list  - > STATIC/SLIDING should  overlap")
    public void  multipleValidTemplateWithOverLapping(List<Integer> templateids,String validate ) throws IOException {
        Processor response = tdTemplateHelper.validateTemplate(templateids);
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String responsedata= JSON.toString(response.ResponseValidator.GetNodeValueAsBool(response_data));

        if (validate== "valid"){
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "true", "Response Data is not TRUE");
        }
        else {
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "false", "Response Data is not FALSE");
        }

        softAssert.assertAll();
    }




    @Test(dataProvider = "dormantUserTestDp",description = "Request with different DORMANT user cut and verify CO_EXISTENCE" )
    public void  dormantUserTest(List<Integer> templateids,String validate ) throws IOException {
        Processor response = tdTemplateHelper.validateTemplate(templateids);
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String responsedata= JSON.toString(response.ResponseValidator.GetNodeValueAsBool(response_data));

        if (validate== "valid"){
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "true", "Response Data is not TRUE");
        }
        else {
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "false", "Response Data is not FALSE");
        }

        softAssert.assertAll();
    }


    @Test(dataProvider = "dormantUserNullAndZeroDaysDp",description = "Request with DORMANT USER - > NULL & ZERO DAYS coexists ")
    public void  dormantUserNullAndZeroDaysValidation(List<Integer> templateids,String validate ) throws IOException {
        Processor response = tdTemplateHelper.validateTemplate(templateids);
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String responsedata= JSON.toString(response.ResponseValidator.GetNodeValueAsBool(response_data));

        if (validate== "valid"){
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "true", "Response Data is not TRUE");
        }
        else {
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "false", "Response Data is not FALSE");
        }

        softAssert.assertAll();
    }



    @Test(dataProvider = "firstOrderAndDormantUserDp",description = "Request with different DORMANT user cut, with FIRST ORDER and verify CO_EXISTENCE" )
    public void  firstOrderAndDormantUser(List<Integer> templateids,String validate ) throws IOException {
        Processor response = tdTemplateHelper.validateTemplate(templateids);
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String responsedata= JSON.toString(response.ResponseValidator.GetNodeValueAsBool(response_data));

        if (validate== "valid"){
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "true", "Response Data is not TRUE");
        }
        else {
            softAssert.assertEquals(response_statuscode, "1" , "Status Code is 1");
            softAssert.assertEquals(responsedata, "false", "Response Data is not FALSE");
        }

        softAssert.assertAll();
    }








}
