package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class Headers {
	@JsonProperty("versionCode")
	private Integer versionCode;
	@JsonProperty("userAgent")
	private String userAgent;

	@JsonProperty("versionCode")
	public Integer getVersionCode() {
	return versionCode;
	}

	@JsonProperty("versionCode")
	public void setVersionCode(Integer versionCode) {
	this.versionCode = versionCode;
	}

	public Headers withVersionCode(Integer versionCode) {
	this.versionCode = versionCode;
	return this;
	}

	@JsonProperty("userAgent")
	public String getUserAgent() {
	return userAgent;
	}

	@JsonProperty("userAgent")
	public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
	}

	public Headers withUserAgent(String userAgent) {
	this.userAgent = userAgent;
	return this;
	}
	public Headers setDefaultData() {
		return this.withUserAgent("ANDROID").withVersionCode(281);
	}
}
