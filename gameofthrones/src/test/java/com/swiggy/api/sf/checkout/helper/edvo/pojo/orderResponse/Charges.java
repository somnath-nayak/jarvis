package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "Vat",
        "Service Charges",
        "Service Tax",
        "Delivery Charges",
        "Packing Charges",
        "Convenience Fee",
        "Cancellation Fee",
        "GST"
})
public class Charges {

    @JsonProperty("Vat")
    private String vat;
    @JsonProperty("Service Charges")
    private String serviceCharges;
    @JsonProperty("Service Tax")
    private String serviceTax;
    @JsonProperty("Delivery Charges")
    private String deliveryCharges;
    @JsonProperty("Packing Charges")
    private String packingCharges;
    @JsonProperty("Convenience Fee")
    private String convenienceFee;
    @JsonProperty("Cancellation Fee")
    private String cancellationFee;
    @JsonProperty("GST")
    private String gST;

    /**
     * No args constructor for use in serialization
     *
     */
    public Charges() {
    }

    /**
     *
     * @param packingCharges
     * @param serviceCharges
     * @param serviceTax
     * @param deliveryCharges
     * @param vat
     * @param gST
     * @param cancellationFee
     * @param convenienceFee
     */
    public Charges(String vat, String serviceCharges, String serviceTax, String deliveryCharges, String packingCharges, String convenienceFee, String cancellationFee, String gST) {
        super();
        this.vat = vat;
        this.serviceCharges = serviceCharges;
        this.serviceTax = serviceTax;
        this.deliveryCharges = deliveryCharges;
        this.packingCharges = packingCharges;
        this.convenienceFee = convenienceFee;
        this.cancellationFee = cancellationFee;
        this.gST = gST;
    }

    @JsonProperty("Vat")
    public String getVat() {
        return vat;
    }

    @JsonProperty("Vat")
    public void setVat(String vat) {
        this.vat = vat;
    }

    @JsonProperty("Service Charges")
    public String getServiceCharges() {
        return serviceCharges;
    }

    @JsonProperty("Service Charges")
    public void setServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    @JsonProperty("Service Tax")
    public String getServiceTax() {
        return serviceTax;
    }

    @JsonProperty("Service Tax")
    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    @JsonProperty("Delivery Charges")
    public String getDeliveryCharges() {
        return deliveryCharges;
    }

    @JsonProperty("Delivery Charges")
    public void setDeliveryCharges(String deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    @JsonProperty("Packing Charges")
    public String getPackingCharges() {
        return packingCharges;
    }

    @JsonProperty("Packing Charges")
    public void setPackingCharges(String packingCharges) {
        this.packingCharges = packingCharges;
    }

    @JsonProperty("Convenience Fee")
    public String getConvenienceFee() {
        return convenienceFee;
    }

    @JsonProperty("Convenience Fee")
    public void setConvenienceFee(String convenienceFee) {
        this.convenienceFee = convenienceFee;
    }

    @JsonProperty("Cancellation Fee")
    public String getCancellationFee() {
        return cancellationFee;
    }

    @JsonProperty("Cancellation Fee")
    public void setCancellationFee(String cancellationFee) {
        this.cancellationFee = cancellationFee;
    }

    @JsonProperty("GST")
    public String getGST() {
        return gST;
    }

    @JsonProperty("GST")
    public void setGST(String gST) {
        this.gST = gST;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("vat", vat).append("serviceCharges", serviceCharges).append("serviceTax", serviceTax).append("deliveryCharges", deliveryCharges).append("packingCharges", packingCharges).append("convenienceFee", convenienceFee).append("cancellationFee", cancellationFee).append("gST", gST).toString();
    }

}