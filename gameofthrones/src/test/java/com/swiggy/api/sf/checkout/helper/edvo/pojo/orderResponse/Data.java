package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Configurations;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.RenderingDetail;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "sharedOrder",
        "order_id",
        "delivery_address",
        "order_items",
        "order_meals",
        "charges",
        "free_gifts",
        "is_coupon_applied",
        "coupon_applied",
        "order_time",
        "customer_id",
        "order_status",
        "post_status",
        "order_type",
        "post_type",
        "post_name",
        "order_placement_status",
        "billing_address_id",
        "sla_time",
        "restaurant_id",
        "restaurant_name",
        "restaurant_address",
        "restaurant_locality",
        "restaurant_type",
        "restaurant_coverage_area",
        "restaurant_city_code",
        "restaurant_city_name",
        "restaurant_area_code",
        "restaurant_cuisine",
        "payment_method",
        "order_delivery_status",
        "ordered_time_in_seconds",
        "delivered_time_in_seconds",
        "delivery_time_in_seconds",
        "order_total",
        "item_total",
        "original_order_total",
        "swiggy_money",
        "order_tax",
        "free_shipping",
        "order_discount",
        "coupon_discount",
        "trade_discount",
        "free_delivery_discount_hit",
        "has_rating",
        "order_spending",
        "order_incoming",
        "order_restaurant_bill",
        "order_notes",
        "restaurant_lat_lng",
        "restaurant_customer_distance",
        "pg_response_time",
        "converted_to_cod",
        "last_failed_order_id",
        "order_delivery_charge",
        "convenience_fee",
        "customer_user_agent",
        "overbooking",
        "payment_confirmation_channel",
        "billing_lat",
        "billing_lng",
        "payment_txn_id",
        "coupon_code",
        "with_de",
        "restaurant_new_slug",
        "distance_calc_method",
        "is_partner_enable",
        "partner_id",
        "type_of_partner",
        "prep_time",
        "restaurant_has_inventory",
        "order_payment_method",
        "pay_by_system_value",
        "de_pickedup_refund",
        "agreement_type",
        "is_ivr_enabled",
        "customer_ip",
        "is_refund_initiated",
        "restaurant_cover_image",
        "restaurant_area_name",
        "cust_lat_lng",
        "key",
        "is_assured",
        "delayed_placing",
        "cod_verification_threshold",
        "on_time",
        "sla_difference",
        "actual_sla_time",
        "payment_txn_status",
        "restaurant_taxation_type",
        "payment",
        "device_id",
        "swuid",
        "tid",
        "sid",
        "listing_version_shown",
        "is_replicated",
        "is_cancellable",
        "cancellation_fee_collected",
        "cancellation_fee_applied",
        "cancellation_fee_collected_total",
        "commission_on_full_bill",
        "is_select",
        "is_first_order_delivered",
        "first_order",
        "rendering_details",
        "mCancellationTime",
        "configurations",
        "success_message",
        "success_title",
        "threshold_fee",
        "distance_fee",
        "time_fee",
        "special_fee",
        "initiation_source"
})
public class Data {

    @JsonProperty("sharedOrder")
    private Boolean sharedOrder;
    @JsonProperty("order_id")
    private Long orderId;
    @JsonProperty("delivery_address")
    private DeliveryAddress deliveryAddress;
    @JsonProperty("order_items")
    private List<OrderItem> orderItems = null;
    @JsonProperty("order_meals")
    private List<OrderMeal> orderMeals = null;
    @JsonProperty("charges")
    private Charges charges;
    @JsonProperty("free_gifts")
    private List<Object> freeGifts = null;
    @JsonProperty("is_coupon_applied")
    private Boolean isCouponApplied;
    @JsonProperty("coupon_applied")
    private String couponApplied;
    @JsonProperty("order_time")
    private String orderTime;
    @JsonProperty("customer_id")
    private String customerId;
    @JsonProperty("order_status")
    private String orderStatus;
    @JsonProperty("post_status")
    private String postStatus;
    @JsonProperty("order_type")
    private String orderType;
    @JsonProperty("post_type")
    private String postType;
    @JsonProperty("post_name")
    private String postName;
    @JsonProperty("order_placement_status")
    private String orderPlacementStatus;
    @JsonProperty("billing_address_id")
    private String billingAddressId;
    @JsonProperty("sla_time")
    private String slaTime;
    @JsonProperty("restaurant_id")
    private String restaurantId;
    @JsonProperty("restaurant_name")
    private String restaurantName;
    @JsonProperty("restaurant_address")
    private String restaurantAddress;
    @JsonProperty("restaurant_locality")
    private String restaurantLocality;
    @JsonProperty("restaurant_type")
    private String restaurantType;
    @JsonProperty("restaurant_coverage_area")
    private String restaurantCoverageArea;
    @JsonProperty("restaurant_city_code")
    private String restaurantCityCode;
    @JsonProperty("restaurant_city_name")
    private String restaurantCityName;
    @JsonProperty("restaurant_area_code")
    private String restaurantAreaCode;
    @JsonProperty("restaurant_cuisine")
    private List<String> restaurantCuisine = null;
    @JsonProperty("payment_method")
    private String paymentMethod;
    @JsonProperty("order_delivery_status")
    private String orderDeliveryStatus;
    @JsonProperty("ordered_time_in_seconds")
    private Integer orderedTimeInSeconds;
    @JsonProperty("delivered_time_in_seconds")
    private String deliveredTimeInSeconds;
    @JsonProperty("delivery_time_in_seconds")
    private String deliveryTimeInSeconds;
    @JsonProperty("order_total")
    private Double orderTotal;
    @JsonProperty("item_total")
    private Double itemTotal;
    @JsonProperty("original_order_total")
    private Integer originalOrderTotal;
    @JsonProperty("swiggy_money")
    private Double swiggyMoney;
    @JsonProperty("order_tax")
    private Double orderTax;
    @JsonProperty("free_shipping")
    private String freeShipping;
    @JsonProperty("order_discount")
    private Integer orderDiscount;
    @JsonProperty("coupon_discount")
    private Double couponDiscount;
    @JsonProperty("trade_discount")
    private Double tradeDiscount;
    @JsonProperty("free_delivery_discount_hit")
    private Integer freeDeliveryDiscountHit;
    @JsonProperty("has_rating")
    private String hasRating;
    @JsonProperty("order_spending")
    private String orderSpending;
    @JsonProperty("order_incoming")
    private String orderIncoming;
    @JsonProperty("order_restaurant_bill")
    private String orderRestaurantBill;
    @JsonProperty("order_notes")
    private String orderNotes;
    @JsonProperty("restaurant_lat_lng")
    private String restaurantLatLng;
    @JsonProperty("restaurant_customer_distance")
    private String restaurantCustomerDistance;
    @JsonProperty("pg_response_time")
    private String pgResponseTime;
    @JsonProperty("converted_to_cod")
    private Boolean convertedToCod;
    @JsonProperty("last_failed_order_id")
    private Integer lastFailedOrderId;
    @JsonProperty("order_delivery_charge")
    private Integer orderDeliveryCharge;
    @JsonProperty("convenience_fee")
    private String convenienceFee;
    @JsonProperty("customer_user_agent")
    private String customerUserAgent;
    @JsonProperty("overbooking")
    private String overbooking;
    @JsonProperty("payment_confirmation_channel")
    private String paymentConfirmationChannel;
    @JsonProperty("billing_lat")
    private String billingLat;
    @JsonProperty("billing_lng")
    private String billingLng;
    @JsonProperty("payment_txn_id")
    private String paymentTxnId;
    @JsonProperty("coupon_code")
    private String couponCode;
    @JsonProperty("with_de")
    private Boolean withDe;
    @JsonProperty("restaurant_new_slug")
    private String restaurantNewSlug;
    @JsonProperty("distance_calc_method")
    private String distanceCalcMethod;
    @JsonProperty("is_partner_enable")
    private Boolean isPartnerEnable;
    @JsonProperty("partner_id")
    private String partnerId;
    @JsonProperty("type_of_partner")
    private Integer typeOfPartner;
    @JsonProperty("prep_time")
    private String prepTime;
    @JsonProperty("restaurant_has_inventory")
    private String restaurantHasInventory;
    @JsonProperty("order_payment_method")
    private String orderPaymentMethod;
    @JsonProperty("pay_by_system_value")
    private Boolean payBySystemValue;
    @JsonProperty("de_pickedup_refund")
    private Integer dePickedupRefund;
    @JsonProperty("agreement_type")
    private String agreementType;
    @JsonProperty("is_ivr_enabled")
    private String isIvrEnabled;
    @JsonProperty("customer_ip")
    private String customerIp;
    @JsonProperty("is_refund_initiated")
    private Integer isRefundInitiated;
    @JsonProperty("restaurant_cover_image")
    private String restaurantCoverImage;
    @JsonProperty("restaurant_area_name")
    private String restaurantAreaName;
    @JsonProperty("cust_lat_lng")
    private CustLatLng custLatLng;
    @JsonProperty("key")
    private String key;
    @JsonProperty("is_assured")
    private Integer isAssured;
    @JsonProperty("delayed_placing")
    private Integer delayedPlacing;
    @JsonProperty("cod_verification_threshold")
    private Integer codVerificationThreshold;
    @JsonProperty("on_time")
    private Boolean onTime;
    @JsonProperty("sla_difference")
    private String slaDifference;
    @JsonProperty("actual_sla_time")
    private String actualSlaTime;
    @JsonProperty("payment_txn_status")
    private String paymentTxnStatus;
    @JsonProperty("restaurant_taxation_type")
    private String restaurantTaxationType;
    @JsonProperty("payment")
    private String payment;
    @JsonProperty("device_id")
    private String deviceId;
    @JsonProperty("swuid")
    private String swuid;
    @JsonProperty("tid")
    private String tid;
    @JsonProperty("sid")
    private String sid;
    @JsonProperty("listing_version_shown")
    private String listingVersionShown;
    @JsonProperty("is_replicated")
    private Boolean isReplicated;
    @JsonProperty("is_cancellable")
    private Boolean isCancellable;
    @JsonProperty("cancellation_fee_collected")
    private Integer cancellationFeeCollected;
    @JsonProperty("cancellation_fee_applied")
    private Integer cancellationFeeApplied;
    @JsonProperty("cancellation_fee_collected_total")
    private Integer cancellationFeeCollectedTotal;
    @JsonProperty("commission_on_full_bill")
    private Boolean commissionOnFullBill;
    @JsonProperty("is_select")
    private Boolean isSelect;
    @JsonProperty("is_first_order_delivered")
    private Boolean isFirstOrderDelivered;
    @JsonProperty("first_order")
    private Boolean firstOrder;
    @JsonProperty("rendering_details")
    private List<RenderingDetail> renderingDetails = null;
    @JsonProperty("mCancellationTime")
    private Integer mCancellationTime;
    @JsonProperty("configurations")
    private Configurations configurations;
    @JsonProperty("success_message")
    private String successMessage;
    @JsonProperty("success_title")
    private String successTitle;
    @JsonProperty("threshold_fee")
    private Integer thresholdFee;
    @JsonProperty("distance_fee")
    private Integer distanceFee;
    @JsonProperty("time_fee")
    private Integer timeFee;
    @JsonProperty("special_fee")
    private Integer specialFee;
    @JsonProperty("initiation_source")
    private String initiationSource;

    @JsonProperty("initiation_source")
    public String getInitiationSource() {
        return initiationSource;
    }

    @JsonProperty("initiation_source")
    public void setInitiationSource(String initiationSource) {
        this.initiationSource = initiationSource;
    }

    @JsonProperty("sharedOrder")
    public Boolean getSharedOrder() {
        return sharedOrder;
    }

    @JsonProperty("sharedOrder")
    public void setSharedOrder(Boolean sharedOrder) {
        this.sharedOrder = sharedOrder;
    }

    @JsonProperty("order_id")
    public Long getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("delivery_address")
    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    @JsonProperty("delivery_address")
    public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    @JsonProperty("order_items")
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    @JsonProperty("order_items")
    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @JsonProperty("order_meals")
    public List<OrderMeal> getOrderMeals() {
        return orderMeals;
    }

    @JsonProperty("order_meals")
    public void setOrderMeals(List<OrderMeal> orderMeals) {
        this.orderMeals = orderMeals;
    }

    @JsonProperty("charges")
    public Charges getCharges() {
        return charges;
    }

    @JsonProperty("charges")
    public void setCharges(Charges charges) {
        this.charges = charges;
    }

    @JsonProperty("free_gifts")
    public List<Object> getFreeGifts() {
        return freeGifts;
    }

    @JsonProperty("free_gifts")
    public void setFreeGifts(List<Object> freeGifts) {
        this.freeGifts = freeGifts;
    }

    @JsonProperty("is_coupon_applied")
    public Boolean getIsCouponApplied() {
        return isCouponApplied;
    }

    @JsonProperty("is_coupon_applied")
    public void setIsCouponApplied(Boolean isCouponApplied) {
        this.isCouponApplied = isCouponApplied;
    }

    @JsonProperty("coupon_applied")
    public String getCouponApplied() {
        return couponApplied;
    }

    @JsonProperty("coupon_applied")
    public void setCouponApplied(String couponApplied) {
        this.couponApplied = couponApplied;
    }

    @JsonProperty("order_time")
    public String getOrderTime() {
        return orderTime;
    }

    @JsonProperty("order_time")
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    @JsonProperty("customer_id")
    public String getCustomerId() {
        return customerId;
    }

    @JsonProperty("customer_id")
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @JsonProperty("order_status")
    public String getOrderStatus() {
        return orderStatus;
    }

    @JsonProperty("order_status")
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @JsonProperty("post_status")
    public String getPostStatus() {
        return postStatus;
    }

    @JsonProperty("post_status")
    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    @JsonProperty("order_type")
    public String getOrderType() {
        return orderType;
    }

    @JsonProperty("order_type")
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @JsonProperty("post_type")
    public String getPostType() {
        return postType;
    }

    @JsonProperty("post_type")
    public void setPostType(String postType) {
        this.postType = postType;
    }

    @JsonProperty("post_name")
    public String getPostName() {
        return postName;
    }

    @JsonProperty("post_name")
    public void setPostName(String postName) {
        this.postName = postName;
    }

    @JsonProperty("order_placement_status")
    public String getOrderPlacementStatus() {
        return orderPlacementStatus;
    }

    @JsonProperty("order_placement_status")
    public void setOrderPlacementStatus(String orderPlacementStatus) {
        this.orderPlacementStatus = orderPlacementStatus;
    }

    @JsonProperty("billing_address_id")
    public String getBillingAddressId() {
        return billingAddressId;
    }

    @JsonProperty("billing_address_id")
    public void setBillingAddressId(String billingAddressId) {
        this.billingAddressId = billingAddressId;
    }

    @JsonProperty("sla_time")
    public String getSlaTime() {
        return slaTime;
    }

    @JsonProperty("sla_time")
    public void setSlaTime(String slaTime) {
        this.slaTime = slaTime;
    }

    @JsonProperty("restaurant_id")
    public String getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurant_id")
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    @JsonProperty("restaurant_name")
    public String getRestaurantName() {
        return restaurantName;
    }

    @JsonProperty("restaurant_name")
    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    @JsonProperty("restaurant_address")
    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    @JsonProperty("restaurant_address")
    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    @JsonProperty("restaurant_locality")
    public String getRestaurantLocality() {
        return restaurantLocality;
    }

    @JsonProperty("restaurant_locality")
    public void setRestaurantLocality(String restaurantLocality) {
        this.restaurantLocality = restaurantLocality;
    }

    @JsonProperty("restaurant_type")
    public String getRestaurantType() {
        return restaurantType;
    }

    @JsonProperty("restaurant_type")
    public void setRestaurantType(String restaurantType) {
        this.restaurantType = restaurantType;
    }

    @JsonProperty("restaurant_coverage_area")
    public String getRestaurantCoverageArea() {
        return restaurantCoverageArea;
    }

    @JsonProperty("restaurant_coverage_area")
    public void setRestaurantCoverageArea(String restaurantCoverageArea) {
        this.restaurantCoverageArea = restaurantCoverageArea;
    }

    @JsonProperty("restaurant_city_code")
    public String getRestaurantCityCode() {
        return restaurantCityCode;
    }

    @JsonProperty("restaurant_city_code")
    public void setRestaurantCityCode(String restaurantCityCode) {
        this.restaurantCityCode = restaurantCityCode;
    }

    @JsonProperty("restaurant_city_name")
    public String getRestaurantCityName() {
        return restaurantCityName;
    }

    @JsonProperty("restaurant_city_name")
    public void setRestaurantCityName(String restaurantCityName) {
        this.restaurantCityName = restaurantCityName;
    }

    @JsonProperty("restaurant_area_code")
    public String getRestaurantAreaCode() {
        return restaurantAreaCode;
    }

    @JsonProperty("restaurant_area_code")
    public void setRestaurantAreaCode(String restaurantAreaCode) {
        this.restaurantAreaCode = restaurantAreaCode;
    }

    @JsonProperty("restaurant_cuisine")
    public List<String> getRestaurantCuisine() {
        return restaurantCuisine;
    }

    @JsonProperty("restaurant_cuisine")
    public void setRestaurantCuisine(List<String> restaurantCuisine) {
        this.restaurantCuisine = restaurantCuisine;
    }

    @JsonProperty("payment_method")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    @JsonProperty("payment_method")
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @JsonProperty("order_delivery_status")
    public String getOrderDeliveryStatus() {
        return orderDeliveryStatus;
    }

    @JsonProperty("order_delivery_status")
    public void setOrderDeliveryStatus(String orderDeliveryStatus) {
        this.orderDeliveryStatus = orderDeliveryStatus;
    }

    @JsonProperty("ordered_time_in_seconds")
    public Integer getOrderedTimeInSeconds() {
        return orderedTimeInSeconds;
    }

    @JsonProperty("ordered_time_in_seconds")
    public void setOrderedTimeInSeconds(Integer orderedTimeInSeconds) {
        this.orderedTimeInSeconds = orderedTimeInSeconds;
    }

    @JsonProperty("delivered_time_in_seconds")
    public String getDeliveredTimeInSeconds() {
        return deliveredTimeInSeconds;
    }

    @JsonProperty("delivered_time_in_seconds")
    public void setDeliveredTimeInSeconds(String deliveredTimeInSeconds) {
        this.deliveredTimeInSeconds = deliveredTimeInSeconds;
    }

    @JsonProperty("delivery_time_in_seconds")
    public String getDeliveryTimeInSeconds() {
        return deliveryTimeInSeconds;
    }

    @JsonProperty("delivery_time_in_seconds")
    public void setDeliveryTimeInSeconds(String deliveryTimeInSeconds) {
        this.deliveryTimeInSeconds = deliveryTimeInSeconds;
    }

    @JsonProperty("order_total")
    public Double getOrderTotal() {
        return orderTotal;
    }

    @JsonProperty("order_total")
    public void setOrderTotal(Double orderTotal) {
        this.orderTotal = orderTotal;
    }

    @JsonProperty("item_total")
    public Double getItemTotal() {
        return itemTotal;
    }

    @JsonProperty("item_total")
    public void setItemTotal(Double itemTotal) {
        this.itemTotal = itemTotal;
    }

    @JsonProperty("original_order_total")
    public Integer getOriginalOrderTotal() {
        return originalOrderTotal;
    }

    @JsonProperty("original_order_total")
    public void setOriginalOrderTotal(Integer originalOrderTotal) {
        this.originalOrderTotal = originalOrderTotal;
    }

    @JsonProperty("swiggy_money")
    public Double getSwiggyMoney() {
        return swiggyMoney;
    }

    @JsonProperty("swiggy_money")
    public void setSwiggyMoney(Double swiggyMoney) {
        this.swiggyMoney = swiggyMoney;
    }

    @JsonProperty("order_tax")
    public Double getOrderTax() {
        return orderTax;
    }

    @JsonProperty("order_tax")
    public void setOrderTax(Double orderTax) {
        this.orderTax = orderTax;
    }

    @JsonProperty("free_shipping")
    public String getFreeShipping() {
        return freeShipping;
    }

    @JsonProperty("free_shipping")
    public void setFreeShipping(String freeShipping) {
        this.freeShipping = freeShipping;
    }

    @JsonProperty("order_discount")
    public Integer getOrderDiscount() {
        return orderDiscount;
    }

    @JsonProperty("order_discount")
    public void setOrderDiscount(Integer orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    @JsonProperty("coupon_discount")
    public Double getCouponDiscount() {
        return couponDiscount;
    }

    @JsonProperty("coupon_discount")
    public void setCouponDiscount(Double couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    @JsonProperty("trade_discount")
    public Double getTradeDiscount() {
        return tradeDiscount;
    }

    @JsonProperty("trade_discount")
    public void setTradeDiscount(Double tradeDiscount) {
        this.tradeDiscount = tradeDiscount;
    }

    @JsonProperty("free_delivery_discount_hit")
    public Integer getFreeDeliveryDiscountHit() {
        return freeDeliveryDiscountHit;
    }

    @JsonProperty("free_delivery_discount_hit")
    public void setFreeDeliveryDiscountHit(Integer freeDeliveryDiscountHit) {
        this.freeDeliveryDiscountHit = freeDeliveryDiscountHit;
    }

    @JsonProperty("has_rating")
    public String getHasRating() {
        return hasRating;
    }

    @JsonProperty("has_rating")
    public void setHasRating(String hasRating) {
        this.hasRating = hasRating;
    }

    @JsonProperty("order_spending")
    public String getOrderSpending() {
        return orderSpending;
    }

    @JsonProperty("order_spending")
    public void setOrderSpending(String orderSpending) {
        this.orderSpending = orderSpending;
    }

    @JsonProperty("order_incoming")
    public String getOrderIncoming() {
        return orderIncoming;
    }

    @JsonProperty("order_incoming")
    public void setOrderIncoming(String orderIncoming) {
        this.orderIncoming = orderIncoming;
    }

    @JsonProperty("order_restaurant_bill")
    public String getOrderRestaurantBill() {
        return orderRestaurantBill;
    }

    @JsonProperty("order_restaurant_bill")
    public void setOrderRestaurantBill(String orderRestaurantBill) {
        this.orderRestaurantBill = orderRestaurantBill;
    }

    @JsonProperty("order_notes")
    public String getOrderNotes() {
        return orderNotes;
    }

    @JsonProperty("order_notes")
    public void setOrderNotes(String orderNotes) {
        this.orderNotes = orderNotes;
    }

    @JsonProperty("restaurant_lat_lng")
    public String getRestaurantLatLng() {
        return restaurantLatLng;
    }

    @JsonProperty("restaurant_lat_lng")
    public void setRestaurantLatLng(String restaurantLatLng) {
        this.restaurantLatLng = restaurantLatLng;
    }

    @JsonProperty("restaurant_customer_distance")
    public String getRestaurantCustomerDistance() {
        return restaurantCustomerDistance;
    }

    @JsonProperty("restaurant_customer_distance")
    public void setRestaurantCustomerDistance(String restaurantCustomerDistance) {
        this.restaurantCustomerDistance = restaurantCustomerDistance;
    }

    @JsonProperty("pg_response_time")
    public String getPgResponseTime() {
        return pgResponseTime;
    }

    @JsonProperty("pg_response_time")
    public void setPgResponseTime(String pgResponseTime) {
        this.pgResponseTime = pgResponseTime;
    }

    @JsonProperty("converted_to_cod")
    public Boolean getConvertedToCod() {
        return convertedToCod;
    }

    @JsonProperty("converted_to_cod")
    public void setConvertedToCod(Boolean convertedToCod) {
        this.convertedToCod = convertedToCod;
    }

    @JsonProperty("last_failed_order_id")
    public Integer getLastFailedOrderId() {
        return lastFailedOrderId;
    }

    @JsonProperty("last_failed_order_id")
    public void setLastFailedOrderId(Integer lastFailedOrderId) {
        this.lastFailedOrderId = lastFailedOrderId;
    }

    @JsonProperty("order_delivery_charge")
    public Integer getOrderDeliveryCharge() {
        return orderDeliveryCharge;
    }

    @JsonProperty("order_delivery_charge")
    public void setOrderDeliveryCharge(Integer orderDeliveryCharge) {
        this.orderDeliveryCharge = orderDeliveryCharge;
    }

    @JsonProperty("convenience_fee")
    public String getConvenienceFee() {
        return convenienceFee;
    }

    @JsonProperty("convenience_fee")
    public void setConvenienceFee(String convenienceFee) {
        this.convenienceFee = convenienceFee;
    }

    @JsonProperty("customer_user_agent")
    public String getCustomerUserAgent() {
        return customerUserAgent;
    }

    @JsonProperty("customer_user_agent")
    public void setCustomerUserAgent(String customerUserAgent) {
        this.customerUserAgent = customerUserAgent;
    }

    @JsonProperty("overbooking")
    public String getOverbooking() {
        return overbooking;
    }

    @JsonProperty("overbooking")
    public void setOverbooking(String overbooking) {
        this.overbooking = overbooking;
    }

    @JsonProperty("payment_confirmation_channel")
    public String getPaymentConfirmationChannel() {
        return paymentConfirmationChannel;
    }

    @JsonProperty("payment_confirmation_channel")
    public void setPaymentConfirmationChannel(String paymentConfirmationChannel) {
        this.paymentConfirmationChannel = paymentConfirmationChannel;
    }

    @JsonProperty("billing_lat")
    public String getBillingLat() {
        return billingLat;
    }

    @JsonProperty("billing_lat")
    public void setBillingLat(String billingLat) {
        this.billingLat = billingLat;
    }

    @JsonProperty("billing_lng")
    public String getBillingLng() {
        return billingLng;
    }

    @JsonProperty("billing_lng")
    public void setBillingLng(String billingLng) {
        this.billingLng = billingLng;
    }

    @JsonProperty("payment_txn_id")
    public String getPaymentTxnId() {
        return paymentTxnId;
    }

    @JsonProperty("payment_txn_id")
    public void setPaymentTxnId(String paymentTxnId) {
        this.paymentTxnId = paymentTxnId;
    }

    @JsonProperty("coupon_code")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("coupon_code")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    @JsonProperty("with_de")
    public Boolean getWithDe() {
        return withDe;
    }

    @JsonProperty("with_de")
    public void setWithDe(Boolean withDe) {
        this.withDe = withDe;
    }

    @JsonProperty("restaurant_new_slug")
    public String getRestaurantNewSlug() {
        return restaurantNewSlug;
    }

    @JsonProperty("restaurant_new_slug")
    public void setRestaurantNewSlug(String restaurantNewSlug) {
        this.restaurantNewSlug = restaurantNewSlug;
    }

    @JsonProperty("distance_calc_method")
    public String getDistanceCalcMethod() {
        return distanceCalcMethod;
    }

    @JsonProperty("distance_calc_method")
    public void setDistanceCalcMethod(String distanceCalcMethod) {
        this.distanceCalcMethod = distanceCalcMethod;
    }

    @JsonProperty("is_partner_enable")
    public Boolean getIsPartnerEnable() {
        return isPartnerEnable;
    }

    @JsonProperty("is_partner_enable")
    public void setIsPartnerEnable(Boolean isPartnerEnable) {
        this.isPartnerEnable = isPartnerEnable;
    }

    @JsonProperty("partner_id")
    public String getPartnerId() {
        return partnerId;
    }

    @JsonProperty("partner_id")
    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    @JsonProperty("type_of_partner")
    public Integer getTypeOfPartner() {
        return typeOfPartner;
    }

    @JsonProperty("type_of_partner")
    public void setTypeOfPartner(Integer typeOfPartner) {
        this.typeOfPartner = typeOfPartner;
    }

    @JsonProperty("prep_time")
    public String getPrepTime() {
        return prepTime;
    }

    @JsonProperty("prep_time")
    public void setPrepTime(String prepTime) {
        this.prepTime = prepTime;
    }

    @JsonProperty("restaurant_has_inventory")
    public String getRestaurantHasInventory() {
        return restaurantHasInventory;
    }

    @JsonProperty("restaurant_has_inventory")
    public void setRestaurantHasInventory(String restaurantHasInventory) {
        this.restaurantHasInventory = restaurantHasInventory;
    }

    @JsonProperty("order_payment_method")
    public String getOrderPaymentMethod() {
        return orderPaymentMethod;
    }

    @JsonProperty("order_payment_method")
    public void setOrderPaymentMethod(String orderPaymentMethod) {
        this.orderPaymentMethod = orderPaymentMethod;
    }

    @JsonProperty("pay_by_system_value")
    public Boolean getPayBySystemValue() {
        return payBySystemValue;
    }

    @JsonProperty("pay_by_system_value")
    public void setPayBySystemValue(Boolean payBySystemValue) {
        this.payBySystemValue = payBySystemValue;
    }

    @JsonProperty("de_pickedup_refund")
    public Integer getDePickedupRefund() {
        return dePickedupRefund;
    }

    @JsonProperty("de_pickedup_refund")
    public void setDePickedupRefund(Integer dePickedupRefund) {
        this.dePickedupRefund = dePickedupRefund;
    }

    @JsonProperty("agreement_type")
    public String getAgreementType() {
        return agreementType;
    }

    @JsonProperty("agreement_type")
    public void setAgreementType(String agreementType) {
        this.agreementType = agreementType;
    }

    @JsonProperty("is_ivr_enabled")
    public String getIsIvrEnabled() {
        return isIvrEnabled;
    }

    @JsonProperty("is_ivr_enabled")
    public void setIsIvrEnabled(String isIvrEnabled) {
        this.isIvrEnabled = isIvrEnabled;
    }

    @JsonProperty("customer_ip")
    public String getCustomerIp() {
        return customerIp;
    }

    @JsonProperty("customer_ip")
    public void setCustomerIp(String customerIp) {
        this.customerIp = customerIp;
    }

    @JsonProperty("is_refund_initiated")
    public Integer getIsRefundInitiated() {
        return isRefundInitiated;
    }

    @JsonProperty("is_refund_initiated")
    public void setIsRefundInitiated(Integer isRefundInitiated) {
        this.isRefundInitiated = isRefundInitiated;
    }

    @JsonProperty("restaurant_cover_image")
    public String getRestaurantCoverImage() {
        return restaurantCoverImage;
    }

    @JsonProperty("restaurant_cover_image")
    public void setRestaurantCoverImage(String restaurantCoverImage) {
        this.restaurantCoverImage = restaurantCoverImage;
    }

    @JsonProperty("restaurant_area_name")
    public String getRestaurantAreaName() {
        return restaurantAreaName;
    }

    @JsonProperty("restaurant_area_name")
    public void setRestaurantAreaName(String restaurantAreaName) {
        this.restaurantAreaName = restaurantAreaName;
    }

    @JsonProperty("cust_lat_lng")
    public CustLatLng getCustLatLng() {
        return custLatLng;
    }

    @JsonProperty("cust_lat_lng")
    public void setCustLatLng(CustLatLng custLatLng) {
        this.custLatLng = custLatLng;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("is_assured")
    public Integer getIsAssured() {
        return isAssured;
    }

    @JsonProperty("is_assured")
    public void setIsAssured(Integer isAssured) {
        this.isAssured = isAssured;
    }

    @JsonProperty("delayed_placing")
    public Integer getDelayedPlacing() {
        return delayedPlacing;
    }

    @JsonProperty("delayed_placing")
    public void setDelayedPlacing(Integer delayedPlacing) {
        this.delayedPlacing = delayedPlacing;
    }

    @JsonProperty("cod_verification_threshold")
    public Integer getCodVerificationThreshold() {
        return codVerificationThreshold;
    }

    @JsonProperty("cod_verification_threshold")
    public void setCodVerificationThreshold(Integer codVerificationThreshold) {
        this.codVerificationThreshold = codVerificationThreshold;
    }

    @JsonProperty("on_time")
    public Boolean getOnTime() {
        return onTime;
    }

    @JsonProperty("on_time")
    public void setOnTime(Boolean onTime) {
        this.onTime = onTime;
    }

    @JsonProperty("sla_difference")
    public String getSlaDifference() {
        return slaDifference;
    }

    @JsonProperty("sla_difference")
    public void setSlaDifference(String slaDifference) {
        this.slaDifference = slaDifference;
    }

    @JsonProperty("actual_sla_time")
    public String getActualSlaTime() {
        return actualSlaTime;
    }

    @JsonProperty("actual_sla_time")
    public void setActualSlaTime(String actualSlaTime) {
        this.actualSlaTime = actualSlaTime;
    }

    @JsonProperty("payment_txn_status")
    public String getPaymentTxnStatus() {
        return paymentTxnStatus;
    }

    @JsonProperty("payment_txn_status")
    public void setPaymentTxnStatus(String paymentTxnStatus) {
        this.paymentTxnStatus = paymentTxnStatus;
    }

    @JsonProperty("restaurant_taxation_type")
    public String getRestaurantTaxationType() {
        return restaurantTaxationType;
    }

    @JsonProperty("restaurant_taxation_type")
    public void setRestaurantTaxationType(String restaurantTaxationType) {
        this.restaurantTaxationType = restaurantTaxationType;
    }

    @JsonProperty("payment")
    public String getPayment() {
        return payment;
    }

    @JsonProperty("payment")
    public void setPayment(String payment) {
        this.payment = payment;
    }

    @JsonProperty("device_id")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty("device_id")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("swuid")
    public String getSwuid() {
        return swuid;
    }

    @JsonProperty("swuid")
    public void setSwuid(String swuid) {
        this.swuid = swuid;
    }

    @JsonProperty("tid")
    public String getTid() {
        return tid;
    }

    @JsonProperty("tid")
    public void setTid(String tid) {
        this.tid = tid;
    }

    @JsonProperty("sid")
    public String getSid() {
        return sid;
    }

    @JsonProperty("sid")
    public void setSid(String sid) {
        this.sid = sid;
    }

    @JsonProperty("listing_version_shown")
    public String getListingVersionShown() {
        return listingVersionShown;
    }

    @JsonProperty("listing_version_shown")
    public void setListingVersionShown(String listingVersionShown) {
        this.listingVersionShown = listingVersionShown;
    }

    @JsonProperty("is_replicated")
    public Boolean getIsReplicated() {
        return isReplicated;
    }

    @JsonProperty("is_replicated")
    public void setIsReplicated(Boolean isReplicated) {
        this.isReplicated = isReplicated;
    }

    @JsonProperty("is_cancellable")
    public Boolean getIsCancellable() {
        return isCancellable;
    }

    @JsonProperty("is_cancellable")
    public void setIsCancellable(Boolean isCancellable) {
        this.isCancellable = isCancellable;
    }

    @JsonProperty("cancellation_fee_collected")
    public Integer getCancellationFeeCollected() {
        return cancellationFeeCollected;
    }

    @JsonProperty("cancellation_fee_collected")
    public void setCancellationFeeCollected(Integer cancellationFeeCollected) {
        this.cancellationFeeCollected = cancellationFeeCollected;
    }

    @JsonProperty("cancellation_fee_applied")
    public Integer getCancellationFeeApplied() {
        return cancellationFeeApplied;
    }

    @JsonProperty("cancellation_fee_applied")
    public void setCancellationFeeApplied(Integer cancellationFeeApplied) {
        this.cancellationFeeApplied = cancellationFeeApplied;
    }

    @JsonProperty("cancellation_fee_collected_total")
    public Integer getCancellationFeeCollectedTotal() {
        return cancellationFeeCollectedTotal;
    }

    @JsonProperty("cancellation_fee_collected_total")
    public void setCancellationFeeCollectedTotal(Integer cancellationFeeCollectedTotal) {
        this.cancellationFeeCollectedTotal = cancellationFeeCollectedTotal;
    }

    @JsonProperty("commission_on_full_bill")
    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    @JsonProperty("commission_on_full_bill")
    public void setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
    }

    @JsonProperty("is_select")
    public Boolean getIsSelect() {
        return isSelect;
    }

    @JsonProperty("is_select")
    public void setIsSelect(Boolean isSelect) {
        this.isSelect = isSelect;
    }

    @JsonProperty("is_first_order_delivered")
    public Boolean getIsFirstOrderDelivered() {
        return isFirstOrderDelivered;
    }

    @JsonProperty("is_first_order_delivered")
    public void setIsFirstOrderDelivered(Boolean isFirstOrderDelivered) {
        this.isFirstOrderDelivered = isFirstOrderDelivered;
    }

    @JsonProperty("first_order")
    public Boolean getFirstOrder() {
        return firstOrder;
    }

    @JsonProperty("first_order")
    public void setFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
    }

    @JsonProperty("rendering_details")
    public List<RenderingDetail> getRenderingDetails() {
        return renderingDetails;
    }

    @JsonProperty("rendering_details")
    public void setRenderingDetails(List<RenderingDetail> renderingDetails) {
        this.renderingDetails = renderingDetails;
    }

    @JsonProperty("mCancellationTime")
    public Integer getMCancellationTime() {
        return mCancellationTime;
    }

    @JsonProperty("mCancellationTime")
    public void setMCancellationTime(Integer mCancellationTime) {
        this.mCancellationTime = mCancellationTime;
    }

    @JsonProperty("configurations")
    public Configurations getConfigurations() {
        return configurations;
    }

    @JsonProperty("configurations")
    public void setConfigurations(Configurations configurations) {
        this.configurations = configurations;
    }

    @JsonProperty("success_message")
    public String getSuccessMessage() {
        return successMessage;
    }

    @JsonProperty("success_message")
    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    @JsonProperty("success_title")
    public String getSuccessTitle() {
        return successTitle;
    }

    @JsonProperty("success_title")
    public void setSuccessTitle(String successTitle) {
        this.successTitle = successTitle;
    }

    @JsonProperty("threshold_fee")
    public Integer getThresholdFee() {
        return thresholdFee;
    }

    @JsonProperty("threshold_fee")
    public void setThresholdFee(Integer thresholdFee) {
        this.thresholdFee = thresholdFee;
    }

    @JsonProperty("distance_fee")
    public Integer getDistanceFee() {
        return distanceFee;
    }

    @JsonProperty("distance_fee")
    public void setDistanceFee(Integer distanceFee) {
        this.distanceFee = distanceFee;
    }

    @JsonProperty("time_fee")
    public Integer getTimeFee() {
        return timeFee;
    }

    @JsonProperty("time_fee")
    public void setTimeFee(Integer timeFee) {
        this.timeFee = timeFee;
    }

    @JsonProperty("special_fee")
    public Integer getSpecialFee() {
        return specialFee;
    }

    @JsonProperty("special_fee")
    public void setSpecialFee(Integer specialFee) {
        this.specialFee = specialFee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sharedOrder", sharedOrder).append("orderId", orderId).append("deliveryAddress", deliveryAddress).append("orderItems", orderItems).append("orderMeals", orderMeals).append("charges", charges).append("freeGifts", freeGifts).append("isCouponApplied", isCouponApplied).append("couponApplied", couponApplied).append("orderTime", orderTime).append("customerId", customerId).append("orderStatus", orderStatus).append("postStatus", postStatus).append("orderType", orderType).append("postType", postType).append("postName", postName).append("orderPlacementStatus", orderPlacementStatus).append("billingAddressId", billingAddressId).append("slaTime", slaTime).append("restaurantId", restaurantId).append("restaurantName", restaurantName).append("restaurantAddress", restaurantAddress).append("restaurantLocality", restaurantLocality).append("restaurantType", restaurantType).append("restaurantCoverageArea", restaurantCoverageArea).append("restaurantCityCode", restaurantCityCode).append("restaurantCityName", restaurantCityName).append("restaurantAreaCode", restaurantAreaCode).append("restaurantCuisine", restaurantCuisine).append("paymentMethod", paymentMethod).append("orderDeliveryStatus", orderDeliveryStatus).append("orderedTimeInSeconds", orderedTimeInSeconds).append("deliveredTimeInSeconds", deliveredTimeInSeconds).append("deliveryTimeInSeconds", deliveryTimeInSeconds).append("orderTotal", orderTotal).append("itemTotal", itemTotal).append("originalOrderTotal", originalOrderTotal).append("swiggyMoney", swiggyMoney).append("orderTax", orderTax).append("freeShipping", freeShipping).append("orderDiscount", orderDiscount).append("couponDiscount", couponDiscount).append("tradeDiscount", tradeDiscount).append("freeDeliveryDiscountHit", freeDeliveryDiscountHit).append("hasRating", hasRating).append("orderSpending", orderSpending).append("orderIncoming", orderIncoming).append("orderRestaurantBill", orderRestaurantBill).append("orderNotes", orderNotes).append("restaurantLatLng", restaurantLatLng).append("restaurantCustomerDistance", restaurantCustomerDistance).append("pgResponseTime", pgResponseTime).append("convertedToCod", convertedToCod).append("lastFailedOrderId", lastFailedOrderId).append("orderDeliveryCharge", orderDeliveryCharge).append("convenienceFee", convenienceFee).append("customerUserAgent", customerUserAgent).append("overbooking", overbooking).append("paymentConfirmationChannel", paymentConfirmationChannel).append("billingLat", billingLat).append("billingLng", billingLng).append("paymentTxnId", paymentTxnId).append("couponCode", couponCode).append("withDe", withDe).append("restaurantNewSlug", restaurantNewSlug).append("distanceCalcMethod", distanceCalcMethod).append("isPartnerEnable", isPartnerEnable).append("partnerId", partnerId).append("typeOfPartner", typeOfPartner).append("prepTime", prepTime).append("restaurantHasInventory", restaurantHasInventory).append("orderPaymentMethod", orderPaymentMethod).append("payBySystemValue", payBySystemValue).append("dePickedupRefund", dePickedupRefund).append("agreementType", agreementType).append("isIvrEnabled", isIvrEnabled).append("customerIp", customerIp).append("isRefundInitiated", isRefundInitiated).append("restaurantCoverImage", restaurantCoverImage).append("restaurantAreaName", restaurantAreaName).append("custLatLng", custLatLng).append("key", key).append("isAssured", isAssured).append("delayedPlacing", delayedPlacing).append("codVerificationThreshold", codVerificationThreshold).append("onTime", onTime).append("slaDifference", slaDifference).append("actualSlaTime", actualSlaTime).append("paymentTxnStatus", paymentTxnStatus).append("restaurantTaxationType", restaurantTaxationType).append("payment", payment).append("deviceId", deviceId).append("swuid", swuid).append("tid", tid).append("sid", sid).append("listingVersionShown", listingVersionShown).append("isReplicated", isReplicated).append("isCancellable", isCancellable).append("cancellationFeeCollected", cancellationFeeCollected).append("cancellationFeeApplied", cancellationFeeApplied).append("cancellationFeeCollectedTotal", cancellationFeeCollectedTotal).append("commissionOnFullBill", commissionOnFullBill).append("isSelect", isSelect).append("isFirstOrderDelivered", isFirstOrderDelivered).append("firstOrder", firstOrder).append("renderingDetails", renderingDetails).append("mCancellationTime", mCancellationTime).append("configurations", configurations).append("successMessage", successMessage).append("successTitle", successTitle).append("thresholdFee", thresholdFee).append("distanceFee", distanceFee).append("timeFee", timeFee).append("specialFee", specialFee).toString();
    }

}
