package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "plan_id",
        "quantity",
})
public class SubscriptionItem {

	@JsonProperty("plan_id")
    private Integer plan_id;
    @JsonProperty("quantity")
    private Integer quantity;

    /**
     * No args constructor for use in serialization
     *
     */
    public SubscriptionItem() {
    }

    /**
     *
     * @param plan_id
     * @param quantity
     */
    public SubscriptionItem(Integer plan_id, Integer quantity) {
        super();
        this.plan_id = plan_id;
        this.quantity = quantity;
    }

    @JsonProperty("plan_id")
    public Integer getplan_id() {
        return plan_id;
    }

    @JsonProperty("plan_id")
    public void setplan_id(Integer plan_id) {
        this.plan_id = plan_id;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public SubscriptionItem withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("plan_id", plan_id)
        		.append("quantity", quantity).toString();
    }

}