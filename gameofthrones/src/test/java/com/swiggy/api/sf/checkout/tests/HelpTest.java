package com.swiggy.api.sf.checkout.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.dp.HelpDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.api.sf.checkout.helper.HelpHelper;

import java.io.IOException;
import java.util.HashMap;

public class HelpTest extends HelpDP {
    Initialize gameofthrones = new Initialize();
    CheckoutHelper checkoutHelper= new CheckoutHelper();
    JsonHelper jsonHelper= new JsonHelper();
    HelpHelper helper=new HelpHelper();
    DeliveryServiceHelper deliveryServiceHelper= new DeliveryServiceHelper();
    OMSHelper omsHelper= new OMSHelper();

    @Test(dataProvider = "userSingleOrder",groups = { "Regression", "Smoke", "Sanity", "Jitender" }, description = "\n 1.Create an order, 2.Hit track api to verify the status and also on_time status")
    public void trackSingleOrder(CreateMenuEntry payload, String unassignedStatus) throws IOException, InterruptedException {
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap=checkoutHelper.createLogin(payload.getpassword(), payload.getmobile());
        String response= checkoutHelper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        String delivery_valid = JsonPath.read(response, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]", "");
        String addressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String validAddressId="0";
        String[] delivery_validList= delivery_valid.split(",");
        String[] addressIdList= addressId.split(",");
        for (int i=0;i<delivery_validList.length;i++){
        	if (delivery_validList[i].equals("1")){
        		validAddressId=addressIdList[i];
        		break;
        	}
        }
        checkoutHelper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        String orderResponse= checkoutHelper.orderPlace(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(validAddressId),payload.getpayment_cod_method(),payload.getorder_comments()).ResponseValidator.GetBodyAsText();
        String orderId= JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[","").replace("]","");
        //System.out.println("OrderId------>"+orderId);
        String trackResponse= helper.track(hashMap.get("Tid"), hashMap.get("Token"),orderId).ResponseValidator.GetBodyAsText();
        String deliveryStatus= JsonPath.read(trackResponse, "$.data.delivery_status").toString().replace("[", "").replace("]", "").replace("\"","");
        //String onTimeStatus= JsonPath.read(trackResponse, "$.data.on_time").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(deliveryStatus,unassignedStatus,"Status is  assigned");
        //Assert.assertEquals(onTimeStatus, CheckoutConstants.trackOnTimeStatus,"unknown status is false");
    }

    @Test(dataProvider = "MultipleOrder",groups = { "Regression", "Smoke", "Sanity", "Jitender" }, description = "")
    public void trackMultipleOrder(CreateMenuEntry payload, String unassignedStatus) throws IOException
    {
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap=checkoutHelper.createLogin(payload.getpassword(), payload.getmobile());
        String response= checkoutHelper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        String delivery_valid = JsonPath.read(response, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]", "");
        String addressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String validAddressId="";
        String[] delivery_validList= delivery_valid.split(",");
        String[] addressIdList= addressId.split(",");
        for (int i=0;i<delivery_validList.length;i++){
        	if (delivery_validList[i].equals("1")){
        		validAddressId=addressIdList[i];
        		break;
        	}
        }
        checkoutHelper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        String orderResponse= checkoutHelper.orderPlace(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(validAddressId),payload.getpayment_cod_method(),payload.getorder_comments()).ResponseValidator.GetBodyAsText();
        String orderId= JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[","").replace("]","");
        //System.out.println("OrderId------>"+orderId);
        String trackResponse= helper.track(hashMap.get("Tid"), hashMap.get("Token"),orderId).ResponseValidator.GetBodyAsText();
        String deliveryStatus= JsonPath.read(trackResponse, "$.data.delivery_status").toString().replace("[", "").replace("]", "").replace("\"","");
        //String onTimeStatus= JsonPath.read(trackResponse, "$.data.on_time").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(deliveryStatus,unassignedStatus,"Status is not assigned");
       // Assert.assertEquals(onTimeStatus, CheckoutConstants.trackOnTimeStatus,"unknown status either 0 or 2");
        //deliveryServiceHelper.processOrderInDelivery();

    }

    @Test(dataProvider = "MultipleAddonOrder",groups = { "Regression", "Smoke", "Sanity", "Jitender" }, description = "")
    public void trackAddonOrder(CreateMenuEntry payload, String unassignedStatus) throws IOException
    {
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap=checkoutHelper.createLogin(payload.getpassword(), payload.getmobile());
        String response= checkoutHelper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String delivery_valid = JsonPath.read(response, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]", "");
        String addressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String validAddressId="";
        String[] delivery_validList= delivery_valid.split(",");
        String[] addressIdList= addressId.split(",");
        for (int i=0;i<delivery_validList.length;i++){
        	if (delivery_validList[i].equals("1")){
        		validAddressId=addressIdList[i];
        		break;
        	}
        }
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        checkoutHelper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        String orderResponse= checkoutHelper.orderPlace(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(validAddressId),payload.getpayment_cod_method(),payload.getorder_comments()).ResponseValidator.GetBodyAsText();
        String orderId= JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[","").replace("]","");
        System.out.println("OrderId------>"+orderId);
        String trackResponse= helper.track(hashMap.get("Tid"), hashMap.get("Token"),orderId).ResponseValidator.GetBodyAsText();
        String deliveryStatus= JsonPath.read(trackResponse, "$.data.delivery_status").toString().replace("[", "").replace("]", "").replace("\"","");
        String onTimeStatus= JsonPath.read(trackResponse, "$.data.on_time").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(deliveryStatus,unassignedStatus,"Status is not assigned");
        Assert.assertEquals(onTimeStatus, CheckoutConstants.trackOnTimeStatus,"unknown status either 0 or 2");
    }

    @Test(dataProvider = "userSingleOrder", groups = {"Regression", "Smoke", "Sanity", "Jitender"}, description = "")
    public void trackMinimalOrder(CreateMenuEntry payload,String unassignedStatus) throws  IOException{
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap=checkoutHelper.createLogin(payload.getpassword(), payload.getmobile());
        String response= checkoutHelper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        String delivery_valid = JsonPath.read(response, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]", "");
        String addressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String validAddressId="";
        String[] delivery_validList= delivery_valid.split(",");
        String[] addressIdList= addressId.split(",");
        for (int i=0;i<delivery_validList.length;i++){
        	if (delivery_validList[i].equals("1")){
        		validAddressId=addressIdList[i];
        		break;
        	}
        }
        checkoutHelper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        String orderResponse= checkoutHelper.orderPlace(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(validAddressId),payload.getpayment_cod_method(),payload.getorder_comments()).ResponseValidator.GetBodyAsText();
        String orderId= JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[","").replace("]","");
        String trackMinimalResponse= helper.track(hashMap.get("Tid"), hashMap.get("Token"),orderId).ResponseValidator.GetBodyAsText();
        String deliveryStatus= JsonPath.read(trackMinimalResponse, "$.data.delivery_status").toString().replace("[", "").replace("]", "").replace("\"","");
        String onTimeStatus= JsonPath.read(trackMinimalResponse, "$.data.on_time").toString().replace("[", "").replace("]", "");
        /*Assert.assertEquals(deliveryStatus,"Status is not assigned");
        Assert.assertEquals(onTimeStatus, CheckoutConstants.trackOnTimeStatus,"unknown status either 0 or 2");*/
    }

    @Test(dataProvider = "userSingleOrder",groups = { "Regression", "Smoke", "Sanity", "Jitender" }, description = "\n 1.Create an order, 2.Hit track api to verify the status and also on_time status")
    public void trackSingleOrderOnTime(CreateMenuEntry payload, String unassignedStatus) throws IOException, InterruptedException {
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap=checkoutHelper.createLogin(payload.getpassword(), payload.getmobile());
        String response= checkoutHelper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        String delivery_valid = JsonPath.read(response, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]", "");
        String addressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String validAddressId="0";
        String[] delivery_validList= delivery_valid.split(",");
        String[] addressIdList= addressId.split(",");
        for (int i=0;i<delivery_validList.length;i++){
        	if (delivery_validList[i].equals("1")){
        		validAddressId=addressIdList[i];
        		break;
        	}
        }
        checkoutHelper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        String orderResponse= checkoutHelper.orderPlace(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(validAddressId),payload.getpayment_cod_method(),payload.getorder_comments()).ResponseValidator.GetBodyAsText();
        String orderId= JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[","").replace("]","");
        //System.out.println("OrderId------>"+orderId);
        String trackResponse= helper.track(hashMap.get("Tid"), hashMap.get("Token"),orderId).ResponseValidator.GetBodyAsText();
        String deliveryStatus= JsonPath.read(trackResponse, "$.data.delivery_status").toString().replace("[", "").replace("]", "").replace("\"","");
        //String onTimeStatus= JsonPath.read(trackResponse, "$.data.on_time").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(deliveryStatus,unassignedStatus,"Status is  assigned");

        //Assert.assertEquals(onTimeStatus, CheckoutConstants.trackOnTimeStatus,"unknown status is false");
    }




}
