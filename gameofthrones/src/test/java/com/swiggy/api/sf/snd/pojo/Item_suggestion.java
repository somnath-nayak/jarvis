package com.swiggy.api.sf.snd.pojo;
public class Item_suggestion
{
    private String item_id;

    private String score;

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public String getScore ()
    {
        return score;
    }

    public void setScore (String score)
    {
        this.score = score;
    }

    public Item_suggestion build(){
        setDefaultValues();
        return this;
    }
    public void setDefaultValues(){

        if(this.getItem_id()==null){
            this.setItem_id("132330,132342");
        }
        if(this.getScore()==null){
            this.setScore("0.5,0.8");
        }

    }

    @Override
    public String toString()
    {
        return "ClassPojo [item_id = "+item_id+", score = "+score+"]";
    }
}