package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "type",
        "key",
        "intermediateText",
        "value",
        "hierarchy",
        "currency",
        "display_text",
        "info_text",
        "is_negative",
        "is_collapsible",
        "meta"
})
public class RenderingDetail {

    @JsonProperty("type")
    private String type;
    @JsonProperty("key")
    private String key;
    @JsonProperty("intermediateText")
    private String intermediateText;
    @JsonProperty("value")
    private String value;
    @JsonProperty("hierarchy")
    private Integer hierarchy;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("display_text")
    private String displayText;
    @JsonProperty("info_text")
    private String infoText;
    @JsonProperty("is_negative")
    private Integer isNegative;
    @JsonProperty("is_collapsible")
    private Integer isCollapsible;
    @JsonProperty("meta")
    private Meta meta;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public RenderingDetail() {
    }

    /**
     *
     * @param intermediateText
     * @param isCollapsible
     * @param infoText
     * @param displayText
     * @param isNegative
     * @param value
     * @param hierarchy
     * @param type
     * @param meta
     * @param key
     * @param currency
     */
    public RenderingDetail(String type, String key, String intermediateText, String value, Integer hierarchy, String currency, String displayText, String infoText, Integer isNegative, Integer isCollapsible, Meta meta) {
        super();
        this.type = type;
        this.key = key;
        this.intermediateText = intermediateText;
        this.value = value;
        this.hierarchy = hierarchy;
        this.currency = currency;
        this.displayText = displayText;
        this.infoText = infoText;
        this.isNegative = isNegative;
        this.isCollapsible = isCollapsible;
        this.meta = meta;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("intermediateText")
    public String getIntermediateText() {
        return intermediateText;
    }

    @JsonProperty("intermediateText")
    public void setIntermediateText(String intermediateText) {
        this.intermediateText = intermediateText;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    @JsonProperty("hierarchy")
    public Integer getHierarchy() {
        return hierarchy;
    }

    @JsonProperty("hierarchy")
    public void setHierarchy(Integer hierarchy) {
        this.hierarchy = hierarchy;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("display_text")
    public String getDisplayText() {
        return displayText;
    }

    @JsonProperty("display_text")
    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    @JsonProperty("info_text")
    public String getInfoText() {
        return infoText;
    }

    @JsonProperty("info_text")
    public void setInfoText(String infoText) {
        this.infoText = infoText;
    }

    @JsonProperty("is_negative")
    public Integer getIsNegative() {
        return isNegative;
    }

    @JsonProperty("is_negative")
    public void setIsNegative(Integer isNegative) {
        this.isNegative = isNegative;
    }

    @JsonProperty("is_collapsible")
    public Integer getIsCollapsible() {
        return isCollapsible;
    }

    @JsonProperty("is_collapsible")
    public void setIsCollapsible(Integer isCollapsible) {
        this.isCollapsible = isCollapsible;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("key", key).append("intermediateText", intermediateText).append("value", value).append("hierarchy", hierarchy).append("currency", currency).append("displayText", displayText).append("infoText", infoText).append("isNegative", isNegative).append("isCollapsible", isCollapsible).append("meta", meta).append("additionalProperties", additionalProperties).toString();
    }

}