package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;


import org.codehaus.jackson.annotate.JsonProperty;


public class OrderDeliveryBuilder {

    public OrderDelivery orderDelivery;
    public OrderDeliveryBuilder() {
        orderDelivery = new OrderDelivery();
    }

    @JsonProperty("order_id")
    public String orderId;
    @JsonProperty("status")
    public String status;

    public OrderDeliveryBuilder withOrderId(String orderId) {
        orderDelivery.setOrderId(orderId);
        return this;
    }

    public OrderDeliveryBuilder withStatus(String status) {
        orderDelivery.setStatus(status);
        return this;
    }

    public OrderDelivery build() {
        getDefaultValue();
        return orderDelivery;
    }

    private void getDefaultValue() {
        if (orderDelivery.getOrderId() == null) {
            orderDelivery.setOrderId("1");
        }
        if (orderDelivery.getStatus() == null) {
            orderDelivery.setStatus("delivered");
        }
    }
}

