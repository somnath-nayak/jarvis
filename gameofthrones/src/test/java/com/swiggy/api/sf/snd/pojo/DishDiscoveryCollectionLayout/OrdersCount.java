package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class OrdersCount {

    private String type;
    private DataString data;

    /**
     * No args constructor for use in serialization
     *
     */
    public OrdersCount() {
    }

    /**
     *
     * @param data
     * @param type
     */
    public OrdersCount(String type, DataString data) {
        super();
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DataString getData() {
        return data;
    }

    public void setData(DataString data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("data", data).toString();
    }

}
