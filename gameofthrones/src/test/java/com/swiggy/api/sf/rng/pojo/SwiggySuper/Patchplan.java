package com.swiggy.api.sf.rng.pojo.SwiggySuper;

public class Patchplan {
	
	
	private String plan_id;
	
	private boolean enabled=true;
	
	private String count="0";
	
	private String valid_till;
	
	private String updated_by="By-Super-Automation-Script";
	
	public Patchplan() {
		
	}
	
	public Patchplan(String plan_id, boolean enabled) {
		this.plan_id = plan_id;
		this.enabled= enabled;
	}
	
	public Patchplan(String plan_id, String valid_till, String count) {
		
		this.plan_id= plan_id;
		this.valid_till= valid_till;
		this.count=count;
	}
	
	
public Patchplan(String plan_id, String count) {
		
		this.plan_id= plan_id;
		this.count=count;
	}
	
public Patchplan(String plan_id, String enabled, String valid_till,String count) {
	
	this.plan_id= plan_id;
	this.enabled=Boolean.parseBoolean(enabled);
	this.valid_till= valid_till;
	this.count=count;
}	

public String getPlan_id() {
	return this.plan_id;
}
	
public void setPlan_id(String plan_id) {
	this.plan_id= plan_id;
}
public boolean getEnabled() {
	return this.enabled;
}
	public void setEnabled(boolean enabled) {
		this.enabled= enabled;
	}
	
	
	public String getValidTill()
	{
		return this.valid_till= valid_till;
	}
	public void setValidTill(String valid_till) {
		this.valid_till= valid_till;
	}
	
	public String getCount() {
		return this.count;
	}
	public void setCount(String count) {
		this.count= count;
	}
	
	public String getCreatedBy() {
		return this.updated_by;
	
}
 public void setUpdatedBy(String update_by) {
	 this.updated_by= updated_by;
 }	
	@Override
	public String toString() {
		
		return "{"
				
				+ "\"plan_id\"" + ":" + plan_id + ","
				+ "\"enabled\"" + ":" + enabled + ","
				+ "\"count\"" + ":" + count + ","
				+ "\"updated_by\"" + ":" + "\"" +updated_by + "\"" + ","
				+ "\"valid_till\"" + ":" + valid_till 
			
				+ "}"
				
				;
	}
	
public String toStringDisbalePlan() {
		
		return "{"
				
				+ "\"plan_id\"" + ":" + plan_id + ","
				+ "\"updated_by\"" + ":" + "\"" +updated_by + "\"" + ","
				+ "\"enabled\"" + ":" + enabled 
				+ "}"
				
				;
	}
	
public String toStringPlanValidityAndCount() {
	
	return "{"
			
			+ "\"plan_id\"" + ":" + plan_id + ","
			+ "\"updated_by\"" + ":" + "\"" +updated_by + "\"" + ","
			+ "\"count\"" + ":" + count + ","
			+ "\"enabled\"" + ":" + enabled +","
			+ "\"valid_till\"" + ":" + valid_till 
			+ "}"
			
			;
}

public String toStringPlanCount() {
	
	return "{"
			
			+ "\"plan_id\"" + ":" + plan_id + ","
			+ "\"updated_by\"" + ":" + "\"" +updated_by + "\"" + ","
			+ "\"enabled\"" + ":" + enabled +","
			+ "\"count\"" + ":" + count 
			+ "}"
			
			;
}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

	