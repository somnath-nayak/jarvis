package com.swiggy.api.sf.snd.pojo;

public class ItemSet {

    private int score;

    private String item_id;

    public ItemSet() {
    }

    public ItemSet(int score, String item_id) {
        this.score = score;
        this.item_id = item_id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }
}
