package com.swiggy.api.sf.checkout.tests;

import java.util.HashMap;

import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.sf.checkout.constants.CafeConstant;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import com.swiggy.api.sf.checkout.dp.CheckoutCafeDP;
import com.swiggy.api.sf.checkout.helper.CafeHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutPricingHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Validator;
import org.testng.annotations.Test;
import java.util.HashMap;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.*;

public class CheckoutCafeTest extends CheckoutCafeDP {

    Initialize gameofthrones = new Initialize();
    CheckoutHelper helper= new CheckoutHelper();
    CheckoutPricingHelper pricingHelper= new CheckoutPricingHelper();
    OMSHelper oMSHelper=new OMSHelper();
    CafeHelper cafeHelper=new CafeHelper();
    RMSHelper rmsHelper=new RMSHelper();
    String orderID=null;
    String tid;
    String token;
    
    @Test(dataProvider="CafeFlow", groups = {"Smoke","chandan"}, description = "set pricing for cart")
    public void CafeFlow(String menu_item_id,String quantity, String restId,String cart_type){
       HashMap<String, String> hashMap=helper.TokenData(CheckoutConstants.mobile1, CheckoutConstants.password1);
       tid=hashMap.get("Tid");
       token=hashMap.get("Token");
       
       //1. Add cart
       cafeHelper.updateCart(tid,token,menu_item_id,quantity,restId,cart_type);
       //2. Place order
       Validator orderValidator=cafeHelper.cafePlaceOrder(tid, token, "Mobikwik-SSO", "Test Cafe Order",cart_type).ResponseValidator;
       pricingHelper.validateSucessStatusCode(orderValidator.GetResponseCode());
       pricingHelper.validateApiResData(orderValidator.GetBodyAsText(),PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
       cafeHelper.validateCafeOrderData(orderValidator.GetBodyAsText(), 
    		   CafeConstant.EXPECTED_CAFE_ORDER_TYPE, CafeConstant.EXPECTED_CAFE_RESTAURANT_TYPE);
       
       orderID=JsonPath.read(orderValidator.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
   	   //3.Track order
       Validator trackOrderValidator=cafeHelper.trackCafeOrder(tid, token,orderID).ResponseValidator;
       pricingHelper.validateSucessStatusCode(trackOrderValidator.GetResponseCode());
       pricingHelper.validateApiResData(trackOrderValidator.GetBodyAsText(),PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
       cafeHelper.validateTrackCafeOrderData(trackOrderValidator.GetBodyAsText());
         //4. Confirm order from OMS-Vendor
      oMSHelper.verifyOrder(orderID, CafeConstant.ORDER_ACTION_CODE);
      cafeHelper.validateTrackOrderMessage(trackOrderResponse(), CafeConstant.AWAITING_CONFIRMATION_MESSAGE);
     
      String rmsToken=rmsHelper.getLoginToken(RMSConstants.username_login, RMSConstants.password_login);
      rmsHelper.confirm(rmsToken, orderID, restId);
      cafeHelper.validateTrackOrderMessage(trackOrderResponse(), CafeConstant.AWAITING_MARKED_READY_MESSAGE);
            
      rmsHelper.markReady(rmsToken, orderID, restId);
      cafeHelper.validateTrackOrderMessage(trackOrderResponse(), CafeConstant.AWAITING_REDEEM_MESSAGE);
      
      //5. Redeem the order
       Validator redeemResValidator= cafeHelper.cafeRedeem(tid,token,orderID).ResponseValidator;
       pricingHelper.validateSucessStatusCode(redeemResValidator.GetResponseCode());
       pricingHelper.validateApiResData(redeemResValidator.GetBodyAsText(),CafeConstant.ORDER_REDEEMED_MESSAGE);
       cafeHelper.validateTrackOrderMessage(trackOrderResponse(), CafeConstant.REDEEM_MESSAGE);
  }
 
    public String trackOrderResponse(){
    	return cafeHelper.trackCafeOrder(tid, token,orderID).ResponseValidator.GetBodyAsText();
    }
 }