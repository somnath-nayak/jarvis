package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.EndToEndHelp;
import com.swiggy.api.sf.rng.constants.EDVOConstants;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.dp.EdvoDP;
import com.swiggy.api.sf.rng.dp.MultiApplyDP;
import com.swiggy.api.sf.rng.helper.EDVOHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import io.advantageous.boon.core.Sys;
import io.gatling.recorder.har.Json;

public class EDVOTest extends EdvoDP {

	HashMap<String, String> requestHeaders = new HashMap<String, String>();
	GameOfThronesService service;
	Processor processor;
	// RngHelper rngHelper = new RngHelper();
	EDVOHelper edvoHelper = new EDVOHelper();
	DBHelper dbHelper = new DBHelper();
	RedisHelper redisHelper = new RedisHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	SoftAssert softAssert = new SoftAssert();
	RngHelper rngHelper = new RngHelper();

	@Test(dataProvider = "createMealTypeEDVOCampData",description="" ,dataProviderClass = EdvoDP.class)
	public void createMealTypeEDVOCampTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createGroupTypeEDVOCampData", dataProviderClass = EdvoDP.class)
	public void createGroupTypeEDVOCampTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createMealTypeEDVOCampForMultipleRestData", dataProviderClass = EdvoDP.class)
	public void createMealTypeEDVOCampForMultipleRestTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createCampaignWithMealOperationMetaAsNullData", dataProviderClass = EdvoDP.class)
	public void createCampaignWithMealOperationMetaAsNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String errorMessageInData = createResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("operationMeta is Invalid"),true);
		Assert.assertEquals(errorMessageInData.contains("operationMeta is Invalid"),true);
		//Assert.assertEquals(statusMessage, "discounts[0].meals[0].operationMeta is Invalid");
		//Assert.assertEquals(errorMessageInData, "discounts[0].meals[0].operationMeta is Invalid");

	}

	@Test(dataProvider = "createCampaignWithOperationTypeAsNullData", dataProviderClass = EdvoDP.class)
	public void createCampaignWithOperationTypeAsNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String errorMessageInData = createResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("operationType is Invalid"),true);
		Assert.assertEquals(errorMessageInData.contains("operationType is Invalid"),true);
		//Assert.assertEquals(statusMessage, "discounts[0].OperationType is Invalid");
		//Assert.assertEquals(errorMessageInData, "discounts[0].OperationType is Invalid");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createCampaignWithOperationTypeAsEmptyStringData", dataProviderClass = EdvoDP.class)
	public void createCampaignWithOperationTypeAsEmptyStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int status = createResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		String error = createResponse.ResponseValidator.GetNodeValue("$.error");
		Assert.assertEquals(status, 400);
		Assert.assertEquals(error, "Bad Request");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createWithRestaurantIdAsNullData", dataProviderClass = EdvoDP.class)
	public void createWithRestaurantIdAsNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		 String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusCode, 2);
		 Assert.assertEquals(statusMessage.contains("restaurant.id is Invalid"),true);

		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createOneEDVOCampOnTwoSameRestaurantData", dataProviderClass = EdvoDP.class)
	public void createOneEDVOCampOnTwoSameRestaurantTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createTwoEDVOCampOnSameRestaurantData", dataProviderClass = EdvoDP.class)
	public void createTwoEDVOCampOnSameRestaurantTest(HashMap<String, String> createEDVO,
													  HashMap<String, String> createSecondEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");

		Processor secondCampCreateResponse = edvoHelper.createEDVOTradeDiscount(createSecondEDVO);
		int statusCodeOfSecondCamp = secondCampCreateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessageOfSecondCamp = secondCampCreateResponse.ResponseValidator.GetNodeValue("$.statusMessage");

		if (statusMessageOfSecondCamp.contains("Restaurant already has an active campaign")) {
			Assert.assertEquals(statusCodeOfSecondCamp, 2);
		}
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createOneEDVOCampOnTwoSameRestaurantAndSameMealData", dataProviderClass = EdvoDP.class)
	public void createOneEDVOCampOnTwoSameRestaurantAndSameMealTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String errorMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(errorMessage.contains("Meal Ids or Group Ids cannot be same in one reques"),true);

	}

	@Test(dataProvider = "createOneEDVOCampOnTwoDifferentRestaurantButSameMealData", dataProviderClass = EdvoDP.class)
	public void createOneEDVOCampOnTwoDifferentRestaurantButSameMealTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String errorMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(errorMessage.contains("Meal Ids or Group Ids cannot be same in one request"),true);

	}

	@Test(dataProvider = "createTwoEDVOCampOnDifferentRestaurantButSameMealData", dataProviderClass = EdvoDP.class)
	public void createTwoEDVOCampOnDifferentRestaurantButSameMealTest(HashMap<String, String> createEDVO,
																	  HashMap<String, String> createSecondEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");

		Processor secondCampCreateResponse = edvoHelper.createEDVOTradeDiscount(createSecondEDVO);
		int statusCodeOfSecondCamp = secondCampCreateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessageOfSecondCamp = secondCampCreateResponse.ResponseValidator.GetNodeValue("$.statusMessage");

		if (statusMessageOfSecondCamp.contains(EDVOConstants.alreadyRunningCampaignMessage)) {
			Assert.assertEquals(statusCodeOfSecondCamp, 2);
		}
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createThreeEDVOCampOnDifferentRestaurantMealIdIsSameInOneCampData", dataProviderClass = EdvoDP.class)
	public void createThreeEDVOCampOnDifferentRestaurantMealIdIsSameInOneCampTest(HashMap<String, String> createEDVO,
																				  HashMap<String, String> createSecondEDVO, HashMap<String, String> createThirdEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");

		Processor secondCampCreateResponse = edvoHelper.createEDVOTradeDiscount(createSecondEDVO);
		int secondCampaignStatusCode = secondCampCreateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String secondCampaignStatusMessage = secondCampCreateResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int secondCampaignId = secondCampCreateResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(secondCampaignStatusCode, 0);
		Assert.assertEquals(secondCampaignStatusMessage, "success");

		Processor thirdCampCreateResponse = edvoHelper.createEDVOTradeDiscount(createThirdEDVO);
		int thirdStatusCodeOfSecondCamp = thirdCampCreateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String thirdCamptatusMessageOfSecondCamp = thirdCampCreateResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");

		if (thirdCamptatusMessageOfSecondCamp.contains(
				"Already running Campaign for one of the given meals on another Restaurant( Restaurant already has an active campaign")) {
			Assert.assertEquals(thirdStatusCodeOfSecondCamp, 2);
		}
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(secondCampaignId));
	}

	@Test(dataProvider = "createTwoEDVOCampOnDifferentRestaurantButSameGroupsData", dataProviderClass = EdvoDP.class)
	public void createTwoEDVOCampOnDifferentRestaurantButSameGroupsTest(HashMap<String, String> createEDVO,
																		HashMap<String, String> createSecondEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");

		Processor secondCreateResponse = edvoHelper.createEDVOTradeDiscount(createSecondEDVO);
		int statusCodeOfSecondCamp = secondCreateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessageOfSecondCamp = secondCreateResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignIdOfSecondCamp = secondCreateResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCodeOfSecondCamp, 0);
		Assert.assertEquals(statusMessageOfSecondCamp, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignIdOfSecondCamp));
	}

	@Test(dataProvider = "createEDVOCampWithDifferentTypeOfRewardsOnMealsData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithDifferentTypeOfRewardsOnMealsTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWithMealIdAsNullData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithMealIdAsNull(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("mealId is Invalid"),true);
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createOneCampWithTwoRestaurantAndOperationTypeIsRestaurantAndMealData", dataProviderClass = EdvoDP.class)
	public void createOneCampWithTwoRestaurantAndOperationTypeIsRestaurantAndMealTest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");

		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains( "BXGY Campaign should have Operation Type MEAL"),true);
		Assert.assertEquals(data.contains( "BXGY Campaign should have Operation Type MEAL"),true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createOneCampWithOneRestaurantAndOperationTypeIsRestaurantData", dataProviderClass = EdvoDP.class)
	public void createOneCampWithOneRestaurantAndOperationTypeIsRestaurantTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");

		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("BXGY Campaign should have Operation Type MEAL"),true);
		Assert.assertEquals(data.contains("BXGY Campaign should have Operation Type MEAL"),true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createTwoEDVOCampSameRestaurantIdButOneCampOperationTypeIsRestaurantData", dataProviderClass = EdvoDP.class)
	public void createTwoEDVOCampSameRestaurantIdButOneCampOperationTypeIsRestaurantTest(
			HashMap<String, String> createEDVO, HashMap<String, String> createSecondEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");

		Processor createResponseOfSecondCamp = edvoHelper.createEDVOTradeDiscount(createSecondEDVO);
		int statusCodeOfSecondCamp = createResponseOfSecondCamp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessageOfSecondCamp = createResponseOfSecondCamp.ResponseValidator.GetNodeValue("$.statusMessage");

		if (statusMessageOfSecondCamp.contains("Restaurant already has an active campaign")) {
			Assert.assertEquals(statusCodeOfSecondCamp, 2);
		}
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWithMealAndGroupRewardBothAreNullTestData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithMealAndGroupRewardBothAreNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "Atleast one entity should have reward object");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereInAMealAllGroupIdsAreSameData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereInAMealAllGroupIdsAreSameTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");

		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "Meal Ids or Group Ids cannot be same in one request");
		Assert.assertEquals(data, "Meal Ids or Group Ids cannot be same in one request");
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOOneCampWhereTwoMealsAreHavingSameGroupIdsData", dataProviderClass = EdvoDP.class)
	public void createEDVOOneCampWhereTwoMealsAreHavingSameGroupIdsTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");

		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("Meal Ids or Group Ids cannot be same in one request"),true);
		Assert.assertEquals(data.contains("Meal Ids or Group Ids cannot be same in one request"),true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "creatEDVOCampWithGroupsIsNullData", dataProviderClass = EdvoDP.class)
	public void creatEDVOCampWithGroupsIsNull(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "discounts[0].meals[0].groups is Invalid");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOOneCampWhereTwoRestaurantsMealsAreHavingSameGroupIdsData", dataProviderClass = EdvoDP.class)
	public void createEDVOOneCampWhereTwoRestaurantsMealsAreHavingSameGroupIdsTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");

		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("Meal Ids or Group Ids cannot be same in one request"),true);
		Assert.assertEquals(data.contains("Meal Ids or Group Ids cannot be same in one request"),true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOTwoCampWhereTheirMealsAreHavingSameGroupIdsData", dataProviderClass = EdvoDP.class)
	public void createEDVOTwoCampWhereTheirMealsAreHavingSameGroupIdsTest(HashMap<String, String> createEDVO,
																		  HashMap<String, String> createSecondEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");

		Processor createResponseOfSencondCamp = edvoHelper.createEDVOTradeDiscount(createSecondEDVO);
		int statusCodeOfSencondCamp = createResponseOfSencondCamp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessageOfSencondCamp = createResponseOfSencondCamp.ResponseValidator
				.GetNodeValue("$.statusMessage");
		int campaignIdOfSencondCamp = createResponseOfSencondCamp.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCodeOfSencondCamp, 0);
		Assert.assertEquals(statusMessageOfSencondCamp, "success");

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignIdOfSencondCamp));
	}

	@Test(dataProvider = "createEDVOCampRewardFunctionAsMAXData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardFunctionAsMAXTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampRewardFunctionAsMINData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardFunctionAsMINTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampRewardFunctionAsNONEData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardFunctionAsNONETest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampRewardFunctionAsEmptyStringData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardFunctionAsEmptyStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int status = createResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		String errorMessage = createResponse.ResponseValidator.GetNodeValue("$.error");

		Assert.assertEquals(status, 400);
		Assert.assertEquals(errorMessage, "Bad Request");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampRewardTypeFlatData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardTypeFlatTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampRewardTypePercentageData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardTypePercentageTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampRewardTypeFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardTypeFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampRewardTypeEmptyStringData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardTypeEmptyStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int status = createResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		String errorMessage = createResponse.ResponseValidator.GetNodeValue("$.error");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(status, 400);
		Assert.assertEquals(errorMessage, "Bad Request", "expected fail--. FREEdELIVERY as rewardType is accepting");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampRewardTypeFREE_DELIVERYData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardTypeFREE_DELIVERYTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");

		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "Reward Type For Meals should be Flat, Percentage or FinalPrice only");
		Assert.assertEquals(data, "Reward Type For Meals should be Flat, Percentage or FinalPrice only");
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampRewardTypeFreebieData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampRewardTypeFreebieTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "Reward Type For Meals should be Flat, Percentage or FinalPrice only");
		Assert.assertEquals(data, "Reward Type For Meals should be Flat, Percentage or FinalPrice only");

		//Assert.assertNotEquals(statusMessage, "success","Expected Fail-- Rigth now not having check for Freebie in RewardType");
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWithInvalidRewardTypePercentageData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithInvalidRewardTypePercentageTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int status = createResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		String error = createResponse.ResponseValidator.GetNodeValue("$.error");

		Assert.assertEquals(status, 400);
		Assert.assertEquals(error, "Bad Request");
	}

	@Test(dataProvider = "createEDVOCampWithInvalidRewardTypeFlatData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithInvalidRewardTypeFlatTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int status = createResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		String error = createResponse.ResponseValidator.GetNodeValue("$.error");

		Assert.assertEquals(status, 400);
		Assert.assertEquals(error, "Bad Request");
	}

	@Test(dataProvider = "createEDVOCampWithInvalidRewardTypeFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithInvalidRewardTypeFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int status = createResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		String error = createResponse.ResponseValidator.GetNodeValue("$.error");

		Assert.assertEquals(status, 400);
		Assert.assertEquals(error, "Bad Request");
	}

	@Test(dataProvider = "createEDVOCampWithRewardValueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithRewardValueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWithRewardValueAsZeroData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithRewardValueAsZeroTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		Assert.assertNotEquals(statusCode, 0, "Expected Fial-----> Currently allowing to set reward value as zero");
		Assert.assertNotEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWithRewardValueAsNullData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithRewardValueAsNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		if (statusMessage.contains("rewardValue is Invalid")) {
			Assert.assertEquals(statusCode, 2);
		}
	}

	@Test(dataProvider = "createEDVOCampWithRewardValueAsHunderedPercentData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithRewardValueAsHunderedPercentTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusCode, 0);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWithRewardValueGreaterThanHunderedPercentData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithRewardValueGreaterThanHunderedPercentTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusCode, 2, "expected Fail --Precent greater than 100, not having check");
	}

	@Test(dataProvider = "createEDVOCampWithRewardCountAsOneAndGreaterThanOneData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithRewardCountAsOneAndGreaterThanOneTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));

	}

	@Test(dataProvider = "createEDVOCampWithMealRewardCountAsNullData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithMealRewardCountAsNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("count is Invalid"),true);
		Assert.assertEquals(data.contains("count is Invalid"),true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));

	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetG2AtPercentOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetG2AtPercentOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetG2AtFlatOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetG2AtFlatOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetG2Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetG2Test(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetG2AtFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetG2AtFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetMinPriceFreeInG2Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetMinPriceFreeInG2Test(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetMinPriceAtFINALPriceInG2Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetMinPriceAtFINALPriceInG2Test(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetMinPriceAtPercentageOFFInG2Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetMinPriceAtPercentageOFFInG2Test(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetMinPriceFlatOFFInG2Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetMinPriceFlatOFFInG2Test(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetMaxPriceFatOFFInG2Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetMaxPriceFatOFFInG2Test(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetMaxPricePercentageOFFInG2Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetMaxPricePercentageOFFInG2Test(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetMaxPriceFreeInG2Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetMaxPriceFreeInG2Test(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1AndG2AndGetMaxPriceAtFinalPriceInG2Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1AndG2AndGetMaxPriceAtFinalPriceInG2Test(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3AtFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3AtFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3FREEData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3FREETest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3PercentageOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3PercentageOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3FlatOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3FlatOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceAtFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceAtFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceFREEData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceFREETest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3MinPricePercentageOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3MinPricePercentageOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceFlatOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3MinPriceFlatOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceAtFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceAtFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceFREEData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3MaxPriceFREETest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG3MaxPricePercentageOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG3MaxPricePercentageOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3PercentageOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3PercentageOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3PercentageOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3PercentageOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3FlatOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3FlatOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3FlatOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3FlatOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3AtFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3AtFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3AtFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3AtFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3FREEData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetMinPriceInG1G2G3FREETest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3FREEData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetMaxPriceInG1G2G3FREETest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetPercentageOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetPercentageOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGetFlatOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGetFlatOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAtFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAtFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyMealAndGet100PercentOFFData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyMealAndGet100PercentOFFTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetXPercentOFFInG1YPercentOFFInG2ZPercentOFFInG3Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetXPercentOFFInG1YPercentOFFInG2ZPercentOFFInG3Test(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetXFlatOFFInG1YFlatOFFInG2ZFlatOFFInG3Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetXFlatOFFInG1YFlatOFFInG2ZFlatOFFInG3Test(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYG3AtFinalPriceZData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYG3AtFinalPriceZTest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndG3FREEData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndG3FREETest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndZPercentOFFInG3Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndZPercentOFFInG3Test(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndFlatZOFFInG3Data", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereBuyG1G2G3AndGetG1AtFinalPriceXG2AtFinalPriceYAndFlatZOFFInG3Test(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereMealCountIsZeroData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereMealCountIsZeroTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("count is Invali"),true);
		Assert.assertEquals(data.contains("count is Invali"),true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereMealRewardCountIsZeroData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereMealRewardCountIsZeroTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains( "count is Invalid"),true);
		Assert.assertEquals(data.contains( "count is Invalid"),true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereGroupCountIsZeroData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereGroupCountIsZeroTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("count is Invalid"),true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereGroupRewardCountIsZeroData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereGroupRewardCountIsZeroTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String Data = createResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("count is Invalid"), true);
		Assert.assertEquals(Data.contains("count is Invalid"), true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createOneEDVOCampOnMultipleRestaurantsSomeOnMealLevelAndSomeOnGroupLevelData", dataProviderClass = EdvoDP.class)
	public void createOneEDVOCampOnMultipleRestaurantsSomeOnMealLevelAndSomeOnGroupLevelTest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampToValidateNameSpaceWithValidInputValueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampToValidateNameSpaceWithValidInputValueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereNameSpaceIsEmptyStringData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereNameSpaceIsEmptyStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "namespace is Invalid");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereNameSpaceNullData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereNameSpaceNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "namespace is Invalid");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWithValidValueForHeaderData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWithValidValueForHeaderTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereHeaderAsEmptyStringData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereHeaderAsEmptyStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "header is Invalid");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereHeaderAsNullValueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereHeaderAsNullValueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "header is Invalid");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampDescriptionWithValidInputValueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampDescriptionWithValidInputValueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampDescriptionForEmptyStringData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampDescriptionForEmptyStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_FIELDS_NULL");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampDescriptionSetAsNullData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampDescriptionSetAsNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_FIELDS_NULL");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereValidFromDateIsPresentData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereValidFromDateIsPresentTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereValidFromIsFutureDateData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereValidFromIsFutureDateTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereValidFromIsPastDateData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereValidFromIsPastDateTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereValidTillDateIsFutureDateData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereValidTillDateIsFutureDateTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereValidTillDateIsSmallerThanValidFromDateData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereValidTillDateIsSmallerThanValidFromDateTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, EDVOConstants.eCampaignDatesInValid);
	}

	@Test(dataProvider = "createEDVOCampWhereValidTillDateIsPresentDateData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereValidTillDateIsPresentDateTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, EDVOConstants.eCampaignDatesInValid);
	}

	@Test(dataProvider = "createEDVOCampWhereValidTillDateIsSameAsValidFromDateData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereValidTillDateIsSameAsValidFromDateTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
	}

	@Test(dataProvider = "createEDVOCampWhereValidTillDateIsGreaterThanValidFromDateData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereValidTillDateIsGreaterThanValidFromDateTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCampaignTypeIsBXGYData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCampaignTypeIsBXGYTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCampaignTypeIsFREE_DELIVERYData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCampaignTypeIsFREE_DELIVERYTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "Campaign Type For Meals should be BXGY only");
	}

	@Test(dataProvider = "createEDVOCampWhereCampaignTypeIsFreebieData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCampaignTypeIsFreebieTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "Campaign Type For Meals should be BXGY only");
	}

	@Test(dataProvider = "createEDVOCampWhereCampaignTypeIsFinalPriceData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCampaignTypeIsFinalPriceTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, EDVOConstants.campTypeOfMealBXGYOnly);
	}

	@Test(dataProvider = "createEDVOCampWhereCampaignTypeIsFlatData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCampaignTypeIsFlatTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, EDVOConstants.campTypeOfMealBXGYOnly);
	}

	@Test(dataProvider = "createEDVOCampWhereCampaignTypeIsPercentageData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCampaignTypeIsPercentageTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, EDVOConstants.campTypeOfMealBXGYOnly);
	}

	@Test(dataProvider = "createEDVOCampWhereCampaignTypeAsEmptyStringData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCampaignTypeAsEmptyStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int status = createResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		String errorMessage = createResponse.ResponseValidator.GetNodeValue("$.error");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(status, 400);
		Assert.assertEquals(errorMessage, "Bad Request");
	}

	@Test(dataProvider = "createEDVOCampWhereCampaignTypeIsNullData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCampaignTypeIsNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "campaignType is Invalid");
	}

	@Test(dataProvider = "createEDVOCampWhereEnabledIsTrueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereEnabledIsTrueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereEnabledIsFalseData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereEnabledIsFalseTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereRestaurantHitIsHunderedData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereRestaurantHitIsHunderedTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereRestaurantHitIsFiveData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereRestaurantHitIsFiveTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereRestaurantHitIsZeroData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereRestaurantHitIsZeroTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereRestaurantHitIsGreaterThanHunderedData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereRestaurantHitIsGreaterThanHunderedTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_HIT_NOT_VALID");
	}

	@Test(dataProvider = "createEDVOCampWhereRestaurantHitIsNegativeValueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereRestaurantHitIsNegativeValueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_HIT_NOT_VALID");
	}

	@Test(dataProvider = "createEDVOCampWhereRestaurantHitIsInvalidValueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereRestaurantHitIsInvalidValueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_HIT_NOT_VALID");
	}

	@Test(dataProvider = "createEDVOCampWhereRestaurantHitAndSwiggyHitIsNotEqualToHunderedData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereRestaurantHitAndSwiggyHitIsNotEqualToHunderedTest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_HIT_NOT_VALID");
	}

	@Test(dataProvider = "createEDVOCampWhereRestaurantHitAndSwiggyHitIsGreaterThanHunderedData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereRestaurantHitAndSwiggyHitIsGreaterThanHunderedTest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_HIT_NOT_VALID");
	}

	@Test(dataProvider = "createEDVOCampWhereSwiggyHitIsValidInputData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereSwiggyHitIsValidInputTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereSwiggyHitIsZeroData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereSwiggyHitIsZeroTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereSwiggyHitLesserThanHunderedData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereSwiggyHitLesserThanHunderedTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereSwiggyHitGreaterThanHunderedData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereSwiggyHitGreaterThanHunderedTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_HIT_NOT_VALID");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereSwiggyHitIsNegativeValueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereSwiggyHitIsNegativeValueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_HIT_NOT_VALID");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereSwiggyHitAndRestaurantHitIsNotEqualToHunderedData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereSwiggyHitAndRestaurantHitIsNotEqualToHunderedTest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_HIT_NOT_VALID");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereSwiggyHitAndRestaurantHitIsGreaterThanHunderedData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereSwiggyHitAndRestaurantHitIsGreaterThanHunderedTest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "E_CAMPAIGN_HIT_NOT_VALID");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCreateByIsValidInputData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCreateByIsValidInputTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCreateByAsEmptyStringData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCreateByAsEmptyStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "createdBy is Invalid");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCreateByAsNullValueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCreateByAsNullValueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "createdBy is Invalid");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCommissionOnFullBillAsTrueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCommissionOnFullBillAsTrueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCommissionOnFullBillAsFalseData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCommissionOnFullBillAsFalseTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereTaxesOnDiscountedBillIsTrueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereTaxesOnDiscountedBillIsTrueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereTaxesOnDiscountedBillIsFalseData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereTaxesOnDiscountedBillIsFalseTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereFirstOrderRestrictionIsTrueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereFirstOrderRestrictionIsTrueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereFirstOrderRestrictionIsFalseData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereFirstOrderRestrictionIsFalseTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereTimeSlotRestrictionIsTrueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereTimeSlotRestrictionIsTrueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereTimeSlotRestrictionIsFalseData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereTimeSlotRestrictionIsFalseTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereUserRestrictionIsTrueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereUserRestrictionIsTrueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereUserRestrictionIsFalseData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereUserRestrictionIsFalseTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereShortDescriptionData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereShortDescriptionTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String shortDescription = edvoHelper.checkEDVOCampaignShortDescriptionFromDB(Integer.toString(campaignId));
		if (!shortDescription.equalsIgnoreCase("short Description is not persent in DB")) {
			System.out.println("shortDescription in DB is  ------>" + shortDescription);
			Assert.assertEquals(statusCode, 0);
			Assert.assertEquals(statusMessage, "success");
		}
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereShortDescriptionAsEmptyStringData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereShortDescriptionAsEmptyStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		// String shortDescription =
		// edvoHelper.checkEDVOCampaignShortDescriptionFromDB(Integer.toString(campaignId));
		// System.out.println("shortDescription is -------> "+ shortDescription);
		// if(!shortDescription.equalsIgnoreCase(null)) {
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereShortDescriptionAsNullData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereShortDescriptionAsNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		// String shortDescription =
		// edvoHelper.checkEDVOCampaignShortDescriptionFromDB(Integer.toString(campaignId));
		// System.out.println("shortDescription is -------> "+ shortDescription);
		// if(!shortDescription.equalsIgnoreCase(null)) {
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereMealCountIsOneData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereMealCountIsOneTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereMealCountIsTwoData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereMealCountIsTwoTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereMealCountAsNullData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereMealCountAsNullTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		String data = createResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage.contains("count is Invalid"),true);
		Assert.assertEquals(data.contains("count is Invalid"),true);
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereMealIsParentIsTrueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereMealIsParentIsTrueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereMealIsParentIsFalseData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereMealIsParentIsFalseTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereTimeSlotRestrictionIsSetData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereTimeSlotRestrictionIsSetTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereTimeSlotRestrictionIsSetToTrueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereTimeSlotRestrictionIsSetToTrueTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(statusCode, 0, "expected fail timeSlotRestrictions is true but slot set as null");
		Assert.assertNotEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereTimeSlotRestrictionIsSetToFalseAndSlotIsNullData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereTimeSlotRestrictionIsSetToFalseAndSlotIsNullTest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereTimeSlotRestrictionIsSetToFalseAndSlotIsSetData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereTimeSlotRestrictionIsSetToFalseAndSlotIsSetTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCheckAllDaysOfSlotWhenTimeSlotRestrictionIsSetToTrueData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCheckAllDaysOfSlotWhenTimeSlotRestrictionIsSetToTrueTest(
			HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCheckDaysTimeOfSlotAsStringData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCheckDaysTimeOfSlotAsStringTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int status = createResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.error");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(status, 400);
		Assert.assertEquals(statusMessage, "Bad Request");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereOpenIsGreaterThanCloseTimeData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereOpenIsGreaterThanCloseTimeTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "Invalid time slots => open time is greater than close time");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCloseTimeIsInvalidData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCloseTimeIsInvalidTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertEquals(statusMessage, "Invalid time slots => open time is greater than close time");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereCloseTimeIsNextDayData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereCloseTimeIsNextDayTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampWhereOpenTimeIsZeroData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampWhereOpenTimeIsZeroTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		// int campaignId= createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 2);
		Assert.assertNotEquals(statusMessage, "success");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampForThirtyDaysDormantData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampForThirtyDaysDormantTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampForSixtyDaysDormantData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampForSixtyDaysDormantTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampForNintyDaysDormantData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampForNintyDaysDormantTest(HashMap<String, String> createEDVO) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVO);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "createEDVOCampForMultiTDWithThirtyDaysDormantData", dataProviderClass = EdvoDP.class)
	public void createEDVOCampForMultiTDWithThirtyDaysDormantTest(
			HashMap<String, String> createEDVOCampWithUserRestriction,
			HashMap<String, String> createEDVOCampWithSwiggyFirstOrder,
			HashMap<String, String> createEDVOCampWithRestaurantFirstOrder,
			HashMap<String, String> createEDVOCampWithThirtyDaysDormant,
			HashMap<String, String> createPublicTypeEDVOCamp) {
		Processor CampWithUserRestriction = edvoHelper.createEDVOTradeDiscount(createEDVOCampWithUserRestriction);
		int statusCode = CampWithUserRestriction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = CampWithUserRestriction.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = CampWithUserRestriction.ResponseValidator.GetNodeValueAsInt("$.data");
		softAssert.assertEquals(statusCode, 0);
		softAssert.assertEquals(statusMessage, "success");

		Processor CampWithSwiggyFirstOrder = edvoHelper.createEDVOTradeDiscount(createEDVOCampWithSwiggyFirstOrder);
		int statusCode2 = CampWithSwiggyFirstOrder.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage2 = CampWithSwiggyFirstOrder.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId2 = CampWithSwiggyFirstOrder.ResponseValidator.GetNodeValueAsInt("$.data");
		softAssert.assertEquals(statusCode2, 0);
		softAssert.assertEquals(statusMessage2, "success");

		Processor CampWithRestaurantFirstOrder = edvoHelper
				.createEDVOTradeDiscount(createEDVOCampWithRestaurantFirstOrder);
		int statusCode3 = CampWithRestaurantFirstOrder.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage3 = CampWithRestaurantFirstOrder.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId3 = CampWithRestaurantFirstOrder.ResponseValidator.GetNodeValueAsInt("$.data");
		softAssert.assertEquals(statusCode3, 0);
		softAssert.assertEquals(statusMessage3, "success");

		Processor CampWithThirtyDaysDormant = edvoHelper.createEDVOTradeDiscount(createEDVOCampWithThirtyDaysDormant);
		int statusCode4 = CampWithThirtyDaysDormant.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage4 = CampWithThirtyDaysDormant.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId4 = CampWithThirtyDaysDormant.ResponseValidator.GetNodeValueAsInt("$.data");
		softAssert.assertEquals(statusCode4, 0);
		softAssert.assertEquals(statusMessage4, "success");

		Processor publicTypeEDVOCamp = edvoHelper.createEDVOTradeDiscount(createPublicTypeEDVOCamp);
		int statusCode5 = publicTypeEDVOCamp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage5 = publicTypeEDVOCamp.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId5 = publicTypeEDVOCamp.ResponseValidator.GetNodeValueAsInt("$.data");
		softAssert.assertEquals(statusCode5, 0);
		softAssert.assertEquals(statusMessage5, "success");
		System.out.println("Campaigns Ids------>" + campaignId + ", " + campaignId2 + ", " + campaignId3 + ", "
				+ campaignId4 + ", " + campaignId5);

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId2));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId3));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId4));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId5));
	}

	@Test(dataProvider = "evaluateMealForMultipleMealIdsOnWhichCampaignIsRunningData", dataProviderClass = EdvoDP.class)
	public void evaluateMealForMultipleMealIdsOnWhichCampaignIsRunningTest(HashMap<String, String> edvoData,
																		   HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		int campaignIdInMealResponse = mealResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String mealHeader = mealResponse.ResponseValidator.GetNodeValue("$.data.meals[0].mealMeta.mealHeader");
		String mealDescription = mealResponse.ResponseValidator
				.GetNodeValue("$.data.meals[0].mealMeta.mealDescription");
		Assert.assertEquals(campaignIdInMealResponse, campaignId);
		softAssert.assertEquals(mealHeader, "buy G1 at FINAL Price 120 ");
		softAssert.assertEquals(mealDescription, "buy G1 at FINAL Price 120 ");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealForWithOneMealIdOnWhichCampaignIsRunningData", dataProviderClass = EdvoDP.class)
	public void evaluateMealForWithOneMealIdOnWhichCampaignIsRunningTest(HashMap<String, String> edvoData,
																		 HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		int campaignIdInMealResponse = mealResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String mealHeader = mealResponse.ResponseValidator.GetNodeValue("$.data.meals[0].mealMeta.mealHeader");
		String mealDescription = mealResponse.ResponseValidator
				.GetNodeValue("$.data.meals[0].mealMeta.mealDescription");
		Assert.assertEquals(campaignIdInMealResponse, campaignId);
		softAssert.assertEquals(mealHeader, "buy G1 at FINAL Price 120 ");
		softAssert.assertEquals(mealDescription, "buy G1 at FINAL Price 120 ");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealForFirstOrderRestrictionData", dataProviderClass = EdvoDP.class)
	public void evaluateMealForFirstOrderRestrictionTest(HashMap<String, String> edvoData,
														 HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		int campaignIdInMealResponse = mealResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String mealHeader = mealResponse.ResponseValidator.GetNodeValue("$.data.meals[0].mealMeta.mealHeader");
		String mealDescription = mealResponse.ResponseValidator
				.GetNodeValue("$.data.meals[0].mealMeta.mealDescription");
		Assert.assertEquals(campaignIdInMealResponse, campaignId);
		softAssert.assertEquals(mealHeader, "buy G1 at FINAL Price 120 ");
		softAssert.assertEquals(mealDescription, "buy G1 at FINAL Price 120 ");
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWithFirstOrderFalseData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWithFirstOrderFalseTest(HashMap<String, String> edvoData,
													HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWhereMealIdIsZeroData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWhereMealIdIsZeroTest(HashMap<String, String> edvoData,
												  HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWhereMealIdIsNullData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWhereMealIdIsNullTest(HashMap<String, String> edvoData,
												  HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertEquals(data, "element cannot be mapped to a null key");
		int statusCodeOfMeal = mealResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfMeal, 2);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWhereMealIdIsNonExistingMealIdData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWhereMealIdIsNonExistingMealIdTest(HashMap<String, String> edvoData,
															   HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWhereRestIdIsZeroData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWhereRestIdIsZeroTest(HashMap<String, String> edvoData,
												  HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWhereRestIdIsNullData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWhereRestIdIsNullTest(HashMap<String, String> edvoData,
												  HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWhereRestIdIsNonExistingIdData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWhereRestIdIsNonExistingIdTest(HashMap<String, String> edvoData,
														   HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingRestIdAsMealIdData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingRestIdAsMealIdTest(HashMap<String, String> edvoData,
														   HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingMealIdAsRestIdData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingMealIdAsRestIdTest(HashMap<String, String> edvoData,
														   HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingMealIdOfTheExpiredCampaignData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingMealIdOfTheExpiredCampaignTest(HashMap<String, String> edvoData,
																	   HashMap<String, String> mealPayload) throws InterruptedException {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Thread.sleep(15000);
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingSupportedIOSVersionData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingSupportedIOSVersionTest(HashMap<String, String> edvoData,
																HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		int campaignIdInMealResponse = mealResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String mealHeader = mealResponse.ResponseValidator.GetNodeValue("$.data.meals[0].mealMeta.mealHeader");
		String mealDescription = mealResponse.ResponseValidator
				.GetNodeValue("$.data.meals[0].mealMeta.mealDescription");
		Assert.assertEquals(campaignIdInMealResponse, campaignId);
		softAssert.assertEquals(mealHeader, "buy G1 at FINAL Price 120 ");
		softAssert.assertEquals(mealDescription, "buy G1 at FINAL Price 120 ");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingNonSupportedIOSVersionData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingNonSupportedIOSVersionTest(HashMap<String, String> edvoData,
																   HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingSupportedAndroidVersionData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingSupportedAndroidVersionData(HashMap<String, String> edvoData,
																	HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		int campaignIdInMealResponse = mealResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String mealHeader = mealResponse.ResponseValidator.GetNodeValue("$.data.meals[0].mealMeta.mealHeader");
		String mealDescription = mealResponse.ResponseValidator
				.GetNodeValue("$.data.meals[0].mealMeta.mealDescription");
		Assert.assertEquals(campaignIdInMealResponse, campaignId);
		softAssert.assertEquals(mealHeader, "buy G1 at FINAL Price 120 ");
		softAssert.assertEquals(mealDescription, "buy G1 at FINAL Price 120 ");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingNonSupportedAndroidVersionData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingNonSupportedAndroidVersionTest(HashMap<String, String> edvoData,
																	   HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

//	@Test(dataProvider = "evaluateMealWherePassingInvalidInputForUserAgentVersionData", dataProviderClass = EdvoDP.class)
//	public void evaluateMealWherePassingInvalidInputForUserAgentVersionTest(HashMap<String, String> edvoData,
//			HashMap<String, String> mealPayload) {
//		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
//		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
//		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
//		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
//		Assert.assertEquals(statusCode, 0);
//		Assert.assertEquals(statusMessage, "success");
//		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
//		int status = mealResponse.ResponseValidator.GetNodeValueAsInt("$.status");
//		Assert.assertEquals(status, 400);
//		String errorMessage = mealResponse.ResponseValidator.GetNodeValue("$.error");
//		Assert.assertEquals(errorMessage, "Bad Request");
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
//	}

	@Test(dataProvider = "evaluateMealWherePassingInvalidUserAgentVersionData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingInvalidUserAgentVersionTest(HashMap<String, String> edvoData,
																	HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		int status = mealResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		Assert.assertEquals(status, 400);
		String errorMessage = mealResponse.ResponseValidator.GetNodeValue("$.error");
		Assert.assertEquals(errorMessage, "Bad Request");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingInvalidVersionCodeForAndroidData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingInvalidVersionCodeForAndroidTest(HashMap<String, String> edvoData,
																		 HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingInvalidVersionCodeForIOSData", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingInvalidVersionCodeForIOSTest(HashMap<String, String> edvoData,
																	 HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		String data = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(data.equalsIgnoreCase("[]"), true);
		String dataRestaurant = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants");
		Assert.assertEquals(dataRestaurant.equalsIgnoreCase("[]"), true);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingInvalidUserAgentWhenVersionIs229Data", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingInvalidUserAgentWhenVersionIs229Test(HashMap<String, String> edvoData,
																			 HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		int status = mealResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		Assert.assertEquals(status, 400);
		String errorMessage = mealResponse.ResponseValidator.GetNodeValue("$.error");
		Assert.assertEquals(errorMessage, "Bad Request");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealWherePassingInvalidUserAgentWhenVersionIs200Data", dataProviderClass = EdvoDP.class)
	public void evaluateMealWherePassingInvalidUserAgentWhenVersionIs200Test(HashMap<String, String> edvoData,
																			 HashMap<String, String> mealPayload) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		int status = mealResponse.ResponseValidator.GetNodeValueAsInt("$.status");
		Assert.assertEquals(status, 400);
		String errorMessage = mealResponse.ResponseValidator.GetNodeValue("$.error");
		Assert.assertEquals(errorMessage, "Bad Request");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
	}

	@Test(dataProvider = "evaluateMealCheckForMultiTDWith30DaysDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateMealCheckForMultiTDWith30DaysDormantData(
			HashMap<String, String> createEDVOCampFirstOrderRestriction,
			HashMap<String, String> createEDVOCampRestaurantFirstOrderRestriction,
			HashMap<String, String> createEDVOPublicTypeCamp, HashMap<String, String> createEDVOCampFor30DaysDormant,
			HashMap<String, String> createEDVOCampForMappedUser, HashMap<String, String> mealPayload) {

		Processor createResponseOfFirstOrderRestriction = edvoHelper
				.createEDVOTradeDiscount(createEDVOCampFirstOrderRestriction);
		int statusCode = createResponseOfFirstOrderRestriction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = createResponseOfFirstOrderRestriction.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId = createResponseOfFirstOrderRestriction.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");

		Processor createResponseOfRestaurantFirstOrderRestriction = edvoHelper
				.createEDVOTradeDiscount(createEDVOCampRestaurantFirstOrderRestriction);
		int statusCode2 = createResponseOfRestaurantFirstOrderRestriction.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		String statusMessage2 = createResponseOfRestaurantFirstOrderRestriction.ResponseValidator
				.GetNodeValue("$.statusMessage");
		int campaignId2 = createResponseOfRestaurantFirstOrderRestriction.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode2, 0);
		Assert.assertEquals(statusMessage2, "success");

		Processor createResponseOfPublicCampaign = edvoHelper.createEDVOTradeDiscount(createEDVOPublicTypeCamp);
		int statusCode3 = createResponseOfPublicCampaign.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage3 = createResponseOfPublicCampaign.ResponseValidator.GetNodeValue("$.statusMessage");
		int campaignId3 = createResponseOfPublicCampaign.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode3, 0);
		Assert.assertEquals(statusMessage3, "success");

		// evaluate for createEDVOCampFirstOrderRestriction
		if (createEDVOCampFirstOrderRestriction.get("17").equalsIgnoreCase("true")
				|| createEDVOCampRestaurantFirstOrderRestriction.get("20").equalsIgnoreCase("true")) {
			mealPayload.put("2", "true");
			mealPayload.put("5", "123");
		}
		Processor mealResponse = edvoHelper.mealEvaluate(mealPayload);
		int campaignIdInMealResponse = mealResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String mealHeader = mealResponse.ResponseValidator.GetNodeValue("$.data.meals[0].mealMeta.mealHeader");
		String mealDescription = mealResponse.ResponseValidator
				.GetNodeValue("$.data.meals[0].mealMeta.mealDescription");
		softAssert.assertEquals(campaignIdInMealResponse, campaignId);
		softAssert.assertEquals(mealHeader, "buy G1 at FINAL Price 120 ");
		softAssert.assertEquals(mealDescription, "buy G1 at FINAL Price 120 ");

		// evaluate for createEDVOCampRestaurantFirstOrderRestriction
		mealPayload.put("2", "false");
		Processor mealResponse2 = edvoHelper.mealEvaluate(mealPayload);
		int campaignIdInMealResponse2 = mealResponse2.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String mealHeader2 = mealResponse2.ResponseValidator.GetNodeValue("$.data.meals[0].mealMeta.mealHeader");
		String mealDescription2 = mealResponse2.ResponseValidator
				.GetNodeValue("$.data.meals[0].mealMeta.mealDescription");
		softAssert.assertEquals(campaignIdInMealResponse2, campaignId2);
		softAssert.assertEquals(mealHeader2, "buy G1 at FINAL Price 120 ");
		softAssert.assertEquals(mealDescription2, "buy G1 at FINAL Price 120 ");

		// evaluate for public campaign
		if (!(createEDVOCampFirstOrderRestriction.get("17").equalsIgnoreCase("true")
				&& createEDVOCampRestaurantFirstOrderRestriction.get("20").equalsIgnoreCase("true")
				&& createEDVOCampFor30DaysDormant.get("19").equalsIgnoreCase("THIRTY_DAYS_DORMATN")
				&& createEDVOCampForMappedUser.get("21").equalsIgnoreCase("true"))) {
			mealPayload.put("2", "false");
			mealPayload.put("5", "0");
		}
		Processor mealResponse3 = edvoHelper.mealEvaluate(mealPayload);
		int campaignIdInMealResponse3 = mealResponse3.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String mealHeader3 = mealResponse3.ResponseValidator.GetNodeValue("$.data.meals[0].mealMeta.mealHeader");
		String mealDescription3 = mealResponse3.ResponseValidator
				.GetNodeValue("$.data.meals[0].mealMeta.mealDescription");
		softAssert.assertEquals(campaignIdInMealResponse3, campaignId3);
		softAssert.assertEquals(mealHeader3, "buy G1 at FINAL Price 120 ");
		softAssert.assertEquals(mealDescription3, "buy G1 at FINAL Price 120 ");

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId2));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campaignId3));

	}

	@Test(dataProvider = "evaluateCartWithTwoRestIdsWhereInOfTheRestIdsCampIsActiveData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithTwoRestIdsWhereInOfTheRestIdsCampIsActiveTest(HashMap<String, String> edvoData,
																			  HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWhereThreeItemsOfGroupIsAddedAndCampaignIsForTwoItemsOfGroupData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWhereThreeItemsOfGroupIsAddedAndCampaignIsForTwoItemsOfGroupTest(
			HashMap<String, String> edvoData, HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "50.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithOneMealRequestData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithOneMealRequestTest(HashMap<String, String> edvoData,
												   HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "40.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithMultipleMealRequestData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithMultipleMealRequestTest(HashMap<String, String> edvoData,
														HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "60.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWhereMealItemRequestsRequestIsEmptyData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWhereMealItemRequestsRequestIsEmptyTest(HashMap<String, String> edvoData,
																	HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		// int campIdInResponse=
		// cartV3Response.ResponseValidator.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		// String discountType =
		// cartV3Response.ResponseValidator.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		String meals = cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals");
		Assert.assertEquals(meals, "[]");
		// Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(cartTradeDiscount, "0.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithTheMealIdOnWhichNoCampaignIsActiveData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithTheMealIdOnWhichNoCampaignIsActiveTest(HashMap<String, String> edvoData,
																	   HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String mealsMeta = cartV3Response.ResponseValidator.GetNodeValue("$.data.meals[0].mealMeta");
		Assert.assertEquals(mealsMeta, null);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWhenActiveCampaigneIsGroupLevelData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWhenActiveCampaigneIsGroupLevelTest(HashMap<String, String> edvoData,
																HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "102.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithThatGroupIdForWhichCampaignIsExpiredData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithThatGroupIdForWhichCampaignIsExpiredTest(HashMap<String, String> edvoData,
																		 HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		String tradeDiscountInfoInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
		Assert.assertEquals(tradeDiscountInfoInResponse, "[]");
		Assert.assertEquals(cartTradeDiscount, "0.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithCombinationOfTwoMealIdsInOneOfWhichCampaignIsActiveAndAnothOneNoActiveCampaignData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithCombinationOfTwoMealIdsInOneOfWhichCampaignIsActiveAndAnothOneNoActiveCampaignTest(
			HashMap<String, String> edvoData, HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "22.5");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsWhereMaxIdsAreCampaignRunningMealIDsData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsWhereMaxIdsAreCampaignRunningMealIDsTest(
			HashMap<String, String> edvoData, HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "398.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsWhereMaxIdsAreCampaignRunningGroupIDsData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsWhereMaxIdsAreCampaignRunningGroupIDsTest(
			HashMap<String, String> edvoData, HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "197.0");
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsOnlyData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithTheCampaignRunningMealIdsAndGroupIdsOnlyTest(HashMap<String, String> edvoData,
																			 HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "663.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartForGettingRewardInOneItemIfTwoItemsAddedData", dataProviderClass = EdvoDP.class)
	public void evaluateCartForGettingRewardInOneItemIfTwoItemsAddedTest(HashMap<String, String> edvoData,
																		 HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "40.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithItemIdNullData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithItemIdNullTest(HashMap<String, String> edvoData,
											   HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "40.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithRestIdOnWhichCampaignIsActiveButMealIdIsDifferentData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithRestIdOnWhichCampaignIsActiveButMealIdIsDifferentTest(HashMap<String, String> edvoData,
																					  HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String restTradeDiscount = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].mealList[0].restaurantTradeDiscount");
		String tradeDiscountInfo = String.valueOf(cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo"));
		Assert.assertEquals(tradeDiscountInfo, "[]");
		Assert.assertEquals(restTradeDiscount, "0.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithRestIdOnWhichCampaignIsNotActiveButMealIdIsOnWhichCampIsActiveData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithRestIdOnWhichCampaignIsNotActiveButMealIdIsOnWhichCampIsActiveTest(
			HashMap<String, String> edvoData, HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String restTradeDiscount = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].mealList[0].restaurantTradeDiscount");
		String tradeDiscountInfo = String.valueOf(cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo"));
		Assert.assertEquals(tradeDiscountInfo, "[]");
		Assert.assertEquals(restTradeDiscount, "0.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartAndCheckMealLevelRewardIsGettingWhenInCampaignRewardIsSetOnBothMealAndGroupLevelData", dataProviderClass = EdvoDP.class)
	public void evaluateCartAndCheckMealLevelRewardIsGettingWhenInCampaignRewardIsSetOnBothMealAndGroupLevelTest(
			HashMap<String, String> edvoData, HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "500.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWhenItemPriceIsNullData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWhenItemPriceIsNullTest(HashMap<String, String> edvoData,
													HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "0.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartFor30DaysDormantUserData", dataProviderClass = EdvoDP.class)
	public void evaluateCartFor30DaysDormantUserTest(HashMap<String, String> edvoData,
													 HashMap<String, String> cartV3Payload, String userId, String restId, String dormantDays) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "380.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartFor60DaysDormantUserData", dataProviderClass = EdvoDP.class)
	public void evaluateCartFor60DaysDormantUserTest(HashMap<String, String> edvoData,
													 HashMap<String, String> cartV3Payload, String userId, String restId, String dormantDays) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "380.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartFor90DaysDormantUserData", dataProviderClass = EdvoDP.class)
	public void evaluateCartFor90DaysDormantUserTest(HashMap<String, String> edvoData,
													 HashMap<String, String> cartV3Payload, String userId, String restId, String dormantDays) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "380.0");

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithRestIdOnWhichCampaignIsActiveButGroupIdIsDifferentData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithRestIdOnWhichCampaignIsActiveButGroupIdIsDifferentTest(HashMap<String, String> edvoData,
																					   HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscount = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "0.0");
		Assert.assertEquals(restTradeDiscount, "0.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartForFirstorderRestrictionData", dataProviderClass = EdvoDP.class)
	public void evaluateCartForFirstorderRestrictionTest(HashMap<String, String> edvoData,
														 HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscount = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		Assert.assertEquals(campIdInResponse, campId);
		Assert.assertEquals(discountType, "BXGY");
		Assert.assertEquals(cartTradeDiscount, "120.0");
		Assert.assertEquals(restTradeDiscount, "120.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithFirstorderRestrictioFalseAndCampIsForFirstOrderTrueData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithFirstorderRestrictioFalseAndCampIsForFirstOrderTrueTest(
			HashMap<String, String> edvoData, HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		String tradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
		String restauranttradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.restaurantTradeDiscount");
		String rewardType = cartV3Response.ResponseValidator.GetNodeValue("$.data.meals[0].rewardType");

		Assert.assertEquals(tradeDiscountInfo, "[]");
		Assert.assertEquals(rewardType, null);
		Assert.assertEquals(cartTradeDiscount, "0.0");
		Assert.assertEquals(restauranttradeDiscountInfo, "0.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithFirstorderRestrictionTrueWhereRestIdIsDiffButMealIdAndGroupIdIsCampRunningData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithFirstorderRestrictionTrueWhereRestIdIsDiffButMealIdAndGroupIdIsCampRunningTest(
			HashMap<String, String> edvoData, HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		String tradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
		String restauranttradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.restaurantTradeDiscount");
		String rewardType = cartV3Response.ResponseValidator.GetNodeValue("$.data.meals[0].rewardType");
		Assert.assertEquals(tradeDiscountInfo, "[]");
		Assert.assertEquals(rewardType, null);
		Assert.assertEquals(cartTradeDiscount, "0.0");
		Assert.assertEquals(restauranttradeDiscountInfo, "0.0");

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartWithFirstorderRestrictionTrueWhereMealIdIsDiffButRestIdAndGroupIdIsCampRunningData", dataProviderClass = EdvoDP.class)
	public void evaluateCartWithFirstorderRestrictionTrueWhereMealIdIsDiffButRestIdAndGroupIdIsCampRunningTest(
			HashMap<String, String> edvoData, HashMap<String, String> cartV3Payload) {
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		String tradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
		String restauranttradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.restaurantTradeDiscount");
		String rewardType = cartV3Response.ResponseValidator.GetNodeValue("$.data.meals[0].rewardType");
		Assert.assertEquals(tradeDiscountInfo, "[]");
		Assert.assertEquals(rewardType, null);
		Assert.assertEquals(cartTradeDiscount, "0.0");
		Assert.assertEquals(restauranttradeDiscountInfo, "0.0");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartForUserCutWith30DaysDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateCartForUserCutWith30DaysDormantTest(HashMap<String, String> edvo30DaysDormantData,
															HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
															HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
															HashMap<String, String> cartPayloadFor30Days, HashMap<String, String> cartPayloadFirstOrder,
															HashMap<String, String> cartPayloadRestFirstOrder, HashMap<String, String> cartPayloadRandomUser,
															HashMap<String, String> cartPayloadPublicUser, String userId, String restId, String dormantDays) {
		// 30-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvo30DaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		Assert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// cart evaluate for 30-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartPayloadFor30Days);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscount = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponse, campId);
		softAssert.assertEquals(discountType, "BXGY");
		softAssert.assertEquals(cartTradeDiscount, "120.0");
		softAssert.assertEquals(restTradeDiscount, "120.0");

		// // cart evaluate for public user
		Processor cartV3ResponsePublicUser = edvoHelper.cartV3Evaluate(cartPayloadPublicUser);
		String cartTradeDiscountPublicUser = String.valueOf(cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponsePublicUser = cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypePublicUser = cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountPublicUser = cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponsePublicUser, createEDVOPublicCampCampId);
		softAssert.assertEquals(discountTypePublicUser, "BXGY");

		System.out.println("td  " + cartTradeDiscountPublicUser);
		// TODO need to check
		softAssert.assertEquals(cartTradeDiscountPublicUser, "50.0");
		softAssert.assertEquals(restTradeDiscountPublicUser, "50.0");

		// // cart evaluate for 1st Order
		Processor cartV3ResponseFirstOrder = edvoHelper.cartV3Evaluate(cartPayloadFirstOrder);
		String cartTradeDiscountFirstOrder = String.valueOf(cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseFirstOrder = cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeFirstOrder = cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountFirstOrder = cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponseFirstOrder, edvoFirstOrderDataCampId);
		softAssert.assertEquals(discountTypeFirstOrder, "BXGY");
		softAssert.assertEquals(cartTradeDiscountFirstOrder, "301.0");
		softAssert.assertEquals(restTradeDiscountFirstOrder, "301.0");

		// cart evaluate for restaurant 1st Order
		Processor cartV3ResponseRestFirstOrder = edvoHelper.cartV3Evaluate(cartPayloadRestFirstOrder);
		String cartTradeDiscountRestFirstOrder = String.valueOf(cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseRestFirstOrder = cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeRestFirstOrder = cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountRestFirstOrder = cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponseRestFirstOrder, edvoCampRestFirstOrderCampId);
		softAssert.assertEquals(discountTypeRestFirstOrder, "BXGY");
		softAssert.assertEquals(cartTradeDiscountRestFirstOrder, "280.0");
		softAssert.assertEquals(restTradeDiscountRestFirstOrder, "280.0");

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor cartV3ResponseRandomUser = edvoHelper.cartV3Evaluate(cartPayloadRandomUser);
		String cartTradeDiscountMappedUser = String.valueOf(cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseMappedUser = cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeMappedUser = cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountMappedUser = cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponseMappedUser, edvoCampRestFirstOrderCampId);
		softAssert.assertEquals(discountTypeMappedUser, "BXGY");
		softAssert.assertEquals(cartTradeDiscountMappedUser, "280.0");
		softAssert.assertEquals(restTradeDiscountMappedUser, "280.0");
		softAssert.assertAll();

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdInResponsePublicUser));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampMappedUserResponseCampId));

	}

	@Test(dataProvider = "evaluateCartForUserCutWith60DaysDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateCartForUserCutWith60DaysDormantTest(HashMap<String, String> edvo60DaysDormantData,
															HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
															HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
															HashMap<String, String> cartPayloadFor60Days, HashMap<String, String> cartPayloadFirstOrder,
															HashMap<String, String> cartPayloadRestFirstOrder, HashMap<String, String> cartPayloadRandomUser,
															HashMap<String, String> cartPayloadPublicUser, String userId, String restId, String dormantDays) {
		// 30-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvo60DaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		Assert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// cart evaluate for 30-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartPayloadFor60Days);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscount = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		// TODO check softAssert
		softAssert.assertEquals(campIdInResponse, campId);
		softAssert.assertEquals(discountType, "BXGY");
		softAssert.assertEquals(cartTradeDiscount, "120.0");
		softAssert.assertEquals(restTradeDiscount, "120.0");

		// // cart evaluate for public user
		Processor cartV3ResponsePublicUser = edvoHelper.cartV3Evaluate(cartPayloadPublicUser);
		String cartTradeDiscountPublicUser = String.valueOf(cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponsePublicUser = cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypePublicUser = cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountPublicUser = cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponsePublicUser, createEDVOPublicCampCampId);
		softAssert.assertEquals(discountTypePublicUser, "BXGY");

		System.out.println("td  " + cartTradeDiscountPublicUser);
		// TODO need to check
		softAssert.assertEquals(cartTradeDiscountPublicUser, "50.0");
		softAssert.assertEquals(restTradeDiscountPublicUser, "50.0");

		// // cart evaluate for 1st Order
		Processor cartV3ResponseFirstOrder = edvoHelper.cartV3Evaluate(cartPayloadFirstOrder);
		String cartTradeDiscountFirstOrder = String.valueOf(cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseFirstOrder = cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeFirstOrder = cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountFirstOrder = cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponseFirstOrder, edvoFirstOrderDataCampId);
		softAssert.assertEquals(discountTypeFirstOrder, "BXGY");
		softAssert.assertEquals(cartTradeDiscountFirstOrder, "301.0");
		softAssert.assertEquals(restTradeDiscountFirstOrder, "301.0");

		// cart evaluate for restaurant 1st Order
		Processor cartV3ResponseRestFirstOrder = edvoHelper.cartV3Evaluate(cartPayloadRestFirstOrder);
		String cartTradeDiscountRestFirstOrder = String.valueOf(cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseRestFirstOrder = cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeRestFirstOrder = cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountRestFirstOrder = cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponseRestFirstOrder, edvoCampRestFirstOrderCampId);
		softAssert.assertEquals(discountTypeRestFirstOrder, "BXGY");
		softAssert.assertEquals(cartTradeDiscountRestFirstOrder, "280.0");
		softAssert.assertEquals(restTradeDiscountRestFirstOrder, "280.0");

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor cartV3ResponseRandomUser = edvoHelper.cartV3Evaluate(cartPayloadRandomUser);
		String cartTradeDiscountMappedUser = String.valueOf(cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseMappedUser = cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeMappedUser = cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountMappedUser = cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponseMappedUser, edvoCampRestFirstOrderCampId);
		softAssert.assertEquals(discountTypeMappedUser, "BXGY");
		softAssert.assertEquals(cartTradeDiscountMappedUser, "280.0");
		softAssert.assertEquals(restTradeDiscountMappedUser, "280.0");
		softAssert.assertAll();

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdInResponsePublicUser));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampMappedUserResponseCampId));

	}

	@Test(dataProvider = "evaluateCartForUserCutWith90DaysDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateCartForUserCutWith90DaysDormantTest(HashMap<String, String> edvo90DaysDormantData,
															HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
															HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
															HashMap<String, String> cartPayloadFor90Days, HashMap<String, String> cartPayloadFirstOrder,
															HashMap<String, String> cartPayloadRestFirstOrder, HashMap<String, String> cartPayloadRandomUser,
															HashMap<String, String> cartPayloadPublicUser, String userId, String restId, String dormantDays) {
		// 30-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvo90DaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		Assert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// cart evaluate for 30-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartPayloadFor90Days);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscount = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		// TODO check softAssert
		softAssert.assertEquals(campIdInResponse, campId);
		softAssert.assertEquals(discountType, "BXGY");
		softAssert.assertEquals(cartTradeDiscount, "120.0");
		softAssert.assertEquals(restTradeDiscount, "120.0");

		// // cart evaluate for public user
		Processor cartV3ResponsePublicUser = edvoHelper.cartV3Evaluate(cartPayloadPublicUser);
		String cartTradeDiscountPublicUser = String.valueOf(cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponsePublicUser = cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypePublicUser = cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountPublicUser = cartV3ResponsePublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponsePublicUser, createEDVOPublicCampCampId);
		softAssert.assertEquals(discountTypePublicUser, "BXGY");

		System.out.println("td  " + cartTradeDiscountPublicUser);
		// TODO need to check
		softAssert.assertEquals(cartTradeDiscountPublicUser, "50.0");
		softAssert.assertEquals(restTradeDiscountPublicUser, "50.0");

		// // cart evaluate for 1st Order
		Processor cartV3ResponseFirstOrder = edvoHelper.cartV3Evaluate(cartPayloadFirstOrder);
		String cartTradeDiscountFirstOrder = String.valueOf(cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseFirstOrder = cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeFirstOrder = cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountFirstOrder = cartV3ResponseFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponseFirstOrder, edvoFirstOrderDataCampId);
		softAssert.assertEquals(discountTypeFirstOrder, "BXGY");
		softAssert.assertEquals(cartTradeDiscountFirstOrder, "301.0");
		softAssert.assertEquals(restTradeDiscountFirstOrder, "301.0");

		// cart evaluate for restaurant 1st Order
		Processor cartV3ResponseRestFirstOrder = edvoHelper.cartV3Evaluate(cartPayloadRestFirstOrder);
		String cartTradeDiscountRestFirstOrder = String.valueOf(cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseRestFirstOrder = cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeRestFirstOrder = cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountRestFirstOrder = cartV3ResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponseRestFirstOrder, edvoCampRestFirstOrderCampId);
		softAssert.assertEquals(discountTypeRestFirstOrder, "BXGY");
		softAssert.assertEquals(cartTradeDiscountRestFirstOrder, "280.0");
		softAssert.assertEquals(restTradeDiscountRestFirstOrder, "280.0");

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor cartV3ResponseRandomUser = edvoHelper.cartV3Evaluate(cartPayloadRandomUser);
		String cartTradeDiscountMappedUser = String.valueOf(cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseMappedUser = cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeMappedUser = cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountMappedUser = cartV3ResponseRandomUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		softAssert.assertEquals(campIdInResponseMappedUser, edvoCampRestFirstOrderCampId);
		softAssert.assertEquals(discountTypeMappedUser, "BXGY");
		softAssert.assertEquals(cartTradeDiscountMappedUser, "280.0");
		softAssert.assertEquals(restTradeDiscountMappedUser, "280.0");
		softAssert.assertAll();

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdInResponsePublicUser));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampMappedUserResponseCampId));

	}

	@Test(dataProvider = "evaluateCartForUser3060And90DaysDormantWhenActiveCampFor30DaysData", dataProviderClass = EdvoDP.class)
	public void evaluateCartForUser3060And90DaysDormantWhenActiveCampFor30DaysTest(
			HashMap<String, String> edvo30DaysDormantData, HashMap<String, String> cartPayloadFor30Days,
			HashMap<String, String> cartPayloadFor60Days, HashMap<String, String> cartPayloadFor90Days,
			String userId30Dormant, String userId60Dormant, String userId90Dormant, String restId,
			String thirtyDormantDays, String sixtyDormantDays, String ninetyDormantDays) {
		// 30-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvo30DaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");

		// cart evaluate for 30-days dormant user
		rngHelper.DormantUser(userId30Dormant, restId, thirtyDormantDays);
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartPayloadFor30Days);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponse = cartV3Response.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountType = cartV3Response.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscount = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		// TODO check softAssert
		softAssert.assertEquals(campIdInResponse, campId);
		softAssert.assertEquals(discountType, "BXGY");
		softAssert.assertEquals(cartTradeDiscount, "120.0");
		softAssert.assertEquals(restTradeDiscount, "120.0");

		// cart evaluate for 60-days dormant user
		rngHelper.DormantUser(userId60Dormant, restId, sixtyDormantDays);
		Processor cartV3ResponseSixtyDays = edvoHelper.cartV3Evaluate(cartPayloadFor60Days);
		String cartTradeDiscountSixtyDays = String.valueOf(cartV3ResponseSixtyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseSixtyDays = cartV3ResponseSixtyDays.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeSixtyDays = cartV3ResponseSixtyDays.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountSixtyDays = cartV3ResponseSixtyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		// TODO check softAssert
		softAssert.assertEquals(campIdInResponseSixtyDays, campId);
		softAssert.assertEquals(discountTypeSixtyDays, "BXGY");
		softAssert.assertEquals(cartTradeDiscountSixtyDays, "120.0");
		softAssert.assertEquals(restTradeDiscountSixtyDays, "120.0");

		// cart evaluate for 90-days dormant user
		rngHelper.DormantUser(userId90Dormant, restId, ninetyDormantDays);
		Processor cartV3ResponseNinetyDays = edvoHelper.cartV3Evaluate(cartPayloadFor90Days);
		String cartTradeDiscountNinetyDays = String.valueOf(cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		// TODO check softAssert
		softAssert.assertEquals(campIdInResponseNinetyDays, campId);
		softAssert.assertEquals(discountTypeNinetyDays, "BXGY");
		softAssert.assertEquals(cartTradeDiscountNinetyDays, "120.0");
		softAssert.assertEquals(restTradeDiscountNinetyDays, "120.0");
		softAssert.assertAll();

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartForUser3060And90DaysDormantWhenActiveCampFor60DaysData", dataProviderClass = EdvoDP.class)
	public void evaluateCartForUser3060And90DaysDormantWhenActiveCampFor60DaysTest(
			HashMap<String, String> edvo60DaysDormantData, HashMap<String, String> cartPayloadFor30Days,
			HashMap<String, String> cartPayloadFor60Days, HashMap<String, String> cartPayloadFor90Days,
			String userId30Dormant, String userId60Dormant, String userId90Dormant, String restId,
			String thirtyDormantDays, String sixtyDormantDays, String ninetyDormantDays) {
		// 30-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvo60DaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");

		// cart evaluate for 30-days dormant user
		rngHelper.DormantUser(userId30Dormant, restId, thirtyDormantDays);
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartPayloadFor30Days);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		String tradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
		String restauranttradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.restaurantTradeDiscount");
		String rewardType = cartV3Response.ResponseValidator.GetNodeValue("$.data.meals[0].rewardType");
		// TODO check softAssert
		softAssert.assertEquals(tradeDiscountInfo, "[]");
		softAssert.assertEquals(rewardType, null);
		softAssert.assertEquals(cartTradeDiscount, "0.0");
		softAssert.assertEquals(restauranttradeDiscountInfo, "0.0");

		// cart evaluate for 60-days dormant user
		rngHelper.DormantUser(userId60Dormant, restId, sixtyDormantDays);
		Processor cartV3ResponseSixtyDays = edvoHelper.cartV3Evaluate(cartPayloadFor60Days);
		String cartTradeDiscountSixtyDays = String.valueOf(cartV3ResponseSixtyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseSixtyDays = cartV3ResponseSixtyDays.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeSixtyDays = cartV3ResponseSixtyDays.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountSixtyDays = cartV3ResponseSixtyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		// TODO check softAssert
		softAssert.assertEquals(campIdInResponseSixtyDays, campId);
		softAssert.assertEquals(discountTypeSixtyDays, "BXGY");
		softAssert.assertEquals(cartTradeDiscountSixtyDays, "75.0");
		softAssert.assertEquals(restTradeDiscountSixtyDays, "75.0");

		// cart evaluate for 90-days dormant user
		rngHelper.DormantUser(userId90Dormant, restId, ninetyDormantDays);
		Processor cartV3ResponseNinetyDays = edvoHelper.cartV3Evaluate(cartPayloadFor90Days);
		String cartTradeDiscountNinetyDays = String.valueOf(cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		// TODO check softAssert
		softAssert.assertEquals(campIdInResponseNinetyDays, campId);
		softAssert.assertEquals(discountTypeNinetyDays, "BXGY");
		softAssert.assertEquals(cartTradeDiscountNinetyDays, "75.0");
		softAssert.assertEquals(restTradeDiscountNinetyDays, "75.0");
		softAssert.assertAll();
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateCartForUser3060And90DaysDormantWhenActiveCampFor90DaysData", dataProviderClass = EdvoDP.class)
	public void evaluateCartForUser3060And90DaysDormantWhenActiveCampFor90DaysTest(
			HashMap<String, String> edvo90DaysDormantData, HashMap<String, String> cartPayloadFor30Days,
			HashMap<String, String> cartPayloadFor60Days, HashMap<String, String> cartPayloadFor90Days,
			String userId30Dormant, String userId60Dormant, String userId90Dormant, String restId,
			String thirtyDormantDays, String sixtyDormantDays, String ninetyDormantDays) {
		// 30-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvo90DaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");

		// cart evaluate for 30-days dormant user
		rngHelper.DormantUser(userId30Dormant, restId, thirtyDormantDays);
		Processor cartV3Response = edvoHelper.cartV3Evaluate(cartPayloadFor30Days);
		String cartTradeDiscount = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		String tradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
		String restauranttradeDiscountInfo = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.restaurantTradeDiscount");
		String rewardType = cartV3Response.ResponseValidator.GetNodeValue("$.data.meals[0].rewardType");
		// TODO check softAssert
		softAssert.assertEquals(tradeDiscountInfo, "[]");
		softAssert.assertEquals(rewardType, null);
		softAssert.assertEquals(cartTradeDiscount, "0.0");
		softAssert.assertEquals(restauranttradeDiscountInfo, "0.0");

		// cart evaluate for 60-days dormant user
		rngHelper.DormantUser(userId60Dormant, restId, sixtyDormantDays);
		Processor cartV3ResponseSixtyDays = edvoHelper.cartV3Evaluate(cartPayloadFor60Days);
		String cartTradeDiscountForSixtyDormant = String.valueOf(
				cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		String tradeDiscountInfoForSixtyDormant = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
		String restauranttradeDiscountInfoForSixtyDormant = cartV3Response.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.restaurantTradeDiscount");
		String rewardTypeForSixtyDormant = cartV3Response.ResponseValidator.GetNodeValue("$.data.meals[0].rewardType");
		// TODO check softAssert
		softAssert.assertEquals(tradeDiscountInfoForSixtyDormant, "[]");
		softAssert.assertEquals(rewardTypeForSixtyDormant, null);
		softAssert.assertEquals(cartTradeDiscountForSixtyDormant, "0.0");
		softAssert.assertEquals(restauranttradeDiscountInfoForSixtyDormant, "0.0");

		// cart evaluate for 90-days dormant user
		rngHelper.DormantUser(userId90Dormant, restId, ninetyDormantDays);
		Processor cartV3ResponseNinetyDays = edvoHelper.cartV3Evaluate(cartPayloadFor90Days);
		String cartTradeDiscountNinetyDays = String.valueOf(cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
		int campIdInResponseNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
		String discountTypeNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
		String restTradeDiscountNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
		// TODO check softAssert
		softAssert.assertEquals(campIdInResponseNinetyDays, campId);
		softAssert.assertEquals(discountTypeNinetyDays, "BXGY");
		softAssert.assertEquals(cartTradeDiscountNinetyDays, "225.0");
		softAssert.assertEquals(restTradeDiscountNinetyDays, "225.0");
		softAssert.assertAll();
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	// @Test(dataProvider =
	// "evaluateCartForMinCartAmountWhenActiveCampForThirtyDaysWithSomeMinCartData",
	// dataProviderClass = EdvoDP.class)
	// public void
	// evaluateCartForMinCartAmountWhenActiveCampForThirtyDaysWithSomeMinCartTest(
	// HashMap<String, String> edvo30DaysDormantData, HashMap<String, String>
	// cartPayloadFor30Days,
	// HashMap<String, String> cartPayloadFor60Days, HashMap<String, String>
	// cartPayloadFor90Days,
	// String userId30Dormant, String userId60Dormant, String userId90Dormant,
	// String restId,
	// String thirtyDormantDays, String sixtyDormantDays, String ninetyDormantDays)
	// {
	// // 30-days dormant
	// Processor campResponse =
	// edvoHelper.createEDVOTradeDiscount(edvo30DaysDormantData);
	// int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
	// String campStatusMessage =
	// campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
	// Assert.assertEquals(campStatusMessage, "success");
	//
	// // cart evaluate for 30-days dormant user
	// rngHelper.DormantUser(userId30Dormant, restId, thirtyDormantDays);
	// Processor cartV3Response = edvoHelper.cartV3Evaluate(cartPayloadFor30Days);
	// String cartTradeDiscount = String.valueOf(
	// cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
	// int campIdInResponse = cartV3Response.ResponseValidator
	// .GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
	// String discountType = cartV3Response.ResponseValidator
	// .GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
	// String restTradeDiscount = cartV3Response.ResponseValidator
	// .GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
	// // TODO check softAssert
	// softAssert.assertEquals(campIdInResponse, campId);
	// softAssert.assertEquals(discountType, "BXGY");
	// softAssert.assertEquals(cartTradeDiscount, "120.0");
	// softAssert.assertEquals(restTradeDiscount, "120.0");
	//
	// // cart evaluate for 60-days dormant user
	// rngHelper.DormantUser(userId60Dormant, restId, sixtyDormantDays);
	// Processor cartV3ResponseSixtyDays =
	// edvoHelper.cartV3Evaluate(cartPayloadFor60Days);
	// String cartTradeDiscountSixtyDays =
	// String.valueOf(cartV3ResponseSixtyDays.ResponseValidator
	// .GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
	// int campIdInResponseSixtyDays = cartV3ResponseSixtyDays.ResponseValidator
	// .GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
	// String discountTypeSixtyDays = cartV3ResponseSixtyDays.ResponseValidator
	// .GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
	// String restTradeDiscountSixtyDays = cartV3ResponseSixtyDays.ResponseValidator
	// .GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
	// // TODO check softAssert
	// softAssert.assertEquals(campIdInResponseSixtyDays, campId);
	// softAssert.assertEquals(discountTypeSixtyDays, "BXGY");
	// softAssert.assertEquals(cartTradeDiscountSixtyDays, "120.0");
	// softAssert.assertEquals(restTradeDiscountSixtyDays, "120.0");
	//
	// // cart evaluate for 90-days dormant user
	// rngHelper.DormantUser(userId90Dormant, restId, ninetyDormantDays);
	// Processor cartV3ResponseNinetyDays =
	// edvoHelper.cartV3Evaluate(cartPayloadFor90Days);
	// String cartTradeDiscountNinetyDays =
	// String.valueOf(cartV3ResponseNinetyDays.ResponseValidator
	// .GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount"));
	// int campIdInResponseNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
	// .GetNodeValueAsInt("$.data.meals[0].tradeDiscountInfo[0].campaignId");
	// String discountTypeNinetyDays = cartV3ResponseNinetyDays.ResponseValidator
	// .GetNodeValue("$.data.meals[0].tradeDiscountInfo[0].discountType");
	// String restTradeDiscountNinetyDays =
	// cartV3ResponseNinetyDays.ResponseValidator
	// .GetNodeValueAsStringFromJsonArray("$.data.meals[0].restaurantTradeDiscount");
	// // TODO check softAssert
	// softAssert.assertEquals(campIdInResponseNinetyDays, campId);
	// softAssert.assertEquals(discountTypeNinetyDays, "BXGY");
	// softAssert.assertEquals(cartTradeDiscountNinetyDays, "120.0");
	// softAssert.assertEquals(restTradeDiscountNinetyDays, "120.0");
	// softAssert.assertAll();
	// //edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	// }

	@Test(dataProvider = "evaluateListingForActiveCamapignData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForActiveCamapignTest(HashMap<String, String> createCampData,
													 HashMap<String, String> listingData) {
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createCampData);
		int campId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		Processor listEvaluateResponse = rngHelper.listEvalulate(listingData.get("0"), listingData.get("1"),
				listingData.get("2"), listingData.get("3"), listingData.get("4"));
		//int campIdInListEvaluate = listEvaluateResponse.ResponseValidator
		//		.GetNodeValueAsInt("$.data.tradeDiscountInfo[0].campaignId");
		String campTypeInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId=="+String.valueOf(campId)+")].discountType");
	//	Assert.assertEquals(campIdInListEvaluate, campId);
		Assert.assertEquals(campTypeInListEvaluate, "[\"BXGY\"]");
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));

		String actualCampId=listEvaluateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId=="+String.valueOf(campId)+")].campaignId");
	//	String actualCampId="$.data.discountList..tradeDiscountInfoList.[?(@.campaignId=="+String.valueOf(campId)+")].campaignId";
		Assert.assertEquals(actualCampId,"["+campId+"]");
	}

	@Test(dataProvider = "evaluateListingForAllPossibleTypeOfActiveCampaignData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForAllPossibleTypeOfActiveCampaignTest(HashMap<String, String> createPercentTypeCampData,
																	  HashMap<String, String> createFinalTypeCampData, HashMap<String, String> createFlatTypeCampData,
																	  HashMap<String, String> createFreeTypeCampData, HashMap<String, String> listingData) {

		// create percentage type of camp
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createPercentTypeCampData);
		int campIdOfPercentage = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// create Final type of camp
		Processor createResponseOfFinalType = edvoHelper.createEDVOTradeDiscount(createFinalTypeCampData);
		int campIdOfFinalType = createResponseOfFinalType.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessageOfFinalType = createResponseOfFinalType.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessageOfFinalType, "success");

		// create Flat type of camp
		Processor createResponseOfFlatType = edvoHelper.createEDVOTradeDiscount(createFlatTypeCampData);
		int campIdOfFlatType = createResponseOfFlatType.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessageOfFlatType = createResponseOfFlatType.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessageOfFlatType, "success");

		// create Free type of camp
		Processor createResponseOfFreeType = edvoHelper.createEDVOTradeDiscount(createFreeTypeCampData);
		int campIdOfFreeType = createResponseOfFreeType.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessageOfFreeType = createResponseOfFreeType.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessageOfFreeType, "success");

		Processor listEvaluateResponse = rngHelper.listEvalulate(listingData.get("0"), listingData.get("1"),
				listingData.get("2"), listingData.get("3"), listingData.get("4"));
		String percentageCampIdInListEvaluate =listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[0]..tradeDiscountInfoList..campaignId");
		int percentageCampIdInListEvaluateInt=Integer.parseInt(percentageCampIdInListEvaluate.replace("[","").replace("]",""));
		System.out.print(percentageCampIdInListEvaluateInt);


		String campTypeInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[0].tradeDiscountInfoList[0].discountType");
		String campTypeTwoInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[1].tradeDiscountInfoList[0].discountType");
		String campTypeThreeInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[2].tradeDiscountInfoList[0].discountType");
		String campTypeFourInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[3].tradeDiscountInfoList[0].discountType");

		String finalPriceCampIdInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[1]..tradeDiscountInfoList..campaignId");
		int finalPriceCampIdInListEvaluateInt=Integer.parseInt(finalPriceCampIdInListEvaluate.replace("[","").replace("]",""));

		String flatCampIdInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[2]..tradeDiscountInfoList..campaignId");
		int flatCampIdInListEvaluateInt=Integer.parseInt(flatCampIdInListEvaluate.replace("[","").replace("]",""));

		String freeCampIdInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[3]..tradeDiscountInfoList..campaignId");
		int freeCampIdInListEvaluateInt=Integer.parseInt(freeCampIdInListEvaluate.replace("[","").replace("]",""));


		if (percentageCampIdInListEvaluateInt == campIdOfPercentage || percentageCampIdInListEvaluateInt == campIdOfFinalType
				|| percentageCampIdInListEvaluateInt == campIdOfFlatType
				|| percentageCampIdInListEvaluateInt == campIdOfFreeType) {
			softAssert.assertEquals(campTypeInListEvaluate, "BXGY");
		}

		if (finalPriceCampIdInListEvaluateInt == campIdOfPercentage || finalPriceCampIdInListEvaluateInt == campIdOfFinalType
				|| finalPriceCampIdInListEvaluateInt == campIdOfFlatType
				|| finalPriceCampIdInListEvaluateInt == campIdOfFreeType) {
			softAssert.assertEquals(campTypeTwoInListEvaluate, "BXGY");
		}

		if (flatCampIdInListEvaluateInt == campIdOfPercentage || flatCampIdInListEvaluateInt == campIdOfFinalType
				|| flatCampIdInListEvaluateInt == campIdOfFlatType || flatCampIdInListEvaluateInt == campIdOfFreeType) {
			softAssert.assertEquals(campTypeThreeInListEvaluate, "BXGY");
		}

		if (freeCampIdInListEvaluateInt == campIdOfPercentage || freeCampIdInListEvaluateInt == campIdOfFinalType
				|| freeCampIdInListEvaluateInt == campIdOfFlatType || freeCampIdInListEvaluateInt == campIdOfFreeType) {
			softAssert.assertEquals(campTypeFourInListEvaluate, "BXGY");
		}

		softAssert.assertAll();

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfPercentage));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfFinalType));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfFlatType));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfFreeType));

	}

	@Test(dataProvider = "evaluateListingAndCheckDisableCampaignIsNotGettingData", dataProviderClass = EdvoDP.class)
	public void evaluateListingAndCheckDisableCampaignIsNotGettingTest(
			HashMap<String, String> createPercentTypeCampData, HashMap<String, String> createFinalTypeCampData,
			HashMap<String, String> createFlatTypeCampData, HashMap<String, String> createFreeTypeCampData,
			HashMap<String, String> listingData) {

		// create percentage type of camp
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createPercentTypeCampData);
		int campIdOfPercentage = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// create Final type of camp
		Processor createResponseOfFinalType = edvoHelper.createEDVOTradeDiscount(createFinalTypeCampData);
		int campIdOfFinalType = createResponseOfFinalType.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessageOfFinalType = createResponseOfFinalType.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessageOfFinalType, "success");

		// create Flat type of camp
		Processor createResponseOfFlatType = edvoHelper.createEDVOTradeDiscount(createFlatTypeCampData);
		int campIdOfFlatType = createResponseOfFlatType.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessageOfFlatType = createResponseOfFlatType.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessageOfFlatType, "success");

		// create Free type of camp
		Processor createResponseOfFreeType = edvoHelper.createEDVOTradeDiscount(createFreeTypeCampData);
		int campIdOfFreeType = createResponseOfFreeType.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessageOfFreeType = createResponseOfFreeType.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessageOfFreeType, "success");

		Processor listEvaluateResponse = rngHelper.listEvalulate(listingData.get("0"), listingData.get("1"),
				listingData.get("2"), listingData.get("3"), listingData.get("4"));

		/*String responseOfListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.restaurantIds");*/

		String tradeDiscountInfoListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		String statusCode=listEvaluateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
	//	softAssert.assertEquals(responseOfListEvaluate, "[]");
		softAssert.assertEquals(tradeDiscountInfoListEvaluate, "[]");
		softAssert.assertEquals(statusCode,"0");

		softAssert.assertAll();

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfPercentage));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfFinalType));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfFlatType));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfFreeType));
	}

	@Test(dataProvider = "evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForThrityDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForThrityDormantTest(
			HashMap<String, String> createThrityDormantCampData, HashMap<String, String> listingThirtyDormantData,
			HashMap<String, String> listingSixtyDormantData, HashMap<String, String> listingNinetyDormantData,
			HashMap<String, String> listingPublicData, ArrayList<String> userIds, String restId,
			ArrayList<String> dormantDays) {

		// create 30 days dormant camp
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createThrityDormantCampData);
		int campIdOfThirtyDormant = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// 30 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(0), restId, dormantDays.get(0));

		Processor listEvaluateResponse = rngHelper.listEvalulate(listingThirtyDormantData.get("0"),
				listingThirtyDormantData.get("1"), listingThirtyDormantData.get("2"), listingThirtyDormantData.get("3"),
				listingThirtyDormantData.get("4"));
		int CampIdInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		String campTypeInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");
		softAssert.assertEquals(CampIdInListEvaluate, campIdOfThirtyDormant);
		softAssert.assertEquals(campTypeInListEvaluate, "BXGY");

		// 60 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(1), restId, dormantDays.get(1));

		Processor sixtyDormantlistEvaluateResponse = rngHelper.listEvalulate(listingSixtyDormantData.get("0"),
				listingSixtyDormantData.get("1"), listingSixtyDormantData.get("2"), listingSixtyDormantData.get("3"),
				listingSixtyDormantData.get("4"));
		int sixtyDormantCampaignListEvaluate = sixtyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		String sixtyDormantDiscountTypeListEvaluate = sixtyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		softAssert.assertEquals(sixtyDormantCampaignListEvaluate, campIdOfThirtyDormant);
		softAssert.assertEquals(sixtyDormantDiscountTypeListEvaluate, "BXGY");

		// 90 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(2), restId, dormantDays.get(2));

		Processor ninetyDormantlistEvaluateResponse = rngHelper.listEvalulate(listingSixtyDormantData.get("0"),
				listingSixtyDormantData.get("1"), listingSixtyDormantData.get("2"), listingSixtyDormantData.get("3"),
				listingSixtyDormantData.get("4"));
		int ninetyDormantCampaignListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String ninetyDormantDiscountTypeListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		softAssert.assertEquals(ninetyDormantCampaignListEvaluate, campIdOfThirtyDormant);
		softAssert.assertEquals(ninetyDormantDiscountTypeListEvaluate, "BXGY");

		// public user listing evaluate
		Processor publiclistEvaluateResponse = rngHelper.listEvalulate(listingPublicData.get("0"),
				listingPublicData.get("1"), listingPublicData.get("2"), listingPublicData.get("3"),
				listingPublicData.get("4"));
		String publicResponseOfListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		String publicTradeDiscountInfoListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		softAssert.assertEquals(publicResponseOfListEvaluate, "[]");
		softAssert.assertEquals(publicTradeDiscountInfoListEvaluate, "[]");

		softAssert.assertEquals(campTypeInListEvaluate, "BXGY");

		softAssert.assertAll();

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfThirtyDormant));
	}

	@Test(dataProvider = "evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForSixtyDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForSixtyDormantTest(
			HashMap<String, String> createSixtyDormantCampData, HashMap<String, String> listingThirtyDormantData,
			HashMap<String, String> listingSixtyDormantData, HashMap<String, String> listingNinetyDormantData,
			HashMap<String, String> listingPublicData, ArrayList<String> userIds, String restId,
			ArrayList<String> dormantDays) {

		// create 60 days dormant camp
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createSixtyDormantCampData);
		int campIdOfSixtytyDormant = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// 30 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(0), restId, dormantDays.get(0));

		Processor listEvaluateResponse = rngHelper.listEvalulate(listingThirtyDormantData.get("0"),
				listingThirtyDormantData.get("1"), listingThirtyDormantData.get("2"), listingThirtyDormantData.get("3"),
				listingThirtyDormantData.get("4"));
		// int CampIdInListEvaluate = listEvaluateResponse.ResponseValidator
		// .GetNodeValueAsInt("$.data.tradeDiscountInfo[0].campaignId");

		String thirtyDormantResponseOfListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		String thirtyDormantTradeDiscountInfoListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(thirtyDormantResponseOfListEvaluate, "[]");
		softAssert.assertEquals(thirtyDormantTradeDiscountInfoListEvaluate, "[]");

		// 60 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(1), restId, dormantDays.get(1));

		Processor sixtyDormantlistEvaluateResponse = rngHelper.listEvalulate(listingSixtyDormantData.get("0"),
				listingSixtyDormantData.get("1"), listingSixtyDormantData.get("2"), listingSixtyDormantData.get("3"),
				listingSixtyDormantData.get("4"));
		int sixtyDormantCampaignListEvaluate = sixtyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		String sixtyDormantDiscountTypeListEvaluate = sixtyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		softAssert.assertEquals(sixtyDormantCampaignListEvaluate, campIdOfSixtytyDormant);
		softAssert.assertEquals(sixtyDormantDiscountTypeListEvaluate, "BXGY");

		// 90 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(2), restId, dormantDays.get(2));

		Processor ninetyDormantlistEvaluateResponse = rngHelper.listEvalulate(listingSixtyDormantData.get("0"),
				listingSixtyDormantData.get("1"), listingSixtyDormantData.get("2"), listingSixtyDormantData.get("3"),
				listingSixtyDormantData.get("4"));
		int ninetyDormantCampaignListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String ninetyDormantDiscountTypeListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		softAssert.assertEquals(ninetyDormantCampaignListEvaluate, campIdOfSixtytyDormant);
		softAssert.assertEquals(ninetyDormantDiscountTypeListEvaluate, "BXGY");

		// public user listing evaluate
		Processor publiclistEvaluateResponse = rngHelper.listEvalulate(listingPublicData.get("0"),
				listingPublicData.get("1"), listingPublicData.get("2"), listingPublicData.get("3"),
				listingPublicData.get("4"));
		String publicResponseOfListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		String publicTradeDiscountInfoListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		softAssert.assertEquals(publicResponseOfListEvaluate, "[]");
		softAssert.assertEquals(publicTradeDiscountInfoListEvaluate, "[]");

		softAssert.assertEquals(sixtyDormantDiscountTypeListEvaluate, "BXGY");

		softAssert.assertAll();
		// edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfSixtytyDormant));
	}

	@Test(dataProvider = "evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForNinetyDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateListingWithThirtySixtyNinetyDormantUserWhenActiveCampaignIsForNinetyDormantTest(
			HashMap<String, String> createNinetyDormantCampData, HashMap<String, String> listingThirtyDormantData,
			HashMap<String, String> listingSixtyDormantData, HashMap<String, String> listingNinetyDormantData,
			HashMap<String, String> listingPublicData, ArrayList<String> userIds, String restId,
			ArrayList<String> dormantDays) {

		// create 30 days dormant camp
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createNinetyDormantCampData);
		int campIdOfNinetytyDormant = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// 30 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(0), restId, dormantDays.get(0));

		Processor listEvaluateResponse = rngHelper.listEvalulate(listingThirtyDormantData.get("0"),
				listingThirtyDormantData.get("1"), listingThirtyDormantData.get("2"), listingThirtyDormantData.get("3"),
				listingThirtyDormantData.get("4"));
		// int CampIdInListEvaluate = listEvaluateResponse.ResponseValidator
		// .GetNodeValueAsInt("$.data.tradeDiscountInfo[0].campaignId");

		String thirtyDormantResponseOfListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		String thirtyDormantTradeDiscountInfoListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(thirtyDormantResponseOfListEvaluate, "[]");
		softAssert.assertEquals(thirtyDormantTradeDiscountInfoListEvaluate, "[]");

		// 60 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(1), restId, dormantDays.get(1));

		Processor sixtyDormantlistEvaluateResponse = rngHelper.listEvalulate(listingSixtyDormantData.get("0"),
				listingSixtyDormantData.get("1"), listingSixtyDormantData.get("2"), listingSixtyDormantData.get("3"),
				listingSixtyDormantData.get("4"));
		String sixtyDormantResponseOfListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		String sixtyDormantTradeDiscountInfoListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(sixtyDormantResponseOfListEvaluate, "[]");
		softAssert.assertEquals(sixtyDormantTradeDiscountInfoListEvaluate, "[]");

		// 90 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(2), restId, dormantDays.get(2));

		Processor ninetyDormantlistEvaluateResponse = rngHelper.listEvalulate(listingSixtyDormantData.get("0"),
				listingSixtyDormantData.get("1"), listingSixtyDormantData.get("2"), listingSixtyDormantData.get("3"),
				listingSixtyDormantData.get("4"));
		int ninetyDormantCampaignListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String ninetyDormantDiscountTypeListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		softAssert.assertEquals(ninetyDormantCampaignListEvaluate, campIdOfNinetytyDormant);
		softAssert.assertEquals(ninetyDormantDiscountTypeListEvaluate, "BXGY");

		// public user listing evaluate
		Processor publiclistEvaluateResponse = rngHelper.listEvalulate(listingPublicData.get("0"),
				listingPublicData.get("1"), listingPublicData.get("2"), listingPublicData.get("3"),
				listingPublicData.get("4"));
		String publicResponseOfListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		String publicTradeDiscountInfoListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		softAssert.assertEquals(publicResponseOfListEvaluate, "[]");
		softAssert.assertEquals(publicTradeDiscountInfoListEvaluate, "[]");

		softAssert.assertEquals(ninetyDormantDiscountTypeListEvaluate, "BXGY");

		softAssert.assertAll();
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfNinetytyDormant));
	}

	@Test(dataProvider = "evaluateListingForUserFirstOrderAndNonFirstOrderUserIdWhenActiveCampaignIsAlsoForUserFirstOrderData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForUserFirstOrderAndNonFirstOrderUserIdWhenActiveCampaignIsAlsoForUserFirstOrderTest(
			HashMap<String, String> createFirstOrderCampData, HashMap<String, String> listingForFirstOrderData,
			HashMap<String, String> listingForNonFirstOrderData) {

		// create first order camp
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createFirstOrderCampData);
		int campIdOfFirstOrder = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		Processor firstOrderlistEvaluateResponse = rngHelper.listEvalulate(listingForFirstOrderData.get("0"),
				listingForFirstOrderData.get("1"), listingForFirstOrderData.get("2"), listingForFirstOrderData.get("3"),
				listingForFirstOrderData.get("4"));
		int firstOrderCampaignListEvaluate = firstOrderlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String firstOrderDiscountTypeListEvaluate = firstOrderlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		softAssert.assertEquals(firstOrderCampaignListEvaluate, campIdOfFirstOrder);
		softAssert.assertEquals(firstOrderDiscountTypeListEvaluate, "BXGY");

		// public user listing evaluate
		Processor publiclistEvaluateResponse = rngHelper.listEvalulate(listingForNonFirstOrderData.get("0"),
				listingForNonFirstOrderData.get("1"), listingForNonFirstOrderData.get("2"),
				listingForNonFirstOrderData.get("3"), listingForNonFirstOrderData.get("4"));
		String publicResponseOfListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		String publicTradeDiscountInfoListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		softAssert.assertEquals(publicResponseOfListEvaluate, "[]");
		softAssert.assertEquals(publicTradeDiscountInfoListEvaluate, "[]");

		softAssert.assertAll();
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(firstOrderCampaignListEvaluate));
	}

	@Test(dataProvider = "evaluateListingForRestaurantOrderAndNonRestFIrstOrderUserIdWhenActiveCampaignIsForRestaurantFirstOrderData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForRestaurantOrderAndNonRestFIrstOrderUserIdWhenActiveCampaignIsForRestaurantFirstOrderTest(
			HashMap<String, String> createRestFirstOrderCampData, HashMap<String, String> listingForRestFirstOrderData,
			HashMap<String, String> listingForNonRestFirstOrderData) {

		// create restaurant first order camp
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createRestFirstOrderCampData);
		int campIdOfRestFirstOrder = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		Processor restFirstOrderlistEvaluateResponse = rngHelper.listEvalulate(listingForRestFirstOrderData.get("0"),
				listingForRestFirstOrderData.get("1"), listingForRestFirstOrderData.get("2"),
				listingForRestFirstOrderData.get("3"), listingForRestFirstOrderData.get("4"));
		String restFirstOrderCampaignListEvaluate = restFirstOrderlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		int restFirstOrderCampaignListEvaluateInt=Integer.parseInt(restFirstOrderCampaignListEvaluate.replace("[","").replace("]",""));

		String restFirstOrderDiscountTypeListEvaluate = restFirstOrderlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		softAssert.assertEquals(restFirstOrderCampaignListEvaluateInt, campIdOfRestFirstOrder);
		softAssert.assertEquals(restFirstOrderDiscountTypeListEvaluate, "BXGY");

		// public user listing evaluate
		Processor publiclistEvaluateResponse = rngHelper.listEvalulate(listingForNonRestFirstOrderData.get("0"),
				listingForNonRestFirstOrderData.get("1"), listingForNonRestFirstOrderData.get("2"),
				listingForNonRestFirstOrderData.get("3"), listingForNonRestFirstOrderData.get("4"));
		String publicResponseOfListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		String publicTradeDiscountInfoListEvaluate = publiclistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		softAssert.assertEquals(publicResponseOfListEvaluate, "[]");
		softAssert.assertEquals(publicTradeDiscountInfoListEvaluate, "[]");

		softAssert.assertAll();
		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfRestFirstOrder));
	}

	@Test(dataProvider = "evaluateListingWithInTheTimeSlotOfCampaignData", dataProviderClass = EdvoDP.class)
	public void evaluateListingWithInTheTimeSlotOfCampaignTest(HashMap<String, String> createEDVOCampWithTimeSlot,
															   HashMap<String, String> listingPayloadWithInTimeSlot) {

		// create camp with time slot
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampWithTimeSlot);
		int campIdOfRestFirstOrder = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Processor campWithInTimeSlotlistEvaluateResponse = rngHelper.listEvalulate(
				listingPayloadWithInTimeSlot.get("0"), listingPayloadWithInTimeSlot.get("1"),
				listingPayloadWithInTimeSlot.get("2"), listingPayloadWithInTimeSlot.get("3"),
				listingPayloadWithInTimeSlot.get("4"));
		int withInTimeSlotCampaignListEvaluate = campWithInTimeSlotlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String withInTimeSlotDiscountTypeListEvaluate = campWithInTimeSlotlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		softAssert.assertEquals(withInTimeSlotCampaignListEvaluate, campIdOfRestFirstOrder);
		softAssert.assertEquals(withInTimeSlotDiscountTypeListEvaluate, "BXGY");

		//edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfRestFirstOrder));
		softAssert.assertAll();
	}

	@Test(dataProvider = "evaluateListingOutOfTheTimeSlotOfCampaignData", dataProviderClass = EdvoDP.class)
	public void evaluateListingOutOfTheTimeSlotOfCampaignTest(HashMap<String, String> createEDVOCampWithTimeSlot,
															  HashMap<String, String> listingPayloadOutOfTimeSlot) {

		// create camp with time slot
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampWithTimeSlot);
		int campIdOfRestFirstOrder = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");

		Processor campWithInTimeSlotlistEvaluateResponse = rngHelper.listEvalulate(listingPayloadOutOfTimeSlot.get("0"),
				listingPayloadOutOfTimeSlot.get("1"), listingPayloadOutOfTimeSlot.get("2"),
				listingPayloadOutOfTimeSlot.get("3"), listingPayloadOutOfTimeSlot.get("4"));
		String TradeDiscountInfoListEvaluate = campWithInTimeSlotlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertNotEquals(TradeDiscountInfoListEvaluate, campIdOfRestFirstOrder);
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfRestFirstOrder));
		softAssert.assertAll();
	}

	@Test(dataProvider = "evaluateListingToCheckCampaignOutOfValidityData", dataProviderClass = EdvoDP.class)
	public void evaluateListingToCheckCampaignOutOfValidityTest(HashMap<String, String> createEDVOCamp,
																HashMap<String, String> listingPayloadOutOfValidity) {

		// create camp with time slot
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCamp);
		int campId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");

		Processor listEvaluateResponse = rngHelper.listEvalulate(listingPayloadOutOfValidity.get("0"),
				listingPayloadOutOfValidity.get("1"), listingPayloadOutOfValidity.get("2"),
				listingPayloadOutOfValidity.get("3"), listingPayloadOutOfValidity.get("4"));
		String tradeDiscountInfoListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");
		String restIdsTypeListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		Assert.assertEquals(tradeDiscountInfoListEvaluate, "[]");
		Assert.assertEquals(restIdsTypeListEvaluate, "[]");

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateListingToCheckCampaignWithInValidityData", dataProviderClass = EdvoDP.class)
	public void evaluateListingToCheckCampaignWithInValidityTest(HashMap<String, String> createEDVOCamp,
																 HashMap<String, String> listingPayloadWithInValidity) {

		// create camp with time slot
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCamp);
		int campIdOfRestFirstOrder = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(campStatusMessage, "success");

		Processor campWithInTimeSlotlistEvaluateResponse = rngHelper.listEvalulate(
				listingPayloadWithInValidity.get("0"), listingPayloadWithInValidity.get("1"),
				listingPayloadWithInValidity.get("2"), listingPayloadWithInValidity.get("3"),
				listingPayloadWithInValidity.get("4"));
		int withInTimeSlotCampaignListEvaluate = campWithInTimeSlotlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String withInTimeSlotDiscountTypeListEvaluate = campWithInTimeSlotlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		Assert.assertEquals(withInTimeSlotCampaignListEvaluate, campIdOfRestFirstOrder);
		Assert.assertEquals(withInTimeSlotDiscountTypeListEvaluate, "BXGY");

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campIdOfRestFirstOrder));
	}

	@Test(dataProvider = "evaluateListingToCheckPublicTypeCampIsVisibleToAllTypeOfUsersWhenNoOtherUserCutCampIsActiveData", dataProviderClass = EdvoDP.class)
	public void evaluateListingToCheckPublicTypeCampIsVisibleToAllTypeOfUsersWhenNoOtherUserCutCampIsActiveTest(
			HashMap<String, String> createPublicCampData, HashMap<String, String> listingThirtyDormantData,
			HashMap<String, String> listingSixtyDormantData, HashMap<String, String> listingNinetyDormantData,
			HashMap<String, String> listingPublicData, ArrayList<String> userIds, String restId,
			ArrayList<String> dormantDays) {

		// create 30 days dormant camp
		Processor createResponse = edvoHelper.createEDVOTradeDiscount(createPublicCampData);
		int campId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// 30 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(0), restId, dormantDays.get(0));

		Processor listEvaluateResponse = rngHelper.listEvalulate(listingThirtyDormantData.get("0"),
				listingThirtyDormantData.get("1"), listingThirtyDormantData.get("2"), listingThirtyDormantData.get("3"),
				listingThirtyDormantData.get("4"));
		int CampIdInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String type=listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(("$.data.discountList[0].tradeDiscountInfoList[0].public"));
		String campTypeInListEvaluate = listEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");
		softAssert.assertEquals(CampIdInListEvaluate, campId);
		softAssert.assertEquals(campTypeInListEvaluate, "BXGY");
		softAssert.assertEquals(type, "true");

		// 60 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(1), restId, dormantDays.get(1));

		Processor sixtyDormantlistEvaluateResponse = rngHelper.listEvalulate(listingSixtyDormantData.get("0"),
				listingSixtyDormantData.get("1"), listingSixtyDormantData.get("2"), listingSixtyDormantData.get("3"),
				listingSixtyDormantData.get("4"));
		int sixtyDormantCampaignListEvaluate = sixtyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String typeSixty=listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(("$.data.discountList[0].tradeDiscountInfoList[0].public"));
		String sixtyDormantDiscountTypeListEvaluate = sixtyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");

		softAssert.assertEquals(sixtyDormantCampaignListEvaluate, campId);
		softAssert.assertEquals(sixtyDormantDiscountTypeListEvaluate, "BXGY");
		softAssert.assertEquals(typeSixty,"true");
		// 90 dormant userId set in redis
		rngHelper.DormantUser(userIds.get(2), restId, dormantDays.get(2));

		Processor ninetyDormantlistEvaluateResponse = rngHelper.listEvalulate(listingSixtyDormantData.get("0"),
				listingSixtyDormantData.get("1"), listingSixtyDormantData.get("2"), listingSixtyDormantData.get("3"),
				listingSixtyDormantData.get("4"));
		int ninetyDormantCampaignListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String ninetyDormantDiscountTypeListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");
		String typeNinety=listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(("$.data.discountList[0].tradeDiscountInfoList[0].public"));
		softAssert.assertEquals(ninetyDormantCampaignListEvaluate, campId);
		softAssert.assertEquals(ninetyDormantDiscountTypeListEvaluate, "BXGY");
		softAssert.assertEquals(typeNinety, "true");
		// public user listing evaluate
		Processor publiclistEvaluateResponse = rngHelper.listEvalulate(listingPublicData.get("0"),
				listingPublicData.get("1"), listingPublicData.get("2"), listingPublicData.get("3"),
				listingPublicData.get("4"));
		int publicCampaignListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		String publicCampDiscountTypeListEvaluate = ninetyDormantlistEvaluateResponse.ResponseValidator
				.GetNodeValue("$.data.discountList[0].tradeDiscountInfoList[0].discountType");
		String typePublic=listEvaluateResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(("$.data.discountList[0].tradeDiscountInfoList[0].public"));
		softAssert.assertEquals(publicCampaignListEvaluate, campId);
		softAssert.assertEquals(publicCampDiscountTypeListEvaluate, "BXGY");
		softAssert.assertEquals(typePublic, "true");
		softAssert.assertAll();

//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
	}

	@Test(dataProvider = "evaluateListingForUserCutWith30DaysDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForUserCutWith30DaysDormantTest(HashMap<String, String> edvo30DaysDormantData,
															   HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
															   HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
															   HashMap<String, String> listingPayloadFor30Days, HashMap<String, String> listingPayloadFirstOrder,
															   HashMap<String, String> listingPayloadRestFirstOrder, HashMap<String, String> listingPayloadRandomUser,
															   HashMap<String, String> listingPayloadPublicUser, String userId, String restId, String dormantDays) {
		// 30-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvo30DaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		softAssert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// listing evaluate for 30-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor listingThirtyDormantResponse = rngHelper.listEvalulate(listingPayloadFor30Days.get("0"),
				listingPayloadFor30Days.get("1"), listingPayloadFor30Days.get("2"), listingPayloadFor30Days.get("3"),
				listingPayloadFor30Days.get("4"));
		int campIdInResponse = listingThirtyDormantResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(campIdInResponse, campId);

		// // listing evaluate for public user
		Processor listingResponsePublicUser = rngHelper.listEvalulate(listingPayloadPublicUser.get("0"),
				listingPayloadPublicUser.get("1"), listingPayloadPublicUser.get("2"), listingPayloadPublicUser.get("3"),
				listingPayloadPublicUser.get("4"));
		int campIdInResponsePublicUser = listingResponsePublicUser.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(campIdInResponsePublicUser, createEDVOPublicCampCampId);

		// listing evaluate for 1st Order
		Processor listingResponseFirstOrder = rngHelper.listEvalulate(listingPayloadFirstOrder.get("0"),
				listingPayloadFirstOrder.get("1"), listingPayloadFirstOrder.get("2"), listingPayloadFirstOrder.get("3"),
				listingPayloadFirstOrder.get("4"));
		int campIdInResponseFirstOrder = listingResponseFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(campIdInResponseFirstOrder, edvoFirstOrderDataCampId);

		// listing evaluate for restaurant 1st Order
		Processor listingResponseRestFirstOrder = rngHelper.listEvalulate(listingPayloadRestFirstOrder.get("0"),
				listingPayloadRestFirstOrder.get("1"), listingPayloadRestFirstOrder.get("2"),
				listingPayloadRestFirstOrder.get("3"), listingPayloadRestFirstOrder.get("4"));
		int campIdInResponseRestFirstOrder = listingResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(campIdInResponseRestFirstOrder, edvoCampRestFirstOrderCampId);

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor linstingResponseRandomUser = rngHelper.listEvalulate(listingPayloadRandomUser.get("0"),
				listingPayloadRandomUser.get("1"), listingPayloadRandomUser.get("2"), listingPayloadRandomUser.get("3"),
				listingPayloadRandomUser.get("4"));
		int campIdInResponseMappedUser = linstingResponseRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(campIdInResponseMappedUser, edvoCampRestFirstOrderCampId);

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(createEDVOPublicCampCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoFirstOrderDataCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));

		softAssert.assertAll();
	}

	@Test(dataProvider = "evaluateListingForUserCutWithSixtyDaysDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForUserCutWithSixtyDaysDormantTest(HashMap<String, String> edvoSixtyDaysDormantData,
																  HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
																  HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
																  HashMap<String, String> listingPayloadForSixtyDays, HashMap<String, String> listingPayloadFirstOrder,
																  HashMap<String, String> listingPayloadRestFirstOrder, HashMap<String, String> listingPayloadRandomUser,
																  HashMap<String, String> listingPayloadPublicUser, String userId, String restId, String dormantDays) {

		// 60-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoSixtyDaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		softAssert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// listing evaluate for 60-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor listingSixtytyDormantResponse = rngHelper.listEvalulate(listingPayloadForSixtyDays.get("0"),
				listingPayloadForSixtyDays.get("1"), listingPayloadForSixtyDays.get("2"),
				listingPayloadForSixtyDays.get("3"), listingPayloadForSixtyDays.get("4"));
		int campIdInResponse = listingSixtytyDormantResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponse, campId);

		// // listing evaluate for public user
		Processor listingResponsePublicUser = rngHelper.listEvalulate(listingPayloadPublicUser.get("0"),
				listingPayloadPublicUser.get("1"), listingPayloadPublicUser.get("2"), listingPayloadPublicUser.get("3"),
				listingPayloadPublicUser.get("4"));
		int campIdInResponsePublicUser = listingResponsePublicUser.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponsePublicUser, createEDVOPublicCampCampId);

		// listing evaluate for 1st Order
		Processor listingResponseFirstOrder = rngHelper.listEvalulate(listingPayloadFirstOrder.get("0"),
				listingPayloadFirstOrder.get("1"), listingPayloadFirstOrder.get("2"), listingPayloadFirstOrder.get("3"),
				listingPayloadFirstOrder.get("4"));
		int campIdInResponseFirstOrder = listingResponseFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponseFirstOrder, edvoFirstOrderDataCampId);

		// listing evaluate for restaurant 1st Order
		Processor listingResponseRestFirstOrder = rngHelper.listEvalulate(listingPayloadRestFirstOrder.get("0"),
				listingPayloadRestFirstOrder.get("1"), listingPayloadRestFirstOrder.get("2"),
				listingPayloadRestFirstOrder.get("3"), listingPayloadRestFirstOrder.get("4"));
		int campIdInResponseRestFirstOrder = listingResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponseRestFirstOrder, edvoCampRestFirstOrderCampId);

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor linstingResponseRandomUser = rngHelper.listEvalulate(listingPayloadRandomUser.get("0"),
				listingPayloadRandomUser.get("1"), listingPayloadRandomUser.get("2"), listingPayloadRandomUser.get("3"),
				listingPayloadRandomUser.get("4"));
		int campIdInResponseMappedUser = linstingResponseRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponseMappedUser, edvoCampRestFirstOrderCampId);

//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(createEDVOPublicCampCampId));
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoFirstOrderDataCampId));
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		softAssert.assertAll();

	}

	@Test(dataProvider = "evaluateListingForUserCutWithNinetyDaysDormantData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForUserCutWithNinetyDaysDormantTest(HashMap<String, String> edvoNinetyDaysDormantData,
																   HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
																   HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
																   HashMap<String, String> listingPayloadForNinetyDays, HashMap<String, String> listingPayloadFirstOrder,
																   HashMap<String, String> listingPayloadRestFirstOrder, HashMap<String, String> listingPayloadRandomUser,
																   HashMap<String, String> listingPayloadPublicUser, String userId, String restId, String dormantDays) {

		// 90-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoNinetyDaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		softAssert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// listing evaluate for 60-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor listingSixtytyDormantResponse = rngHelper.listEvalulate(listingPayloadForNinetyDays.get("0"),
				listingPayloadForNinetyDays.get("1"), listingPayloadForNinetyDays.get("2"),
				listingPayloadForNinetyDays.get("3"), listingPayloadForNinetyDays.get("4"));
		int campIdInResponse = listingSixtytyDormantResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(campIdInResponse, campId);

		// // listing evaluate for public user
		Processor listingResponsePublicUser = rngHelper.listEvalulate(listingPayloadPublicUser.get("0"),
				listingPayloadPublicUser.get("1"), listingPayloadPublicUser.get("2"), listingPayloadPublicUser.get("3"),
				listingPayloadPublicUser.get("4"));
		int campIdInResponsePublicUser = listingResponsePublicUser.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponsePublicUser, createEDVOPublicCampCampId);

		// listing evaluate for 1st Order
		Processor listingResponseFirstOrder = rngHelper.listEvalulate(listingPayloadFirstOrder.get("0"),
				listingPayloadFirstOrder.get("1"), listingPayloadFirstOrder.get("2"), listingPayloadFirstOrder.get("3"),
				listingPayloadFirstOrder.get("4"));
		int campIdInResponseFirstOrder = listingResponseFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponseFirstOrder, edvoFirstOrderDataCampId);

		// listing evaluate for restaurant 1st Order
		Processor listingResponseRestFirstOrder = rngHelper.listEvalulate(listingPayloadRestFirstOrder.get("0"),
				listingPayloadRestFirstOrder.get("1"), listingPayloadRestFirstOrder.get("2"),
				listingPayloadRestFirstOrder.get("3"), listingPayloadRestFirstOrder.get("4"));
		int campIdInResponseRestFirstOrder = listingResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponseRestFirstOrder, edvoCampRestFirstOrderCampId);

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor linstingResponseRandomUser = rngHelper.listEvalulate(listingPayloadRandomUser.get("0"),
				listingPayloadRandomUser.get("1"), listingPayloadRandomUser.get("2"), listingPayloadRandomUser.get("3"),
				listingPayloadRandomUser.get("4"));
		int campIdInResponseMappedUser = linstingResponseRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponseMappedUser, edvoCampRestFirstOrderCampId);

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(createEDVOPublicCampCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoFirstOrderDataCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		softAssert.assertAll();
	}

	@Test(dataProvider = "evaluateListingForIOSVersionData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForIOSVersionTest(HashMap<String, String> edvoNinetyDaysDormantData,
												 HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
												 HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
												 HashMap<String, String> listingPayloadForNinetyDays, HashMap<String, String> listingPayloadFirstOrder,
												 HashMap<String, String> listingPayloadRestFirstOrder, HashMap<String, String> listingPayloadRandomUser,
												 HashMap<String, String> listingPayloadPublicUser, String userId, String restId, String dormantDays) {

		// 90-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoNinetyDaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		softAssert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// listing evaluate for 60-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor listingSixtytyDormantResponse = rngHelper.listEvalulate(listingPayloadForNinetyDays.get("0"),
				listingPayloadForNinetyDays.get("1"), listingPayloadForNinetyDays.get("2"),
				listingPayloadForNinetyDays.get("3"), listingPayloadForNinetyDays.get("4"));
		int campIdInResponse = listingSixtytyDormantResponse.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		// // listing evaluate for public user
		Processor listingResponsePublicUser = rngHelper.listEvalulate(listingPayloadPublicUser.get("0"),
				listingPayloadPublicUser.get("1"), listingPayloadPublicUser.get("2"), listingPayloadPublicUser.get("3"),
				listingPayloadPublicUser.get("4"));
		int campIdInResponsePublicUser = listingResponsePublicUser.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(campIdInResponsePublicUser, createEDVOPublicCampCampId);

		// listing evaluate for 1st Order
		Processor listingResponseFirstOrder = rngHelper.listEvalulate(listingPayloadFirstOrder.get("0"),
				listingPayloadFirstOrder.get("1"), listingPayloadFirstOrder.get("2"), listingPayloadFirstOrder.get("3"),
				listingPayloadFirstOrder.get("4"));
		int campIdInResponseFirstOrder = listingResponseFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponseFirstOrder, edvoFirstOrderDataCampId);

		// listing evaluate for restaurant 1st Order
		Processor listingResponseRestFirstOrder = rngHelper.listEvalulate(listingPayloadRestFirstOrder.get("0"),
				listingPayloadRestFirstOrder.get("1"), listingPayloadRestFirstOrder.get("2"),
				listingPayloadRestFirstOrder.get("3"), listingPayloadRestFirstOrder.get("4"));
		int campIdInResponseRestFirstOrder = listingResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponseRestFirstOrder, edvoCampRestFirstOrderCampId);

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor linstingResponseRandomUser = rngHelper.listEvalulate(listingPayloadRandomUser.get("0"),
				listingPayloadRandomUser.get("1"), listingPayloadRandomUser.get("2"), listingPayloadRandomUser.get("3"),
				listingPayloadRandomUser.get("4"));
		int campIdInResponseMappedUser = linstingResponseRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");
		softAssert.assertEquals(campIdInResponseMappedUser, edvoCampRestFirstOrderCampId);

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(createEDVOPublicCampCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampMappedUserResponseCampId));

		softAssert.assertAll();
	}

	@Test(dataProvider = "evaluateListingForInvalidIOSVersionData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForInvalidIOSVersionTest(HashMap<String, String> edvoNinetyDaysDormantData,
														HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
														HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
														HashMap<String, String> listingPayloadForNinetyDays, HashMap<String, String> listingPayloadFirstOrder,
														HashMap<String, String> listingPayloadRestFirstOrder, HashMap<String, String> listingPayloadRandomUser,
														HashMap<String, String> listingPayloadPublicUser, String userId, String restId, String dormantDays) {

		// 90-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoNinetyDaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		softAssert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// listing evaluate for 60-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor listingSixtytyDormantResponse = rngHelper.listEvalulate(listingPayloadForNinetyDays.get("0"),
				listingPayloadForNinetyDays.get("1"), listingPayloadForNinetyDays.get("2"),
				listingPayloadForNinetyDays.get("3"), listingPayloadForNinetyDays.get("4"));
		String tradeDiscountInfoListEvaluate = listingSixtytyDormantResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(tradeDiscountInfoListEvaluate, "[]");

		// // listing evaluate for public user
		Processor listingResponsePublicUser = rngHelper.listEvalulate(listingPayloadPublicUser.get("0"),
				listingPayloadPublicUser.get("1"), listingPayloadPublicUser.get("2"), listingPayloadPublicUser.get("3"),
				listingPayloadPublicUser.get("4"));
		String tradeDiscountInfoPubliListEvaluate = listingResponsePublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(tradeDiscountInfoPubliListEvaluate, "[]");

		// listing evaluate for 1st Order
		Processor listingResponseFirstOrder = rngHelper.listEvalulate(listingPayloadFirstOrder.get("0"),
				listingPayloadFirstOrder.get("1"), listingPayloadFirstOrder.get("2"), listingPayloadFirstOrder.get("3"),
				listingPayloadFirstOrder.get("4"));
		String tradeDiscountInf0FirstOrderListEvaluate = listingResponseFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(tradeDiscountInf0FirstOrderListEvaluate, "[]");

		// listing evaluate for restaurant 1st Order
		Processor listingResponseRestFirstOrder = rngHelper.listEvalulate(listingPayloadRestFirstOrder.get("0"),
				listingPayloadRestFirstOrder.get("1"), listingPayloadRestFirstOrder.get("2"),
				listingPayloadRestFirstOrder.get("3"), listingPayloadRestFirstOrder.get("4"));
		String tradeDiscountInfoRestFirstOrderListEvaluate = listingResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(tradeDiscountInfoRestFirstOrderListEvaluate, "[]");

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor linstingResponseRandomUser = rngHelper.listEvalulate(listingPayloadRandomUser.get("0"),
				listingPayloadRandomUser.get("1"), listingPayloadRandomUser.get("2"), listingPayloadRandomUser.get("3"),
				listingPayloadRandomUser.get("4"));
		String tradeDiscountInfoRandomUserListEvaluate = linstingResponseRandomUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(tradeDiscountInfoRandomUserListEvaluate, "[]");

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(createEDVOPublicCampCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoFirstOrderDataCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		softAssert.assertAll();
	}

	@Test(dataProvider = "evaluateListingForInvalidAndroidVersionData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForInvalidAndroidVersionTest(HashMap<String, String> edvoNinetyDaysDormantData,
															HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
															HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
															HashMap<String, String> listingPayloadForNinetyDays, HashMap<String, String> listingPayloadFirstOrder,
															HashMap<String, String> listingPayloadRestFirstOrder, HashMap<String, String> listingPayloadRandomUser,
															HashMap<String, String> listingPayloadPublicUser, String userId, String restId, String dormantDays) {

		// 90-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoNinetyDaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		softAssert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// listing evaluate for 60-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor listingSixtytyDormantResponse = rngHelper.listEvalulate(listingPayloadForNinetyDays.get("0"),
				listingPayloadForNinetyDays.get("1"), listingPayloadForNinetyDays.get("2"),
				listingPayloadForNinetyDays.get("3"), listingPayloadForNinetyDays.get("4"));
		String tradeDiscountInfoListEvaluate = listingSixtytyDormantResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(tradeDiscountInfoListEvaluate, "[]");

		// // listing evaluate for public user
		Processor listingResponsePublicUser = rngHelper.listEvalulate(listingPayloadPublicUser.get("0"),
				listingPayloadPublicUser.get("1"), listingPayloadPublicUser.get("2"), listingPayloadPublicUser.get("3"),
				listingPayloadPublicUser.get("4"));
		String tradeDiscountInfoPubliListEvaluate = listingResponsePublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(tradeDiscountInfoPubliListEvaluate, "[]");

		// listing evaluate for 1st Order
		Processor listingResponseFirstOrder = rngHelper.listEvalulate(listingPayloadFirstOrder.get("0"),
				listingPayloadFirstOrder.get("1"), listingPayloadFirstOrder.get("2"), listingPayloadFirstOrder.get("3"),
				listingPayloadFirstOrder.get("4"));
		String tradeDiscountInf0FirstOrderListEvaluate = listingResponseFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		softAssert.assertEquals(tradeDiscountInf0FirstOrderListEvaluate, "[]");

		// listing evaluate for restaurant 1st Order
		Processor listingResponseRestFirstOrder = rngHelper.listEvalulate(listingPayloadRestFirstOrder.get("0"),
				listingPayloadRestFirstOrder.get("1"), listingPayloadRestFirstOrder.get("2"),
				listingPayloadRestFirstOrder.get("3"), listingPayloadRestFirstOrder.get("4"));
		String tradeDiscountInfoRestFirstOrderListEvaluate = listingResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		Assert.assertEquals(tradeDiscountInfoRestFirstOrderListEvaluate, "[]");

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor linstingResponseRandomUser = rngHelper.listEvalulate(listingPayloadRandomUser.get("0"),
				listingPayloadRandomUser.get("1"), listingPayloadRandomUser.get("2"), listingPayloadRandomUser.get("3"),
				listingPayloadRandomUser.get("4"));
		String tradeDiscountInfoRandomUserListEvaluate = linstingResponseRandomUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList");

		Assert.assertEquals(tradeDiscountInfoRandomUserListEvaluate, "[]");

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(createEDVOPublicCampCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampMappedUserResponseCampId));

		softAssert.assertAll();

	}

	@Test(dataProvider = "evaluateListingForAndroidVersionData", dataProviderClass = EdvoDP.class)
	public void evaluateListingForAndroidVersionTest(HashMap<String, String> edvoNinetyDaysDormantData,
													 HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
													 HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp,
													 HashMap<String, String> listingPayloadForNinetyDays, HashMap<String, String> listingPayloadFirstOrder,
													 HashMap<String, String> listingPayloadRestFirstOrder, HashMap<String, String> listingPayloadRandomUser,
													 HashMap<String, String> listingPayloadPublicUser, String userId, String restId, String dormantDays) {

		// 90-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoNinetyDaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		softAssert.assertEquals(createEDVOPublicCampStatusMessage, "success");
		// listing evaluate for 90-days dormant user
		rngHelper.DormantUser(userId, restId, dormantDays);
		Processor listingSixtytyDormantResponse = rngHelper.listEvalulate(listingPayloadForNinetyDays.get("0"),
				listingPayloadForNinetyDays.get("1"), listingPayloadForNinetyDays.get("2"),
				listingPayloadForNinetyDays.get("3"), listingPayloadForNinetyDays.get("4"));
		String tradeDiscountInfoListEvaluate = listingSixtytyDormantResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(tradeDiscountInfoListEvaluate, Integer.toString(campId));

		// // listing evaluate for public user
		Processor listingResponsePublicUser = rngHelper.listEvalulate(listingPayloadPublicUser.get("0"),
				listingPayloadPublicUser.get("1"), listingPayloadPublicUser.get("2"), listingPayloadPublicUser.get("3"),
				listingPayloadPublicUser.get("4"));
		String tradeDiscountInfoPubliListEvaluate = listingResponsePublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(tradeDiscountInfoPubliListEvaluate, Integer.toString(createEDVOPublicCampCampId));

		// listing evaluate for 1st Order
		Processor listingResponseFirstOrder = rngHelper.listEvalulate(listingPayloadFirstOrder.get("0"),
				listingPayloadFirstOrder.get("1"), listingPayloadFirstOrder.get("2"), listingPayloadFirstOrder.get("3"),
				listingPayloadFirstOrder.get("4"));
		String tradeDiscountInf0FirstOrderListEvaluate = listingResponseFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(tradeDiscountInf0FirstOrderListEvaluate, Integer.toString(edvoFirstOrderDataCampId));

		// listing evaluate for restaurant 1st Order
		Processor listingResponseRestFirstOrder = rngHelper.listEvalulate(listingPayloadRestFirstOrder.get("0"),
				listingPayloadRestFirstOrder.get("1"), listingPayloadRestFirstOrder.get("2"),
				listingPayloadRestFirstOrder.get("3"), listingPayloadRestFirstOrder.get("4"));
		String tradeDiscountInfoRestFirstOrderListEvaluate = listingResponseRestFirstOrder.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(tradeDiscountInfoRestFirstOrderListEvaluate, Integer.toString(edvoCampRestFirstOrderCampId));

		// cart evaluate for with random User id to show rest. 1st order campaign
		Processor linstingResponseRandomUser = rngHelper.listEvalulate(listingPayloadRandomUser.get("0"),
				listingPayloadRandomUser.get("1"), listingPayloadRandomUser.get("2"), listingPayloadRandomUser.get("3"),
				listingPayloadRandomUser.get("4"));
		String tradeDiscountInfoRandomUserListEvaluate = linstingResponseRandomUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.discountList[0].tradeDiscountInfoList[0].campaignId");

		softAssert.assertEquals(tradeDiscountInfoRandomUserListEvaluate, Integer.toString(edvoCampRestFirstOrderCampId));
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(createEDVOPublicCampCampId));
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
//		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampMappedUserResponseCampId));
		softAssert.assertAll();

	}

	@Test(dataProvider = "getActiveCampaignData", dataProviderClass = EdvoDP.class)
	public void getActiveCampaignTest(HashMap<String, String> edvoNinetyDaysDormantData,
									  HashMap<String, String> edvoFirstOrderData, HashMap<String, String> createEDVOCampRestFirstOrder,
									  HashMap<String, String> createEDVOCampMappedUser, HashMap<String, String> createEDVOPublicCamp) {

		// 90-days dormant
		Processor campResponse = edvoHelper.createEDVOTradeDiscount(edvoNinetyDaysDormantData);
		int campId = campResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String campStatusMessage = campResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(campStatusMessage, "success");

		// user 1st order
		Processor edvoFirstOrderDataResponse = edvoHelper.createEDVOTradeDiscount(edvoFirstOrderData);
		int edvoFirstOrderDataCampId = edvoFirstOrderDataResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoFirstOrderDataCampStatusMessage = edvoFirstOrderDataResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoFirstOrderDataCampStatusMessage, "success");

		// // rest 1st order
		Processor edvoCampRestFirstOrderResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampRestFirstOrder);
		int edvoCampRestFirstOrderCampId = edvoCampRestFirstOrderResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampRestFirstOrderResponseStatusMessage = edvoCampRestFirstOrderResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampRestFirstOrderResponseStatusMessage, "success");

		// mapped user
		Processor edvoCampMappedUserResponse = edvoHelper.createEDVOTradeDiscount(createEDVOCampMappedUser);
		int edvoCampMappedUserResponseCampId = edvoCampMappedUserResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String edvoCampMappedUserResponseStatusMessage = edvoCampMappedUserResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		softAssert.assertEquals(edvoCampMappedUserResponseStatusMessage, "success");

		// public camp
		Processor createEDVOPublicCampResponse = edvoHelper.createEDVOTradeDiscount(createEDVOPublicCamp);
		int createEDVOPublicCampCampId = createEDVOPublicCampResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String createEDVOPublicCampStatusMessage = createEDVOPublicCampResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		System.out.println("message " + campStatusMessage);
		softAssert.assertEquals(createEDVOPublicCampStatusMessage, "success");

		// String campIds= String.valueOf(campId) + createEDVOPublicCampCampId
		// +createEDVOPublicCampCampId+ edvoCampRestFirstOrderCampId
		// +edvoCampMappedUserResponseCampId;
		// get campaigns

		List<String> campaignIds = new ArrayList();
		campaignIds.add(0, String.valueOf(campId));
		campaignIds.add(1, String.valueOf(createEDVOPublicCampCampId));
		campaignIds.add(2, String.valueOf(createEDVOPublicCampCampId));
		campaignIds.add(3, String.valueOf(edvoCampRestFirstOrderCampId));
		campaignIds.add(4, String.valueOf(edvoCampMappedUserResponseCampId));
		if(!CollectionUtils.isEmpty(campaignIds)) {
			System.out.println("size " + campaignIds.size());
			edvoHelper.getTradeDiscountV2(String.join(",", campaignIds));
		}
		softAssert.assertAll();

		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(campId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(createEDVOPublicCampCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(createEDVOPublicCampCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampRestFirstOrderCampId));
		edvoHelper.deleteEDVOCampaignFromDB(Integer.toString(edvoCampMappedUserResponseCampId));
	}

}