package com.swiggy.api.sf.checkout.pojo.mySms;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;


@JsonPropertyOrder({
        "apiKey",
        "authToken",
        "addresses"
})
public class MySmsDeleteAllMessages {

    @JsonProperty("apiKey")
    private String apiKey;
    @JsonProperty("authToken")
    private String authToken;
    @JsonProperty("addresses")
    private List<String> addresses = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public MySmsDeleteAllMessages() {
    }

    /**
     *
     * @param authToken
     * @param addresses
     * @param apiKey
     */
    public MySmsDeleteAllMessages(String apiKey, String authToken, List<String> addresses) {
        super();
        this.apiKey = apiKey;
        this.authToken = authToken;
        this.addresses = addresses;
    }

    @JsonProperty("apiKey")
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty("apiKey")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonProperty("authToken")
    public String getAuthToken() {
        return authToken;
    }

    @JsonProperty("authToken")
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @JsonProperty("addresses")
    public List<String> getAddresses() {
        return addresses;
    }

    @JsonProperty("addresses")
    public void setAddresses(List<String> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("apiKey", apiKey).append("authToken", authToken).append("addresses", addresses).toString();
    }

}