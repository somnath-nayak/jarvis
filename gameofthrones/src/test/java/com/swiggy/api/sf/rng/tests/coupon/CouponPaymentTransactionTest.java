package com.swiggy.api.sf.rng.tests.coupon;

import com.swiggy.api.sf.rng.constants.CouponConstants;
import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.dp.coupondp.CouponPaymentTransactionDP;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.ApplyCouponPOJO;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.OrderCountByPaymentMethodPOJO;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;

public class CouponPaymentTransactionTest extends DP {

    RngHelper rngHelper = new RngHelper();


    @Test(dataProvider = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = " create discount type public coupon with num. transaction payment type as 1")
    public void createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int numTxnPaymentType = updateResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1);

    }

    @Test(dataProvider = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneForNonSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = " create discount type public coupon with num. transaction payment type as 1")
    public void createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int numTxnPaymentType = updateResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        int couponUserTypeId = updateResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1);
        Assert.assertEquals(couponUserTypeId, 2);
    }

    @Test(dataProvider = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneForSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = " create discount type public coupon with num. transaction payment type as 1")
    public void createPublicDiscountTypeCouponWithNumTxnPaymentTypeOneForSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int numTxnPaymentType = updateResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        int couponUserTypeId = updateResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1);
        Assert.assertEquals(couponUserTypeId, 1);
    }

    @Test(dataProvider = "createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "create discount type freeShipping coupon with num. transaction payment type as 1")
    public void createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createProcessor = rngHelper.createCoupon(payload);
        int statusCode = createProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createProcessor.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createProcessor.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createProcessor.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1);
        Assert.assertEquals(couponUserTypeId, 0);
        Assert.assertEquals(freeShipping, true);
    }

    @Test(dataProvider = "createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneForSuperuserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "create discount type freeShipping coupon with num. transaction payment type as 1")
    public void createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneForSuperuserTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createProcessor = rngHelper.createCoupon(payload);
        int statusCode = createProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createProcessor.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createProcessor.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createProcessor.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1);
        Assert.assertEquals(couponUserTypeId, 1);
        Assert.assertEquals(freeShipping, true);
    }

    @Test(dataProvider = "createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneForNonSuperuserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "create discount type freeShipping coupon with num. transaction payment type as 1")
    public void createPublicFreeShippingTypeCouponWithNumTxnPaymentTypeOneForNonSuperuserTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createProcessor = rngHelper.createCoupon(payload);
        int statusCode = createProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createProcessor.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createProcessor.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createProcessor.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 2, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, true, "Fail because --> free shipping true in not set in coupon creation");
    }

    @Test(dataProvider = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 0, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, false, "Fail because --> free shipping true in not set in coupon creation");
        RngHelper.couponUserMap(code, mapUserId, couponId);
    }

    @Test(dataProvider = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneForSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneForSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 1, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, false, "Fail because --> free shipping true in not set in coupon creation");
        RngHelper.couponUserMap(code, mapUserId, couponId);
    }

    @Test(dataProvider = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneForNonSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeOneForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 2, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, false, "Fail because --> free shipping true in not set in coupon creation");
        RngHelper.couponUserMap(code, mapUserId, couponId);
    }

    @Test(dataProvider = "createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 0, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, true, "Fail because --> free shipping true in not set in coupon creation");
        Processor couponMappingResponse = RngHelper.couponUserMap(code, mapUserId, couponId);
        String mappingStatus = couponMappingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        String mappingId = couponMappingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        Assert.assertEquals(mappingStatus, "1");
        Assert.assertNotEquals(mappingId, null);
    }

    @Test(dataProvider = "createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneForSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneForSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 1, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, true, "Fail because --> free shipping true in not set in coupon creation");
        Processor couponMappingResponse = RngHelper.couponUserMap(code, mapUserId, couponId);
        String mappingStatus = couponMappingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        String mappingId = couponMappingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        Assert.assertEquals(mappingStatus, "1");
        Assert.assertNotEquals(mappingId, null);
    }

    @Test(dataProvider = "createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneForNonSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createUserMappedFreeShippingTypeCouponWithNumTxnPaymentTypeOneForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 1, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 2, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, true, "Fail because --> free shipping true in not set in coupon creation");
        Processor couponMappingResponse = RngHelper.couponUserMap(code, mapUserId, couponId);
        String mappingStatus = couponMappingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        String mappingId = couponMappingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        Assert.assertEquals(mappingStatus, "1");
        Assert.assertNotEquals(mappingId, null);
    }

    @Test(dataProvider = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods ")
    public void createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createUserMappedDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createUserMappedFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create public discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create public discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create public discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createPublicDiscountTypeCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create public discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create public discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create public discount type coupon with num. transaction payment type as 1 by selecting multiple payment methods with user type Super")
    public void createPublicFreeShippingCouponWithNumTxnPaymentTypeAsOneWithMultiplePaymethodsForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    @Test(dataProvider = "createPublicFreeShippingCouponWithNumTxnPaymentTypeAsZeroData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createPublicFreeShippingCouponWithNumTxnPaymentTypeAsZeroTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 0, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 0, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, true, "Fail because --> free shipping true in not set in coupon creation");
    }

    @Test(dataProvider = "createPublicDiscountCouponWithNumTxnPaymentTypeAsZeroData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createPublicDiscountCouponWithNumTxnPaymentTypeAsZeroTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 0, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 0, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, false, "Fail because --> free shipping true in not set in coupon creation");
    }

    @Test(dataProvider = "createMappedUserDiscountCouponWithNumTxnPaymentTypeAsZeroData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createMappedUserDiscountCouponWithNumTxnPaymentTypeAsZeroTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 0, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 0, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, false, "Fail because --> free shipping true in not set in coupon creation");
    }

    @Test(dataProvider = "createMappedUserFreeShippingCouponWithNumTxnPaymentTypeAsZeroData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create user mapped discount type coupon with num. transaction payment type as 1")
    public void createMappedUserFreeShippingCouponWithNumTxnPaymentTypeAsZeroTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 0, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 0, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, true, "Fail because --> free shipping true in not set in coupon creation");
    }

    @Test(dataProvider = "createCouponWithNumTxnPaymentTypeAsNullData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as null")
    public void createCouponWithNumTxnPaymentTypeAsNullTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        int couponUserTypeId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");

        int numTxnPaymentType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.num_txn_payment_type");
        Assert.assertEquals(statusCode, 1, "Failure because --> Free shipping Coupon not created for num_txn_payment_type");
        Assert.assertEquals(numTxnPaymentType, 0, "Fail because --> num. transaction payment type 1 is not set in coupon creation");
        Assert.assertEquals(couponUserTypeId, 0, "Fail because --> coupon user type is not 1 in coupon creation");
        Assert.assertEquals(freeShipping, true, "Fail because --> free shipping true in not set in coupon creation");
    }

    @Test(dataProvider = "createCouponWithNumTxnPaymentTypeAsOneAndPreferredPaymentMethodAsEmptyStringData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void createCouponWithNumTxnPaymentTypeAsOneAndPreferredPaymentMethodAsEmptyStringTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Failure because --> Coupon is created when num_txn_payment_type is 1 and payment method is empty string");
        Assert.assertEquals(statusMessage, CouponConstants.invalidPaymentMethod);
    }

    // get user id city public coupon

    @Test(dataProvider = "getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneTest(HashMap<String, Object> data) {
        // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityID = Integer.toString(Utility.getRandom(1, 20));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
        Assert.assertEquals(statusMessage, null);

        List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");

        Integer userIdInSegmentation = 0;
        if (updatePayloadList.size() > 0) {
            userIdInSegmentation = updatePayloadList.get(0).getUserId();
        }

        Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);
        int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");
        Assert.assertEquals(updateStatusMessage, null);

        RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "1", "1", "1");
        Processor getUserCityResp = RngHelper.getCouponUserCity(Integer.toString(userIdInSegmentation), cityID);
        String getUserCityCouponCode = getUserCityResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id==" + couponId + ")].code");
        Assert.assertEquals(getUserCityCouponCode, "[\"" + code + "\"]");
    }

    @Test(dataProvider = "getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentCountIsOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentCountIsOneTest(HashMap<String, Object> data) {
        // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityID = Integer.toString(Utility.getRandom(1, 20));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
        Assert.assertEquals(statusMessage, null);

        List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");

        Integer userIdInSegmentation = 0;
        if (updatePayloadList.size() > 0) {
            userIdInSegmentation = updatePayloadList.get(0).getUserId();
        }

        Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);
        int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");
        Assert.assertEquals(updateStatusMessage, null);

        RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "1", "1", "1");
        Processor getUserCityResp = RngHelper.getCouponUserCity(Integer.toString(userIdInSegmentation), cityID);
//          String getUserCityCouponCode = getUserCityResp.ResponseValidator.GetNodeValueAsStringFromJsonArray( "$.data[?(@.id=="+ couponId+")].code" );
//            Assert.assertEquals(getUserCityCouponCode,code);

        int GetUserCityStatusCode = getUserCityResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(GetUserCityStatusCode, 1);
        String getUserCityCouponCode = getUserCityResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id==" + couponId + ")].code");
        Assert.assertEquals(getUserCityCouponCode, "[]");

    }

    @Test(dataProvider = "getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZeroData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZeroTest(HashMap<String, Object> data) {
        // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityID = Integer.toString(Utility.getRandom(1, 20));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
        Assert.assertEquals(statusMessage, null);

        List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");

        Integer userIdInSegmentation = 0;
        if (updatePayloadList.size() > 0) {
            userIdInSegmentation = updatePayloadList.get(0).getUserId();
        }

        Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);
        int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");
        Assert.assertEquals(updateStatusMessage, null);

        RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "10", "1", "1");
        Processor getUserCityResp = RngHelper.getCouponUserCity(Integer.toString(userIdInSegmentation), cityID);
        String getUserCityCouponCode = getUserCityResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id==" + couponId + ")].code");
        Assert.assertEquals(getUserCityCouponCode, "[\"" + code + "\"]");

//         int GetUserCityStatusCode = getUserCityResp.ResponseValidator.GetNodeValueAsInt( "$.statusCode" );
//          Assert.assertEquals(GetUserCityStatusCode,0);
    }

     @Test(dataProvider = "getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForNewUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
     public void getUserIdCityIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForNewUserTest(HashMap<String, Object> data) {
         String userId = Integer.toString(Utility.getRandom(1, 99900));
         String cityID = Integer.toString(Utility.getRandom(1, 20));

         CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
         String code = payload.getCode();
         Processor createResponse = rngHelper.createCoupon(payload);
         int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
         String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
         int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
         Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
         Assert.assertEquals(statusMessage, null);
         Processor getUserCityResp = RngHelper.getCouponUserCity(userId, cityID);
         String getUserCityCouponCode = getUserCityResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id==" + couponId + ")].code");
         Assert.assertEquals(getUserCityCouponCode, "[\"" + code + "\"]");

            int GetUserCityStatusCode = getUserCityResp.ResponseValidator.GetNodeValueAsInt( "$.statusCode" );
             Assert.assertEquals(GetUserCityStatusCode,1);
     }

    // get user id  public coupon
    @Test(dataProvider = "getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneTest(HashMap<String, Object> data) {
        // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
        Assert.assertEquals(statusMessage, null);

        List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");

        Integer userIdInSegmentation = 0;
        if (updatePayloadList.size() > 0) {
            userIdInSegmentation = updatePayloadList.get(0).getUserId();
        }

        Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);
        int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");
        Assert.assertEquals(updateStatusMessage, null);

        RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "1", "1", "1");
        Processor getUserCityResp = RngHelper.getCouponUser(Integer.toString(userIdInSegmentation));
        String getUserCityCouponCode = getUserCityResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id==" + couponId + ")].code");
        Assert.assertEquals(getUserCityCouponCode, "[\"" + code + "\"]");
    }

    @Test(dataProvider = "getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOneTest(HashMap<String, Object> data) {
        // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityID = Integer.toString(Utility.getRandom(1, 20));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
        Assert.assertEquals(statusMessage, null);

        List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");

        Integer userIdInSegmentation = 0;
        if (updatePayloadList.size() > 0) {
            userIdInSegmentation = updatePayloadList.get(0).getUserId();
        }

        Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);
        int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");
        Assert.assertEquals(updateStatusMessage, null);

        RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "1", "1", "1");
        Processor getUserCityResp = RngHelper.getCouponUser(Integer.toString(userIdInSegmentation));
        String getUserCityCouponCode = getUserCityResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id==" + couponId + ")].code");
        int GetUserCityStatusCode = getUserCityResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(GetUserCityStatusCode, 1);
        Assert.assertEquals(getUserCityCouponCode, "[]");

    }

    @Test(dataProvider = "getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZeroData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZeroTest(HashMap<String, Object> data) {
        // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityID = Integer.toString(Utility.getRandom(1, 20));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
        Assert.assertEquals(statusMessage, null);

        List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");
        Integer userIdInSegmentation = 0;
        if (updatePayloadList.size() > 0) {
            userIdInSegmentation = updatePayloadList.get(0).getUserId();
        }

        Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);
        int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");
        Assert.assertEquals(updateStatusMessage, null);

        RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "10", "1", "1");
        Processor getUserCityResp = RngHelper.getCouponUser(Integer.toString(userIdInSegmentation));
        String getUserCityCouponCode = getUserCityResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id==" + couponId + ")].code");
         int GetUserCityStatusCode = getUserCityResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
         Assert.assertEquals(GetUserCityStatusCode, 1);
       Assert.assertEquals(getUserCityCouponCode, "[\"" + code + "\"]");
    }
      @Test(dataProvider = "getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForNewUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
      public void getUserIdPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForNewUserTest(HashMap <String,Object> data) {
           String userId = Integer.toString(Utility.getRandom(1, 99900));
          String cityID = Integer.toString(Utility.getRandom(1, 20));

          CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
          String code = payload.getCode();
          Processor createResponse = rngHelper.createCoupon(payload);
          int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
          String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
          int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
          Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
          Assert.assertEquals(statusMessage, null);
          Processor getUserCityResp = RngHelper.getCouponUser(userId);
          String getUserCityCouponCode = getUserCityResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.id==" + couponId + ")].code");
           int GetUserCityStatusCode = getUserCityResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
           Assert.assertEquals(GetUserCityStatusCode, 1);
         Assert.assertEquals(getUserCityCouponCode, "[\"" + code + "\"]");
      }


    // apply  public coupon
    
    @Test(dataProvider = "applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneTest(HashMap<String, Object> data) {
        // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityID = Integer.toString(Utility.getRandom(1, 20));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
        Assert.assertEquals(statusMessage, null);

        List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");

        Integer userIdInSegmentation = 0;
        if (updatePayloadList.size() > 0) {
            userIdInSegmentation = updatePayloadList.get(0).getUserId();
        }

        Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);
        int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");
        Assert.assertEquals(updateStatusMessage, null);

        RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "1", "1", "1");

        String applyCoupon = new ApplyCouponPOJO(code, Integer.toString(userIdInSegmentation), false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");
    }

    @Test(dataProvider = "applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOneTest(HashMap<String, Object> data) {
        // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityID = Integer.toString(Utility.getRandom(1, 20));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
        Assert.assertEquals(statusMessage, null);

        List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");

        Integer userIdInSegmentation = 0;
        if (updatePayloadList.size() > 0) {
            userIdInSegmentation = updatePayloadList.get(0).getUserId();
        }

        Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);
        int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");
        Assert.assertEquals(updateStatusMessage, null);

        RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "1", "1", "1");
        String applyCoupon = new ApplyCouponPOJO(code, Integer.toString(userIdInSegmentation), false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        //String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        // String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        //  Assert.assertEquals(codeInApplyResp, code);
        Assert.assertNotEquals(couponError, CouponConstants.invalidCouponUserTyeId);
        // Assert.assertEquals(discountAmount, "20.0");

    }


    @Test(dataProvider = "applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZeroData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
    public void applyPublicTypeCouponNUserTypeALLWithNumTxnPaymentTypeAsZeroTest(HashMap<String, Object> data) {
        // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityID = Integer.toString(Utility.getRandom(1, 20));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
        Assert.assertEquals(statusMessage, null);

        List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");
        Integer userIdInSegmentation = 0;
        if (updatePayloadList.size() > 0) {
            userIdInSegmentation = updatePayloadList.get(0).getUserId();
        }

        Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);
        int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");
        Assert.assertEquals(updateStatusMessage, null);

        RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "100", "1", "1");

        String applyCoupon = new ApplyCouponPOJO(code, Integer.toString(userIdInSegmentation), false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");
    }

      @Test(dataProvider = "applyUserMappedCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOneData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
      public void applyUserMappedCouponNUserTypeALLWithNumTxnPaymentTypeAsOneWhereUserOrderPaymentcountCountIsOneTest(HashMap<String, Object> data) {
          // String mapUserId = Integer.toString(Utility.getRandom(1, 99900));                                                                                                                                                                                                                                                         
          String cityID = Integer.toString(Utility.getRandom(1, 20));                                                                                                                                                                                                                                                                  
                                                                                                                                                                                                                                                                                                                                       
          CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");                                                                                                                                                                                                                                                  
          String code = payload.getCode();                                                                                                                                                                                                                                                                                             
          Processor createResponse = rngHelper.createCoupon(payload);                                                                                                                                                                                                                                                                  
          int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");                                                                                                                                                                                                                                         
          String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");                                                                                                                                                                                                                                     
          int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");                                                                                                                                                                                                                                              
          Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");                                                                                                                                                                                                             
          Assert.assertEquals(statusMessage, null);
          List<OrderCountByPaymentMethodPOJO> updatePayloadList = (List<OrderCountByPaymentMethodPOJO>) data.get("orderCountByPaymentMethodPOJO");                                                                                                                                                                                     
                                                                                                                                                                                                                                                                                                                                       
          Integer userIdInSegmentation = 0;                                                                                                                                                                                                                                                                                            
          if (updatePayloadList.size() > 0) {                                                                                                                                                                                                                                                                                          
              userIdInSegmentation = updatePayloadList.get(0).getUserId();                                                                                                                                                                                                                                                             
          }
          Processor updateOrderCountResponse = rngHelper.updateOrderCountByPaymentMethod(updatePayloadList);                                                                                                                                                                                                                           
          int updateStatusCode = updateOrderCountResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");                                                                                                                                                                                                                         
          String updateStatusMessage = updateOrderCountResponse.ResponseValidator.GetNodeValue("$.statusMessage");                                                                                                                                                                                                                     
          Assert.assertEquals(updateStatusCode, 1, "Failure because --> Number of payment count in not updated in customer order count buy payment method table");                                                                                                                                                                     
          Assert.assertEquals(updateStatusMessage, null);                                                                                                                                                                                                                                                                              
           RngHelper.couponUserMap(code,Integer.toString(userIdInSegmentation),Integer.toString(couponId));                                                                                                                                                                                                                                                                                                                               
          RngHelper.insertOrderCountInCustomerOrderTable(Integer.toString(userIdInSegmentation), "1", "1", "1");                                                                                                                                                                                                                       
          String applyCoupon = new ApplyCouponPOJO(code, Integer.toString(userIdInSegmentation), false, 200.0, 2).toString();                                                                                                                                                                                                          
          Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
          String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
          Assert.assertEquals(couponError, CouponConstants.couponNotApplicableForUser);
                                                                                                                                                                                                                                                                                                                                       
      }                                                                                                                                                                                                                                                                                                                                
       @Test(dataProvider = "applyPublicCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForFirstOrderUserData", dataProviderClass = CouponPaymentTransactionDP.class, description = "Create coupon with num. transaction payment type as 1 and payment method is empty string")
       public void applyPublicCouponNUserTypeALLWithNumTxnPaymentTypeAsOneForFirstOrderUserTest(HashMap<String, Object> data) {
            String userId = Integer.toString(Utility.getRandom(1, 99900));
           String cityID = Integer.toString(Utility.getRandom(1, 20));

           CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
           String code = payload.getCode();
           Processor createResponse = rngHelper.createCoupon(payload);
           int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
           String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
           int couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
           Assert.assertEquals(statusCode, 1, "Failure because --> Coupon is not created when num_txn_payment_type is 1 ");
           Assert.assertEquals(statusMessage, null);
           String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
           Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
           String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
           String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
            String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
             Assert.assertEquals(codeInApplyResp, code);
           Assert.assertEquals(couponError,null );
            Assert.assertEquals(discountAmount, "20.0");

       }













































}