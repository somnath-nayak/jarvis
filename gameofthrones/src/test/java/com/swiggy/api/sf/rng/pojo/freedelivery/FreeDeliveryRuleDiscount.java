/**
 * @author manu.chadha
 */

package com.swiggy.api.sf.rng.pojo.freedelivery;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;


public class FreeDeliveryRuleDiscount {

    @JsonProperty("type")
    private String type;
    @JsonProperty("discountLevel")
    private String discountLevel;
    @JsonProperty("minCartAmount")
    private String minCartAmount;

    /**
     * No args constructor for use in serialization
     *
     */
    public FreeDeliveryRuleDiscount() {
    }

    /**
     *
     * @param minCartAmount
     * @param discountLevel
     * @param type
     */
    public FreeDeliveryRuleDiscount(String type, String discountLevel, String minCartAmount) {
        super();
        this.type = type;
        this.discountLevel = discountLevel;
        this.minCartAmount = minCartAmount;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("discountLevel")
    public String getDiscountLevel() {
        return discountLevel;
    }

    @JsonProperty("discountLevel")
    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    @JsonProperty("minCartAmount")
    public String getMinCartAmount() {
        return minCartAmount;
    }

    @JsonProperty("minCartAmount")
    public void setMinCartAmount(String minCartAmount) {
        this.minCartAmount = minCartAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("discountLevel", discountLevel).append("minCartAmount", minCartAmount).toString();
    }

}