package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "cityEntity"
})
public class CityPOJO {
    @JsonProperty("cityEntity")
    private CityEntityPOJO cityEntity;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public CityPOJO() {
    }

    /**
     *
     * @param cityEntity
     */
    public CityPOJO(CityEntityPOJO cityEntity) {
        super();
        this.cityEntity = cityEntity;
    }

    @JsonProperty("cityEntity")
    public CityEntityPOJO getCityEntity() {
        return cityEntity;
    }

    @JsonProperty("cityEntity")
    public void setCityEntity(CityEntityPOJO cityEntity) {
        this.cityEntity = cityEntity;
    }

    public CityPOJO withCityEntity(CityEntityPOJO cityEntity) {
        this.cityEntity = cityEntity;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CityPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public CityPOJO setDefault() {
        CityEntityPOJO cityEntityPOJO = new CityEntityPOJO().setDefault();
        return this.withCityEntity(cityEntityPOJO);
    }

}
