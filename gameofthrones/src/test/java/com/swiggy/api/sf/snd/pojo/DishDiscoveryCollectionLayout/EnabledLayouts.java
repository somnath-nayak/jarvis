package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class EnabledLayouts {

    private LISTING lISTING;

    /**
     * No args constructor for use in serialization
     *
     */
    public EnabledLayouts() {
    }

    /**
     *
     * @param lISTING
     */
    public EnabledLayouts(LISTING lISTING) {
        super();
        this.lISTING = lISTING;
    }

    public LISTING getLISTING() {
        return lISTING;
    }

    public void setLISTING(LISTING lISTING) {
        this.lISTING = lISTING;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("lISTING", lISTING).toString();
    }

}