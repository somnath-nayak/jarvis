package com.swiggy.api.sf.checkout.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonPropertyOrder({
        "otp"
})
public class ValidatePhonepeOtp {

    @JsonProperty("otp")
    private String otp;

    /**
     * No args constructor for use in serialization
     *
     */
    public ValidatePhonepeOtp() {
    }

    /**
     *
     * @param otp
     */

    public ValidatePhonepeOtp(String otp) {
        super();
        this.otp = otp;
    }

    @JsonProperty("otp")
    public String getotp() {
        return otp;
    }

    @JsonProperty("otp")
    public void setotp(String otp) {
        this.otp = otp;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("otp", otp).toString();
    }

}