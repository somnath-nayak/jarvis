package com.swiggy.api.sf.snd.tests;

import com.swiggy.api.sf.snd.helper.ListingTroubleShooterHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;
import org.testng.Assert;

public class ListingTroubleShooterTest {

    ListingTroubleShooterHelper helper = new ListingTroubleShooterHelper();

    String[] latLng =  {"12.9326","77.6036"};
    String cityId = "1";
    String restId = "223";

    @Test()
    public void debugListingWithDelivery() {

        Processor listingDelivery = helper.deliveryListingDebug(new String[]{latLng[0], latLng[1],cityId, restId});
        String i3 = ((Integer) listingDelivery.ResponseValidator.GetResponseCode()).toString();
        System.out.println("listingWithDelivery ==== "+i3);

        Assert.assertEquals(i3,"200","Delivery issue || Delivery is throwing " +i3+"  for listing. Please connect with RNG QA ");
        if (i3.equals("200")){

            String deliveryResponse = listingDelivery.ResponseValidator.GetBodyAsText();
            Assert.assertNotNull(deliveryResponse,"Delivery response is empty");
        }
    }

    @Test(dependsOnMethods ={"debugListingWithDelivery"})
    public void debugListingSand(){

        Processor aggregatorWithSAND = helper.aggregator(latLng);
        String i4 = ((Integer)aggregatorWithSAND.ResponseValidator.GetResponseCode()).toString();
        System.out.println("AggregatorWithSAND ==== "+i4);

        Assert.assertEquals(i4,"200","SAND issue || SAND is throwing " +i4+"  for Aggregator. Please connect with SAND QA ");
    }

    @Test(dependsOnMethods ={"debugListingWithDelivery","debugListingSand"})
    public void debugListingFromRNG(){

        Processor listingWithGandalf = helper.listingWithGandalf(latLng[0], latLng[1], "");
        String i2 = ((Integer)listingWithGandalf.ResponseValidator.GetResponseCode()).toString();
        System.out.println("listingWithGandalf ==== "+i2);

        Assert.assertEquals(i2,"200","Gandalf issue || Gandalf is throwing " +i2+"  for listing. Please connect with RNG QA ");
    }


    @Test(dependsOnMethods ={"debugListingWithDelivery","debugListingSand","debugListingFromRNG"})
    public void debugListingFromCheckout(){

        Processor aggregatorWithCheckout = helper.aggregatorWithCheckout(latLng);
        String i = ((Integer) aggregatorWithCheckout.ResponseValidator.GetResponseCode()).toString();
        System.out.println("aggregatorWithCheckout ==== "+i);

        Assert.assertEquals(i,"200","Checkout issue || Checkout is throwing " +i+"  for aggregator. Please connect with CHECKOUT QA ");

        Processor listingWithCheckout = helper.listingWithCheckout(latLng[0], latLng[1], "");
        String i1 = ((Integer)listingWithCheckout.ResponseValidator.GetResponseCode()).toString();
        System.out.println("listingWithCheckout ==== "+i1);

        Assert.assertEquals(i1,"200","Checkout issue || Checkout is throwing " +i1+"  for listing. Please connect with CHECKOUT QA ");

    }


}
