package com.swiggy.api.sf.snd.tests.sandcontroller;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.HeaderHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import java.util.*;

public class SandAggregator extends SnDDp {

    SANDHelper sandHelper = new SANDHelper();
    HeaderHelper headerHelper = new HeaderHelper();

    @Test(dataProvider = "LatLong", description = "To verify only restaurant listing is present through sand aggregator", priority = 1)
    public void toGetRestaurantListing(String lat, String lon) throws Exception {
        String responseOfSandAggregator = sandHelper.aggregator(new String[]{lat, lon}).ResponseValidator.GetBodyAsText();
        System.out.println(responseOfSandAggregator);
        int statusCodeOfSandAggegatorResponse = JsonPath.read(responseOfSandAggregator, "$.statusCode");


        if (statusCodeOfSandAggegatorResponse == 0) {

            List<String> restIds = Arrays.asList(headerHelper.JsonString(responseOfSandAggregator, "$.data.restaurants[*].id").split(","));
            System.out.println(restIds);

            HashMap<String, Object> restaurantMap = new HashMap<String, Object>();
            restaurantMap = sandHelper.resturantAvailableAndServiceable(responseOfSandAggregator);


            if (restaurantMap.get("opened").toString().equalsIgnoreCase("true")) {
                Reporter.log("We have restaurants which are available in the mentioned environment");
                System.out.println("We have restaurants which are available in the mentioned environment");

                Reporter.log(" ");
                System.out.println(" ");


                Reporter.log("Now checking whether the restaurants are serviceable");
                System.out.println("Now checking whether the restaurants are serviceable");

                Reporter.log(" ");
                System.out.println(" ");


                if (restaurantMap.get("serviceability").toString().equalsIgnoreCase("SERVICEABLE")) {
                    Reporter.log("Yay !!!! The restaurants are serviceable : " + "This is the restaurant which is serviceable and also open : -- " + restaurantMap.get("restaurantId"));
                    System.out.println("Yay !!!! The restaurants are serviceable");
                    Assert.assertTrue(true, "The restaurants are available and serviceable");
                } else {

                    Assert.assertTrue(false, "Since restaurants are unserviceable, Please contact delivery QA team for making restaurants serviceable  : id's :" + restIds);
                }

            } else {

                Reporter.log("Aargh ! looks like no restaurant is open....");
                System.out.println("Aargh ! looks like no restaurant is open....");
                Reporter.log(" ");
                System.out.println(" ");

                Reporter.log("Lets debug ...Wait ... Checking in Sand Redis");
                System.out.println("Lets debug ...Wait ... Checking in Sand Redis");

                    /*To get the menu id from DB, we make a DB call and get the value for a given Key. For this we have to
                    1. make a query and send to a helper method
                    2. Also send Key for which we need a value.

                    The result can be captured in the String menuIdQueryResult
                    */

                /*Building the query */
                //Integer restId = (Integer)(restaurantMap.get("restaurantId"));
                String query = "select menu_id from restaurant_menu_map where restaurant_id=" + restaurantMap.get("restauranntId");

                Long menuIdQueryResult = sandHelper.getValueFromDb(query, "menu_id");

                Reporter.log("Menu id for the request : " + menuIdQueryResult);
                System.out.println("Menu id for the request : " + menuIdQueryResult);

                sandHelper.addingMenuToRedis(menuIdQueryResult, 7);
                List<Map<String, Object>> itemMap = sandHelper.putAllItemsInRedis((Integer) restaurantMap.get("restaurantId"), 7);

                String avMenuRedisKey = "AV_MENU_" + menuIdQueryResult;
                String avItemRedisKey = "AV_ITEM_" + itemMap.get(0).get("item_id").toString();

                System.out.println("AV menu id :" + avMenuRedisKey);
                System.out.println("AV item id :" + avItemRedisKey);

                //To check the redis once its added
                RedisHelper redisHelper = new RedisHelper();

                /*Now to check the menu is present for the menu id inserted*/

                Object avMenuObjectFromRedis = redisHelper.getValue("sandredisstage", 0, avMenuRedisKey);
                if (avMenuObjectFromRedis != null) {
                    boolean status = JsonPath.read(avMenuObjectFromRedis.toString(), "$.status");
                    if (status) {
                        Object avItemObjectFromRedis = redisHelper.getValue("sandredisstage", 0, avItemRedisKey);
                        if (avItemObjectFromRedis != null) {
                            boolean statusOfItem = JsonPath.read(avMenuObjectFromRedis.toString(), "$.status");
                            if (statusOfItem) {
                                Assert.assertTrue(true);
                            }
                        }
                    }
                }


                System.out.println("Lets make sand aggregator call again to check the available events are pushed and are part of API response");
                Reporter.log("Lets make sand aggregator call again to check the available events are pushed and are part of API response");

                String responseOfSandAggregatorAgain = sandHelper.aggregator(new String[]{lat, lon}).ResponseValidator.GetBodyAsText();
                int statusCodeOfSandAggegatorResponseAgain = JsonPath.read(responseOfSandAggregatorAgain, "$.statusCode");
                if (statusCodeOfSandAggegatorResponseAgain == 0) {
                    List<Integer> sortedListAgain = JsonPath.read(responseOfSandAggregatorAgain, "$.data.sortedRestaurants");
                    for (Object restIdInAgainResponse : sortedListAgain) {
                        if (restIdInAgainResponse == restaurantMap.get("restaurantId")) {
                            Assert.assertTrue(true);
                            break;
                        }
                    }
                }

            }
        } else {
            System.out.println("Sand aggregator is throwing 500, Please check with Sand QA ....");
            Assert.assertEquals(statusCodeOfSandAggegatorResponse, "200", "Sand Aggregator issue || Sand Aggregator is throwing " + statusCodeOfSandAggegatorResponse + "  for aggregator. Please connect with SAND QA ");

        }
    }


    @Test(dataProvider = "LatLong", description = "Checking whether gandalf is working or not", dependsOnMethods = "toGetRestaurantListing")
    public void debugListingFromRNG(String lat, String lon) {

        Reporter.log("----------------------########----------------------");
        System.out.println("----------------------########----------------------");

        Reporter.log("Now Checking Gandalf with lsiting ..... ");
        System.out.println("Now Checking Gandalf with lsiting ..... ");

        Reporter.log("----------------------########----------------------");
        System.out.println("----------------------########----------------------");

        Processor listingWithGandalf = sandHelper.listingWithGandalf(lat, lon, "");
        String i2 = ((Integer) listingWithGandalf.ResponseValidator.GetResponseCode()).toString();

        if (i2.equalsIgnoreCase("200")) {

            Reporter.log("----------------------########----------------------");
            System.out.println("----------------------########----------------------");

            System.out.println("Gandalf is working fine .....");
            Reporter.log("Gandalf is working fine .....");

            Assert.assertTrue(true, "Gandalf is working fine with listing....");
        } else {
            Assert.assertTrue(false, "Gandalf issue || Gandalf is throwing " + i2 + "  for listing. Please connect with RNG QA ");
        }
    }


    @Test(dataProvider = "LatLong", description = "Checking whether checkout is working or not.", dependsOnMethods = "debugAggregatorFromCheckout")
    public void debugListingFromCheckout(String lat, String lon) {

        Reporter.log("----------------------########----------------------");
        System.out.println("----------------------########----------------------");

        Reporter.log("Now verifying listing with Checkout ..... ");
        System.out.println("Now verifying listing with Checkout ..... ");

        Reporter.log("----------------------########----------------------");
        System.out.println("----------------------########----------------------");

        Processor listingWithCheckout = sandHelper.listingWithCheckout(lat, lon);
        String i1 = ((Integer) listingWithCheckout.ResponseValidator.GetResponseCode()).toString();


        Reporter.log("----------------------########----------------------");
        System.out.println("----------------------########----------------------");


        Assert.assertEquals(i1, "200", "Checkout issue || Checkout is throwing " + i1 + "  for listing. Please connect with CHECKOUT QA ");

    }


    @Test(dataProvider = "LatLong", description = "Checking whether checkout is working for aggregator or not.", dependsOnMethods = "debugListingFromRNG")
    public void debugAggregatorFromCheckout(String lat, String lon) {

        Reporter.log("----------------------########----------------------");
        System.out.println("----------------------########----------------------");

        Reporter.log("Now verifying aggregator with Checkout ..... ");
        System.out.println("Now verifying aggregator with Checkout ..... ");

        Reporter.log("----------------------########----------------------");
        System.out.println("----------------------########----------------------");

        Processor aggregatorWithCheckout = sandHelper.aggregatorWithCheckout(new String[]{lat, lon});
        String i = ((Integer) aggregatorWithCheckout.ResponseValidator.GetResponseCode()).toString();

        System.out.println("aggregatorWithCheckout ==== " + i);

        Assert.assertEquals(i, "200", "Checkout issue || Checkout is throwing " + i + "  for aggregator. Please connect with CHECKOUT QA ");


    }

}