/**
 * @author manu.chadha
 */


package com.swiggy.api.sf.rng.pojo.freedelivery;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class FreeDeliverySlot {

    @JsonProperty("openTime")
    private Integer openTime;
    @JsonProperty("closeTime")
    private Integer closeTime;
    @JsonProperty("day")
    private String day;

    /**
     * No args constructor for use in serialization
     *
     */
    public FreeDeliverySlot() {
    }

    /**
     *
     * @param closeTime
     * @param openTime
     * @param day
     */
    public FreeDeliverySlot(Integer openTime, Integer closeTime, String day) {
        super();
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.day = day;
    }

    @JsonProperty("openTime")
    public Integer getOpenTime() {
        return openTime;
    }

    @JsonProperty("openTime")
    public void setOpenTime(Integer openTime) {
        this.openTime = openTime;
    }

    @JsonProperty("closeTime")
    public Integer getCloseTime() {
        return closeTime;
    }

    @JsonProperty("closeTime")
    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    @JsonProperty("day")
    public String getDay() {
        return day;
    }

    @JsonProperty("day")
    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("openTime", openTime).append("closeTime", closeTime).append("day", day).toString();
    }

}