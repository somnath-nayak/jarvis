package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "restaurantListing",
        "cartBlobPOJO",
        "cartUserPOJO",
        "cartPricePOJO",
        "superUser",
        "typeOfPartner",
        "orderRequest",
        "swiggyMoneyApplicable"
})
public class CartPOJO {

    @JsonProperty("restaurantListing")
    private RestaurantListingPOJO restaurantListing;
    @JsonProperty("cartBlob")
    private CartBlobPOJO cartBlob;
    @JsonProperty("cartUser")
    private CartUserPOJO cartUser;
    @JsonProperty("cartPrice")
    private CartPricePOJO cartPrice;
    @JsonProperty("superUser")
    private Boolean superUser;
    @JsonProperty("typeOfPartner")
    private String typeOfPartner;
    @JsonProperty("orderRequest")
    private String orderRequest;
    @JsonProperty("swiggyMoneyApplicable")
    private Boolean swiggyMoneyApplicable;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public CartPOJO() {
    }

    /**
     *
     * @param cartBlob
     * @param restaurantListing
     * @param cartUser
     * @param orderRequest
     * @param swiggyMoneyApplicable
     * @param typeOfPartner
     * @param cartPrice
     * @param superUser
     */
    public CartPOJO(RestaurantListingPOJO restaurantListing, CartBlobPOJO cartBlob, CartUserPOJO cartUser, CartPricePOJO cartPrice, Boolean superUser, String typeOfPartner, String orderRequest, Boolean swiggyMoneyApplicable) {
        super();
        this.restaurantListing = restaurantListing;
        this.cartBlob = cartBlob;
        this.cartUser = cartUser;
        this.cartPrice = cartPrice;
        this.superUser = superUser;
        this.typeOfPartner = typeOfPartner;
        this.orderRequest = orderRequest;
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
    }

    @JsonProperty("restaurantListing")
    public RestaurantListingPOJO getRestaurantListing() {
        return restaurantListing;
    }

    @JsonProperty("restaurantListing")
    public void setRestaurantListing(RestaurantListingPOJO restaurantListing) {
        this.restaurantListing = restaurantListing;
    }

    public CartPOJO withRestaurantListing(RestaurantListingPOJO restaurantListing) {
        this.restaurantListing = restaurantListing;
        return this;
    }

    @JsonProperty("cartBlob")
    public CartBlobPOJO getCartBlob() {
        return cartBlob;
    }

    @JsonProperty("cartBlob")
    public void setCartBlob(CartBlobPOJO cartBlob) {
        this.cartBlob = cartBlob;
    }

    public CartPOJO withCartBlob(CartBlobPOJO cartBlob) {
        this.cartBlob = cartBlob;
        return this;
    }

    @JsonProperty("cartUser")
    public CartUserPOJO getCartUser() {
        return cartUser;
    }

    @JsonProperty("cartUser")
    public void setCartUser(CartUserPOJO cartUser) {
        this.cartUser = cartUser;
    }

    public CartPOJO withCartUser(CartUserPOJO cartUser) {
        this.cartUser = cartUser;
        return this;
    }

    @JsonProperty("cartPrice")
    public CartPricePOJO getCartPrice() {
        return cartPrice;
    }

    @JsonProperty("cartPrice")
    public void setCartPrice(CartPricePOJO cartPrice) {
        this.cartPrice = cartPrice;
    }

    public CartPOJO withCartPrice(CartPricePOJO cartPrice) {
        this.cartPrice = cartPrice;
        return this;
    }

    @JsonProperty("superUser")
    public Boolean getSuperUser() {
        return superUser;
    }

    @JsonProperty("superUser")
    public void setSuperUser(Boolean superUser) {
        this.superUser = superUser;
    }

    public CartPOJO withSuperUser(Boolean superUser) {
        this.superUser = superUser;
        return this;
    }

    @JsonProperty("typeOfPartner")
    public Object getTypeOfPartner() {
        return typeOfPartner;
    }

    @JsonProperty("typeOfPartner")
    public void setTypeOfPartner(String typeOfPartner) {
        this.typeOfPartner = typeOfPartner;
    }

    public CartPOJO withTypeOfPartner(String typeOfPartner) {
        this.typeOfPartner = typeOfPartner;
        return this;
    }

    @JsonProperty("orderRequest")
    public String getOrderRequest() {
        return orderRequest;
    }

    @JsonProperty("orderRequest")
    public void setOrderRequest(String orderRequest) {
        this.orderRequest = orderRequest;
    }

    public CartPOJO withOrderRequest(String orderRequest) {
        this.orderRequest = orderRequest;
        return this;
    }

    @JsonProperty("swiggyMoneyApplicable")
    public Boolean getSwiggyMoneyApplicable() {
        return swiggyMoneyApplicable;
    }

    @JsonProperty("swiggyMoneyApplicable")
    public void setSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
    }

    public CartPOJO withSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CartPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public CartPOJO setDefault(){

        RestaurantListingPOJO restaurantListingPOJO = new RestaurantListingPOJO().setDefault();
        CartBlobPOJO cartBlobPOJO = new CartBlobPOJO().setDefault();
        CartPricePOJO cartPricePOJO = new CartPricePOJO().setDefault();
        CartUserPOJO cartUserPOJO = new CartUserPOJO().setDefault();
        return this.withRestaurantListing(restaurantListingPOJO)
                .withCartBlob(cartBlobPOJO)
                .withCartUser(cartUserPOJO)
                .withCartPrice(cartPricePOJO)
                .withSuperUser(false)
                .withTypeOfPartner(null)
                .withOrderRequest(null)
                .withSwiggyMoneyApplicable(true);
    }


}
