package com.swiggy.api.sf.checkout.pojo.mySms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;


@JsonPropertyOrder({
        "apiKey",
        "authToken",
        "address"
})
public class MySmsDeleteMessage {

    @JsonProperty("apiKey")
    private String apiKey;
    @JsonProperty("authToken")
    private String authToken;
    @JsonProperty("address")
    private String address;

    /**
     * No args constructor for use in serialization
     *
     */
    public MySmsDeleteMessage() {
    }

    /**
     *
     * @param address
     * @param authToken
     * @param apiKey
     */
    public MySmsDeleteMessage(String apiKey, String authToken, String address) {
        super();
        this.apiKey = apiKey;
        this.authToken = authToken;
        this.address = address;
    }

    @JsonProperty("apiKey")
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty("apiKey")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonProperty("authToken")
    public String getAuthToken() {
        return authToken;
    }

    @JsonProperty("authToken")
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("apiKey", apiKey).append("authToken", authToken).append("address", address).toString();
    }

}