package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class City {

	@JsonProperty("cityEntity")
	private CityEntity cityEntity;

	@JsonProperty("cityEntity")
	public CityEntity getCityEntity() {
	return cityEntity;
	}

	@JsonProperty("cityEntity")
	public void setCityEntity(CityEntity cityEntity) {
	this.cityEntity = cityEntity;
	}

	public City withCityEntity(CityEntity cityEntity) {
	this.cityEntity = cityEntity;
	return this;
	}
}
