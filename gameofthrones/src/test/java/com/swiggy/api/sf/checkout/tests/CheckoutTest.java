
package com.swiggy.api.sf.checkout.tests;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import io.gatling.core.json.Json;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.dp.CheckoutDP;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.SuperHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.CartValidator;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import com.swiggy.automation.common.utils.APIUtils;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.ExcelHelper;
import framework.gameofthrones.Tyrion.JsonHelper;

public class CheckoutTest extends CheckoutDP {

    Initialize gameofthrones = new Initialize();
    CheckoutHelper helper= new CheckoutHelper();
    SnDHelper sndHelper= new SnDHelper();
    SANDHelper sandHelper= new SANDHelper();
    CartValidator cartValidator =new CartValidator();
    SuperHelper superHelper=new SuperHelper();
    EDVOCartHelper eDVOCartHelper=new EDVOCartHelper();
    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();
    RngHelper rngHelper = new RngHelper();
    JsonHelper jsonHelper = new JsonHelper();
    String order_id=null;
    String order_key=null;
    String order=null;
    AddressHelper addressHelper=new AddressHelper();
    String menuItemId;
    String restaurantID;
    String tid;
    String token;
    
//   @Test(priority=-100)
//    public void tt()
//    {
//    	  ExcelHelper ex=new ExcelHelper();
//   	   System.out.println("Hai1 :"+ex.getXLSDataFilePath());
//   	   System.out.println("Hai2 :"+ex.getXLSDataSheetName());
//    }

   @BeforeMethod
   public void beforeMethod(){
	   HashMap<String, String> hashMap=helper.TokenData(CheckoutConstants.mobile1, CheckoutConstants.password1);
	   tid=hashMap.get("Tid");
       token=hashMap.get("Token");
	   restaurantID=sandHelper.Aggregator(new String[]{CheckoutConstants.lat,CheckoutConstants.lng});
       Cart processor=eDVOCartHelper.getCartPayload(new String[0], restaurantID, false, false, true);
       menuItemId=processor.getCartItems().get(0).getMenuItemId();   
   }
   
   @Test(dataProvider = "getinventory",priority=0)
    public void GetBulkInventory(String item_schedule_id)
    {
    	 
    	 GenertaeRandomOrderID();
    	 HashMap<String, String> requestheaders = new HashMap<String, String>();
     requestheaders.put("content-type", "application/json");
         GameOfThronesService service = new GameOfThronesService("checkout", "getinventoryv1", gameofthrones);
         String[] payloadparams = new String[1];
         payloadparams[0] = item_schedule_id; 
         Processor processor = new Processor(service, requestheaders, payloadparams);
        String resp=processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(200,processor.ResponseValidator.GetResponseCode());
        String statusMessage=JsonPath.read(resp,"$.statusMessage");
        int statusCode=JsonPath.read(resp,"$.statusCode");
        Assert.assertEquals(0,statusCode);	
        Assert.assertEquals("Success",statusMessage);

    }
     
    @Test(dataProvider = "getInventoryLock",priority=1)
     public void GetInventoryLock(String item_schedule_id, String inventory, String request_id, String order_id)
     {
     	 HashMap<String, String> requestheaders = new HashMap<String, String>();
          requestheaders.put("content-type", "application/json");

          GameOfThronesService service = new GameOfThronesService("checkout", "getinventorylockv1", gameofthrones);
          String[] payloadparams = new String[4];
          payloadparams[0] = item_schedule_id;
          payloadparams[1] = inventory;
          payloadparams[2] = request_id;
          payloadparams[3] = order_id; 
          Processor processor = new Processor(service, requestheaders, payloadparams);
         String resp=processor.ResponseValidator.GetBodyAsText();
         System.out.println(resp);
         Assert.assertEquals(200,processor.ResponseValidator.GetResponseCode());
         String statusMessage=JsonPath.read(resp,"$.statusMessage");
         int statusCode=JsonPath.read(resp,"$.statusCode");
         Assert.assertEquals(statusCode,0);
         Assert.assertEquals("SUCCESS: Lock Acquired",statusMessage);

     }
     
    @Test(dataProvider = "getInventoryLockSameOrderId",priority=4)
     public void GetInventoryLockWithduplicateOrderId(String item_schedule_id, String inventory, String request_id, String order_id)
     {
     	 HashMap<String, String> requestheaders = new HashMap<String, String>();
          requestheaders.put("content-type", "application/json");

          GameOfThronesService service = new GameOfThronesService("checkout", "getinventorylockv1", gameofthrones);
          String[] payloadparams = new String[4];
          payloadparams[0] = item_schedule_id;
          payloadparams[1] = inventory;
          payloadparams[2] = request_id;
          payloadparams[3] = order_id; 
          Processor processor = new Processor(service, requestheaders, payloadparams);
         String resp=processor.ResponseValidator.GetBodyAsText();
         System.out.println(resp);
         Assert.assertEquals(200,processor.ResponseValidator.GetResponseCode());
         String statusMessage=JsonPath.read(resp,"$.statusMessage");
         int statusCode=JsonPath.read(resp,"$.statusCode");
         Assert.assertEquals(statusCode,1);
         Assert.assertEquals("FAILURE: Unable to get inventory lock. Duplicate Orders",statusMessage);

     }
     
    @Test(dataProvider = "confirmInventoryLockSameOrderId",priority=5)
     public void ConfirmInventoryLockWithduplicateOrderId(String item_schedule_id, String inventory, String request_id, String order_id)
     {
     	
         String resp=helper.ConfirmLock(item_schedule_id, inventory, request_id, order_id).ResponseValidator.GetBodyAsText();
         String statusMessage=JsonPath.read(resp,"$.statusMessage");
         int statusCode=JsonPath.read(resp,"$.statusCode");
         Assert.assertEquals(1,statusCode);
         Assert.assertEquals("FAILURE: Cannot Confirm order",statusMessage);

     }
     
    @Test(dataProvider = "confirmInventoryLock",priority=3)
     public void ConfirmInventoryLock(String item_schedule_id, String inventory, String request_id, String order_id)
     {
     	
         String resp=helper.ConfirmLock(item_schedule_id, inventory, request_id, order_id).ResponseValidator.GetBodyAsText();
         String statusMessage=JsonPath.read(resp,"$.statusMessage");
         int statusCode=JsonPath.read(resp,"$.statusCode");
         Assert.assertEquals(0,statusCode);
         Assert.assertEquals("SUCCESS: Order Confirmed",statusMessage);

     }
     
     
    @Test(dataProvider = "removeInventoryLock",priority=6)
     public void RemoveInventoryLock(String item_schedule_id, String inventory, String request_id, String order_id)
     {
     	
         String resp=helper.RemoveLock(item_schedule_id, inventory, request_id, order_id).ResponseValidator.GetBodyAsText();
         String statusMessage=JsonPath.read(resp,"$.statusMessage");
         int statusCode=JsonPath.read(resp,"$.statusCode");
         Assert.assertEquals(0,statusCode);
         Assert.assertEquals("SUCCESS: Lock Removed",statusMessage);

     }
     
     
    @Test(dataProvider = "cancelInventory",priority=6)
     public void cancelInventoryLock(String item_schedule_id, String inventory, String request_id, String order_id)
     {
     	
         String resp=helper.cancelInventory(item_schedule_id, inventory, request_id, order_id).ResponseValidator.GetBodyAsText();
         String statusMessage=JsonPath.read(resp,"$.statusMessage");
         int statusCode=JsonPath.read(resp,"$.statusCode");
         Assert.assertEquals(0,statusCode);
         Assert.assertEquals("Success: Order Successfully Cancelled",statusMessage);

     }
    
   @Test(dataProvider = "inventoryExpire",priority=7)
    public void InventoryExpire(String item_schedule_id, String inventory, String request_id, String order_id)
    {
        String resp=helper.InvExpire(item_schedule_id,inventory,  request_id, order_id).ResponseValidator.GetBodyAsText();
        String statusMessage=JsonPath.read(resp,"$.statusMessage");
        int statusCode=JsonPath.read(resp,"$.statusCode");
        Assert.assertEquals(0,statusCode);
        Assert.assertEquals("Success",statusMessage);
    }

   @Test(dataProvider="cartCreationV2", groups = {"Regression","Smoke"}, description = "1.Get Tid, token from login 2.Create a normal cart 3.Verify using get cart")
    public void CreateCartWithoutDiscount(String mobile, String password, String lat, String lng, String PayloadTime)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String restaurantId=helper.restId(lat,lng);
        String response= helper.CreateCart(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId).ResponseValidator.GetBodyAsText();
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
    }


   @Test(dataProvider = "cartDeletion",groups = { "Regression","Smoke", "Jitender"}, description = "1.Get Tid, token from login 2.Create a cart which contains a")
    public void CreateAndDeleteCart(String mobile, String password, String lat, String lng, String PayloadTime, String message){
        int n = Integer.valueOf(PayloadTime);
        //String restaurantId=helper.restId(lat, lng);
        //String response= helper.CreateCart(n, tid,token, restaurantID).ResponseValidator.GetBodyAsText();
        String response=superHelper.callCreateCart(tid, token, menuItemId, CheckoutConstants.ITEM_QUANTITY, restaurantID, "", "");
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String getCart1= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String deleteCart= helper.DeleteCart(tid,token).ResponseValidator.GetBodyAsText();
        String cartMessage= JsonPath.read(deleteCart, "$.statusMessage").toString().replace("[","").replace("]","");
        String deleteTid=JsonPath.read(response, "$.tid").toString().replace("[","").replace("]","");
        Assert.assertEquals(deleteTid,tid);
        Assert.assertEquals(cartMessage, message);
        String getCartAfterDelete= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
        String dataNode= JsonPath.read(getCartAfterDelete, "$.data");
        Assert.assertNull(dataNode);
    }

   @Test(dataProvider = "cartCreationAddon", groups = { "Regression","Smoke", "Jitender"}, description = "1.Get Tid, token from login 2.Create a cart which contains addon 3.verify Cart using get cart api")
    public void CreateCartAddOnCart(String mobile, String password, String lat, String lng, String PayloadTime,String restID) {
        //HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        //List<String> restList= rngHelper.getRestaurantList();
        int n = Integer.valueOf(PayloadTime);
        //String restId = restaurantId;
        String response = helper.CreateCartAddons(n, tid, token, restaurantID).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String addOnGroupList = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..group_id").toString().replace("[", "").replace("]", "");
        String choiceIdList = JsonPath.read(response, "$.data..addons..choices..id").toString().replace("[", "").replace("]", "");
        List<String> choiceIDlist1= Arrays.asList(choiceIdList.split(","));
        List<String> addOnlist= Arrays.asList(addOnGroupList.split(","));
        Assert.assertNotNull(addOnGroupList);
        String rest= sandHelper.getRestaurantMenu(restaurantID).ResponseValidator.GetBodyAsText();
        String addOnGroupList1 = JsonPath.read(response, "$.data..addons..group_id").toString().replace("[", "").replace("]", "");
        String addOnChoiceList1 = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..choices..id").toString().replace("[", "").replace("]", "");
        Assert.assertTrue(addOnGroupList1.contains(addOnlist.get(0)));
        Assert.assertTrue(addOnChoiceList1.contains(choiceIDlist1.get(0)));
        String getCart1= helper.GetCart(tid, token).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
    }

   @Test(dataProvider = "cartCreationDeleteAddon",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login 2.Create a addon cart, 3.verify Cart using get cart api 4. Delete the cart")
    public void CreateAndDeleteAddOnCart(String mobile, String password, String lat, String lng, String PayloadTime, String message) {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        List<String> restList= rngHelper.getRestaurantList();
        int n = Integer.valueOf(PayloadTime);
        //String restId = restaurantId;
        String response = helper.CreateCartAddons(n, hashMap.get("Tid"), hashMap.get("Token"),restList.get(0)).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String addOnGroupList = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..group_id").toString().replace("[", "").replace("]", "");
        String choiceIdList = JsonPath.read(response, "$.data..addons..choices..id").toString().replace("[", "").replace("]", "");
        List<String> choiceIDlist1= Arrays.asList(choiceIdList.split(","));
        List<String> addOnlist= Arrays.asList(addOnGroupList.split(","));
        Assert.assertNotNull(addOnGroupList);
        String rest= sandHelper.getRestaurantMenu(restList.get(0)).ResponseValidator.GetBodyAsText();
        String addOnGroupList1 = JsonPath.read(response, "$.data..addons..group_id").toString().replace("[", "").replace("]", "");
        String addOnChoiceList1 = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..choices..id").toString().replace("[", "").replace("]", "");
        Assert.assertTrue(addOnGroupList1.contains(addOnlist.get(0)));
        Assert.assertTrue(addOnChoiceList1.contains(choiceIDlist1.get(0)));
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        //helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String deleteCart= helper.DeleteCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String cartMessage= JsonPath.read(deleteCart, "$.statusMessage").toString().replace("[","").replace("]","");
        String deleteTid=JsonPath.read(response, "$.tid").toString().replace("[","").replace("]","");
        Assert.assertEquals(deleteTid,hashMap.get("Tid"));
        Assert.assertEquals(cartMessage, message);
        String getCartAfterDelete= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String dataNode= JsonPath.read(getCartAfterDelete, "$.data");
        Assert.assertNull(dataNode);
    }


   @Test(dataProvider = "cartCreationWithCoupon",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a cart, 3.verify Cart using get cart api, 4. Apply coupon, 5. Verify using get cart api")
    public void CreateAndApplyCouponOnCart(String mobile, String password, String lat, String lng, String PayloadTime, String couponcode)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String restaurantId=helper.restId(lat,lng);
        String response= helper.CreateCart(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId).ResponseValidator.GetBodyAsText();
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), couponcode).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(applyCouponResponse,restId,menuItem,n);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
    }

   @Test(dataProvider="cartCreationClosed", groups = {"Regression","Smoke"}, description = "1.Get Tid, token from login 2.Create a normal cart for a closed 3.Verify using get cart")
    public void CreateCartRestaurantClosed(String mobile, String password, String lat, String lng, String PayloadTime, String closeMesssage)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String restaurantId=helper.closedRestId(lat,lng);
        String response= helper.CreateCart(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId).ResponseValidator.GetBodyAsText();
        String close= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(closeMesssage,close,"Restaurant is in open state");
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
    }

   @Test(dataProvider = "deleteCouponOnCart",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a cart, 3.verify Cart using get cart api, 4. Apply coupon, 5. Verify using get api, 6.Delete coupon, 7. Again verify using get cart api")
    public void ApplyAndDeleteCouponOnCart(String mobile, String password, String lat, String lng, String PayloadTime, String couponcode) {
        int n = Integer.valueOf(PayloadTime);
        String response=superHelper.callCreateCart(tid, token, menuItemId, CheckoutConstants.ITEM_QUANTITY, restaurantID, "", "");
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String applyCouponResponse= helper.ApplyCouponOnCart(tid,token, couponcode).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(applyCouponResponse,restId,menuItem,n);
        String getCart1= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String deleteCouponResponse= helper.DeleteCouponOnCart(tid,token, couponcode).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(deleteCouponResponse,restId,menuItem,n);
    }

   @Test(dataProvider = "addonWithCoupon",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a cart, 3.verify Cart using get cart api, 4. Apply coupon, 5. Verify using get api")
    public void ApplyCouponOnAddOnCart(String mobile, String password, String lat, String lng, String PayloadTime, String couponCode) {
        int n = Integer.valueOf(PayloadTime);
        String response = helper.CreateCartAddons(n, tid,token, restaurantID).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String addOnGroupList = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..group_id").toString().replace("[", "").replace("]", "");
        String choiceIdList = JsonPath.read(response, "$.data..addons..choices..id").toString().replace("[", "").replace("]", "");
        List<String> choiceIDlist1= Arrays.asList(choiceIdList.split(","));
        List<String> addOnlist= Arrays.asList(addOnGroupList.split(","));
        Assert.assertNotNull(addOnGroupList);
        String getCart1= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String applyCouponResponse= helper.ApplyCouponOnCart(tid,token, couponCode).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(applyCouponResponse,restId,menuItem,n);
        String getCart2= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart2,restId,menuItem,n);
    }

   @Test(dataProvider = "addonWithDeleteCoupon",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Apply coupon, 5. Verify using get api, 6. Delete Coupon, 7.Verify using get acart api ")
    public void ApplyAndDeleteCouponAddOnCart(String mobile, String password, String lat, String lng, String PayloadTime, String couponCode) {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        String tid=hashMap.get("Tid");
        String token=hashMap.get("Token");
        int n = Integer.valueOf(PayloadTime);
        //List<String> restList= rngHelper.getRestaurantList();
        String restID=sandHelper.Aggregator(new String[]{CheckoutConstants.lat,CheckoutConstants.lng});
        Cart processor=eDVOCartHelper.getCartPayload(new String[0], restID, false, false, true);
		String menuItemId=processor.getCartItems().get(0).getMenuItemId();
        String response=superHelper.callCreateCart(tid, token, menuItemId, CheckoutConstants.ITEM_QUANTITY, restID, "", "");
        //String response = helper.CreateCartAddons(n, hashMap.get("Tid"), hashMap.get("Token"), restID).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String addOnGroupList = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..group_id").toString().replace("[", "").replace("]", "");
        String choiceIdList = JsonPath.read(response, "$.data..addons..choices..id").toString().replace("[", "").replace("]", "");
        List<String> choiceIDlist1= Arrays.asList(choiceIdList.split(","));
        List<String> addOnlist= Arrays.asList(addOnGroupList.split(","));
        Assert.assertNotNull(addOnGroupList);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), couponCode).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(applyCouponResponse,restId,menuItem,n);
        String getCart2= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String deleteCouponResponse= helper.DeleteCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), couponCode).ResponseValidator.GetBodyAsText();
        String getRestId = JsonPath.read(getCart2, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String getMenuItem = JsonPath.read(getCart2, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        helper.validateDataFromResponseNew(deleteCouponResponse,getRestId,getMenuItem,n);
    }

   @Test(dataProvider = "negativeQuantity",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart and qauntity will be like 0, 3.verify Cart using get cart api")
    public void negativeScenariosOnCart(String mobile, String password, String lat, String lng, String PayloadTime, String restaurantId, String quantity, int status, String messsage) {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        //String restId = restaurantId;
        String response = helper.CreateCartAddonsNegative(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId,quantity).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        int stat=Integer.parseInt(statusCode);
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        if(Integer.parseInt(statusCode)==1)
        {
            Assert.assertEquals(stat,status);
            Assert.assertEquals(messsage,statusMessage);
        }
        else
        {
            String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
            String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
            String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
            helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        }
    }


    /*@Test(dataProvider = "createCartFlatTrade",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a normal cart, 3.verify Cart using get cart api, 4. Verify TD Flat disocunt 5.Verify using get cart api")
    public void TradeDiscountFlatOnCart(String mobile, String password,  String PayloadTime)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        List<String> restList= rngHelper.getRestaurantList();
        int n = Integer.valueOf(PayloadTime);
        String response = helper.CreateCart(n, hashMap.get("Tid"), hashMap.get("Token"), restList.get(4)).ResponseValidator.GetBodyAsText();
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        helper.validateDataFromResponseNew(response,restId,menuItem,n);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
    }*/

   @Test(dataProvider = "tradeDiscountFlatAddon",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD disocunt 5.Verify using get cart api")
    public void TradeDiscountFlatAddOnCart(String mobile, String password,  String PayloadTime)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        List<String> restList= rngHelper.getRestaurantList();
        int n = Integer.valueOf(PayloadTime);
        String response = helper.CreateCartAddons(n, hashMap.get("Tid"), hashMap.get("Token"), restList.get(4)).ResponseValidator.GetBodyAsText();
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        helper.validateDataFromResponseNew(response,restId,menuItem,n);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
    }

    /*@Test(dataProvider = "createCartTradeAndCoupon",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD Free Delivery discount 5. Apply coupon on the same cart 6.Verify using get cart api")
    public void TradeDiscountAndCouponOnCart(String mobile, String password, String lat, String lng, String PayloadTime, String Coupon)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        List<String> restList= rngHelper.getRestaurantList();
        int n = Integer.valueOf(PayloadTime);
        String response = helper.CreateCartAddons(n, hashMap.get("Tid"), hashMap.get("Token"), restList.get(4)).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        helper.validateDataFromResponseNew(response,restId,menuItem,n);
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), Coupon).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(applyCouponResponse,restId,menuItem,n);
        String getCart2= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart2,restId,menuItem,n);
    }*/

   // @Test (dataProvider = "createCartSchema", groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a normal cart, 3.Verify Cart validation for the same cart")
    public void createCartSchemaValidationTest(String mobile, String password, String lat, String lng, String PayloadTime, String restaurantId) throws IOException, ProcessingException {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        List<String> restList= rngHelper.getRestaurantList();
        int n = Integer.valueOf(PayloadTime);
        String response = helper.CreateCart(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId).ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Checkout/createcarttv2.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,response);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For create cart API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");

    }

   @Test (dataProvider = "applyCouponSchema", groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a normal cart,3. Apply coupon  4.Verify apply coupon validation for the same cart")
    public void applyCouponSchemaValidationTest(String mobile, String password, String lat, String lng, String PayloadTime, String restaurantId, String couponCode) throws IOException, ProcessingException {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String response = helper.CreateCart(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId).ResponseValidator.GetBodyAsText();
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), couponCode).ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Checkout/applycoupon.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,applyCouponResponse);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For apply coupon API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test (dataProvider = "applyCouponSchema", groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a normal cart,3. Apply coupon  4.Verify apply coupon validation for the same cart")
    public void deleteCouponSchemaValidationTest(String mobile, String password, String lat, String lng, String PayloadTime, String restaurantId, String couponCode) throws IOException, ProcessingException {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String response = helper.CreateCart(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId).ResponseValidator.GetBodyAsText();
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), couponCode).ResponseValidator.GetBodyAsText();
        String deleteCouponResponse= helper.DeleteCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), couponCode).ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Checkout/deletecoupon.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,deleteCouponResponse);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete coupon API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    /*@Test(dataProvider = "createCartTradeAndCouponAndDelete",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD Free Delivery discount 5. Apply coupon on the same cart 6.Verify using get cart api")
    public void TradeDiscountAndCouponAndDeleteCart(String mobile, String password, String lat, String lng, String PayloadTime, String Coupon, String message)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        List<String> restList= rngHelper.getRestaurantList();
        int n = Integer.valueOf(PayloadTime);
        String response = helper.CreateCartAddons(n, hashMap.get("Tid"), hashMap.get("Token"), restList.get(4)).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        helper.validateDataFromResponseNew(response,restId,menuItem,n);
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), Coupon).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(applyCouponResponse,restId,menuItem,n);
        String getCart2= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart2,restId,menuItem,n);
        String deleteCart= helper.DeleteCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String cartMessage= JsonPath.read(deleteCart, "$.statusMessage").toString().replace("[","").replace("]","");
        String deleteTid=JsonPath.read(response, "$.tid").toString().replace("[","").replace("]","");

        Assert.assertEquals(deleteTid,hashMap.get("Tid"));
        Assert.assertEquals(cartMessage, message);
        String getCartAfterDelete= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String dataNode= JsonPath.read(getCartAfterDelete, "$.data");
        Assert.assertNull(dataNode);

    }*/

/*@Test (dataProvider = "deleteCouponSchema", groups = {"regression"})
    public void deleteCouponSchemaValidationTest(String mobile, String password, String lat, String lng, String PayloadTime, String restaurantId, String couponCode) throws IOException, ProcessingException {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String response = helper.CreateCart(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId).ResponseValidator.GetBodyAsText();
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), couponCode).ResponseValidator.GetBodyAsText();
        String deleteCouponResponse= helper.DeleteCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), couponCode).ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/Data/SchemaSet/Json/Checkout/applycoupon.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,deleteCouponResponse);
        Assert.assertTrue(missingNodeList.isEmpty() + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");

    }*/


    /*@Test( dataProvider = "createCartAddonVariants")
    public void createCartAddonVariant(String mobile, String password, String payload)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"),payload).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem, noOfProducts);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
    }*/

   @Test(dataProvider = "outofstock")
    public void outOfStock(CreateMenuEntry payload) throws  IOException
    {
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(payload.getpassword(), payload.getmobile());
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem, noOfProducts);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
    }


   @Test(dataProvider="cartCheckTotalV2", groups = {"Sanity","Smoke"}, description = "cartCheckTotal")
    public void cartCheckTotal(String mobile, String password, String lat, String lng, String PayloadTime)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String restaurantId=helper.restId(lat,lng);
        String response= helper.CreateCart(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId).ResponseValidator.GetBodyAsText();
        //System.out.println("---TID->"+hashMap.get("Tid"));
        //System.out.println("---->"+hashMap.get("Token"));
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
    }


   @Test(dataProvider="minimalCartV2", groups = {"Sanity","Smoke"}, description = "getMinimalCart")
    public void minimalCart(String mobile, String password, String lat, String lng, String PayloadTime)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String restaurantId=helper.restId(lat,lng);
        String response= helper.cartMinimalV2(n, hashMap.get("Tid"), hashMap.get("Token"), restaurantId).ResponseValidator.GetBodyAsText();
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
    }

   @Test(dataProvider = "VariantsDominoOrder",groups = { "Regression", "Smoke", "Sanity", "Jitender", "Dominos" }, description = "1.Git TId , TOken from Login ,2.Create a normal cart using Domino's 3.Verify using get api")
    public void VariantDominoOrder(CreateMenuEntry payload) throws  IOException
    {
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(payload.getpassword(), payload.getmobile());
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem, noOfProducts);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
    }

   @Test(dataProvider = "TDForNormalCart",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD disocunt 5.Verify using get acart api")
    public void TradeDiscountPercentageOnCart(String TDPayload,CreateMenuEntry cartpayload) throws IOException
    {
        String tdResponse=rngHelper.createTD(TDPayload).ResponseValidator.GetBodyAsText();
        String cartPayload= jsonHelper.getObjectToJSON(cartpayload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
    }

   @Test(dataProvider = "TDForAddonCart",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD disocunt 5.Verify using get acart api")
    public void TradeDiscountPercentageAddOnCart(String TDPayload,CreateMenuEntry cartpayload) throws IOException
    {
        String tdResponse=rngHelper.createTD(TDPayload).ResponseValidator.GetBodyAsText();
        String cartPayload= jsonHelper.getObjectToJSON(cartpayload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
    }

   @Test(dataProvider = "TDForFlatCart1",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD disocunt 5.Verify using get acart api")
    public void TradeFlatDiscountOnCart(String itemId,String quantity,String restId) throws IOException
    {
	   rngHelper.createPercentageTD(restId);
       HashMap<String, String> hashMap=helper.createLogin(CheckoutConstants.password1, Long.parseLong(CheckoutConstants.mobile));   
       //String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
       String response= helper.invokeCreateCartAPI(hashMap.get("Tid"), hashMap.get("Token"), itemId, quantity, restId).ResponseValidator.GetBodyAsText(); 
       String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
       String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
       String responseRestId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
       String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
       String[] menuItems= menuItem.split(",");
       int noOfProducts = menuItems.length;
       helper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
       Assert.assertNotNull(CampaignHeader);
       Assert.assertNotNull(Trade_SubTotal);
       String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
       helper.validateDataFromResponseNew(getCart1,responseRestId,menuItem,noOfProducts);
       }

   @Test(dataProvider = "TDForFreeDelCart",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD disocunt 5.Verify using get acart api")
    public void TradeDiscountFreeDelOnCart(String TDPayload,CreateMenuEntry cartpayload) throws IOException
    {
        //String tdResponse=rngHelper.createTD(TDPayload).ResponseValidator.GetBodyAsText();
        rngHelper.createPercentageTD(cartpayload.getRestaurantId().toString());
        String cartPayload= jsonHelper.getObjectToJSON(cartpayload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
    }

   @Test(dataProvider = "TDForFreeDelAddonCart",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD disocunt 5.Verify using get acart api")
    public void TradeDiscountFreeDelAddOnCart(String TDPayload,CreateMenuEntry cartpayload) throws IOException
    {
        String tdResponse=rngHelper.createTD(TDPayload).ResponseValidator.GetBodyAsText();
        String cartPayload= jsonHelper.getObjectToJSON(cartpayload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
    }

   @Test(dataProvider = "TDAndCouponOnCart",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD Free Delivery discount 5. Apply coupon on the same cart 6.Verify using get cart api")
    public void TradeDiscountAnCouponOnCart(String TDPayload,CreateMenuEntry cartpayload, String coupon) throws IOException
    {
        String tdResponse=rngHelper.createTD(TDPayload).ResponseValidator.GetBodyAsText();
        String cartPayload= jsonHelper.getObjectToJSON(cartpayload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), coupon).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(applyCouponResponse,restId,menuItem,noOfProducts);
        String getCart2= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart2,restId,menuItem,noOfProducts);
    }

   @Test(dataProvider = "TDForCateogoryCart",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD Free Delivery discount 5. Apply coupon on the same cart 6.Verify using get cart api")
    public void TradeDiscountCategoryOnCart(String TDPayload,CreateMenuEntry cartpayload) throws IOException {
        String tdResponse = rngHelper.createTD(TDPayload).ResponseValidator.GetBodyAsText();
        String cartPayload = jsonHelper.getObjectToJSON(cartpayload.getCartItems());
        HashMap<String, String> hashMap = helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
        String response = helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String CampaignHeader = JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal = JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems = menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response, restId, menuItem, noOfProducts);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1 = helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1, restId, menuItem, noOfProducts);
    }

   @Test(dataProvider = "TDAndCouponAndDeleteCart",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD Free Delivery discount 5. Apply coupon on the same cart 6.Verify using get cart api")
    public void TradeDiscountAnCouponAndDeleteCart(String TDPayload,CreateMenuEntry cartpayload, String coupon, String message) throws IOException
    {
        String tdResponse=rngHelper.createTD(TDPayload).ResponseValidator.GetBodyAsText();
	    String cartPayload= jsonHelper.getObjectToJSON(cartpayload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), coupon).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(applyCouponResponse,restId,menuItem,noOfProducts);
        String getCart2= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        helper.validateDataFromResponseNew(getCart2,restId,menuItem,noOfProducts);
        String deleteCart= helper.DeleteCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String cartMessage= JsonPath.read(deleteCart, "$.statusMessage").toString().replace("[","").replace("]","");
        String deleteTid=JsonPath.read(response, "$.tid").toString().replace("[","").replace("]","");

        Assert.assertEquals(deleteTid,hashMap.get("Tid"));
        Assert.assertEquals(cartMessage, message);
        String getCartAfterDelete= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String dataNode= JsonPath.read(getCartAfterDelete, "$.data");
        Assert.assertNull(dataNode);
    }

   @Test(dataProvider="userSingleOrder",priority=19)
    public void testOrder(CreateMenuEntry cartpayload) throws IOException
    {
        String cartPayload= jsonHelper.getObjectToJSON(cartpayload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
        String response= helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        //String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        //String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String statusCode=JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(statusCode, "0","Cart Not created");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        helper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        String addressID=getAddressIDfromCartResponse(hashMap.get("Tid"), hashMap.get("Token"),response);
        order= helper.orderPlace1(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(addressID),cartpayload.getpayment_cod_method(),cartpayload.getorder_comments()).ResponseValidator.GetBodyAsText();

        order_id= JsonPath.read(order,"$.data.order_id").toString().replace("[","").replace("]","");
        //order_key= JsonPath.read(order,"$.data.order_key").toString().replace("[","").replace("]","");
        System.out.println(order_id);
        Assert.assertNotNull(order_id);
    }
   
  
   @Test(dataProvider="cartCreationCheck", groups = {"Regression","Smoke"}, description = " 1.Create a normal cart 2.Verify using get cart")
    public void CheckTotalCart(String mobile, String password,String lat, String lng, String PayloadTime)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String restaurantId=helper.restId(lat,lng);
        String response= helper.CreateCartCheckTotals(n, restaurantId).ResponseValidator.GetBodyAsText();
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[","").replace("]","");
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String getStatusMessage = JsonPath.read(getCart1, "$.statusMessage").toString().replace("[", "")
                .replace("]", "");
        Assert.assertNotEquals(statusMessage, getStatusMessage, "Restaurant is same");
        //helper.validateDataFromResponseNew(response,restId,menuItem,n);
    }


   @Test(dataProvider = "cartDeletion",groups = { "Regression","Smoke", "Jitender"}, description = "1.Get Tid, token from login 2.Create a cart which contains a")
    public void CreateAndDeleteCheckTotalCart( String mobile, String password,String lat, String lng, String PayloadTime, String message)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String restaurantId=helper.restId(lat, lng);
        String response= helper.CreateCartCheckTotals(n, restaurantId).ResponseValidator.GetBodyAsText();
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[","").replace("]","");
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        //helper.validateDataFromResponseNew(response,restId,menuItem,n);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String getStatusMessage = JsonPath.read(getCart1, "$.statusMessage").toString().replace("[", "")
                .replace("]", "");
        Assert.assertNotEquals(statusMessage, getStatusMessage, "Restaurant is same");
        String deleteCart= helper.DeleteCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String cartMessage= JsonPath.read(deleteCart, "$.statusMessage").toString().replace("[","").replace("]","");
        Assert.assertEquals(cartMessage, message);
        String getCart= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String getStatusMessage1 = JsonPath.read(getCart1, "$.statusMessage").toString().replace("[", "")
                .replace("]", "");
        Assert.assertNotEquals(statusMessage, getStatusMessage1, "Restaurant is same");
        String dataNode= JsonPath.read(getCart, "$.data");
        Assert.assertNull(dataNode);
    }

   @Test(dataProvider = "cartCreationAddon", groups = { "Regression","Smoke", "Jitender"}, description = "1.Get Tid, token from login 2.Create a cart which contains addon 3.verify Cart using get cart api")
    public void CreateCartCheckTotalAddOnCart(String mobile, String password, String lat, String lng, String PayloadTime,String restID) {
	   HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        //List<String> restList= rngHelper.getRestaurantList();
        int n = Integer.valueOf(PayloadTime);
        //helper.getAddonsNew(restID,n);
        HashMap<Integer,HashMap<String,String>> m1=helper.getMenuAddonFromCartOfRest(restID);
        System.out.println(m1);
        HashMap<String, String> mapNum=m1.get(0);
        String response = helper.CreateCartAddonNew(hashMap.get("Tid"), hashMap.get("Token"),mapNum.get("menu_item_id"),"1", mapNum.get("addons_choice_id"),
        		mapNum.get("addons_group_id"), mapNum.get("addons_name"), mapNum.get("addons_price"), mapNum.get("restaurantId")).ResponseValidator.GetBodyAsText();  
        //String response = helper.CreateCartAddonsCheckTotal(n, restID).ResponseValidator.GetBodyAsText();
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[","").replace("]","");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String addOnGroupList = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..group_id").toString().replace("[", "").replace("]", "");
        String choiceIdList = JsonPath.read(response, "$.data..addons..choices..id").toString().replace("[", "").replace("]", "");
        List<String> choiceIDlist1= Arrays.asList(choiceIdList.split(","));
        List<String> addOnlist= Arrays.asList(addOnGroupList.split(","));
        Assert.assertNotNull(addOnGroupList);
        String rest= sandHelper.getRestaurantMenu(restID).ResponseValidator.GetBodyAsText();
        String addOnGroupList1 = JsonPath.read(response, "$.data..addons..group_id").toString().replace("[", "").replace("]", "");
        String addOnChoiceList1 = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..choices..id").toString().replace("[", "").replace("]", "");
        Assert.assertTrue(addOnGroupList1.contains(addOnlist.get(0)));
        Assert.assertTrue(addOnChoiceList1.contains(choiceIDlist1.get(0)));
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        //helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String getStatusMessage = JsonPath.read(getCart1, "$.statusMessage").toString().replace("[", "")
                .replace("]", "");
        Assert.assertEquals(statusMessage, getStatusMessage, "Restaurant is same");
    }

   @Test(dataProvider = "cartCreationDeleteAddon",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login 2.Create a addon cart, 3.verify Cart using get cart api 4. Delete the cart")
    public void CreateAndDeleteCheckTotalAddOnCart(String mobile, String password, String lat, String lng, String PayloadTime, String message) {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        List<String> restList= rngHelper.getRestaurantList();
        int n = Integer.valueOf(PayloadTime);
        //String restId = restaurantId;
        String response = helper.CreateCartAddonsCheckTotal(n,restList.get(0)).ResponseValidator.GetBodyAsText();
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[","").replace("]","");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String addOnGroupList = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..group_id").toString().replace("[", "").replace("]", "");
        String choiceIdList = JsonPath.read(response, "$.data..addons..choices..id").toString().replace("[", "").replace("]", "");
        List<String> choiceIDlist1= Arrays.asList(choiceIdList.split(","));
        List<String> addOnlist= Arrays.asList(addOnGroupList.split(","));
        Assert.assertNotNull(addOnGroupList);
        String rest= sandHelper.getRestaurantMenu(restList.get(0)).ResponseValidator.GetBodyAsText();
        String addOnGroupList1 = JsonPath.read(response, "$.data..addons..group_id").toString().replace("[", "").replace("]", "");
        String addOnChoiceList1 = JsonPath.read(response, "$.data.cart_menu_items..valid_addons..choices..id").toString().replace("[", "").replace("]", "");
        Assert.assertTrue(addOnGroupList1.contains(addOnlist.get(0)));
        Assert.assertTrue(addOnChoiceList1.contains(choiceIDlist1.get(0)));
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        //helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
        String deleteCart= helper.DeleteCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String cartMessage= JsonPath.read(deleteCart, "$.statusMessage").toString().replace("[","").replace("]","");
        String deleteTid=JsonPath.read(response, "$.tid").toString().replace("[","").replace("]","");
        Assert.assertEquals(cartMessage, message);
        String getCartAfterDelete= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String getStatusMessage = JsonPath.read(getCartAfterDelete, "$.statusMessage").toString().replace("[", "")
                .replace("]", "");
        Assert.assertNotEquals(statusMessage, getStatusMessage, "Restaurant is same");
    }


   @Test(dataProvider = "cartCreationWithCoupon",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a cart, 3.verify Cart using get cart api, 4. Apply coupon, 5. Verify using get cart api")
    public void CreateAndApplyCouponOnCheckTotalCart(String mobile, String password, String lat, String lng, String PayloadTime, String couponcode)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String restaurantId=helper.restId(lat,lng);
        String response= helper.cartCheckTotal(n, restaurantId).ResponseValidator.GetBodyAsText();
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[","").replace("]","");
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String applyCouponResponse= helper.ApplyCouponOnCart(hashMap.get("Tid"), hashMap.get("Token"), couponcode).ResponseValidator.GetBodyAsText();
        //helper.validateDataFromResponseNew(applyCouponResponse,restId,menuItem,n);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String getStatusMessage = JsonPath.read(getCart1, "$.statusMessage").toString().replace("[", "")
                .replace("]", "");
        Assert.assertNotEquals(statusMessage, getStatusMessage, "Restaurant is same");
        //helper.validateDataFromResponseNew(getCart1,restId,menuItem,n);
    }

   @Test(dataProvider = "cartCreationDeleteAddon",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a cart, 3.verify Cart using get cart api, 4. Apply coupon, 5. Verify using get cart api")
    public void CheckTotalCartwithNoAddress(String mobile, String password, String lat, String lng, String PayloadTime, String couponcode)
    {
        HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        int n = Integer.valueOf(PayloadTime);
        String restaurantId=helper.restId(lat,lng);
        String response= helper.cartCheckTotal(n, restaurantId).ResponseValidator.GetBodyAsText();
        String restId=JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");
        String addressNode= JsonPath.read(response, "$.data..addresses").toString().replace("[","").replace("]","");
        System.out.println("--->"+addressNode);
        Assert.assertEquals("",addressNode,"address Node is present");
    }

   @Test(dataProvider = "TDForNormalCart",groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a addon cart, 3.verify Cart using get cart api, 4. Verify TD disocunt 5.Verify using get acart api")
    public void TradeDiscountPercentageOnCheckTotalCart(String TDPayload,CreateMenuEntry cartpayload) throws IOException
    {
        String tdResponse=rngHelper.createTD(TDPayload).ResponseValidator.GetBodyAsText();
        String cartPayload= jsonHelper.getObjectToJSON(cartpayload.getCartItems());
        HashMap<String, String> hashMap=helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
        String response= helper.CheckTotalCreate(cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
        String statusMessage= JsonPath.read(response, "$.statusMessage").toString().replace("[","").replace("]","");
        String CampaignHeader= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders..header").toString().replace("[", "").replace("]", "");
        String Trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString().replace("[", "").replace("]", "");
        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
        String[] menuItems= menuItem.split(",");
        int noOfProducts = menuItems.length;
        //helper.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
        Assert.assertNotNull(CampaignHeader);
        Assert.assertNotNull(Trade_SubTotal);
        String getCart1= helper.GetCart(hashMap.get("Tid"), hashMap.get("Token")).ResponseValidator.GetBodyAsText();
        String getStatusMessage = JsonPath.read(getCart1, "$.statusMessage").toString().replace("[", "").replace("]", "");
                
        Assert.assertNotEquals(statusMessage, getStatusMessage, "Restaurant is");
        //helper.validateDataFromResponseNew(getCart1,restId,menuItem,noOfProducts);
    }
   
   @Test(dataProvider = "updateCafeCartData",groups = { "Regression", "Smoke"}, description = "1.Creating cafe cart and Placing cafe order" )
   public void createCafeOrder(String restaurantId, boolean isAddonRequired, boolean isCartItemRequired,String payment,String comments,int statusCode, String statusMessage) {
	  // Processor cartProcessor = helper.updateCart("229", false, true);
	  
	     HashMap<String, String> obj=helper.createLogin(CheckoutConstants.password1, Long.parseLong(CheckoutConstants.mobile1));
	     Processor cartProcessor = helper.updateCafeCart(obj.get("Tid"),obj.get("Token"),restaurantId, isAddonRequired, isCartItemRequired);
	        CartV2Response cartV2Response = Utility.jsonDecode(cartProcessor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
	        cartValidator.smokeCheck(statusCode, statusMessage,cartProcessor);
	        order = helper.cafeOrder(obj.get("Tid"),obj.get("Token"), payment, comments).ResponseValidator.GetBodyAsText();
	        order_id = JsonPath.read(order, "$.data.order_id").toString().replace("[", "").replace("]", "");
			System.out.println(order_id);
	        Assert.assertNotNull(order_id);
   }
   
   public String getAddressIDfromCartResponse(String tid,String token,String cartResponse){
	   	String addressId="";
	   	System.out.println(cartResponse);
	   	String[] validAddressList=JsonPath.read(cartResponse, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]", "").split(",");
	      for(int i=0;i<validAddressList.length;i++){
	   	   if(validAddressList[i].equals("1")){
	   		   addressId=JsonPath.read(cartResponse, "$.data.addresses.["+i+"]..id").toString().replace("[", "").replace("]", "").replace("\"","");
	   		   return addressId;
	   	   }
	   	   }
	   	       String lat= JsonPath.read(cartResponse, "$.data.restaurant_details.lat").toString().replace("[", "").replace("]","").replace("\"","");
	   		   String lng= JsonPath.read(cartResponse, "$.data.restaurant_details.lng").toString().replace("[", "").replace("]","").replace("\"","");
	   		   String mobileNum= JsonPath.read(cartResponse, "$.data.phone_no").toString().replace("[", "").replace("]","").replace("\"","");
	   		   String addResponse= addressHelper.NewAddress(tid,token,"TestName",mobileNum,"TestAddress","TestLandMark","TestArea" ,lat, lng, "","","Home").ResponseValidator.GetBodyAsText();
	   	       addressId= JsonPath.read(addResponse, "$.data..address_id").toString().replace("[", "").replace("]","");
	   	       System.out.println("New created Serviceable Address ID -: "+addressId);
			return addressId;
	   	
	   }
}