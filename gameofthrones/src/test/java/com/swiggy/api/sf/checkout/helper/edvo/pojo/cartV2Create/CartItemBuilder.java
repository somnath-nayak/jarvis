package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import java.util.List;

public class CartItemBuilder {
    private CartItem cartItem;

    public CartItemBuilder(){
        cartItem = new CartItem();
    }

    public CartItemBuilder addons(List<Addon> addons){
        cartItem.setAddons(addons);
        return this;
    }

    public CartItemBuilder variants(List<Variant> variants){
        cartItem.setVariants(variants);
        return this;
    }

    public CartItemBuilder menuItemId(String menuItemId){
        cartItem.setMenuItemId(menuItemId);
        return this;
    }

    public CartItemBuilder quantity(Integer quantity){
        cartItem.setQuantity(quantity);
        return this;
    }

    public CartItem build(){
        return cartItem;
    }
}
