package com.swiggy.api.sf.rng.pojo.ads.Segmentation;

import org.codehaus.jackson.annotate.JsonProperty;

public class OrderCancelMessage {
   
   @JsonProperty("order_id")
    private String orderId;
    
    @JsonProperty("status")
    private String status;

    @JsonProperty("payment_method")
    private String paymentMethod;

    @JsonProperty("customer_id")
    private Long customerId;

    @JsonProperty("order_type")
    private String orderType;
    
    @JsonProperty("shared_order")
    private boolean sharedOrder;
    
    public String getOrderId() {
        return orderId;
    }
    
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getPaymentMethod() {
        return paymentMethod;
    }
    
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    public Long getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
    
    public String getOrderType() {
        return orderType;
    }
    
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
    
    public boolean isSharedOrder() {
        return sharedOrder;
    }
    
    public void setSharedOrder(boolean sharedOrder) {
        this.sharedOrder = sharedOrder;
    }
    
  
}
