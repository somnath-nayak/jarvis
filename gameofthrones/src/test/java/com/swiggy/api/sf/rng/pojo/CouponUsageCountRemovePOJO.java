package com.swiggy.api.sf.rng.pojo;

import com.swiggy.api.sf.rng.helper.RngHelper;
import org.codehaus.jackson.annotate.JsonProperty;

public class CouponUsageCountRemovePOJO {

    @JsonProperty("code")
    private String code;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("orderId")
    private String orderId;

    public String getCode() {
        return code;
    }

    public String getUserId() {
        return userId;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public CouponUsageCountRemovePOJO withCode(String code) {
        this.code = code;
        return this;
    }
    public CouponUsageCountRemovePOJO withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public CouponUsageCountRemovePOJO withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }


    public CouponUsageCountRemovePOJO setDefaultData(String code,String userId,String orderId) {

        return this.withCode(code)
                .withUserId(userId)
                .withOrderId(orderId);
    }

}
