package com.swiggy.api.sf.checkout.dp;

import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.EditTimeConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.tests.EditOrderV2;
import org.testng.annotations.DataProvider;

import java.util.HashMap;


public class EditOrderDP {
    HashMap<String, String> headers;
    CheckoutHelper cartupdatehelp = new CheckoutHelper();
    String tid;
    String token;
    EditOrderV2 e = new EditOrderV2();

    public  EditOrderDP() {
        String mobile= System.getenv("mobile");
        String password= System.getenv("password");

            if (mobile == null || mobile.isEmpty()){
                mobile=CheckoutConstants.mobile;
            }

            if (password == null ||password.isEmpty()){
                password=CheckoutConstants.password;
            }

            HashMap<String, String> hashMap=cartupdatehelp.getTokenDataCheckout(mobile,password);
            tid=hashMap.get("Tid");
            token=hashMap.get("Token");
        }


    @DataProvider(name = "OrderEditAndConfirm")
    public Object[][] OrderEditAndConfirm() throws Exception {

        String itemId="";
        String Menu_Item_Id = cartupdatehelp.getMenu();
        Long Order_Id = cartupdatehelp.order_id(tid,token);

        try{
            itemId=cartupdatehelp.getMenu();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return new Object[][]{

                {tid,token,Integer.parseInt(EditTimeConstants.REST_ID),EditTimeConstants.Quantity,Menu_Item_Id,Order_Id,1}

        };
    }


}
