package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "date",
        "slot"
})
public class PreorderSlot {

    @JsonProperty("date")
    private Long date;
    @JsonProperty("slot")
    private Slot slot;

    /**
     * No args constructor for use in serialization
     *
     */
    public PreorderSlot() {
    }

    /**
     *
     * @param slot
     * @param date
     */
    public PreorderSlot(Long date, Slot slot) {
        super();
        this.date = date;
        this.slot = slot;
    }

    @JsonProperty("date")
    public Long getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Long date) {
        this.date = date;
    }

    @JsonProperty("slot")
    public Slot getSlot() {
        return slot;
    }

    @JsonProperty("slot")
    public void setSlot(Slot slot) {
        this.slot = slot;
    }

}