package com.swiggy.api.sf.rng.pojo.SwiggySuper;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.SwiggySuper
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "plan_id",
        "benefit_id",
        "created_by"
})
public class PlanBenefitMapping {
    @JsonProperty("plan_id")
    private Integer plan_id;
    @JsonProperty("benefit_id")
    private Integer benefit_id;
    @JsonProperty("created_by")
    private String created_by;

    @JsonProperty("plan_id")
    public Integer getPlan_id() {
        return plan_id;
    }

    @JsonProperty("plan_id")
    public void setPlan_id(Integer plan_id) {
        this.plan_id = plan_id;
    }

    @JsonProperty("benefit_id")
    public Integer getBenefit_id() {
        return benefit_id;
    }

    @JsonProperty("benefit_id")
    public void setBenefit_id(Integer benefit_id) {
        this.benefit_id = benefit_id;
    }

    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public void setDefaultValues(int planID, int benefitID) {
        if (this.getPlan_id() == null)
            this.setPlan_id(planID);
        if (this.getBenefit_id() == null)
            this.setBenefit_id(benefitID);
        if (this.getCreated_by() == null)
            this.setCreated_by("Test_Automation@swiggy.in");
    }

    public PlanBenefitMapping build (int planID, int  benefitID) {
        setDefaultValues(planID,benefitID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("plan_id", plan_id).append("benefit_id", benefit_id).append("created_by", created_by).toString();
    }
}
