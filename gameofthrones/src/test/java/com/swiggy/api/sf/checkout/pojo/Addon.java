package com.swiggy.api.sf.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Addon {

    private String choice_id;
    private String group_id;
    private String name;
    private Integer price;

    /**
     * No args constructor for use in serialization
     *
     */
    public Addon() {
    }

    /**
     *
     * @param group_id
     * @param price
     * @param choice_id
     * @param name
     */
    public Addon(String choice_id, String group_id, String name, Integer price) {
        super();
        this.choice_id = choice_id;
        this.group_id = group_id;
        this.name = name;
        this.price = price;
    }

    public String getchoice_id() {
        return choice_id;
    }

    public void setchoice_id(String choice_id) {
        this.choice_id = choice_id;
    }

    public String getgroup_id() {
        return group_id;
    }

    public void setgroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("choice_id", choice_id).append("group_id", group_id).append("name", name).append("price", price).toString();
    }

}