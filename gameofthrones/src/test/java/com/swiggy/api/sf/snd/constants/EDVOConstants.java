package com.swiggy.api.sf.snd.constants;

public interface EDVOConstants {

    String rngDB = "rng";
    String cmsDB = "cms";
    String lat="12.932731";
    String lng="77.603634";

    String mockAPI ="api/v4/restaurants";
    int mockstatusCode = 200;
    String mockcontentType = "application/json";


    String deleteEDVOMeal1= "delete from meals.group_item where group_id in (select id from meals.group_definition where meal_id in (select id from meals.meal_definition where rest_id=";
    String deleteEDVOMeal2= "delete from meals.group_definition where meal_id in (select id from meals.meal_definition where rest_id=";
    String deleteEDVOMeal3= "delete from meals.exit_page where meal_id in (select id from meals.meal_definition where rest_id=";
    String deleteEDVOMeal4= "delete from meals.launch_page where meal_id in (select id from meals.meal_definition where rest_id=";
    String deleteEDVOMeal5= "delete from meals.screen_entity where meal_id in (select id from meals.meal_definition where rest_id=";
    String deleteEDVOMeal6 = "delete from meals.meal_definition where rest_id=";
    String deleteEDVOMeal7= "))";
    String deleteEDVOMeal8= ")";

    String getGroup1 = "select id from meals.group_definition where meal_id =";
    String getItem = "select item_id from meals.group_item where group_id =";
    String getMinQuantity = "select min_total from group_definition where id =";
    String getMaxQuantity = "select max_total from group_definition where id =";

    String updateMaxTotal = "Update group_definition set max_total = ";
    String updateMinTotal = "Update group_definition set min_total = ";
    String updateMaxChoice = "Update group_definition set max_choice = ";
    String updateMinChoice = "Update group_definition set min_choice = ";


    String negativeMealData1="select id from meals.meal_definition order by rest_id desc limit 1" ;

    String getEDVOText="select operation_meta from trade_discount.campaign_restaurant_map where campaign_id=";
    String EDVOSubText = "Select sub_text from meals.meal_definition where id= ";


    String updateEDVOImage = "Update meals.meal_definition set image_id=";
    String updateEDVOText = "Update meals.meal_definition set text=";
    String updateEDVOSubText = "Update meals.meal_definition set sub_text=";
    String updateEDVOmeal = " where id = ";


}
