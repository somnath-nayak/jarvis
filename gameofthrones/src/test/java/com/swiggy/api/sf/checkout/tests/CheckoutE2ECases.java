package com.swiggy.api.sf.checkout.tests;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.IStatusConstants;
import com.swiggy.api.sf.checkout.dp.CheckoutDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;

import framework.gameofthrones.JonSnow.Processor;

public class CheckoutE2ECases extends CheckoutDP {
	String tid = null;
	String token = null;

	@BeforeClass
	public void setUp() {
		Processor p = SnDHelper.consumerLogin(CheckoutConstants.mobile, CheckoutConstants.password);
		tid = p.ResponseValidator.GetNodeValue("tid");
		token = p.ResponseValidator.GetNodeValue("data.token");
	}


	@Test(description = "VerifyGetAllPaymentOptionsForAgivenUser")
	public void verifySODEOXOptionIngetAllPaymentsForaGivenUser() throws Exception {
		CheckoutHelper c = new CheckoutHelper();
		SoftAssert s = new SoftAssert();
		Processor pp = c.getAllPayments(tid, token);
		String resp = pp.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		Assert.assertEquals(statusCode, IStatusConstants.STATUTSCODE_SUCCESS);
		Assert.assertEquals(JsonPath.read(resp, "$.statusMessage"), IStatusConstants.STATUSMESSAGE_SUCCESS);
		List<Map<String, Object>> sodexometaList = CheckoutHelper.getPaymentMeta("Sodexo");
		Map<String, Object> sodexoMetaInfo = sodexometaList.get(0);
		JsonNode object = CheckoutHelper.convertStringtoJSON(
				JsonPath.read(resp, "$.data.payment_group[*].payment_methods[?(@.name==\"Sodexo\")]").toString());
		if (sodexoMetaInfo.get("is_deleted").equals(0)) {
			s.assertEquals(object.get(0).get("name").asText(), sodexoMetaInfo.get("name"), "paymentMethodName");
			s.assertEquals(object.get(0).get("display_name").asText(), sodexoMetaInfo.get("display_name"),
					"paymentMethodDisplayName");
			s.assertEquals(object.get(0).get("payment_code").asText(), sodexoMetaInfo.get("payment_code"),
					"paymentMethodPaymentCode");
			s.assertEquals(object.get(0).get("priority").asInt(), sodexoMetaInfo.get("priority"),
					"paymentMethodPriority");
			s.assertEquals(object.get(0).get("enabled").asInt(), sodexoMetaInfo.get("enabled"),
					"paymentMethodIsEnabled");
			JsonNode metdata = CheckoutHelper.convertStringtoJSON(sodexoMetaInfo.get("meta").toString());
			s.assertEquals(object.get(0).get("meta").get("promo_msg"), metdata.get("promo_msg"),
					"paymentMethod promo_msg");
			s.assertEquals(object.get(0).get("meta").get("icon_url").asText(), metdata.get("icon_url").asText(),
					"paymentMethod icon_url");
			s.assertEquals(object.get(0).get("meta").get("disable_message").asText(),
					metdata.get("disable_message").asText(), "paymentMethod disable_message");
			s.assertAll();
		} else if (sodexoMetaInfo.get("is_deleted").equals(0)) {
			Assert.assertTrue(object.size() == 0);
		}
	}

	@Test(dataProvider = "cartTestSet1",groups = {"Regression"}, description = "Create cart with one item->Get cart->Edit Cart with multiple item>Get Cart")
	public void cartTest1(String lat, String lng) {
		CheckoutHelper c = new CheckoutHelper();
		String restId = c.restId(lat, lng);
		int n = 1;
		String cartResponse = c.CreateCart(n, tid, token, restId).ResponseValidator.GetBodyAsText();
		String menuItem = JsonPath.read(cartResponse, "$.data.cart_menu_items..menu_item_id").toString()
				.replace("[", "").replace("]", "");
		c.validateDataFromResponse(cartResponse, restId, menuItem, n);
		String getCartResp = c.GetCart(tid, token).ResponseValidator.GetBodyAsText();
		c.validateDataFromResponseNew(getCartResp, restId, menuItem, n);
		n = 4;
		cartResponse = c.CreateCart(n, tid, token, restId).ResponseValidator.GetBodyAsText();
		menuItem = JsonPath.read(cartResponse, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "")
				.replace("]", "");
		c.validateDataFromResponse(cartResponse, restId, menuItem, n);
		getCartResp = c.GetCart(tid, token).ResponseValidator.GetBodyAsText();
		c.validateDataFromResponseNew(getCartResp, restId, menuItem, n);
	}

	@Test(dataProvider = "cartTestSet1",groups = {"Regression"}, description = "Create cart->Delete Cart->Get Cart")
	public void cartTest2(String lat, String lng) {
		SoftAssert s = new SoftAssert();
		CheckoutHelper c = new CheckoutHelper();
		String restId = c.restId(lat, lng);
		int n = 1;
		String cartResponse = c.CreateCart(n, tid, token, restId).ResponseValidator.GetBodyAsText();
		String menuItem = JsonPath.read(cartResponse, "$.data.cart_menu_items..menu_item_id").toString()
				.replace("[", "").replace("]", "");
		c.validateDataFromResponse(cartResponse, restId, menuItem, n);
		String cc = c.DeleteCart(tid, token).ResponseValidator.GetBodyAsText();
		s.assertEquals(Integer.parseInt(JsonPath.read(cc, "$.statusCode")), IStatusConstants.STATUTSCODE_SUCCESS);
		s.assertEquals(JsonPath.read(cc, "$.statusMessage"), IStatusConstants.CART_DELETION_MESSAGE);
		s.assertAll();
		String getCartResp = c.GetCart(tid, token).ResponseValidator.GetBodyAsText();
		c.validateDataFromResponseNew(getCartResp, restId, menuItem, n);
	}

	@Test(dataProvider = "cartTestSet1",groups = {"Regression"}, description = "Create cart->checkTotals->DeleteCart-->checkTotals")
	public void cartTest3(String lat, String lng) {
		SoftAssert s = new SoftAssert();
		CheckoutHelper c = new CheckoutHelper();
		String restId = c.restId(lat, lng);
		int n = 1;
		String cartResponse = c.CreateCart(n, tid, token, restId).ResponseValidator.GetBodyAsText();
		String menuItem = JsonPath.read(cartResponse, "$.data.cart_menu_items..menu_item_id").toString()
				.replace("[", "").replace("]", "");
		c.validateDataFromResponse(cartResponse, restId, menuItem, n);
		String cc = c.cartCheckTotal(n, restId).ResponseValidator.GetBodyAsText();
		c.validateDataFromResponse(cc, restId, menuItem, n);
		cc = c.DeleteCart(tid, token).ResponseValidator.GetBodyAsText();
		s.assertEquals(Integer.parseInt(JsonPath.read(cc, "$.statusCode")), IStatusConstants.STATUTSCODE_SUCCESS);
		s.assertEquals(JsonPath.read(cc, "$.statusMessage"), IStatusConstants.CART_DELETION_MESSAGE);
		s.assertAll();
		cc = c.cartCheckTotal(n, restId).ResponseValidator.GetBodyAsText();
		c.validateDataFromResponse(cc, restId, menuItem, n);
	}

	@Test(dataProvider = "cartTestSet4",groups = {"Regression"}, description = "Create cart with coupon->Get cart->Remove coupon-> get cart")
	public void cartTest4(String mobile, String password, String lat, String lng, String PayloadTime,
			String couponcode) {
		CheckoutHelper c = new CheckoutHelper();
		String restId = c.restId(lat, lng);
		int n = Integer.valueOf(PayloadTime);
		String cartResponse = c.CreateCart(n, tid, token, restId).ResponseValidator.GetBodyAsText();
		String menuItem = JsonPath.read(cartResponse, "$.data.cart_menu_items..menu_item_id").toString()
				.replace("[", "").replace("]", "");
		String applyCouponResponse = c.ApplyCouponOnCart(tid, token, couponcode).ResponseValidator.GetBodyAsText();
		c.validateDataFromResponseNew(applyCouponResponse, restId, menuItem, n);
		String cc = c.GetCart(tid, token).ResponseValidator.GetBodyAsText();
		c.validateDataFromResponse(cc, restId, menuItem, n);
		String removeCouponReponse = c.DeleteCouponOnCart(tid, token, couponcode).ResponseValidator.GetBodyAsText();
		c.validateDataFromResponseNew(removeCouponReponse, restId, menuItem, n);
		String ccc = c.GetCart(tid, token).ResponseValidator.GetBodyAsText();
		c.validateDataFromResponse(ccc, restId, menuItem, n);
	}

}
