package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "category",
        "sub_category"
})
public class CategoryDetails {

    @JsonProperty("category")
    private String category;
    @JsonProperty("sub_category")
    private String subCategory;

    /**
     * No args constructor for use in serialization
     *
     */
    public CategoryDetails() {
    }

    /**
     *
     * @param category
     * @param subCategory
     */
    public CategoryDetails(String category, String subCategory) {
        super();
        this.category = category;
        this.subCategory = subCategory;
    }

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("sub_category")
    public String getSubCategory() {
        return subCategory;
    }

    @JsonProperty("sub_category")
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("category", category).append("subCategory", subCategory).toString();
    }

}