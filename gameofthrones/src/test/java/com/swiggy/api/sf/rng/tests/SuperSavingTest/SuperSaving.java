package com.swiggy.api.sf.rng.tests.SuperSavingTest;


import com.rabbitmq.client.AMQP;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.dp.SuperSaving.SuperSavingDP;
import com.swiggy.api.sf.rng.helper.RmqHelper;
import com.swiggy.api.sf.rng.helper.SuperDbHelper;
import com.swiggy.api.sf.rng.helper.SuperHelper;
import com.swiggy.api.sf.rng.helper.SuperSavingHelper;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings.CreateOrder;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings.TradeDiscountBreakup;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.KafkaHelper;
import org.mortbay.util.ajax.JSON;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

public class SuperSaving {

    RmqHelper rmqHelper = new RmqHelper(RngConstants.RMQ_USERNAME,RngConstants.RMQ_PASSWORD);
    KafkaHelper kafkaHelper = new KafkaHelper();
    SuperSavingHelper superSavingHelper = new SuperSavingHelper();
    SoftAssert softAssert = new SoftAssert();
    SuperHelper superHelper = new SuperHelper();
    JsonHelper jsonHelper = new JsonHelper();

    @Test(dataProvider = "publicUserSubscriptionFreeDelData", description = "Create one Order using Free-Del/FreeBie, verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void nonIncentiveFreeDeliverOrFreebieForSuperUser(String planPayload, String numOfPlans,
                                                             String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails) throws Exception {

        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getNonIncentiveSubscription(planPayload, numOfPlans, benefitPayload);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));

        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        byte[] byte_arr = str.getBytes(StandardCharsets.UTF_8);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");
        softAssert.assertAll();

        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }


    @Test(dataProvider = "publicUserSubscriptionFreeDelData", description = "Create more than 1 Order using Free-Del/FreeBie, verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void nonIncentiveMultipleFreeDeliverOrFreeBieForSuperUser(String planPayload, String numOfPlans,
                                                                     String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails) throws Exception {
        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getNonIncentiveSubscription(planPayload, numOfPlans, benefitPayload);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));


        /**
         Create Order No.1 and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);

        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        System.out.println("orderid 1" + orderid);
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);


        //API call for super saving benefits
        Thread.sleep(1000);// explicit sleep for events, redis & DB sync
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());
        String total_unlocked = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].total_unlocked"));

        //Assertion after 1st order
        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()));
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()));
        softAssert.assertTrue(api_response_totaloffersavings.contains("1"));
        softAssert.assertTrue(total_unlocked.contains("1"));

        /**
         Create Order No.2and push to RMQ
         **/
        CreateOrder createOrderPayload2 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str2 = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str2);
        String orderid2 = String.valueOf(createOrderPayload2.getOrderId());
        System.out.println("orderid2" + orderid2);
        String order_delivered_payload2 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid2));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload2);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse2 = superSavingHelper.getPlansByUserId(publicUserId);

        String api_response_reward_type2 = JSON.toString(superSavingsBenefitsResponse2.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount2 = JSON.toString(superSavingsBenefitsResponse2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String total_unlocked2 = JSON.toString(superSavingsBenefitsResponse2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].total_unlocked"));
        String api_response_totaloffersavings2 = superSavingsBenefitsResponse2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");

        Double expected_discount_amount2 = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());
        Double expected_total_discount = expected_discount_amount + expected_discount_amount2;

        //Assertion after 2nd order
        softAssert.assertTrue(api_response_reward_type2.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()));
        softAssert.assertTrue(api_response_discount_amount2.contains(expected_total_discount.toString()));
        softAssert.assertTrue(total_unlocked2.contains("2"));
        softAssert.assertTrue(api_response_totaloffersavings2.contains("2"));

        softAssert.assertAll();
        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }


    @Test(dataProvider = "mappedUserSubsCriptionWithIncentivePercentageData", description = "Create one Order using Free-Del/FreeBie & Percentage , verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void freeDelFreebieAndPercentageTest(String planPayload, String numOfPlans, String benefitPayload,
                                                HashMap<String, String> createIncentiveData, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails) throws Exception {

        HashMap<String, String> getIds = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload, createIncentiveData);
        String publicUserId = getIds.get("publicUserId");
        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        byte[] byte_arr = str.getBytes(StandardCharsets.UTF_8);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE mismatched....");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT_AMOUNT mismatched.....");
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()), "TOTAL_OFFER_SAVINGS mismatched ....");

        //Assertion for Percentage offer
        String api_response_reward_type1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[1].reward_type"));
        String api_response_discount_amount1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[1].discount"));
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Double expected_discount_amount1 = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(1).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type1.contains(createOrderPayload.getTradeDiscountBreakup().get(1).getRewardType()), "REWARD_TYPE mismatched....");
        softAssert.assertTrue(api_response_discount_amount1.contains(expected_discount_amount1.toString()), "DISCOUNT_AMOUNT mismatched.....");
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()), "TOTAL_OFFER_SAVINGS mismatched ....");

        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_super_savings)), "TOTAL SUPER SAVINGS mismatch....");
        softAssert.assertAll();

        SuperDbHelper.deleteFreebieBenefit();
    }


    @Test(dataProvider = "mappedUserSubsCriptionWithIncentiveFlatData", description = "Create one Order using Free-Del/FreeBie & FLAT , verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void freeDelFreebieAndFlatTest(String planPayload, String numOfPlans, String benefitPayload,
                                          HashMap<String, String> createIncentiveData, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails) throws Exception {


        HashMap<String, String> getIds = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload, createIncentiveData);
        String publicUserId = getIds.get("publicUserId");
        String benefitId = getIds.get("benefitId");

        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        byte[] byte_arr = str.getBytes(StandardCharsets.UTF_8);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE mismatched....");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT_AMOUNT mismatched.....");
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()), "TOTAL_OFFER_SAVINGS mismatched ....");

        //Assertion for Percentage offer
        String api_response_reward_type1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[1].reward_type"));
        String api_response_discount_amount1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[1].discount"));
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Double expected_discount_amount1 = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(1).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type1.contains(createOrderPayload.getTradeDiscountBreakup().get(1).getRewardType()), "REWARD_TYPE mismatched....");
        softAssert.assertTrue(api_response_discount_amount1.contains(expected_discount_amount1.toString()), "DISCOUNT_AMOUNT mismatched.....");
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()), "TOTAL_OFFER_SAVINGS mismatched ....");

        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_super_savings)), "TOTAL SUPER SAVINGS mismatch....");
        softAssert.assertAll();
        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();

    }


    @Test(dataProvider = "mappedUserSubscriptionWithIncentivePercentageMultipleOrdersData", description = "Create more than 1 Orders using Free-Del & Percentage , verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void freeDelAndPercentageMultipleOrdersTest(String planPayload, String numOfPlans, String benefitPayload,
                                                       HashMap<String, String> createIncentiveData, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails) throws Exception {
        HashMap<String, String> getIds = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload, createIncentiveData);
        String publicUserId = getIds.get("publicUserId");
        String benefitId = getIds.get("benefitId");


        /**
         Create Order No.1 and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);

        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        System.out.println("orderid 1" + orderid);
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        int reward_type_index = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(0).getRewardType());
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index + "].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index + "].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());
        int expected_total_offer_savings = tdBuilderList.size();
        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()));
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()));
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(expected_total_offer_savings));

        //Assertion for Percentage offer
        int reward_type_index_percentage = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(1).getRewardType());
        String api_response_reward_type1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index_percentage + "].reward_type"));
        String api_response_discount_amount1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index_percentage + "].discount"));
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Double expected_discount_amount1 = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(1).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type1.toLowerCase().contains(createOrderPayload.getTradeDiscountBreakup().get(1).getRewardType().toLowerCase()));
        softAssert.assertTrue(api_response_discount_amount1.contains(expected_discount_amount1.toString()));
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()));

        int expected_total_saving = (int) (expected_discount_amount + expected_discount_amount1);
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_offer_savings)));

        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }

        /**
         Create Order No.2and push to RMQ
         **/
        CreateOrder createOrderPayload2 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str2 = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str2);
        String orderid2 = String.valueOf(createOrderPayload2.getOrderId());
        System.out.println("orderid2" + orderid2);
        String order_delivered_payload2 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid2));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload2);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse1 = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        int reward_type_index2 = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse1, tdBuilderList.get(0).getRewardType());
        String api_response_reward_type2 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index2 + "].reward_type"));
        String api_response_discount_amount2 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index2 + "].discount"));
        String api_response_totaloffersavings2 = superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount2 = Double.valueOf(createOrderPayload2.getTradeDiscountBreakup().get(0).getDiscountAmount());
        softAssert.assertTrue(api_response_reward_type2.toLowerCase().contains(createOrderPayload2.getTradeDiscountBreakup().get(0).getRewardType().toLowerCase()));

        double total_expected_discount_amount_freedel = expected_discount_amount2 + expected_discount_amount; //sum of previous order discount and current order discount
        softAssert.assertTrue(api_response_discount_amount2.contains(String.valueOf(total_expected_discount_amount_freedel)));
        expected_total_offer_savings = expected_total_offer_savings + tdBuilderList.size();
        softAssert.assertEquals(api_response_totaloffersavings2, String.valueOf(expected_total_offer_savings));

        //Assertion for Percentage offer
        int reward_type_index_percentage2 = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse1, tdBuilderList.get(1).getRewardType());
        String api_response_reward_type3 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index_percentage2 + "].reward_type"));
        String api_response_discount_amount3 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index_percentage2 + "].discount"));
        Double expected_discount_amount3 = expected_discount_amount1 + Double.valueOf(createOrderPayload2.getTradeDiscountBreakup().get(1).getDiscountAmount()); //sum of previous order discount and current order discount

        softAssert.assertTrue(api_response_reward_type3.contains(createOrderPayload2.getTradeDiscountBreakup().get(1).getRewardType()));
        softAssert.assertTrue(api_response_discount_amount3.contains(expected_discount_amount3.toString()));
        softAssert.assertEquals(api_response_totaloffersavings2, String.valueOf(expected_total_offer_savings));
        String api_response_total_super_savings1 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));

        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings1.contains(String.valueOf(expected_total_super_savings)));
        softAssert.assertAll();
        SuperDbHelper.deleteBenefitFromDB(benefitId);
    }


    @Test(dataProvider = "mappedUserSubscriptionWithIncentiveFlatMultipleOrdersData", description = "Create more than 1 Orders using Free-Del & Percentage , verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void freeDelAndFlatMultipleOrdersTest(String planPayload, String numOfPlans, String benefitPayload,
                                                 HashMap<String, String> createIncentiveData, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails) throws Exception {
        HashMap<String, String> getIds = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload, createIncentiveData);
        String publicUserId = getIds.get("publicUserId");
        String benefitId = getIds.get("benefitId");


        /**
         Create Order No.1 and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);

        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        System.out.println("orderid 1" + orderid);
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        int reward_type_index = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(0).getRewardType());
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index + "].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index + "].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());
        int expected_total_offer_savings = tdBuilderList.size();
        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()));
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()));
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(expected_total_offer_savings));

        //Assertion for Percentage offer
        int reward_type_index_percentage = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(1).getRewardType());
        String api_response_reward_type1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index_percentage + "].reward_type"));
        String api_response_discount_amount1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index_percentage + "].discount"));
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Double expected_discount_amount1 = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(1).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type1.toLowerCase().contains(createOrderPayload.getTradeDiscountBreakup().get(1).getRewardType().toLowerCase()));
        softAssert.assertTrue(api_response_discount_amount1.contains(expected_discount_amount1.toString()));
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()));

        int expected_total_saving = (int) (expected_discount_amount + expected_discount_amount1);
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_offer_savings)));

        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }

        /**
         Create Order No.2and push to RMQ
         **/
        CreateOrder createOrderPayload2 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str2 = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str2);
        String orderid2 = String.valueOf(createOrderPayload2.getOrderId());
        System.out.println("orderid2" + orderid2);
        String order_delivered_payload2 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid2));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload2);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse1 = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        int reward_type_index2 = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse1, tdBuilderList.get(0).getRewardType());
        String api_response_reward_type2 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index2 + "].reward_type"));
        String api_response_discount_amount2 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index2 + "].discount"));
        String api_response_totaloffersavings2 = superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount2 = Double.valueOf(createOrderPayload2.getTradeDiscountBreakup().get(0).getDiscountAmount());
        softAssert.assertTrue(api_response_reward_type2.toLowerCase().contains(createOrderPayload2.getTradeDiscountBreakup().get(0).getRewardType().toLowerCase()));

        double total_expected_discount_amount_freedel = expected_discount_amount2 + expected_discount_amount; //sum of previous order discount and current order discount
        softAssert.assertTrue(api_response_discount_amount2.contains(String.valueOf(total_expected_discount_amount_freedel)));
        expected_total_offer_savings = expected_total_offer_savings + tdBuilderList.size();
        softAssert.assertEquals(api_response_totaloffersavings2, String.valueOf(expected_total_offer_savings));

        //Assertion for Percentage offer
        int reward_type_index_percentage2 = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse1, tdBuilderList.get(1).getRewardType());
        String api_response_reward_type3 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index_percentage2 + "].reward_type"));
        String api_response_discount_amount3 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index_percentage2 + "].discount"));
        Double expected_discount_amount3 = expected_discount_amount1 + Double.valueOf(createOrderPayload2.getTradeDiscountBreakup().get(1).getDiscountAmount()); //sum of previous order discount and current order discount

        softAssert.assertTrue(api_response_reward_type3.contains(createOrderPayload2.getTradeDiscountBreakup().get(1).getRewardType()));
        softAssert.assertTrue(api_response_discount_amount3.contains(expected_discount_amount3.toString()));
        softAssert.assertEquals(api_response_totaloffersavings2, String.valueOf(expected_total_offer_savings));
        String api_response_total_super_savings1 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));

        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings1.contains(String.valueOf(expected_total_super_savings)));
        softAssert.assertAll();
        SuperDbHelper.deleteBenefitFromDB(benefitId);
    }


    @Test(dataProvider = "mappedUserSubscriptionFreeBieWithIncentivePercentageMultipleOrdersData", description = "Create more than 1 Orders using FreeBie & Percentage , verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void freeBieAndPercentageMultipleOrdersTest(String planPayload, String numOfPlans, String benefitPayload,
                                                       HashMap<String, String> createIncentiveData, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails) throws Exception {
        HashMap<String, String> getIds = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload, createIncentiveData);
        String publicUserId = getIds.get("publicUserId");
        String benefitId = getIds.get("benefitId");


        /**
         Create Order No.1 and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);

        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        System.out.println("orderid 1" + orderid);
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        int reward_type_index = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(0).getRewardType());
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index + "].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index + "].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());
        int expected_total_offer_savings = tdBuilderList.size();
        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()));
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()));
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(expected_total_offer_savings));

        //Assertion for Percentage offer
        int reward_type_index_percentage = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(1).getRewardType());
        String api_response_reward_type1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index_percentage + "].reward_type"));
        String api_response_discount_amount1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index_percentage + "].discount"));
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Double expected_discount_amount1 = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(1).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type1.toLowerCase().contains(createOrderPayload.getTradeDiscountBreakup().get(1).getRewardType().toLowerCase()));
        softAssert.assertTrue(api_response_discount_amount1.contains(expected_discount_amount1.toString()));
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()));

        int expected_total_saving = (int) (expected_discount_amount + expected_discount_amount1);
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_offer_savings)));

        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }

        /**
         Create Order No.2and push to RMQ
         **/
        CreateOrder createOrderPayload2 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str2 = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str2);
        String orderid2 = String.valueOf(createOrderPayload2.getOrderId());
        System.out.println("orderid2" + orderid2);
        String order_delivered_payload2 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid2));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload2);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse1 = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        int reward_type_index2 = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse1, tdBuilderList.get(0).getRewardType());
        String api_response_reward_type2 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index2 + "].reward_type"));
        String api_response_discount_amount2 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index2 + "].discount"));
        String api_response_totaloffersavings2 = superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount2 = Double.valueOf(createOrderPayload2.getTradeDiscountBreakup().get(0).getDiscountAmount());
        softAssert.assertTrue(api_response_reward_type2.toLowerCase().contains(createOrderPayload2.getTradeDiscountBreakup().get(0).getRewardType().toLowerCase()));

        double total_expected_discount_amount_freedel = expected_discount_amount2 + expected_discount_amount; //sum of previous order discount and current order discount
        softAssert.assertTrue(api_response_discount_amount2.contains(String.valueOf(total_expected_discount_amount_freedel)));
        expected_total_offer_savings = expected_total_offer_savings + tdBuilderList.size();
        softAssert.assertEquals(api_response_totaloffersavings2, String.valueOf(expected_total_offer_savings));

        //Assertion for Percentage offer
        int reward_type_index_percentage2 = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse1, tdBuilderList.get(1).getRewardType());
        String api_response_reward_type3 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index_percentage2 + "].reward_type"));
        String api_response_discount_amount3 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index_percentage2 + "].discount"));
        Double expected_discount_amount3 = expected_discount_amount1 + Double.valueOf(createOrderPayload2.getTradeDiscountBreakup().get(1).getDiscountAmount()); //sum of previous order discount and current order discount

        softAssert.assertTrue(api_response_reward_type3.contains(createOrderPayload2.getTradeDiscountBreakup().get(1).getRewardType()));
        softAssert.assertTrue(api_response_discount_amount3.contains(expected_discount_amount3.toString()));
        softAssert.assertEquals(api_response_totaloffersavings2, String.valueOf(expected_total_offer_savings));
        String api_response_total_super_savings1 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));

        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings1.contains(String.valueOf(expected_total_super_savings)));
        softAssert.assertAll();
        SuperDbHelper.deleteFreebieBenefit();
        SuperDbHelper.deleteBenefitFromDB(benefitId);
    }


    @Test(dataProvider = "mappedUserSubscriptionFreeBieWithIncentiveFlatMultipleOrdersData", description = "Create more than 1 Orders using FreeBie & Percentage , verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void freeBieAndFlatMultipleOrdersTest(String planPayload, String numOfPlans, String benefitPayload,
                                                 HashMap<String, String> createIncentiveData, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails) throws Exception {

        HashMap<String, String> getIds = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload, createIncentiveData);
        String publicUserId = getIds.get("publicUserId");
        String benefitId = getIds.get("benefitId");


        /**
         Create Order No.1 and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);

        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        System.out.println("orderid 1" + orderid);
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        int reward_type_index = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(0).getRewardType());
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index + "].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index + "].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());
        int expected_total_offer_savings = tdBuilderList.size();
        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()));
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()));
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(expected_total_offer_savings));

        //Assertion for Percentage offer
        int reward_type_index_percentage = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(1).getRewardType());
        String api_response_reward_type1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index_percentage + "].reward_type"));
        String api_response_discount_amount1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index_percentage + "].discount"));
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Double expected_discount_amount1 = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(1).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type1.toLowerCase().contains(createOrderPayload.getTradeDiscountBreakup().get(1).getRewardType().toLowerCase()));
        softAssert.assertTrue(api_response_discount_amount1.contains(expected_discount_amount1.toString()));
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()));

        int expected_total_saving = (int) (expected_discount_amount + expected_discount_amount1);
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_offer_savings)));

        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }

        /**
         Create Order No.2and push to RMQ
         **/
        CreateOrder createOrderPayload2 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str2 = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str2);
        String orderid2 = String.valueOf(createOrderPayload2.getOrderId());
        System.out.println("orderid2" + orderid2);
        String order_delivered_payload2 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid2));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload2);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse1 = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        int reward_type_index2 = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse1, tdBuilderList.get(0).getRewardType());
        String api_response_reward_type2 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index2 + "].reward_type"));
        String api_response_discount_amount2 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index2 + "].discount"));
        String api_response_totaloffersavings2 = superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount2 = Double.valueOf(createOrderPayload2.getTradeDiscountBreakup().get(0).getDiscountAmount());
        softAssert.assertTrue(api_response_reward_type2.toLowerCase().contains(createOrderPayload2.getTradeDiscountBreakup().get(0).getRewardType().toLowerCase()), "REWARD_TYPE mismatch....");

        double total_expected_discount_amount_freedel = expected_discount_amount2 + expected_discount_amount; //sum of previous order discount and current order discount
        softAssert.assertTrue(api_response_discount_amount2.contains(String.valueOf(total_expected_discount_amount_freedel)));
        expected_total_offer_savings = expected_total_offer_savings + tdBuilderList.size();
        softAssert.assertEquals(api_response_totaloffersavings2, String.valueOf(expected_total_offer_savings));

        //Assertion for Percentage offer
        int reward_type_index_percentage2 = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse1, tdBuilderList.get(1).getRewardType());
        String api_response_reward_type3 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValue("$.data.super_savings[" + reward_type_index_percentage2 + "].reward_type"));
        String api_response_discount_amount3 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[" + reward_type_index_percentage2 + "].discount"));
        Double expected_discount_amount3 = expected_discount_amount1 + Double.valueOf(createOrderPayload2.getTradeDiscountBreakup().get(1).getDiscountAmount()); //sum of previous order discount and current order discount

        softAssert.assertTrue(api_response_reward_type3.contains(createOrderPayload2.getTradeDiscountBreakup().get(1).getRewardType()), "REWARD_TYPE mismatch....");
        softAssert.assertTrue(api_response_discount_amount3.contains(expected_discount_amount3.toString()), "DISCOUNT_AMOUNT mismatch");
        softAssert.assertEquals(api_response_totaloffersavings2, String.valueOf(expected_total_offer_savings));
        String api_response_total_super_savings1 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));

        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings1.contains(String.valueOf(expected_total_super_savings)), "TOTAL SAVINGS for SUPER USER is not matching....");
        softAssert.assertAll();
        SuperDbHelper.deleteFreebieBenefit();
        SuperDbHelper.deleteBenefitFromDB(benefitId);
    }

    /**
     * COUPON
     **/

    @Test(dataProvider = "nonIncentiveFreeDeliverOrFreebieForSuperUserWithCouponCodeData", description = "Create one Order using Free-Del/FreeBie,with COUPON code only with discount amount verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void nonIncentiveFreeDeliverOrFreebieForSuperUserWithCouponCode(HashMap<String, Object> coupon_map, String planPayload, String numOfPlans,
                                                                           String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails,String coupon_discountamount) throws Exception {



        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getNonIncentiveSubscription(planPayload, numOfPlans, benefitPayload);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));

        /*Create COUPON for super user*/
        superSavingHelper.applyPublicDiscountTypeSuperCouponForUserTypeSuperTest(coupon_map);


        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        byte[] byte_arr = str.getBytes(StandardCharsets.UTF_8);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        int reward_type_index = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(0).getRewardType());

        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings["+reward_type_index+"].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings["+reward_type_index+"].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");

        //Coupons verification
        int reward_type_index_forcoupons = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, "supercoupons");
        String api_response_reward_type_coupons = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings["+reward_type_index_forcoupons+"].reward_type"));
        String api_response_discount_amount_coupons = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings["+reward_type_index_forcoupons+"].discount"));
        softAssert.assertTrue(api_response_reward_type_coupons.contains("supercoupons"), "REWARD_TYPE is not as expected...");

        //softAssert.assertTrue(api_response_discount_amount_coupons.contains( coupon_map.get("createCouponPOJO"), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_discount_amount_coupons, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");



        softAssert.assertEquals(api_response_totaloffersavings, "2", "TOTAL_OFFER_SAVINGS mismatched ....");
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount() ;
        }
        //TD discount amount +  COUPONS discount amount
        expected_total_super_savings=expected_total_super_savings+Integer.valueOf(coupon_discountamount);
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_super_savings)), "TOTAL SUPER SAVINGS mismatch....");
        softAssert.assertAll();

        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }



    @Test(dataProvider = "incentiveFreeDeliverOrFreebieForSuperUserWithCouponCodeData", description = "Create one Order using Free-Del/FreeBie with Percentage ,with COUPON code only with discount amount verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void incentivePercentageFreeDeliverOrFreebieForSuperUserWithCouponCode(HashMap<String, Object> coupon_map, String planPayload, String numOfPlans,
                                                                           String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails,String coupon_discountamount,HashMap<String, String> createIncentiveData) throws Exception {



        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload,createIncentiveData);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));

        /*Create COUPON for super user*/
        superSavingHelper.applyPublicDiscountTypeSuperCouponForUserTypeSuperTest(coupon_map);


        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        byte[] byte_arr = str.getBytes(StandardCharsets.UTF_8);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        int reward_type_index = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(0).getRewardType());

        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings["+reward_type_index+"].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings["+reward_type_index+"].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");

        //Coupons verification
        int reward_type_index_forcoupons = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, "supercoupons");
        String api_response_reward_type_coupons = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings["+reward_type_index_forcoupons+"].reward_type"));
        String api_response_discount_amount_coupons = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings["+reward_type_index_forcoupons+"].discount"));
        softAssert.assertTrue(api_response_reward_type_coupons.contains("supercoupons"), "REWARD_TYPE is not as expected...");

        //softAssert.assertTrue(api_response_discount_amount_coupons.contains( coupon_map.get("createCouponPOJO"), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_discount_amount_coupons, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");



        softAssert.assertEquals(api_response_totaloffersavings, "3", "TOTAL_OFFER_SAVINGS mismatched ....");
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount() ;
        }
        //TD discount amount +  COUPONS discount amount
        expected_total_super_savings=expected_total_super_savings+Integer.valueOf(coupon_discountamount);
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_super_savings)), "TOTAL SUPER SAVINGS mismatch....");
        softAssert.assertAll();

        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }


    @Test(dataProvider = "incentiveFlatFreeDeliverOrFreebieForSuperUserWithCouponCodeFlat", description = "Create one Order using Free-Del/FreeBie with FLAT,with COUPON code only with discount amount verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void incentiveFlatFreeDeliverOrFreebieForSuperUserWithCouponCode(HashMap<String, Object> coupon_map, String planPayload, String numOfPlans,
                                                                                  String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails,String coupon_discountamount,HashMap<String, String> createIncentiveData) throws Exception {



        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload,createIncentiveData);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));

        /*Create COUPON for super user*/
        superSavingHelper.applyPublicDiscountTypeSuperCouponForUserTypeSuperTest(coupon_map);


        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        byte[] byte_arr = str.getBytes(StandardCharsets.UTF_8);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        int reward_type_index = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(0).getRewardType());

        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings["+reward_type_index+"].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings["+reward_type_index+"].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");

        //Coupons verification
        int reward_type_index_forcoupons = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, "supercoupons");
        String api_response_reward_type_coupons = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings["+reward_type_index_forcoupons+"].reward_type"));
        String api_response_discount_amount_coupons = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings["+reward_type_index_forcoupons+"].discount"));
        softAssert.assertTrue(api_response_reward_type_coupons.contains("supercoupons"), "REWARD_TYPE is not as expected...");

        //softAssert.assertTrue(api_response_discount_amount_coupons.contains( coupon_map.get("createCouponPOJO"), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_discount_amount_coupons, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");



        softAssert.assertEquals(api_response_totaloffersavings, "3", "TOTAL_OFFER_SAVINGS mismatched ....");
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount() ;
        }
        //TD discount amount +  COUPONS discount amount
        expected_total_super_savings=expected_total_super_savings+Integer.valueOf(coupon_discountamount);
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_super_savings)), "TOTAL SUPER SAVINGS mismatch....");
        softAssert.assertAll();

        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }


    @Test(dataProvider = "incentiveFlatFreeDeliverFreebieForSuperUserWithCouponCodeData", description = "Create one Order using Free-Del,FreeBie,with COUPON code and Percentage/Flat verify SUPERSAVING BENEFITS", dataProviderClass = SuperSavingDP.class)
    public void incentiveFlatFreeDeliverFreebieForSuperUserWithCouponCode(HashMap<String, Object> coupon_map, String planPayload, String numOfPlans,
                                                                            String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails,String coupon_discountamount,HashMap<String, String> createIncentiveData) throws Exception {



        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload,createIncentiveData);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));

        /*Create COUPON for super user*/
        superSavingHelper.applyPublicDiscountTypeSuperCouponForUserTypeSuperTest(coupon_map);


        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        byte[] byte_arr = str.getBytes(StandardCharsets.UTF_8);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        int reward_type_index = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, tdBuilderList.get(0).getRewardType());

        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings["+reward_type_index+"].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings["+reward_type_index+"].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");

        //Coupons verification
        int reward_type_index_forcoupons = superSavingHelper.getRewardTypeIndex(superSavingsBenefitsResponse, "supercoupons");
        String api_response_reward_type_coupons = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings["+reward_type_index_forcoupons+"].reward_type"));
        String api_response_discount_amount_coupons = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings["+reward_type_index_forcoupons+"].discount"));
        softAssert.assertTrue(api_response_reward_type_coupons.contains("supercoupons"), "REWARD_TYPE is not as expected...");

        //softAssert.assertTrue(api_response_discount_amount_coupons.contains( coupon_map.get("createCouponPOJO"), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_discount_amount_coupons, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");



        softAssert.assertEquals(api_response_totaloffersavings, "4", "TOTAL_OFFER_SAVINGS mismatched ....");
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount() ;
        }
        //TD discount amount +  COUPONS discount amount
        expected_total_super_savings=expected_total_super_savings+Integer.valueOf(coupon_discountamount);
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_super_savings)), "TOTAL SUPER SAVINGS mismatch....");
        softAssert.assertAll();

        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }






    /***WAS SUPER**/
    @Test(dataProvider = "wasSuperPublicUserSubscriptionFreeDelData", description = "Create one Order using Free-Del/FreeBie, verify SUPERSAVING BENEFITS for WAS SUPER user no order placed when user was SUPER", dataProviderClass = SuperSavingDP.class)
    public void wasSuperNonIncentiveFreeDeliverOrFreebie(String planPayload, String numOfPlans,
                                                         String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails, HashMap<String, String> wasSuperPlan) throws Exception {

        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getNonIncentiveSubscription(planPayload, numOfPlans, benefitPayload);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));

        //patch api call to expire plan
        wasSuperPlan.put("0", String.valueOf(planId));
        String patchPlanPayload = superHelper.populatePatchPlanPayload(wasSuperPlan);
        System.out.println("patch plan payload-->>>" + patchPlanPayload);
        superHelper.patchPlan(patchPlanPayload);

        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        byte[] byte_arr = str.getBytes(StandardCharsets.UTF_8);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data.super_savings"));
        softAssert.assertEquals(api_response_reward_type, "[]", "SUPER SAVINGS Array is not empty .....");

        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        softAssert.assertEquals(api_response_totaloffersavings, "0", "SUPER USER TOTAL OFFER SAVINGS is not ZERO...");

        String api_response_totalsupersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings");
        softAssert.assertEquals(api_response_totalsupersavings, "0", "SUPER USER TOTAL SUPER SAVINGS is not ZERO...");
        String api_response_cut_off = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cut_off");
        softAssert.assertEquals(api_response_cut_off, "0", "SUPER USER TOTAL CUT OFF is not ZERO...");
        softAssert.assertAll();

        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }


    @Test(dataProvider = "nonIncentiveFreeDeliverOrFreebieForSuperUserAndSubscriptionExpiredData", description = "Create one Order using Free-Del/FreeBie, verify SUPERSAVING BENEFITS and expiry the plan and create new orders ", dataProviderClass = SuperSavingDP.class)
    public void nonIncentiveFreeDeliverOrFreebieForSuperUserAndSubscriptionExpired(String planPayload, String numOfPlans, String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String,
            String> orderdetails, HashMap<String, String> wasSuperPlan, HashMap<String, String> orderdetails_for_wassuper) throws Exception {

        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getNonIncentiveSubscription(planPayload, numOfPlans, benefitPayload);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));


        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        byte[] byte_arr = str.getBytes(StandardCharsets.UTF_8);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");

        //patch api call to expire plan
        wasSuperPlan.put("0", String.valueOf(planId));
        String patchPlanPayload = superHelper.populatePatchPlanPayload(wasSuperPlan);
        System.out.println("patch plan payload-->>>" + patchPlanPayload);
        superHelper.patchPlan(patchPlanPayload);


        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload1 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails_for_wassuper);
        String str1 = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str1);
        String orderid1 = String.valueOf(createOrderPayload1.getOrderId());
        String order_delivered_payload1 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid1));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload1);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse1 = superSavingHelper.getPlansByUserId(publicUserId);
        //Assertion for WAS_SUPER- No new super user savings should be added
        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");

        softAssert.assertAll();
        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }



    @Test(dataProvider = "freeDelFreebieAndPercentageWasSuperTestData", description = "Create one Order using Free-Del/FreeBie & Percentage SUPER, verify SUPERSAVING BENEFITS and create order when user is WAS SUPER ", dataProviderClass = SuperSavingDP.class)
    public void freeDelFreebieAndPercentageWasSuperTest(String planPayload, String numOfPlans, String benefitPayload,
                                                        HashMap<String, String> createIncentiveData, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails,
                                                        HashMap<String,String> wasSuperPlan,HashMap<String, String> orderdetails_wassuper ) throws Exception {

        HashMap<String, String> getIds = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload, createIncentiveData);
        String publicUserId = getIds.get("publicUserId");
        String planId= getIds.get("planId");
        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE mismatched....");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT_AMOUNT mismatched.....");
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()), "TOTAL_OFFER_SAVINGS mismatched ....");

        //Assertion for Percentage offer
        String api_response_reward_type1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[1].reward_type"));
        String api_response_discount_amount1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[1].discount"));
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Double expected_discount_amount1 = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(1).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type1.contains(createOrderPayload.getTradeDiscountBreakup().get(1).getRewardType()), "REWARD_TYPE mismatched....");
        softAssert.assertTrue(api_response_discount_amount1.contains(expected_discount_amount1.toString()), "DISCOUNT_AMOUNT mismatched.....");
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()), "TOTAL_OFFER_SAVINGS mismatched ....");

        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_super_savings)), "TOTAL SUPER SAVINGS mismatch....");


        //patch api call to expire plan
        wasSuperPlan.put("0",planId);
        String patchPlanPayload = superHelper.populatePatchPlanPayload(wasSuperPlan);
        System.out.println("patch plan payload-->>>" + patchPlanPayload);
        superHelper.patchPlan(patchPlanPayload);

        /**
         Create Order and push to RMQ - after SUPER plan expiry
         **/
        CreateOrder createOrderPayload1 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails_wassuper);
        String str1 = jsonHelper.getObjectToJSON(createOrderPayload1);

        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str1);
        String orderid1 = String.valueOf(createOrderPayload1.getOrderId());
        String order_delivered_payload1 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid1));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload1);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse1 = superSavingHelper.getPlansByUserId(publicUserId);
        //Assertion for total super savings
        String api_response_total_super_savings1 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Integer expected_total_super_savings1 = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings1 + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings1.contains(String.valueOf(expected_total_super_savings1)), "TOTAL SUPER SAVINGS mismatch....");
        softAssert.assertAll();

        SuperDbHelper.deleteFreebieBenefit();
    }


    @Test(dataProvider = "freeDelFreebieAndFlatWasSuperTestData", description = "Create one Order using Free-Del/FreeBie & Percentage SUPER, verify SUPERSAVING BENEFITS and create order when user is WAS SUPER ", dataProviderClass = SuperSavingDP.class)
    public void freeDelFreebieAndFlatWasSuperTest(String planPayload, String numOfPlans, String benefitPayload,
                                                        HashMap<String, String> createIncentiveData, List<TradeDiscountBreakup> tdBuilderList, HashMap<String, String> orderdetails,
                                                        HashMap<String,String> wasSuperPlan,HashMap<String, String> orderdetails_wassuper ) throws Exception {

        HashMap<String, String> getIds = superSavingHelper.getIncentiveSubscription(planPayload, numOfPlans, benefitPayload, createIncentiveData);
        String publicUserId = getIds.get("publicUserId");
        String planId= getIds.get("planId");
        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);

        //Assertion for Free-Del
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE mismatched....");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT_AMOUNT mismatched.....");
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()), "TOTAL_OFFER_SAVINGS mismatched ....");

        //Assertion for Percentage offer
        String api_response_reward_type1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[1].reward_type"));
        String api_response_discount_amount1 = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[1].discount"));
        String api_response_total_super_savings = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Double expected_discount_amount1 = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(1).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type1.contains(createOrderPayload.getTradeDiscountBreakup().get(1).getRewardType()), "REWARD_TYPE mismatched....");
        softAssert.assertTrue(api_response_discount_amount1.contains(expected_discount_amount1.toString()), "DISCOUNT_AMOUNT mismatched.....");
        softAssert.assertEquals(api_response_totaloffersavings, String.valueOf(tdBuilderList.size()), "TOTAL_OFFER_SAVINGS mismatched ....");

        //Assertion for total super savings
        Integer expected_total_super_savings = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings.contains(String.valueOf(expected_total_super_savings)), "TOTAL SUPER SAVINGS mismatch....");


        //patch api call to expire plan
        wasSuperPlan.put("0",planId);
        String patchPlanPayload = superHelper.populatePatchPlanPayload(wasSuperPlan);
        System.out.println("patch plan payload-->>>" + patchPlanPayload);
        superHelper.patchPlan(patchPlanPayload);

        /**
         Create Order and push to RMQ - after SUPER plan expiry
         **/
        CreateOrder createOrderPayload1 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails_wassuper);
        String str1 = jsonHelper.getObjectToJSON(createOrderPayload1);

        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str1);
        String orderid1 = String.valueOf(createOrderPayload1.getOrderId());
        String order_delivered_payload1 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid1));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload1);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse1 = superSavingHelper.getPlansByUserId(publicUserId);
        //Assertion for total super savings
        String api_response_total_super_savings1 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_super_savings"));
        Integer expected_total_super_savings1 = 0;
        for (TradeDiscountBreakup tdlist : tdBuilderList) {
            expected_total_super_savings = expected_total_super_savings1 + tdlist.getDiscountAmount();
        }
        softAssert.assertTrue(api_response_total_super_savings1.contains(String.valueOf(expected_total_super_savings1)), "TOTAL SUPER SAVINGS mismatch....");
        softAssert.assertAll();

        SuperDbHelper.deleteFreebieBenefit();
    }


    @Test(dataProvider = "nonIncentiveFreeDeliverOrFreebieForSuperUserAndSubscriptionExpiredData", description = "Create one Order using Free-Del/FreeBie- SUPER user, expiry the plan -create new orders WAS SUPER & subscribe again to SUPER and order creation", dataProviderClass = SuperSavingDP.class)
    public void nonIncentiveWasSuperToSuperFreeDeliverOrFreebie(String planPayload, String numOfPlans, String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String,
            String> orderdetails, HashMap<String, String> wasSuperPlan, HashMap<String, String> orderdetails_for_wassuper) throws Exception {


        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getNonIncentiveSubscription(planPayload, numOfPlans, benefitPayload);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));

        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");

        //patch api call to expire plan
        wasSuperPlan.put("0", planId);
        String patchPlanPayload = superHelper.populatePatchPlanPayload(wasSuperPlan);
        System.out.println("patch plan payload-->>>" + patchPlanPayload);
        superHelper.patchPlan(patchPlanPayload);

        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload1 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails_for_wassuper);
        String str1 = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str1);
        String orderid1 = String.valueOf(createOrderPayload1.getOrderId());
        String order_delivered_payload1 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid1));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload1);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse1 = superSavingHelper.getPlansByUserId(publicUserId);
        //Assertion for WAS_SUPER- No new super user savings should be added
        String api_response_reward_type1 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount1 = JSON.toString(superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings1 = superSavingsBenefitsResponse1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount1 = Double.valueOf(createOrderPayload1.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type1.contains(createOrderPayload1.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount1.contains(expected_discount_amount1.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings1, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");


        //patch api call to renew plan
        wasSuperPlan.put("0", planId);
        wasSuperPlan.put("2",String.valueOf(superSavingHelper.getOneMonthFutureInMilliSecondsFromCurrentDate())); //valid till update to one month in future from the current date
        String patchPlanPayload1 = superHelper.populatePatchPlanPayload(wasSuperPlan);
        System.out.println("patch plan payload-->>>" + patchPlanPayload1);
        superHelper.patchPlan(patchPlanPayload);

        /**
         Create Order and push to RMQ - After renewal
         **/
        CreateOrder createOrderPayload2 = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str2 = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str2);
        String orderid2 = String.valueOf(createOrderPayload2.getOrderId());
        String order_delivered_payload2 = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid2));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload2);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse2 = superSavingHelper.getPlansByUserId(publicUserId);
        String api_response_reward_type2 = JSON.toString(superSavingsBenefitsResponse2.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount2 = JSON.toString(superSavingsBenefitsResponse2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings2 = superSavingsBenefitsResponse2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount2 = expected_discount_amount + Double.valueOf(createOrderPayload2.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type2.contains(createOrderPayload2.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount2.contains(expected_discount_amount2.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings2, "2", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");
        softAssert.assertAll();
        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }



    @Test(dataProvider = "superWasSuperToSuperFreeDeliverOrFreebieData", description = "Super user - > Expired -> Renewal to Super user and then get order Free Del / Freebie", dataProviderClass = SuperSavingDP.class)
    public void superWasSuperToSuperFreeDeliverOrFreebie(String planPayload, String numOfPlans, String benefitPayload, List<TradeDiscountBreakup> tdBuilderList, HashMap<String,
            String> orderdetails, HashMap<String, String> wasSuperPlan) throws Exception {


        //Subscribe to plan, benfits with user mapping
        HashMap<String, String> getids = superSavingHelper.getNonIncentiveSubscription(planPayload, numOfPlans, benefitPayload);
        String publicUserId = String.valueOf(getids.get("publicUserId"));
        String planId = String.valueOf(getids.get("planId"));
        String benefitId = String.valueOf(getids.get("benefitId"));


        //patch api call to expire plan
        wasSuperPlan.put("0", planId);
        String patchPlanPayload = superHelper.populatePatchPlanPayload(wasSuperPlan);
        System.out.println("patch plan payload-->>>" + patchPlanPayload);
        superHelper.patchPlan(patchPlanPayload);


        //patch api call to renew plan
        wasSuperPlan.put("0", planId);
        wasSuperPlan.put("2",String.valueOf(superSavingHelper.getOneMonthFutureInMilliSecondsFromCurrentDate())); //valid till update to one month in future from the current date
        String patchPlanPayload1 = superHelper.populatePatchPlanPayload(wasSuperPlan);
        System.out.println("patch plan payload-->>>" + patchPlanPayload1);
        superHelper.patchPlan(patchPlanPayload);


        /**
         Create Order and push to RMQ
         **/
        CreateOrder createOrderPayload = superSavingHelper.getCreateOrderPayload(tdBuilderList, publicUserId, orderdetails);
        String str = jsonHelper.getObjectToJSON(createOrderPayload);
        rmqHelper.pushMessageToExchange("rng", "swiggy.order_create", new AMQP.BasicProperties().builder().contentType("application/json"), str);
        String orderid = String.valueOf(createOrderPayload.getOrderId());
        String order_delivered_payload = jsonHelper.getObjectToJSON(superSavingHelper.getOrderDeliveredPayload(orderid));

        /*KAFKA event push for Order Delivery*/
        kafkaHelper.runProducer("rng", "order_status_update_v2", order_delivered_payload);

        //API call for super saving benefits
        Thread.sleep(1000);
        Processor superSavingsBenefitsResponse = superSavingHelper.getPlansByUserId(publicUserId);
        String api_response_reward_type = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValue("$.data.super_savings[0].reward_type"));
        String api_response_discount_amount = JSON.toString(superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super_savings[0].discount"));
        String api_response_totaloffersavings = superSavingsBenefitsResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total_offers_savings");
        Double expected_discount_amount = Double.valueOf(createOrderPayload.getTradeDiscountBreakup().get(0).getDiscountAmount());

        softAssert.assertTrue(api_response_reward_type.contains(createOrderPayload.getTradeDiscountBreakup().get(0).getRewardType()), "REWARD_TYPE is not as expected...");
        softAssert.assertTrue(api_response_discount_amount.contains(expected_discount_amount.toString()), "DISCOUNT AMOUNT mismatched...");
        softAssert.assertEquals(api_response_totaloffersavings, "1", "SUPER USER TOTAL OFFER SAVINGS are not matching ...");
        softAssert.assertAll();
        SuperDbHelper.deleteBenefitFromDB(benefitId);
        SuperDbHelper.deleteFreebieBenefit();
    }









}

