package com.swiggy.api.sf.checkout.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.SchemaValidationConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.dp.FriedManDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutPricingHelper;
import com.swiggy.api.sf.checkout.helper.FriedManV1Validator;
import com.swiggy.api.sf.checkout.helper.PreOrderCartPayload;

import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.RedisHelper;

import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.*;

import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.SuperHelper;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.MinAmount;
import com.swiggy.api.sf.snd.helper.SANDHelper;

import java.nio.file.*;
import com.swiggy.api.sf.checkout.helper.FriedManV0Validator;
import com.swiggy.api.sf.checkout.helper.ServicableRegularRestaurant;

public class FriedManTest extends FriedManDP {
	
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	CheckoutHelper helper= new CheckoutHelper();
	SoftAssert softAssertion= new SoftAssert();
	CheckoutPricingHelper pricingHelper=new CheckoutPricingHelper();
	RedisHelper redisHelper = new RedisHelper();
	FriedManV1Validator friedManValidator=new FriedManV1Validator();
	FriedManV0Validator FriedManV0Validator=new FriedManV0Validator();
	EDVOCartHelper eDVOCartHelper=new EDVOCartHelper();
	ServicableRegularRestaurant ServicableRestaurant=new ServicableRegularRestaurant();
	PreOrderCartPayload preOrderCartPayload=new PreOrderCartPayload();
	
	
	String tid;
    String token;
    String userID;
    String couponCode;
    String planId="0";
    String orderId;
    int iOSVersion;
    int webVersion;
    int AndroidVersion;
    
    
    String schemaPath= SchemaValidationConstants.SCHEMA_PATH;
    String mobile= System.getenv("mobile");
	String password= System.getenv("password");
	
    @BeforeClass
    public void login(){
    	if (mobile == null || mobile.isEmpty()){
    		mobile=CheckoutConstants.mobile;
    		}
    	
    	if (password == null ||password.isEmpty()){
    		password=CheckoutConstants.password;
    		}
    	
    	HashMap<String, String> hashMap=helper.TokenData(mobile,password);
    	tid=hashMap.get("Tid");
        token=hashMap.get("Token");
        userID=hashMap.get("CustomerId");
    }
    
    
    @BeforeMethod
    public void initializeData(){
    	helper.resetUserCredit(userID);
    	deleteCheckoutCache(userID);
    	iOSVersion=  helper.getRenderingVersion("new_cart_rendering_ios");
    	AndroidVersion=helper.getRenderingVersion("new_cart_rendering_android");
    	webVersion=helper.getRenderingVersion("new_cart_rendering_web");
    }
    
    
    
    @Test(dataProvider="friedMan", groups = {"friedMan","chandan"},description = "fried Man Test")
	public void createCartSchemaValidatorTest(String userAgent,String versionCode,String cartType,
			boolean isCouponApplied,String TDType,boolean isGSTApplied,
			boolean isPackagingCharge,boolean isDelFee,boolean isthresholdFee,
			boolean isTimeFee,boolean isDistanceFee,boolean specialFee,
			double swiggyMoney,double cancellationFee)throws IOException, ProcessingException {
		
    	createCoupon(isCouponApplied);
    	
    	String restId = null;
    	Cart cartPayload = null;
    	
    	if(cartType.equalsIgnoreCase("regular") ||cartType.equalsIgnoreCase("superSec") || cartType.equalsIgnoreCase("asSuper")){
    	cartPayload=ServicableRestaurant.getRegularRestData(cartType,isCouponApplied,helper, eDVOCartHelper,userID);
    	restId=cartPayload.getRestaurantId();
    	} else if(cartType.equalsIgnoreCase("preorder")){
    		cartPayload=preOrderCartPayload.getPreOrderCartPayload(helper, eDVOCartHelper,couponCode);
    		restId=cartPayload.getRestaurantId();
    	}
    	
    	resetRestaurant(restId);
    	resetSurgeFee(restId);
    	setTestData(isCouponApplied,TDType,isGSTApplied,
    			    isPackagingCharge,isDelFee,isthresholdFee,
    			    isTimeFee,isDistanceFee,specialFee,swiggyMoney,cancellationFee,restId);
    	
    	String cartResponse=setCartData(cartType,userAgent,versionCode,cartPayload,restId);
    	
    	Integer inputVerCode=Integer.parseInt(versionCode);
    	
    	if (userAgent.equals("Swiggy-Android") && inputVerCode>=AndroidVersion || userAgent.equals("Swiggy-iOS") && inputVerCode>=iOSVersion ||
    			userAgent.equals("Web") && inputVerCode>=webVersion){
    	         System.out.println("New Flow");
    	         friedManValidator.validateCartDetails(cartResponse);
    	} else{
    		System.out.println("Old flow");
    		FriedManV0Validator.validateV0CartDetails(cartResponse);
    	}
		softAssertion.assertAll();
	}
    
    
    public String setCartData(String cartType,String userAgent,String versionCode,Cart cartPayload,String restId ){
    	 String cartResponse = null;
    	if(cartType.equalsIgnoreCase("regular")){
           cartResponse=createCart(userAgent,versionCode,cartPayload);
        	} else if(cartType.equalsIgnoreCase("superSec")){
               cartResponse=createCart(userAgent,versionCode,cartPayload);
            } else if(cartType.equalsIgnoreCase("asSuper")){
               createCart(userAgent,versionCode,cartPayload);
               String placeOrderRes=placeOrder(userAgent,versionCode,cartPayload);
               orderId=getOrderId(placeOrderRes);
               cartPayload=eDVOCartHelper.getCartPayloadSuper(null,restId, false, false, true,couponCode,0);
               cartResponse=createCart(userAgent,versionCode,cartPayload);
            } else if(cartType.equalsIgnoreCase("PREORDER")){
            	cartResponse=createCart(userAgent,versionCode,cartPayload);
            } else if(cartType.equalsIgnoreCase("EDVO")){
            	String[] mealId=null;
            	cartPayload=eDVOCartHelper.getCartPayload(mealId, restId, false, true, true);
            	cartResponse=createCart(userAgent,versionCode,cartPayload);
            } else if(cartType.equalsIgnoreCase("CAFE")){
            	cartPayload=eDVOCartHelper.getCafeCartPayload(restId, false, true, true,couponCode);
            	cartResponse=createCart(userAgent,versionCode,cartPayload);
            } else if(cartType.equalsIgnoreCase("POP")){
            	cartPayload=eDVOCartHelper.getCartPayloadPopOrder(null,restId, false, true, true);
            	cartResponse=createCart(userAgent,versionCode,cartPayload);
            } 
    	
		return cartResponse;
    }
    
    
    public void setTestData(boolean isCouponApplied,String TDType,boolean isGSTApplied,
			boolean isPackagingCharge,boolean isDelFee,boolean isthresholdFee,
			boolean isTimeFee,boolean isDistanceFee,boolean specialFee,
			double swiggyMoney,double cancellationFee,String restId) throws IOException{
    	
    	if(TDType.equals("flatTD")){
    		ServicableRestaurant.createTD(CheckoutConstants.fee, restId);
    		Reporter.log("Creating flatTD on restaurant, id-"+restId,true);
    	} else if(TDType.equals("FreeDelTD")){
    		ServicableRestaurant.createFreeDelTD(restId, false);
    		Reporter.log("Creating FreeDelTD on restaurant, id-"+restId,true);
    	} else {
    		ServicableRestaurant.disabledActiveTD(restId);
    		Reporter.log("Disabling all TD from restaurant, id-"+restId,true);
    	}
    	
    	if(isGSTApplied==true){
    		helper.updateItemGST(restId);
    		helper.updatePackagingGST(restId);
    		deleteResGSTCache(restId);
    	}
    	
    	if (isPackagingCharge==true){
    		helper.updatePackingFee(restId, CheckoutConstants.fee);
    		deleteResCache(restId);
    		Reporter.log("Applying Packaging Charge -"+CheckoutConstants.fee,true);
    	}
    	
    	if (isDelFee==true){
    		helper.updateDeliveryFee(restId, CheckoutConstants.fee);
    		deleteResCache(restId);
    		
    	}
    	
    	if (isthresholdFee==true){
    		setThresholdFee(restId, CheckoutConstants.cartTotalRange,CheckoutConstants.fee);
    		deleteCheckoutPricingCache(restId);
    		Reporter.log("Applying threshold fee-"+CheckoutConstants.fee,true);
    	}
    	
    	if (isTimeFee==true){
    		setTimeFee(restId,CheckoutConstants.fee);
    		deleteCheckoutPricingCache(restId);
    		Reporter.log("Applying time fee-"+CheckoutConstants.fee,true);
    	}
    	
    	if (isDistanceFee==true){
    		setDistanceFee(restId, CheckoutConstants.defaultDistanceTo,CheckoutConstants.fee);
    		deleteCheckoutPricingCache(restId);
    		Reporter.log("Applying Distance fee-"+CheckoutConstants.fee,true);
    	}
    	if (specialFee==true){
    		setSpecialFee(restId,CheckoutConstants.fee);
    		deleteCheckoutPricingCache(restId);
    		Reporter.log("Applying Special fee-"+CheckoutConstants.fee,true);
    	}
    	
    	if (swiggyMoney>0){
    		helper.updateSwiggyMoney(userID, swiggyMoney);
    		deleteCheckoutCache(userID);
    		Reporter.log("Adding Swiggy Money-"+swiggyMoney,true);
    	}
    	
    	if (cancellationFee>0){
    		helper.updateCancellationFee(userID, cancellationFee);
    		deleteCheckoutCache(userID);
    		Reporter.log("Adding cancellationFee-"+cancellationFee,true);
    	}
    	
    }
    
    
	public String readFile(String fileName) throws IOException, ProcessingException{
		String s=System.getProperty("user.dir") + schemaPath+fileName;
			    
	    String data = ""; 
	    data = new String(Files.readAllBytes(Paths.get(s))); 
	    return data; 
	}

	
    public String createCart(String userAgent,String versionCode,Cart cartPayload){
	  HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
	  String cartResponse=helper.createCart(header,Utility.jsonEncode(cartPayload))
				                  .ResponseValidator.GetBodyAsText();
      helper.validateApiResStatusData(cartResponse);
      return cartResponse;
    }
  
    
  public String placeOrder(String userAgent,String versionCode,Cart cartPayload){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
	    String cartResponse=createCart(userAgent,versionCode,cartPayload);
		String addressId=helper.getAddressIDfromCartResponse(tid, token, cartResponse);
		String placeOrderRes=helper.orderPlaceV2(header, addressId,superConstant.PAYMENT_METHOD_CASH,superConstant.DEF_STRING)
				.ResponseValidator.GetBodyAsText();
		helper.validateApiResStatusData(placeOrderRes);
  return placeOrderRes;
   }
  
   public String cancelOrder(String OrderID){
  	String cancelOrderRes=helper.OrderCancel(tid,token,CheckoutConstants.authorization, superConstant.DEF_STRING,
              superConstant.EXPECTED_TRUE_FLAG, superConstant.ZERO_AS_STRING, superConstant.DEF_STRING, OrderID)
              .ResponseValidator.GetBodyAsText();
  	helper.validateApiResStatusData(cancelOrderRes);
      return cancelOrderRes;
   }
  
   
  public String getOrderId(String placeOrderRes){
  	String orderId=JsonPath.read(placeOrderRes, "$.data..order_id").toString().replace("[","").replace("]","");
  	System.out.println("Order Id:--->"+orderId);
  	return orderId;
  }
  
  public void setThresholdFee(String restId,String cartTotalRangeTo,String fee){
	  String thresholdFeeRes= pricingHelper.setThresholdFee(restId,CheckoutConstants.defaultZeroHour,CheckoutConstants.default23Hour,fee,
			  fee,CheckoutConstants.defaultZero,cartTotalRangeTo,fee)
			                               .ResponseValidator.GetBodyAsText();
      helper.validateApiResStatusData(thresholdFeeRes);
  }
  
  public void setDistanceFee(String restId,String distanceTo,String fee){
	  String distanceFeeRes= pricingHelper.setDistanceFee(restId,CheckoutConstants.defaultZeroHour,CheckoutConstants.default23Hour,
			  fee,"1",distanceTo,fee).ResponseValidator.GetBodyAsText();
      helper.validateApiResStatusData(distanceFeeRes);
  }
  
  public void setTimeFee(String restId,String fee){
	  String timeFeeRes= pricingHelper.setTimeFee(restId,CheckoutConstants.defaultZeroHour,CheckoutConstants.default23Hour,fee)
			                          .ResponseValidator.GetBodyAsText();
      helper.validateApiResStatusData(timeFeeRes);
  }
  
  public void setSpecialFee(String restId,String fee){
	  String fromDate=helper.getDayPlusOrMinusCurrentDateTime_yyyy_mm_dd_hh_mm(-1);
	  //helper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm();
	  String toDate=helper.getDayPlusOrMinusCurrentDateTime_yyyy_mm_dd_hh_mm(1);
	  String specialFeeRes= pricingHelper.setSpecialFee(restId,"false",fee,"1",fromDate,toDate)
		                                  .ResponseValidator.GetBodyAsText();
      helper.validateApiResStatusData(specialFeeRes);
  }
    
   public void deleteResCache(String restID){
	   redisHelper.deleteKey("sandredisstage",2,"METADATA_RESTAURANT_"+restID);
	   redisHelper.deleteKey("sandredisstage",2,"RESTAURANT_"+restID);
   }
   
   public void deleteResGSTCache(String restID){
	   redisHelper.deleteKey("sandredisstage",0,"RESTAURANT_GST_NEW_"+restID);	   
   }
   
   public void deleteCheckoutCache(String userID){
	   redisHelper.deleteKey("checkoutredis",0,"user_credit_"+userID);
   }
   
   public void deleteCheckoutPricingCache(String restID){
	   redisHelper.deleteKey("checkoutredis",0,"REST_PRICING_"+restID);
   }
 
 
   public void resetRestaurant(String restID){
	   if (restID!=null){
   	      helper.resetGST(restID);
   	      //deleteResGSTCache(restID);
   	      helper.resetRestFee(restID);
   	      //deleteResCache(restID);
   }}
   
   public void resetSurgeFee(String restID){
	   if (restID!=null){
		        deleteCheckoutPricingCache(restID);
	    		setThresholdFee(restID, CheckoutConstants.cartTotalRange,CheckoutConstants.defaultZero);
	    		setTimeFee(restID,CheckoutConstants.defaultZero);
	    		setDistanceFee(restID, CheckoutConstants.defaultDistanceTo,CheckoutConstants.defaultZero);
	    		setSpecialFee(restID,CheckoutConstants.defaultZero);
	    		deleteCheckoutPricingCache(restID);
	    	}
   }
   
   public void createCoupon(boolean isCouponApplied) throws IOException{
	   if(isCouponApplied==true){
   		couponCode=ServicableRestaurant.getCoupon();
   	}
   }
   
   @AfterMethod
   public void resetData(){
	   if (orderId!=null){
		   cancelOrder(orderId);
	   }
   }
}