package com.swiggy.api.sf.rng.dp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.rng.helper.CreateEDVOOperationMeta;
import com.swiggy.api.sf.rng.helper.CreateReward;
import com.swiggy.api.sf.rng.helper.Utility;

public class MultiApplyDP {

	@DataProvider(name = "createMultiApplyCouponDiscountAmountData")
	public static Object[][] createMultiApplyCouponDiscountAmountData() {
		{
			// TCs of DiscountAmount, Discount% and UpperCap

			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();

			String duplicateType = "false";
			// String.valueOf(a1).toString();
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "100");
			hashMap.put("35", "1");
			hashMap.put("36", "75");
			hashMap.put("37", "0");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "55");
			hashMap.put("55", "0");
			hashMap.put("56", "75");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "100");
			hashMap1.put("35", "1");
			hashMap1.put("36", "75");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "55");
			hashMap1.put("55", "0");
			hashMap1.put("56", "75");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			hashMap2.put("0", "ALPHACODE");
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "100");
			hashMap2.put("35", "1");
			hashMap2.put("36", "0");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "0");
			hashMap2.put("55", "10");
			hashMap2.put("56", "75");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType },
					{ hashMap2, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponInvalidDiscountAmountData")
	public static Object[][] createMultiApplyCouponInvalidDiscountAmountData() {
		{ // negative TCs of DiscountAmount, Discount% and UpperCap
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();
			HashMap<String, String> hashMap4 = new HashMap<String, String>();
			HashMap<String, String> hashMap5 = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "100");
			hashMap.put("35", "1");
			hashMap.put("36", "null");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "null");
			hashMap.put("55", "10");
			hashMap.put("56", "75");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "100");
			hashMap1.put("35", "1");
			hashMap1.put("36", "175");
			hashMap1.put("37", "0");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "200");
			hashMap1.put("55", "0");
			hashMap1.put("56", "150");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			hashMap2.put("0", "ALPHACODE");
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "100");
			hashMap2.put("35", "1");
			hashMap2.put("36", "0");
			hashMap2.put("37", "0");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "0");
			hashMap2.put("55", "0");
			hashMap2.put("56", "150");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");

			hashMap3.put("0", "ALPHACODE");
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1519279920000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "100");
			hashMap3.put("35", "1");
			hashMap3.put("36", "0");
			hashMap3.put("37", "0");
			hashMap3.put("38", "0");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Discount");
			hashMap3.put("52", "10");
			hashMap3.put("53", "1");
			hashMap3.put("54", "0");
			hashMap3.put("55", "0");
			hashMap3.put("56", "0");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");

			hashMap4.put("0", "ALPHACODE");
			hashMap4.put("1", "MULTICOUPON");
			hashMap4.put("2", "Internal");
			hashMap4.put("3", "AUTOMATION");
			hashMap4.put("4", "2");
			hashMap4.put("5", "10");
			hashMap4.put("6", "false");
			hashMap4.put("7", "false");
			hashMap4.put("8", "false");
			hashMap4.put("9", "false");
			hashMap4.put("10", "false");
			hashMap4.put("11", "false");
			hashMap4.put("12", "false");
			hashMap4.put("13", "false");
			hashMap4.put("14", "false");
			hashMap4.put("15", "false");
			hashMap4.put("16", "false");
			hashMap4.put("17", "1518934320000");
			hashMap4.put("18", "1519279920000");
			hashMap4.put("19", "ankita.yadav@swiggy.in");
			hashMap4.put("20", "1518934320000");
			hashMap4.put("21", "1518934320000");
			hashMap4.put("22", "null");
			hashMap4.put("23", "null");
			hashMap4.put("24", "null");
			hashMap4.put("25", "-1");
			hashMap4.put("26", "false");
			hashMap4.put("27", "0");
			hashMap4.put("28", "null");
			hashMap4.put("29", "null");
			hashMap4.put("30", "null");
			hashMap4.put("31", "null");
			hashMap4.put("32", "1");
			hashMap4.put("33", "Discount");
			hashMap4.put("34", "100");
			hashMap4.put("35", "1");
			hashMap4.put("36", "null");
			hashMap4.put("37", "null");
			hashMap4.put("38", "null");
			hashMap4.put("39", "false");
			hashMap4.put("40", "0");
			hashMap4.put("41", "-1");
			hashMap4.put("42", "1st transaction desc.");
			hashMap4.put("43", "1st transaction");
			hashMap4.put("44", "\"com.swiggy.poc.test1\"");
			hashMap4.put("45", "w2fh4c46d0bszhl89voy");
			hashMap4.put("46", "false");
			hashMap4.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap4.put("48", "ANDROID");
			hashMap4.put("49", "2");
			hashMap4.put("50", "2");
			hashMap4.put("51", "Discount");
			hashMap4.put("52", "10");
			hashMap4.put("53", "1");
			hashMap4.put("54", "null");
			hashMap4.put("55", "null");
			hashMap4.put("56", "null");
			hashMap4.put("57", "false");
			hashMap4.put("58", "0");
			hashMap4.put("59", "-1");
			hashMap4.put("60", "1st transaction desc.");
			hashMap4.put("61", "1st transaction");
			hashMap4.put("62", "\"com.swiggy.poc.test1\"");
			hashMap4.put("63", "w2fh4c46d0bszhl89voy");
			hashMap4.put("64", "false");
			hashMap4.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap4.put("66", "ANDROID");
			hashMap4.put("67", "2");
			hashMap4.put("68", "false");
			hashMap4.put("69", "false");
			hashMap4.put("70", "false");

			hashMap5.put("0", "ALPHACODE");
			hashMap5.put("1", "MULTICOUPON");
			hashMap5.put("2", "Internal");
			hashMap5.put("3", "AUTOMATION");
			hashMap5.put("4", "2");
			hashMap5.put("5", "10");
			hashMap5.put("6", "false");
			hashMap5.put("7", "false");
			hashMap5.put("8", "false");
			hashMap5.put("9", "false");
			hashMap5.put("10", "false");
			hashMap5.put("11", "false");
			hashMap5.put("12", "false");
			hashMap5.put("13", "false");
			hashMap5.put("14", "false");
			hashMap5.put("15", "false");
			hashMap5.put("16", "false");
			hashMap5.put("17", "1518934320000");
			hashMap5.put("18", "1519279920000");
			hashMap5.put("19", "ankita.yadav@swiggy.in");
			hashMap5.put("20", "1518934320000");
			hashMap5.put("21", "1518934320000");
			hashMap5.put("22", "null");
			hashMap5.put("23", "null");
			hashMap5.put("24", "null");
			hashMap5.put("25", "-1");
			hashMap5.put("26", "false");
			hashMap5.put("27", "0");
			hashMap5.put("28", "null");
			hashMap5.put("29", "null");
			hashMap5.put("30", "null");
			hashMap5.put("31", "null");
			hashMap5.put("32", "1");
			hashMap5.put("33", "Discount");
			hashMap5.put("34", "100");
			hashMap5.put("35", "1");
			hashMap5.put("36", "null");
			hashMap5.put("37", "null");
			hashMap5.put("38", "75");
			hashMap5.put("39", "false");
			hashMap5.put("40", "0");
			hashMap5.put("41", "-1");
			hashMap5.put("42", "1st transaction desc.");
			hashMap5.put("43", "1st transaction");
			hashMap5.put("44", "\"com.swiggy.poc.test1\"");
			hashMap5.put("45", "w2fh4c46d0bszhl89voy");
			hashMap5.put("46", "false");
			hashMap5.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap5.put("48", "ANDROID");
			hashMap5.put("49", "2");
			hashMap5.put("50", "2");
			hashMap5.put("51", "Discount");
			hashMap5.put("52", "10");
			hashMap5.put("53", "1");
			hashMap5.put("54", "null");
			hashMap5.put("55", "null");
			hashMap5.put("56", "90");
			hashMap5.put("57", "false");
			hashMap5.put("58", "0");
			hashMap5.put("59", "-1");
			hashMap5.put("60", "1st transaction desc.");
			hashMap5.put("61", "1st transaction");
			hashMap5.put("62", "\"com.swiggy.poc.test1\"");
			hashMap5.put("63", "w2fh4c46d0bszhl89voy");
			hashMap5.put("64", "false");
			hashMap5.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap5.put("66", "ANDROID");
			hashMap5.put("67", "2");
			hashMap5.put("68", "false");
			hashMap5.put("69", "false");
			hashMap5.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType },
					{ hashMap2, duplicateType }, { hashMap3, duplicateType }, { hashMap4, duplicateType },
					{ hashMap5, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponFreeShippingData")
	public static Object[][] createMultiApplyCouponFreeShippingData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();

			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "true");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "true");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "true");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			hashMap2.put("0", "ALPHACODE");
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "10");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "true");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");

			hashMap3.put("0", "ALPHACODE");
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1519279920000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "10");
			hashMap3.put("35", "1");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Discount");
			hashMap3.put("52", "10");
			hashMap3.put("53", "1");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");
			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType },
					{ hashMap2, duplicateType }, { hashMap3, duplicateType }, };

		}
	}

	@DataProvider(name = "createMultiApplyCouponuserClientData")
	public static Object[][] createMultiApplyCouponUserClient() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();
			HashMap<String, String> hashMap4 = new HashMap<String, String>();
			HashMap<String, String> hashMap5 = new HashMap<String, String>();
			HashMap<String, String> hashMap6 = new HashMap<String, String>();
			HashMap<String, String> hashMap7 = new HashMap<String, String>();
			HashMap<String, String> hashMap8 = new HashMap<String, String>();
			HashMap<String, String> hashMap9 = new HashMap<String, String>();
			HashMap<String, String> hashMap10 = new HashMap<String, String>();
			HashMap<String, String> hashMap11 = new HashMap<String, String>();
			HashMap<String, String> hashMap12 = new HashMap<String, String>();

			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "1");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "1");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			hashMap2.put("0", "ALPHACODE");
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "10");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "2");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "2");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");

			hashMap3.put("0", "ALPHACODE");
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "10");
			hashMap3.put("35", "1");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "3");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Discount");
			hashMap3.put("52", "10");
			hashMap3.put("53", "1");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "3");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");

			hashMap4.put("0", "ALPHACODE");
			hashMap4.put("1", "MULTICOUPON");
			hashMap4.put("2", "Internal");
			hashMap4.put("3", "AUTOMATION");
			hashMap4.put("4", "2");
			hashMap4.put("5", "10");
			hashMap4.put("6", "false");
			hashMap4.put("7", "false");
			hashMap4.put("8", "false");
			hashMap4.put("9", "false");
			hashMap4.put("10", "false");
			hashMap4.put("11", "false");
			hashMap4.put("12", "false");
			hashMap4.put("13", "false");
			hashMap4.put("14", "false");
			hashMap4.put("15", "false");
			hashMap4.put("16", "false");
			hashMap4.put("17", "1518934320000");
			hashMap4.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap4.put("19", "ankita.yadav@swiggy.in");
			hashMap4.put("20", "1518934320000");
			hashMap4.put("21", "1518934320000");
			hashMap4.put("22", "null");
			hashMap4.put("23", "null");
			hashMap4.put("24", "null");
			hashMap4.put("25", "-1");
			hashMap4.put("26", "false");
			hashMap4.put("27", "0");
			hashMap4.put("28", "null");
			hashMap4.put("29", "null");
			hashMap4.put("30", "null");
			hashMap4.put("31", "null");
			hashMap4.put("32", "1");
			hashMap4.put("33", "Discount");
			hashMap4.put("34", "10");
			hashMap4.put("35", "1");
			hashMap4.put("36", "50");
			hashMap4.put("37", "10");
			hashMap4.put("38", "90");
			hashMap4.put("39", "false");
			hashMap4.put("40", "0");
			hashMap4.put("41", "-1");
			hashMap4.put("42", "1st transaction desc.");
			hashMap4.put("43", "1st transaction");
			hashMap4.put("44", "\"com.swiggy.poc.test1\"");
			hashMap4.put("45", "w2fh4c46d0bszhl89voy");
			hashMap4.put("46", "false");
			hashMap4.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap4.put("48", "ANDROID");
			hashMap4.put("49", "2");
			hashMap4.put("50", "2");
			hashMap4.put("51", "Discount");
			hashMap4.put("52", "10");
			hashMap4.put("53", "1");
			hashMap4.put("54", "50");
			hashMap4.put("55", "10");
			hashMap4.put("56", "90");
			hashMap4.put("57", "false");
			hashMap4.put("58", "4");
			hashMap4.put("59", "-1");
			hashMap4.put("60", "1st transaction desc.");
			hashMap4.put("61", "1st transaction");
			hashMap4.put("62", "\"com.swiggy.poc.test1\"");
			hashMap4.put("63", "w2fh4c46d0bszhl89voy");
			hashMap4.put("64", "false");
			hashMap4.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap4.put("66", "ANDROID");
			hashMap4.put("67", "2");
			hashMap4.put("68", "false");
			hashMap4.put("69", "false");
			hashMap4.put("70", "false");

			hashMap5.put("0", "ALPHACODE");
			hashMap5.put("1", "MULTICOUPON");
			hashMap5.put("2", "Internal");
			hashMap5.put("3", "AUTOMATION");
			hashMap5.put("4", "2");
			hashMap5.put("5", "10");
			hashMap5.put("6", "false");
			hashMap5.put("7", "false");
			hashMap5.put("8", "false");
			hashMap5.put("9", "false");
			hashMap5.put("10", "false");
			hashMap5.put("11", "false");
			hashMap5.put("12", "false");
			hashMap5.put("13", "false");
			hashMap5.put("14", "false");
			hashMap5.put("15", "false");
			hashMap5.put("16", "false");
			hashMap5.put("17", "1518934320000");
			hashMap5.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap5.put("19", "ankita.yadav@swiggy.in");
			hashMap5.put("20", "1518934320000");
			hashMap5.put("21", "1518934320000");
			hashMap5.put("22", "null");
			hashMap5.put("23", "null");
			hashMap5.put("24", "null");
			hashMap5.put("25", "-1");
			hashMap5.put("26", "false");
			hashMap5.put("27", "0");
			hashMap5.put("28", "null");
			hashMap5.put("29", "null");
			hashMap5.put("30", "null");
			hashMap5.put("31", "null");
			hashMap5.put("32", "1");
			hashMap5.put("33", "Discount");
			hashMap5.put("34", "10");
			hashMap5.put("35", "1");
			hashMap5.put("36", "50");
			hashMap5.put("37", "10");
			hashMap5.put("38", "90");
			hashMap5.put("39", "false");
			hashMap5.put("40", "1");
			hashMap5.put("41", "-1");
			hashMap5.put("42", "1st transaction desc.");
			hashMap5.put("43", "1st transaction");
			hashMap5.put("44", "\"com.swiggy.poc.test1\"");
			hashMap5.put("45", "w2fh4c46d0bszhl89voy");
			hashMap5.put("46", "false");
			hashMap5.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap5.put("48", "ANDROID");
			hashMap5.put("49", "2");
			hashMap5.put("50", "2");
			hashMap5.put("51", "Discount");
			hashMap5.put("52", "10");
			hashMap5.put("53", "1");
			hashMap5.put("54", "50");
			hashMap5.put("55", "10");
			hashMap5.put("56", "90");
			hashMap5.put("57", "false");
			hashMap5.put("58", "0");
			hashMap5.put("59", "-1");
			hashMap5.put("60", "1st transaction desc.");
			hashMap5.put("61", "1st transaction");
			hashMap5.put("62", "\"com.swiggy.poc.test1\"");
			hashMap5.put("63", "w2fh4c46d0bszhl89voy");
			hashMap5.put("64", "false");
			hashMap5.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap5.put("66", "ANDROID");
			hashMap5.put("67", "2");
			hashMap5.put("68", "false");
			hashMap5.put("69", "false");
			hashMap5.put("70", "false");

			hashMap6.put("0", "ALPHACODE");
			hashMap6.put("1", "MULTICOUPON");
			hashMap6.put("2", "Internal");
			hashMap6.put("3", "AUTOMATION");
			hashMap6.put("4", "2");
			hashMap6.put("5", "10");
			hashMap6.put("6", "false");
			hashMap6.put("7", "false");
			hashMap6.put("8", "false");
			hashMap6.put("9", "false");
			hashMap6.put("10", "false");
			hashMap6.put("11", "false");
			hashMap6.put("12", "false");
			hashMap6.put("13", "false");
			hashMap6.put("14", "false");
			hashMap6.put("15", "false");
			hashMap6.put("16", "false");
			hashMap6.put("17", "1518934320000");
			hashMap6.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap6.put("19", "ankita.yadav@swiggy.in");
			hashMap6.put("20", "1518934320000");
			hashMap6.put("21", "1518934320000");
			hashMap6.put("22", "null");
			hashMap6.put("23", "null");
			hashMap6.put("24", "null");
			hashMap6.put("25", "-1");
			hashMap6.put("26", "false");
			hashMap6.put("27", "0");
			hashMap6.put("28", "null");
			hashMap6.put("29", "null");
			hashMap6.put("30", "null");
			hashMap6.put("31", "null");
			hashMap6.put("32", "1");
			hashMap6.put("33", "Discount");
			hashMap6.put("34", "10");
			hashMap6.put("35", "1");
			hashMap6.put("36", "50");
			hashMap6.put("37", "10");
			hashMap6.put("38", "90");
			hashMap6.put("39", "false");
			hashMap6.put("40", "1");
			hashMap6.put("41", "-1");
			hashMap6.put("42", "1st transaction desc.");
			hashMap6.put("43", "1st transaction");
			hashMap6.put("44", "\"com.swiggy.poc.test1\"");
			hashMap6.put("45", "w2fh4c46d0bszhl89voy");
			hashMap6.put("46", "false");
			hashMap6.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap6.put("48", "ANDROID");
			hashMap6.put("49", "2");
			hashMap6.put("50", "2");
			hashMap6.put("51", "Discount");
			hashMap6.put("52", "10");
			hashMap6.put("53", "1");
			hashMap6.put("54", "50");
			hashMap6.put("55", "10");
			hashMap6.put("56", "90");
			hashMap6.put("57", "false");
			hashMap6.put("58", "2");
			hashMap6.put("59", "-1");
			hashMap6.put("60", "1st transaction desc.");
			hashMap6.put("61", "1st transaction");
			hashMap6.put("62", "\"com.swiggy.poc.test1\"");
			hashMap6.put("63", "w2fh4c46d0bszhl89voy");
			hashMap6.put("64", "false");
			hashMap6.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap6.put("66", "ANDROID");
			hashMap6.put("67", "2");
			hashMap6.put("68", "false");
			hashMap6.put("69", "false");
			hashMap6.put("70", "false");

			hashMap7.put("0", "ALPHACODE");
			hashMap7.put("1", "MULTICOUPON");
			hashMap7.put("2", "Internal");
			hashMap7.put("3", "AUTOMATION");
			hashMap7.put("4", "2");
			hashMap7.put("5", "10");
			hashMap7.put("6", "false");
			hashMap7.put("7", "false");
			hashMap7.put("8", "false");
			hashMap7.put("9", "false");
			hashMap7.put("10", "false");
			hashMap7.put("11", "false");
			hashMap7.put("12", "false");
			hashMap7.put("13", "false");
			hashMap7.put("14", "false");
			hashMap7.put("15", "false");
			hashMap7.put("16", "false");
			hashMap7.put("17", "1518934320000");
			hashMap7.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap7.put("19", "ankita.yadav@swiggy.in");
			hashMap7.put("20", "1518934320000");
			hashMap7.put("21", "1518934320000");
			hashMap7.put("22", "null");
			hashMap7.put("23", "null");
			hashMap7.put("24", "null");
			hashMap7.put("25", "-1");
			hashMap7.put("26", "false");
			hashMap7.put("27", "0");
			hashMap7.put("28", "null");
			hashMap7.put("29", "null");
			hashMap7.put("30", "null");
			hashMap7.put("31", "null");
			hashMap7.put("32", "1");
			hashMap7.put("33", "Discount");
			hashMap7.put("34", "10");
			hashMap7.put("35", "1");
			hashMap7.put("36", "50");
			hashMap7.put("37", "10");
			hashMap7.put("38", "90");
			hashMap7.put("39", "false");
			hashMap7.put("40", "1");
			hashMap7.put("41", "-1");
			hashMap7.put("42", "1st transaction desc.");
			hashMap7.put("43", "1st transaction");
			hashMap7.put("44", "\"com.swiggy.poc.test1\"");
			hashMap7.put("45", "w2fh4c46d0bszhl89voy");
			hashMap7.put("46", "false");
			hashMap7.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap7.put("48", "ANDROID");
			hashMap7.put("49", "2");
			hashMap7.put("50", "2");
			hashMap7.put("51", "Discount");
			hashMap7.put("52", "10");
			hashMap7.put("53", "1");
			hashMap7.put("54", "50");
			hashMap7.put("55", "10");
			hashMap7.put("56", "90");
			hashMap7.put("57", "false");
			hashMap7.put("58", "3");
			hashMap7.put("59", "-1");
			hashMap7.put("60", "1st transaction desc.");
			hashMap7.put("61", "1st transaction");
			hashMap7.put("62", "\"com.swiggy.poc.test1\"");
			hashMap7.put("63", "w2fh4c46d0bszhl89voy");
			hashMap7.put("64", "false");
			hashMap7.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap7.put("66", "ANDROID");
			hashMap7.put("67", "2");
			hashMap7.put("68", "false");
			hashMap7.put("69", "false");
			hashMap7.put("70", "false");

			hashMap8.put("0", "ALPHACODE");
			hashMap8.put("1", "MULTICOUPON");
			hashMap8.put("2", "Internal");
			hashMap8.put("3", "AUTOMATION");
			hashMap8.put("4", "2");
			hashMap8.put("5", "10");
			hashMap8.put("6", "false");
			hashMap8.put("7", "false");
			hashMap8.put("8", "false");
			hashMap8.put("9", "false");
			hashMap8.put("10", "false");
			hashMap8.put("11", "false");
			hashMap8.put("12", "false");
			hashMap8.put("13", "false");
			hashMap8.put("14", "false");
			hashMap8.put("15", "false");
			hashMap8.put("16", "false");
			hashMap8.put("17", "1518934320000");
			hashMap8.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap8.put("19", "ankita.yadav@swiggy.in");
			hashMap8.put("20", "1518934320000");
			hashMap8.put("21", "1518934320000");
			hashMap8.put("22", "null");
			hashMap8.put("23", "null");
			hashMap8.put("24", "null");
			hashMap8.put("25", "-1");
			hashMap8.put("26", "false");
			hashMap8.put("27", "0");
			hashMap8.put("28", "null");
			hashMap8.put("29", "null");
			hashMap8.put("30", "null");
			hashMap8.put("31", "null");
			hashMap8.put("32", "1");
			hashMap8.put("33", "Discount");
			hashMap8.put("34", "10");
			hashMap8.put("35", "1");
			hashMap8.put("36", "50");
			hashMap8.put("37", "10");
			hashMap8.put("38", "90");
			hashMap8.put("39", "false");
			hashMap8.put("40", "1");
			hashMap8.put("41", "-1");
			hashMap8.put("42", "1st transaction desc.");
			hashMap8.put("43", "1st transaction");
			hashMap8.put("44", "\"com.swiggy.poc.test1\"");
			hashMap8.put("45", "w2fh4c46d0bszhl89voy");
			hashMap8.put("46", "false");
			hashMap8.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap8.put("48", "ANDROID");
			hashMap8.put("49", "2");
			hashMap8.put("50", "2");
			hashMap8.put("51", "Discount");
			hashMap8.put("52", "10");
			hashMap8.put("53", "1");
			hashMap8.put("54", "50");
			hashMap8.put("55", "10");
			hashMap8.put("56", "90");
			hashMap8.put("57", "false");
			hashMap8.put("58", "4");
			hashMap8.put("59", "-1");
			hashMap8.put("60", "1st transaction desc.");
			hashMap8.put("61", "1st transaction");
			hashMap8.put("62", "\"com.swiggy.poc.test1\"");
			hashMap8.put("63", "w2fh4c46d0bszhl89voy");
			hashMap8.put("64", "false");
			hashMap8.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap8.put("66", "ANDROID");
			hashMap8.put("67", "2");
			hashMap8.put("68", "false");
			hashMap8.put("69", "false");
			hashMap8.put("70", "false");

			hashMap9.put("0", "ALPHACODE");
			hashMap9.put("1", "MULTICOUPON");
			hashMap9.put("2", "Internal");
			hashMap9.put("3", "AUTOMATION");
			hashMap9.put("4", "2");
			hashMap9.put("5", "10");
			hashMap9.put("6", "false");
			hashMap9.put("7", "false");
			hashMap9.put("8", "false");
			hashMap9.put("9", "false");
			hashMap9.put("10", "false");
			hashMap9.put("11", "false");
			hashMap9.put("12", "false");
			hashMap9.put("13", "false");
			hashMap9.put("14", "false");
			hashMap9.put("15", "false");
			hashMap9.put("16", "false");
			hashMap9.put("17", "1518934320000");
			hashMap9.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap9.put("19", "ankita.yadav@swiggy.in");
			hashMap9.put("20", "1518934320000");
			hashMap9.put("21", "1518934320000");
			hashMap9.put("22", "null");
			hashMap9.put("23", "null");
			hashMap9.put("24", "null");
			hashMap9.put("25", "-1");
			hashMap9.put("26", "false");
			hashMap9.put("27", "0");
			hashMap9.put("28", "null");
			hashMap9.put("29", "null");
			hashMap9.put("30", "null");
			hashMap9.put("31", "null");
			hashMap9.put("32", "1");
			hashMap9.put("33", "Discount");
			hashMap9.put("34", "10");
			hashMap9.put("35", "1");
			hashMap9.put("36", "50");
			hashMap9.put("37", "10");
			hashMap9.put("38", "90");
			hashMap9.put("39", "false");
			hashMap9.put("40", "2");
			hashMap9.put("41", "-1");
			hashMap9.put("42", "1st transaction desc.");
			hashMap9.put("43", "1st transaction");
			hashMap9.put("44", "\"com.swiggy.poc.test1\"");
			hashMap9.put("45", "w2fh4c46d0bszhl89voy");
			hashMap9.put("46", "false");
			hashMap9.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap9.put("48", "ANDROID");
			hashMap9.put("49", "2");
			hashMap9.put("50", "2");
			hashMap9.put("51", "Discount");
			hashMap9.put("52", "10");
			hashMap9.put("53", "1");
			hashMap9.put("54", "50");
			hashMap9.put("55", "10");
			hashMap9.put("56", "90");
			hashMap9.put("57", "false");
			hashMap9.put("58", "0");
			hashMap9.put("59", "-1");
			hashMap9.put("60", "1st transaction desc.");
			hashMap9.put("61", "1st transaction");
			hashMap9.put("62", "\"com.swiggy.poc.test1\"");
			hashMap9.put("63", "w2fh4c46d0bszhl89voy");
			hashMap9.put("64", "false");
			hashMap9.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap9.put("66", "ANDROID");
			hashMap9.put("67", "2");
			hashMap9.put("68", "false");
			hashMap9.put("69", "false");
			hashMap9.put("70", "false");

			hashMap10.put("0", "ALPHACODE");
			hashMap10.put("1", "MULTICOUPON");
			hashMap10.put("2", "Internal");
			hashMap10.put("3", "AUTOMATION");
			hashMap10.put("4", "2");
			hashMap10.put("5", "10");
			hashMap10.put("6", "false");
			hashMap10.put("7", "false");
			hashMap10.put("8", "false");
			hashMap10.put("9", "false");
			hashMap10.put("10", "false");
			hashMap10.put("11", "false");
			hashMap10.put("12", "false");
			hashMap10.put("13", "false");
			hashMap10.put("14", "false");
			hashMap10.put("15", "false");
			hashMap10.put("16", "false");
			hashMap10.put("17", "1518934320000");
			hashMap10.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap10.put("19", "ankita.yadav@swiggy.in");
			hashMap10.put("20", "1518934320000");
			hashMap10.put("21", "1518934320000");
			hashMap10.put("22", "null");
			hashMap10.put("23", "null");
			hashMap10.put("24", "null");
			hashMap10.put("25", "-1");
			hashMap10.put("26", "false");
			hashMap10.put("27", "0");
			hashMap10.put("28", "null");
			hashMap10.put("29", "null");
			hashMap10.put("30", "null");
			hashMap10.put("31", "null");
			hashMap10.put("32", "1");
			hashMap10.put("33", "Discount");
			hashMap10.put("34", "10");
			hashMap10.put("35", "1");
			hashMap10.put("36", "50");
			hashMap10.put("37", "10");
			hashMap10.put("38", "90");
			hashMap10.put("39", "false");
			hashMap10.put("40", "2");
			hashMap10.put("41", "-1");
			hashMap10.put("42", "1st transaction desc.");
			hashMap10.put("43", "1st transaction");
			hashMap10.put("44", "\"com.swiggy.poc.test1\"");
			hashMap10.put("45", "w2fh4c46d0bszhl89voy");
			hashMap10.put("46", "false");
			hashMap10.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap10.put("48", "ANDROID");
			hashMap10.put("49", "2");
			hashMap10.put("50", "2");
			hashMap10.put("51", "Discount");
			hashMap10.put("52", "10");
			hashMap10.put("53", "1");
			hashMap10.put("54", "50");
			hashMap10.put("55", "10");
			hashMap10.put("56", "90");
			hashMap10.put("57", "false");
			hashMap10.put("58", "1");
			hashMap10.put("59", "-1");
			hashMap10.put("60", "1st transaction desc.");
			hashMap10.put("61", "1st transaction");
			hashMap10.put("62", "\"com.swiggy.poc.test1\"");
			hashMap10.put("63", "w2fh4c46d0bszhl89voy");
			hashMap10.put("64", "false");
			hashMap10.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap10.put("66", "ANDROID");
			hashMap10.put("67", "2");
			hashMap10.put("68", "false");
			hashMap10.put("69", "false");
			hashMap10.put("70", "false");

			hashMap11.put("0", "ALPHACODE");
			hashMap11.put("1", "MULTICOUPON");
			hashMap11.put("2", "Internal");
			hashMap11.put("3", "AUTOMATION");
			hashMap11.put("4", "2");
			hashMap11.put("5", "10");
			hashMap11.put("6", "false");
			hashMap11.put("7", "false");
			hashMap11.put("8", "false");
			hashMap11.put("9", "false");
			hashMap11.put("10", "false");
			hashMap11.put("11", "false");
			hashMap11.put("12", "false");
			hashMap11.put("13", "false");
			hashMap11.put("14", "false");
			hashMap11.put("15", "false");
			hashMap11.put("16", "false");
			hashMap11.put("17", "1518934320000");
			hashMap11.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap11.put("19", "ankita.yadav@swiggy.in");
			hashMap11.put("20", "1518934320000");
			hashMap11.put("21", "1518934320000");
			hashMap11.put("22", "null");
			hashMap11.put("23", "null");
			hashMap11.put("24", "null");
			hashMap11.put("25", "-1");
			hashMap11.put("26", "false");
			hashMap11.put("27", "0");
			hashMap11.put("28", "null");
			hashMap11.put("29", "null");
			hashMap11.put("30", "null");
			hashMap11.put("31", "null");
			hashMap11.put("32", "1");
			hashMap11.put("33", "Discount");
			hashMap11.put("34", "10");
			hashMap11.put("35", "1");
			hashMap11.put("36", "50");
			hashMap11.put("37", "10");
			hashMap11.put("38", "90");
			hashMap11.put("39", "false");
			hashMap11.put("40", "2");
			hashMap11.put("41", "-1");
			hashMap11.put("42", "1st transaction desc.");
			hashMap11.put("43", "1st transaction");
			hashMap11.put("44", "\"com.swiggy.poc.test1\"");
			hashMap11.put("45", "w2fh4c46d0bszhl89voy");
			hashMap11.put("46", "false");
			hashMap11.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap11.put("48", "ANDROID");
			hashMap11.put("49", "2");
			hashMap11.put("50", "2");
			hashMap11.put("51", "Discount");
			hashMap11.put("52", "10");
			hashMap11.put("53", "1");
			hashMap11.put("54", "50");
			hashMap11.put("55", "10");
			hashMap11.put("56", "90");
			hashMap11.put("57", "false");
			hashMap11.put("58", "3");
			hashMap11.put("59", "-1");
			hashMap11.put("60", "1st transaction desc.");
			hashMap11.put("61", "1st transaction");
			hashMap11.put("62", "\"com.swiggy.poc.test1\"");
			hashMap11.put("63", "w2fh4c46d0bszhl89voy");
			hashMap11.put("64", "false");
			hashMap11.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap11.put("66", "ANDROID");
			hashMap11.put("67", "2");
			hashMap11.put("68", "false");
			hashMap11.put("69", "false");
			hashMap11.put("70", "false");

			hashMap12.put("0", "ALPHACODE");
			hashMap12.put("1", "MULTICOUPON");
			hashMap12.put("2", "Internal");
			hashMap12.put("3", "AUTOMATION");
			hashMap12.put("4", "2");
			hashMap12.put("5", "10");
			hashMap12.put("6", "false");
			hashMap12.put("7", "false");
			hashMap12.put("8", "false");
			hashMap12.put("9", "false");
			hashMap12.put("10", "false");
			hashMap12.put("11", "false");
			hashMap12.put("12", "false");
			hashMap12.put("13", "false");
			hashMap12.put("14", "false");
			hashMap12.put("15", "false");
			hashMap12.put("16", "false");
			hashMap12.put("17", "1518934320000");
			hashMap12.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap12.put("19", "ankita.yadav@swiggy.in");
			hashMap12.put("20", "1518934320000");
			hashMap12.put("21", "1518934320000");
			hashMap12.put("22", "null");
			hashMap12.put("23", "null");
			hashMap12.put("24", "null");
			hashMap12.put("25", "-1");
			hashMap12.put("26", "false");
			hashMap12.put("27", "0");
			hashMap12.put("28", "null");
			hashMap12.put("29", "null");
			hashMap12.put("30", "null");
			hashMap12.put("31", "null");
			hashMap12.put("32", "1");
			hashMap12.put("33", "Discount");
			hashMap12.put("34", "10");
			hashMap12.put("35", "1");
			hashMap12.put("36", "50");
			hashMap12.put("37", "10");
			hashMap12.put("38", "90");
			hashMap12.put("39", "false");
			hashMap12.put("40", "2");
			hashMap12.put("41", "-1");
			hashMap12.put("42", "1st transaction desc.");
			hashMap12.put("43", "1st transaction");
			hashMap12.put("44", "\"com.swiggy.poc.test1\"");
			hashMap12.put("45", "w2fh4c46d0bszhl89voy");
			hashMap12.put("46", "false");
			hashMap12.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap12.put("48", "ANDROID");
			hashMap12.put("49", "2");
			hashMap12.put("50", "2");
			hashMap12.put("51", "Discount");
			hashMap12.put("52", "10");
			hashMap12.put("53", "1");
			hashMap12.put("54", "50");
			hashMap12.put("55", "10");
			hashMap12.put("56", "90");
			hashMap12.put("57", "false");
			hashMap12.put("58", "4");
			hashMap12.put("59", "-1");
			hashMap12.put("60", "1st transaction desc.");
			hashMap12.put("61", "1st transaction");
			hashMap12.put("62", "\"com.swiggy.poc.test1\"");
			hashMap12.put("63", "w2fh4c46d0bszhl89voy");
			hashMap12.put("64", "false");
			hashMap12.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap12.put("66", "ANDROID");
			hashMap12.put("67", "2");
			hashMap12.put("68", "false");
			hashMap12.put("69", "false");
			hashMap12.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType },
					{ hashMap2, duplicateType }, { hashMap3, duplicateType }, { hashMap4, duplicateType },
					{ hashMap5, duplicateType }, { hashMap6, duplicateType }, { hashMap7, duplicateType },
					{ hashMap8, duplicateType }, { hashMap9, duplicateType }, { hashMap10, duplicateType },
					{ hashMap11, duplicateType }, { hashMap12, duplicateType }, };

		}
	}

	@DataProvider(name = "createMultiApplyCouponDiscountItemData")
	public static Object[][] createMultiApplyCouponDiscountItemData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();

			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "0");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "0");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			hashMap2.put("0", "ALPHACODE");
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "10");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "0");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");

			hashMap3.put("0", "ALPHACODE");
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1519279920000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "10");
			hashMap3.put("35", "1");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "0");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Discount");
			hashMap3.put("52", "10");
			hashMap3.put("53", "1");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType },
					{ hashMap2, duplicateType }, { hashMap3, duplicateType }, };

		}
	}

	@DataProvider(name = "createMultiApplyCouponInvalidDiscountItemData")
	public static Object[][] createMultiApplyCouponInvalidDiscountItemData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "null");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "null");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponDescriptionData")
	public static Object[][] createMultiApplyCouponDescriptionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction Test Description");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "2nd transaction Test Description");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponInvalidDescriptionData")
	public static Object[][] createMultiApplyCouponInvalidDescriptionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponTitleData")
	public static Object[][] createMultiApplyCouponTitleData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction TITLE data");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "2nd transaction TITLE data");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponInvalidTitleData")
	public static Object[][] createMultiApplyCouponInvalidTitleData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "");
			hashMap.put("44", "\"com.swiggy.poc.test1\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "");
			hashMap.put("62", "\"com.swiggy.poc.test1\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponTermsAndConditionData")
	public static Object[][] createMultiApplyCouponTermsAndConditionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"com.swiggy.poc.test1\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponInvalidTermsAndConditionData")
	public static Object[][] createMultiApplyCouponInvalidTermsAndConditionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponLogoIdData")
	public static Object[][] createMultiApplyCouponLogoIdData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";
			// Swiggy Logo id (Default)
			// hashMap.put("0", "ALPHACODE");
			// hashMap.put("1", "MULTICOUPON");
			// hashMap.put("2", "Internal");
			// hashMap.put("3", "AUTOMATION");
			// hashMap.put("4", "2");
			// hashMap.put("5", "10");
			// hashMap.put("6", "false");
			// hashMap.put("7", "false");
			// hashMap.put("8", "false");
			// hashMap.put("9", "false");
			// hashMap.put("10", "false");
			// hashMap.put("11", "false");
			// hashMap.put("12", "false");
			// hashMap.put("13", "false");
			// hashMap.put("14", "false");
			// hashMap.put("15", "false");
			// hashMap.put("16", "false");
			// hashMap.put("17", "1518934320000");
			// hashMap.put("18", "1519279920000");
			// hashMap.put("19", "ankita.yadav@swiggy.in");
			// hashMap.put("20", "1518934320000");
			// hashMap.put("21", "1518934320000");
			// hashMap.put("22", "null");
			// hashMap.put("23", "null");
			// hashMap.put("24", "null");
			// hashMap.put("25", "-1");
			// hashMap.put("26", "false");
			// hashMap.put("27", "0");
			// hashMap.put("28", "null");
			// hashMap.put("29", "null");
			// hashMap.put("30", "null");
			// hashMap.put("31", "null");
			// hashMap.put("32", "1");
			// hashMap.put("33", "Discount");
			// hashMap.put("34", "10");
			// hashMap.put("35", "1");
			// hashMap.put("36", "50");
			// hashMap.put("37", "10");
			// hashMap.put("38", "90");
			// hashMap.put("39", "false");
			// hashMap.put("40", "0");
			// hashMap.put("41", "-1");
			// hashMap.put("42", "1st transaction desc.");
			// hashMap.put("43", "1st transaction");
			// hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("45", "w2fh4c46d0bszhl89voy");
			// hashMap.put("46", "false");
			// hashMap.put("47",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("48", "ANDROID");
			// hashMap.put("49", "2");
			// hashMap.put("50", "2");
			// hashMap.put("51", "Discount");
			// hashMap.put("52", "10");
			// hashMap.put("53", "1");
			// hashMap.put("54", "50");
			// hashMap.put("55", "10");
			// hashMap.put("56", "90");
			// hashMap.put("57", "false");
			// hashMap.put("58", "0");
			// hashMap.put("59", "-1");
			// hashMap.put("60", "1st transaction desc.");
			// hashMap.put("61", "1st transaction");
			// hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("63", "w2fh4c46d0bszhl89voy");
			// hashMap.put("64", "false");
			// hashMap.put("65",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("66", "ANDROID");
			// hashMap.put("67", "2");
			// hashMap.put("68", "false");
			// hashMap.put("69", "false");
			// hashMap.put("70", "false");
			//
			// Standard Chartered logo id
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "yq4s58hrz1wa4ktv3udb");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// mobikwik
			// hashMap.put("0", "ALPHACODE");
			// hashMap.put("1", "MULTICOUPON");
			// hashMap.put("2", "Internal");
			// hashMap.put("3", "AUTOMATION");
			// hashMap.put("4", "2");
			// hashMap.put("5", "10");
			// hashMap.put("6", "false");
			// hashMap.put("7", "false");
			// hashMap.put("8", "false");
			// hashMap.put("9", "false");
			// hashMap.put("10", "false");
			// hashMap.put("11", "false");
			// hashMap.put("12", "false");
			// hashMap.put("13", "false");
			// hashMap.put("14", "false");
			// hashMap.put("15", "false");
			// hashMap.put("16", "false");
			// hashMap.put("17", "1518934320000");
			// hashMap.put("18", "1519279920000");
			// hashMap.put("19", "ankita.yadav@swiggy.in");
			// hashMap.put("20", "1518934320000");
			// hashMap.put("21", "1518934320000");
			// hashMap.put("22", "null");
			// hashMap.put("23", "null");
			// hashMap.put("24", "null");
			// hashMap.put("25", "-1");
			// hashMap.put("26", "false");
			// hashMap.put("27", "0");
			// hashMap.put("28", "null");
			// hashMap.put("29", "null");
			// hashMap.put("30", "null");
			// hashMap.put("31", "null");
			// hashMap.put("32", "1");
			// hashMap.put("33", "Discount");
			// hashMap.put("34", "10");
			// hashMap.put("35", "1");
			// hashMap.put("36", "50");
			// hashMap.put("37", "10");
			// hashMap.put("38", "90");
			// hashMap.put("39", "false");
			// hashMap.put("40", "0");
			// hashMap.put("41", "-1");
			// hashMap.put("42", "1st transaction desc.");
			// hashMap.put("43", "1st transaction");
			// hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("45", "uumd7ddmumm6o00ac1mk");
			// hashMap.put("46", "false");
			// hashMap.put("47",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("48", "ANDROID");
			// hashMap.put("49", "2");
			// hashMap.put("50", "2");
			// hashMap.put("51", "Discount");
			// hashMap.put("52", "10");
			// hashMap.put("53", "1");
			// hashMap.put("54", "50");
			// hashMap.put("55", "10");
			// hashMap.put("56", "90");
			// hashMap.put("57", "false");
			// hashMap.put("58", "0");
			// hashMap.put("59", "-1");
			// hashMap.put("60", "1st transaction desc.");
			// hashMap.put("61", "1st transaction");
			// hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("63", "yq4s58hrz1wa4ktv3udb");
			// hashMap.put("64", "false");
			// hashMap.put("65",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("66", "ANDROID");
			// hashMap.put("67", "2");
			// hashMap.put("68", "false");
			// hashMap.put("69", "false");
			// hashMap.put("70", "false");
			//
			// //FreeCharge logo id
			// hashMap.put("0", "ALPHACODE");
			// hashMap.put("1", "MULTICOUPON");
			// hashMap.put("2", "Internal");
			// hashMap.put("3", "AUTOMATION");
			// hashMap.put("4", "2");
			// hashMap.put("5", "10");
			// hashMap.put("6", "false");
			// hashMap.put("7", "false");
			// hashMap.put("8", "false");
			// hashMap.put("9", "false");
			// hashMap.put("10", "false");
			// hashMap.put("11", "false");
			// hashMap.put("12", "false");
			// hashMap.put("13", "false");
			// hashMap.put("14", "false");
			// hashMap.put("15", "false");
			// hashMap.put("16", "false");
			// hashMap.put("17", "1518934320000");
			// hashMap.put("18", "1519279920000");
			// hashMap.put("19", "ankita.yadav@swiggy.in");
			// hashMap.put("20", "1518934320000");
			// hashMap.put("21", "1518934320000");
			// hashMap.put("22", "null");
			// hashMap.put("23", "null");
			// hashMap.put("24", "null");
			// hashMap.put("25", "-1");
			// hashMap.put("26", "false");
			// hashMap.put("27", "0");
			// hashMap.put("28", "null");
			// hashMap.put("29", "null");
			// hashMap.put("30", "null");
			// hashMap.put("31", "null");
			// hashMap.put("32", "1");
			// hashMap.put("33", "Discount");
			// hashMap.put("34", "10");
			// hashMap.put("35", "1");
			// hashMap.put("36", "50");
			// hashMap.put("37", "10");
			// hashMap.put("38", "90");
			// hashMap.put("39", "false");
			// hashMap.put("40", "0");
			// hashMap.put("41", "-1");
			// hashMap.put("42", "1st transaction desc.");
			// hashMap.put("43", "1st transaction");
			// hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("45", "uumd7ddmumm6o00ac1mk");
			// hashMap.put("46", "false");
			// hashMap.put("47",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("48", "ANDROID");
			// hashMap.put("49", "2");
			// hashMap.put("50", "2");
			// hashMap.put("51", "Discount");
			// hashMap.put("52", "10");
			// hashMap.put("53", "1");
			// hashMap.put("54", "50");
			// hashMap.put("55", "10");
			// hashMap.put("56", "90");
			// hashMap.put("57", "false");
			// hashMap.put("58", "0");
			// hashMap.put("59", "-1");
			// hashMap.put("60", "1st transaction desc.");
			// hashMap.put("61", "1st transaction");
			// hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("63", "fxefc4yntssypnrgqarn");
			// hashMap.put("64", "false");
			// hashMap.put("65",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("66", "ANDROID");
			// hashMap.put("67", "2");
			// hashMap.put("68", "false");
			// hashMap.put("69", "false");
			// hashMap.put("70", "false");
			//
			// //Phone Pay logo id
			// hashMap.put("0", "ALPHACODE");
			// hashMap.put("1", "MULTICOUPON");
			// hashMap.put("2", "Internal");
			// hashMap.put("3", "AUTOMATION");
			// hashMap.put("4", "2");
			// hashMap.put("5", "10");
			// hashMap.put("6", "false");
			// hashMap.put("7", "false");
			// hashMap.put("8", "false");
			// hashMap.put("9", "false");
			// hashMap.put("10", "false");
			// hashMap.put("11", "false");
			// hashMap.put("12", "false");
			// hashMap.put("13", "false");
			// hashMap.put("14", "false");
			// hashMap.put("15", "false");
			// hashMap.put("16", "false");
			// hashMap.put("17", "1518934320000");
			// hashMap.put("18", "1519279920000");
			// hashMap.put("19", "ankita.yadav@swiggy.in");
			// hashMap.put("20", "1518934320000");
			// hashMap.put("21", "1518934320000");
			// hashMap.put("22", "null");
			// hashMap.put("23", "null");
			// hashMap.put("24", "null");
			// hashMap.put("25", "-1");
			// hashMap.put("26", "false");
			// hashMap.put("27", "0");
			// hashMap.put("28", "null");
			// hashMap.put("29", "null");
			// hashMap.put("30", "null");
			// hashMap.put("31", "null");
			// hashMap.put("32", "1");
			// hashMap.put("33", "Discount");
			// hashMap.put("34", "10");
			// hashMap.put("35", "1");
			// hashMap.put("36", "50");
			// hashMap.put("37", "10");
			// hashMap.put("38", "90");
			// hashMap.put("39", "false");
			// hashMap.put("40", "0");
			// hashMap.put("41", "-1");
			// hashMap.put("42", "1st transaction desc.");
			// hashMap.put("43", "1st transaction");
			// hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("45", "guq23clnz3ivaiu7ztsa");
			// hashMap.put("46", "false");
			// hashMap.put("47",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("48", "ANDROID");
			// hashMap.put("49", "2");
			// hashMap.put("50", "2");
			// hashMap.put("51", "Discount");
			// hashMap.put("52", "10");
			// hashMap.put("53", "1");
			// hashMap.put("54", "50");
			// hashMap.put("55", "10");
			// hashMap.put("56", "90");
			// hashMap.put("57", "false");
			// hashMap.put("58", "0");
			// hashMap.put("59", "-1");
			// hashMap.put("60", "1st transaction desc.");
			// hashMap.put("61", "1st transaction");
			// hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("63", "fxefc4yntssypnrgqarn");
			// hashMap.put("64", "false");
			// hashMap.put("65",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("66", "ANDROID");
			// hashMap.put("67", "2");
			// hashMap.put("68", "false");
			// hashMap.put("69", "false");
			// hashMap.put("70", "false");
			//
			// //lazy Pay logo id
			// hashMap.put("0", "ALPHACODE");
			// hashMap.put("1", "MULTICOUPON");
			// hashMap.put("2", "Internal");
			// hashMap.put("3", "AUTOMATION");
			// hashMap.put("4", "2");
			// hashMap.put("5", "10");
			// hashMap.put("6", "false");
			// hashMap.put("7", "false");
			// hashMap.put("8", "false");
			// hashMap.put("9", "false");
			// hashMap.put("10", "false");
			// hashMap.put("11", "false");
			// hashMap.put("12", "false");
			// hashMap.put("13", "false");
			// hashMap.put("14", "false");
			// hashMap.put("15", "false");
			// hashMap.put("16", "false");
			// hashMap.put("17", "1518934320000");
			// hashMap.put("18", "1519279920000");
			// hashMap.put("19", "ankita.yadav@swiggy.in");
			// hashMap.put("20", "1518934320000");
			// hashMap.put("21", "1518934320000");
			// hashMap.put("22", "null");
			// hashMap.put("23", "null");
			// hashMap.put("24", "null");
			// hashMap.put("25", "-1");
			// hashMap.put("26", "false");
			// hashMap.put("27", "0");
			// hashMap.put("28", "null");
			// hashMap.put("29", "null");
			// hashMap.put("30", "null");
			// hashMap.put("31", "null");
			// hashMap.put("32", "1");
			// hashMap.put("33", "Discount");
			// hashMap.put("34", "10");
			// hashMap.put("35", "1");
			// hashMap.put("36", "50");
			// hashMap.put("37", "10");
			// hashMap.put("38", "90");
			// hashMap.put("39", "false");
			// hashMap.put("40", "0");
			// hashMap.put("41", "-1");
			// hashMap.put("42", "1st transaction desc.");
			// hashMap.put("43", "1st transaction");
			// hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("45", "guq23clnz3ivaiu7ztsa");
			// hashMap.put("46", "false");
			// hashMap.put("47",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("48", "ANDROID");
			// hashMap.put("49", "2");
			// hashMap.put("50", "2");
			// hashMap.put("51", "Discount");
			// hashMap.put("52", "10");
			// hashMap.put("53", "1");
			// hashMap.put("54", "50");
			// hashMap.put("55", "10");
			// hashMap.put("56", "90");
			// hashMap.put("57", "false");
			// hashMap.put("58", "0");
			// hashMap.put("59", "-1");
			// hashMap.put("60", "1st transaction desc.");
			// hashMap.put("61", "1st transaction");
			// hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("63", "kj4khfwj6knnkskw4df3");
			// hashMap.put("64", "false");
			// hashMap.put("65",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("66", "ANDROID");
			// hashMap.put("67", "2");
			// hashMap.put("68", "false");
			// hashMap.put("69", "false");
			// hashMap.put("70", "false");
			//
			//
			// //Kotak logo id
			// hashMap.put("0", "ALPHACODE");
			// hashMap.put("1", "MULTICOUPON");
			// hashMap.put("2", "Internal");
			// hashMap.put("3", "AUTOMATION");
			// hashMap.put("4", "2");
			// hashMap.put("5", "10");
			// hashMap.put("6", "false");
			// hashMap.put("7", "false");
			// hashMap.put("8", "false");
			// hashMap.put("9", "false");
			// hashMap.put("10", "false");
			// hashMap.put("11", "false");
			// hashMap.put("12", "false");
			// hashMap.put("13", "false");
			// hashMap.put("14", "false");
			// hashMap.put("15", "false");
			// hashMap.put("16", "false");
			// hashMap.put("17", "1518934320000");
			// hashMap.put("18", "1519279920000");
			// hashMap.put("19", "ankita.yadav@swiggy.in");
			// hashMap.put("20", "1518934320000");
			// hashMap.put("21", "1518934320000");
			// hashMap.put("22", "null");
			// hashMap.put("23", "null");
			// hashMap.put("24", "null");
			// hashMap.put("25", "-1");
			// hashMap.put("26", "false");
			// hashMap.put("27", "0");
			// hashMap.put("28", "null");
			// hashMap.put("29", "null");
			// hashMap.put("30", "null");
			// hashMap.put("31", "null");
			// hashMap.put("32", "1");
			// hashMap.put("33", "Discount");
			// hashMap.put("34", "10");
			// hashMap.put("35", "1");
			// hashMap.put("36", "50");
			// hashMap.put("37", "10");
			// hashMap.put("38", "90");
			// hashMap.put("39", "false");
			// hashMap.put("40", "0");
			// hashMap.put("41", "-1");
			// hashMap.put("42", "1st transaction desc.");
			// hashMap.put("43", "1st transaction");
			// hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("45", "vspvwgmfdm7olpyoiwxa");
			// hashMap.put("46", "false");
			// hashMap.put("47",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("48", "ANDROID");
			// hashMap.put("49", "2");
			// hashMap.put("50", "2");
			// hashMap.put("51", "Discount");
			// hashMap.put("52", "10");
			// hashMap.put("53", "1");
			// hashMap.put("54", "50");
			// hashMap.put("55", "10");
			// hashMap.put("56", "90");
			// hashMap.put("57", "false");
			// hashMap.put("58", "0");
			// hashMap.put("59", "-1");
			// hashMap.put("60", "1st transaction desc.");
			// hashMap.put("61", "1st transaction");
			// hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("63", "kj4khfwj6knnkskw4df3");
			// hashMap.put("64", "false");
			// hashMap.put("65",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("66", "ANDROID");
			// hashMap.put("67", "2");
			// hashMap.put("68", "false");
			// hashMap.put("69", "false");
			// hashMap.put("70", "false");
			//
			// //American express logo id
			// hashMap.put("0", "ALPHACODE");
			// hashMap.put("1", "MULTICOUPON");
			// hashMap.put("2", "Internal");
			// hashMap.put("3", "AUTOMATION");
			// hashMap.put("4", "2");
			// hashMap.put("5", "10");
			// hashMap.put("6", "false");
			// hashMap.put("7", "false");
			// hashMap.put("8", "false");
			// hashMap.put("9", "false");
			// hashMap.put("10", "false");
			// hashMap.put("11", "false");
			// hashMap.put("12", "false");
			// hashMap.put("13", "false");
			// hashMap.put("14", "false");
			// hashMap.put("15", "false");
			// hashMap.put("16", "false");
			// hashMap.put("17", "1518934320000");
			// hashMap.put("18", "1519279920000");
			// hashMap.put("19", "ankita.yadav@swiggy.in");
			// hashMap.put("20", "1518934320000");
			// hashMap.put("21", "1518934320000");
			// hashMap.put("22", "null");
			// hashMap.put("23", "null");
			// hashMap.put("24", "null");
			// hashMap.put("25", "-1");
			// hashMap.put("26", "false");
			// hashMap.put("27", "0");
			// hashMap.put("28", "null");
			// hashMap.put("29", "null");
			// hashMap.put("30", "null");
			// hashMap.put("31", "null");
			// hashMap.put("32", "1");
			// hashMap.put("33", "Discount");
			// hashMap.put("34", "10");
			// hashMap.put("35", "1");
			// hashMap.put("36", "50");
			// hashMap.put("37", "10");
			// hashMap.put("38", "90");
			// hashMap.put("39", "false");
			// hashMap.put("40", "0");
			// hashMap.put("41", "-1");
			// hashMap.put("42", "1st transaction desc.");
			// hashMap.put("43", "1st transaction");
			// hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("45", "vspvwgmfdm7olpyoiwxa");
			// hashMap.put("46", "false");
			// hashMap.put("47",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("48", "ANDROID");
			// hashMap.put("49", "2");
			// hashMap.put("50", "2");
			// hashMap.put("51", "Discount");
			// hashMap.put("52", "10");
			// hashMap.put("53", "1");
			// hashMap.put("54", "50");
			// hashMap.put("55", "10");
			// hashMap.put("56", "90");
			// hashMap.put("57", "false");
			// hashMap.put("58", "0");
			// hashMap.put("59", "-1");
			// hashMap.put("60", "1st transaction desc.");
			// hashMap.put("61", "1st transaction");
			// hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("63", "qdl1kmsddzoswwhlcosg");
			// hashMap.put("64", "false");
			// hashMap.put("65",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("66", "ANDROID");
			// hashMap.put("67", "2");
			// hashMap.put("68", "false");
			// hashMap.put("69", "false");
			// hashMap.put("70", "false");
			//
			// //Sodexo logo id
			//
			// hashMap.put("0", "ALPHACODE");
			// hashMap.put("1", "MULTICOUPON");
			// hashMap.put("2", "Internal");
			// hashMap.put("3", "AUTOMATION");
			// hashMap.put("4", "2");
			// hashMap.put("5", "10");
			// hashMap.put("6", "false");
			// hashMap.put("7", "false");
			// hashMap.put("8", "false");
			// hashMap.put("9", "false");
			// hashMap.put("10", "false");
			// hashMap.put("11", "false");
			// hashMap.put("12", "false");
			// hashMap.put("13", "false");
			// hashMap.put("14", "false");
			// hashMap.put("15", "false");
			// hashMap.put("16", "false");
			// hashMap.put("17", "1518934320000");
			// hashMap.put("18", "1519279920000");
			// hashMap.put("19", "ankita.yadav@swiggy.in");
			// hashMap.put("20", "1518934320000");
			// hashMap.put("21", "1518934320000");
			// hashMap.put("22", "null");
			// hashMap.put("23", "null");
			// hashMap.put("24", "null");
			// hashMap.put("25", "-1");
			// hashMap.put("26", "false");
			// hashMap.put("27", "0");
			// hashMap.put("28", "null");
			// hashMap.put("29", "null");
			// hashMap.put("30", "null");
			// hashMap.put("31", "null");
			// hashMap.put("32", "1");
			// hashMap.put("33", "Discount");
			// hashMap.put("34", "10");
			// hashMap.put("35", "1");
			// hashMap.put("36", "50");
			// hashMap.put("37", "10");
			// hashMap.put("38", "90");
			// hashMap.put("39", "false");
			// hashMap.put("40", "0");
			// hashMap.put("41", "-1");
			// hashMap.put("42", "1st transaction desc.");
			// hashMap.put("43", "1st transaction");
			// hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("45", "lq8egr0tyay6vykis5rg");
			// hashMap.put("46", "false");
			// hashMap.put("47",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("48", "ANDROID");
			// hashMap.put("49", "2");
			// hashMap.put("50", "2");
			// hashMap.put("51", "Discount");
			// hashMap.put("52", "10");
			// hashMap.put("53", "1");
			// hashMap.put("54", "50");
			// hashMap.put("55", "10");
			// hashMap.put("56", "90");
			// hashMap.put("57", "false");
			// hashMap.put("58", "0");
			// hashMap.put("59", "-1");
			// hashMap.put("60", "1st transaction desc.");
			// hashMap.put("61", "1st transaction");
			// hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("63", "qdl1kmsddzoswwhlcosg");
			// hashMap.put("64", "false");
			// hashMap.put("65",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("66", "ANDROID");
			// hashMap.put("67", "2");
			// hashMap.put("68", "false");
			// hashMap.put("69", "false");
			// hashMap.put("70", "false");
			//
			// //PayTM logo id
			// hashMap.put("0", "ALPHACODE");
			// hashMap.put("1", "MULTICOUPON");
			// hashMap.put("2", "Internal");
			// hashMap.put("3", "AUTOMATION");
			// hashMap.put("4", "2");
			// hashMap.put("5", "10");
			// hashMap.put("6", "false");
			// hashMap.put("7", "false");
			// hashMap.put("8", "false");
			// hashMap.put("9", "false");
			// hashMap.put("10", "false");
			// hashMap.put("11", "false");
			// hashMap.put("12", "false");
			// hashMap.put("13", "false");
			// hashMap.put("14", "false");
			// hashMap.put("15", "false");
			// hashMap.put("16", "false");
			// hashMap.put("17", "1518934320000");
			// hashMap.put("18", "1519279920000");
			// hashMap.put("19", "ankita.yadav@swiggy.in");
			// hashMap.put("20", "1518934320000");
			// hashMap.put("21", "1518934320000");
			// hashMap.put("22", "null");
			// hashMap.put("23", "null");
			// hashMap.put("24", "null");
			// hashMap.put("25", "-1");
			// hashMap.put("26", "false");
			// hashMap.put("27", "0");
			// hashMap.put("28", "null");
			// hashMap.put("29", "null");
			// hashMap.put("30", "null");
			// hashMap.put("31", "null");
			// hashMap.put("32", "1");
			// hashMap.put("33", "Discount");
			// hashMap.put("34", "10");
			// hashMap.put("35", "1");
			// hashMap.put("36", "50");
			// hashMap.put("37", "10");
			// hashMap.put("38", "90");
			// hashMap.put("39", "false");
			// hashMap.put("40", "0");
			// hashMap.put("41", "-1");
			// hashMap.put("42", "1st transaction desc.");
			// hashMap.put("43", "1st transaction");
			// hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("45", "lq8egr0tyay6vykis5rg");
			// hashMap.put("46", "false");
			// hashMap.put("47",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("48", "ANDROID");
			// hashMap.put("49", "2");
			// hashMap.put("50", "2");
			// hashMap.put("51", "Discount");
			// hashMap.put("52", "10");
			// hashMap.put("53", "1");
			// hashMap.put("54", "50");
			// hashMap.put("55", "10");
			// hashMap.put("56", "90");
			// hashMap.put("57", "false");
			// hashMap.put("58", "0");
			// hashMap.put("59", "-1");
			// hashMap.put("60", "1st transaction desc.");
			// hashMap.put("61", "1st transaction");
			// hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			// hashMap.put("63", "paytm_vxi7qb");
			// hashMap.put("64", "false");
			// hashMap.put("65",
			// "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			// hashMap.put("66", "ANDROID");
			// hashMap.put("67", "2");
			// hashMap.put("68", "false");
			// hashMap.put("69", "false");
			// hashMap.put("70", "false");

			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponInvalidLogoIdData")
	public static Object[][] createMultiApplyCouponInvalidLogoIdData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyFirstOrderRestrictionCouponData")
	public static Object[][] createMultiApplyFirstOrderRestrictionCouponData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			String duplicateType = "false";

			// FristOrderRestriction - true(46)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "true");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// FristOrderRestriction - false(46)
			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponPreferredPaymentMethodData")
	public static Object[][] createMultiApplyCouponPreferredPaymentMethodData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();
			HashMap<String, String> hashMap4 = new HashMap<String, String>();
			HashMap<String, String> hashMap5 = new HashMap<String, String>();
			HashMap<String, String> hashMap6 = new HashMap<String, String>();
			HashMap<String, String> hashMap7 = new HashMap<String, String>();

			String duplicateType = "false";

			// all payment methods
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// PayTM & Juspay
			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			// PayTM & Cash
			hashMap2.put("0", "ALPHACODE");
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "10");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47", "\"PayTM\",\"Cash\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65", "\"PayTM\",\"Cash\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");

			// Cash & Juspay-NB
			hashMap3.put("0", "ALPHACODE");
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1519279920000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "10");
			hashMap3.put("35", "1");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47", "\"Cash\",\"Juspay-NB\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Discount");
			hashMap3.put("52", "10");
			hashMap3.put("53", "1");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65", "\"Cash\",\"Juspay-NB\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");

			// PayTM-SSO & Mobikwik
			hashMap4.put("0", "ALPHACODE");
			hashMap4.put("1", "MULTICOUPON");
			hashMap4.put("2", "Internal");
			hashMap4.put("3", "AUTOMATION");
			hashMap4.put("4", "2");
			hashMap4.put("5", "10");
			hashMap4.put("6", "false");
			hashMap4.put("7", "false");
			hashMap4.put("8", "false");
			hashMap4.put("9", "false");
			hashMap4.put("10", "false");
			hashMap4.put("11", "false");
			hashMap4.put("12", "false");
			hashMap4.put("13", "false");
			hashMap4.put("14", "false");
			hashMap4.put("15", "false");
			hashMap4.put("16", "false");
			hashMap4.put("17", "1518934320000");
			hashMap4.put("18", "1519279920000");
			hashMap4.put("19", "ankita.yadav@swiggy.in");
			hashMap4.put("20", "1518934320000");
			hashMap4.put("21", "1518934320000");
			hashMap4.put("22", "null");
			hashMap4.put("23", "null");
			hashMap4.put("24", "null");
			hashMap4.put("25", "-1");
			hashMap4.put("26", "false");
			hashMap4.put("27", "0");
			hashMap4.put("28", "null");
			hashMap4.put("29", "null");
			hashMap4.put("30", "null");
			hashMap4.put("31", "null");
			hashMap4.put("32", "1");
			hashMap4.put("33", "Discount");
			hashMap4.put("34", "10");
			hashMap4.put("35", "1");
			hashMap4.put("36", "50");
			hashMap4.put("37", "10");
			hashMap4.put("38", "90");
			hashMap4.put("39", "false");
			hashMap4.put("40", "0");
			hashMap4.put("41", "-1");
			hashMap4.put("42", "1st transaction desc.");
			hashMap4.put("43", "1st transaction");
			hashMap4.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap4.put("45", "w2fh4c46d0bszhl89voy");
			hashMap4.put("46", "false");
			hashMap4.put("47", "\"PayTM-SSO\",\"Mobikwik\"");
			hashMap4.put("48", "ANDROID");
			hashMap4.put("49", "2");
			hashMap4.put("50", "2");
			hashMap4.put("51", "Discount");
			hashMap4.put("52", "10");
			hashMap4.put("53", "1");
			hashMap4.put("54", "50");
			hashMap4.put("55", "10");
			hashMap4.put("56", "90");
			hashMap4.put("57", "false");
			hashMap4.put("58", "0");
			hashMap4.put("59", "-1");
			hashMap4.put("60", "1st transaction desc.");
			hashMap4.put("61", "1st transaction");
			hashMap4.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap4.put("63", "w2fh4c46d0bszhl89voy");
			hashMap4.put("64", "false");
			hashMap4.put("65", "\"Cash\",\"Juspay-NB\"");
			hashMap4.put("66", "ANDROID");
			hashMap4.put("67", "2");
			hashMap4.put("68", "false");
			hashMap4.put("69", "false");
			hashMap4.put("70", "false");

			// Mobikwik-SSO & "Freecharge-SSO
			hashMap5.put("0", "ALPHACODE");
			hashMap5.put("1", "MULTICOUPON");
			hashMap5.put("2", "Internal");
			hashMap5.put("3", "AUTOMATION");
			hashMap5.put("4", "2");
			hashMap5.put("5", "10");
			hashMap5.put("6", "false");
			hashMap5.put("7", "false");
			hashMap5.put("8", "false");
			hashMap5.put("9", "false");
			hashMap5.put("10", "false");
			hashMap5.put("11", "false");
			hashMap5.put("12", "false");
			hashMap5.put("13", "false");
			hashMap5.put("14", "false");
			hashMap5.put("15", "false");
			hashMap5.put("16", "false");
			hashMap5.put("17", "1518934320000");
			hashMap5.put("18", "1519279920000");
			hashMap5.put("19", "ankita.yadav@swiggy.in");
			hashMap5.put("20", "1518934320000");
			hashMap5.put("21", "1518934320000");
			hashMap5.put("22", "null");
			hashMap5.put("23", "null");
			hashMap5.put("24", "null");
			hashMap5.put("25", "-1");
			hashMap5.put("26", "false");
			hashMap5.put("27", "0");
			hashMap5.put("28", "null");
			hashMap5.put("29", "null");
			hashMap5.put("30", "null");
			hashMap5.put("31", "null");
			hashMap5.put("32", "1");
			hashMap5.put("33", "Discount");
			hashMap5.put("34", "10");
			hashMap5.put("35", "1");
			hashMap5.put("36", "50");
			hashMap5.put("37", "10");
			hashMap5.put("38", "90");
			hashMap5.put("39", "false");
			hashMap5.put("40", "0");
			hashMap5.put("41", "-1");
			hashMap5.put("42", "1st transaction desc.");
			hashMap5.put("43", "1st transaction");
			hashMap5.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap5.put("45", "w2fh4c46d0bszhl89voy");
			hashMap5.put("46", "false");
			hashMap5.put("47", "\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap5.put("48", "ANDROID");
			hashMap5.put("49", "2");
			hashMap5.put("50", "2");
			hashMap5.put("51", "Discount");
			hashMap5.put("52", "10");
			hashMap5.put("53", "1");
			hashMap5.put("54", "50");
			hashMap5.put("55", "10");
			hashMap5.put("56", "90");
			hashMap5.put("57", "false");
			hashMap5.put("58", "0");
			hashMap5.put("59", "-1");
			hashMap5.put("60", "1st transaction desc.");
			hashMap5.put("61", "1st transaction");
			hashMap5.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap5.put("63", "w2fh4c46d0bszhl89voy");
			hashMap5.put("64", "false");
			hashMap5.put("65", "\"Juspay-NB\",\"PayTM-SSO\"");
			hashMap5.put("66", "ANDROID");
			hashMap5.put("67", "2");
			hashMap5.put("68", "false");
			hashMap5.put("69", "false");
			hashMap5.put("70", "false");

			// Freecharge & Sodexo
			hashMap6.put("0", "ALPHACODE");
			hashMap6.put("1", "MULTICOUPON");
			hashMap6.put("2", "Internal");
			hashMap6.put("3", "AUTOMATION");
			hashMap6.put("4", "2");
			hashMap6.put("5", "10");
			hashMap6.put("6", "false");
			hashMap6.put("7", "false");
			hashMap6.put("8", "false");
			hashMap6.put("9", "false");
			hashMap6.put("10", "false");
			hashMap6.put("11", "false");
			hashMap6.put("12", "false");
			hashMap6.put("13", "false");
			hashMap6.put("14", "false");
			hashMap6.put("15", "false");
			hashMap6.put("16", "false");
			hashMap6.put("17", "1518934320000");
			hashMap6.put("18", "1519279920000");
			hashMap6.put("19", "ankita.yadav@swiggy.in");
			hashMap6.put("20", "1518934320000");
			hashMap6.put("21", "1518934320000");
			hashMap6.put("22", "null");
			hashMap6.put("23", "null");
			hashMap6.put("24", "null");
			hashMap6.put("25", "-1");
			hashMap6.put("26", "false");
			hashMap6.put("27", "0");
			hashMap6.put("28", "null");
			hashMap6.put("29", "null");
			hashMap6.put("30", "null");
			hashMap6.put("31", "null");
			hashMap6.put("32", "1");
			hashMap6.put("33", "Discount");
			hashMap6.put("34", "10");
			hashMap6.put("35", "1");
			hashMap6.put("36", "50");
			hashMap6.put("37", "10");
			hashMap6.put("38", "90");
			hashMap6.put("39", "false");
			hashMap6.put("40", "0");
			hashMap6.put("41", "-1");
			hashMap6.put("42", "1st transaction desc.");
			hashMap6.put("43", "1st transaction");
			hashMap6.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap6.put("45", "w2fh4c46d0bszhl89voy");
			hashMap6.put("46", "false");
			hashMap6.put("47", "\"Freecharge\",\"Sodexo\"");
			hashMap6.put("48", "ANDROID");
			hashMap6.put("49", "2");
			hashMap6.put("50", "2");
			hashMap6.put("51", "Discount");
			hashMap6.put("52", "10");
			hashMap6.put("53", "1");
			hashMap6.put("54", "50");
			hashMap6.put("55", "10");
			hashMap6.put("56", "90");
			hashMap6.put("57", "false");
			hashMap6.put("58", "0");
			hashMap6.put("59", "-1");
			hashMap6.put("60", "1st transaction desc.");
			hashMap6.put("61", "1st transaction");
			hashMap6.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap6.put("63", "w2fh4c46d0bszhl89voy");
			hashMap6.put("64", "false");
			hashMap6.put("65", "\"Mobikwik-SSO\",\"Freecharge-SSO\"");
			hashMap6.put("66", "ANDROID");
			hashMap6.put("67", "2");
			hashMap6.put("68", "false");
			hashMap6.put("69", "false");
			hashMap6.put("70", "false");

			// Freecharge & Sodexo
			hashMap7.put("0", "ALPHACODE");
			hashMap7.put("1", "MULTICOUPON");
			hashMap7.put("2", "Internal");
			hashMap7.put("3", "AUTOMATION");
			hashMap7.put("4", "2");
			hashMap7.put("5", "10");
			hashMap7.put("6", "false");
			hashMap7.put("7", "false");
			hashMap7.put("8", "false");
			hashMap7.put("9", "false");
			hashMap7.put("10", "false");
			hashMap7.put("11", "false");
			hashMap7.put("12", "false");
			hashMap7.put("13", "false");
			hashMap7.put("14", "false");
			hashMap7.put("15", "false");
			hashMap7.put("16", "false");
			hashMap7.put("17", "1518934320000");
			hashMap7.put("18", "1519279920000");
			hashMap7.put("19", "ankita.yadav@swiggy.in");
			hashMap7.put("20", "1518934320000");
			hashMap7.put("21", "1518934320000");
			hashMap7.put("22", "null");
			hashMap7.put("23", "null");
			hashMap7.put("24", "null");
			hashMap7.put("25", "-1");
			hashMap7.put("26", "false");
			hashMap7.put("27", "0");
			hashMap7.put("28", "null");
			hashMap7.put("29", "null");
			hashMap7.put("30", "null");
			hashMap7.put("31", "null");
			hashMap7.put("32", "1");
			hashMap7.put("33", "Discount");
			hashMap7.put("34", "10");
			hashMap7.put("35", "1");
			hashMap7.put("36", "50");
			hashMap7.put("37", "10");
			hashMap7.put("38", "90");
			hashMap7.put("39", "false");
			hashMap7.put("40", "0");
			hashMap7.put("41", "-1");
			hashMap7.put("42", "1st transaction desc.");
			hashMap7.put("43", "1st transaction");
			hashMap7.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap7.put("45", "w2fh4c46d0bszhl89voy");
			hashMap7.put("46", "false");
			hashMap7.put("47", "\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap7.put("48", "ANDROID");
			hashMap7.put("49", "2");
			hashMap7.put("50", "2");
			hashMap7.put("51", "Discount");
			hashMap7.put("52", "10");
			hashMap7.put("53", "1");
			hashMap7.put("54", "50");
			hashMap7.put("55", "10");
			hashMap7.put("56", "90");
			hashMap7.put("57", "false");
			hashMap7.put("58", "0");
			hashMap7.put("59", "-1");
			hashMap7.put("60", "1st transaction desc.");
			hashMap7.put("61", "1st transaction");
			hashMap7.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap7.put("63", "w2fh4c46d0bszhl89voy");
			hashMap7.put("64", "false");
			hashMap7.put("65", "\"Freecharge\",\"Sodexo\"");
			hashMap7.put("66", "ANDROID");
			hashMap7.put("67", "2");
			hashMap7.put("68", "false");
			hashMap7.put("69", "false");
			hashMap7.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType },
					{ hashMap2, duplicateType }, { hashMap3, duplicateType }, { hashMap4, duplicateType },
					{ hashMap5, duplicateType }, { hashMap6, duplicateType }, { hashMap7, duplicateType } };
		}
	}

	@DataProvider(name = "createMultiApplyCouponInvalidPreferredPaymentMethodData")
	public static Object[][] createMultiApplyCouponInvalidPreferredPaymentMethodData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"com.swiggy.poc.test1\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyExpiryOffsetCouponData")
	public static Object[][] createMultiApplyExpiryOffsetCouponData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();
			HashMap<String, String> hashMap4 = new HashMap<String, String>();
			HashMap<String, String> hashMap5 = new HashMap<String, String>();

			String duplicateType = "false";
			// key 25 (-1) key 49 (2) key 67 (2)
			// coupon expiry offset is -1
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"PayTM\",\"Cash\",\"PayTM-SSO\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// coupon expiry offset - 25(10) , 49 (2), 67 (2)
			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "10");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47",
					"\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"PayTM\",\"Cash\",\"PayTM-SSO\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			// 25 (-1), 49 (2) ,67 (2)
			hashMap2.put("0", "ALPHACODE");
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "Discount");
			hashMap2.put("34", "10");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47",
					"\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "Discount");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65", "\"PayTM\",\"Cash\",\"PayTM-SSO\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");

			// key 25 (-1) , 49 (-1) , 67 (-1)
			hashMap3.put("0", "ALPHACODE");
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1519279920000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "Discount");
			hashMap3.put("34", "10");
			hashMap3.put("35", "1");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47",
					"\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "-1");
			hashMap3.put("50", "2");
			hashMap3.put("51", "Discount");
			hashMap3.put("52", "10");
			hashMap3.put("53", "1");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65", "\"PayTM\",\"Cash\",\"PayTM-SSO\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "-1");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");

			// 25 (-1), 49 (-1) ,67 (2)
			hashMap4.put("0", "ALPHACODE");
			hashMap4.put("1", "MULTICOUPON");
			hashMap4.put("2", "Internal");
			hashMap4.put("3", "AUTOMATION");
			hashMap4.put("4", "2");
			hashMap4.put("5", "10");
			hashMap4.put("6", "false");
			hashMap4.put("7", "false");
			hashMap4.put("8", "false");
			hashMap4.put("9", "false");
			hashMap4.put("10", "false");
			hashMap4.put("11", "false");
			hashMap4.put("12", "false");
			hashMap4.put("13", "false");
			hashMap4.put("14", "false");
			hashMap4.put("15", "false");
			hashMap4.put("16", "false");
			hashMap4.put("17", "1518934320000");
			hashMap4.put("18", "1519279920000");
			hashMap4.put("19", "ankita.yadav@swiggy.in");
			hashMap4.put("20", "1518934320000");
			hashMap4.put("21", "1518934320000");
			hashMap4.put("22", "null");
			hashMap4.put("23", "null");
			hashMap4.put("24", "null");
			hashMap4.put("25", "-1");
			hashMap4.put("26", "false");
			hashMap4.put("27", "0");
			hashMap4.put("28", "null");
			hashMap4.put("29", "null");
			hashMap4.put("30", "null");
			hashMap4.put("31", "null");
			hashMap4.put("32", "1");
			hashMap4.put("33", "Discount");
			hashMap4.put("34", "10");
			hashMap4.put("35", "1");
			hashMap4.put("36", "50");
			hashMap4.put("37", "10");
			hashMap4.put("38", "90");
			hashMap4.put("39", "false");
			hashMap4.put("40", "0");
			hashMap4.put("41", "-1");
			hashMap4.put("42", "1st transaction desc.");
			hashMap4.put("43", "1st transaction");
			hashMap4.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap4.put("45", "w2fh4c46d0bszhl89voy");
			hashMap4.put("46", "false");
			hashMap4.put("47",
					"\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap4.put("48", "ANDROID");
			hashMap4.put("49", "-1");
			hashMap4.put("50", "2");
			hashMap4.put("51", "Discount");
			hashMap4.put("52", "10");
			hashMap4.put("53", "1");
			hashMap4.put("54", "50");
			hashMap4.put("55", "10");
			hashMap4.put("56", "90");
			hashMap4.put("57", "false");
			hashMap4.put("58", "0");
			hashMap4.put("59", "-1");
			hashMap4.put("60", "1st transaction desc.");
			hashMap4.put("61", "1st transaction");
			hashMap4.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap4.put("63", "w2fh4c46d0bszhl89voy");
			hashMap4.put("64", "false");
			hashMap4.put("65", "\"PayTM\",\"Cash\",\"PayTM-SSO\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap4.put("66", "ANDROID");
			hashMap4.put("67", "2");
			hashMap4.put("68", "false");
			hashMap4.put("69", "false");
			hashMap4.put("70", "false");

			// 25(-1),49 (2) , 67 (-1)
			hashMap5.put("0", "ALPHACODE");
			hashMap5.put("1", "MULTICOUPON");
			hashMap5.put("2", "Internal");
			hashMap5.put("3", "AUTOMATION");
			hashMap5.put("4", "2");
			hashMap5.put("5", "10");
			hashMap5.put("6", "false");
			hashMap5.put("7", "false");
			hashMap5.put("8", "false");
			hashMap5.put("9", "false");
			hashMap5.put("10", "false");
			hashMap5.put("11", "false");
			hashMap5.put("12", "false");
			hashMap5.put("13", "false");
			hashMap5.put("14", "false");
			hashMap5.put("15", "false");
			hashMap5.put("16", "false");
			hashMap5.put("17", "1518934320000");
			hashMap5.put("18", "1519279920000");
			hashMap5.put("19", "ankita.yadav@swiggy.in");
			hashMap5.put("20", "1518934320000");
			hashMap5.put("21", "1518934320000");
			hashMap5.put("22", "null");
			hashMap5.put("23", "null");
			hashMap5.put("24", "null");
			hashMap5.put("25", "-1");
			hashMap5.put("26", "false");
			hashMap5.put("27", "0");
			hashMap5.put("28", "null");
			hashMap5.put("29", "null");
			hashMap5.put("30", "null");
			hashMap5.put("31", "null");
			hashMap5.put("32", "1");
			hashMap5.put("33", "Discount");
			hashMap5.put("34", "10");
			hashMap5.put("35", "1");
			hashMap5.put("36", "50");
			hashMap5.put("37", "10");
			hashMap5.put("38", "90");
			hashMap5.put("39", "false");
			hashMap5.put("40", "0");
			hashMap5.put("41", "-1");
			hashMap5.put("42", "1st transaction desc.");
			hashMap5.put("43", "1st transaction");
			hashMap5.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap5.put("45", "w2fh4c46d0bszhl89voy");
			hashMap5.put("46", "false");
			hashMap5.put("47",
					"\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap5.put("48", "ANDROID");
			hashMap5.put("49", "2");
			hashMap5.put("50", "2");
			hashMap5.put("51", "Discount");
			hashMap5.put("52", "10");
			hashMap5.put("53", "1");
			hashMap5.put("54", "50");
			hashMap5.put("55", "10");
			hashMap5.put("56", "90");
			hashMap5.put("57", "false");
			hashMap5.put("58", "0");
			hashMap5.put("59", "-1");
			hashMap5.put("60", "1st transaction desc.");
			hashMap5.put("61", "1st transaction");
			hashMap5.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap5.put("63", "w2fh4c46d0bszhl89voy");
			hashMap5.put("64", "false");
			hashMap5.put("65", "\"PayTM\",\"Cash\",\"PayTM-SSO\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap5.put("66", "ANDROID");
			hashMap5.put("67", "-1");
			hashMap5.put("68", "false");
			hashMap5.put("69", "false");
			hashMap5.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType },
					{ hashMap2, duplicateType }, { hashMap3, duplicateType }, { hashMap4, duplicateType },
					{ hashMap5, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponApplicableWithSwiggyMoneyData")
	public static Object[][] createMultiApplyCouponApplicableWithSwiggyMoneyData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// applicableWithSwiggyMoney is true (14)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "true");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// applicableWithSwiggyMoney is false (14)
			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType }, };

		}
	}

	@DataProvider(name = "createMultiApplyCouponApplicableInReferralCouponPresenceData")
	public static Object[][] createMultiApplyCouponApplicableInReferralCouponPresenceData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// applicableInReferralCouponPresence is true (15)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "true");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// applicableInReferralCouponPresence is false (15)
			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "true");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType }, };

		}
	}

	@DataProvider(name = "createMultiApplyCouponAutoApplyData")
	public static Object[][] createMultiApplyCouponAutoApplyData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// AutoApply is true (16)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "true");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// AutoApply is false (16)
			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType }, };

		}
	}

	@DataProvider(name = "createMultiApplyCouponCustomerRestrictionData")
	public static Object[][] createMultiApplyCouponCustomerRestrictionTest() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			String duplicateType = "false";

			// customerRestriction false (6)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// customerRestriction true (6)
			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "true");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponCityRestrictionData")
	public static Object[][] createMultiApplyCouponCityRestrictionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// cityRestriction is false (7)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// cityRestriction is true(7)

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponAreaRestrictionData")
	public static Object[][] createMultiApplyCouponAreaRestrictionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// areaRestriction is false (8)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// areaRestriction is true(8)

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "true");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponCuisineRestrictionData")
	public static Object[][] createMultiApplyCouponCuisineRestrictionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// CuisineRestriction is false (9)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// CuisineRestriction is true(9)

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "true");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponRestaurantRestrictionData")
	public static Object[][] createMultiApplyCouponRestaurantRestrictionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// RestaurantRestriction is false (10)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// RestaurantRestriction is true(10)

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "true");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponCategoryRestrictionData")
	public static Object[][] createMultiApplyCouponCategoryRestrictionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// categoryRestriction is false (11)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// categoryRestriction is true(11)

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "true");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponItemRestrictionData")
	public static Object[][] createMultiApplyCouponItemRestrictionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// itemRestriction is false (12)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// itemRestriction is true(12)

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "true");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponSlotRestrictionData")
	public static Object[][] createMultiApplyCouponSlotRestrictionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// slotRestriction is false (13)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// slotRestriction is true(13)

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "true");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "createMultiApplyCouponFreeGiftsData")
	public static Object[][] createMultiApplyCouponFreeGiftsData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";

			// FreeGifts is false (26)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			// FreeGifts is false (26)

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "true");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "true");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType }, { hashMap1, duplicateType } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponTotalAvailableData")
	public static Object[][] updateMultiApplyCouponTotalAvailableData() {
		{
			HashMap<Object, String> hashMap = new HashMap<Object, String>();
			HashMap<Object, String> hashMap1 = new HashMap<Object, String>();

			String duplicateType = "false";
			String key = "5";
			String updateValue = "23456";

			// TC total available update (5)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			return new Object[][] { { hashMap, duplicateType, key, updateValue } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponValidFromData")
	public static Object[][] updateMultiApplyCouponValidFromData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";
			String key = "17";
			String updateValue = "1521007920000";

			// TC for validFrom update (17)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType, key, updateValue } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponValidTillData")
	public static Object[][] updateMultiApplyCouponValidTillData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";
			String key = "18";
			String updateValue = "1522368000000";
			// TC for validTill update (18)
			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType, key, updateValue } };

		}
	}

	@DataProvider(name = "updateMultiApplyCoupoAutoApplyData")
	public static Object[][] updateMultiApplyCoupoAutoApplyData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";
			String key = "16";
			String updateValue = "true";
			// TC for autoApply update (16)

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			return new Object[][] { { hashMap, duplicateType, key, updateValue } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponApplicableWithSwiggyMoneyData")
	public static Object[][] updateMultiApplyCouponApplicableWithSwiggyMoneyData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";
			String key = "14";
			String updateValue = "true";

			// TC for applicableWithSwiggyMoney update (14)

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			return new Object[][] { { hashMap, duplicateType, key, updateValue } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponFreeGiftsData")
	public static Object[][] updateMultiApplyCouponFreeGiftsData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";
			String key = "26";
			String updateValue = "true";

			// TC for freeGifts update (26)

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType, key, updateValue } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponCouponTypeData")
	public static Object[][] updateMultiApplyCouponCouponTypeData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();
			HashMap<String, String> hashMap2 = new HashMap<String, String>();
			HashMap<String, String> hashMap3 = new HashMap<String, String>();

			String duplicateType = "false";
			String key = "33";
			String updateValueCashBack = "CashBack";
			String updateValueDiscount = "Discount";
			String updateValuePayTM = "PayTM";
			String updateValueMarketing = "Marketing";

			// TC for couponType update (26)

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "CashBack");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "CashBack");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			hashMap2.put("0", "ALPHACODE");
			hashMap2.put("1", "MULTICOUPON");
			hashMap2.put("2", "Internal");
			hashMap2.put("3", "AUTOMATION");
			hashMap2.put("4", "2");
			hashMap2.put("5", "10");
			hashMap2.put("6", "false");
			hashMap2.put("7", "false");
			hashMap2.put("8", "false");
			hashMap2.put("9", "false");
			hashMap2.put("10", "false");
			hashMap2.put("11", "false");
			hashMap2.put("12", "false");
			hashMap2.put("13", "false");
			hashMap2.put("14", "false");
			hashMap2.put("15", "false");
			hashMap2.put("16", "false");
			hashMap2.put("17", "1518934320000");
			hashMap2.put("18", "1519279920000");
			hashMap2.put("19", "ankita.yadav@swiggy.in");
			hashMap2.put("20", "1518934320000");
			hashMap2.put("21", "1518934320000");
			hashMap2.put("22", "null");
			hashMap2.put("23", "null");
			hashMap2.put("24", "null");
			hashMap2.put("25", "-1");
			hashMap2.put("26", "false");
			hashMap2.put("27", "0");
			hashMap2.put("28", "null");
			hashMap2.put("29", "null");
			hashMap2.put("30", "null");
			hashMap2.put("31", "null");
			hashMap2.put("32", "1");
			hashMap2.put("33", "CashBack");
			hashMap2.put("34", "10");
			hashMap2.put("35", "1");
			hashMap2.put("36", "50");
			hashMap2.put("37", "10");
			hashMap2.put("38", "90");
			hashMap2.put("39", "false");
			hashMap2.put("40", "0");
			hashMap2.put("41", "-1");
			hashMap2.put("42", "1st transaction desc.");
			hashMap2.put("43", "1st transaction");
			hashMap2.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap2.put("45", "w2fh4c46d0bszhl89voy");
			hashMap2.put("46", "false");
			hashMap2.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap2.put("48", "ANDROID");
			hashMap2.put("49", "2");
			hashMap2.put("50", "2");
			hashMap2.put("51", "CashBack");
			hashMap2.put("52", "10");
			hashMap2.put("53", "1");
			hashMap2.put("54", "50");
			hashMap2.put("55", "10");
			hashMap2.put("56", "90");
			hashMap2.put("57", "false");
			hashMap2.put("58", "0");
			hashMap2.put("59", "-1");
			hashMap2.put("60", "1st transaction desc.");
			hashMap2.put("61", "1st transaction");
			hashMap2.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap2.put("63", "w2fh4c46d0bszhl89voy");
			hashMap2.put("64", "false");
			hashMap2.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap2.put("66", "ANDROID");
			hashMap2.put("67", "2");
			hashMap2.put("68", "false");
			hashMap2.put("69", "false");
			hashMap2.put("70", "false");

			hashMap3.put("0", "ALPHACODE");
			hashMap3.put("1", "MULTICOUPON");
			hashMap3.put("2", "Internal");
			hashMap3.put("3", "AUTOMATION");
			hashMap3.put("4", "2");
			hashMap3.put("5", "10");
			hashMap3.put("6", "false");
			hashMap3.put("7", "false");
			hashMap3.put("8", "false");
			hashMap3.put("9", "false");
			hashMap3.put("10", "false");
			hashMap3.put("11", "false");
			hashMap3.put("12", "false");
			hashMap3.put("13", "false");
			hashMap3.put("14", "false");
			hashMap3.put("15", "false");
			hashMap3.put("16", "false");
			hashMap3.put("17", "1518934320000");
			hashMap3.put("18", "1519279920000");
			hashMap3.put("19", "ankita.yadav@swiggy.in");
			hashMap3.put("20", "1518934320000");
			hashMap3.put("21", "1518934320000");
			hashMap3.put("22", "null");
			hashMap3.put("23", "null");
			hashMap3.put("24", "null");
			hashMap3.put("25", "-1");
			hashMap3.put("26", "false");
			hashMap3.put("27", "0");
			hashMap3.put("28", "null");
			hashMap3.put("29", "null");
			hashMap3.put("30", "null");
			hashMap3.put("31", "null");
			hashMap3.put("32", "1");
			hashMap3.put("33", "PayTM");
			hashMap3.put("34", "10");
			hashMap3.put("35", "1");
			hashMap3.put("36", "50");
			hashMap3.put("37", "10");
			hashMap3.put("38", "90");
			hashMap3.put("39", "false");
			hashMap3.put("40", "0");
			hashMap3.put("41", "-1");
			hashMap3.put("42", "1st transaction desc.");
			hashMap3.put("43", "1st transaction");
			hashMap3.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap3.put("45", "w2fh4c46d0bszhl89voy");
			hashMap3.put("46", "false");
			hashMap3.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap3.put("48", "ANDROID");
			hashMap3.put("49", "2");
			hashMap3.put("50", "2");
			hashMap3.put("51", "PayTM");
			hashMap3.put("52", "10");
			hashMap3.put("53", "1");
			hashMap3.put("54", "50");
			hashMap3.put("55", "10");
			hashMap3.put("56", "90");
			hashMap3.put("57", "false");
			hashMap3.put("58", "0");
			hashMap3.put("59", "-1");
			hashMap3.put("60", "1st transaction desc.");
			hashMap3.put("61", "1st transaction");
			hashMap3.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap3.put("63", "w2fh4c46d0bszhl89voy");
			hashMap3.put("64", "false");
			hashMap3.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap3.put("66", "ANDROID");
			hashMap3.put("67", "2");
			hashMap3.put("68", "false");
			hashMap3.put("69", "false");
			hashMap3.put("70", "false");

			return new Object[][] { { hashMap, duplicateType, key, updateValueCashBack },
					{ hashMap1, duplicateType, key, updateValueDiscount },
					{ hashMap2, duplicateType, key, updateValuePayTM },
					{ hashMap3, duplicateType, key, updateValueMarketing } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponTitleData")
	public static Object[][] updateMultiApplyCouponTitleData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> hashMap1 = new HashMap<String, String>();

			String duplicateType = "false";
			String transactionOneKey = "43";
			String transactionTwoKey = "61";
			String updateTitle = "UpdatedTitleOne";
			String updateTitleTwo = "UpdatedTitleTwo";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			hashMap1.put("0", "ALPHACODE");
			hashMap1.put("1", "MULTICOUPON");
			hashMap1.put("2", "Internal");
			hashMap1.put("3", "AUTOMATION");
			hashMap1.put("4", "2");
			hashMap1.put("5", "10");
			hashMap1.put("6", "false");
			hashMap1.put("7", "false");
			hashMap1.put("8", "false");
			hashMap1.put("9", "false");
			hashMap1.put("10", "false");
			hashMap1.put("11", "false");
			hashMap1.put("12", "false");
			hashMap1.put("13", "false");
			hashMap1.put("14", "false");
			hashMap1.put("15", "false");
			hashMap1.put("16", "false");
			hashMap1.put("17", "1518934320000");
			hashMap1.put("18", "1519279920000");
			hashMap1.put("19", "ankita.yadav@swiggy.in");
			hashMap1.put("20", "1518934320000");
			hashMap1.put("21", "1518934320000");
			hashMap1.put("22", "null");
			hashMap1.put("23", "null");
			hashMap1.put("24", "null");
			hashMap1.put("25", "-1");
			hashMap1.put("26", "false");
			hashMap1.put("27", "0");
			hashMap1.put("28", "null");
			hashMap1.put("29", "null");
			hashMap1.put("30", "null");
			hashMap1.put("31", "null");
			hashMap1.put("32", "1");
			hashMap1.put("33", "Discount");
			hashMap1.put("34", "10");
			hashMap1.put("35", "1");
			hashMap1.put("36", "50");
			hashMap1.put("37", "10");
			hashMap1.put("38", "90");
			hashMap1.put("39", "false");
			hashMap1.put("40", "0");
			hashMap1.put("41", "-1");
			hashMap1.put("42", "1st transaction desc.");
			hashMap1.put("43", "1st transaction");
			hashMap1.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("45", "w2fh4c46d0bszhl89voy");
			hashMap1.put("46", "false");
			hashMap1.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("48", "ANDROID");
			hashMap1.put("49", "2");
			hashMap1.put("50", "2");
			hashMap1.put("51", "Discount");
			hashMap1.put("52", "10");
			hashMap1.put("53", "1");
			hashMap1.put("54", "50");
			hashMap1.put("55", "10");
			hashMap1.put("56", "90");
			hashMap1.put("57", "false");
			hashMap1.put("58", "0");
			hashMap1.put("59", "-1");
			hashMap1.put("60", "1st transaction desc.");
			hashMap1.put("61", "1st transaction");
			hashMap1.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap1.put("63", "w2fh4c46d0bszhl89voy");
			hashMap1.put("64", "false");
			hashMap1.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap1.put("66", "ANDROID");
			hashMap1.put("67", "2");
			hashMap1.put("68", "false");
			hashMap1.put("69", "false");
			hashMap1.put("70", "false");

			return new Object[][] { { hashMap, duplicateType, transactionOneKey, updateTitle },
					{ hashMap1, duplicateType, transactionTwoKey, updateTitleTwo } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponDescriptionData")
	public static Object[][] updateMultiApplyCouponDescriptionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";
			String transactionOneKey = "42";
			String transactionOneUpdateDescription = "Updated Description of Transaction one";
			String transactionTwoKey = "60";
			String transactionTwoUpdateDescription = "Updated Description of Transaction two";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction description");
			hashMap.put("43", "1st transaction title");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "2nd transaction desc.");
			hashMap.put("61", "1st transaction Title");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType, transactionOneKey, transactionOneUpdateDescription },
					{ hashMap, duplicateType, transactionTwoKey, transactionTwoUpdateDescription } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponLogoData")
	public static Object[][] updateMultiApplyCouponLogoData() {
		{
			HashMap<String, String> lazyPayLogo = new HashMap<String, String>();
			HashMap<String, String> kotakLogo = new HashMap<String, String>();
			String duplicateType = "false";
			String transactionOneKey = "45";
			String transactionOneLazyPayLogo = "kj4khfwj6knnkskw4df3";
			String transactionTwoKey = "63";
			String transactionTwoKotakLogo = "vspvwgmfdm7olpyoiwxa";

			lazyPayLogo.put("0", "ALPHACODE");
			lazyPayLogo.put("1", "MULTICOUPON");
			lazyPayLogo.put("2", "Internal");
			lazyPayLogo.put("3", "AUTOMATION");
			lazyPayLogo.put("4", "2");
			lazyPayLogo.put("5", "10");
			lazyPayLogo.put("6", "false");
			lazyPayLogo.put("7", "false");
			lazyPayLogo.put("8", "false");
			lazyPayLogo.put("9", "false");
			lazyPayLogo.put("10", "false");
			lazyPayLogo.put("11", "false");
			lazyPayLogo.put("12", "false");
			lazyPayLogo.put("13", "false");
			lazyPayLogo.put("14", "false");
			lazyPayLogo.put("15", "false");
			lazyPayLogo.put("16", "false");
			lazyPayLogo.put("17", "1518934320000");
			lazyPayLogo.put("18", "1519279920000");
			lazyPayLogo.put("19", "ankita.yadav@swiggy.in");
			lazyPayLogo.put("20", "1518934320000");
			lazyPayLogo.put("21", "1518934320000");
			lazyPayLogo.put("22", "null");
			lazyPayLogo.put("23", "null");
			lazyPayLogo.put("24", "null");
			lazyPayLogo.put("25", "-1");
			lazyPayLogo.put("26", "false");
			lazyPayLogo.put("27", "0");
			lazyPayLogo.put("28", "null");
			lazyPayLogo.put("29", "null");
			lazyPayLogo.put("30", "null");
			lazyPayLogo.put("31", "null");
			lazyPayLogo.put("32", "1");
			lazyPayLogo.put("33", "Discount");
			lazyPayLogo.put("34", "10");
			lazyPayLogo.put("35", "1");
			lazyPayLogo.put("36", "50");
			lazyPayLogo.put("37", "10");
			lazyPayLogo.put("38", "90");
			lazyPayLogo.put("39", "false");
			lazyPayLogo.put("40", "0");
			lazyPayLogo.put("41", "-1");
			lazyPayLogo.put("42", "1st transaction desc.");
			lazyPayLogo.put("43", "1st transaction");
			lazyPayLogo.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			lazyPayLogo.put("45", "w2fh4c46d0bszhl89voy");
			lazyPayLogo.put("46", "false");
			lazyPayLogo.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			lazyPayLogo.put("48", "ANDROID");
			lazyPayLogo.put("49", "2");
			lazyPayLogo.put("50", "2");
			lazyPayLogo.put("51", "Discount");
			lazyPayLogo.put("52", "10");
			lazyPayLogo.put("53", "1");
			lazyPayLogo.put("54", "50");
			lazyPayLogo.put("55", "10");
			lazyPayLogo.put("56", "90");
			lazyPayLogo.put("57", "false");
			lazyPayLogo.put("58", "0");
			lazyPayLogo.put("59", "-1");
			lazyPayLogo.put("60", "1st transaction desc.");
			lazyPayLogo.put("61", "1st transaction");
			lazyPayLogo.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			lazyPayLogo.put("63", "w2fh4c46d0bszhl89voy");
			lazyPayLogo.put("64", "false");
			lazyPayLogo.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			lazyPayLogo.put("66", "ANDROID");
			lazyPayLogo.put("67", "2");
			lazyPayLogo.put("68", "false");
			lazyPayLogo.put("69", "false");
			lazyPayLogo.put("70", "false");

			kotakLogo.put("0", "ALPHACODE");
			kotakLogo.put("1", "MULTICOUPON");
			kotakLogo.put("2", "Internal");
			kotakLogo.put("3", "AUTOMATION");
			kotakLogo.put("4", "2");
			kotakLogo.put("5", "10");
			kotakLogo.put("6", "false");
			kotakLogo.put("7", "false");
			kotakLogo.put("8", "false");
			kotakLogo.put("9", "false");
			kotakLogo.put("10", "false");
			kotakLogo.put("11", "false");
			kotakLogo.put("12", "false");
			kotakLogo.put("13", "false");
			kotakLogo.put("14", "false");
			kotakLogo.put("15", "false");
			kotakLogo.put("16", "false");
			kotakLogo.put("17", "1518934320000");
			kotakLogo.put("18", "1519279920000");
			kotakLogo.put("19", "ankita.yadav@swiggy.in");
			kotakLogo.put("20", "1518934320000");
			kotakLogo.put("21", "1518934320000");
			kotakLogo.put("22", "null");
			kotakLogo.put("23", "null");
			kotakLogo.put("24", "null");
			kotakLogo.put("25", "-1");
			kotakLogo.put("26", "false");
			kotakLogo.put("27", "0");
			kotakLogo.put("28", "null");
			kotakLogo.put("29", "null");
			kotakLogo.put("30", "null");
			kotakLogo.put("31", "null");
			kotakLogo.put("32", "1");
			kotakLogo.put("33", "Discount");
			kotakLogo.put("34", "10");
			kotakLogo.put("35", "1");
			kotakLogo.put("36", "50");
			kotakLogo.put("37", "10");
			kotakLogo.put("38", "90");
			kotakLogo.put("39", "false");
			kotakLogo.put("40", "0");
			kotakLogo.put("41", "-1");
			kotakLogo.put("42", "1st transaction desc.");
			kotakLogo.put("43", "1st transaction");
			kotakLogo.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			kotakLogo.put("45", "w2fh4c46d0bszhl89voy");
			kotakLogo.put("46", "false");
			kotakLogo.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			kotakLogo.put("48", "ANDROID");
			kotakLogo.put("49", "2");
			kotakLogo.put("50", "2");
			kotakLogo.put("51", "Discount");
			kotakLogo.put("52", "10");
			kotakLogo.put("53", "1");
			kotakLogo.put("54", "50");
			kotakLogo.put("55", "10");
			kotakLogo.put("56", "90");
			kotakLogo.put("57", "false");
			kotakLogo.put("58", "0");
			kotakLogo.put("59", "-1");
			kotakLogo.put("60", "1st transaction desc.");
			kotakLogo.put("61", "1st transaction");
			kotakLogo.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			kotakLogo.put("63", "w2fh4c46d0bszhl89voy");
			kotakLogo.put("64", "false");
			kotakLogo.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			kotakLogo.put("66", "ANDROID");
			kotakLogo.put("67", "2");
			kotakLogo.put("68", "false");
			kotakLogo.put("69", "false");
			kotakLogo.put("70", "false");

			return new Object[][] { { lazyPayLogo, duplicateType, transactionOneKey, transactionOneLazyPayLogo },
					{ kotakLogo, duplicateType, transactionTwoKey, transactionTwoKotakLogo } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponCartMinAmountData")
	public static Object[][] updateMultiApplyCouponCartMinAmountData() {
		{
			HashMap<String, String> MinCartAmount = new HashMap<String, String>();
			HashMap<String, String> MinCartAmountTransactionTwo = new HashMap<String, String>();
			String duplicateType = "false";
			String transactionOneKey = "34";
			String transactionOneMinCartAmount = "299.99";
			String transactionTwoKey = "52";
			String transactionTwoMinCartAmount = "199.99";

			MinCartAmount.put("0", "ALPHACODE");
			MinCartAmount.put("1", "MULTICOUPON");
			MinCartAmount.put("2", "Internal");
			MinCartAmount.put("3", "AUTOMATION");
			MinCartAmount.put("4", "2");
			MinCartAmount.put("5", "10");
			MinCartAmount.put("6", "false");
			MinCartAmount.put("7", "false");
			MinCartAmount.put("8", "false");
			MinCartAmount.put("9", "false");
			MinCartAmount.put("10", "false");
			MinCartAmount.put("11", "false");
			MinCartAmount.put("12", "false");
			MinCartAmount.put("13", "false");
			MinCartAmount.put("14", "false");
			MinCartAmount.put("15", "false");
			MinCartAmount.put("16", "false");
			MinCartAmount.put("17", "1518934320000");
			MinCartAmount.put("18", "1519279920000");
			MinCartAmount.put("19", "ankita.yadav@swiggy.in");
			MinCartAmount.put("20", "1518934320000");
			MinCartAmount.put("21", "1518934320000");
			MinCartAmount.put("22", "null");
			MinCartAmount.put("23", "null");
			MinCartAmount.put("24", "null");
			MinCartAmount.put("25", "-1");
			MinCartAmount.put("26", "false");
			MinCartAmount.put("27", "0");
			MinCartAmount.put("28", "null");
			MinCartAmount.put("29", "null");
			MinCartAmount.put("30", "null");
			MinCartAmount.put("31", "null");
			MinCartAmount.put("32", "1");
			MinCartAmount.put("33", "Discount");
			MinCartAmount.put("34", "10");
			MinCartAmount.put("35", "1");
			MinCartAmount.put("36", "50");
			MinCartAmount.put("37", "10");
			MinCartAmount.put("38", "90");
			MinCartAmount.put("39", "false");
			MinCartAmount.put("40", "0");
			MinCartAmount.put("41", "-1");
			MinCartAmount.put("42", "1st transaction desc.");
			MinCartAmount.put("43", "1st transaction");
			MinCartAmount.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			MinCartAmount.put("45", "w2fh4c46d0bszhl89voy");
			MinCartAmount.put("46", "false");
			MinCartAmount.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			MinCartAmount.put("48", "ANDROID");
			MinCartAmount.put("49", "2");
			MinCartAmount.put("50", "2");
			MinCartAmount.put("51", "Discount");
			MinCartAmount.put("52", "10");
			MinCartAmount.put("53", "1");
			MinCartAmount.put("54", "50");
			MinCartAmount.put("55", "10");
			MinCartAmount.put("56", "90");
			MinCartAmount.put("57", "false");
			MinCartAmount.put("58", "0");
			MinCartAmount.put("59", "-1");
			MinCartAmount.put("60", "1st transaction desc.");
			MinCartAmount.put("61", "1st transaction");
			MinCartAmount.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			MinCartAmount.put("63", "w2fh4c46d0bszhl89voy");
			MinCartAmount.put("64", "false");
			MinCartAmount.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			MinCartAmount.put("66", "ANDROID");
			MinCartAmount.put("67", "2");
			MinCartAmount.put("68", "false");
			MinCartAmount.put("69", "false");
			MinCartAmount.put("70", "false");

			MinCartAmountTransactionTwo.put("0", "ALPHACODE");
			MinCartAmountTransactionTwo.put("1", "MULTICOUPON");
			MinCartAmountTransactionTwo.put("2", "Internal");
			MinCartAmountTransactionTwo.put("3", "AUTOMATION");
			MinCartAmountTransactionTwo.put("4", "2");
			MinCartAmountTransactionTwo.put("5", "10");
			MinCartAmountTransactionTwo.put("6", "false");
			MinCartAmountTransactionTwo.put("7", "false");
			MinCartAmountTransactionTwo.put("8", "false");
			MinCartAmountTransactionTwo.put("9", "false");
			MinCartAmountTransactionTwo.put("10", "false");
			MinCartAmountTransactionTwo.put("11", "false");
			MinCartAmountTransactionTwo.put("12", "false");
			MinCartAmountTransactionTwo.put("13", "false");
			MinCartAmountTransactionTwo.put("14", "false");
			MinCartAmountTransactionTwo.put("15", "false");
			MinCartAmountTransactionTwo.put("16", "false");
			MinCartAmountTransactionTwo.put("17", "1518934320000");
			MinCartAmountTransactionTwo.put("18", "1519279920000");
			MinCartAmountTransactionTwo.put("19", "ankita.yadav@swiggy.in");
			MinCartAmountTransactionTwo.put("20", "1518934320000");
			MinCartAmountTransactionTwo.put("21", "1518934320000");
			MinCartAmountTransactionTwo.put("22", "null");
			MinCartAmountTransactionTwo.put("23", "null");
			MinCartAmountTransactionTwo.put("24", "null");
			MinCartAmountTransactionTwo.put("25", "-1");
			MinCartAmountTransactionTwo.put("26", "false");
			MinCartAmountTransactionTwo.put("27", "0");
			MinCartAmountTransactionTwo.put("28", "null");
			MinCartAmountTransactionTwo.put("29", "null");
			MinCartAmountTransactionTwo.put("30", "null");
			MinCartAmountTransactionTwo.put("31", "null");
			MinCartAmountTransactionTwo.put("32", "1");
			MinCartAmountTransactionTwo.put("33", "Discount");
			MinCartAmountTransactionTwo.put("34", "10");
			MinCartAmountTransactionTwo.put("35", "1");
			MinCartAmountTransactionTwo.put("36", "50");
			MinCartAmountTransactionTwo.put("37", "10");
			MinCartAmountTransactionTwo.put("38", "90");
			MinCartAmountTransactionTwo.put("39", "false");
			MinCartAmountTransactionTwo.put("40", "0");
			MinCartAmountTransactionTwo.put("41", "-1");
			MinCartAmountTransactionTwo.put("42", "1st transaction desc.");
			MinCartAmountTransactionTwo.put("43", "1st transaction");
			MinCartAmountTransactionTwo.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			MinCartAmountTransactionTwo.put("45", "w2fh4c46d0bszhl89voy");
			MinCartAmountTransactionTwo.put("46", "false");
			MinCartAmountTransactionTwo.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			MinCartAmountTransactionTwo.put("48", "ANDROID");
			MinCartAmountTransactionTwo.put("49", "2");
			MinCartAmountTransactionTwo.put("50", "2");
			MinCartAmountTransactionTwo.put("51", "Discount");
			MinCartAmountTransactionTwo.put("52", "10");
			MinCartAmountTransactionTwo.put("53", "1");
			MinCartAmountTransactionTwo.put("54", "50");
			MinCartAmountTransactionTwo.put("55", "10");
			MinCartAmountTransactionTwo.put("56", "90");
			MinCartAmountTransactionTwo.put("57", "false");
			MinCartAmountTransactionTwo.put("58", "0");
			MinCartAmountTransactionTwo.put("59", "-1");
			MinCartAmountTransactionTwo.put("60", "1st transaction desc.");
			MinCartAmountTransactionTwo.put("61", "1st transaction");
			MinCartAmountTransactionTwo.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			MinCartAmountTransactionTwo.put("63", "w2fh4c46d0bszhl89voy");
			MinCartAmountTransactionTwo.put("64", "false");
			MinCartAmountTransactionTwo.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			MinCartAmountTransactionTwo.put("66", "ANDROID");
			MinCartAmountTransactionTwo.put("67", "2");
			MinCartAmountTransactionTwo.put("68", "false");
			MinCartAmountTransactionTwo.put("69", "false");
			MinCartAmountTransactionTwo.put("70", "false");

			return new Object[][] { { MinCartAmount, duplicateType, transactionOneKey, transactionOneMinCartAmount },
					{ MinCartAmountTransactionTwo, duplicateType, transactionTwoKey, transactionTwoMinCartAmount } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponCartMinQuantityData")
	public static Object[][] updateMultiApplyCouponCartMinQuantityData() {
		{
			HashMap<String, String> cartMinQty = new HashMap<String, String>();
			HashMap<String, String> cartMinQtyTransactionTwo = new HashMap<String, String>();
			String duplicateType = "false";
			String transactionOneKey = "35";
			String transactionOneCartMinQuantity = "2";
			String transactionTwoKey = "53";
			String transactionTwoCartMinQuantity = "2";

			cartMinQty.put("0", "ALPHACODE");
			cartMinQty.put("1", "MULTICOUPON");
			cartMinQty.put("2", "Internal");
			cartMinQty.put("3", "AUTOMATION");
			cartMinQty.put("4", "2");
			cartMinQty.put("5", "10");
			cartMinQty.put("6", "false");
			cartMinQty.put("7", "false");
			cartMinQty.put("8", "false");
			cartMinQty.put("9", "false");
			cartMinQty.put("10", "false");
			cartMinQty.put("11", "false");
			cartMinQty.put("12", "false");
			cartMinQty.put("13", "false");
			cartMinQty.put("14", "false");
			cartMinQty.put("15", "false");
			cartMinQty.put("16", "false");
			cartMinQty.put("17", "1518934320000");
			cartMinQty.put("18", "1519279920000");
			cartMinQty.put("19", "ankita.yadav@swiggy.in");
			cartMinQty.put("20", "1518934320000");
			cartMinQty.put("21", "1518934320000");
			cartMinQty.put("22", "null");
			cartMinQty.put("23", "null");
			cartMinQty.put("24", "null");
			cartMinQty.put("25", "-1");
			cartMinQty.put("26", "false");
			cartMinQty.put("27", "0");
			cartMinQty.put("28", "null");
			cartMinQty.put("29", "null");
			cartMinQty.put("30", "null");
			cartMinQty.put("31", "null");
			cartMinQty.put("32", "1");
			cartMinQty.put("33", "Discount");
			cartMinQty.put("34", "10");
			cartMinQty.put("35", "1");
			cartMinQty.put("36", "50");
			cartMinQty.put("37", "10");
			cartMinQty.put("38", "90");
			cartMinQty.put("39", "false");
			cartMinQty.put("40", "0");
			cartMinQty.put("41", "-1");
			cartMinQty.put("42", "1st transaction desc.");
			cartMinQty.put("43", "1st transaction");
			cartMinQty.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			cartMinQty.put("45", "w2fh4c46d0bszhl89voy");
			cartMinQty.put("46", "false");
			cartMinQty.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			cartMinQty.put("48", "ANDROID");
			cartMinQty.put("49", "2");
			cartMinQty.put("50", "2");
			cartMinQty.put("51", "Discount");
			cartMinQty.put("52", "10");
			cartMinQty.put("53", "1");
			cartMinQty.put("54", "50");
			cartMinQty.put("55", "10");
			cartMinQty.put("56", "90");
			cartMinQty.put("57", "false");
			cartMinQty.put("58", "0");
			cartMinQty.put("59", "-1");
			cartMinQty.put("60", "1st transaction desc.");
			cartMinQty.put("61", "1st transaction");
			cartMinQty.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			cartMinQty.put("63", "w2fh4c46d0bszhl89voy");
			cartMinQty.put("64", "false");
			cartMinQty.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			cartMinQty.put("66", "ANDROID");
			cartMinQty.put("67", "2");
			cartMinQty.put("68", "false");
			cartMinQty.put("69", "false");
			cartMinQty.put("70", "false");

			cartMinQtyTransactionTwo.put("0", "ALPHACODE");
			cartMinQtyTransactionTwo.put("1", "MULTICOUPON");
			cartMinQtyTransactionTwo.put("2", "Internal");
			cartMinQtyTransactionTwo.put("3", "AUTOMATION");
			cartMinQtyTransactionTwo.put("4", "2");
			cartMinQtyTransactionTwo.put("5", "10");
			cartMinQtyTransactionTwo.put("6", "false");
			cartMinQtyTransactionTwo.put("7", "false");
			cartMinQtyTransactionTwo.put("8", "false");
			cartMinQtyTransactionTwo.put("9", "false");
			cartMinQtyTransactionTwo.put("10", "false");
			cartMinQtyTransactionTwo.put("11", "false");
			cartMinQtyTransactionTwo.put("12", "false");
			cartMinQtyTransactionTwo.put("13", "false");
			cartMinQtyTransactionTwo.put("14", "false");
			cartMinQtyTransactionTwo.put("15", "false");
			cartMinQtyTransactionTwo.put("16", "false");
			cartMinQtyTransactionTwo.put("17", "1518934320000");
			cartMinQtyTransactionTwo.put("18", "1519279920000");
			cartMinQtyTransactionTwo.put("19", "ankita.yadav@swiggy.in");
			cartMinQtyTransactionTwo.put("20", "1518934320000");
			cartMinQtyTransactionTwo.put("21", "1518934320000");
			cartMinQtyTransactionTwo.put("22", "null");
			cartMinQtyTransactionTwo.put("23", "null");
			cartMinQtyTransactionTwo.put("24", "null");
			cartMinQtyTransactionTwo.put("25", "-1");
			cartMinQtyTransactionTwo.put("26", "false");
			cartMinQtyTransactionTwo.put("27", "0");
			cartMinQtyTransactionTwo.put("28", "null");
			cartMinQtyTransactionTwo.put("29", "null");
			cartMinQtyTransactionTwo.put("30", "null");
			cartMinQtyTransactionTwo.put("31", "null");
			cartMinQtyTransactionTwo.put("32", "1");
			cartMinQtyTransactionTwo.put("33", "Discount");
			cartMinQtyTransactionTwo.put("34", "10");
			cartMinQtyTransactionTwo.put("35", "1");
			cartMinQtyTransactionTwo.put("36", "50");
			cartMinQtyTransactionTwo.put("37", "10");
			cartMinQtyTransactionTwo.put("38", "90");
			cartMinQtyTransactionTwo.put("39", "false");
			cartMinQtyTransactionTwo.put("40", "0");
			cartMinQtyTransactionTwo.put("41", "-1");
			cartMinQtyTransactionTwo.put("42", "1st transaction desc.");
			cartMinQtyTransactionTwo.put("43", "1st transaction");
			cartMinQtyTransactionTwo.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			cartMinQtyTransactionTwo.put("45", "w2fh4c46d0bszhl89voy");
			cartMinQtyTransactionTwo.put("46", "false");
			cartMinQtyTransactionTwo.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			cartMinQtyTransactionTwo.put("48", "ANDROID");
			cartMinQtyTransactionTwo.put("49", "2");
			cartMinQtyTransactionTwo.put("50", "2");
			cartMinQtyTransactionTwo.put("51", "Discount");
			cartMinQtyTransactionTwo.put("52", "10");
			cartMinQtyTransactionTwo.put("53", "1");
			cartMinQtyTransactionTwo.put("54", "50");
			cartMinQtyTransactionTwo.put("55", "10");
			cartMinQtyTransactionTwo.put("56", "90");
			cartMinQtyTransactionTwo.put("57", "false");
			cartMinQtyTransactionTwo.put("58", "0");
			cartMinQtyTransactionTwo.put("59", "-1");
			cartMinQtyTransactionTwo.put("60", "1st transaction desc.");
			cartMinQtyTransactionTwo.put("61", "1st transaction");
			cartMinQtyTransactionTwo.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			cartMinQtyTransactionTwo.put("63", "w2fh4c46d0bszhl89voy");
			cartMinQtyTransactionTwo.put("64", "false");
			cartMinQtyTransactionTwo.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			cartMinQtyTransactionTwo.put("66", "ANDROID");
			cartMinQtyTransactionTwo.put("67", "2");
			cartMinQtyTransactionTwo.put("68", "false");
			cartMinQtyTransactionTwo.put("69", "false");
			cartMinQtyTransactionTwo.put("70", "false");

			return new Object[][] { { cartMinQty, duplicateType, transactionOneKey, transactionOneCartMinQuantity },
					{ cartMinQtyTransactionTwo, duplicateType, transactionTwoKey, transactionTwoCartMinQuantity } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponTncData")
	public static Object[][] updateMultiApplyCouponTncData() {
		{
			HashMap<String, String> tncTransactionOne = new HashMap<String, String>();
			HashMap<String, String> tncTransactionTwo = new HashMap<String, String>();
			String duplicateType = "false";
			String transactionOneKey = "44";
			String transactionOnetnc = "\"update1\",\"update1\",\"update1\"";
			String transactionTwoKey = "62";
			String transactionTwotnc = "\"update2\",\"update2\",\"update2\"";

			tncTransactionOne.put("0", "ALPHACODE");
			tncTransactionOne.put("1", "MULTICOUPON");
			tncTransactionOne.put("2", "Internal");
			tncTransactionOne.put("3", "AUTOMATION");
			tncTransactionOne.put("4", "2");
			tncTransactionOne.put("5", "10");
			tncTransactionOne.put("6", "false");
			tncTransactionOne.put("7", "false");
			tncTransactionOne.put("8", "false");
			tncTransactionOne.put("9", "false");
			tncTransactionOne.put("10", "false");
			tncTransactionOne.put("11", "false");
			tncTransactionOne.put("12", "false");
			tncTransactionOne.put("13", "false");
			tncTransactionOne.put("14", "false");
			tncTransactionOne.put("15", "false");
			tncTransactionOne.put("16", "false");
			tncTransactionOne.put("17", "1518934320000");
			tncTransactionOne.put("18", "1519279920000");
			tncTransactionOne.put("19", "ankita.yadav@swiggy.in");
			tncTransactionOne.put("20", "1518934320000");
			tncTransactionOne.put("21", "1518934320000");
			tncTransactionOne.put("22", "null");
			tncTransactionOne.put("23", "null");
			tncTransactionOne.put("24", "null");
			tncTransactionOne.put("25", "-1");
			tncTransactionOne.put("26", "false");
			tncTransactionOne.put("27", "0");
			tncTransactionOne.put("28", "null");
			tncTransactionOne.put("29", "null");
			tncTransactionOne.put("30", "null");
			tncTransactionOne.put("31", "null");
			tncTransactionOne.put("32", "1");
			tncTransactionOne.put("33", "Discount");
			tncTransactionOne.put("34", "10");
			tncTransactionOne.put("35", "1");
			tncTransactionOne.put("36", "50");
			tncTransactionOne.put("37", "10");
			tncTransactionOne.put("38", "90");
			tncTransactionOne.put("39", "false");
			tncTransactionOne.put("40", "0");
			tncTransactionOne.put("41", "-1");
			tncTransactionOne.put("42", "1st transaction desc.");
			tncTransactionOne.put("43", "1st transaction");
			tncTransactionOne.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			tncTransactionOne.put("45", "w2fh4c46d0bszhl89voy");
			tncTransactionOne.put("46", "false");
			tncTransactionOne.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			tncTransactionOne.put("48", "ANDROID");
			tncTransactionOne.put("49", "2");
			tncTransactionOne.put("50", "2");
			tncTransactionOne.put("51", "Discount");
			tncTransactionOne.put("52", "10");
			tncTransactionOne.put("53", "1");
			tncTransactionOne.put("54", "50");
			tncTransactionOne.put("55", "10");
			tncTransactionOne.put("56", "90");
			tncTransactionOne.put("57", "false");
			tncTransactionOne.put("58", "0");
			tncTransactionOne.put("59", "-1");
			tncTransactionOne.put("60", "1st transaction desc.");
			tncTransactionOne.put("61", "1st transaction");
			tncTransactionOne.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			tncTransactionOne.put("63", "w2fh4c46d0bszhl89voy");
			tncTransactionOne.put("64", "false");
			tncTransactionOne.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			tncTransactionOne.put("66", "ANDROID");
			tncTransactionOne.put("67", "2");
			tncTransactionOne.put("68", "false");
			tncTransactionOne.put("69", "false");
			tncTransactionOne.put("70", "false");

			tncTransactionTwo.put("0", "ALPHACODE");
			tncTransactionTwo.put("1", "MULTICOUPON");
			tncTransactionTwo.put("2", "Internal");
			tncTransactionTwo.put("3", "AUTOMATION");
			tncTransactionTwo.put("4", "2");
			tncTransactionTwo.put("5", "10");
			tncTransactionTwo.put("6", "false");
			tncTransactionTwo.put("7", "false");
			tncTransactionTwo.put("8", "false");
			tncTransactionTwo.put("9", "false");
			tncTransactionTwo.put("10", "false");
			tncTransactionTwo.put("11", "false");
			tncTransactionTwo.put("12", "false");
			tncTransactionTwo.put("13", "false");
			tncTransactionTwo.put("14", "false");
			tncTransactionTwo.put("15", "false");
			tncTransactionTwo.put("16", "false");
			tncTransactionTwo.put("17", "1518934320000");
			tncTransactionTwo.put("18", "1519279920000");
			tncTransactionTwo.put("19", "ankita.yadav@swiggy.in");
			tncTransactionTwo.put("20", "1518934320000");
			tncTransactionTwo.put("21", "1518934320000");
			tncTransactionTwo.put("22", "null");
			tncTransactionTwo.put("23", "null");
			tncTransactionTwo.put("24", "null");
			tncTransactionTwo.put("25", "-1");
			tncTransactionTwo.put("26", "false");
			tncTransactionTwo.put("27", "0");
			tncTransactionTwo.put("28", "null");
			tncTransactionTwo.put("29", "null");
			tncTransactionTwo.put("30", "null");
			tncTransactionTwo.put("31", "null");
			tncTransactionTwo.put("32", "1");
			tncTransactionTwo.put("33", "Discount");
			tncTransactionTwo.put("34", "10");
			tncTransactionTwo.put("35", "1");
			tncTransactionTwo.put("36", "50");
			tncTransactionTwo.put("37", "10");
			tncTransactionTwo.put("38", "90");
			tncTransactionTwo.put("39", "false");
			tncTransactionTwo.put("40", "0");
			tncTransactionTwo.put("41", "-1");
			tncTransactionTwo.put("42", "1st transaction desc.");
			tncTransactionTwo.put("43", "1st transaction");
			tncTransactionTwo.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			tncTransactionTwo.put("45", "w2fh4c46d0bszhl89voy");
			tncTransactionTwo.put("46", "false");
			tncTransactionTwo.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			tncTransactionTwo.put("48", "ANDROID");
			tncTransactionTwo.put("49", "2");
			tncTransactionTwo.put("50", "2");
			tncTransactionTwo.put("51", "Discount");
			tncTransactionTwo.put("52", "10");
			tncTransactionTwo.put("53", "1");
			tncTransactionTwo.put("54", "50");
			tncTransactionTwo.put("55", "10");
			tncTransactionTwo.put("56", "90");
			tncTransactionTwo.put("57", "false");
			tncTransactionTwo.put("58", "0");
			tncTransactionTwo.put("59", "-1");
			tncTransactionTwo.put("60", "1st transaction desc.");
			tncTransactionTwo.put("61", "1st transaction");
			tncTransactionTwo.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			tncTransactionTwo.put("63", "w2fh4c46d0bszhl89voy");
			tncTransactionTwo.put("64", "false");
			tncTransactionTwo.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			tncTransactionTwo.put("66", "ANDROID");
			tncTransactionTwo.put("67", "2");
			tncTransactionTwo.put("68", "false");
			tncTransactionTwo.put("69", "false");
			tncTransactionTwo.put("70", "false");

			return new Object[][] { { tncTransactionOne, duplicateType, transactionOneKey, transactionOnetnc },
					{ tncTransactionOne, duplicateType, transactionTwoKey, transactionTwotnc } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponDiscountPercentData")
	public static Object[][] updateMultiApplyCouponDiscountPercentData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			HashMap<String, String> discountPercentUpdate = new HashMap<String, String>();
			String duplicateType = "false";
			String updateTransactionOneKey = "37";
			String discountPercentTransactionOne = "15.0";
			String updateTransactionTwoKey = "55";
			String discountPercentTransactionTwo = "0.0";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");

			discountPercentUpdate.put("0", "ALPHACODE");
			discountPercentUpdate.put("1", "MULTICOUPON");
			discountPercentUpdate.put("2", "Internal");
			discountPercentUpdate.put("3", "AUTOMATION");
			discountPercentUpdate.put("4", "2");
			discountPercentUpdate.put("5", "10");
			discountPercentUpdate.put("6", "false");
			discountPercentUpdate.put("7", "false");
			discountPercentUpdate.put("8", "false");
			discountPercentUpdate.put("9", "false");
			discountPercentUpdate.put("10", "false");
			discountPercentUpdate.put("11", "false");
			discountPercentUpdate.put("12", "false");
			discountPercentUpdate.put("13", "false");
			discountPercentUpdate.put("14", "false");
			discountPercentUpdate.put("15", "false");
			discountPercentUpdate.put("16", "false");
			discountPercentUpdate.put("17", "1518934320000");
			discountPercentUpdate.put("18", "1519279920000");
			discountPercentUpdate.put("19", "ankita.yadav@swiggy.in");
			discountPercentUpdate.put("20", "1518934320000");
			discountPercentUpdate.put("21", "1518934320000");
			discountPercentUpdate.put("22", "null");
			discountPercentUpdate.put("23", "null");
			discountPercentUpdate.put("24", "null");
			discountPercentUpdate.put("25", "-1");
			discountPercentUpdate.put("26", "false");
			discountPercentUpdate.put("27", "0");
			discountPercentUpdate.put("28", "null");
			discountPercentUpdate.put("29", "null");
			discountPercentUpdate.put("30", "null");
			discountPercentUpdate.put("31", "null");
			discountPercentUpdate.put("32", "1");
			discountPercentUpdate.put("33", "Discount");
			discountPercentUpdate.put("34", "10");
			discountPercentUpdate.put("35", "1");
			discountPercentUpdate.put("36", "50");
			discountPercentUpdate.put("37", "10");
			discountPercentUpdate.put("38", "90");
			discountPercentUpdate.put("39", "false");
			discountPercentUpdate.put("40", "0");
			discountPercentUpdate.put("41", "-1");
			discountPercentUpdate.put("42", "1st transaction desc.");
			discountPercentUpdate.put("43", "1st transaction");
			discountPercentUpdate.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			discountPercentUpdate.put("45", "w2fh4c46d0bszhl89voy");
			discountPercentUpdate.put("46", "false");
			discountPercentUpdate.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			discountPercentUpdate.put("48", "ANDROID");
			discountPercentUpdate.put("49", "2");
			discountPercentUpdate.put("50", "2");
			discountPercentUpdate.put("51", "Discount");
			discountPercentUpdate.put("52", "10");
			discountPercentUpdate.put("53", "1");
			discountPercentUpdate.put("54", "50");
			discountPercentUpdate.put("55", "10");
			discountPercentUpdate.put("56", "90");
			discountPercentUpdate.put("57", "false");
			discountPercentUpdate.put("58", "0");
			discountPercentUpdate.put("59", "-1");
			discountPercentUpdate.put("60", "1st transaction desc.");
			discountPercentUpdate.put("61", "1st transaction");
			discountPercentUpdate.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			discountPercentUpdate.put("63", "w2fh4c46d0bszhl89voy");
			discountPercentUpdate.put("64", "false");
			discountPercentUpdate.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			discountPercentUpdate.put("66", "ANDROID");
			discountPercentUpdate.put("67", "2");
			discountPercentUpdate.put("68", "false");
			discountPercentUpdate.put("69", "false");
			discountPercentUpdate.put("70", "false");
			return new Object[][] { { hashMap, duplicateType, updateTransactionOneKey, discountPercentTransactionOne },
					{ discountPercentUpdate, duplicateType, updateTransactionTwoKey, discountPercentTransactionTwo } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponDiscountAmountData")
	public static Object[][] updateMultiApplyCouponDiscountAmountData() {
		{
			HashMap<String, String> discountAmount = new HashMap<String, String>();
			String duplicateType = "false";
			String updateTransactionOneKey = "36";
			String discountAmountTransactionOne = "175";
			String updateTransactionTwoKey = "54";
			String discountAmountTransactionTwo = "100";

			discountAmount.put("0", "ALPHACODE");
			discountAmount.put("1", "MULTICOUPON");
			discountAmount.put("2", "Internal");
			discountAmount.put("3", "AUTOMATION");
			discountAmount.put("4", "2");
			discountAmount.put("5", "10");
			discountAmount.put("6", "false");
			discountAmount.put("7", "false");
			discountAmount.put("8", "false");
			discountAmount.put("9", "false");
			discountAmount.put("10", "false");
			discountAmount.put("11", "false");
			discountAmount.put("12", "false");
			discountAmount.put("13", "false");
			discountAmount.put("14", "false");
			discountAmount.put("15", "false");
			discountAmount.put("16", "false");
			discountAmount.put("17", "1518934320000");
			discountAmount.put("18", "1519279920000");
			discountAmount.put("19", "ankita.yadav@swiggy.in");
			discountAmount.put("20", "1518934320000");
			discountAmount.put("21", "1518934320000");
			discountAmount.put("22", "null");
			discountAmount.put("23", "null");
			discountAmount.put("24", "null");
			discountAmount.put("25", "-1");
			discountAmount.put("26", "false");
			discountAmount.put("27", "0");
			discountAmount.put("28", "null");
			discountAmount.put("29", "null");
			discountAmount.put("30", "null");
			discountAmount.put("31", "null");
			discountAmount.put("32", "1");
			discountAmount.put("33", "Discount");
			discountAmount.put("34", "10");
			discountAmount.put("35", "1");
			discountAmount.put("36", "50");
			discountAmount.put("37", "10");
			discountAmount.put("38", "90");
			discountAmount.put("39", "false");
			discountAmount.put("40", "0");
			discountAmount.put("41", "-1");
			discountAmount.put("42", "1st transaction desc.");
			discountAmount.put("43", "1st transaction");
			discountAmount.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			discountAmount.put("45", "w2fh4c46d0bszhl89voy");
			discountAmount.put("46", "false");
			discountAmount.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			discountAmount.put("48", "ANDROID");
			discountAmount.put("49", "2");
			discountAmount.put("50", "2");
			discountAmount.put("51", "Discount");
			discountAmount.put("52", "10");
			discountAmount.put("53", "1");
			discountAmount.put("54", "50");
			discountAmount.put("55", "10");
			discountAmount.put("56", "90");
			discountAmount.put("57", "false");
			discountAmount.put("58", "0");
			discountAmount.put("59", "-1");
			discountAmount.put("60", "1st transaction desc.");
			discountAmount.put("61", "1st transaction");
			discountAmount.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			discountAmount.put("63", "w2fh4c46d0bszhl89voy");
			discountAmount.put("64", "false");
			discountAmount.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			discountAmount.put("66", "ANDROID");
			discountAmount.put("67", "2");
			discountAmount.put("68", "false");
			discountAmount.put("69", "false");
			discountAmount.put("70", "false");

			return new Object[][] { { discountAmount, duplicateType, updateTransactionOneKey,
					discountAmountTransactionOne, updateTransactionTwoKey, discountAmountTransactionTwo } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponUpperCapData")
	public static Object[][] updateMultiApplyCouponUpperCapData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";
			String updateTransactionOneKey = "38";
			String upperCapTransactionOne = "220.0";
			String updateTransactionTwoKey = "56";
			String upperCapTransactionTwo = "320.0";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType, updateTransactionOneKey, upperCapTransactionOne,
					updateTransactionTwoKey, upperCapTransactionTwo } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponDiscountItemData")
	public static Object[][] updateMultiApplyCouponDiscountItemData() {
		{
			HashMap<String, String> discountItem = new HashMap<String, String>();
			String duplicateType = "false";
			String updateTransactionOneKey = "41";
			String discountItemTransactionOne = "0";
			String updateTransactionTwoKey = "59";
			String discountItemTransactionTwo = "-1";

			discountItem.put("0", "ALPHACODE");
			discountItem.put("1", "MULTICOUPON");
			discountItem.put("2", "Internal");
			discountItem.put("3", "AUTOMATION");
			discountItem.put("4", "2");
			discountItem.put("5", "10");
			discountItem.put("6", "false");
			discountItem.put("7", "false");
			discountItem.put("8", "false");
			discountItem.put("9", "false");
			discountItem.put("10", "false");
			discountItem.put("11", "false");
			discountItem.put("12", "false");
			discountItem.put("13", "false");
			discountItem.put("14", "false");
			discountItem.put("15", "false");
			discountItem.put("16", "false");
			discountItem.put("17", "1518934320000");
			discountItem.put("18", "1519279920000");
			discountItem.put("19", "ankita.yadav@swiggy.in");
			discountItem.put("20", "1518934320000");
			discountItem.put("21", "1518934320000");
			discountItem.put("22", "null");
			discountItem.put("23", "null");
			discountItem.put("24", "null");
			discountItem.put("25", "-1");
			discountItem.put("26", "false");
			discountItem.put("27", "0");
			discountItem.put("28", "null");
			discountItem.put("29", "null");
			discountItem.put("30", "null");
			discountItem.put("31", "null");
			discountItem.put("32", "1");
			discountItem.put("33", "Discount");
			discountItem.put("34", "10");
			discountItem.put("35", "1");
			discountItem.put("36", "50");
			discountItem.put("37", "10");
			discountItem.put("38", "90");
			discountItem.put("39", "false");
			discountItem.put("40", "0");
			discountItem.put("41", "-1");
			discountItem.put("42", "1st transaction desc.");
			discountItem.put("43", "1st transaction");
			discountItem.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			discountItem.put("45", "w2fh4c46d0bszhl89voy");
			discountItem.put("46", "false");
			discountItem.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			discountItem.put("48", "ANDROID");
			discountItem.put("49", "2");
			discountItem.put("50", "2");
			discountItem.put("51", "Discount");
			discountItem.put("52", "10");
			discountItem.put("53", "1");
			discountItem.put("54", "50");
			discountItem.put("55", "10");
			discountItem.put("56", "90");
			discountItem.put("57", "false");
			discountItem.put("58", "0");
			discountItem.put("59", "0");
			discountItem.put("60", "1st transaction desc.");
			discountItem.put("61", "1st transaction");
			discountItem.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			discountItem.put("63", "w2fh4c46d0bszhl89voy");
			discountItem.put("64", "false");
			discountItem.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			discountItem.put("66", "ANDROID");
			discountItem.put("67", "2");
			discountItem.put("68", "false");
			discountItem.put("69", "false");
			discountItem.put("70", "false");

			return new Object[][] { { discountItem, duplicateType, updateTransactionOneKey, discountItemTransactionOne,
					updateTransactionTwoKey, discountItemTransactionTwo } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponFreeShippingData")
	public static Object[][] updateMultiApplyCouponFreeShippingData() {
		{
			HashMap<String, String> freeShipping = new HashMap<String, String>();
			String duplicateType = "false";
			String updateTransactionOneKey = "39";
			String freeShippingTransactionOne = "true";
			String updateTransactionTwoKey = "57";
			String freeShippingTransactionTwo = "false";

			freeShipping.put("0", "ALPHACODE");
			freeShipping.put("1", "MULTICOUPON");
			freeShipping.put("2", "Internal");
			freeShipping.put("3", "AUTOMATION");
			freeShipping.put("4", "2");
			freeShipping.put("5", "10");
			freeShipping.put("6", "false");
			freeShipping.put("7", "false");
			freeShipping.put("8", "false");
			freeShipping.put("9", "false");
			freeShipping.put("10", "false");
			freeShipping.put("11", "false");
			freeShipping.put("12", "false");
			freeShipping.put("13", "false");
			freeShipping.put("14", "false");
			freeShipping.put("15", "false");
			freeShipping.put("16", "false");
			freeShipping.put("17", "1518934320000");
			freeShipping.put("18", "1519279920000");
			freeShipping.put("19", "ankita.yadav@swiggy.in");
			freeShipping.put("20", "1518934320000");
			freeShipping.put("21", "1518934320000");
			freeShipping.put("22", "null");
			freeShipping.put("23", "null");
			freeShipping.put("24", "null");
			freeShipping.put("25", "-1");
			freeShipping.put("26", "false");
			freeShipping.put("27", "0");
			freeShipping.put("28", "null");
			freeShipping.put("29", "null");
			freeShipping.put("30", "null");
			freeShipping.put("31", "null");
			freeShipping.put("32", "1");
			freeShipping.put("33", "Discount");
			freeShipping.put("34", "10");
			freeShipping.put("35", "1");
			freeShipping.put("36", "50");
			freeShipping.put("37", "10");
			freeShipping.put("38", "90");
			freeShipping.put("39", "false");
			freeShipping.put("40", "0");
			freeShipping.put("41", "-1");
			freeShipping.put("42", "1st transaction desc.");
			freeShipping.put("43", "1st transaction");
			freeShipping.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			freeShipping.put("45", "w2fh4c46d0bszhl89voy");
			freeShipping.put("46", "false");
			freeShipping.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			freeShipping.put("48", "ANDROID");
			freeShipping.put("49", "2");
			freeShipping.put("50", "2");
			freeShipping.put("51", "Discount");
			freeShipping.put("52", "10");
			freeShipping.put("53", "1");
			freeShipping.put("54", "50");
			freeShipping.put("55", "10");
			freeShipping.put("56", "90");
			freeShipping.put("57", "true");
			freeShipping.put("58", "0");
			freeShipping.put("59", "-1");
			freeShipping.put("60", "1st transaction desc.");
			freeShipping.put("61", "1st transaction");
			freeShipping.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			freeShipping.put("63", "w2fh4c46d0bszhl89voy");
			freeShipping.put("64", "false");
			freeShipping.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			freeShipping.put("66", "ANDROID");
			freeShipping.put("67", "2");
			freeShipping.put("68", "false");
			freeShipping.put("69", "false");
			freeShipping.put("70", "false");

			return new Object[][] { { freeShipping, duplicateType, updateTransactionOneKey, freeShippingTransactionOne,
					updateTransactionTwoKey, freeShippingTransactionTwo } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponFirstOrderRestrictionData")
	public static Object[][] updateMultiApplyCouponFirstOrderRestrictionData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";
			String updateTransactionOneKey = "46";
			String firstOrderRestrictionTransactionOne = "true";
			String firstOrderRestrictionUpdateAgain = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType, updateTransactionOneKey,
					firstOrderRestrictionTransactionOne, firstOrderRestrictionUpdateAgain } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponPreferredPaymentMethodData")
	public static Object[][] updateMultiApplyCouponPreferredPaymentMethodData() {
		{
			HashMap<String, String> preferredPaymentMethod = new HashMap<String, String>();
			String duplicateType = "false";
			String updateTransactionOneKey = "47";
			String preferredPaymentMethodTransactionOne = "\"PayTM\",\"Cash\",\"Juspay-NB\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"";
			String updateTransactionTwoKey = "65";
			String preferredPaymentMethodTransactionTwo = "\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"";

			preferredPaymentMethod.put("0", "ALPHACODE");
			preferredPaymentMethod.put("1", "MULTICOUPON");
			preferredPaymentMethod.put("2", "Internal");
			preferredPaymentMethod.put("3", "AUTOMATION");
			preferredPaymentMethod.put("4", "2");
			preferredPaymentMethod.put("5", "10");
			preferredPaymentMethod.put("6", "false");
			preferredPaymentMethod.put("7", "false");
			preferredPaymentMethod.put("8", "false");
			preferredPaymentMethod.put("9", "false");
			preferredPaymentMethod.put("10", "false");
			preferredPaymentMethod.put("11", "false");
			preferredPaymentMethod.put("12", "false");
			preferredPaymentMethod.put("13", "false");
			preferredPaymentMethod.put("14", "false");
			preferredPaymentMethod.put("15", "false");
			preferredPaymentMethod.put("16", "false");
			preferredPaymentMethod.put("17", "1518934320000");
			preferredPaymentMethod.put("18", "1519279920000");
			preferredPaymentMethod.put("19", "ankita.yadav@swiggy.in");
			preferredPaymentMethod.put("20", "1518934320000");
			preferredPaymentMethod.put("21", "1518934320000");
			preferredPaymentMethod.put("22", "null");
			preferredPaymentMethod.put("23", "null");
			preferredPaymentMethod.put("24", "null");
			preferredPaymentMethod.put("25", "-1");
			preferredPaymentMethod.put("26", "false");
			preferredPaymentMethod.put("27", "0");
			preferredPaymentMethod.put("28", "null");
			preferredPaymentMethod.put("29", "null");
			preferredPaymentMethod.put("30", "null");
			preferredPaymentMethod.put("31", "null");
			preferredPaymentMethod.put("32", "1");
			preferredPaymentMethod.put("33", "Discount");
			preferredPaymentMethod.put("34", "10");
			preferredPaymentMethod.put("35", "1");
			preferredPaymentMethod.put("36", "50");
			preferredPaymentMethod.put("37", "10");
			preferredPaymentMethod.put("38", "90");
			preferredPaymentMethod.put("39", "false");
			preferredPaymentMethod.put("40", "0");
			preferredPaymentMethod.put("41", "-1");
			preferredPaymentMethod.put("42", "1st transaction desc.");
			preferredPaymentMethod.put("43", "1st transaction");
			preferredPaymentMethod.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			preferredPaymentMethod.put("45", "w2fh4c46d0bszhl89voy");
			preferredPaymentMethod.put("46", "false");
			preferredPaymentMethod.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			preferredPaymentMethod.put("48", "ANDROID");
			preferredPaymentMethod.put("49", "2");
			preferredPaymentMethod.put("50", "2");
			preferredPaymentMethod.put("51", "Discount");
			preferredPaymentMethod.put("52", "10");
			preferredPaymentMethod.put("53", "1");
			preferredPaymentMethod.put("54", "50");
			preferredPaymentMethod.put("55", "10");
			preferredPaymentMethod.put("56", "90");
			preferredPaymentMethod.put("57", "false");
			preferredPaymentMethod.put("58", "0");
			preferredPaymentMethod.put("59", "-1");
			preferredPaymentMethod.put("60", "1st transaction desc.");
			preferredPaymentMethod.put("61", "1st transaction");
			preferredPaymentMethod.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			preferredPaymentMethod.put("63", "w2fh4c46d0bszhl89voy");
			preferredPaymentMethod.put("64", "false");
			preferredPaymentMethod.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			preferredPaymentMethod.put("66", "ANDROID");
			preferredPaymentMethod.put("67", "2");
			preferredPaymentMethod.put("68", "false");
			preferredPaymentMethod.put("69", "false");
			preferredPaymentMethod.put("70", "false");
			return new Object[][] { { preferredPaymentMethod, duplicateType, updateTransactionOneKey,
					preferredPaymentMethodTransactionOne, updateTransactionTwoKey,
					preferredPaymentMethodTransactionTwo } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponUserClientData")
	public static Object[][] updateMultiApplyCouponUserClientData() {
		{
			HashMap<String, String> userClient = new HashMap<String, String>();
			String duplicateType = "false";
			String updateTransactionOneKey = "40";
			String userClientTransactionOne = "1";
			String updateTransactionTwoKey = "58";
			String userClientTransactionTwo = "2";
			String userClientTransactionThree = "3";
			String userClientTransactionFour = "4";
			String userClientTransactionZero = "0";

			userClient.put("0", "ALPHACODE");
			userClient.put("1", "MULTICOUPON");
			userClient.put("2", "Internal");
			userClient.put("3", "AUTOMATION");
			userClient.put("4", "2");
			userClient.put("5", "10");
			userClient.put("6", "false");
			userClient.put("7", "false");
			userClient.put("8", "false");
			userClient.put("9", "false");
			userClient.put("10", "false");
			userClient.put("11", "false");
			userClient.put("12", "false");
			userClient.put("13", "false");
			userClient.put("14", "false");
			userClient.put("15", "false");
			userClient.put("16", "false");
			userClient.put("17", "1518934320000");
			userClient.put("18", "1519279920000");
			userClient.put("19", "ankita.yadav@swiggy.in");
			userClient.put("20", "1518934320000");
			userClient.put("21", "1518934320000");
			userClient.put("22", "null");
			userClient.put("23", "null");
			userClient.put("24", "null");
			userClient.put("25", "-1");
			userClient.put("26", "false");
			userClient.put("27", "0");
			userClient.put("28", "null");
			userClient.put("29", "null");
			userClient.put("30", "null");
			userClient.put("31", "null");
			userClient.put("32", "1");
			userClient.put("33", "Discount");
			userClient.put("34", "10");
			userClient.put("35", "1");
			userClient.put("36", "50");
			userClient.put("37", "10");
			userClient.put("38", "90");
			userClient.put("39", "false");
			userClient.put("40", "0");
			userClient.put("41", "-1");
			userClient.put("42", "1st transaction desc.");
			userClient.put("43", "1st transaction");
			userClient.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			userClient.put("45", "w2fh4c46d0bszhl89voy");
			userClient.put("46", "false");
			userClient.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			userClient.put("48", "ANDROID");
			userClient.put("49", "2");
			userClient.put("50", "2");
			userClient.put("51", "Discount");
			userClient.put("52", "10");
			userClient.put("53", "1");
			userClient.put("54", "50");
			userClient.put("55", "10");
			userClient.put("56", "90");
			userClient.put("57", "false");
			userClient.put("58", "0");
			userClient.put("59", "-1");
			userClient.put("60", "1st transaction desc.");
			userClient.put("61", "1st transaction");
			userClient.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			userClient.put("63", "w2fh4c46d0bszhl89voy");
			userClient.put("64", "false");
			userClient.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			userClient.put("66", "ANDROID");
			userClient.put("67", "2");
			userClient.put("68", "false");
			userClient.put("69", "false");
			userClient.put("70", "false");

			return new Object[][] { { userClient, duplicateType, updateTransactionOneKey, userClientTransactionZero,
					updateTransactionTwoKey, userClientTransactionOne, userClientTransactionTwo,
					userClientTransactionThree, userClientTransactionFour } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponOffsetData")
	public static Object[][] updateMultiApplyCouponOffsetData() {
		{
			HashMap<String, String> offset = new HashMap<String, String>();
			String duplicateType = "false";
			String updateTransactionOneKey = "49";
			String offsetTransactionOne = "0";
			String updateTransactionTwoKey = "67";
			String offsetTransactionTwo = "-1";
			String offsetTransactionOneReset = "1";

			offset.put("0", "ALPHACODE");
			offset.put("1", "MULTICOUPON");
			offset.put("2", "Internal");
			offset.put("3", "AUTOMATION");
			offset.put("4", "2");
			offset.put("5", "10");
			offset.put("6", "false");
			offset.put("7", "false");
			offset.put("8", "false");
			offset.put("9", "false");
			offset.put("10", "false");
			offset.put("11", "false");
			offset.put("12", "false");
			offset.put("13", "false");
			offset.put("14", "false");
			offset.put("15", "false");
			offset.put("16", "false");
			offset.put("17", "1518934320000");
			offset.put("18", "1519279920000");
			offset.put("19", "ankita.yadav@swiggy.in");
			offset.put("20", "1518934320000");
			offset.put("21", "1518934320000");
			offset.put("22", "null");
			offset.put("23", "null");
			offset.put("24", "null");
			offset.put("25", "-1");
			offset.put("26", "false");
			offset.put("27", "0");
			offset.put("28", "null");
			offset.put("29", "null");
			offset.put("30", "null");
			offset.put("31", "null");
			offset.put("32", "1");
			offset.put("33", "Discount");
			offset.put("34", "10");
			offset.put("35", "1");
			offset.put("36", "50");
			offset.put("37", "10");
			offset.put("38", "90");
			offset.put("39", "false");
			offset.put("40", "0");
			offset.put("41", "-1");
			offset.put("42", "1st transaction desc.");
			offset.put("43", "1st transaction");
			offset.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			offset.put("45", "w2fh4c46d0bszhl89voy");
			offset.put("46", "false");
			offset.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			offset.put("48", "ANDROID");
			offset.put("49", "2");
			offset.put("50", "2");
			offset.put("51", "Discount");
			offset.put("52", "10");
			offset.put("53", "1");
			offset.put("54", "50");
			offset.put("55", "10");
			offset.put("56", "90");
			offset.put("57", "false");
			offset.put("58", "0");
			offset.put("59", "-1");
			offset.put("60", "1st transaction desc.");
			offset.put("61", "1st transaction");
			offset.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			offset.put("63", "w2fh4c46d0bszhl89voy");
			offset.put("64", "false");
			offset.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			offset.put("66", "ANDROID");
			offset.put("67", "2");
			offset.put("68", "false");
			offset.put("69", "false");
			offset.put("70", "false");
			return new Object[][] { { offset, duplicateType, updateTransactionOneKey, offsetTransactionOne,
					updateTransactionTwoKey, offsetTransactionTwo, offsetTransactionOneReset } };

		}
	}

	@DataProvider(name = "updateMultiApplyCouponForNonExistingCouponCodeData")
	public static Object[][] updateMultiApplyCouponForNonExistingCouponCodeData() {
		{
			HashMap<String, String> updateNonExistingCoupon = new HashMap<String, String>();
			String nonExistingCouponCode = "Eleven";
			String updateKey = "1";
			String updateValue = "Update non existing coupon";

			updateNonExistingCoupon.put("0", "Eleven");
			updateNonExistingCoupon.put("1", "MULTICOUPON");
			updateNonExistingCoupon.put("2", "Internal");
			updateNonExistingCoupon.put("3", "AUTOMATION");
			updateNonExistingCoupon.put("4", "2");
			updateNonExistingCoupon.put("5", "10");
			updateNonExistingCoupon.put("6", "false");
			updateNonExistingCoupon.put("7", "false");
			updateNonExistingCoupon.put("8", "false");
			updateNonExistingCoupon.put("9", "false");
			updateNonExistingCoupon.put("10", "false");
			updateNonExistingCoupon.put("11", "false");
			updateNonExistingCoupon.put("12", "false");
			updateNonExistingCoupon.put("13", "false");
			updateNonExistingCoupon.put("14", "false");
			updateNonExistingCoupon.put("15", "false");
			updateNonExistingCoupon.put("16", "false");
			updateNonExistingCoupon.put("17", "1518934320000");
			updateNonExistingCoupon.put("18", "1519279920000");
			updateNonExistingCoupon.put("19", "ankita.yadav@swiggy.in");
			updateNonExistingCoupon.put("20", "1518934320000");
			updateNonExistingCoupon.put("21", "1518934320000");
			updateNonExistingCoupon.put("22", "null");
			updateNonExistingCoupon.put("23", "null");
			updateNonExistingCoupon.put("24", "null");
			updateNonExistingCoupon.put("25", "-1");
			updateNonExistingCoupon.put("26", "false");
			updateNonExistingCoupon.put("27", "0");
			updateNonExistingCoupon.put("28", "null");
			updateNonExistingCoupon.put("29", "null");
			updateNonExistingCoupon.put("30", "null");
			updateNonExistingCoupon.put("31", "null");
			updateNonExistingCoupon.put("32", "1");
			updateNonExistingCoupon.put("33", "Discount");
			updateNonExistingCoupon.put("34", "10");
			updateNonExistingCoupon.put("35", "1");
			updateNonExistingCoupon.put("36", "50");
			updateNonExistingCoupon.put("37", "10");
			updateNonExistingCoupon.put("38", "90");
			updateNonExistingCoupon.put("39", "false");
			updateNonExistingCoupon.put("40", "0");
			updateNonExistingCoupon.put("41", "-1");
			updateNonExistingCoupon.put("42", "1st transaction desc.");
			updateNonExistingCoupon.put("43", "1st transaction");
			updateNonExistingCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			updateNonExistingCoupon.put("45", "w2fh4c46d0bszhl89voy");
			updateNonExistingCoupon.put("46", "false");
			updateNonExistingCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			updateNonExistingCoupon.put("48", "ANDROID");
			updateNonExistingCoupon.put("49", "2");
			updateNonExistingCoupon.put("50", "2");
			updateNonExistingCoupon.put("51", "Discount");
			updateNonExistingCoupon.put("52", "10");
			updateNonExistingCoupon.put("53", "1");
			updateNonExistingCoupon.put("54", "50");
			updateNonExistingCoupon.put("55", "10");
			updateNonExistingCoupon.put("56", "90");
			updateNonExistingCoupon.put("57", "false");
			updateNonExistingCoupon.put("58", "0");
			updateNonExistingCoupon.put("59", "-1");
			updateNonExistingCoupon.put("60", "1st transaction desc.");
			updateNonExistingCoupon.put("61", "1st transaction");
			updateNonExistingCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			updateNonExistingCoupon.put("63", "w2fh4c46d0bszhl89voy");
			updateNonExistingCoupon.put("64", "false");
			updateNonExistingCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			updateNonExistingCoupon.put("66", "ANDROID");
			updateNonExistingCoupon.put("67", "2");
			updateNonExistingCoupon.put("68", "false");
			updateNonExistingCoupon.put("69", "false");
			updateNonExistingCoupon.put("70", "false");

			return new Object[][] { { updateNonExistingCoupon, nonExistingCouponCode, updateKey, updateValue } };

		}
	}

	@DataProvider(name = "checkMultiApplyCouponConfigurationInGetMultiApplyCouponData")
	public static Object[][] checkMultiApplyCouponConfigurationInGetMultiApplyCouponData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON Config. Check");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION Config. Check");
			hashMap.put("4", "2");
			hashMap.put("5", "100");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1521676800000");
			hashMap.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1521676800000");
			hashMap.put("21", "1522195200000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "100");
			hashMap.put("35", "1");
			hashMap.put("36", "50");
			hashMap.put("37", "10");
			hashMap.put("38", "90");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc. config. check");
			hashMap.put("43", "1st transaction config. check");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10");
			hashMap.put("53", "1");
			hashMap.put("54", "50");
			hashMap.put("55", "10");
			hashMap.put("56", "90");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "checkMultiApplyCouponTransactionConfigurationInGetMultiApplyCouponData")
	public static Object[][] checkMultiApplyCouponTransactionConfigurationInGetMultiApplyCouponData() {
		{
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String duplicateType = "false";

			hashMap.put("0", "ALPHACODE");
			hashMap.put("1", "MULTICOUPON");
			hashMap.put("2", "Internal");
			hashMap.put("3", "AUTOMATION");
			hashMap.put("4", "2");
			hashMap.put("5", "10");
			hashMap.put("6", "false");
			hashMap.put("7", "false");
			hashMap.put("8", "false");
			hashMap.put("9", "false");
			hashMap.put("10", "false");
			hashMap.put("11", "false");
			hashMap.put("12", "false");
			hashMap.put("13", "false");
			hashMap.put("14", "false");
			hashMap.put("15", "false");
			hashMap.put("16", "false");
			hashMap.put("17", "1518934320000");
			hashMap.put("18", "1519279920000");
			hashMap.put("19", "ankita.yadav@swiggy.in");
			hashMap.put("20", "1518934320000");
			hashMap.put("21", "1518934320000");
			hashMap.put("22", "null");
			hashMap.put("23", "null");
			hashMap.put("24", "null");
			hashMap.put("25", "-1");
			hashMap.put("26", "false");
			hashMap.put("27", "0");
			hashMap.put("28", "null");
			hashMap.put("29", "null");
			hashMap.put("30", "null");
			hashMap.put("31", "null");
			hashMap.put("32", "1");
			hashMap.put("33", "Discount");
			hashMap.put("34", "10.0");
			hashMap.put("35", "1");
			hashMap.put("36", "50.0");
			hashMap.put("37", "10.0");
			hashMap.put("38", "90.0");
			hashMap.put("39", "false");
			hashMap.put("40", "0");
			hashMap.put("41", "-1");
			hashMap.put("42", "1st transaction desc.");
			hashMap.put("43", "1st transaction");
			hashMap.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("45", "w2fh4c46d0bszhl89voy");
			hashMap.put("46", "false");
			hashMap.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("48", "ANDROID");
			hashMap.put("49", "2");
			hashMap.put("50", "2");
			hashMap.put("51", "Discount");
			hashMap.put("52", "10.0");
			hashMap.put("53", "1");
			hashMap.put("54", "50.0");
			hashMap.put("55", "10.0");
			hashMap.put("56", "90.0");
			hashMap.put("57", "false");
			hashMap.put("58", "0");
			hashMap.put("59", "-1");
			hashMap.put("60", "1st transaction desc.");
			hashMap.put("61", "1st transaction");
			hashMap.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			hashMap.put("63", "w2fh4c46d0bszhl89voy");
			hashMap.put("64", "false");
			hashMap.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			hashMap.put("66", "ANDROID");
			hashMap.put("67", "2");
			hashMap.put("68", "false");
			hashMap.put("69", "false");
			hashMap.put("70", "false");
			return new Object[][] { { hashMap, duplicateType } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponSiwggyVendorData")
	public static Object[][] applyMultiApplyCouponSiwggyVendorData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponVendorCheck = new HashMap<String, String>();
			String duplicateType = "false";

			// vendor type(27) - 0
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// vendorType (21) - 0
			applyCouponVendorCheck.put("0", "ALPHATEST");
			applyCouponVendorCheck.put("1", "2");
			applyCouponVendorCheck.put("2", "1");
			applyCouponVendorCheck.put("3", "0.0");
			applyCouponVendorCheck.put("4", "0.0");
			applyCouponVendorCheck.put("5", "null");
			applyCouponVendorCheck.put("6", "5282696");
			applyCouponVendorCheck.put("7", "1");
			applyCouponVendorCheck.put("8", "28157");
			applyCouponVendorCheck.put("9", "3249402");
			applyCouponVendorCheck.put("10", "0.0");
			applyCouponVendorCheck.put("11", "false");
			applyCouponVendorCheck.put("12", "1");
			applyCouponVendorCheck.put("13", "200");
			applyCouponVendorCheck.put("14", "5282696_0_variants_addons");
			applyCouponVendorCheck.put("15", "1");
			applyCouponVendorCheck.put("16", "150");
			applyCouponVendorCheck.put("17", "Cash");
			applyCouponVendorCheck.put("18", "false");
			applyCouponVendorCheck.put("19", "false");
			applyCouponVendorCheck.put("20", "false");
			applyCouponVendorCheck.put("21", "0");
			applyCouponVendorCheck.put("22", "false");
			applyCouponVendorCheck.put("23", "245");
			applyCouponVendorCheck.put("24", "ANDROID");
			applyCouponVendorCheck.put("25", "null");
			applyCouponVendorCheck.put("26", "null");

			return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponVendorCheck } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponDominosVendorData")
	public static Object[][] applyMultiApplyCouponDominosVendorData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponVendorCheck = new HashMap<String, String>();
			String duplicateType = "false";

			// vendor type(27) - 12
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "12");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// vendorType (21) - 12
			applyCouponVendorCheck.put("0", "ALPHATEST");
			applyCouponVendorCheck.put("1", "2");
			applyCouponVendorCheck.put("2", "1");
			applyCouponVendorCheck.put("3", "0.0");
			applyCouponVendorCheck.put("4", "0.0");
			applyCouponVendorCheck.put("5", "null");
			applyCouponVendorCheck.put("6", "5282696");
			applyCouponVendorCheck.put("7", "1");
			applyCouponVendorCheck.put("8", "28157");
			applyCouponVendorCheck.put("9", "3249402");
			applyCouponVendorCheck.put("10", "0.0");
			applyCouponVendorCheck.put("11", "false");
			applyCouponVendorCheck.put("12", "1");
			applyCouponVendorCheck.put("13", "200");
			applyCouponVendorCheck.put("14", "5282696_0_variants_addons");
			applyCouponVendorCheck.put("15", "1");
			applyCouponVendorCheck.put("16", "150");
			applyCouponVendorCheck.put("17", "Cash");
			applyCouponVendorCheck.put("18", "false");
			applyCouponVendorCheck.put("19", "false");
			applyCouponVendorCheck.put("20", "false");
			applyCouponVendorCheck.put("21", "12");
			applyCouponVendorCheck.put("22", "false");
			applyCouponVendorCheck.put("23", "245");
			applyCouponVendorCheck.put("24", "ANDROID");
			applyCouponVendorCheck.put("25", "null");
			applyCouponVendorCheck.put("26", "null");

			return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponVendorCheck } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponInvalidVendorData")
	public static Object[][] applyMultiApplyCouponInvalidVendorData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponVendorCheck = new HashMap<String, String>();
			String duplicateType = "false";

			// vendor type(27) - 12
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18",Utility.getFuturetimeInMilliSeconds(345600000));
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "12");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// vendorType (21) - 0
			applyCouponVendorCheck.put("0", "ALPHATEST");
			applyCouponVendorCheck.put("1", "2");
			applyCouponVendorCheck.put("2", "1");
			applyCouponVendorCheck.put("3", "0.0");
			applyCouponVendorCheck.put("4", "0.0");
			applyCouponVendorCheck.put("5", "null");
			applyCouponVendorCheck.put("6", "5282696");
			applyCouponVendorCheck.put("7", "1");
			applyCouponVendorCheck.put("8", "28157");
			applyCouponVendorCheck.put("9", "3249402");
			applyCouponVendorCheck.put("10", "0.0");
			applyCouponVendorCheck.put("11", "false");
			applyCouponVendorCheck.put("12", "1");
			applyCouponVendorCheck.put("13", "200");
			applyCouponVendorCheck.put("14", "5282696_0_variants_addons");
			applyCouponVendorCheck.put("15", "1");
			applyCouponVendorCheck.put("16", "150");
			applyCouponVendorCheck.put("17", "Cash");
			applyCouponVendorCheck.put("18", "false");
			applyCouponVendorCheck.put("19", "false");
			applyCouponVendorCheck.put("20", "false");
			applyCouponVendorCheck.put("21", "0");
			applyCouponVendorCheck.put("22", "false");
			applyCouponVendorCheck.put("23", "245");
			applyCouponVendorCheck.put("24", "ANDROID");
			applyCouponVendorCheck.put("25", "null");
			applyCouponVendorCheck.put("26", "null");

			return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponVendorCheck }};

		}
	}


	@DataProvider(name = "applyMultiApplyCouponInvalidVendorNegativeData")
	public static Object[][] applyMultiApplyCouponInvalidVendorNegativeData() {
		{
			HashMap<String, String> createMultiApplyCouponSwiggyVendor = new HashMap<String, String>();
			HashMap<String, String> applyCouponDominosVendorCheck = new HashMap<String, String>();
			String duplicateType = "false";

			// vendor type(27) - 0
			createMultiApplyCouponSwiggyVendor.put("0", "ALPHACODE");
			createMultiApplyCouponSwiggyVendor.put("1", "MULTICOUPON");
			createMultiApplyCouponSwiggyVendor.put("2", "Internal");
			createMultiApplyCouponSwiggyVendor.put("3", "AUTOMATION");
			createMultiApplyCouponSwiggyVendor.put("4", "2");
			createMultiApplyCouponSwiggyVendor.put("5", "10");
			createMultiApplyCouponSwiggyVendor.put("6", "false");
			createMultiApplyCouponSwiggyVendor.put("7", "false");
			createMultiApplyCouponSwiggyVendor.put("8", "false");
			createMultiApplyCouponSwiggyVendor.put("9", "false");
			createMultiApplyCouponSwiggyVendor.put("10", "false");
			createMultiApplyCouponSwiggyVendor.put("11", "false");
			createMultiApplyCouponSwiggyVendor.put("12", "false");
			createMultiApplyCouponSwiggyVendor.put("13", "false");
			createMultiApplyCouponSwiggyVendor.put("14", "false");
			createMultiApplyCouponSwiggyVendor.put("15", "false");
			createMultiApplyCouponSwiggyVendor.put("16", "false");
			createMultiApplyCouponSwiggyVendor.put("17", "1518934320000");
			createMultiApplyCouponSwiggyVendor.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createMultiApplyCouponSwiggyVendor.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCouponSwiggyVendor.put("20", "1518934320000");
			createMultiApplyCouponSwiggyVendor.put("21", "1518934320000");
			createMultiApplyCouponSwiggyVendor.put("22", "null");
			createMultiApplyCouponSwiggyVendor.put("23", "null");
			createMultiApplyCouponSwiggyVendor.put("24", "null");
			createMultiApplyCouponSwiggyVendor.put("25", "-1");
			createMultiApplyCouponSwiggyVendor.put("26", "false");
			createMultiApplyCouponSwiggyVendor.put("27", "0");
			createMultiApplyCouponSwiggyVendor.put("28", "null");
			createMultiApplyCouponSwiggyVendor.put("29", "null");
			createMultiApplyCouponSwiggyVendor.put("30", "null");
			createMultiApplyCouponSwiggyVendor.put("31", "null");
			createMultiApplyCouponSwiggyVendor.put("32", "1");
			createMultiApplyCouponSwiggyVendor.put("33", "Discount");
			createMultiApplyCouponSwiggyVendor.put("34", "10");
			createMultiApplyCouponSwiggyVendor.put("35", "1");
			createMultiApplyCouponSwiggyVendor.put("36", "50");
			createMultiApplyCouponSwiggyVendor.put("37", "10");
			createMultiApplyCouponSwiggyVendor.put("38", "90");
			createMultiApplyCouponSwiggyVendor.put("39", "false");
			createMultiApplyCouponSwiggyVendor.put("40", "0");
			createMultiApplyCouponSwiggyVendor.put("41", "-1");
			createMultiApplyCouponSwiggyVendor.put("42", "1st transaction desc.");
			createMultiApplyCouponSwiggyVendor.put("43", "1st transaction");
			createMultiApplyCouponSwiggyVendor.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCouponSwiggyVendor.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCouponSwiggyVendor.put("46", "false");
			createMultiApplyCouponSwiggyVendor.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCouponSwiggyVendor.put("48", "ANDROID");
			createMultiApplyCouponSwiggyVendor.put("49", "2");
			createMultiApplyCouponSwiggyVendor.put("50", "2");
			createMultiApplyCouponSwiggyVendor.put("51", "Discount");
			createMultiApplyCouponSwiggyVendor.put("52", "10");
			createMultiApplyCouponSwiggyVendor.put("53", "1");
			createMultiApplyCouponSwiggyVendor.put("54", "50");
			createMultiApplyCouponSwiggyVendor.put("55", "10");
			createMultiApplyCouponSwiggyVendor.put("56", "90");
			createMultiApplyCouponSwiggyVendor.put("57", "false");
			createMultiApplyCouponSwiggyVendor.put("58", "0");
			createMultiApplyCouponSwiggyVendor.put("59", "-1");
			createMultiApplyCouponSwiggyVendor.put("60", "1st transaction desc.");
			createMultiApplyCouponSwiggyVendor.put("61", "1st transaction");
			createMultiApplyCouponSwiggyVendor.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCouponSwiggyVendor.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCouponSwiggyVendor.put("64", "false");
			createMultiApplyCouponSwiggyVendor.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCouponSwiggyVendor.put("66", "ANDROID");
			createMultiApplyCouponSwiggyVendor.put("67", "2");
			createMultiApplyCouponSwiggyVendor.put("68", "false");
			createMultiApplyCouponSwiggyVendor.put("69", "false");
			createMultiApplyCouponSwiggyVendor.put("70", "false");

			// vendorType (21) - 12
			applyCouponDominosVendorCheck.put("0", "ALPHATEST");
			applyCouponDominosVendorCheck.put("1", "2");
			applyCouponDominosVendorCheck.put("2", "1");
			applyCouponDominosVendorCheck.put("3", "0.0");
			applyCouponDominosVendorCheck.put("4", "0.0");
			applyCouponDominosVendorCheck.put("5", "null");
			applyCouponDominosVendorCheck.put("6", "5282696");
			applyCouponDominosVendorCheck.put("7", "1");
			applyCouponDominosVendorCheck.put("8", "28157");
			applyCouponDominosVendorCheck.put("9", "3249402");
			applyCouponDominosVendorCheck.put("10", "0.0");
			applyCouponDominosVendorCheck.put("11", "false");
			applyCouponDominosVendorCheck.put("12", "1");
			applyCouponDominosVendorCheck.put("13", "200");
			applyCouponDominosVendorCheck.put("14", "5282696_0_variants_addons");
			applyCouponDominosVendorCheck.put("15", "1");
			applyCouponDominosVendorCheck.put("16", "150");
			applyCouponDominosVendorCheck.put("17", "Cash");
			applyCouponDominosVendorCheck.put("18", "false");
			applyCouponDominosVendorCheck.put("19", "false");
			applyCouponDominosVendorCheck.put("20", "false");
			applyCouponDominosVendorCheck.put("21", "12");
			applyCouponDominosVendorCheck.put("22", "false");
			applyCouponDominosVendorCheck.put("23", "245");
			applyCouponDominosVendorCheck.put("24", "ANDROID");
			applyCouponDominosVendorCheck.put("25", "null");
			applyCouponDominosVendorCheck.put("26", "null");

			return new Object[][] {{ createMultiApplyCouponSwiggyVendor, duplicateType, applyCouponDominosVendorCheck } };
		}}


	@DataProvider(name = "applyFirstOrderRestrictionTrueMultiApplyCouponData")
	public static Object[][] applyFirstOrderRestrictionTrueMultiApplyCouponData() {
		{
			HashMap<String, String> createFirstOrderRestrictionMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponFirstOrderRestrictionCheck = new HashMap<String, String>();
			String duplicateType = "false";

			// First order restriction(46) true
			createFirstOrderRestrictionMultiApplyCoupon.put("0", "ALPHACODE");
			createFirstOrderRestrictionMultiApplyCoupon.put("1", "MULTICOUPON");
			createFirstOrderRestrictionMultiApplyCoupon.put("2", "Internal");
			createFirstOrderRestrictionMultiApplyCoupon.put("3", "AUTOMATION");
			createFirstOrderRestrictionMultiApplyCoupon.put("4", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("5", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("6", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("7", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("8", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("9", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("10", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("11", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("12", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("13", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("14", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("15", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("16", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("17", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createFirstOrderRestrictionMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createFirstOrderRestrictionMultiApplyCoupon.put("20", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("21", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("22", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("23", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("24", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("25", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("26", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("27", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("28", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("29", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("30", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("31", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("32", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("33", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("34", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("35", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("36", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("37", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("38", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("39", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("40", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("41", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("42", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("43", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("46", "true");
			createFirstOrderRestrictionMultiApplyCoupon.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("48", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("49", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("50", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("51", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("52", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("53", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("54", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("55", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("56", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("57", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("58", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("59", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("60", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("61", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("64", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("66", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("67", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("68", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("69", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("70", "false");

			// firstorder (11) - true
			applyCouponFirstOrderRestrictionCheck.put("0", "ALPHATEST");
			applyCouponFirstOrderRestrictionCheck.put("1", "2");
			applyCouponFirstOrderRestrictionCheck.put("2", "1");
			applyCouponFirstOrderRestrictionCheck.put("3", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("4", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("5", "null");
			applyCouponFirstOrderRestrictionCheck.put("6", "5282696");
			applyCouponFirstOrderRestrictionCheck.put("7", "1");
			applyCouponFirstOrderRestrictionCheck.put("8", "28157");
			applyCouponFirstOrderRestrictionCheck.put("9", "3249402");
			applyCouponFirstOrderRestrictionCheck.put("10", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("11", "true");
			applyCouponFirstOrderRestrictionCheck.put("12", "1");
			applyCouponFirstOrderRestrictionCheck.put("13", "200");
			applyCouponFirstOrderRestrictionCheck.put("14", "5282696_0_variants_addons");
			applyCouponFirstOrderRestrictionCheck.put("15", "1");
			applyCouponFirstOrderRestrictionCheck.put("16", "150");
			applyCouponFirstOrderRestrictionCheck.put("17", "Cash");
			applyCouponFirstOrderRestrictionCheck.put("18", "false");
			applyCouponFirstOrderRestrictionCheck.put("19", "false");
			applyCouponFirstOrderRestrictionCheck.put("20", "false");
			applyCouponFirstOrderRestrictionCheck.put("21", "0");
			applyCouponFirstOrderRestrictionCheck.put("22", "false");
			applyCouponFirstOrderRestrictionCheck.put("23", "245");
			applyCouponFirstOrderRestrictionCheck.put("24", "ANDROID");
			applyCouponFirstOrderRestrictionCheck.put("25", "null");
			applyCouponFirstOrderRestrictionCheck.put("26", "null");

			return new Object[][] { { createFirstOrderRestrictionMultiApplyCoupon, duplicateType,
					applyCouponFirstOrderRestrictionCheck } };

		}
	}

	@DataProvider(name = "applyFirstOrderRestrictionFalseMultiApplyCouponData")
	public static Object[][] applyFirstOrderRestrictionFalseMultiApplyCouponData() {
		{
			HashMap<String, String> createFirstOrderRestrictionMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponFirstOrderRestrictionCheck = new HashMap<String, String>();
			String duplicateType = "false";

			// First order restriction(46) true
			createFirstOrderRestrictionMultiApplyCoupon.put("0", "ALPHACODE");
			createFirstOrderRestrictionMultiApplyCoupon.put("1", "MULTICOUPON");
			createFirstOrderRestrictionMultiApplyCoupon.put("2", "Internal");
			createFirstOrderRestrictionMultiApplyCoupon.put("3", "AUTOMATION");
			createFirstOrderRestrictionMultiApplyCoupon.put("4", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("5", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("6", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("7", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("8", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("9", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("10", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("11", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("12", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("13", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("14", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("15", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("16", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("17", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createFirstOrderRestrictionMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createFirstOrderRestrictionMultiApplyCoupon.put("20", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("21", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("22", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("23", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("24", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("25", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("26", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("27", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("28", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("29", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("30", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("31", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("32", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("33", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("34", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("35", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("36", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("37", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("38", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("39", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("40", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("41", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("42", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("43", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("46", "true");
			createFirstOrderRestrictionMultiApplyCoupon.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("48", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("49", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("50", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("51", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("52", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("53", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("54", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("55", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("56", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("57", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("58", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("59", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("60", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("61", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("64", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("66", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("67", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("68", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("69", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("70", "false");

			// firstorder (11) - false
			applyCouponFirstOrderRestrictionCheck.put("0", "ALPHATEST");
			applyCouponFirstOrderRestrictionCheck.put("1", "2");
			applyCouponFirstOrderRestrictionCheck.put("2", "1");
			applyCouponFirstOrderRestrictionCheck.put("3", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("4", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("5", "null");
			applyCouponFirstOrderRestrictionCheck.put("6", "5282696");
			applyCouponFirstOrderRestrictionCheck.put("7", "1");
			applyCouponFirstOrderRestrictionCheck.put("8", "28175");
			applyCouponFirstOrderRestrictionCheck.put("9", "189065");
			applyCouponFirstOrderRestrictionCheck.put("10", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("11", "false");
			applyCouponFirstOrderRestrictionCheck.put("12", "1");
			applyCouponFirstOrderRestrictionCheck.put("13", "200");
			applyCouponFirstOrderRestrictionCheck.put("14", "5282696_0_variants_addons");
			applyCouponFirstOrderRestrictionCheck.put("15", "1");
			applyCouponFirstOrderRestrictionCheck.put("16", "150");
			applyCouponFirstOrderRestrictionCheck.put("17", "Cash");
			applyCouponFirstOrderRestrictionCheck.put("18", "false");
			applyCouponFirstOrderRestrictionCheck.put("19", "false");
			applyCouponFirstOrderRestrictionCheck.put("20", "false");
			applyCouponFirstOrderRestrictionCheck.put("21", "0");
			applyCouponFirstOrderRestrictionCheck.put("22", "false");
			applyCouponFirstOrderRestrictionCheck.put("23", "245");
			applyCouponFirstOrderRestrictionCheck.put("24", "ANDROID");
			applyCouponFirstOrderRestrictionCheck.put("25", "null");
			applyCouponFirstOrderRestrictionCheck.put("26", "null");

			return new Object[][] { { createFirstOrderRestrictionMultiApplyCoupon, duplicateType,
					applyCouponFirstOrderRestrictionCheck } };

		}
	}

	@DataProvider(name = "applyFirstOrderRestrictionMultiApplyCouponData")
	public static Object[][] applyFirstOrderRestrictionMultiApplyCouponData() {
		{
			HashMap<String, String> createFirstOrderRestrictionMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponFirstOrderRestrictionCheck = new HashMap<String, String>();

			String duplicateType = "false";

			// First order restriction(46) false
			createFirstOrderRestrictionMultiApplyCoupon.put("0", "ALPHACODE");
			createFirstOrderRestrictionMultiApplyCoupon.put("1", "MULTICOUPON");
			createFirstOrderRestrictionMultiApplyCoupon.put("2", "Internal");
			createFirstOrderRestrictionMultiApplyCoupon.put("3", "AUTOMATION");
			createFirstOrderRestrictionMultiApplyCoupon.put("4", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("5", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("6", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("7", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("8", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("9", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("10", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("11", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("12", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("13", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("14", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("15", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("16", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("17", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createFirstOrderRestrictionMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createFirstOrderRestrictionMultiApplyCoupon.put("20", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("21", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("22", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("23", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("24", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("25", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("26", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("27", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("28", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("29", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("30", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("31", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("32", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("33", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("34", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("35", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("36", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("37", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("38", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("39", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("40", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("41", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("42", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("43", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("46", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("48", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("49", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("50", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("51", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("52", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("53", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("54", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("55", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("56", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("57", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("58", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("59", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("60", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("61", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("64", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("66", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("67", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("68", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("69", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("70", "false");

			// firstorder (11) - false
			applyCouponFirstOrderRestrictionCheck.put("0", "ALPHATEST");
			applyCouponFirstOrderRestrictionCheck.put("1", "2");
			applyCouponFirstOrderRestrictionCheck.put("2", "1");
			applyCouponFirstOrderRestrictionCheck.put("3", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("4", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("5", "null");
			applyCouponFirstOrderRestrictionCheck.put("6", "5282696");
			applyCouponFirstOrderRestrictionCheck.put("7", "1");
			applyCouponFirstOrderRestrictionCheck.put("8", "28157");
			applyCouponFirstOrderRestrictionCheck.put("9", "3249402");
			applyCouponFirstOrderRestrictionCheck.put("10", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("11", "false");
			applyCouponFirstOrderRestrictionCheck.put("12", "1");
			applyCouponFirstOrderRestrictionCheck.put("13", "200");
			applyCouponFirstOrderRestrictionCheck.put("14", "5282696_0_variants_addons");
			applyCouponFirstOrderRestrictionCheck.put("15", "1");
			applyCouponFirstOrderRestrictionCheck.put("16", "150");
			applyCouponFirstOrderRestrictionCheck.put("17", "Cash");
			applyCouponFirstOrderRestrictionCheck.put("18", "false");
			applyCouponFirstOrderRestrictionCheck.put("19", "false");
			applyCouponFirstOrderRestrictionCheck.put("20", "false");
			applyCouponFirstOrderRestrictionCheck.put("21", "0");
			applyCouponFirstOrderRestrictionCheck.put("22", "false");
			applyCouponFirstOrderRestrictionCheck.put("23", "245");
			applyCouponFirstOrderRestrictionCheck.put("24", "ANDROID");
			applyCouponFirstOrderRestrictionCheck.put("25", "null");
			applyCouponFirstOrderRestrictionCheck.put("26", "null");

			return new Object[][] {
					{ createFirstOrderRestrictionMultiApplyCoupon, duplicateType,
							applyCouponFirstOrderRestrictionCheck }};

		}
	}




	@DataProvider(name = "applyFirstOrderRestrictionMultiApplyCouponNegativeTest")
	public static Object[][] applyFirstOrderRestrictionMultiApplyCouponNegativeData() {
		{
		HashMap<String, String> createFirstOrderRestrictionMultiApplyCoupon = new HashMap<String, String>();
		HashMap<String, String> applyCouponFirstOrderRestrictionTrueCheck = new HashMap<String, String>();
			String duplicateType = "false";

			createFirstOrderRestrictionMultiApplyCoupon.put("0", "ALPHACODE");
			createFirstOrderRestrictionMultiApplyCoupon.put("1", "MULTICOUPON");
			createFirstOrderRestrictionMultiApplyCoupon.put("2", "Internal");
			createFirstOrderRestrictionMultiApplyCoupon.put("3", "AUTOMATION");
			createFirstOrderRestrictionMultiApplyCoupon.put("4", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("5", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("6", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("7", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("8", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("9", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("10", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("11", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("12", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("13", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("14", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("15", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("16", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("17", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createFirstOrderRestrictionMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createFirstOrderRestrictionMultiApplyCoupon.put("20", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("21", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("22", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("23", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("24", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("25", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("26", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("27", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("28", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("29", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("30", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("31", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("32", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("33", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("34", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("35", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("36", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("37", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("38", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("39", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("40", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("41", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("42", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("43", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("46", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("48", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("49", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("50", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("51", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("52", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("53", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("54", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("55", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("56", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("57", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("58", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("59", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("60", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("61", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("64", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("66", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("67", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("68", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("69", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("70", "false");

			// firstorder (11) - true
			applyCouponFirstOrderRestrictionTrueCheck.put("0", "ALPHATEST");
			applyCouponFirstOrderRestrictionTrueCheck.put("1", "2");
			applyCouponFirstOrderRestrictionTrueCheck.put("2", "1");
			applyCouponFirstOrderRestrictionTrueCheck.put("3", "0.0");
			applyCouponFirstOrderRestrictionTrueCheck.put("4", "0.0");
			applyCouponFirstOrderRestrictionTrueCheck.put("5", "null");
			applyCouponFirstOrderRestrictionTrueCheck.put("6", "5282696");
			applyCouponFirstOrderRestrictionTrueCheck.put("7", "1");
			applyCouponFirstOrderRestrictionTrueCheck.put("8", "28157");
			applyCouponFirstOrderRestrictionTrueCheck.put("9", "3249402");
			applyCouponFirstOrderRestrictionTrueCheck.put("10", "0.0");
			applyCouponFirstOrderRestrictionTrueCheck.put("11", "true");
			applyCouponFirstOrderRestrictionTrueCheck.put("12", "1");
			applyCouponFirstOrderRestrictionTrueCheck.put("13", "200");
			applyCouponFirstOrderRestrictionTrueCheck.put("14", "5282696_0_variants_addons");
			applyCouponFirstOrderRestrictionTrueCheck.put("15", "1");
			applyCouponFirstOrderRestrictionTrueCheck.put("16", "150");
			applyCouponFirstOrderRestrictionTrueCheck.put("17", "Cash");
			applyCouponFirstOrderRestrictionTrueCheck.put("18", "false");
			applyCouponFirstOrderRestrictionTrueCheck.put("19", "false");
			applyCouponFirstOrderRestrictionTrueCheck.put("20", "false");
			applyCouponFirstOrderRestrictionTrueCheck.put("21", "0");
			applyCouponFirstOrderRestrictionTrueCheck.put("22", "false");
			applyCouponFirstOrderRestrictionTrueCheck.put("23", "245");
			applyCouponFirstOrderRestrictionTrueCheck.put("24", "ANDROID");
			applyCouponFirstOrderRestrictionTrueCheck.put("25", "null");
			applyCouponFirstOrderRestrictionTrueCheck.put("26", "null");

			return new Object[][] {
					{ createFirstOrderRestrictionMultiApplyCoupon, duplicateType,
							applyCouponFirstOrderRestrictionTrueCheck } };


		}
	}

	@DataProvider(name = "applyFirstOrderRestrictionNullMultiApplyCouponData")
	public static Object[][] applyFirstOrderRestrictionNullMultiApplyCouponData() {
		{
			HashMap<String, String> createFirstOrderRestrictionMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponFirstOrderRestrictionCheck = new HashMap<String, String>();
			String duplicateType = "false";

			// First order restriction(46) true
			createFirstOrderRestrictionMultiApplyCoupon.put("0", "ALPHACODE");
			createFirstOrderRestrictionMultiApplyCoupon.put("1", "MULTICOUPON");
			createFirstOrderRestrictionMultiApplyCoupon.put("2", "Internal");
			createFirstOrderRestrictionMultiApplyCoupon.put("3", "AUTOMATION");
			createFirstOrderRestrictionMultiApplyCoupon.put("4", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("5", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("6", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("7", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("8", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("9", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("10", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("11", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("12", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("13", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("14", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("15", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("16", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("17", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createFirstOrderRestrictionMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createFirstOrderRestrictionMultiApplyCoupon.put("20", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("21", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("22", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("23", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("24", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("25", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("26", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("27", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("28", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("29", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("30", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("31", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("32", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("33", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("34", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("35", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("36", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("37", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("38", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("39", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("40", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("41", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("42", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("43", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("46", "true");
			createFirstOrderRestrictionMultiApplyCoupon.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("48", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("49", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("50", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("51", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("52", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("53", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("54", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("55", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("56", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("57", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("58", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("59", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("60", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("61", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("64", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("66", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("67", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("68", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("69", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("70", "false");

			// firstorder (11) - null
			applyCouponFirstOrderRestrictionCheck.put("0", "ALPHATEST");
			applyCouponFirstOrderRestrictionCheck.put("1", "2");
			applyCouponFirstOrderRestrictionCheck.put("2", "1");
			applyCouponFirstOrderRestrictionCheck.put("3", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("4", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("5", "null");
			applyCouponFirstOrderRestrictionCheck.put("6", "5282696");
			applyCouponFirstOrderRestrictionCheck.put("7", "1");
			applyCouponFirstOrderRestrictionCheck.put("8", "28157");
			applyCouponFirstOrderRestrictionCheck.put("9", "3249402");
			applyCouponFirstOrderRestrictionCheck.put("10", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("11", "null");
			applyCouponFirstOrderRestrictionCheck.put("12", "1");
			applyCouponFirstOrderRestrictionCheck.put("13", "200");
			applyCouponFirstOrderRestrictionCheck.put("14", "5282696_0_variants_addons");
			applyCouponFirstOrderRestrictionCheck.put("15", "1");
			applyCouponFirstOrderRestrictionCheck.put("16", "150");
			applyCouponFirstOrderRestrictionCheck.put("17", "Cash");
			applyCouponFirstOrderRestrictionCheck.put("18", "false");
			applyCouponFirstOrderRestrictionCheck.put("19", "false");
			applyCouponFirstOrderRestrictionCheck.put("20", "false");
			applyCouponFirstOrderRestrictionCheck.put("21", "0");
			applyCouponFirstOrderRestrictionCheck.put("22", "false");
			applyCouponFirstOrderRestrictionCheck.put("23", "245");
			applyCouponFirstOrderRestrictionCheck.put("24", "ANDROID");
			applyCouponFirstOrderRestrictionCheck.put("25", "null");
			applyCouponFirstOrderRestrictionCheck.put("26", "null");

			return new Object[][] { { createFirstOrderRestrictionMultiApplyCoupon, duplicateType,
					applyCouponFirstOrderRestrictionCheck } };

		}
	}

	@DataProvider(name = "applyNonExistingMultiApplyCouponCodeData")
	public static Object[][] applyNonExistingMultiApplyCouponCodeData() {
		{
			HashMap<String, String> createFirstOrderRestrictionMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponFirstOrderRestrictionCheck = new HashMap<String, String>();
			String duplicateType = "true";

			// coupon code(01) NONEXISTING
			createFirstOrderRestrictionMultiApplyCoupon.put("0", "NONEXISTING");
			createFirstOrderRestrictionMultiApplyCoupon.put("1", "MULTICOUPON");
			createFirstOrderRestrictionMultiApplyCoupon.put("2", "Internal");
			createFirstOrderRestrictionMultiApplyCoupon.put("3", "AUTOMATION");
			createFirstOrderRestrictionMultiApplyCoupon.put("4", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("5", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("6", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("7", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("8", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("9", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("10", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("11", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("12", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("13", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("14", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("15", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("16", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("17", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createFirstOrderRestrictionMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createFirstOrderRestrictionMultiApplyCoupon.put("20", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("21", "1518934320000");
			createFirstOrderRestrictionMultiApplyCoupon.put("22", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("23", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("24", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("25", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("26", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("27", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("28", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("29", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("30", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("31", "null");
			createFirstOrderRestrictionMultiApplyCoupon.put("32", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("33", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("34", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("35", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("36", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("37", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("38", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("39", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("40", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("41", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("42", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("43", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("46", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("48", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("49", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("50", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("51", "Discount");
			createFirstOrderRestrictionMultiApplyCoupon.put("52", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("53", "1");
			createFirstOrderRestrictionMultiApplyCoupon.put("54", "50");
			createFirstOrderRestrictionMultiApplyCoupon.put("55", "10");
			createFirstOrderRestrictionMultiApplyCoupon.put("56", "90");
			createFirstOrderRestrictionMultiApplyCoupon.put("57", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("58", "0");
			createFirstOrderRestrictionMultiApplyCoupon.put("59", "-1");
			createFirstOrderRestrictionMultiApplyCoupon.put("60", "1st transaction desc.");
			createFirstOrderRestrictionMultiApplyCoupon.put("61", "1st transaction");
			createFirstOrderRestrictionMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createFirstOrderRestrictionMultiApplyCoupon.put("64", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createFirstOrderRestrictionMultiApplyCoupon.put("66", "ANDROID");
			createFirstOrderRestrictionMultiApplyCoupon.put("67", "2");
			createFirstOrderRestrictionMultiApplyCoupon.put("68", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("69", "false");
			createFirstOrderRestrictionMultiApplyCoupon.put("70", "false");

			// code (01) NONEXISTIN
			applyCouponFirstOrderRestrictionCheck.put("0", "NONEXISTIN");
			applyCouponFirstOrderRestrictionCheck.put("1", "2");
			applyCouponFirstOrderRestrictionCheck.put("2", "1");
			applyCouponFirstOrderRestrictionCheck.put("3", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("4", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("5", "null");
			applyCouponFirstOrderRestrictionCheck.put("6", "5282696");
			applyCouponFirstOrderRestrictionCheck.put("7", "1");
			applyCouponFirstOrderRestrictionCheck.put("8", "28157");
			applyCouponFirstOrderRestrictionCheck.put("9", "3249402");
			applyCouponFirstOrderRestrictionCheck.put("10", "0.0");
			applyCouponFirstOrderRestrictionCheck.put("11", "false");
			applyCouponFirstOrderRestrictionCheck.put("12", "1");
			applyCouponFirstOrderRestrictionCheck.put("13", "200");
			applyCouponFirstOrderRestrictionCheck.put("14", "5282696_0_variants_addons");
			applyCouponFirstOrderRestrictionCheck.put("15", "1");
			applyCouponFirstOrderRestrictionCheck.put("16", "150");
			applyCouponFirstOrderRestrictionCheck.put("17", "Cash");
			applyCouponFirstOrderRestrictionCheck.put("18", "false");
			applyCouponFirstOrderRestrictionCheck.put("19", "false");
			applyCouponFirstOrderRestrictionCheck.put("20", "false");
			applyCouponFirstOrderRestrictionCheck.put("21", "12");
			applyCouponFirstOrderRestrictionCheck.put("22", "false");
			applyCouponFirstOrderRestrictionCheck.put("23", "245");
			applyCouponFirstOrderRestrictionCheck.put("24", "ANDROID");
			applyCouponFirstOrderRestrictionCheck.put("25", "null");
			applyCouponFirstOrderRestrictionCheck.put("26", "null");

			return new Object[][] { { createFirstOrderRestrictionMultiApplyCoupon, duplicateType,
					applyCouponFirstOrderRestrictionCheck } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponApplicableWithSwiggyMoneyTrueData")
	public static Object[][] applyMultiApplyCouponApplicableWithSwiggyMoneyTrueData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();

			HashMap<String, String> applyCouponApplicableWithSiwggyMoney = new HashMap<String, String>();
			String duplicateType = "false";

			// applicableWithSwiggyMoney (14) true
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "true");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// applicableWIthSiwggyMoney (19 & 22) true & swiggy Money(10) 25.0
			applyCouponApplicableWithSiwggyMoney.put("0", "ALPHACODE");
			applyCouponApplicableWithSiwggyMoney.put("1", "2");
			applyCouponApplicableWithSiwggyMoney.put("2", "1");
			applyCouponApplicableWithSiwggyMoney.put("3", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("4", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("5", "null");
			applyCouponApplicableWithSiwggyMoney.put("6", "5282696");
			applyCouponApplicableWithSiwggyMoney.put("7", "1");
			applyCouponApplicableWithSiwggyMoney.put("8", "28157");
			applyCouponApplicableWithSiwggyMoney.put("9", "3249402");
			applyCouponApplicableWithSiwggyMoney.put("10", "25.0");
			applyCouponApplicableWithSiwggyMoney.put("11", "false");
			applyCouponApplicableWithSiwggyMoney.put("12", "1");
			applyCouponApplicableWithSiwggyMoney.put("13", "200");
			applyCouponApplicableWithSiwggyMoney.put("14", "5282696_0_variants_addons");
			applyCouponApplicableWithSiwggyMoney.put("15", "1");
			applyCouponApplicableWithSiwggyMoney.put("16", "150");
			applyCouponApplicableWithSiwggyMoney.put("17", "Cash");
			applyCouponApplicableWithSiwggyMoney.put("18", "false");
			applyCouponApplicableWithSiwggyMoney.put("19", "true");
			applyCouponApplicableWithSiwggyMoney.put("20", "false");
			applyCouponApplicableWithSiwggyMoney.put("21", "0");
			applyCouponApplicableWithSiwggyMoney.put("22", "true");
			applyCouponApplicableWithSiwggyMoney.put("23", "245");
			applyCouponApplicableWithSiwggyMoney.put("24", "ANDROID");
			applyCouponApplicableWithSiwggyMoney.put("25", "null");
			applyCouponApplicableWithSiwggyMoney.put("26", "null");

			return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponApplicableWithSiwggyMoney } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponApplicableWithSwiggyMoneyFalseData")
	public static Object[][] applyMultiApplyCouponApplicableWithSwiggyMoneyFalseData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponApplicableWithSiwggyMoney = new HashMap<String, String>();
			HashMap<String, String> applyCouponApplicableWithSiwggyMoneyZero = new HashMap<String, String>();
			String duplicateType = "false";

			// applicableWithSwiggyMoney (14) true
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "true");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// applicableWIthSiwggyMoney (19 & 22) false & swiggy Money(10) 25.0
			applyCouponApplicableWithSiwggyMoney.put("0", "ALPHACODE");
			applyCouponApplicableWithSiwggyMoney.put("1", "2");
			applyCouponApplicableWithSiwggyMoney.put("2", "1");
			applyCouponApplicableWithSiwggyMoney.put("3", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("4", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("5", "null");
			applyCouponApplicableWithSiwggyMoney.put("6", "5282696");
			applyCouponApplicableWithSiwggyMoney.put("7", "1");
			applyCouponApplicableWithSiwggyMoney.put("8", "28157");
			applyCouponApplicableWithSiwggyMoney.put("9", "3249402");
			applyCouponApplicableWithSiwggyMoney.put("10", "25.0");
			applyCouponApplicableWithSiwggyMoney.put("11", "false");
			applyCouponApplicableWithSiwggyMoney.put("12", "1");
			applyCouponApplicableWithSiwggyMoney.put("13", "200");
			applyCouponApplicableWithSiwggyMoney.put("14", "5282696_0_variants_addons");
			applyCouponApplicableWithSiwggyMoney.put("15", "1");
			applyCouponApplicableWithSiwggyMoney.put("16", "150");
			applyCouponApplicableWithSiwggyMoney.put("17", "Cash");
			applyCouponApplicableWithSiwggyMoney.put("18", "false");
			applyCouponApplicableWithSiwggyMoney.put("19", "false");
			applyCouponApplicableWithSiwggyMoney.put("20", "false");
			applyCouponApplicableWithSiwggyMoney.put("21", "0");
			applyCouponApplicableWithSiwggyMoney.put("22", "false");
			applyCouponApplicableWithSiwggyMoney.put("23", "245");
			applyCouponApplicableWithSiwggyMoney.put("24", "ANDROID");
			applyCouponApplicableWithSiwggyMoney.put("25", "null");
			applyCouponApplicableWithSiwggyMoney.put("26", "null");

			// applicableWIthSiwggyMoney (19 & 22) false & swiggy Money(10) 0.0
			applyCouponApplicableWithSiwggyMoneyZero.put("0", "ALPHACODE");
			applyCouponApplicableWithSiwggyMoneyZero.put("1", "2");
			applyCouponApplicableWithSiwggyMoneyZero.put("2", "1");
			applyCouponApplicableWithSiwggyMoneyZero.put("3", "0.0");
			applyCouponApplicableWithSiwggyMoneyZero.put("4", "0.0");
			applyCouponApplicableWithSiwggyMoneyZero.put("5", "null");
			applyCouponApplicableWithSiwggyMoneyZero.put("6", "5282696");
			applyCouponApplicableWithSiwggyMoneyZero.put("7", "1");
			applyCouponApplicableWithSiwggyMoneyZero.put("8", "28157");
			applyCouponApplicableWithSiwggyMoneyZero.put("9", "3249402");
			applyCouponApplicableWithSiwggyMoneyZero.put("10", "0.0");
			applyCouponApplicableWithSiwggyMoneyZero.put("11", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("12", "1");
			applyCouponApplicableWithSiwggyMoneyZero.put("13", "200");
			applyCouponApplicableWithSiwggyMoneyZero.put("14", "5282696_0_variants_addons");
			applyCouponApplicableWithSiwggyMoneyZero.put("15", "1");
			applyCouponApplicableWithSiwggyMoneyZero.put("16", "150");
			applyCouponApplicableWithSiwggyMoneyZero.put("17", "Cash");
			applyCouponApplicableWithSiwggyMoneyZero.put("18", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("19", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("20", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("21", "0");
			applyCouponApplicableWithSiwggyMoneyZero.put("22", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("23", "245");
			applyCouponApplicableWithSiwggyMoneyZero.put("24", "ANDROID");
			applyCouponApplicableWithSiwggyMoneyZero.put("25", "null");
			applyCouponApplicableWithSiwggyMoneyZero.put("26", "null");

			return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponApplicableWithSiwggyMoney },
					{ createMultiApplyCoupon, duplicateType, applyCouponApplicableWithSiwggyMoneyZero } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponApplicableWithSwiggyMoneyData")
	public static Object[][] applyMultiApplyCouponApplicableWithSwiggyMoneyData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();

			HashMap<String, String> applyCouponApplicableWithSiwggyMoney = new HashMap<String, String>();
			String duplicateType = "false";

			// applicableWithSwiggyMoney (14) false
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", "1525046400000");
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// applicableWIthSiwggyMoney (19 & 22) true & swiggy Money(10) 25.0
			applyCouponApplicableWithSiwggyMoney.put("0", "ALPHACODE");
			applyCouponApplicableWithSiwggyMoney.put("1", "2");
			applyCouponApplicableWithSiwggyMoney.put("2", "1");
			applyCouponApplicableWithSiwggyMoney.put("3", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("4", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("5", "null");
			applyCouponApplicableWithSiwggyMoney.put("6", "5282696");
			applyCouponApplicableWithSiwggyMoney.put("7", "1");
			applyCouponApplicableWithSiwggyMoney.put("8", "28157");
			applyCouponApplicableWithSiwggyMoney.put("9", "3249402");
			applyCouponApplicableWithSiwggyMoney.put("10", "25.0");
			applyCouponApplicableWithSiwggyMoney.put("11", "false");
			applyCouponApplicableWithSiwggyMoney.put("12", "1");
			applyCouponApplicableWithSiwggyMoney.put("13", "200");
			applyCouponApplicableWithSiwggyMoney.put("14", "5282696_0_variants_addons");
			applyCouponApplicableWithSiwggyMoney.put("15", "1");
			applyCouponApplicableWithSiwggyMoney.put("16", "150");
			applyCouponApplicableWithSiwggyMoney.put("17", "Cash");
			applyCouponApplicableWithSiwggyMoney.put("18", "false");
			applyCouponApplicableWithSiwggyMoney.put("19", "true");
			applyCouponApplicableWithSiwggyMoney.put("20", "false");
			applyCouponApplicableWithSiwggyMoney.put("21", "12");
			applyCouponApplicableWithSiwggyMoney.put("22", "true");
			applyCouponApplicableWithSiwggyMoney.put("23", "245");
			applyCouponApplicableWithSiwggyMoney.put("24", "ANDROID");
			applyCouponApplicableWithSiwggyMoney.put("25", "null");
			applyCouponApplicableWithSiwggyMoney.put("26", "null");

			return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponApplicableWithSiwggyMoney } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponApplicableWithSwiggyMoneyTwoData")
	public static Object[][] applyMultiApplyCouponApplicableWithSwiggyMoneyTwoData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponApplicableWithSiwggyMoney = new HashMap<String, String>();
			HashMap<String, String> applyCouponApplicableWithSiwggyMoneyZero = new HashMap<String, String>();
			HashMap<String, String> applyCouponApplicableWithSiwggyMoneyNull = new HashMap<String, String>();
			HashMap<String, String> applyCouponIsApplicableWithSiwggyMoneyNull = new HashMap<String, String>();
			HashMap<String, String> applyCouponIsApplicableWithSiwggyMoney = new HashMap<String, String>();

			String duplicateType = "false";

			// applicableWithSwiggyMoney (14) false
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// applicableWIthSiwggyMoney (19 & 22) false & swiggy Money(10) 25.0
			applyCouponApplicableWithSiwggyMoney.put("0", "ALPHACODE");
			applyCouponApplicableWithSiwggyMoney.put("1", "2");
			applyCouponApplicableWithSiwggyMoney.put("2", "1");
			applyCouponApplicableWithSiwggyMoney.put("3", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("4", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("5", "null");
			applyCouponApplicableWithSiwggyMoney.put("6", "5282696");
			applyCouponApplicableWithSiwggyMoney.put("7", "1");
			applyCouponApplicableWithSiwggyMoney.put("8", "28157");
			applyCouponApplicableWithSiwggyMoney.put("9", "3249402");
			applyCouponApplicableWithSiwggyMoney.put("10", "25.0");
			applyCouponApplicableWithSiwggyMoney.put("11", "false");
			applyCouponApplicableWithSiwggyMoney.put("12", "1");
			applyCouponApplicableWithSiwggyMoney.put("13", "200");
			applyCouponApplicableWithSiwggyMoney.put("14", "5282696_0_variants_addons");
			applyCouponApplicableWithSiwggyMoney.put("15", "1");
			applyCouponApplicableWithSiwggyMoney.put("16", "150");
			applyCouponApplicableWithSiwggyMoney.put("17", "Cash");
			applyCouponApplicableWithSiwggyMoney.put("18", "false");
			applyCouponApplicableWithSiwggyMoney.put("19", "false");
			applyCouponApplicableWithSiwggyMoney.put("20", "false");
			applyCouponApplicableWithSiwggyMoney.put("21", "0");
			applyCouponApplicableWithSiwggyMoney.put("22", "false");
			applyCouponApplicableWithSiwggyMoney.put("23", "245");
			applyCouponApplicableWithSiwggyMoney.put("24", "ANDROID");
			applyCouponApplicableWithSiwggyMoney.put("25", "null");
			applyCouponApplicableWithSiwggyMoney.put("26", "null");

			// applicableWIthSiwggyMoney (19 & 22) false & swiggy Money(10) 0.0
			applyCouponApplicableWithSiwggyMoneyZero.put("0", "ALPHACODE");
			applyCouponApplicableWithSiwggyMoneyZero.put("1", "2");
			applyCouponApplicableWithSiwggyMoneyZero.put("2", "1");
			applyCouponApplicableWithSiwggyMoneyZero.put("3", "0.0");
			applyCouponApplicableWithSiwggyMoneyZero.put("4", "0.0");
			applyCouponApplicableWithSiwggyMoneyZero.put("5", "null");
			applyCouponApplicableWithSiwggyMoneyZero.put("6", "5282696");
			applyCouponApplicableWithSiwggyMoneyZero.put("7", "1");
			applyCouponApplicableWithSiwggyMoneyZero.put("8", "28157");
			applyCouponApplicableWithSiwggyMoneyZero.put("9", "3249402");
			applyCouponApplicableWithSiwggyMoneyZero.put("10", "0.0");
			applyCouponApplicableWithSiwggyMoneyZero.put("11", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("12", "1");
			applyCouponApplicableWithSiwggyMoneyZero.put("13", "200");
			applyCouponApplicableWithSiwggyMoneyZero.put("14", "5282696_0_variants_addons");
			applyCouponApplicableWithSiwggyMoneyZero.put("15", "1");
			applyCouponApplicableWithSiwggyMoneyZero.put("16", "150");
			applyCouponApplicableWithSiwggyMoneyZero.put("17", "Cash");
			applyCouponApplicableWithSiwggyMoneyZero.put("18", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("19", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("20", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("21", "0");
			applyCouponApplicableWithSiwggyMoneyZero.put("22", "false");
			applyCouponApplicableWithSiwggyMoneyZero.put("23", "245");
			applyCouponApplicableWithSiwggyMoneyZero.put("24", "ANDROID");
			applyCouponApplicableWithSiwggyMoneyZero.put("25", "null");
			applyCouponApplicableWithSiwggyMoneyZero.put("26", "null");

			// applicableWIthSiwggyMoney (19 & 22) true & swiggy Money(10) null
			applyCouponApplicableWithSiwggyMoneyNull.put("0", "ALPHACODE");
			applyCouponApplicableWithSiwggyMoneyNull.put("1", "2");
			applyCouponApplicableWithSiwggyMoneyNull.put("2", "1");
			applyCouponApplicableWithSiwggyMoneyNull.put("3", "0.0");
			applyCouponApplicableWithSiwggyMoneyNull.put("4", "0.0");
			applyCouponApplicableWithSiwggyMoneyNull.put("5", "null");
			applyCouponApplicableWithSiwggyMoneyNull.put("6", "5282696");
			applyCouponApplicableWithSiwggyMoneyNull.put("7", "1");
			applyCouponApplicableWithSiwggyMoneyNull.put("8", "28157");
			applyCouponApplicableWithSiwggyMoneyNull.put("9", "3249402");
			applyCouponApplicableWithSiwggyMoneyNull.put("10", "null");
			applyCouponApplicableWithSiwggyMoneyNull.put("11", "false");
			applyCouponApplicableWithSiwggyMoneyNull.put("12", "1");
			applyCouponApplicableWithSiwggyMoneyNull.put("13", "200");
			applyCouponApplicableWithSiwggyMoneyNull.put("14", "5282696_0_variants_addons");
			applyCouponApplicableWithSiwggyMoneyNull.put("15", "1");
			applyCouponApplicableWithSiwggyMoneyNull.put("16", "150");
			applyCouponApplicableWithSiwggyMoneyNull.put("17", "Cash");
			applyCouponApplicableWithSiwggyMoneyNull.put("18", "false");
			applyCouponApplicableWithSiwggyMoneyNull.put("19", "true");
			applyCouponApplicableWithSiwggyMoneyNull.put("20", "false");
			applyCouponApplicableWithSiwggyMoneyNull.put("21", "0");
			applyCouponApplicableWithSiwggyMoneyNull.put("22", "true");
			applyCouponApplicableWithSiwggyMoneyNull.put("23", "245");
			applyCouponApplicableWithSiwggyMoneyNull.put("24", "ANDROID");
			applyCouponApplicableWithSiwggyMoneyNull.put("25", "null");
			applyCouponApplicableWithSiwggyMoneyNull.put("26", "null");

			// applicableWIthSiwggyMoney (19 & 22) null & swiggy Money(10) 0.0
			applyCouponIsApplicableWithSiwggyMoneyNull.put("0", "ALPHACODE");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("1", "2");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("2", "1");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("3", "0.0");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("4", "0.0");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("5", "null");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("6", "5282696");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("7", "1");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("8", "28157");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("9", "3249402");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("10", "0.0");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("11", "false");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("12", "1");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("13", "200");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("14", "5282696_0_variants_addons");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("15", "1");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("16", "150");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("17", "Cash");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("18", "false");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("19", "null");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("20", "false");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("21", "0");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("22", "null");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("23", "245");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("24", "ANDROID");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("25", "null");
			applyCouponIsApplicableWithSiwggyMoneyNull.put("26", "null");

			// applicableWIthSiwggyMoney (19 & 22) null & swiggy Money(10) null
			applyCouponIsApplicableWithSiwggyMoney.put("0", "ALPHACODE");
			applyCouponIsApplicableWithSiwggyMoney.put("1", "2");
			applyCouponIsApplicableWithSiwggyMoney.put("2", "1");
			applyCouponIsApplicableWithSiwggyMoney.put("3", "0.0");
			applyCouponIsApplicableWithSiwggyMoney.put("4", "0.0");
			applyCouponIsApplicableWithSiwggyMoney.put("5", "null");
			applyCouponIsApplicableWithSiwggyMoney.put("6", "5282696");
			applyCouponIsApplicableWithSiwggyMoney.put("7", "1");
			applyCouponIsApplicableWithSiwggyMoney.put("8", "28157");
			applyCouponIsApplicableWithSiwggyMoney.put("9", "3249402");
			applyCouponIsApplicableWithSiwggyMoney.put("10", "null");
			applyCouponIsApplicableWithSiwggyMoney.put("11", "false");
			applyCouponIsApplicableWithSiwggyMoney.put("12", "1");
			applyCouponIsApplicableWithSiwggyMoney.put("13", "200");
			applyCouponIsApplicableWithSiwggyMoney.put("14", "5282696_0_variants_addons");
			applyCouponIsApplicableWithSiwggyMoney.put("15", "1");
			applyCouponIsApplicableWithSiwggyMoney.put("16", "150");
			applyCouponIsApplicableWithSiwggyMoney.put("17", "Cash");
			applyCouponIsApplicableWithSiwggyMoney.put("18", "false");
			applyCouponIsApplicableWithSiwggyMoney.put("19", "null");
			applyCouponIsApplicableWithSiwggyMoney.put("20", "false");
			applyCouponIsApplicableWithSiwggyMoney.put("21", "0");
			applyCouponIsApplicableWithSiwggyMoney.put("22", "null");
			applyCouponIsApplicableWithSiwggyMoney.put("23", "245");
			applyCouponIsApplicableWithSiwggyMoney.put("24", "ANDROID");
			applyCouponIsApplicableWithSiwggyMoney.put("25", "null");
			applyCouponIsApplicableWithSiwggyMoney.put("26", "null");

			return new Object[][] {
					// { createMultiApplyCoupon, duplicateType, applyCouponApplicableWithSiwggyMoney
					// },
					// { createMultiApplyCoupon, duplicateType,
					// applyCouponApplicableWithSiwggyMoneyZero },
					// { createMultiApplyCoupon, duplicateType,
					// applyCouponApplicableWithSiwggyMoneyNull },
					// { createMultiApplyCoupon, duplicateType,
					// applyCouponIsApplicableWithSiwggyMoneyNull },
					{ createMultiApplyCoupon, duplicateType, applyCouponIsApplicableWithSiwggyMoney }, };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponIsApplicableWithSwiggyMoneyData")
	public static Object[][] applyMultiApplyCouponIsApplicableWithSwiggyMoneyData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponIsApplicableWithSiwggyMoney = new HashMap<String, String>();

			String duplicateType = "false";

			// applicableWithSwiggyMoney (14) false
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", "1525046400000");
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// applicableWIthSiwggyMoney (19 & 22) null & swiggy Money(10) 10.0
			applyCouponIsApplicableWithSiwggyMoney.put("0", "ALPHACODE");
			applyCouponIsApplicableWithSiwggyMoney.put("1", "2");
			applyCouponIsApplicableWithSiwggyMoney.put("2", "1");
			applyCouponIsApplicableWithSiwggyMoney.put("3", "0.0");
			applyCouponIsApplicableWithSiwggyMoney.put("4", "0.0");
			applyCouponIsApplicableWithSiwggyMoney.put("5", "null");
			applyCouponIsApplicableWithSiwggyMoney.put("6", "5282696");
			applyCouponIsApplicableWithSiwggyMoney.put("7", "1");
			applyCouponIsApplicableWithSiwggyMoney.put("8", "28157");
			applyCouponIsApplicableWithSiwggyMoney.put("9", "3249402");
			applyCouponIsApplicableWithSiwggyMoney.put("10", "10.0");
			applyCouponIsApplicableWithSiwggyMoney.put("11", "false");
			applyCouponIsApplicableWithSiwggyMoney.put("12", "1");
			applyCouponIsApplicableWithSiwggyMoney.put("13", "200");
			applyCouponIsApplicableWithSiwggyMoney.put("14", "5282696_0_variants_addons");
			applyCouponIsApplicableWithSiwggyMoney.put("15", "1");
			applyCouponIsApplicableWithSiwggyMoney.put("16", "150");
			applyCouponIsApplicableWithSiwggyMoney.put("17", "Cash");
			applyCouponIsApplicableWithSiwggyMoney.put("18", "false");
			applyCouponIsApplicableWithSiwggyMoney.put("19", "null");
			applyCouponIsApplicableWithSiwggyMoney.put("20", "false");
			applyCouponIsApplicableWithSiwggyMoney.put("21", "12");
			applyCouponIsApplicableWithSiwggyMoney.put("22", "null");
			applyCouponIsApplicableWithSiwggyMoney.put("23", "245");
			applyCouponIsApplicableWithSiwggyMoney.put("24", "ANDROID");
			applyCouponIsApplicableWithSiwggyMoney.put("25", "null");
			applyCouponIsApplicableWithSiwggyMoney.put("26", "null");

			return new Object[][] {
					{ createMultiApplyCoupon, duplicateType, applyCouponIsApplicableWithSiwggyMoney }, };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponApplyDateIsGreaterThanValidTillDateData")
	public static Object[][] applyMultiApplyCouponApplyDateIsGreaterThanValidTillDateData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponApplicableWithSiwggyMoney = new HashMap<String, String>();
			String duplicateType = "false";

			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", "1525046400000");
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// time is (26) 1525910400000
			applyCouponApplicableWithSiwggyMoney.put("0", "ALPHACODE");
			applyCouponApplicableWithSiwggyMoney.put("1", "2");
			applyCouponApplicableWithSiwggyMoney.put("2", "1");
			applyCouponApplicableWithSiwggyMoney.put("3", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("4", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("5", "null");
			applyCouponApplicableWithSiwggyMoney.put("6", "5282696");
			applyCouponApplicableWithSiwggyMoney.put("7", "1");
			applyCouponApplicableWithSiwggyMoney.put("8", "28157");
			applyCouponApplicableWithSiwggyMoney.put("9", "3249402");
			applyCouponApplicableWithSiwggyMoney.put("10", "0.0");
			applyCouponApplicableWithSiwggyMoney.put("11", "false");
			applyCouponApplicableWithSiwggyMoney.put("12", "1");
			applyCouponApplicableWithSiwggyMoney.put("13", "200");
			applyCouponApplicableWithSiwggyMoney.put("14", "5282696_0_variants_addons");
			applyCouponApplicableWithSiwggyMoney.put("15", "1");
			applyCouponApplicableWithSiwggyMoney.put("16", "150");
			applyCouponApplicableWithSiwggyMoney.put("17", "Cash");
			applyCouponApplicableWithSiwggyMoney.put("18", "false");
			applyCouponApplicableWithSiwggyMoney.put("19", "false");
			applyCouponApplicableWithSiwggyMoney.put("20", "false");
			applyCouponApplicableWithSiwggyMoney.put("21", "0");
			applyCouponApplicableWithSiwggyMoney.put("22", "false");
			applyCouponApplicableWithSiwggyMoney.put("23", "245");
			applyCouponApplicableWithSiwggyMoney.put("24", "ANDROID");
			applyCouponApplicableWithSiwggyMoney.put("25", "null");
			applyCouponApplicableWithSiwggyMoney.put("26", "1525910400000");

			return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponApplicableWithSiwggyMoney } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponApplyDateIsWithInOrEqualTOValidTillDateData")
	public static Object[][] applyMultiApplyCouponApplyDateIsWithInOrEqualTOValidTillDateData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponTimeIsWithInValidTillDate = new HashMap<String, String>();
			HashMap<String, String> applyCouponDateSameAsValidTillDate = new HashMap<String, String>();

			String duplicateType = "false";
			// coupon valid from (17) and valid is (18)
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", "1525046400000");
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// time is (26) 1522627200000 with in the valid till date
			applyCouponTimeIsWithInValidTillDate.put("0", "ALPHACODE");
			applyCouponTimeIsWithInValidTillDate.put("1", "2");
			applyCouponTimeIsWithInValidTillDate.put("2", "1");
			applyCouponTimeIsWithInValidTillDate.put("3", "0.0");
			applyCouponTimeIsWithInValidTillDate.put("4", "0.0");
			applyCouponTimeIsWithInValidTillDate.put("5", "null");
			applyCouponTimeIsWithInValidTillDate.put("6", "5282696");
			applyCouponTimeIsWithInValidTillDate.put("7", "1");
			applyCouponTimeIsWithInValidTillDate.put("8", "28157");
			applyCouponTimeIsWithInValidTillDate.put("9", "3249402");
			applyCouponTimeIsWithInValidTillDate.put("10", "0.0");
			applyCouponTimeIsWithInValidTillDate.put("11", "false");
			applyCouponTimeIsWithInValidTillDate.put("12", "1");
			applyCouponTimeIsWithInValidTillDate.put("13", "200");
			applyCouponTimeIsWithInValidTillDate.put("14", "5282696_0_variants_addons");
			applyCouponTimeIsWithInValidTillDate.put("15", "1");
			applyCouponTimeIsWithInValidTillDate.put("16", "150");
			applyCouponTimeIsWithInValidTillDate.put("17", "Cash");
			applyCouponTimeIsWithInValidTillDate.put("18", "false");
			applyCouponTimeIsWithInValidTillDate.put("19", "false");
			applyCouponTimeIsWithInValidTillDate.put("20", "false");
			applyCouponTimeIsWithInValidTillDate.put("21", "0");
			applyCouponTimeIsWithInValidTillDate.put("22", "false");
			applyCouponTimeIsWithInValidTillDate.put("23", "245");
			applyCouponTimeIsWithInValidTillDate.put("24", "ANDROID");
			applyCouponTimeIsWithInValidTillDate.put("25", "null");
			applyCouponTimeIsWithInValidTillDate.put("26", "1522627200000");

			// time is (26) 1525046400000 apply time is same as valid till date
						applyCouponDateSameAsValidTillDate.put("0", "ALPHACODE");
						applyCouponDateSameAsValidTillDate.put("1", "2");
						applyCouponDateSameAsValidTillDate.put("2", "1");
						applyCouponDateSameAsValidTillDate.put("3", "0.0");
						applyCouponDateSameAsValidTillDate.put("4", "0.0");
						applyCouponDateSameAsValidTillDate.put("5", "null");
						applyCouponDateSameAsValidTillDate.put("6", "5282696");
						applyCouponDateSameAsValidTillDate.put("7", "1");
						applyCouponDateSameAsValidTillDate.put("8", "28157");
						applyCouponDateSameAsValidTillDate.put("9", "3249402");
						applyCouponDateSameAsValidTillDate.put("10", "0.0");
						applyCouponDateSameAsValidTillDate.put("11", "false");
						applyCouponDateSameAsValidTillDate.put("12", "1");
						applyCouponDateSameAsValidTillDate.put("13", "200");
						applyCouponDateSameAsValidTillDate.put("14", "5282696_0_variants_addons");
						applyCouponDateSameAsValidTillDate.put("15", "1");
						applyCouponDateSameAsValidTillDate.put("16", "150");
						applyCouponDateSameAsValidTillDate.put("17", "Cash");
						applyCouponDateSameAsValidTillDate.put("18", "false");
						applyCouponDateSameAsValidTillDate.put("19", "false");
						applyCouponDateSameAsValidTillDate.put("20", "false");
						applyCouponDateSameAsValidTillDate.put("21", "0");
						applyCouponDateSameAsValidTillDate.put("22", "false");
						applyCouponDateSameAsValidTillDate.put("23", "245");
						applyCouponDateSameAsValidTillDate.put("24", "ANDROID");
						applyCouponDateSameAsValidTillDate.put("25", "null");
						applyCouponDateSameAsValidTillDate.put("26", "1525046400000");

						return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponTimeIsWithInValidTillDate },
								{ createMultiApplyCoupon, duplicateType,
										applyCouponDateSameAsValidTillDate } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponApplyDateIsSameValidFromDateData")
	public static Object[][] applyMultiApplyCouponApplyDateIsSameValidFromDateData() {
		{
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponForFirstTransaction = new HashMap<String, String>();
			HashMap<String, String> applyCouponDateSetAsNullForFirstTransaction = new HashMap<String, String>();
			HashMap<String, String> applyCouponDateSetAsNullForSecondTransaction = new HashMap<String, String>();
			HashMap<String, String> applyCouponForSecondTransaction = new HashMap<String, String>();

			String duplicateType = "false";
			// coupon offSet (25) -1, transaction one offset (49) 2 , transaction two offset
			// (67) 2
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1518934320000");
			createMultiApplyCoupon.put("18", Utility.getFuturetimeInMilliSeconds(345600000));
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "2");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Discount");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "10");
			createMultiApplyCoupon.put("56", "90");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "2");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// time is (26) 1518934320000 (apply date Same as valid from date of coupon)
			applyCouponForFirstTransaction.put("0", "ALPHACODE");
			applyCouponForFirstTransaction.put("1", "2");
			applyCouponForFirstTransaction.put("2", "1");
			applyCouponForFirstTransaction.put("3", "0.0");
			applyCouponForFirstTransaction.put("4", "0.0");
			applyCouponForFirstTransaction.put("5", "null");
			applyCouponForFirstTransaction.put("6", "5282696");
			applyCouponForFirstTransaction.put("7", "1");
			applyCouponForFirstTransaction.put("8", "28157");
			applyCouponForFirstTransaction.put("9", "3249402");
			applyCouponForFirstTransaction.put("10", "0.0");
			applyCouponForFirstTransaction.put("11", "false");
			applyCouponForFirstTransaction.put("12", "1");
			applyCouponForFirstTransaction.put("13", "200");
			applyCouponForFirstTransaction.put("14", "5282696_0_variants_addons");
			applyCouponForFirstTransaction.put("15", "1");
			applyCouponForFirstTransaction.put("16", "150");
			applyCouponForFirstTransaction.put("17", "Cash");
			applyCouponForFirstTransaction.put("18", "false");
			applyCouponForFirstTransaction.put("19", "false");
			applyCouponForFirstTransaction.put("20", "false");
			applyCouponForFirstTransaction.put("21", "0");
			applyCouponForFirstTransaction.put("22", "false");
			applyCouponForFirstTransaction.put("23", "245");
			applyCouponForFirstTransaction.put("24", "ANDROID");
			applyCouponForFirstTransaction.put("25", "null");
			applyCouponForFirstTransaction.put("26", "1518934320000");

			// time is (26) 1518934320000 (apply date Same as valid from date of coupon)
			applyCouponForSecondTransaction.put("0", "ALPHACODE");
			applyCouponForSecondTransaction.put("1", "2");
			applyCouponForSecondTransaction.put("2", "1");
			applyCouponForSecondTransaction.put("3", "0.0");
			applyCouponForSecondTransaction.put("4", "0.0");
			applyCouponForSecondTransaction.put("5", "null");
			applyCouponForSecondTransaction.put("6", "5282696");
			applyCouponForSecondTransaction.put("7", "1");
			applyCouponForSecondTransaction.put("8", "28157");
			applyCouponForSecondTransaction.put("9", "3249402");
			applyCouponForSecondTransaction.put("10", "0.0");
			applyCouponForSecondTransaction.put("11", "false");
			applyCouponForSecondTransaction.put("12", "1");
			applyCouponForSecondTransaction.put("13", "200");
			applyCouponForSecondTransaction.put("14", "5282696_0_variants_addons");
			applyCouponForSecondTransaction.put("15", "1");
			applyCouponForSecondTransaction.put("16", "150");
			applyCouponForSecondTransaction.put("17", "Cash");
			applyCouponForSecondTransaction.put("18", "false");
			applyCouponForSecondTransaction.put("19", "false");
			applyCouponForSecondTransaction.put("20", "false");
			applyCouponForSecondTransaction.put("21", "0");
			applyCouponForSecondTransaction.put("22", "false");
			applyCouponForSecondTransaction.put("23", "245");
			applyCouponForSecondTransaction.put("24", "ANDROID");
			applyCouponForSecondTransaction.put("25", "null");
			applyCouponForSecondTransaction.put("26", "1518934320000");

			// time is (26) null (in case of "Time" as null it will take present value)
			applyCouponDateSetAsNullForFirstTransaction.put("0", "ALPHACODE");
			applyCouponDateSetAsNullForFirstTransaction.put("1", "2");
			applyCouponDateSetAsNullForFirstTransaction.put("2", "1");
			applyCouponDateSetAsNullForFirstTransaction.put("3", "0.0");
			applyCouponDateSetAsNullForFirstTransaction.put("4", "0.0");
			applyCouponDateSetAsNullForFirstTransaction.put("5", "null");
			applyCouponDateSetAsNullForFirstTransaction.put("6", "5282696");
			applyCouponDateSetAsNullForFirstTransaction.put("7", "1");
			applyCouponDateSetAsNullForFirstTransaction.put("8", "28157");
			applyCouponDateSetAsNullForFirstTransaction.put("9", "3249402");
			applyCouponDateSetAsNullForFirstTransaction.put("10", "0.0");
			applyCouponDateSetAsNullForFirstTransaction.put("11", "false");
			applyCouponDateSetAsNullForFirstTransaction.put("12", "1");
			applyCouponDateSetAsNullForFirstTransaction.put("13", "200");
			applyCouponDateSetAsNullForFirstTransaction.put("14", "5282696_0_variants_addons");
			applyCouponDateSetAsNullForFirstTransaction.put("15", "1");
			applyCouponDateSetAsNullForFirstTransaction.put("16", "150");
			applyCouponDateSetAsNullForFirstTransaction.put("17", "Cash");
			applyCouponDateSetAsNullForFirstTransaction.put("18", "false");
			applyCouponDateSetAsNullForFirstTransaction.put("19", "false");
			applyCouponDateSetAsNullForFirstTransaction.put("20", "false");
			applyCouponDateSetAsNullForFirstTransaction.put("21", "0");
			applyCouponDateSetAsNullForFirstTransaction.put("22", "false");
			applyCouponDateSetAsNullForFirstTransaction.put("23", "245");
			applyCouponDateSetAsNullForFirstTransaction.put("24", "ANDROID");
			applyCouponDateSetAsNullForFirstTransaction.put("25", "null");
			applyCouponDateSetAsNullForFirstTransaction.put("26", "null");

			// time is (26) null (in case of "Time" as null it will take present value)
			applyCouponDateSetAsNullForSecondTransaction.put("0", "ALPHACODE");
			applyCouponDateSetAsNullForSecondTransaction.put("1", "2");
			applyCouponDateSetAsNullForSecondTransaction.put("2", "1");
			applyCouponDateSetAsNullForSecondTransaction.put("3", "0.0");
			applyCouponDateSetAsNullForSecondTransaction.put("4", "0.0");
			applyCouponDateSetAsNullForSecondTransaction.put("5", "null");
			applyCouponDateSetAsNullForSecondTransaction.put("6", "5282696");
			applyCouponDateSetAsNullForSecondTransaction.put("7", "1");
			applyCouponDateSetAsNullForSecondTransaction.put("8", "28157");
			applyCouponDateSetAsNullForSecondTransaction.put("9", "3249402");
			applyCouponDateSetAsNullForSecondTransaction.put("10", "0.0");
			applyCouponDateSetAsNullForSecondTransaction.put("11", "false");
			applyCouponDateSetAsNullForSecondTransaction.put("12", "1");
			applyCouponDateSetAsNullForSecondTransaction.put("13", "200");
			applyCouponDateSetAsNullForSecondTransaction.put("14", "5282696_0_variants_addons");
			applyCouponDateSetAsNullForSecondTransaction.put("15", "1");
			applyCouponDateSetAsNullForSecondTransaction.put("16", "150");
			applyCouponDateSetAsNullForSecondTransaction.put("17", "Cash");
			applyCouponDateSetAsNullForSecondTransaction.put("18", "false");
			applyCouponDateSetAsNullForSecondTransaction.put("19", "false");
			applyCouponDateSetAsNullForSecondTransaction.put("20", "false");
			applyCouponDateSetAsNullForSecondTransaction.put("21", "0");
			applyCouponDateSetAsNullForSecondTransaction.put("22", "false");
			applyCouponDateSetAsNullForSecondTransaction.put("23", "245");
			applyCouponDateSetAsNullForSecondTransaction.put("24", "ANDROID");
			applyCouponDateSetAsNullForSecondTransaction.put("25", "null");
			applyCouponDateSetAsNullForSecondTransaction.put("26", "null");

			return new Object[][] {
					{ createMultiApplyCoupon, duplicateType, applyCouponForFirstTransaction,
							applyCouponForSecondTransaction },
					{ createMultiApplyCoupon, duplicateType, applyCouponDateSetAsNullForFirstTransaction,
							applyCouponDateSetAsNullForSecondTransaction } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponTransactionOffsetMinusOneDateData")
	public static Object[][] applyMultiApplyCouponTransactionOffsetMinusOneDateData() {
		{
			HashMap<String, String> applyCouponForFirstTransaction = new HashMap<String, String>();
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponForSecondTransaction = new HashMap<String, String>();
			String duplicateType = "false";

			// expiry offset (25, 49 & 67) -1
			// Coupon type (33) for 1st transaction is Discount
			// Coupon type (51) for 1st transaction is Discount
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1522627200000");
			createMultiApplyCoupon.put("18", "1525046400000");
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "-1");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Flat");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "0");
			createMultiApplyCoupon.put("56", "40");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "-1");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// time today date
			applyCouponForFirstTransaction.put("0", "ALPHACODE");
			applyCouponForFirstTransaction.put("1", "2");
			applyCouponForFirstTransaction.put("2", "1");
			applyCouponForFirstTransaction.put("3", "0.0");
			applyCouponForFirstTransaction.put("4", "0.0");
			applyCouponForFirstTransaction.put("5", "null");
			applyCouponForFirstTransaction.put("6", "5282696");
			applyCouponForFirstTransaction.put("7", "1");
			applyCouponForFirstTransaction.put("8", "28157");
			applyCouponForFirstTransaction.put("9", "3249402");
			applyCouponForFirstTransaction.put("10", "0.0");
			applyCouponForFirstTransaction.put("11", "false");
			applyCouponForFirstTransaction.put("12", "1");
			applyCouponForFirstTransaction.put("13", "200");
			applyCouponForFirstTransaction.put("14", "5282696_0_variants_addons");
			applyCouponForFirstTransaction.put("15", "1");
			applyCouponForFirstTransaction.put("16", "150");
			applyCouponForFirstTransaction.put("17", "Cash");
			applyCouponForFirstTransaction.put("18", "false");
			applyCouponForFirstTransaction.put("19", "false");
			applyCouponForFirstTransaction.put("20", "false");
			applyCouponForFirstTransaction.put("21", "0");
			applyCouponForFirstTransaction.put("22", "false");
			applyCouponForFirstTransaction.put("23", "245");
			applyCouponForFirstTransaction.put("24", "ANDROID");
			applyCouponForFirstTransaction.put("25", "null");
			applyCouponForFirstTransaction.put("26", "1522627200000");

			// apply time is 4th day from day created
			applyCouponForSecondTransaction.put("0", "ALPHACODE");
			applyCouponForSecondTransaction.put("1", "2");
			applyCouponForSecondTransaction.put("2", "1");
			applyCouponForSecondTransaction.put("3", "0.0");
			applyCouponForSecondTransaction.put("4", "0.0");
			applyCouponForSecondTransaction.put("5", "null");
			applyCouponForSecondTransaction.put("6", "5282696");
			applyCouponForSecondTransaction.put("7", "1");
			applyCouponForSecondTransaction.put("8", "28157");
			applyCouponForSecondTransaction.put("9", "3249402");
			applyCouponForSecondTransaction.put("10", "0.0");
			applyCouponForSecondTransaction.put("11", "false");
			applyCouponForSecondTransaction.put("12", "1");
			applyCouponForSecondTransaction.put("13", "200");
			applyCouponForSecondTransaction.put("14", "5282696_0_variants_addons");
			applyCouponForSecondTransaction.put("15", "1");
			applyCouponForSecondTransaction.put("16", "150");
			applyCouponForSecondTransaction.put("17", "Cash");
			applyCouponForSecondTransaction.put("18", "false");
			applyCouponForSecondTransaction.put("19", "false");
			applyCouponForSecondTransaction.put("20", "false");
			applyCouponForSecondTransaction.put("21", "0");
			applyCouponForSecondTransaction.put("22", "false");
			applyCouponForSecondTransaction.put("23", "245");
			applyCouponForSecondTransaction.put("24", "ANDROID");
			applyCouponForSecondTransaction.put("25", "null");
			applyCouponForSecondTransaction.put("26", "1522886400000");

			return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponForFirstTransaction,
					applyCouponForSecondTransaction } };

		}
	}

	@DataProvider(name = "applyMultiApplyCouponCheckCouponTypeOfEachTransactionData")
	public static Object[][] applyMultiApplyCouponCheckCouponTypeOfEachTransactionData() {
		{
			HashMap<String, String> applyCouponForFirstTransaction = new HashMap<String, String>();
			HashMap<String, String> createMultiApplyCoupon = new HashMap<String, String>();
			HashMap<String, String> applyCouponForSecondTransaction = new HashMap<String, String>();
			String duplicateType = "false";

			// expiry offset (25, 49 & 67) -1
			// Coupon type (33) for 1st transaction is Discount
			// Coupon type (51) for 2nd transaction is Discount
			createMultiApplyCoupon.put("0", "ALPHACODE");
			createMultiApplyCoupon.put("1", "MULTICOUPON");
			createMultiApplyCoupon.put("2", "Internal");
			createMultiApplyCoupon.put("3", "AUTOMATION");
			createMultiApplyCoupon.put("4", "2");
			createMultiApplyCoupon.put("5", "10");
			createMultiApplyCoupon.put("6", "false");
			createMultiApplyCoupon.put("7", "false");
			createMultiApplyCoupon.put("8", "false");
			createMultiApplyCoupon.put("9", "false");
			createMultiApplyCoupon.put("10", "false");
			createMultiApplyCoupon.put("11", "false");
			createMultiApplyCoupon.put("12", "false");
			createMultiApplyCoupon.put("13", "false");
			createMultiApplyCoupon.put("14", "false");
			createMultiApplyCoupon.put("15", "false");
			createMultiApplyCoupon.put("16", "false");
			createMultiApplyCoupon.put("17", "1522627200000");
			createMultiApplyCoupon.put("18", "1525046400000");
			createMultiApplyCoupon.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyCoupon.put("20", "1518934320000");
			createMultiApplyCoupon.put("21", "1518934320000");
			createMultiApplyCoupon.put("22", "null");
			createMultiApplyCoupon.put("23", "null");
			createMultiApplyCoupon.put("24", "null");
			createMultiApplyCoupon.put("25", "-1");
			createMultiApplyCoupon.put("26", "false");
			createMultiApplyCoupon.put("27", "0");
			createMultiApplyCoupon.put("28", "null");
			createMultiApplyCoupon.put("29", "null");
			createMultiApplyCoupon.put("30", "null");
			createMultiApplyCoupon.put("31", "null");
			createMultiApplyCoupon.put("32", "1");
			createMultiApplyCoupon.put("33", "Discount");
			createMultiApplyCoupon.put("34", "10");
			createMultiApplyCoupon.put("35", "1");
			createMultiApplyCoupon.put("36", "50");
			createMultiApplyCoupon.put("37", "10");
			createMultiApplyCoupon.put("38", "90");
			createMultiApplyCoupon.put("39", "false");
			createMultiApplyCoupon.put("40", "0");
			createMultiApplyCoupon.put("41", "-1");
			createMultiApplyCoupon.put("42", "1st transaction desc.");
			createMultiApplyCoupon.put("43", "1st transaction");
			createMultiApplyCoupon.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("46", "false");
			createMultiApplyCoupon.put("47", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("48", "ANDROID");
			createMultiApplyCoupon.put("49", "-1");
			createMultiApplyCoupon.put("50", "2");
			createMultiApplyCoupon.put("51", "Flat");
			createMultiApplyCoupon.put("52", "10");
			createMultiApplyCoupon.put("53", "1");
			createMultiApplyCoupon.put("54", "50");
			createMultiApplyCoupon.put("55", "0");
			createMultiApplyCoupon.put("56", "40");
			createMultiApplyCoupon.put("57", "false");
			createMultiApplyCoupon.put("58", "0");
			createMultiApplyCoupon.put("59", "-1");
			createMultiApplyCoupon.put("60", "1st transaction desc.");
			createMultiApplyCoupon.put("61", "1st transaction");
			createMultiApplyCoupon.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyCoupon.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyCoupon.put("64", "false");
			createMultiApplyCoupon.put("65", "\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
					+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyCoupon.put("66", "ANDROID");
			createMultiApplyCoupon.put("67", "-1");
			createMultiApplyCoupon.put("68", "false");
			createMultiApplyCoupon.put("69", "false");
			createMultiApplyCoupon.put("70", "false");

			// time today date
			applyCouponForFirstTransaction.put("0", "ALPHACODE");
			applyCouponForFirstTransaction.put("1", "2");
			applyCouponForFirstTransaction.put("2", "1");
			applyCouponForFirstTransaction.put("3", "0.0");
			applyCouponForFirstTransaction.put("4", "0.0");
			applyCouponForFirstTransaction.put("5", "null");
			applyCouponForFirstTransaction.put("6", "5282696");
			applyCouponForFirstTransaction.put("7", "1");
			applyCouponForFirstTransaction.put("8", "28157");
			applyCouponForFirstTransaction.put("9", "3249402");
			applyCouponForFirstTransaction.put("10", "0.0");
			applyCouponForFirstTransaction.put("11", "false");
			applyCouponForFirstTransaction.put("12", "1");
			applyCouponForFirstTransaction.put("13", "200");
			applyCouponForFirstTransaction.put("14", "5282696_0_variants_addons");
			applyCouponForFirstTransaction.put("15", "1");
			applyCouponForFirstTransaction.put("16", "150");
			applyCouponForFirstTransaction.put("17", "Cash");
			applyCouponForFirstTransaction.put("18", "false");
			applyCouponForFirstTransaction.put("19", "false");
			applyCouponForFirstTransaction.put("20", "false");
			applyCouponForFirstTransaction.put("21", "0");
			applyCouponForFirstTransaction.put("22", "false");
			applyCouponForFirstTransaction.put("23", "245");
			applyCouponForFirstTransaction.put("24", "ANDROID");
			applyCouponForFirstTransaction.put("25", "null");
			applyCouponForFirstTransaction.put("26", "1522800000000");

			// apply time is same as in transaction one (today date)
			applyCouponForSecondTransaction.put("0", "ALPHACODE");
			applyCouponForSecondTransaction.put("1", "2");
			applyCouponForSecondTransaction.put("2", "1");
			applyCouponForSecondTransaction.put("3", "0.0");
			applyCouponForSecondTransaction.put("4", "0.0");
			applyCouponForSecondTransaction.put("5", "null");
			applyCouponForSecondTransaction.put("6", "5282696");
			applyCouponForSecondTransaction.put("7", "1");
			applyCouponForSecondTransaction.put("8", "28157");
			applyCouponForSecondTransaction.put("9", "3249402");
			applyCouponForSecondTransaction.put("10", "0.0");
			applyCouponForSecondTransaction.put("11", "false");
			applyCouponForSecondTransaction.put("12", "1");
			applyCouponForSecondTransaction.put("13", "200");
			applyCouponForSecondTransaction.put("14", "5282696_0_variants_addons");
			applyCouponForSecondTransaction.put("15", "1");
			applyCouponForSecondTransaction.put("16", "150");
			applyCouponForSecondTransaction.put("17", "Cash");
			applyCouponForSecondTransaction.put("18", "false");
			applyCouponForSecondTransaction.put("19", "false");
			applyCouponForSecondTransaction.put("20", "false");
			applyCouponForSecondTransaction.put("21", "0");
			applyCouponForSecondTransaction.put("22", "false");
			applyCouponForSecondTransaction.put("23", "245");
			applyCouponForSecondTransaction.put("24", "ANDROID");
			applyCouponForSecondTransaction.put("25", "null");
			applyCouponForSecondTransaction.put("26", "1522800000000");

			return new Object[][] { { createMultiApplyCoupon, duplicateType, applyCouponForFirstTransaction,
					applyCouponForSecondTransaction } };

		}
	}

	
	
	@DataProvider(name = "applyMultiApplyCouponToCheckCartMinAmountData")
	public static Object[][] applyMultiApplyCouponToCheckCartMinAmount() {
		{
			HashMap<String, String> createMultiApplyWithMinCartAmount = new HashMap<String, String>();
			HashMap<String, String> applyCouponMinCartAmount = new HashMap<String, String>();
			String duplicateType="false";
			
		//minCartAmount (34 & 52) 99
			createMultiApplyWithMinCartAmount.put("0", "ALPHACODE"); 
			createMultiApplyWithMinCartAmount.put("1", "MULTICOUPON");
			createMultiApplyWithMinCartAmount.put("2", "Internal");
			createMultiApplyWithMinCartAmount.put("3", "AUTOMATION");
			createMultiApplyWithMinCartAmount.put("4", "2");
			createMultiApplyWithMinCartAmount.put("5", "10");
			createMultiApplyWithMinCartAmount.put("6", "false");
			createMultiApplyWithMinCartAmount.put("7", "false");
			createMultiApplyWithMinCartAmount.put("8", "false");
			createMultiApplyWithMinCartAmount.put("9", "false");
			createMultiApplyWithMinCartAmount.put("10", "false");
			createMultiApplyWithMinCartAmount.put("11", "false");
			createMultiApplyWithMinCartAmount.put("12", "false");
			createMultiApplyWithMinCartAmount.put("13", "false");
			createMultiApplyWithMinCartAmount.put("14", "false");
			createMultiApplyWithMinCartAmount.put("15", "false");
			createMultiApplyWithMinCartAmount.put("16", "false");
			
			createMultiApplyWithMinCartAmount.put("17", "1518934320000");
			createMultiApplyWithMinCartAmount.put("18", "1519279920000");
			createMultiApplyWithMinCartAmount.put("19", "ankita.yadav@swiggy.in");
			createMultiApplyWithMinCartAmount.put("20", "1518934320000");
			createMultiApplyWithMinCartAmount.put("21", "1518934320000");
			createMultiApplyWithMinCartAmount.put("22", "null");
			createMultiApplyWithMinCartAmount.put("23", "null");
			createMultiApplyWithMinCartAmount.put("24", "null");
			createMultiApplyWithMinCartAmount.put("25", "-1");
			createMultiApplyWithMinCartAmount.put("26", "false");
			createMultiApplyWithMinCartAmount.put("27", "0");
			createMultiApplyWithMinCartAmount.put("28", "null");
			createMultiApplyWithMinCartAmount.put("29", "null");
			createMultiApplyWithMinCartAmount.put("30", "null");
			createMultiApplyWithMinCartAmount.put("31", "null");
			createMultiApplyWithMinCartAmount.put("32", "1");
			createMultiApplyWithMinCartAmount.put("33", "Discount");
			createMultiApplyWithMinCartAmount.put("34", "99");
			createMultiApplyWithMinCartAmount.put("35", "1");
			createMultiApplyWithMinCartAmount.put("36", "50");
			createMultiApplyWithMinCartAmount.put("37", "10");
			createMultiApplyWithMinCartAmount.put("38", "90");
			createMultiApplyWithMinCartAmount.put("39", "false");
			createMultiApplyWithMinCartAmount.put("40", "0");
			createMultiApplyWithMinCartAmount.put("41", "-1");
			createMultiApplyWithMinCartAmount.put("42", "1st transaction desc.");
			createMultiApplyWithMinCartAmount.put("43", "1st transaction");
			createMultiApplyWithMinCartAmount.put("44", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyWithMinCartAmount.put("45", "w2fh4c46d0bszhl89voy");
			createMultiApplyWithMinCartAmount.put("46", "false");
			createMultiApplyWithMinCartAmount.put("47",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyWithMinCartAmount.put("48", "ANDROID");
			createMultiApplyWithMinCartAmount.put("49", "2");
			createMultiApplyWithMinCartAmount.put("50", "2");
			createMultiApplyWithMinCartAmount.put("51", "Discount");
			createMultiApplyWithMinCartAmount.put("52", "99");
			createMultiApplyWithMinCartAmount.put("53", "1");
			createMultiApplyWithMinCartAmount.put("54", "50");
			createMultiApplyWithMinCartAmount.put("55", "10");
			createMultiApplyWithMinCartAmount.put("56", "90");
			createMultiApplyWithMinCartAmount.put("57", "false");
			createMultiApplyWithMinCartAmount.put("58", "0");
			createMultiApplyWithMinCartAmount.put("59", "-1");
			createMultiApplyWithMinCartAmount.put("60", "1st transaction desc.");
			createMultiApplyWithMinCartAmount.put("61", "1st transaction");
			createMultiApplyWithMinCartAmount.put("62", "\"com.swiggy.poc.test1\",\"test2\",\"test3\"");
			createMultiApplyWithMinCartAmount.put("63", "w2fh4c46d0bszhl89voy");
			createMultiApplyWithMinCartAmount.put("64", "false");
			createMultiApplyWithMinCartAmount.put("65",
					"\"Juspay\",\"PayTM\",\"Cash\",\"Juspay-NB\",\"PayTM-SSO\",\"Mobikwik\""
							+ ",\"Mobikwik-SSO\",\"Freecharge-SSO\",\"Freecharge\",\"Sodexo\",\"PayLater_Lazypay\",\"PhonePe\"");
			createMultiApplyWithMinCartAmount.put("66", "ANDROID");
			createMultiApplyWithMinCartAmount.put("67", "2");
			createMultiApplyWithMinCartAmount.put("68", "false");
			createMultiApplyWithMinCartAmount.put("69", "false");
			createMultiApplyWithMinCartAmount.put("70", "false");
			
			// vendorType (21) - 0
						applyCouponMinCartAmount.put("0", "ALPHATEST");
						applyCouponMinCartAmount.put("1", "2");
						applyCouponMinCartAmount.put("2", "1");
						applyCouponMinCartAmount.put("3", "0.0");
						applyCouponMinCartAmount.put("4", "0.0");
						applyCouponMinCartAmount.put("5", "null");
						applyCouponMinCartAmount.put("6", "5282696");
						applyCouponMinCartAmount.put("7", "1");
						applyCouponMinCartAmount.put("8", "28157");
						applyCouponMinCartAmount.put("9", "3249402");
						applyCouponMinCartAmount.put("10", "0.0");
						applyCouponMinCartAmount.put("11", "false");
						applyCouponMinCartAmount.put("12", "1");
						applyCouponMinCartAmount.put("13", "99");
						applyCouponMinCartAmount.put("14", "5282696_0_variants_addons");
						applyCouponMinCartAmount.put("15", "1");
						applyCouponMinCartAmount.put("16", "150");
						applyCouponMinCartAmount.put("17", "Cash");
						applyCouponMinCartAmount.put("18", "false");
						applyCouponMinCartAmount.put("19", "false");
						applyCouponMinCartAmount.put("20", "false");
						applyCouponMinCartAmount.put("21", "0");
						applyCouponMinCartAmount.put("22", "false");
						applyCouponMinCartAmount.put("23", "245");
						applyCouponMinCartAmount.put("24", "ANDROID");
						applyCouponMinCartAmount.put("25", "null");
						applyCouponMinCartAmount.put("26", "null");
			
			return new Object[][] { { createMultiApplyWithMinCartAmount,  duplicateType} };

		}}
	
	@DataProvider(name = "EDVOData")
	public static Object[][] EDVOData() {
		{
			ArrayList <String> listOfRestIds = new ArrayList();
			ArrayList <String> operationTypeOfCamp = new ArrayList<String>();
			ArrayList <String> numberOfMealsInARest= new ArrayList();
			ArrayList <String> allMealIds= new ArrayList();
			ArrayList <String> allMealsOperationMeta = new ArrayList<String>();
			ArrayList <String> allMealRewards = new ArrayList<String>();
			ArrayList <Integer> numberOfGroupsCountInEachMeal= new ArrayList<Integer>();
			ArrayList <String> allGroupIdsOfMeals = new ArrayList<String>();
			ArrayList <String>  allGroupsRewards= new ArrayList<String>();
	 		
			//listOfAllRestaurantIds
			listOfRestIds.add(0, "23789");
			
			// list of operationType of restaurants in campaign
			operationTypeOfCamp.add(0, "\"MEAL\"");
			
			//list of number of meals in restaurants
			numberOfMealsInARest.add(0,"2");
			
			// list of All MealIds
			allMealIds.add(0, "1799");
			allMealIds.add(1, "1777");
			
			// list of all Meals OperationMeta
			allMealsOperationMeta.add(0, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer"," buy 2 at price of one (max of 2) offer").toString());
			allMealsOperationMeta.add(1, new CreateEDVOOperationMeta("buy 2 at price of one (max of 2) offer"," buy 2 at price of one (max of 2) offer").toString());
			 
			// list of all Meal rewards
			allMealRewards.add(0, new CreateReward("","Percentage","10","1").toString());
			allMealRewards.add(1, new CreateReward("","Percentage","10","1").toString());
			
			// list of number of Groups in each meal
			numberOfGroupsCountInEachMeal.add(0,2 );
			numberOfGroupsCountInEachMeal.add(1,1 );
			
			//list of groupIds in Meal
			allGroupIdsOfMeals.add(0,"3599");
			allGroupIdsOfMeals.add(1,"3555");
			allGroupIdsOfMeals.add(1,"3556");
			
			//list of all GroupsReward
			allGroupsRewards.add(0, new CreateReward("","Percentage","20","1").toString());
			
			allGroupsRewards.add(1, new CreateReward("","Percentage","30","1").toString());
			allGroupsRewards.add(2, new CreateReward("","Percentage","15","1").toString());
			
			return new Object[][] { { listOfRestIds,  operationTypeOfCamp,numberOfMealsInARest, allMealIds,allMealsOperationMeta,allMealRewards,
				
				numberOfGroupsCountInEachMeal,allGroupIdsOfMeals , allGroupsRewards} };

		}}
	
	
	
}
