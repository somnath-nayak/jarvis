
package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.helper.JarvisHelper;
import com.swiggy.api.erp.delivery.tests.Jarvis;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.EditTimeConstants;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartBuilder;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartItem;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartItemBuilder;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderCancelCheck.OrderCancelCheckRequest;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.revertCancellationFee.RevertCancellationFeeRequest;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.checkout.helper.sanityHelper.EditOrderHelper;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.OrderEditPOJO;
import com.swiggy.api.sf.checkout.tests.EditOrderV2;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import static java.lang.Math.toIntExact;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.math.BigInteger;
import java.text.Format;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class CheckoutHelper {

	String sessionId = null;
	String token = null;
	String tid = null;
	Long serviceableAddress = null;
	Initialize gameofthrones = Initializer.getInitializer();
	//Initialize gameofthrones = new Initialize();
	SnDHelper sndHelper = new SnDHelper();
	//CMSHelper cmsHelper = new CMSHelper();
	SANDHelper sandHelper = new SANDHelper();
	JsonHelper jsonHelper = new JsonHelper();
	EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
    EditOrderHelper Orderhelp = new EditOrderHelper();
    Processor processor = null;


    public Processor InvExpire(String item_schedule_id, String inventory, String request_id, String order_id) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "inventoryexpirev1", gameofthrones);
		String[] payloadparams = new String[4];
		payloadparams[0] = item_schedule_id;
		payloadparams[1] = inventory;
		payloadparams[2] = request_id;
		payloadparams[3] = order_id;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		Assert.assertEquals(200, processor.ResponseValidator.GetResponseCode());
		return processor;
	}

	public Processor ConfirmLock(String item_schedule_id, String inventory, String request_id, String order_id) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "confirmInventoryLockv1", gameofthrones);
		String[] payloadparams = new String[4];
		payloadparams[0] = item_schedule_id;
		payloadparams[1] = inventory;
		payloadparams[2] = request_id;
		payloadparams[3] = order_id;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		Assert.assertEquals(200, processor.ResponseValidator.GetResponseCode());
		return processor;
	}

	public Processor RemoveLock(String item_schedule_id, String inventory, String request_id, String order_id) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "removeInventoryLockv1", gameofthrones);
		String[] payloadparams = new String[4];
		payloadparams[0] = item_schedule_id;
		payloadparams[1] = inventory;
		payloadparams[2] = request_id;
		payloadparams[3] = order_id;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		Assert.assertEquals(200, processor.ResponseValidator.GetResponseCode());
		return processor;
	}

	public Processor cancelInventory(String item_schedule_id, String inventory, String request_id, String order_id) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "cancelInventoryv1", gameofthrones);
		String[] payloadparams = new String[4];
		payloadparams[0] = item_schedule_id;
		payloadparams[1] = inventory;
		payloadparams[2] = request_id;
		payloadparams[3] = order_id;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		Assert.assertEquals(200, processor.ResponseValidator.GetResponseCode());
		return processor;
	}

	/*
	 * public String[] restaurantMenu() { String response=
	 * sndHelper.Menu().ResponseValidator.GetBodyAsText(); String
	 * MenuItems=JsonPath.read(response,"$.data.categories..menu[0]..id").toString()
	 * .replace("[","").replace("]",""); //System.out.println(MenuItems); String[]
	 * menuLists= MenuItems.split(","); return menuLists;
	 *
	 * }
	 */






    /*public String[] restaurantMenu()
    {
        String response= sndHelper.Menu().ResponseValidator.GetBodyAsText();
		String MenuItems=JsonPath.read(response,"$.data.categories..menu[0]..id").toString().replace("[","").replace("]","");
		//System.out.println(MenuItems);
		String[] menuLists= MenuItems.split(",");
		return menuLists;

    }*/

	public Processor invokeCreateCartAPI(String tid, String token, String itemId, String quantity, String restId) {
		GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2", gameofthrones);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token",token);
		return new Processor(service, headers, new String[]{itemId, quantity, restId});
	}

	public Processor invokegetAllAddressAPI() {
		GameOfThronesService service = new GameOfThronesService("checkout", "getalladdressv1", gameofthrones);
		HashMap<String, String> headers = new HashMap<String, String>();
		// String[] token= Token();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token", token);
		return new Processor(service, headers);
	}

	public Processor invokegetAllAddressAPI(String tid, String token) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getalladdressv1", gameofthrones);
		HashMap<String, String> headers = new HashMap<String, String>();
		// String[] token= Token();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token", token);
		return new Processor(service, headers);
	}


	public static Processor addNewAddress(HashMap<String, String> adressData, String tid, String token) {
		Initialize init = new Initialize();
		GameOfThronesService service = new GameOfThronesService("checkout", "newaddress", init);
		HashMap<String, String> headers = new HashMap<String, String>();
		// String[] token= Token();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token", token);
		String[] payload = new String[]{adressData.get("name"), adressData.get("mobile"), adressData.get("address"), adressData.get("landmark"), adressData.get("area"), adressData.get("lat"), adressData.get("lng"), adressData.get("flat_no"), adressData.get("city"), adressData.get("annotation")};
		return new Processor(service, headers, payload);
	}

	public Long getServiceableAddressId(JsonNode node) {

		int size = node.get("data").get("addresses").size();
		for (int i = 0; i < size; i++) {
			System.out.println(node.get("data").get("addresses").get(i).get("delivery_valid").asInt());
			if (node.get("data").get("addresses").get(i).get("delivery_valid").asInt() == 1) {
				Long a = node.get("data").get("addresses").get(i).get("id").asLong();
				System.out.println("Found the Serviceable Address ,addressId :" + a);
				return a;
			}
		}
		return null;
	}

	public Processor invokePlaceOrderAPI(Long serviceableAddressId) {
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token", token);
		return new Processor(service, headers,
				new String[]{String.valueOf(serviceableAddressId), "Cash", "TestUser"});
	}

	public Processor invokePlaceOrderAPI(Long serviceableAddressId, String tid, String token) {
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token", token);
		return new Processor(service, headers,
				new String[]{String.valueOf(serviceableAddressId), "Cash", "TestUser"});
	}

	public Processor placeOrder(String mobile, String password, String itemId, String quantity, String restId)
			throws Exception {

		Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
		tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		DeleteCart(tid, token);
		Processor cartResponse = invokeCreateCartAPI(tid, token, itemId, quantity, restId);
		if (cartResponse.ResponseValidator.GetNodeValueAsInt("statusCode") == 0) {
			Processor getAllAddressResp = invokegetAllAddressAPI();
			serviceableAddress = getServiceableAddressId(
					CommonAPIHelper.convertStringtoJSON(getAllAddressResp.ResponseValidator.GetBodyAsText()));
			return invokePlaceOrderAPI(serviceableAddress);

		}
		return null;
	}

	public Processor placeOrderWithTidToken(String itemId, String quantity, String restId, String tid, String token)
			throws Exception {

		Processor cartResponse = invokeCreateCartAPI(tid, token, itemId, quantity, restId);
		if (cartResponse.ResponseValidator.GetNodeValueAsInt("statusCode") == 0) {
			Processor getAllAddressResp = invokegetAllAddressAPI(tid, token);
			serviceableAddress = getServiceableAddressId(
					CommonAPIHelper.convertStringtoJSON(getAllAddressResp.ResponseValidator.GetBodyAsText()));
			return invokePlaceOrderAPI(serviceableAddress, tid, token);

		}
		return null;
	}

	public Processor CreateCart(int n, String tid, String token, String restId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2nitems", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = get(n, restId).toString();
		payloadparams[1] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor CreateCartCheckTotals(int n, String restId) {
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "checktotalv2", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = get(n, restId).toString();
		payloadparams[1] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor CreateCartAddons(int n, String tid, String token, String restId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2nitems", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = getAddons(n, restId).toString();
		payloadparams[1] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor CreateCartAddonsCheckTotal(int n, String restId) {
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "checktotalv2", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = getAddons(n, restId).toString();
		payloadparams[1] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}



	public String RestaurantId(String lat, String lng, String tid, String token) {
		String response = sandHelper.getListingV3(lat, lng, tid, token).ResponseValidator.GetBodyAsText();
		String restId = JsonPath.read(response, "$.data..rest_list..restaurants..id").toString().replace("[", "")
				.replace("]", "");
		String[] restList = restId.split(",");
		int rnd = new Random().nextInt(restList.length);
		return restList[rnd];
	}

	public JSONArray get(int n, String restId) {
		List<String> l = pickNRandom(restId);
		JSONArray obj = new JSONArray();
		try {
			for (int i = 1; i < n + 1; i++) {
				// 1st object
				JSONObject list1 = new JSONObject();
				list1.put("menu_item_id", l.get(i - 1));
				list1.put("quantity", 1);
				obj.put(list1);
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return obj;
	}

	public List<Map<String, Object>> Addons(String restId) {
		DBHelper dbHelper = new DBHelper();
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CheckoutConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CheckoutConstants.addonQuery + restId);
		return list;
	}

	public JSONArray getAddons(int n, String restIds) {
		// List<String> l= pickNRandom(restId);
		List<Map<String, Object>> list = Addons(restIds);
		JSONArray obj = new JSONArray();
		try {
			if (!list.isEmpty()) {
				for (int i = 1; i < n + 1; i++) {
					// 1st object
					JSONObject list1 = new JSONObject();
					String p = String.valueOf(Double.parseDouble(list.get(i - 1).get("price").toString()) * 100)
							.replace(".", "");
					String p1 = p.substring(0, p.length() - 1);
					JSONObject jo = new JSONObject();
					jo.put("choice_id", list.get(i - 1).get("choiceid").toString());
					jo.put("group_id", list.get(i - 1).get("groupid").toString());
					jo.put("name", list.get(i - 1).get("name").toString());
					jo.put("price", Integer.parseInt(p1));
					JSONArray ja = new JSONArray();
					ja.put(jo);
					list1.put("addons", ja);
					list1.put("menu_item_id", list.get(i - 1).get("item_id").toString());
					list1.put("quantity", 1);
					obj.put(list1);
				}
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return obj;
	}

	public JSONArray getVariants(int n, String restId) {
		// List<String> l= pickNRandom(restId);
		DBHelper dbHelper = new DBHelper();
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
		List<Map<String, Object>> list = sqlTemplate.queryForList(
				"select ram.rest_id, ag.`item_id` menuid, ag.id groupid, ad.id choiceid, ad.name, ad.price from `restaurant_addon_map` ram inner join addons ad on ad.id=ram.`addon_id` inner join addon_groups ag on ag.`id`= ad.addon_group_id where ram.rest_id="
						+ restId);
		JSONArray obj = new JSONArray();
		//JSONArray obj1 = new JSONArray();
		try {
			for (int i = 1; i < n + 1; i++) {
				// 1st object
				JSONObject list1 = new JSONObject();
				String p = String.valueOf(Double.parseDouble(list.get(i - 1).get("price").toString()) * 100)
						.replace(".", "");
				String p1 = p.substring(0, p.length() - 1);
				JSONObject jo = new JSONObject();
				jo.put("choice_id", list.get(i - 1).get("choiceid").toString());
				jo.put("group_id", list.get(i - 1).get("groupid").toString());
				jo.put("name", list.get(i - 1).get("name").toString());
				jo.put("price", Integer.parseInt(p1));
				JSONArray ja = new JSONArray();
				ja.put(jo);
				JSONObject variants = new JSONObject();
				variants.put("variation_id", "");
				variants.put("group_id", "");
				variants.put("name", "");
				variants.put("price", "");
				list1.put("addons", ja);
				list1.put("variants", variants);

				list1.put("menu_item_id", list.get(i - 1).get("menuid").toString());
				list1.put("quantity", i + 1);
				obj.put(list1);
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return obj;

	}

	public JSONArray getNegative(int n, String restId, String quantity) {
		List<String> l = pickNRandom(restId);
		List<String> neg = NegativeQuantity();
		JSONArray obj = new JSONArray();
		try {
			for (int i = 1; i < n + 1; i++) {
				// 1st object
				JSONObject list1 = new JSONObject();
				list1.put("menu_item_id", l.get(i));
				list1.put("quantity", Double.parseDouble(quantity));
				obj.put(list1);
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return obj;
	}

	public Processor CreateCartAddonsNegative(int n, String tid, String token, String restId, String quantity) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2nitems", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = getNegative(n, restId, quantity).toString();
		payloadparams[1] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public List<String> pickNRandom(String restID) {
		String rest = sandHelper.getRestaurantMenu(restID).ResponseValidator.GetBodyAsText();
		String MenuItems = JsonPath.read(rest, "$.data.menu..id").toString().replace("[", "")
				.replace("]", "").replace("\"", "");
		//System.out.println(MenuItems);
		String[] menuLists = MenuItems.split(",");
		System.out.println("menu length--->" + menuLists.length);
		List<String> givenList = Arrays.asList(menuLists);
		List<String> copy = new LinkedList<String>(givenList);
		Collections.shuffle(copy);
		return copy;
	}

	public Processor GetCart(String tid, String Token) {
		HashMap<String, String> requestheaders = requestHeader(tid, Token);
		GameOfThronesService service = new GameOfThronesService("checkout", "getcart", gameofthrones);
		HashMap<String, String> headers = new HashMap<String, String>();
		Processor processor = new Processor(service, requestheaders, null, null);
		return processor;
	}

	public Processor DeleteCart(String tid, String Token) {
		HashMap<String, String> requestheaders = requestHeader(tid, Token);
		GameOfThronesService service = new GameOfThronesService("checkout", "deletecart", gameofthrones);
		HashMap<String, String> headers = new HashMap<String, String>();
		Processor processor = new Processor(service, requestheaders, null, null);
		return processor;
	}

	public Processor ApplyCouponOnCart(String tid, String token, String couponCode) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "applycoupon", gameofthrones);
		String[] payloadparams = {couponCode};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor DeleteCouponOnCart(String tid, String token, String couponCode) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "deletecoupon", gameofthrones);
		String[] payloadparams = {couponCode};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public void validateDataFromResponse(String response, String restId, String menuItem, int n) {
		String restGetId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "")
				.replace("]", "");
		String menuItemGet = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "")
				.replace("]", "");
		Assert.assertEquals(restId, restGetId);
		Assert.assertEquals(menuItem, menuItemGet);

		String basePrice = JsonPath.read(response, "$.data.cart_menu_items..base_price").toString().replace("[", "")
				.replace("]", "");
		String[] basePriceArray = basePrice.split(",");

		String Quantity = JsonPath.read(response, "$.data.cart_menu_items..quantity").toString().replace("[", "")
				.replace("]", "");
		String[] QuantityArray = Quantity.split(",");

		String AddonsPrice = JsonPath.read(response, "$.data.cart_menu_items..addons_price").toString().replace("[", "")
				.replace("]", "");
		String[] AddonsPriceArray = AddonsPrice.split(",");

		String VariantsPrice = JsonPath.read(response, "$.data.cart_menu_items..variants_price").toString()
				.replace("[", "").replace("]", "");
		String[] VariantsPriceArray = VariantsPrice.split(",");

		for (int i = 0; i < VariantsPriceArray.length; i++) {
			if (VariantsPriceArray[i] == null)
				VariantsPriceArray[i] = "0.0";
		}

		for (int i = 0; i < AddonsPriceArray.length; i++) {
			if (AddonsPriceArray[i] == null)
				AddonsPriceArray[i] = "0.0";
		}

		String subTotalResponse = JsonPath.read(response, "$.data.cart_menu_items..subtotal").toString()
				.replace("[", "").replace("]", "");
		String[] subTotalResponseArray = subTotalResponse.split(",");

		String packingPrice = JsonPath.read(response, "$.data.cart_menu_items..packing_charge").toString()
				.replace("[", "").replace("]", "");
		String[] packingPriceArray = packingPrice.split(",");

		String cartId = JsonPath.read(response, "$.data.cartId").toString().replace("[", "").replace("]", "");
		int cartID1 = Integer.valueOf(cartId);
		System.out.println("CArt Id---->" + cartID1);
		Assert.assertNotNull(cartID1);

		String totalItemLevel = JsonPath.read(response, "$.data.cart_menu_items..total").toString().replace("[", "")
				.replace("]", "");
		String[] TotalItemLevel = totalItemLevel.split(",");
		Double[] TotalItemLevel1 = new Double[TotalItemLevel.length];

		String totalitem = JsonPath.read(response, "$.data.item_total").toString().replace("[", "").replace("]", "");
		Double TotalItem = Double.parseDouble(totalitem);

		String serviceCharges = JsonPath.read(response, "$.data.cart_menu_items..item_taxes.service_charges").toString()
				.replace("[", "").replace("]", "");
		String[] serviceCharges1 = serviceCharges.split(",");
		Double[] ServiceCharges1 = new Double[serviceCharges1.length];

		Double TTotalItemLevel1 = 0.0;

		for (int i = 0; i < n; i++) {
			TTotalItemLevel1 = TTotalItemLevel1 + Double.parseDouble(TotalItemLevel[i]) / 100;
		}
		System.out.println("Item totAL" + TTotalItemLevel1);

		Assert.assertEquals(TotalItem, TTotalItemLevel1);

		Double[] basePrice1 = new Double[basePriceArray.length];
		for (int i = 0; i < n; i++) {
			basePrice1[i] = Double.parseDouble(basePriceArray[i]);
		}

		Double[] QuantityArray1 = new Double[QuantityArray.length];
		for (int i = 0; i < n; i++) {
			QuantityArray1[i] = Double.parseDouble(QuantityArray[i]);
		}

		Double[] packingPriceArray1 = new Double[packingPriceArray.length];
		for (int i = 0; i < n; i++) {
			packingPriceArray1[i] = Double.parseDouble(packingPriceArray[i]);
		}

		/*
		 * Double[] AddOnsArray1= new Double[AddonsPriceArray.length]; for(int i = 0; i
		 * < n; i++) { AddOnsArray1[i] =
		 * Double.parseDouble(AddonsPriceArray[i].replace(null,""));
		 * System.out.println(AddOnsArray1[i]); }
		 *
		 * Double[] VariantsPriceArray1= new Double[VariantsPriceArray.length]; for(int
		 * i = 0; i < n; i++) { VariantsPriceArray1[i] =
		 * Double.parseDouble(VariantsPriceArray[i].replace(null,""));
		 * System.out.println("Variants"+VariantsPriceArray1[i]); }
		 */

		Double BasePrice = 0.0;
		for (int i = 0; i < n; i++) {
			BasePrice = BasePrice + basePrice1[i] * QuantityArray1[i];

		}

		System.out.println("0---->" + BasePrice);

		// Assert.assertEquals(BasePrice, TTotalItemLevel1);

		String deliveryCharges = JsonPath.read(response, "$.data.delivery_charges").toString().replace("[", "")
				.replace("]", "");
		Double deliveryCharges1 = Double.parseDouble(deliveryCharges);
		String restaurantPackingCharges = JsonPath.read(response, "$.data.restaurant_packing_charges").toString()
				.replace("[", "").replace("]", "");
		Double restaurantPackingCharges1 = Double.parseDouble(restaurantPackingCharges);

		Double BasePrice1 = BasePrice / 100;

		Double BasePrice2 = 0.0;

		BasePrice2 = BasePrice1 + deliveryCharges1 + restaurantPackingCharges1;
		System.out.println("uiiii" + BasePrice2);
		String billTotal1 = JsonPath.read(response, "$.data.bill_total").toString().replace("[", "").replace("]", "");
		Double billTotal2 = Double.parseDouble(billTotal1);
		System.out.println(billTotal2);

		String cancellationCharge = JsonPath.read(response, "$.data.cancellation_fee").toString().replace("[", "")
				.replace("]", "");
		Double cancellationCharge1 = Double.parseDouble(cancellationCharge);
		String subTotall = JsonPath.read(response, "$.data.cart_subtotal").toString().replace("[", "").replace("]", "");
		Double subTotall1 = Double.parseDouble(subTotall);
		Assert.assertEquals(BasePrice1, subTotall1);

		String swiggy_money = JsonPath.read(response, "$.data.user_swiggy_money").toString().replace("[", "")
				.replace("]", "");
		Double swiggy_money1 = Double.parseDouble(swiggy_money);

		Double TotalItemWithoutSwiggyMoney = subTotall1 + cancellationCharge1 + restaurantPackingCharges1;
		Assert.assertEquals(subTotall1 + restaurantPackingCharges1, TotalItemWithoutSwiggyMoney - cancellationCharge1);

		Double TotalPriceAfterSubSwiggyMoney = TotalItemWithoutSwiggyMoney - swiggy_money1;

		String FinalTotal = JsonPath.read(response, "$.data.cart_total").toString().replace("[", "").replace("]", "");
		Double FinalTotal1 = Double.parseDouble(FinalTotal);

		Assert.assertEquals(FinalTotal1, TotalPriceAfterSubSwiggyMoney);
		String slaRangeMin = JsonPath.read(response, "$.data.sla_range_min").toString().replace("[", "").replace("]",
				"");
		Assert.assertNotNull(slaRangeMin);
		String slaRangeMax = JsonPath.read(response, "$.data.sla_range_min").toString().replace("[", "").replace("]",
				"");
		Assert.assertNotNull(slaRangeMax);
		String slaTime = JsonPath.read(response, "$.data.sla_time").toString().replace("[", "").replace("]", "");
		Assert.assertNotNull(slaTime);
	}

	public void validateDataFromResponseNew(String response, String restId, String menuItem, int n) {
		String restGetId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "")
				.replace("]", "");
		String menuItemGet = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "")
				.replace("]", "");
		Assert.assertEquals(restId, restGetId);
		Assert.assertEquals(menuItem, menuItemGet);
		String basePrice = JsonPath.read(response, "$.data.cart_menu_items..base_price").toString().replace("[", "")
				.replace("]", "");
		String[] basePriceArray = basePrice.split(",");
		String Quantity = JsonPath.read(response, "$.data.cart_menu_items..quantity").toString().replace("[", "")
				.replace("]", "");
		String[] QuantityArray = Quantity.split(",");
		String AddonsPrice = JsonPath.read(response, "$.data.cart_menu_items..addons_price").toString().replace("[", "")
				.replace("]", "");
		String[] AddonsPriceArray = AddonsPrice.split(",");
		String VariantsPrice = JsonPath.read(response, "$.data.cart_menu_items..variants_price").toString()
				.replace("[", "").replace("]", "");
		String[] VariantsPriceArray = VariantsPrice.split(",");
		for (int i = 0; i < VariantsPriceArray.length; i++) {
			if (VariantsPriceArray[i].equalsIgnoreCase("null"))
				VariantsPriceArray[i] = "0.0";

		}
		for (int i = 0; i < AddonsPriceArray.length; i++) {
			if (VariantsPriceArray[i].equalsIgnoreCase("null"))
				AddonsPriceArray[i] = "0.0";
		}
		String subTotalResponse = JsonPath.read(response, "$.data.cart_menu_items..subtotal").toString()
				.replace("[", "").replace("]", "");
		String[] subTotalResponseArray = subTotalResponse.split(",");
		String packingPrice = JsonPath.read(response, "$.data.cart_menu_items..packing_charge").toString()
				.replace("[", "").replace("]", "");
		String[] packingPriceArray = packingPrice.split(",");
		String tradeDiscount = JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount").toString()
				.replace("[", "").replace("]", "");
		String[] tradeDiscountArray = tradeDiscount.split(",");
		String cartId = JsonPath.read(response, "$.data.cartId").toString().replace("[", "").replace("]", "");
		int cartID1 = Integer.valueOf(cartId);
		System.out.println("CArt Id---->" + cartID1);
		Assert.assertNotNull(cartID1);
		String CMSTotalResponse = JsonPath.read(response, "$.data.cart_menu_items..total").toString().replace("[", "")
				.replace("]", "");
		String[] CMSTotalResponseArray = CMSTotalResponse.split(",");
		Double TOTALCMS = 0.0;
		int length = CMSTotalResponseArray.length;
		if (length != 0) {
			Double[] CMSTotalResponseArray1 = new Double[CMSTotalResponseArray.length];
			for (int i = 0; i < length; i++) {
				TOTALCMS = TOTALCMS + Double.parseDouble(CMSTotalResponseArray[i]) / 100
						- (Double.parseDouble(packingPriceArray[i]) / 100 + Double.parseDouble(tradeDiscountArray[i]) / 100);
			}
			System.out.println("TOTALCMS:->" + TOTALCMS);
		}
		String TotalResp = JsonPath.read(response, "$.data.item_total").toString().replace("[", "").replace("]", "");
		Double TotalResp1 = Double.parseDouble(TotalResp);
		System.out.println("item_total:->" + TotalResp1);
		Assert.assertEquals(TOTALCMS, TotalResp1);
	}

	public void validateDataForListing(String response, String restId, String menuItem, int n) {
		String restGetId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "")
				.replace("]", "");
		String menuItemGet = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "")
				.replace("]", "");
		Assert.assertEquals(restId, restGetId);
		Assert.assertEquals(menuItem, menuItemGet);
		String basePrice = JsonPath.read(response, "$.data.cart_menu_items..base_price").toString().replace("[", "")
				.replace("]", "");
		String[] basePriceArray = basePrice.split(",");
		String Quantity = JsonPath.read(response, "$.data.cart_menu_items..quantity").toString().replace("[", "")
				.replace("]", "");
		String[] QuantityArray = Quantity.split(",");
		String AddonsPrice = JsonPath.read(response, "$.data.cart_menu_items..addons_price").toString().replace("[", "")
				.replace("]", "");
		String[] AddonsPriceArray = AddonsPrice.split(",");
		String VariantsPrice = JsonPath.read(response, "$.data.cart_menu_items..variants_price").toString()
				.replace("[", "").replace("]", "");
		String[] VariantsPriceArray = VariantsPrice.split(",");
		for (int i = 0; i < VariantsPriceArray.length; i++) {
			if (VariantsPriceArray[i].equalsIgnoreCase("null"))
				VariantsPriceArray[i] = "0.0";

		}
		for (int i = 0; i < AddonsPriceArray.length; i++) {
			if (VariantsPriceArray[i].equalsIgnoreCase("null"))
				AddonsPriceArray[i] = "0.0";
		}
		String subTotalResponse = JsonPath.read(response, "$.data.cart_menu_items..subtotal").toString()
				.replace("[", "").replace("]", "");
		String[] subTotalResponseArray = subTotalResponse.split(",");
		String packingPrice = JsonPath.read(response, "$.data.cart_menu_items..packing_charge").toString()
				.replace("[", "").replace("]", "");
		String[] packingPriceArray = packingPrice.split(",");
		String cartId = JsonPath.read(response, "$.data.cartId").toString().replace("[", "").replace("]", "");
		int cartID1 = Integer.valueOf(cartId);
		System.out.println("CArt Id---->" + cartID1);
		Assert.assertNotNull(cartID1);
		String CMSTotalResponse = JsonPath.read(response, "$.data.cart_menu_items..total").toString().replace("[", "")
				.replace("]", "");
		String[] CMSTotalResponseArray = CMSTotalResponse.split(",");
		Double[] CMSTotalResponseArray1 = new Double[CMSTotalResponseArray.length];
		Double TOTALCMS = 0.0;
		for (int i = 0; i < n; i++) {
			TOTALCMS = TOTALCMS + Double.parseDouble(CMSTotalResponseArray[i]) / 100
					- Double.parseDouble(packingPriceArray[i]) / 100;
		}
		System.out.println(TOTALCMS);
		String TotalResp = JsonPath.read(response, "$.data.item_total").toString().replace("[", "").replace("]", "");
		Double TotalResp1 = Double.parseDouble(TotalResp);
		System.out.println(TotalResp1);
		Assert.assertEquals(TOTALCMS, TotalResp1);
	}


	public List<String> NegativeQuantity() {
		ArrayList<String> values = new ArrayList<>(Arrays.asList("-1", "", "1.25", "0"));
		return values;
	}

	public String[] Token() {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		String password1 = CheckoutConstants.password;
		String mobile1 = CheckoutConstants.mobile;
		GameOfThronesService service = new GameOfThronesService("checkout", "applogin", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = password1;
		payloadparams[1] = mobile1;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		Assert.assertEquals(CheckoutConstants.success, processor.ResponseValidator.GetResponseCode());
		String resp = processor.ResponseValidator.GetBodyAsText();
		String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
		String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
		String[] tokenData = {tid, token};
		return tokenData;
	}

	public String restId(String lat, String lng) {
		String[] p = new String[]{lat, lng};
		String resp = sandHelper.aggregator(p).ResponseValidator.GetBodyAsText();
		String restIds = JsonPath.read(resp, "$.data.restaurants[*].id").toString().replace("[", "").replace("]", "")
				.replace("\"", "");
		// System.out.println(restIds);
		List<String> list = Arrays.asList(restIds.split(","));
		List<String> openRest = new ArrayList<>();
		String open = JsonPath.read(resp, "$.data.restaurants[*].availability.opened").toString().replace("[", "")
				.replace("]", "");
		List<String> openList = Arrays.asList(open.split(","));
		for (int i = 0; i < openList.size(); i++) {
			if (openList.get(i).equalsIgnoreCase("true")) {
				openRest.add(list.get(i));
			}
		}
		//int random = new Random().nextInt(openRest.size());
		//return openRest.get(random);
		int i = 0;
		while (i <= openRest.size()) {
			if (menuItemsList(openRest.get(i)) <= 5) {
				openRest.remove(i);
				i++;
			} else {
				break;
			}
		}
		//return openRest.get(new Random().nextInt(openRest.size()));
		return openRest.get(0);
	}


	public String closedRestId(String lat, String lng) {
		String[] p = new String[]{lat, lng};
		String resp = sandHelper.aggregator(p).ResponseValidator.GetBodyAsText();
		String restIds = JsonPath.read(resp, "$.data.restaurants[*].id").toString().replace("[", "").replace("]", "")
				.replace("\"", "");
		// System.out.println(restIds);
		List<String> list = Arrays.asList(restIds.split(","));
		List<String> openRest = new ArrayList<>();
		String open = JsonPath.read(resp, "$.data.restaurants[*].availability.opened").toString().replace("[", "")
				.replace("]", "");
		List<String> openList = Arrays.asList(open.split(","));
		for (int i = 0; i < openList.size(); i++) {
			if (openList.get(i).equalsIgnoreCase("false")) {
				openRest.add(list.get(i));
			}
		}
		int random = new Random().nextInt(openRest.size());
		//return openRest.get(random);
		for (int i = 0; i < openRest.size(); i++) {
			if (menuItemsList(openRest.get(random)) <= 1) {
				openRest.remove(random);
			} else {
				break;
			}
		}
		return openRest.get(new Random().nextInt(openRest.size()));
	}

	public int menuItemsList(String restID) {
		String rest = sandHelper.getRestaurantMenu(restID).ResponseValidator.GetBodyAsText();
		String MenuItems = JsonPath.read(rest, "$.data.menu..id").toString().replace("[", "")
				.replace("]", "");
		String[] menuLists = MenuItems.split(",");
		System.out.println("menu length--->" + menuLists.length);
		return menuLists.length;
	}


	public Processor OrderCancel(String tid, String token, String auth, String reason, String feeApplicability,
								 String cancellation_fee, String cancellation_reason, String orderId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", auth);
		GameOfThronesService service = new GameOfThronesService("checkout", "ordercancelv2", gameofthrones);
		String[] payloadparams = {reason, feeApplicability, cancellation_fee, cancellation_reason};
		String[] urlParams = {orderId};
		Processor processor = new Processor(service, requestheaders, payloadparams, urlParams);
		return processor;
	}
	/* Order cancel method for Delivery */

	public Processor OrderCancel(String mobile, String password, String orderId) {
		HashMap<String, String> token = TokenData(mobile, password);
		HashMap<String, String> requestheaders = requestHeader(token.get("Tid"), token.get("Token"));
		requestheaders.put("Authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
		GameOfThronesService service = new GameOfThronesService("checkout", "ordercancel", gameofthrones);
		String reason = "testing";
		String feeApplicability = "true";
		String cancellation_fee = "0";
		String cancellation_reason = "Test Order";
		String[] payloadparams = {reason, feeApplicability, cancellation_fee, cancellation_reason};
		String[] urlParams = {orderId};
		Processor processor = new Processor(service, requestheaders, payloadparams, urlParams);
		return processor;
	}

	/*
	 * public List<String> restautantList() { DBHelper dbHelper = new DBHelper();
	 * SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms"); List<Map<String,
	 * Object>> list = sqlTemplate.
	 * queryForList("select ram.rest_id, ag.`item_id` menuid, ag.id groupid, ad.id choiceid, ad.name, ad.price from `restaurant_addon_map` ram inner join addons ad on ad.id=ram.`addon_id` inner join addon_groups ag on ag.`id`= ad.addon_group_id where ram.rest_id="
	 * +restId);
	 *
	 * return list.get(0).get("price");
	 *
	 *
	 *
	 * }
	 */

	public static HashMap<String, String> requestHeader(String tid, String token) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("tid", tid);
		requestheaders.put("token", token);
		return requestheaders;
	}

	public HashMap<String, String> TokenData(String mobile, String password) {
		String resp = SnDHelper.consumerLogin(mobile, password).ResponseValidator.GetBodyAsText();
		String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
		String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
		String customerId = JsonPath.read(resp, "$.data.customer_id").toString().replace("[", "").replace("]", "");
		String deviceId = JsonPath.read(resp, "$.deviceId").toString().replace("[", "").replace("]", "");
		String sid = JsonPath.read(resp, "$.sid").toString().replace("[", "").replace("]", "");
		String userId = JsonPath.read(resp, "$.data.addresses[0]..user_id").toString().replace("[", "").replace("]", "");
		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put("Tid", tid);
		hashMap.put("Token", token);
		hashMap.put("CustomerId", customerId);
		hashMap.put("userId", userId);
		hashMap.put("DeviceId", deviceId);
		hashMap.put("sid", sid);
		return hashMap;
	}

	public HashMap<String, String> getTokenDataCheckout(String mobile, String password) {
		String resp = loginFromCheckout(mobile, password).ResponseValidator.GetBodyAsText();
		String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
		String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
		String customerId = JsonPath.read(resp, "$.data.customer_id").toString().replace("[", "").replace("]", "");
		String deviceId = JsonPath.read(resp, "$.deviceId").toString().replace("[", "").replace("]", "");
		String sid = JsonPath.read(resp, "$.sid").toString().replace("[", "").replace("]", "");
		String userId = JsonPath.read(resp, "$.data.addresses[0]..user_id").toString().replace("[", "").replace("]", "");
		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put("Tid", tid);
		hashMap.put("Token", token);
		hashMap.put("CustomerId", customerId);
		hashMap.put("userId", userId);
		hashMap.put("DeviceId", deviceId);
		hashMap.put("sid", sid);
		hashMap.put("content-type", "application/json");
		hashMap.put("Authorization", EditTimeConstants.AUTHORIZATION);
		return hashMap;
	}

	public Processor loginFromCheckout(String mobile, String password) {
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "logincheckout", gameofthrones);
		return new Processor(gameOfThronesService, headers, new String[]{mobile, password});
	}

	public Processor OrderRating(String tid, String token, String user_id, String order_id, String deliveryRating,
								 String restaurantRating, String restaurantId, String restaurantName, String orderDate, String cityCode,
								 String areaCode) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "addrating", gameofthrones);
		String[] payloadparams = {user_id, order_id, deliveryRating, restaurantRating, restaurantId, restaurantName,
				orderDate, cityCode, areaCode};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}
	/*  Create cart method */

	public Processor CreateCartAddon(String tid, String token, String payload) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "create", gameofthrones);
		String[] payloadparams = {payload};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor getAllPayments(String tid, String token) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "getAllPayments", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public static List<Map<String, Object>> getAllPayments() {
		DBHelper dbh = new DBHelper();
		String sql = CheckoutConstants.getAllPayments;
		SqlTemplate sqlTemplate = dbh.getMySqlTemplate(CheckoutConstants.checkout);
		List<Map<String, Object>> payments = sqlTemplate.queryForList(sql);
		return payments;
	}

	public static List<Map<String, Object>> getPaymentMeta(String paymentName) {
		DBHelper dbh = new DBHelper();
		String sql = CheckoutConstants.getPaymentMeta + paymentName + "\"";
		SqlTemplate sqlTemplate = dbh.getMySqlTemplate(CheckoutConstants.checkout);
		List<Map<String, Object>> payments = sqlTemplate.queryForList(sql);
		System.out.println("Hello=" + sql);
		System.out.println(payments);
		return payments;
	}


	public static JsonNode convertStringtoJSON(String response)
			throws
			Exception {
		JsonNode obj1 = null;
		try {
			JsonFactory factory = new JsonFactory();
			JsonParser jp1 = factory.createJsonParser(response);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
			mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS,
					true);
			obj1 = mapper.readTree(jp1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj1;
	}

	public Processor CreateCartAddon(String tid, String token, String cart, Integer rest) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2nitems", gameofthrones);
		String[] payloadparams = {cart, String.valueOf(rest)};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor CheckTotalCreate(String cart, Integer rest) {
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "checktotalv2", gameofthrones);
		String[] payloadparams = {cart, String.valueOf(rest)};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor CreateCartPOP(String tid, String token, String cart, Integer rest) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "popcart", gameofthrones);
		String[] payloadparams = {cart, String.valueOf(rest)};
		Processor processor = new Processor(service, requestheaders, payloadparams);

		return processor;
	}

	/*  Create cart method */
	public HashMap<String, String> createLogin(String password, Long mobile) {
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "login", gameofthrones);
		String[] payloadParam = {password, String.valueOf(mobile)};
		Processor processor = new Processor(service, requestheaders, payloadParam);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
		String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put("Tid", tid);
		hashMap.put("Token", token);
		return hashMap;
	}

	public Processor orderPlace(String tid, String token, Integer addressId, String payment, String comments) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
		String[] payloadparams = {String.valueOf(addressId), payment, comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	/**
	 * Sangeetha
	 */

	public Cart getCartPayload(String restaurantId, boolean isAddonRequired, boolean isCartItemRequired) {
		CartBuilder cartBuilder = new CartBuilder()
				.cartItems(getCartItems(restaurantId, 1, isAddonRequired, isCartItemRequired))
				.cartType("CAFE")
				.restaurantId(restaurantId);

		return cartBuilder.build();
	}

	public Processor updateCafeCart(String tid, String token, String restaurantId, boolean isAddonRequired, boolean isCartItemRequired) {
		Cart cart = getCartPayload(restaurantId, isAddonRequired, isCartItemRequired);
		String payload = null;
		this.tid = tid;
		this.token = token;
		payload = Utility.jsonEncode(cart);
		return createCartV2(payload);
	}

	public Processor createCartV2(String payload) {


		HashMap<String, String> requestHeader = requestHeader(tid, token);
		GameOfThronesService gots = new GameOfThronesService("checkout", "createcartv2edvo", gameofthrones);
		return new Processor(gots, requestHeader, new String[]{payload}, null);

	}


	public List<CartItem> getCartItems(String restaurantId, Integer quantity, boolean isAddonRequired, boolean isCartItemRequired) {
		List<CartItem> cartItems = new ArrayList<>();
		if (!isCartItemRequired) {
			return cartItems;
		}
		Processor processor = sandHelper.menuV3(restaurantId);
		List<Integer> inStocks = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.inStock");
		List<Integer> itemIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.id");

		for (int i = 0; i < inStocks.size(); i++) {
			CartItem cartItem = null;
//	            if (processor.ResponseValidator.DoesNodeExists("data.menu.items." + itemIds.get(i).intValue() + ".addons")){
			cartItem = new CartItemBuilder()
					.menuItemId(String.valueOf(itemIds.get(i).intValue()))
					.variants(edvoCartHelper.getVariantsForNormalItem(processor, i, 1, itemIds.get(i).intValue()))
					.quantity(quantity)
					.addons(edvoCartHelper.getAddonsForCartMenuItem(processor, itemIds.get(i), isAddonRequired))
					.build();
			cartItems.add(cartItem);
			break;
//	            }
		}
		return cartItems;
	}


	public Processor cafeOrder(String tid, String token, String payment, String comments) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "cafeorder", gameofthrones);
		String[] payloadparams = {payment, comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}


	public Processor cartCheckTotal(int n, String restId) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "checktotalv2", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = get(n, restId).toString();
		payloadparams[1] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}


	public Processor orderPlace1(String tid, String token, Integer addressId, String payment, String comments) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
		String[] payloadparams = {String.valueOf(addressId), payment, comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
		//String order= processor.ResponseValidator.GetBodyAsText();
		//String orderId=processor.ResponseValidator.GetNodeValue("data.order_id");
		//String orderId= JsonPath.read(order, "$.data.order_id").toString().replace("[","").replace("]","");

	}
	
	/* public Processor orderPlaceWithoutPaymentMode(String tid, String token,Integer addressId, String comments)
	{
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev3", gameofthrones);
		String []payloadparams = {String.valueOf(addressId),comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
		//String order= processor.ResponseValidator.GetBodyAsText();
		//String orderId=processor.ResponseValidator.GetNodeValue("data.order_id");
		//String orderId= JsonPath.read(order, "$.data.order_id").toString().replace("[","").replace("]","");

	} */

	public Processor orderInvalidAddressId(String tid, String token, Integer addressId, String payment, String comments) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
		String[] payloadparams = {String.valueOf(addressId), payment, comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
		//String order= processor.ResponseValidator.GetBodyAsText();
		//String orderId=processor.ResponseValidator.GetNodeValue("data.order_id");
		//String orderId= JsonPath.read(order, "$.data.order_id").toString().replace("[","").replace("]","");

	}

	public Processor orderPlacewithoutAddress(String tid, String token, String payment, String comments) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev2", gameofthrones);
		String[] payloadparams = {payment, comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
		//String order= processor.ResponseValidator.GetBodyAsText();
		//String orderId=processor.ResponseValidator.GetNodeValue("data.order_id");
		//String orderId= JsonPath.read(order, "$.data.order_id").toString().replace("[","").replace("]","");

	}

	public Processor orderPlaceWithoutAuth(Integer addressId, String payment, String comments) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
		String[] payloadparams = {String.valueOf(addressId), payment, comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
		//String order= processor.ResponseValidator.GetBodyAsText();
		//String orderId=processor.ResponseValidator.GetNodeValue("data.order_id");
		//String orderId= JsonPath.read(order, "$.data.order_id").toString().replace("[","").replace("]","");

	}

	public Processor getSingleOrder(String order_id, String tid, String token) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("tid", tid);
		requestheaders.put("token", token);
		GameOfThronesService service = new GameOfThronesService("checkout", "getSingleOrder", gameofthrones);
		Processor processor = new Processor(service, requestheaders, null, new String[]{order_id});
		return processor;
	}


	public Processor getSingleOrderKey(String order_key, String tid, String token) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("tid", tid);
		requestheaders.put("token", token);
		GameOfThronesService service = new GameOfThronesService("checkout", "getSingleOrderKey", gameofthrones);
		Processor processor = new Processor(service, requestheaders, null, new String[]{order_key});
		return processor;
	}

	public Processor getOrder(String tid, String token) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getOrder1", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("tid", tid);
		requestheaders.put("token", token);

		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public Processor getLastOrder(String tid, String token) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getLastOrder", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("tid", tid);
		requestheaders.put("token", token);

		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public Processor cartMinimalV2(int n, String tid, String token, String restId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "minimalcartv2", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = get(n, restId).toString();
		payloadparams[1] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}


	public Processor partialRefund(String order_id, String amount, String source, String reason, String requester) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "refundpartial", gameofthrones);
		String[] payloadparams = new String[5];
		payloadparams[0] = order_id;
		payloadparams[1] = amount;
		payloadparams[2] = source;
		payloadparams[3] = reason;
		payloadparams[4] = requester;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}


	public Processor fullRefund(String tid, String token, String order_id, String source, String reason, String requester) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "refundinitiatev1", gameofthrones);
		String[] payloadparams = new String[4];
		payloadparams[0] = order_id;
		payloadparams[1] = source;
		payloadparams[2] = reason;
		payloadparams[3] = requester;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}


	public Processor refundCron() {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "refundcron", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public Processor payURefund(String refundId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "refundpayu", gameofthrones);
		String[] queryparams = new String[1];
		queryparams[0] = refundId;
		Processor processor = new Processor(service, requestheaders, queryparams);
		return processor;
	}

	public Processor reverseToPG(String order_id, String source, String reason, String requester, String refund_id, String refund_request_id) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "reversetopg", gameofthrones);
		String[] payloadparams = new String[6];
		payloadparams[0] = order_id;
		payloadparams[1] = source;
		payloadparams[2] = reason;
		payloadparams[3] = requester;
		payloadparams[4] = refund_id;
		payloadparams[5] = refund_request_id;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor refundStatus(String orderId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "refundstatus", gameofthrones);
		String[] queryparams = new String[1];
		queryparams[1] = orderId;
		Processor processor = new Processor(service, requestheaders, queryparams);
		return processor;
	}

	public Processor mobikwikLinkWallet() {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "mobikwiklinkwalletv1", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}


	public Processor mobikwikValidateOTP(String otp) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "mobikwikvalidateotpv1", gameofthrones);
		String[] queryparams = new String[1];
		queryparams[0] = otp;
		Processor processor = new Processor(service, requestheaders, null, queryparams);
		return processor;
	}

	public Processor mobiKwikCheckBalance() {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "mobikwikcheckbalance", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}



	/*CRM helper for cancellation fee*/

	public void cancellationFee(String mobile) {
		DBHelper dbHelper = new DBHelper();
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
		sqlTemplate.update(CheckoutConstants.cancellationFee + mobile);
		System.out.println("Cancellation fee is removed");
	}

	public Processor mobiKwikCalculateCheckSumAndToken(String amount, String orderid, String redirectUrl) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "mobikwikcalculatechecksum", gameofthrones);
		String[] payloadparams = new String[3];
		payloadparams[0] = amount;
		payloadparams[1] = orderid;
		payloadparams[2] = redirectUrl;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor mobiKwikWithDrawMoney(String amount, String orderId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "mobikwikwithdrawamount", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = amount;
		payloadparams[1] = orderId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor mobiKwikRefundMoney(String amount, String orderId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "mobikwikrefund", gameofthrones);

		String[] payloadparams = new String[2];
		payloadparams[0] = amount;
		payloadparams[1] = orderId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor mobiKwikDelinkWallet(String tid, String token) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "mobikwikdelinkv1", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}


	public Processor paytmLinkWallet(String tid, String token) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "paytmlinkwalletv1", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}


	public Processor paytmValidateOTP(String tid, String token, String state, String otp) {
		//HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "paytmvalidateotpv1", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = state;
		payloadparams[1] = otp;
		return new Processor(service, createReqHeader(tid, token), new String[]{state, otp});
		//return processor();
	}

	public HashMap<String, String> createReqHeader(String tid, String token) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Tid", tid);
		requestheaders.put("token", token);
		return requestheaders;
	}

//	 public Processor paytmValidateOTP(String addressId, String sla, HashMap<String, String> headers) {
//	        return edvoCartHelper.reserveCart(addressId, sla, headers);
//	    }

	public Processor paytmCalculateCheckSumAndToken() {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "paytmgetssotokenv1", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public Processor paytmDelinkWallet(String tid, String token) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "paytmdelinkv1", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public Processor paytmCheckBalance() {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "paytmcheckbalancev1", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public Processor paytmblockAmount(String ssoToken, String orderId, String amount) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "paytmblockamountv1", gameofthrones);
		String[] payloadparams = new String[3];
		payloadparams[0] = ssoToken;
		payloadparams[1] = orderId;
		payloadparams[2] = amount;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}


	public Processor paytmWithDrawMoney(String ssoToken, String orderId, String amount, String custId, String deviceId, String custIp) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "paytmwithdrawamountv1", gameofthrones);
		String[] payloadparams = new String[6];
		payloadparams[0] = ssoToken;
		payloadparams[1] = orderId;
		payloadparams[2] = amount;
		payloadparams[3] = custId;
		payloadparams[4] = deviceId;
		payloadparams[5] = custIp;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;

	}


	public Processor paytmRefundMoney(String orderId, String refundAmount, String txnId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "paytmrefundamountv1", gameofthrones);
		String[] payloadparams = new String[3];
		payloadparams[0] = orderId;
		payloadparams[1] = refundAmount;
		payloadparams[2] = txnId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	/* Freecharge */

	public Processor freechargeLinkWallet(String tid, String token) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "freechargelinkwalletv1", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}


	public Processor freechargeValidateOTP(String tid, String token, String state, String otp) {
		//HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "freechargevalidateotpv1", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = state;
		payloadparams[1] = otp;
		return new Processor(service, createReqHeader(tid, token), new String[]{state, otp});
		//return processor();
	}

	public Processor freechargeCheckBalance(String tid, String token) {
		//HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "freechargecheckbalancev1", gameofthrones);
		Processor processor = new Processor(service, createReqHeader(tid, token));
		return processor;
	}


	/**
	 * Generate Order Id method
	 * when addressId is known
	 *
	 * @param payload
	 */

	public String generateOrder(CreateMenuEntry payload) throws IOException {
		String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		HashMap<String, String> hashMap = createLogin(payload.getpassword(), payload.getmobile());
		String response = CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		String orderResponse = orderPlace(hashMap.get("Tid"), hashMap.get("Token"), payload.getaddress_id(), payload.getpayment_cod_method(), payload.getorder_comments()).ResponseValidator.GetBodyAsText();
		String orderId = JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[", "").replace("]", "");
		return orderId;
	}

	/**
	 * Create Order Id method
	 * when addressId is unknown
	 *
	 * @param payload
	 */

	public String createOrder(CreateMenuEntry payload) throws IOException {
		String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		HashMap<String, String> hashMap = createLogin(payload.getpassword(), payload.getmobile());
		String response = CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		String AddressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "");
		String[] AddressIdArray = (AddressId.split(","));
		String deliveryValid = JsonPath.read(response, "$.data..addresses..delivery_valid").toString().replace("[", "").replace("]", "");
		String[] delivery = deliveryValid.split(",");
		List<String> addressIdList = new ArrayList<>();
		for (int i = 0; i < delivery.length; i++) {
			if (delivery[i].equals("1")) {
				addressIdList.add(AddressIdArray[i]);
			}
		}
		String orderResponse = orderPlace(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(addressIdList.get(0)), payload.getpayment_cod_method(), payload.getorder_comments()).ResponseValidator.GetBodyAsText();
		String orderId = JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[", "").replace("]", "").toString();
		return orderId;
	}

	public String create(CreateMenuEntry payload) throws IOException {
		String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		HashMap<String, String> hashMap = createLogin(payload.getpassword(), payload.getmobile());
		String response = CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		String orderResponse = orderPlace(hashMap.get("Tid"), hashMap.get("Token"), payload.getaddress_id(), payload.getpayment_cod_method(), payload.getorder_comments()).ResponseValidator.GetBodyAsText();
		String orderId = JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[", "").replace("]", "");
		return orderId;
	}


	/**
	 * Generate Order Id method
	 * when addressId is not known
	 *
	 * @param payload
	 */

	/*public String generateOrderAddress(CreateMenuEntry payload)throws IOException
	{
		String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
		HashMap<String, String> hashMap=createLogin(payload.getpassword(), payload.getmobile());
		String response= CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		String addressId= addressHelper.deliveryValid(hashMap.get("Tid"), hashMap.get("Token"));
		String orderResponse= orderPlace(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(addressId),payload.getpayment_cod_method(),payload.getorder_comments()).ResponseValidator.GetBodyAsText();
		String orderId= JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[","").replace("]","");
		return orderId;
	}*/
	public String generateCart(CreateMenuEntry payload) throws IOException {
		String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		HashMap<String, String> hashMap = createLogin(payload.getpassword(), payload.getmobile());
		String response = CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		return response;
	}

	public String orderCreatewithspecifiedaddress(CreateMenuEntry payload) throws IOException {
		String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		HashMap<String, String> hashMap = createLogin(payload.getpassword(), payload.getmobile());
		String response = CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		String orderResponse = orderPlace(hashMap.get("Tid"), hashMap.get("Token"), payload.getaddress_id(), payload.getpayment_cod_method(), payload.getorder_comments()).ResponseValidator.GetBodyAsText();
		String orderId = JsonPath.read(orderResponse, "$.data.order_id").toString().replace("[", "").replace("]", "");
		return orderId;
	}

	public String createOrderData(CreateMenuEntry payload) throws Exception {

		String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		HashMap<String, String> hashMap = createLogin(payload.getpassword(), payload.getmobile());
		String response = CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload,
				payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		String AddressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "")
				.replace("\"", "");
		String[] AddressIdArray = (AddressId.split(","));
		String deliveryValid = JsonPath.read(response, "$.data..addresses..delivery_valid").toString().replace("[", "")
				.replace("]", "");
		String[] delivery = deliveryValid.split(",");
		List<String> addressIdList = new ArrayList<>();
		for (int i = 0; i < delivery.length; i++) {
			if (delivery[i].equals("1")) {
				addressIdList.add(AddressIdArray[i]);
			}
		}

		return orderPlace(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(addressIdList.get(0)),
				payload.getpayment_cod_method(), payload.getorder_comments()).ResponseValidator.GetBodyAsText();
	}


	public String createOrderData(CreateMenuEntry payload, String custLatLng) throws Exception {
		String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		HashMap<String, String> hashMap = createLogin(payload.getpassword(), payload.getmobile());
		String response = CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload,
				payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		String AddressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "")
				.replace("\"", "");
		String[] AddressIdArray = (AddressId.split(","));
		String deliveryValid = JsonPath.read(response, "$.data..addresses..delivery_valid").toString().replace("[", "")
				.replace("]", "");
		String[] delivery = deliveryValid.split(",");
		List<String> addressIdList = new ArrayList<>();
		for (int i = 0; i < delivery.length; i++) {
			if (delivery[i].equals("1")) {
				addressIdList.add(AddressIdArray[i]);
			}
		}

		return orderPlace(hashMap.get("Tid"), hashMap.get("Token"), Integer.parseInt(addressIdList.get(0)),
				payload.getpayment_cod_method(), payload.getorder_comments()).ResponseValidator.GetBodyAsText();
	}

	public Processor reserveDE(String tid, String token, String address_id, String sla) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Tid", tid);
		requestheaders.put("token", token);
		GameOfThronesService service = new GameOfThronesService(
				"checkout", "rts_reserve_de", gameofthrones);
		String[] payloadparams = new String[]{address_id, sla};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}


	public HashMap<Integer, HashMap<String, String>> getMenuAddonFromCartOfRest(String restId) {
		Processor processor = SnDHelper.menuV3RestId(restId);
		String response = processor.ResponseValidator.GetBodyAsText();
		System.out.println(processor.ResponseValidator.GetBodyAsText());
		String listID = JsonPath.read(response, "$.data.menu.items[*].id").toString().replace("[", "");
		String[] itemlist = listID.split(",");
		String isAddon = "";
		String isAddonInStock = "";
		String isItemInStock = "";
		String nullVariant = "";
		int mapNum = 0;
		HashMap<Integer, HashMap<String, String>> menuItem = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> addonItem = new HashMap<String, String>();
		for (int i = 0; i < itemlist.length; i++) {
			try {
				isAddon = JsonPath.read(response, "$.data.menu.items." + itemlist[i] + "..addons").toString().replace("[", "").replace("]", "");
				isAddonInStock = JsonPath.read(response, "$.data.menu.items." + itemlist[i] + ".addons.[0].choices.[0].inStock").toString().replace("[", "").replace("]", "");
				nullVariant = JsonPath.read(response, "$.data.menu.." + itemlist[i] + "..variants_new.variant_groups").toString().replace("[", "").replace("]", "");
			} catch (Exception t) {
				continue;
			}

			if (!isAddon.equals("") && isAddonInStock.equals("1") && nullVariant.equals("")) {
				isItemInStock = JsonPath.read(response, "$.data.menu.items." + itemlist[i] + ".inStock").toString().replace("[", "").replace("]", "");
				if (isItemInStock.equals("1")) {
					String choiceId = JsonPath.read(response, "$.data.menu.items." + itemlist[i] + ".addons.[0].choices.[0].id").toString().replace("[", "").replace("]", "");
					String groupId = JsonPath.read(response, "$.data.menu.items." + itemlist[i] + "..addons..group_id").toString().replace("[", "").replace("]", "");

					if (!choiceId.equals("") & !groupId.equals("")) {
						addonItem.put("addons_choice_id", choiceId);
						addonItem.put("addons_group_id", groupId);
						addonItem.put("addons_name",
								JsonPath.read(response, "$.data.menu.items." + itemlist[i] + "..addons..group_name").toString().replace("[", "").replace("]", "").replace("\"", ""));
						addonItem.put("addons_price",
								JsonPath.read(response, "$.data.menu.items." + itemlist[i] + ".addons.[0].choices.[0].price").toString().replace("[", "").replace("]", ""));
						addonItem.put("menu_item_id", itemlist[i]);
						addonItem.put("restaurantId", restId);
						menuItem.put(mapNum, addonItem);
						mapNum++;
					} else {
						continue;
					}
				}
			}
		}
		System.out.println("-----------------------------------------" + menuItem + "---");
		return menuItem;
	}

	public Processor CreateCartAddonNew(String tid, String token, String menu_item_id, String quantity, String choice_id, String group_id,
										String name, String price, String restId) {
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("Tid", tid);
		requestheaders.put("token", token);
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2Addon", gameofthrones);
		String[] payloadparams = new String[7];
		payloadparams[0] = menu_item_id;
		payloadparams[1] = quantity;
		payloadparams[2] = choice_id;
		payloadparams[3] = group_id;
		payloadparams[4] = name;
		payloadparams[5] = price;
		payloadparams[6] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor orderPlaceV2(String tid, String token, String addressId, String paymentMethod, String comments) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev2_1", gameofthrones);
		String[] payloadparams = {addressId, paymentMethod, comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public String getAddressIDfromCartResponse(String tid, String token, String cartResponse) {
		String addressId = "";
		System.out.println(cartResponse);
		String[] validAddressList = JsonPath.read(cartResponse, "$.data.addresses..delivery_valid").toString().replace("[", "").replace("]", "").split(",");
		for (int i = 0; i < validAddressList.length; i++) {
			if (validAddressList[i].equals("1")) {
				addressId = JsonPath.read(cartResponse, "$.data.addresses.[" + i + "]..id").toString().replace("[", "").replace("]", "").replace("\"", "");
				return addressId;
			}
		}
		String lat = JsonPath.read(cartResponse, "$.data.restaurant_details.lat").toString().replace("[", "").replace("]", "").replace("\"", "");
		String lng = JsonPath.read(cartResponse, "$.data.restaurant_details.lng").toString().replace("[", "").replace("]", "").replace("\"", "");
		String mobileNum = JsonPath.read(cartResponse, "$.data.phone_no").toString().replace("[", "").replace("]", "").replace("\"", "");
		String addResponse = addNewAddress(tid, token, "TestName", mobileNum, "TestAddress", "TestLandMark", "TestArea", lat, lng, "", "", "Test_Location").ResponseValidator.GetBodyAsText();
		addressId = JsonPath.read(addResponse, "$.data..address_id").toString().replace("[", "").replace("]", "");
		System.out.println("New created Serviceable Address ID -: " + addressId);
		return addressId;
	}

	public Processor addNewAddress(String tid, String token, String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city, String annotation) {
		GameOfThronesService service = new GameOfThronesService("checkout", "newaddress", gameofthrones);
		String[] payloadparams = {name, mobile, address, landmark, area, lat, lng, flat_no, city, annotation};
		Processor processor = new Processor(service, requestHeader(tid, token), payloadparams);
		return processor;
	}

	public Processor placeSpecialOrder(String tid, String token, String paymentMethod, String orderComment, String orderType) {
		GameOfThronesService service = new GameOfThronesService("checkout", "placeSpecialOrder", gameofthrones);
		String[] payloadparams = {paymentMethod, orderComment, orderType};
		Processor processor = new Processor(service, requestHeader(tid, token), payloadparams);
		return processor;
	}

	public Processor editOrderCheck(String tid, String token, String orderID, String source, String menuID, String quantity, String restId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "editordercheck", gameofthrones);
		String[] payloadparams = {orderID, source, menuID, quantity, restId};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor editOrderConfirm(String tid, String token, String orderID, String source, String menuID, String quantity, String restId) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "editorderconfirm", gameofthrones);
		String[] payloadparams = {orderID, source, menuID, quantity, restId};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public String getMenuItemWithoutVariant(String restroMenuList, Integer minPrice) {
		System.out.println(restroMenuList);
		LinkedHashMap<String, Object> menuItems = JsonPath.read(restroMenuList, "$.data.menu.items");
		List<String> items = new ArrayList<String>();
		List<Object> itemVariation = null;
		for (Map.Entry<String, Object> entry : menuItems.entrySet()) {

			try {
				itemVariation = JsonPath.read(entry.getValue(), "$.variants_new.variant_groups.*.variations");
			} catch (PathNotFoundException e) {
			}
			if ((null == itemVariation || itemVariation.size() == 0) && (((int) JsonPath.read(entry.getValue(), "$.price") / 100) >= minPrice)) {

				items.add(entry.getKey());
			}
		}
		if (items.size() != 0) {
			Random random = new Random();
			return items.get(random.nextInt(items.size()));
		} else {
			return "";
		}
	}

	public Processor editOrderCheck(String[] payload) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>() {{
			put("Content-Type", "application/json");
			put("Authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
		}};

		GameOfThronesService service = new GameOfThronesService("checkout", "editordercheck", gameofthrones);

		Processor processor = new Processor(service, requestHeaders, payload);
		return processor;
	}

	public Processor editOrderCheck(HashMap<String, String> payload) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>() {{
			put("Content-Type", "application/json");
			put("Authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
		}};
		GameOfThronesService service = new GameOfThronesService("checkout", "editordercheckPOJO", gameofthrones);
		return new Processor(service, requestHeaders, orderEditPayload(payload));
	}

	public Processor editOrderConfirm(String[] payload) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>() {{
			put("Content-Type", "application/json");
			put("Authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
		}};
		GameOfThronesService service = new GameOfThronesService("checkout", "editorderconfirm", gameofthrones);
		return new Processor(service, requestHeaders, payload);
	}

	public Processor editOrderConfirm(HashMap<String, String> payload) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>() {{
			put("Content-Type", "application/json");
			put("Authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
		}};
		GameOfThronesService service = new GameOfThronesService("checkout", "editorderconfirmPOJO", gameofthrones);
		return new Processor(service, requestHeaders, orderEditPayload(payload));
	}

	public String[] orderEditPayload(HashMap<String, String> payload) {
		OrderEditPOJO orderEditPOJO = new OrderEditPOJO();
		orderEditPOJO.setCouponCode(payload.get("couponCode"));
		orderEditPOJO.setOrderId(Long.valueOf(payload.get("orderID")));
		orderEditPOJO.setRestaurantId(Integer.valueOf(payload.get("restID")));
		orderEditPOJO.setInitiationSource(Integer.valueOf(payload.get("initSource")));
		List<CartItem> cart = new ArrayList<CartItem>() {{
			add(new CartItem()
					.withMenuItemId(payload.get("menuID"))
					.withAddons(null)
					.withQuantity(Integer.valueOf(payload.get("quantity"))));
		}};
		orderEditPOJO.setCartItems(cart);
		String[] strArr = null;
		try {
			strArr = new String[]{jsonHelper.getObjectToJSON(orderEditPOJO)};
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strArr;
	}

	public void validateSucessStatusCode(int statusCode) {
		Assert.assertEquals(statusCode, PricingConstants.SUCCESS_CODE);
	}

	public void validateApiResStatusData(String response) {
		String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
		Assert.assertEquals(getStatusCode, PricingConstants.VALID_STATUS_CODE, "HTTPS response is not 0");
		String isSuccessful = JsonPath.read(response, "$.successful").toString().replace("[", "").replace("]", "");
		Assert.assertEquals(isSuccessful, CheckoutConstants.IS_SUCCESSFUL_FLAG, "Successful flag is false");
	}

	public Processor getOrderDetails(String tid, String token, String orderID) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getOrderDetailsV1", gameofthrones);
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		String[] urlParams = {orderID};
		Processor processor = new Processor(service, requestheaders, null, urlParams);
		return processor;
	}

	public Processor createCart(String tid, String token, String cartPayload) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2edvo", gameofthrones);
		return new Processor(service, requestheaders, new String[]{cartPayload}, null);
	}

	public Processor editOrderCheck(String tid, String token, String[] payload) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
        requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "editordercheckv3", gameofthrones);
		Processor processor = new Processor(service, requestheaders, payload, null);
		return processor;
	}

	public Processor editOrderConfirm(String tid, String token, String[] payload) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "editorderconfirmv3", gameofthrones);
		Processor processor = new Processor(service, requestheaders, payload);
		return processor;
	}

	public Processor cloneOrderCheck(String tid, String token, String[] payload) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "cloneordercheckv2", gameofthrones);
		Processor processor = new Processor(service, requestheaders, payload, null);
		return processor;

	}

	public Processor cloneOrderConfirm(String tid, String token, String[] payload) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "cloneorderconfirmv2", gameofthrones);
		Processor processor = new Processor(service, requestheaders, payload, null);
		return processor;
	}

	public Processor invokeApi(String serviceName, String apiName, HashMap<String, String> headers, String[] payloadParam, String[] urlParam) {
		GameOfThronesService service = new GameOfThronesService(serviceName, apiName, gameofthrones);
		Processor processor = new Processor(service, headers, payloadParam, urlParam);
		return processor;
	}

	public String getEditOrderPayload(Cart cartPayload) {

		List<CartItem> cartItems = new ArrayList<>();
		for (int i = 0; i < cartPayload.getCartItems().size(); i++) {
			CartItem cartItem = cartPayload.getCartItems().get(i);
			cartItem.setQuantity(cartItem.getQuantity() + 1);
			cartItems.add(cartItem);
		}
		cartPayload.setCartItems(cartItems);

		String cartItemEntity = Utility.jsonEncode(cartPayload);
		cartItemEntity = cartItemEntity.substring(1, cartItemEntity.length() - 1);

		return cartItemEntity;
	}

	public Processor revertCancellationFee(String tid, String token, String[] payload) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		GameOfThronesService service = new GameOfThronesService("checkout", "revertcancellationfee", gameofthrones);
		Processor processor = new Processor(service, requestheaders, payload, null);
		return processor;
	}

	public String[] revertCancellationFeePayload(String orderId, String reason, String amount, String createdBy) {
		RevertCancellationFeeRequest revertCancellationFeeRequest = new RevertCancellationFeeRequest(orderId, reason, amount, createdBy);
		return new String[]{Utility.jsonEncode(revertCancellationFeeRequest)};
	}

	public void dbUpdateOperation(String dbName, String dbQuery) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(dbName);
		sqlTemplate.update(dbQuery);

	}



	public HashMap<String, String> setCompleteHeader(String tid, String token,
													 String userAgent, String versionCode) {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Authorization", CheckoutConstants.authorization);
		requestheaders.put("tid", tid);
		requestheaders.put("token", token);
		requestheaders.put("User-Agent", userAgent);
		requestheaders.put("Version-Code", versionCode);
		return requestheaders;
	}

	public Processor createCart(HashMap<String, String> requestheaders, String cartPayload) {
		GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2edvo", gameofthrones);
		return new Processor(service, requestheaders, new String[]{cartPayload}, null);
	}

	public Processor getCart(HashMap<String, String> requestheaders) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getcart", gameofthrones);
		Processor processor = new Processor(service, requestheaders, null, null);
		return processor;
	}

	public Processor orderPlaceV2(HashMap<String, String> requestheaders, String addressId, String paymentMethod, String comments) {
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev2_1", gameofthrones);
		String[] payloadparams = {addressId, paymentMethod, comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor getOrderDetails(HashMap<String, String> requestheaders, String orderID) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getOrderDetailsV1", gameofthrones);
		String[] urlParams = {orderID};
		Processor processor = new Processor(service, requestheaders, null, urlParams);
		return processor;
	}

	public Processor getSingleOrder(HashMap<String, String> requestheaders, String orderId) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getSingleOrder", gameofthrones);
		Processor processor = new Processor(service, requestheaders, null, new String[]{orderId});
		return processor;
	}

	public Processor getSingleOrderKey(HashMap<String, String> requestheaders, String orderKey) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getSingleOrderKey", gameofthrones);
		Processor processor = new Processor(service, requestheaders, null, new String[]{orderKey});
		return processor;
	}

	public Processor getLastOrder(HashMap<String, String> requestheaders) {
		GameOfThronesService service = new GameOfThronesService("checkout1", "getLastOrder", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public Processor getAllOrders(HashMap<String, String> requestheaders) {
		GameOfThronesService service = new GameOfThronesService("checkout1", "getAllOrders", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public Processor getMinimalOrderDetails(HashMap<String, String> requestheaders, String orderKey) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getMinOrderDetails", gameofthrones);
		String[] urlParams = {orderKey};
		Processor processor = new Processor(service, requestheaders, null, urlParams);
		return processor;
	}

	public Processor track(HashMap<String, String> requestheaders, String orderId) {
		GameOfThronesService service = new GameOfThronesService("checkout", "track", gameofthrones);
		String[] urlParams = {orderId};
		Processor processor = new Processor(service, requestheaders, null, urlParams);
		return processor;
	}

	public Processor editOrderCheck(HashMap<String, String> requestheaders, String[] payload) {
		GameOfThronesService service = new GameOfThronesService("checkout", "editordercheckv2", gameofthrones);
		Processor processor = new Processor(service, requestheaders, payload, null);
		return processor;
	}

	public static String getCurrentDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static String getFutureDate() {
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		Date tomorrow = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(tomorrow);
	}


	public Processor editOrderConfirm(HashMap<String, String> requestheaders, String[] payload) {
		GameOfThronesService service = new GameOfThronesService("checkout", "editorderconfirmv3", gameofthrones);
		Processor processor = new Processor(service, requestheaders, payload);
		return processor;
	}

	public Processor OrderCancel(HashMap<String, String> requestheaders, String reason, String feeApplicability,
								 String cancellation_fee, String cancellation_reason, String orderId) {
		GameOfThronesService service = new GameOfThronesService("checkout", "ordercancel", gameofthrones);
		String[] payloadparams = {reason, feeApplicability, cancellation_fee, cancellation_reason};
		String[] urlParams = {orderId};
		Processor processor = new Processor(service, requestheaders, payloadparams, urlParams);
		return processor;
	}

	public Processor cloneOrderCheck(HashMap<String, String> requestheaders, String[] payload) {
		GameOfThronesService service = new GameOfThronesService("checkout", "cloneordercheck", gameofthrones);
		Processor processor = new Processor(service, requestheaders, payload, null);
		return processor;
	}

	public Processor cloneOrderConfirm(HashMap<String, String> requestheaders, String[] payload) {
		GameOfThronesService service = new GameOfThronesService("checkout", "cloneorderconfirm", gameofthrones);
		Processor processor = new Processor(service, requestheaders, payload, null);
		return processor;
	}

	public Processor getRestList(String[] latLngPage) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		requestHeader.put("User-Agent", "Swiggy-Android");
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listingv5", gameofthrones);
		Processor processor = new Processor(service, requestHeader, null, latLngPage);
		return processor;
	}


	public Processor createCartCheckTotals(String cartPayload) {
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "checktotalv2edvo", gameofthrones);
		Processor processor = new Processor(service, requestheaders, new String[]{cartPayload});
		return processor;
	}

	public Processor getAllPayments(HashMap<String, String> requestheaders) {
		GameOfThronesService service = new GameOfThronesService("checkout", "getAllPayments", gameofthrones);
		Processor processor = new Processor(service, requestheaders);
		return processor;
	}

	public Processor trackMinimalOrder(HashMap<String, String> requestheaders, String orderKey) {
		GameOfThronesService service = new GameOfThronesService("checkout1", "trackminimalorder", gameofthrones);
		String[] urlParams = {orderKey};
		Processor processor = new Processor(service, requestheaders, null, urlParams);
		return processor;
	}

	public Processor DeleteCart(HashMap<String, String> requestheaders) {

		GameOfThronesService service = new GameOfThronesService("checkout", "deletecart", gameofthrones);
		HashMap<String, String> headers = new HashMap<String, String>();
		Processor processor = new Processor(service, requestheaders, null, null);
		return processor;
	}

	public Processor deleteCart(HashMap<String, String> requestheaders) {
		GameOfThronesService service = new GameOfThronesService("checkout", "deletecart", gameofthrones);
		return new Processor(service, requestheaders, null, null);
	}

	public Processor CreateCartCheckTotals(HashMap<String, String> requestheaders, int n, String restId) {
		GameOfThronesService service = new GameOfThronesService("checkout", "checktotalv2", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = get(n, restId).toString();
		payloadparams[1] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor cartMinimalV2(HashMap<String, String> requestheaders, int n, String restId) {
		GameOfThronesService service = new GameOfThronesService("checkout", "minimalcartv2", gameofthrones);
		String[] payloadparams = new String[2];
		payloadparams[0] = get(n, restId).toString();
		payloadparams[1] = restId;
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor applyCouponOnCart(HashMap<String, String> requestheaders, String couponCode) {
		GameOfThronesService service = new GameOfThronesService("checkout", "applycoupon", gameofthrones);
		String[] payloadparams = {couponCode};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor orderCancelCheck(HashMap<String, String> requestheaders, String payload) {
		GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "cancelcheck", gameofthrones);
		Processor processor = new Processor(gameOfThronesService, requestheaders, new String[]{payload}, null);
		return processor;
	}

	public Processor confirmOrder(HashMap<String, String> requestheaders) {
		GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "confirmOrder", gameofthrones);
		Processor processor = new Processor(gameOfThronesService, requestheaders, null, null);
		return processor;
	}

	public Processor deleteCouponOnCart(HashMap<String, String> requestheaders, String couponCode) {
		GameOfThronesService service = new GameOfThronesService("checkout", "deletecoupon", gameofthrones);
		String[] payloadparams = {couponCode};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}

	public Processor createCartCheckTotals(HashMap<String, String> requestheaders, String cartPayload) {
		GameOfThronesService service = new GameOfThronesService("checkout", "checktotalv2edvo", gameofthrones);
		Processor processor = new Processor(service, requestheaders, new String[]{cartPayload});
		return processor;
	}

	public Processor updateMinimalCart(HashMap<String, String> requestheaders, String cartPayload) {
		GameOfThronesService service = new GameOfThronesService("checkout", "updateMinimal", gameofthrones);
		return new Processor(service, requestheaders, new String[]{cartPayload});
	}

	public String orderCancelCheckPayload(String orderId, boolean cancellationFeeApplicability) {
		OrderCancelCheckRequest orderCancelCheckRequest = new OrderCancelCheckRequest(orderId, cancellationFeeApplicability);
		return Utility.jsonEncode(orderCancelCheckRequest);
	}

	public String getCurrentDateTimeIn_yyyy_mm_dd_hh_mm() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String strDate = sdf.format(cal.getTime());
		System.out.println("Current Date_Time  =====>  " + strDate);
		return strDate;
	}

	public String getDayPlusOrMinusCurrentDateTime_yyyy_mm_dd_hh_mm(int day) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, day);
		String finalDateTime = dateFormat.format(cal.getTime());
		System.out.println("Final Date-Time after add/subtract(min.): " + day + " to current date-time is: " + finalDateTime);
		return finalDateTime;
	}

	public static String getCurrentDateTime_yyyy_mm_dd_hh_mm_ss() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String finalDateTime = dateFormat.format(date);
		System.out.println("current date-time is: " + finalDateTime);
		return finalDateTime;
	}

	public void updateSwiggyMoney(String userId, double swiggyMoney) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CheckoutConstants.checkout);
		sqlTemplate.update(CheckoutConstants.updateSwiggyMoney + swiggyMoney + CheckoutConstants.whereUserId + userId);
	}

	public void updateCancellationFee(String userId, double calcellationFee) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CheckoutConstants.checkout);
		sqlTemplate.update(CheckoutConstants.updateCancellationFee + calcellationFee + CheckoutConstants.whereUserId + userId);
	}

	public void resetUserCredit(String userId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CheckoutConstants.checkout);
		System.out.println("Update Query: -" + CheckoutConstants.resetUserCredit + userId);
		sqlTemplate.update(CheckoutConstants.resetUserCredit + userId);
	}

	public int getRenderingVersion(String optionName) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CheckoutConstants.checkout);
		Object[] params = new Object[]{optionName};
		return sqlTemplate.queryForObjectInteger(CheckoutConstants.renderingVersionCode, params, String.class);
	}

	public void updateDeliveryFee(String restID, String delFee) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String currentTime = getCurrentDateTime_yyyy_mm_dd_hh_mm_ss();
		String query = CheckoutConstants.updateDelFee + delFee + CheckoutConstants.updatedOn +
				currentTime + CheckoutConstants.whereId + restID;
		System.out.println(query);
		sqlTemplate.update(query);
	}

	public void updatePackingFee(String restID, String packingCharge) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String currentTime = getCurrentDateTime_yyyy_mm_dd_hh_mm_ss();
		String query = CheckoutConstants.updatePackingFee + packingCharge + CheckoutConstants.updatedOn +
				currentTime + CheckoutConstants.whereId + restID;
		System.out.println(query);
		sqlTemplate.update(query);
	}

	public void updateItemGST(String restID) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String currentTime = getCurrentDateTime_yyyy_mm_dd_hh_mm_ss();
		String query = CheckoutConstants.updateItemGST + currentTime + CheckoutConstants.whereRestId + restID;
		System.out.println(query);
		sqlTemplate.update(query);
	}

	public void updatePackagingGST(String restID) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String currentTime = getCurrentDateTime_yyyy_mm_dd_hh_mm_ss();
		String query = CheckoutConstants.updatePackagingGST + currentTime + CheckoutConstants.whereRestId + restID;
		System.out.println(query);
		sqlTemplate.update(query);
	}

	public void resetGST(String restID) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String currentTime = getCurrentDateTime_yyyy_mm_dd_hh_mm_ss();
		String query = CheckoutConstants.resetGST + currentTime + CheckoutConstants.whereRestId + restID;
		System.out.println(query);
		sqlTemplate.update(query);
	}

	public void resetRestFee(String restID) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String currentTime = getCurrentDateTime_yyyy_mm_dd_hh_mm_ss();
		String query = CheckoutConstants.resetRestFee + currentTime + CheckoutConstants.whereId + restID;
		System.out.println(query);
		sqlTemplate.update(query);
	}


	public Processor getPreOrderSlot(String restID) {
		String[] queryParam = {restID};
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService gots = new GameOfThronesService("sand", "preOrderSlot", gameofthrones);
		Processor processor = new Processor(gots, requestHeader, null, queryParam);
		return processor;
	}

	public Processor getRestaurantMenu(String restID) {
		Processor processor = sandHelper.getRestaurantMenu(restID);
		return processor;
	}

	public static HashMap<String, String> loginData(String mobile, String password) {
		String resp = SnDHelper.consumerLogin(mobile, password).ResponseValidator.GetBodyAsText();
		String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
		String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
		String customerId = JsonPath.read(resp, "$.data.customer_id").toString().replace("[", "").replace("]", "");
		String deviceId = JsonPath.read(resp, "$.deviceId").toString().replace("[", "").replace("]", "");
		String sid = JsonPath.read(resp, "$.sid").toString().replace("[", "").replace("]", "");
		String userId = JsonPath.read(resp, "$.data.addresses[0]..user_id").toString().replace("[", "").replace("]", "");
		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put("Tid", tid);
		hashMap.put("Token", token);
		hashMap.put("CustomerId", customerId);
		hashMap.put("userId", userId);
		hashMap.put("DeviceId", deviceId);
		hashMap.put("sid", sid);
		return hashMap;
	}


	public Processor isAddresServicable(String tid, String token, String lat, String lng) {
		HashMap<String, String> requestheaders = requestHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "addressservicable", gameofthrones);
		String[] latlng = {lat, lng};
		Processor processor = new Processor(service, requestheaders, null, latlng);
		return processor;
	}

	public void invokeMakeServicableApi(String restId) throws Exception {
		System.setProperty("rest_id_for_serviceability", restId);
		new Jarvis().serviceablityCheck();

	}


	public String getOrderKey(String placeOrderRes) {
		String[] rendkeys = JsonPath.read(placeOrderRes, "$.data..order_data..rendering_details..key")
				.toString().replace("[", "").replace("]", "").replace("\"", "").split(",");
		String[] keys = JsonPath.read(placeOrderRes, "$.data..order_data..key")
				.toString().replace("[", "").replace("]", "").replace("\"", "").split(",");

		ArrayList<String> key = new ArrayList<String>();
		for (int i = 0; i < keys.length; i++) {
			if (!Arrays.asList(rendkeys).contains(keys[i]))
				key.add(keys[i]);
		}
		System.out.println("Order Key:--->" + key.get(0));
		return key.get(0);
	}

	public static void validateApiResSanity(String response) {
		String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
		Assert.assertEquals(getStatusCode, PricingConstants.VALID_STATUS_CODE, "HTTPS response is not 0");
		String isSuccessful = JsonPath.read(response, "$.successful").toString().replace("[", "").replace("]", "");
		Assert.assertEquals(isSuccessful, CheckoutConstants.IS_SUCCESSFUL_FLAG, "Successful flag is false");
	}

	//e.g - Tue (not TUE)
	public static String getDay() {
		Format formatter = new SimpleDateFormat("EEE");
		String day = formatter.format(new Date());
		return day;
	}

	public String invokePlaceOrder(String tid, String token, String cartResponse) {
		String placeOrderRes = null;
		HashMap<String, String> header = setCompleteHeader(tid, token, "Swiggy-Android", "310");
		String addressId = getAddressIDfromCartResponse(tid, token, cartResponse);
		String isAddressServiceable = invokeIsAddressServiceable(tid, token, cartResponse);

		if (isAddressServiceable.equals(false)) {
			makeAddressServiceable(cartResponse);
		}

		String paymentRes = invokeGetAllPayments(tid, token, "Swiggy-Android", "310");
		String isCodEnabled = isCodEnabled(paymentRes);

		if (isCodEnabled.equals("true")) {
			placeOrderRes = orderPlaceV2(header, addressId, superConstant.PAYMENT_METHOD_CASH, superConstant.DEF_STRING)
					.ResponseValidator.GetBodyAsText();
			validateApiResStatusData(placeOrderRes);
		} else {
			throw new SkipException("Skipping test, COD is disabled");
		}
		return placeOrderRes;
	}


	public String invokeIsAddressServiceable(String tid, String token, String cartResponse) {

		String lat = JsonPath.read(cartResponse, "$.data.restaurant_details.lat")
				.toString().replace("[", "").replace("]", "").replace("\"", "");
		String lng = JsonPath.read(cartResponse, "$.data.restaurant_details.lng")
				.toString().replace("[", "").replace("]", "").replace("\"", "");

		String addresServiceableRes = isAddresServicable(tid, token, lat, lng).ResponseValidator.GetBodyAsText();

		String isAddressServiceable = JsonPath.read(addresServiceableRes, "$.data.is_address_serviceable")
				.toString().replace("[", "").replace("]", "").replace("\"", "");

		return isAddressServiceable;
	}

	public void makeAddressServiceable(String cartResponse) {

		String restId = JsonPath.read(cartResponse, "$.data.restaurant_details.id")
				.toString().replace("[", "").replace("]", "").replace("\"", "");
		try {
			invokeMakeServicableApi(restId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String invokeCreateCart(String tid, String token, String cartPayload) {
		HashMap<String, String> header = setCompleteHeader(tid, token, "Swiggy-Android", "310");
		String cartResponse = createCart(header, cartPayload).ResponseValidator.GetBodyAsText();
		return cartResponse;
	}

	public String invokeGetAllPayments(String tid, String token, String userAgent, String versionCode) {
		HashMap<String, String> header = setCompleteHeader(tid, token, userAgent, versionCode);
		String getAllPaymentsRes = getAllPayments(header).ResponseValidator.GetBodyAsText();
		validateApiResStatusData(getAllPaymentsRes);
		return getAllPaymentsRes;
	}

	public String isCodEnabled(String paymentRes) {
		String isCodEnabled = "";
		String[] groupName = JsonPath.read(paymentRes, "$.data..payment_group[*]..group_name")
				.toString().replace("[", "").replace("]", "").replace("\"", "")
				.split(",");
		for (int i = 0; i < groupName.length; i++) {
			System.out.println("Group Name:- " + groupName[i]);

			String preferSection = JsonPath.read(paymentRes, "$.data..payment_group.[0]..payment_methods..name")
					.toString().replace("[", "").replace("]", "").replace("\"", "");

			if (preferSection.equals("cod")) {
				isCodEnabled = JsonPath.read(paymentRes, "$.data..payment_group.[0]..payment_methods..enabled")
						.toString().replace("[", "").replace("]", "");

				return isCodEnabled;
			}

			if (groupName[i].equals("cod")) {
				isCodEnabled = JsonPath.read(paymentRes, "$.data..payment_group.[" + i + "]..payment_methods..enabled")
						.toString().replace("[", "").replace("]", "");
			}
		}
		return isCodEnabled;
	}


	public String getMenu()  {
		SoftAssert sc = new SoftAssert();
		String res = Orderhelp.Menu().ResponseValidator.GetBodyAsText();
		String restaurantId = JsonPath.read(res, "$.data.id").toString();
		sc.assertEquals(restaurantId, EditTimeConstants.REST_ID, "RestaurentId matched");
		int status_code = JsonPath.read(res, "$.statusCode");
		sc.assertEquals(status_code, 0, "Status code  matching");
		Reporter.log("------------------Menu Succesful------------------");
		String[] itemIds = JsonPath.read(res, "$.data.menu.items..id").toString().replace("[", "").replace("]", "").split(",");
		if(itemIds==null){
			Reporter.log("Items Not Found");
		}
		String itemId = null;
		for (int i = 0; i <= itemIds.length - 1; i++) {

			String temp = itemIds[i];

			String s = "$.data.menu.items."+itemIds[i]+".inStock";

			if (JsonPath.read(res, s).toString().replace("[", "").replace("]", "").equalsIgnoreCase("1")) {
				String ItemIdRequierd = itemIds[i];
				System.out.println(ItemIdRequierd);
				itemId = temp;
				break;
			}

		}
		return itemId;
	}
    public Integer getaddressId(String Tid,String Token) throws Exception {
        SoftAssert sc = new SoftAssert();
        String res = invokeCreateCartAPI(Tid,Token,getMenu(),"1",EditTimeConstants.REST_ID).ResponseValidator.GetBodyAsText();
        String restaurantId = JsonPath.read(res, "$.data.restaurant_details.id").toString();
        sc.assertEquals(restaurantId, EditTimeConstants.REST_ID, "RestaurentId matched");
        int status_code = JsonPath.read(res, "$.statusCode");
        sc.assertEquals(status_code, 0, "Status code  matching");
        Reporter.log("------------------Checkout Succesful------------------");
        String[] AddressIds = JsonPath.read(res, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "").split(",");
		if(AddressIds==null){
			Reporter.log("Items Not Found");
		}
        System.out.println(AddressIds+"AddressIds without double quotes");
        int[] array = Arrays.stream(AddressIds).mapToInt(Integer::parseInt).toArray();
		System.out.println("------Arrays"+array);
        Integer AddressId= null;
        for (int i = 0; i <= array.length - 1; i++) {

            int temp = array[i];

            String s = String.format("$.data.addresses"+"["+i+"]"+"..delivery_valid", temp);
            //  System.out.println(Boolean.valueOf(JsonPath.read(res,s).toString().replace("[","").replace("]","")));

            if (JsonPath.read(res,s).toString().replace("[", "").

					replace("]", "").equalsIgnoreCase("1")) {
                int AddressIdrequired = array[i];
                System.out.println(AddressIdrequired);
                AddressId = temp;
                break;
            }
        }
        return AddressId;
	}
	public Long order_id(String Tid,String Token) throws Exception {
		SoftAssert sc = new SoftAssert();
		String res = Orderhelp.orderPlace(Tid,Token,getaddressId(Tid,Token), CheckoutConstants.paymentMethod, CheckoutConstants.orderComments).ResponseValidator.GetBodyAsText();
		String Order = JsonPath.read(res, "$.data.order_id").toString().replace("[", "").replace("]","").replace("\"", "").trim();
	    sc.assertNotEquals(Order,null,"OrderId Captured");
	    Long Order_Id = Long.parseLong(Order);
		System.out.println("IntValue"+Order_Id);
		sc.assertNotEquals(Order_Id, null, " OrderId Found");
		int status_code = JsonPath.read(res, "$.statusCode");
		sc.assertEquals(status_code, 0, "Status code  matching");
		Reporter.log("------------------Menu Succesful------------------");
		return Order_Id;
	}
}

