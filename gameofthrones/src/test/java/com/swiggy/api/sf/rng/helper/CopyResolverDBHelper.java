package com.swiggy.api.sf.rng.helper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;

import static com.swiggy.api.sf.rng.constants.RngConstants.marketingDB;

public class CopyResolverDBHelper {
    static String disableCampaignQuery = "update campaign SET enabled=0 where id IN (select `campaign_id` from `campaign_rule_map` where `restaurant_id`= \"#1\");";
//    static String disableFreebieSuperCampaign="update campaign SET enabled=0 where id IN (select campaign_id from campaign_rule_map where rule_id IN (select `id` from `rule` where `entity_id`= \"#1\");";

    TdCachingHelper tdCachingHelper = new TdCachingHelper();

    public void disableFreeDelSuperCampaign(String restid) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(marketingDB);
        String disableCampaign = TDCachingDbHelper.replaceOneWord(disableCampaignQuery, restid);
        System.out.println("Disabled query: " + disableCampaign);
        sqlTemplate.execute(disableCampaign);
        tdCachingHelper.evictKeyFromGuava();
        tdCachingHelper.deleteDataFromCacheForRestaurant(restid);
    }

    public void disableFreebieSuperCampaign(String restid) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(marketingDB);
        String disableCampaign = TDCachingDbHelper.replaceOneWord(disableCampaignQuery, restid);
        System.out.println("Disabled query: " + disableCampaign);
        sqlTemplate.execute(disableCampaign);
        tdCachingHelper.evictKeyFromGuava();
        tdCachingHelper.deleteDataFromCacheForRestaurant(restid);
    }

}
