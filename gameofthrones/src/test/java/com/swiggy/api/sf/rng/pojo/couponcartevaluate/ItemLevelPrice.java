package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class ItemLevelPrice {
	@JsonProperty("11725392_0_variants_addons")
	private _117253920VariantsAddons _117253920VariantsAddons;
	@JsonProperty("11725392_0_variants_addons")
	public _117253920VariantsAddons get117253920VariantsAddons() {
		return _117253920VariantsAddons;
	}
	@JsonProperty("11725392_0_variants_addons")
	public void set117253920VariantsAddons(_117253920VariantsAddons _117253920VariantsAddons) {
		this._117253920VariantsAddons = _117253920VariantsAddons;
	}
	public ItemLevelPrice with117253920VariantsAddons(_117253920VariantsAddons _117253920VariantsAddons) {
		this._117253920VariantsAddons = _117253920VariantsAddons;
		return this;
	}
}
