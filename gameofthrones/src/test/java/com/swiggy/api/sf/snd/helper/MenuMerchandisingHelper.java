package com.swiggy.api.sf.snd.helper;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import static com.swiggy.api.sf.snd.constants.MenuMerchandisingConstants.authorisation;


public class MenuMerchandisingHelper {

    Initialize gameofthrones = new Initialize();
    RedisHelper redisHelper= new RedisHelper();
    JsonHelper jsonHelper = new JsonHelper();

    public Processor getMenuV4(String restId,String lat, String lng)
    {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("version-code", "5001");
        requestHeaders.put("user-agent", "Swiggy-Android");
        GameOfThronesService service = new GameOfThronesService("sand", "menuV4", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] { restId,lat,lng});
        errorCodeHandler(processor,"menuv4");
        return processor;
    }
    public Processor getMenuV4VersionCheck(String Id,String lat, String lng)
    {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("version-code", "500");
        requestHeaders.put("user-agent", "Swiggy-Android");
        GameOfThronesService service = new GameOfThronesService("sand", "menuV4", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] { Id,lat,lng});
        errorCodeHandler(processor,"menuv4");
        return processor;
    }

    public Processor getMerchandisingInfo(String restId){

        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("User-Agent","Swiggy-Android");
        requestHeaders.put("version-code","5001");
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Authorization", authorisation);
        GameOfThronesService service = new GameOfThronesService("sandmerch", "merchandising", gameofthrones);
        Processor processor = new Processor(service, requestHeaders,null,new String[] {restId});
        errorCodeHandler(processor,"merchandising");
        return processor;
    }

    public Processor getMerchandisingInfoVersionCheck(String restId){

        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("User-Agent","Swiggy-Android");
        requestHeaders.put("version-code","500");
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Authorization", authorisation);
        GameOfThronesService service = new GameOfThronesService("sandmerch", "merchandising", gameofthrones);
        Processor processor = new Processor(service, requestHeaders,null,new String[] {restId});
        errorCodeHandler(processor,"merchandising");
        return processor;
    }

    // helper to read redis

    public String getLayoutTypeFromRedis( String restId) {

        String hashValue1 = redisHelper.getHashValue( "sandredisstage", 0,  "RESTAURANT_MERCHANDISE_LAYOUT", "RESTAURANT_"+restId);
        return hashValue1;
    }


    public HashMap<String, String> getMerchLayoutMappingConfigFromRedis( String layoutType) {

        HashMap<String, String> hashValue1 = redisHelper.hKeysValues( "sandredisstage", 0,  "MERCHANDISE_LAYOUT_CONFIG_KEY_"+layoutType);
        return hashValue1;
    }

    public HashMap<String, String> getSectionConfigFromRedis() {

        HashMap<String, String> hashValue1 = redisHelper.hKeysValues( "sandredisstage", 0,  "MERCHANDISE_SECTION_CONFIG");
        return hashValue1;
    }

    public String getCarouselFromRedis( String restId, String bannerId) throws JSONException {

        String hashValue1 = redisHelper.getHashValue( "sandredisstage", 0,  "RESTAURANT_CAROUSELS_HKEY_"+restId, "ITEM_CAROUSEL_BANNER_"+bannerId);
        return hashValue1;
    }

    //helper to set redis values

    public void setLayoutInRedis(String layoutType, String restId){

        String value="";
        if (layoutType == "high_touch") {
            value = "{\"restaurantId\":" + restId +",\"layoutType\":\"high_touch\",\"updatedOn\":1536920222469,\"updatedBy\":\"srishty\"}";
        }
        else if (layoutType== "low_touch" ){
            value = "{\"restaurantId\":" + restId +",\"layoutType\":\"low_touch\",\"updatedOn\":1536920222469,\"updatedBy\":\"srishty\"}";
        }
        else if (layoutType== "normal" ){
            value = "{\"restaurantId\":" + restId +",\"layoutType\":\"normal\",\"updatedOn\":1536920222469,\"updatedBy\":\"srishty\"}";
        }
        redisHelper.setHashValue("sandredisstage", 0, "RESTAURANT_MERCHANDISE_LAYOUT", "RESTAURANT_"+restId, value);
    }


    public void setSectionsForHighTouch(){

        String sectionConfig0 = "" ;
        String sectionConfig1 = "" ;
        String layoutType= "high_touch";

        if (layoutType == "high_touch") {
            sectionConfig0 = "{\"id\":0,\"name\":\"menuCarousels\",\"title\":\"Top Items\",\"min\":1,\"max\":6,\"priority\":1,\"enabled\":true}";
            sectionConfig1 = "{\"id\":1,\"name\":\"collectionsV2\",\"title\":\"Specials\",\"min\":1,\"max\":6,\"priority\":1,\"enabled\":true}";
        }

        redisHelper.setHashValue("sandredisstage", 0,"MERCHANDISE_SECTION_CONFIG","SECTION_0",sectionConfig0);
        redisHelper.setHashValue("sandredisstage", 0,"MERCHANDISE_SECTION_CONFIG","SECTION_1",sectionConfig1);

        String sectionLayoutMapping0 = "";
        String sectionLayoutMapping1 = "";


        if (layoutType == "high_touch"){
            sectionLayoutMapping0 = "{\"id\":0,\"name\":\"high_touch\",\"sectionId\":0}";
            sectionLayoutMapping1 = "{\"id\":1,\"name\":\"high_touch\",\"sectionId\":1}";
        }

        redisHelper.setHashValue("sandredisstage", 0, "MERCHANDISE_LAYOUT_CONFIG_KEY_"+ layoutType, "LAYOUT_MAPPING_ID_0", sectionLayoutMapping0);
        redisHelper.setHashValue("sandredisstage", 0, "MERCHANDISE_LAYOUT_CONFIG_KEY_"+ layoutType, "LAYOUT_MAPPING_ID_1", sectionLayoutMapping1);
    }

    public void addNewSectionConfig(int sectionId){

        String sectionConfig = "" ;
        String sectionLayoutMapping = "";
        String layoutType= "high_touch";

        if (layoutType == "high_touch") {
            sectionConfig = "{\"id\":" + sectionId + ",\"name\":\"test-section" + sectionId + "\",\"title\":\"Top Items\",\"min\":1,\"max\":6,\"priority\":1,\"enabled\":true}";
            redisHelper.setHashValue("sandredisstage", 0, "MERCHANDISE_SECTION_CONFIG", "SECTION_"+sectionId, sectionConfig);

            sectionLayoutMapping = "{\"id\":" + sectionId + ",\"name\":\"high_touch\",\"sectionId\":" + sectionId + "}";
            redisHelper.setHashValue("sandredisstage", 0, "MERCHANDISE_LAYOUT_CONFIG_KEY_" + layoutType, "LAYOUT_MAPPING_ID_" + sectionId, sectionLayoutMapping);
        }

    }

    public void updateSectionPriority(int sectionId){

        String sectionConfig = "" ;
        String sectionLayoutMapping = "";
        String layoutType= "high_touch";

        if (layoutType == "high_touch") {

            sectionConfig = "{\"id\":" + sectionId + ",\"name\":\"test-section" + sectionId + "\",\"title\":\"Top Items\",\"min\":1,\"max\":6,\"priority\":1,\"enabled\":true}";
            redisHelper.setHashValue("sandredisstage", 0, "MERCHANDISE_SECTION_CONFIG", "SECTION_"+sectionId, sectionConfig);

            sectionLayoutMapping = "{\"id\":" + sectionId + ",\"name\":\"high_touch\",\"sectionId\":" + sectionId + "}";
            redisHelper.setHashValue("sandredisstage", 0, "MERCHANDISE_LAYOUT_CONFIG_KEY_" + layoutType, "LAYOUT_MAPPING_ID_" + sectionId, sectionLayoutMapping);
        }

    }

    public String readSectionPriority(int sectionId) throws JSONException {

        HashMap<String, String> map = new HashMap<String, String>();
        sectionId = sectionId-1;
        String hashValue = redisHelper.getHashValue("sandredisstage",0,"MERCHANDISE_SECTION_CONFIG","SECTION_"+sectionId);

        JSONObject jsonObject = new JSONObject(hashValue);
        Iterator<?> keys = jsonObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jsonObject.getString(key);
            map.put(key, value);

        }

        String priority = map.get("priority");
        return priority;

    }

    public int getCurrentDay(){
            return Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    }

    public String buildStartTime(){

        LocalDate localDate = LocalDate.now().minusDays(10L);
        Date date = new Date( localDate.atStartOfDay(ZoneId.of("Asia/Kolkata")).toEpochSecond() * 1000);
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String startTime = format.format(date);
        return startTime;
    }

    public String buildEndTime() {

        LocalDate localDate = LocalDate.now().plusDays(10L);
        Date date = new Date( localDate.atStartOfDay(ZoneId.of("Asia/Kolkata")).toEpochSecond() * 1000);
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String endTime = format.format(date);
        return endTime;
    }

    public void errorCodeHandler(Processor processor, String api) {
        if (processor.ResponseValidator.GetResponseCode() == 500) {
            Assert.assertTrue(false, "Api is down=" + api);
        }
    }

    /*
    carousel create helper
    * */
    public Processor createCarousel(CarouselPOJO carouselPOJO) throws IOException {
        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        hashmap.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
        GameOfThronesService got = new GameOfThronesService("sand", "carouselbannerpojo", gameofthrones);
        Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(carouselPOJO)});
        errorCodeHandler(processor, "carousel API");
        return processor;
    }

    /* delete high touch
     */
    public void deleteHighTouchFromRedis(String restId){
        redisHelper.deleteKeyFromHashMap("sandredisstage", 0,  "RESTAURANT_MERCHANDISE_LAYOUT", "RESTAURANT_"+restId);
    }

    /* delete carousel
     */
    public void deleteCarouselFromRedis(String restId,String bannerId){
        redisHelper.deleteKeyFromHashMap("sandredisstage", 0,  "RESTAURANT_CAROUSEL_HKEY_"+restId, bannerId);
    }

    /*
    delete all carousels
     */
    public void deleteAllCarousels(String restId){
        redisHelper.deleteKey("sandredisstage", 0,"RESTAURANT_CAROUSELS_HKEY_"+restId);
    }

    /* delete section
     */
    public void deleteSectionFromRedis(String id){
        redisHelper.deleteKeyFromHashMap("sandredisstage", 0,  "MERCHANDISE_SECTION_CONFIG", "SECTION_"+id);
    }

    /* delete all sections
     */
    public void deleteAllSections(){
        HashMap<String, String> sectionConfigFromRedis = getSectionConfigFromRedis();
        int size = sectionConfigFromRedis.size();
        for (int i=0;i<size;i++)
        {
            deleteSectionFromRedis(String.valueOf(i));
        }
    }

    /* get value for section
     */
    public String getValuefromRedisForGivenSection(int sectionId) throws JSONException {
        HashMap<String, String> sectionConfigFromRedis = getSectionConfigFromRedis();
        String sectionValue = sectionConfigFromRedis.get("SECTION_" + sectionId);
        return sectionValue;
    }

    public void createMinimumCarousels(CarouselPOJO carouselPOJO) throws IOException {
        Integer restId = carouselPOJO.getRestaurantId();
        Processor merchandisingInfoResp = getMerchandisingInfo(restId.toString());
        int min = Integer.valueOf(merchandisingInfoResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.sectionConfigList[?(@.name=='menuCarousels')].min").replace("]","").replace("[","").replace("\"",""));
        for (int i=1; i<=min;i++){
            Processor carousel = createCarousel(carouselPOJO);
            int bannerId = carousel.ResponseValidator.GetNodeValueAsInt("$.data");
            System.out.println("bannerID ==========="+ bannerId);
        }
    }

    public void createMaxCarousels(CarouselPOJO carouselPOJO) throws IOException {
        Integer restId = carouselPOJO.getRestaurantId();
        Processor merchandisingInfoResp = getMerchandisingInfo(restId.toString());
        int max = Integer.valueOf(merchandisingInfoResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.sectionConfigList[?(@.name=='menuCarousels')].max").replace("]","").replace("[","").replace("\"",""));
        for (int i=1;i<=max;i++){
            Processor carousel = createCarousel(carouselPOJO);
            int bannerId = carousel.ResponseValidator.GetNodeValueAsInt("$.data");
            System.out.println("bannerID ==========="+ bannerId);
        }
    }

    public int getOpenTime(){
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR, 2);
        return Integer.parseInt(new SimpleDateFormat("HHmm").format(now.getTime()));
    }

    public Processor sectionConfig(String [] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sandpresentational", "sectionconfig", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }


    public Processor readLayoutType(String [] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sandpresentational", "readlayouttype", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor setLayoutType(String [] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sandpresentational", "setLayoutType", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor readSectionConfigAsPerLayoutType(String layoutType) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sandpresentational", "sectionconfiglayouttype", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null,new String [] {layoutType});
        return processor;
    }

    public Processor disableLayout(String layoutType, String sectionId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sandpresentational", "disablesectionconfig", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null,new String [] {layoutType,sectionId});
        return processor;
    }

    public String getMerchLayoutMappingFromRedis( String layoutType,String sectionId) {
        String hashValue1 = redisHelper.getHashValue( "sandredisstage", 0,  "MERCHANDISE_LAYOUT_CONFIG_KEY_"+layoutType,"LAYOUT_MAPPING_ID_" + sectionId);
        return hashValue1;
    }

    public Processor layoutRefresh(String [] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sandpresentational", "layoutrefresh", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor refreshSectionConfigAsPerLayoutType(String layoutType) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sandpresentational", "refreshsectionconfig", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null,new String [] {layoutType});
        return processor;
    }


    public void setRestLayout(String restId, String layout_type,String updatedBy, String updatedOn) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update("insert into menumerch.restaurant_layout values ("+restId +",'"+ layout_type  + "','" + updatedBy + "',"+ updatedOn + ")");
       // sqlTemplate.update("insert into menumerch.restaurant_layout values ("+restId +","+ layout_type  + "," + updatedBy + ")");

    }

    public String getMerchSectionConfigFromRedis( String sectionId) {
        String hashValue1 = redisHelper.getHashValue( "sandredisstage", 0,  "MERCHANDISE_SECTION_CONFIG","SECTION_" + sectionId);
        System.out.println(hashValue1);
        return hashValue1;
    }

    public void setSectionConfig(String id,String name, String title, String min, String max, String priority, String enabled) {

       // sqlTemplate.update("insert into menumerch.merchandise_section_config values ("+id +",'"+ name  + "','" + title +  "','" + min+  "','" + max +  "','" + priority +  "','" + enabled + "')");
       String ins= "INSERT INTO `menumerch`.`merchandise_section_config` (`id`, `name`, `title`, `min`, `max`, `priority`, `enabled`) VALUES("+id +",'"+ name  + "','" + title +  "','" + min+  "','" + max +  "','" + priority +  "','" + enabled + "')";
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
       // sqlTemplate.execute(CmsConstants.insert_wp_options + id + "','" + name + "'," + "'" + title + "','" + min + "','" + max + "','" + priority + "','" + enabled + CmsConstants.wp_options_end);
       // System.out.println(ins);
        sqlTemplate.execute(ins);
    }

}
