package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DataTag {

    private String tag;

    /**
     * No args constructor for use in serialization
     *
     */
    public DataTag() {
    }

    /**
     *
     * @param tag
     */
    public DataTag(String tag) {
        super();
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("tag", tag).toString();
    }

}