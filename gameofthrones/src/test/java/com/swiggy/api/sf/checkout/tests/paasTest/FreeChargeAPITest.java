package com.swiggy.api.sf.checkout.tests.paasTest;

import com.swiggy.api.sf.checkout.constants.PaasConstant;
import com.swiggy.api.sf.checkout.helper.paas.MySmsValidator;
import com.swiggy.api.sf.checkout.helper.paas.PaasWalletValidator;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;

public class FreeChargeAPITest {

    PaasWalletValidator paasWalletValidator = new PaasWalletValidator();
    MySmsValidator mySmsValidator = new MySmsValidator();
    SoftAssert softAssertion= new SoftAssert();

    String mobile = System.getenv("mobile");
    String password = System.getenv("password");

    String tid;
    String token;
    String otpId;
    String authToken;

    @BeforeClass
    public void login() {
        if (mobile == null || mobile.isEmpty()) {
            mobile = PaasConstant.MOBILE;
        }
        if (password == null || password.isEmpty()) {
            password = PaasConstant.PASSWORD;
        }

        HashMap<String, String> hashMap = paasWalletValidator.loginTokenData(mobile, password);
        tid = hashMap.get("tid");
        token = hashMap.get("token");
        authToken = mySmsValidator.getAuthToken();
    }

    @Test(priority = 1)
    public void invokeFreeChargeLinkApi() {

        int statusCode = paasWalletValidator.callfreechargeCheckBalance(tid, token);

        if (statusCode != 0) {
            mySmsValidator.deleteAllConversation(authToken);

            Processor processor = paasWalletValidator.validatefreechargeLink(tid,token);
            otpId =processor.ResponseValidator.GetNodeValue("$.data.otpId");
            Assert.assertNotNull(otpId,"OTP-ID is null in FreeChargeLinkApi response");

        } else {
            paasWalletValidator.validatefreechargeDelink(tid, token);
            invokeFreeChargeLinkApi();
        }
    }

    @Test (dependsOnMethods="invokeFreeChargeLinkApi",priority = 2)
    public void invokeFreeChargeValidateOTPApi() {

        String otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.FREECHARGE_ADDRESS);
        int retry = 0;

        while (otp==null && retry < PaasConstant.MAX_RETRY){
            paasWalletValidator.validateFreechargeResendOtp(tid,token,otpId);
            otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.FREECHARGE_ADDRESS);
            retry++;
        }
        paasWalletValidator.validatefreechargeOtp(tid, token, otpId,otp);
    }


    @Test (dependsOnMethods="invokeFreeChargeValidateOTPApi",priority = 3)
    public void invokeFreeChargeCheckBalanceApi() {
        paasWalletValidator.validatefreechargeCheckBalance(tid, token);
    }


    @Test (dependsOnMethods="invokeFreeChargeValidateOTPApi",priority = 4)
    public void invokeFreechargeHasTokenApi() {
        paasWalletValidator.validatefreechargeHasToken(tid, token);
    }

    @Test (dependsOnMethods="invokeFreeChargeValidateOTPApi",priority = 5)
    public void invokeFreeChargeDeLinkApi() {
        paasWalletValidator.validatefreechargeDelink(tid, token);
    }



}