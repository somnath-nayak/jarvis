package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "userId",
        "swiggyMoney",
        "firstOrder"
})
public class CartUserPOJO {

    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("swiggyMoney")
    private Integer swiggyMoney;
    @JsonProperty("firstOrder")
    private Boolean firstOrder;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public CartUserPOJO() {
    }

    /**
     *
     * @param firstOrder
     * @param userId
     * @param swiggyMoney
     */
    public CartUserPOJO(Integer userId, Integer swiggyMoney, Boolean firstOrder) {
        super();
        this.userId = userId;
        this.swiggyMoney = swiggyMoney;
        this.firstOrder = firstOrder;
    }

    @JsonProperty("userId")
    public Integer getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public CartUserPOJO withUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    @JsonProperty("swiggyMoney")
    public Integer getSwiggyMoney() {
        return swiggyMoney;
    }

    @JsonProperty("swiggyMoney")
    public void setSwiggyMoney(Integer swiggyMoney) {
        this.swiggyMoney = swiggyMoney;
    }

    public CartUserPOJO withSwiggyMoney(Integer swiggyMoney) {
        this.swiggyMoney = swiggyMoney;
        return this;
    }

    @JsonProperty("firstOrder")
    public Boolean getFirstOrder() {
        return firstOrder;
    }

    @JsonProperty("firstOrder")
    public void setFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
    }

    public CartUserPOJO withFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CartUserPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public CartUserPOJO setDefault() {
        return this.withFirstOrder(true)
                .withSwiggyMoney(0)
                .withUserId(6240342);
    }

}
