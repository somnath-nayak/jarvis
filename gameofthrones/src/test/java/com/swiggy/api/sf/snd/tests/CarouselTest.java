package com.swiggy.api.sf.snd.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import com.swiggy.api.sf.snd.dp.CarouselDP;
import com.swiggy.api.sf.snd.helper.CarouselHelper;
import com.swiggy.api.sf.snd.helper.MenuMerchandisingHelper;
import com.swiggy.api.sf.snd.pojo.UpdateCarousel.UpdateCarouselPOJO;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.RedisHelper;
import net.minidev.json.JSONArray;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class CarouselTest extends CarouselDP {

    CarouselHelper carouselHelper = new CarouselHelper();
    MenuMerchandisingHelper menuMerchandisingHelper = new MenuMerchandisingHelper();
    RedisHelper redisHelper = new RedisHelper();
    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();

    @Test
    public void getAllCarouselJob()  {

        Processor processor = carouselHelper.getAllCarouselJob();
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
    }

    @Test(dataProvider="createCarouselWithBulkPayload")
    public void getCarouselJobByValidId(ArrayList<CarouselPOJO> carouselArray) throws IOException {

        Processor processorCreateCarousel = carouselHelper.createCarouselWithBulkPayload(carouselArray);
        String jobId = processorCreateCarousel.ResponseValidator.GetNodeValue("$.data");

        Processor processor = carouselHelper.getCarouselJobById(jobId);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

        String deletedJobId = processor.ResponseValidator.GetNodeValue("$.data.jobId");
        assertEquals(deletedJobId, jobId);

        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String expectedStatusMessage = "done successfully";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);

        String resp = processor.ResponseValidator.GetBodyAsText();
        boolean jobIdExists = processor.ResponseValidator.DoesNodeExists(resp,"$.data..jobId");
        boolean bulkBannerResponses = processor.ResponseValidator.DoesNodeExists(resp,"$.data..bulkBannerResponses");

        boolean jobStatus = processor.ResponseValidator.DoesNodeExists(resp,"$.data..jobStatus");
        boolean exceptionMessage =processor.ResponseValidator.DoesNodeExists(resp,"$.data..exceptionMessage");
        boolean percentageCompleted = processor.ResponseValidator.DoesNodeExists(resp,"$.data..percentageCompleted");

        assertEquals(jobIdExists,true);
        assertEquals(bulkBannerResponses,true);
        assertEquals(jobStatus,true);
        assertEquals(exceptionMessage,true);
        assertEquals(percentageCompleted,true);

    }

    @Test(dataProvider="createCarouselWithBulkPayload")
    public void getCarouselJobByInvalidId(ArrayList<CarouselPOJO> carouselArray) throws IOException {

        Processor processorCreateCarousel = carouselHelper.createCarouselWithBulkPayload(carouselArray);
        String jobId = processorCreateCarousel.ResponseValidator.GetNodeValue("$.data");

        String invalidJobId = jobId+"invalid";
        Processor processor = carouselHelper.getCarouselJobById(invalidJobId);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

        String deletedJobId = processor.ResponseValidator.GetNodeValue("$.data");
        assertEquals(deletedJobId, null);

        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String expectedStatusMessage = "done successfully";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);

    }

    @Test(dataProvider = "getCarouselByPage")
    public void getCarouselByPage(String pageNumber, String pageSize, String type) {

        Processor processor = carouselHelper.getCarouselByPage(pageNumber,pageSize,type);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
        JSONArray typeArray = processor.ResponseValidator.GetNodeValueAsJsonArray("$.data..type");
        int size = typeArray.size();

        for (int i =0;i<size;i++){
            String typeAPI = (String) typeArray.get(i);
            assertEquals(typeAPI,type);
        }

        assertEquals(String.valueOf(size),pageSize);
    }

    //@Test(dataProvider = "getCarouselAll") Deprecated
    public void getAllCarousel(String type)  {

        Processor processor = carouselHelper.getAllCarousel(type);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
    }

    @Test(dataProvider = "refreshRest")
    public void refreshCache(String restId){
        HashMap<String,String> hashMap = new HashMap<>();
        Processor processor = carouselHelper.refreshCacheForRestId(restId);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
        hashMap = redisHelper.hKeysValues("sandredisstage", 0,"RESTAURANT_CAROUSELS_HKEY_"+restId);

        hashMap.clear();
        menuMerchandisingHelper.deleteAllCarousels(restId);
        hashMap = redisHelper.hKeysValues("sandredisstage", 0,"RESTAURANT_CAROUSELS_HKEY_"+restId);
        boolean empty = hashMap.isEmpty();
        assertEquals(empty,true);

        Processor processor2 = carouselHelper.refreshCacheForRestId(restId);
        hashMap = redisHelper.hKeysValues("sandredisstage", 0,"RESTAURANT_CAROUSELS_HKEY_"+restId);
        boolean empty2 = hashMap.isEmpty();
        assertEquals(empty2,false);

    }

    @Test(dataProvider="createCarouselWithBulkPayload")
    public void deleteCarouselJobById(ArrayList<CarouselPOJO> carouselArray) throws IOException {

        Processor processorCreateCarousel = carouselHelper.createCarouselWithBulkPayload(carouselArray);
        String jobId = processorCreateCarousel.ResponseValidator.GetNodeValue("$.data");
        Processor processor = carouselHelper.deleteCarouselJobById(jobId);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
        String deletedJobId = processor.ResponseValidator.GetNodeValue("$.data.jobId");
        assertEquals(deletedJobId, jobId);
        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String expectedStatusMessage = "done successfully";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);
        String resp = processor.ResponseValidator.GetBodyAsText();
        boolean jobIdExists = processor.ResponseValidator.DoesNodeExists(resp,"$.data..jobId");
        boolean bulkBannerResponses = processor.ResponseValidator.DoesNodeExists(resp,"$.data..bulkBannerResponses");

        boolean jobStatus = processor.ResponseValidator.DoesNodeExists(resp,"$.data..jobStatus");
        boolean exceptionMessage =processor.ResponseValidator.DoesNodeExists(resp,"$.data..exceptionMessage");
        boolean percentageCompleted = processor.ResponseValidator.DoesNodeExists(resp,"$.data..percentageCompleted");

        assertEquals(jobIdExists,true);
        assertEquals(bulkBannerResponses,true);
        assertEquals(jobStatus,true);
        assertEquals(exceptionMessage,true);
        assertEquals(percentageCompleted,true);

    }

    @Test(dataProvider="createCarouselWithBulkPayload")
    public void deleteCarouselJobByInvalidId(ArrayList<CarouselPOJO> carouselArray) throws IOException {

        Processor processorCreateCarousel = carouselHelper.createCarouselWithBulkPayload(carouselArray);
        String jobId = processorCreateCarousel.ResponseValidator.GetNodeValue("$.data");
        String invalidJobId = jobId+"invalid";
        Processor processor = carouselHelper.deleteCarouselJobById(invalidJobId);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
        boolean deletedJobId = processor.ResponseValidator.DoesNodeExists("$.data.jobId");
        assertEquals(deletedJobId,false );
        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String expectedStatusMessage = "done successfully";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);

    }

    @Test(dataProvider = "createCarouselValid")
    public void createCarousel(CarouselPOJO carouselPOJO) throws IOException {

        Processor processor = carouselHelper.createCarousel(carouselPOJO);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
    }

    @Test(dataProvider="createCarouselWithBulkPayload")
    public void createCarouselWithBulkPayload(ArrayList<CarouselPOJO> carouselArray) throws IOException {

        Processor processor = carouselHelper.createCarouselWithBulkPayload(carouselArray);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

    }

    @Test(dataProvider = "createCarouselInvalidTD")
    public void createCarouselInvalidTD(CarouselPOJO carouselPOJO) throws IOException {

        Integer restaurantId = carouselPOJO.getRestaurantId();
        String expectedStatusMessage = "Associated with invalid campaign ids";
        Processor processor = carouselHelper.createCarousel(carouselPOJO);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);
        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,1);
        String dataAPI = processor.ResponseValidator.GetNodeValue("$.data");
        assertNull(dataAPI);

    }

    @Test(dataProvider = "createCarouselInvalidRestItemMapping")
    public void createCarouselInvalidRestItemMapping(CarouselPOJO carouselPOJO) throws IOException {

        Integer restaurantId = carouselPOJO.getRestaurantId();
        String expectedStatusMessage = "Invalid restaurant id: "+restaurantId.toString();
        Processor processor = carouselHelper.createCarousel(carouselPOJO);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);
        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,1);
        String dataAPI = processor.ResponseValidator.GetNodeValue("$.data");
        assertNull(dataAPI);

    }

    @Test(dataProvider = "getCarouselValidId")
    public void getCarouselByValidId(String bannerId) {

        Processor processor = carouselHelper.getCarouselById(bannerId);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
        int bannerIdAPI = processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        assertEquals(String.valueOf(bannerIdAPI),bannerId);
        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
    }

    @Test(dataProvider = "getCarouselInvalidId")
    public void getCarouselByInvalidId(String bannerId) {

        String expectedStatusMessage = "Banner not found.";
        Processor processor = carouselHelper.getCarouselById(bannerId);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,1);
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);

    }

    @Test(dataProvider = "createCarouselValid")
    public void verifyUpdateCarousel(CarouselPOJO carouselPOJO) throws IOException {

        String expectedStatusMessage = "Banner updated succesfully";
        UpdateCarouselPOJO updateCarouselPOJO = new UpdateCarouselPOJO();
        Processor processor = carouselHelper.createCarousel(carouselPOJO);
        int bannerIdAPI = processor.ResponseValidator.GetNodeValueAsInt("$.data");

        /* Update enabled
         */

        Processor updateEnabled = carouselHelper.updateCarousel(updateCarouselPOJO.setDefault().withEnabled(false).withId(bannerIdAPI));
        int apiStatusCode = updateEnabled.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String response = updateEnabled.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);
        String dataAPI = updateEnabled.ResponseValidator.GetNodeValue("$.data");
        assertNull(dataAPI);

        Processor getCarouselResp = carouselHelper.getCarouselById(String.valueOf(bannerIdAPI));
        String enabled = getCarouselResp.ResponseValidator.GetNodeValueAsJsonArray("$.data..enabled").get(0).toString();
        assertEquals(enabled,"0");

    }

    @Test(dataProvider = "createCarouselValid")
    public void verifyUpdateCarouselForPriority(CarouselPOJO carouselPOJO) throws IOException {

        String expectedStatusMessage = "Banner updated succesfully";
        UpdateCarouselPOJO updateCarouselPOJO = new UpdateCarouselPOJO();
        Processor processor = carouselHelper.createCarousel(carouselPOJO);
        int bannerIdAPI = processor.ResponseValidator.GetNodeValueAsInt("$.data");

        /* Update priority
         */

        Processor updatePriority = carouselHelper.updateCarousel(updateCarouselPOJO.setDefault().withId(bannerIdAPI).withPriority("23"));
        int apiStatusCode = updatePriority.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String response = updatePriority.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);
        String dataAPI = updatePriority.ResponseValidator.GetNodeValue("$.data");
        assertNull(dataAPI);

        Processor getCarouselResp = carouselHelper.getCarouselById(String.valueOf(bannerIdAPI));
        String priority = getCarouselResp.ResponseValidator.GetNodeValueAsJsonArray("$.data..priority").get(0).toString();
        assertEquals(priority,"23");

        /* Update updatedBy
         */

        Processor updateUpdatedBy = carouselHelper.updateCarousel(updateCarouselPOJO.withUpdatedBy("test-Update-Carousel"));
        Processor getCarouselResp2 = carouselHelper.getCarouselById(String.valueOf(bannerIdAPI));
        String updatedBy = getCarouselResp2.ResponseValidator.GetNodeValueAsJsonArray("$.data..updatedBy").get(0).toString();
        assertEquals(updatedBy,"test-Update-Carousel");
    }

    @Test(dataProvider = "createCarouselWithBulkCSVInvalid")
    public void createCarouselWithBulkCSV(String filepath) throws JSONException {

        Processor processor = carouselHelper.createCarouselWithBulkCSV(filepath);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

        String jobId = processor.ResponseValidator.GetNodeValue("$.data");


        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String expectedStatusMessage = "Banners job successfully created!";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);

//        getjobcarouselByID
        Processor getCarouselJobByIdResponse = carouselHelper.getCarouselJobById(jobId);
        String getJobId = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..jobId").get(0).toString();
        assertEquals(getJobId, jobId);

        String getJobStatus = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..jobStatus").get(0).toString();
        assertEquals(getJobStatus, "FAILED");
    }

    @Test(dataProvider = "createCarouselWithBulkCSVValid")
    public void createCarouselWithBulkCSVValid(String filepath) throws JSONException {

        Processor processor = carouselHelper.createCarouselWithBulkCSV(filepath);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

        String jobId = processor.ResponseValidator.GetNodeValue("$.data");


        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String expectedStatusMessage = "Banners job successfully created!";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);

//        getjobcarouselByID
        Processor getCarouselJobByIdResponse = carouselHelper.getCarouselJobById(jobId);
        String getJobId = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..jobId").get(0).toString();
        assertEquals(getJobId, jobId);

        String getJobStatus = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..jobStatus").get(0).toString();
        assertEquals(getJobStatus, "COMPLETED");

        String percentage = String.valueOf(getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsInt("$.data.percentageCompleted"));
        assertEquals(percentage, "100");

        String bannerId = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..bulkBannerResponses..bannerId").get(0).toString();
        String redirectLink = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..bulkBannerResponses..redirectLink").get(0).toString();
        String created = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..bulkBannerResponses..created").get(0).toString();
        assertEquals(created,"true");

        Processor getCarouselByIdResponse = carouselHelper.getCarouselById(bannerId);
        String carouselId = getCarouselByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..id").get(0).toString();
        assertEquals(carouselId,bannerId);

        String redirectLinkActual = getCarouselByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..redirectLink").get(0).toString();
        assertEquals(redirectLinkActual,redirectLink);

    }

    @Test(dataProvider = "createCarouselWithBulkCSVEmpty")
    public void createCarouselWithBulkCSVEmpty(String filepath) throws JSONException {

        Processor processor = carouselHelper.createCarouselWithBulkCSV(filepath);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

        String jobId = processor.ResponseValidator.GetNodeValue("$.data");

        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,2);
        String expectedStatusMessage = "At least one banner is expected";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);
    }

    @Test(dataProvider = "createCarouselWithBulkCSVInvalidFormatOfData")
    public void createCarouselWithBulkCSVInvalidFormatOfData(String filepath) throws JSONException {

        Processor processor = carouselHelper.createCarouselWithBulkCSV(filepath);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

        String jobId = processor.ResponseValidator.GetNodeValue("$.data");
        assertEquals(jobId,null);

        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,1);
        String expectedStatusMessage = "Invalid file received. Could not parse: For input string: \"jhdjhjhj\"";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);
    }

    @Test(dataProvider = "createCarouselWithBulkCSVInvalidFileFormat")
    public void createCarouselWithBulkCSVInvalidFileFormat(String filepath) throws JSONException {

        Processor processor = carouselHelper.createCarouselWithBulkCSV(filepath);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

        String jobId = processor.ResponseValidator.GetNodeValue("$.data");
        assertEquals(jobId,null);

        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,1);
    }


    @Test(dataProvider = "createCarouselWithBulkCSVInvalidTDMapped")
    public void createCarouselWithBulkCSVInvalidTDMapped(String filepath) throws JSONException {

        Processor processor = carouselHelper.createCarouselWithBulkCSV(filepath);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

        String jobId = processor.ResponseValidator.GetNodeValue("$.data");

        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String expectedStatusMessage = "Banners job successfully created!";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);

//        getjobcarouselByID
        Processor getCarouselJobByIdResponse = carouselHelper.getCarouselJobById(jobId);
        String getJobId = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..jobId").get(0).toString();
        assertEquals(getJobId, jobId);

        String getJobStatus = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..jobStatus").get(0).toString();
        assertEquals(getJobStatus, "FAILED");
        String expectedMessage = "Associated with invalid campaign ids";
        String message = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..bulkBannerResponses..message").get(0).toString();
        assertEquals(message,expectedMessage);

        String created = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..bulkBannerResponses..created").get(0).toString();
        assertEquals(created,"false");
    }

    @Test(dataProvider = "createCarouselWithBulkCSVInvalidItemRestMapping")
    public void createCarouselWithBulkCSVInvalidItemRestMapping(String filepath) throws JSONException {

        Processor processor = carouselHelper.createCarouselWithBulkCSV(filepath);
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);

        String jobId = processor.ResponseValidator.GetNodeValue("$.data");

        int apiStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        assertEquals(apiStatusCode,0);
        String expectedStatusMessage = "Banners job successfully created!";
        String response = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(response,expectedStatusMessage);

//        getjobcarouselByID
        Processor getCarouselJobByIdResponse = carouselHelper.getCarouselJobById(jobId);
        String getJobId = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..jobId").get(0).toString();
        assertEquals(getJobId, jobId);

        String getJobStatus = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..jobStatus").get(0).toString();
        assertEquals(getJobStatus, "FAILED");
        String expectedMessage = "Invalid restaurant id: 227";
        String message = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..bulkBannerResponses..message").get(0).toString();
        assertEquals(message,expectedMessage);

        String created = getCarouselJobByIdResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data..bulkBannerResponses..created").get(0).toString();
        assertEquals(created,"false");
    }

    @Test(dataProvider = "deleteCarouselBulkValid")
    public void deleteCarouselBulk (ArrayList<Integer> bannerIds) throws IOException {
        Processor processor = carouselHelper.disableCarouselBulk(bannerIds);
        int responseCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(responseCode,200);

        String disabledBannerId = bannerIds.get(0).toString();
        Processor getCarouselByIdResp = carouselHelper.getCarouselById(disabledBannerId);
        int enabledStatus = getCarouselByIdResp.ResponseValidator.GetNodeValueAsInt("$.data.enabled");
        assertEquals(enabledStatus,0);

        String disabledStatus = processor.ResponseValidator.GetNodeValueAsJsonArray("$.data[0]..disabled").get(0).toString();
        assertEquals(disabledStatus,"true");
    }

    @Test(dataProvider = "deleteCarouselBulkInvalid")
    public void deleteCarouselBulkInvalid(ArrayList<Integer> bannerIds) throws IOException {
        Processor processor = carouselHelper.disableCarouselBulk(bannerIds);
        int responseCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(responseCode,200);

        String disabledBannerId = bannerIds.get(0).toString();
        Processor getCarouselByIdResp = carouselHelper.getCarouselById(disabledBannerId);
        String expectedBannerMessage = "Banner not found.";
        String bannerMessage = getCarouselByIdResp.ResponseValidator.GetNodeValue("$.statusMessage");
        assertEquals(bannerMessage,expectedBannerMessage);

        String disabledStatus = processor.ResponseValidator.GetNodeValueAsJsonArray("$.data[0]..disabled").get(0).toString();
        assertEquals(disabledStatus,"false");
    }

    /* JSONSCHEMA validations
    * */

    @Test (dataProvider = "getCarouselValidId")
    public void getCarouselJSONSchemavalidation(String bannerId) throws IOException, ProcessingException, InterruptedException {

        Processor processor = carouselHelper.getCarouselById(bannerId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/getCarouselById.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For getCarouselByID API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test (dataProvider = "deleteCarouselBulkValid")
    public void deleteCarouselBulkJSONSchemavalidation(ArrayList<Integer> bannerIds) throws IOException, ProcessingException {

        Processor processor = carouselHelper.disableCarouselBulk(bannerIds);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/disableCarouselBulk.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For deleteCarouselBulk API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test (dataProvider = "getCarouselByPage")
    public void getCarouselByPageJSONSchemavalidation(String pageNumber, String pageSize, String type)throws IOException, ProcessingException {

        Processor processor = carouselHelper.getCarouselByPage( pageNumber,  pageSize,  type);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/getCarouselByPage.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For getCarouselByPage API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "createCarouselWithBulkCSVValid")
    public void createCarouselWithBulkCSVValidJSONSchemavalidation(String filepath) throws IOException, ProcessingException, JSONException {

        Processor processor = carouselHelper.createCarouselWithBulkCSV(filepath);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/createCarouselWithBulkCSV.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For createCarouselWithBulkCSVValid API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "refreshRest")
    public void refreshCacheJSONSchemaValidation(String restId)throws IOException, ProcessingException, JSONException {

        Processor processor = carouselHelper.refreshCacheForRestId(restId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/refreshCarouselData.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For refreshCarouselData API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider="createCarouselWithBulkPayload")
    public void getCarouselJobByValidIdJSONSchemaValidation(ArrayList<CarouselPOJO> carouselArray) throws IOException,ProcessingException {

        Processor processorCreateCarousel = carouselHelper.createCarouselWithBulkPayload(carouselArray);
        String jobId = processorCreateCarousel.ResponseValidator.GetNodeValue("$.data");

        Processor processor = carouselHelper.getCarouselJobById(jobId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/getCarouselJobById.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For getCarouselJobById API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test
    public void getAllCarouselJobJSONSchemaValidation() throws IOException,ProcessingException {

        Processor processor = carouselHelper.getAllCarouselJob();
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/getCarouselJobAll.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For getCarouselJobAll API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider="createCarouselWithBulkPayload")
    public void deleteCarouselJobByIdJSONSchemaValidation(ArrayList<CarouselPOJO> carouselArray) throws IOException,ProcessingException {

        Processor processorCreateCarousel = carouselHelper.createCarouselWithBulkPayload(carouselArray);
        String jobId = processorCreateCarousel.ResponseValidator.GetNodeValue("$.data");
        Processor processor = carouselHelper.deleteCarouselJobById(jobId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/deleteCarouselJobById.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For deleteCarouselJobById API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

}
