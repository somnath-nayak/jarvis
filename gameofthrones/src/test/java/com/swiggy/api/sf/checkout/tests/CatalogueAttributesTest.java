package com.swiggy.api.sf.checkout.tests;

import com.swiggy.api.sf.checkout.constants.CatalogueAttributesConstants;
import com.swiggy.api.sf.checkout.dp.CatalogueAttributesDP;
import com.swiggy.api.sf.checkout.helper.CatalogueAttributesHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.getLastOrderResponse.GetLastOrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.getSingleOrderResponse.GetSingleOrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.OrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.checkout.helper.rts.validator.GenericValidator;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class CatalogueAttributesTest extends CatalogueAttributesDP {
    CatalogueAttributesHelper catalogueAttributesHelper = new CatalogueAttributesHelper();
    GenericValidator genericValidator = new GenericValidator();
    SoftAssert softAssert;

    @Test(dataProvider = "cartPayload", groups = "sanity", description = "Test CartV2 API for Catalogue Attributes...!!")
    public void cartUpdateTest(String restaurantId, String[] itemIds,
                               boolean isAddonRequired,
                               boolean isItemLevelCheckRequired,
                               String tdType,
                               String cartType){
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        CartV2Response cartV2Response = catalogueAttributesHelper.updateCartV2(
                itemIds, isAddonRequired, cartType);
        genericValidator.smokeCheck(CatalogueAttributesConstants.STATUS_CODE_CARTV2_SUCCESS,
                CatalogueAttributesConstants.STATUS_MESSAGE_CARTV2_SUCCESS, cartV2Response);
        Reporter.log(Utility.jsonEncode(cartV2Response), true);
        catalogueAttributesHelper.validateCartForAttributes(cartV2Response, isItemLevelCheckRequired, isAddonRequired);
    }

    @Test(dataProvider = "cartPayload", groups = "sanity", description = "Test Get Cart API for Catalogue Attributes...!!")
    public void getCartTest(String restaurantId, String[] itemIds,
                               boolean isAddonRequired,
                               boolean isItemLevelCheckRequired,
                               String tdType,
                               String cartType){
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        CartV2Response cartV2Response = catalogueAttributesHelper.updateCartV2(
                itemIds, isAddonRequired, cartType);
        genericValidator.smokeCheck(CatalogueAttributesConstants.STATUS_CODE_CARTV2_SUCCESS,
                CatalogueAttributesConstants.STATUS_MESSAGE_CARTV2_SUCCESS, cartV2Response);
        CartV2Response getCartV2Response = catalogueAttributesHelper.getCartV2();
        genericValidator.smokeCheck(CatalogueAttributesConstants.STATUS_CODE_CARTV2_SUCCESS,
                CatalogueAttributesConstants.STATUS_MESSAGE_CARTV2_SUCCESS, getCartV2Response);
        catalogueAttributesHelper.validateCartForAttributes(getCartV2Response, isItemLevelCheckRequired, isAddonRequired);
    }

    @Test(dataProvider = "cartPayload", groups = "sanity", description = "Test Check Totals API for Catalogue Attributes...!!")
    public void checkTotalsTest(String restaurantId, String[] itemIds,
                            boolean isAddonRequired,
                            boolean isItemLevelCheckRequired,
                            String tdType,
                            String cartType){
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        CartV2Response checkTotalsResponse = catalogueAttributesHelper.checkTotals(
                itemIds, isAddonRequired, cartType);
        genericValidator.smokeCheck(CatalogueAttributesConstants.STATUS_CODE_CARTV2_SUCCESS,
                CatalogueAttributesConstants.STATUS_MESSAGE_CARTV2_SUCCESS, checkTotalsResponse);
        catalogueAttributesHelper.validateCartForAttributes(checkTotalsResponse, isItemLevelCheckRequired, isAddonRequired);
    }

    @Test(dataProvider = "cartPayload", groups = "sanity", description = "Test Cart Minimal API for Catalogue Attributes...!!")
    public void cartMinimalTest(String restaurantId, String[] itemIds,
                               boolean isAddonRequired,
                               boolean isItemLevelCheckRequired,
                               String tdType,
                               String cartType){
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        CartV2Response cartMinimal = catalogueAttributesHelper.cartMinimal(
                itemIds, isAddonRequired, cartType);
        genericValidator.smokeCheck(CatalogueAttributesConstants.STATUS_CODE_CARTV2_SUCCESS,
                CatalogueAttributesConstants.STATUS_MESSAGE_CARTV2_SUCCESS, cartMinimal);
        Reporter.log(Utility.jsonEncode(cartMinimal), true);
        catalogueAttributesHelper.validateCartForAttributes(cartMinimal, isItemLevelCheckRequired, isAddonRequired);
    }

    @Test(dataProvider = "cartPayloadForOrder", groups = "sanity", description = "Test Place Order API for Catalogue Attributes")
    public void placeOrderTest(String restaurantId, String[] itemIds,
                               boolean isAddonRequired,
                               boolean isItemLevelCheckRequired,
                               String tdType,
                               String cartType,
                               String paymentMethod,
                               String orderComment){
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        OrderResponse orderResponse = catalogueAttributesHelper.placeOrder(itemIds, isAddonRequired, cartType,
                paymentMethod, orderComment);
        System.out.println("[ORDER RESPONSE] -- " + Utility.jsonEncode(orderResponse));
        softAssert = new SoftAssert();
        softAssert.assertEquals(orderResponse.getStatusCode().intValue(), 0, "Status Code mismatch for [PLACE ORDER]");
        softAssert.assertEquals(orderResponse.getStatusMessage(), "done successfully", "Status Message mismatch for [PLACE ORDER]");
        softAssert.assertAll();
        catalogueAttributesHelper.validateOrderForAttributes(itemIds, orderResponse, isItemLevelCheckRequired, isAddonRequired);
    }


    @Test(dataProvider = "cartPayloadForOrder", groups = "sanity", description = "Test Edit Order Check API for Catalogue Attributes")
    public void editOrderCheckTest(String restaurantId, String[] itemIds,
                               boolean isAddonRequired,
                               boolean isItemLevelCheckRequired,
                               String tdType,
                               String cartType,
                               String paymentMethod,
                               String orderComment){
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        CartV2Response editOrderCheck = catalogueAttributesHelper.getEditOrderCheck(itemIds, isAddonRequired, cartType,
                paymentMethod, orderComment);
        Reporter.log("[ORDER RESPONSE] -- " + Utility.jsonEncode(editOrderCheck), true);
        softAssert = new SoftAssert();
        softAssert.assertEquals(editOrderCheck.getStatusCode().intValue(), CatalogueAttributesConstants.STATUS_CODE_CARTV2_SUCCESS,
                "Status Code mismatch for [Edit ORDER CHECK]");
        softAssert.assertEquals(editOrderCheck.getStatusMessage(), CatalogueAttributesConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                "Status Message mismatch for [EDIT ORDER CHECK]");
        softAssert.assertAll();
        catalogueAttributesHelper.validateCartV2(itemIds, editOrderCheck, isAddonRequired);
    }

    @Test(dataProvider = "cartPayloadForOrder", groups = "sanity", description = "Test Edit Order Confirm API for Catalogue Attributes")
    public void editOrderConfirmTest(String restaurantId, String[] itemIds,
                                   boolean isAddonRequired,
                                   boolean isItemLevelCheckRequired,
                                   String tdType,
                                   String cartType,
                                   String paymentMethod,
                                   String orderComment){
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        OrderResponse editOrderConfirm = catalogueAttributesHelper.getEditOrderConfirm(itemIds, isAddonRequired, cartType,
                paymentMethod, orderComment);
        Reporter.log("[ORDER RESPONSE] -- " + Utility.jsonEncode(editOrderConfirm), true);
        softAssert = new SoftAssert();
        softAssert.assertEquals(editOrderConfirm.getStatusCode().intValue(), 0, "Status Code mismatch for [EDIT ORDER CONFIRM]");
        softAssert.assertEquals(editOrderConfirm.getStatusMessage(), "done successfully", "Status Message mismatch for [EDIT ORDER CONFIRM]");
        softAssert.assertAll();
        catalogueAttributesHelper.validateOrderForAttributes(itemIds, editOrderConfirm, isItemLevelCheckRequired, isAddonRequired);
    }

    @Test(dataProvider = "cartPayloadForOrder", groups = "sanity", description = "Test Clone Order Check API for Catalogue Attributes")
    public void CloneOrderCheckTest(String restaurantId, String[] itemIds,
                                   boolean isAddonRequired,
                                   boolean isItemLevelCheckRequired,
                                   String tdType,
                                   String cartType,
                                   String paymentMethod,
                                   String orderComment){
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        CartV2Response editOrderCheck = catalogueAttributesHelper.cloneOrderCheck(itemIds, isAddonRequired, cartType,
                paymentMethod, orderComment);
        Reporter.log("[ORDER RESPONSE] -- " + Utility.jsonEncode(editOrderCheck), true);
        softAssert = new SoftAssert();
        softAssert.assertEquals(editOrderCheck.getStatusCode().intValue(), CatalogueAttributesConstants.STATUS_CODE_CARTV2_SUCCESS,
                "Status Code mismatch for [CLONE ORDER CHECK]");
        softAssert.assertEquals(editOrderCheck.getStatusMessage(), CatalogueAttributesConstants.STATUS_MESSAGE_CARTV2_SUCCESS,
                "Status Message mismatch for [CLONE ORDER CHECK]");
        softAssert.assertAll();
        catalogueAttributesHelper.validateCartV2(itemIds, editOrderCheck, isAddonRequired);
    }

    @Test(dataProvider = "cartPayloadForOrder", groups = "sanity", description = "Test Clone Order Confirm API for Catalogue Attributes")
    public void cloneOrderConfirmTest(String restaurantId, String[] itemIds,
                                     boolean isAddonRequired,
                                     boolean isItemLevelCheckRequired,
                                     String tdType,
                                     String cartType,
                                     String paymentMethod,
                                     String orderComment){
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        OrderResponse editOrderConfirm = catalogueAttributesHelper.cloneOrderConfirm(itemIds, isAddonRequired, cartType,
                paymentMethod, orderComment);
        Reporter.log("[ORDER RESPONSE] -- " + Utility.jsonEncode(editOrderConfirm), true);
        softAssert = new SoftAssert();
        softAssert.assertEquals(editOrderConfirm.getStatusCode().intValue(), 0, "Status Code mismatch for [CLONE ORDER CONFIRM]");
        softAssert.assertEquals(editOrderConfirm.getStatusMessage(), "done successfully", "Status Message mismatch for [CLONE ORDER CONFIRM]");
        softAssert.assertAll();
        catalogueAttributesHelper.validateOrderForAttributes(itemIds, editOrderConfirm, isItemLevelCheckRequired, isAddonRequired);
    }

    @Test(dataProvider = "cartPayloadForOrder", groups = "sanity", description = "Test Single Order API for Catalogue Attributes")
    public void singleOrderTest(String restaurantId, String[] itemIds,
                                      boolean isAddonRequired,
                                      boolean isItemLevelCheckRequired,
                                      String tdType,
                                      String cartType,
                                      String paymentMethod,
                                      String orderComment) {
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        OrderResponse orderResponse = catalogueAttributesHelper.placeOrder(itemIds, isAddonRequired, cartType,
                paymentMethod, orderComment);
        Long orderId = orderResponse.getData().getOrderId();
        GetSingleOrderResponse getSingleOrderResponse = catalogueAttributesHelper.singleOrder(String.valueOf(orderId));
        softAssert = new SoftAssert();
        softAssert.assertEquals(getSingleOrderResponse.getStatusCode().intValue(), 0, "Status Code mismatch for [SINGLE ORDER]");
        softAssert.assertEquals(getSingleOrderResponse.getStatusMessage(), "done successfully", "Status Message mismatch for [SINGLE ORDER]");
        softAssert.assertAll();
        orderResponse.setStatusCode(getSingleOrderResponse.getStatusCode());
        orderResponse.setStatusMessage(getSingleOrderResponse.getStatusMessage());
        orderResponse.setTid(getSingleOrderResponse.getTid());
        orderResponse.setSid(getSingleOrderResponse.getSid());
        orderResponse.setDeviceId(getSingleOrderResponse.getDeviceId());
        orderResponse.setData(getSingleOrderResponse.getData().getOrders().get(0));
        catalogueAttributesHelper.validateOrderForAttributes(itemIds, orderResponse, isItemLevelCheckRequired, isAddonRequired);
    }

    @Test(dataProvider = "cartPayloadForOrder", groups = "sanity", description = "Test Last Order API for Catalogue Attributes")
    public void lastOrderTest(String restaurantId, String[] itemIds,
                                boolean isAddonRequired,
                                boolean isItemLevelCheckRequired,
                                String tdType,
                                String cartType,
                                String paymentMethod,
                                String orderComment) {
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        OrderResponse orderResponse = catalogueAttributesHelper.placeOrder(itemIds, isAddonRequired, cartType,
                paymentMethod, orderComment);
        Long orderId = orderResponse.getData().getOrderId();
        GetLastOrderResponse getLastOrderResponse = catalogueAttributesHelper.lastOrder();
        softAssert = new SoftAssert();
        softAssert.assertEquals(orderResponse.getStatusCode().intValue(), 0, "Status Code mismatch for [LAST ORDER]");
        softAssert.assertEquals(orderResponse.getStatusMessage(), "done successfully", "Status Message mismatch for [LAST ORDER]");
        softAssert.assertAll();
        orderResponse.setStatusCode(getLastOrderResponse.getStatusCode());
        orderResponse.setStatusMessage(getLastOrderResponse.getStatusMessage());
        orderResponse.setTid(getLastOrderResponse.getTid());
        orderResponse.setSid(getLastOrderResponse.getSid());
        orderResponse.setDeviceId(getLastOrderResponse.getDeviceId());
        orderResponse.setData(getLastOrderResponse.getData().getLastOrder());
        catalogueAttributesHelper.validateOrderForAttributes(itemIds, orderResponse, isItemLevelCheckRequired, isAddonRequired);
    }

    @Test(dataProvider = "cartPayloadForOrder", groups = "sanity", description = "Test Get All Order API for Catalogue Attributes")
    public void getAllOrderTest(String restaurantId, String[] itemIds,
                                boolean isAddonRequired,
                                boolean isItemLevelCheckRequired,
                                String tdType,
                                String cartType,
                                String paymentMethod,
                                String orderComment) {
        catalogueAttributesHelper.createOrUpdateTradeDiscount(restaurantId, tdType);
        OrderResponse orderResponse = catalogueAttributesHelper.placeOrder(itemIds, isAddonRequired, cartType,
                paymentMethod, orderComment);
        Long orderId = orderResponse.getData().getOrderId();
        GetSingleOrderResponse getAllOrderResponse = catalogueAttributesHelper.getAllOrders();
        softAssert = new SoftAssert();
        softAssert.assertEquals(getAllOrderResponse.getStatusCode().intValue(), 0, "Status Code mismatch for [GET ALL ORDER]");
        softAssert.assertEquals(getAllOrderResponse.getStatusMessage(), "done successfully", "Status Message mismatch for [GET ALL ORDER]");
        softAssert.assertAll();
        orderResponse.setStatusCode(getAllOrderResponse.getStatusCode());
        orderResponse.setStatusMessage(getAllOrderResponse.getStatusMessage());
        orderResponse.setTid(getAllOrderResponse.getTid());
        orderResponse.setSid(getAllOrderResponse.getSid());
        orderResponse.setDeviceId(getAllOrderResponse.getDeviceId());
        orderResponse.setData(getAllOrderResponse.getData().getOrders().get(0));
        catalogueAttributesHelper.validateOrderForAttributes(itemIds, orderResponse, isItemLevelCheckRequired, isAddonRequired);
    }

    @AfterMethod
    public void cancelAllOrders(ITestResult iTestResult){
        if(!iTestResult.getMethod().getMethodName().equals("checkTotalsTest")
                || !iTestResult.getMethod().getMethodName().equals("cartUpdateTest")
                || !iTestResult.getMethod().getMethodName().equals("getCartTest")){
            catalogueAttributesHelper.cancelAllOrder();
        }
    }

}
