package com.swiggy.api.sf.checkout.helper.rts.validator;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

public class GenericValidator {

    public void smokeCheck(int statusCode, String statusMessage, Processor processor, String errorMessage){
        SoftAssert softAssert = new SoftAssert();
        Reporter.log("Expected StatusCode = '" + statusCode + "' <==> Actual StatusCode = '" + processor.ResponseValidator.GetNodeValueAsInt("statusCode") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode, errorMessage);
        Reporter.log("Expected StatusMessage = '" + statusMessage + "' <==> Actual StatusMessage = '" + processor.ResponseValidator.GetNodeValue("statusMessage") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage, errorMessage);
        softAssert.assertAll();
    }

    public void smokeCheck(int statusCode, String statusMessage, Processor processor){
        SoftAssert softAssert = new SoftAssert();
        Reporter.log("Expected StatusCode = '" + statusCode + "' <==> Actual StatusCode = '" + processor.ResponseValidator.GetNodeValueAsInt("statusCode") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode, "StatusCode mismatch");
        Reporter.log("Expected StatusMessage = '" + statusMessage + "' <==> Actual StatusMessage = '" + processor.ResponseValidator.GetNodeValue("statusMessage") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage, "StatusMessage mismatch");
        softAssert.assertAll();
    }

    public void smokeCheck(int statusCode, String statusMessage, CartV2Response cartV2Response){
        SoftAssert softAssert = new SoftAssert();
        Reporter.log("Expected StatusCode = '" + statusCode + "' <==> Actual StatusCode = '" + cartV2Response.getStatusCode() + "'", true);
        softAssert.assertEquals(cartV2Response.getStatusCode().intValue(), statusCode, "StatusCode mismatch");
        Reporter.log("Expected StatusMessage = '" + statusMessage + "' <==> Actual StatusMessage = '" + cartV2Response.getStatusMessage() + "'", true);
        softAssert.assertEquals(cartV2Response.getStatusMessage(), statusMessage, "StatusMessage mismatch");
        softAssert.assertAll();
    }

}
