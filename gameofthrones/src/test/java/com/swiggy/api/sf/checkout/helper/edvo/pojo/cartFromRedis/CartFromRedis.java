package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartFromRedis;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "id",
        "originalId",
        "orderId",
        "sessionId",
        "phoneNo",
        "emailId",
        "cartBlob",
        "createdOn",
        "updatedBy",
        "updatedAt"
})
public class CartFromRedis {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("originalId")
    private Integer originalId;
    @JsonProperty("orderId")
    private Long orderId;
    @JsonProperty("sessionId")
    private Object sessionId;
    @JsonProperty("phoneNo")
    private String phoneNo;
    @JsonProperty("emailId")
    private String emailId;
    @JsonProperty("cartBlob")
    private String cartBlob;
    @JsonProperty("createdOn")
    private BigInteger createdOn;
    @JsonProperty("updatedBy")
    private Object updatedBy;
    @JsonProperty("updatedAt")
    private BigInteger updatedAt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("originalId")
    public Integer getOriginalId() {
        return originalId;
    }

    @JsonProperty("originalId")
    public void setOriginalId(Integer originalId) {
        this.originalId = originalId;
    }

    @JsonProperty("orderId")
    public Long getOrderId() {
        return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("sessionId")
    public Object getSessionId() {
        return sessionId;
    }

    @JsonProperty("sessionId")
    public void setSessionId(Object sessionId) {
        this.sessionId = sessionId;
    }

    @JsonProperty("phoneNo")
    public String getPhoneNo() {
        return phoneNo;
    }

    @JsonProperty("phoneNo")
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @JsonProperty("emailId")
    public String getEmailId() {
        return emailId;
    }

    @JsonProperty("emailId")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @JsonProperty("cartBlob")
    public String getCartBlob() {
        return cartBlob;
    }

    @JsonProperty("cartBlob")
    public void setCartBlob(String cartBlob) {
        this.cartBlob = cartBlob;
    }

    @JsonProperty("createdOn")
    public BigInteger getCreatedOn() {
        return createdOn;
    }

    @JsonProperty("createdOn")
    public void setCreatedOn(BigInteger createdOn) {
        this.createdOn = createdOn;
    }

    @JsonProperty("updatedBy")
    public Object getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updatedBy")
    public void setUpdatedBy(Object updatedBy) {
        this.updatedBy = updatedBy;
    }

    @JsonProperty("updatedAt")
    public BigInteger getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updatedAt")
    public void setUpdatedAt(BigInteger updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}