package com.swiggy.api.sf.checkout.dp;

import com.swiggy.api.sf.checkout.constants.AddressConstants;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.checkout.pojo.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddressDP {

    AddressPOJO addressPOJO=new AddressPOJO();
    static String mobile= System.getenv("mobile");
    String password= System.getenv("password");
    static String tid;
    static String token;
    static String addressPayload;

    @BeforeClass
    public void setAddressPayload() throws IOException {
        if (mobile == null || mobile.isEmpty()){
            mobile= CheckoutConstants.mobile;}

        if (password == null ||password.isEmpty()){
            password=CheckoutConstants.password;}

        HashMap<String, String> hashMap=CheckoutHelper.loginData(mobile,password);
        tid=hashMap.get("Tid");
        token=hashMap.get("Token");

        addressPOJO.setDefaultData();
        addressPOJO.setMobile(mobile);
        addressPayload= Utility.jsonEncode(addressPOJO);
    }

    @DataProvider(name = "newAddressAnnotation")
    public Object[][] newAddressAnnotation() throws IOException {
        addressPOJO.setMobile(mobile);
        addressPOJO.setAnnotation("Home");
        String addressPayloadHome= Utility.jsonEncode(addressPOJO);
        addressPOJO.setAnnotation("Work");
        String addressPayloadWork= Utility.jsonEncode(addressPOJO);
        addressPOJO.setAnnotation("OTHERS_TEST");
        String addressPayloadOthers= Utility.jsonEncode(addressPOJO);
        return new Object[][]{
                {"Swiggy-Android", "310",addressPayloadHome},
                {"Swiggy-Android", "310",addressPayloadWork},
                {"Swiggy-Android", "310",addressPayloadOthers},
                {"Web", "309",addressPayloadHome},
                {"Web", "309",addressPayloadWork},
                {"Web", "309",addressPayloadOthers},
                {"Swiggy-iOS","234",addressPayloadHome},
                {"Swiggy-iOS","234",addressPayloadWork},
                {"Swiggy-iOS","234",addressPayloadOthers}
        };
    }


    @DataProvider(name = "deleteAddress")
    public static Object[][] deleteAddress(){
        return new Object[][]{
                {"Swiggy-Android", "310",addressPayload},
                {"Web", "309",addressPayload},
                {"Swiggy-iOS","234",addressPayload}
        };
    }

    @DataProvider(name = "deleteExistingAddress")
    public static Object[][] deleteExistingAddress()
    {return new Object[][]{
            {"Swiggy-Android", "310"},
            {"Web", "309"},
            {"Swiggy-iOS","234"}
            };
    }

    @DataProvider(name = "deleteNonExistingAddress")
    public static Object[][] deleteNonExistingAddress()
    {return new Object[][]{
            {"Swiggy-Android", "310", AddressConstants.inValidMessage},
            {"Web", "309", AddressConstants.inValidMessage},
            {"Swiggy-iOS","234", AddressConstants.inValidMessage}
    };
    }

    @DataProvider(name = "NewAddressAnnotation")
    public static Object[][] newAddressAnn() {
        return new Object[][]{
                {tid,token, "Rest123", mobile, "199, Near Sai Baba Temple, 7th Block, Koramangala", "Udupi garden", "Swiggy Test", "12.911282", "77.67624999999998", "78212", "Bangalore", "WORK",AddressConstants.statusmessage, AddressConstants.validStatusCode},
        };
    }

    @DataProvider(name = "UpdateAddressAnnotation")
    public static Object[][] UpdateAddressAnnotation() {
        return new Object[][]{
                {tid,token, "Rest123", "7338471171", "199, BTM layout, 2nd stage", "office Land Mark", "Kormangala","test7", "12.911282", "77.67", "78212", "Hyderabad", "WORK", AddressConstants.updateAddressMesage, AddressConstants.validStatusCode},
                {tid,token, "Rest123", "7338471171", "199, BTM layout, 2nd stage", "Udupi garden","AXABuilding" ,"test7", "12.911282", "77.676", "78212", "Hyderabad", "HOME",AddressConstants.updateAddressMesage, AddressConstants.validStatusCode},
                {tid,token, "Dean", "9020202102", "199, 7th Block, Koramangala", "Pain","ffff", "Swiggy Test", "12.911282", "77.67", "78212", "Bangalore", "OTHER",AddressConstants.updateAddressMesage, AddressConstants.validStatusCode}
        };
    }

    @DataProvider(name = "UpdateExistAddress")
    public static Object[][] UpdateExistAddress() {
        return new Object[][]{
                {tid,token, "Seth", "7338471171", "199, BTM layout, 2nd stage", " office Land Mark", "Kormangala", "12.911282", "77.67624999999998", "78212", "Hyderabad", "WORK", AddressConstants.updateAddressMesage},
                {tid,token, "Roman", "7338471171", "199, BTM layout, 2nd stage", " Udupi garden","AXABuilding" , "12.911282", "77.676", "78212", "Hyderabad", "HOME",AddressConstants.updateAddressMesage},
                {tid,token, "Dean", "4270873889", "199, CCA SChool, 7th Block, Koramangala", "Pain", "Swiggy Test", "12.9182", "77.67", "78212", "Bangalore", "OTHER",AddressConstants.updateAddressMesage}
        };
    }

    @DataProvider(name = "MandatoryFields")
    public static Object[][] MandatoryFields() {
        return new Object[][]{
                {tid,token, "", "7338471171", "199, BTM layout, 2nd stage", " office Land Mark","test7", "12.911282", "77.67624999999998", "78212", "Hyderabad", "WORK", AddressConstants.invalidAddressMessage, AddressConstants.invalidStatusCode},
                {tid,token, "Rest123", "7338471171", "", " Udupi garden","test7", "12.911282", "77.67624999999998", "78212", "Hyderabad", "HOME", AddressConstants.invalidAddressMessage, AddressConstants.invalidStatusCode},
                {tid,token, "Dean", "9020202102", "199, CCA SChool, 7th Block, Koramangala","ffff", "Swiggy Test", "", "77.67", "78212", "Bangalore", "OTHER", AddressConstants.invalidAddressMessage, AddressConstants.invalidStatusCode},
                {tid,token, "Dean", "9020202102", "199, CCA SChool, 7th Block, Koramangala","ffff", "Swiggy Test", "12.33", "", "78212", "Bangalore", "OTHER", AddressConstants.invalidAddressMessage, AddressConstants.invalidStatusCode},
        };
    }

    @DataProvider(name = "ServicableAddress")
    public static Object[][] ServicableAddress() {
        return new Object[][]{
                {tid,token, "Seth", "7338471171", "199, BTM layout, 2nd stage", " office Land Mark","test7", "12.934533", "77.626579", "78212", "Hyderabad", "WORK", AddressConstants.statusmessage, AddressConstants.validStatusCode},
                {tid,token, "Rest123", "7338471171", "rkoouttanowhere", " Udupi garden","test7", "12.911282", "77.67624999999998", "78212", "Hyderabad", "HOME", AddressConstants.statusmessage, AddressConstants.validStatusCode},
                };
    }

    @DataProvider(name = "mobileNumberCheck")
    public static Object[][] mobileNumberCheck() {
        return new Object[][]{
                {tid,token,  "Shant", "7338471171", "199, BTM layout, 2nd stage", " office Land Mark", "Swiggy Test", "12.911282", "77.6762", "78212", "Hyderabad", "WORK", AddressConstants.statusmessage, AddressConstants.validStatusCode},
                {tid,token, "Rest123", "7338471171", "199, Near Sai Baba Temple, 7th Block, Koramangala", "Udupi garden", "Swiggy Test", "12.911282", "77.67624999999998", "78212", "Bangalore", "HOME",AddressConstants.statusmessage, AddressConstants.validStatusCode},
                {tid,token, "Dean", "9020202102", "199, CCA SChool, 7th Block, Koramangala", "ffff", "Swiggy Test", "12.911282", "77.67", "78212", "Bangalore", "OTHER",AddressConstants.statusmessage, AddressConstants.validStatusCode}
        };
    }

    @DataProvider(name="MultipleOrderAddAddress")
    public Object[][] MultipleOrderAddAddress() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        List<String> list = helper.pickNRandom("281");
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, list.get(0), 1));
        cart.add(new Cart(addon, variant, list.get(1), 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password(password)
                .mobile(Long.parseLong(mobile))
                .cart(cart)
                .restaurant(281)
                .name("jk")
                .landmark("Golf Course road")
                .address("Old Gurgaon")
                .area("DLF")
                .lat("12.277")
                .lng("77.919")
                .annotation("OTHER")
                .buildTillAddress();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name="AddAndDeleteAddress")
    public Object[][] AddAndDeleteAddress() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        List<String> list = helper.pickNRandom("281");
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, list.get(0), 1));
        cart.add(new Cart(addon, variant, list.get(1), 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password(password)
                .mobile(Long.parseLong(mobile))
                .cart(cart)
                .restaurant(281)
                .name("jk")
                .addressMobile(mobile)
                .landmark("Golf Course road")
                .address("Old Gurgaon")
                .area("DLF")
                .lat("12.277")
                .lng("77.919")
                .annotation("OTHER")
                .buildTillAddress();
        return new Object[][]{
                {createOrder,AddressConstants.deleteMessage}
        };
    }


    @DataProvider(name="addAddressSchemaValidation")
    public Object[][] addAddressSchemaValidation() throws IOException {

        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password(password)
                .mobile(Long.parseLong(mobile))
                .name("jk")
                .addressMobile(mobile)
                .landmark("Golf Course road")
                .address("Old Gurgaon")
                .area("DLF")
                .lat("12.277")
                .lng("77.919")
                .annotation("OTHER")
                .buildAddress();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name="deleteAddressSchemaValidation")
    public Object[][] deleteAddressSchemaValidation() throws IOException {

        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password(password)
                .mobile(Long.parseLong(mobile))
                .buildLogin();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name="updateAddressSchemaValidation")
    public Object[][] updateAddressSchemaValidation() throws IOException {

        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password(password)
                .mobile(Long.parseLong(mobile))
                .name("jk")
                .addressMobile("7490223323")
                .landmark("Golf Course road")
                .address("Old Gurgaon")
                .area("DLF")
                .lat("12.277")
                .lng("77.919")
                .annotation("OTHER")
                .buildAddress();
        return new Object[][]{
                {createOrder}
        };
    }

    @DataProvider(name="getAllAddressSchemaValidation")
    public Object[][] getAllAddressSchemaValidation() throws IOException {

        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password(password)
                .mobile(Long.parseLong(mobile))
                .buildLogin();
        return new Object[][]{
                {createOrder}
        };
    }
}
