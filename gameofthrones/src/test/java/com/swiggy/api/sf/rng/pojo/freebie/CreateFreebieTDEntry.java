package com.swiggy.api.sf.rng.pojo.freebie;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class CreateFreebieTDEntry {

private String namespace;
private String header;
private String description;
private String shortDescription;
@JsonProperty("valid_from")
private String validFrom;
@JsonProperty("valid_till")
private String validTill;
@JsonProperty("campaign_type")
private String campaignType;
@JsonProperty("dormant_user_type")
private String dormantUserType;
@JsonProperty("restaurant_hit")
private String restaurantHit;
@JsonProperty("swiggy_hit")
private String swiggyHit;
private String discountLevel;
private List<FreebieRestaurantList> restaurantList = null;
private FreebieRuleDiscount ruleDiscount;
private String enabled;
private String createdBy;
private String updatedBy;
private String firstOrderRestriction;
private String timeSlotRestriction;
private List<FreebieSlot> slots = null;
private String commissionOnFullBill;
private String taxesOnDiscountedBill;
private String userRestriction;
private String restaurantFirstOrder;

    @JsonProperty("isSuper")
    private Boolean isSuper;

/**
* No args constructor for use in serialization
* 
*/
public CreateFreebieTDEntry() {
}

/**
* 
* @param ruleDiscount
* @param timeSlotRestriction
* @param enabled
* @param restaurantFirstOrder
* //@param dormant_user_type
* @param campaignType
* @param restaurantList
* @param commissionOnFullBill
* @param userRestriction
* @param swiggyHit
* @param restaurantHit
* @param validTill
* @param header
* @param updatedBy
* @param namespace
* @param firstOrderRestriction
* @param createdBy
* @param shortDescription
* @param slots
* @param description
* @param validFrom
* @param discountLevel
* @param taxesOnDiscountedBill
*/
public CreateFreebieTDEntry(String namespace, String header, String description, String shortDescription, String validFrom, String validTill, String campaignType, String dormantUserType, String restaurantHit, String swiggyHit, String discountLevel, List<FreebieRestaurantList> restaurantList, FreebieRuleDiscount ruleDiscount, String enabled, String createdBy, String updatedBy, String firstOrderRestriction, String timeSlotRestriction, List<FreebieSlot> slots, String commissionOnFullBill, String taxesOnDiscountedBill, String userRestriction, String restaurantFirstOrder,Boolean isSuper) {
super();
this.namespace = namespace;
this.header = header;
this.description = description;
this.shortDescription = shortDescription;
this.validFrom = validFrom;
this.validTill = validTill;
this.campaignType = campaignType;
this.dormantUserType = dormantUserType;
this.restaurantHit = restaurantHit;
this.swiggyHit = swiggyHit;
this.discountLevel = discountLevel;
this.restaurantList = restaurantList;
this.ruleDiscount = ruleDiscount;
this.enabled = enabled;
this.createdBy = createdBy;
this.updatedBy = updatedBy;
this.firstOrderRestriction = firstOrderRestriction;
this.timeSlotRestriction = timeSlotRestriction;
this.slots = slots;
this.commissionOnFullBill = commissionOnFullBill;
this.taxesOnDiscountedBill = taxesOnDiscountedBill;
this.userRestriction = userRestriction;
this.restaurantFirstOrder = restaurantFirstOrder;
this.isSuper= isSuper;
}

public String getNamespace() {
return namespace;
}

public void setNamespace(String namespace) {
this.namespace = namespace;
}

public String getHeader() {
return header;
}

public void setHeader(String header) {
this.header = header;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getShortDescription() {
return shortDescription;
}

public void setShortDescription(String shortDescription) {
this.shortDescription = shortDescription;
}

public String getValidFrom() {
return validFrom;
}

public void setValidFrom(String validFrom) {
this.validFrom = validFrom;
}

public String getValidTill() {
return validTill;
}

public void setValidTill(String validTill) {
this.validTill = validTill;
}

public String getCampaignType() {
return campaignType;
}

public void setCampaignType(String campaignType) {
this.campaignType = campaignType;
}

public String getDormantUserType() {
return dormantUserType;
}

public void setDormantUserType(String dormantUserType) {
this.dormantUserType = dormantUserType;
}

public String getRestaurantHit() {
return restaurantHit;
}

public void setRestaurantHit(String restaurantHit) {
this.restaurantHit = restaurantHit;
}

public String getSwiggyHit() {
return swiggyHit;
}

public void setSwiggyHit(String swiggyHit) {
this.swiggyHit = swiggyHit;
}

public String getDiscountLevel() {
return discountLevel;
}

public void setDiscountLevel(String discountLevel) {
this.discountLevel = discountLevel;
}

public List<FreebieRestaurantList> getRestaurantList() {
return restaurantList;
}

public void setRestaurantList(List<FreebieRestaurantList> restaurantList) {
this.restaurantList = restaurantList;
}

public FreebieRuleDiscount getRuleDiscount() {
return ruleDiscount;
}

public void setRuleDiscount(FreebieRuleDiscount ruleDiscount) {
this.ruleDiscount = ruleDiscount;
}

public String getEnabled() {
return enabled;
}

public void setEnabled(String enabled) {
this.enabled = enabled;
}

public String getCreatedBy() {
return createdBy;
}

public void setCreatedBy(String createdBy) {
this.createdBy = createdBy;
}

public String getUpdatedBy() {
return updatedBy;
}

public void setUpdatedBy(String updatedBy) {
this.updatedBy = updatedBy;
}

public String getFirstOrderRestriction() {
return firstOrderRestriction;
}

public void setFirstOrderRestriction(String firstOrderRestriction) {
this.firstOrderRestriction = firstOrderRestriction;
}

public String getTimeSlotRestriction() {
return timeSlotRestriction;
}

public void setTimeSlotRestriction(String timeSlotRestriction) {
this.timeSlotRestriction = timeSlotRestriction;
}

public List<FreebieSlot> getSlots() {
return slots;
}

public void setSlots(List<FreebieSlot> slots) {
this.slots = slots;
}

public String getCommissionOnFullBill() {
return commissionOnFullBill;
}

public void setCommissionOnFullBill(String commissionOnFullBill) {
this.commissionOnFullBill = commissionOnFullBill;
}

public String getTaxesOnDiscountedBill() {
return taxesOnDiscountedBill;
}

public void setTaxesOnDiscountedBill(String taxesOnDiscountedBill) {
this.taxesOnDiscountedBill = taxesOnDiscountedBill;
}

public String getUserRestriction() {
return userRestriction;
}

public void setUserRestriction(String userRestriction) {
this.userRestriction = userRestriction;
}

public String getRestaurantFirstOrder() {
return restaurantFirstOrder;
}

public void setRestaurantFirstOrder(String restaurantFirstOrder) {
this.restaurantFirstOrder = restaurantFirstOrder;
}

    public Boolean getIsSuper() {
        return isSuper;
    }

    public void setIsSuper(Boolean isSuper) {
        this.isSuper = isSuper;
    }



    @Override
public String toString() {
return new ToStringBuilder(this).append("namespace", namespace).append("header", header).append("description", description).append("shortDescription", shortDescription).append("validFrom", validFrom).append("validTill", validTill).append("campaignType", campaignType).append("dormant_user_type", dormantUserType).append("restaurantHit", restaurantHit).append("swiggyHit", swiggyHit).append("discountLevel", discountLevel).append("restaurantList", restaurantList).append("ruleDiscount", ruleDiscount).append("enabled", enabled).append("createdBy", createdBy).append("updatedBy", updatedBy).append("firstOrderRestriction", firstOrderRestriction).append("timeSlotRestriction", timeSlotRestriction).append("slots", slots).append("commissionOnFullBill", commissionOnFullBill).append("taxesOnDiscountedBill", taxesOnDiscountedBill).append("userRestriction", userRestriction).append("restaurantFirstOrder", restaurantFirstOrder).append("isSuper",isSuper).toString();
}

}