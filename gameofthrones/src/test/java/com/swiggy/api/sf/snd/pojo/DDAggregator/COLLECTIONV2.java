package com.swiggy.api.sf.snd.pojo.DDAggregator;


import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class COLLECTIONV2 {

    private String latlng;
    private String customerPage;
    private List<ViewTypeRequest> viewTypeRequests = null;

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getCustomerPage() {
        return customerPage;
    }

    public void setCustomerPage(String customerPage) {
        this.customerPage = customerPage;
    }

    public List<ViewTypeRequest> getViewTypeRequests() {
        return viewTypeRequests;
    }

    public void setViewTypeRequests(List<ViewTypeRequest> viewTypeRequests) {
        this.viewTypeRequests = viewTypeRequests;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("latlng", latlng).append("customerPage", customerPage).append("viewTypeRequests", viewTypeRequests).toString();
    }

}
