package com.swiggy.api.sf.snd.pojo.DD;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class EnabledFilters {

    private List<Tag> tags = null;
    private List<ItemPrice> itemPrice = null;
    private List<RestaurantId> restaurantId = null;
    public List<RestaurantDiscountTypeRewardValue> restaurantDiscountTypeRewardValue=null;

    private List<RestaurantmultiTD> restaurantmultiTD = null;

    public List<RestaurantmultiTD> getRestaurantmultiTD() {
        return restaurantmultiTD;
    }

    public void setRestaurantmultiTD(List<RestaurantmultiTD> restaurantmultiTD) {
        this.restaurantmultiTD = restaurantmultiTD;
    }

    public List<RestaurantDiscountTypeRewardValue> getrestaurantDiscountTypeRewardValue() {
        return restaurantDiscountTypeRewardValue;
    }

    public void setrestaurantDiscountTypeRewardValue(List<RestaurantDiscountTypeRewardValue> restaurantDiscountTypeRewardValue) {
        this.restaurantDiscountTypeRewardValue = restaurantDiscountTypeRewardValue;
    }

    public List<Tag> getTags() {
        return tags;
    }
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<RestaurantId> getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(List<RestaurantId> restaurantId) {
        this.restaurantId = restaurantId;
    }

    public List<ItemPrice> getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(List<ItemPrice> itemPrice) {
        this.itemPrice = itemPrice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("tags", tags).append("itemPrice", itemPrice).append("restaurantId", restaurantId).append("restaurantDiscountTypeRewardValue", restaurantDiscountTypeRewardValue).append("restaurantmultiTD", restaurantmultiTD).toString();
    }

}
