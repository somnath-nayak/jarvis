package com.swiggy.api.sf.rng.pojo.SwiggySuper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "subscription_id",
        "order_id",
        "created_by"
})
public class UpdateSubscription {

    @JsonProperty("subscription_id")
    private String subscriptionId;
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("created_by")
    private String createdBy;

    /**
     * No args constructor for use in serialization
     *
     */
    public UpdateSubscription() {
    }

    /**
     *
     * @param createdBy
     * @param orderId
     * @param subscriptionId
     */
    public UpdateSubscription(String subscriptionId, String orderId, String createdBy) {
        super();
        this.subscriptionId = subscriptionId;
        this.orderId = orderId;
        this.createdBy = createdBy;
    }

    @JsonProperty("subscription_id")
    public String getSubscriptionId() {
        return subscriptionId;
    }

    @JsonProperty("subscription_id")
    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public UpdateSubscription withSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
        return this;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public UpdateSubscription withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("created_by")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UpdateSubscription withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
}