package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class CityEntity {
	@JsonProperty("id")
	private Integer id;

	@JsonProperty("id")
	public Integer getId() {
	return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
	this.id = id;
	}

	public CityEntity withId(Integer id) {
	this.id = id;
	return this;
	}
}
