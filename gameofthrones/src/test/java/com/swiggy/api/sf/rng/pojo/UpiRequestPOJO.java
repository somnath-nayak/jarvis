package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "vpa",
        "handle"
})
public class UpiRequestPOJO {

    @JsonProperty("vpa")
    private String vpa;
    @JsonProperty("handle")
    private String handle;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public UpiRequestPOJO() {
    }

    /**
     *
     * @param handle
     * @param vpa
     */
    public UpiRequestPOJO(String vpa, String handle) {
        super();
        this.vpa = vpa;
        this.handle = handle;
    }

    @JsonProperty("vpa")
    public String getVpa() {
        return vpa;
    }

    @JsonProperty("vpa")
    public void setVpa(String vpa) {
        this.vpa = vpa;
    }

    public UpiRequestPOJO withVpa(String vpa) {
        this.vpa = vpa;
        return this;
    }

    @JsonProperty("handle")
    public String getHandle() {
        return handle;
    }

    @JsonProperty("handle")
    public void setHandle(String handle) {
        this.handle = handle;
    }

    public UpiRequestPOJO withHandle(String handle) {
        this.handle = handle;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public UpiRequestPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public UpiRequestPOJO setDefault() {
        return this.withHandle("abhinavchandel@okicici")
                .withVpa("okicici");
    }

}
