package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.lang.time.DateUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.redis.E;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;

import framework.gameofthrones.JonSnow.Processor;

public class CreateTDDataCreation {

	static  int n =0;
	CreatePercentageTDWithRestrictions cr = new CreatePercentageTDWithRestrictions();
	CreateFlatTDWithRestrictions cf = new CreateFlatTDWithRestrictions();
	CreateFreebieTDWithRestrictions cfb = new CreateFreebieTDWithRestrictions();
	CreateFreedeliveryTDWithRestrictions cfd = new CreateFreedeliveryTDWithRestrictions();
	CreateNTds ctd= new CreateNTds();
	RngHelper rngHelper = new RngHelper();
	ArrayList types = new ArrayList();
	
	
	
	public List CreateTD(int noOfCampagian) throws IOException
	{
	 
		
		ArrayList al = new ArrayList();
		 n = rngHelper.roundUp(noOfCampagian,60)/60;
		 
		for (int i = 0; i < n; i++) {
			
			al.add(cr.getPercentageTD("SFO", "Restaurant"));
			al.add( cr.getPercentageTD("RFO", "Restaurant"));
			al.add( cr.getPercentageTD("NR", "Restaurant"));
			al.add( cr.getPercentageTD("UR", "Restaurant"));
			al.add( cr.getPercentageTD("DU", "Restaurant"));
			al.add( cr.getPercentageTD("TR", "Restaurant"));
			al.add(cr.getPercentageTD("SFO", "Category"));
			al.add( cr.getPercentageTD("RFO", "Category"));
			al.add( cr.getPercentageTD("NR", "Category"));
			al.add( cr.getPercentageTD("UR", "Category"));
			al.add( cr.getPercentageTD("DU", "Category"));
			al.add( cr.getPercentageTD("TR", "Category"));
			al.add(cr.getPercentageTD("SFO", "Subcategory"));
			al.add( cr.getPercentageTD("RFO", "Subcategory"));
			al.add( cr.getPercentageTD("NR", "Subcategory"));
			al.add( cr.getPercentageTD("UR", "Subcategory"));
			al.add( cr.getPercentageTD("DU", "Subcategory"));
			al.add( cr.getPercentageTD("TR", "Subcategory"));
			al.add(cr.getPercentageTD("SFO", "Item"));
			al.add( cr.getPercentageTD("RFO", "Item"));
			al.add( cr.getPercentageTD("NR", "Item"));
			al.add( cr.getPercentageTD("UR", "Item"));
			al.add( cr.getPercentageTD("DU", "Item"));
			al.add( cr.getPercentageTD("TR", "Item"));
			
			al.add(cf.getFlatTD("SFO", "Restaurant"));
			al.add(cf.getFlatTD("RFO", "Restaurant"));
			al.add(cf.getFlatTD("NR", "Restaurant"));
			al.add(cf.getFlatTD("UR", "Restaurant"));
			al.add(cf.getFlatTD("DU", "Restaurant"));
			al.add(cf.getFlatTD("TR", "Restaurant"));
			al.add(cf.getFlatTD("SFO", "Category"));
			al.add(cf.getFlatTD("RFO", "Category"));
			al.add(cf.getFlatTD("NR", "Category"));
			al.add(cf.getFlatTD("UR", "Category"));
			al.add(cf.getFlatTD("DU", "Category"));
			al.add(cf.getFlatTD("TR", "Category"));
			al.add(cf.getFlatTD("SFO", "Subcategory"));
			al.add(cf.getFlatTD("RFO", "Subcategory"));
			al.add(cf.getFlatTD("NR", "Subcategory"));
			al.add(cf.getFlatTD("UR", "Subcategory"));
			al.add(cf.getFlatTD("DU", "Subcategory"));
			al.add(cf.getFlatTD("TR", "Subcategory"));
			al.add(cf.getFlatTD("SFO", "Item"));
			al.add(cf.getFlatTD("RFO", "Item"));
			al.add(cf.getFlatTD("NR", "Item"));
			al.add(cf.getFlatTD("UR", "Item"));
			al.add(cf.getFlatTD("DU", "Item"));
			al.add(cf.getFlatTD("TR", "Item"));
		
			al.add(cfb.getFreebieTD("SFO", "Restaurant"));
			al.add(cfb.getFreebieTD("RFO", "Restaurant"));
			al.add(cfb.getFreebieTD("NR", "Restaurant"));
			al.add(cfb.getFreebieTD("UR", "Restaurant"));
			al.add(cfb.getFreebieTD("DU", "Restaurant"));
			al.add(cfb.getFreebieTD("TR", "Restaurant"));
			
			al.add(cfd.getFreeDevliveryTD("SFO", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("RFO", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("NR", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("UR", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("DU", "Restaurant"));
			al.add(cfd.getFreeDevliveryTD("TR", "Restaurant"));
		
		
		}
		
		return al;
    }

	
	
	public List CreateTD(String campaignType,int noOfCampagian) throws IOException
	{
		int n;
	
		if(campaignType.equalsIgnoreCase("Percentage")||campaignType.equalsIgnoreCase("Flat"))
		{
		  n = rngHelper.roundUp(noOfCampagian,24)/24;
		}
		else
		{
			n = rngHelper.roundUp(noOfCampagian,6)/6;
        }
		RngHelper rngHelper = new RngHelper();
		ArrayList al = new ArrayList();
		switch(campaignType)
		{
		case "Percentage"   : ctd.createPercentageTD(n, al, cr);break;
		case "Flat"         : ctd.createFlatTD(n, al, cf);break;
		case "Freebie"      : ctd.createFreebieTD(n, al, cfb);break;
		case "Freedelivery" : ctd.createFreedeliveryTD(n, al, cfd);break; 
		}
		return al;
    }
	
	
	/*public List CreateTD(String campaignType,String level,int noOfCampagian) throws IOException
	{
		RngHelper rngHelper = new RngHelper();
		ArrayList al = new ArrayList();
		
		int n;
	
		if(campaignType==null&level!=null)
		{
			if(level.equalsIgnoreCase("Restaurant"))
			{
		      n = rngHelper.roundUp(noOfCampagian,20)/20;
			}else
				
			{
				 n = rngHelper.roundUp(noOfCampagian,10)/10;
			}
		  switch(level)
			{
			case "Restaurant"       : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
			case "Category"         : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
			case "Subcategory"      : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
			case "Item"             : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
			}
		}

        if(level==null&campaignType!=null)
        {
        	if(campaignType.equalsIgnoreCase("Percentage")||campaignType.equalsIgnoreCase("Flat"))
    		{
    		  n = rngHelper.roundUp(noOfCampagian,20)/20;
    		}
    		else
    		{
    			n = rngHelper.roundUp(noOfCampagian,5)/5;
            }
    		switch(campaignType)
    		{
    		case "Percentage"   : ctd.createPercentageTD(n, al, cr);break;
    		case "Flat"         : ctd.createFlatTD(n, al, cf);break;
    		case "Freebie"      : ctd.createFreebieTD(n, al, cfb);break;
    		case "Freedelivery" : ctd.createFreedeliveryTD(n, al, cfd);break; 
    		}
    		
        }
        
        
        if(level!=null&campaignType!=null)
        {
        	if(campaignType.equalsIgnoreCase("Percentage")||campaignType.equalsIgnoreCase("Flat"))
    		{
    		  n = rngHelper.roundUp(noOfCampagian,20)/20;
    		}
    		else
    		{
    			n = rngHelper.roundUp(noOfCampagian,5)/5;
            }
    		switch(campaignType)
    		{
    		case "Percentage"   : ctd.createLevelBasedTD(n, al, LIst,level);break;
    		case "Flat"         : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
    		case "Freebie"      : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
    		case "Freedelivery" : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
    		}
    		
        }
        
		return al;
    }

	*/
	
	
	public List CreateTD(List campaignType,List levels,int noOfCampagian) throws IOException
	{
		RngHelper rngHelper = new RngHelper();
		ArrayList al = new ArrayList();
		Iterator it;
		Iterator it1;
		int n;
	
		if(campaignType.isEmpty()&!(levels.isEmpty()))
		{
		it =	levels.listIterator();
		while(it.hasNext())
		{
			String level = it.next().toString();
			if(level.equalsIgnoreCase("Restaurant"))
			{
		      n = rngHelper.roundUp(noOfCampagian,24)/24;
			}else
				
			{
				 n = rngHelper.roundUp(noOfCampagian,12)/12;
			}
		 
		 switch(level)
			{
			case "Restaurant"       : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
			case "Category"         : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
			case "Subcategory"      : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
			case "Item"             : ctd.createLevelBasedTD(n, al, cr,cf,cfb,cfd,level);break;
			}
		}
         
	}
        if(levels.isEmpty() &!(campaignType.isEmpty()))
        {
        
        	it =	campaignType.listIterator();
        	while(it.hasNext())
        	{
       		String types = it.next().toString();
   
        	if(types.equalsIgnoreCase("Percentage")||types.equalsIgnoreCase("Flat"))
    		{
    		  n = rngHelper.roundUp(noOfCampagian,24)/24;
    		}
    		else
    		{
    			n = rngHelper.roundUp(noOfCampagian,6)/6;
            }
    		switch(types)
    		{
    		case "Percentage"   : ctd.createPercentageTD(n, al, cr);break;
    		case "Flat"         : ctd.createFlatTD(n, al, cf);break;
    		case "Freebie"      : ctd.createFreebieTD(n, al, cfb);break;
    		case "Freedelivery" : ctd.createFreedeliveryTD(n, al, cfd);break; 
    		}
        	}
        	
        }
        
        
        if(!(levels.isEmpty())&!(campaignType.isEmpty()))
        {
        	 n = rngHelper.roundUp(noOfCampagian,6)/6;
        	it =	campaignType.iterator();
        	while(it.hasNext())
        	{
        	 String type = it.next().toString();
        	 it1 = levels.iterator();
        	 while(it1.hasNext())
        	 {
        	 String level = it1.next().toString();
        	
    		switch(type)
    		{
    		case "Percentage"   : ctd.createTypeLevelBasedTD(n, al,cr,level);break;
    		case "Flat"         : ctd.createTypeLevelBasedTD(n, al,cf,level);break;
    		case "Freebie"      : ctd.createTypeLevelBasedTD(n, al,cfb,level);break;
    		case "Freedelivery" : ctd.createTypeLevelBasedTD(n, al,cfd,level);break;
    		}
            }
        }
        
		
    }
		return al;

	
	}	
	
	
	public List CreateTD(List campaignType,List levels,int noOfCampagian,boolean isMultiTD) throws IOException
	{
		RngHelper rngHelper = new RngHelper();
		ArrayList al = new ArrayList();
		Iterator it;
		Iterator it1;
		int n;
	
		if (isMultiTD) {
			if (campaignType.isEmpty() & !(levels.isEmpty())) {
				it = levels.listIterator();
				while (it.hasNext()) {
					String level = it.next().toString();
					if (level.equalsIgnoreCase("Restaurant")) {
						n = rngHelper.roundUp(noOfCampagian, 20) / 20;
					} else

					{
						n = rngHelper.roundUp(noOfCampagian, 10) / 10;
					}

					switch (level) {
					case "Restaurant":
						ctd.createLevelBasedTD(n, al, cr, cf, cfb, cfd, level,isMultiTD);
						break;
					case "Category":
						ctd.createLevelBasedTD(n, al, cr, cf, cfb, cfd, level,isMultiTD);
						break;
					case "Subcategory":
						ctd.createLevelBasedTD(n, al, cr, cf, cfb, cfd, level,isMultiTD);
						break;
					case "Item":
						ctd.createLevelBasedTD(n, al, cr, cf, cfb, cfd, level,isMultiTD);
						break;
					}
				}

			}
			if (levels.isEmpty() & !(campaignType.isEmpty())) {

				it = campaignType.listIterator();
				while (it.hasNext()) {
					String types = it.next().toString();

					if (types.equalsIgnoreCase("Percentage") || types.equalsIgnoreCase("Flat")) {
						n = rngHelper.roundUp(noOfCampagian, 20) / 20;
					} else {
						n = rngHelper.roundUp(noOfCampagian, 5) / 5;
					}
					switch (types) {
					case "Percentage":
						ctd.createPercentageTD(n, al, cr,isMultiTD);
						break;
					case "Flat":
						ctd.createFlatTD(n, al, cf,isMultiTD);
						break;
					case "Freebie":
						ctd.createFreebieTD(n, al, cfb,isMultiTD);
						break;
					case "Freedelivery":
						ctd.createFreedeliveryTD(n, al, cfd,isMultiTD);
						break;
					}
				}

			}
			if (!(levels.isEmpty()) & !(campaignType.isEmpty())) {
				n = rngHelper.roundUp(noOfCampagian, 5) / 5;
				it = campaignType.iterator();
				while (it.hasNext()) {
					String type = it.next().toString();
					it1 = levels.iterator();
					while (it1.hasNext()) {
						String level = it1.next().toString();

						switch (type) {
						case "Percentage":
							ctd.createTypeLevelBasedTD(n, al, cr, level,isMultiTD);
							break;
						case "Flat":
							ctd.createTypeLevelBasedTD(n, al, cf, level,isMultiTD);
							break;
						case "Freebie":
							ctd.createTypeLevelBasedTD(n, al, cfb, level,isMultiTD);
							break;
						case "Freedelivery":
							ctd.createTypeLevelBasedTD(n, al, cfd, level);
							break;
						}
					}
				}

			} 
		}else
		{
			
		}
		return al;

	
	}	
	
	
}
