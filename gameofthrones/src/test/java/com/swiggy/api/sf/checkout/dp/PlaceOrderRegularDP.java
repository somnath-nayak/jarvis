package com.swiggy.api.sf.checkout.dp;


import com.swiggy.api.sf.checkout.tests.PlaceOrderRegularTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import java.io.IOException;

public class PlaceOrderRegularDP {

    @DataProvider
    public static Object[][] cartData() {
        return new Object[][]{

//Test case count : 36*7=  252
//Parameter->1.User Agent,2. version-code,3.cart payload,4.index,5.coupon code 6. Trade discount

 //1.create Cart Without Addons &  no coupon & no TD
                {"Swiggy-Android", "310",false,1,null,"noTD"},
                {"Web", "309",false,1,null,"noTD"},
                {"Swiggy-iOS", "234",false,1,null,"noTD"},
 //2.create Cart Without Addons & no coupon & freeDelTD
                 {"Swiggy-Android", "310",false,1,null,"FreeDelTD"},
                 {"Web", "309",false,1,null,"FreeDelTD"},
                 {"Swiggy-iOS", "234",false,1,null,"FreeDelTD"},
 //3.create Cart Without Addons & no coupon & flatTD
                 {"Swiggy-Android", "310",false,1,null,"flatTD"},
                 {"Web", "309",false,1,null,"flatTD"},
                 {"Swiggy-iOS", "234",false,1,null,"flatTD"},
 //4.create Cart Without Addons & with Coupon & no TD
                 {"Swiggy-Android", "310",false,1,"couponCode","noTD"},
                 {"Web", "309",false,1,"couponCode","noTD"},
                 {"Swiggy-iOS", "234",false,1,"couponCode","noTD"},
 //5.create Cart Without Addons & with Coupon & flatTD
                {"Swiggy-Android", "310",false,1,"couponCode","flatTD"},
                {"Web", "309",false,1,"couponCode","flatTD"},
                {"Swiggy-iOS", "234",false,1,"couponCode","flatTD"},
//6.create Cart Without Addons & with Coupon & freeDelTD
                {"Swiggy-Android", "310",false,1,"couponCode","freeDelTD"},
                {"Web", "309",false,1,"couponCode","freeDelTD"},
                {"Swiggy-iOS", "234",false,1,"couponCode","freeDelTD"},
 //7.create Cart With Addons & no coupon & no TD
                {"Swiggy-Android", "310",true,1,null,"noTD"},
                {"Web", "309",true,1,null,"noTD"},
                {"Swiggy-iOS", "234",true,1,null,"noTD"},
 //8.create Cart With Addons & no coupon & freeDelTD
                {"Swiggy-Android", "310",true,1,null,"freeDelTD"},
                {"Web", "309",true,1,null,"freeDelTD"},
                {"Swiggy-iOS", "234",true,1,null,"freeDelTD"},
 //9.create Cart With Addons & no coupon & flatTD
                {"Swiggy-Android", "310",true,1,null,"flatTD"},
                {"Web", "309",true,1,null,"flatTD"},
                {"Swiggy-iOS", "234",true,1,null,"flatTD"},
 //10.create Cart With Addons & Coupon & noTD
                {"Swiggy-Android", "310",true,1,"couponCode","noTD"},
                {"Web", "309",true,1,"couponCode","noTD"},
                {"Swiggy-iOS", "234",true,1,"couponCode","noTD"},
//11.create Cart With Addons & Coupon & freeDelTD
                {"Swiggy-Android", "310",true,1,"couponCode","freeDelTD"},
                {"Web", "309",true,1,"couponCode","freeDelTD"},
                {"Swiggy-iOS", "234",true,1,"couponCode","freeDelTD"},
//12.create Cart With Addons & Coupon & flatTD
                {"Swiggy-Android", "310",true,1,"couponCode","flatTD"},
                {"Web", "309",true,1,"couponCode","flatTD"},
                {"Swiggy-iOS", "234",true,1,"couponCode","flatTD"}
        };
  }


    @Factory(dataProvider="cartData")
    public Object[] createInstances(String userAgent,String versionCode,
                                    boolean isCartWithAddons,int index,String couponCode,String tdType) throws IOException {
        return new Object[] {new PlaceOrderRegularTest(userAgent,versionCode,isCartWithAddons,index,couponCode,tdType)};
    }

}