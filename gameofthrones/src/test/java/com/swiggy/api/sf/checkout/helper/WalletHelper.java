package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.EditTimeConstants;
import com.swiggy.api.sf.checkout.constants.WalletConstants;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.OrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.Validator;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.util.HashMap;

public class  WalletHelper {
	
	String sessionId = null;
	String token = null;
	String tid = null;
	Long serviceableAddress = null;

	Initialize gameofthrones = new Initialize();
	SnDHelper sndHelper = new SnDHelper();
	CMSHelper cmsHelper = new CMSHelper();
	SANDHelper sandHelper = new SANDHelper();
	JsonHelper jsonHelper= new JsonHelper();
	CheckoutHelper helper= new CheckoutHelper();
	EditTimeHelper editHelper = new EditTimeHelper();
    Validator responseValidator;
    EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
    AddressHelper addressHelper=new AddressHelper();
    String balanceBeforeOrder = null;
    String balanceAfterOrder = null;
    String orderKey=null;
    String orderID=null;
    String paymentType=null;
    String orderTotal = null;
    
    public Processor login(String mobile, String password){
        return sandHelper.login(new String[]{mobile,password});
    }
    
    public HashMap<String, String> createReqHeader(String tid, String token){
    	HashMap<String, String> requestheaders = new HashMap<String, String>();
    requestheaders.put("content-type", "application/json");
    requestheaders.put("Tid", tid);
    requestheaders.put("token", token);
    requestheaders.put("version-code", EditTimeConstants.VERSION_CODE);
    requestheaders.put("User-Agent", EditTimeConstants.USER_AGENT);
    requestheaders.put("Authorization", EditTimeConstants.AUTHORIZATION);
    return requestheaders;
}
    
    public Validator paymentTypeValidation(String itemId, String quantity, String restId)
    {
     	 /* Login */ 
    		HashMap<String, String> hashMap=helper.TokenData(EditTimeConstants.mobile1, EditTimeConstants.password1);
       tid=hashMap.get("Tid");
       token=hashMap.get("Token");
       
       /*Cart Creation*/
       
       	Validator cartValidator = updateCartET(tid, token, itemId, quantity, restId).ResponseValidator;
       	editHelper.statusCodeCheck(cartValidator.GetResponseCode());
       	editHelper.cartResCheck(cartValidator.GetBodyAsText(), EditTimeConstants.CART_UPDATED_SUCCESSFULLY);
         
       /* Place order */
         String addressID=editHelper.getAddressIDfromCartResponse(tid, token,cartValidator);
         System.out.println(addressID);
         
         Validator orderValidator= orderPlace(tid, token, addressID, CheckoutConstants.paymentFreecharge, CheckoutConstants.orderComments).ResponseValidator;
         
         orderID=JsonPath.read(orderValidator.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
         System.out.println(orderID);
         orderKey=fetchOrderKey(orderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
         orderKey=orderKey.replace("\"", "").replace("\"", "");
         //this.orderKey=orderKey;
         System.out.println(orderKey);
         return orderValidator;
    }
    
    public Validator placeOrderFreecharge(String tid, String token,String itemId, String quantity, String restId)
    {
   
       /*Cart Creation*/
       
       	Validator cartValidator = updateCartET(tid, token, itemId, quantity, restId).ResponseValidator;
       	editHelper.statusCodeCheck(cartValidator.GetResponseCode());
       	editHelper.cartResCheck(cartValidator.GetBodyAsText(), EditTimeConstants.CART_UPDATED_SUCCESSFULLY);
         
       /* Place order */
         String addressID=editHelper.getAddressIDfromCartResponse(tid, token,cartValidator);
         System.out.println(addressID);
         
         Validator orderValidator= orderPlace(tid, token, addressID, CheckoutConstants.paymentFreecharge, CheckoutConstants.orderComments).ResponseValidator;
         
         orderID=JsonPath.read(orderValidator.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
         System.out.println(orderID);
         
         orderKey=editHelper.fetchOrderKey(orderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
         orderKey=orderKey.replace("\"", "").replace("\"", "");
         //this.orderKey=orderKey;
         System.out.println(orderKey);
         
         orderTotal=JsonPath.read(orderValidator.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
         System.out.println(orderTotal);
         
         return orderValidator;
    }
    
    
    public String fetchOrderKey(String response,String statusMessage)
    {
    	String fetchOrderKeyValue = JsonPath.read(response, "$.data.key").toString().replace("[", "").replace("]", "");
    	return fetchOrderKeyValue;
    }
    
    public String freeChargeChekBalBeforeOrder(String tid,String token)
    {
   	 /* Login */ 
	HashMap<String, String> hashMap=helper.TokenData(EditTimeConstants.mobile1, EditTimeConstants.password1);
   tid=hashMap.get("Tid");
   token=hashMap.get("Token");
   
   	/* Check of current balance */
   Validator chkBalance = helper.freechargeCheckBalance(tid,token).ResponseValidator;
   editHelper.statusCodeCheck(chkBalance.GetResponseCode());
   editHelper.orderResCheck(chkBalance.GetBodyAsText(),WalletConstants.STATUS_CODE ,WalletConstants.EXECUTED_SUCCESSFULLY);
   
   balanceBeforeOrder=JsonPath.read(chkBalance.GetBodyAsText(), "$.data.walletBalance").toString().replace("[", "").replace("]", "");
   System.out.println(balanceBeforeOrder); 
   
   return balanceBeforeOrder;
    }
    
    public String fetchPaymentType(String response,String message)
    {
    		
    		paymentType=JsonPath.read(response,"$.data.order_payment_method").toString().replace("[", "").replace("]", "");
    		
    		return paymentType;
    		
    }
    
    public Validator editOrderCheck(String tid, String token, String type,String itemId, String quantity, String restId,String newItemId,String newquantity)
    {
    		Validator placeOrder = paymentTypeValidation(itemId, quantity, restId) ;  
         orderID=JsonPath.read(placeOrder.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
    
    	 /* Edit order check*/
    	
        Validator editCheckOrderValidator = editHelper.editOrderCheckET(Long.valueOf(orderID),type,newItemId,newquantity,restId).ResponseValidator;
        System.out.println(editCheckOrderValidator.GetBodyAsText());
        editHelper.statusCodeCheck(editCheckOrderValidator.GetResponseCode());
        return editCheckOrderValidator;
    }
    
    
    public Validator editOrderConfirm(String tid, String token,String type,String itemId, String quantity, String restId,String newItemId,String newquantity)
    {
    	
    		Validator editCheckOrderValidator = editOrderCheck(tid, token, type,itemId, quantity, restId, newItemId, newquantity);
    	/* Edit order confirm*/
        Validator editConfirmOrderValidator = editHelper.editOrderCheckConfirm(Long.valueOf(orderID),type,newItemId,newquantity, restId).ResponseValidator;
        System.out.println(editConfirmOrderValidator.GetBodyAsText());
        editHelper.statusCodeCheck(editConfirmOrderValidator.GetResponseCode());
       
        Validator cancelorder = cancelOrder(orderID).ResponseValidator;
        return editConfirmOrderValidator;
    }
    
    public Validator getSingleOrder(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	Validator placeOrder = paymentTypeValidation(itemId, quantity, restId) ; 
    orderID=JsonPath.read(placeOrder.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
    	Processor getSingleOrder=helper.getSingleOrder(orderID, tid, token);
    	System.out.println(getSingleOrder);
    	editHelper.statusCodeCheck(getSingleOrder.ResponseValidator.GetResponseCode());
    	editHelper.orderResCheck(getSingleOrder.ResponseValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
    	Validator getSingleOrderV = getSingleOrder.ResponseValidator;
    	Validator cancelorder = cancelOrder(orderID).ResponseValidator;
    	return getSingleOrderV;
    }
    
    public Validator getSingleOrderKey(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    		Validator placeOrder = paymentTypeValidation(itemId, quantity, restId) ; 
    	 	Validator  orderKeyValidator=helper.getSingleOrderKey(orderKey,tid,token).ResponseValidator;
	   	System.out.println(orderKeyValidator);
	   	editHelper.statusCodeCheck(orderKeyValidator.GetResponseCode());
	   	editHelper.orderResCheck(orderKeyValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
	   	Validator cancelorder = cancelOrder(orderID).ResponseValidator;
	    return orderKeyValidator;
    }
    
    public Validator getLastOrderFG(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
     	Validator placeOrder = paymentTypeValidation(itemId, quantity, restId) ; 
     	Validator  lastOrderValidator=helper.getLastOrder(tid,token).ResponseValidator;
	   	System.out.println(lastOrderValidator);
	   	editHelper.statusCodeCheck(lastOrderValidator.GetResponseCode());
	   	editHelper.orderResCheck(lastOrderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
	   	Validator cancelorder = cancelOrder(orderID).ResponseValidator;
    return lastOrderValidator;
    }
    
    public String getOrderFG(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	Validator placeOrder = paymentTypeValidation(itemId, quantity, restId) ; 
    Validator  getOrderValidator=helper.getOrder(tid,token).ResponseValidator;
    System.out.println(getOrderValidator);
 	String getInititaionCheck = editHelper.getOrderCheck(getOrderValidator.GetBodyAsText(), orderID);
 	editHelper.statusCodeCheck(getOrderValidator.GetResponseCode());
 	editHelper.orderResCheck(getOrderValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
 	Validator cancelorder = cancelOrder(orderID).ResponseValidator;
    return getInititaionCheck;
    }
    
    public Processor cancelOrder(String orderId){
    	 HashMap<String, String> requestheaders=editHelper.createReqHeader(tid, token);
        String[] payloadparams = {EditTimeConstants.CANCELLATION_REASON , EditTimeConstants.CANCELLATION_FEE_APPLICABILITY, EditTimeConstants.CANCELLATION_FEE, EditTimeConstants.CANCELLATION_REASON};
        String[] urlParams = new String[]{orderId};
        GameOfThronesService service = new GameOfThronesService("checkout", "ordercancel", gameofthrones);
        Processor processor = new Processor(service, requestheaders, payloadparams, urlParams);
        return  processor;
    }
    
    public Processor cancelAfterPlaceOrder(String tid, String token,String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage){
		
    	//Validator orderId = cancelOrder(orderID).ResponseValidator;
    	
    	return cancelOrder(String.valueOf(orderID));
    
    }
    

    public Processor getCancelOrder(String tid, String token,String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage){
    	  
    	Validator placeOrder = paymentTypeValidation(itemId, quantity, restId) ;  
    	orderID = JsonPath.read(placeOrder.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
    System.out.println(orderID);
    return cancelOrder(String.valueOf(orderID));
    }
    
    public Validator getCloneOrderCheck(String tid, String token,String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage){
       
    		Processor cancelOrder = getCancelOrder(tid, token, type, itemId, quantity, restId, newItemId, newquantity ,cartEditStatusCode , cartEditmessage,cartConfirmStatusCode,cartConfirmMessage);
        OrderResponse orderResponse = Utility.jsonDecode(cancelOrder.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        String orderId = orderResponse.getData().getOrderId().toString();
        /*Clone Order Check*/
        Processor cloneOrderCheck = cloneOrderCheck(String.valueOf(orderId),type,itemId,quantity,restId);
        System.out.println(cloneOrderCheck);
    		editHelper.statusCodeCheck(cloneOrderCheck.ResponseValidator.GetResponseCode());
    		editHelper.orderResCheck(cloneOrderCheck.ResponseValidator.GetBodyAsText(), EditTimeConstants.CART_UPDATED_SUCCESSFULLY);
    		Validator cloneOrderChk = cloneOrderCheck.ResponseValidator;
    		return cloneOrderChk;
    }
    		
    public Validator getCloneOrderConfirm(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage){
       
    		Validator clonecheck = getCloneOrderCheck(tid, token, type, itemId, quantity, restId, newItemId, newquantity ,cartEditStatusCode , cartEditmessage,cartConfirmStatusCode,cartConfirmMessage);
    		orderID=JsonPath.read(clonecheck.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
    		System.out.println(orderID);
        /*Clone Order Confirm*/
        Processor cloneOrderConfirm = cloneOrderCheck(String.valueOf(orderID),type,itemId,quantity,restId);
        System.out.println(cloneOrderConfirm);
    		editHelper.statusCodeCheck(cloneOrderConfirm.ResponseValidator.GetResponseCode());
    		editHelper.orderResCheck(cloneOrderConfirm.ResponseValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
    		Validator cloneOrderCfm = cloneOrderConfirm.ResponseValidator;
    		return cloneOrderCfm;
    }


    public Processor cloneOrderCheck(String orderId,String init_source,String menu_item_id,String quantity,String restId){
    	HashMap<String, String> requestheaders = editHelper.createReqHeader("EditTimeConstants.AUTHORIZATION", "null");
    	GameOfThronesService service = new GameOfThronesService("checkout", "cloneordercheck", gameofthrones);
    	String[] payloadparams = {String.valueOf(orderId),init_source,menu_item_id,quantity,restId};
    return new Processor(service, requestheaders, payloadparams, null); }
    
    
    public Processor cloneOrderConfirm(String orderId,String init_source,String menu_item_id,String quantity,String restId){
    	HashMap<String, String> requestheaders = editHelper.createReqHeader("EditTimeConstants.AUTHORIZATION", "null");
    	GameOfThronesService service = new GameOfThronesService("checkout", "cloneordercheck", gameofthrones);
    	String[] payloadparams = {String.valueOf(orderId),init_source,menu_item_id,quantity,restId};
    return new Processor(service, requestheaders, payloadparams, null); }
    


    
    public Processor updateCartET(String tid,String token,String itemId, String quantity, String restId) {
		GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2", gameofthrones);
		 HashMap<String, String> requestheaders=createReqHeader(tid, token);
		 System.out.println("#####" + requestheaders);
		 String[] payloadparams = {itemId,quantity,restId};
		return new Processor(service, requestheaders, payloadparams);
	}
    
	public Processor orderPlace(String tid, String token,String addressId, String payment, String comments)
	{
		HashMap<String, String> requestheaders=createReqHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
		String []payloadparams = {String.valueOf(addressId),payment,comments};
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}
	
	
	public Processor loginTokenFreecharge(String tid,String token)
    {
		 /* Login */ 
		HashMap<String, String> hashMap=helper.TokenData(EditTimeConstants.mobile1, EditTimeConstants.password1);
	    tid=hashMap.get("Tid");
	    token=hashMap.get("Token");
	   
	    GameOfThronesService service = new GameOfThronesService("checkout", "freechargelogintokenv1", gameofthrones);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");        
        requestheaders.put("tid", tid);
        requestheaders.put("token", token);
        Processor processor = new Processor(service, requestheaders);
        return processor;
    }
	
	
	public Processor withDrawAmountFreecharge(String tid, String token,String amount,String orderId)
    {
		HashMap<String, String> requestheaders=createReqHeader(tid, token);
		GameOfThronesService service = new GameOfThronesService("checkout", "freechargewithdrawamountv1", gameofthrones);
        String[] payloadparams = new String[]{amount,orderId};
        Processor processor = new Processor(service, requestheaders,payloadparams);
        return processor;
    }

    public Validator withdrawAmountCheck(String tid, String token, String type,String itemId, String quantity, String restId,String newItemId,String newquantity)
    {
    	
    		Validator placeOrder = placeOrderFreecharge(tid,token,itemId, quantity, restId) ;  
        System.out.println(placeOrder);
         
         
         /* Withdrqa amount after place order */   
    	    Validator wdcheck = withDrawAmountFreecharge(tid,token,orderTotal,orderID).ResponseValidator;
        System.out.println(wdcheck.GetBodyAsText());
        editHelper.statusCodeCheck(wdcheck.GetResponseCode());
        
         	
         return wdcheck;

    }
    
}
    

    