package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class MENULET {

    private String type;
    private String imageId;
    private String title;
    private String subTitle;
    private String backgroundImageId;
    private String mainPageDescription;
    private List<String> layoutVariables = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public MENULET() {
    }

    /**
     *
     * @param subTitle
     * @param title
     * @param imageId
     * @param backgroundImageId
     * @param layoutVariables
     * @param type
     * @param mainPageDescription
     */
    public MENULET(String type, String imageId, String title, String subTitle, String backgroundImageId, String mainPageDescription, List<String> layoutVariables) {
        super();
        this.type = type;
        this.imageId = imageId;
        this.title = title;
        this.subTitle = subTitle;
        this.backgroundImageId = backgroundImageId;
        this.mainPageDescription = mainPageDescription;
        this.layoutVariables = layoutVariables;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getBackgroundImageId() {
        return backgroundImageId;
    }

    public void setBackgroundImageId(String backgroundImageId) {
        this.backgroundImageId = backgroundImageId;
    }

    public String getMainPageDescription() {
        return mainPageDescription;
    }

    public void setMainPageDescription(String mainPageDescription) {
        this.mainPageDescription = mainPageDescription;
    }

    public List<String> getLayoutVariables() {
        return layoutVariables;
    }

    public void setLayoutVariables(List<String> layoutVariables) {
        this.layoutVariables = layoutVariables;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("imageId", imageId).append("title", title).append("subTitle", subTitle).append("backgroundImageId", backgroundImageId).append("mainPageDescription", mainPageDescription).append("layoutVariables", layoutVariables).toString();
    }

}