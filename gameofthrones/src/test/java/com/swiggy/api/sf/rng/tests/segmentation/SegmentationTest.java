package com.swiggy.api.sf.rng.tests.segmentation;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.sf.rng.constants.SegmentationConstants;
import com.swiggy.api.sf.rng.dp.Segmentation.SegmentationDP;
import com.swiggy.api.sf.rng.helper.RmqHelper;
import com.swiggy.api.sf.rng.helper.SegmentationHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.ads.Segmentation.OrderCancelMessage;
import com.swiggy.api.sf.rng.pojo.ads.Segmentation.OrderCreateMessage;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.TimeoutException;

public class SegmentationTest {
    
    SegmentationHelper segmentationHelper = new SegmentationHelper();
    WireMockHelper wireMockHelper = new WireMockHelper();
    SoftAssert softAssert = new SoftAssert();
    RmqHelper rmqHelper = new RmqHelper(SegmentationConstants.segmentationRMQUser,SegmentationConstants.getSegmentationRMQPwd);
    
    
    @Test(dataProvider = "checkDataInDBForOrderStatsAndOrderCancelDP", dataProviderClass = SegmentationDP.class, groups = {"segmentationCase"},description = "Checking after order cancel user order and payment related count is updated(decremented) in DB")
    public void checkDataInDBForOrderStatsAndOrderCancelTest(
            OrderCreateMessage payload,
            OrderCancelMessage payloadTwo, String userId, String orderId, String restId) throws ParseException, IOException, TimeoutException {
    
        rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payload);
        
        // Checkout response mock
        segmentationHelper.stubCheckoutOrderResponse(userId, orderId, restId, SegmentationConstants.cartType[2], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), "refund-initiated", "cancel");
    
        rmqHelper.pushMessageToExchange("segmentation", "rng_order_cancel", new AMQP.BasicProperties().builder().contentType("application/json"), payloadTwo);
        
        //get CartType All Order Counts Of User
        Processor cartTypeNPaymentMethodOrderCount = segmentationHelper.getCartTypeAllOrderCountsOfUser(userId);
        int orderCountOfCart = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.orderCounts.Pop");
        int orderCountByPaymentMethod = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.totalCountBO.orderCountByPaymentMethod.PayTM-SSO");
        //get User Order Count By Payment Method
        Processor responseOfOrderCountPaymentMethod = segmentationHelper.getUserOrderCountByPaymentMethod(userId, true);
        int paymentMethodOrderCount = responseOfOrderCountPaymentMethod.ResponseValidator.GetNodeValueAsInt("$.data.orderCountByPaymentMethod.PayTM-SSO");
        String lastOrderTime = responseOfOrderCountPaymentMethod.ResponseValidator.GetNodeValue("$.data.lastOrderTime");
        
        //get User Restaurant Last Order Map
        String[] restaurantIds = {restId};
        Processor responseOfRestLastOrderMap = segmentationHelper.getUserRestaurantLastOrderMap(userId, restaurantIds);
        String userLastRestOrderMap = responseOfRestLastOrderMap.ResponseValidator.GetNodeValue("$.data.restaurantLastOrderMap." + restId);
        //get User all OrderCount
        Processor respUserOrderCount = segmentationHelper.getUserOrderCount(userId);
        int userOrdersCount = respUserOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.orderCount");
        String orderCountByPaymentMethodInUserOrderCount = respUserOrderCount.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orderCountByPaymentMethod");
        
        softAssert.assertEquals(userLastRestOrderMap, null);
        softAssert.assertEquals(orderCountOfCart, 0);
        softAssert.assertEquals(orderCountByPaymentMethod, 0);
        softAssert.assertEquals(paymentMethodOrderCount, 0);
        softAssert.assertEquals(lastOrderTime, null);
        softAssert.assertEquals(userOrdersCount, 0);
        softAssert.assertEquals(orderCountByPaymentMethodInUserOrderCount, "{}");
        softAssert.assertAll();
    }
    
    
    @Test(dataProvider = "getCartTypePOPOrderCountByUserIdDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = " checking Cart type POP order count")
    public void getCartTypePOPOrderCountByUserIdTest(OrderCreateMessage payload, String userId) throws IOException, TimeoutException {
    
        rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payload);
        Processor cartTypeNPaymentMethodOrderCount = segmentationHelper.getCartTypeAllOrderCountsOfUser(userId);
        int orderCountOfCart = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.orderCounts.Pop");
        int orderCountByPaymentMethod = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.totalCountBO.orderCountByPaymentMethod.PayTM-SSO");
        softAssert.assertEquals(orderCountOfCart, 1);
        softAssert.assertEquals(orderCountByPaymentMethod, 1);
        softAssert.assertAll();
    }
    
    @Test(dataProvider = "getMultipleCartTypeCAFEAndPOPOrderCountByUserIdDP", dataProviderClass = SegmentationDP.class, groups = {"segmentationCase"},  description = "Checking Multiple cart type order count ")
    public void getMultipleCartTypeCAFEAndPOPOrderCountByUserIdTest(OrderCreateMessage payload, OrderCreateMessage payloadSecond, String userId, String orderSecondId, String restId) throws IOException, TimeoutException {
        
        // Push message to queue for cart type CAFE
        rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payload);
        
        //Mock checkout response for Order type POP
        segmentationHelper.stubCheckoutOrderResponse(userId, orderSecondId, restId, SegmentationConstants.cartType[2], SegmentationConstants.paymentMethod[0], Utility.getDate(), SegmentationConstants.paymentTxnStatus[0], SegmentationConstants.orderStatus[0]);
        
        // Push message to queue for cart type POP
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payloadSecond);
        
        Processor cartTypeNPaymentMethodOrderCount = segmentationHelper.getCartTypeAllOrderCountsOfUser(userId);
        int orderCountOfCAFECart = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.orderCounts.Cafe");
        int orderCountOfPOPCart = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.orderCounts.Pop");
        int orderCountByPaymentMethod = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.totalCountBO.orderCountByPaymentMethod.PayTM-SSO");
        softAssert.assertEquals(orderCountOfCAFECart, 1);
        softAssert.assertEquals(orderCountOfPOPCart, 1);
        softAssert.assertEquals(orderCountByPaymentMethod, 2);
        softAssert.assertAll();
    }
    
    
    @Test(dataProvider = "getCartTypeOrderCountForNewUserIdDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "checking cart type order for new user")
    public void getCartTypeOrderCountForNewUserIdTest(String userId) throws IOException {
        
        Processor cartTypeNPaymentMethodOrderCount = segmentationHelper.getCartTypeAllOrderCountsOfUser(userId);
        int dataId = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.id");
        int orderCount = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.orderCount");
        String customerId = Integer.toString(cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.customerId"));
        int firstOrderCityId = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.firstOrderCityId");
        int lastOrderCityId = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValueAsInt("$.data.lastOrderCityId");
        String lastOrderTime = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValue("$.data.lastOrderTime");
        String orderCountByPaymentMethod = cartTypeNPaymentMethodOrderCount.ResponseValidator.GetNodeValue("$.data.orderCountByPaymentMethod");
        
        softAssert.assertEquals(orderCountByPaymentMethod, null);
        softAssert.assertEquals(lastOrderTime, null);
        softAssert.assertEquals(lastOrderCityId, 0);
        softAssert.assertEquals(firstOrderCityId, 0);
        softAssert.assertEquals(dataId, 0);
        softAssert.assertEquals(orderCount, -1);
        softAssert.assertEquals(customerId, userId);
        softAssert.assertAll();
    }
    
    
    @Test(dataProvider = "getOrderCountByPaymentMethodByUserIdDP", dataProviderClass = SegmentationDP.class, groups = {"segmentationCase"},description = "Checking order count by payment method")
    public void getOrderCountByPaymentMethodByUserIdTest(OrderCreateMessage payload, OrderCreateMessage payloadSecond, String userId, String orderSecondId, String restId) throws IOException, TimeoutException {
        
        // Push message to queue for Payment type PayTM
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payload);
        
        //Mock checkout response for Payment type PhonePe
        segmentationHelper.stubCheckoutOrderResponse(userId, orderSecondId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[8], Utility.getDate(), SegmentationConstants.paymentTxnStatus[0], SegmentationConstants.orderStatus[0]);
        
        // Push message to queue for Payment type PhonePe
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payloadSecond);
        
        Processor OrderCountByPaymentMethod = segmentationHelper.getUserOrderCountByPaymentMethod(userId, true);
        int orderCountByPaymentMethodPayTM = OrderCountByPaymentMethod.ResponseValidator.GetNodeValueAsInt("$.data.orderCountByPaymentMethod.PayTM-SSO");
        int orderCountByPaymentMethodPhonePay = OrderCountByPaymentMethod.ResponseValidator.GetNodeValueAsInt("$.data.orderCountByPaymentMethod.PhonePe");
        int totalOrderCount = OrderCountByPaymentMethod.ResponseValidator.GetNodeValueAsInt("$.data.orderCount");
        String customerId = Integer.toString(OrderCountByPaymentMethod.ResponseValidator.GetNodeValueAsInt("$.data.customerId"));
        softAssert.assertEquals(orderCountByPaymentMethodPayTM, 1);
        softAssert.assertEquals(orderCountByPaymentMethodPhonePay, 1);
        softAssert.assertEquals(totalOrderCount, 2);
        softAssert.assertEquals(customerId, userId);
        softAssert.assertAll();
    }
    
    @Test(dataProvider = "getOrderCountWhenPaymentMethodIsFalseByUserIdDP", dataProviderClass = SegmentationDP.class, groups = {"segmentationCase"}, description = "Checking order count when payment method is false")
    public void getOrderCountWhenPaymentMethodIsFalseByUserIdTest(OrderCreateMessage payload, OrderCreateMessage payloadSecond, String userId, String orderSecondId, String restId) throws IOException, TimeoutException {
        
        // Push message to queue for Payment type PayTM
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payload);
        
        //Mock checkout response for Payment type PhonePe
        segmentationHelper.stubCheckoutOrderResponse(userId, orderSecondId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[8], Utility.getDate(), SegmentationConstants.paymentTxnStatus[0], SegmentationConstants.orderStatus[0]);
        
        // Push message to queue for Payment type PhonePe
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payloadSecond);
        
        Processor OrderCountByPaymentMethod = segmentationHelper.getUserOrderCountByPaymentMethod(userId, false);
        String orderCountByPaymentMethodInResponse = OrderCountByPaymentMethod.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orderCountByPaymentMethod");
        int totalOrderCount = OrderCountByPaymentMethod.ResponseValidator.GetNodeValueAsInt("$.data.orderCount");
        String customerId = Integer.toString(OrderCountByPaymentMethod.ResponseValidator.GetNodeValueAsInt("$.data.customerId"));
        softAssert.assertEquals(orderCountByPaymentMethodInResponse, "{}");
        softAssert.assertEquals(totalOrderCount, 2);
        softAssert.assertEquals(customerId, userId);
        softAssert.assertAll();
    }
    
    @Test(dataProvider = "getUserRestaurantLastOrderByUserIdDP", dataProviderClass = SegmentationDP.class, groups = {"segmentationCase"}, description = "Checking user last restaurant order")
    public void getUserRestaurantLastOrderByUserIdTest(OrderCreateMessage payload, OrderCreateMessage payloadSecond, String userId, String orderSecondId, String[] restId, String orderTime) throws IOException, TimeoutException {
        
        // Push message to queue for Payment type PayTM
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payload);
        
        //Mock checkout response for Payment type PhonePe
        segmentationHelper.stubCheckoutOrderResponse(userId, orderSecondId, restId[0], SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[8], Utility.getDate(), SegmentationConstants.paymentTxnStatus[0], SegmentationConstants.orderStatus[0]);
        
        // Push message to queue for Payment type PhonePe
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payloadSecond);
        
        Processor response = segmentationHelper.getUserRestaurantLastOrderMap(userId, restId);
       segmentationHelper.userRestaurantLastOrderAssertionCheck(response , restId[0] , orderTime);

    }
    
    // new user
    @Test(dataProvider = "getNewUserRestaurantLastOrderByUserIdDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking new user restaurant last order")
    public void getNewUserRestaurantLastOrderByUserIdTest(String userId, String[] restId) throws IOException {
        
        Processor response = segmentationHelper.getUserRestaurantLastOrderMap(userId, restId);
        segmentationHelper.userRestaurantLastOrderAssertionCheck(response , restId[0] , "null");

    }
    
    @Test(dataProvider = "getUserRestaurant30daysOrderByUserIdDP", dataProviderClass = SegmentationDP.class, groups = {"segmentationCase"}, description = "Checking 30days dormant type user")
    public void getUserRestaurant30daysOrderByUserIdTest(OrderCreateMessage payloadSecond, String userId, String orderSecondId, String[] restId, String orderTime) throws IOException, TimeoutException {
        
        //Mock checkout response for Payment type PhonePe
        segmentationHelper.stubCheckoutOrderResponse(userId, orderSecondId, restId[0], SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[8], orderTime, SegmentationConstants.paymentTxnStatus[0], SegmentationConstants.orderStatus[0]);
        
        // Push message to queue for Payment type PhonePe
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payloadSecond);
        
        Processor response = segmentationHelper.getUserRestaurantLastOrderMap(userId, restId);
        segmentationHelper.userRestaurantLastOrderAssertionCheck(response , restId[0] , orderTime);

    }
    
    @Test(dataProvider = "getUserRestaurant60daysOrderByUserIdDP", dataProviderClass = SegmentationDP.class, groups = {"segmentationCase"},description = "checking 60 days dormant type user")
    public void getUserRestaurant60daysOrderByUserIdTest(OrderCreateMessage payloadSecond, String userId, String orderSecondId, String[] restId, String orderTime) throws IOException, TimeoutException {
        
        //Mock checkout response for Payment type PhonePe
        segmentationHelper.stubCheckoutOrderResponse(userId, orderSecondId, restId[0], SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[8], orderTime, SegmentationConstants.paymentTxnStatus[0], SegmentationConstants.orderStatus[0]);
        
        // Push message to queue for Payment type PhonePe
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payloadSecond);
        
        Processor response = segmentationHelper.getUserRestaurantLastOrderMap(userId, restId);
        segmentationHelper.userRestaurantLastOrderAssertionCheck(response , restId[0] , orderTime);

    }
    
    @Test(dataProvider = "getUserRestaurant90daysOrderByUserIdDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking 90days dormant type user order")
    public void getUserRestaurant90daysOrderByUserIdTest(OrderCreateMessage payloadSecond, String userId, String orderSecondId, String[] restId, String orderTime) throws IOException, TimeoutException {
        
        //Mock checkout response for Payment type PhonePe
        segmentationHelper.stubCheckoutOrderResponse(userId, orderSecondId, restId[0], SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[8], orderTime, SegmentationConstants.paymentTxnStatus[0], SegmentationConstants.orderStatus[0]);
        
        // Push message to queue for Payment type PhonePe
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payloadSecond);
        
        Processor response = segmentationHelper.getUserRestaurantLastOrderMap(userId, restId);
        segmentationHelper.userRestaurantLastOrderAssertionCheck(response , restId[0] , orderTime);

    }
    
    // user id ZERO
    @Test(dataProvider = "getUserRestaurantLastOrderByUserIdAsZeroDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking with the user id as ZERO")
    public void getUserRestaurantLastOrderByUserIdAsZeroTest(String userId, String[] restId) throws IOException {
        
        Processor userRestaurantLastOrder = segmentationHelper.getUserRestaurantLastOrderMap(userId, restId);
        String restLastOrderTimeInResp = userRestaurantLastOrder.ResponseValidator.GetNodeValue("$.data");
        String statusMessage = userRestaurantLastOrder.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(restLastOrderTimeInResp, null);
        softAssert.assertEquals(statusMessage, "user Id cannot be 0 or null");
        softAssert.assertAll();
    }
    
    // restaurant id  & user id both are ZERO
    @Test(dataProvider = "getUserRestaurantLastOrderByRestaurantIdAndUserIdBothAsZeroDP", dataProviderClass = SegmentationDP.class, groups = {"segmentationCase"}, description = "hitting with the rest id and user id both as ZERO")
    public void getUserRestaurantLastOrderByRestaurantIdAndUserIdBothAsZeroTest(String userId, String[] restId) throws IOException {
        String restaurantId = restId[0] + "=";
        Processor userRestaurantLastOrder = segmentationHelper.getUserRestaurantLastOrderMap(userId, restId);
        String statusCode = userRestaurantLastOrder.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        String statusMessage = userRestaurantLastOrder.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(statusMessage, "user Id cannot be 0 or null");
        softAssert.assertNotEquals(statusCode, 0);
        
        softAssert.assertAll();
    }
    
    // User segment for the non existing user id
    @Test(dataProvider = "getUserSegmentDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking user segment")
    public void getUserSegmentTest(String userId) throws IOException {
        
        Processor userSegment = segmentationHelper.getUserSegment(userId);
        String id = userSegment.ResponseValidator.GetNodeValue("$.data.id");
        String valueSegment = userSegment.ResponseValidator.GetNodeValue("$.data.value_segment");
        String marketingSegment = userSegment.ResponseValidator.GetNodeValue("$.data.marketing_segment");
        String premiumSegment = userSegment.ResponseValidator.GetNodeValue("$.data.premium_segment");
        String igccSegment = userSegment.ResponseValidator.GetNodeValue("$.data.igcc_segment");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(id, null);
        softAssert.assertEquals(valueSegment, "L");
        softAssert.assertEquals(marketingSegment, "D");
        softAssert.assertEquals(premiumSegment, null);
        softAssert.assertEquals(igccSegment, null);
        
        softAssert.assertAll();
    }
    
    @Test(dataProvider = "getUserIGCCSegmentMediumDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking IGCC segment")
    public void getUserIGCCSegmentMediumTest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[2], SegmentationConstants.marketingSegment[1], SegmentationConstants.premiumSegment[0]);

    }
    
    @Test(dataProvider = "getUserIGCCSegmentNADP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"},  description = "Checking IGCC segment")
    public void getUserIGCCSegmentNATest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[3], SegmentationConstants.marketingSegment[1], SegmentationConstants.premiumSegment[0]);

    }
    
    @Test(dataProvider = "getUserIGCCSegmentNULLDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking IGCC segment")
    public void getUserIGCCSegmentNULLTest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[4], SegmentationConstants.marketingSegment[1], SegmentationConstants.premiumSegment[0]);

    }
    
    @Test(dataProvider = "getUserIGCCSegmentAsEmptyStringDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checkign IGCC segment")
    public void getUserIGCCSegmentAsEmptyStringTest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[0], "", SegmentationConstants.marketingSegment[1], SegmentationConstants.premiumSegment[0]);

    }
    
    @Test(dataProvider = "getUserMarketingSegmentAsBDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking marketing segment")
    public void getUserMarketingSegmentAsBTest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[4], SegmentationConstants.marketingSegment[1], SegmentationConstants.premiumSegment[1]);

    }
    
    @Test(dataProvider = "getUserMarketingSegmentAsNULLDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking Marketing segment")
    public void getUserMarketingSegmentAsNULLTest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[4], SegmentationConstants.marketingSegment[4], SegmentationConstants.premiumSegment[1]);

    }
    
    @Test(dataProvider = "getUserValueSegmentAsMDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking value segment")
    public void getUserValueSegmentAsMTest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[2], SegmentationConstants.igccSegment[1], SegmentationConstants.marketingSegment[2], SegmentationConstants.premiumSegment[1]);
    }
    
    @Test(dataProvider = "getUserValueSegmentAsNADP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking value segment")
    public void getUserValueSegmentAsNATest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[3], SegmentationConstants.igccSegment[1], SegmentationConstants.marketingSegment[2], SegmentationConstants.premiumSegment[1]);

    }
    
    @Test(dataProvider = "getUserValueSegmentAsNULLDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking value segment")
    public void getUserValueSegmentAsNULLTest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[4], SegmentationConstants.igccSegment[1], SegmentationConstants.marketingSegment[2], SegmentationConstants.premiumSegment[1]);
    }
    
    @Test(dataProvider = "getUserMarketingSegmentAsEmptyStringDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking marketing segment")
    public void getUserMarketingSegmentAsEmptyStringTest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[4], "", SegmentationConstants.premiumSegment[1]);
    }
    
    // Premium segment as 1 , premium segment zero is covered in above test cases
    @Test(dataProvider = "getUserMarketingSegmentAsPremiumSegmentAsOneDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking marketing segment")
    public void getUserMarketingSegmentAsPremiumSegmentAsOneTest(String userId) throws IOException {
        Processor response = segmentationHelper.getUserSegment(userId);
        segmentationHelper.userSegmentAssertionCheck(response , userId , SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[0], SegmentationConstants.marketingSegment[2], SegmentationConstants.premiumSegment[1]);
    }

    
    @Test(dataProvider = "getUserOrdersDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "checking total order count of user")
    public void getUserOrdersTest(OrderCreateMessage payload, OrderCreateMessage payloadSecond, String userId, String orderSecondId, String[] restId) throws IOException, TimeoutException {
        
        // Push message to queue for Payment type PayTM
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payload);
        
        //Mock checkout response for Payment type PhonePe
        segmentationHelper. stubCheckoutOrderResponse(userId, orderSecondId, restId[0], SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[8], Utility.getDate(), SegmentationConstants.paymentTxnStatus[0], SegmentationConstants.orderStatus[0]);
        
        // Push message to queue for Payment type PhonePe
       rmqHelper.pushMessageToExchange("segmentation", "segmentation_order_stats", new AMQP.BasicProperties().builder().contentType("application/json"), payloadSecond);
        
        Processor response = segmentationHelper.getUserOrderCount(userId);
        segmentationHelper.userOrderCountAssertionCheck(response, "2", userId);

    }
    
    @Test(dataProvider = "getUserOrdersCountWhenNoOrderPresentForUserDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking order count of user when no order is present ")
    public void getUserOrdersCountWhenNoOrderPresentForUserTest(String userId) throws IOException {
        
        Processor response = segmentationHelper.getUserOrderCount(userId);
        segmentationHelper.userOrderCountAssertionCheck(response,"0" , userId );

    }
    
    @Test(dataProvider = "getUserDeviceIdDP", dataProviderClass = SegmentationDP.class, description = "Checking new user with device id")
    public void getUserDeviceIdTest(String deviceId) throws IOException {
        
        Processor response = segmentationHelper.getNewUserByDeviceId(deviceId);
        segmentationHelper.newUserByDeviceAssertionCheck( response ,0, 1);

    }
    
    // device id is not present in DB so new user is TRUE
    @Test(dataProvider = "getNewUserDeviceIdDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking new user with device id")
    public void getNewUserDeviceIdTest(String deviceId) throws IOException {
        Processor response = segmentationHelper.getNewUserByDeviceId(deviceId);
        segmentationHelper.newUserByDeviceAssertionCheck(response , 1,1);

    }
    
    // Multiple device id is present in DB for same user id then new_user is  false
    @Test(dataProvider = "getNewUserDeviceIdForAUserIdWithMultipleDeviceIdDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking new user with device id")
    public void getNewUserDeviceIdForAUserIdWithMultipleDeviceIdTest(String deviceId, String deviceIdTwo) throws IOException {
        
        Processor responseForDeviceOne = segmentationHelper.getNewUserByDeviceId(deviceId);
        segmentationHelper.newUserByDeviceAssertionCheck(responseForDeviceOne, 0, 1);
        
        Processor responseForDeviceTwoResponse = segmentationHelper.getNewUserByDeviceId(deviceIdTwo);
        segmentationHelper.newUserByDeviceAssertionCheck(responseForDeviceTwoResponse, 0, 1);
    }
    
    // Multiple user id is present in DB for same device id then new_user is  false
    @Test(dataProvider = "getNewUserDeviceIdForADeviceIdWithMultipleUserIdDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking new user with device id")
    public void getNewUserDeviceIdForADeviceIdWithMultipleUserIdTest(String deviceId) throws IOException {
        Processor response = segmentationHelper.getNewUserByDeviceId(deviceId);
        segmentationHelper.newUserByDeviceAssertionCheck(response, 0, 1);

    }
    
    // restaurant id ZERO
    @Test(dataProvider = "getUserRestaurantLastOrderByRestaurantIdAsZeroDP", dataProviderClass = SegmentationDP.class,groups = {"segmentationCase"}, description = "Checking with the restaurant id ZERO")
    public void getUserRestaurantLastOrderByRestaurantIdAsZeroTest(String userId, String[] restId) throws IOException {
        Processor userRestaurantLastOrder = segmentationHelper.getUserRestaurantLastOrderMap(userId, restId);
        String restLastOrderTimeInResp = userRestaurantLastOrder.ResponseValidator.GetNodeValue("$.data.restaurantLastOrderMap." + restId[0]);
        String statusMessage = userRestaurantLastOrder.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(restLastOrderTimeInResp, null);
        softAssert.assertEquals(statusMessage, null, "Need to Fix --> For restaurant id zero getting null instead of some error message");
        softAssert.assertAll();
    }
    
}
