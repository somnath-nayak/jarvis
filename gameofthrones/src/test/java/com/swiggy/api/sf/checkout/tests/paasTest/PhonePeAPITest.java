package com.swiggy.api.sf.checkout.tests.paasTest;

import com.swiggy.api.sf.checkout.constants.PaasConstant;
import com.swiggy.api.sf.checkout.helper.paas.MySmsValidator;
import com.swiggy.api.sf.checkout.helper.paas.PaasWalletValidator;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

public class PhonePeAPITest {

    PaasWalletValidator paasWalletValidator = new PaasWalletValidator();
    MySmsValidator mySmsValidator = new MySmsValidator();

    String mobile = System.getenv("mobile");
    String password = System.getenv("password");

    String tid;
    String token;
    String authToken;

    @BeforeClass
    public void login() {
        if (mobile == null || mobile.isEmpty()) {
            mobile = PaasConstant.MOBILE;
        }
        if (password == null || password.isEmpty()) {
            password = PaasConstant.PASSWORD;
        }

        HashMap<String, String> hashMap = paasWalletValidator.loginTokenData(mobile, password);
        tid = hashMap.get("tid");
        token = hashMap.get("token");
        authToken = mySmsValidator.getAuthToken();
    }

    @Test(priority = 1)
    public void invokePhonePeLinkApi() {
        int statusCode  = paasWalletValidator.callPhonepeIsLink(tid, token);

        if (statusCode != 0) {
            mySmsValidator.deleteAllConversation(authToken);
            paasWalletValidator.validatePhonepeSendOtp(tid,token);
        } else {
            paasWalletValidator.callPhonepeDelink(tid, token);
            invokePhonePeLinkApi();
        }
    }

    @Test (dependsOnMethods="invokePhonePeLinkApi",priority = 2)
    public void invokePhonepeValidateOTPApi() {
        String otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.PHONEPE_ADDRESS);
        int retry = 0;

        while (otp==null && retry < PaasConstant.MAX_RETRY){
            paasWalletValidator.validatePhonepeResendOtp(tid,token);
            otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.FREECHARGE_ADDRESS);
            retry++;
        }
        paasWalletValidator.validatePhonepeOtp(tid,token,otp);
    }

    @Test (dependsOnMethods="invokePhonepeValidateOTPApi",priority = 3)
    public void invokePhonepeIsLinkedApi() {
        paasWalletValidator.validatePhonepeIsLink(tid, token);

    }

    @Test (dependsOnMethods="invokePhonepeValidateOTPApi",priority = 4)
    public void invokePhonepeCheckBalanceApi() {
        paasWalletValidator.validatePhonepeCheckBalance(tid, token);
    }

    @Test (dependsOnMethods="invokePhonepeValidateOTPApi",priority = 5)
    public void invokePhonepeProfileChecksumApi() {
        paasWalletValidator.validatePhonepeProfileChecksum(tid, token);
    }


    @Test (dependsOnMethods="invokePhonepeValidateOTPApi",priority = 6)
    public void invokePhonepeDeLinkApi() {
        paasWalletValidator.validatePhonepeDelink(tid, token);
    }


}