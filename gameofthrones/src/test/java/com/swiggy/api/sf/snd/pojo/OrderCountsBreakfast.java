package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class OrderCountsBreakfast {

    private String type;
    private Double min;

    /**
     * No args constructor for use in serialization
     *
     */
    public OrderCountsBreakfast() {
    }

    /**
     *
     * @param min
     * @param type
     */
    public OrderCountsBreakfast(String type, Double min) {
        super();
        this.type = type;
        this.min = min;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("min", min).toString();
    }

}
