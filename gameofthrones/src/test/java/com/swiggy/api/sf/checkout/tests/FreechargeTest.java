package com.swiggy.api.sf.checkout.tests;


import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.EditTimeConstants;
import com.swiggy.api.sf.checkout.constants.WalletConstants;
import com.swiggy.api.sf.checkout.dp.WalletDP;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.EditTimeHelper;
import com.swiggy.api.sf.checkout.helper.WalletHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.CartValidator;
import com.swiggy.api.sf.checkout.helper.edvo.helper.OrderValidator;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.JonSnow.Validator;
import framework.gameofthrones.Tyrion.JsonHelper;

public class FreechargeTest extends WalletDP{
	
    Initialize gameofthrones = new Initialize();
    CheckoutHelper helper= new CheckoutHelper();
    SnDHelper sndHelper= new SnDHelper();
    SANDHelper sandHelper= new SANDHelper();
    CartValidator cartValidator =new CartValidator();
    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();
    RngHelper rngHelper = new RngHelper();
    JsonHelper jsonHelper = new JsonHelper();
    OrderValidator orderValidator = new OrderValidator();
    EditTimeHelper editHelper = new EditTimeHelper();
    AddressHelper addressHelper=new AddressHelper();
    WalletHelper walletHelper = new WalletHelper();
    String orderID=null;
    String init_source=null;
    String order_key=null;
    String order=null;
    String tid;
    String token;
    String type=null;
    String balanceBeforeOrder = null;
    String balanceAfterOrder = null;
    String orderStatus = null;
    String orderTotal = null;
    
    /* Currently blocking the Link wallet scenario and same should be done manually */
    
/*    @Test(dataProvider="PayTM_Wallet",groups = {"Smoke","Sangeetha"}, description = "Link Wallet")
    public void linkWallet(String code,String message) {
      Login  
    HashMap<String, String> hashMap=helper.TokenData(EditTimeConstants.mobile1, EditTimeConstants.password1);
    tid=hashMap.get("Tid");
    token=hashMap.get("Token");
    Validator linkValidator= helper.paytmLinkWallet(tid,token).ResponseValidator;
    editHelper.statusCodeCheck(linkValidator.GetResponseCode());
    editHelper.orderResCheck(linkValidator.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
    String state = JsonPath.read(linkValidator.GetBodyAsText(), "$.data.state").toString().replace("[", "").replace("]", "");
    System.out.println(state);
    String state = "6d0ced5b-7004-5346-9986-b88a62a4400f";
    String otp="267620";
    Validator otpValidator = helper.paytmValidateOTP(tid,token,state,otp).ResponseValidator;
    editHelper.statusCodeCheck(otpValidator.GetResponseCode());
    editHelper.cartResCheck(otpValidator.GetBodyAsText(),WalletConstants.VALIDATE_OTP_MESSAGE);
    String accesstoken = JsonPath.read(otpValidator.GetBodyAsText(), "$.data.accessToken").toString().replace("[", "").replace("]", "");
    System.out.println(accesstoken);
   }
   
   */
    
    @Test(dataProvider="freeChargeWallet",groups = {"Smoke","sanity","Sangeetha"}, description = "1.Freecharge check balance before order")
    public void freeChargeCheckBalance(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	/* Login */ 
    	HashMap<String, String> hashMap=helper.TokenData(EditTimeConstants.mobile1, EditTimeConstants.password1);
    tid=hashMap.get("Tid");
    token=hashMap.get("Token");
    
    /* Check of current balance */
    	Validator chkBalance = helper.freechargeCheckBalance(tid,token).ResponseValidator;
    	editHelper.statusCodeCheck(chkBalance.GetResponseCode());
    	editHelper.orderResCheck(chkBalance.GetBodyAsText(),WalletConstants.STATUS_CODE ,WalletConstants.EXECUTED_SUCCESSFULLY);
    }
    
    
    @Test(dataProvider="freeChargeWallet",groups = {"Smoke","sanity","Sangeetha"}, description = "1.Login 2.Create cart 3.Place order via Freecharge")
    public void paymetTypeVerification(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {  
    		/* Login */ 
    		HashMap<String, String> hashMap=helper.TokenData(EditTimeConstants.mobile1, EditTimeConstants.password1);
    		tid=hashMap.get("Tid");
    		token=hashMap.get("Token");
    		
    	   /* Fetch balance */
    	   String fetchBalance = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
    	   System.out.println(fetchBalance); 
    	   
       /* Placing order via Freecharge*/
    	   Validator placeOrder = walletHelper.placeOrderFreecharge(tid,token,itemId, quantity, restId);
    	   editHelper.statusCodeCheck(placeOrder.GetResponseCode());
       editHelper.orderResCheck(placeOrder.GetBodyAsText(), WalletConstants.STATUS_MESSAGE);
       
       /* Payment type verification*/
       String paymentType = walletHelper.fetchPaymentType(placeOrder.GetBodyAsText(), EditTimeConstants.STATUS_MESSAGE);
       Assert.assertEquals(paymentType, CheckoutConstants.paymentFreecharge);
       
      /* Cancel order */
       orderID = JsonPath.read(placeOrder.GetBodyAsText(), "$.data..order_id").toString().replace("[", "").replace("]", "");
       System.out.println(orderID);
       Validator cancelOrder = walletHelper.cancelOrder(orderID).ResponseValidator;
       editHelper.statusCodeCheck(placeOrder.GetResponseCode());
       editHelper.orderResCheck(placeOrder.GetBodyAsText(), WalletConstants.STATUS_MESSAGE);
       
       /* Order Status Check */
       orderStatus = JsonPath.read(cancelOrder.GetBodyAsText(), "$.data.order_status").toString().replace("[", "").replace("]", "");
       System.out.println(orderStatus);
       
       /* Fetch balance */
	   String fetchBalanceafterorder = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
	   System.out.println(fetchBalanceafterorder);
       
       Assert.assertEquals(orderStatus, WalletConstants.CANCELLED_STATE);
       Assert.assertEquals(fetchBalanceafterorder,fetchBalance);
       
       }
    
    @Test (dataProvider = "freeChargeWallet", groups = {"Smoke","sanity","Sangeetha"}, description = "Cancel Order Check")
    public void cancelOrderTest(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	/* Fetch balance */
 	   String fetchBalance = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
 	   System.out.println(fetchBalance); 
 	   
 	 /* cancel order */  
    		Validator cancelOrder = walletHelper.getCancelOrder(tid, token, type, itemId, quantity, restId, newItemId, newquantity,cartEditStatusCode,cartEditmessage,cartConfirmStatusCode,cartConfirmMessage).ResponseValidator;
    		editHelper.statusCodeCheck(cancelOrder.GetResponseCode());
    	    editHelper.orderResCheck(cancelOrder.GetBodyAsText(), WalletConstants.STATUS_MESSAGE);
    	    
    	  /* Order Status Check */
         orderStatus = JsonPath.read(cancelOrder.GetBodyAsText(), "$.data.order_status").toString().replace("[", "").replace("]", "");
  	     System.out.println(orderStatus);
    	     Assert.assertEquals(orderStatus, WalletConstants.CANCELLED_STATE);
    	    
    	  /* Fetch balance */
    	    String fetchBalanceafterorder = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
    		System.out.println(fetchBalanceafterorder);
    	     
    	    Assert.assertEquals(fetchBalanceafterorder,fetchBalance);
        
    }
    
    @Test (dataProvider = "freeChargeWallet", groups = {"Smoke","sanity","Sangeetha"}, description = "login Token Check FG")
    public void loginTokenCheckFG(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	/* Check on login token for FG*/
    	Validator loginTokenchk = walletHelper.loginTokenFreecharge(tid, token).ResponseValidator;
    	editHelper.statusCodeCheck(loginTokenchk.GetResponseCode());
    	editHelper.orderResCheck(loginTokenchk.GetBodyAsText(),WalletConstants.STATUS_CODE ,WalletConstants.EXECUTED_SUCCESSFULLY);
    	
    	/* Assert for token*/
    		 String fetchLoginToken = JsonPath.read(loginTokenchk.GetBodyAsText(), "$.data.login_token").toString().replace("[", "").replace("]", "");
 	     System.out.println(fetchLoginToken);
	     Assert.assertNotNull(fetchLoginToken);
    	
    }
  
    
    @Test (dataProvider = "freeChargeWallet", groups = {"Smoke","sanity","Sangeetha"}, description = "Freecharge - Edit order check / Edit order confirm")
    public void editOrderChkConfirmFreeCharge(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	 /* Fetch balance */
 	   String fetchBalance = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
 	   System.out.println(fetchBalance); 
 	       
    /* Edit order confirm*/
 	  
 	   Validator editConfirmOrderValidator=walletHelper.editOrderConfirm(tid, token, type, itemId, quantity, restId, newItemId, newquantity);
 	   editHelper.orderResCheck(editConfirmOrderValidator.GetBodyAsText(),cartConfirmStatusCode, cartConfirmMessage);
 	 
    /* Fetch balance */
    	    String fetchBalanceafterorder = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
    		System.out.println(fetchBalanceafterorder);
    	     
    	    Assert.assertEquals(fetchBalanceafterorder,fetchBalance);
    	   
    }
    
    
    @Test (dataProvider = "freeChargeWallet", groups = {"Smoke","sanity","Sangeetha"}, description = "Freecharge - Get Single Order by Order ID")
    public void getSingleOrderFreeCharge(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	 /* Fetch balance */
 	   String fetchBalance = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
 	   System.out.println(fetchBalance); 
 	       
    /* Get Single Order */
 	  
 	   Validator getSingleOrderchk = walletHelper.getSingleOrder(type, itemId, quantity, restId, newItemId, newquantity, cartEditStatusCode, cartEditmessage, cartConfirmStatusCode, cartConfirmMessage);
 	   editHelper.orderResCheck(getSingleOrderchk.GetBodyAsText(),cartConfirmStatusCode, cartConfirmMessage);
 	 
    /* Fetch balance */
    	    String fetchBalanceafterorder = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
    		System.out.println(fetchBalanceafterorder);
    	     
    	    Assert.assertEquals(fetchBalanceafterorder,fetchBalance);
    	   
    }
    
    
    @Test (dataProvider = "freeChargeWallet", groups = {"Smoke","sanity","Sangeetha"}, description = "Freecharge - Get Single Order by Order Key")
    public void getSingleOrderKeyFreeCharge(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	 /* Fetch balance */
 	   String fetchBalance = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
 	   System.out.println(fetchBalance); 
 	       
    /* Get Single Order */
 	  
 	   Validator getSingleOrderchk = walletHelper.getSingleOrderKey(type, itemId, quantity, restId, newItemId, newquantity, cartEditStatusCode, cartEditmessage, cartConfirmStatusCode, cartConfirmMessage);
 	   editHelper.orderResCheck(getSingleOrderchk.GetBodyAsText(),cartConfirmStatusCode, cartConfirmMessage);
 	 
    /* Fetch balance */
    	    String fetchBalanceafterorder = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
    		System.out.println(fetchBalanceafterorder);
    	     
    	    Assert.assertEquals(fetchBalanceafterorder,fetchBalance);
    	   
    }

    
    @Test (dataProvider = "freeChargeWallet", groups = {"Smoke","sanity","Sangeetha"}, description = "Freecharge - Get Single Order by Order Key")
    public void getLastOrderFreeCharge(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	 /* Fetch balance */
 	   String fetchBalance = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
 	   System.out.println(fetchBalance); 
 	       
    /* Get Single Order */
 	  
 	   Validator getLastOrderchk = walletHelper.getLastOrderFG(type, itemId, quantity, restId, newItemId, newquantity, cartEditStatusCode, cartEditmessage, cartConfirmStatusCode, cartConfirmMessage);
 	   editHelper.orderResCheck(getLastOrderchk.GetBodyAsText(),cartConfirmStatusCode, cartConfirmMessage);
 	 
    /* Fetch balance */
    	    String fetchBalanceafterorder = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
    		System.out.println(fetchBalanceafterorder);
    	     
    	    Assert.assertEquals(fetchBalanceafterorder,fetchBalance);
    	   
    }
    
    @Test (dataProvider = "freeChargeWallet", groups = {"Smoke","sanity","Sangeetha"}, description = "Freecharge - Get Order")
    public void getOrderFreeCharge(String type, String itemId,String quantity, String restId,String newItemId,String newquantity,String cartEditStatusCode,String cartEditmessage,String cartConfirmStatusCode,String cartConfirmMessage)
    {
    	 /* Fetch balance */
 	   String fetchBalance = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
 	   System.out.println(fetchBalance); 
 	       
    /* Get Single Order */
 	  
 	   Validator getLastOrderchk = walletHelper.getLastOrderFG(type, itemId, quantity, restId, newItemId, newquantity, cartEditStatusCode, cartEditmessage, cartConfirmStatusCode, cartConfirmMessage);
 	   editHelper.orderResCheck(getLastOrderchk.GetBodyAsText(),cartConfirmStatusCode, cartConfirmMessage);
 	 
    /* Fetch balance */
    	    String fetchBalanceafterorder = walletHelper.freeChargeChekBalBeforeOrder(tid,token);
    		System.out.println(fetchBalanceafterorder);
    	     
    	    Assert.assertEquals(fetchBalanceafterorder,fetchBalance);
    	   
    }
    
    
}