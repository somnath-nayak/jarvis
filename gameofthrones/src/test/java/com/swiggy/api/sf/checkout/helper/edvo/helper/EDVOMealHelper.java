package com.swiggy.api.sf.checkout.helper.edvo.helper;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.sf.checkout.constants.EDVOConstants;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.mealTDRequest.*;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.mealTDRequest.Group;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.pojo.meals.createMeal.*;
import com.swiggy.api.sf.snd.pojo.meals.createMealResponse.CreateMealResponse;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.Reporter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EDVOMealHelper {

    Initialize gameofthrones = new Initialize();
    EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
    RngHelper rngHelper = new RngHelper();
    static HashMap<String, Processor> getMealsResponse = new HashMap<>();
    public String twoScreenMeal;
    public String threeScreenMeal;

    /**
     * Create Meal Trade Discount (EDVO)
     * @param payload
     * @return
     */
    public Processor mealTDCreate(String payload){
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("mealtd", "edvomealtdcreate", gameofthrones);
        return new Processor(gots, headers, new String[]{payload}, null);
    }

    /**
     * Update existing Meal Trade Discount (EDVO)
     * @param payload
     * @return
     */
    public Processor mealTDUpdate(String payload){
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("mealtd", "edvomealtdupdate", gameofthrones);
        return new Processor(gots, headers, new String[]{payload}, null);
    }

    /**
     * Gets a EDVO Meal based on mealId and restaurantId
     * @param mealId
     * @param restaurantId
     * @return
     */
    public Processor getMeals(String mealId, String restaurantId){
        String[] urlPayload = new String[]{mealId, restaurantId};
        GameOfThronesService gots = new GameOfThronesService("sand", "getedvomeal", gameofthrones);
        return new Processor(gots, null, null, urlPayload);
    }

    public Processor getMeal(String mealId, String restaurantId){
        if(getMealsResponse.containsKey(mealId)){
            return getMealsResponse.get(mealId);
        }else {
            Processor processor = getMeals(mealId, restaurantId);
            if(processor.ResponseValidator.GetNodeValueAsInt("$.statusCode") == 0) {
                getMealsResponse.put(mealId, processor);
                return processor;
            }else {
                Reporter.log("[Get Meals Call Failed]", true);
                Reporter.log("[Get Meals Response] \n ==> " + processor.ResponseValidator.GetBodyAsText(), true);
            }
        }
        return null;
    }


    public List<Integer> getGroupIdsFromMeal(String mealId, String restaurantId){
        Processor processor = getMeal(mealId, restaurantId);
        edvoCartHelper.smokeCheck(0, "done successfully", processor, "Get Meals API Failed");
        List<Integer> groupIds = new ArrayList<>();
        try{
            groupIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.screens.[*].group.id");
        }catch (PathNotFoundException e){
            Assert.fail("Screens Not Found In 'Get Meals' API Response for MealId = " + mealId + " and RestaurantId = " + restaurantId + "...!! \n" +
                    "[Get Meals Response] --> " + processor.ResponseValidator.GetBodyAsText());
        }
        return groupIds;
    }

    public MealTDRequest getMealTDPayload(boolean isMealTDIdRequired, Integer restId, Integer mealId, Integer mealTdID, String tdLevel,
                                   String rewardType, Integer rewardValue, String rewardFunction, Integer rewardCount,
                                   String operationType, List<Integer> groupIds, Integer count){

        //Rewards
        List<Reward> rewards =new ArrayList<>();
        rewards.add(getRewardPayload(rewardType, rewardValue, rewardFunction, rewardCount));

        //Discount
        Discount discount = getDiscountPayload(operationType, isMealTDIdRequired, restId, mealTdID, tdLevel, rewardType, rewardValue, rewardFunction, rewardCount, mealId, groupIds, count);
        List<Discount> discountList = new ArrayList<>();
        discountList.add(discount);

        MealTDRequestBuilder mealTDRequestBuilder = new MealTDRequestBuilder()
                .namespace(EDVOConstants.NAMESPACE)
                .header(EDVOConstants.HEADER)
                .description(EDVOConstants.DESCRIPTION)
                .validFrom(String.valueOf(Utility.getCurrentTimeStampInEpoch()))
                .validTill(String.valueOf(Utility.getTimeStampPlusOneYearInEpoch()))
                .campaignType(EDVOConstants.CAMPAIGN_TYPE)
                .restaurantHit(EDVOConstants.RESTAURANT_HIT)
                .enabled(EDVOConstants.ENABLED)
                .swiggyHit(EDVOConstants.SWIGGY_HIT)
                .createdBy(EDVOConstants.CREATED_BY)
                .discountCap(EDVOConstants.DISCOUNT_CAP)
                .discounts(discountList)
                .slots(new ArrayList<>())
                .commissionOnFullBill(EDVOConstants.COMMISION_ON_FULL_BILL)
                .taxesOnDiscountedBill(EDVOConstants.TAXES_ON_DISCOUNTED_BILL)
                .firstOrderRestriction(EDVOConstants.FIRST_ORDER_RESTRICTION)
                .firstOrderRestriction(EDVOConstants.FIRST_ORDER_RESTRICTION)
                .userRestriction(EDVOConstants.USER_RESTRICTION);

        if(isMealTDIdRequired){ mealTDRequestBuilder.id(mealTdID);}
        MealTDRequest mealTDRequest = mealTDRequestBuilder.build();
        return mealTDRequest;
    }

    public Discount getDiscountPayload(String operationType, boolean isMealIdRequired, Integer restId, Integer mealTdID, String tdLevel,
                                       String rewardType, Integer rewardValue, String rewardFunction, Integer rewardCount, Integer mealId, List<Integer> groupIds, Integer count){
        Discount discount = new Discount();
        List<Meal> meals = new ArrayList<>();
        Meal meal = getMealPayload(tdLevel, mealId, EDVOConstants.HEADER, EDVOConstants.DESCRIPTION, rewardType, rewardValue, rewardFunction, rewardCount, groupIds, count);
        meals.add(meal);

        Restaurant restaurant = new Restaurant();
        restaurant.setId(restId);

        discount.setRestaurant(restaurant);
        discount.setOperationType(operationType);
        discount.setMeals(meals);

        return discount;
    }

    public Meal getMealPayload(String tdLevel, Integer mealId, String mealHeader, String mealDescripton, String rewardType,
                               Integer rewardValue, String rewardFunction, Integer rewardCount,
                               List<Integer> groupIds, Integer count){
        Meal meal = new Meal();
        meal.setMealId(mealId);
        meal.setOperationMeta(getOperationMetaPayload(mealHeader, mealDescripton));

        List<Reward> rewards = new ArrayList<>();
        rewards.add(getRewardPayload(rewardType, rewardValue, rewardFunction, rewardCount));
        if(tdLevel.equals(EDVOConstants.TD_MEAL_LEVEL)){
            meal.setRewards(rewards);
        }else {rewards.add(null);}

        List<Group> groups = getGroupPayload(rewardType, rewardValue, rewardFunction, rewardCount, groupIds, count, tdLevel);
        meal.setGroups(groups);
        return meal;
    }

    public OperationMeta getOperationMetaPayload(String mealHeader, String mealDescripton){
        OperationMeta operationMeta = new OperationMeta();
        operationMeta.setMealHeader(mealHeader);
        operationMeta.setMealDescription(mealDescripton);
        return operationMeta;
    }

    //Creates reward for EDVO Meal TD
    public Reward getRewardPayload(String rewardType, Integer rewardValue, String rewardFunction, Integer rewardCount){
        Reward reward = new Reward();
        reward.setRewardType(rewardType);
        reward.setRewardValue(rewardValue);
        reward.setRewardFunction(rewardFunction);
        reward.setCount(rewardCount);
        return reward;
    }

    //Creates Group for EDVO Meal TD
    public List<Group> getGroupPayload(String rewardType, Integer rewardValue, String rewardFunction, Integer rewardCount, List<Integer> groupIds, Integer count, String tdLevel){
        List<Group> groups = new ArrayList<>();
        List<Reward> rewards = new ArrayList<>();

        if(tdLevel.equals(EDVOConstants.TD_MEAL_LEVEL)){rewards = null; }
        else {rewards.add(getRewardPayload(rewardType, rewardValue, rewardFunction, rewardCount));}

        for(int i=0; i<groupIds.size(); i++){
            Group group = new Group();
            group.setGroupId(groupIds.get(i));
            group.setCount(count);
            group.setRewards(rewards);
            groups.add(group);
        }

        return groups;
    }

    public Processor createOrUpdateMealTD(Integer restaurantId, Integer mealId, String tdLevel, String rewardType,
                                     Integer rewardValue, String rewardFunction, Integer rewardCount){
        Integer mealTdID = 0;
        boolean isMealTDIdRequired = false;
        String operationType = EDVOConstants.OPERATION_TYPE;
        Integer count = 1;
        List<Integer> groupIds = getGroupIdsFromMeal(String.valueOf(mealId), String.valueOf(restaurantId));

        MealTDRequest mealTDRequest = getMealTDPayload(isMealTDIdRequired, restaurantId, mealId, mealTdID, tdLevel, rewardType, rewardValue,
                rewardFunction, rewardCount, operationType, groupIds, count);
        Reporter.log("Meal TD Payload || " + Utility.jsonEncode(mealTDRequest));
        Processor processor = mealTDCreate(Utility.jsonEncode(mealTDRequest));
        if(processor.ResponseValidator.GetNodeValueAsInt("$.statusCode") == 2 ){
            isMealTDIdRequired = true;
            String message = processor.ResponseValidator.GetNodeValue("$.statusMessage");
            message = message.substring(message.lastIndexOf(" ") + 1);
//            message = message.substring(message.lastIndexOf("campaign") + 9, message.lastIndexOf(" "));
            System.out.println(message);
            mealTdID = Integer.valueOf(message);
            mealTDRequest = getMealTDPayload(isMealTDIdRequired, restaurantId, mealId, mealTdID, tdLevel, rewardType, rewardValue,
                    rewardFunction, rewardCount, operationType, groupIds, count);
            processor = mealTDUpdate(Utility.jsonEncode(mealTDRequest));
        }
        return processor;
    }

    public Processor createOrUpdateMealTD(Integer restId, Integer mealId, String tdType){
        rngHelper.disabledActiveTD(String.valueOf(restId));
        String tdLevel = null;
        String rewardType = null;
        Integer rewardValue = 0;
        String rewardFunction = null;
        Integer rewardCount = null;

        if(tdType.equals(EDVOConstants.MEAL_TD_TYPE_01)){ // (buy x and y items, at Price P)
            tdLevel = EDVOConstants.TD_MEAL_LEVEL;
            rewardType = EDVOConstants.REWARD_TYPE_FINALPRICE;
            rewardValue = EDVOConstants.REWARD_VALUE_FINAL_PRICE;
            rewardCount = getRewardCount(mealId);
        }else if(tdType.equals(EDVOConstants.MEAL_TD_TYPE_02)){ // (buy x,y with %P off)
            tdLevel = EDVOConstants.TD_MEAL_LEVEL;
            rewardType = EDVOConstants.REWARD_TYPE_PERCENTAGE;
            rewardValue = EDVOConstants.REWARD_VALUE_PERCENTAGE;
            rewardCount = getRewardCount(mealId);
        }else if(tdType.equals(EDVOConstants.MEAL_TD_TYPE_03)){ // (buy x,y with  flat P off)
            tdLevel = EDVOConstants.TD_MEAL_LEVEL;
            rewardType = EDVOConstants.REWARD_TYPE_FLAT;
            rewardValue = EDVOConstants.REWARD_VALUE_FLAT;
            rewardCount = getRewardCount(mealId);
        }else if(tdType.equals(EDVOConstants.MEAL_TD_TYPE_04)){ // (buy x and y items at Price P each)
            tdLevel = EDVOConstants.TD_ITEM_LEVEL;
            rewardType = EDVOConstants.REWARD_TYPE_FINALPRICE;
            rewardValue = EDVOConstants.REWARD_VALUE_FINAL_PRICE;
            rewardFunction = EDVOConstants.REWARD_FUNCTION_MIN;
            rewardCount = 1;
        }else if(tdType.equals(EDVOConstants.MEAL_TD_TYPE_05)){ // (buy x and y items with %P discount each)
            tdLevel = EDVOConstants.TD_ITEM_LEVEL;
            rewardType = EDVOConstants.REWARD_TYPE_PERCENTAGE;
            rewardValue = EDVOConstants.REWARD_VALUE_PERCENTAGE;
            rewardFunction = EDVOConstants.REWARD_FUNCTION_MIN;
            rewardCount = 1;
        }else if(tdType.equals(EDVOConstants.MEAL_TD_TYPE_06)){ // (buy x and y items with flat P discount each)
            tdLevel = EDVOConstants.TD_ITEM_LEVEL;
            rewardType = EDVOConstants.REWARD_TYPE_FLAT;
            rewardValue = EDVOConstants.REWARD_VALUE_FLAT;
            rewardFunction = EDVOConstants.REWARD_FUNCTION_MIN;
            rewardCount = 1;
        }else if(tdType.equals(EDVOConstants.MEAL_TD_TYPE_07)){ // (buy x,y , get y item free(y is min among both)
            tdLevel = EDVOConstants.TD_MEAL_LEVEL;
            rewardType = EDVOConstants.REWARD_TYPE_PERCENTAGE;
            rewardValue = EDVOConstants.REWARD_VALUE_BOGO;
            rewardFunction = EDVOConstants.REWARD_FUNCTION_MIN;
            rewardCount = 1;
        }else if(tdType.equals(EDVOConstants.MEAL_TD_TYPE_08)){ // (buy x, y and z, get x at %P1, y at %P2, z at %P3)
            tdLevel = EDVOConstants.TD_ITEM_LEVEL;
            rewardType = EDVOConstants.REWARD_TYPE_PERCENTAGE;
            rewardValue = EDVOConstants.REWARD_VALUE_PERCENTAGE;
            rewardCount = getRewardCount(mealId);
        }
        return createOrUpdateMealTD(restId, mealId, tdLevel, rewardType, rewardValue, rewardFunction, rewardCount);
    }

    public Integer getRewardCount(Integer mealId){
        return mealId.equals(Integer.valueOf(twoScreenMeal)) ? 2 : 3;
    }

    //Gets Default Create Meal Payload
    public CreateMealPayload getCreateMealPayload(String restaurantId, int screenCount){
        return getCreateMealPayload(restaurantId, screenCount, screenCount, screenCount);
    }

    //Gets the Create Meal Payload Pojo
    public CreateMealPayload getCreateMealPayload(String restaurantId, int screenCount, int minTotal, int maxTotal){
        Processor menuV4 = edvoCartHelper.getMenuV4(restaurantId);
        CreateMealPayload createMealPayload = new CreateMealPayload();
        createMealPayload.setRestId(Integer.valueOf(restaurantId));
        createMealPayload.setMinTotal(minTotal);
        createMealPayload.setMaxTotal(maxTotal);
        createMealPayload.setExternalId(Integer.valueOf(restaurantId));
        createMealPayload.setTdNecessary(false);
        createMealPayload.setName(EDVOConstants.NAME_1 + screenCount + EDVOConstants.NAME_2);
        createMealPayload.setScreens(getScreens(restaurantId, screenCount, 1));
        createMealPayload.setLaunchPage(getLaunchPage());
        createMealPayload.setExitPage(getExitPage());
        return createMealPayload;
    }

    //Gets the Launch Page for Meal
    public LaunchPage getLaunchPage(){
        LaunchPage launchPage = new LaunchPage();
        launchPage.setBgColor(EDVOConstants.LP_BGCOLOR);
        launchPage.setCommunicationText(EDVOConstants.LP_COMMUNICATION_TEXT);
        launchPage.setImage(EDVOConstants.LP_IMAGE);
        launchPage.setMainText(EDVOConstants.LP_MAIN_TEXT);
        launchPage.setSubText(EDVOConstants.LP_SUB_TEXT);
        launchPage.setTagColor(EDVOConstants.LP_TAG_COLOR);
        launchPage.setTagText(EDVOConstants.LP_TAG_TEXT);
        launchPage.setTextColor(EDVOConstants.LP_TEXT_COLOR);
        return launchPage;
    }

    //Gets the Exit Page for Meal
    public ExitPage getExitPage(){
        ExitPage exitPage = new ExitPage();
        exitPage.setBgColor(EDVOConstants.EP_BGCOLOR);
        exitPage.setImage(EDVOConstants.EP_IMAGE);
        exitPage.setMainText(EDVOConstants.EP_MAIN_TEXT);
        exitPage.setSubText(EDVOConstants.EP_SUB_TEXT);
        exitPage.setTextColor(EDVOConstants.EP_TEXT_COLOR);
        return exitPage;
    }

    //Gets the Meal Screens for provided paramenters
    public List<Screen> getScreens(String restaurantId, int screenCount, int itemQuantity){
        Processor processor = edvoCartHelper.getMenuV4(restaurantId);
        List<Integer> inStocks = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.inStock");
        List<Integer> itemIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.id");
        List<Screen> screens = new ArrayList<>();
        Screen screen = null;
        com.swiggy.api.sf.snd.pojo.meals.createMeal.Group group = null;
        List<Item> items = null;
        Item item = null;
        int count = 0; //Counter to avoid adding same item twice in a meal
        int itemCounter = 0; //Counter to make sure the number of items in each group is same as itemQuantity

        //Loop for Screen based on Screen Count
        for (int i=0; i<screenCount; i++){
            screen = new Screen();
            group = new com.swiggy.api.sf.snd.pojo.meals.createMeal.Group();
            items = new ArrayList<Item>();
            itemCounter = 0;

            //Loop for Adding Items into Meal Group
            for (int j = count; itemCounter < itemQuantity && j < itemIds.size(); j++) {
                //If item is in stock then add the item
                if (inStocks.get(j)==1){
                    item = new Item();
                    item.setItemId(itemIds.get(j));
                    item.setMaxQuantity(1);
                    item.setAddons(null);
                    item.setVariants(null);
                    items.add(item);
                    itemCounter++;
                    count = j+1;
                    if(itemCounter<=itemQuantity)
                    break;
                }
            }
            group.setItems(items);
            group.setId(i);
            screen.setGroup(group);
            screen.setTitle(EDVOConstants.SCREEN_TITLE);
            screen.setDescription(EDVOConstants.SCREEN_DESCRIPTION);
            screens.add(screen);
        }
        return screens;
    }

    public String createMealAndReturnID(String restaurantId, int screenCount){
        CreateMealPayload createMealPayload = getCreateMealPayload(restaurantId, screenCount);
        String payload = Utility.jsonEncode(createMealPayload);
        Processor processor = edvoCartHelper.createMeal(payload);
        CreateMealResponse createMealResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CreateMealResponse.class);
        int mealId = 0;
        try {
            mealId = createMealResponse.getId();
        }catch (NullPointerException e){
            Assert.fail("[Meal Id not found after Meal Creation...!!]");
        }
        return String.valueOf(mealId);
    }

}
