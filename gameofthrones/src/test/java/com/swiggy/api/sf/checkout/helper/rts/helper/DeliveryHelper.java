package com.swiggy.api.sf.checkout.helper.rts.helper;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.RTSHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;

import java.util.List;

public class DeliveryHelper {
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    RTSHelper rtsHelper = new RTSHelper();

    //Create new Delivery Boy for provided ZONE ID and returns the DE ID
    public String createNewDE(int zoneId){
        return deliveryDataHelper.CreateDE(zoneId);
    }

    //Enables a DE for particular lat lng
    public void enableDE(String deliveryBoyID, String lat, String lng, String zoneId, String cityID, String enableDE){
        Processor processor;
        try {
            processor = rtsHelper.updateDE(deliveryBoyID, lat,
                    lng, zoneId,
                    enableDE, cityID);
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "updateDE FAILED...");
            processor = rtsHelper.updateDEExecutiveLocation(deliveryBoyID, lat,
                    lng, cityID, enableDE, zoneId);
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "updateDEExecutiveLocation FAILED...");

            deliveryDataHelper.delocationupdate(lat,lng, deliveryBoyID, DeliveryConstant.delivery_app_version);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Unreserves a DE for provided orderId
    public Processor unreserveDE(String orderId){
        Processor processor = null;
        try {rtsHelper.cancelTask(orderId);}
        catch (InterruptedException e) {e.printStackTrace();}
        return processor;
    }

    //Unreserves a DE for provided orderId
    public Processor unreserveDE(Long orderId){
        Processor processor = null;
        try {rtsHelper.cancelTask(String.valueOf(orderId.longValue()));}
        catch (InterruptedException e) {e.printStackTrace();}
        return processor;
    }

    //Unreserves DE for provided List of orderIds
    public void unreserveDE(List<Long> orderIds){
        if(orderIds!=null) {
            for (Long orderId : orderIds) {
                unreserveDE(String.valueOf(orderId));
            }
        }
    }
}
