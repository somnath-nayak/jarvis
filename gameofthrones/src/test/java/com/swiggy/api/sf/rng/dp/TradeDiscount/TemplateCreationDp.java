package com.swiggy.api.sf.rng.dp.TradeDiscount;

import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.helper.TDHelper.TDTemplateHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemplateCreationDp {

    static TDTemplateHelper tdTemplateHelper = new TDTemplateHelper();

    /*****CREATE TEMPLATE*******/
    @DataProvider(name = "createValidTemplateDp")
    public static Object[][] createValidTemplateDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));
        HashMap<String, String> requestmap = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        return new Object[][]{{requestmap}};
    }

    @DataProvider(name = "createTemplateForDifferentCampaignTypeDp")
    public static Object[][] createTemplateForDifferentCampaignTypeDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");

        return new Object[][]{{requestmap1}, {requestmap3}, {requestmap4}};
    }


    @DataProvider(name = "createTemplateInDifferentCaseDp")
    public static Object[][] createTemplateInDifferentCaseDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));

        HashMap<String, String> requestmap1 = createRequestMap("Free_Delivery", validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap("PERCENTAGE", validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap("FLAT", validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");


        return new Object[][]{{requestmap1}, {requestmap3}, {requestmap4}};
    }


    @DataProvider(name = "createTemplateForDifferentCampaignTypeWithValidTimeStampDp")
    public static Object[][] createTemplateForDifferentCampaignTypeWithValidTimeStampDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");

        return new Object[][]{{requestmap1},{requestmap3}, {requestmap4}};
    }

    @DataProvider(name = "createTemplateForDifferentCampaignTypeWithValidTimeStampWithCurrentStampDp")
    public static Object[][] createTemplateForDifferentCampaignTypeWithValidTimeStampWithCurrentStampDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(0)); //with current stamp
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");

        return new Object[][]{{requestmap1},  {requestmap3}, {requestmap4}};
    }


    @DataProvider(name = "createTemplateForDifferentCampaignTypWithFirstOrderEnabledDp")
    public static Object[][] createTemplateForDifferentCampaignTypWithFirstOrderEnabledDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(0));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));

        //FIRST ORDER enabled -> FALSE
        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");

        //FIRST ORDER enabled -> TRUE
        HashMap<String, String> requestmap5 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "true", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap7 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "true", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap8 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "true", "30", RngConstants.ZERO_DAYS_DORMANT, "false");


        //DORMANT USER TESTCASES
        HashMap<String, String> requestmap9 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap11 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.SIXTY_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap12 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.NINETY_DAYS_DORMANT, "false");


        return new Object[][]{{requestmap1}, {requestmap3}, {requestmap4},
                {requestmap5},  {requestmap7}, {requestmap8},
                {requestmap9},  {requestmap11}, {requestmap12}};
    }


    @DataProvider(name = "createTemplateForDifferentCampaignTypWithoutDormantUserDp")
    public static Object[][] createTemplateForDifferentCampaignTypWithoutDormantUserDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(0));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));

        //FIRST ORDER enabled -> FALSE & DORMANT USER as NULL
        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", null, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", null, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", null, "false");

        //FIRST ORDER enabled -> TRUE & DORMANT USER as NULL
        HashMap<String, String> requestmap5 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "true", "30", null, "false");
        HashMap<String, String> requestmap7 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "true", "30", null, "false");
        HashMap<String, String> requestmap8 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "true", "30", null, "false");

        return new Object[][]{{requestmap1}, {requestmap3}, {requestmap4},
                {requestmap5},  {requestmap7}, {requestmap8}};
    }


    /*****DISABLE TEMPLATE*******/
    @DataProvider(name = "disableValidTemplateIdDp")
    public static Object[][] disableValidTemplateIdDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));
        HashMap<String, String> requestmap = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");


        //SINGLE VALID TEMPLATE ID
        List<Integer> templateList = new ArrayList<>();
        Map<String, Object> dbresults = tdTemplateHelper.getRandomTemplateIdFromDB();
        String validid = dbresults.get("id").toString();
        templateList.add(Integer.valueOf(validid));

        //MULTIPLE VALID TEMPLATE ID
        List<Integer> muliTemplateList = new ArrayList<>();
        Map<String, Object> dbresults1 = tdTemplateHelper.getRandomTemplateIdFromDB();
        Map<String, Object> dbresults2 = tdTemplateHelper.getRandomTemplateIdFromDB();
        Map<String, Object> dbresults3 = tdTemplateHelper.getRandomTemplateIdFromDB();

        muliTemplateList.add(Integer.valueOf(dbresults2.get("id").toString()));
        muliTemplateList.add(Integer.valueOf(dbresults1.get("id").toString()));
        muliTemplateList.add(Integer.valueOf(dbresults3.get("id").toString()));

        return new Object[][]{{requestmap, templateList}, {requestmap, muliTemplateList}};
    }


    @DataProvider(name = "disableValidTemplateIdAndVerifyDbDp")
    public static Object[][] disableValidTemplateIdAndVerifyDbDp() {

        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));
        HashMap<String, String> requestmap = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");

        //SINGLE VALID TEMPLATE ID
        List<Integer> templateList = new ArrayList<>();
        Map<String, Object> dbresults = tdTemplateHelper.getRandomEnabledTemplateIdFromDB();
        String validid = dbresults.get("id").toString();
        templateList.add(Integer.valueOf(validid));

        //MULTIPLE VALID TEMPLATE ID
        List<Integer> muliTemplateList = new ArrayList<>();
        Map<String, Object> dbresults1 = tdTemplateHelper.getRandomEnabledTemplateIdFromDB();
        Map<String, Object> dbresults2 = tdTemplateHelper.getRandomEnabledTemplateIdFromDB();
        Map<String, Object> dbresults3 = tdTemplateHelper.getRandomEnabledTemplateIdFromDB();

        muliTemplateList.add(Integer.valueOf(dbresults1.get("id").toString()));
        muliTemplateList.add(Integer.valueOf(dbresults2.get("id").toString()));
        muliTemplateList.add(Integer.valueOf(dbresults3.get("id").toString()));

        return new Object[][]{{requestmap, templateList}, {requestmap, muliTemplateList}};
    }


    /*****GET TEMPLATE BY ID*******/

    @DataProvider(name = "getExpiredTemplateIdDp")
    public static Object[][] getExpiredTemplateIdDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-2));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-1));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");

        return new Object[][]{{requestmap1}, {requestmap3}, {requestmap4}};
    }


    /*****GET TEMPLATE LIST*******/

    @DataProvider(name = "getTemplateByTemplateNameDp")
    public static Object[][] getTemplateByTemplateNameDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-2));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-1));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        String enabled = "true";
        String templatename = "test"; //partial name
        String completeTemplateName = "Test-Template"; //complete Template name

        return new Object[][]{{requestmap1, enabled, templatename}, {requestmap3, enabled, templatename}, {requestmap4, enabled, templatename},
                {requestmap1, enabled, completeTemplateName}, {requestmap3, enabled, completeTemplateName}, {requestmap4, enabled, completeTemplateName}};
    }


    @DataProvider(name = "getDisabledTemplateByTemplateNameDp")
    public static Object[][] getDisabledTemplateByTemplateNameDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-2));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-1));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
         HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        String enabled = "false";
        String templatename = "test"; //partial name
        String completeTemplateName = "Test-Template"; //complete Template name

        return new Object[][]{{requestmap1, enabled, templatename} , {requestmap3, enabled, templatename}, {requestmap4, enabled, templatename},
                {requestmap1, enabled, completeTemplateName},  {requestmap3, enabled, completeTemplateName}, {requestmap4, enabled, completeTemplateName}};
    }


    @DataProvider(name = "getTemplateByTemplateNameWithoutEnabledDp")
    public static Object[][] getTemplateByTemplateNameWithoutEnabledDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-2));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-1));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");

        String templatename = "test"; //partial name
        String completeTemplateName = "Test-Template"; //complete Template name
        String blankTemplateName = "";

        return new Object[][]{{requestmap1, templatename}, {requestmap3, templatename}, {requestmap4, templatename},
                {requestmap1, completeTemplateName}, {requestmap3, completeTemplateName}, {requestmap4, completeTemplateName},
                {requestmap1, blankTemplateName},  {requestmap3, blankTemplateName}, {requestmap4, blankTemplateName}};
    }


    @DataProvider(name = "getTemplateByEnabledDp")
    public static Object[][] getTemplateByEnabledDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-2));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-1));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        String enabled = "true";
        String enabled_false = "false";
        String enabled_blank = "";


        return new Object[][]{{requestmap1, enabled}, {requestmap3, enabled}, {requestmap4, enabled}, //enabled TRUE
                {requestmap1, enabled_false}, {requestmap3, enabled_false}, {requestmap4, enabled_false}, //enabled FALSE
                {requestmap1, enabled_blank},  {requestmap3, enabled_blank}, {requestmap4, enabled_blank}}; //enabled  blank default FALSE
    }


    @DataProvider(name = "getTemplateWithoutAnyParamDp")
    public static Object[][] getTemplateWithoutAnyParamDp() {
        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-2));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(-1));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
         HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "30", RngConstants.ZERO_DAYS_DORMANT, "false");


        return new Object[][]{{requestmap1},  {requestmap3}, {requestmap4}};
    }


    /*****ONBOARD TEMPLATE*******/

    @DataProvider(name = "onboardTemplateWithValidTemplateAndRestIdsDp")
    public static Object[][] onboardTemplateWithValidTemplateAndRestIdsDp() {


        List<Integer> singleRestIdsList = new ArrayList<>();
        singleRestIdsList.add(Utility.getRandom(1, 100000));

        List<Integer> multiRestIdsList = new ArrayList<>();
        multiRestIdsList.add(Utility.getRandom(1, 100000));
        multiRestIdsList.add(Utility.getRandom(1, 100000));
        multiRestIdsList.add(Utility.getRandom(1, 100000));


        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false");

        return new Object[][]{{requestmap1, singleRestIdsList}, {requestmap3, singleRestIdsList}, {requestmap4, singleRestIdsList}, //single restaurant template onboarding
                {requestmap1, multiRestIdsList}, {requestmap3, multiRestIdsList}, {requestmap4, multiRestIdsList} //multi restaurant template onboarding
        };

    }


    @DataProvider(name = "onboardTemplateWithOptionalParamsDp")
    public static Object[][] onboardTemplateWithOptionalParamsDp() {


        List<Integer> singleRestIdsList = new ArrayList<>();
        singleRestIdsList.add(Utility.getRandom(1, 100000));

        List<Integer> multiRestIdsList = new ArrayList<>();
        multiRestIdsList.add(Utility.getRandom(1, 100000));
        multiRestIdsList.add(Utility.getRandom(1, 100000));
        multiRestIdsList.add(Utility.getRandom(1, 100000));


        String validfrom = String.valueOf(tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(3));
        String validTill = String.valueOf(tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(2));

        HashMap<String, String> requestmap1 = createRequestMap(RngConstants.FREE_DELIVERY, validfrom, validTill, "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap3 = createRequestMap(RngConstants.PERCENTAGE, validfrom, validTill, "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false");
        HashMap<String, String> requestmap4 = createRequestMap(RngConstants.FLAT, validfrom, validTill, "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false");

        return new Object[][]
                {{requestmap1, singleRestIdsList, "10", null, null}, {requestmap3, singleRestIdsList, "10", null, null}, {requestmap4, singleRestIdsList, "10", null, null}, //single restaurant template onboarding with DURATION  in  request payload
                        {requestmap1, multiRestIdsList, "10", null, null},  {requestmap3, multiRestIdsList, "10", null, null}, {requestmap4, multiRestIdsList, "10", null, null}, //multi restaurant template onboarding with DURATION  in  request payload

                        {requestmap1, singleRestIdsList, null, validfrom, null}, {requestmap3, singleRestIdsList, null, validfrom, null}, {requestmap4, singleRestIdsList, null, validfrom, null}, //single restaurant template onboarding with START DATE  in  request payload
                        {requestmap1, multiRestIdsList, null, validfrom, null},  {requestmap3, multiRestIdsList, null, validfrom, null}, {requestmap4, multiRestIdsList, null, validfrom, null}, //multi restaurant template onboarding with START DATE  in  request payload

                        {requestmap1, singleRestIdsList, "10", validfrom, null},  {requestmap3, singleRestIdsList, "10", validfrom, null}, {requestmap4, singleRestIdsList, "10", validfrom, null}, //single restaurant template onboarding with START DATE  & Duration in  request payload
                        {requestmap1, multiRestIdsList, "10", validfrom, null},  {requestmap3, multiRestIdsList, "10", validfrom, null}, {requestmap4, multiRestIdsList, "10", validfrom, null},//multi restaurant template onboarding with START DATE & Duration  in  request payload

                        {requestmap1, singleRestIdsList, "10", validfrom, RngConstants.INGESTIONSOURCE_BULK_SYSTEM}, {requestmap3, singleRestIdsList, "10", validfrom, RngConstants.INGESTIONSOURCE_GROWTH_PACK}, {requestmap4, singleRestIdsList, "10", validfrom, RngConstants.INGESTIONSOURCE_VENDOR_APP}, //single restaurant template onboarding with START DATE  , Duration & INGESTION SOURCE in  request payload
                        {requestmap1, multiRestIdsList, "10", validfrom, RngConstants.INGESTIONSOURCE_BULK_SYSTEM},  {requestmap3, multiRestIdsList, "10", validfrom, RngConstants.INGESTIONSOURCE_GROWTH_PACK}, {requestmap4, multiRestIdsList, "10", validfrom, RngConstants.INGESTIONSOURCE_VENDOR_APP} //multi restaurant template onboarding with START DATE & Duration  & INGESTION SOURCE in  request payload

                };

    }


    private static HashMap<String, String> createRequestMap(String campaigntype, String validfrom, String validtill, String firstorder, String mincartamount, String dormantUserType, String userRestriction) {

        HashMap<String, String> requestmap = new HashMap<>();
        requestmap.put(RngConstants.CAMPAIGN_TYPE, campaigntype);
        requestmap.put(RngConstants.VALID_FROM, validfrom);
        requestmap.put(RngConstants.VALID_TILL, validtill);
        requestmap.put(RngConstants.MIN_CART_AMOUNT, mincartamount);
        requestmap.put(RngConstants.FIRST_ORDER, firstorder);
        requestmap.put(RngConstants.DORMANT_USER_TYPE, dormantUserType);
        requestmap.put(RngConstants.USER_RESTRICTION, userRestriction);
        return requestmap;
    }


    /***VALIDATE TEMPLATE****/

    @DataProvider(name = "validateTemplateDp")
    public static Object[][] validateTemplateDp() {

        List<Integer> templateids = new ArrayList<>();

        return new Object[][]
                {{templateids}};
    }


    @DataProvider(name = "singleValidTemplateIdDp")
    public static Object[][] singleValidTemplateIdDp() throws IOException {

        List<Integer> templateids = new ArrayList<>();
        templateids.add(Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.FREE_DELIVERY)));

        return new Object[][]
                {{templateids}};
    }


    @DataProvider(name = "multipleValidTemplateIdDp")
    public static Object[][] multipleValidTemplateIdDp() throws IOException {

        int free_del = Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.FREE_DELIVERY));
        int flat = Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.FLAT));
        int percentage = Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.PERCENTAGE));

        //2 Templates VALID
        List<Integer> templateids1 = new ArrayList<>();
        templateids1.add(free_del);
        templateids1.add(flat);


        //2 Templates INVALID
        List<Integer> templateids3 = new ArrayList<>();
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.FREE_DELIVERY)));
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.FREE_DELIVERY)));


        List<Integer> templateids5 = new ArrayList<>();
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.FLAT)));
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.FLAT)));

        List<Integer> templateids6 = new ArrayList<>();
        templateids6.add(Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.PERCENTAGE)));
        templateids6.add(Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.PERCENTAGE)));


        String valid = "valid";
        String invalid = "invalid";


        return new Object[][]
                {{templateids1, valid},
                        {templateids3, invalid}, {templateids5, invalid}, {templateids6, invalid}};
    }


    @DataProvider(name = "percentageAndFlatTemplateCoexistenseDp")
    public static Object[][] percentageAndFlatTemplateCoexistenseDp() throws IOException {

        int free_del = Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.FREE_DELIVERY));
        int flat = Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.FLAT));
        int percentage = Integer.valueOf(tdTemplateHelper.createTemplate(RngConstants.PERCENTAGE));

        //2 Template ID's
        List<Integer> templateids1 = new ArrayList<>();
        templateids1.add(flat);
        templateids1.add(percentage);
        //3 Template ID's
        List<Integer> templateids2 = new ArrayList<>();
        templateids2.add(flat);
        templateids2.add(percentage);
        templateids2.add(free_del);


        return new Object[][]
                {{templateids1}};
    }


    @DataProvider(name = "multipleValidTemplateWithoutOverLappingDp")
    public static Object[][] multipleValidTemplateWithoutOverLappingDp() throws IOException {


        //2 Templates VALID
        //SLIDING time span type
        List<Integer> templateids1 = new ArrayList<>();
        //First template creation valid from - > now, valid_till -> 1month
        templateids1.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SLIDING)));
        //Second template valid from 31st day (given in mins)
        templateids1.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "44640", "2", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SLIDING)));

        List<Integer> templateids3 = new ArrayList<>();
        //First template creation valid from - > now, valid_till -> 1month
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SLIDING)));
        //Second template valid from 31st day (given in mins)
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "44640", "2", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SLIDING)));

        List<Integer> templateids4 = new ArrayList<>();
        //First template creation valid from - > now, valid_till -> 1month
        templateids4.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SLIDING)));
        //Second template valid from 31st day (given in mins)
        templateids4.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "44640", "2", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SLIDING)));

        // STATIC time span type
        List<Integer> templateids5 = new ArrayList<>();
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_STATIC)));
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "44640", "2", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_STATIC)));

        List<Integer> templateids7 = new ArrayList<>();
        templateids7.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_STATIC)));
        templateids7.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "44640", "2", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_STATIC)));

        List<Integer> templateids8 = new ArrayList<>();
        templateids8.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_STATIC)));
        templateids8.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "44640", "2", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_STATIC)));

        // SUGGESTED time span type
        List<Integer> templateids9 = new ArrayList<>();
        templateids9.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids9.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "44640", "2", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));


        List<Integer> templateids11 = new ArrayList<>();
        templateids11.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids11.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "44640", "2", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids12 = new ArrayList<>();
        templateids12.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids12.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "44640", "2", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        String valid = "valid";
        String invalid = "invalid";

        return new Object[][]
                {
                        {templateids1, valid},  {templateids3, valid}, {templateids4, valid},    // All campaign types with SLIDING timespan
                        {templateids5, valid},  {templateids7, valid}, {templateids8, valid},    // All campaign types with STATIC timespan
                        {templateids9, invalid}, {templateids11, invalid}, {templateids12, invalid}  // All campaign types with SUGGESTED timespan - > valid from validtill is not used  and these templates overlap

                };
    }


    @DataProvider(name = "multipleValidTemplateWithOverLappingDp")
    public static Object[][] multipleValidTemplateWithOverLappingDp() throws IOException {

        List<Integer> templateids9 = new ArrayList<>();
        templateids9.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids9.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids10 = new ArrayList<>();
        templateids10.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids10.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        String valid = "valid";

        return new Object[][]
                {
                        {templateids9, valid}, {templateids10, valid} //Same time with different campaign

                };
    }


    @DataProvider(name = "dormantUserTestDp")
    public static Object[][] dormantUserTestDp() throws IOException {

        List<Integer> templateids1 = new ArrayList<>();
        templateids1.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids1.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.THIRTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids2 = new ArrayList<>();
        templateids2.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids2.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.SIXTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids3 = new ArrayList<>();
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.NINETY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids4 = new ArrayList<>();
        templateids4.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids4.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids5 = new ArrayList<>();
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.THIRTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.THIRTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        List<Integer> templateids6 = new ArrayList<>();
        templateids6.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.SIXTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids6.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.SIXTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        List<Integer> templateids7 = new ArrayList<>();
        templateids7.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.NINETY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids7.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.NINETY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));


        String valid = "valid";
        String invalid = "invalid";

        return new Object[][]
                {
                        {templateids1, valid}, {templateids2, valid}, {templateids3, valid},
                        {templateids4, invalid}, {templateids5, invalid}, {templateids6, invalid}, {templateids7, invalid}

                };
    }





    @DataProvider(name = "dormantUserNullAndZeroDaysDp")
    public static Object[][] dormantUserNullAndZeroDaysDp() throws IOException {

        List<Integer> templateids1 = new ArrayList<>();
        templateids1.add(Integer.valueOf(tdTemplateHelper.createTemplateWithDormantUserNull(RngConstants.FREE_DELIVERY,"false","99",null,"false")));
        templateids1.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids2 = new ArrayList<>();
        templateids2.add(Integer.valueOf(tdTemplateHelper.createTemplateWithDormantUserNull(RngConstants.FLAT,"false","99",null,"false")));
        templateids2.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids3 = new ArrayList<>();
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplateWithDormantUserNull(RngConstants.PERCENTAGE,"false","99",null,"false")));
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids4 = new ArrayList<>();
        templateids4.add(Integer.valueOf(tdTemplateHelper.createTemplateWithDormantUserNull(RngConstants.FREE_DELIVERY,"false","99",null,"false")));
        templateids4.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids5 = new ArrayList<>();
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplateWithDormantUserNull(RngConstants.FREE_DELIVERY,"false","99",null,"false")));
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        String valid = "valid";
        String invalid = "invalid";

        return new Object[][]
                {
                        {templateids1, invalid},{templateids2, invalid},{templateids3, invalid},
                        {templateids4, valid},{templateids5, valid},
                };
    }



    @DataProvider(name = "firstOrderAndDormantUserDp")
    public static Object[][] firstOrderAndDormantUserDp() throws IOException {

        //First Order
        List<Integer> templateids1 = new ArrayList<>();
        templateids1.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids1.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "true", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids2 = new ArrayList<>();
        templateids2.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.THIRTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids2.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "true", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));

        List<Integer> templateids3 = new ArrayList<>();
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.NINETY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids3.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "true", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));


        List<Integer> templateids4 = new ArrayList<>();
        templateids4.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.SIXTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));
        templateids4.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "true", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED)));


        // RFO
        List<Integer> templateids5 = new ArrayList<>();
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.SIXTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"true")));
        templateids5.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"false")));

        List<Integer> templateids6 = new ArrayList<>();
        templateids6.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.SIXTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"true")));
        templateids6.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FLAT, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"false")));

        List<Integer> templateids7 = new ArrayList<>();
        templateids7.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.SIXTY_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"true")));
        templateids7.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.PERCENTAGE, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"false")));

        List<Integer> templateids8 = new ArrayList<>();
        templateids8.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"true")));
        templateids8.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"false")));

        //Co-Exists RFO TRUE
        List<Integer> templateids9 = new ArrayList<>();
        templateids9.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"true")));
        templateids9.add(Integer.valueOf(tdTemplateHelper.createTemplateWithAllParams(RngConstants.FREE_DELIVERY, "1", "1", "false", "99", RngConstants.ZERO_DAYS_DORMANT, "false", RngConstants.TD_TEMPLATE_TIMESPANTYPE_SUGGESTED,"true")));



        String valid = "valid";
        String invalid = "invalid";

        return new Object[][]
                {
                        {templateids1, valid},{templateids2, valid},{templateids3, valid},{templateids4, valid},
                        {templateids5,valid},{templateids6,valid},{templateids7,valid},{templateids8,valid},
                        {templateids9,invalid}

                };
    }







}