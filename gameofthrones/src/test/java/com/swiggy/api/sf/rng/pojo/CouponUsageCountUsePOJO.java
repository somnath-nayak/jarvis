package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Random;

public class CouponUsageCountUsePOJO {

    @JsonProperty("code")
    private String code;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("orderId")
    private String orderId;

    public String getCode() {
        return code;
    }

    public String getUserId() {
        return userId;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public CouponUsageCountUsePOJO withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public CouponUsageCountUsePOJO setDefaultData() {
        return this.withOrderId(String.valueOf(new Random().nextInt(10000)));
    }
}
