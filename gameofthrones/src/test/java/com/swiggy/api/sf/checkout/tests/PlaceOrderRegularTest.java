
package com.swiggy.api.sf.checkout.tests;


import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.helper.CartPayloadHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.MinAmount;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PlaceOrderRegularTest {

    CheckoutHelper helper= new CheckoutHelper();
    SoftAssert softAssertion= new SoftAssert();


    static String mobile= System.getenv("mobile");
    static String password= System.getenv("password");
    static String tid;
    static String token;

    String orderId;
    String orderKey;


    final String userAgent;
    final String versionCode;
    final boolean isCartWithAddons;
    final int index;
    final String couponCode;
    final String tdType;

    Cart cartPayload;
    static Cart cartpayloadNoAddons;
    static Cart cartpayloadAddons;


    public PlaceOrderRegularTest(String userAgent, String versionCode, boolean isCartWithAddons, int index,
                                 String couponCode, String tdType) {
        super();
        this.userAgent =userAgent;
        this.versionCode =versionCode;
        this.isCartWithAddons =isCartWithAddons;
        this.index        =index;
        this.couponCode   =couponCode;
        this.tdType=tdType;
    }


    static{
        if (mobile == null || mobile.isEmpty()){
            mobile=CheckoutConstants.mobile;
        }
        if (password == null ||password.isEmpty()){
            password= CheckoutConstants.password;
        }
        HashMap<String, String> hashMap=CheckoutHelper.loginData(mobile,password);
        tid=hashMap.get("Tid");
        token=hashMap.get("Token");

        setCartPayload();
    }



    public static void setCartPayload(){
        cartpayloadNoAddons=new CartPayloadHelper().getObjectCartPayloadRegular("12.9279", "77.6271",false);
        System.out.println("cartpayloadNoAddons"+cartpayloadNoAddons);
        try{
            cartpayloadAddons=new CartPayloadHelper().getObjectCartPayloadRegular("12.9279", "77.6271",true);
            System.out.println("cartpayloadNoAddons"+cartpayloadAddons);
        } catch (Exception e){
            e.printStackTrace();
            //cartpayloadAddons=CartPayloadHelper.getObjectCartPayloadRegular("12.9279", "77.6271",true);
        }
    }


    public static String getCoupon() {
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO();
        try {
            createCouponPOJO.setDefaultData().withMinAmount(new MinAmount().withCart(0))
                    .withValidFrom(CheckoutHelper.getCurrentDate())
                    .withValidTill(CheckoutHelper.getFutureDate())
                    .withApplicableWithSwiggyMoney(true);
        } catch (Exception e){
            e.printStackTrace();
        }
        String couponCode=createCouponPOJO.getCode();
        new RngHelper().createCoupon(createCouponPOJO);
        return couponCode;
    }


   @Test(alwaysRun = false,groups = {"Regression","Sanity","Chandan"},description = "Place regular Order Tests")
   public void placeRegularCart() {

       if(isCartWithAddons){
           cartPayload=cartpayloadAddons;
       } else {
           cartPayload=cartpayloadNoAddons;
       }

       String cartResponse;
        if(couponCode!=null && !couponCode.isEmpty()){
            cartPayload.setCouponCode(getCoupon());
        }
        if(tdType.equalsIgnoreCase("noTd")){
            cartResponse=createCart(userAgent,versionCode,cartPayload);
        } else {
            createTD(tdType,cartPayload.getRestaurantId());
            cartResponse=createCart(userAgent,versionCode,cartPayload);
            validateTDOnCartResponse(cartResponse);
            disableTD(cartPayload.getRestaurantId());
        }
        helper.validateDataFromResponseNew(cartResponse,cartPayload.getRestaurantId(),
                            cartPayload.getCartItems().get(index-1).getMenuItemId(),index);

       double cancellationFee=Double.parseDouble(getCancellationFee(cartResponse));

       if (cancellationFee>0){
           invokeRevertCancellationFee(cartResponse);
       }

       String placeOrderRes=placeOrder(userAgent,versionCode,cartResponse);
       orderId=getOrderId(placeOrderRes);
       orderKey=getOrderKey(placeOrderRes);

       Assert.assertNotNull(orderId,"orderId is null");
       Assert.assertNotNull(orderKey, "orderKey is null");

       int cartTotal=(int) Double.parseDouble(getCartTotal(cartResponse));
       int orderTotal=(int) Double.parseDouble(getOrderTotal(placeOrderRes));

       //Assert.assertEquals(cartTotal,orderTotal,"Mismatch in cart total & order total");

    }


    @Test(dependsOnMethods="placeRegularCart",groups = {"Regression","Sanity","Chandan"},description = "invoke Get Last Order")
    public void invokeGetLastOrder() {

        String getLastOrderRes=getLastOrder("","");

        String lastOrderId= JsonPath.read(getLastOrderRes,"$.data.last_order.order_id")
                                .toString().replace("[","").replace("]","");
        Assert.assertEquals(lastOrderId,orderId);
    }


    @Test(dependsOnMethods="placeRegularCart",groups = {"Regression","Sanity","Chandan"},description = "invoke Get Single Order")
    public void invokeGetSingleOrder() {

        String getSingleOrderRes=getSingleOrderKey("","",orderKey);
    }


    @Test(dependsOnMethods="placeRegularCart",description = "invoke Get Order Details")
    public void invokeGetOrderDetails() {

        String getOrderDetails=getOrderDetails("", "",orderId);
    }


    @Test(dependsOnMethods="placeRegularCart",groups = {"Regression","Sanity","Chandan"},description = "invoke Get All Orders")
    public void invokeGetAllOrders() {
        String getAllOrdersRes=getAllOrders("", "");
    }

    @Test(dependsOnMethods="placeRegularCart",groups = {"Regression","Sanity","Chandan"},description = "invoke Get Minimal Order Details")
    public void invokeGetMinimalOrderDetails() {

        String getMinimalOrderDetails=getMinimalOrderDetails("","",orderKey);
    }


    @Test(dependsOnMethods="placeRegularCart",groups = {"Regression","Sanity","Chandan"},description = "invoke Cancel Order")
    public void invokeHCancelOrder(){
        if (orderId!=null || !orderId.isEmpty()) {
            String cancelOrderRes = cancelOrder("", "", orderId);
        }
    }


    public String createCart(String userAgent,String versionCode,Cart cartPayload){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String cartResponse=helper.createCart(header,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cartResponse);
        return cartResponse;
    }


    public String placeOrder(String userAgent,String versionCode,String cartResponse){
        String placeOrderRes=null;
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        //String cartResponse=createCart(userAgent,versionCode,cartPayload);
        String addressId=helper.getAddressIDfromCartResponse(tid, token, cartResponse);
        String isAddressServiceable=invokeIsAddressServicable(cartResponse);

        if (isAddressServiceable.equals(false)){
            makeAddressServiceable(cartResponse);
        }

        String paymentRes=getAllPayments(userAgent,versionCode);
        String isCodEnabled=isCodEnabled(paymentRes);

        if(isCodEnabled.equals("true")){
            placeOrderRes=helper.orderPlaceV2(header, addressId, superConstant.PAYMENT_METHOD_CASH,superConstant.DEF_STRING)
                    .ResponseValidator.GetBodyAsText();
            helper.validateApiResStatusData(placeOrderRes);
        } else {
            //throw new SkipException("Skipping test, COD is disabled");
        }
        return placeOrderRes;
    }



    public String getAllPayments(String userAgent,String versionCode){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String getAllPaymentsRes=helper.getAllPayments(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getAllPaymentsRes);
        return getAllPaymentsRes;
    }


    public String getOrderId(String placeOrderRes){
        String orderId=JsonPath.read(placeOrderRes, "$.data..order_id").toString().replace("[","").replace("]","");
        System.out.println("Order Id:--->"+orderId);
        return orderId;
    }

    public String getOrderKey(String placeOrderRes){
        String[] rendkeys=JsonPath.read(placeOrderRes, "$.data..order_data..rendering_details..key")
                .toString().replace("[","").replace("]","").replace("\"","").split(",");
        String[] keys=JsonPath.read(placeOrderRes, "$.data..order_data..key")
                .toString().replace("[","").replace("]","").replace("\"","").split(",");

        ArrayList<String> key = new ArrayList<String>();
        for(int i = 0; i < keys.length; i++){
            if(!Arrays.asList(rendkeys).contains(keys[i]))
                key.add(keys[i]);
        }
        System.out.println("Order Key:--->"+key.get(0));
        return key.get(0);
    }

    public String getOrderDetails(String userAgent,String versionCode,String OrderID){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String getOrderRes=helper.getOrderDetails(header,OrderID)
                .ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getOrderRes);
        return getOrderRes;
    }

    public String getSingleOrderKey(String userAgent,String versionCode,String orderKey){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String getSingleOrderRes=helper.getSingleOrderKey(header,orderKey)
                .ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getSingleOrderRes);
        return getSingleOrderRes;
    }

    public String getLastOrder(String userAgent,String versionCode){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String getLastOrderRes=helper.getLastOrder(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getLastOrderRes);
        return getLastOrderRes;
    }

    public String getMinimalOrderDetails(String userAgent,String versionCode,String orderKey){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String getLastOrderRes=helper.getMinimalOrderDetails(header,orderKey).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getLastOrderRes);
        return getLastOrderRes;
    }

    public String getAllOrders(String userAgent,String versionCode){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String getAllOrdersRes=helper.getAllOrders(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getAllOrdersRes);
        return getAllOrdersRes;
    }

    public String cancelOrder(String userAgent,String versionCode,String OrderID){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String cancelOrderRes=helper.OrderCancel(header, superConstant.DEF_STRING,
                superConstant.EXPECTED_TRUE_FLAG, superConstant.ZERO_AS_STRING, superConstant.DEF_STRING, OrderID)
                .ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cancelOrderRes);
        return cancelOrderRes;
    }

    public void createTD(String tdType,String restID) {
        try{
        if(tdType.equalsIgnoreCase("flatTD")){
            new RngHelper().createFlatWithMinCartAmountAtRestaurantLevel("0",restID,"1");
        } else if(tdType.equalsIgnoreCase("FreeDelTD")) {
            new RngHelper().createFreeDeliveryTD(restID, false, false, false, false);
        }} catch (Exception e){
            e.printStackTrace();
        }
    }

    public void disableTD(String restID){
        new RngHelper().disabledActiveTD(restID);
    }

    public void validateTDOnCartResponse(String response){
        String tdCampaign= JsonPath.read(response, "$.data.appliedTradeCampaignHeaders")
                .toString().replace("[", "").replace("]", "");
        String trade_SubTotal= JsonPath.read(response, "$.data.cart_menu_items..subtotal_trade_discount")
                .toString().replace("[", "").replace("]", "");
        Double tdTotal = JsonPath.read(response, "$.data.trade_discount_total");
        softAssertion.assertNotNull(tdCampaign);
        softAssertion.assertNotEquals(trade_SubTotal,0);
        }


    public String isCodEnabled(String paymentRes){
        String isCodEnabled="";
        String[] groupName=JsonPath.read(paymentRes, "$.data..payment_group[*]..group_name")
                .toString().replace("[","").replace("]","").replace("\"","")
                .split(",");
        for (int i=0;i<groupName.length;i++){
            System.out.println(groupName[i]);
            if(groupName[i].equals("cod")){
                isCodEnabled=JsonPath.read(paymentRes, "$.data..payment_group.["+i+"]..payment_methods..enabled")
                        .toString().replace("[","").replace("]","");
            }
        }
        return isCodEnabled;
    }


    public String getCancellationFee(String cartResponse){
        String cancellationFee= JsonPath.read(cartResponse, "$.data.cancellation_fee")
                .toString().replace("[", "").replace("]", "");
        return cancellationFee;
    }

    public void invokeRevertCancellationFee(String cartResponse){
        String cancellationFee=getCancellationFee(cartResponse);
        String getLastOrderRes=getLastOrder("","");
        String lastOrderID= JsonPath.read(getLastOrderRes, "$.data.last_order.order_id")
                                        .toString().replace("[", "").replace("]", "");
        helper.revertCancellationFeePayload(lastOrderID,"Test",cancellationFee,"GOT");
    }

    public String getCartTotal(String cartResponse){
        String cartTotal= JsonPath.read(cartResponse, "$.data.cart_total")
                .toString().replace("[", "").replace("]", "");
        return cartTotal;
    }

    public String getOrderTotal(String orderResponse){
        String orderTotal= JsonPath.read(orderResponse, "$.data..order_total")
                .toString().replace("[", "").replace("]", "");
        return orderTotal;
    }

    public String invokeIsAddressServicable(String cartResponse){

        String lat= JsonPath.read(cartResponse, "$.data.restaurant_details.lat")
                                            .toString().replace("[", "").replace("]","").replace("\"","");
        String lng= JsonPath.read(cartResponse, "$.data.restaurant_details.lng")
                                            .toString().replace("[", "").replace("]","").replace("\"","");

        String addresServiceableRes= helper.isAddresServicable(tid,token,lat,lng).ResponseValidator.GetBodyAsText();

        String isAddressServiceable= JsonPath.read(addresServiceableRes, "$.data.is_address_serviceable")
                .toString().replace("[", "").replace("]","").replace("\"","");

        return isAddressServiceable;
    }

    public void makeAddressServiceable(String cartResponse){

        String restId= JsonPath.read(cartResponse, "$.data.restaurant_details.id")
                .toString().replace("[", "").replace("]","").replace("\"","");
        try {
            helper.invokeMakeServicableApi(restId);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}