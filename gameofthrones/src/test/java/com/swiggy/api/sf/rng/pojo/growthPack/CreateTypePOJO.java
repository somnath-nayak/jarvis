package com.swiggy.api.sf.rng.pojo.growthPack;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "name",
            "meta",
            "created_by"
    })
    public class CreateTypePOJO {

        @JsonProperty("name")
        private String name;
        @JsonProperty("meta")
        private TypeMetaPOJO meta;
        @JsonProperty("created_by")
        private String created_by;

        /**
         * No args constructor for use in serialization
         *
         */
        public CreateTypePOJO() {
        }

        /**
         *
         * @param created_by
         * @param name
         * @param meta
         */
        public CreateTypePOJO(String name, TypeMetaPOJO meta, String created_by) {
            super();
            this.name = name;
            this.meta = meta;
            this.created_by = created_by;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("meta")
        public TypeMetaPOJO getMeta() {
            return meta;
        }

        @JsonProperty("meta")
        public void setMeta(TypeMetaPOJO meta) {
            this.meta = meta;
        }

        @JsonProperty("created_by")
        public String getCreated_by() {
            return created_by;
        }

        @JsonProperty("created_by")
        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("name", name).append("meta", meta).append("created_by", created_by).toString();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(created_by).append(name).append(meta).toHashCode();
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if ((other instanceof CreateTypePOJO) == false) {
                return false;
            }
            CreateTypePOJO rhs = ((CreateTypePOJO) other);
            return new EqualsBuilder().append(created_by, rhs.created_by).append(name, rhs.name).append(meta, rhs.meta).isEquals();
        }




        public CreateTypePOJO withName(String name) {
             this.name=name;
             return this;
        }
        public CreateTypePOJO withTypeMeta(TypeMetaPOJO meta) {
            this.meta=meta;
            return this;
        }

        public CreateTypePOJO withCreatedBy(String created_By) {
            this.created_by =created_By;
            return this;
        }
    public CreateTypePOJO setDefault() {
            return this.withName("TurboType")
                    .withTypeMeta(new TypeMetaPOJO().setDefault())
                    .withCreatedBy("binit.anand@swiggy.in");
    }

}
