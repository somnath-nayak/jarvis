package com.swiggy.api.sf.snd.helper;



import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.pojo.DD.*;
import com.swiggy.api.sf.snd.pojo.ItemSet;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.KafkaHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class SANDHelper implements SANDConstants {
    //Initialize gameofthrones = new Initialize();
    Initialize gameofthrones =Initializer.getInitializer();

    RedisHelper redisHelper = new RedisHelper();
    CMSHelper cmsHelper = new CMSHelper();
    SnDHelper sDHelper = new SnDHelper();
    KafkaHelper kafkaHelper = new KafkaHelper();
    HeaderHelper headerHelper = new HeaderHelper();
    Random random = new Random();
    Date date = new Date();

    public String tid = null;
    public String token = null;
    public String customerId = null;
    public String deviceId = null;
    public String itemId = null;
    public String restId = null;
    public String quantity = null;
    public String orderId = null;
    private Processor processor;


    /**
     * Assertion method for Smoke Testing
     *
     * @param statusCode
     * @param statusMessage
     * @param processor
     */
    public void smokeCheck(int statusCode, String statusMessage, Processor processor) {
        Reporter.log("Expected StatusCode = '" + statusCode + "' <==> Actual StatusCode = '" + processor.ResponseValidator.GetNodeValueAsInt("statusCode") + "'", true);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode, "StatusCode mismatch");
        Reporter.log("Expected StatusMessage = '" + statusMessage + "' <==> Actual StatusMessage = '" + processor.ResponseValidator.GetNodeValue("statusMessage") + "'", true);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage, "StatusMessage mismatch");
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param payload
     * @return Processor instance
     */
    public Processor login(String[] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();

        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "loginV22", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor login1(String mobile, String password) {
        GameOfThronesService gots = new GameOfThronesService("sand", "loginV22", gameofthrones);
        return new Processor(gots, contentType(), new String[]{mobile, password});
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @return Processor instance
     */
    public Processor getBlackZone(String headersAuthorization) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", headersAuthorization);    //"Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
        GameOfThronesService gots = new GameOfThronesService("sand", "getblackzone", gameofthrones);
        Processor processor = new Processor(gots, requestHeader);
        return processor;
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param lat
     * @param lng
     * @param headersTID
     * @param headersToken
     * @return Processor instance
     */
    public Processor getListingV3(String lat, String lng, String headersTID, String headersToken) {
        String[] queryParam = {lat, lng};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Tid", headersTID);
        requestHeader.put("token", headersToken);

        GameOfThronesService gots = new GameOfThronesService("sand", "getlistingv3", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param lat
     * @param lng
     * @param headersTID
     * @param headersToken
     * @return Processor instance
     */
    public Processor getListingV2(String lat, String lng, String headersTID, String headersToken, String headersDeviceID) {
        String[] queryParam = {lat, lng};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Tid", headersTID);
        requestHeader.put("token", headersToken);
        requestHeader.put("deviceId", headersDeviceID);

        GameOfThronesService gots = new GameOfThronesService("sand", "getlistingv2", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param lat
     * @param lng
     * @param restID
     * @param headersXDebugAuth
     * @return Processor instance
     */
    public Processor listingDebug(String lat, String lng, String restID, String headersXDebugAuth) {
        String[] queryParam = {lat, lng, restID};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("X-debug-auth", headersXDebugAuth);

        GameOfThronesService gots = new GameOfThronesService("sand", "listingdebug", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param payload
     * @return Processor instance
     */
    public Processor aggregator(String[] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "aggregator", gameofthrones);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }


    /**
     * Returns full JSON response as Processor instance
     *
     * @param restID
     * @return Processor instance
     */
    public Processor getRestaurantMenu(String restID) {
        String[] queryParam = {restID};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "restaurantmenu", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param restaurantID
     * @param headersToken
     * @return Processor instance
     */
    public Processor getPolygon(String[] restaurantID, String headersToken) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", headersToken);
        GameOfThronesService gots = new GameOfThronesService("sand", "getpolygon", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, restaurantID);
        return processor;
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param restaurantID
     * @param headersAuthorization
     * @return Processor instance
     */

    public Processor getResraurantHolidaySlots(String[] restaurantID, String headersAuthorization) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", headersAuthorization);
        GameOfThronesService gots = new GameOfThronesService("sand", "getResraurantHolidaySlots", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, restaurantID);
        return processor;
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param payload
     * @param headersAuthorization
     * @return Processor instance
     */

    public Processor createRestaurantHolidaySlots(String[] payload, String headersAuthorization) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", headersAuthorization);
        GameOfThronesService gots = new GameOfThronesService("sand", "createresraurantholidayslots", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param holidaySlotID
     * @param headersAuthorization
     * @return Processor instance
     */
    public Processor deleteRestaurantHolidaySlots(String[] holidaySlotID, String headersAuthorization) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Authorization", headersAuthorization);
        GameOfThronesService gots = new GameOfThronesService("sanduser", "deleteresraurantholidayslots", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, holidaySlotID);
        return processor;
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param payload
     * @param headersAuthorization
     * @return Processor instance
     */
    public Processor updateRestaurantHolidaySlots(String[] payload, String headersAuthorization) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", headersAuthorization);
        GameOfThronesService gots = new GameOfThronesService("sand", "updateresraurantholidayslots", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }


    public Processor menuV4RestId(String restID, String lat, String lng) {
        GameOfThronesService service = new GameOfThronesService("sand", "menuV4", gameofthrones);
        return new Processor(service, contentType(), null, new String[]{restID, lat, lng});
    }
    public Processor menuV4RestId(String restID) {
        GameOfThronesService service = new GameOfThronesService("sand", "menuv4withoutlatlng", gameofthrones);
        return new Processor(service, contentType(), null, new String[]{restID});
    }

    public Processor menuV4RestId(String restID, String lat, String lng, String tid, String token) {
        GameOfThronesService service = new GameOfThronesService("sand", "menuV4", gameofthrones);
        return new Processor(service, headerHelper.requestHeader(tid, token), null, new String[]{restID, lat, lng});
    }

    /**
     * Returns full JSON response as Processor instance
     *
     * @param lat
     * @param lng
     * @param headersCustomerID
     * @return Processor instance
     */

    public Processor favorites(String lat, String lng, String headersCustomerID) {
        String[] queryParams = {lat, lng};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("customerId", headersCustomerID);
        GameOfThronesService gots = new GameOfThronesService("sand", "favorites", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParams);
        return processor;
    }


    /**
     * Returns full JSON response as Processor instance
     *
     * @param lat
     * @param lng
     * @param searchKeyword
     * @param page
     * @return Processor instance
     */
    public Processor searchV2(String lat, String lng, String searchKeyword, String page) {
        String[] queryParams = {lat, lng, searchKeyword, page};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "searchV2", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParams);
        return processor;
    }

    public Processor searchV2(String lat, String lng, String searchKeyword) {
        String[] queryParams = {lat, lng, searchKeyword};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "searchV2", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParams);
        return processor;
    }

    public Processor samay(String[] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sandsamay", "samay", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }


    public Processor similarRestaurants(String lat, String lng, String restId) {
        String[] queryParams = {lat, lng, restId};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "similarRestaurantsV1", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParams);
        return processor;
    }


    public Processor setFavorite(String[] payload, String headersTid, String headersToken, String headersCustomerId) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Tid", headersTid);
        requestHeader.put("token", headersToken);
        requestHeader.put("customerId", headersCustomerId);
        GameOfThronesService gots = new GameOfThronesService("sand", "setfavorite", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }


    public Processor setFavorite(String mobile, String password, String[] payload) {
        CheckoutHelper checkoutHelper = new CheckoutHelper();
        HashMap<String, String> tokenData = checkoutHelper.TokenData(mobile, password);
        GameOfThronesService gots = new GameOfThronesService("sand", "setfavorite", gameofthrones);
        Processor processor = new Processor(gots, tokenData, payload);
        return processor;
    }


    public Processor quickMenu(String restId, String categories) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "quickmenu", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, new String[]{restId, categories});
        return processor;
    }


    public String listingTagExclusiveRestaurant() {
        List<Map<String, Object>> restList = cmsHelper.getExclusiveRestaurant(1);
        if (restList.size() <= 0) {
            Assert.fail("No such restaurants found in DB..!!");
        }
        String latlng = restList.get(1).get("lat_long").toString();
        String lat = latlng.split(",")[0];
        String lng = latlng.split(",")[1];
        Processor processor = sDHelper.getAggregatorDetails(lat, lng);
        return JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants." + restList.get(1).get("id") + ".dynamicTags").toString();
    }


    public String listingTagNonExclusiveRestaurant() {
        List<Map<String, Object>> restList = cmsHelper.getExclusiveRestaurant(0);
        if (restList.size() <= 0) {
            Assert.fail("No such restaurants found in DB..!!");
        }
        String latlng = restList.get(1).get("lat_long").toString();
        String lat = latlng.split(",")[0];
        String lng = latlng.split(",")[1];
        Processor processor = sDHelper.getAggregatorDetails(lat, lng);
        return JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants." + restList.get(3).get("id") + ".dynamicTags").toString();
    }


    public String listingTagNewArrivalRestaurant() {
        List<Map<String, Object>> restList = cmsHelper.getNewArrivalRestaurant();
        if (restList.size() <= 0) {
            Assert.fail("No such restaurants found in DB..!!");
        }
        String latlng = restList.get(1).get("lat_long").toString();
        String lat = latlng.split(",")[0];
        String lng = latlng.split(",")[1];
        Processor processor = sDHelper.getAggregatorDetails(lat, lng);
        return JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants." + restList.get(1).get("id") + ".dynamicTags").toString();
    }


    public String listingTagNonNewArrivalRestaurant() {
        List<Map<String, Object>> restList = cmsHelper.getNonNewArrivalRestaurant();
        if (restList.size() <= 0) {
            Assert.fail("No such restaurants found in DB..!!");
        }
        String latlng = restList.get(1).get("lat_long").toString();
        String lat = latlng.split(",")[0];
        String lng = latlng.split(",")[1];
        Processor processor = sDHelper.getAggregatorDetails(lat, lng);
        return JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants." + restList.get(3).get("id") + ".dynamicTags").toString();
    }


    public String[] getListingTagPromotedRestaurant(int cityId, boolean isPromoted) {
        Processor psProcessor = prioritySlots(cityId);
        String restId = getLiveRestaurantFromPrioritySlots(psProcessor, isPromoted)[0];
        String lat_lng = new CMSHelper().getLatLngByRestID(restId);
        Processor aggregatorProcessor = sDHelper.getAggregatorDetails(lat_lng.split(",")[0], lat_lng.split(",")[1]);

        return new String[]{(JsonPath.read(aggregatorProcessor.ResponseValidator.GetBodyAsText(), "$.data.restaurants." + restId + ".dynamicTags").toString()), restId};
    }

    public String getDateForNewArrivalRestaurant() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -14);
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime());
    }


    public Processor presentationController(String mobile, String password, String[] payload) {
        CheckoutHelper checkoutHelper = new CheckoutHelper();
        HashMap<String, String> tokenData = checkoutHelper.TokenData(mobile, password);
        GameOfThronesService gots = new GameOfThronesService("sand", "presentationcontroller", gameofthrones);
        Processor processor = new Processor(gots, tokenData, payload);
        return processor;
    }

    public Processor prioritySlots(int cityId) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sandpriorityslot", "priorityslot", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, new String[]{String.valueOf(cityId)});
        return processor;
    }


    public String[] getLiveRestaurantFromPrioritySlots(Processor processor, boolean isPromoted) {
        List<Map<String, Object>> restNameList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.live");
        for (int i = 0; i < restNameList.size(); i++) {
            int startSlot = Integer.parseInt(String.valueOf(restNameList.get(i).get("slot_start")));
            int endSlot = Integer.parseInt(String.valueOf(restNameList.get(i).get("slot_end")));
            boolean actualPromoted = Boolean.valueOf(String.valueOf(restNameList.get(i).get("promoted")));
            if (!(String.valueOf(restNameList.get(i).get("name")).toLowerCase().contains("domino"))
                    && getCurrentTimeInHHMM() >= startSlot && getCurrentTimeInHHMM() <= endSlot
                    && getCurrentTimeInHHMM() >= startSlot && getCurrentTimeInHHMM() <= endSlot
                    && actualPromoted == isPromoted
                    ) {
                return new String[]{String.valueOf(restNameList.get(i).get("rest_id")), String.valueOf(restNameList.get(i).get("name")),
                        String.valueOf(restNameList.get(i).get("position"))};
            }
        }
        Assert.fail("No Live restaurant found with Promted as " + isPromoted + ", excluding Domino's");
        return null;
    }


    public int getCurrentTimeInHHMM() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
        String time = sdf.format(new Date(cal.getTimeInMillis()));
        return Integer.parseInt(time);
    }


    public String[] listingTagPosition(int cityId, boolean isPromoted) {
        SnDHelper sDHelper = new SnDHelper();
        Processor psProcessor = prioritySlots(cityId);
        String[] liveRest = getLiveRestaurantFromPrioritySlots(psProcessor, isPromoted);
        String restId = liveRest[0];
        int liveRestPosition = Integer.parseInt(liveRest[2]) - 1;
        String lat_lng = new CMSHelper().getLatLngByRestID(restId);
        Processor aggregatorProcessor = sDHelper.getAggregatorDetails(lat_lng.split(",")[0], lat_lng.split(",")[1]);
        return new String[]{(JsonPath.read(aggregatorProcessor.ResponseValidator.GetBodyAsText(),
                "$.data.sortedRestaurants[" + liveRestPosition + "].restaurantId").toString()),
                restId, liveRest[1]};
    }



    public Set<String> crossSellingRedis(String itemId, String restId) {
        List<HashMap<String, String>> restCatList = getCrossSellingRestaurantCategory(restId, itemId);
        Set<String> itemIdSet = new HashSet<>();
        for (int i = 0; i < restCatList.size(); i++) {
            List<HashMap<String, String>> crossSellingRestItem = getCrossSellingRestaurantItem(restId, restCatList.get(i).get("name"));
            for (int j = 0; j < crossSellingRestItem.size(); j++) {
                itemIdSet.add(crossSellingRestItem.get(j).get("id"));
            }
        }
        return itemIdSet;
    }


    public String getItemTag(String itemId) {
        return JsonPath.read(redisconnectget("SC_ITEM_TAGS_", itemId, 0), "category");
    }

    public List<HashMap<String, String>> getCrossSellingRestaurantCategory(String restId, String itemId) {
        return JsonPath.read(redisconnectget("CROSS_SELLING_RESTAURANT_CATEGORY_", restId, 5), "$.[\"" + getItemTag(itemId) + "\"][*]");
    }

    public List<HashMap<String, String>> getCrossSellingRestaurantItem(String restId, String crossSellingRestaurantCategory) {
        return JsonPath.read(redisconnectget("CROSS_SELLING_RESTAURANT_ITEM_", restId, 5), "$.[\"" + crossSellingRestaurantCategory + "\"][*]");
    }

    /**
     * Srishty Start
     * ****************************************************************************
     */

    public HashMap<String, String> generateHeaders(String customerId, String tid, String deviceId, String useragent, String token) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("customerId", customerId);
        requestHeader.put("tid", tid);
        requestHeader.put("deviceId", deviceId);
        requestHeader.put("user-agent", useragent);
        requestHeader.put("token", token);
        return requestHeader;

    }

    public Processor getLocationBasedFeature(String lat, String lng, String locationFeature) {
        String[] queryParams = {lat, lng, locationFeature};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "locationBasedFeature", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParams);
        return processor;
    }

   public Processor launch(String experiment, String mobile, String password, String itemId, String restId, String quantity) throws Exception {
        CheckoutHelper checkoutHelper = new CheckoutHelper();
        HashMap<String, String> hm = checkoutHelper.TokenData(mobile, password);
        customerId = hm.get("CustomerId");
        tid = hm.get("Tid");
        deviceId = hm.get("DeviceId");
        token = hm.get("Token");
        String useragent = SANDConstants.UserAgent[0];
        Processor proc = checkoutHelper.placeOrderWithTidToken(itemId, quantity, restId, tid, token);
      //  orderId = orderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.order_id");
        System.out.println("printing OrderId ---------" + orderId);
        GameOfThronesService gots = new GameOfThronesService("sand", "launchv1", gameofthrones);
        HashMap<String, String> requestHeader = generateHeaders(customerId, tid, deviceId, useragent, token);
        Processor processor = new Processor(gots, requestHeader, new String[]{experiment});
        return processor;
    }

 public Processor getLaunchFeedback(String experiment) {
        String useragent = SANDConstants.UserAgent[0];
        GameOfThronesService gots = new GameOfThronesService("sand", "launchv1", gameofthrones);
        HashMap<String, String> requestHeader = generateHeaders(customerId, tid, deviceId, useragent, token);
        Processor processor = new Processor(gots, requestHeader, new String[]{experiment});
        return processor;
    }

    public Processor getSettings(String option1, String val1, String option2, String val2) {
        String useragent = SANDConstants.UserAgent[0];
        String[] queryParams = {option1, val1, option2, val2};
        GameOfThronesService gots = new GameOfThronesService("sand", "settings", gameofthrones);
        HashMap<String, String> requestHeader = generateHeaders(customerId, tid, deviceId, useragent, token);
        Processor processor = new Processor(gots, requestHeader, null, queryParams);
        return processor;
    }

    public Processor getAreaListing(String area) {
        String useragent = SANDConstants.UserAgent[0];
        String[] queryParams = {area};
        GameOfThronesService gots = new GameOfThronesService("sand", "areaListing", gameofthrones);
        HashMap<String, String> requestHeader = generateHeaders(customerId, tid, deviceId, useragent, token);
        Processor processor = new Processor(gots, requestHeader, null, queryParams);
        return processor;
    }

    public String redisconnectget(String keyinitial, String entity, int redisdb) {
        RedisHelper red = new RedisHelper();
        String s = keyinitial;
        String key = s.concat(entity);
        String value = (String) red.getValue("analyticsredis", redisdb, key);
        return value;

    }

    public int getTime(LocalDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmm");
        String text = date.format(formatter);
        int parsedTime = Integer.parseInt(text);
        return parsedTime;
    }

    public int getSlotIdforItem(int parsedTime) {
        int slotId = 0;
        if (parsedTime >= 0 && parsedTime <= 700)
            slotId = 0;
        else if (parsedTime >= 700 && parsedTime <= 1130)
            slotId = 1;
        else if (parsedTime >= 1130 && parsedTime <= 1530)
            slotId = 2;
        else if (parsedTime >= 1530 && parsedTime <= 1830)
            slotId = 3;
        else if (parsedTime >= 1830 && parsedTime <= 2300)
            slotId = 4;
        else if (parsedTime >= 2300 && parsedTime <= 2400)
            slotId = 0;
        return slotId;
    }

    public Processor menuV3(String restID) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "menuv3RestId", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, new String[]{restID});
        return processor;
    }

    public Processor crossSelling(String[] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sandcrossselling", "crossselling", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }


    public org.json.JSONArray getJsonArrayForCrossSelling(String[][] items) {
        org.json.JSONArray jsonArray = new org.json.JSONArray();
        JSONObject obj = null;
        for (int i = 0; i < items.length; i++) {
            try {
                obj = new JSONObject();
                obj.put("itemId", items[i][0]);
                obj.put("quantity", items[i][1]);
                obj.put("globalCategory", items[i][2]);
                obj.put("globalSubCategory", items[i][3]);

            } catch (JSONException e) {
            }
            jsonArray.put(obj);
        }
        return jsonArray;
    }
    /**
     * Srishty End
     * ############################################################################
     */


    /***
     * Solr indexing api
     * @param restId
     * @return
     */

    public Processor solrIndexing(String restId) {
        HashMap<String, String> requestheaders = new HashMap<String, String>() {{
            put("content-type", "application/json");
        }};
        GameOfThronesService service = new GameOfThronesService("sandsolr", "solrindex", gameofthrones);
        return new Processor(service, requestheaders, null, new String[]{restId});
    }

    /***
     * Create City Polygon
     * @param name
     * @param tag
     * @param latLng
     * @return
     */
    public Processor cityPolygon(String name, String tag, String latLng) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
        GameOfThronesService service = new GameOfThronesService("sand", "citypolygon", gameofthrones);
        if (name == null) {
            name = "ji";
        }
        String[] payloadparams = {name, tag, latLng};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }


    public HashMap<String, String> loginData(String mobile, String password) {
        GameOfThronesService service = new GameOfThronesService("sand", "consumerloginv2", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        Processor p = new Processor(service, headers, new String[]{mobile, password});
        String resp = p.ResponseValidator.GetBodyAsText();
        String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
        String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
        String userId = JsonPath.read(resp, "$.data.customer_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        HashMap<String, String> hashMap = new HashMap<String, String>() {{
            put("Tid", tid);
            put("Token", token);
            put("userId", userId);
        }};
        return hashMap;
    }

    public Processor loginDataProcessor(String mobile, String password) {
        GameOfThronesService service = new GameOfThronesService("sand", "consumerloginv2", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        Processor p = new Processor(service, headers, new String[]{mobile, password});
        return p;
    }


    public void changeSLA(String delivery, String maxSla, String minSla, String zoneId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(delivery);
        int a = sqlTemplate.update(SANDConstants.updateSla + maxSla + SANDConstants.updateSla1 + minSla + SANDConstants.whereId + zoneId);
        System.out.println(a);
    }

    public void rainModeType(String delivery, String rainModeType, String zoneId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(delivery);
        int a = sqlTemplate.update(SANDConstants.rainModeType + rainModeType + SANDConstants.whereId + zoneId);
        System.out.println(a);
    }

    public void zoneRainParams(String delivery, String lastMile, String zoneId) {
        SystemConfigProvider.getTemplate(delivery).update(SANDConstants.zoneRainParams + SANDConstants.whereId + zoneId);
    }

    public String basic() {
        String s = null;
        try {
            s = Base64.getEncoder().encodeToString((SANDConstants.userName + ":" + SANDConstants.pass).getBytes("utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Basic " + s;
    }

    public HashMap<String, String> getHeader() {
        HashMap<String, String> hm = new HashMap<>();
        hm.put("Content-Type", "application/json");
        hm.put("Authorization", basic());
        return hm;
    }

    public HashMap<String, String> getTidHeader() {
        HashMap<String, String> hm = loginData(SANDConstants.mobile, SANDConstants.password);
        hm.put("Content-Type", "application/json");
        return hm;
    }

    private HashMap<String, String> getHeaderTid() {
        HashMap<String, String> hm = loginData(SANDConstants.mobile, SANDConstants.password);
        hm.put("Content-Type", "application/json");
        hm.put("Authorization", basic());
        return hm;
    }

    public String getMobile() {
        return String.valueOf((long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L);
    }

    public Processor assetHistory(String historyId) {
        GameOfThronesService service = new GameOfThronesService("sand", "assethistory", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{historyId});
    }

    public Processor assetUser(String userId) {
        GameOfThronesService service = new GameOfThronesService("sand", "assetuser", gameofthrones);
        return new Processor(service, getHeaderTid(), null, new String[]{userId});
    }

    public Processor userInternal(String referralCode, String value) {
        GameOfThronesService service = new GameOfThronesService("sand", "userinternal", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{referralCode, value});
    }

    public Processor userInternalWithoutAuth(String referralCode, String value) {
        HashMap<String, String> hm = new HashMap<>();
        hm.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sand", "userinternal", gameofthrones);
        return new Processor(service, hm, null, new String[]{referralCode, value});
    }


    public Processor userProfileAdmin(String param, String key) {
        GameOfThronesService service = new GameOfThronesService("sand", "userprofileadmin", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{param + "=" + key});
    }

    public Processor refreshBlock() {
        GameOfThronesService service = new GameOfThronesService("sand", "refreshblockdata", gameofthrones);
        return new Processor(service, getHeader(), null, null);
    }

    public Processor userInternalDisable(String userId) {
        GameOfThronesService service = new GameOfThronesService("sand", "userinternaldisable", gameofthrones);
        return new Processor(service, getHeader(), new String[]{userId}, new String[]{userId});
    }

    public Processor userInternalpatch(String key,String mobile, String userId) {
        GameOfThronesService service = new GameOfThronesService("sand", "userinternalpatch", gameofthrones);
        return new Processor(service, getHeader(), new String[]{key,mobile}, new String[]{userId});
    }

    public Processor userProfileGet(String tid, String token, String addressReqd, String detailsReqd) {
        HashMap<String, String> hMap = new HashMap<String, String>() {{
            put("tid", tid);
            put("token", token);
            contentType();
        }};
        GameOfThronesService service = new GameOfThronesService("sand", "userprofile", gameofthrones);
        return new Processor(service, hMap, null, new String[]{addressReqd, detailsReqd});
    }


    public Processor userProfileUpdate(String addressReqd, String detailsReqd) {
        GameOfThronesService service = new GameOfThronesService("sand", "userprofileupdate", gameofthrones);
        return new Processor(service, getTidHeader(), null, new String[]{addressReqd, detailsReqd});
    }

    public Processor loginCheck(String mobile) {
        GameOfThronesService service = new GameOfThronesService("sand", "logincheckv2", gameofthrones);
        return new Processor(service, contentType(), null,new String[]{mobile});
    }

    public Processor updateMobile(String mobile, String tid, String token) {
        GameOfThronesService service = new GameOfThronesService("sand", "updatemobile", gameofthrones);
        return new Processor(service, header(tid, token), null, new String[]{mobile});
    }

    public Processor setPassword(String mobile, String password, String tid, String token) {
        GameOfThronesService service = new GameOfThronesService("sand", "passwordsetv2", gameofthrones);
        return new Processor(service, header(tid, token), new String[]{mobile,password});
    }


    private HashMap<String, String> header(String tid, String token) {
        HashMap<String, String> hashMap = new HashMap<String, String>() {{
            put("tid", tid);
            put("token", token);
            contentType();
        }};
        return hashMap;
    }

    public Processor updateMobileOtp(String tid, String token, String mobile, String otp) {
        GameOfThronesService service = new GameOfThronesService("sand", "updatemobileotp", gameofthrones);
        return new Processor(service, header(tid, token), new String[]{mobile, otp});
    }

    public Processor updateEmail(String emailId, String previousEmailId, String userId) throws UnsupportedEncodingException {
        GameOfThronesService service = new GameOfThronesService("sand", "updateemail", gameofthrones);
        return new Processor(service, getTidHeader(), new String[]{emailId, previousEmailId, userId});
    }

    public Processor updateEmail(String uId) {
        GameOfThronesService service = new GameOfThronesService("sand", "emailverify", gameofthrones);
        return new Processor(service, getTidHeader(), null, new String[]{uId});
    }


    public Map<String, Object> referralCode(String userId) {
        String s=SANDConstants.referralCode + "'" + userId + "'";
        System.out.println(s);
        Map<String, Object> referral = SystemConfigProvider.getTemplate(CmsConstants.cmsDB).queryForMap(SANDConstants.referralCode + "'" + userId + "'");
        return referral;
    }

    public Map<String, Object> userSignUp(String mobile) {
        Map<String, Object> referral = SystemConfigProvider.getTemplate("cms").queryForMap(SANDConstants.signUp + "'" + mobile + "'");
        return referral;
    }

    public String getOTP(String mobile) {
        return redisHelper.getValue(SANDConstants.sandRedis, 0, mobile + SANDConstants.otp).toString();
    }

    public String getotpFromRedis(String tid) {
        return JsonPath.read(redisHelper.getValue(SANDConstants.sandRedis, 0, tid).toString(), "user.otp");
    }


    public String updateMobileRedis(String userId, String newMobile){
        return redisHelper.getValue("sandredisstage", 0, SANDConstants.mobileUpdateKey + userId + "_" + newMobile).toString();
    }

    public Processor signUp1 (String mobile, String email, String name){
        GameOfThronesService service = new GameOfThronesService("sand", "signupv1", gameofthrones);
        return new Processor(service, contentType(), null, new String[]{mobile, email, name});
    }

    public Processor sendOTP(String mobile){
        GameOfThronesService service = new GameOfThronesService("sand", "otpsendv2", gameofthrones);
        return new Processor(service, contentType(), null, new String[]{mobile});
    }

    public Processor esUpdate(String indexName){
        GameOfThronesService service = new GameOfThronesService("sandes", "esupdate", gameofthrones);
        return new Processor(service, contentType(), null, new String[]{indexName});
    }

    public Processor verifyOTP(String tid, String otp){
        GameOfThronesService service = new GameOfThronesService("sand", "otpverifyv1", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>() {{
            contentType();
            put("tid", tid);
        }};
        return new Processor(service, headers, null, new String[]{getotpFromRedis(tid)});
    }

    public Processor verifyOTP1 (String tid){
        GameOfThronesService service = new GameOfThronesService("sand", "otpverifyv1", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>() {{
            contentType();
            put("tid", tid);
        }};
        return new Processor(service, headers, null, new String[]{getotpFromRedis(tid)});
    }

    public Processor signUpV2(String name, String mobile, String email, String password){
        GameOfThronesService service = new GameOfThronesService("sand", "signupv2", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>() {{
            contentType();
            put("tid", tid);
        }};
        return  new Processor(service, headers,  new String []{name,mobile,email,password});
    }



    public Processor userProfileAdminSuper (String param, String key, String...optionalKeys){
        GameOfThronesService service = new GameOfThronesService("sand", "userprofileadminsuper", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{param + "=" + key, convertArray(optionalKeys)});
    }

    public Processor userProfileAdminSuperWithoutAuth (String param, String key, String...optionalKeys){
        GameOfThronesService service = new GameOfThronesService("sand", "userprofileadminsuper", gameofthrones);
        return new Processor(service, contentType(), null, new String[]{param + "=" + key, convertArray(optionalKeys)});
    }
    public Processor userProfileSuperGet (String tid, String token, String cardDetailsReqd, String
            addressReqd, String...optionalKeys){
        HashMap<String, String> hMap = new HashMap<String, String>() {{
            put("tid", tid);
            put("token", token);
            contentType();
        }};
        GameOfThronesService service = new GameOfThronesService("sand", "getuserprofilesuper", gameofthrones);
        return new Processor(service, hMap, null, new String[]{convertArray(optionalKeys), cardDetailsReqd, addressReqd});
    }

    public Processor userInternalSuper (String user, String value, String...optionalKeys){
        GameOfThronesService service = new GameOfThronesService("sand", "userinternalsuper", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{user, value, convertArray(optionalKeys)});
    }

    public Processor userInternalOptional (String user, String...optionalKeys){
        GameOfThronesService service = new GameOfThronesService("sand", "userinternaloptional", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{user, convertArray(optionalKeys)});
    }

    public Processor userOptional (String tid, String token, String user, String...optionalKeys){
        HashMap<String, String> hMap = new HashMap<String, String>() {{
            put("tid", tid);
            put("token", token);
            contentType();
        }};
        GameOfThronesService service = new GameOfThronesService("sanduser", "useroptional", gameofthrones);
        return new Processor(service, hMap, null, new String[]{user, convertArray(optionalKeys)});
    }

    public Processor deleteCacheKey(String user, String keys){
        GameOfThronesService service = new GameOfThronesService("sanduser", "deletecachekey", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{user, keys});
    }

    public Processor addSchema(String tid, String token, String payload){
        GameOfThronesService service = new GameOfThronesService("sand", "addschema", gameofthrones);
        return new Processor(service, getHeader(), new String[]{payload});
    }

    public Processor loginCall (String mobile){
        HashMap<String, String> hMap = new HashMap<String, String>() {{
            put("tid", tid);
            put("token", token);
            contentType();
        }};
        GameOfThronesService service = new GameOfThronesService("sand", "logincall", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{mobile});
    }


    public HashMap<String, String> createUser(String name, String mobile, String email, String password)
    {
        String response = signUpV2(name, mobile, email, password).ResponseValidator.GetBodyAsText();
        String tid = JsonPath.read(response, "$.tid").toString().replace("[", "").replace("]", "").replace("\"", "");
        String otpResponse = verifyOTP(tid, "").ResponseValidator.GetBodyAsText();
        HashMap<String, String> hm = new HashMap<String, String>() {{
            put("tid", JsonString(otpResponse, "$.tid"));
            put("token", JsonString(otpResponse, "$.data.token"));
            put("userId", JsonString(otpResponse, "$.data.customer_id"));
            put("referral_code", JsonString(otpResponse, "$.data.referral_code"));
        }};
        return hm;
    }

    public Processor createUserProcessor(String name, String mobile, String email, String password){
        String response = signUpV2(name, mobile, email, password).ResponseValidator.GetBodyAsText();
        String tid = JsonPath.read(response, "$.tid").toString().replace("[", "").replace("]", "").replace("\"", "");
        Processor processor = verifyOTP(tid, getOTP(mobile));
        return processor;
    }

    private HashMap<String, String> contentType() {
        HashMap<String, String> requestheaders = new HashMap<String, String>() {{
            put("content-type", "application/json");
        }};
        return requestheaders;
    }

    public String JsonString (String response, String jsonPath){
        return JsonPath.read(response, jsonPath).toString().replace("[", "").replace("]", "").replace("\"", "");
    }

    public String JsonString1 (String response, String jsonPath){
        return JsonPath.read(response, jsonPath).toString().replaceAll("\\]|\\[| ", "");
    }

    public String superCache(String userId, String su){

        Object superValue = redisHelper.getValue(SANDConstants.sandRedis, 5, cache + userId + "_" + su);
        if(superValue != null) {
            return redisHelper.getValue(SANDConstants.sandRedis, 5, cache + userId + "_" + su).toString();
        }
        return null;

    }

    public void superCacheDel(String userId){
        redisHelper.deleteKey(SANDConstants.sandRedis, 6, cache + userId + "_" + "SUPER_DETAILS");

    }

    public void superKeyDelFromCache(String userId, String suffix){
        redisHelper.deleteKey(SANDConstants.sandRedis, 5, cache + userId + "_" + suffix);

    }


    public void superCacheSet(String userId, String su, String value){
        redisHelper.setValue(SANDConstants.sandRedis,6,cache + userId + "_" + su, value);

    }

    public int rainMode(String rainModeType, String zoneId){
        return SystemConfigProvider.getTemplate(SANDConstants.delivery).update(rainMode+rainModeType+"'"+" "+rainModeEnd+zoneId+"'"+";");
    }


    public String getemailString () {
        String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) {
            int index = (int) (rnd.nextFloat() * CHARS.length());
            salt.append(CHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr + "@gmail.com";
    }

    public String convertArray (String...optionalKeys){
        return Arrays.toString(optionalKeys).replaceAll("\\]|\\[|\"| ", "");
    }

    /***
     * Map City Id wityh polygonId in DB
     * @param cityId
     * @param polygonId
     */


    public void mapCityPolygonWithCity (String cityId, String polygonId){
        SystemConfigProvider.getTemplate(CmsConstants.cmsDB).execute(cityMapPolygon(cityId, polygonId));
    }

    public String catalogAttributes ( int database, String key, String itemValue){
        return redisHelper.getHashValue(SANDConstants.sandRedis, database, key, itemValue);
    }

    public List<String> keys ( int database, String keys){
        return redisHelper.hKeys(SANDConstants.sandRedis, database, keys);
    }

    public String getMenuId (String restId, String menuType){
        Map<String, Object> data;
        try {
            data = SystemConfigProvider.getTemplate(cmsDB).queryForMap("select * from swiggy.restaurant_menu_map where restaurant_id=? and menu_type=?", restId, menuType);
        } catch (Exception e) {
            return null;
        }
        return data.get("menu_id").toString();
    }

    public String redisHget ( int database, String key, String value){
        return redisHelper.getHashValue(SANDConstants.sandRedis, database, key, value);
    }


    public Processor loginHelper (String mobile, String pasword){
        HashMap<String, String> hMap = new HashMap<>();
        hMap.put("Content-type", "application/json");
        String[] payload = {mobile, pasword};
        GameOfThronesService got = new GameOfThronesService("sand", "loginV22", gameofthrones);
        Processor processor = new Processor(got, hMap, payload);
        return processor;
    }

    public Processor loginHelperTest (String password, String mobile, String message){
        HashMap<String, String> log = new HashMap<>();
        log.put("Content_type", "application/json");
        String[] payload = {password, mobile, message};
        GameOfThronesService fetch = new GameOfThronesService("sand", "loginv2test", gameofthrones);
        Processor processor = new Processor(fetch, log, payload);
        return processor;
    }


    public void kafka (String restId, String epoc){
        try {
            File file = new File("../Data/Payloads/JSON/listing_tag");
            String queue = FileUtils.readFileToString(file);
            queue = queue.replace("${id}", restId).replace("${version_code}", epoc);
            kafkaHelper.sendMessage(SANDConstants.kafkaName, SANDConstants.restaurantTopicName, queue);
        } catch (Exception e) {
            Assert.assertTrue(false, "Exception thrown" + e);
        }
    }

    public String dateTime (Integer amount){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, amount);
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime());
        return date;
    }

    public String extendedTime(int i)
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        LocalDateTime d = LocalDateTime.now();
        String dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss").format(d.plusMinutes(i));
        String DT=modifiedDate+" "+dateFormat;
        return DT;
    }


    public Processor collection(String collectionId){
        GameOfThronesService service = new GameOfThronesService("sand", "collection", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{collectionId});
    }

    public  Map<String, Object> collectionId(){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForMap(SANDConstants.collectionId);
    }

    public void collectionQuery(String identifier){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(SANDConstants.getDynamicCollection(identifier, extendedTime(0), extendedTime(10)));
    }

    public Processor searchV2(String keyword,String lat, String lng, String tid, String token) {
        GameOfThronesService gots = new GameOfThronesService("sand", "searchV2", gameofthrones);
        return new Processor(gots, headerHelper.requestHeader(tid,token), null, new String[]{lat,lng,keyword,"1"});
    }

    public Processor aggregator(String[] payload, String tid, String token) {
        GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
        return new Processor(service,headerHelper.requestHeader(tid,token), payload);
    }

    public String Aggregator (String[]payload){
        List<String> openRestList = new ArrayList<>();
        HashMap<String, String> hMap = new HashMap<>();
        hMap.put("Content_Type", "application/json");
        try {
            GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
            Processor processor = new Processor(service, hMap, payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            List<String> restIds = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].id").split(","));
            List<String> open = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].availability.opened").split(","));
            List<String> restaurantServicibility = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants.[*].sla.serviceability").split(","));
            for (int i = 0; i < restIds.size(); i++) {
                if (open.get(i).equalsIgnoreCase("true") && restaurantServicibility.get(i).equalsIgnoreCase("SERVICEABLE")) {
                    openRestList.add(restIds.get(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (AssertionError e) {
            e.printStackTrace();
        }

        return openRestList.get(0);
    }

    public String getrestaurantMenu (String restId){
        List<String> itemIds = new ArrayList<>();
        HashMap<String, String> hMap = new HashMap<>();
        hMap.put("Content_Type", "application/json");
        try {
            GameOfThronesService service = new GameOfThronesService("sand", "menuv3RestId", gameofthrones);
            Processor processor = new Processor(service, hMap, null, new String[]{restId});
            String rest = processor.ResponseValidator.GetBodyAsText();
            List<String> menuLists = Arrays.asList(headerHelper.JsonString(rest, "$.data.menu.items[*].id").split(","));
            List<String> stockList = Arrays.asList(headerHelper.JsonString(rest, "$.data.menu.items[*].inStock").split(","));
            List<String> enabledList = Arrays.asList(headerHelper.JsonString(rest, "$.data.menu.items[*].enabled").split(","));
            for (int i = 0; i < menuLists.size(); i++) {
                if (stockList.get(i).equals("1") && enabledList.get(i).equals("1")) {
                    itemIds.add(menuLists.get(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (AssertionError e) {
            e.printStackTrace();
        }
        int n = random.nextInt(itemIds.size());
        System.out.println("item" + itemIds.get(n));
        return itemIds.get(n);
    }

    public List<String> OpenRestaurantList(String[] payload) {
        List<String> openRestList = new ArrayList<>();
        HashMap<String, String> hMap=new HashMap<>();
        hMap.put("Content_Type","application/json" );
        try {
            GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
            Processor processor = new Processor(service,hMap , payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            List<String> restIds = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].id").split(","));
            List<String> open = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].availability.opened").split(","));
            List<String> restaurantServicibility = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants.[*].sla.serviceability").split(","));
            for (int i = 0; i < restIds.size(); i++) {
                if (open.get(i).equalsIgnoreCase("true") && restaurantServicibility.get(i).equalsIgnoreCase("SERVICEABLE")) {
                    openRestList.add(restIds.get(i));
                }
            }
        }
        catch(Exception e)
        {e.printStackTrace();
        }
        catch(AssertionError e)
        { e.printStackTrace(); }
        return openRestList;
    }

    public Processor menuReorder(String restId, String lat, String lng, String custId) {
        HashMap<String, String> hMap=new HashMap<>();
        hMap.put("Content_Type","application/json" );
        hMap.put("customerId","314");

        GameOfThronesService service = new GameOfThronesService("sand", "menuV4", gameofthrones);
        Processor processor = new Processor(service, hMap, null, new String[]{restId,lat,lng, custId});
        return processor;
    }

    //Search a dish or restname
    public Processor SearchV2(String lat, String lng,String keyword) {
        GameOfThronesService gots = new GameOfThronesService("sand", "searchV2", gameofthrones);
        return new Processor(gots, headerHelper.requestHeader(tid,token), null, new String[]{lat,lng,keyword});
    }

    //When search is having Page type with dishItem
    public Processor SearchV2(String lat, String lng,String keyword, String page, String item) {
        GameOfThronesService gots = new GameOfThronesService("sand", "searchV2", gameofthrones);
        return new Processor(gots, headerHelper.requestHeader(tid,token), null, new String[]{lat,lng,keyword+page+item});
    }

    public void deleteCheckoutPricingCache(String restID){
        redisHelper.deleteKey("checkoutredis",0,"REST_PRICING_"+restID);
    }

    public Processor ColV2(String s) {
        GameOfThronesService gots = new GameOfThronesService("sand", "colv2", gameofthrones);
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        return new Processor(gots,requestHeader, new String[]{s});
    }

    public Processor solrRead( String restId, String content_type) {
        String[] queryParams = {restId, content_type};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("solrsandread", "solrread", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParams);
        return processor;
    }

    public Processor solrUpdate( String restId) {
        String[] queryParams = {restId};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sandsolr", "solrindex", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParams);
        return processor;
    }

    public String analytics(String connection, int db , String key){
        return redisHelper.getValue(connection, db, key).toString();
    }

    public List<Map<String, Object>> cuisine(String rest) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
        List<Map<String, Object>> list= sqlTemplate.queryForList(cuisine+rest+"'");
        return list;
    }

    public List<Map<String, Object>> cuisineList() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
        List<Map<String, Object>> list= sqlTemplate.queryForList(cuisineList);
        return list;
    }

    public HashMap<String, List<String>> crossSellingGlobalRules() {
        HashMap<String, List<String>> map = new HashMap<>();
        List<String> temp = new ArrayList<>();
        temp.add("main course - complement/main dish");
        temp.add("main course - standalone");
        temp.add("main course - complement/main dish/wet");
        temp.add("main course - complement/staple");
        temp.add("meals");
        temp.add("thali");
        temp.add("combos");
        map.put("main course",temp);
        temp = new ArrayList<>();
        temp.add("beverage - branded cola/cold");
        temp.add("beverage - unbranded milkshake");
        temp.add("beverage - unbranded juice/cold");
        temp.add("beverage - unbranded others");
        map.put("beverage",temp);
        temp = new ArrayList<>();
        temp.add("starters - salads");
        temp.add("starters");
        map.put("starters",temp);
        temp = new ArrayList<>();
        temp.add("desserts - sweets");
        temp.add("desserts - pastries");
        temp.add("desserts - ice cream");
        temp.add("desserts - others");
        map.put("desserts",temp);
        temp = new ArrayList<>();
        temp.add("snacks- others");
        temp.add("snacks - breakfast");
        temp.add("snacks - evening");
        map.put("snacks",temp);
        map.put("salads",new ArrayList<String>(Arrays.asList("salads")));
        map.put("soup",new ArrayList<String>(Arrays.asList("soup")));
        map.put("sides",new ArrayList<String>(Arrays.asList("sides")));
        return map;
    }

    public List<List<Map<String,Object>>> crossSelling(String category_name, String restId,List<String> categories) {
        List<Map<String,Object>> itemDetails = cmsHelper.getItemDetails(restId);
        List<Map<String,Object>> first_map = getItemDetails(restId, category_name);
        List<List<Map<String,Object>>> map = new ArrayList<>();
        for (int j= 0; j<categories.size();j++){
            map.add(getItemDetails(restId, categories.get(j)));
        }
        for(int i=0;i<map.size();i++) {
            System.out.println(crossSell(crossSellingGlobalRules().get(category_name.toLowerCase()).get(0).toString(),map.get(i)));

        }
        Gson gson = new Gson();
        String json = gson.toJson(map);
        return map;
    }

    public List<Map<String, Object>> getItemDetails(String rest_id, String category_name) {
        List<Map<String,Object>> itemDetails = cmsHelper.getItemDetails(rest_id);
        List<Map<String,Object>> map = new ArrayList<>();
        for(int i =0;i< itemDetails.size();i++) {
            if (itemDetails.get(i).get("category_name").toString().equalsIgnoreCase(category_name)) {
                map.add(itemDetails.get(i));
            }
        }
        return map;
    }

    public Map<String, List<ItemSet>> crossSell(String smartcategory, List<Map<String, Object>> items){
        List<String> category= crossSellingGlobalRules().get(smartcategory);
        ItemSet itemSet = new ItemSet();
        itemSet.setScore(8);
        List<ItemSet> list = new ArrayList<>();
        for(int i=0;i<=2;i++){
            itemSet = new ItemSet();
            itemSet.setScore(8);
            itemSet.setItem_id(items.get(i).get("item_id").toString());
            list.add(itemSet);
        }
        Map<String, List<ItemSet>> map= new HashMap<>();
        map.put(smartcategory, list);
        return map;
    }
    
    public Processor consumerSignUp(String name, String mobile, String email, String password) {
		GameOfThronesService service = new GameOfThronesService("sand", "signupv2", gameofthrones);
		return new Processor(service, contentType(), new String[]{name, mobile, email, password});
	}

    public Processor referredTo(String referralCode) {
        GameOfThronesService service = new GameOfThronesService("sand", "referredto", gameofthrones);
        return new Processor(service, getHeader(), null,new String[]{referralCode});
    }

    public Processor referredBy(String referralCode) {
        GameOfThronesService service = new GameOfThronesService("sand", "referredby", gameofthrones);
        return new Processor(service, getHeader(), null,new String[]{referralCode});
    }

    public Processor isFirstOrder(String referralCode) {
        GameOfThronesService service = new GameOfThronesService("sand", "isfirstorder", gameofthrones);
        return new Processor(service, getHeader(), null,new String[]{referralCode});
    }


    public String getOTPWithPolling(String mobile, int pollinSeconds) {
		boolean flag = true;
		String otp = null;
		while(flag) {
			otp =  redisHelper.getValue("sandredisstage", 0, mobile + SANDConstants.otp).toString();
			if((!otp.isEmpty()) && (null!= otp)) {
				flag = false;
			} else {
				System.out.println("OTP not updated in DB, retrying now");
				if(pollinSeconds>0) {
					pollinSeconds--;
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				} else {
					flag=false;
				}
			}
		}
		return otp;
	}
    
    public HashMap<String, String> createOrderWithSignUp(String name, String mobile, String email, String password) {
		String response = consumerSignUp(name, mobile, email, password).ResponseValidator.GetBodyAsText();
		String tid = JsonPath.read(response, "$.tid").toString().replace("[", "").replace("]", "").replace("\"", "");
		String otpResponse = verifyOTP(tid, getOTPWithPolling(mobile,5)).ResponseValidator.GetBodyAsText();
		HashMap<String, String> hm = new HashMap<String, String>() {{
			put("tid", JsonString(otpResponse, "$.tid"));
			put("token", JsonString(otpResponse, "$.data.token"));
			put("userId", JsonString(otpResponse, "$.data.customer_id"));
		}};
		return hm;
	}
    
    public List<String> aggregatorRestList(String[] payload) {
		List<String> openRestList = new ArrayList<>();
		HashMap<String, String> hMap=new HashMap<>();
		hMap.put("Content_Type","application/json" );
		try {
			GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
			Processor processor = new Processor(service,hMap , payload);
			String response = processor.ResponseValidator.GetBodyAsText();
			List<String> restIds = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].id").split(","));
			List<String> open = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].availability.opened").split(","));
			List<String> restaurantServicibility = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants.[*].sla.serviceability").split(","));
			for (int i = 0; i < restIds.size(); i++) {
				if (open.get(i).equalsIgnoreCase("true") && restaurantServicibility.get(i).equalsIgnoreCase("SERVICEABLE")) {
					openRestList.add(restIds.get(i));
				}
			}
		}
		catch(Exception e)
		{e.printStackTrace();
		}
		catch(AssertionError e)
		{ e.printStackTrace(); }

		return openRestList;
	}

    public String randomAlpha() {
        int count=6;
        String alphabet= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*alphabet.length());
            builder.append(alphabet.charAt(character));
        }return builder.toString();
    }

    public Map<String, Object> getSlug(String restId){
        return SystemConfigProvider.getTemplate(CmsConstants.cmsDB).queryForMap(SANDConstants.slug +restId+"'");
    }

    public String day(String pattern){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String day = new SimpleDateFormat(pattern, Locale.ENGLISH).format(date.getTime());
        return  day;
    }

    public String dateTime(int i)
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        LocalDateTime d = LocalDateTime.now();
        String dateFormat = DateTimeFormatter.ofPattern("HH:mm").format(d.plusMinutes(i));
        String DT=modifiedDate+" "+dateFormat;
        return DT;
    }

    public Map<String,Object> query(String userId){
        Map<String, Object> map=SystemConfigProvider.getTemplate(CmsConstants.cmsDB).queryForMap(SANDConstants.referralCode+userId);
        return map;
    }

    public void restAndItemAvailability() throws Exception {
        System.out.println("Starting");
        SANDHelper helper=new SANDHelper();
        RedisHelper redisHelper=new RedisHelper();
        String response=helper.aggregator(new String[]{"12.9325585", "77.603578"}).ResponseValidator.GetBodyAsText();
        List<String> restIds = Arrays.asList(helper.JsonString(response, "$.data.restaurants[*].id").split(","));
        System.out.println(restIds);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
        for(String restid: restIds)
        {
            //AV Menu
            String menu_query="select menu_id from restaurant_menu_map where restaurant_id='"+restid+"';";
            System.out.println(menu_query);
            List<Map<String, Object>> lists = sqlTemplate.queryForList(menu_query);
            for (int i = 0; i < lists.size(); i++) {
                String menu_id=lists.get(i).get("menu_id").toString();
                System.out.println("Menu Id===="+menu_id);
                redisHelper.setValueJson("sandredisstage",0,"AV_MENU_"+menu_id,"{\"status\":true,\"nextChangeTime\":1636364740000}");
            }
            //AV Items
            String item_query="select item_id from item_menu_map where menu_id in(select id from menu_taxonomy where parent in(select id from menu_taxonomy where parent in(select menu_id from restaurant_menu_map where restaurant_id ="+restid+") and active=1) and active=1) and active=1;";
            List<Map<String, Object>> itemLists = sqlTemplate.queryForList(item_query);
            for (int i = 0; i < itemLists.size(); i++) {
                String item_id=itemLists.get(i).get("item_id").toString();
                System.out.println("Item Id===="+item_id);
                redisHelper.setValueJson("sandredisstage",0,"AV_ITEM_"+item_id,"{\"status\":true,\"nextChangeTime\":1636364740000}");
            }
        }

    }


    public void TMenu(String restId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
        String menu_query = "select menu_id from restaurant_menu_map where restaurant_id='" + restId + "';";
        System.out.println(menu_query);
        List<Map<String, Object>> lists = sqlTemplate.queryForList(menu_query);
        for (int i = 0; i < lists.size(); i++) {
            String menu_id = lists.get(i).get("menu_id").toString();
            System.out.println("Menu Id====" + menu_id);
            redisHelper.setValueJson("sandredisstage", 0, "AV_MENU_" + menu_id, "{\"status\":true,\"nextChangeTime\":1636364740000}");

        }
        //AV Items
        String item_query = "select item_id from item_menu_map where menu_id in(select id from menu_taxonomy where parent in(select id from menu_taxonomy where parent in(select menu_id from restaurant_menu_map where restaurant_id =" + restId + ") and active=1) and active=1) and active=1;";
        List<Map<String, Object>> itemLists = sqlTemplate.queryForList(item_query);
        for (int i = 0; i < 1; i++) {
            String item_id = itemLists.get(i).get("item_id").toString();
            System.out.println("Item Id====" + item_id);
            redisHelper.setValueJson("sandredisstage", 0, "AV_ITEM_" + item_id, "{\"status\":true,\"nextChangeTime\":1636364740000}");
        }

    }

    public String restName(String restId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
        return sqlTemplate.queryForMap(restName+restId+"'").get("name").toString();
    }

    public Processor dDCollection(String c){
        GameOfThronesService service = new GameOfThronesService("sandcoll", "ddcollection", gameofthrones);
        return new Processor(service, contentType(), new String[]{c});
    }



   /* @Test
    public void t() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        EventBuilder eventBuilder= new EventBuilder();
        GstDetails gstDetails= new GstDetails();
        ItemSuggestion itemSuggestion= new ItemSuggestion();
        List<ItemSuggestion> itemSuggestionList= new ArrayList<>();
        itemSuggestion.setItemId(22221);
        itemSuggestion.setScore(22.22);
        itemSuggestionList.add(itemSuggestion);
        gstDetails.setCgst("Cgst");
        gstDetails.setIgst("Igst");
        gstDetails.setSgst("Sgst");
        gstDetails.setInclusive(false);
        Data data = new Data();
        data.setServiceTax("serviceTAx");
        data.setVat("VAt");
        data.setTimestamp(22222222);
        data.setName("edfdfdfd");
        data.setIsPerishable(22222);
        data.setIsDiscoverable(true);
        data.setIsSpicy(0);
        data.setIsVeg(0);
        data.setAddonFreeLimit(122222);
        data.setAddonLimit(3333333);
        data.setCategory("category");
        data.setCategoryDesc("caetgoryDesc");
        data.setCategoryId(111);
        data.setCategoryOrder(2222);
        data.setCropChoices(1111);
        data.setDescription("description");
        data.setRecommended(true);
        data.setRestaurantId(222222222);
        data.setEligibleForLongDistance(22112);
        data.setEnabled(0);
        data.setGstDetails(gstDetails);
        data.setId(111);
        data.setInStock(0);
        data.setMenuId(11);
        data.setItemSuggestion(itemSuggestionList);
        VariantsV2 v2= new VariantsV2();
        Variation variation= new Variation();
        variation.setDefault(1111);
        variation.setExternalVariantId("sdsdsd");
        List<Variation> variations= new ArrayList<>();
        variations.add(variation);
        VariantGroup variantGrop= new VariantGroup();
        PricingModel pricingModel= new PricingModel();
        List<Object> addonCombi= new ArrayList<>();
        pricingModel.setPrice(22.22);
        pricingModel.setAddonCombinations(addonCombi);
        Variation_ variation_= new Variation_();
        variation_.setGroupId("2gorupid");
        variation_.setThirdPartyGroupId("3rdPartyGroupId");
        variation_.setThirdPartyVariationId("3rdPartyVAtiationId");
        variation_.setVariationId("ssssss");
        List<Variation_> variation_s= new ArrayList<>();
        variation_s.add(variation_);
        pricingModel.setVariations(variation_s);

        variantGrop.setName("name");
        variantGrop.setExternalGroupId("ssss");
        variantGrop.setOrder(1111);
        variantGrop.setGroupId("ssddssd");
        variantGrop.setOrder(111);
        variantGrop.setVariations(variations);
        List<VariantGroup> variantGroup= new ArrayList<>();
        variantGroup.add(variantGrop);
        List<PricingModel> pricingModelList= new ArrayList<>();
        pricingModelList.add(pricingModel);

        v2.setVariantGroups(variantGroup);
        v2.setPricingModels(pricingModelList);
        data.setVariantsV2(v2);
        Event r=eventBuilder.data(data).eventType("sddsds").id(2323).meta(null).timeStamp(23424242).opType("eerrere").version("fddfdfdf").build();
        jsonHelper.getObjectToJSON(r);
        System.out.println("ueueue"+jsonHelper.getObjectToJSON(r));
    }*/

    public Processor aggregatorMocked(String[] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sandmock", "aggregator", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }



    public void setRestaurantMetaInfo(String restaurantId, String value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(SANDConstants.setMetaInfo+value+SANDConstants.WhereId+restaurantId);
    }


    public Processor aggregator(String[] payload,String... userAgent) {


        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("User-Agent",userAgent[0]);
        if(userAgent.length>1){
            requestHeader.put("version-code","400");
        }
        requestHeader.put("Authorization","Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GameOfThronesService gots = new GameOfThronesService("sand", "aggregator", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor aggregator(String[] payload,String userAgent, String tid, String token) {
        HashMap<String, String> requestHeader = headerHelper.requestHeader(tid,token);
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("User-Agent",userAgent);
        requestHeader.put("version-code","500");
        requestHeader.put("tid",tid);
//        requestHeader.put("token",token);
        GameOfThronesService gots = new GameOfThronesService("sand", "aggregator", gameofthrones);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor menuV4RestIdWithChannel(String restID, String tid, String token, String versionCode, String userAgent) {
        HashMap<String, String> hmap = new HashMap();
        if(null!=tid) {
            hmap.put("tid", tid);
        }
        if(null!=token) {
            hmap.put("token", token);
        }
        if(null!=versionCode) {
            hmap.put("version-code", versionCode);
        }
        if(null!=userAgent) {
                hmap.put("User-Agent", userAgent);
        }
        GameOfThronesService service = new GameOfThronesService("sand", "menuV4", gameofthrones);
        return new Processor(service, hmap, null, new String[]{restID, lat, lng});
    }


    public Map<String, Object> sndDb(){
        System.out.println(SystemConfigProvider.getTemplate("snddb").queryForMap(SANDConstants.coll));

        return SystemConfigProvider.getTemplate("snddb").queryForMap(SANDConstants.coll);
    }

    public int sndCol(String tag) throws InterruptedException{
        Thread.sleep(4000);
        System.out.println(SystemConfigProvider.getTemplate("snddb").update(SANDConstants.COC(tag)));
        return SystemConfigProvider.getTemplate("snddb").update(SANDConstants.COC(tag));
    }

    public String id(){
        return SystemConfigProvider.getTemplate("snddb").queryForMap(SANDConstants.q).get("id").toString();
    }


    public void sndDD(){
        String id = SystemConfigProvider.getTemplate("snddb").queryForMap(SANDConstants.q).get("id").toString();
        SANDConstants.cc(id);
    }


    public void sndDW(){
        String id = SystemConfigProvider.getTemplate("snddb").queryForMap(SANDConstants.q).get("id").toString();
        SANDConstants.cw(id);
    }


    public void sndD(){
        System.out.println(SANDConstants.co);
        SystemConfigProvider.getTemplate("snddb").update(SANDConstants.co);
        String id = SystemConfigProvider.getTemplate("snddb").queryForMap(SANDConstants.q).get("id").toString();
        SANDConstants.cc(id);
    }




    public DD DCol() throws IOException, IOException {
        EnabledFilters ef= new EnabledFilters();
        Filters filters = new Filters();
        RestaurantDiscountType restaurantDiscountType= new RestaurantDiscountType();
        RestaurantDiscountOpType restaurantDiscountOpType= new RestaurantDiscountOpType();
        restaurantDiscountType.setType("STRING_EQUALS_FILTER");
        restaurantDiscountType.setValue("freebie");


        restaurantDiscountType.setId("restaurantDiscountType");
        restaurantDiscountType.setQueryId("restaurantDiscountType0");

        List<RestaurantDiscountType> restaurantDiscountTypes= new ArrayList<>();
        restaurantDiscountTypes.add(restaurantDiscountType);

        restaurantDiscountOpType.setType("STRING_EQUALS_FILTER");
        restaurantDiscountOpType.setValue("super");

        restaurantDiscountOpType.setId("restaurantDiscountOpType");
        restaurantDiscountOpType.setQueryId("restaurantDiscountOpType0");

        List<RestaurantDiscountOpType> restaurantDiscountOpTypes= new ArrayList<>();
        restaurantDiscountOpTypes.add(restaurantDiscountOpType);
        filters.setRestaurantDiscountOpType(restaurantDiscountOpTypes);
        filters.setRestaurantDiscountType(restaurantDiscountTypes);


        RestaurantmultiTD restaurantmultiTD= new RestaurantmultiTD();
        restaurantmultiTD.setType("MATCH_EXPRESSION_IN_ANY_MAP_FIELD_FILTER");
        restaurantmultiTD.setQuery("restaurantDiscountType0 AND restaurantDiscountOpType0");

        restaurantmultiTD.setId("restaurantmultiTD");
        restaurantmultiTD.setFilters(filters);
        restaurantmultiTD.setQueryId("restaurantmultiTD0");

        List<RestaurantmultiTD> restaurantmultiTDS= new ArrayList<>();
        restaurantmultiTDS.add(restaurantmultiTD);

        ef.setRestaurantmultiTD(restaurantmultiTDS);

        return new DDBuilder().enabledFilters(ef).build();

    }


    public void setLayoutInRedis(String layoutType, String restId){

        String value="";
        if (layoutType == "high_touch") {
            value = "{\"restaurantId\":" + restId +",\"layoutType\":\"high_touch\",\"updatedOn\":1536920222469,\"updatedBy\":\"srishty\"}";
        }
        else if (layoutType== "low_touch" ){
            value = "{\"restaurantId\":" + restId +",\"layoutType\":\"low_touch\",\"updatedOn\":1536920222469,\"updatedBy\":\"srishty\"}";
        }
        else if (layoutType== "normal" ){
            value = "{\"restaurantId\":" + restId +",\"layoutType\":\"normal\",\"updatedOn\":1536920222469,\"updatedBy\":\"srishty\"}";
        }
        redisHelper.setHashValue("sandredisstage", 0, "RESTAURANT_MERCHANDISE_LAYOUT", "RESTAURANT_"+restId, value);
    }

    public void setSectionsForHighTouch(){

        String sectionConfig0 = "" ;
        String sectionConfig1 = "" ;
        String layoutType= "high_touch";

        if (layoutType == "high_touch") {
            sectionConfig0 = "{\"id\":0,\"name\":\"menuCarousels\",\"title\":\"Top Items\",\"min\":2,\"max\":6,\"priority\":1,\"enabled\":true}";
            sectionConfig1 = "{\"id\":1,\"name\":\"specials\",\"title\":\"Specials\",\"min\":2,\"max\":6,\"priority\":1,\"enabled\":true}";
        }

        redisHelper.setHashValue("sandredisstage", 0,"MERCHANDISE_SECTION_CONFIG","SECTION_0",sectionConfig0);
        redisHelper.setHashValue("sandredisstage", 0,"MERCHANDISE_SECTION_CONFIG","SECTION_1",sectionConfig1);

        String sectionLayoutMapping0 = "";
        String sectionLayoutMapping1 = "";


        if (layoutType == "high_touch"){
            sectionLayoutMapping0 = "{\"id\":0,\"name\":\"high_touch\",\"sectionId\":0}";
            sectionLayoutMapping1 = "{\"id\":1,\"name\":\"high_touch\",\"sectionId\":1}";
        }

        redisHelper.setHashValue("sandredisstage", 0, "MERCHANDISE_LAYOUT_CONFIG_KEY_"+ layoutType, "LAYOUT_MAPPING_ID_0", sectionLayoutMapping0);
        redisHelper.setHashValue("sandredisstage", 0, "MERCHANDISE_LAYOUT_CONFIG_KEY_"+ layoutType, "LAYOUT_MAPPING_ID_1", sectionLayoutMapping1);
    }

    public List<HashMap<String,Integer>> aggregatorRestAreaCityList(String[] payload) {
        List<HashMap<String,Integer>> openRestList = new ArrayList<>();
        HashMap<String,Integer> restInfo;
        HashMap<String, String> hMap=new HashMap<>();
        hMap.put("Content_Type","application/json" );

        try {
            GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
            Processor processor = new Processor(service,hMap , payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            List<String> restIds = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].id").split(","));
            List<String> open = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].availability.opened").split(","));
            List<String> restaurantServicibility = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants.[*].sla.serviceability").split(","));
            List<Integer> areaId = JsonPath.read(response, "$.data.restaurants[*].areaId");
            List<Integer> cityId = JsonPath.read(response, "$.data.restaurants[*].cityId");
            List<String>  restName = JsonPath.read(response, "$.data.restaurants[*].name");
            for (int i = 0; i < restIds.size(); i++) {
                if (open.get(i).equalsIgnoreCase("true") && restaurantServicibility.get(i).equalsIgnoreCase("SERVICEABLE")) {
                    restInfo = new HashMap<>();
                    restInfo.put("restID",Integer.valueOf(restIds.get(i)));
                    restInfo.put("areaID",areaId.get(i));
                    restInfo.put("cityID",cityId.get(i));
//                    restInfo.put("restName",restName.get(i));
                    openRestList.add(restInfo);
                }
            }
        }
        catch(Exception e)
        {e.printStackTrace();
        }
        catch(AssertionError e)
        { e.printStackTrace(); }

        return openRestList;
    }

    public List<HashMap<String,String>> aggregatorRestNameAreaCityList(String[] payload) {
        List<HashMap<String,String>> openRestList = new ArrayList<>();
        HashMap<String,String> restInfo;
        HashMap<String, String> hMap=new HashMap<>();
        hMap.put("Content_Type","application/json" );

        try {
            GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
            Processor processor = new Processor(service,hMap , payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            List<String> restIds = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].id").split(","));
            List<String> open = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].availability.opened").split(","));
            List<String> restaurantServicibility = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants.[*].sla.serviceability").split(","));
            List<Integer> areaId = JsonPath.read(response, "$.data.restaurants[*].areaId");
            List<Integer> cityId = JsonPath.read(response, "$.data.restaurants[*].cityId");
            List<String>  restName = JsonPath.read(response, "$.data.restaurants[*].name");
            for (int i = 0; i < restIds.size(); i++) {
                if (open.get(i).equalsIgnoreCase("true") && restaurantServicibility.get(i).equalsIgnoreCase("SERVICEABLE")) {
                    restInfo = new HashMap<>();
                    restInfo.put("restID",restIds.get(i));
                    restInfo.put("areaID",String.valueOf(areaId.get(i)));
                    restInfo.put("cityID",String.valueOf(cityId.get(i)));
                    restInfo.put("restName",restName.get(i));
                    openRestList.add(restInfo);
                }
            }
        }
        catch(Exception e)
        {e.printStackTrace();
        }
        catch(AssertionError e)
        { e.printStackTrace(); }

        return openRestList;
    }

    public List<String> aggregatorRestListSearch(String[] payload) {
        List<String> RestList = new ArrayList<>();
        HashMap<String, String> hMap=new HashMap<>();
        hMap.put("Content_Type","application/json" );
        try {
            GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
            Processor processor = new Processor(service,hMap , payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            List<String> restIds = Arrays.asList(headerHelper.JsonString(response, "$.data.restaurants[*].id").split(","));
            for (int i = 0; i < restIds.size(); i++) {
                    RestList.add(restIds.get(i));
            }
        }
        catch(Exception e)
        {e.printStackTrace();
        }
        catch(AssertionError e)
        { e.printStackTrace(); }

        return RestList;
    }


    public HashMap<String,Object>  resturantAvailableAndServiceable(String responseAsString)throws Exception {
        List<String> restIds = Arrays.asList(headerHelper.JsonString(responseAsString, "$.data.restaurants[*].id").split(","));

        JSONObject object = new JSONObject(responseAsString);
        JSONObject dataObject = object.getJSONObject("data");
        JSONObject restaurantObject = dataObject.getJSONObject("restaurants");
        HashMap<String, Object> hashMap = new HashMap<>();

        for (String restaurantId : restIds) {
            JSONObject eachRestaurantObject = restaurantObject.getJSONObject(restaurantId);
            JSONObject availabilityObject = eachRestaurantObject.getJSONObject("availability");
            boolean isOpened = availabilityObject.getBoolean("opened");
            hashMap.put("opened", isOpened);
            if (isOpened) {
                eachRestaurantObject = restaurantObject.getJSONObject(restaurantId);
                JSONObject slaObject = eachRestaurantObject.getJSONObject("sla");
                String serviceability = (String) slaObject.get("serviceability");
                hashMap.put("serviceability", serviceability);
                if (serviceability.equalsIgnoreCase("SERVICEABLE")) {
                    hashMap.put("restaurantId", restaurantId);
                    break;
                }


            }

        }
        return hashMap;
    }

        public Processor menuV4UuidSlug(String restID, String lat, String lng, String uuid, String slug){
            GameOfThronesService service = new GameOfThronesService("sand", "menuV4", gameofthrones);
            return new Processor(service, contentType(), null, new String[]{restID, lat, lng, uuid, slug});

        }

        public Processor archiveUser (String[]payload, String disable_reason, String updated_by){
            String[] queryParam = {disable_reason, updated_by};
            HashMap<String, String> requestHeader = new HashMap<String, String>();
            requestHeader.put("Content-Type", "application/json");
            requestHeader.put("Authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
            GameOfThronesService gots = new GameOfThronesService("sanduser", "userarchival", gameofthrones);
            Processor processor = new Processor(gots, requestHeader, payload, queryParam);
            return processor;

        }

        public Processor menuV4RestIdWithVersionAndUserAgent (String restID, String lat, String lng, String
        version, String userAgent){
            HashMap<String, String> requestHeader = new HashMap<String, String>();
            requestHeader.put("User-Agent", userAgent);
            requestHeader.put("version-code", version);
            requestHeader.put("swuid", "swap");
            GameOfThronesService service = new GameOfThronesService("sand", "menuV4", gameofthrones);
            return new Processor(service, requestHeader, null, new String[]{restID, lat, lng});
        }

        public Processor searchV2WithVersionAndUserAgent (String lat, String lng, String searchKeyword, String
        version, String userAgent){
            String[] queryParams = {lat, lng, searchKeyword};
            HashMap<String, String> requestHeader = new HashMap<String, String>();
            requestHeader.put("Content-Type", "application/json");
            requestHeader.put("User-Agent", userAgent);
            requestHeader.put("version-code", version);
            requestHeader.put("swuid", "swap");
            GameOfThronesService gots = new GameOfThronesService("sand", "searchV2", gameofthrones);
            Processor processor = new Processor(gots, requestHeader, null, queryParams);
            return processor;
        }


        public Long getValueFromDb (String query, String keyName){
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cmsDB);
            Map<String, Object> queryResult = sqlTemplate.queryForList(query).get(0);
            return (Long) queryResult.get(keyName);
        }

        public long getEpochTime ( int days){
            return days * 24 * 60 * 60 * 1000;
        }

        public void addingMenuToRedis ( long menu_id, int days){

            long getEpochTimeForNextMentionedDate = System.currentTimeMillis() + getEpochTime(days);

            RedisHelper redisHelper = new RedisHelper();
            redisHelper.setValueJson("sandredisstage", 0, "AV_MENU_" + menu_id, "{\"status\":true,\"nextChangeTime\":" + getEpochTimeForNextMentionedDate + "}");
        }

        public List<Map<String, Object>> putAllItemsInRedis ( int restId, int days){
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");

            long getEpochTimeForNextMentionedDate = System.currentTimeMillis() + getEpochTime(days);

            String item_query = "select item_id from item_menu_map where menu_id in(select id from menu_taxonomy where parent in(select id from menu_taxonomy where parent in(select menu_id from restaurant_menu_map where restaurant_id =" + restId + ") and active=1) and active=1) and active=1;";
            List<Map<String, Object>> itemLists = sqlTemplate.queryForList(item_query);
            for (int i = 0; i < itemLists.size(); i++) {
                String item_id = itemLists.get(i).get("item_id").toString();
                System.out.println("Item Id====" + item_id);
                redisHelper.setValueJson("sandredisstage", 0, "AV_ITEM_" + item_id, "{\"status\":true,\"nextChangeTime\":" + getEpochTimeForNextMentionedDate + "}");
            }
            return itemLists;
        }

        public Processor listingWithGandalf (String lat, String lng, String pageType){
            Processor processor;
            HashMap headers = new HashMap<String, String>();
            headers.put("version-code", RngConstants.VersionCode229);
            GameOfThronesService service = new GameOfThronesService("dweblisting", "listingpage", gameofthrones);
            String[] urlparams = new String[]{lat, lng, pageType};
            processor = new Processor(service, headers, null, urlparams);
            return processor;

        }

        public Processor listingWithCheckout (String lat, String lng){

            HashMap headers = new HashMap<String, String>();
            headers.put("version-code", RngConstants.VersionCode229);
            GameOfThronesService service = new GameOfThronesService("checkout", "listingpage", gameofthrones);
            String[] urlparams = new String[]{lat, lng};
            Processor processor = new Processor(service, headers, null, urlparams);
            return processor;

        }

        public Processor aggregatorWithCheckout (String[]payload)
        {

            HashMap<String, String> requestHeader = new HashMap<>();
            requestHeader.put("Content-Type", "application/json");
            GameOfThronesService gots = new GameOfThronesService("checkout", "aggregator", gameofthrones);
            Processor processor = new Processor(gots, requestHeader, payload, null);
            return processor;
        }


}

