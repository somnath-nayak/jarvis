package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.rng.pojo.Category;
import com.swiggy.api.sf.rng.pojo.CreateFlatTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateFlatTdEntry;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;
import com.swiggy.api.sf.rng.pojo.Menu;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.Slot;
import com.swiggy.api.sf.rng.pojo.SubCategory;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class CreateFlatTDWithRestrictions {
	
	getObject go = new getObject();
	static 	int i =0;
	
	public String getFlatTD(String restrictionType,String level) throws IOException
	{
		i++;
	    List<RestaurantList> restaurantList = new ArrayList<>();
	    JsonHelper jsonHelper = new JsonHelper();
	    CreateFlatTdEntry flatTradeDiscount =null ;
	    switch(level)
		{
		
		case "Restaurant"  : restaurantList.add(go.getRestaurantObject());break;
		case "Category"    : restaurantList.add(go.getCategoryObject());break;
		case "Subcategory" : restaurantList.add(go.getsubCategoryObject());break;
		case "Item"        : restaurantList.add(go.getItemObject());break;
		}
		
		
	
	{
		switch(restrictionType)
		{
		
		    case "SFO": flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat",level, "10","0" ).userRestriction(false).restaurantFirstOrder(false)
					.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;

		    case "RFO": flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat",level, "10","0" ).userRestriction(false).restaurantFirstOrder(true)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;


		    case "NR":  flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat",level, "10","0" ).userRestriction(false).restaurantFirstOrder(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;


		    case "UR":  flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat", level, "10","0" ).userRestriction(true).restaurantFirstOrder(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;

		    case "DU": flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat",level, "10","0" ).userRestriction(false).restaurantFirstOrder(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").build();break;

		    case "TR": List<Slot> slots = new ArrayList<>();
	                   slots.add(new Slot("2350", "ALL", "0005"));
                      flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					.slots(slots)
					.ruleDiscount("Flat",level, "10","0" ).userRestriction(false).restaurantFirstOrder(false)
					.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;
		
		}     
		      return   jsonHelper.getObjectToJSON(flatTradeDiscount);
	}
	}

	public String getFlatTD(String restrictionType,String level,List<RestaurantList> restaurantList) throws IOException
	{
		i++;
	   
	    JsonHelper jsonHelper = new JsonHelper();
	    CreateFlatTdEntry flatTradeDiscount =null ;
	   
	{
		switch(restrictionType)
		{
		
		    case "SFO": flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat",level, "10","0" ).userRestriction(false).restaurantFirstOrder(false)
					.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;

		    case "RFO": flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat",level, "10","0" ).userRestriction(false).restaurantFirstOrder(true)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;


		    case "NR":  flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat",level, "10","0" ).userRestriction(false).restaurantFirstOrder(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;


		    case "UR":  flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat", level, "10","0" ).userRestriction(true).restaurantFirstOrder(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;

		    case "DU": flatTradeDiscount = new CreateFlatTdBuilder()
					.nameSpace("FlatTestTD")
					.header("FlatTestTD")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
					// .slots(slots)
					.ruleDiscount("Flat",level, "10","0" ).userRestriction(false).restaurantFirstOrder(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").build();break;

		}     
		      return   jsonHelper.getObjectToJSON(flatTradeDiscount);
	}
	}

	
	
}
