package com.swiggy.api.sf.rng.pojo.SwiggySuper;

import io.advantageous.boon.json.annotations.JsonProperty;

public class Cancelsubscription {
	
	@JsonProperty ("")
	private String subscription_id;
	@JsonProperty ("")
	private String order_id;
	@JsonProperty ("")
	private String user_id;
	@JsonProperty ("false")
	private String enabled;
	@JsonProperty ("By-Super-Automation-Script")
	private String created_by;
	
	public Cancelsubscription() {
		
	}
	public Cancelsubscription(String subscription_id) {
		this.subscription_id= subscription_id;
	}
	public Cancelsubscription(String order_id, String user_id) {
		this.order_id= order_id;
		this.user_id= user_id;
	}
	public Cancelsubscription(String subscription_id,String order_id, String user_id) {
		this.subscription_id= subscription_id;
		this.order_id= order_id;
		this.user_id= user_id;
	}
	
	public String getSubscriptionId() {
		return this.subscription_id;
	}
	public void setSusbscriptionId(String subscription_id) {
		this.subscription_id = subscription_id;
	}
	public String getOderId() {
		return this.order_id;
	}
	public void setOrderId(String order_id) {
		this.order_id = order_id;
	}
	public String getUserId() {
		return this.user_id;
	}
	public void setUserId(String user_id) {
		this.user_id = user_id;
	}
	public String getEnabled() {
		return this.enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getCreatedBy() {
		return this.created_by;
	}
	public void setCreatedBy(String created_by) {
		this.created_by = created_by;
	}
	
	@Override
	public String toString() {
	
						return "{" 
									+ "\"subscription_id\"" + ":" + subscription_id + ","
									+ "\"order_id\"" + ":" + order_id + ","
									+"\"created_by\"" + ":" + "\"" + created_by + "\"" + ","
									+ "\"user_id\"" + ":" + user_id 
				            +"}"   ;
						}
	public String CancelSubscriptionByUserIdOrderId() {
						return "{" 
									+"\"order_id\"" + ":" + order_id + ","
									+"\"created_by\"" + ":" + "\"" + created_by + "\"" + ","
									+ "\"user_id\"" + ":" + user_id 
				            +"}"   ;
						}

public String cancelSubscriptionBySubscriptionId() {
						
						return "{" 
								+"\"created_by\"" + ":" + "\"" + created_by + "\"" + ","
								+ "\"subscription_id\"" + ":" + subscription_id 
			            +"}"   ;
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

	