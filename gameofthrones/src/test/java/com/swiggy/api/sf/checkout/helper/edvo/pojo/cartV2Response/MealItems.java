package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "mealItems"
})
public class MealItems {

    @JsonProperty("mealItems")
    private List<MealItem> mealItems = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public MealItems() {
    }

    /**
     *
     * @param mealItems
     */
    public MealItems(List<MealItem> mealItems) {
        super();
        this.mealItems = mealItems;
    }

    @JsonProperty("mealItems")
    public List<MealItem> getMealItems() {
        return mealItems;
    }

    @JsonProperty("mealItems")
    public void setMealItems(List<MealItem> mealItems) {
        this.mealItems = mealItems;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mealItems", mealItems).append("additionalProperties", additionalProperties).toString();
    }

}