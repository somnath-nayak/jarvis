package com.swiggy.api.sf.snd.dp;

import com.swiggy.api.erp.cms.pojo.HolidaySlot;
import com.swiggy.api.erp.cms.pojo.SlotBuilder;
import com.swiggy.api.sf.rng.helper.RandomNumber;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.*;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.pojo.*;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DishDiscoveryDP {
    JsonHelper jsonHelper= new JsonHelper();
    RngHelper rngHelper= new RngHelper();
    RandomNumber rm = new RandomNumber(2000000,3000000);

    @DataProvider(name = "getItemName")
    public Object[][] getItemName() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<ItemName> itemName= new ArrayList<>();
        itemName.add(new ItemName("STRING_EQUALS_FILTER","curry"));
        /*List<OrderCountsBreakfast> lis= new ArrayList<>();
        lis.add(new OrderCountsBreakfast("NUMBER_IN_RANGE_FILTER",2));
*/
        l.add(new SearchRequest("ITEM", new Filters(null,itemName , null, null, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<ItemName> itemName2= new ArrayList<>();
        itemName2.add(new ItemName("STRING_EQUALS_FILTER","biryani"));
        l2.add(new SearchRequest("ITEM", new Filters(null,itemName2 , null, null, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();


        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests),"curry"},{jsonHelper.getObjectToJSON(searchRequests2),"biryani"}
        };
    }

    @DataProvider(name = "getItemComb")
    public Object[][] getItemComb() throws IOException {
        List<SearchRequest> l = new ArrayList<>();
        List<ItemName> itemName = new ArrayList<>();
        itemName.add(new ItemName("STRING_EQUALS_FILTER", "curry"));
        List<ItemPrice> itemPrice= new ArrayList<>();
        itemPrice.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",150.0,250.0));
        List<ItemEnabled> itemEnabled= new ArrayList<>();
        itemEnabled.add(new ItemEnabled("BOOLEAN_IS_FILTER","true"));
        l.add(new SearchRequest("ITEM", new Filters(null,itemName , itemPrice, itemEnabled, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2 = new ArrayList<>();
        List<ItemName> itemName2 = new ArrayList<>();
        itemName2.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));

        List<ItemPrice> itemPrice2= new ArrayList<>();
        itemPrice2.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",100.0,250.0));
        l2.add(new SearchRequest("ITEM", new Filters(null,itemName2 , itemPrice2, null, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();


        List<SearchRequest> l3 = new ArrayList<>();
        List<ItemName> itemName3 = new ArrayList<>();
        itemName3.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice3= new ArrayList<>();
        itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",100.0,250.0));

        List<ItemEnabled> itemENabled= new ArrayList<>();
        itemENabled.add(new ItemEnabled("BOOLEAN_IS_FILTER","true"));

        List<ItemLongDistanceEnabled> itemLongENabled= new ArrayList<>();
        itemLongENabled.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER","true"));

        l3.add(new SearchRequest("ITEM", new Filters(null,itemName3 , itemPrice3, itemENabled, itemLongENabled, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests3= new SearchBuilder().searchrequest(l3)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l4 = new ArrayList<>();
        List<ItemPrice> itemPrice4= new ArrayList<>();
        itemPrice4.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",100.0,250.0));

        List<ItemEnabled> itemENabled4= new ArrayList<>();
        itemENabled4.add(new ItemEnabled("BOOLEAN_IS_FILTER","true"));

        List<ItemLongDistanceEnabled> itemLongENabled4= new ArrayList<>();
        itemLongENabled4.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER","true"));

        l4.add(new SearchRequest("ITEM", new Filters(null,null , itemPrice4, itemENabled4, itemLongENabled4, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests4= new SearchBuilder().searchrequest(l4)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l5 = new ArrayList<>();
        List<ItemEnabled> itemENabled5= new ArrayList<>();
        itemENabled5.add(new ItemEnabled("BOOLEAN_IS_FILTER","true"));

        List<ItemLongDistanceEnabled> itemLongENabled5= new ArrayList<>();
        itemLongENabled5.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER","true"));

        l5.add(new SearchRequest("ITEM", new Filters(null,null , null, itemENabled5, itemLongENabled5, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests5= new SearchBuilder().searchrequest(l5)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();


        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests),"curry"}, {jsonHelper.getObjectToJSON(searchRequests2),"biryani"},{jsonHelper.getObjectToJSON(searchRequests3),"biryani"}, {jsonHelper.getObjectToJSON(searchRequests4),"curry"}, {jsonHelper.getObjectToJSON(searchRequests5),"curry"}
    };
    }

    /**
     * @author jitender.kumar
     *
     * @return item and restaurant Objects
     * with combination of OR, AND
     * @throws IOException
     */

    @DataProvider(name = "getItemRestComb")
    public Object[][] getItemRestComb() throws IOException {
        List<SearchRequest> l = new ArrayList<>();
        List<ItemName> itemName = new ArrayList<>();
        itemName.add(new ItemName(SANDConstants.stringFilter, "curry"));

        List<ItemPrice> itemPrice = new ArrayList<>();
        itemPrice.add(new ItemPrice(SANDConstants.numberFilter, 150.0, 250.0));
        List<ItemEnabled> itemEnabled = new ArrayList<>();
        itemEnabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLocation> restLoc= new ArrayList<>();
        restLoc.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(12.932,77.602),4.0));

        l.add(new SearchRequest("ITEM", new Filters(null, itemName, itemPrice, itemEnabled, null, null, null, restLoc, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests = new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2 = new ArrayList<>();
        List<ItemName> itemName2 = new ArrayList<>();
        itemName2.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));

        List<ItemPrice> itemPrice2 = new ArrayList<>();
        itemPrice2.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));

        List<RestaurantEnabled> restaurantEnabled= new ArrayList<>();
        restaurantEnabled.add(new RestaurantEnabled("BOOLEAN_IS_FILTER", "true"));

        l2.add(new SearchRequest("ITEM", new Filters(null, itemName2, itemPrice2, null, null, null, null, null, restaurantEnabled, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2 = new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        /*--------------------------Multiple City , ItemEnabled, ItemLongDistance Enabled, Itemname, Item Price---------------------*/
        List<SearchRequest> l3 = new ArrayList<>();
        List<ItemName> itemName3 = new ArrayList<>();
        itemName3.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice3 = new ArrayList<>();
        itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));

        List<ItemEnabled> itemENabled = new ArrayList<>();
        itemENabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));

        List<ItemLongDistanceEnabled> itemLongENabled = new ArrayList<>();
        itemLongENabled.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLongDistanceEnabled> RestaurantLongDistance3 = new ArrayList<>();
        RestaurantLongDistance3.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<CityName> cityName3= new ArrayList<>();
        cityName3.add(new CityName("STRING_EQUALS_FILTER","Bangalore"));
        cityName3.add(new CityName("STRING_EQUALS_FILTER", "Hyderabad"));
        l3.add(new SearchRequest("ITEM", new Filters(null, itemName3, itemPrice3, itemENabled, null, null, null, null, restaurantEnabled, RestaurantLongDistance3, null, cityName3, null, null, null, null), 20));
        SearchRequests searchRequests3 = new SearchBuilder().searchrequest(l3)
                .requestContext(null).buildSearchConan();

        /*----------------------------------------------*/
        List<SearchRequest> l4 = new ArrayList<>();
        List<ItemName> itemName4 = new ArrayList<>();
        itemName4.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice4 = new ArrayList<>();
        itemPrice4.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));

        List<ItemEnabled> itemENabled4 = new ArrayList<>();
        itemENabled4.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));

        List<ItemLongDistanceEnabled> itemLongENabled4 = new ArrayList<>();
        itemLongENabled4.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLongDistanceEnabled> RestaurantLongDistance4 = new ArrayList<>();
        RestaurantLongDistance4.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<CityName> cityName4= new ArrayList<>();
        cityName4.add(new CityName("STRING_EQUALS_FILTER","Bangalore"));
        cityName4.add(new CityName("STRING_EQUALS_FILTER", "Hyderabad"));

        List<CityId> cityIds4= new ArrayList<>();
        cityIds4.add(new CityId(SANDConstants.numberEqualsFilter,1.0));
        l4.add(new SearchRequest("ITEM", new Filters(null, itemName4, itemPrice4, itemENabled4, itemLongENabled4, null, null, null, restaurantEnabled, RestaurantLongDistance4, cityIds4, cityName4, null, null, null, null), 20));
        SearchRequests searchRequests4 = new SearchBuilder().searchrequest(l4)
                .requestContext(null).buildSearchConan();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests), "curry"}, {jsonHelper.getObjectToJSON(searchRequests2), "biryani"},{jsonHelper.getObjectToJSON(searchRequests3), "biryani"}, {jsonHelper.getObjectToJSON(searchRequests4), "biryani"}};
    }

    @DataProvider(name = "getItemRestTagComb")
    public Object[][] getItemRestTagComb() throws IOException {
        List<SearchRequest> l = new ArrayList<>();
        List<ItemName> itemName = new ArrayList<>();
        itemName.add(new ItemName("STRING_EQUALS_FILTER", "curry"));

        List<ItemPrice> itemPrice = new ArrayList<>();
        itemPrice.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 150.0, 250.0));
        List<ItemEnabled> itemEnabled = new ArrayList<>();
        itemEnabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLocation> restLoc= new ArrayList<>();
        restLoc.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(12.932,77.602),4.0));
        List<Tag> tags= new ArrayList<>();
        tags.add(new Tag("TAG_EQUALS_FILTER", "itemdish","curry"));

        l.add(new SearchRequest("ITEM", new Filters(null, itemName, itemPrice, itemEnabled, null, null, null, restLoc, null, null, null, null, null, null, tags, null), 10));
        SearchRequests searchRequests = new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2 = new ArrayList<>();
        List<ItemName> itemName2 = new ArrayList<>();
        itemName2.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));

        List<ItemPrice> itemPrice2 = new ArrayList<>();
        itemPrice2.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));

        List<RestaurantEnabled> restaurantEnabled= new ArrayList<>();
        restaurantEnabled.add(new RestaurantEnabled("BOOLEAN_IS_FILTER", "true"));
        List<Tag> tags2= new ArrayList<>();
        tags2.add(new Tag("TAG_EQUALS_FILTER", "itemdish","biryani"));

        l2.add(new SearchRequest("ITEM", new Filters(null, itemName2, itemPrice2, null, null, null, null, null, restaurantEnabled, null, null, null, null, null, tags2, null), 10));
        SearchRequests searchRequests2 = new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        /*--------------------------Multiple City , ItemEnabled, ItemLongDistance Enabled, Itemname, Item Price---------------------*/
        List<SearchRequest> l3 = new ArrayList<>();
        List<ItemName> itemName3 = new ArrayList<>();
        itemName3.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice3 = new ArrayList<>();
        itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));

        List<ItemEnabled> itemENabled = new ArrayList<>();
        itemENabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));

        List<Tag> tags3= new ArrayList<>();
        tags3.add(new Tag("TAG_EQUALS_FILTER", "itemdish","biryani"));

        List<ItemLongDistanceEnabled> itemLongENabled = new ArrayList<>();
        itemLongENabled.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLongDistanceEnabled> RestaurantLongDistance3 = new ArrayList<>();
        RestaurantLongDistance3.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<CityName> cityName3= new ArrayList<>();
        cityName3.add(new CityName("STRING_EQUALS_FILTER","Bangalore"));
        cityName3.add(new CityName("STRING_EQUALS_FILTER", "Hyderabad"));
        l3.add(new SearchRequest("ITEM", new Filters(null, itemName3, itemPrice3, itemENabled, null, null, null, null, restaurantEnabled, RestaurantLongDistance3, null, cityName3, null, null, tags3, null), 20));
        SearchRequests searchRequests3 = new SearchBuilder().searchrequest(l3)
                .requestContext(null).buildSearchConan();
        /*----------------------------------------------------------------------------------------*/

        List<SearchRequest> l4 = new ArrayList<>();
        List<ItemName> itemName4 = new ArrayList<>();
        itemName4.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice4 = new ArrayList<>();
        itemPrice4.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));

        List<ItemEnabled> itemENabled4 = new ArrayList<>();
        itemENabled4.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));

        List<Tag> tags4= new ArrayList<>();
        tags4.add(new Tag("TAG_EQUALS_FILTER", "itemdish","biryani"));
        tags4.add(new Tag("TAG_EQUALS_FILTER", "Non-Veg","biryani"));

        List<ItemLongDistanceEnabled> itemLongENabled4 = new ArrayList<>();
        itemLongENabled4.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLongDistanceEnabled> RestaurantLongDistance4 = new ArrayList<>();
        RestaurantLongDistance4.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<CityName> cityName4= new ArrayList<>();
        cityName4.add(new CityName("STRING_EQUALS_FILTER","Bangalore"));
        cityName4.add(new CityName("STRING_EQUALS_FILTER", "Hyderabad"));
        l4.add(new SearchRequest("ITEM", new Filters(null, itemName4, itemPrice4, itemENabled4, null, null, null, null, restaurantEnabled, RestaurantLongDistance4, null, cityName4, null, null, tags4, null), 20));
        SearchRequests searchRequests4 = new SearchBuilder().searchrequest(l4)
                .requestContext(null).buildSearchConan();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests), "curry"}, {jsonHelper.getObjectToJSON(searchRequests2), "biryani"},{jsonHelper.getObjectToJSON(searchRequests3), "biryani"}, {jsonHelper.getObjectToJSON(searchRequests4), "biryani"}};
    }

    @DataProvider(name="getItemIdPrice")
    public Object[][] getItemIdPrice() throws IOException{
        List<SearchRequest> list = new ArrayList<>();
        List<ItemPrice> itemPrice = new ArrayList<>();
        itemPrice.add(new ItemPrice(SANDConstants.numberFilter, 10.0, 250.0));
        List<ItemEnabled> itemEnabled = new ArrayList<>();
        itemEnabled.add(new ItemEnabled(SANDConstants.booleanFilter, "true"));
        List<RestaurantLocation> restLoc= new ArrayList<>();
        restLoc.add(new RestaurantLocation(SANDConstants.latLng, new Point(12.932,77.602),4.0));
        List<Tag> tags= new ArrayList<>();
        tags.add(new Tag(SANDConstants.tagFilter, "itemdish","biryani"));
        list.add(new SearchRequest("ITEM", new Filters(null, null, itemPrice, itemEnabled, null, null, null, restLoc, null, null, null, null, null, null, tags, null), 10));
        SearchRequests searchRequests = new SearchBuilder().searchrequest(list)
                .requestContext(null).buildSearchConan();

        List<SearchRequest> list2 = new ArrayList<>();
        List<ItemPrice> itemPrice2 = new ArrayList<>();
        itemPrice2.add(new ItemPrice(SANDConstants.numberFilter, 10.0, 250.0));
        List<ItemId> itemId = new ArrayList<>();
        itemId.add(new ItemId(SANDConstants.numberFilter, 1844787.0));

        List<RestaurantLocation> restLoc2= new ArrayList<>();
        restLoc2.add(new RestaurantLocation(SANDConstants.latLng, new Point(12.932,77.602),4.0));
        List<ItemLongDistanceEnabled> itemLongDistanceEnabled2= new ArrayList<>();
        itemLongDistanceEnabled2.add(new ItemLongDistanceEnabled(SANDConstants.booleanFilter,"true"));

        List<Tag> tags2= new ArrayList<>();
        tags2.add(new Tag(SANDConstants.tagFilter, "itemdish","biryani"));
        list2.add(new SearchRequest("ITEM", new Filters(null, null, itemPrice, itemEnabled, itemLongDistanceEnabled2, null, null, restLoc, null, null, null, null, null, null, tags, null), 10));
        SearchRequests searchRequests2 = new SearchBuilder().searchrequest(list2)
                .requestContext(null).buildSearchConan();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests), "biryani"},{jsonHelper.getObjectToJSON(searchRequests2), "biryani"}};
    }

    @DataProvider(name = "getItemRestTagOrdersCount")
    public Object[][] getItemRestTagOrdersCount() throws IOException {
        List<SearchRequest> l = new ArrayList<>();
        List<ItemName> itemName = new ArrayList<>();
        itemName.add(new ItemName("STRING_EQUALS_FILTER", "curry"));

        List<ItemPrice> itemPrice = new ArrayList<>();
        itemPrice.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 10.0, 250.0));
        List<ItemEnabled> itemEnabled = new ArrayList<>();
        itemEnabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLocation> restLoc= new ArrayList<>();
        restLoc.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(12.932,77.602),4.0));
        List<Tag> tags= new ArrayList<>();
        tags.add(new Tag("TAG_EQUALS_FILTER", "itemdish","curry"));
        List<OrderCountsBreakfast> orderCountsBreakfastList= new ArrayList<>();
        orderCountsBreakfastList.add(new OrderCountsBreakfast(SANDConstants.numberFilter,0.0));

        l.add(new SearchRequest("ITEM", new Filters(null, itemName, itemPrice, itemEnabled, null, null, null, restLoc, null, null, null, null, null, null, tags, orderCountsBreakfastList), 10));
        SearchRequests searchRequests = new SearchBuilder().searchrequest(l)
                .requestContext(null).buildSearchConan();

        List<SearchRequest> l2 = new ArrayList<>();
        List<ItemName> itemName2 = new ArrayList<>();
        itemName2.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));

        List<ItemPrice> itemPrice2 = new ArrayList<>();
        itemPrice2.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));
       /* List<RestaurantName> restName2= new ArrayList<>();
        restName2.add(new RestaurantName("STRING_EQUALS_FILTER", ""));*/
        List<RestaurantEnabled> restaurantEnabled= new ArrayList<>();
        restaurantEnabled.add(new RestaurantEnabled("BOOLEAN_IS_FILTER", "true"));
        List<Tag> tags2= new ArrayList<>();
        tags2.add(new Tag("TAG_EQUALS_FILTER", "itemdish","biryani"));

        l2.add(new SearchRequest("ITEM", new Filters(null, itemName2, itemPrice2, null, null, null, null, null, restaurantEnabled, null, null, null, null, null, tags2, orderCountsBreakfastList), 10));
        SearchRequests searchRequests2 = new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        /*--------------------------Multiple City , ItemEnabled, ItemLongDistance Enabled, Itemname, Item Price, OrderCount---------------------*/
        List<SearchRequest> l3 = new ArrayList<>();
        List<ItemName> itemName3 = new ArrayList<>();
        itemName3.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice3 = new ArrayList<>();
        itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));

        List<ItemEnabled> itemENabled = new ArrayList<>();
        itemENabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));

        List<Tag> tags3= new ArrayList<>();
        tags3.add(new Tag("TAG_EQUALS_FILTER", "itemdish","biryani"));
        tags3.add(new Tag("TAG_EQUALS_FILTER", "","biryani"));

        List<ItemLongDistanceEnabled> itemLongENabled = new ArrayList<>();
        itemLongENabled.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLongDistanceEnabled> RestaurantLongDistance3 = new ArrayList<>();
        RestaurantLongDistance3.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<CityName> cityName3= new ArrayList<>();
        cityName3.add(new CityName("STRING_EQUALS_FILTER","Bangalore"));
        cityName3.add(new CityName("STRING_EQUALS_FILTER", "Gurgaon"));
        l3.add(new SearchRequest("ITEM", new Filters(null, itemName3, itemPrice3, itemENabled, null, null, null, null, restaurantEnabled, RestaurantLongDistance3, null, cityName3, null, null, tags3, orderCountsBreakfastList), 30));
        SearchRequests searchRequests3 = new SearchBuilder().searchrequest(l3)
                .requestContext(null).buildSearchConan();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests), "curry"}, {jsonHelper.getObjectToJSON(searchRequests2), "biryani"},{jsonHelper.getObjectToJSON(searchRequests3), "biryani"}};
    }

    @DataProvider(name = "getItemRestTagAreaOrdersCount")
    public Object[][] getItemRestTagAreaOrdersCount() throws IOException {
        List<SearchRequest> l = new ArrayList<>();
        List<ItemName> itemName = new ArrayList<>();
        itemName.add(new ItemName("STRING_EQUALS_FILTER", "curry"));

        List<ItemPrice> itemPrice = new ArrayList<>();
        itemPrice.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 10.0, 250.0));
        List<ItemEnabled> itemEnabled = new ArrayList<>();
        itemEnabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLocation> restLoc= new ArrayList<>();
        restLoc.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(12.932,77.602),4.0));
        List<Tag> tags= new ArrayList<>();
        tags.add(new Tag("TAG_EQUALS_FILTER", "itemdish","curry"));
        List<OrderCountsBreakfast> orderCountsBreakfastList= new ArrayList<>();
        orderCountsBreakfastList.add(new OrderCountsBreakfast(SANDConstants.numberFilter,0.0));
        List<AreaName> areaNames= new ArrayList();
        areaNames.add(new AreaName(SANDConstants.stringFilter,"Kothrud"));


        l.add(new SearchRequest("ITEM", new Filters(null, itemName, itemPrice, itemEnabled, null, null, null, restLoc, null, null, null, null, null, areaNames, tags, orderCountsBreakfastList), 10));
        SearchRequests searchRequests = new SearchBuilder().searchrequest(l)
                .requestContext(null).buildSearchConan();

        List<SearchRequest> l2 = new ArrayList<>();
        List<ItemName> itemName2 = new ArrayList<>();
        itemName2.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));

        List<ItemPrice> itemPrice2 = new ArrayList<>();
        itemPrice2.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));
       /* List<RestaurantName> restName2= new ArrayList<>();
        restName2.add(new RestaurantName("STRING_EQUALS_FILTER", ""));*/
        List<RestaurantEnabled> restaurantEnabled= new ArrayList<>();
        restaurantEnabled.add(new RestaurantEnabled("BOOLEAN_IS_FILTER", "true"));
        List<Tag> tags2= new ArrayList<>();
        tags2.add(new Tag("TAG_EQUALS_FILTER", "itemdish","biryani"));
        List<CityName> cityName2= new ArrayList<>();
        cityName2.add(new CityName("STRING_EQUALS_FILTER","Pune"));
        List<RestaurantLocation> restLoc1= new ArrayList<>();
        restLoc1.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(17.437034, 78.44503600000007),4.0));


        l2.add(new SearchRequest("ITEM", new Filters(null, itemName2, itemPrice2, null, null, null, null, restLoc1, restaurantEnabled, null, null, cityName2, null, areaNames, tags2, null), 10));
        SearchRequests searchRequests2 = new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(17.437034, 78.44503600000007))).buildSearchConan();
        /*--------------------------Multiple City , ItemEnabled, ItemLongDistance Enabled, Itemname, Item Price, OrderCount---------------------*/
        List<SearchRequest> l3 = new ArrayList<>();
        List<ItemName> itemName3 = new ArrayList<>();
        itemName3.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice3 = new ArrayList<>();
        itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));

        List<ItemEnabled> itemENabled = new ArrayList<>();
        itemENabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));

        List<Tag> tags3= new ArrayList<>();
        tags3.add(new Tag("TAG_EQUALS_FILTER", "itemdish","biryani"));
        tags3.add(new Tag("TAG_EQUALS_FILTER", "","biryani"));

        List<ItemLongDistanceEnabled> itemLongENabled = new ArrayList<>();
        itemLongENabled.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLongDistanceEnabled> RestaurantLongDistance3 = new ArrayList<>();
        RestaurantLongDistance3.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<CityName> cityName3= new ArrayList<>();
        cityName3.add(new CityName("STRING_EQUALS_FILTER","Bangalore"));
        cityName3.add(new CityName("STRING_EQUALS_FILTER", "Gurgaon"));
        l3.add(new SearchRequest("ITEM", new Filters(null, itemName3, itemPrice3, itemENabled, null, null, null, null, restaurantEnabled, RestaurantLongDistance3, null, cityName3, null, null, tags3, orderCountsBreakfastList), 30));
        SearchRequests searchRequests3 = new SearchBuilder().searchrequest(l3)
                .requestContext(null).buildSearchConan();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests), "curry"}, {jsonHelper.getObjectToJSON(searchRequests2), "biryani"},{jsonHelper.getObjectToJSON(searchRequests3), "biryani"}};
    }

    @DataProvider(name = "getItemIdTagAreaOrdersCount")
    public Object[][] getItemIdTagAreaOrdersCount() throws IOException {
        List<SearchRequest> l = new ArrayList<>();
        List<ItemName> itemName = new ArrayList<>();
        itemName.add(new ItemName("STRING_EQUALS_FILTER", "curry"));

        List<ItemPrice> itemPrice = new ArrayList<>();
        itemPrice.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 10.0, 250.0));
        List<ItemEnabled> itemEnabled = new ArrayList<>();
        itemEnabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLocation> restLoc= new ArrayList<>();
        restLoc.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(12.932,77.602),4.0));
        List<Tag> tags= new ArrayList<>();
        tags.add(new Tag("TAG_EQUALS_FILTER", "itemdish","curry"));
        List<OrderCountsBreakfast> orderCountsBreakfastList= new ArrayList<>();
        orderCountsBreakfastList.add(new OrderCountsBreakfast(SANDConstants.numberFilter,0.0));
        List<AreaId> areaId= new ArrayList();
        areaId.add(new AreaId(SANDConstants.numberEqualsFilter,116.0));

        List<CityId> cityId= new ArrayList();
        cityId.add(new CityId(SANDConstants.numberEqualsFilter,6.0));


        l.add(new SearchRequest("ITEM", new Filters(null, itemName, itemPrice, itemEnabled, null, null, null, restLoc, null, null, null, null, null, null, tags, orderCountsBreakfastList), 10));
        SearchRequests searchRequests = new SearchBuilder().searchrequest(l)
                .requestContext(null).buildSearchConan();

        List<SearchRequest> l2 = new ArrayList<>();
        List<ItemName> itemName2 = new ArrayList<>();
        itemName2.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice2 = new ArrayList<>();
        itemPrice2.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));
        List<RestaurantEnabled> restaurantEnabled= new ArrayList<>();
        restaurantEnabled.add(new RestaurantEnabled(SANDConstants.booleanFilter, "true"));

        List<RestaurantLocation> restLoc1= new ArrayList<>();
        restLoc1.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(18.508077361540966, 73.79226882962348),4.0));

        l2.add(new SearchRequest("ITEM", new Filters(null, itemName2, itemPrice2, null, null, null, null, restLoc1, restaurantEnabled, null, cityId, null, areaId, null, null, null), 10));
        SearchRequests searchRequests2 = new SearchBuilder().searchrequest(l2)
                .requestContext(null).buildSearchConan();
        /*--------------------------Multiple City , ItemEnabled, ItemLongDistance Enabled, Itemname, Item Price, OrderCount---------------------*/
        List<SearchRequest> l3 = new ArrayList<>();
        List<ItemName> itemName3 = new ArrayList<>();
        itemName3.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice3 = new ArrayList<>();
        itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));
        List<ItemEnabled> itemENabled = new ArrayList<>();
        itemENabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));

        List<Tag> tags3= new ArrayList<>();
        tags3.add(new Tag("TAG_EQUALS_FILTER", "itemdish","biryani"));
        List<ItemLongDistanceEnabled> itemLongENabled = new ArrayList<>();
        itemLongENabled.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLongDistanceEnabled> RestaurantLongDistance3 = new ArrayList<>();
        RestaurantLongDistance3.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<CityName> cityName3= new ArrayList<>();
        cityName3.add(new CityName("STRING_EQUALS_FILTER","Bangalore"));
        cityName3.add(new CityName("STRING_EQUALS_FILTER", "Gurgaon"));
        l3.add(new SearchRequest("ITEM", new Filters(null, itemName3, itemPrice3, itemENabled, null, null, null, null, restaurantEnabled, RestaurantLongDistance3, cityId, null, areaId, null, tags3, orderCountsBreakfastList), 30));
        SearchRequests searchRequests3 = new SearchBuilder().searchrequest(l3)
                .requestContext(null).buildSearchConan();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests), "curry"}, {jsonHelper.getObjectToJSON(searchRequests2), "biryani"},{jsonHelper.getObjectToJSON(searchRequests3), "biryani"}};
    }

    @DataProvider(name = "getItemIdRestIDTagArea")
    public Object[][] getItemIdRestIDTagArea() throws IOException {
        List<SearchRequest> l = new ArrayList<>();
        List<ItemName> itemName = new ArrayList<>();
        itemName.add(new ItemName("STRING_EQUALS_FILTER", "curry"));

        List<ItemPrice> itemPrice = new ArrayList<>();
        itemPrice.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 10.0, 250.0));
        List<ItemEnabled> itemEnabled = new ArrayList<>();
        itemEnabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLocation> restLoc= new ArrayList<>();
        restLoc.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(12.932,77.602),4.0));
        List<Tag> tags= new ArrayList<>();
        tags.add(new Tag("TAG_EQUALS_FILTER", "itemdish","curry"));
        List<OrderCountsBreakfast> orderCountsBreakfastList= new ArrayList<>();
        orderCountsBreakfastList.add(new OrderCountsBreakfast(SANDConstants.numberFilter,0.0));
        List<AreaId> areaId= new ArrayList();
        areaId.add(new AreaId(SANDConstants.numberEqualsFilter,116.0));
        List<RestaurantId> restaurantId= new ArrayList<>();
        restaurantId.add(new RestaurantId(SANDConstants.numberEqualsFilter,8145.0));

        List<CityId> cityId= new ArrayList();
        cityId.add(new CityId(SANDConstants.numberEqualsFilter,6.0));


        l.add(new SearchRequest("ITEM", new Filters(null, itemName, itemPrice, itemEnabled, null, restaurantId, null, null, null, null, null, null, null, null, tags, orderCountsBreakfastList), 10));
        SearchRequests searchRequests = new SearchBuilder().searchrequest(l)
                .requestContext(null).buildSearchConan();

        List<SearchRequest> l2 = new ArrayList<>();
        List<ItemName> itemName2 = new ArrayList<>();
        itemName2.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice2 = new ArrayList<>();
        itemPrice2.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));
        List<RestaurantEnabled> restaurantEnabled= new ArrayList<>();
        restaurantEnabled.add(new RestaurantEnabled(SANDConstants.booleanFilter, "true"));

        List<RestaurantLocation> restLoc1= new ArrayList<>();
        restLoc1.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(18.5017, 73.81580699999995),4.0));

        l2.add(new SearchRequest("ITEM", new Filters(null, itemName2, itemPrice2, itemEnabled, null, restaurantId, null, restLoc1, restaurantEnabled, null, cityId, null, areaId, null, null, null), 10));
        SearchRequests searchRequests2 = new SearchBuilder().searchrequest(l2)
                .requestContext(null).buildSearchConan();
        /*--------------------------Multiple City , ItemEnabled, ItemLongDistance Enabled, Item Name, Item Price, OrderCount---------------------*/
        List<SearchRequest> l3 = new ArrayList<>();
        List<ItemName> itemName3 = new ArrayList<>();
        itemName3.add(new ItemName("STRING_EQUALS_FILTER", "biryani"));
        List<ItemPrice> itemPrice3 = new ArrayList<>();
        itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER", 100.0, 250.0));
        List<ItemEnabled> itemENabled = new ArrayList<>();
        itemENabled.add(new ItemEnabled("BOOLEAN_IS_FILTER", "true"));

        List<Tag> tags3= new ArrayList<>();
        tags3.add(new Tag("TAG_EQUALS_FILTER", "itemdish","biryani"));
        List<ItemLongDistanceEnabled> itemLongENabled = new ArrayList<>();
        itemLongENabled.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));
        List<RestaurantLongDistanceEnabled> RestaurantLongDistance3 = new ArrayList<>();
        RestaurantLongDistance3.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER", "true"));

        l3.add(new SearchRequest("ITEM", new Filters(null, itemName3, itemPrice3, itemENabled, null, null, null, null, restaurantEnabled, RestaurantLongDistance3, cityId, null, areaId, null, tags3, orderCountsBreakfastList), 30));
        SearchRequests searchRequests3 = new SearchBuilder().searchrequest(l3)
                .requestContext(null).buildSearchConan();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests), "curry"}, {jsonHelper.getObjectToJSON(searchRequests2), "biryani"},{jsonHelper.getObjectToJSON(searchRequests3), "biryani"}};
    }




    @DataProvider(name = "getItemId")
    public Object[][] getItemId() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<ItemId> itemId= new ArrayList<>();
        itemId.add(new ItemId("NUMBER_EQUALS_FILTER",4383736.0));
        l.add(new SearchRequest("ITEM", new Filters(itemId,null , null, null, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)}
        };
    }

    @DataProvider(name = "getItemPrice")
    public Object[][] getItemPrice() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<ItemPrice> itemPrice= new ArrayList<>();
        itemPrice.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",150.0,250.0));
        l.add(new SearchRequest("ITEM", new Filters(null,null , itemPrice, null, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<ItemPrice> itemPrice2= new ArrayList<>();
        itemPrice2.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",0.0,200.0));
        itemPrice2.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",150.0,250.0));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , itemPrice2, null, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l3= new ArrayList<>();
        List<ItemPrice> itemPrice3= new ArrayList<>();
        itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",100.0, null));
        //itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",150.0,250.0));
        l3.add(new SearchRequest("ITEM", new Filters(null,null , itemPrice3, null, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests3= new SearchBuilder().searchrequest(l3)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l4= new ArrayList<>();
        List<ItemPrice> itemPrice4= new ArrayList<>();
        itemPrice4.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",null, 200.0));
        //itemPrice3.add(new ItemPrice("NUMBER_IN_RANGE_FILTER",150.0,250.0));
        l4.add(new SearchRequest("ITEM", new Filters(null,null , itemPrice4, null, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests4= new SearchBuilder().searchrequest(l4)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)},{jsonHelper.getObjectToJSON(searchRequests3)},{jsonHelper.getObjectToJSON(searchRequests4)}
        };
    }

    @DataProvider(name = "getItemEnabled")
    public Object[][] getItemEnabled() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<ItemEnabled> itemEnabled= new ArrayList<>();
        itemEnabled.add(new ItemEnabled("BOOLEAN_IS_FILTER","true"));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, itemEnabled, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<ItemEnabled> itemEnabled2= new ArrayList<>();
        itemEnabled2.add(new ItemEnabled("BOOLEAN_IS_FILTER","false"));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, itemEnabled2, null, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(null).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)}
        };
    }

    @DataProvider(name = "getItemLongDistance")
    public Object[][] getItemLongDistance() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<ItemLongDistanceEnabled> itemEnabled= new ArrayList<>();
        itemEnabled.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER","true"));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, itemEnabled, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<ItemLongDistanceEnabled> itemEnabled2= new ArrayList<>();
        itemEnabled2.add(new ItemLongDistanceEnabled("BOOLEAN_IS_FILTER","false"));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, itemEnabled2, null, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(null).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)}
        };
    }

    @DataProvider(name = "getRestId")
    public Object[][] getRestId() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<RestaurantId> restId= new ArrayList<>();
        restId.add(new RestaurantId("NUMBER_EQUALS_FILTER",9990.0));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, restId, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<RestaurantId> restId2= new ArrayList<>();
        restId2.add(new RestaurantId("NUMBER_EQUALS_FILTER",223.0));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, restId2, null, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests),"9990.0"},{jsonHelper.getObjectToJSON(searchRequests2),"223.0"}
        };
    }

    @DataProvider(name = "getRestName")
    public Object[][] getRestName() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<RestaurantName> restName= new ArrayList<>();
        restName.add(new RestaurantName("STRING_EQUALS_FILTER","Swiggy FoodCourt"));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, restName, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<RestaurantName> restName2= new ArrayList<>();
        restName2.add(new RestaurantName("STRING_EQUALS_FILTER","Truffles"));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, restName2, null, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)}
        };
    }

    @DataProvider(name = "getRestLoc")
    public Object[][] getRestLoc() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<RestaurantLocation> restLoc= new ArrayList<>();
        restLoc.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(12.932,77.602),4.0));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, restLoc, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(null).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<RestaurantLocation> restLoc2= new ArrayList<>();
        restLoc2.add(new RestaurantLocation("LATLONG_NEAR_POINT", new Point(12.937,77.602),4.0));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, restLoc2, null, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(null).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)}
        };
    }

    @DataProvider(name = "getRestEnabled")
    public Object[][] getRestEnabled() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<RestaurantEnabled> restEnabled= new ArrayList<>();
        restEnabled.add(new RestaurantEnabled("BOOLEAN_IS_FILTER","true"));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, restEnabled, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<RestaurantEnabled> restEnabled2= new ArrayList<>();
        restEnabled2.add(new RestaurantEnabled("BOOLEAN_IS_FILTER","false"));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, restEnabled2, null, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(null).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)}
        };
    }

    @DataProvider(name = "getRestLongDistance")
    public Object[][] getRestLongDistance() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<RestaurantLongDistanceEnabled> restEnabled= new ArrayList<>();
        restEnabled.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER","true"));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, restEnabled, null, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<RestaurantLongDistanceEnabled> restEnabled2= new ArrayList<>();
        restEnabled2.add(new RestaurantLongDistanceEnabled("BOOLEAN_IS_FILTER","false"));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, restEnabled2, null, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(null).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)}
        };
    }

    @DataProvider(name = "getCityId")
    public Object[][] getCityId() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<CityId> city= new ArrayList<>();
        city.add(new CityId("NUMBER_EQUALS_FILTER",1.0));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, null, city, null, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<CityId> city2= new ArrayList<>();
        city2.add(new CityId("NUMBER_EQUALS_FILTER",1.0));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, null, city2, null, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests),"1.0"},{jsonHelper.getObjectToJSON(searchRequests2),"1.0"}
        };
    }

    @DataProvider(name = "getCityName")
    public Object[][] getCityName() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<CityName> city= new ArrayList<>();
        city.add(new CityName("STRING_EQUALS_FILTER","Bangalore"));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, null, null, city, null, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<CityName> city2= new ArrayList<>();
        city2.add(new CityName("STRING_EQUALS_FILTER","Bangalore"));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, null, null, city2, null, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)}
        };
    }

    @DataProvider(name = "getAreaId")
    public Object[][] getAreaId() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<AreaId> area= new ArrayList<>();
        area.add(new AreaId("NUMBER_EQUALS_FILTER",160.0));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, null, null, null, area, null, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<AreaId> area2= new ArrayList<>();
        area2.add(new AreaId("NUMBER_EQUALS_FILTER",1.0));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, null, null, null, area2, null, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)}
        };
    }

    @DataProvider(name = "getAreaName")
    public Object[][] getAreaName() throws IOException {
        List<SearchRequest> l= new ArrayList<>();
        List<AreaName> area= new ArrayList<>();
        area.add(new AreaName("STRING_EQUALS_FILTER","BTM"));
        l.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, null, null, null, null, area, null, null), 10));
        SearchRequests searchRequests= new SearchBuilder().searchrequest(l)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();

        List<SearchRequest> l2= new ArrayList<>();
        List<AreaName> area2= new ArrayList<>();
        area2.add(new AreaName("STRING_EQUALS_FILTER","Koramangala"));
        l2.add(new SearchRequest("ITEM", new Filters(null,null , null, null, null, null, null, null, null, null, null, null, null, area2, null, null), 10));
        SearchRequests searchRequests2= new SearchBuilder().searchrequest(l2)
                .requestContext(new RequestContext(new LatLong(12.937, 77.602))).buildSearchConan();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(searchRequests)},{jsonHelper.getObjectToJSON(searchRequests2)}
        };
    }

    @DataProvider(name = "collectionLatLng")
    public Object[][] collectionLatLng(){
        return new Object[][]{{"12.932,77.602"}};
    }

    @DataProvider(name = "collectionLatLng1")
    public Object[][] collectionLatLng1(){
        return new Object[][]{{"12.932,77.602","12.932", "77.602" }};
    }

    @DataProvider(name = "collectionLatLng2")
    public Object[][] collectionLatLng2(){
        return new Object[][]{{"12.9166, 77.6101","12.9166", "77.6101"}};
    }

    @DataProvider(name = "collectionLatLngTag")
    public Object[][] collectionLatLngTag(){
        return new Object[][]{{"12.932,77.602","12.932", "77.602", SANDConstants.tagNumber}};
    }

    @DataProvider(name = "collectionLatLngTagEquals")
    public Object[][] collectionLatLngTagEquals(){
        return new Object[][]{{"12.932,77.602","12.932", "77.602", SANDConstants.tagEquals}};
    }

    @DataProvider(name = "collectionLatLngSla")
    public Object[][] collectionLatLngSla(){
        return new Object[][]{{"12.932,77.602","12.932", "77.602", SANDConstants.restSla}};
    }

    @DataProvider(name = "collectionLatLngItem")
    public Object[][] collectionLatLngItem(){
        return new Object[][]{{"12.932,77.602","12.932", "77.602", SANDConstants.tagNumber}};
    }

    @DataProvider(name = "collectionLatLngLong")
    public Object[][] collectionLatLngLong(){
        return new Object[][]{{"12.932,77.602","12.932", "77.602", SANDConstants.longDis}};
    }

    @DataProvider(name = "collectionLatLngMulti")
    public Object[][] collectionLatLngMulti(){
        return new Object[][]{{"12.932,77.602","12.932", "77.602", SANDConstants.multiTd}};
    }

    @DataProvider(name = "collectionLatLngMultiBTM")
    public Object[][] collectionLatLngMultiBTM(){
        return new Object[][]{{"12.9166, 77.6101","12.9166", "77.6101", SANDConstants.multiTd}};
    }

    @DataProvider(name = "collectionLatLngMultiBTM99")
    public Object[][] collectionLatLngMultiBTM99(){
        return new Object[][]{{"12.9166, 77.6101","12.9166", "77.6101", SANDConstants.price99}};
    }



    @DataProvider(name = "collectionTDRest")
    public Object[][] collectionTDRest() throws IOException{

        List<Slot> slots = new ArrayList<>();
        slots.add(new Slot("1430", "ALL", "1419"));
        // Creating RestaurantLists
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        restaurantLists1.add(new RestaurantList("316", "Test", new ArrayList<>()));

        CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 3600).toInstant().getEpochSecond() * 1000))
                .campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Jitender")
                .discountLevel("Restaurant").restaurantList(restaurantLists1)
                 //.slots(slots)
                .ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").userRestriction(false)
                .timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
                .commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

        return new Object[][]{{"12.932,77.602"}};
    }

    @DataProvider(name = "collectionItemTD")
    public Object[][] collectionItemTD() throws IOException{

    CreateTdEntry percetageItemTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
            .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
            .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
            .campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
            .discountLevel("Item")
            .restaurantList(rngHelper.createRandomRestaurantList("212", true, 2, true, 2, true, 2))
            /** .slots(slots) **/
            .ruleDiscount("Percentage", "Item", "10", "0", "2000").userRestriction(false).timeSlotRestriction(false)
            .firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
            .dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();
        rngHelper.createTD(jsonHelper.getObjectToJSON(percetageItemTradeDiscount));
        return new Object[][]{{"12.932,77.602"}};
    }

    @DataProvider(name = "collectionHolidaySlot")
    public Object[][] collectionHolidaySlot() throws IOException{
        return new Object[][]{{"12.932,77.602"}};
    }

    public String dateTime()
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        String formatTime = new SimpleDateFormat("HH:mm:ss").format(date);
        String DT=modifiedDate+"T"+formatTime;
        return DT;
    }
    public String extendedTime(int i)
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        LocalDateTime d = LocalDateTime.now();
        String dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss").format(d.plusMinutes(i));
        String DT=modifiedDate+"T"+dateFormat;
        return DT;
    }

    @DataProvider(name="collectionFLatTD")
    public Object[][] collectionFLatTD() throws IOException{
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        restaurantLists1.add(new RestaurantList("212", "Test", new ArrayList<>()));
        CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
                .campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
                .discountLevel("Restaurant").restaurantList(restaurantLists1)
                // .slots(slots)
                .ruleDiscount("Flat", "Restaurant", "100", "0").userRestriction(false).timeSlotRestriction(false)
                .firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT").build();

        rngHelper.createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
        return new Object[][]{{"12.932,77.602"}};
    }


    @DataProvider(name = "collectionTDItem")
    public Object[][] collectionTDItem() throws IOException{
        Menu menu = new Menu();
        String itemId = new Integer(rm.nextInt()).toString();
        menu.setId(itemId);
        menu.setName("testItem");
        List<Menu> menus = new ArrayList<>();
        menus.add(menu);

        SubCategory subCategory = new SubCategory();
        String SubcategoryId = new Integer(rm.nextInt()).toString();
        subCategory.setId(SubcategoryId);
        subCategory.setName("test");
        subCategory.setMenu(menus);

        List<SubCategory>subCategories =  new ArrayList<>();
        subCategories.add(subCategory);

        Category category = new Category();
        String categoryId = new Integer(rm.nextInt()).toString();
        category.setId(categoryId);
        category.setName("test");
        category.setSubCategories(subCategories);

        List<Category> categories = new ArrayList<>();
        categories.add(category);

        RestaurantList restaurant = new RestaurantList();
        String restId = "3727";
        restaurant.setId(restId);
        restaurant.setName("test");
        restaurant.setCategories(categories);

        List<RestaurantList> restaurantList = new ArrayList<>();
        restaurantList.add(restaurant);
        //restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
        //HashMap<String, String> item_map = getitem(restaurantLists1.get(0));
        //disabledActiveTD(restId);
        CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder()
                .nameSpace("ItemLevel")
                .header("ItemLevel")
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
                .campaign_type("Flat")
                .restaurant_hit("100")
                .enabled(true)
                .swiggy_hit("0")
                .createdBy("Manu")
                .discountLevel("Restaurant")
                .restaurantList(restaurantList)
                //.slots(slots)
                .ruleDiscount("Flat", "Item", "20", "0")
                .userRestriction(false)
                .timeSlotRestriction(false)
                .firstOrderRestriction(true)
                .taxesOnDiscountedBill(false)
                .commissionOnFullBill(false)
                .dormant_user_type("ZERO_DAYS_DORMANT")
                .build();

        Processor processor = rngHelper.createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
        return new Object[][]{{"12.932,77.602"}};
    }

    public int getRandomNo(int minimum,int maximum){
        Random rn = new Random();
        int range = maximum - minimum + 1;
        int randomNum =  rn.nextInt(range) + minimum;
        return randomNum;
    }
}