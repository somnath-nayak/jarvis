package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ItemPrice {

    private String type;
    private Double min;
    private Double max;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemPrice() {
    }

    /**
     *
     * @param min
     * @param max
     * @param type
     */
    public ItemPrice(String type, Double min, Double max) {
        super();
        this.type = type;
        this.min = min;
        this.max = max;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("min", min).append("max", max).toString();
    }

}