package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import java.util.List;

public class MealItemBuilder {
    private MealItem mealItem;

    public MealItemBuilder(){
        mealItem = new MealItem();
    }

    public MealItemBuilder mealId(Integer mealId){
        mealItem.setMealId(mealId);
        return this;
    }

    public MealItemBuilder quantity(Integer quantity){
        mealItem.setQuantity(quantity);
        return this;
    }

    public MealItemBuilder groups(List<Group> groups){
        mealItem.setGroups(groups);
        return this;
    }

    public MealItem build(){
        return mealItem;
    }
}
