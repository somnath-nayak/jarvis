package com.swiggy.api.sf.rng.pojo.freebie;

import org.apache.commons.lang.time.DateUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CreateFreebieTdBuilder {

	private CreateFreebieTDEntry createFreebieTDEntry;

	public CreateFreebieTdBuilder() {
		createFreebieTDEntry = new CreateFreebieTDEntry();
	}

	public CreateFreebieTdBuilder nameSpace(String nameSpace) {
		createFreebieTDEntry.setNamespace(nameSpace);
		return this;
	}

	public CreateFreebieTdBuilder header(String header) {
		createFreebieTDEntry.setHeader(header);
		return this;
	}

	public CreateFreebieTdBuilder description(String description) {
		createFreebieTDEntry.setDescription(description);
		return this;
	}

	public CreateFreebieTdBuilder valid_from(String valid_from) {
		createFreebieTDEntry.setValidFrom(valid_from);
		return this;
	}

	public CreateFreebieTdBuilder valid_till(String valid_till) {
		createFreebieTDEntry.setValidTill(valid_till);
		return this;
	}

	public CreateFreebieTdBuilder campaign_type(String campaign_type) {
		createFreebieTDEntry.setCampaignType(campaign_type);
		return this;
	}

	public CreateFreebieTdBuilder restaurant_hit(String restaurant_hit) {
		createFreebieTDEntry.setRestaurantHit(restaurant_hit);
		return this;
	}

	public CreateFreebieTdBuilder enabled(String enabled) {
		createFreebieTDEntry.setEnabled(enabled);
		return this;
	}

	public CreateFreebieTdBuilder swiggy_hit(String swiggy_hit) {
		createFreebieTDEntry.setSwiggyHit(swiggy_hit);
		return this;
	}

	public CreateFreebieTdBuilder createdBy(String createdBy) {
		createFreebieTDEntry.setCreatedBy(createdBy);
		return this;
	}

	public CreateFreebieTdBuilder discountLevel(String discountLevel) {
		createFreebieTDEntry.setDiscountLevel(discountLevel);
		return this;
	}

	public CreateFreebieTdBuilder commissionOnFullBill(String commissionOnFullBill) {
		createFreebieTDEntry.setCommissionOnFullBill(commissionOnFullBill);
		return this;
	}

	public CreateFreebieTdBuilder taxesOnDiscountedBill(String taxesOnDiscountedBill) {
		createFreebieTDEntry.setTaxesOnDiscountedBill(taxesOnDiscountedBill);
		return this;
	}

	public CreateFreebieTdBuilder firstOrderRestriction(String firstOrderRestriction) {
		createFreebieTDEntry.setFirstOrderRestriction(firstOrderRestriction);
		return this;
	}

	public CreateFreebieTdBuilder restaurantFirstOrder(String restaurantFirstOrder) {
		createFreebieTDEntry.setRestaurantFirstOrder(restaurantFirstOrder);
		return this;
	}

	public CreateFreebieTdBuilder timeSlotRestriction(String timeSlotRestriction) {
		createFreebieTDEntry.setTimeSlotRestriction(timeSlotRestriction);
		return this;
	}

	public CreateFreebieTdBuilder userRestriction(String userRestriction) {
		createFreebieTDEntry.setUserRestriction(userRestriction);
		return this;
	}

	public CreateFreebieTdBuilder dormant_user_type(String dormant_user_type) {
		createFreebieTDEntry.setDormantUserType(dormant_user_type);
		return this;
	}

	public CreateFreebieTdBuilder slots(List<FreebieSlot> slotsDetails) {
		ArrayList<FreebieSlot> slotEntries = new ArrayList<>();
		for (FreebieSlot slotEntry : slotsDetails) {
			slotEntries.add(slotEntry);
		}
		createFreebieTDEntry.setSlots(slotEntries);
		return this;
	}

	public CreateFreebieTdBuilder ruleDiscount(String type, String discountLevel, String minCartAmount, String itemId) {
		createFreebieTDEntry.setRuleDiscount(new FreebieRuleDiscount(type, discountLevel, minCartAmount, itemId));
		return this;
	}

	public CreateFreebieTdBuilder restaurantList(List<FreebieRestaurantList> restaurantLists) {
		createFreebieTDEntry.setRestaurantList(restaurantLists);
		return this;
	}


	public CreateFreebieTdBuilder isSuper(Boolean isSuper) {
		createFreebieTDEntry.setIsSuper(isSuper);
		return this;
	}

	public CreateFreebieTDEntry build() throws IOException {
		defaultData();
		return createFreebieTDEntry;
	}

	private void defaultData() throws IOException {
		if (createFreebieTDEntry.getNamespace() == null) {
			createFreebieTDEntry.setNamespace("DefaultNamespace");
		}
		if (createFreebieTDEntry.getDescription() == null) {
			createFreebieTDEntry.setDescription("Default Description");
		}
		if (createFreebieTDEntry.getSlots() == null) {
			createFreebieTDEntry.setSlots(new ArrayList<>());
		}
		if (createFreebieTDEntry.getHeader() == null) {
			createFreebieTDEntry.setHeader("Default Header");
		}
		if (createFreebieTDEntry.getCreatedBy() == null) {
			createFreebieTDEntry.setCreatedBy("Default Created By-Manu");
		}
		if (createFreebieTDEntry.getUserRestriction() == null) {
			createFreebieTDEntry.setUserRestriction("false");
		}
		if (createFreebieTDEntry.getTimeSlotRestriction() == null) {
			createFreebieTDEntry.setTimeSlotRestriction("false");
		}
		if (createFreebieTDEntry.getFirstOrderRestriction() == null) {
			createFreebieTDEntry.setFirstOrderRestriction("false");
		}
		if (createFreebieTDEntry.getTaxesOnDiscountedBill() == null) {
			createFreebieTDEntry.setTaxesOnDiscountedBill("false");
		}
		if (createFreebieTDEntry.getCommissionOnFullBill() == null) {
			createFreebieTDEntry.setCommissionOnFullBill("false");
		}
		if (createFreebieTDEntry.getDormantUserType() == null) {
			createFreebieTDEntry.setDormantUserType("ZERO_DAYS_DORMANT");
		}
		if (createFreebieTDEntry.getValidTill() == null) {
			createFreebieTDEntry.setValidTill(
					String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000));
		}
		if (createFreebieTDEntry.getValidFrom() == null) {
			createFreebieTDEntry.setValidFrom(
					String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000));
		}
		if (createFreebieTDEntry.getRestaurantHit() == null) {
			createFreebieTDEntry.setRestaurantHit("100");
		}

		if (createFreebieTDEntry.getSwiggyHit() == null) {
			createFreebieTDEntry.setSwiggyHit("0");
		}

		if (createFreebieTDEntry.getEnabled() == null) {
			createFreebieTDEntry.setEnabled("true");
		}

		// set values if required...
	}

}