package com.swiggy.api.sf.snd.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.constants.IStatusConstants;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.*;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.KafkaHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import net.minidev.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;


public class SnDTest extends SnDDp {
	SnDHelper sDHelper = new SnDHelper();
	SANDHelper helper = new SANDHelper();
	POPHelper popHelper = new POPHelper();
	CommonAPIHelper apiHelper = new CommonAPIHelper();
	CMSHelper cmsHelper = new CMSHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	CheckoutHelper checkoutHelper = new CheckoutHelper();
	DeliveryServiceHelper deliveryhelper = new DeliveryServiceHelper();
	SwiggyCafeHelper cafeHelper = new SwiggyCafeHelper();
	RedisHelper redisHelper = new RedisHelper();
	KafkaHelper kafkaHelper = new KafkaHelper();
	HeaderHelper headerHelper = new HeaderHelper();
	RngHelper rngHelper = new RngHelper();
	OrderPlace orderPlace = new OrderPlace();
	DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
	EDVOHelper edvoHelper= new EDVOHelper();


	@Test(dataProvider = "getLatLong", groups = {"sanity", "smoke", "regression"})
	public void aggregatorTest(String lat, String lng) {
		String resp = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");
	}


	@Test(dataProvider = "holidaySlotsDetails", groups = {"sanity", "smoke", "regression"})
	public void holidaySlots(Integer restId, String fromTime, String toTime, HashMap hm) {
		Processor p = sDHelper.createHolidaySlots(restId, fromTime, toTime);
		String resp = p.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(p.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals((Integer) statusCode, hm.get("statusCode"));
		Assert.assertEquals(statusMessage, hm.get("statusMessage").toString());
	}

	@Test(dataProvider = "popListingValid")
	public void popListingValidTest(String lat, String lng, int statusCode, String statusMessage, int cacheExpiryTime,
									int scrubber, String type, String imageUrl, String text, String subText, String card_cardType,
									String card_layoutAlignmentType, String card_type, String card_subType, String card_text,
									String card_subText, String card_timerData, String card_imageUrl, String card_extendedMessage,
									String card_buttonText) {
		Processor processor = popHelper.popListing(lat, lng);

		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("data.cacheExpiryTime"), cacheExpiryTime);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("data.scrubber"), scrubber);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.configs.popMeta.launchMessage.type"), type);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.configs.popMeta.launchMessage.imageUrl"),
				imageUrl);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.configs.popMeta.launchMessage.text"), text);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.configs.popMeta.launchMessage.subText"),
				subText);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].cardType"), card_cardType);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].layoutAlignmentType"),
				card_layoutAlignmentType);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].data.type"), card_type);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].data.subType"), card_subType);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].data.data.text"), card_text);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].data.data.subText"), card_subText);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].data.data.timerData"),
				card_timerData);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].data.data.imageUrl"),
				card_imageUrl);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].data.data.extendedMessage"),
				card_extendedMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data.cards[0].data.data.buttonText"),
				card_buttonText);
	}


	//	@Test(dataProvider = "validateAggregator", groups = { "sanity", "smoke", "regression" })
	void aggregatorAreaChecksTest(String lat, String lng, int openTime, int closeTime, int enabledStatus,
								  String bannerMessage, int isOpen, int timeSlotOpenHour, int timeSlotCloseHour) {
		SoftAssert softAssert = new SoftAssert();
		Processor processer = sDHelper.getAggregatorDetails(lat, lng);
		String resp = processer.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processer.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");
		String[] restaurantIds = JsonPath.read(resp, "$.data.restaurants[*].id");
		int randomId = apiHelper.getRandomNo(0, restaurantIds.length);
		String restaurantId = restaurantIds[randomId];
		String cityId = JsonPath.read(resp, "$.data.restaurants[" + randomId + "].city.cityEntity.id");
		String areaId = JsonPath.read(resp, "$.data.restaurants[" + randomId + "].area.areaEntity.id");
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].openTime")),
				cmsHelper.getCityOpenTime(cityId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].closeTime")),
				cmsHelper.getCityCloseTime(cityId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].enabled")),
				cmsHelper.getCityEnabled(cityId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].bannerMessage")),
				cmsHelper.getCityBannerMessage(cityId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].isOpen")),
				cmsHelper.getCityOpen(cityId));
		softAssert.assertEquals(
				Integer.parseInt(
						JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city.timeSlot.openHour")),
				cmsHelper.getCitySlotOpenHour(cityId));
		softAssert.assertEquals(
				Integer.parseInt(
						JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city.timeSlot.closeHour")),
				cmsHelper.getCitySlotCloseHour(cityId));

		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].openTime")),
				cmsHelper.getAreaOpenTime(areaId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].closeTime")),
				cmsHelper.getAreaCloseTime(areaId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].enabled")),
				cmsHelper.getAreaEnabled(areaId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].bannerMessage")),
				cmsHelper.getAreaBannerMessage(areaId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].isOpen")),
				cmsHelper.getAreaOpen(areaId));
		softAssert.assertEquals(
				Integer.parseInt(
						JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area.timeSlot.openHour")),
				cmsHelper.getAreaOpenTime(areaId));
		softAssert.assertEquals(
				Integer.parseInt(
						JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area.timeSlot.closeHour")),
				cmsHelper.getAreaCloseTime(areaId));
		cmsHelper.setCityOpenTime(cityId, openTime);
		cmsHelper.setCityCloseTime(cityId, closeTime);
		cmsHelper.setCityEnabled(cityId, enabledStatus);
		cmsHelper.setCityBanner(cityId, bannerMessage);
        cmsHelper.setCityOpen(cityId, isOpen);
		cmsHelper.setCityslotsOpenHour(cityId, timeSlotOpenHour);
		cmsHelper.setCityslotsCloseHour(cityId, timeSlotCloseHour);


		cmsHelper.setAreaOpenTime(areaId, openTime);
		cmsHelper.setAreaCloseTime(areaId, closeTime);
		cmsHelper.setAreaEnabled(areaId, enabledStatus);
		cmsHelper.setAreaBanner(areaId, bannerMessage);
        cmsHelper.setAreaOpen(areaId, isOpen);
		cmsHelper.setAreaslotsOpenHour(areaId, timeSlotOpenHour);
		cmsHelper.setAreaslotsCloseHour(areaId, timeSlotCloseHour);

		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].openTime")),
				cmsHelper.getCityOpenTime(cityId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].closeTime")),
				cmsHelper.getCityCloseTime(cityId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].enabled")),
				cmsHelper.getCityEnabled(cityId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].bannerMessage")),
				cmsHelper.getCityBannerMessage(cityId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city[*].isOpen")),
				cmsHelper.getCityOpen(cityId));
		softAssert.assertEquals(
				Integer.parseInt(
						JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city.timeSlot.openHour")),
				cmsHelper.getCitySlotOpenHour(cityId));
		softAssert.assertEquals(
				Integer.parseInt(
						JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].city.timeSlot.closeHour")),
				cmsHelper.getCitySlotCloseHour(cityId));

		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].openTime")),
				cmsHelper.getAreaOpenTime(areaId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].closeTime")),
				cmsHelper.getAreaCloseTime(areaId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].enabled")),
				cmsHelper.getAreaEnabled(areaId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].bannerMessage")),
				cmsHelper.getAreaBannerMessage(areaId));
		softAssert.assertEquals(
				Integer.parseInt(JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area[*].isOpen")),
				cmsHelper.getAreaOpen(areaId));
		softAssert.assertEquals(
				Integer.parseInt(
						JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area.timeSlot.openHour")),
				cmsHelper.getAreaOpenTime(areaId));
		softAssert.assertEquals(
				Integer.parseInt(
						JsonPath.read(resp, "$.data.restaurants[" + restaurantId + "].area.timeSlot.closeHour")),
				cmsHelper.getAreaCloseTime(areaId));

		softAssert.assertAll();
	}


	@Test(dataProvider = "getLatLong", groups = {"schemaValidator"})
	void aggregatorSchemaValidatorTest(String lat, String lng) throws IOException, ProcessingException {
		Processor processer = sDHelper.getPopAggregatorDetails(lat, lng);
		String resp = processer.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/popaggregator.txt");
        System.out.println("jsonschema   $$$$" + jsonschema);
		List missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes=>" + isEmpty);
		Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
	}

	@Test(dataProvider = "setpassword", priority = -100)
	public void verifySetPassword(String mobile, String password, String newPassword) {
		SoftAssert s = new SoftAssert();
		Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
		String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		Processor passwordSetResponse = SnDHelper.setPassword(newPassword, tid, token);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValueAsInt("statusCode"),
				IStatusConstants.STATUTSCODE_SUCCESS);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValue("statusMessage"),
				IStatusConstants.STATUSMESSAGE_SUCCESS);
		loginResponse = SnDHelper.consumerLogin(CheckoutConstants.mobile, newPassword);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValueAsInt("statusCode"),
				IStatusConstants.STATUTSCODE_SUCCESS);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValue("statusMessage"),
				IStatusConstants.STATUSMESSAGE_SUCCESS);
		tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		passwordSetResponse = SnDHelper.setPassword(CheckoutConstants.password, tid, token);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValueAsInt("statusCode"),
				IStatusConstants.STATUTSCODE_SUCCESS);
		s.assertEquals(passwordSetResponse.ResponseValidator.GetNodeValue("statusMessage"),
				IStatusConstants.STATUSMESSAGE_SUCCESS);
	}


	/**
	 * Shaswat Start
	 * *
	 *
	 * @throws IOException
	 * @throws ProcessingException **************************************************************************
	 */

	@Test(dataProvider = "searchV2SmokeTestData", description = "Smoke test for SearchV2 API at restaurant level")
	public void searchV2SmokeTest(String lat, String lng, String searchKeyword, int statusCode, String statusMessage) throws IOException, ProcessingException {
		Processor processor = helper.searchV2(lat, lng, searchKeyword);
		helper.smokeCheck(statusCode, statusMessage, processor);
	}

	@Test(dataProvider = "searchV2RestaurantLevel", description = "Valid test for SearchV2 API at restaurant level")
	public void searchV2RestaurantLevel(String lat, String lng, String searchKeyword) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor solrUpdate = helper.solrUpdate(restId);
		Processor solrRead = helper.solrRead(restId, SANDConstants.json_contenttype);
		Assert.assertTrue(solrRead.ResponseValidator.DoesNodeExists("$.response.docs", "Doc is empty"));
		Processor processor = helper.searchV2(lat, lng, searchKeyword);
		String name = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.restaurants..restaurants..name").replace("[", "").replace("]", "").replace("\"", "");
		Assert.assertTrue(name.contains(searchKeyword), "Restaurant name does not exist");
		}

	@Test(dataProvider = "searchV2CuisineLevel", description = "Valid test for SearchV2 API at cuisine level")
	public void searchV2CuisineLevelValidTest(String lat, String lng, String searchKeyword) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor solrUpdate = helper.solrUpdate(restId);
		Processor solrRead = helper.solrRead(restId, SANDConstants.json_contenttype);
		Assert.assertTrue(solrRead.ResponseValidator.DoesNodeExists("$.response.docs", "Doc is empty"));
		Processor processor = helper.searchV2(lat, lng, searchKeyword);
		String cuisine = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..restaurants..cuisine").replace("[", "").replace("]", "").replace("\"", "");
		System.out.println(cuisine);
		Assert.assertTrue(cuisine.contains(searchKeyword), "Cuisine does not exist");
	}

	@Test(dataProvider = "searchV2ItemLevel", description = "Valid test for SearchV2 API at item level")
	public void searchV2ItemLevelValidTest(String lat, String lng, String searchKeyword, String page) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor solrUpdate = helper.solrUpdate(restId);
		Processor solrRead = helper.solrRead(restId, SANDConstants.json_contenttype);
		Assert.assertTrue(solrRead.ResponseValidator.DoesNodeExists("$.response.docs", "Doc is empty"));
		Processor processor = helper.searchV2(lat, lng, searchKeyword, page);
		String name = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.restaurants..restaurants..menuItems[0].name").replace("[", "").replace("]", "").replace("\"", "");
		Assert.assertTrue(name.contains(searchKeyword), "Item does not exist");
		Assert.assertTrue(name.contains(searchKeyword), "Item does not exist");
	}

	@Test(dataProvider = "searchV2EmptyParamCombinationTestData", description = "Test SearchV2 API with combination of empty parameter")
	public void searchV2EmptyParamCombinationTest(String lat, String lng, String searchKeyword, String page, int statusCode, String[] message) {
		Processor processor = helper.searchV2(lat, lng, searchKeyword, page);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), message[0]);
		for (int i = 1; i < message.length; i++) {
			System.out.println("Expected: " + message[i] + "== Actual: " + processor.ResponseValidator.GetNodeValue("data[" + (i - 1) + "]"));
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("data[" + (i - 1) + "]"), message[i]);
		}
	}

	@Test(dataProvider = "searchV2InvalidRestaurant", description = "Test SearchV2 API with invalid search key at restaurant")
	public void searchV2InvalidSearchkeyTest(String lat, String lng, String searchKeyword) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor solrUpdate = helper.solrUpdate(restId);
		Processor solrRead = helper.solrRead(restId, SANDConstants.json_contenttype);
		Assert.assertTrue(solrRead.ResponseValidator.DoesNodeExists("$.response.docs", "Doc is empty"));
		Processor processor = helper.searchV2(lat, lng, searchKeyword);
		String name = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.restaurants..restaurants..name").replace("[", "").replace("]", "").replace("\"", "");
		Assert.assertFalse(name.contains(searchKeyword), "Restaurant name exist");
	}

	@Test(dataProvider = "searchV2InvalidCuisine", description = "Invalid test for SearchV2 API at cuisine level")
	public void searchV2CuisineLevelInValidTest(String lat, String lng, String searchKeyword) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor solrUpdate = helper.solrUpdate(restId);
		Processor solrRead = helper.solrRead(restId, SANDConstants.json_contenttype);
		Assert.assertTrue(solrRead.ResponseValidator.DoesNodeExists("$.response.docs", "Doc is empty"));
		Processor processor = helper.searchV2(lat, lng, searchKeyword);
		String cuisine = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..restaurants..cuisine").replace("[", "").replace("]", "").replace("\"", "");
		Assert.assertFalse(cuisine.contains(searchKeyword), "Cuisine exists");
	}

	@Test(dataProvider = "searchV2InvalidItem", description = "Test SearchV2 API with invalid search key at item level")
	public void searchV2InvalidSearchkeyItemLevelTest(String lat, String lng, String searchKeyword, String page) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor solrUpdate = helper.solrUpdate(restId);
		Processor solrRead = helper.solrRead(restId, SANDConstants.json_contenttype);
		Assert.assertTrue(solrRead.ResponseValidator.DoesNodeExists("$.response.docs", "Doc is empty"));
		Processor processor = helper.searchV2(lat, lng, searchKeyword, page);
		String name = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.restaurants..restaurants..menuItems[0].name").replace("[", "").replace("]", "").replace("\"", "");
		Assert.assertFalse(name.contains(searchKeyword), "Item exists");
	}

	@Test(dataProvider = "SearchV2FreeDeliverAtRestaurantLevel", description = "Verify free delivery in restaurant level")
	public void searchV2FreeDeliveryAtRestaurantLevel(String searchKeyword, String lat, String lng, String minCartAmount, boolean firstorderRestriction, boolean restaurantFirstOrder, boolean userRestriction, String dormant_user_type, boolean is_surge) throws IOException {
		List<String> l = orderPlace.Aggregator(new String[]{lat, lng}, null, null);
		String re = helper.menuV4RestId(l.get(0), lat, lng).ResponseValidator.GetBodyAsText();
		String name = helper.JsonString(re, "$.data.name");
		String restId = l.get(0);
		HashMap<String, String> response = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, firstorderRestriction, restaurantFirstOrder, userRestriction, dormant_user_type, is_surge);
		String searchResponse = helper.SearchV2(name, lat, lng).ResponseValidator.GetBodyAsText();
		Assert.assertTrue(helper.JsonString(searchResponse, "$.data..tradeCampaignHeaders..discountType").contains(SANDConstants.freeDel), "No free delivery on the product");
	}

	@Test(dataProvider = "SearchV2DiscountAtRestaurantLevel", description = "Verify discount at restaurant level")
	public void SearchV2DiscountAtRestaurantLevel(String lat, String lng) throws IOException {
		List<String> l = orderPlace.Aggregator(new String[]{lat, lng}, null, null);
		String re = helper.menuV4RestId(l.get(0), lat, lng).ResponseValidator.GetBodyAsText();
		String name = helper.JsonString(re, "$.data.name");
		rngHelper.createPercentageTD(l.get(0));
		String response = helper.searchV2(name, lat, lng).ResponseValidator.GetBodyAsText();
		Assert.assertTrue(helper.JsonString(response, "$.data..tradeCampaignHeaders..discountType").contains(SANDConstants.percent), "No off on the product");
	}

	@Test(dataProvider = "SearchV2FlatDiscountAtRestaurantLevel", description = "Verify if any flat discount at restaurant level")
	public void searchV2FlatDiscountAtRestaurantLevel(String minCartAmount, String flatTDAmount, String lat, String lng) throws IOException {
		List<String> l = orderPlace.Aggregator(new String[]{lat, lng}, null, null);
		String re = helper.menuV4RestId(l.get(0), lat, lng).ResponseValidator.GetBodyAsText();
		String name = helper.JsonString(re, "$.data.name");
		HashMap<String, String> response = rngHelper.createFlatWithNoMinAmountAtRestaurantLevel(minCartAmount, flatTDAmount);
		String resp = helper.searchV2(name, lat, lng).ResponseValidator.GetBodyAsText();
		Assert.assertTrue(helper.JsonString(resp, "$.data..tradeCampaignHeaders..discountType").contains(SANDConstants.flat), "No flat discount on the product");
	}

	@Test(dataProvider = "SearchV2FeebieTDAtRestaurantLevel", description = "Verify freebie at restaurant level")
	public void searchV2FeebieTdAtRestaurantLevel(String minCartAmount, String lat, String lng, String freebieItemId) throws IOException {
		List<String> l = orderPlace.Aggregator(new String[]{lat, lng}, null, null);
		String re = helper.menuV4RestId(l.get(0), lat, lng).ResponseValidator.GetBodyAsText();
		String name = helper.JsonString(re, "$.data.name");
		String restId = l.get(0);
		HashMap<String, String> response = rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, freebieItemId);
		String resp = helper.searchV2(name, lat, lng).ResponseValidator.GetBodyAsText();
		Assert.assertTrue(helper.JsonString(resp, "$.data..tradeCampaignHeaders..discountType").contains(SANDConstants.freebie), "No freebie");
	}

	/*@Test(dataProvider = "searchV2SchemaValidationTestData", groups = {"shaswat", "schemaValidation"}, description = "Schema check for SearchV2 API")
	public void searchV2SchemaValidationTest(String lat, String lng, String searchKeyword, int statusCode, String statusMessage) throws IOException, ProcessingException {
		Processor processor = helper.searchV2(lat, lng, searchKeyword);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/searchV2.txt");
		;
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
	}*/

	@Test(dataProvider = "samaySmokeTestData", groups = {"shaswat", "smoke"}, description = "Smoke test for Samay API")
	public void samaySmokeTest(String itemID, String restId, String itemType, String merged, int statusCode) throws IOException, ProcessingException {
		String[] payload = {itemID, restId, itemType, merged};
		Processor processor = helper.samay(payload);
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), statusCode);
	}

	@Test(dataProvider = "samaySchemaValidationTestData", groups = {"shaswat", "schemaValidation"}, description = "Schema check for Samay API")
	public void samaySchemaValidationTest(String itemID, String restId, String itemType, String merged) throws IOException, ProcessingException {
		String[] payload = {itemID, restId, itemType, merged};
		Processor processor = helper.samay(payload);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/samay.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
	}

	//deprecated

	/*@Test(dataProvider = "similarRestaurantSmokeTestData", groups = {"shaswat", "smoke", "jenkins"}, description = "Smoke test for Similar Restaurant API")
	public void similarRestaurantSmokeTest(String lat, String lng, String restName, int statusCode, String statusMessage) {
		String restId = String.valueOf(((BigInteger) cmsHelper.getRestaurantIdByNameExact(restName).get(0).get("id")).intValue());
		Processor processor = helper.similarRestaurants(lat, lng, restId);
		helper.smokeCheck(statusCode, statusMessage, processor);
	}

	@Test(dataProvider = "similarRestaurantServiceabilityOpenedTestData", groups = {"shaswat", "sanity", "regression", "jenkins"}, description = "Test Similar Restaurant API for searviceability and opened status")
	public void similarRestaurantServiceabilityOpenedTest(String lat, String lng, String restName, int statusCode, String statusMessage, String serviceability, boolean opened) {
		String restId = String.valueOf(((BigInteger) cmsHelper.getRestaurantIdByNameExact(restName).get(0).get("id")).intValue());
		Processor processor = helper.similarRestaurants(lat, lng, restId);
		List serviceabilityList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants..sla.serviceability");
		List openedList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants..availability.opened");
		for (int i = 0; i < serviceabilityList.size(); i++) {
			Assert.assertEquals(serviceabilityList.get(i), serviceability);
			Assert.assertEquals(openedList.get(i), opened);
		}
	}

	@Test(dataProvider = "similarRestaurantSchemaTestData", groups = {"shaswat", "schemaValidation", "jenkins"}, description = "Schema check for Similar Restaurants")
	public void similarRestaurantSchemaTest(String lat, String lng, String restName) throws IOException, ProcessingException {
		String restId = String.valueOf(((BigInteger) cmsHelper.getRestaurantIdByNameExact(restName).get(0).get("id")).intValue());
		Processor processor = helper.similarRestaurants(lat, lng, restId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/similarRestaurants.txt");
		;
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For Similar Restaurants API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
		processor.ResponseValidator.DoesNodeExists("$.data.restaurants..feeDetails.fees..message", resp);

	}

	@Test(dataProvider = "similarRestaurantsVsMenuV3TestData", groups = {"shaswat", "sanity", "regression", "jenkins"},
			description = "Test Similar Restaurant API for searviceability and opened status same as in MenuV3 API")
	public void similarRestaurantsVsMenuV3Test(String lat, String lng, String restName, String mobile, String password, String serviceability, boolean opened) throws IOException, ProcessingException {
		String restId = String.valueOf(((BigInteger) cmsHelper.getRestaurantIdByNameExact(restName).get(0).get("id")).intValue());
		Processor processor = helper.similarRestaurants(lat, lng, restId);
		System.out.println("#####" + JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants"));
		if (JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants").toString().equals("{}")) {
			Assert.fail("No Similar Restaurants found...!!");
		}
		List serviceabilityList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants..sla.serviceability");
		List openedList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants..availability.opened");
		for (int i = 0; i < serviceabilityList.size(); i++) {
			Assert.assertEquals(serviceabilityList.get(i), serviceability);
			Assert.assertEquals(openedList.get(i), opened);
		}
		Processor loginV2Processor = SnDHelper.consumerLogin(mobile, password);
		Processor menuV3Processor = sDHelper.menuV3(restId, lat, lng, loginV2Processor.ResponseValidator.GetNodeValue("tid"), loginV2Processor.ResponseValidator.GetNodeValue("$.data.token"));
		String serviceabilityListMenuV3 = menuV3Processor.ResponseValidator.GetNodeValue("$.data.sla.serviceability");
		String openedListMenuV3 = menuV3Processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.availability.opened");
		Assert.assertEquals(serviceabilityListMenuV3, serviceability, "Restaurant is non-serviceable");
		Assert.assertEquals(Boolean.parseBoolean(openedListMenuV3), opened, "Restaurant is closed");
	}*/

	@Test(dataProvider = "setFavoriteSmokeTestData", description = "Smoke test for Set Favorite API")
	public void setFavoriteSmokeTest(String mobile, String password, String markFavorite,
									 String entityType, String lat, String lng, String unmarkFavorite, int statusCode,
									 String statusMessage) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor processor = helper.setFavorite(mobile, password, new String[]{markFavorite, entityType, restId});
		Processor processorp = helper.setFavorite(mobile, password, new String[]{unmarkFavorite, entityType, restId});
		helper.smokeCheck(statusCode, statusMessage, processor);
		helper.smokeCheck(statusCode, statusMessage, processorp);
	}

	@Test(dataProvider = "setFavorite", description = "Verify if restaurant is set as favorite")
	public void setFavoriteValidTest(String mobile, String password, String markFavorite,
									 String entityType, String lat, String lng,
									 int statusCode, String statusMessage) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor loginProcessor = SnDHelper.consumerLogin(mobile, password);
		Processor processor = helper.setFavorite(mobile, password, new String[]{markFavorite, entityType, restId});
		helper.smokeCheck(statusCode, statusMessage, processor);
		Processor getFavoriteProcessor = helper.favorites(lat, lng,
				loginProcessor.ResponseValidator.GetNodeValue("data.customer_id"));
		List<String> actualRestIdList = JsonPath.read(getFavoriteProcessor.ResponseValidator.GetBodyAsText(), "$.data.restaurants.*.id");
		boolean flag = false;
		for (int i = 0; i < actualRestIdList.size(); i++) {
			if (actualRestIdList.get(i).equals(restId)) {
				flag = true;
			}
		}
		Assert.assertTrue(flag, "Restaurant ID not found in favorites");
	}

	@Test(dataProvider = "removeFavorite", description = "Verify if restaurant is removed from favorites ")
	public void removeFavoriteValidTest(String mobile, String password, String entityType, String unmarkFavourite, String lat, String lng, int statusCode, String statusMessage) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor loginProcessor = SnDHelper.consumerLogin(mobile, password);
		Processor processor = helper.setFavorite(mobile, password, new String[]{unmarkFavourite, entityType, restId});
		helper.smokeCheck(statusCode, statusMessage, processor);
		Processor getProcessor = helper.favorites(lat, lng, loginProcessor.ResponseValidator.GetNodeValue("data.customer_id"));
		List<String> actualRestIdList = JsonPath.read(getProcessor.ResponseValidator.GetBodyAsText(), "$.data.restaurants.*.id");
		boolean flag = false;
		for (int i = 0; i < actualRestIdList.size(); i++) {
			if (actualRestIdList.get(i).equals(restId)) {
				flag = true;
			}
		}
		Assert.assertFalse(flag, "Restaurant ID not removed from favorites");
	}

	@Test(dataProvider = "getFavoriteSmokeTestData", description = "Smoke test for Get Favorite API")
	public void getFavoriteSmokeTest(String lat, String lng, String mobile, String password, int statusCode, String statusMessage) {
		Processor loginProcessor = SnDHelper.consumerLogin(mobile, password);
		Processor getProcessor = helper.favorites(lat, lng, loginProcessor.ResponseValidator.GetNodeValue("data.customer_id"));
		helper.smokeCheck(statusCode, statusMessage, getProcessor);
	}

	/*@Test(dataProvider = "setFavoriteSchemaTestData", groups = {"shaswat", "schemaValidation", "jenkins"}, description = "Schema check for Set Favorite API")
	public void setFavoriteSchemaTest(String mobile, String password, String markFavorite,
									  String entityType, String restName, String unmarkFavourite) throws IOException, ProcessingException {
		String entityId = String.valueOf(((BigInteger) cmsHelper.getRestaurantIdByNameExact(restName).
				get(0).get("id")).intValue());
		Processor loginProcessor = sDHelper.consumerLogin(mobile, password);
		Processor processor = helper.setFavorite(mobile, password, new String[]{markFavorite, entityType, entityId});

		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/setFavorite.txt");
		;
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For Set Favorite API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);

		processor = helper.setFavorite(mobile, password, new String[]{unmarkFavourite, entityType, entityId});
	}

	@Test(dataProvider = "getFavoriteSchemaTestData", groups = {"shaswat", "schemaValidation", "jenkins"}, description = "Schema check for Get Favorite API")
	public void getFavoriteSchemaTest(String mobile, String password, String markFavorite,
									  String entityType, String restName, String unmarkFavourite, String lat, String lng) throws IOException, ProcessingException {
		String entityId = String.valueOf(((BigInteger) cmsHelper.getRestaurantIdByNameExact(restName).
				get(0).get("id")).intValue());
		Processor loginProcessor = sDHelper.consumerLogin(mobile, password);
		Processor setFavoriteProcessor = helper.setFavorite(mobile, password, new String[]{markFavorite, entityType, entityId});

		Processor processor = helper.favorites(lat, lng, loginProcessor.ResponseValidator.GetNodeValue("data.customer_id"));
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/getFavorite.txt");
		;
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For Get Favorite API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.restaurants.*.feeDetails.fees..message", resp), "Message Node doesn't exist");

		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".availability.nextCloseMessage", resp), "Node 'nextCloseMessage' doesn't exists");
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".availability.nextOpenTimeMessage", resp), "Node 'nextOpenTimeMessage' doesn't exists");
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".availability.nextCloseTime", resp), "Node 'nextCloseTime' doesn't exists");
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".availability.nextOpenTime", resp), "Node 'nextOpenTime' doesn't exists");
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".availability.closedReason", resp), "Node 'closedReason' doesn't exists");
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".availability.closeMessage", resp), "Node 'closeMessage' doesn't exists");

		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".area.areaEntity.bannerMessage", resp), "Node 'bannerMessage' doesn't exists");
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".parentId", resp), "Node 'parentId' doesn't exists");
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".tradeDiscountInfo", resp), "Node 'tradeDiscountInfo' doesn't exists");
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.." + entityId + ".feeDetails.icon", resp), "Node 'tradeDiscountInfo' doesn't exists");


		processor = helper.setFavorite(mobile, password, new String[]{unmarkFavourite, entityType, entityId});
	}*/

	@Test(dataProvider = "getQuickMenuSmokeTestData", groups = {"shaswat", "smoke", "jenkins"}, description = "Smoke test for Quick Menu API")
	public void getQuickMenuSmokeTest(String restName, String categories, int statusCode, String statusMessage) {
		String restId = String.valueOf(((BigInteger) cmsHelper.getRestaurantIdByNameExact(restName).
				get(0).get("id")).intValue());
		Processor processor = helper.quickMenu(restId, categories);
		helper.smokeCheck(statusCode, statusMessage, processor);
	}

	/*@Test(dataProvider = "getQuickMenuSchemaTestData", groups = {"shaswat", "schemaValidation", "jenkins"}, description = "Schema check for Quick Menu API")
	public void getQuickMenuSchemaTest(String restName, String categories) throws IOException, ProcessingException {
		String restId = String.valueOf(((BigInteger) cmsHelper.getRestaurantIdByNameExact(restName).
				get(0).get("id")).intValue());
		Processor processor = helper.quickMenu(restId, categories);

		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/quickmenu.txt");
		;
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For QuickMenu API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
	}*/

	@Test(dataProvider = "quickMenuVsMenuV4", description = "Checks that the item are enabled and in stock")
	public void quickMenu(String categories, String lat, String lng) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor qmProcessor = helper.quickMenu(restId, categories);
		List<String> enabled = Arrays.asList(JsonPath.read(qmProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.items..enabled").toString().replace("[", "").replace("]", "").split(","));
		List<String> enabledList = new ArrayList<>();
		List<String> inStock = Arrays.asList(JsonPath.read(qmProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.items..inStock").toString().replace("[", "").replace("]", "").split(","));
		List<String> inStockList = new ArrayList<>();
		for (int i = 0; i < enabled.size(); i++) {
			if (enabled.get(i).equals("1")) {
				enabledList.add(enabled.get(i));
				Assert.assertEquals(enabledList.get(i), "1", "not enabled");
			}
			if (inStock.get(i).equals("1")) {
				inStockList.add(inStock.get(i));
				Assert.assertEquals(inStockList.get(i), "1", "out of stock");
			}
		}
	}

	@Test(dataProvider = "quickMenuVsMenuV4",description = "Checks that the item ids are same in in Quick Menu and MenuV4 response")
	public void quickMenuVsMenuV4ItemId( String categories, String lat, String lng) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor qmProcessor = helper.quickMenu(restId, categories);
		Processor mProcessor = SnDHelper.menuV4RestId(restId, lat, lng);
		//Check for Presence of ITEM ID
		List<Integer> qmItemID = JsonPath.read(qmProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.items[*].id");
		List<Integer> mItemID = JsonPath.read(mProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.items[*].id");
		for (int i = 0; i < qmItemID.size(); i++) {
			Assert.assertEquals(qmItemID.get(i).intValue(), mItemID.get(i).intValue(), "Item ID mismatch");
		}
	}

/*	@Test(dataProvider = "quickMenuVsMenuV4", description = "Checks that collections is same in Quick Menu and MenuV3 response")
	public void quickMenuVsMenuV4Colections(String categories, String lat, String lng) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		Processor qmProcessor = helper.quickMenu(restId, categories);
		Processor mProcessor = SnDHelper.menuV3RestId(restId);
		//Check position of names in collection are same for Quick Menu and MenuV3
		JSONArray quickMenuCollections = JsonPath.read(qmProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.collections..name");
		JSONArray menuV3Collections = JsonPath.read(mProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.collections..name");
		try {
			JSONAssert.assertEquals(quickMenuCollections.toJSONString(), menuV3Collections.toJSONString(), true);
		} catch (JSONException e) {
			e.printStackTrace();
			Assert.fail("Collections are not same for Quick Menu and and Menu V4 response..!!");
		}
		//Check position of names in sub-collection are same for Quick Menu and MenuV3
		JSONArray quickMenuSubCollections = JsonPath.read(qmProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.collections..subCollections..name");
		JSONArray menuV3SubCollections = JsonPath.read(mProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.collections..subCollections..name");
		try {
			JSONAssert.assertEquals(quickMenuSubCollections.toJSONString(), menuV3SubCollections.toJSONString(), true);
		} catch (JSONException e) {
			e.printStackTrace();
			Assert.fail("Sub Collections are not same for Quick Menu and and Menu V3 response..!!");
		}
		//Check Item-Views of Collection
		List<String> quickMenuItemIds = JsonPath.read(qmProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.collections[*].itemViews[*].id");
		List<String> menuV3ItemIds = JsonPath.read(mProcessor.ResponseValidator.GetBodyAsText(), "$.data.menu.collections[*].itemViews[*].id");
		for (int i = 0; i < quickMenuItemIds.size(); i++) {
			boolean flag = false;
			for (int j = 0; j < menuV3ItemIds.size(); j++) {
				if (quickMenuItemIds.get(i).equals(menuV3ItemIds.get(j))) {
					flag = true;
				}
			}
			if (!flag) {
				Assert.fail("Quick Menu Item ID = " + quickMenuItemIds.get(i) + " not found in Menu V3");
			}
		}
	}*/

	/*@Test(groups = {"shaswat", "sanity", "regression", "jenkins"}, description = "Checks for Exclusive Restaurant")
	public void listingTagExclusiveRestaurantTest() {
		Assert.assertTrue((helper.listingTagExclusiveRestaurant()).contains("EXCLUSIVE"));
	}

	@Test(groups = {"shaswat", "sanity", "regression", "jenkins"}, description = "Checks for Non Exclusive Restaurant")
	public void listingTagNonExclusiveRestaurantTest() {
		Assert.assertFalse((helper.listingTagNonExclusiveRestaurant()).contains("EXCLUSIVE"));
	}

	@Test(groups = {"shaswat", "sanity", "regression", "jenkins"}, description = "Checks for New Arrival Restaurant")
	public void listingTagNewArrivalRestaurantTest() {
		Assert.assertTrue(helper.listingTagNewArrivalRestaurant().contains("NEW"));
	}

	@Test(groups = {"shaswat", "sanity", "regression", "jenkins"}, description = "Checks for New Arrival Restaurant")
	public void listingTagNonNewArrivalRestaurantTest() {
		Assert.assertFalse(helper.listingTagNonNewArrivalRestaurant().contains("NEW"));
	}*/

	@Test(dataProvider = "presentationControllerSmokeTestData", groups = {"shaswat", "smoke"}, description = "Smoke check for Presentation Controller")
	public void presentationControllerSmokeTest(String mobile, String password, String[] payload, int statusCode, String statusMessage) {
		Processor processor = helper.presentationController(mobile, password, payload);
		helper.smokeCheck(statusCode, statusMessage, processor);
	}

	@Test(dataProvider = "presentationControllerValidTestData", groups = {"shaswat", "sanity", "regression"}, description = "Valid test for Presentation Controller")
	public void presentationControllerValidTest(String mobile, String password, String[] payload, int cityId) {
		Processor processor = helper.presentationController(mobile, password, payload);
		List<String> polygonList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.polygons");
		Assert.assertTrue(polygonList.size() > 0, "No Polygon found or Polygon count is not greater than zero");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("$.data.cityId"), cityId, "City ID doesn't match");
	}

	@Test(dataProvider = "presentationControllerWithNoPolygonTestData", groups = {"shaswat", "regression"}, description = "No polygon with default city check for Presentation Controller")
	public void presentationControllerWithNoPolygonTest(String mobile, String password, String[] payload, int cityId) {
		Processor processor = helper.presentationController(mobile, password, payload);
		List<String> polygonList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.polygons");
		Assert.assertTrue(polygonList.isEmpty(), "Polygon found or Polygon count is greater than zero");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("$.data.cityId"), cityId, "City ID is not default or not equals to '0'");
	}

	@Test(dataProvider = "presentationControllerSchemaTestData", groups = {"shaswat", "schemaValidation"}, description = "Schema check for Presentation Controller")
	public void presentationControllerSchemaTest(String mobile, String password, String[] payload) throws IOException, ProcessingException {
		Processor processor = helper.presentationController(mobile, password, payload);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/quickmenu.txt");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For Presentation Controller API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
	}

	/*@Test(dataProvider = "listingTagPromotedRestaurantValidTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Checks for Promoted Restaurant")
	public void listingTagPromotedRestaurantValidTest(int cityId, boolean isPromoted) {
		String[] resp = helper.getListingTagPromotedRestaurant(cityId, isPromoted);
		String dynamicTags = resp[0];
		Assert.assertTrue(dynamicTags.contains("PROMOTED"), "Restaurant '" + resp[1] + "' is not PROMOTED");
	}

	@Test(dataProvider = "listingTagNonPromotedRestaurantTestData", groups = {"shaswat", "sanity", "regression"}, description = "Checks for Non Promoted Restaurant")
	public void listingTagNonPromotedRestaurantTest(int cityId, boolean isPromoted) {
		String[] resp = helper.getListingTagPromotedRestaurant(cityId, isPromoted);
		String dynamicTags = resp[0];
		Assert.assertFalse(dynamicTags.contains("PROMOTED"), "Restaurant '" + resp[1] + "' is PROMOTED");
	}

	@Test(dataProvider = "listingTagPromotedRestaurantPositionTestData", groups = {"shaswat", "smoke", "sanity", "regression"},
			description = "Checks for Promoted Restaurant")
	public void listingTagPromotedRestaurantPositionTest(int cityId, boolean isPromoted) {
		String[] listingTagPositionWithRestId = helper.listingTagPosition(cityId, isPromoted);
		String actualRest = listingTagPositionWithRestId[0];
		String expectedRest = listingTagPositionWithRestId[1];
		String position = listingTagPositionWithRestId[2];
		Assert.assertEquals(actualRest, expectedRest, "Expected restaurant '" + expectedRest + "', "
				+ "but found restaurant '" + actualRest + " at position '" + position + "'");
	}*/

	@Test(dataProvider = "crossSellingSmokeTestData", groups = {"shaswat", "smoke"}, description = "Smoke test for 'Cross Selling' API")
	public void crossSellingSmokeTest(String itemId, String quantity, String globalCategory,
									  String globalSubCategory, String restId, int statusCode, String statusMessage) {
		String[][] items = new String[][]{
				{itemId, quantity, globalCategory, globalSubCategory}};
		String[] payload = new String[]{helper.getJsonArrayForCrossSelling(items).toString(), restId};
		Processor processor = helper.crossSelling(payload);
		helper.smokeCheck(statusCode, statusMessage, processor);
	}

/*	@Test(dataProvider = "crossSellingSchemaTestData", groups = {"shaswat", "schemaValidation"}, description = "Schema test for 'Cross Selling' API")
	public void crossSellingSchemaTest(String itemId, String quantity, String globalCategory, String globalSubCategory, String restId)
			throws IOException, ProcessingException {
		String[][] items = new String[][]{
				{itemId, quantity, globalCategory, globalSubCategory}};
		String[] payload = new String[]{helper.getJsonArrayForCrossSelling(items).toString(), restId};
		Processor processor = helper.crossSelling(payload);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/crossSelling.txt");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For Cross Selling API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
	}*/

	@Test(dataProvider = "crossSellingValidTestData", description = "Valid test for 'Cross Selling' API")
	public void crossSellingValidTest(String itemId, String quantity, String globalCategory, String globalSubCategory, String restId, String lat, String lng) {


		//String getItemTag =  JsonPath.read(redisInfo4, "category");
		//List<HashMap<String, String>> crossSellingRestaurantCategory = JsonPath.read(redisInfo1, "$.[" + getItemTag + "][*]");

		//List<HashMap<String, String>> restCatList = crossSellingRestaurantCategory;
		//Set<String> itemIdSet = new HashSet<>();*/


		String CROSS_SELLING_RESTAURANT_ITEM = helper.analytics(SANDConstants.analytics, SANDConstants.analyticsDb, "CROSS_SELLING_RESTAURANT_ITEM_223");

		String CROSS_SELLING_RESTAURANT_CATEGORY = helper.analytics(SANDConstants.analytics, SANDConstants.analyticsDb, "CROSS_SELLING_RESTAURANT_CATEGORY_223");

		String CROSS_SELLING_RULES_GLOBAL_CATEGORY = helper.analytics(SANDConstants.analytics, SANDConstants.analyticsDb, "CROSS_SELLING_RULES_GLOBAL_CATEGORY");

		String ITEM_DETAILS = helper.analytics(SANDConstants.analytics, 0, "ITEM_DETAILS_545224");
		System.out.println(ITEM_DETAILS);
		String SC_ITEM_TAGS = helper.analytics(SANDConstants.analytics, 0, "SC_ITEM_TAGS_545224");


		//String SC_Menu_ITEM_TAGS= helper.analytics(SANDConstants.analytics, 0, "SC_ITEM_TAGS_"+itemId);

		Processor response = helper.menuV4RestId(restId,lat,lng);
		// System.out.println(response+"menuresponse");

		String SC_Menu_ITEM_TAGS = helper.analytics(SANDConstants.analytics, 0, "SC_ITEM_TAGS_" + itemId);
		String category = helper.JsonString(SC_Menu_ITEM_TAGS, "$.category");

		//System.out.println("$."+category+".[?(@.'score'== 8)].name");
		String path = "$.['" + category + "'].[?(@.'score'== 8)].name";
		String crossSellingRestCategory = helper.JsonString(CROSS_SELLING_RESTAURANT_CATEGORY, path);


		List<String> crossSellingItem = Arrays.asList(helper.JsonString(CROSS_SELLING_RESTAURANT_ITEM, "$." + crossSellingRestCategory + ".[?(@.score )].id").split(","));
		for (int i = crossSellingItem.size(); i >= 5; i--) {
			String ITEM_DETAILS_Selling = helper.analytics(SANDConstants.analytics, 0, "ITEM_DETAILS_" + crossSellingItem.get(i));
			String veg = helper.JsonString(ITEM_DETAILS_Selling, "$.is_veg");
		}

		String menucategory = helper.JsonString(response.ResponseValidator.GetBodyAsText(), "$.data.menu.items.545428.category");
		String veg = helper.JsonString(response.ResponseValidator.GetBodyAsText(), "$.data.menu.items.545428.isVeg");

		//String getItemTag =  JsonPath.read(SC_ITEM_TAGS, "$.category");
		//List<HashMap<String, String>> crossSellingRestaurantCategory = JsonPath.read(CROSS_SELLING_RESTAURANT_CATEGORY, "$.[" + getItemTag+"545224" + "]");
		//String s = helper.JsonString(CROSS_SELLING_RESTAURANT_ITEM,"$...starters - salads..id");
		String[][] items = new String[][]{
				{itemId, quantity, globalCategory, globalSubCategory}
		};
		String[] payload = new String[]{helper.getJsonArrayForCrossSelling(items).toString(), restId};
		Processor processor = helper.crossSelling(payload);
		List<HashMap<String, Integer>> list = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.items[*]");

		List<Integer> itemIdScore = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.items[*].score");
		for (int i = 0; i < itemIdScore.size() - 1; i++) {
			if (itemIdScore.get(i).intValue() < itemIdScore.get(i + 1).intValue()) {
				Assert.fail("Item Score is not in descending order. " + itemIdScore.get(i).intValue() + " < " + itemIdScore.get(i + 1).intValue());
			}
		}
	}


	@Test(dataProvider = "crossSellingAddItemFromResponseTestData", description = "Valid test for 'Cross Selling' API")
	public void crossSellingAddItemFromResponseTest(String itemId, String quantity, String globalCategory, String globalSubCategory, String restId) {
		String[][] items = new String[][]{
				{itemId, quantity, globalCategory, globalSubCategory}};

		//HashMap<String, String> sid = checkoutHelper.TokenData(SANDConstants.consumerSessionTstMobile, SANDConstants.password);
		String[] payload = new String[]{helper.getJsonArrayForCrossSelling(items).toString(), restId};

		Processor processor = helper.crossSelling(payload);
		List<Integer> itemIdListResponse = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.items[*].id");
		if (itemIdListResponse.size() == 0) {
			Assert.assertNotNull(itemIdListResponse, "Items list is not empty..!!!");
		}
		String selectedItemId = String.valueOf(itemIdListResponse.get(0).intValue());
		items = new String[][]{
				{itemId, quantity, globalCategory, globalSubCategory},
				{selectedItemId, quantity, globalCategory, globalSubCategory}
		};
		payload = new String[]{helper.getJsonArrayForCrossSelling(items).toString(), restId};
		processor = helper.crossSelling(payload);
		String emptyItems = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.items").toString();
		itemIdListResponse = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.items[*].id");
		if (itemIdListResponse.size() == 0) {
			Assert.fail("Items list is empty..!!!");
		}
		boolean flag = false;
		for (int i = 0; i < itemIdListResponse.size(); i++) {
			if (itemIdListResponse.get(i).intValue() == Integer.parseInt(selectedItemId)) {
				flag = true;
				break;
			}
		}
		Assert.assertFalse(flag, "Item ID found in response. Item ID = " + selectedItemId);
	}

	@Test(dataProvider = "crossSellingAvailibilityOfItemsTestData", description = "Valid test for availibility of items displayed in 'Cross Selling' API")
	public void crossSellingAvailibilityOfItemsTest(String itemId, String quantity, String globalCategory, String globalSubCategory, String restId) {
		String[][] items = new String[][]{{itemId, quantity, globalCategory, globalSubCategory}};
		//HashMap<String, String> sid = checkoutHelper.TokenData(SANDConstants.consumerSessionTstMobile, SANDConstants.password);
		String[] payload = new String[]{helper.getJsonArrayForCrossSelling(items).toString(), restId};
		Processor processor = helper.crossSelling(payload);
		List<Integer> itemIdListResponse = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.items[*].id");
		if (itemIdListResponse.size() == 0) {
			Assert.fail("Items list is empty..!!!");
		}
		Processor menuV3Processor = SnDHelper.menuV3RestId(restId);
		int itemIdAvail = 0;
		for (int i = 0; i < itemIdListResponse.size(); i++) {
			itemIdAvail = menuV3Processor.ResponseValidator.GetNodeValueAsInt("$.data.menu.items." + itemIdListResponse.get(i).intValue() + ".inStock");
			if (itemIdAvail == 0) {
				Assert.fail("Cross Selling Item is not available. Item Id:" + itemIdListResponse.get(i).intValue());
			}
		}
	}


	/**
	 * Shaswat End
	 * #######################################################################
	 */

	/**
	 * Srishty Start
	 * ****************************************************************************
	 */

	//location based cafe deprecated
	/*@Test(dataProvider = "locationBasedCafeEnabled", groups = {"sanity", "regression", "srishty"}, description = "Verify response with only cafe feature for LocationBasedFeature is Enabled")
	public void locationFeatureCafeEnabled(String lat, String lng, String feature) {
		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		boolean isCafe = processor.ResponseValidator.DoesNodeExists("$.data.SWIGGY_CAFE", null);
		Assert.assertEquals(isCafe, true);
		String available = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.SWIGGY_CAFE.available");
		Boolean b = Boolean.parseBoolean(available);
		Assert.assertEquals(b.booleanValue(), true);
	}

	@Test(dataProvider = "locationBasedCafeDisabled", groups = {"sanity", "regression", "srishty"}, description = "Verify response with only cafe feature for LocationBasedFeature is disabled")
	public void locationFeatureCafeDisabled(String lat, String lng, String feature) {

		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		boolean isCafe = processor.ResponseValidator.DoesNodeExists("$.data.SWIGGY_CAFE", null);
		Assert.assertEquals(isCafe, true);
		String available = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.SWIGGY_CAFE.available");
		Boolean b = Boolean.parseBoolean(available);
		Assert.assertEquals(b.booleanValue(), false);

	}*/

	@Test(dataProvider = "locationBasedPOPEnabled", description = " Verify the response for area where POP is enabled for LocationBasedFeature")
	public void locationFeaturePOPEnabled(String lat, String lng, String feature) {
		//String popLatLng = deliveryDataHelper.getPopLatLongFromZone(10);
		//System.out.println(popLatLng);
		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		String available = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.SWIGGY_POP.available");
		Boolean b = Boolean.parseBoolean(available);
		Assert.assertEquals(b.booleanValue(), true);

	}

/*	@Test(dataProvider = "locationBasedAllEnabled", groups = {"sanity", "regression", "srishty"}, description = " Verify the response for area where both POP and SWIGGY ASSURED is enabled for LocationBasedFeature")
	public void locationFeatureAllEnabled(String lat, String lng, String feature) {
		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		boolean available = processor.ResponseValidator.DoesNodeExists("$.data.SWIGGY_POP.available");
		String helptext = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.SWIGGY_ASSURED.swiggy_assured_help_text");
		Assert.assertTrue(available, "Node is not available");
	}


	@Test(dataProvider = "locationBasedAllDisabled", groups = {"sanity", "regression", "srishty"}, description = " Verify the response for area where both POP and SWIGGY ASSURED is disabled for LocationBasedFeature")
	public void locationFeatureAllDisabled(String lat, String lng, String feature) {

		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		String available = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.SWIGGY_POP.available");
		Boolean b = Boolean.parseBoolean(available);
		String helptext = processor.ResponseValidator.GetNodeValue("$.data.SWIGGY_ASSURED.swiggy_assured_help_text");
		Assert.assertEquals(b.booleanValue(), false);
		Assert.assertEquals(helptext, null);

	}*/

	/*@Test(dataProvider = "locationBasedAllEnabled", groups = {"schemaValidation", "srishty"}, description = "Schema Validation for Location Based Feature")
	public void locationBasedfeatureSchemaValidate(String lat, String lng, String feature) throws IOException, ProcessingException {

		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/locationBasedFeature.txt");
		;
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		System.out.println("missingNodeList===" + missingNodeList.size());
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For Location based Feature API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
	}*/

	/*@Test(dataProvider = "launch", groups = {"smoke", "sanity", "regression", "schemaValidation", "srishty"}, description = "Verify the smoke, schema validation, tracking details, wp_options and a/b experiments data for launch API")
	public void launchTest(String experiment, String mobile, String password, String itemId, String restId, String quantity) throws Exception {

		/*
		 * Launch Smoke Test
		 */
		/*helper.launch(experiment, mobile, password, itemId, "223", quantity);
		Processor processor = helper.launch(experiment, mobile, password, itemId, "223", quantity);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");

		/*
		 * Json Schema Validation
		 */

		/*String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/launch.txt");
		;
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		System.out.println("missingNodeList===" + missingNodeList.size());
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For Launch API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");

		/*
		 * Checking Tracking details
		 */

		/*List orderIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.track.orders..order_id");
		System.out.println(orderIds+"orderid");
		//Assert.assertEquals(orderIds.get(orderIds.size() - 1).toString(), helper.orderId);


		List trackable = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), ("$.data.track.orders..trackable"));
		for (int i = 0; i < trackable.size(); i++) {
			//	System.out.println("Trackable: " + trackable.get(i).toString());
			Assert.assertEquals(trackable.get(i).toString(), "1");
		}

		/*
		 * Cancelling the Order after the tests
		 */

		/*String reason = "Test-Order-SAND-LaunchAPI";
		String feeApplicability = "false";
		String Cancellation_Fee = "0";
		//for(int i=0; i<orderIds.size(); i++){
		checkoutHelper.OrderCancel(helper.tid, helper.token, CheckoutConstants.authorization, reason, feeApplicability, Cancellation_Fee, CheckoutConstants.cancellationmessage, helper.orderId);
		System.out.println("Cancelled Test-Order-------" + orderIds.get(orderIds.size() - 1).toString());
		//}*/

	//}



	/*@Test(dataProvider = "settings", groups = {"schemaValidation", "srishty"}, description = "Verify schema check for settings API")
	public void settingsJSONSchemavalidation(String opt1, String val1, String opt2, String val2) throws IOException, ProcessingException {

		Processor processor = helper.getSettings(opt1, val1, opt2, val2);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/settings.txt");
		;
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		System.out.println("missingNodeList===" + missingNodeList.size());
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For Settings API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
	}*/

	/*@Test(dataProvider = "areaListingSchema", groups = {"schemaValidation"})
	public void areaListingJSONSchemavalidation(String area) throws IOException, ProcessingException {

		Processor processor = helper.getAreaListing(area);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/areaListing.txt");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		System.out.println("missingNodeList===" + missingNodeList.size());
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For Area_Listing API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");

	}*/

	@Test(dataProvider = "menuTags", groups = {"srishty", "smoke", "sanity", "regression"}, description = "verify menu tags for bestseller and Must Try")
	public void verifyMenuTags(String restId) {

		String ribbonData = null;
		SoftAssert sa = new SoftAssert();
		Processor menuResponse = helper.menuV3(restId);
		List menuItemIds = JsonPath.read(menuResponse.ResponseValidator.GetBodyAsText(), "$.data.menu.items[*].id");

		LocalDateTime date1 = LocalDateTime.now();
		int time = helper.getTime(date1);
		int slotId = helper.getSlotIdforItem(time);

		String keyInitial = "DATA_RESTAURANT_INFO_";
		String entity = restId;
		int redisdb = 9;
		String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);


		HashMap<String, String> mustTry = JsonPath.read(redisInfo, "$.item_ratings");
		List<String> itemIdsMustTry = new ArrayList<String>(mustTry.keySet());
		boolean flag = false;
		for (int j = 0; j < itemIdsMustTry.size(); j++) {
			boolean itemMatched = menuItemIds.contains(Integer.parseInt(itemIdsMustTry.get(j).trim().toString()));
			System.out.println(itemMatched + itemIdsMustTry.get(j).trim().toString());
			if (itemMatched == true) {
				flag = true;
				System.out.println(itemMatched);
				String itemIdMatched = itemIdsMustTry.get(j);
				System.out.println("ItemId matched :" + itemIdMatched);
				ribbonData = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..menu.items." + itemIdMatched + ".ribbon.text");
				Assert.assertEquals(ribbonData, "[\"MUST TRY\"]");
				break;
			}
		}

		if (!flag) {
			Assert.fail(" Must Try assertion failed");
		}

		HashMap<String, String> bestseller = JsonPath.read(redisInfo, "$.most_ordered." + slotId);
		List<String> itemIdsbestseller = new ArrayList<String>(bestseller.keySet());
		flag = false;
		for (int j = 0; j < itemIdsbestseller.size(); j++) {
			boolean itemMatched = menuItemIds.contains(Integer.parseInt(itemIdsbestseller.get(j).trim().toString()));
			System.out.println(itemMatched + itemIdsbestseller.get(j).trim().toString());
			if (itemMatched == true) {
				flag = true;
				System.out.println(itemMatched);
				String itemIdMatched = itemIdsbestseller.get(j);
				System.out.println("ItemId matched :" + itemIdMatched);
				ribbonData = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..menu.items." + itemIdMatched + ".ribbon.text");
				Assert.assertEquals(ribbonData, "[\"BESTSELLER\"]");
				break;
			}
			if (!flag) {
				Assert.fail(" Bestseller assertion failed");
			}
		}

	}

	@Test(dataProvider = "cafeListing", groups = {"schemaValidation", "srishty", "cafe"}, description = "Verify schema check for cafelisting API")
	public void cafeListingJSONSchemavalidation(String corporateId, String passcode, String password, String mobile) throws IOException, ProcessingException {

		Processor processor = cafeHelper.getCafeListing(corporateId, passcode, password, mobile);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/getCafeListing.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		System.out.println("missingNodeList===" + missingNodeList.size());
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For cafe-listing API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
	}

	@Test(dataProvider = "CorporateByLatLong", groups = {"schemaValidation", "srishty", "cafe"}, description = "Verify schema check for corporate Listing API")
	public void corporateByLatLongJSONSchemavalidation(String mobile, String password, String lat, String lng) throws IOException, ProcessingException {

		Processor processor = cafeHelper.getCorporateByLatLong(mobile, password, lat, lng);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/getCorporatesByLatLong.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		System.out.println("missingNodeList===" + missingNodeList.size());
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For corporate Listing API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
	}

	@Test(dataProvider = "authenticateCorporate", groups = {"schemaValidation", "srishty", "cafe"}, description = "Verify schema check for authenticate Corporate API")
	public void authenticateCorporateJSONSchemavalidation(String mobile, String password, String corporateId, String passcode) throws IOException, ProcessingException {

		Processor processor = cafeHelper.authenticateCorporate(mobile, password, corporateId, passcode);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/Sand/authenticateCorporate.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		System.out.println("missingNodeList===" + missingNodeList.size());
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For authenticate Corporate API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
	}

	@Test(dataProvider = "authenticateCorporate", groups = {"functional", "regression", "srishty", "cafe"}, description = "Verify authenticate Corporate API for valid data")
	public void authenticateCorporate(String mobile, String password, String corporateId, String passcode) {
		Processor processor = cafeHelper.authenticateCorporate(mobile, password, corporateId, passcode);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");
		boolean authenticated = JsonPath.read(resp, "$.data.authenticated");
		Assert.assertEquals(authenticated, true);
	}

	@Test(dataProvider = "authenticateCorporateNegative", groups = {"functional", "regression", "srishty", "cafe"}, description = "Verify authenticate Corporate API for valid data")
	public void authenticateCorporateNegative(String mobile, String password, String corporateId, String passcode) {
		Processor processor = cafeHelper.authenticateCorporate(mobile, password, corporateId, passcode);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");
		boolean authenticated = JsonPath.read(resp, "$.data.authenticated");
		Assert.assertEquals(authenticated, false);
	}

	@Test(dataProvider = "CorporateByLatLong", groups = {"functional", "regression", "srishty", "cafe"}, description = "Verify corporate Listing API for valid lat-long")
	public void getCorporateByLatLong(String mobile, String password, String lat, String lng) throws IOException, ProcessingException {

		Processor processor = cafeHelper.getCorporateByLatLong(mobile, password, lat, lng);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 1);
		Assert.assertEquals(statusMessage, "done successfully");

	}

	@Test(dataProvider = "menuV4", groups = {"functional", "regression", "srishty", "cafe"}, description = "Verify menu V4 for cafe")
	public void verifyCafeInMenuV4(String restId, String lat, String lng) {

		/*
		 * Smoke Test
		 */
		Processor processor = SnDHelper.menuV4RestId(restId, lat, lng);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");
		boolean flag = processor.ResponseValidator.DoesNodeExists("$.data.cafe", resp);
		Assert.assertEquals(flag, true);
		boolean flag1 = JsonPath.read(resp, "$.data.cafe");
		Assert.assertEquals(flag1, true);
	}

	@Test(dataProvider = "menuV4Negative", groups = {"functional", "regression", "srishty", "cafe"}, description = "Verify menu V4 for cafe")
	public void verifyCafeInMenuV4Negative(String restId, String lat, String lng) {

		/*
		 * Smoke Test
		 */
		Processor processor = SnDHelper.menuV4RestId(restId, lat, lng);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");
		boolean flag = JsonPath.read(resp, "$.data.cafe");
		Assert.assertEquals(flag, false);
	}

	@Test(dataProvider = "cafeListing", groups = {"functional", "regression", "srishty", "cafe"}, description = "Verify cafe listing for given lat long")
	public void getcafeListingPositive(String corporateId, String passcode, String password, String mobile) {

		/*
		 * Smoke Test
		 */
		Processor processor = cafeHelper.getCafeListing(corporateId, passcode, password, mobile);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");

        /*
         functional validation
         */

		String data = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(data, null);
	}

	@Test(dataProvider = "cafeListingNegative", groups = {"functional", "regression", "srishty", "cafe"}, description = "Verify cafe listing for given lat long")
	public void getcafeListingNegative(String corporateId, String passcode, String password, String mobile) {

		/*
		 * Smoke Test
		 */
		Processor processor = cafeHelper.getCafeListing(corporateId, passcode, password, mobile);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 1);
		Assert.assertEquals(statusMessage, "Corporate not found");

        /*
         functional validation
         */

		String data = processor.ResponseValidator.GetNodeValue("$.data");
		Assert.assertNull(data);
	}

	@Test(dataProvider = "CorporateByLatLong", groups = {"functional", "srishty", "cafe"}, description = "Verify corporate Listing API")
	public void verifyCorporateByLatLong(String mobile, String password, String lat, String lng) {
		Processor processor = cafeHelper.getCorporateByLatLong(mobile, password, lat, lng);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");
		HashMap tempList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.corporates");
		boolean empty = tempList.isEmpty();
		Assert.assertEquals(empty, false);
	}

	@Test(dataProvider = "CorporateByLatLongNegative", groups = {"functional", "srishty", "cafe"}, description = "Verify corporate Listing API where cafe is not there")
	public void verifyCorporateByLatLongNegative(String mobile, String password, String lat, String lng) {
		Processor processor = cafeHelper.getCorporateByLatLong(mobile, password, lat, lng);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");
		HashMap tempList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.corporates");
		boolean empty = tempList.isEmpty();
		Assert.assertEquals(empty, true);
	}

	@Test(dataProvider = "CorporateByLatLong", groups = {"functional", "srishty", "cafe"}, description = "Verify disable corporate")
	public void verifyDisableCorporate(String mobile, String password, String lat, String lng) throws InterruptedException {

		boolean temp = true;
		Processor processor = cafeHelper.getCorporateByLatLong(mobile, password, lat, lng);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");

		JSONArray corporateIds = JsonPath.read(resp, "$.data.corporates[*].id");
		cafeHelper.disableCorporate(corporateIds.get(0).toString());
		//clearing cache
		cafeHelper.clearCacheCafe();
		Processor processorNew = cafeHelper.getCorporateByLatLong(mobile, password, lat, lng);
		String respNew = processorNew.ResponseValidator.GetBodyAsText();
		String[] corporateIdsNew = JsonPath.read(respNew, "$.data.corporates[*].id").toString().split(",");
		temp = corporateIdsNew.equals(corporateIds.get(0));
		Assert.assertEquals(temp, false);

		cafeHelper.enableCorporate(corporateIds.get(0).toString());
		Thread.sleep(1000);
		cafeHelper.clearCacheCafe();
		Thread.sleep(1000);

	}

	@Test(dataProvider = "CorporateByLatLong", groups = {"functional", "srishty", "cafe"}, description = "Verify disable corporate")
	public void verifyDisableCafe(String mobile, String password, String lat, String lng) throws InterruptedException {
		//Processor processor = cafeHelper.clearCacheCafe();
		Processor processor = cafeHelper.getCorporateByLatLong(mobile, password, lat, lng);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "done successfully");

		JSONArray cafeIds = JsonPath.read(resp, "$.data.corporates..cafes[*].id");
		cafeHelper.disableCafe(cafeIds.get(0).toString());
		//clearing cache
		cafeHelper.clearCacheCafe();
		Processor processorNew = cafeHelper.getCorporateByLatLong(mobile, password, lat, lng);
		String respNew = processor.ResponseValidator.GetBodyAsText();
		String[] cafeIdsNew = JsonPath.read(respNew, "$.data.corporates..cafes[*].id").toString().split(",");
		boolean temp = cafeIdsNew.equals(cafeIds.get(0));
		Assert.assertEquals(temp, false);

		cafeHelper.enableCafe(cafeIds.get(0).toString());
		Thread.sleep(1000);
		cafeHelper.clearCacheCafe();
		Thread.sleep(1000);
	}

	@Test
	public void clearCacheCafe() {
		Processor processor = cafeHelper.clearCacheCafe();
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);

	}

	/*
	 * Srishty End
	 * ############################################################################
	 */

	@Test(dataProvider = "locationBasedRain")
	public void locationBasedRain(String lat, String lng, String feature) {
		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String pop = JsonPath.read(resp, "$.data.SWIGGY_POP.available").toString().replace("[", "").replace("]", "");
		Assert.assertEquals(pop, "false", "Pop is available");

	}

	@Test(dataProvider = "rainAggregator")
	void rainAggregator(String lat, String lng) {
		String restID = helper.Aggregator(new String[]{lat, lng});
		String resp = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		List<String> restIDList = Arrays.asList(JsonPath.read(resp, "$.data.restaurants..id").toString().replace("[", "").replace("]", "").replaceAll("\"", "").split(","));
		boolean flag = false;

		for (int i = 0; i < restIDList.size(); i++) {
			if (restIDList.get(i).equals(restID)) {
				flag = true;
			}
		}
		Assert.assertTrue(flag, "Restaurant not found ");
	}


	/*@Test(dataProvider = "rainServiceability")
	void rainServiceability(String lat, String lng, String restaurantId, String area_id) throws Exception {
		//ServiceablilityHelper serviceablilityHelper = new ServiceablilityHelper();
		//String resp = serviceablilityHelper.serviceabilityCheckMenu(lat, lng, restaurantId, area_id);
		String serviceability= JsonPath.read(resp,"$.data.restaurantServiceabilities..serviceability").toString().replace("[","").replace("]","");
		Assert.assertEquals(serviceability, "SERVICEABLE_WITH_BANNER");
		//Assert.assertEquals(serviceability, "SERVICEABLE");
	}

	@Test(dataProvider = "rainModeType")
	void rainModeType(String lat, String lng, String restaurantId, String area_id) {
		//ServiceablilityHelper serviceablilityHelper = new ServiceablilityHelper();
		//String resp = serviceablilityHelper.serviceabilityCheckMenu(lat, lng, restaurantId, area_id);
		String rainMode= JsonPath.read(resp,"$.data.restaurantServiceabilities..rainMode").toString().replace("[","").replace("]","");
		Assert.assertEquals(rainMode, "normal");
		//Assert.assertEquals(rainMode, "extreme");
	}*/

	@Test(dataProvider = "rainCity")
	public void cityAvailibility(String lat, String lng) {
		String restID = helper.Aggregator(new String[]{lat, lng});
		String resp = helper.menuV3(restID).ResponseValidator.GetBodyAsText();
		String city_name = JsonPath.read(resp, "$.data.city").toString().replace("[", "").replace("]", "");
		cmsHelper.getCityId(city_name);
		cmsHelper.getCityDetails(CmsConstants.city_code);
		Assert.assertEquals(cmsHelper.getCityDetails(CmsConstants.city_code).get("is_open"), 1, "city closed");
		Assert.assertEquals(cmsHelper.getCityDetails(CmsConstants.city_code).get("enabled"), 1, "city closed");
		cmsHelper.setCityEnabled(CmsConstants.city_code, 0);
		cmsHelper.getCityHolidaySlot(CmsConstants.city_code);

		Assert.assertEquals(cmsHelper.getCityDetails(CmsConstants.city_code).get("closed_reason"), "", "city closed");

		LocalDateTime.now();

		int openTime = cmsHelper.getCityOpenTime(CmsConstants.city_code);
		cmsHelper.setCityOpenTime(CmsConstants.city_code, 0);
		int closeTime = cmsHelper.getCityCloseTime(CmsConstants.city_code);
		cmsHelper.setCityCloseTime(CmsConstants.city_code, 2359);
		int openHour = cmsHelper.getCitySlotOpenHour(CmsConstants.city_code);
		cmsHelper.setCityslotsOpenHour(CmsConstants.city_code, 1000);
		int closeHour = cmsHelper.getCitySlotCloseHour(CmsConstants.city_code);
		cmsHelper.setCityslotsCloseHour(CmsConstants.city_code, 2100);

	}

	@Test(dataProvider = "rainArea")
	public void areaAvailibility(String lat, String lng) {
		String restID = helper.Aggregator(new String[]{lat, lng});
		String resp = helper.menuV3(restID).ResponseValidator.GetBodyAsText();
		String area_name = JsonPath.read(resp, "$.data.area").toString().replace("[", "").replace("]", "");
		cmsHelper.getAreaId(area_name);
		cmsHelper.getAreaDetails(CmsConstants.area_code);
		Assert.assertEquals(cmsHelper.getAreaDetails(CmsConstants.area_code).get("is_open"), 1, "area closed");
		cmsHelper.setAreaOpen(CmsConstants.area_code, 1);
		Assert.assertEquals(cmsHelper.getAreaDetails(CmsConstants.area_code).get("enabled"), 1, "area disabled");
		cmsHelper.setAreaEnabled(CmsConstants.area_code, 1);
		cmsHelper.getAreaHolidaySlot(CmsConstants.area_code);

		Assert.assertEquals(cmsHelper.getAreaDetails(CmsConstants.area_code).get("closed_reason"), "Strike / Shutdown - Local Area", "area closed");

		cmsHelper.getAreaOpenTime(CmsConstants.area_code);
		cmsHelper.setAreaOpenTime(CmsConstants.area_code, 0);
		cmsHelper.getAreaCloseTime(CmsConstants.area_code);
		cmsHelper.setAreaCloseTime(CmsConstants.area_code, 2359);
		cmsHelper.getAreaSlotOpenHour(CmsConstants.area_code);
		cmsHelper.setAreaslotsOpenHour(CmsConstants.area_code, 1000);
		cmsHelper.getAreaSlotCloseHour(CmsConstants.area_code);
		cmsHelper.setAreaslotsCloseHour(CmsConstants.area_code, 2100);

	}

	@Test(dataProvider = "rainRestaurant")
	public void restaurantAvailibility(String lat, String lng) {
		String restID = helper.Aggregator(new String[]{lat, lng});
		String resp = helper.menuV3(restID).ResponseValidator.GetBodyAsText();
		String isopen = JsonPath.read(resp, "$.data.availability.opened").toString().replace("[", "").replace("]", "");
		Assert.assertEquals(isopen, "true", "restaurant closed");
		cmsHelper.getRestaurantEnabled(restID);
		cmsHelper.setRestaurantEnabled(restID, 0);
		Assert.assertEquals(cmsHelper.getRestaurantEnabled(restID), 1, "restaurant disabled");
		cmsHelper.getRestaurantHolidaySlot(restID);
		cmsHelper.setRestaurantHolidaySlot(restID, "1000", "2100");

		cmsHelper.getRestaurantOpenTime(restID);
		cmsHelper.setRestaurantOpenTime(restID, 1000);
		cmsHelper.getRestaurantCloseTime(restID);
		cmsHelper.setRestaurantCloseTime(restID, 2100);
		cmsHelper.getRestaurantTimeSlot(restID);
		cmsHelper.setRestaurantSlot(restID, "1000", "2100", "MON");
	}

	@Test(dataProvider = "rainMenu")
	public void menuAvailibility(String lat, String lng) {
		String restID = helper.Aggregator(new String[]{lat, lng});
		String resp = helper.menuV3(restID).ResponseValidator.GetBodyAsText();
		cmsHelper.getMenuMap(restID);
		cmsHelper.getMenuHolidaySlot("3161");
		cmsHelper.setMenuHolidaySlot("3161", "1000", "2100");
	}

	@Test(dataProvider = "rainItem")
	public void itemAvailibility(String lat, String lng) {
		String restID = helper.Aggregator(new String[]{lat, lng});
		String resp = helper.menuV3(restID).ResponseValidator.GetBodyAsText();
		cmsHelper.getItemIsEnabled(CmsConstants.item_id);
		cmsHelper.setItemIsEnabled(CmsConstants.item_id, 1);

		Assert.assertEquals(cmsHelper.getItemIsEnabled(CmsConstants.item_id), 1, "item disabled");
		cmsHelper.getItemSlot(CmsConstants.item_id);
		cmsHelper.setItemSlot(CmsConstants.item_id, "MON", "1000", "2100");

		cmsHelper.getItemHolidaySlot(CmsConstants.item_id);
		cmsHelper.setItemHolidaySlot(CmsConstants.item_id, "1000", "2100");

	}

	@Test(dataProvider = "exclusiveTag", description = "Checks for Exclusive Restaurant", enabled = false)
	public void exclusiveRestaurant(int is_exclusive, String lat, String lng) {
		String rest_id = helper.Aggregator(new String[]{lat, lng});
		String check = cmsHelper.getWpOptionsValue(SANDConstants.listingTag);
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.positiveWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		cmsHelper.setExclusiveRestaurant(rest_id, is_exclusive);
		helper.kafka(rest_id, String.valueOf(Instant.now().getEpochSecond()));
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String exclusive = JsonPath.read(response, " $.data.restaurants." + rest_id + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(exclusive.contains("\"EXCLUSIVE\""), "Restaurant not exclusive");
	}

	@Test(dataProvider = "nonExclusiveTag", description = "Checks for Non Exclusive Restaurant", enabled = false)
	public void nonExclusiveRestaurant(int is_exclusive, String lat, String lng) {
		String rest_id = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.positiveWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		cmsHelper.setExclusiveRestaurant(rest_id, is_exclusive);
		helper.kafka(rest_id, String.valueOf(Instant.now().getEpochSecond()));
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String exclusive = JsonPath.read(response, " $.data.restaurants." + rest_id + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertFalse(exclusive.contains("\"EXCLUSIVE\""), "Restaurant is exclusive");
	}


	@Test(dataProvider = "exclusiveTag", description = "Negative case for Exclusive Restaurant", enabled = false)
	public void negative_exclusiveRestaurant(int is_exclusive, String lat, String lng) {
		String rest_id = helper.Aggregator(new String[]{lat, lng});
		String check = cmsHelper.getWpOptionsValue(SANDConstants.listingTag);
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.negativeWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		cmsHelper.setExclusiveRestaurant(rest_id, is_exclusive);
		helper.kafka(rest_id, String.valueOf(Instant.now().getEpochSecond()));
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String exclusive = JsonPath.read(response, " $.data.restaurants." + rest_id + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(exclusive.contains("\"EXCLUSIVE\""), "Restaurant is not exclusive");
	}

	@Test(dataProvider = "nonExclusiveTag", description = "Checks for Non Exclusive Restaurant", enabled = false)
	public void negative_nonExclusiveRestaurant(int is_exclusive, String lat, String lng) {
		String rest_id = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.negativeWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		cmsHelper.setExclusiveRestaurant(rest_id, is_exclusive);
		helper.kafka(rest_id, String.valueOf(Instant.now().getEpochSecond()));
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String exclusive = JsonPath.read(response, " $.data.restaurants." + rest_id + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertFalse(exclusive.contains("\"EXCLUSIVE\""), "Restaurant is exclusive");
	}

	@Test(dataProvider = "newTag", description = "Checks for New Arrival Restaurant", enabled = false)
	public void newRestaurant(String time, String lat, String lng) {
		String rest_id = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.positiveWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		cmsHelper.setLiveDate(rest_id, time);
		//cmsHelper.setExclusiveRestaurant(rest_id, 0);
		helper.kafka(rest_id, String.valueOf(Instant.now().getEpochSecond()));
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String newTag = JsonPath.read(response, " $.data.restaurants." + rest_id + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(newTag.contains("\"NEW\""), "Restaurant is not new");
	}


	@Test(dataProvider = "notNewTag", description = "Checks for Non New Arrival Restaurant", enabled = false)
	public void newTagRestaurant(String time, String lat, String lng) {
		String rest_id = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.positiveWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		cmsHelper.setLiveDate(rest_id, time);
		helper.kafka(rest_id, String.valueOf(Instant.now().getEpochSecond()));
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String newTag = JsonPath.read(response, " $.data.restaurants." + rest_id + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertFalse(newTag.contains("\"NEW\""), "Restaurant is new");
	}


	@Test(dataProvider = "newTag", description = "Negative case for New Arrival Restaurant", enabled = false)
	public void negative_newRestaurant(String time, String lat, String lng) {
		String rest_id = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.negativeWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		cmsHelper.setLiveDate(rest_id, time);
		//cmsHelper.setExclusiveRestaurant(rest_id, 0);
		helper.kafka(rest_id, String.valueOf(Instant.now().getEpochSecond()));
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String newTag = JsonPath.read(response, " $.data.restaurants." + rest_id + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(newTag.contains("\"NEW\""), "Restaurant is not new");
	}


	@Test(dataProvider = "notNewTag", description = "Negative case for Non New Arrival Restaurant", enabled = false)
	public void negative_newTagRestaurant(String time, String lat, String lng) {
		String rest_id = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.negativeWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		cmsHelper.setLiveDate(rest_id, time);
		helper.kafka(rest_id, String.valueOf(Instant.now().getEpochSecond()));
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String newTag = JsonPath.read(response, " $.data.restaurants." + rest_id + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertFalse(newTag.contains("\"NEW\""), "Restaurant is new");
	}


	@Test(dataProvider = "popularTag", description = "Checks for Popular Restaurant", enabled = false)
	public void popularTag(String lat, String lng) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.positiveWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		LocalDateTime date1 = LocalDateTime.now();
		int time = helper.getTime(date1);
		String keyInitial = SANDConstants.keyInitial;
		String entity = restId;
		int redisdb = SANDConstants.db;
		String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);
		redisHelper.setValueJson(SANDConstants.analytics, redisdb, keyInitial + restId, "{\"area_id\": 1, \"rest_similarity\": {}, \"ratings\": 0, \"orders\": {}, \"commissions\": {}, \"cat_subcats\": [], \"visits\": {}, \"rest_latent_features_slotwise\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"aov\": 0, \"taste_profile\": {\"1\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"0\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"3\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"2\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"4\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\"}, \"avg_ratings\": 0, \"commission_overall\": 0, \"rest_id\": \" + restId + \", \"total_orders\": 0, \"aov_slotwise\": {},\"item_ratings\": {}, \"revenue\": {\"1\": {}, \"0\": {}, \"3\": {\"web\": 1.0,\"android\": 1.0, \"ios\": 1.0}, \"2\": {\"web\": 1.32221929473, \"android\": 1.32221929473, \"ios\": 1.32221929473}, \"4\": {\"web\": 1.60205999133, \"android\": 1.60205999133, \"ios\": 1.60205999133}}, \"cancels\": {}, \"most_ordered\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"city_id\": 1, \"edits\": {}, \"popular\": 1}");
		helper.kafka(restId, String.valueOf(Instant.now().getEpochSecond()));
		Integer popular = JsonPath.read(redisInfo, "$.popular");
		String populartag = JsonPath.read(response, " $.data.restaurants." + restId + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(populartag.contains("\"POPULAR\""), "Restaurant is not popular");
	}

	@Test(dataProvider = "nonPopularTag", description = "Checks for Non Popular Restaurant", enabled = false)
	public void nonPopularopularTag(String lat, String lng) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.positiveWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String popularTag = JsonPath.read(response, " $.data.restaurants." + restId + "").toString().replace("[", "").replace("]", "");
		LocalDateTime date1 = LocalDateTime.now();
		int time = helper.getTime(date1);
		String keyInitial = SANDConstants.keyInitial;
		String entity = restId;
		int redisdb = SANDConstants.db;
		String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);
		redisHelper.setValueJson(SANDConstants.analytics, redisdb, keyInitial + restId, "{\"area_id\": 1, \"rest_similarity\": {}, \"ratings\": 0, \"orders\": {}, \"commissions\": {}, \"cat_subcats\": [], \"visits\": {}, \"rest_latent_features_slotwise\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"aov\": 0, \"taste_profile\": {\"1\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"0\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"3\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"2\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"4\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\"}, \"avg_ratings\": 0, \"commission_overall\": 0, \"rest_id\":  \" + restId + \", \"total_orders\": 0, \"aov_slotwise\": {},\"item_ratings\": {}, \"revenue\": {\"1\": {}, \"0\": {}, \"3\": {\"web\": 1.0,\"android\": 1.0, \"ios\": 1.0}, \"2\": {\"web\": 1.32221929473, \"android\": 1.32221929473, \"ios\": 1.32221929473}, \"4\": {\"web\": 1.60205999133, \"android\": 1.60205999133, \"ios\": 1.60205999133}}, \"cancels\": {}, \"most_ordered\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"city_id\": 1, \"edits\": {}, \"popular\": 1}");
		helper.kafka(restId, String.valueOf(Instant.now().getEpochSecond()));
		Integer popular = JsonPath.read(redisInfo, "$.popular");
		String populartag = JsonPath.read(response, " $.data.restaurants." + restId + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertFalse(populartag.contains("\"POPULAR\""), "Restaurant is popular");
	}

	@Test(dataProvider = "popularTag", description = "Negative case for Popular Restaurant", enabled = false)
	public void negative_popularTag(String lat, String lng) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.negativeWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		LocalDateTime date1 = LocalDateTime.now();
		int time = helper.getTime(date1);
		String keyInitial = SANDConstants.keyInitial;
		String entity = restId;
		int redisdb = SANDConstants.db;
		String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);
		redisHelper.setValueJson(SANDConstants.analytics, redisdb, keyInitial + restId, "{\"area_id\": 1, \"rest_similarity\": {}, \"ratings\": 0, \"orders\": {}, \"commissions\": {}, \"cat_subcats\": [], \"visits\": {}, \"rest_latent_features_slotwise\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"aov\": 0, \"taste_profile\": {\"1\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"0\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"3\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"2\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"4\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\"}, \"avg_ratings\": 0, \"commission_overall\": 0, \"rest_id\": \" + restId + \", \"total_orders\": 0, \"aov_slotwise\": {},\"item_ratings\": {}, \"revenue\": {\"1\": {}, \"0\": {}, \"3\": {\"web\": 1.0,\"android\": 1.0, \"ios\": 1.0}, \"2\": {\"web\": 1.32221929473, \"android\": 1.32221929473, \"ios\": 1.32221929473}, \"4\": {\"web\": 1.60205999133, \"android\": 1.60205999133, \"ios\": 1.60205999133}}, \"cancels\": {}, \"most_ordered\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"city_id\": 1, \"edits\": {}, \"popular\": 1}");
		helper.kafka(restId, String.valueOf(Instant.now().getEpochSecond()));
		Integer popular = JsonPath.read(redisInfo, "$.popular");
		String populartag = JsonPath.read(response, " $.data.restaurants." + restId + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(populartag.contains("\"POPULAR\""), "Restaurant is not popular");
	}

	@Test(dataProvider = "nonPopularTag", description = "Negative case for Non Popular Restaurant", enabled = false)
	public void negative_nonPopularopularTag(String lat, String lng) {
		String restId = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.negativeWPOptions, "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, SANDConstants.index, SANDConstants.delKey);
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		String popularTag = JsonPath.read(response, " $.data.restaurants.\" + restId +").toString().replace("[", "").replace("]", "");
		LocalDateTime date1 = LocalDateTime.now();
		int time = helper.getTime(date1);
		String keyInitial = SANDConstants.keyInitial;
		String entity = restId;
		int redisdb = SANDConstants.db;
		String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);
		redisHelper.setValueJson(SANDConstants.analytics, redisdb, keyInitial + restId, "{\"area_id\": 1, \"rest_similarity\": {}, \"ratings\": 0, \"orders\": {}, \"commissions\": {}, \"cat_subcats\": [], \"visits\": {}, \"rest_latent_features_slotwise\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"aov\": 0, \"taste_profile\": {\"1\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"0\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"3\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"2\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"4\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\"}, \"avg_ratings\": 0, \"commission_overall\": 0, \"rest_id\": \" + restId + \", \"total_orders\": 0, \"aov_slotwise\": {},\"item_ratings\": {}, \"revenue\": {\"1\": {}, \"0\": {}, \"3\": {\"web\": 1.0,\"android\": 1.0, \"ios\": 1.0}, \"2\": {\"web\": 1.32221929473, \"android\": 1.32221929473, \"ios\": 1.32221929473}, \"4\": {\"web\": 1.60205999133, \"android\": 1.60205999133, \"ios\": 1.60205999133}}, \"cancels\": {}, \"most_ordered\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"city_id\": 1, \"edits\": {}, \"popular\": 1}");
		helper.kafka(restId, String.valueOf(Instant.now().getEpochSecond()));
		Integer popular = JsonPath.read(redisInfo, "$.popular");
		String populartag = JsonPath.read(response, " $.data.restaurants." + restId + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertFalse(populartag.contains("\"POPULAR\""), "Restaurant is popular");
	}

	/*@Test(dataProvider = "premiumTag", description = "Checks for Premium Restaurant",enabled = false)
	public void premiumTag( String lat, String lng)  {
	    String restId=helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag,SANDConstants.positiveWPOptions,"true");
		cmsHelper.setWpOptions("min_aov_for_premium", "0.8", "true");
		cmsHelper.setWpOptions("min_ratings_for_premium", "4.0", "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, 2, SANDConstants.delKey);
		cmsHelper.getWpOptionsValue("min_aov_for_premium");
		cmsHelper.getWpOptionsValue("min_rating_for_premium");
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		LocalDateTime date1 = LocalDateTime.now();
		int time = helper.getTime(date1);
		String keyInitial = SANDConstants.keyInitial;
		String entity = restId;
		int redisdb = SANDConstants.db;
		String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);
		redisHelper.setValueJson(SANDConstants.analytics,redisdb, keyInitial + restId, "{\"area_id\": 1, \"rest_similarity\": {}, \"ratings\": 4.0, \"orders\": {}, \"commissions\": {}, \"cat_subcats\": [], \"visits\": {}, \"rest_latent_features_slotwise\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"aov\": 0.8, \"taste_profile\": {\"1\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"0\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"3\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"2\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"4\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\"}, \"avg_ratings\": 0, \"commission_overall\": 0, \"rest_id\": " + restId +", \"total_orders\": 0, \"aov_slotwise\": {},\"item_ratings\": {}, \"revenue\": {\"1\": {}, \"0\": {}, \"3\": {\"web\": 1.0,\"android\": 1.0, \"ios\": 1.0}, \"2\": {\"web\": 1.32221929473, \"android\": 1.32221929473, \"ios\": 1.32221929473}, \"4\": {\"web\": 1.60205999133, \"android\": 1.60205999133, \"ios\": 1.60205999133}}, \"cancels\": {}, \"most_ordered\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"city_id\": 1, \"edits\": {}, \"popular\": 1}"
		);
		helper.kafka(restId,String.valueOf(Instant.now().getEpochSecond()));
		String premiumTag = JsonPath.read(response, " $.data.restaurants." + restId + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(premiumTag.contains("\"PREMIUM\""),"Restaurant is not premium");
	}


	@Test(dataProvider = "nonPremiumTag", description = "Checks for Non Premium Restaurant",enabled = false)
	public void nonPremiumTag( String lat, String lng) {
	    String restId=helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag,SANDConstants.positiveWPOptions,"true");
		cmsHelper.setWpOptions("min_aov_for_premium", "2.0", "true");
		cmsHelper.setWpOptions("min_ratings_for_premium", "5.0", "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, 2, SANDConstants.delKey);
		cmsHelper.getWpOptionsValue("min_aov_for_premium");
		cmsHelper.getWpOptionsValue("min_rating_for_premium");
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		LocalDateTime date1 = LocalDateTime.now();
		int time = helper.getTime(date1);
		String keyInitial = SANDConstants.keyInitial;
		String entity = restId;
		int redisdb = SANDConstants.db;
		String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);
		redisHelper.setValueJson(SANDConstants.analytics, redisdb, keyInitial + restId, "{\"area_id\": 1, \"rest_similarity\": {}, \"ratings\": 5.0, \"orders\": {}, \"commissions\": {}, \"cat_subcats\": [], \"visits\": {}, \"rest_latent_features_slotwise\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"aov\": 2.0, \"taste_profile\": {\"1\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"0\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"3\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"2\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"4\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\"}, \"avg_ratings\": 0, \"commission_overall\": 0, \"rest_id\": " + restId +", \"total_orders\": 0, \"aov_slotwise\": {},\"item_ratings\": {}, \"revenue\": {\"1\": {}, \"0\": {}, \"3\": {\"web\": 1.0,\"android\": 1.0, \"ios\": 1.0}, \"2\": {\"web\": 1.32221929473, \"android\": 1.32221929473, \"ios\": 1.32221929473}, \"4\": {\"web\": 1.60205999133, \"android\": 1.60205999133, \"ios\": 1.60205999133}}, \"cancels\": {}, \"most_ordered\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"city_id\": 1, \"edits\": {}, \"popular\": 1}"
		);
		helper.kafka(restId,String.valueOf(Instant.now().getEpochSecond()));
		String premiumTag = JsonPath.read(response, " $.data.restaurants." + restId + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(premiumTag.contains("\"PREMIUM\""),"Restaurant is premium");
	}

	@Test(dataProvider = "premiumTag", description = "Negative case for Premium Restaurant",enabled = false)
	public void negative_premiumTag( String lat, String lng)  {
	    String restId=helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag,SANDConstants.negativeWPOptions,"true");
		cmsHelper.setWpOptions("min_aov_for_premium", "0.8", "true");
		cmsHelper.setWpOptions("min_ratings_for_premium", "4.0", "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, 2, SANDConstants.delKey);
		cmsHelper.getWpOptionsValue("min_aov_for_premium");
		cmsHelper.getWpOptionsValue("min_rating_for_premium");
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		LocalDateTime date1 = LocalDateTime.now();
		int time = helper.getTime(date1);
		String keyInitial = SANDConstants.keyInitial;
		String entity = restId;
		int redisdb =SANDConstants.db;
		String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);
		redisHelper.setValueJson(SANDConstants.analytics, redisdb, keyInitial + restId, "{\"area_id\": 1, \"rest_similarity\": {}, \"ratings\": 4.0, \"orders\": {}, \"commissions\": {}, \"cat_subcats\": [], \"visits\": {}, \"rest_latent_features_slotwise\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"aov\": 0.8, \"taste_profile\": {\"1\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"0\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"3\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"2\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"4\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\"}, \"avg_ratings\": 0, \"commission_overall\": 0, \"rest_id\": " + restId + ", \"total_orders\": 0, \"aov_slotwise\": {},\"item_ratings\": {}, \"revenue\": {\"1\": {}, \"0\": {}, \"3\": {\"web\": 1.0,\"android\": 1.0, \"ios\": 1.0}, \"2\": {\"web\": 1.32221929473, \"android\": 1.32221929473, \"ios\": 1.32221929473}, \"4\": {\"web\": 1.60205999133, \"android\": 1.60205999133, \"ios\": 1.60205999133}}, \"cancels\": {}, \"most_ordered\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"city_id\": 1, \"edits\": {}, \"popular\": 1}"
		);
		helper.kafka(restId,String.valueOf(Instant.now().getEpochSecond()));
		String premiumTag = JsonPath.read(response, " $.data.restaurants." + restId + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(premiumTag.contains("\"PREMIUM\""),"Restaurant is not premium");
	}

	@Test(dataProvider = "nonPremiumTag", description = "Negative case for Non Premium Restaurant",enabled = false)
	public void negative_nonPremiumTag( String lat, String lng)  {
	    String restId=helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setWpOptions(SANDConstants.listingTag, SANDConstants.negativeWPOptions,"true");
		cmsHelper.setWpOptions("min_aov_for_premium", "2.0", "true");
		cmsHelper.setWpOptions("min_ratings_for_premium", "5.0", "true");
		redisHelper.deleteKey(SANDConstants.sandRedis, 2, SANDConstants.delKey);
		cmsHelper.getWpOptionsValue("min_aov_for_premium");
		cmsHelper.getWpOptionsValue("min_rating_for_premium");
		String response = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		LocalDateTime date1 = LocalDateTime.now();
		int time = helper.getTime(date1);
		String keyInitial = SANDConstants.keyInitial;
		String entity = restId;
		int redisdb = SANDConstants.db;
		String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);
		redisHelper.setValueJson(SANDConstants.analytics, redisdb, keyInitial + restId, "{\"area_id\": 1, \"rest_similarity\": {}, \"ratings\": 5.0, \"orders\": {}, \"commissions\": {}, \"cat_subcats\": [], \"visits\": {}, \"rest_latent_features_slotwise\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"aov\": 2.0, \"taste_profile\": {\"1\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"0\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"3\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"2\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\", \"4\": \"0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.9678,0.0216,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0\"}, \"avg_ratings\": 0, \"commission_overall\": 0, \"rest_id\": " + restId + ", \"total_orders\": 0, \"aov_slotwise\": {},\"item_ratings\": {}, \"revenue\": {\"1\": {}, \"0\": {}, \"3\": {\"web\": 1.0,\"android\": 1.0, \"ios\": 1.0}, \"2\": {\"web\": 1.32221929473, \"android\": 1.32221929473, \"ios\": 1.32221929473}, \"4\": {\"web\": 1.60205999133, \"android\": 1.60205999133, \"ios\": 1.60205999133}}, \"cancels\": {}, \"most_ordered\": {\"1\": {}, \"0\": {}, \"3\": {}, \"2\": {}, \"4\": {}}, \"city_id\": 1, \"edits\": {}, \"popular\": 1}");
		helper.kafka(restId,String.valueOf(Instant.now().getEpochSecond()));
		String premiumTag = JsonPath.read(response, " $.data.restaurants." + restId + ".dynamicTags").toString().replace("[", "").replace("]", "");
		Assert.assertTrue(premiumTag.contains("\"PREMIUM\""),"Restaurant is premium");
	}*/


	//menu tags




	//menuV4
	@Test(dataProvider = "MenuV4",enabled = false)
	public void checkRest(String lat, String lng) {
		String aggregatorResponse = helper.aggregator(new String[]{lat, lng}).ResponseValidator.GetBodyAsText();
		List<String> restaurantId = Arrays.asList(JsonPath.read(aggregatorResponse, "$.data.restaurants..id").toString().replace("[", "").replace("]", "").replace("\"", "").split(","));
		for (int r = 0; r < restaurantId.size(); r++) {
			String response = helper.menuV4RestId(String.valueOf(restaurantId.get(r)), lat, lng).ResponseValidator.GetBodyAsText();
			String statusCode = JsonPath.read(response, " $.statusCode").toString().replace("[", "").replace("]", "");
			Assert.assertEquals(statusCode, "0", "status code is not 0");
			String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
			Assert.assertEquals(statusMessage, "done successfully", "does not match");
			String restaurantServiceLine = JsonPath.read(response, "$.data.restaurantServiceLine").toString().replace("[", "").replace("]", "");
			Assert.assertEquals(restaurantServiceLine, restaurantServiceLine, "POP");
			List<String> enabled = Arrays.asList(JsonPath.read(response, "$.data.menu.items..enabled").toString().replace("[", "").replace("]", "").split(","));
			List<String> enabledList = new ArrayList<>();
			List<String> inStock = Arrays.asList(JsonPath.read(response, "$.data.menu.items..inStock").toString().replace("[", "").replace("]", "").split(","));
			List<String> inStockList = new ArrayList<>();
			for (int i = 0; i < enabled.size(); i++) {
				if (enabled.get(i).equals("1")) {
					enabledList.add(enabled.get(i));
					Assert.assertEquals(enabledList.get(i), "1", "not enabled");
				}
				if (inStock.get(i).equals("1")) {
					inStockList.add(inStock.get(i));
					Assert.assertEquals(inStockList.get(i), "1", "out of stock");
				} else if (inStock.get(i).equals("0")) {
					inStockList.add(inStock.get(i));
					Assert.assertEquals(inStockList.get(i), "0", "in stock");
				}
			}
		}
	}

	@Test(dataProvider = "MenuV4")
	public void addons(String lat, String lng) {
		String aggregatorResponse = helper.aggregator(new String[]{lat, lng}).ResponseValidator.GetBodyAsText();
		List<String> restaurantId = Arrays.asList(JsonPath.read(aggregatorResponse, "$.data.restaurants..id").toString().replace("[", "").replace("]", "").replace("\"", "").split(","));
		for (int r = 0; r < 1; r++) {
			String response = helper.menuV4RestId(String.valueOf(restaurantId.get(r)), lat, lng).ResponseValidator.GetBodyAsText();
			List<String> itemID = Arrays.asList(JsonPath.read(response, "$.data.menu.items..id").toString().replace("[", "").replace("]", "").replace("\"", "").split(","));
			List<String> inStock = Arrays.asList(JsonPath.read(response, "$.data.menu.items..inStock").toString().replace("[", "").replace("]", "").split(","));
			List<String> inStockList = new ArrayList<>();
			for (int i = 0; i < inStock.size(); i++) {
				if (inStock.get(i).equals("1")) {
					inStockList.add(inStock.get(i));
					Assert.assertEquals(inStockList.get(i), "1", "out of stock");

					HashMap<String, List<String>> addonsMap = new HashMap<String, List<String>>();
					for (int x = 0; x < itemID.size(); x++) {
						try {
							List<String> addons = Arrays.asList(JsonPath.read(response, "$.data.menu.items." + Integer.parseInt(itemID.get(x)) + ".addons..id").toString().replace("[", "").replace("]", "").split(","));
							addonsMap.put(itemID.get(x), addons);
						} catch (PathNotFoundException e) {
						}
					}
				}
			}
		}
	}

	@Test(dataProvider = "LatLong", description = "Verify status, status code , status message")
	public void aggregator(String lat, String lng) {
		String resp = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		Processor p = helper.aggregator(new String[]{lat, lng});
		String response = p.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(p.ResponseValidator.GetResponseCode(), 200);
		Assert.assertEquals(statusCode, 0, "status code is 1");
		Assert.assertEquals(statusMessage, "done successfully");
	}

	@Test(dataProvider = "LatLong", description = "Check number of closed restaurants")
	public void aggregatorAvailability(String lat, String lng) {
		String resp = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(statusCode, 0, "status code is 1");
		Assert.assertEquals(statusMessage, "done successfully");
		List<String> restIds = Arrays.asList(headerHelper.JsonString(resp, "$.data.restaurants[*].id").split(","));
		List<String> open = Arrays.asList(headerHelper.JsonString(resp, "$.data.restaurants[*].availability.opened").split(","));
		List<String> openRestList = new ArrayList<>();
		for (int i = 0; i < restIds.size(); i++) {
			if (open.get(i).equalsIgnoreCase("true")) {
				openRestList.add(restIds.get(i));
			}
		}
		int closedRestaurant = restIds.size() - openRestList.size();
	}

	@Test(dataProvider = "LatLong", description = "Verify cuisine of restaurants with db")
	public void cuisine(String lat, String lng) {
		String resp = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(statusCode, 0, "status code is 1");
		Assert.assertEquals(statusMessage, "done successfully");
		List<String> restIds = Arrays.asList(headerHelper.JsonString(resp, "$.data.restaurants[*].id").split(","));
		HashMap<String, List<String>> restaurantCuisine = new HashMap<>();
		for (int i = 0; i < restIds.size(); i++) {
			List<String> cuisine = Arrays.asList(headerHelper.JsonString(resp, "$.data.restaurants." + restIds.get(i) + ".cuisines"));
			restaurantCuisine.put(restIds.get(i), cuisine);
			List<Map<String, Object>> l = helper.cuisine(restIds.get(i));
			Assert.assertEquals(restaurantCuisine.get(restIds.get(i)).toString().replaceAll("\\]|\\[", ""), l.get(0).get("cuisine"), "Response and db does not matches");
		}
	}

	@Test(dataProvider = "LatLong", description = "Verify if sorted restaurants node is present or not")
	public void sortedRestaurants(String lat, String lng) {
		boolean resp = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.DoesNodeExists("data.sortedRestaurants");
		Assert.assertTrue(resp, "sorted restaurants are not present");
	}

	@Test(dataProvider = "LatLong", description = "Verify if all sorted restaurants are opened or not")
	public void verifysortedRestaurants(String lat, String lng) {
		String resp = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(statusCode, 0, "status code is 1");
		Assert.assertEquals(statusMessage, "done successfully");
		List<String> open = Arrays.asList(headerHelper.JsonString(resp, "$.data.restaurants[*].availability.opened").split(","));
		List<String> sortedrestIds = Arrays.asList(headerHelper.JsonString(resp, "$.data.sortedRestaurants..restaurantId").split(","));
		for (int i = 0; i < sortedrestIds.size(); i++) {
			String openrestaurant = headerHelper.JsonString(resp, "$.data.restaurants." + sortedrestIds.get(i) + ".availability.opened");
			System.out.println(openrestaurant);
			Assert.assertEquals(openrestaurant, "true", "restaurant closed");
		}
	}

	@Test(dataProvider = "LatLong", description = "Verify if carousel node is present or not")
	public void carousel(String lat, String lng) {
		Boolean resp = sDHelper.getAggregatorDetails(lat, lng).ResponseValidator.DoesNodeExists("data.carousels");
		Assert.assertTrue(resp, "Carousel is not present");
	}

	@Test(dataProvider = "menuReorder", description = "Verify satus code and message")
	public void menuReorder(String restId, String lat, String lng, String custId) {
		String keyInitial = "CUST_PROFILE_" + custId;
		int redisdb = SANDConstants.db;
		String redisInfo = (String) redisHelper.getValue("sandredisstage", redisdb, keyInitial);
		String Response = helper.menuReorder(restId, lat, lng, custId).ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(Response, "$.statusCode");
		String statusMessage = JsonPath.read(Response, "$.statusMessage");
		Assert.assertEquals(statusCode, 0, "status code is 1");
		Assert.assertEquals(statusMessage, "done successfully");
	}

	@Test(dataProvider = "menuReorder", description = "Verify if reorder node is present")
	public void menuReorder_node(String restId, String lat, String lng, String custId) {
		String keyInitial = "CUST_PROFILE_" + custId;
		int redisdb = SANDConstants.db;
		String redisInfo = (String) redisHelper.getValue("sandredisstage", redisdb, keyInitial);
		String Response = helper.menuReorder(restId, lat, lng, custId).ResponseValidator.GetBodyAsText();
		List<String> reorderNode = Arrays.asList(headerHelper.JsonString(Response, "$.data..widgets..type").split(","));
		Assert.assertTrue(reorderNode.contains("reorder"), "Reorder does not exist");
	}

	@Test(dataProvider = "menuReorder", description = "Verify if redis data and response are same")
	public void MenuReorder(String restId, String lat, String lng, String custId) {
		String keyInitial = "CUST_PROFILE_" + custId;
		int redisdb = SANDConstants.db;
		String redisInfo = (String) redisHelper.getValue("sandredisstage", redisdb, keyInitial);
		System.out.println(redisInfo);
		String redis = headerHelper.JsonString(redisInfo, "$.orderProfileSlotwise..1.." + restId);
		List<String> redis_list = new ArrayList<>();
		try {
			JSONObject json = new JSONObject(redis);
			Iterator itr = json.keys();
			while (itr.hasNext()) {
				redis_list.add(itr.next().toString());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String Response = helper.menuReorder(restId, lat, lng, custId).ResponseValidator.GetBodyAsText();
		List<String> menuV4 = Arrays.asList(headerHelper.JsonString(Response, "$.data..widgets[?(@.type=='reorder')]..id").split(","));
		Assert.assertEquals(menuV4, redis_list, "data in both does not matches");
	}

	/*@Test
	public  void cross() throws IOException {
		crossSelling c = new crossSelling();
		c.setcategory("salad");

		JsonHelper jsonHelper = new JsonHelper();
		//"beverage - unbranded milkshake\": [{\"score\": 1, \"id\": \"2847205\"}
		ItemSet itemSet = new ItemSet();
		itemSet.setItem_id("65437");
		itemSet.setScore(6);
		c.setItemSets(new ItemSet[]{itemSet});
		System.out.println(jsonHelper.getObjectToJSON(c));
	}

	@Test(dataProvider = "cross")
	public void  itemDetails(String rest_id, String category_name){
		System.out.println(helper.getItemDetails(rest_id,category_name));
	}

	@Test(dataProvider = "crossSell")
    public void crossSell(String category_name, String restId,List<String> categories){
		System.out.println(helper.crossSelling(category_name,  restId, categories));
	}*/

	@Test(dataProvider = "areaListingPostive", description = "Verify the smoke test for Area listing API")
	public void areaListing(String area) {
		Processor processor = helper.getAreaListing(area);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), SANDConstants.responseCode,"Response code is different");
		Assert.assertEquals(statusCode, SANDConstants.StatusCode,"Status code is different");
		Assert.assertEquals(statusMessage, SANDConstants.msg,"Status Message is different");
	}

	@Test(dataProvider = "areaListingPostive", description = "Verify area name and area id for Area listing API")
	public void areaListingNameId(String area) {
		Processor processor = helper.getAreaListing(area);
		List areaName = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants..areaName");
		for (int i = 0; i < areaName.size(); i++) {
			boolean flag = area.equalsIgnoreCase(areaName.get(i).toString());
			Assert.assertEquals(flag, true,"Area name is incorrect");
		}
		List areaId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.restaurants..areaId");
		for (int i = 0; i < areaId.size(); i++) {
			boolean flag = (areaId.get(0).toString()).equalsIgnoreCase(areaId.get(i).toString());
			Assert.assertEquals(flag, true,"Incorrect area id");
		}
	}

	@Test(dataProvider = "areaListingPostive", description = "Verify if all the restaurants showing in response are enabled")
	public void areaListingRestaurant(String area) {
		Processor processor = helper.getAreaListing(area);
		String resp = processor.ResponseValidator.GetBodyAsText();
		List restaurants = Arrays.asList(JsonPath.read(resp, "$.data.restaurants..id").toString().replace("[", "").replace("]", "").split(","));
		for (int i = 0; i < restaurants.size(); i++) {
			Assert.assertEquals(cmsHelper.getRestaurantEnabled(restaurants.get(i).toString()), SANDConstants.enabled, "restaurant disabled");
		}
	}

	@Test(dataProvider = "areaListingNegative", description = "Verify response for negative area")
	public void areaListingNegative(String area) {
		Processor processor = helper.getAreaListing(area);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), SANDConstants.responseCode);
		Assert.assertEquals(statusCode, SANDConstants.statusCodeError,"Status code is different");
		Assert.assertEquals(statusMessage, SANDConstants.areaStatusMsgError,"Area is correct");
	}

	@Test(dataProvider = "locationBasedAssuredEnabled", description = " Verify if swiggy assured is enabled")
	public void locationFeatureAssuredEnabled(String lat, String lng, String feature) {
		String assured = cmsHelper.getWpOptionsValue(SANDConstants.SwiggyAssuredWpOption[0]);
		Assert.assertEquals(assured, "1", "Swiggy assured is disabled");
	}

	@Test(dataProvider = "locationBasedAssuredEnabled", description = " Verify if swiggy assured node exists")
	public void locationFeatureAssuredNode(String lat, String lng, String feature) {
		String assured = cmsHelper.getWpOptionsValue(SANDConstants.SwiggyAssuredWpOption[1]);
		String assuredCity = cmsHelper.getWpOptionsValue(SANDConstants.SwiggyAssuredWpOption[2]);
		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		boolean node = processor.ResponseValidator.DoesNodeExists("data.SWIGGY_ASSURED.swiggy_assured_help_text");
		Assert.assertTrue(node, "Swiggy Assured node does not exists");
	}

	@Test(dataProvider = "locationBasedAssuredEnabled", description = " Verify if cash back percentage matches in response and db")
	public void locationFeatureAssuredCashBackPercent(String lat, String lng, String feature) {
		String assured = cmsHelper.getWpOptionsValue(SANDConstants.SwiggyAssuredWpOption[1]);
		String assuredCity = cmsHelper.getWpOptionsValue(SANDConstants.SwiggyAssuredWpOption[2]);
		String CBPercent = headerHelper.JsonString(assured, "$.0.swiggy_select_cashback_percentage");
		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		String node = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.SWIGGY_ASSURED.swiggy_select_cashback_percentage").replace("[", "").replace("]", "");
		Assert.assertEquals(node, CBPercent, "Cash back percent is different");
	}

	@Test(dataProvider = "locationBasedAssuredEnabled", description = " Verify if cash back amount matches in response and db")
	public void locationFeatureAssuredCashBackAmount(String lat, String lng, String feature) {
		String assured = cmsHelper.getWpOptionsValue(SANDConstants.SwiggyAssuredWpOption[1]);
		String assuredCity = cmsHelper.getWpOptionsValue(SANDConstants.SwiggyAssuredWpOption[2]);
		String CBAmount = headerHelper.JsonString(assured, "$.0.swiggy_select_cashback_max_amount");
		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		String node = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.SWIGGY_ASSURED.swiggy_select_cashback_max_amount").replace("[", "").replace("]", "");
		Assert.assertEquals(node, CBAmount, "Cash back amount is different");
	}

	@Test(dataProvider = "locationBasedPOPAssured", description = "Verify smoke test for LocationBasedFeature ")
	public void locationFeatureSmokeTest(String lat, String lng, String feature) {
		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), SANDConstants.responseCode,"Response code is different");
		Assert.assertEquals(statusCode, SANDConstants.StatusCode,"Status code is different");
		Assert.assertEquals(statusMessage, SANDConstants.msg,"Status message is different");
	}

	@Test(dataProvider = "locationBasedPOPEnabled", description = "Verify if swiggy pop node exists ")
	public void locationFeaturePOP(String lat, String lng, String feature) {
		Processor processor = helper.getLocationBasedFeature(lat, lng, feature);
		boolean isPOP = processor.ResponseValidator.DoesNodeExists("$.data.SWIGGY_POP", null);
		Assert.assertEquals(isPOP, true,"POP is disabled");
	}

	@Test(dataProvider = "settings", description = "Verify the smoke test for settings API ")
	public void settingsTest(String opt1, String val1, String opt2, String val2) {
		Processor processor = helper.getSettings(opt1, val1, opt2, val2);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), SANDConstants.responseCode,"Response code is different");
		Assert.assertEquals(statusCode, SANDConstants.StatusCode,"Status code is different");
		Assert.assertEquals(statusMessage, SANDConstants.msg,"Status message is different");
	}

	@Test(dataProvider = "settings", description = "Verify the response for valid option key for settings API ")
	public void validsettingsTest(String opt1, String val1, String opt2, String val2) {
		Processor processor = helper.getSettings(opt1, val1, opt2, val2);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String CMSvalue1 = cmsHelper.getWpOptionsValue(opt1);
		String value1 = JsonPath.read(resp, "$.data." + SANDConstants.options[0]);
		boolean flag1 = CMSvalue1.equalsIgnoreCase("Key Not Found");
		if (flag1 == false)
		{
			Assert.assertEquals(value1, CMSvalue1);
		} else
		{
			Assert.fail();
		}
	}

	@Test(dataProvider = "settings", description = "Verify the  response for invalid option key for settings API ")
	public void  invalidsettingsTest(String opt1, String val1, String opt2, String val2) {
		Processor processor = helper.getSettings(opt1, val1, opt2, val2);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String CMSValue2 = cmsHelper.getWpOptionsValue(opt2);
		String value2 = JsonPath.read(resp, "$.data." + SANDConstants.options[1]);
		boolean flag2 = CMSValue2.equalsIgnoreCase("Key Not Found");
		if (flag2 == true) {
			Assert.assertEquals(value2, SANDConstants.values[1]);
		}
	}

	@Test(dataProvider = "launch", description = "Verify API")
	public void launch(String experiment, String mobile, String password, String itemId, String restId, String quantity) throws Exception {
    String address = cmsHelper.getWpOptionsValue("address_default_schedule");
    System.out.println(address);
		String day = helper.day("EE");
		List Annotation = JsonPath.read(address,"$.."+day);
		Processor processor = helper.launch(experiment, mobile, password, itemId, restId, quantity);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String annotation = JsonPath.read(resp, "$.data.default_address.annotation").toString().replace("[", "").replace("]", "").toLowerCase();
	    Assert.assertTrue(Annotation.contains(annotation),"There is a different annotation");
	}

	@Test(dataProvider = "launch",  description = "Verify the smoke data for launch API")
	public void launchSmoke(String experiment, String mobile, String password, String itemId, String restId, String quantity) throws Exception {
		Processor processor = helper.launch(experiment, mobile, password, itemId, restId, quantity);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		String statusMessage = JsonPath.read(resp, "$.statusMessage");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), SANDConstants.responseCode);
		Assert.assertEquals(statusCode, SANDConstants.StatusCode);
		Assert.assertEquals(statusMessage, SANDConstants.msg);
	}

	@Test(dataProvider = "launch",  description = "Verify the smoke data for launch API")
	public void launchTrack(String experiment, String mobile, String password, String itemId, String restId, String quantity) throws Exception {
		Processor processor = helper.launch(experiment, mobile, password, itemId, restId, quantity);
		String resp = processor.ResponseValidator.GetBodyAsText();
		List orderIds = JsonPath.read(resp, "$.data.track.orders..order_id");
		Assert.assertTrue(orderIds.contains(orderIds.get(orderIds.size() - 1)), "Order id is different");
		List trackable = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), ("$.data.track.orders..trackable"));
		for (int i = 0; i < trackable.size(); i++) {
			Assert.assertEquals(trackable.get(i).toString(), "1","order is not trackable");
		}
	}

	@Test(dataProvider = "launch", description = "Verify the feedback data for launch API")
	public void launchFeedbackTest(String experiment, String mobile, String password, String itemId, String restId, String quantity) throws Exception {
		SoftAssert softAssert = new SoftAssert();
		String resp = helper.launch(experiment, mobile, password, itemId, restId, quantity).ResponseValidator.GetBodyAsText();
		List orderIds = JsonPath.read(resp, "$.data.track.orders..order_id");
		/*
		 * Marking an Order as Delivered
		 */
		String oId = orderIds.get(orderIds.size() - 1).toString();
		deliveryhelper.makeDEActive(DeliveryConstant.snded_id);
		deliveryhelper.makeDEFree(DeliveryConstant.snded_id);
		deliveryhelper.processOrderInDelivery(oId,"delivered",DeliveryConstant.snded_id);

			Processor processornew = helper.getLaunchFeedback(SANDConstants.experimentName);
			String response = processornew.ResponseValidator.GetBodyAsText();
			List feedback = JsonPath.read(response, "$.data.feedback.orders..order_id");
			Assert.assertEquals(feedback.get(feedback.size() - 1).toString(), oId);
			List order_status = JsonPath.read(response, "$.data.feedback.orders..order_delivery_status");
			Assert.assertEquals(order_status.get(order_status.size() - 1).toString(), "delivered","Not delivered");
	}

    @Test(dataProvider = "30MinsOrFree",description = "Verify swiggy select restaurant on aggregator response")
	public void swiggyAssuredAggregator(String lat, String lng){
		String restId = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setIsSwiggySelect(restId,1);
		cmsHelper.setIsSwiggyAssured(restId,1);
		helper.setRestaurantMetaInfo(restId,"{\"configuration\":{\"menu\":{},\"cart\":{},\"payment\":{},\"tracking\":{},\"order\":{},\"delivery\":{},\"analytics\":{},\"assured\":{\"distance\":\"5.5\"}},\"messages\":{}}");
		String response = helper.aggregator(new String[]{lat, lng}).ResponseValidator.GetBodyAsText();
		String select = JsonPath.read(response,"$.data.restaurants."+restId+"..select").toString().replace("[","").replace("]","");
        Assert.assertEquals(select,"true","Restaurant is not swiggy assured");
	}

	@Test(dataProvider = "30MinsOrFree",description = "Verify swiggy select restaurant on menu response")
	public void swiggyAssuredMenu(String lat, String lng){
		String restId = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setIsSwiggySelect(restId,1);
		cmsHelper.setIsSwiggyAssured(restId,1);
		helper.setRestaurantMetaInfo(restId,"{\"configuration\":{\"menu\":{},\"cart\":{},\"payment\":{},\"tracking\":{},\"order\":{},\"delivery\":{},\"analytics\":{},\"assured\":{\"distance\":\"5.5\"}},\"messages\":{}}");
		Processor processor = edvoHelper.getMenuV4(restId,lat,lng);
		String response = processor.ResponseValidator.GetBodyAsText();
		String select = JsonPath.read(response,"$.data.select").toString();
		Assert.assertEquals(select,"true","Restaurant is not swiggy assured");
	}

	@Test(dataProvider = "30MinsOrFreeNegative",description = "Verify swiggy select restaurant on menu response when distance of customer is far")
	public void swiggyAssuredMenuNegative(String lat, String lng){
		String restId = helper.Aggregator(new String[]{lat, lng});
		cmsHelper.setIsSwiggySelect(restId,1);
		cmsHelper.setIsSwiggyAssured(restId,1);
		helper.setRestaurantMetaInfo(restId,"{\"configuration\":{\"menu\":{},\"cart\":{},\"payment\":{},\"tracking\":{},\"order\":{},\"delivery\":{},\"analytics\":{},\"assured\":{\"distance\":\"5.5\"}},\"messages\":{}}");
		Processor processor = edvoHelper.getMenuV4(restId,lat,lng);
		String response = processor.ResponseValidator.GetBodyAsText();
		String select = JsonPath.read(response,"$.data.select").toString();
		Assert.assertEquals(select,"false","Restaurant is not far from customer latlng");
	}


	}
