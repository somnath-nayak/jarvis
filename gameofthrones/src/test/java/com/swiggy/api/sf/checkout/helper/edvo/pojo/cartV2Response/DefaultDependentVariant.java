package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "group_id",
        "variation_id"
})
public class DefaultDependentVariant {

    @JsonProperty("group_id")
    private String groupId;
    @JsonProperty("variation_id")
    private String variationId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public DefaultDependentVariant() {
    }

    /**
     *
     * @param groupId
     * @param variationId
     */
    public DefaultDependentVariant(String groupId, String variationId) {
        super();
        this.groupId = groupId;
        this.variationId = variationId;
    }

    @JsonProperty("group_id")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("variation_id")
    public String getVariationId() {
        return variationId;
    }

    @JsonProperty("variation_id")
    public void setVariationId(String variationId) {
        this.variationId = variationId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("groupId", groupId).append("variationId", variationId).append("additionalProperties", additionalProperties).toString();
    }

}