package com.swiggy.api.sf.rng.helper;
import java.sql.*;
import com.swiggy.api.sf.rng.constants.SegmentationConstants;
import com.swiggy.api.sf.rng.pojo.ads.Segmentation.UserSegment;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;

public class SegmentationDbHelper {
    
    static String insertDeviceDetail = "INSERT INTO `device_user_map`  (device_id, user_id ,updated_on) VALUES (\"#1\" , #2 , \"#3\");";
    
    static String insertUserSegmentData = "INSERT INTO `user_segment` ( `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `cancellation_probability`, `churn_probability`, `contact_affinity`, `high_end_segment`, `marketing_segment`, `premium_segment`, `user_id`, `value_segment`, `igcc_segment`) VALUES ( 1, \"2018-05-13 06:20:13\", \"Ankita\", \"2018-05-13 06:20:13\", \"Ankita\", 0, 0, 0, 0, ?, ?, ?, ?, ?);";
    
//    public static void insertOrderDetails(String orderId, String userId, String orderBody) {
//        TDCachingDbHelper.replaceThreeWords(swiggyOrder, orderId, userId, orderBody);
//
//    }
    
    public static void setDeviceId(String deviceId, String userId, String date) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(SegmentationConstants.segmentation);
        sqlTemplate.execute(TDCachingDbHelper.replaceThreeWords(insertDeviceDetail, deviceId, userId, date));
    }
    
    public static void setUserSegmentDataInDB(UserSegment userSegment) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(SegmentationConstants.segmentation);
        String query=insertUserSegmentData;
      //  String changedUserString = query.replace("'","''");
        System.out.println("Premium segment value is --------->>   " +userSegment.getPremiumSegment());
         sqlTemplate.execute(query,new PreparedStatementCallback<Boolean>(){
            @Override
            public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
                
                ps.setString(1,userSegment.getMarketingSegment());
                ps.setInt(2,userSegment.getPremiumSegment());
                ps.setString(3,userSegment.getUserId());
                ps.setString(4,userSegment.getValueSegment());
                ps.setString(5,userSegment.getIgccSegment());
                return ps.execute();
            
            }
        });
    
    }
    
    }
