package com.swiggy.api.sf.checkout.helper;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.Random;

public class CommonAPIHelper {
	
	public static JsonNode convertStringtoJSON(String response)
			throws IOException
			 {
		JsonNode obj1 = null;
		try {
			JsonFactory factory = new JsonFactory();
			JsonParser jp1 = factory.createJsonParser(response);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
			mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS,
					true);
			obj1 = mapper.readTree(jp1);
		} catch (Exception e) {
		         e.printStackTrace();
		}

		return obj1;
	}


	public int getRandomNo(int minimum,int maximum){
		Random rn = new Random();
		int range = maximum - minimum + 1;
		int randomNum =  rn.nextInt(range) + minimum;
		return randomNum;
	}

	public String getTimeStamp() {
		long ts = System.nanoTime();
		String timeStamp = String.valueOf(ts);
		return timeStamp;
	}

}
