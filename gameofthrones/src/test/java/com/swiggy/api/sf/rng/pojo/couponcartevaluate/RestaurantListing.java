package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class RestaurantListing {
	@JsonProperty("area")
	private Area area;
	@JsonProperty("city")
	private City city;

	@JsonProperty("area")
	public Area getArea() {
		return area;
	}

	@JsonProperty("area")
	public void setArea(Area area) {
		this.area = area;
	}

	public RestaurantListing withArea(Area area) {
		this.area = area;
		return this;
	}

	@JsonProperty("city")
	public City getCity() {
		return city;
	}

	@JsonProperty("city")
	public void setCity(City city) {
		this.city = city;
	}

	public RestaurantListing withCity(City city) {
		this.city = city;
		return this;
	}
}
