package com.swiggy.api.sf.snd.pojo.DDAggregator;



import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class CollectionData {

    @JsonProperty("RESTAURANT_LISTING")
    private RESTAURANTLISTING RESTAURANT_LISTING;
    @JsonProperty("COLLECTION")
    private COLLECTION COLLECTION;
    @JsonProperty("CAROUSEL")
    private CAROUSEL CAROUSEL;
    @JsonProperty("COLLECTIONV2")
    private COLLECTIONV2 COLLECTIONV2;

    public RESTAURANTLISTING getRESTAURANTLISTING() {
        return RESTAURANT_LISTING;
    }

    public void setRESTAURANTLISTING(RESTAURANTLISTING rESTAURANTLISTING) {
        this.RESTAURANT_LISTING = rESTAURANTLISTING;
    }

    public COLLECTION getCOLLECTION() {
        return COLLECTION;
    }

    public void setCOLLECTION(COLLECTION cOLLECTION) {
        this.COLLECTION = cOLLECTION;
    }

    public CAROUSEL getCAROUSEL() {
        return CAROUSEL;
    }

    public void setCAROUSEL(CAROUSEL cAROUSEL) {
        this.CAROUSEL = cAROUSEL;
    }

    public COLLECTIONV2 getCOLLECTIONV2() {
        return COLLECTIONV2;
    }

    public void setCOLLECTIONV2(COLLECTIONV2 cOLLECTIONV2) {
        this.COLLECTIONV2 = cOLLECTIONV2;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("RESTAURANT_LISTING", RESTAURANT_LISTING).append("COLLECTION", COLLECTION).append("CAROUSEL", CAROUSEL).append("COLLECTIONV2", COLLECTIONV2).toString();
    }

}