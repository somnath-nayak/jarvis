package com.swiggy.api.sf.checkout.helper.sanityHelper;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.swiggy.api.sf.checkout.helper.CafeHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutAPIHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.Reporter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class SanityHelper {
    EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
    CheckoutAPIHelper checkoutAPIHelper = new CheckoutAPIHelper();
    CafeHelper cafeHelper=new CafeHelper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();

    public HashMap<String, String> login(String mobile, String password) {
        return edvoCartHelper.getHeaders(mobile, password);
    }

    public HashMap<String, String> setHeaders(String tid, String token, String userAgent, String versionCode, String swuid) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("tid", tid);
        headers.put("token", token);
        headers.put("User-Agent", userAgent);
        headers.put("Version-Code", versionCode);
        headers.put("swuid", swuid);
        return headers;
    }

    public void JSONSchemaValidator(Processor processor, String jsonSchemaFilePath) throws IOException, ProcessingException {
        String response = processor.ResponseValidator.GetBodyAsText();
        String userDir = System.getProperty("user.dir");
        userDir = userDir.substring(0, userDir.indexOf("gameofthrones"));
        String jsonSchema = new ToolBox().readFileAsString(userDir + jsonSchemaFilePath);
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonSchema, response);
        Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(response);
        Reporter.log("Contain empty nodes => " + isEmpty, true);
        Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
    }

    public Processor cartUpdate(HashMap<String, String> headers,
                                String restaurantId,
                                boolean isCartItemRequired,
                                int cartItemQuantity,
                                boolean isAddonRequiredForCartItems,
                                boolean isMealItemRequired,
                                int mealItemQuantity,
                                boolean isAddonRequiredForMealItems,
                                String[] mealIds,
                                String cartType){
        Cart cart = edvoCartHelper.getCartPayload(restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
        String payload = Utility.jsonEncode(cart);
        return edvoCartHelper.createCartV2(payload, headers);
    }

    public Processor getCart(HashMap<String, String> headers){
        return checkoutAPIHelper.getCart(headers);
    }

    public Processor checkTotals(HashMap<String, String> headers,
                                 String restaurantId,
                                 boolean isCartItemRequired,
                                 int cartItemQuantity,
                                 boolean isAddonRequiredForCartItems,
                                 boolean isMealItemRequired,
                                 int mealItemQuantity,
                                 boolean isAddonRequiredForMealItems,
                                 String[] mealIds,
                                 String cartType) {
        Cart cart = edvoCartHelper.getCartPayload(restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
        String payload = Utility.jsonEncode(cart);
        return checkoutAPIHelper.checkTotalCart(payload, headers);
    }

    public Processor checkBankListAll(HashMap<String, String> headers) {

        return checkoutAPIHelper.checkBankListAll(headers);
    }

    public Processor checkCardsJuspay(HashMap<String, String> headers){

        return checkoutAPIHelper.checkCardsJuspay(headers);
    }

    public Processor checkApplyCoupon(HashMap<String, String> headers,
                                      String coupon_code) {

        String payload = coupon_code;
        return checkoutAPIHelper.checkApplyCoupon(payload, headers);
    }

    public Processor checkRemoveCoupon(HashMap<String, String> headers,
                                       String coupon_code) {

        String payload = coupon_code;
        return checkoutAPIHelper.checkRemoveCoupon(payload, headers);
    }

    public Processor checkGetInventory(HashMap<String, String> headers) {

        return checkoutAPIHelper.checkGetInventory(headers);
    }

    public Processor checkSettingsWalletEmailUpdateURL(HashMap<String, String> headers) {

        return checkoutAPIHelper.checkSettingsWalletEmailUpdateURL(headers);
    }

    public Processor checkGetAllPayment(HashMap<String, String> headers) {

        return checkoutAPIHelper.checkGetAllPayment(headers);
    }

    public Processor checkCafeUpdateCart(String tid, String token, String menu_item_id, String quantity, String restId, String cart_type) {

        return cafeHelper.updateCartCafe(tid, token, menu_item_id,quantity,restId,cart_type);
    }
}
