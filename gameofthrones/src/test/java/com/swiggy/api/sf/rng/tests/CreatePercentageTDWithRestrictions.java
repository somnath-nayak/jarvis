package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.rng.pojo.Category;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;
import com.swiggy.api.sf.rng.pojo.Menu;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.Slot;
import com.swiggy.api.sf.rng.pojo.SubCategory;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class CreatePercentageTDWithRestrictions {
	
	getObject go = new getObject();
	static 	int i =0;
	
	public String getPercentageTD(String restrictionType,String level) throws IOException
	{
		i++;
	    List<RestaurantList> restaurantList = new ArrayList<>();
	    JsonHelper jsonHelper = new JsonHelper();
	    CreateTdEntry percetageTradeDiscount = null ;
	    switch(level)
		{
		
		case "Restaurant"  : restaurantList.add(go.getRestaurantObject());break;
		case "Category"    : restaurantList.add(go.getCategoryObject());break;
		case "Subcategory" : restaurantList.add(go.getsubCategoryObject());break;
		case "Item"        : restaurantList.add(go.getItemObject());break;
		}
		
		
	
	{
		switch(restrictionType)
		{
		
		    case "SFO":  percetageTradeDiscount = new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;

		    case "RFO":  percetageTradeDiscount = new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();
                    break;

		    case "NR":  percetageTradeDiscount = new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false).restaurantFirstOrder(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;


		    case "UR":  percetageTradeDiscount =  new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(true)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false).restaurantFirstOrder(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;

		    case "DU": percetageTradeDiscount = new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false).restaurantFirstOrder(false)
					.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").build();break;

		    case "TR": List<Slot> slots = new ArrayList<>();
			          slots.add(new Slot("2350", "ALL", "0005"));
		    	     percetageTradeDiscount = new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(false).slots(slots)
					.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false).restaurantFirstOrder(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;

		}     
		      return   jsonHelper.getObjectToJSON(percetageTradeDiscount);
	}
	}

	
	public String getPercentageTD(String restrictionType,String level,List<RestaurantList> restaurantList) throws IOException
	{
		i++;
	   
	    JsonHelper jsonHelper = new JsonHelper();
	    CreateTdEntry percetageTradeDiscount = null ;
	 
		
	
	{
		switch(restrictionType)
		{
		
		    case "SFO":  percetageTradeDiscount = new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;

		    case "RFO":  percetageTradeDiscount = new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();


		    case "NR":  percetageTradeDiscount = new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false).restaurantFirstOrder(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;


		    case "UR":  percetageTradeDiscount =  new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(true)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false).restaurantFirstOrder(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();break;

		    case "DU": percetageTradeDiscount = new CreateTdBuilder().nameSpace("TestDataDiscount")
					.header("TestDataDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 50).toInstant().getEpochSecond() * 1000))
					.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Manu")
					.discountLevel(level).restaurantList(restaurantList)
				    .ruleDiscount("Percentage", level, "10", "0", "2000").userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false).restaurantFirstOrder(false)
					.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").build();break;

		}     
		      return   jsonHelper.getObjectToJSON(percetageTradeDiscount);
	}
	}

	
	
}
