package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.OrderPlace;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SearchTest extends SnDDp {
    SANDHelper sandHelper= new SANDHelper();
    OrderPlace orderPlace= new OrderPlace();
    RngHelper rngHelper = new RngHelper();
    RestaurantHelper restaurantHelper= new RestaurantHelper();
    CMSHelper cmsHelper = new CMSHelper();

    List<String> restList= new ArrayList<>();
    List<String> restIds= new ArrayList<>();


  /*  @BeforeSuite
    public void createRest() throws IOException,InterruptedException{
        Random random = new Random();

        int  n = random.nextInt(79) + 1;
        String restName="Test78"+String.valueOf(n);
        sandHelper.esUpdate("test_index29"+String.valueOf(n));
        for(int i=0; i<6; i++) {
            int rest = restaurantHelper.createDefaultRestaurant(restName+i);
            System.out.println(rest);
            int catt = restaurantHelper.createCat(rest);
            int subCat = restaurantHelper.createsubcat(rest, catt);
            System.out.println(rest + "cat" + catt + "sub" + subCat);
            restaurantHelper.enableRestaurant(rest, "12.9326, 77.6036");
            restaurantHelper.pushRestaurantEvent(String.valueOf(rest));
            restaurantHelper.createItem(rest, catt, subCat);
            sandHelper.solrIndexing(String.valueOf(rest));
            restList.add(restName+i);
            restIds.add(String.valueOf(rest));
            Thread.sleep(4000);
        }
    }*/
  String[] latLng =  {"12.9165757","77.6101163"};


    @BeforeClass
    public void getServiceableAndOpenRestaurant(){
        restIds = sandHelper.aggregatorRestListSearch(latLng);
        Assert.assertFalse(restIds.isEmpty(),"Assertion Failed :: No serviceable and open rest found.");
    }

    @Test(description = "create a test rest and search it")
    public void searchTestNewRest(){
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        String searchResponse=sandHelper.searchV2(keyword,"12.9165757","77.6101163", null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(searchResponse,"$.data.restaurants..id").split(",")).contains(restId), "no rest ids are present");
    }

    @Test(description = "create a test rest and search it", dataProvider = "freeDelSearch")
    public void searchTestNewRestFreeDel(String lat, String lng,String freeDel){
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        HashMap<String, String> hMap;
        try {
            hMap = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId,false, false, false, "ZERO_DAYS_DORMANT", false);
            String getTid= RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        sandHelper.solrIndexing(String.valueOf(restId));
        String searchResponse= sandHelper.SearchV2(lat,lng,keyword).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchResponse,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }

    @Test(description = "create a test rest and search it", dataProvider = "perSearch")
    public void searchTestNewRestPercentage(String lat, String lng,String percentage){
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        HashMap<String, String> hMap;
        try {
            hMap = rngHelper.createPercentageWithNoMinAmountAtRestaurantLevel(restId);
            String getTid = RngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), percentage, "No discounts present or different type of discount is there");

        } catch (Exception e) {
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        sandHelper.solrIndexing(String.valueOf(restId));
        String response = sandHelper.searchV2(lat, lng,keyword).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(response, "$.data..tradeCampaignHeaders..discountType").contains(percentage), "No off on the product");
    }

    @Test(description = "create a test rest and search it", dataProvider = "flatSearch")
    public void searchTestNewRestFlat(String lat, String lng,String flat){
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        HashMap<String, String> hMap;
        try {
            hMap=rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", restId,"10");
            String getTid= RngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), flat, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        sandHelper.solrIndexing(String.valueOf(restId));
        String response = sandHelper.searchV2(lat, lng,keyword).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(response, "$.data..tradeCampaignHeaders..discountType").contains(flat), "No off on the product");
    }

    @Test(description = "create a test rest and search it", dataProvider = "freebieSearch")
    public void searchTestNewRestFreebie(String lat, String lng,String freebie){
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        HashMap<String, String> hMap;
        sandHelper.TMenu(restId);
        String item = orderPlace.getItemFromMenu(restId, null, null);
        System.out.println("itemddd"+item);
        try {
            hMap = rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel("10", restId, item);
            String getTid = RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), freebie, "No discounts present or different type of discount is there");
        } catch (Exception e) {
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        sandHelper.solrIndexing(String.valueOf(restId));
        String response = sandHelper.searchV2(lat, lng,keyword).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(response, "$.data..tradeCampaignHeaders..discountType").contains(freebie), "No off on the product");
    }

    @Test(dataProvider = "multi", description ="apply flat and free del on a rest id and verify the same on menu")
    public void multiTdSearchFlat(String lat ,String lng){
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);

        try {
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", restId,"10", false);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, false, false, "ZERO_DAYS_DORMANT", false, false);
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        sandHelper.solrIndexing(String.valueOf(restId));
        String searchAfterDiscount=sandHelper.searchV2(keyword,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }

    @Test(dataProvider = "multi", description ="apply per and free del on a rest id and verify the same on menu" )
    public void multiTdSearchPer(String lat ,String lng) throws InterruptedException{
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        try {
            rngHelper.createPercentageTD(restId);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restId, false, false, false, "ZERO_DAYS_DORMANT", false);
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        sandHelper.solrIndexing(String.valueOf(restId));
        Thread.sleep(2000);
        String searchAfterDiscount=sandHelper.searchV2(keyword,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(sandHelper.JsonString(searchAfterDiscount,"$.data..tradeCampaignHeaders..discountType").contains(freeDel), "No free delivery on the product");
    }

    @Test(dataProvider = "LatLong", description = "Verify cuisine of restaurants with db")
    public void cuisine(String lat, String lng) {
        String resp = sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(statusCode, 0, "status code is 1");
        Assert.assertEquals(statusMessage, "done successfully");
        List<String> restIds = Arrays.asList(sandHelper.JsonString(resp, "$.data.restaurants[*].id").split(","));
        HashMap<String, List<String>> restaurantCuisine = new HashMap<>();
        for (int i = 0; i < restIds.size(); i++) {
            List<String> cuisine = Arrays.asList(sandHelper.JsonString(resp, "$.data.restaurants." + restIds.get(i) + ".cuisines"));
            restaurantCuisine.put(restIds.get(i), cuisine);
            List<Map<String, Object>> l = sandHelper.cuisine(restIds.get(i));
            Assert.assertEquals(restaurantCuisine.get(restIds.get(i)).toString().replaceAll("\\]|\\[", ""), l.get(0).get("cuisine"), "Response and db does not matches");
        }
    }

    @Test(dataProvider = "searchByCuisine",description ="verify all nodes in search response", groups = {"Search"})
    public void searchNodes(String lat, String lng, String cuisine){
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        sandHelper.solrIndexing(String.valueOf(restId));
        String searchResponse= sandHelper.SearchV2(lat,lng,keyword).ResponseValidator.GetBodyAsText();
        List<String> op=Arrays.asList(SANDConstants.nodes);
        List<String> type= Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..type").split(","));
        List<String> restaurantNodes= Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..restaurants").split(","));
        for (int i=0; i<type.size(); i++) {
            Assert.assertTrue(op.contains(type.get(i)), "type is not present");
            Assert.assertTrue(Optional.of(restaurantNodes.get(i)).isPresent(), "restaurant node is not present");
        }
    }


    @Test(dataProvider = "searchByCuisine",description ="verify all nodes in search response")
    public void searchParamInItemList(String lat, String lng, String cuisine) {
        String searchResponse = sandHelper.SearchV2(lat, lng, "pizza").ResponseValidator.GetBodyAsText();
        String items =sandHelper.JsonString(searchResponse, "$.data.items");
        Pattern pattern = Pattern.compile(",");
        List<String> list = pattern.splitAsStream(items)
                .map(String::valueOf)
                .collect(Collectors.toList());
        Assert.assertTrue(list.contains("pizza"), "item is not found");
    }


    @Test(dataProvider = "searchByCuisine",description ="verify dish sugesstion with 2 or more words in search response")
    public void searchParam2Words(String lat, String lng, String cuisine) {
        String searchResponse = sandHelper.SearchV2(lat, lng, "panner+paratha").ResponseValidator.GetBodyAsText();
        String items = sandHelper.JsonString(searchResponse, "$.data.items");
        Pattern pattern = Pattern.compile(",");
        List<String> list=pattern.splitAsStream(items)
                .map(String::valueOf)
                .collect(Collectors.toList());
        //Assert.assertEquals();
    }

    @Test(dataProvider = "searchV2Page",description ="Search by dishItem and verify in response")
    public void searchNoDishSuggestion(String lat, String lng, String dishItem, String page, String item){
        String searchResponse= sandHelper.SearchV2(lat,lng, dishItem,page, item).ResponseValidator.GetBodyAsText();
        List<String> list=Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.items").split(","));
        Assert.assertTrue(list.contains(null) || list.contains(""), "item is present in menu Items of the rest");
    }


    @Test(dataProvider = "searchByCuisine",description ="verify rating  search response")
    public void searchAvgRating(String lat, String lng, String cuisine) {
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        sandHelper.solrIndexing(String.valueOf(restId));
        String searchResponse = sandHelper.SearchV2(lat, lng, keyword).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..avg_rating").split(",")).size()>0, "avg rating is not present");

    }

    @Test(dataProvider = "searchByCuisine",description ="verify costfortwo in search response")
    public void searchCostForTwo(String lat, String lng, String cuisine) {
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        sandHelper.solrIndexing(String.valueOf(restId));
        String searchResponse = sandHelper.SearchV2(lat, lng, keyword).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..costForTwoMessage").split(",")).size()>0, "avg rating is not present");

    }

    @Test(dataProvider = "searchByCuisine",description ="verify cuisine in search response")
    public void searchForCuisine(String lat, String lng, String cuisine) {
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        sandHelper.solrIndexing(String.valueOf(restId));
        String searchResponse = sandHelper.SearchV2(lat, lng, keyword).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..cuisine").split(",")).size()>0, "avg rating is not present");

    }


    @Test(dataProvider = "searchByCuisine",description ="verify long distance in search response")
    public void searchForLongDistance(String lat, String lng, String cuisine) {
        String restId = restIds.get(0);
        Map restDetails=cmsHelper.getRestaurantDetails(restId);
        List<String> keys = Arrays.asList(restDetails.get("name").toString().split(""));
        String keyword = keys.get(0)+keys.get(1)+keys.get(2);
        sandHelper.solrIndexing(String.valueOf(restId));
        String searchResponse = sandHelper.SearchV2(lat, lng, keyword).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..cuisine").split(",")).size()>0, "avg rating is not present");

    }

    @Test(dataProvider = "rainMode", description = "apply a rain mode and verify the rain mode on aggregator")
    public void searchRainMode(String lat, String lng,String rainMode, String removeRainMode, String heavy) throws InterruptedException{
        String response=sandHelper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> restIds= Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..id").split(","));
        List<String> zoneId = Arrays.asList(sandHelper.JsonString(response, "$.data.restaurants..areaId").split(","));
        sandHelper.rainMode(rainMode, zoneId.get(0));
        List<Integer> list = new ArrayList<>();
        for(int i=0; i<zoneId.size(); i++){
            if(zoneId.get(0).equals(zoneId.get(i))){
                list.add(i);
            }
        }
        Thread.sleep(1000);
        List<String> op=Arrays.asList(SANDConstants.nodes);
        String re= sandHelper.menuV4RestId(restIds.get(list.get(1)),lat,lng).ResponseValidator.GetBodyAsText();
        String name=sandHelper.JsonString(re,"$.data.name").replace(" ","+");
        sandHelper.solrIndexing(restIds.get(list.get(1)));
        String searchResponse=sandHelper.searchV2(name,lat,lng, null, null).ResponseValidator.GetBodyAsText();
        try{
            Assert.assertTrue(!Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.restaurants..type").split(",")).isEmpty(), "rain mode is not heavy");
        }catch (Exception e){
            sandHelper.rainMode(removeRainMode, zoneId.get(0));
            Assert.assertTrue(false, "rain mode is not applied");
        }

    }

    @Test(dataProvider = "searchByCuisine",description ="verify search query is present atleast one in item suggestion")
    public void searchValidString(String lat, String lng, String cuisine) {
        String product = "pizza";
        String searchResponse = sandHelper.SearchV2(lat, lng, product).ResponseValidator.GetBodyAsText();
        List<String> myList=Arrays.asList(sandHelper.JsonString(searchResponse, "$.data.items").split(","));
            Assert.assertTrue(myList.contains(product), "data is not present");
        }



    }





