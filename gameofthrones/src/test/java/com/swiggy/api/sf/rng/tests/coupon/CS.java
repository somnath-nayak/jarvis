package com.swiggy.api.sf.rng.tests.coupon;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;


import com.swiggy.api.sf.rng.constants.CopyConstants;
import com.swiggy.api.sf.rng.dp.coupondp.*;
import com.swiggy.api.sf.rng.pojo.PaymentCodeMappingPOJO;
import com.swiggy.api.sf.rng.constants.CouponConstants;
import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.dp.MultiApplyDP;

import com.swiggy.api.sf.rng.dp.coupondp.CouponSuperDP;
import com.swiggy.api.sf.rng.helper.*;
import com.swiggy.api.sf.rng.pojo.ApplyCouponPOJO;
import com.swiggy.api.sf.rng.pojo.CouponUsageCountRemovePOJO;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.CreateMultiApplyCouponPOJO;
import com.swiggy.api.sf.rng.tests.RandomNumber;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.rng.helper.*;
import com.swiggy.api.sf.rng.pojo.CouponTimeSlotPOJO;
import com.swiggy.api.sf.rng.pojo.*;
import com.swiggy.api.sf.rng.pojo.CouponUsageCountRemovePOJO;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.apache.commons.lang3.StringEscapeUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import scala.concurrent.java8.FuturesConvertersImpl;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import static com.swiggy.api.erp.delivery.constants.ClusterConstants.cityId;
import static com.swiggy.api.sf.rng.helper.RngHelper.getCurrentDate;
import static com.swiggy.api.sf.rng.helper.RngHelper.getDateToSetInCouponUseCountDate;
import static com.swiggy.api.sf.rng.helper.RngHelper.getOneMonthAhead;

public class CS extends DP {
    HashMap<String, String> requestHeaders = new HashMap<String, String>();
    GameOfThronesService service;
    Processor processor;
    RngHelper rngHelper = new RngHelper();
    DBHelper dbHelper = new DBHelper();
    RedisHelper redisHelper = new RedisHelper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    SuperHelper superHelper = new SuperHelper();
    SANDHelper sandHelper = new SANDHelper();

    CouponRedisHelper couponRedisHelper = new CouponRedisHelper();
    CouponUsageCountRemovePOJO couponUsageCountRemovePOJO = new CouponUsageCountRemovePOJO();

    @Test(dataProvider = "createCouponData", dataProviderClass = DP.class)
    public void createCoupon(String coupon_type, String name, String code, String description, String is_private,
                             String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                             String customer_restriction, String city_restriction, String area_restriction,
                             String restaurant_restriction, String category_restriction, String item_restriction,
                             String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                             String first_order_restriction, String preferred_payment_method, String user_client,
                             String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                             String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                             String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                             String supported_ios_version, String updated_by, String title, String tnc, String logo)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String resp = ccProcessor.ResponseValidator.GetBodyAsText();
        System.out.println("Ank " + System.getProperty("user.dir"));
        String couponCode = ccProcessor.ResponseValidator.GetNodeValue("$.data.code");
        // String jsonschema = new ToolBox()
        // .readFileAsString(System.getProperty("user.dir") +
        // "/../Data/SchemaSet/Json/RNG/cs_createcoupon.txt");
        // List<String> missingNodeList =

        // schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
        // Assert.assertTrue(missingNodeList.isEmpty(),
        // missingNodeList + " Nodes Are Missing Or Not Matching For Createcoupon API");
        // boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        // System.out.println("Contain empty nodes => " + isEmpty);
        // Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");

        Assert.assertTrue(ccProcessor.ResponseValidator.DoesNodeExists("$.data.coupon_type", resp));
        Assert.assertTrue(ccProcessor.ResponseValidator.DoesNodeExists("$.data.preferred_payment_method", resp));
        Assert.assertTrue(ccProcessor.ResponseValidator.DoesNodeExists("$.statusMessage", resp));
        Assert.assertEquals("0", couponRedisHelper.getCouponUsageCountRedis(couponCode), "uses count is not matching with redis data");
    }

    @Test(dataProvider = "createCouponDataDiscountAmountNullCheck", dataProviderClass = DP.class, description = "Null validation on discount amount field")
    public void createCouponDataDiscountAmountNullCheck(String coupon_type, String name, String code, String description, String is_private,
                                                        String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                                                        String customer_restriction, String city_restriction, String area_restriction,
                                                        String restaurant_restriction, String category_restriction, String item_restriction,
                                                        String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                                        String first_order_restriction, String preferred_payment_method, String user_client,
                                                        String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                        String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                        String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                                        String supported_ios_version, String updated_by, String title, String tnc, String logo)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String resp = ccProcessor.ResponseValidator.GetBodyAsText();
        System.out.println("Ank " + System.getProperty("user.dir"));
        int statusCode = ccProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = ccProcessor.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertTrue(ccProcessor.ResponseValidator.DoesNodeExists("$.data.coupon_type", resp));
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "discountAmount is Invalid");
    }

    @Test(dataProvider = "createCouponDataWithNegativeScenario", dataProviderClass = DP.class)
    public void createCouponWithNegativeScenario(String coupon_type, String name, String code, String description, String is_private,
                                                 String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                                                 String customer_restriction, String city_restriction, String area_restriction,
                                                 String restaurant_restriction, String category_restriction, String item_restriction,
                                                 String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                                 String first_order_restriction, String preferred_payment_method, String user_client,
                                                 String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                 String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                 String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                                 String supported_ios_version, String updated_by, String title, String tnc, String logo)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String resp = ccProcessor.ResponseValidator.GetBodyAsText();
        System.out.println("Ank " + System.getProperty("user.dir"));
//        String couponCode = ccProcessor.ResponseValidator.GetNodeValue("$.data.code");
        String data = ccProcessor.ResponseValidator.GetNodeValue("$.data");

        // String jsonschema = new ToolBox()
        // .readFileAsString(System.getProperty("user.dir") +
        // "/../Data/SchemaSet/Json/RNG/cs_createcoupon.txt");
        // List<String> missingNodeList =

        // schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
        // Assert.assertTrue(missingNodeList.isEmpty(),
        // missingNodeList + " Nodes Are Missing Or Not Matching For Createcoupon API");
        // boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        // System.out.println("Contain empty nodes => " + isEmpty);
        // Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");

        Assert.assertTrue(ccProcessor.ResponseValidator.DoesNodeExists("$.data.coupon_type", resp));
        Assert.assertTrue(ccProcessor.ResponseValidator.DoesNodeExists("$.data.preferred_payment_method", resp));
        Assert.assertTrue(ccProcessor.ResponseValidator.DoesNodeExists("$.statusMessage", resp));
        Assert.assertEquals(data, null, "getting null.should reflect proper error message");
//        Assert.assertEquals("0",couponRedisHelper.getCouponUsageCountRedis(couponCode),"Expected Failure,getting null.should reflect proper error message");
    }


    @Test(dataProvider = "createDuplicateCouponData", dataProviderClass = DP.class)
    public void createDuplicateCoupon(String coupon_type, String name, String code, String description,
                                      String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                      String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                      String restaurant_restriction, String category_restriction, String item_restriction,
                                      String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                      String first_order_restriction, String preferred_payment_method, String user_client,
                                      String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                      String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                      String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                      String supported_ios_version, String updated_by, String title, String tnc, String logo)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String coupon1_code = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));

        Processor ccProcessor2 = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String resp2 = ccProcessor.ResponseValidator.GetBodyAsText();
        String statusCode = JsonPath.read(resp2, "$.statusMessage");
        String dataSet = JsonPath.read(resp2, "$.data").toString();

        // String status =
        // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
        // String jsonschema2 = new
        // ToolBox().readFileAsString(System.getProperty("user.dir") +
        // "/Data/SchemaSet/Json/RNG/cs_createcoupon.txt");
        // List<String> missingNodeList2 =
        // schemaValidatorUtils.validateServiceSchema(jsonschema2, resp2);
        // Assert.assertEquals(dataSet, null);
        Assert.assertNotNull(dataSet);

    }

    @Test(dataProvider = "createCouponUserMapData", dataProviderClass = DP.class)
    public void createCouponUserMap(String coupon_type, String name, String code, String description, String is_private,
                                    String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                                    String customer_restriction, String city_restriction, String area_restriction,
                                    String restaurant_restriction, String category_restriction, String item_restriction,
                                    String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                    String first_order_restriction, String preferred_payment_method, String user_client,
                                    String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                    String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                    String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                    String supported_ios_version, String updated_by, String userId, String title, String tnc, String logo) {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor cumProcessor = RngHelper.couponUserMap(code, userId, couponId);

    }

    @Test(dataProvider = "createCouponCityMapData", dataProviderClass = DP.class)
    public void createCouponCityMap(String coupon_type, String name, String code, String description, String is_private,
                                    String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                                    String customer_restriction, String city_restriction, String area_restriction,
                                    String restaurant_restriction, String category_restriction, String item_restriction,
                                    String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                    String first_order_restriction, String preferred_payment_method, String user_client,
                                    String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                    String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                    String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                    String supported_ios_version, String updated_by, String cityId, String title, String tnc, String logo) {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor cumProcessor = RngHelper.couponCityMap(code, cityId, couponId);
    }

    @Test(dataProvider = "createCouponItemMapData", dataProviderClass = DP.class)
    public void createCouponItemMap(String coupon_type, String name, String code, String description, String is_private,
                                    String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                                    String customer_restriction, String city_restriction, String area_restriction,
                                    String restaurant_restriction, String category_restriction, String item_restriction,
                                    String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                    String first_order_restriction, String preferred_payment_method, String user_client,
                                    String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                    String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                    String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                    String supported_ios_version, String updated_by, String itemId, String title, String tnc, String logo) {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor cumProcessor = RngHelper.couponCityMap(code, itemId, couponId);
    }

    @Test(dataProvider = "createMultipleCouponItemMapData", dataProviderClass = DP.class)
    public void createMultipleCouponItemMap(String coupon_type, String name, String code, String description,
                                            String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                            String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                            String restaurant_restriction, String category_restriction, String item_restriction,
                                            String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                            String first_order_restriction, String preferred_payment_method, String user_client,
                                            String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                            String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                            String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                            String supported_ios_version, String updated_by, String itemId, String title, String tnc, String logo)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String coupon_code = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        Processor cumProcessor = RngHelper.couponItemMap(coupon_code, itemId, couponId);

        String Mapping_Id = String
                .valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String data = String.valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        System.out.println("Coupon code in mapping API is   " + coupon_code);
        System.out.println("mapping id is   " + Mapping_Id);
        System.out.println("Coupon id is  " + couponId);
        Assert.assertNotNull(data, "Mapping fails");

        // String resp = cumProcessor.ResponseValidator.GetBodyAsText();
        // String jsonschema = new ToolBox()
        // .readFileAsString(System.getProperty("user.dir") +
        // "/Data/SchemaSet/Json/RNG/cs_itemMap.txt");
        // List<String> missingNodeList =
        // schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
        // Assert.assertTrue(missingNodeList.isEmpty(),
        // missingNodeList + " Nodes Are Missing Or Not Matching For
        // MultipleCouponItemMapping API");
        // boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        // System.out.println("Contain empty nodes => " + isEmpty);
        // Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");

    }

    @Test(dataProvider = "createMultipleCouponCityMapData", dataProviderClass = DP.class)
    public void createMultipleCouponCityMap(String coupon_type, String name, String code, String description,
                                            String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                            String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                            String restaurant_restriction, String category_restriction, String item_restriction,
                                            String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                            String first_order_restriction, String preferred_payment_method, String user_client,
                                            String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                            String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                            String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                            String supported_ios_version, String updated_by, String cityId, String title, String tnc, String logo)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String coupon_code = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        Processor cumProcessor = RngHelper.couponCityMap(coupon_code, cityId, couponId);
        String Mapping_Id = String
                .valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String data = String.valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        System.out.println("Coupon code in mapping API is   " + coupon_code);
        System.out.println("mapping id is   " + Mapping_Id);
        Assert.assertNotNull(data, "Mapping fails");
        // String resp = cumProcessor.ResponseValidator.GetBodyAsText();
        // String jsonschema = new
        // ToolBox().readFileAsString(System.getProperty("user.dir") +
        // "/Data/SchemaSet/Json/RNG/cs_cityMap.txt");
        // List<String> missingNodeList =
        // schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
        // Assert.assertTrue(missingNodeList.isEmpty(),
        // missingNodeList + " Nodes Are Missing Or Not Matching For
        // MultipleCouponCityMapping API");
        // boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        // System.out.println("Contain empty nodes => " + isEmpty);
        // Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");

    }

    @Test(dataProvider = "createMultipleCouponUserMapData", dataProviderClass = DP.class)
    public void createMultipleCouponUserMap(String coupon_type, String name, String code, String description,
                                            String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                            String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                            String restaurant_restriction, String category_restriction, String item_restriction,
                                            String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                            String first_order_restriction, String preferred_payment_method, String user_client,
                                            String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                            String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                            String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                            String supported_ios_version, String updated_by, String userId, String title, String tnc, String logo)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String coupon_code = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        Processor cumProcessor = RngHelper.couponUserMap(coupon_code, userId, couponId);
        String Mapping_Id = String
                .valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String data = String.valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        System.out.println("Coupon code in mapping API is   " + coupon_code);
        System.out.println("mapping id is   " + Mapping_Id);
        Assert.assertNotNull(data, "Mapping fails");
        System.out.println("coupon id is  " + couponId);
        // String resp = cumProcessor.ResponseValidator.GetBodyAsText();
        // String jsonschema = new ToolBox()
        // .readFileAsString(System.getProperty("user.dir") +
        // "/Data/SchemaSet/Json/RNG/cs_userMap.txt");
        // List<String> missingNodeList =
        // schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
        // Assert.assertTrue(missingNodeList.isEmpty(),
        // missingNodeList + " Nodes Are Missing Or Not Matching For
        // MultipleCouponUserMapping API");
        // boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        // System.out.println("Contain empty nodes => " + isEmpty);
        // Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
    }

    @Test(dataProvider = "createMultipleCouponRestaurantMapData", dataProviderClass = DP.class)
    public void createMultipleCouponRestaurantMap(String coupon_type, String name, String code, String description,
                                                  String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                                  String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                                  String restaurant_restriction, String category_restriction, String item_restriction,
                                                  String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                                  String first_order_restriction, String preferred_payment_method, String user_client,
                                                  String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                  String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                  String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                                  String supported_ios_version, String updated_by, String restaurantId, String title, String tnc, String logo)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String coupon_code = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        Processor cumProcessor = RngHelper.couponRestaurantMap(coupon_code, restaurantId, couponId);
        String Mapping_Id = String
                .valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String data = String.valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        System.out.println("Coupon code in mapping API is   " + coupon_code);
        System.out.println("mapping id is   " + Mapping_Id);
        System.out.println("coupon id is  " + couponId);
        Assert.assertNotNull(data, "Mapping fails");
        // String resp = cumProcessor.ResponseValidator.GetBodyAsText();
        // String jsonschema = new ToolBox()
        // .readFileAsString(System.getProperty("user.dir") +
        // "/Data/SchemaSet/Json/RNG/cs_restaurantMap.txt");
        // List<String> missingNodeList =
        // schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
        // Assert.assertTrue(missingNodeList.isEmpty(),
        // missingNodeList + " Nodes Are Missing Or Not Matching For
        // MultipleCouponRestaurantMapping API");
        // boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        // System.out.println("Contain empty nodes => " + isEmpty);
        // Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
    }

    @Test(dataProvider = "createMultipleCouponAreaMapData", dataProviderClass = DP.class)
    public void createMultipleCouponAreaMap(String coupon_type, String name, String code, String description,
                                            String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                            String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                            String restaurant_restriction, String category_restriction, String item_restriction,
                                            String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                            String first_order_restriction, String preferred_payment_method, String user_client,
                                            String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                            String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                            String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                            String supported_ios_version, String updated_by, String areaId, String title, String tnc, String logo)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String coupon_code = ccProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.code");
        Processor cumProcessor = RngHelper.couponAreaMap(coupon_code, areaId, couponId);
        String Mapping_Id = String
                .valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String data = String.valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        // String resp = cumProcessor.ResponseValidator.GetBodyAsText();
        // String jsonschema = new ToolBox()
        // .readFileAsString(System.getProperty("user.dir") +
        // "/Data/SchemaSet/Json/RNG/cs_areaMap.txt");
        // List<String> missingNodeList =
        // schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
        // Assert.assertTrue(missingNodeList.isEmpty(),
        // missingNodeList + " Nodes Are Missing Or Not Matching couponUserCity API");
        // boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        // System.out.println("Contain empty nodes => " + isEmpty);
        System.out.println("Coupon code in mapping API is   " + coupon_code);
        System.out.println("mapping id is   " + Mapping_Id);
        Assert.assertNotNull(data, "Mapping fails");
        // Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
    }

    // Private type coupon
    @Test(dataProvider = "couponUserCity", dataProviderClass = DP.class)
    public void getCouponUserCity(String coupon_type, String name, String code, String description, String is_private,
                                  String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                                  String customer_restriction, String city_restriction, String area_restriction,
                                  String restaurant_restriction, String category_restriction, String item_restriction,
                                  String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                  String first_order_restriction, String preferred_payment_method, String user_client,
                                  String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                  String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                  String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                  String supported_ios_version, String updated_by, String userId, String cityId, String type, String title,
                                  String tnc, String logo) throws IOException, ProcessingException {
        if (type == "both") {
            // both User & City Mapped
            Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                    valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                    area_restriction, restaurant_restriction, category_restriction, item_restriction,
                    discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                    preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                    minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                    applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
                    updated_by, title, tnc, logo);
            String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            String coupon_code = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
            RngHelper.couponUserMap(coupon_code, userId, couponId);
            // System.out.println("UserMapProcessor is "+UserMapProcessor);
            RngHelper.couponCityMap(coupon_code, cityId, couponId);
            Processor cuProcessor = RngHelper.getCouponUserCity(userId, cityId);
            List<String> al = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(), "$.data..id");
            RngHelper.couponUserMapRemove(coupon_code, userId, couponId);
            // fSystem.out.println(al.contains(couponId));
            // Assert.assertEquals(true, al.contains(couponId));
            System.out.println(al);
            System.out.println("created Coupon ID is    " + couponId);
            System.out.println("Mapped user id is  " + userId);
            System.out.println("mapped city id is   " + cityId);
            Assert.assertEquals(al.contains(couponId), true);

        } else if (type == "city") {
            // only city
            Processor cc1Processor = RngHelper.createCoupon(coupon_type, name, code, description, is_private,
                    valid_from, valid_till, total_available, totalPerUser, minAmountCart, customer_restriction,
                    city_restriction, area_restriction, restaurant_restriction, category_restriction, item_restriction,
                    discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                    preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                    minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                    applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
                    updated_by, title, tnc, logo);
            String couponId2 = String.valueOf(cc1Processor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            String coupon_code2 = String.valueOf(cc1Processor.ResponseValidator.GetNodeValue("$.data.code"));
            RngHelper.couponCityMap(coupon_code2, cityId, couponId2);
            // couponId="12345";
            Processor cuProcessor = RngHelper.getCouponUserCity(userId, cityId);
            List al = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(), "$.data..id");
            System.out.println("city only coupon id is   " + couponId2);
            Assert.assertEquals(al.contains(couponId2), false);
        } else {
            // only User
            Processor cc2Processor = RngHelper.createCoupon(coupon_type, name, code, description, is_private,
                    valid_from, valid_till, total_available, totalPerUser, minAmountCart, customer_restriction,
                    city_restriction, area_restriction, restaurant_restriction, category_restriction, item_restriction,
                    discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                    preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                    minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                    applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
                    updated_by, title, tnc, logo);
            String couponId3 = String.valueOf(cc2Processor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            String coupon_code3 = String.valueOf(cc2Processor.ResponseValidator.GetNodeValue("$.data.code"));
            RngHelper.couponUserMap(coupon_code3, userId, couponId3);
            // couponId="12345";
            Processor cuProcessor = RngHelper.getCouponUserCity(userId, cityId);
            List al = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(), "$.data..id");
            // fSystem.out.println(al.contains(couponI3));
            Assert.assertEquals(true, al.contains(couponId3));
        }
    }

    // Private
    @Test(dataProvider = "getCouponUserCityInvalidData")
    public void getCouponUserCityInvalidTest(String coupon_type, String name, String code, String description,
                                             String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                             String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                             String restaurant_restriction, String category_restriction, String item_restriction,
                                             String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                             String first_order_restriction, String preferred_payment_method, String user_client,
                                             String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                             String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                             String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                             String supported_ios_version, String updated_by, String userId, String cityId, String type, String title,
                                             String tnc, String logo) throws IOException, ProcessingException {
        if (type == "both") {
            // both User & City Mapped
            Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                    valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                    area_restriction, restaurant_restriction, category_restriction, item_restriction,
                    discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                    preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                    minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                    applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
                    updated_by, title, tnc, logo);
            String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            String coupon_code = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
            Processor cumProcessor = RngHelper.couponUserMap(coupon_code, userId, couponId);
            RngHelper.couponCityMap(coupon_code, cityId, couponId);
            String Mapping_Id = String
                    .valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
            String data = String.valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
            System.out.println("Coupon code in mapping API is   " + coupon_code);
            System.out.println("in user coupon mapping, mapping id is   " + Mapping_Id);
            Assert.assertNotNull(data, "Mapping fails");
            System.out.println("coupon id is  " + couponId);
            Processor cuProcessor = RngHelper.getCouponUserCity("991", "2");
            List al = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(), "$.data..id");
            // fSystem.out.println(al.contains(couponId));
            Assert.assertEquals(al.contains(couponId), false);
        } else if (type == "city") {
            // only city
            Processor cc1Processor = RngHelper.createCoupon(coupon_type, name, code, description, is_private,
                    valid_from, valid_till, total_available, totalPerUser, minAmountCart, customer_restriction,
                    city_restriction, area_restriction, restaurant_restriction, category_restriction, item_restriction,
                    discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                    preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                    minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                    applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
                    updated_by, title, tnc, logo);

            String couponId2 = String.valueOf(cc1Processor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            String coupon_code = String.valueOf(cc1Processor.ResponseValidator.GetNodeValue("$.data.code"));
            Processor cumProcessor = RngHelper.couponCityMap(coupon_code, cityId, couponId2);
            String Mapping_Id = String
                    .valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
            String data = String.valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
            System.out.println("Coupon code in mapping API is   " + coupon_code);
            System.out.println("mapping id is   " + Mapping_Id);
            Assert.assertNotNull(data, "Mapping fails");
            System.out.println("coupon id is  " + couponId2);
            // couponId="12345";
            Processor cuProcessor = RngHelper.getCouponUserCity(userId, "2");
            List al = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(), "$.data..id");
            // fSystem.out.println(al.contains(couponId2));
            Assert.assertEquals(al.contains(couponId2), false);
        } else {
            // only User
            Processor cc1Processor = RngHelper.createCoupon(coupon_type, name, code, description, is_private,
                    valid_from, valid_till, total_available, totalPerUser, minAmountCart, customer_restriction,
                    city_restriction, area_restriction, restaurant_restriction, category_restriction, item_restriction,
                    discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                    preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                    minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                    applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
                    updated_by, title, tnc, logo);
            String couponId3 = String.valueOf(cc1Processor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            String coupon_code = String.valueOf(cc1Processor.ResponseValidator.GetNodeValue("$.data.code"));
            Processor cumProcessor = RngHelper.couponUserMap(coupon_code, userId, couponId3);

            String Mapping_Id = String
                    .valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
            String data = String.valueOf(cumProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
            System.out.println("Coupon code in mapping API is   " + coupon_code);
            System.out.println("mapping id is   " + Mapping_Id);
            Assert.assertNotNull(data, "Mapping fails");
            System.out.println("coupon id is  " + couponId3);
            // couponId="12345";
            Processor cuProcessor = RngHelper.getCouponUserCity("991", cityId);
            List al = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(), "$.data..id");
            // fSystem.out.println(al.contains(couponI3));
            Assert.assertEquals(al.contains(couponId3), false);
        }
    }

    // Public
    @Test(dataProvider = "publicCouponUserCity", dataProviderClass = DP.class)
    public void getPublicCouponUserCity(String coupon_type, String name, String code, String description,
                                        String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                        String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                        String restaurant_restriction, String category_restriction, String item_restriction,
                                        String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                        String first_order_restriction, String preferred_payment_method, String user_client,
                                        String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                        String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                        String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                        String supported_ios_version, String updated_by, String userId, String cityId, String title, String tnc,
                                        String logo) throws IOException, ProcessingException {
        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);
        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor cuProcessor = RngHelper.getCouponUserCity(userId, cityId);
        List al = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(), "$.data..id");
        // fSystem.out.println(al.contains(couponId));
        Assert.assertEquals(al.contains(couponId), true);

    }

    @Test(dataProvider = "couponUserData", dataProviderClass = DP.class)
    public void getCouponUser(String coupon_type, String name, String code, String description, String is_private,
                              String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                              String customer_restriction, String city_restriction, String area_restriction,
                              String restaurant_restriction, String category_restriction, String item_restriction,
                              String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                              String first_order_restriction, String preferred_payment_method, String user_client,
                              String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                              String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                              String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                              String supported_ios_version, String updated_by, String userId, String type, String title, String tnc,
                              String logo) throws IOException, ProcessingException, InterruptedException {
        String couponId, couponId1, couponId2;
        String codeRe2, codeRe1 = null;
        // public coupon
        System.out.println(type);
        if (type.equals("public")) {
            System.out.println(type);
            Processor csProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                    valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                    area_restriction, restaurant_restriction, category_restriction, item_restriction,
                    discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                    preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                    minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                    applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
                    updated_by, title, tnc, logo);

            couponId1 = String.valueOf(csProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            // String codeRe=csProcessor.ResponseValidator.GetNodeValue("$..code");
            Processor cuProcessor = RngHelper.getCouponUser(userId);
            List al = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(), "$.data..id");
            // System.out.println(al.contains(couponId1));
            if (al.contains(couponId1) == true) {
                Assert.assertEquals(true, al.contains(couponId1));
            }
        } else {// user mapped coupon
            System.out.println(type + "userMapping");
            Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                    valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                    area_restriction, restaurant_restriction, category_restriction, item_restriction,
                    discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                    preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                    minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                    applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
                    updated_by, title, tnc, logo);
            couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            codeRe1 = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
            // RngHelper.couponUserMapRemove(code, userId, couponId);
            Thread.sleep(1000);
            RngHelper.couponUserMap(codeRe1, userId, couponId);
            // coupon mapped for some other user
            Processor cssProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private,
                    valid_from, valid_till, total_available, totalPerUser, minAmountCart, customer_restriction,
                    city_restriction, area_restriction, restaurant_restriction, category_restriction, item_restriction,
                    discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                    preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                    minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                    applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
                    updated_by, title, tnc, logo);
            couponId2 = String.valueOf(cssProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            codeRe2 = String.valueOf(cssProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..code"));
            RngHelper.couponUserMap(codeRe2, "991", couponId2);
            Processor cuProcessor = RngHelper.getCouponUser(userId);
            RngHelper.couponUserMapRemove(codeRe1, userId, couponId);
            RngHelper.couponUserMapRemove(codeRe2, "991", couponId2);
            List al = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(), "$.data..id");
            System.out.println(al.contains(couponId));
            RngHelper.couponUserMapRemove(codeRe1, userId, couponId);
            // RngHelper.couponUserMapRemove(codeRe2, "991", couponId2);
            if (al.contains(couponId) == true && (al.contains(couponId2)) != true) {
                Assert.assertEquals(al.contains(couponId), true);
                Assert.assertEquals(al.contains(couponId2), false);
            }
        }
    }

    // @Test(dataProvider = "couponUserData", dataProviderClass = DP.class)
    // public void getCouponUserInvalidTest1(String coupon_type, String name, String
    // code, String description,
    // String is_private, String valid_from, String valid_till, String
    // total_available, String totalPerUser,
    // String minAmountCart, String customer_restriction, String city_restriction,
    // String area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String minQuantityCart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String userId, String type,
    // String title, String tnc,
    // String logo) throws IOException, ProcessingException {
    //
    // if (userId.equalsIgnoreCase(type)) {
    // Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code,
    // description, is_private, valid_from,
    // valid_till, total_available, totalPerUser, minAmountCart,
    // customer_restriction, city_restriction,
    // area_restriction, restaurant_restriction, category_restriction,
    // item_restriction,
    // discount_percentage, discount_amount, free_shipping, free_gifts,
    // first_order_restriction,
    // preferred_payment_method, user_client, slot_restriction, createdOn, image,
    // coupon_bucket,
    // minQuantityCart, cuisine_restriction, discount_item, usage_count,
    // expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo);
    // String couponId =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
    // // couponId="12345";
    // Processor cuProcessor = RngHelper.getCouponUser(userId);
    // String status = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(),
    // "$.status");
    // // System.out.println(al.contains(couponId));
    // Assert.assertEquals(status, "400");
    // } else {
    // Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code,
    // description, is_private, valid_from,
    // valid_till, total_available, totalPerUser, minAmountCart,
    // customer_restriction, city_restriction,
    // area_restriction, restaurant_restriction, category_restriction,
    // item_restriction,
    // discount_percentage, discount_amount, free_shipping, free_gifts,
    // first_order_restriction,
    // preferred_payment_method, user_client, slot_restriction, createdOn, image,
    // coupon_bucket,
    // minQuantityCart, cuisine_restriction, discount_item, usage_count,
    // expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo);
    // String couponId =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
    // // couponId="12345";
    // Processor cuProcessor = RngHelper.getCouponUser(userId);
    // List ls = JsonPath.read(cuProcessor.ResponseValidator.GetBodyAsText(),
    // "$..data");
    // // System.out.println(al.contains(couponId));
    // Assert.assertEquals( ls.contains(couponId),true);
    // }
    // }

    @Test(dataProvider = "applyCoupon", dataProviderClass = DP.class)
    public void applyCoupon(String coupon_type, String name, String code, String description, String is_private,
                            String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                            String customer_restriction, String city_restriction, String area_restriction,
                            String restaurant_restriction, String category_restriction, String item_restriction,
                            String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                            String first_order_restriction, String preferred_payment_method, String user_client,
                            String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                            String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                            String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                            String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                            String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                            String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                            String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                            String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                            String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                            String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                            String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                            String mapAreaId, String mapCityId) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        if (type.equalsIgnoreCase("userPrivate")) {
            RngHelper.couponUserMap(couponCode, userIdMap, couponId);
        }
        if (type.equalsIgnoreCase("restaurantPrivate")) {
            RngHelper.couponRestaurantMap(couponCode, restId, couponId);
        }
        if (type.equalsIgnoreCase("areaPrivate")) {
            RngHelper.couponAreaMap(couponCode, cart_areaId, couponId);
        }
        if (type.equalsIgnoreCase("cityPrivate")) {
            RngHelper.couponCityMap(couponCode, mapCityId, couponId);

        }
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        String valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        String coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        // Processor cuProcessor = RngHelper.getCouponUserCity(userId, cityId);
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(coup_err, "null");
    }

    @Test(dataProvider = "applyCouponInvalidData", dataProviderClass = DP.class)
    public void applyCouponInvalid(String coupon_type, String name, String code, String description, String is_private,
                                   String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                                   String customer_restriction, String city_restriction, String area_restriction,
                                   String restaurant_restriction, String category_restriction, String item_restriction,
                                   String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                   String first_order_restriction, String preferred_payment_method, String user_client,
                                   String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                   String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                   String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                   String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                   String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                   String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                   String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                   String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                   String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                   String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                   String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                   String mapAreaId, String mapCityId) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        if (type.equalsIgnoreCase("userPrivate")) {
            RngHelper.couponUserMap(couponCode, userIdMap, couponId);
        }
        if (type.equalsIgnoreCase("restaurantPrivate")) {
            RngHelper.couponRestaurantMap(couponCode, restId, couponId);
        }
        if (type.equalsIgnoreCase("areaPrivate")) {
            RngHelper.couponAreaMap(couponCode, mapAreaId, couponId);
        }
        if (type.equalsIgnoreCase("cityPrivate")) {
            RngHelper.couponCityMap(couponCode, mapCityId, couponId);

        }

        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        String valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        String coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        // Processor cuProcessor = RngHelper.getCouponUserCity(userId, cityId);
        Assert.assertEquals(valid, "false");
        // Assert.assertEquals(coup_err, "null");
        Assert.assertNotNull(coup_err);
    }

    @Test(dataProvider = "applyCouponAndUsageCount", dataProviderClass = DP.class)
    public void applyCouponAndUsageCount(String coupon_type, String name, String code, String description,
                                         String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                         String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                         String restaurant_restriction, String category_restriction, String item_restriction,
                                         String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                         String first_order_restriction, String preferred_payment_method, String user_client,
                                         String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                         String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                         String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                         String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                         String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                         String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                         String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                         String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                         String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                         String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                         String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                         String mapAreaId, String mapCityId, String orderId, String count) throws Exception, IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";

        int perUser = Integer.parseInt(count);
        int totalCoupons = Integer.parseInt(total_available);
        int total_Per_User = Integer.parseInt(totalPerUser);
        if (totalCoupons == 0 && total_Per_User == 0) {
            perUser = 15;
        }
        if (totalCoupons != 0 && total_Per_User == 0) {
            perUser = totalCoupons;
        }
        Assert.assertEquals(String.valueOf(0), couponRedisHelper.getCouponUsageCountRedis(couponCode), "uses count is not matching with redis data");/* count 0 validation when coupon is applied */
        for (int i = 1; i <= perUser; i++) {
            Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                    cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                    cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                    cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                    itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                    superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent,
                    payment_codes, time);
            valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
            coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
            RngHelper.couponUsageCount(couponCode, userId, orderId);
            System.out.println("i value" + i);
        }
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Total per user is   " + perUser);
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(coup_err, "null");
        Assert.assertEquals(String.valueOf(perUser), couponRedisHelper.getCouponUsageCountRedis(couponCode), "uses count is not matching with redis data");

    }

    @Test(dataProvider = "applyCouponMultipleUser", dataProviderClass = DP.class)
    public void applyCouponMultipleUser(String coupon_type, String name, String code, String description,
                                        String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                        String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                        String restaurant_restriction, String category_restriction, String item_restriction,
                                        String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                        String first_order_restriction, String preferred_payment_method, String user_client,
                                        String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                        String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                        String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                        String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                        String cart_cityId, String cartBlob_swiggyTradeDiscount,
                                        String cartBlob_restaurantTradeDiscount,
                                        String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                        String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                        String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                        String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                        String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                        String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                        String mapAreaId, String mapCityId, String orderId, String count, String userId2)
            throws Exception, IOException, ProcessingException, InterruptedException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        int totalCoupons = Integer.parseInt(total_available);
        int total_Per_User = Integer.parseInt(totalPerUser);
        int user1Hit = 0;
        int user2Hit;
        for (int i = 1; i <= totalCoupons / 2; i++) {
            Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                    cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                    cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                    cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                    itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                    superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent,
                    payment_codes, time);
            valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
            coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
            RngHelper.couponUsageCount(couponCode, userId, orderId);
            Thread.sleep(1000);
            user1Hit = i;
            System.out.println(" user 1st usage hit  " + "\"" + i + "\"");
            Assert.assertEquals(String.valueOf(user1Hit), couponRedisHelper.getCouponUsageCountRedis(couponCode), "Used Coupon count is not matched with redis Coupon Usages count");
        }

        for (int j = 1; j <= (totalCoupons / 2) + 1; j++) {
            Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                    cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                    cartItems_menu_item_id, cartItems_quantity, restaurantId, userId2, swiggyMoney, firstOrder,
                    cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                    itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                    superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent,
                    payment_codes, time);
            valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
            coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
            RngHelper.couponUsageCount(couponCode, userId2, orderId);
            Thread.sleep(1000);
            System.out.println(" user 2nd usage hit  " + "\"" + j + "\"");
            user2Hit = (totalCoupons / 2) + j;
            Assert.assertEquals(String.valueOf(user2Hit), couponRedisHelper.getCouponUsageCountRedis(couponCode), "Used Coupon count is not matched with redis Coupon Usages count");
        }
        System.out.println("Total available is  " + total_available);
        System.out.println("totalPerUser is " + totalPerUser);
        System.out.println("user one usage hit " + "\"" + user1Hit + "\"");
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        Assert.assertNotEquals(String.valueOf(total_available), couponRedisHelper.getCouponUsageCountRedis(couponCode), "Available Coupon Count is Equal to Redis Coupon Usage Count");
        Assert.assertEquals(coup_err, "E_COUPON_USAGE_LIMIT_REACHED");
        Assert.assertEquals(valid, "false");

    }


    @Test(dataProvider = "applyCouponMultipleUser", dataProviderClass = DP.class)
    public void removeCouponUsageCountMultipleUser(String coupon_type, String name, String code, String description,
                                                   String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                                   String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                                   String restaurant_restriction, String category_restriction, String item_restriction,
                                                   String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                                   String first_order_restriction, String preferred_payment_method, String user_client,
                                                   String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                   String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                   String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                                   String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                                   String cart_cityId, String cartBlob_swiggyTradeDiscount,
                                                   String cartBlob_restaurantTradeDiscount,
                                                   String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                                   String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                                   String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                                   String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                                   String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                                   String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                                   String mapAreaId, String mapCityId, String orderId, String count, String userId2)
            throws Exception, IOException, ProcessingException, InterruptedException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        int totalCoupons = Integer.parseInt(total_available);
        int total_Per_User = Integer.parseInt(totalPerUser);
        int user1Hit = 0;
        rngHelper.removeCouponUsagesCount(couponUsageCountRemovePOJO.setDefaultData(couponCode, userId, orderId));
        Assert.assertEquals("0", couponRedisHelper.getCouponUsageCountRedis(couponCode));

        int user2Hit;
        for (int i = 1; i <= totalCoupons / 2; i++) {
            Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                    cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                    cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                    cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                    itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                    superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent,
                    payment_codes, time);
            valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
            coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
            RngHelper.couponUsageCount_remove(couponCode, userId, orderId);
            Thread.sleep(1000);
            user1Hit = i;
            System.out.println(" user 1st usage hit  " + "\"" + i + "\"");
            rngHelper.removeCouponUsagesCount(couponUsageCountRemovePOJO.setDefaultData(couponCode, userId, orderId));
            Assert.assertEquals("0", couponRedisHelper.getCouponUsageCountRedis(couponCode));

        }

        for (int j = 1; j <= (totalCoupons / 2) + 1; j++) {
            Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                    cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                    cartItems_menu_item_id, cartItems_quantity, restaurantId, userId2, swiggyMoney, firstOrder,
                    cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                    itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                    superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent,
                    payment_codes, time);
            valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
            coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
            RngHelper.couponUsageCount_remove(couponCode, userId, orderId);
            Thread.sleep(1000);
            System.out.println(" user 2nd usage hit  " + "\"" + j + "\"");
            user2Hit = (totalCoupons / 2) + j;
            rngHelper.removeCouponUsagesCount(couponUsageCountRemovePOJO.setDefaultData(couponCode, userId, orderId));
            Assert.assertEquals("0", couponRedisHelper.getCouponUsageCountRedis(couponCode));
        }
        System.out.println("Total available is  " + total_available);
        System.out.println("totalPerUser is " + totalPerUser);
        System.out.println("user one usage hit " + "\"" + user1Hit + "\"");
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        rngHelper.removeCouponUsagesCount(couponUsageCountRemovePOJO.setDefaultData(couponCode, userId, orderId));
        Assert.assertEquals("0", couponRedisHelper.getCouponUsageCountRedis(couponCode));
    }


    @Test(dataProvider = "applyCouponUsageCountInvalid", dataProviderClass = DP.class)
    public void applyCouponUsageCountInvalid(String coupon_type, String name, String code, String description,
                                             String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                             String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                             String restaurant_restriction, String category_restriction, String item_restriction,
                                             String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                             String first_order_restriction, String preferred_payment_method, String user_client,
                                             String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                             String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                             String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                             String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                             String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                             String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                             String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                             String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                             String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                             String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                             String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                             String mapAreaId, String mapCityId, String orderId, String count)
            throws IOException, ProcessingException, InterruptedException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";

        int perUser = Integer.parseInt(count);
        for (int i = 0; i <= perUser; i++) {
            Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                    cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                    cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                    cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                    itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                    superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent,
                    payment_codes, time);
            valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
            coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
            RngHelper.couponUsageCount(couponCode, userId, orderId);
            Thread.sleep(1000);
            System.out.println(i);
        }
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println(perUser);
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(coup_err, "E_COUPON_USAGE_LIMIT_REACHED");
        // Assert.assertNotNull(coup_err);
    }

    @Test(dataProvider = "applyCouponApplicableWithSwiggyMoneyInvalid", dataProviderClass = DP.class)
    public void applyCouponApplicableWithSwiggyMoneyInvalid(String coupon_type, String name, String code,
                                                            String description, String is_private, String valid_from, String valid_till, String total_available,
                                                            String totalPerUser, String minAmountCart, String customer_restriction, String city_restriction,
                                                            String area_restriction, String restaurant_restriction, String category_restriction,
                                                            String item_restriction, String discount_percentage, String discount_amount, String free_shipping,
                                                            String free_gifts, String first_order_restriction, String preferred_payment_method, String user_client,
                                                            String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                            String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                            String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                                            String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                                            String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                                            String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                                            String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                                            String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                                            String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                                            String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                                            String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                                            String mapAreaId, String mapCityId, String orderId, String count) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(coup_err, "E_COUPON_NOT_APPLICABLE_WITH_SWIGGY_MONEY");
    }

    @Test(dataProvider = "applyCouponApplicableWithSwiggyMoney", dataProviderClass = DP.class)
    public void applyCouponApplicableWithSwiggyMoney(String coupon_type, String name, String code, String description,
                                                     String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                                     String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                                     String restaurant_restriction, String category_restriction, String item_restriction,
                                                     String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                                     String first_order_restriction, String preferred_payment_method, String user_client,
                                                     String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                     String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                     String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                                     String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                                     String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                                     String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                                     String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                                     String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                                     String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                                     String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                                     String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                                     String mapAreaId, String mapCityId, String orderId, String count) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(coup_err, "null");
    }

    @Test(dataProvider = "applyCouponApplicableWithSwiggyMoneyTrue", dataProviderClass = DP.class)
    public void applyCouponApplicableWithSwiggyMoneyTrue(String coupon_type, String name, String code,
                                                         String description, String is_private, String valid_from, String valid_till, String total_available,
                                                         String totalPerUser, String minAmountCart, String customer_restriction, String city_restriction,
                                                         String area_restriction, String restaurant_restriction, String category_restriction,
                                                         String item_restriction, String discount_percentage, String discount_amount, String free_shipping,
                                                         String free_gifts, String first_order_restriction, String preferred_payment_method, String user_client,
                                                         String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                         String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                         String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                                         String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                                         String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                                         String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                                         String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                                         String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                                         String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                                         String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                                         String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                                         String mapAreaId, String mapCityId, String orderId, String count) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(coup_err, "null");
    }

    @Test(dataProvider = "applyCouponApplicableUserClient", dataProviderClass = DP.class)
    public void applyCouponApplicableUserClient(String coupon_type, String name, String code, String description,
                                                String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                                String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                                String restaurant_restriction, String category_restriction, String item_restriction,
                                                String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                                String first_order_restriction, String preferred_payment_method, String user_client,
                                                String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                                String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                                String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                                String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                                String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                                String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                                String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                                String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                                String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                                String mapAreaId, String mapCityId, String orderId, String count) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("coupon config user_client is " + user_client);
        System.out.println("In apply headers_userAgent is " + headers_userAgent);
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(coup_err, "null");
    }

    @Test(dataProvider = "applyCouponApplicableUserClientInvalid", dataProviderClass = DP.class)
    public void applyCouponApplicableUserClientInvalid(String coupon_type, String name, String code, String description,
                                                       String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                                       String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                                       String restaurant_restriction, String category_restriction, String item_restriction,
                                                       String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                                       String first_order_restriction, String preferred_payment_method, String user_client,
                                                       String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                       String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                       String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                                       String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                                       String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                                       String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                                       String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                                       String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                                       String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                                       String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                                       String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                                       String mapAreaId, String mapCityId, String orderId, String count) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("coupon config user_client is " + user_client);
        System.out.println("In apply headers_userAgent is " + headers_userAgent);
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        Assert.assertEquals(valid, "false");
        if (user_client.equalsIgnoreCase("3")) {
            Assert.assertEquals(coup_err, "E_COUPON_APPLICABLE_ONLY_ON_ANDROID_ORDER");
        }
        if (user_client.equalsIgnoreCase("1")) {
            Assert.assertEquals(coup_err, "E_COUPON_APPLICABLE_ONLY_ON_WEB_ORDER");
        }
        if (user_client.equalsIgnoreCase("4")) {
            Assert.assertEquals(coup_err, "E_COUPON_APPLICABLE_ONLY_ON_IOS_ORDER");
        }
    }

    @Test(dataProvider = "applyCouponMinCartAmount", dataProviderClass = DP.class)
    public void applyCouponMinCartAmount(String coupon_type, String name, String code, String description,
                                         String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                         String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                         String restaurant_restriction, String category_restriction, String item_restriction,
                                         String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                         String first_order_restriction, String preferred_payment_method, String user_client,
                                         String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                         String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                         String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                         String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                         String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                         String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                         String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                         String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                         String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                         String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                         String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                         String mapAreaId, String mapCityId) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        int cartPriceCartTotal = Integer.parseInt(cartPrice_cartTotal);
        int min_Amount_Cart = Integer.parseInt(minAmountCart);
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("coupon config user_client is " + user_client);
        System.out.println("In apply headers_userAgent is " + headers_userAgent);
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");

        if (cartPriceCartTotal < min_Amount_Cart) {
            Assert.assertEquals(valid, "false");
            Assert.assertEquals(coup_err, "E_COUPON_CART_MIN_AMOUNT_NOT_MET");
        }
        if (cartPriceCartTotal > min_Amount_Cart | cartPriceCartTotal == min_Amount_Cart) {
            Assert.assertEquals(valid, "true");
            Assert.assertEquals(coup_err, "null");
        }

    }

    @Test(dataProvider = "applyCouponMinQuantity", dataProviderClass = DP.class)
    public void applyCouponMinQuantity(String coupon_type, String name, String code, String description,
                                       String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                       String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                       String restaurant_restriction, String category_restriction, String item_restriction,
                                       String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                       String first_order_restriction, String preferred_payment_method, String user_client,
                                       String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                       String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                       String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                       String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                       String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                       String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                       String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                       String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                       String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                       String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                       String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                       String mapAreaId, String mapCityId) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        int cartPricequantity = Integer.parseInt(cartPrice_quantity);
        int min_Quantity_Cart = Integer.parseInt(minQuantityCart);
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");

        if (cartPricequantity < min_Quantity_Cart) {
            Assert.assertEquals(valid, "false");
            Assert.assertEquals(coup_err, "E_COUPON_CART_MIN_QUANTITY_NOT_MET");
        }
        if (cartPricequantity > min_Quantity_Cart | cartPricequantity == min_Quantity_Cart) {
            Assert.assertEquals(valid, "true");
            Assert.assertEquals(coup_err, "null");
        }

    }

    @Test(dataProvider = "applyCouponFirstOrder", dataProviderClass = DP.class)
    public void applyCouponFirstOrder(String coupon_type, String name, String code, String description,
                                      String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                      String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                      String restaurant_restriction, String category_restriction, String item_restriction,
                                      String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                      String first_order_restriction, String preferred_payment_method, String user_client,
                                      String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                      String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                      String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                      String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                      String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                      String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                      String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                      String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                      String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                      String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                      String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                      String mapAreaId, String mapCityId) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";
        // int first_Order=Integer.parseInt(firstOrder);
        int firstOrderRestriction = Integer.parseInt(first_order_restriction);
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");

        if ((firstOrder.equalsIgnoreCase("false") && firstOrderRestriction == 1)) {
            Assert.assertEquals(valid, "false");
            Assert.assertEquals(coup_err, "E_COUPON_APPLICABLE_ONLY_ON_FIRST_ORDER");
        }
        if (firstOrder.equalsIgnoreCase("true") && firstOrderRestriction == 1) {
            Assert.assertEquals(valid, "true");
            Assert.assertEquals(coup_err, "null");
        }

    }

    @Test(dataProvider = "applyCouponPreOrderExpired", dataProviderClass = DP.class)
    public void applyCouponPreOrderExpired(String coupon_type, String name, String code, String description,
                                           String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                           String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                           String restaurant_restriction, String category_restriction, String item_restriction,
                                           String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                           String first_order_restriction, String preferred_payment_method, String user_client,
                                           String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                           String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                           String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                           String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                           String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                           String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                           String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                           String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                           String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                           String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                           String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                           String mapAreaId, String mapCityId) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";

        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(coup_err, "E_COUPON_EXPIRED");
    }

    @Test(dataProvider = "applyCouponPreOrder", dataProviderClass = DP.class)
    public void applyCouponPreOrder(String coupon_type, String name, String code, String description, String is_private,
                                    String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                                    String customer_restriction, String city_restriction, String area_restriction,
                                    String restaurant_restriction, String category_restriction, String item_restriction,
                                    String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                    String first_order_restriction, String preferred_payment_method, String user_client,
                                    String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                    String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                    String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
                                    String supported_ios_version, String updated_by, String title, String tnc, String logo, String cart_areaId,
                                    String cart_cityId, String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount,
                                    String cartItems_itemKey, String cartItems_menu_item_id, String cartItems_quantity, String restaurantId,
                                    String userId, String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
                                    String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
                                    String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
                                    String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
                                    String headers_userAgent, String payment_codes, String time, String type, String userIdMap, String restId,
                                    String mapAreaId, String mapCityId) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String apply_time = ccProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid_till");
        String valid = "";
        String coup_err = "";

        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                apply_time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        System.out.println("applying time is  " + "\"" + apply_time + "\"");

        Assert.assertEquals(valid, "true");
        Assert.assertEquals(coup_err, "null");
    }

    // @Test(dataProvider = "createAllTypebulkCouponData", dataProviderClass =
    // DP.class)
    // public void createAllTypebulkCouponData(String code_prefix, String
    // code_suffix_length, String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // int processId =
    // ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.code");
    // Assert.assertNotEquals(processId, 0);
    // }

    @Test(dataProvider = "applyCouponVendorDominos", dataProviderClass = DP.class)
    public void applyCouponVendor(String coupon_type, String name, String code, String description, String is_private,
                                  String valid_from, String valid_till, String total_available, String totalPerUser, String minAmountCart,
                                  String customer_restriction, String city_restriction, String area_restriction,
                                  String restaurant_restriction, String category_restriction, String item_restriction,
                                  String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                  String first_order_restriction, String preferred_payment_method, String user_client,
                                  String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                  String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                  String applicable_with_swiggy_money, String vendor, String is_bank_discount, String refund_source,
                                  String pg_message, String supported_ios_version, String updated_by, String title, String tnc, String logo,
                                  String cart_areaId, String cart_cityId, String cartBlob_swiggyTradeDiscount,
                                  String cartBlob_restaurantTradeDiscount, String cartItems_itemKey, String cartItems_menu_item_id,
                                  String cartItems_quantity, String restaurantId, String userId, String swiggyMoney, String firstOrder,
                                  String cartPrice_quantity, String cartPrice_cartTotal, String cartPrice_itemLevelPrice,
                                  String itemLevelPrice_quantity, String itemLevelPrice_subTotal, String preferredPaymentMethod,
                                  String referralPresence, String cartPrice_swiggyMoneyApplicable, String superUser, String typeOfPartner,
                                  String swiggyMoneyApplicable, String headers_versionCode, String headers_userAgent, String payment_codes,
                                  String time, String type, String userIdMap, String restId, String mapAreaId, String mapCityId)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCouponVendor(coupon_type, name, code, description, is_private,
                valid_from, valid_till, total_available, totalPerUser, minAmountCart, customer_restriction,
                city_restriction, area_restriction, restaurant_restriction, category_restriction, item_restriction,
                discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                applicable_with_swiggy_money, vendor, is_bank_discount, refund_source, pg_message,
                supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";

        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        System.out.println("in create coupon vendor is  " + "\"" + vendor + "\"");
        System.out.println("in applying coupon typeOfPartner is  " + "\"" + typeOfPartner + "\"");
        if (typeOfPartner.equalsIgnoreCase(vendor)) {
            Assert.assertEquals(valid, "true");
            Assert.assertEquals(coup_err, "null");
        }
        if (!(typeOfPartner.equalsIgnoreCase(vendor))) {
            Assert.assertEquals(valid, "false");
            Assert.assertEquals(coup_err, "E_COUPON_NOT_APPLICABLE_ON_RESTAURANT");
        }

    }

    @Test(dataProvider = "applyCouponVendorSwiggy", dataProviderClass = DP.class)
    public void applyCouponVendorSwiggy(String coupon_type, String name, String code, String description,
                                        String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                        String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                        String restaurant_restriction, String category_restriction, String item_restriction,
                                        String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                        String first_order_restriction, String preferred_payment_method, String user_client,
                                        String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                        String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                        String applicable_with_swiggy_money, String vendor, String is_bank_discount, String refund_source,
                                        String pg_message, String supported_ios_version, String updated_by, String title, String tnc, String logo,
                                        String cart_areaId, String cart_cityId, String cartBlob_swiggyTradeDiscount,
                                        String cartBlob_restaurantTradeDiscount, String cartItems_itemKey, String cartItems_menu_item_id,
                                        String cartItems_quantity, String restaurantId, String userId, String swiggyMoney, String firstOrder,
                                        String cartPrice_quantity, String cartPrice_cartTotal, String cartPrice_itemLevelPrice,
                                        String itemLevelPrice_quantity, String itemLevelPrice_subTotal, String preferredPaymentMethod,
                                        String referralPresence, String cartPrice_swiggyMoneyApplicable, String superUser, String typeOfPartner,
                                        String swiggyMoneyApplicable, String headers_versionCode, String headers_userAgent, String payment_codes,
                                        String time, String type, String userIdMap, String restId, String mapAreaId, String mapCityId)
            throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCouponVendor(coupon_type, name, code, description, is_private,
                valid_from, valid_till, total_available, totalPerUser, minAmountCart, customer_restriction,
                city_restriction, area_restriction, restaurant_restriction, category_restriction, item_restriction,
                discount_percentage, discount_amount, free_shipping, free_gifts, first_order_restriction,
                preferred_payment_method, user_client, slot_restriction, updatedOn, createdOn, image, coupon_bucket,
                minQuantityCart, cuisine_restriction, discount_item, usage_count, expiry_offset,
                applicable_with_swiggy_money, vendor, is_bank_discount, refund_source, pg_message,
                supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid = "";
        String coup_err = "";

        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                time);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println("Created Coupon id is    " + "\"" + couponId + "\"" + "  and coupon code is  " + "\""
                + couponCode + "\"");
        System.out.println("Error in applying coupon is  " + "\"" + coup_err + "\"");
        System.out.println("in create coupon vendor is  " + "\"" + vendor + "\"");
        System.out.println("in applying coupon typeOfPartner is  " + "\"" + typeOfPartner + "\"");
        if (typeOfPartner.equalsIgnoreCase(vendor)) {
            Assert.assertEquals(valid, "true");
            Assert.assertEquals(coup_err, "null");
        }
        if (!(typeOfPartner.equalsIgnoreCase(vendor))) {
            Assert.assertEquals(valid, "false");
            Assert.assertEquals(coup_err, "E_COUPON_NOT_APPLICABLE_ON_RESTAURANT");
        }

    }

    // @Test(dataProvider = "applyCouponOutOfTheTimeSlot", dataProviderClass =
    // DP.class)
    // public void applyCouponOutOfTheTimeSlot(String coupon_type, String name,
    // String code, String description,
    // String is_private, String valid_from, String valid_till, String
    // total_available, String totalPerUser,
    // String minAmountCart, String customer_restriction, String city_restriction,
    // String area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String minQuantityCart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String vendor, String is_bank_discount,
    // String refund_source,
    // String pg_message, String supported_ios_version, String updated_by, String
    // title, String tnc, String logo,
    // String cart_areaId, String cart_cityId, String cartBlob_swiggyTradeDiscount,
    // String cartBlob_restaurantTradeDiscount, String cartItems_itemKey, String
    // cartItems_menu_item_id,
    // String cartItems_quantity, String restaurantId, String userId, String
    // swiggyMoney, String firstOrder,
    // String cartPrice_quantity, String cartPrice_cartTotal, String
    // cartPrice_itemLevelPrice,
    // String itemLevelPrice_quantity, String itemLevelPrice_subTotal, String
    // preferredPaymentMethod,
    // String referralPresence, String cartPrice_swiggyMoneyApplicable, String
    // superUser, String typeOfPartner,
    // String swiggyMoneyApplicable, String headers_versionCode, String
    // headers_userAgent, String payment_codes,
    // String time, String type, String userIdMap, String restId, String mapAreaId,
    // String mapCityId, String day,
    // String openTime, String closeTime, String order_id, String createdBy, String
    // updatedBy, String updatedOn,
    // String timeSlot_createdOn) throws IOException, ProcessingException {
    //
    // Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code,
    // description, is_private, valid_from,
    // valid_till, total_available, totalPerUser, minAmountCart,
    // customer_restriction, city_restriction,
    // area_restriction, restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage,
    // discount_amount, free_shipping, free_gifts, first_order_restriction,
    // preferred_payment_method,
    // user_client, slot_restriction, createdOn, image, coupon_bucket,
    // minQuantityCart, cuisine_restriction,
    // discount_item, usage_count, expiry_offset, applicable_with_swiggy_money,
    // is_bank_discount,
    // refund_source, pg_message, supported_ios_version, updated_by, title, tnc,
    // logo);
    //
    // String couponId =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
    // String couponCode =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
    // String valid = "";
    // String coup_err = "";
    // RngHelper.postCouponTimeSlot(day, openTime, closeTime, couponCode, order_id,
    // couponId, createdBy, updatedBy,
    // updatedOn, timeSlot_createdOn);
    //
    // Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId,
    // cart_cityId,
    // cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount,
    // cartItems_itemKey,
    // cartItems_menu_item_id, cartItems_quantity, restaurantId, userId,
    // swiggyMoney, firstOrder,
    // cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice,
    // itemLevelPrice_quantity,
    // itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence,
    // cartPrice_swiggyMoneyApplicable,
    // superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode,
    // headers_userAgent, payment_codes,
    // time);
    // valid =
    // applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
    // coup_err =
    // String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
    // System.out.println("Created Coupon id is " + "\"" + couponId + "\"" + " and
    // coupon code is " + "\""
    // + couponCode + "\"");
    // System.out.println("Error in applying coupon is " + "\"" + coup_err + "\"");
    // System.out.println("in create coupon mapped time slot is " + "\"" + "day is "
    // + day + " openTime is "
    // + openTime + " closeTime is " + closeTime + " valid_from " + valid_from + "
    // valid_till " + valid_till
    // + "\"");
    // System.out.println("in applying coupon time slot is " + "\"" + time + "\"");
    // Assert.assertEquals(valid, "true");
    // //Assert.assertEquals(coup_err, "E_COUPON_INVALID_TIME_SLOT");
    //
    // }


    @Test(dataProvider = "applyCouponPreOrderWithInValidity", dataProviderClass = DP.class)
    public void applyCouponPreOrderWithInValidity(String coupon_type, String name, String code, String description,
                                                  String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
                                                  String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
                                                  String restaurant_restriction, String category_restriction, String item_restriction,
                                                  String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
                                                  String first_order_restriction, String preferred_payment_method, String user_client,
                                                  String slot_restriction, String updatedOn1, String createdOn, String image, String coupon_bucket, String minQuantityCart,
                                                  String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
                                                  String applicable_with_swiggy_money, String vendor, String is_bank_discount, String refund_source,
                                                  String pg_message, String supported_ios_version, String updated_by, String title, String tnc, String logo,
                                                  String cart_areaId, String cart_cityId, String cartBlob_swiggyTradeDiscount,
                                                  String cartBlob_restaurantTradeDiscount, String cartItems_itemKey, String cartItems_menu_item_id,
                                                  String cartItems_quantity, String restaurantId, String userId, String swiggyMoney, String firstOrder,
                                                  String cartPrice_quantity, String cartPrice_cartTotal, String cartPrice_itemLevelPrice,
                                                  String itemLevelPrice_quantity, String itemLevelPrice_subTotal, String preferredPaymentMethod,
                                                  String referralPresence, String cartPrice_swiggyMoneyApplicable, String superUser, String typeOfPartner,
                                                  String swiggyMoneyApplicable, String headers_versionCode, String headers_userAgent, String payment_codes,
                                                  String time, String type, String userIdMap, String restId, String mapAreaId, String mapCityId, String day,
                                                  String openTime, String closeTime, String order_id, String createdBy, String updatedBy, String updatedOn,
                                                  String timeSlot_createdOn) throws IOException, ProcessingException {

        Processor ccProcessor = RngHelper.createCoupon(coupon_type, name, code, description, is_private, valid_from,
                valid_till, total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction,
                area_restriction, restaurant_restriction, category_restriction, item_restriction, discount_percentage,
                discount_amount, free_shipping, free_gifts, first_order_restriction, preferred_payment_method,
                user_client, slot_restriction, updatedOn1, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction,
                discount_item, usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount,
                refund_source, pg_message, supported_ios_version, updated_by, title, tnc, logo);

        String couponId = String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String couponCode = String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.code"));
        String valid_tillDate = ccProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid_till");
        System.out.println("In apply time is ------" + valid_tillDate);
        String valid = "";
        String coup_err = "";
        RngHelper.postCouponTimeSlot(day, openTime, closeTime, couponCode, order_id, couponId, createdBy, updatedBy,
                updatedOn, timeSlot_createdOn);
        Processor applyProcessor = RngHelper.applyCoupon(couponCode, cart_areaId, cart_cityId,
                cartBlob_swiggyTradeDiscount, cartBlob_restaurantTradeDiscount, cartItems_itemKey,
                cartItems_menu_item_id, cartItems_quantity, restaurantId, userId, swiggyMoney, firstOrder,
                cartPrice_quantity, cartPrice_cartTotal, cartPrice_itemLevelPrice, itemLevelPrice_quantity,
                itemLevelPrice_subTotal, preferredPaymentMethod, referralPresence, cartPrice_swiggyMoneyApplicable,
                superUser, typeOfPartner, swiggyMoneyApplicable, headers_versionCode, headers_userAgent, payment_codes,
                valid_tillDate);
        valid = applyProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid");
        coup_err = String.valueOf(applyProcessor.ResponseValidator.GetNodeValue("$.data.couponError"));
        System.out.println(
                "Created Coupon id is " + "\"" + couponId + "\"" + " and coupon code is " + "\"" + couponCode + "\"");
        System.out.println("Error in applying coupon is " + "\"" + coup_err + "\"");
        System.out.println("in create coupon mapped time slot is " + "\"" + "day is " + day + " openTime is " + openTime
                + " closeTime is " + closeTime + " valid_from " + valid_from + " valid_till " + valid_till + "\"");
        System.out.println("in applying coupon time slot is " + "\"" + time + "\"");
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(coup_err, "null");

    }

    @Test(dataProvider = "referralFlowData", dataProviderClass = DP.class)
    public void referralFlow(String name, String mobile, String email, String password) throws Exception {

        String mboli_num = RngHelper.checkUserByMobile(mobile);
        if (mobile.equalsIgnoreCase(mboli_num)) {
            RngHelper.deleteUserByMobile(mobile);
        }
        String referralCodeNUserId = RngHelper.getReferralCodeFromWpUsers();
        String[] referral_userId = referralCodeNUserId.split(",");
        String referral = referral_userId[0];
        String userId = referral_userId[1];
        Processor tidProcessor = RngHelper.signUpWithReferral(name, mobile, email, password, referral);
        String tid = String.valueOf(tidProcessor.ResponseValidator.GetNodeValue("$.tid"));
        System.out.println("tid for signup is   " + tid);
        String otp = SnDHelper.getOTP(tid);
        SnDHelper.verifyOTP(otp, tid);
        // Sign in 1st time
        SnDHelper.consumerLogin(mobile, password);
        Processor P = SnDHelper.consumerLoginCheck(mobile);
        String login_verify = String.valueOf(P.ResponseValidator.GetNodeValue("$.statusMessage"));
        System.out.println("Verify login    " + login_verify);
        System.out.println("referral code is  " + referral + " user id is " + userId);
        String refer_code = RngHelper.getReferralCode(mobile);
        System.out.println("1st time login user referral code is  " + refer_code);
        String RC1 = RngHelper.checkRC1Coupon("'%RC1-" + refer_code + "%'");
        System.out.println("RC1 for this user is     " + RC1);
        Assert.assertEquals(RC1, "RC1-" + refer_code);
        // get order placed and delivered
        System.out.println("referralCode used to signUp is  " + referral);
        rngHelper.delivered();
        System.out.println("RC1 for this user is     " + RC1);
        System.out.println("1st time login user referral code is  " + refer_code);
        String RC2 = RngHelper.checkRC2Coupon("'%RC2-" + refer_code + "%'");
        System.out.println("RC2 for this user is   " + RC2);
        Assert.assertEquals(RC2, "RC2-" + refer_code);
        System.out.println("referralCode used to signUp is  " + referral);
        Processor getResponse = RngHelper.getReferralUserId(userId);
    }

    @Test(dataProvider = "referralUserIdGetNPostData", dataProviderClass = DP.class)
    public void referralUserIdGetNPost(String count, String updatedAt, String name, String mobile, String email,
                                       String password) throws Exception {

        String referral_UserId = RngHelper.getReferralCodeFromWpUsers();
        String referralUserId[] = referral_UserId.split(",");
        String refCode = referralUserId[0];
        String user_Id = referralUserId[1];
        String mboli_num = RngHelper.checkUserByMobile(mobile);
        if (mobile.equalsIgnoreCase(mboli_num)) {
            RngHelper.deleteUserByMobile(mobile);
        }
        Processor tidProcessor = RngHelper.signUpWithReferral(name, mobile, email, password, refCode);
        String tid = String.valueOf(tidProcessor.ResponseValidator.GetNodeValue("$.tid"));
        System.out.println("tid for signup is   " + tid);
        String otp = SnDHelper.getOTP(tid);
        SnDHelper.verifyOTP(otp, tid);
        // Sign in 1st time
        SnDHelper.consumerLogin(mobile, password);
        String userId = RngHelper.getUserByMobile(mobile);
        Processor P = SnDHelper.consumerLoginCheck(mobile);
        String login_verify = P.ResponseValidator.GetNodeValue("$.statusMessage");
        System.out.println("Verify login    " + login_verify);
        System.out.println("referralCode  used for signUp is    " + refCode);
        Processor postResponse = RngHelper.postRefferalUserId(userId, count, updatedAt);
        Processor getResponse = RngHelper.getReferralUserId(userId);
        String check_count = String.valueOf(getResponse.ResponseValidator.GetNodeValueAsInt("$.data.count"));
        Assert.assertEquals(check_count, count);

    }

    @Test(dataProvider = "getReferralUserIdData", dataProviderClass = DP.class)
    public void getReferralUserId(String name, String mobile, String email, String password, String userId) {
        String newLogin_userId = "";

        if (userId.isEmpty()) {
            String mboli_num = RngHelper.checkUserByMobile(mobile);
            if (mobile.equalsIgnoreCase(mboli_num)) {
                RngHelper.deleteUserByMobile(mobile);
            }
            Processor tidProcessor = SnDHelper.consumerSignUp(name, mobile, email, password);
            String tid = String.valueOf(tidProcessor.ResponseValidator.GetNodeValue("$.tid"));
            System.out.println("tid for signup is   " + tid);
            String otp = SnDHelper.getOTP(tid);
            SnDHelper.verifyOTP(otp, tid);
            // Sign in 1st time
            Processor loginProcessor = SnDHelper.consumerLogin(mobile, password);
            newLogin_userId = loginProcessor.ResponseValidator.GetNodeValue("$.data.customer_id");
        }
        int userId1;
        Processor resp;

        if (userId.isEmpty()) {
            userId = newLogin_userId;
        }
        resp = RngHelper.getReferralUserId(userId);
        if (userId.equalsIgnoreCase(null)) {
            String message = resp.ResponseValidator.GetNodeValue("$.message");
            int statusCode = resp.ResponseValidator.GetNodeValueAsInt("$.status");
            Assert.assertEquals(message, "Bad Request");
            Assert.assertEquals(statusCode, 400);
        }

        if (userId.equalsIgnoreCase("0") || userId.equals(newLogin_userId)) {
            String statusMessage = String.valueOf(resp.ResponseValidator.GetNodeValue("$.statusMessage"));
            String data = String.valueOf(resp.ResponseValidator.GetNodeValue("$.data"));
            Assert.assertEquals(data, "null");
            Assert.assertEquals(statusMessage, "SUCCESS");
        }
    }

    @Test(dataProvider = "postReferralUserIdData", dataProviderClass = DP.class)
    public void postReferralUserId(String name, String mobile, String email, String password, String userId,
                                   String count, String updatedAt) throws InterruptedException {
        if (userId.isEmpty() || userId.equals("null")) {
            String mboli_num = RngHelper.checkUserByMobile(mobile);
            if (mobile.equalsIgnoreCase(mboli_num)) {
                RngHelper.deleteUserByMobile(mobile);
            }
            Processor tidProcessor = SnDHelper.consumerSignUp(name, mobile, email, password);
            String tid = String.valueOf(tidProcessor.ResponseValidator.GetNodeValue("$.tid"));
            System.out.println("tid for signup is   " + tid);
            String otp = SnDHelper.getOTP(tid);
            SnDHelper.verifyOTP(otp, tid);
        }
        // Sign in 1st time.
        Processor loginProcessor = SnDHelper.consumerLogin(mobile, password);
        String login_userid = loginProcessor.ResponseValidator.GetNodeValue("$.data.customer_id").toString();
        if (userId.isEmpty()) {
            userId = login_userid;
        }
        Processor respProcessor = RngHelper.postRefferalUserId(userId, count, updatedAt);
        if (userId.equalsIgnoreCase("0") || userId.equalsIgnoreCase(login_userid)) {
            String statusMessage = respProcessor.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(statusMessage, "SUCCESS");
        }
        Thread.sleep(1000);

    }

    @Test(dataProvider = "createMultiApplyCouponData", dataProviderClass = DP.class)
    public void createMultiApplyCouponTest(HashMap<String, String> a1, String type) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        int responseId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseId, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createDuplicateMultiApplyCouponData", dataProviderClass = DP.class)
    public void createDuplicateMultiApplyCouponTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        String code = a1.get(Integer.toString(0));
        String codeInDB = RngHelper.checkMultiApplyCoupon(code);
        if (codeInDB.equalsIgnoreCase(code)) {
            RngHelper.deleteCoupon(code);
        }
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        int responseId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseId, null);
        Assert.assertEquals(responseSatusCode, 1);
        Processor processor = RngHelper.createMultiApplyCoupon(a1, type);
        String resonseData = processor.ResponseValidator.GetNodeValue("$.data");
        int responseCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(resonseData, null);
        Assert.assertEquals(responseCode, 0);

        String response = processor.ResponseValidator.GetNodeValue("$.data.message");
        Assert.assertEquals(response, "Coupon Already Exist");
    }

    @Test(dataProvider = "createMultiApplyCouponTransactionsData", dataProviderClass = DP.class)
    public void createMultiApplyCouponTransactionsTest(HashMap<String, String> a1, String type, String totalPerUser)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        if (!(totalPerUser.equalsIgnoreCase("equal"))) {
            String responseStatusMessage = processorMulti.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(responseStatusMessage, "totalPerUserEqualToNoOfTransactions is Invalid");
        }
        if (totalPerUser.equalsIgnoreCase("equal")) {
            int responseId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
            int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertNotEquals(responseId, null);
            Assert.assertEquals(responseSatusCode, 1);
        }
    }

    @Test(dataProvider = "createMultiApplyCouponUsageCountData", dataProviderClass = DP.class)
    public void createMultiApplyCouponUsageCountTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        System.out.println("Expected fail-----------------------> UsageCOuntTCs");
        Assert.assertEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 0);

    }

    @Test(dataProvider = "createMultiApplyTypeOfCouponsData", dataProviderClass = DP.class)
    public void createMultiApplyTypeOfCouponsTest(HashMap<String, String> a1, String type) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createInvalidMultiApplyTypeOfCouponsData", dataProviderClass = DP.class)
    public void createInvalidMultiApplyTypeOfCouponsTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        System.out.println("Expected fail-------------------Type of Coupon in MultiApply");
        Assert.assertEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 0);

    }

    @Test(dataProvider = "createMultiApplyCouponCartMinAmountData", dataProviderClass = DP.class)
    public void createMultiApplyCouponCartMinAmountTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponInvalidCartMinAmountData", dataProviderClass = DP.class)
    public void createMultiApplyCouponInvalidCartMinAmountTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponCartMinQtyData", dataProviderClass = DP.class)
    public void createMultiApplyCouponCartMinQtyTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponInvalidCartMinQtyData", dataProviderClass = DP.class)
    public void createMultiApplyCouponInvalidCartMinQtyTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponDiscountAmountData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponDiscountAmountTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponInvalidDiscountAmountData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponInvalidDiscountAmountTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String discountAmount = a1.get(Integer.toString(54));
        String discountPercent = a1.get(Integer.toString(55));
        if (discountAmount.equals("null") && discountPercent.equals("null")
                || discountAmount.equals("0") && discountPercent.equals("0")) {
            Assert.assertEquals(responseData, null,
                    "Expected Fail----- null and Zero is handled if both discountAmount & discountPercent set to null or zero");
            Assert.assertEquals(responseSatusCode, 0, "");
        } else {
            Assert.assertNotEquals(responseData, null);
            Assert.assertEquals(responseSatusCode, 1);
        }
    }

    @Test(dataProvider = "createMultiApplyCouponFreeShippingData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponFreeShippingTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String discountAmount = a1.get(Integer.toString(54));
        String discountPercent = a1.get(Integer.toString(55));
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponuserClientData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponuserClientTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String discountAmount = a1.get(Integer.toString(54));
        String discountPercent = a1.get(Integer.toString(55));
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponDiscountItemData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponDiscountItemTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String discountAmount = a1.get(Integer.toString(54));
        String discountPercent = a1.get(Integer.toString(55));
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponInvalidDiscountItemData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponInvalidDiscountItemTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String discountAmount = a1.get(Integer.toString(54));
        String discountPercent = a1.get(Integer.toString(55));
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponDescriptionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponDescriptionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String discountAmount = a1.get(Integer.toString(54));
        String discountPercent = a1.get(Integer.toString(55));
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponInvalidDescriptionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponInvalidDescriptionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String discountAmount = a1.get(Integer.toString(54));
        String discountPercent = a1.get(Integer.toString(55));
        Assert.assertEquals(responseData, null, "Expected fail in Description-empty String is not handled");
        Assert.assertEquals(responseSatusCode, 0);

    }

    @Test(dataProvider = "createMultiApplyCouponTitleData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponTitleTest(HashMap<String, String> a1, String type) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String discountAmount = a1.get(Integer.toString(54));
        String discountPercent = a1.get(Integer.toString(55));
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponInvalidTitleData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponInvalidTitleTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String discountAmount = a1.get(Integer.toString(54));
        String discountPercent = a1.get(Integer.toString(55));
        Assert.assertEquals(responseData, null, "Expected fail in Title-empty Stringis not handled");
        Assert.assertEquals(responseSatusCode, 0);

    }

    @Test(dataProvider = "createMultiApplyCouponTermsAndConditionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponTermsAndConditionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponInvalidTermsAndConditionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponInvalidTermsAndConditionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(responseData, null, "Expected fail in TNC-empty String is not handled");
        Assert.assertEquals(responseSatusCode, 0);

    }

    @Test(dataProvider = "createMultiApplyCouponLogoIdData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponLogoIdTest(HashMap<String, String> a1, String type) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponInvalidLogoIdData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponInvalidLogoIdTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(responseData, null, "Expected fail in LogoID-empty String is not handled");
        Assert.assertEquals(responseSatusCode, 0);

    }

    @Test(dataProvider = "createMultiApplyFirstOrderRestrictionCouponData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyFirstOrderRestrictionCouponTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponPreferredPaymentMethodData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponPreferredPaymentMethodTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponInvalidPreferredPaymentMethodData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponInvalidPreferredPaymentMethodTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(responseData, null, "expected fail for Payment method");
        Assert.assertEquals(responseSatusCode, 0);

    }

    @Test(dataProvider = "createMultiApplyExpiryOffsetCouponData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyExpiryOffsetCouponTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponApplicableWithSwiggyMoneyData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponApplicableWithSwiggyMoneyTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponApplicableInReferralCouponPresenceData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponApplicableInReferralCouponPresenceTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponAutoApplyData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponAutoApplyTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponCustomerRestrictionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponCustomerRestrictionData(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponCityRestrictionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponCityRestrictionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponAreaRestrictionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponAreaRestrictionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponCuisineRestrictionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponCuisineRestrictionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponRestaurantRestrictionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponRestaurantRestrictionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);

    }

    @Test(dataProvider = "createMultiApplyCouponCategoryRestrictionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponCategoryRestrictionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
    }

    @Test(dataProvider = "createMultiApplyCouponItemRestrictionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponItemRestrictionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
    }

    @Test(dataProvider = "createMultiApplyCouponSlotRestrictionData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponSlotRestrictionTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
    }

    @Test(dataProvider = "createMultiApplyCouponFreeGiftsData", dataProviderClass = MultiApplyDP.class)
    public void createMultiApplyCouponFreeGiftsTest(HashMap<String, String> a1, String type)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
    }

    @Test(dataProvider = "updateMultiApplyCouponTotalAvailableData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponTotalAvailableTest(HashMap<String, String> a1, String type, String key,
                                                         String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String id = Integer.toBinaryString(couponId);
        String code = RngHelper.getMultiApplyCouponId(id);
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        int totalCount = updateResp.ResponseValidator.GetNodeValueAsInt("$.data.totalAvailable");
        String totalAvail = Integer.toString(totalCount);
        Assert.assertEquals(totalAvail, updateValue);
    }

    @Test(dataProvider = "updateMultiApplyCouponValidFromData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponValidFromTest(HashMap<String, String> a1, String type, String key,
                                                    String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String id = Integer.toBinaryString(couponId);
        String code = RngHelper.getMultiApplyCouponId(id);
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        String validFrom = String
                .valueOf(updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.validFrom"));
        Assert.assertEquals(validFrom, updateValue);
    }

    @Test(dataProvider = "updateMultiApplyCouponValidTillData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponValidTillTest(HashMap<String, String> a1, String type, String key,
                                                    String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        // String id=Integer.toString(couponId);
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        String validTill = String
                .valueOf(updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.validTill"));
        Assert.assertEquals(validTill, updateValue);
    }

    @Test(dataProvider = "updateMultiApplyCoupoAutoApplyData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCoupoAutoApplyTest(HashMap<String, String> a1, String type, String key,
                                                   String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        // String id=Integer.toString(couponId);
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        String autoApply = String
                .valueOf(updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.autoApply"));
        Assert.assertEquals(autoApply, updateValue);
    }

    @Test(dataProvider = "updateMultiApplyCouponApplicableWithSwiggyMoneyData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponApplicableWithSwiggyMoneyTest(HashMap<String, String> a1, String type, String key,
                                                                    String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        // String id=Integer.toString(couponId);
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        String applicableWithSwiggyMoney = String.valueOf(
                updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.applicableWithSwiggyMoney"));
        Assert.assertEquals(applicableWithSwiggyMoney, updateValue);
    }

    @Test(dataProvider = "updateMultiApplyCouponFreeGiftsData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponFreeGiftsTest(HashMap<String, String> a1, String type, String key,
                                                    String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        // String id=Integer.toString(couponId);
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        String freeGifts = String
                .valueOf(updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.freeGifts"));
        Assert.assertEquals(freeGifts, updateValue);
    }

    @Test(dataProvider = "updateMultiApplyCouponCouponTypeData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponCouponTypeTest(HashMap<String, String> a1, String type, String key,
                                                     String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(code);
        String updatedCouponType = getCouponResp.ResponseValidator.GetNodeValue("$.data.metadata[0].couponType");
        Assert.assertEquals(updatedCouponType, updateValue);

    }

    @Test(dataProvider = "updateMultiApplyCouponTitleData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponTitleTest(HashMap<String, String> a1, String type, String key, String updateValue)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(code);
        if (key.equalsIgnoreCase("43")) {
            String updatedCouponTitle = getCouponResp.ResponseValidator.GetNodeValue("$.data.metadata[0].title");
            Assert.assertEquals(updatedCouponTitle, updateValue);
        } else {
            String updatedCouponTitleTwo = getCouponResp.ResponseValidator.GetNodeValue("$.data.metadata[1].title");
            System.out.println("Transaction two updated title is -----> " + updatedCouponTitleTwo);
            Assert.assertEquals(updatedCouponTitleTwo, updateValue);
        }
    }

    @Test(dataProvider = "updateMultiApplyCouponDescriptionData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponDescriptionTest(HashMap<String, String> a1, String type, String key,
                                                      String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(code);
        if (key.equalsIgnoreCase("42")) {
            String updatedCouponDescription = getCouponResp.ResponseValidator
                    .GetNodeValue("$.data.metadata[0].description");
            Assert.assertEquals(updatedCouponDescription, updateValue);
        } else {
            String updatedCouponDescriptionTwo = getCouponResp.ResponseValidator
                    .GetNodeValue("$.data.metadata[1].description");
            System.out.println("Transaction two updated Description is ----->" + updatedCouponDescriptionTwo);
            Assert.assertEquals(updatedCouponDescriptionTwo, updateValue);
        }

    }

    @Test(dataProvider = "updateMultiApplyCouponLogoData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponLogoTest(HashMap<String, String> a1, String type, String key, String updateValue)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(code);
        if (key.equalsIgnoreCase("45")) {
            String updatedCouponLogoOne = getCouponResp.ResponseValidator.GetNodeValue("$.data.metadata[0].logoId");
            Assert.assertEquals(updatedCouponLogoOne, updateValue);
        } else {
            String updatedCouponLogoTwo = getCouponResp.ResponseValidator.GetNodeValue("$.data.metadata[1].logoId");
            System.out.println("Transaction two updated logo is ----->" + updatedCouponLogoTwo);
            Assert.assertEquals(updatedCouponLogoTwo, updateValue);
        }

    }

    @Test(dataProvider = "updateMultiApplyCouponCartMinAmountData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponCartMinAmountTest(HashMap<String, String> a1, String type, String key,
                                                        String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(code);
        if (key.equalsIgnoreCase("34")) {
            String updatedCouponCartMinAmountOne = String.valueOf(
                    getCouponResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].cartMinAmount"));
            Assert.assertEquals(updatedCouponCartMinAmountOne, updateValue);
        } else {
            String updatedCouponCartMinAmountTwo = String.valueOf(
                    getCouponResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].cartMinAmount"));
            System.out.println("Transaction two updated CartMinAmount is ----->" + updatedCouponCartMinAmountTwo);
            Assert.assertEquals(updatedCouponCartMinAmountTwo, updateValue);
        }

    }

    @Test(dataProvider = "updateMultiApplyCouponCartMinQuantityData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponCartMinQuantityTest(HashMap<String, String> a1, String type, String key,
                                                          String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(code);
        if (key.equalsIgnoreCase("35")) {
            int updatedCouponCartMinAmountoOne = getCouponResp.ResponseValidator
                    .GetNodeValueAsInt("$.data.metadata[0].cartMinQty");
            Assert.assertEquals(updatedCouponCartMinAmountoOne, Integer.parseInt(updateValue));
        } else {
            int updatedCouponCartMinQtyTwo = getCouponResp.ResponseValidator
                    .GetNodeValueAsInt("$.data.metadata[1].cartMinQty");
            System.out.println("Transaction two updated CartMinQty is ----->" + updatedCouponCartMinQtyTwo);
            Assert.assertEquals(updatedCouponCartMinQtyTwo, Integer.parseInt(updateValue));
        }

    }

    @Test(dataProvider = "updateMultiApplyCouponTncData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponTncTest(HashMap<String, String> a1, String type, String key, String updateValue)
            throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(code);
        updateValue = "[" + updateValue + "]";
        if (key.equalsIgnoreCase("44")) {
            String updatedCouponTnc = String.valueOf(
                    getCouponResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].tnc"));

            Assert.assertEquals(updatedCouponTnc, updateValue);
        } else {
            String updatedCouponTncTwo = String.valueOf(
                    getCouponResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].tnc"));
            System.out.println("Transaction two updated Tnc is -----> " + updatedCouponTncTwo);
            Assert.assertEquals(updatedCouponTncTwo, updateValue);
        }

    }

    @Test(dataProvider = "updateMultiApplyCouponDiscountPercentData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponDiscountPercentTest(HashMap<String, String> a1, String type, String key,
                                                          String updateValue) throws InterruptedException {
        Processor processorMulti = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = processorMulti.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int responseSatusCode = processorMulti.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(responseData, null);
        Assert.assertEquals(responseSatusCode, 1);
        int couponId = processorMulti.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String code = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, code, key, updateValue);
        String updateData = updateResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertNotEquals(updateData, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(code);
        if (key.equalsIgnoreCase("37")) {
            String updatedCouponDiscountPercentOne = String.valueOf(getCouponResp.ResponseValidator
                    .GetNodeValueAsStringFromJsonArray("$.data.metadata[0].discountPercent"));
            Assert.assertEquals(updatedCouponDiscountPercentOne, updateValue);
        } else {
            String updatedCouponDiscountPercentTwo = String.valueOf(getCouponResp.ResponseValidator
                    .GetNodeValueAsStringFromJsonArray("$.data.metadata[1].discountPercent"));
            System.out.println("Transaction two updated discountPercent is ----->" + updatedCouponDiscountPercentTwo);
            Assert.assertEquals(updatedCouponDiscountPercentTwo, updateValue);
        }

    }

    @Test(dataProvider = "updateMultiApplyCouponDiscountAmountData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponDiscountAmountTest(HashMap<String, String> a1, String type, String Key,
                                                         String valueUpdate, String secondKey, String updateValueSecond) {

        Processor createResp = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = String.valueOf(createResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotEquals(responseData, null);
        int couponId = createResp.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String couponCode = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, valueUpdate);
        Assert.assertNotEquals(updateResp, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedDiscountAmount = String.valueOf(
                getCouponResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].discountAmount"));
        Assert.assertNotEquals(updatedDiscountAmount, valueUpdate);

        Processor updateRespSecond = RngHelper.updateMultiApplyCoupon(a1, couponCode, secondKey, updateValueSecond);
        Assert.assertNotEquals(updateRespSecond, null);
        Processor getCoupon = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedDiscountAmountSecond = String.valueOf(
                getCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].discountAmount"));
        Assert.assertNotEquals(updatedDiscountAmountSecond, updateValueSecond);
    }

    @Test(dataProvider = "updateMultiApplyCouponUpperCapData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponUpperCapTest(HashMap<String, String> a1, String type, String Key,
                                                   String valueUpdate, String secondKey, String updateValueSecond) {

        Processor createResp = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = String.valueOf(createResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotEquals(responseData, null);
        int couponId = createResp.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String couponCode = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, valueUpdate);
        Assert.assertNotEquals(updateResp, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedUpperCap = String.valueOf(
                getCouponResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].upperCap"));
        Assert.assertEquals(updatedUpperCap, valueUpdate);

        Processor updateRespSecond = RngHelper.updateMultiApplyCoupon(a1, couponCode, secondKey, updateValueSecond);
        Assert.assertNotEquals(updateRespSecond, null);
        Processor getCoupon = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedUpperCapSecond = String
                .valueOf(getCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].upperCap"));
        Assert.assertEquals(updatedUpperCapSecond, updateValueSecond);
    }

    @Test(dataProvider = "updateMultiApplyCouponFreeShippingData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponFreeShippingTest(HashMap<String, String> a1, String type, String Key,
                                                       String valueUpdate, String secondKey, String updateValueSecond) {
        Processor createResp = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = String.valueOf(createResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotEquals(responseData, null);
        int couponId = createResp.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String couponCode = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, valueUpdate);
        Assert.assertNotEquals(updateResp, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedFreeShipping = String.valueOf(
                getCouponResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].freeShipping"));
        Assert.assertEquals(updatedFreeShipping, valueUpdate);

        Processor updateRespSecond = RngHelper.updateMultiApplyCoupon(a1, couponCode, secondKey, updateValueSecond);
        Assert.assertNotEquals(updateRespSecond, null);
        Processor getCoupon = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedFreeShippingSecond = String.valueOf(
                getCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].freeShipping"));
        Assert.assertEquals(updatedFreeShippingSecond, updateValueSecond);
    }

    @Test(dataProvider = "updateMultiApplyCouponFirstOrderRestrictionData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponFirstOrderRestrictionTest(HashMap<String, String> a1, String type, String Key,
                                                                String valueUpdate, String updateValueSecond) {
        Processor createResp = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = String.valueOf(createResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotEquals(responseData, null);
        int couponId = createResp.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String couponCode = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, valueUpdate);
        Assert.assertNotEquals(updateResp, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedFreeShipping = String.valueOf(getCouponResp.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.metadata[0].firstOrderRestriction"));
        Assert.assertEquals(updatedFreeShipping, valueUpdate);
        Processor updateRespSecond = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, updateValueSecond);
        Assert.assertNotEquals(updateRespSecond, null);
        Processor getCoupon = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedFreeShippingSecond = String.valueOf(getCoupon.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.metadata[0].firstOrderRestriction"));
        Assert.assertEquals(updatedFreeShippingSecond, updateValueSecond);
    }

    @Test(dataProvider = "updateMultiApplyCouponPreferredPaymentMethodData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponPreferredPaymentMethodTest(HashMap<String, String> a1, String type, String Key,
                                                                 String valueUpdate, String secondKey, String updateValueSecond) {
        Processor createResp = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = String.valueOf(createResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotEquals(responseData, null);
        int couponId = createResp.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String couponCode = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, valueUpdate);
        Assert.assertNotEquals(updateResp, null);
        valueUpdate = "[" + valueUpdate + "]";
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedPreferredPaymethod = String.valueOf(getCouponResp.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.metadata[0].preferredPaymentMethod"));
        Assert.assertEquals(updatedPreferredPaymethod, valueUpdate);
        Processor updateRespSecond = RngHelper.updateMultiApplyCoupon(a1, couponCode, secondKey, updateValueSecond);
        Assert.assertNotEquals(updateRespSecond, null);
        updateValueSecond = "[" + updateValueSecond + "]";
        Processor getCoupon = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedPreferredPaymethodSecond = String.valueOf(getCoupon.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.metadata[1].preferredPaymentMethod"));
        Assert.assertEquals(updatedPreferredPaymethodSecond, updateValueSecond);
    }

    @Test(dataProvider = "updateMultiApplyCouponUserClientData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponUserClientTest(HashMap<String, String> a1, String type, String Key,
                                                     String valueUpdateZero, String secondKey, String updateValueOne, String updateValueTwo,
                                                     String updateValueThree, String updateValueFour) {
        Processor createResp = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = String.valueOf(createResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotEquals(responseData, null);
        int couponId = createResp.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String couponCode = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));

        // user client zero
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, valueUpdateZero);
        Assert.assertNotEquals(updateResp, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedUserClient = String.valueOf(
                getCouponResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].userClient"));
        Assert.assertEquals(updatedUserClient, valueUpdateZero);

        // user client one
        Processor updateRespSecond = RngHelper.updateMultiApplyCoupon(a1, couponCode, secondKey, updateValueOne);
        Assert.assertNotEquals(updateRespSecond, null);

        Processor getCoupon = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedUserClientSecond = String.valueOf(
                getCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].userClient"));
        Assert.assertEquals(updatedUserClientSecond, updateValueOne);

        // user client two
        Processor updateRespThird = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, updateValueTwo);
        Assert.assertNotEquals(updateRespThird, null);
        Processor getCouponRespThird = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedUserClientThird = String.valueOf(getCouponRespThird.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.metadata[0].userClient"));
        Assert.assertEquals(updatedUserClientThird, updateValueTwo);

        // user client three
        Processor updateRespFourth = RngHelper.updateMultiApplyCoupon(a1, couponCode, secondKey, updateValueThree);
        Assert.assertNotEquals(updateRespFourth, null);
        Processor getCouponFourth = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedUserClientFourth = String.valueOf(
                getCouponFourth.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].userClient"));
        Assert.assertEquals(updatedUserClientFourth, updateValueThree);

        // user client four
        Processor updateRespFivth = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, updateValueFour);
        Assert.assertNotEquals(updateRespFivth, null);
        Processor getCouponFivth = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedUserClientFivth = String.valueOf(
                getCouponFivth.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].userClient"));
        Assert.assertEquals(updatedUserClientFivth, updateValueFour);
    }

    @Test(dataProvider = "updateMultiApplyCouponOffsetData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponOffsetTest(HashMap<String, String> a1, String type, String Key,
                                                 String valueUpdate, String secondKey, String updateValueSecond, String updateValueThird) {
        Processor createResp = RngHelper.createMultiApplyCoupon(a1, type);
        String responseData = String.valueOf(createResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotEquals(responseData, null);
        int couponId = createResp.ResponseValidator.GetNodeValueAsInt("$.data.id");
        String couponCode = RngHelper.getMultiApplyCouponId(Integer.toString(couponId));
        // offset 0
        Processor updateResp = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, valueUpdate);
        Assert.assertNotEquals(updateResp, null);
        Processor getCouponResp = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedOffset = String.valueOf(getCouponResp.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.metadata[0].offset"));
        Assert.assertEquals(updatedOffset, valueUpdate);
        //offset -1
        Processor updateRespSecond = RngHelper.updateMultiApplyCoupon(a1, couponCode, secondKey, updateValueSecond);
        Assert.assertNotEquals(updateRespSecond, null);
        Processor getCoupon = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedOffsetSecond = String.valueOf(getCoupon.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.metadata[1].offset"));
        Assert.assertEquals(updatedOffsetSecond, updateValueSecond);
        //offset 1
        Processor updateRespThird = RngHelper.updateMultiApplyCoupon(a1, couponCode, Key, updateValueThird);
        Assert.assertNotEquals(updateRespThird, null);
        Processor getCouponThird = RngHelper.getMultiApplyCoupon(couponCode);
        String updatedOffsetThird = String.valueOf(getCouponThird.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.metadata[0].offset"));
        Assert.assertEquals(updatedOffsetThird, updateValueThird);
    }

    @Test(dataProvider = "updateMultiApplyCouponForNonExistingCouponCodeData", dataProviderClass = MultiApplyDP.class)
    public void updateMultiApplyCouponForNonExistingCouponCodeTest(HashMap<String, String> a1, String nonExistingCouponCode, String Key,
                                                                   String valueUpdate) {

        Processor updateResponse = RngHelper.updateMultiApplyCoupon(a1, nonExistingCouponCode, Key, valueUpdate);
        String response = updateResponse.ResponseValidator.GetNodeValue("$.data");
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(response, null);
        Assert.assertEquals(statusCode, 0);

    }

    @Test(dataProvider = "checkMultiApplyCouponConfigurationInGetMultiApplyCouponData", dataProviderClass = MultiApplyDP.class)
    public void checkMultiApplyCouponConfigurationInGetMultiApplyCouponTest(HashMap<String, String> a1, String type) {

        Processor updateResponse = RngHelper.createMultiApplyCoupon(a1, type);
        String response = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        Processor getMultiApplyResp = RngHelper.getMultiApplyCoupon(couponCode);
        String data = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int getStatusCode = getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(data, null);
        Assert.assertEquals(getStatusCode, 1);
        String validFrom = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.validFrom"));
        String validTill = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.validTill"));
        String totalAvailable = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.totalAvailable"));
        String totalPerUser = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.totalPerUser"));
        String autoApply = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.autoApply"));
        String applicableWithSwiggyMoney = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.applicableWithSwiggyMoney"));
        String applicableInReferralCouponPresence = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.applicableInReferralCouponsPresence"));
        String customerRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.customerRestriction"));
        String cityRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cityRestriction"));
        String areaRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.areaRestriction"));
        String cuisineRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cuisineRestriction"));
        String restaurantRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantRestriction"));
        String categoryRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.categoryRestriction"));
        String itemRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.itemRestriction"));
        String slotRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slotRestriction"));
        String updatedBy = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.updatedBy");
        String createdOn = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.createdOn"));
        String updatedOn = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.updatedOn"));
        int expiryOffset = getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.data.expiryOffset");
        String freeGifts = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.freeGifts"));
        //String slotRestriction = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.slots");
        String isPrivate = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.isPrivate"));
        String name = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.name");
        String couponBucket = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.bucket");
        String vendor = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.data.vendor"));
        Assert.assertEquals(name, a1.get(Integer.toString(1)));
        Assert.assertEquals(couponBucket, a1.get(Integer.toString(2)));
        Assert.assertEquals(isPrivate, a1.get(Integer.toString(69)));
        Assert.assertEquals(Integer.toString(expiryOffset), a1.get(Integer.toString(25)));
        Assert.assertEquals(validFrom, a1.get(Integer.toString(17)));
        Assert.assertEquals(validTill, a1.get(Integer.toString(18)));
        Assert.assertEquals(totalAvailable, a1.get(Integer.toString(5)));
        Assert.assertEquals(totalPerUser, a1.get(Integer.toString(4)));
        Assert.assertEquals(autoApply, a1.get(Integer.toString(16)));
        Assert.assertEquals(applicableWithSwiggyMoney, a1.get(Integer.toString(14)));
        Assert.assertEquals(customerRestriction, a1.get(Integer.toString(6)));
        Assert.assertEquals(cityRestriction, a1.get(Integer.toString(7)));
        Assert.assertEquals(areaRestriction, a1.get(Integer.toString(8)));
        Assert.assertEquals(restaurantRestriction, a1.get(Integer.toString(10)));
        Assert.assertEquals(categoryRestriction, a1.get(Integer.toString(11)));
        Assert.assertEquals(itemRestriction, a1.get(Integer.toString(12)));
        Assert.assertEquals(freeGifts, a1.get(Integer.toString(26)));
        Assert.assertEquals(slotRestriction, a1.get(Integer.toString(13)));
        Assert.assertEquals(updatedBy, a1.get(Integer.toString(19)));
        Assert.assertEquals(createdOn, a1.get(Integer.toString(20)));
        Assert.assertEquals(updatedOn, a1.get(Integer.toString(21)));
        Assert.assertEquals(vendor, a1.get(Integer.toString(27)));

    }


    @Test(dataProvider = "checkMultiApplyCouponTransactionConfigurationInGetMultiApplyCouponData", dataProviderClass = MultiApplyDP.class)
    public void checkMultiApplyCouponTransactionConfigurationInGetMultiApplyCouponTest(HashMap<String, String> a1, String type) {

        Processor updateResponse = RngHelper.createMultiApplyCoupon(a1, type);
        String response = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        Processor getMultiApplyResp = RngHelper.getMultiApplyCoupon(couponCode);
        String data = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int getStatusCode = getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(data, null);
        Assert.assertEquals(getStatusCode, 1);
        // getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata..usageCount");
        String transactionOneCouponType = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.metadata[0].couponType");
        String transactionTwoCouponType = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.metadata[1].couponType");
        String transactionOneCartMinAmount = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].cartMinAmount"));
        String transactionTwoCartMinAmount = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].cartMinAmount"));
        String transactionOneCartMinQuantity = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].cartMinQty"));
        String transactionTwoCartMinQuantity = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].cartMinQty"));
        String transactionOneDiscountPercent = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].discountPercent"));
        String transactionTwoDiscountPercent = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].discountPercent"));
        String transactionOneDiscountAmount = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].discountAmount"));
        String transactionTwoDiscountAmount = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].discountAmount"));
        String transactionOneUpperCap = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].upperCap"));
        String transactionTwoUpperCap = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].upperCap"));
        String transactionOneFreeShipping = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].freeShipping"));
        String transactionTwoFreeShipping = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].freeShipping"));
        int transactionOneUserClient = getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.data.metadata[0].userClient");
        int transactionTwoUserClient = getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.data.metadata[1].userClient");
        int transactionOneDiscountItem = getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.data.metadata[0].discountItem");
        int transactionTwoDiscountItem = getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.data.metadata[1].discountItem");
        String transactionOneDescription = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.metadata[0].description");
        String transactionTwoDescription = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.metadata[1].description");
        String transactionOneTitle = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.metadata[0].title");
        String transactionTwoTitle = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.metadata[1].title");
        String transactionOneLogoId = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.metadata[0].logoId");
        String transactionTwoLogoId = getMultiApplyResp.ResponseValidator.GetNodeValue("$.data.metadata[1].logoId");
        String transactionOneTnc = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].tnc"));
        String transactionTwoTnc = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].tnc"));
        String transactionOneFirstOrderRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].firstOrderRestriction"));
        String transactionTwoFirstOrderRestriction = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].firstOrderRestriction"));
        String transactionOnePreferredPaymentMethod = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[0].preferredPaymentMethod"));
        String transactionTwoPreferredPaymentMethod = String.valueOf(getMultiApplyResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metadata[1].preferredPaymentMethod"));
        int transactionOneOffset = getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.data.metadata[0].offset");
        int transactionTwoOffset = getMultiApplyResp.ResponseValidator.GetNodeValueAsInt("$.data.metadata[1].offset");


        Assert.assertEquals(transactionOneCouponType, a1.get(Integer.toString(33)));
        Assert.assertEquals(transactionTwoCouponType, a1.get(Integer.toString(51)));
        Assert.assertEquals(transactionOneCartMinAmount, a1.get(Integer.toString(34)));
        Assert.assertEquals(transactionOneCartMinAmount, a1.get(Integer.toString(52)));
        Assert.assertEquals(transactionOneCartMinQuantity, a1.get(Integer.toString(35)));
        Assert.assertEquals(transactionTwoCartMinQuantity, a1.get(Integer.toString(53)));
        Assert.assertEquals(transactionTwoDiscountAmount, a1.get(Integer.toString(36)));
        Assert.assertEquals(transactionTwoDiscountAmount, a1.get(Integer.toString(54)));
        Assert.assertEquals(transactionOneDiscountPercent, a1.get(Integer.toString(37)));
        Assert.assertEquals(transactionTwoDiscountPercent, a1.get(Integer.toString(55)));
        Assert.assertEquals(transactionOneUpperCap, a1.get(Integer.toString(38)));
        Assert.assertEquals(transactionTwoUpperCap, a1.get(Integer.toString(56)));
        Assert.assertEquals(transactionOneFreeShipping, a1.get(Integer.toString(39)));
        Assert.assertEquals(transactionTwoFreeShipping, a1.get(Integer.toString(57)));
        Assert.assertEquals(Integer.toString(transactionOneUserClient), a1.get(Integer.toString(40)));
        Assert.assertEquals(Integer.toString(transactionTwoUserClient), a1.get(Integer.toString(58)));
        Assert.assertEquals(Integer.toString(transactionOneDiscountItem), a1.get(Integer.toString(41)));
        Assert.assertEquals(Integer.toString(transactionTwoDiscountItem), a1.get(Integer.toString(59)));
        Assert.assertEquals(transactionOneDescription, a1.get(Integer.toString(42)));
        Assert.assertEquals(transactionTwoDescription, a1.get(Integer.toString(60)));
        Assert.assertEquals(transactionOneTitle, a1.get(Integer.toString(43)));
        Assert.assertEquals(transactionTwoTitle, a1.get(Integer.toString(61)));
        Assert.assertEquals(transactionOneTnc, "[" + a1.get(Integer.toString(44)) + "]");
        Assert.assertEquals(transactionTwoTnc, "[" + a1.get(Integer.toString(62)) + "]");
        Assert.assertEquals(transactionOneLogoId, a1.get(Integer.toString(45)));
        Assert.assertEquals(transactionTwoLogoId, a1.get(Integer.toString(63)));
        Assert.assertEquals(transactionOneFirstOrderRestriction, a1.get(Integer.toString(46)));
        Assert.assertEquals(transactionOneFirstOrderRestriction, a1.get(Integer.toString(64)));
        Assert.assertEquals(transactionOnePreferredPaymentMethod, "[" + a1.get(Integer.toString(47)) + "]");
        Assert.assertEquals(transactionTwoPreferredPaymentMethod, "[" + a1.get(Integer.toString(65)) + "]");
        Assert.assertEquals(Integer.toString(transactionOneOffset), a1.get(Integer.toString(49)));
        Assert.assertEquals(Integer.toString(transactionTwoOffset), a1.get(Integer.toString(67)));

    }

    @Test(dataProvider = "applyMultiApplyCouponSiwggyVendorData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponSiwggyVendorTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        //String description= applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");

    }

    @Test(dataProvider = "applyMultiApplyCouponDominosVendorData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponDominosVendorTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {

        //if try to create multiApply coupon for Dominos(12) vendor then it will defaultly create for swiggy(0),coz in code it is hardCoded and incase of APPLy not checking for Vendor type
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponError = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCodeResp, 1);
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_ON_RESTAURANT");
    }

    @Test(dataProvider = "applyMultiApplyCouponInvalidVendorData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponInvalidVendorTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);

        //if try to create multiApply coupon for Dominos(12) vendor then it will defaultly create for swiggy(0) coz in code it is hardCoded and incase of APPLy not checking for Vendor type
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");
    }


    @Test(dataProvider = "applyMultiApplyCouponInvalidVendorNegativeData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponInvalidVendorNegativeTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);

        //if try to create multiApply coupon for Dominos(12) vendor then it will defaultly create for swiggy(0) coz in code it is hardCoded and incase of APPLy not checking for Vendor type
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponError = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_ON_RESTAURANT");
    }

    @Test(dataProvider = "applyFirstOrderRestrictionTrueMultiApplyCouponData", dataProviderClass = MultiApplyDP.class)
    public void applyFirstOrderRestrictionTrueMultiApplyCouponTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        //String description= applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");

    }

    @Test(dataProvider = "applyFirstOrderRestrictionFalseMultiApplyCouponData", dataProviderClass = MultiApplyDP.class)
    public void applyFirstOrderRestrictionFalseMultiApplyCouponTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        //String description= applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(couponErrorMessage, "E_COUPON_APPLICABLE_ONLY_ON_FIRST_ORDER");

    }

    @Test(dataProvider = "applyFirstOrderRestrictionMultiApplyCouponData", dataProviderClass = MultiApplyDP.class)
    public void applyFirstOrderRestrictionMultiApplyCouponTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        //String description= applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");


    }

    @Test(dataProvider = "applyFirstOrderRestrictionMultiApplyCouponNegativeTest", dataProviderClass = MultiApplyDP.class)
    public void applyFirstOrderRestrictionMultiApplyCouponNegativeTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        //String description= applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");


    }

    @Test(dataProvider = "applyFirstOrderRestrictionNullMultiApplyCouponData", dataProviderClass = MultiApplyDP.class)
    public void applyFirstOrderRestrictionNullMultiApplyCouponTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        //String description= applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(couponErrorMessage, "E_COUPON_APPLICABLE_ONLY_ON_FIRST_ORDER");

    }

    @Test(dataProvider = "applyNonExistingMultiApplyCouponCodeData", dataProviderClass = MultiApplyDP.class)
    public void applyNonExistingMultiApplyCouponCodeData(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {

        String codeExistingStatus = RngHelper.checkMultiApplyCoupon(createPayload.get(Integer.toString(0)));

        if (codeExistingStatus != null) {
            RngHelper.deleteCoupon(createPayload.get(Integer.toString(0)));
        }
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(couponErrorMessage, "E_COUPON_DOES_NOT_EXIST");

    }

    @Test(dataProvider = "applyMultiApplyCouponApplicableWithSwiggyMoneyTrueData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponApplicableWithSwiggyMoneyTrueTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(couponErrorMessage, null);
    }

    @Test(dataProvider = "applyMultiApplyCouponApplicableWithSwiggyMoneyFalseData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponApplicableWithSwiggyMoneyFalseTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(couponErrorMessage, null);
    }

    @Test(dataProvider = "applyMultiApplyCouponApplicableWithSwiggyMoneyData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponApplicableWithSwiggyMoneyTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(couponErrorMessage, "E_COUPON_NOT_APPLICABLE_WITH_SWIGGY_MONEY");
    }

    @Test(dataProvider = "applyMultiApplyCouponApplicableWithSwiggyMoneyTwoData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponApplicableWithSwiggyMoneyTwoTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(couponErrorMessage, null);
    }

    @Test(dataProvider = "applyMultiApplyCouponIsApplicableWithSwiggyMoneyData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponIsApplicableWithSwiggyMoneyTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(couponErrorMessage, "E_COUPON_NOT_APPLICABLE_WITH_SWIGGY_MONEY");
    }

    @Test(dataProvider = "applyMultiApplyCouponApplyDateIsWithInOrEqualTOValidTillDateData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponApplyDateIsWithInOrEqualTOValidTillDateTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(couponErrorMessage, null);
    }

    @Test(dataProvider = "applyMultiApplyCouponApplyDateIsGreaterThanValidTillDateData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponApplyDateIsGreaterThanValidTillDateTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "false");
        Assert.assertEquals(couponErrorMessage, "E_COUPON_EXPIRED");
    }

    @Test(dataProvider = "applyMultiApplyCouponApplyDateIsSameValidFromDateData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponApplyDateIsSameValidFromDateTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload, HashMap<String, String> applyPayloadForSecondTransaction) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(couponErrorMessage, null);
        String userId = applyPayload.get(Integer.toString(9));
        String orderId = "45678";
        RngHelper.couponUsageCount(couponCode, userId, orderId);
//        String usageCount = RngHelper.getMultiApplyCouponUsageCount(couponId);
//        Assert.assertEquals(usageCount, "1");
        Assert.assertEquals("1", couponRedisHelper.getCouponUsageCountRedis(couponCode), "uses count is not matching with redis data");
        applyPayloadForSecondTransaction.put("0", couponCode);
        Processor applySecondTransactionResponse = rngHelper.couponApply(applyPayloadForSecondTransaction);
        String dataRespSecondTransaction = String.valueOf(applySecondTransactionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeRespSecondTransaction = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String validSecondTransaction = String.valueOf(applySecondTransactionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessageSecondTransaction = applySecondTransactionResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataRespSecondTransaction, null);
        Assert.assertEquals(statusCodeRespSecondTransaction, 1);
        Assert.assertEquals(validSecondTransaction, "true");
        Assert.assertEquals(couponErrorMessageSecondTransaction, null);
        String orderIdTransactionTwo = "22222";
        RngHelper.couponUsageCount(couponCode, userId, orderIdTransactionTwo);
//        String usageCountSecondTime = RngHelper.getMultiApplyCouponUsageCount(couponId);
//        Assert.assertEquals(usageCountSecondTime, "2");
        Assert.assertEquals("2", couponRedisHelper.getCouponUsageCountRedis(couponCode), "uses count is not matching with redis data");

    }

    @Test(dataProvider = "applyMultiApplyCouponTransactionOffsetMinusOneDateData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponTransactionOffsetMinusOneDateTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload, HashMap<String, String> applyPayloadForSecondTransaction) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(couponErrorMessage, null);
        String userId = applyPayload.get(Integer.toString(9));
        String orderIdTransactionOne = "22345";
        RngHelper.couponUsageCount(couponCode, userId, orderIdTransactionOne);
//        String usageCount = RngHelper.getMultiApplyCouponUsageCount(couponId);
//        Assert.assertEquals(usageCount, "1");
        Assert.assertEquals("1", couponRedisHelper.getCouponUsageCountRedis(couponCode), "uses count is not matching with redis data");
        applyPayloadForSecondTransaction.put("0", couponCode);
        Processor applySecondTransactionResponse = rngHelper.couponApply(applyPayloadForSecondTransaction);
        String dataRespSecondTransaction = String.valueOf(applySecondTransactionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeRespSecondTransaction = applySecondTransactionResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String validSecondTransaction = String.valueOf(applySecondTransactionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessageSecondTransaction = applySecondTransactionResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataRespSecondTransaction, null);
        Assert.assertEquals(statusCodeRespSecondTransaction, 1);
        Assert.assertEquals(validSecondTransaction, "true");
        Assert.assertEquals(couponErrorMessageSecondTransaction, null);
        String orderIdTransactionTwo = "22222";
        RngHelper.couponUsageCount(couponCode, userId, orderIdTransactionTwo);
//        String usageCountSecondTime = RngHelper.getMultiApplyCouponUsageCount(couponId);
//        Assert.assertEquals(usageCountSecondTime, "2");
        Assert.assertEquals("2", couponRedisHelper.getCouponUsageCountRedis(couponCode), "uses count is not matching with redis data");

    }

    @Test(dataProvider = "applyMultiApplyCouponCheckCouponTypeOfEachTransactionData", dataProviderClass = MultiApplyDP.class)
    public void applyMultiApplyCouponCheckCouponTypeOfEachTransactionTest(HashMap<String, String> createPayload, String type, HashMap<String, String> applyPayload, HashMap<String, String> applyPayloadForSecondTransaction) {
        Processor createResponse = RngHelper.createMultiApplyCoupon(createPayload, type);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        applyPayload.put("0", couponCode);
        Processor applyResponse = rngHelper.couponApply(applyPayload);
        String dataResp = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeResp = applyResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String valid = String.valueOf(applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessage = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String couponTypeInTransactionOne = applyResponse.ResponseValidator.GetNodeValue("$.data.coupon_type");
        String couponType = createPayload.get(Integer.toString(33));
        Assert.assertNotEquals(dataResp, null);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(valid, "true");
        Assert.assertEquals(couponErrorMessage, null);
        Assert.assertEquals(couponTypeInTransactionOne, couponType);
        String userId = applyPayload.get(Integer.toString(9));
        String orderIdTransactionOne = "22345";
        RngHelper.couponUsageCount(couponCode, userId, orderIdTransactionOne);
//        String usageCount = RngHelper.getMultiApplyCouponUsageCount(couponId);
//        Assert.assertEquals(usageCount, "1");
        Assert.assertEquals("1", couponRedisHelper.getCouponUsageCountRedis(couponCode), "uses count is not matching with redis data");
        applyPayloadForSecondTransaction.put("0", couponCode);
        Processor applySecondTransactionResponse = rngHelper.couponApply(applyPayloadForSecondTransaction);
        String dataRespSecondTransaction = String.valueOf(applySecondTransactionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCodeRespSecondTransaction = applySecondTransactionResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String validSecondTransaction = String.valueOf(applySecondTransactionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.valid"));
        String couponErrorMessageSecondTransaction = applySecondTransactionResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(dataRespSecondTransaction, null);
        Assert.assertEquals(statusCodeRespSecondTransaction, 1);
        Assert.assertEquals(validSecondTransaction, "true");
        Assert.assertEquals(couponErrorMessageSecondTransaction, null);
        String couponTypeInTransactionTwo = applySecondTransactionResponse.ResponseValidator.GetNodeValue("$.data.coupon_type");
        String couponTypeTwo = createPayload.get(Integer.toString(51));
        Assert.assertEquals(couponTypeInTransactionTwo, couponTypeTwo);
        String orderIdTransactionTwo = "22222";
        RngHelper.couponUsageCount(couponCode, userId, orderIdTransactionTwo);
//        String usageCountSecondTime = RngHelper.getMultiApplyCouponUsageCount(couponId);
//        Assert.assertEquals(usageCountSecondTime, "2");
        Assert.assertEquals("2", couponRedisHelper.getCouponUsageCountRedis(couponCode), "uses count is not matching with redis data");

    }


    // @Test(dataProvider = "bulkCouponCode_prefixData", dataProviderClass =
    // DP.class)
    // public void bulkCouponCode_prefixData(String code_prefix, String
    // code_suffix_length, String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // String processId =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.message"));
    // String msg = "codePrefix is Invalid";
    // Assert.assertEquals(processId, msg);
    // }
    //
    // @Test(dataProvider = "bulkCouponCode_repeatPrefixData", dataProviderClass =
    // DP.class)
    // public void bulkCouponCode_repeatPrefixData(String code_prefix, String
    // code_suffix_length, String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    // Processor cc1Processor = RngHelper.bulkCoupon(code_prefix,
    // code_suffix_length, total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // String processId =
    // String.valueOf(cc1Processor.ResponseValidator.GetNodeValue("$.data.message"));
    // String msg = "Bulk Coupon Creation Running for Prefix REP. Try After
    // sometime.";
    // Assert.assertEquals(processId, msg);
    // }
    //
    // @Test(dataProvider = "bulkCouponCode_suffixData", dataProviderClass =
    // DP.class)
    // public void bulkCouponCode_suffixData(String code_prefix, String
    // code_suffix_length, String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // String processMsg =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.message"));
    // String pMsg = "Bulk Upload Started Successfully";
    // String nMsg = "codeSuffixLength is Invalid";
    // if (processMsg.equalsIgnoreCase(pMsg)) {
    // Assert.assertEquals(processMsg, pMsg, "Bulk Upload Not Started");
    // }
    //
    // else {
    // Assert.assertEquals(processMsg, nMsg, "Not validating codeSuffixLength");
    // }
    // }
    //
    // @Test(dataProvider = "bulkCouponCode_TotalCouponData", dataProviderClass =
    // DP.class)
    // public void bulkCouponCode_TotalCouponData(String code_prefix, String
    // code_suffix_length, String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // String processMsg =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValue("$.data.message"));
    // String pMsg = "Bulk Upload Started Successfully";
    // String nMsg = "totalCoupons is Invalid";
    // if (processMsg.equalsIgnoreCase(pMsg)) {
    // Assert.assertEquals(processMsg, pMsg, "Bulk Upload Not Started");
    // }
    //
    // else {
    // Assert.assertEquals(processMsg, nMsg, "Accepting zero for total coupon");
    // }
    // }
    //
    // @Test(dataProvider = "bulkCouponCode_timeSlotData", dataProviderClass =
    // DP.class)
    // public void bulkCouponCode_timeSlotData(String code_prefix, String
    // code_suffix_length, String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // String processId =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.code"));
    // // String pMsg="Bulk Upload Started Successfully";
    // Processor allCoupons = RngHelper.getCouponBulkAll();
    // // String nMsg="totalCoupons is Invalid";
    // // if(processMsg.equalsIgnoreCase(pMsg))
    // // Assert.assertEquals(processMsg,pMsg,"Bulk Upload Not Started-TimeSlot
    // TCs");
    // System.out.println(processId);
    // String timeSlot = JsonPath
    // .read(allCoupons.ResponseValidator.GetBodyAsText(),
    // "$.data[?(@.operationId==" + processId + ")]..timeSlot")
    // .toString().replace("[", "").replace("]", "");
    // System.out.println("String is" + timeSlot + "asdasd" + timeSlot.isEmpty());
    // // String time = timeSlot.mkString();
    // if (timeSlot.equalsIgnoreCase("null")) {
    // Assert.fail();
    // }
    //
    // }
    //
    // @Test(dataProvider = "getBulkCoupon", dataProviderClass = DP.class)
    // public void getBulkCoupon(String code_prefix, String code_suffix_length,
    // String total_coupons, String coupon_type,
    // String name, String description, String is_private, String valid_from, String
    // valid_till,
    // String total_available, String totalPerUser, String min_amount_cart, String
    // customer_restriction,
    // String city_restriction, String area_restriction, String
    // restaurant_restriction,
    // String category_restriction, String item_restriction, String
    // discount_percentage, String discount_amount,
    // String free_shipping, String free_gifts, String first_order_restriction,
    // String preferred_payment_method,
    // String user_client, String slot_restriction, String createdOn, String image,
    // String coupon_bucket,
    // String min_quantity_cart, String cuisine_restriction, String discount_item,
    // String usage_count,
    // String expiry_offset, String applicable_with_swiggy_money, String
    // is_bank_discount, String refund_source,
    // String pg_message, String supported_ios_version, String updated_by, String
    // title, String tnc,
    // String logo_id, String openTime, String closeTime, String day, String type)
    // throws IOException, ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // Processor allCoupons = RngHelper.getCouponBulkAll();
    // String data =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
    // System.out.println(data.equalsIgnoreCase("null"));
    // Assert.assertNotSame(data, "null");
    // }
    //
    // @Test(dataProvider = "getBulkCouponAll", dataProviderClass = DP.class)
    // public void getBulkCouponAll(String code_prefix, String code_suffix_length,
    // String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // String opId =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.code"));
    // Processor allCoupons = RngHelper.getCouponBulkAll();
    // System.out.println(opId);
    // List ls = JsonPath.read(allCoupons.ResponseValidator.GetBodyAsText(),
    // "$.data..operationId");
    // String recentCoupon = ls.get(0).toString();
    // System.out.println(ls);
    // // Assert.assertEquals(true, ls.contains(opId));
    // // Assert.assertEquals(true, ls.contains(opId));
    // System.out.println(recentCoupon);
    // Assert.assertEquals(recentCoupon, opId);
    // }
    //
    // @Test(dataProvider = "checkTotalCouponsInBulk", dataProviderClass = DP.class)
    // public void checkTotalCouponsInBulk(String code_prefix, String
    // code_suffix_length, String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // String opId =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.code"));
    // Processor allCoupons = RngHelper.getCouponBulkAll();
    // String total_cou = JsonPath
    // .read(allCoupons.ResponseValidator.GetBodyAsText(),
    // "$.data..[?(@.operationId==" + opId + ")]..total_coupons")
    // .toString().replace("[", "").replace("]", "");
    // System.out.println(total_coupons + "total_coup in get API" + total_cou);
    // Assert.assertEquals(total_coupons, total_cou);
    //
    // }
    //
    // @Test(dataProvider = "checkBulkCouponStatus", dataProviderClass = DP.class)
    // public void checkBulkCouponStatus(String code_prefix, String
    // code_suffix_length, String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // String opId =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.code"));
    // Processor allCoupons = RngHelper.getCouponBulkAll();
    // String total_cou = String.valueOf(allCoupons.ResponseValidator
    // .GetNodeValueAsStringFromJsonArray("$.data.[?(@.operationId==" + opId +
    // ")]..operationStatus"));
    // Assert.assertEquals("SUCCESS", total_cou);
    // }
    //
    // @Test(dataProvider = "checkBulkCouponCodePrefix", dataProviderClass =
    // DP.class)
    // public void checkBulkCouponCodePrefix(String code_prefix, String
    // code_suffix_length, String total_coupons,
    // String coupon_type, String name, String description, String is_private,
    // String valid_from,
    // String valid_till, String total_available, String totalPerUser, String
    // min_amount_cart,
    // String customer_restriction, String city_restriction, String
    // area_restriction,
    // String restaurant_restriction, String category_restriction, String
    // item_restriction,
    // String discount_percentage, String discount_amount, String free_shipping,
    // String free_gifts,
    // String first_order_restriction, String preferred_payment_method, String
    // user_client,
    // String slot_restriction, String createdOn, String image, String
    // coupon_bucket, String min_quantity_cart,
    // String cuisine_restriction, String discount_item, String usage_count, String
    // expiry_offset,
    // String applicable_with_swiggy_money, String is_bank_discount, String
    // refund_source, String pg_message,
    // String supported_ios_version, String updated_by, String title, String tnc,
    // String logo_id, String openTime,
    // String closeTime, String day, String type) throws IOException,
    // ProcessingException {
    //
    // Processor ccProcessor = RngHelper.bulkCoupon(code_prefix, code_suffix_length,
    // total_coupons, coupon_type, name,
    // description, is_private, valid_from, valid_till, total_available,
    // totalPerUser, min_amount_cart,
    // customer_restriction, city_restriction, area_restriction,
    // restaurant_restriction, category_restriction,
    // item_restriction, discount_percentage, discount_amount, free_shipping,
    // free_gifts,
    // first_order_restriction, preferred_payment_method, user_client,
    // slot_restriction, createdOn, image,
    // coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item,
    // usage_count, expiry_offset,
    // applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    // supported_ios_version,
    // updated_by, title, tnc, logo_id, openTime, closeTime, day, type);
    //
    // System.out.println(code_prefix + "in create API");
    // String opId =
    // String.valueOf(ccProcessor.ResponseValidator.GetNodeValueAsInt("$.data.code"));
    // System.out.println(opId + "OperationID is");
    // System.out.println(code_prefix + "in create API");
    // Processor allCoupons = RngHelper.getCouponBulkAll();
    // String codePrefix = String.valueOf(allCoupons.ResponseValidator
    // .GetNodeValueAsStringFromJsonArray("$.data.[?(@.operationId==" + opId +
    // ")]..code_prefix"));
    // code_prefix = "[" + "\"" + code_prefix + "\"" + "]";
    // // System.out.println(codePrefix+ "in Get API");
    // Assert.assertEquals(codePrefix, code_prefix);
    // }

    // Super related changes for coupon   ========  coupon-user-type-id = 1 (super users) =======

    @Test(dataProvider = "createPublicDiscountTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void createPublicDiscountTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
    }

    @Test(dataProvider = "createUserMapDiscountTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void createUserMapDiscountTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }

    @Test(dataProvider = "createPublicFreeDelTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void createPublicFreeDelTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
    }

    @Test(dataProvider = "createUserMapFreeDelTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void createUserMapFreeDelTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }


//    =========================================  coupon-user-type-id = 2 (non-super users) ============


    @Test(dataProvider = "createPublicDiscountTypeCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Non-Super (2)")
    public void createPublicDiscountTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type_Non-Super");
    }

    @Test(dataProvider = "createUserMapDiscountTypeCouponForUserTypeNonSuperTest", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Non-Super (2)")
    public void createUserMapDiscountTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type_Non-Super");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }

    @Test(dataProvider = "createPublicFreeDelTypeCouponForUserTypeNonSuperTest", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Non-Super (2)")
    public void createPublicFreeDelTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type_Non-Super");
        Assert.assertEquals(freeShipping, true);
    }

    @Test(dataProvider = "createUserMapFreeDelTypeCouponForUserTypeNonSuperTest", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Non-Super (2)")
    public void createUserMapFreeDelTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type_NON-Super");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }


//    =========================================  coupon-user-type-id = 0 (ALL users) ============


    @Test(dataProvider = "createPublicDiscountTypeCouponForUserTypeALLData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as ALL (0)")
    public void createPublicDiscountTypeCouponForUserTypeALLTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-ALL");
    }

    @Test(dataProvider = "createUserMapDiscountTypeCouponForUserTypeALLData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as ALL (0)")
    public void createUserMapDiscountTypeCouponForUserTypeALLTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-ALL");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }

    @Test(dataProvider = "createPublicFreeDelTypeCouponForUserTypeALLData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as ALL (0)")
    public void createPublicFreeDelTypeCouponForUserTypeALLTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-ALL");
        Assert.assertEquals(freeShipping, true);
    }

    @Test(dataProvider = "createUserMapFreeDelTypeCouponForUserTypeALLData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as ALL (0)")
    public void createUserMapFreeDelTypeCouponForUserTypeALLTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-ALL");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }

    //    =========================================  Invalid coupon-user-type-id = 3  ============


    @Test(dataProvider = "createPublicDiscountTypeCouponForInvalidUserTypeIdData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for Invalid coupon_user_type_id 3")
    public void createPublicDiscountTypeCouponForInvalidUserTypeIdTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Fialure because --> Coupon is creating with the invalid coupon_user_type-id 3");
        Assert.assertEquals(statusMessage.contains(CouponConstants.invalidCouponUserTyeId), true);
    }

    @Test(dataProvider = "createUserMapDiscountTypeCouponForInvalidUserTypeIdData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for Invalid coupon_user_type_id 3")
    public void createUserMapDiscountTypeCouponForInvalidUserTypeIdTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Fialure because --> Coupon is creating with the invalid coupon_user_type-id 3");
        Assert.assertEquals(statusMessage.contains(CouponConstants.invalidCouponUserTyeId), true);

    }

    @Test(dataProvider = "createPublicFreeDelTypeCouponForInvalidUserTypeIdData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for Invalid coupon_user_type_id 3)")
    public void createPublicFreeDelTypeCouponForInvalidUserTypeIdTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Fialure because --> Coupon is creating with the invalid coupon_user_type-id 3");
        Assert.assertEquals(statusMessage.contains(CouponConstants.invalidCouponUserTyeId), true);
    }

    @Test(dataProvider = "createUserMapFreeDelTypeCouponForInvalidUserTypeIdData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for Invalid coupon_user_type_id 3")
    public void createUserMapFreeDelTypeCouponForInvalidUserTypeIdTest(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        Assert.assertEquals(statusCode, 0, "Fialure because --> Coupon is creating with the invalid coupon_user_type-id 3");
        Assert.assertEquals(statusMessage.contains(CouponConstants.invalidCouponUserTyeId), true);
    }


// ===========================================Get by user id-city id  for Super user  ========================


    @Test(dataProvider = "getByUserIdCityIdPublicDiscountTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdPublicDiscountTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        activeUserSubscription = "" + userId;
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(activeUserSubscription, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getByUserIdCityIdUserMapDiscountTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapDiscountTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

    }

    @Test(dataProvider = "GetByUserIdCityIdPublicFreeDelTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdCityIdPublicFreeDelTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        activeUserSubscription = "" + userId;
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getByUserIdCityUserMapFreeDelTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdCityUserMapFreeDelTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

// ===========================================Get by user id-city id  Super type coupon for non-Super user  ========================


    @Test(dataProvider = "getByUserIdCityIdPublicDiscountTypeSuperCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdPublicDiscountTypeSuperCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String coupon_code = String.valueOf(updateResponse.ResponseValidator.GetNodeValue("$.data.code"));
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponData = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==\"+couponId+\")].id");
        String statusMessage = getByUserIdCityId.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponData, "[]");
        Assert.assertEquals(statusMessage, null);
    }


    @Test(dataProvider = "getByUserIdCityIdUserMapDiscountTypeSuperCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapDiscountTypeSuperCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponData = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==\"+couponId+\")].id");
        String statusMessage = getByUserIdCityId.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponData, "[]");
        Assert.assertEquals(statusMessage, null);

    }

    @Test(dataProvider = "GetByUserIdCityIdPublicFreeDelTypeSuperCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdCityIdPublicFreeDelTypeSuperCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponData = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==\"+couponId+\")].id");
        String statusMessage = getByUserIdCityId.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponData, "[]");
        Assert.assertEquals(statusMessage, null);
    }

    @Test(dataProvider = "getByUserIdCityUserMapFreeDelTypeSuperCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdCityUserMapFreeDelTypeSuperCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponData = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==\"+couponId+\")].id");
        String statusMessage = getByUserIdCityId.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponData, "[]");
        Assert.assertEquals(statusMessage, null);
    }

    //===========================================Get by user id-city id  non-Super type coupon for non-Super user  ========================


    @Test(dataProvider = "getByUserIdCityIdPublicDiscountTypeNonSuperCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdPublicDiscountTypeNonSuperCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

    }

    @Test(dataProvider = "GetByUserIdCityIdPublicFreeDelTypeNonSuperCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdCityIdPublicFreeDelTypeNonSuperCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getByUserIdCityIdUserMapFreeDelTypeNonSuperCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapFreeDelTypeNonSuperCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    // ========== Get by user id city id , when no mapping for user ==============
    @Test(dataProvider = "getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeNonSuperWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeNonSuperWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

    }

    @Test(dataProvider = "getByUserIdCityIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

    }

    @Test(dataProvider = "getByUserIdCityUserMapFreeDelTypeSuperCouponForUserTypeSuperDataWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdCityUserMapFreeDelTypeSuperCouponForUserTypeSuperDataWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getByUserIdCityUserMapFreeDelTypeNonSuperCouponForUserTypeNonSuperDataWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdCityUserMapFreeDelTypeNonSuperCouponForUserTypeNonSuperDataWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getByUserIdCityIdUserMapDiscountTypeAllCouponForUserTypeALLWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapDiscountTypeAllCouponForUserTypeALLWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, userId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        // Super user id
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
        //non super id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUserCity(userIdTwo, cityId);
        int statusCodeOfGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetResp, 1);
        Assert.assertEquals(couponIdInGetResp, "[]");


    }

    @Test(dataProvider = "getByUserIdCityUserMapFreeDelTypeALLCouponForUserTypeALLDataWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdCityUserMapFreeDelTypeALLCouponForUserTypeALLDataWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        RngHelper.couponUserMap(code, userId, couponId);
        // super user id
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

        //non-super user id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUserCity(userIdTwo, cityId);
        int statusCodeOfGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetResp, 1);
        Assert.assertEquals(couponIdInGetResp, "[]");
    }


    //===========================================Get by user id-city id  non-Super type coupon for Super user  ========================


    @Test(dataProvider = "getByUserIdCityIdPublicDiscountTypeNonSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdPublicDiscountTypeNonSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }


    @Test(dataProvider = "getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapDiscountTypeNonSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

    }

    @Test(dataProvider = "GetByUserIdCityIdPublicFreeDelTypeNonSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdCityIdPublicFreeDelTypeNonSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getByUserIdCityIdUserMapFreeDelTypeNonSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapFreeDelTypeNonSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    //===========================================Get by user id-city id  ALL user type coupon  ========================

    @Test(dataProvider = "getByUserIdCityIdPublicDiscountALLTypeCouponForBothUsersSuperNnonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdPublicDiscountALLTypeCouponFoBothUsersSuperNnonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //super user id
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        // non Super id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUserCity(userIdTwo, cityId);
        int statusCodeOfGetTwo = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetTwo = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetTwo, 1);
        Assert.assertEquals(couponIdInGetTwo, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getByUserIdCityIdUserMapDiscountALLTypeCouponForBothUserSuperNnonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapDiscountALLTypeCouponForBothUserSuperNnonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        Processor mappingProcessorResp = RngHelper.couponUserMap(code, userIdTwo, couponId);
        String Mapping_IdResp = String.valueOf(mappingProcessorResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingDataResp = String.valueOf(mappingProcessorResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        // non Super id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUserCity(userIdTwo, cityId);
        int statusCodeOfGetTwo = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetTwo = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetTwo, 1);
        Assert.assertEquals(couponIdInGetTwo, "[\"" + couponId + "\"]");

    }

    @Test(dataProvider = "GetByUserIdCityIdPublicFreeDelALLTypeCouponForBothUserSuperNnonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdCityIdPublicFreeDelALLTypeCouponForBothUserSuperNnonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        // non Super id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUserCity(userIdTwo, cityId);
        int statusCodeOfGetTwo = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetTwo = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetTwo, 1);
        Assert.assertEquals(couponIdInGetTwo, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getByUserIdCityIdUserMapFreeDelALLTypeCouponForBothUserSuperNnonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdCityIdUserMapFreeDelALLTypeCouponForBothUserSuperNnonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor mappingProcessorResp = RngHelper.couponUserMap(code, userIdTwo, couponId);
        String Mapping_IdResp = String.valueOf(mappingProcessorResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingDataResp = String.valueOf(mappingProcessorResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        // non Super id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUserCity(userIdTwo, cityId);
        int statusCodeOfGetTwo = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetTwo = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetTwo, 1);
        Assert.assertEquals(couponIdInGetTwo, "[\"" + couponId + "\"]");

    }

    // ====== Get by user id Super type coupon for Super user   ========================================

    @Test(dataProvider = "getByUserIdPublicDiscountTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdPublicDiscountTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //super user id
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");


    }

    @Test(dataProvider = "GetByUserIdPublicFreeDelTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdPublicFreeDelTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    // ====== Get by user id Super type coupon for Non-Super user   ========================================

    @Test(dataProvider = "getByUserIdPublicDiscountTypeSuperCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdPublicDiscountTypeSuperCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //super user id
        Processor getByUserIdCityId = RngHelper.getCouponUser(userIdTwo);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }


    @Test(dataProvider = "getByUserIdUserMapDiscountTypeSuperCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapDiscountTypeSuperCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");


    }

    @Test(dataProvider = "GetByUserIdPublicFreeDelTypeSuperCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdPublicFreeDelTypeSuperCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);
        superHelper.cancelSubscriptionOfUser(userId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getByUserIdUserMapFreeDelTypeSuperCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapFreeDelTypeSuperCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }


    // ====== Get by user id Non-Super type coupon for Non-Super user   ========================================

    @Test(dataProvider = "getByUserIdPublicDiscountTypeNonSuperCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdPublicDiscountTypeNonSuperCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //super user id
        Processor getByUserIdCityId = RngHelper.getCouponUser(userIdTwo);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getByUserIdUserMapDiscountTypeNonSuperCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapDiscountTypeNonSuperCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");


    }

    @Test(dataProvider = "GetByUserIdPublicFreeDelTypeNonSuperCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdPublicFreeDelTypeNonSuperCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);

        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getByUserIdUserMapFreeDelTypeNonSuperCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapFreeDelTypeNonSuperCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    // ====== Get by user id Non-Super type coupon for Super user   ========================================

    @Test(dataProvider = "getByUserIdPublicDiscountTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdPublicDiscountTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //super user id
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }


    @Test(dataProvider = "getByUserIdUserMapDiscountTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapDiscountTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");


    }

    @Test(dataProvider = "GetByUserIdPublicFreeDelTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdPublicFreeDelTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getByUserIdUserMapFreeDelTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapFreeDelTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    // ====== Get by user id ALL type coupon for both - Super & Non-Super users   ========================================

    @Test(dataProvider = "getByUserIdPublicDiscountALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdPublicDiscountALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //super user id
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        //non-super user id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUser(userIdTwo);
        int statusCodeOfGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetResp, 1);
        Assert.assertEquals(couponIdInGetResp, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        Processor mappingProcessorResp = RngHelper.couponUserMap(code, userIdTwo, couponId);
        String Mapping_IdResp = String.valueOf(mappingProcessorResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingDataResp = String.valueOf(mappingProcessorResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        //non-super user id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUser(userIdTwo);
        int statusCodeOfGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetResp, 1);
        Assert.assertEquals(couponIdInGetResp, "[\"" + couponId + "\"]");


    }

    @Test(dataProvider = "GetByUserIdPublicFreeDelALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetByUserIdPublicFreeDelALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(freeShipping, true);

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");


        //non-super user id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUser(userId);
        int statusCodeOfGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetResp, 1);
        Assert.assertEquals(couponIdInGetResp, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        Processor mappingProcessorResp = RngHelper.couponUserMap(code, userIdTwo, couponId);
        String Mapping_IdResp = String.valueOf(mappingProcessorResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingDataResp = String.valueOf(mappingProcessorResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        //non-super user id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUser(userIdTwo);
        int statusCodeOfGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetResp, 1);
        Assert.assertEquals(couponIdInGetResp, "[\"" + couponId + "\"]");
    }

    // ====== Get by user id  when no user mapping   ========================================

    @Test(dataProvider = "getByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");


    }

    @Test(dataProvider = "getByUserIdUserMapFreeDelTypeNonSuperCouponFoNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapFreeDelTypeNonSuperCouponFoNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        //Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
//		String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
//		String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
//		Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

    }

    @Test(dataProvider = "getByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperDataWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperDataWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getByUserIdUserMapDiscountTypeAllCouponForUserTypeALLWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapDiscountTypeAllCouponForUserTypeALLWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, userId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        // Super user id
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
        //non super id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUser(userIdTwo);
        int statusCodeOfGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetResp, 1);
        Assert.assertEquals(couponIdInGetResp, "[]");


    }

    @Test(dataProvider = "getByUserIdUserMapFreeDelTypeALLCouponForUserTypeALLDataWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getByUserIdUserMapFreeDelTypeALLCouponForUserTypeALLDataWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        RngHelper.couponUserMap(code, userId, couponId);
        // super user id
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

        //non-super user id
        Processor getByUserIdCityIdResp = RngHelper.getCouponUser(userIdTwo);
        int statusCodeOfGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGetResp = getByUserIdCityIdResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGetResp, 1);
        Assert.assertEquals(couponIdInGetResp, "[]");

    }

// ============== apply Super user coupon  as Super user ======================

    @Test(dataProvider = "applyPublicDiscountTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyPublicDiscountTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String code = payload.getCode();
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, userId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");
    }

    @Test(dataProvider = "applyUserMapDiscountTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyUserMapDiscountTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");
    }

    @Test(dataProvider = "applyPublicFreeDelTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyPublicFreeDelTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);
    }


    @Test(dataProvider = "applyUserMapFreeDelTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyUserMapFreeDelTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);
    }

    // ============== apply non-Super user coupon  as Super user ======================

    @Test(dataProvider = "applyPublicDiscountTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyPublicDiscountTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String code = payload.getCode();
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(couponError, null);

    }

    @Test(dataProvider = "applyUserMapDiscountTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyUserMapDiscountTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(couponError, null);

    }

    @Test(dataProvider = "applyPublicFreeDelTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyPublicFreeDelTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");

        Assert.assertNotEquals(couponError, null);

    }


    @Test(dataProvider = "applyUserMapFreeDelTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyUserMapFreeDelTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");

        Assert.assertNotEquals(couponError, null);

    }

    // ============== apply non-Super user coupon  as non-Super user ======================

    @Test(dataProvider = "applyPublicDiscountTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyPublicDiscountTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String code = payload.getCode();
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");

    }

    @Test(dataProvider = "applyUserMapDiscountTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyUserMapDiscountTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");

    }

    @Test(dataProvider = "applyPublicFreeDelTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyPublicFreeDelTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);

    }


    @Test(dataProvider = "applyUserMapFreeDelTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyUserMapFreeDelTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);

    }
// ============== apply Super user coupon  as non-Super user ======================


    @Test(dataProvider = "applyPublicDiscountTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyPublicDiscountTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String code = payload.getCode();
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER");

    }

    @Test(dataProvider = "applyUserMapDiscountTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyUserMapDiscountTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER");

    }

    @Test(dataProvider = "applyPublicFreeDelTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyPublicFreeDelTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER");

    }

    @Test(dataProvider = "applyUserMapFreeDelTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyUserMapFreeDelTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER");

    }
// ============== apply ALL type coupon  as both- Super & Non-Super user ======================

    @Test(dataProvider = "applyPublicDiscountTypeALLCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyPublicDiscountTypeALLCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String code = payload.getCode();
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");

        String applyCouponPayload = new ApplyCouponPOJO(code, userIdTwo, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String codeInApply = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmountResp = applyCouponResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApply, code);
        Assert.assertEquals(couponErrorResp, null);
        Assert.assertEquals(discountAmountResp, "20.0");

    }

    @Test(dataProvider = "applyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, userId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");
        Processor mappingResp = RngHelper.couponUserMap(code, userIdTwo, couponId);
        String mappingDataResp = String.valueOf(mappingResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");
        String applyCouponPayload = new ApplyCouponPOJO(code, userIdTwo, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String codeInApply = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmountResp = applyCouponResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApply, code);
        Assert.assertEquals(couponErrorResp, null);
        Assert.assertEquals(discountAmountResp, "20.0");

    }

    @Test(dataProvider = "applyPublicFreeDelALLTypeCouponForSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyPublicFreeDelALLTypeCouponForSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean freeShipping = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String codeInApply = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmountResp = applyCouponResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingInApply = applyCouponResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApply, code);
        Assert.assertEquals(couponErrorResp, null);
        Assert.assertEquals(discountAmountResp, "0.0");
        Assert.assertEquals(freeShippingInApply, true);

    }


    @Test(dataProvider = "applyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);


        Processor mapping = RngHelper.couponUserMap(code, userId, couponId);
        String mappingDataResp = String.valueOf(mapping.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String codeInApply = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmountResp = applyCouponResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShipping = applyCouponResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApply, code);
        Assert.assertEquals(couponErrorResp, null);
        Assert.assertEquals(discountAmountResp, "0.0");
        Assert.assertEquals(freeShipping, true);


    }
//============================== Apply ALL type coupon when no user mapping  =======================================

    @Test(dataProvider = "applyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_TO_USER");
        String applyCouponPayload = new ApplyCouponPOJO(code, userIdTwo, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponErrorResp, "E_COUPON_NOT_APPLICABLE_TO_USER");

    }

    @Test(dataProvider = "applyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_TO_USER");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponErrorResp, "E_COUPON_NOT_APPLICABLE_TO_USER");

    }

    //============================== Apply Super type when no user mapping  =======================================

    @Test(dataProvider = "applyUserMapDiscountSuperTypeCouponForSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyUserMapDiscountSuperTypeCouponForSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_TO_USER");


    }

    @Test(dataProvider = "applyUserMapFreeDelSuperTypeCouponForSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyUserMapFreeDelSuperTypeCouponForSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponErrorResp, "E_COUPON_NOT_APPLICABLE_TO_USER");

    }

    //============================== Apply Super type when no user mapping  =======================================

    @Test(dataProvider = "applyUserMapDiscountNonSuperTypeCouponForNonSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyUserMapDiscountNonSuperTypeCouponForNonSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_TO_USER");


    }

    @Test(dataProvider = "applyUserMapFreeDelNonSuperTypeCouponForNonSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyUserMapFreeDelNonSuperTypeCouponForNonSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        String code = payload.getCode();
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        boolean isPrivate = updateResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponId = updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(isPrivate, true);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponErrorResp, "E_COUPON_NOT_APPLICABLE_TO_USER");

    }
// =========== Create multiApply coupon for User type super =================

    @Test(dataProvider = "createMultiApplyPublicDiscountTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void createMultiApplyPublicDiscountTypeCouponForUserTypeSuperDataTest(HashMap<String, Object> data) {

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer id = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertNotEquals(id, null);
    }

    @Test(dataProvider = "createMultiAPplyUserMapDiscountTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void createMultiAPplyUserMapDiscountTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        String code = payload.getCode();
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");

        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }

    @Test(dataProvider = "createMultiApplyPublicFreeDelTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void createMultiApplyPublicFreeDelTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer id = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertNotEquals(id, null);
        Assert.assertEquals(freeShipping, true);
    }

    @Test(dataProvider = "createMultiApplyUserMapFreeDelTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void createMultiApplyUserMapFreeDelTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer id = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertNotEquals(id, null);
        Assert.assertEquals(freeShipping, true);
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }

    // =========== Create multiApply coupon for User type non super =================

    @Test(dataProvider = "createMultiApplyPublicDiscountTypeCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void createMultiApplyPublicDiscountTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer id = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        int userType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");

        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertNotEquals(id, null);
        Assert.assertEquals(userType, 2);
    }

    @Test(dataProvider = "createMultiAPplyUserMapDiscountTypeCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void createMultiAPplyUserMapDiscountTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        String code = payload.getCode();
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        int userType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        Assert.assertEquals(userType, 2);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }

    @Test(dataProvider = "createMultiApplyPublicFreeDelTypeCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void createMultiApplyPublicFreeDelTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer id = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        int userType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        Assert.assertEquals(userType, 2);
        Assert.assertNotEquals(id, null);
        Assert.assertEquals(freeShipping, true);
    }

    @Test(dataProvider = "createMultiApplyUserMapFreeDelTypeCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void createMultiApplyUserMapFreeDelTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer id = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        int userType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        Assert.assertEquals(userType, 2);
        Assert.assertNotEquals(id, null);
        Assert.assertEquals(freeShipping, true);
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }

    // =========== Create multiApply coupon for User type  ALL =================

    @Test(dataProvider = "createMultiApplyPublicDiscountTypeCouponForUserTypeALLData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void createMultiApplyPublicDiscountTypeCouponForUserTypeALLTest(HashMap<String, Object> data) {

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer id = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        int userType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");

        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertNotEquals(id, null);
        Assert.assertEquals(userType, 0);
    }

    @Test(dataProvider = "createMultiAPplyUserMapDiscountTypeCouponForUserTypeALLData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void createMultiAPplyUserMapDiscountTypeCouponForUserTypeALLTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        String code = payload.getCode();
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        int userType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        Assert.assertEquals(userType, 0);
        Assert.assertEquals(statusCode, 1);
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }

    @Test(dataProvider = "createMultiApplyPublicFreeDelTypeCouponForUserTypeALLData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void createMultiApplyPublicFreeDelTypeCouponForUserTypeALLTest(HashMap<String, Object> data) {

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer id = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        int userType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        Assert.assertEquals(userType, 0);
        Assert.assertNotEquals(id, null);
        Assert.assertEquals(freeShipping, true);
    }

    @Test(dataProvider = "createMultiApplyUserMapFreeDelTypeCouponForUserTypeALLData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void createMultiApplyUserMapFreeDelTypeCouponForUserTypeALLTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer id = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        int userType = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.coupon_user_type_id");
        Assert.assertEquals(userType, 0);
        Assert.assertNotEquals(id, null);
        Assert.assertEquals(freeShipping, true);
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, couponId);
        // code,  userId,  couponId
    }


    // =========== Create multiApply coupon for User type  other than 0,1 & 2 =================

    @Test(dataProvider = "createMultiApplyPublicDiscountTypeCouponForInvalidUserTypeData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void createMultiApplyPublicDiscountTypeCouponForInvalidUserTypeTest(HashMap<String, Object> data) {

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 0, "Fialure because --> Coupon not created for coupon_user_type-Super");

    }

    @Test(dataProvider = "createMultiAPplyUserMapDiscountTypeCouponForInvalidUserTypeData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void createMultiAPplyUserMapDiscountTypeCouponForInvalidUserTypeTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        String code = payload.getCode();
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 0, "Fialure because --> Coupon not created for coupon_user_type-Super");
    }

    @Test(dataProvider = "createMultiApplyPublicFreeDelTypeCouponForUserInvalidTypeData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void createMultiApplyPublicFreeDelTypeCouponForUserInvalidTypeTest(HashMap<String, Object> data) {

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 0, "Fialure because --> Coupon not created for coupon_user_type-Super");

    }

    @Test(dataProvider = "createMultiApplyUserMapFreeDelTypeCouponForInvalidUserTypeData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void createMultiApplyUserMapFreeDelTypeCouponForInvalidUserTypeTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 0, "Fialure because --> Coupon not created for coupon_user_type-Super");
    }

    // ===========================================Get by user id-city id  for Super user  ========================


    @Test(dataProvider = "getMultiApplyByUserIdCityIdPublicDiscountTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityIdPublicDiscountTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        activeUserSubscription = "" + userId;
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(activeUserSubscription, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getMultiApplyByUserIdCityIdUserMapDiscountTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityIdUserMapDiscountTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

    }

    @Test(dataProvider = "GetMultiApplyByUserIdCityIdPublicFreeDelTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetMultiApplyByUserIdCityIdPublicFreeDelTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        //Assert.assertEquals(createResponse,true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        activeUserSubscription = "" + userId;
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdCityUserMapFreeDelTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityUserMapFreeDelTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }
    // ===========================================Get by user id-city id by Non Super user id  ========================


    @Test(dataProvider = "getMultiApplyByUserIdCityIdPublicDiscountSuperTypeCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityIdPublicDiscountSuperTypeCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }


    @Test(dataProvider = "getMultiApplyByUserIdCityIdUserMapDiscountSuperTypeCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityIdUserMapDiscountSuperTypeCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

    }

    @Test(dataProvider = "GetMultiApplyByUserIdCityIdPublicFreeDelSuperTypeCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetMultiApplyByUserIdCityIdPublicFreeDelSuperTypeCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type_super");
        String code = payload.getCode();
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdCityUserMapFreeDelSuperTypeCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityUserMapFreeDelSuperTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type_super");
        String code = payload.getCode();
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    // ===========================================Get by user id-city id  non super type of coupon for Non Super user id  ========================


    @Test(dataProvider = "getMultiApplyByUserIdCityIdPublicDiscountNonSuperTypeCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityIdPublicDiscountNonSuperTypeCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getMultiApplyByUserIdCityIdUserMapDiscountNonSuperTypeCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityIdUserMapDiscountNonSuperTypeCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

    }

    @Test(dataProvider = "GetMultiApplyByUserIdCityIdPublicFreeDelNonSuperTypeCouponForUserNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetMultiApplyByUserIdCityIdPublicFreeDelNonSuperTypeCouponForUserNonSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type_super");
        String code = payload.getCode();
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdCityUserMapFreeDelNonSuperTypeCouponForUserTypeNonSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityUserMapFreeDelNonSuperTypeCouponForUserTypeNonSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type_super");
        String code = payload.getCode();
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    // ===========================================Get by user id-city id  non-super type coupon for Super user  ========================


    @Test(dataProvider = "getMultiApplyByUserIdCityIdPublicDiscountNonSuperTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityIdPublicDiscountNonSuperTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        activeUserSubscription = "" + userId;
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(activeUserSubscription, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }


    @Test(dataProvider = "getMultiApplyByUserIdCityIdUserMapDiscountNonSuperTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityIdUserMapDiscountNonSuperTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

    }

    @Test(dataProvider = "GetMultiApplyByUserIdCityIdPublicFreeDelNonSuperTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetMultiApplyByUserIdCityIdPublicFreeDelNonSuperTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        //Assert.assertEquals(createResponse,true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        activeUserSubscription = "" + userId;
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(userId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdCityUserMapFreeDelNonSuperTypeCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdCityUserMapFreeDelNonSuperTypeCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String code = payload.getCode();
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //activeUserSubscription = ""+mapUserId ;
        RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        // code,  userId,  couponId
        Processor getByUserIdCityId = RngHelper.getCouponUserCity(mapUserId, cityId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }
    // ====== Get by user id Super type coupon for Super user   ========================================

    @Test(dataProvider = "getMultiApplyByUserIdPublicDiscountTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdPublicDiscountTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //super user id
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");


    }

    @Test(dataProvider = "GetMultiApplyByUserIdPublicFreeDelTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetMultiApplyByUserIdPublicFreeDelTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    // ====== Get by user id Super type coupon for non Super user   ========================================

    @Test(dataProvider = "getMultiApplyByUserIdPublicDiscountTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdPublicDiscountTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }


    @Test(dataProvider = "getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");


    }

    @Test(dataProvider = "GetMultiApplyByUserIdPublicFreeDelTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetMultiApplyByUserIdPublicFreeDelTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    // ====== Get by user id Non Super type coupon for Super user   ========================================

    @Test(dataProvider = "getMultiApplyByUserIdPublicDiscountTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdPublicDiscountTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //super user id
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }


    @Test(dataProvider = "getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");


    }

    @Test(dataProvider = "GetMultiApplyByUserIdPublicFreeDelTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetMultiApplyByUserIdPublicFreeDelTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    // ====== Get by user id Non-Super type coupon for Non-Super user   ========================================

    @Test(dataProvider = "getMultiApplyByUserIdPublicDiscountTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdPublicDiscountTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperTypeData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperTypeTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        sandHelper.superCacheDel(CopyConstants.nonSuperUserId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");


    }

    @Test(dataProvider = "GetMultiApplyByUserIdPublicFreeDelTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetMultiApplyByUserIdPublicFreeDelTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        Assert.assertEquals(freeShipping, true);
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");
    }

    // ====== Get by user id ALL type coupon for both Super and Non-Super user   ========================================

    @Test(dataProvider = "getMultiApplyByUserIdPublicDiscountALLTypeCouponForBothUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdPublicDiscountALLTypeCouponForBothUserTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userIdTwo);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userIdTwo);
        Processor getByUserId = RngHelper.getCouponUser(userIdTwo);
        int statusCodeInResp = getByUserId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInResp = getByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeInResp, 1);
        Assert.assertEquals(couponIdInResp, "[\"" + couponId + "\"]");
    }


    @Test(dataProvider = "getMultiApplyByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperTypeData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperTypeTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, Integer.toString(couponId));
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        Processor mappingPro = RngHelper.couponUserMap(code, userIdTwo, Integer.toString(couponId));
        String mappingDataResp = String.valueOf(mappingPro.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userIdTwo);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userIdTwo);
        Processor getByUserId = RngHelper.getCouponUser(userIdTwo);
        int statusCodeInResp = getByUserId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInResp = getByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeInResp, 1);
        Assert.assertEquals(couponIdInResp, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "GetMultiApplyByUserIdPublicFreeDelALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void GetMultiApplyByUserIdPublicFreeDelALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type_super");
        Assert.assertEquals(freeShipping, true);
        Processor getByUserIdCityId = RngHelper.getCouponUser(userId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userIdTwo);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userIdTwo);
        Processor getByUserId = RngHelper.getCouponUser(userIdTwo);
        int statusCodeInResp = getByUserId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInResp = getByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeInResp, 1);
        Assert.assertEquals(couponIdInResp, "[\"" + couponId + "\"]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);

        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[\"" + couponId + "\"]");

        Processor mappingPro = RngHelper.couponUserMap(code, userIdTwo, couponId);
        String mappingDataResp = String.valueOf(mappingPro.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userIdTwo);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userIdTwo);
        Processor getByUserId = RngHelper.getCouponUser(userIdTwo);
        int statusCodeInResp = getByUserId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInResp = getByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeInResp, 1);
        Assert.assertEquals(couponIdInResp, "[\"" + couponId + "\"]");
    }

    // ====== Get by user id ALL type coupon for both Super and Non-Super user   ========================================

    @Test(dataProvider = "getMultiApplyByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperTypeWhenNoUserMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapDiscountALLTypeCouponForBothSuperNnonSuperTypeWhenNoUserMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userIdTwo);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userIdTwo);
        Processor getByUserId = RngHelper.getCouponUser(userIdTwo);
        int statusCodeInResp = getByUserId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInResp = getByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeInResp, 1);
        Assert.assertEquals(couponIdInResp, "[]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenNoUserMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenNoUserMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userIdTwo);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userIdTwo);
        Processor getByUserId = RngHelper.getCouponUser(userIdTwo);
        int statusCodeInResp = getByUserId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInResp = getByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeInResp, 1);
        Assert.assertEquals(couponIdInResp, "[]");
    }

    // ====== Get by user id Non-Super type coupon for Non-Super user when no user mapping   ========================================

    @Test(dataProvider = "getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperTypeWhenNoUserMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapDiscountTypeNonSuperCouponForNonSuperTypeWhenNoUserMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    @Test(dataProvider = "getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForNonSuperUserWhenNoUserMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapFreeDelTypeNonSuperCouponForNonSuperUserWhenNoUserMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);

        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }

    // ====== Get by user id Super type coupon for Super user when no user mapped========================================


    @Test(dataProvider = "getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenNoUserMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapDiscountTypeSuperCouponForUserTypeSuperWhenNoUserMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Integer couponId = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id");
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");


    }

    @Test(dataProvider = "getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperWhenNoUserMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void getMultiApplyByUserIdUserMapFreeDelTypeSuperCouponForUserTypeSuperWhenNoUserMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String cityId = Integer.toString(Utility.getRandom(1, 10));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        Processor getByUserIdCityId = RngHelper.getCouponUser(mapUserId);
        int statusCodeOfGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponIdInGet = getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + couponId + ")].id");
        Assert.assertEquals(statusCodeOfGet, 1);
        Assert.assertEquals(couponIdInGet, "[]");
    }


//    ======== APPLY ==========================

// ============== apply Super user coupon  as Super user ======================

    @Test(dataProvider = "applyMultiApplyPublicDiscountTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyPublicDiscountTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, userId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");
    }

    @Test(dataProvider = "applyMultiApplyUserMapDiscountTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapDiscountTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");
    }

    @Test(dataProvider = "applyMultiApplyPublicFreeDelTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyMultiApplyPublicFreeDelTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, false);
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);
    }


    @Test(dataProvider = "applyMultiApplyUserMapFreeDelTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapFreeDelTypeSuperCouponForUserTypeSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);
    }

    // ============== apply non-Super user coupon  as Super user ======================

    @Test(dataProvider = "applyMultiApplyPublicDiscountTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyPublicDiscountTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(couponError, null);

    }

    @Test(dataProvider = "applyMultiApplyUserMapDiscountTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapDiscountTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertNotEquals(couponError, null);

    }

    @Test(dataProvider = "applyMultiApplyPublicFreeDelTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyMultiApplyPublicFreeDelTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, false);
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(freeShipping, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");

        Assert.assertNotEquals(couponError, null);

    }


    @Test(dataProvider = "applyMultiApplyUserMapFreeDelTypeNonSuperCouponForUserSuperData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapFreeDelTypeNonSuperCouponForUserSuperTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");

        Assert.assertNotEquals(couponError, null);

    }

    // ============== apply non-Super user coupon  as non-Super user ======================

    @Test(dataProvider = "applyMultiAPplyPublicDiscountTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiAPplyPublicDiscountTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");

    }

    @Test(dataProvider = "applyMultiApplyUserMapDiscountTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapDiscountTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");

    }

    @Test(dataProvider = "applyMultiApplyPublicFreeDelTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyMultiApplyPublicFreeDelTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(freeShipping, true);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);
    }

    @Test(dataProvider = "applyMultiApplyUserMapFreeDelTypeNonSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapFreeDelTypeNonSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);

    }
// ============== apply Super user coupon  as non-Super user ======================


    @Test(dataProvider = "applyMultiApplyPublicDiscountTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyPublicDiscountTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER");

    }

    @Test(dataProvider = "applyMultiApplyUserMapDiscountTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapDiscountTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(mapUserId);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER");

    }

    @Test(dataProvider = "applyMultiApplyPublicFreeDelTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyMultiApplyPublicFreeDelTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, false);
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(freeShipping, true);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER");

    }

    @Test(dataProvider = "applyMultiApplyUserMapFreeDelTypeSuperCouponForNonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapFreeDelTypeSuperCouponForNonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER");

    }
// ============== apply ALL type coupon  as both- Super & Non-Super user ======================

    @Test(dataProvider = "applyMultiApplyPublicDiscountTypeALLCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyPublicDiscountTypeALLCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");

        String applyCouponPayload = new ApplyCouponPOJO(code, userIdTwo, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String codeInApply = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmountResp = applyCouponResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApply, code);
        Assert.assertEquals(couponErrorResp, null);
        Assert.assertEquals(discountAmountResp, "20.0");

    }

    @Test(dataProvider = "applyMultiApplyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, userId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "20.0");
        Processor mappingResp = RngHelper.couponUserMap(code, userIdTwo, couponId);
        String mappingDataResp = String.valueOf(mappingResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");
        String applyCouponPayload = new ApplyCouponPOJO(code, userIdTwo, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String codeInApply = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmountResp = applyCouponResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        Assert.assertEquals(codeInApply, code);
        Assert.assertEquals(couponErrorResp, null);
        Assert.assertEquals(discountAmountResp, "20.0");

    }

    @Test(dataProvider = "applyMultiApplyPublicFreeDelALLTypeCouponForSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of FreeDel type for coupon_user_type as Super (1)")
    public void applyMultiApplyPublicFreeDelALLTypeCouponForSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean freeShipping = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.metadata[0].freeShipping");
        Assert.assertEquals(freeShipping, true);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String codeInApply = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmountResp = applyCouponResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingInApply = applyCouponResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApply, code);
        Assert.assertEquals(couponErrorResp, null);
        Assert.assertEquals(discountAmountResp, "0.0");
        Assert.assertEquals(freeShippingInApply, true);

    }


    @Test(dataProvider = "applyMultiApplyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        Processor mappingProcessor = RngHelper.couponUserMap(code, mapUserId, couponId);
        String Mapping_Id = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
        String mappingData = String.valueOf(mappingProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingData, "Mapping fail");
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShippingRes = applyResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApplyResp, code);
        Assert.assertEquals(couponError, null);
        Assert.assertEquals(discountAmount, "0.0");
        Assert.assertEquals(freeShippingRes, true);


        Processor mapping = RngHelper.couponUserMap(code, userId, couponId);
        String mappingDataResp = String.valueOf(mapping.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        Assert.assertNotNull(mappingDataResp, "Mapping fail");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String codeInApply = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmountResp = applyCouponResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        boolean freeShipping = applyCouponResponse.ResponseValidator.GetNodeValueAsBool("$.data.free_shipping");
        Assert.assertEquals(codeInApply, code);
        Assert.assertEquals(couponErrorResp, null);
        Assert.assertEquals(discountAmountResp, "0.0");
        Assert.assertEquals(freeShipping, true);


    }
//============================== Apply ALL type coupon when no user mapping  =======================================

    @Test(dataProvider = "applyMultiApplyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapDiscountALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_TO_USER");
        String applyCouponPayload = new ApplyCouponPOJO(code, userIdTwo, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponErrorResp, "E_COUPON_NOT_APPLICABLE_TO_USER");

    }

    @Test(dataProvider = "applyMultiApplyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapFreeDelALLTypeCouponForBothSuperNnonSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String applyCoupon = new ApplyCouponPOJO(code, mapUserId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_TO_USER");

        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponErrorResp, "E_COUPON_NOT_APPLICABLE_TO_USER");

    }

    //============================== Apply Super type when no user mapping  =======================================

    @Test(dataProvider = "applyMultiApplyUserMapDiscountSuperTypeCouponForSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapDiscountSuperTypeCouponForSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_TO_USER");


    }

    @Test(dataProvider = "applyMultiApplyUserMapFreeDelSuperTypeCouponForSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapFreeDelSuperTypeCouponForSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponErrorResp, "E_COUPON_NOT_APPLICABLE_TO_USER");

    }

    //============================== Apply Super type when no user mapping  =======================================

    @Test(dataProvider = "applyMultiApplyUserMapDiscountNonSuperTypeCouponForNonSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Discount type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapDiscountNonSuperTypeCouponForNonSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String userIdTwo = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String applyCoupon = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_TO_USER");

    }

    @Test(dataProvider = "applyMultiApplyUserMapFreeDelNonSuperTypeCouponForNonSuperUserWhenUserNotMappedData", dataProviderClass = CouponSuperDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void applyMultiApplyUserMapFreeDelNonSuperTypeCouponForNonSuperUserWhenUserNotMappedTest(HashMap<String, Object> data) {
        String mapUserId = Integer.toString(Utility.getRandom(1, 99900));
        String userId = Integer.toString(Utility.getRandom(1, 99900));

        CreateMultiApplyCouponPOJO payload = (CreateMultiApplyCouponPOJO) data.get("createMultiApplyCouponPOJO");
        String code = payload.getCode();
        Processor createResponse = rngHelper.createMutiCoupon(payload);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Assert.assertEquals(statusCode, 1, "Failure because --> Coupon not created for coupon_user_type-Super");
        boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.isPrivate");
        Assert.assertEquals(isPrivate, true);
        String applyCouponPayload = new ApplyCouponPOJO(code, userId, false, 200.0, 2).toString();
        Processor applyCouponResponse = RngHelper.applyCoupon(applyCouponPayload, code);
        String couponErrorResp = applyCouponResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponErrorResp, "E_COUPON_NOT_APPLICABLE_TO_USER");

    }




    /* <=================>TimeSlot Cases<=====================> */


    @Test(dataProvider = "createPublicDiscountTypeCouponForUserTypeNonSuperData", dataProviderClass = CouponTimeSlotDP.class, description = "Create user mapped coupon of Free Del. type for coupon_user_type as Super (1)")
    public void couponTimeSlotDayOverLapPublicUserTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("mon", "1600", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("tue", "1600", "300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String couponErrorResp = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        int statusCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode");
        Assert.assertEquals(couponErrorResp, "TimeSlot is Invalid");
        Assert.assertEquals(statusCode, 0);

    }


    @Test(dataProvider = "createPrivateDiscountTypeWithFreeDelCouponForUserTypeNonSuperData", dataProviderClass = CouponTimeSlotDP.class)
    public void couponTimeSlotDayOverLapPrivateUserTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("mon", "1600", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("tue", "1600", "300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String couponErrorResp = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        int statusCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode");
        Assert.assertEquals(couponErrorResp, "TimeSlot is Invalid");
        Assert.assertEquals(statusCode, 0);

    }


    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponTimeSlotAllDayPublicTypeUserTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("All", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }

    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponTimeSlotAllDayPrivateTypeUserTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("All", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }


    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPublicValidTimeSlotOverlapDayTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "0", "1600", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("All", "1500", "2359", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }

    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPublicValidTimeSlotOverlapDayCrossedTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "1000", "1600", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("All", "1500", "300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_INVALID_DAY");

    }


    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPrivateValidTimeSlotOverlapDayCrossedTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "1000", "1600", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("All", "1500", "300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_INVALID_DAY");

    }


    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPrivateValidTimeSlotOverlapDayTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "0", "1600", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("All", "1500", "2359", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }

    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPublicValidTimeSlotOverlapDaySpecificTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("mon", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("tue", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String responseDay1 = processor.ResponseValidator.GetNodeValue("$.data[0].day");
        System.out.println(responseDay1);
        String responseDay2 = processor.ResponseValidator.GetNodeValue("$.data[1].day");
        System.out.println(responseDay2);
        String currentDay = RngHelper.getDay();
        System.out.println(currentDay);
        if ((responseDay1.equalsIgnoreCase(currentDay)) || (responseDay2.equalsIgnoreCase(currentDay))) {
            String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
            Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
            String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
            Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

        } else {
            String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
            Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
            String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
            Assert.assertEquals(couponError, "E_COUPON_INVALID_DAY");
        }

    }

    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPrivateValidTimeSlotOverlapDaySpecificTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("mon", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("tue", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String responseDay1 = processor.ResponseValidator.GetNodeValue("$.data[0].day");
        System.out.println(responseDay1);
        String responseDay2 = processor.ResponseValidator.GetNodeValue("$.data[1].day");
        System.out.println(responseDay2);
        String currentDay = RngHelper.getDay();
        System.out.println(currentDay);
        if ((responseDay1.equalsIgnoreCase(currentDay)) || (responseDay2.equalsIgnoreCase(currentDay))) {
            String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
            Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
            String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
            Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

        } else {
            String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
            Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
            String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
            Assert.assertEquals(couponError, "E_COUPON_INVALID_DAY");
        }

    }

    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void createPublicUserTypeCouponWithTimeSlotRestrictionTrueTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        Boolean slotRestriction = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.slot_restriction");
        System.out.println(slotRestriction);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("mon", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("tue", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String responseDay1 = processor.ResponseValidator.GetNodeValue("$.data[0].day");
        System.out.println(responseDay1);
        String responseDay2 = processor.ResponseValidator.GetNodeValue("$.data[1].day");
        System.out.println(responseDay2);
        String currentDay = RngHelper.getDay();
        System.out.println(currentDay);
        if ((responseDay1.equalsIgnoreCase(currentDay)) || (responseDay2.equalsIgnoreCase(currentDay))) {
            String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
            Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
            String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
            Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");
            Assert.assertEquals(slotRestriction, Boolean.TRUE);

        } else {
            List<CouponTimeSlotPOJO> payload2 = new ArrayList<CouponTimeSlotPOJO>();
            CouponTimeSlotPOJO couponTimeSlotPOJO2 = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
            payload2.add(couponTimeSlotPOJO2);
            CouponTimeSlotPOJO couponTimeSlotPOJO3 = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
            payload2.add(couponTimeSlotPOJO3);
            Processor processor2 = rngHelper.couponTimeSlotOverLap(payload2);
            String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
            Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
            String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
            Assert.assertEquals(couponError, null);
            Assert.assertEquals(slotRestriction, Boolean.TRUE);

        }

    }


    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void createPrivateUserTypeCouponWithTimeSlotRestrictionTrueTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        Boolean slotRestriction = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.slot_restriction");
        System.out.println(slotRestriction);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("MON", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("TUE", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String responseDay1 = processor.ResponseValidator.GetNodeValue("$.data[0].day");
        System.out.println(responseDay1);
        String responseDay2 = processor.ResponseValidator.GetNodeValue("$.data[1].day");
        System.out.println(responseDay2);
        String currentDay = RngHelper.getDay();
        System.out.println(currentDay);
        if ((responseDay1.equalsIgnoreCase(currentDay)) || (responseDay2.equalsIgnoreCase(currentDay))) {
            String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
            Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
            String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
            Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");
            Assert.assertEquals(slotRestriction, Boolean.TRUE);

        } else {
            List<CouponTimeSlotPOJO> payload2 = new ArrayList<CouponTimeSlotPOJO>();
            CouponTimeSlotPOJO couponTimeSlotPOJO2 = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
            payload2.add(couponTimeSlotPOJO2);
            CouponTimeSlotPOJO couponTimeSlotPOJO3 = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
            payload2.add(couponTimeSlotPOJO3);
            Processor processor2 = rngHelper.couponTimeSlotOverLap(payload2);
            String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
            Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
            String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
            Assert.assertEquals(couponError, null);
            Assert.assertEquals(slotRestriction, Boolean.TRUE);

        }


    }

    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionFalse", dataProviderClass = CouponTimeSlotDP.class)
    public void createPublicUserTypeCouponWithTimeSlotRestrictionFalseTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1800", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "1800", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }

    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionFalse", dataProviderClass = CouponTimeSlotDP.class)
    public void createPrivateUserTypeCouponWithTimeSlotRestrictionFalseTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "1600", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }

    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void destroyTimeSlotForPublicUserTypeCouponSlotRestrictionTrueTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        Processor destroyProcessor = rngHelper.destroyCouponTimeSlot(couponId);
        Boolean dataValue = destroyProcessor.ResponseValidator.GetNodeValueAsBool("$.data");
        System.out.println(dataValue);
        Assert.assertTrue(dataValue);
        String couponSlotTableData = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableData);
        Assert.assertEquals(couponSlotTableData, "code is not present in DB");
        List<CouponTimeSlotPOJO> payload2 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJOS = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload2.add(couponTimeSlotPOJOS);
        CouponTimeSlotPOJO couponTimeSlotPOJO2 = new CouponHelper().timeSlotMaping("all", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload2.add(couponTimeSlotPOJO2);
        Processor processor2 = rngHelper.couponTimeSlotOverLap(payload2);
        String couponSlotTableDataAfterSlotApi = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableDataAfterSlotApi);
        Assert.assertEquals(couponSlotTableDataAfterSlotApi, couponCode);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }

    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void destroyTimeSlotForPrivateUserTypeCouponSlotRestrictionTrueTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        Processor destroyProcessor = rngHelper.destroyCouponTimeSlot(couponId);
        Boolean dataValue = destroyProcessor.ResponseValidator.GetNodeValueAsBool("$.data");
        System.out.println(dataValue);
        Assert.assertTrue(dataValue);
        String couponSlotTableData = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableData);
        Assert.assertEquals(couponSlotTableData, "code is not present in DB");
        List<CouponTimeSlotPOJO> payload2 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJOS = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload2.add(couponTimeSlotPOJOS);
        CouponTimeSlotPOJO couponTimeSlotPOJO2 = new CouponHelper().timeSlotMaping("all", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload2.add(couponTimeSlotPOJO2);
        Processor processor2 = rngHelper.couponTimeSlotOverLap(payload2);
        String couponSlotTableDataAfterSlotApi = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableDataAfterSlotApi);
        Assert.assertEquals(couponSlotTableDataAfterSlotApi, couponCode);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");
    }

    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionFalse", dataProviderClass = CouponTimeSlotDP.class)
    public void destroyTimeSlotForPublicUserTypeCouponSlotRestrictionFalseTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "1600", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        Processor destroyProcessor = rngHelper.destroyCouponTimeSlot(couponId);
        Boolean dataValue = destroyProcessor.ResponseValidator.GetNodeValueAsBool("$.data");
        System.out.println(dataValue);
        Assert.assertTrue(dataValue);
        String couponSlotTableData = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableData);
        Assert.assertEquals(couponSlotTableData, "code is not present in DB");
        List<CouponTimeSlotPOJO> payload2 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJOS = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload2.add(couponTimeSlotPOJOS);
        CouponTimeSlotPOJO couponTimeSlotPOJO2 = new CouponHelper().timeSlotMaping("all", "1600", "2300", couponCode, Integer.valueOf(couponId));
        payload2.add(couponTimeSlotPOJO2);
        Processor processor2 = rngHelper.couponTimeSlotOverLap(payload2);
        String couponSlotTableDataAfterSlotApi = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableDataAfterSlotApi);
        Assert.assertEquals(couponSlotTableDataAfterSlotApi, couponCode);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }


    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionFalse", dataProviderClass = CouponTimeSlotDP.class)
    public void destroyTimeSlotForPrivateUserTypeCouponSlotRestrictionFalseTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "1600", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        Processor destroyProcessor = rngHelper.destroyCouponTimeSlot(couponId);
        Boolean dataValue = destroyProcessor.ResponseValidator.GetNodeValueAsBool("$.data");
        System.out.println(dataValue);
        Assert.assertTrue(dataValue);
        String couponSlotTableData = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableData);
        Assert.assertEquals(couponSlotTableData, "code is not present in DB");
        List<CouponTimeSlotPOJO> payload2 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJOS = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload2.add(couponTimeSlotPOJOS);
        CouponTimeSlotPOJO couponTimeSlotPOJO2 = new CouponHelper().timeSlotMaping("all", "1600", "2300", couponCode, Integer.valueOf(couponId));
        payload2.add(couponTimeSlotPOJO2);
        Processor processor2 = rngHelper.couponTimeSlotOverLap(payload2);
        String couponSlotTableDataAfterSlotApi = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableDataAfterSlotApi);
        Assert.assertEquals(couponSlotTableDataAfterSlotApi, couponCode);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }

    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void destroyTimeSlotForPublicUserTypeCouponSlotRestrictionTrueWithWrongCouponIDTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String couponSlotTableDataAfterSlotApi = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableDataAfterSlotApi);
        Assert.assertEquals(couponSlotTableDataAfterSlotApi, couponCode);
        Processor destroyProcessor = rngHelper.destroyCouponTimeSlot(couponId + "123");
        Boolean dataValue = destroyProcessor.ResponseValidator.GetNodeValueAsBool("$.data");
        System.out.println(dataValue);
        Assert.assertTrue(dataValue);
        String couponSlotTableData = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableData);
        Assert.assertEquals(couponSlotTableData, couponCode);
        String couponSlotAudTableData = rngHelper.checkCouponAvailabilityCouponSlotAud(couponCode);
        System.out.println("=======>>>>" + couponSlotAudTableData);
        Assert.assertEquals(couponSlotAudTableData, couponCode);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");
        String couponSlotcloseTime = rngHelper.checkCouponTimeSlotAvailability(couponCode);
        System.out.println("=======>>>>" + couponSlotcloseTime);
        Assert.assertEquals(couponSlotcloseTime, "2300");
    }


    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void destroyTimeSlotForPrivateUserTypeCouponSlotRestrictionTrueWithWrongCouponIDTest(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String couponSlotTableDataAfterSlotApi = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableDataAfterSlotApi);
        Assert.assertEquals(couponSlotTableDataAfterSlotApi, couponCode);
        Processor destroyProcessor = rngHelper.destroyCouponTimeSlot(couponId + "123");
        Boolean dataValue = destroyProcessor.ResponseValidator.GetNodeValueAsBool("$.data");
        System.out.println(dataValue);
        Assert.assertTrue(dataValue);
        String couponSlotTableData = rngHelper.checkCouponAvailabilityCouponSlot(couponCode);
        System.out.println("=======>>>>" + couponSlotTableData);
        Assert.assertEquals(couponSlotTableData, couponCode);
        String couponSlotAudTableData = rngHelper.checkCouponAvailabilityCouponSlotAud(couponCode);
        System.out.println("=======>>>>" + couponSlotAudTableData);
        Assert.assertEquals(couponSlotAudTableData, couponCode);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");
        String couponSlotcloseTime = rngHelper.checkCouponTimeSlotAvailability(couponCode);
        System.out.println("=======>>>>" + couponSlotcloseTime);
        Assert.assertEquals(couponSlotcloseTime, "2300");

    }


    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPublicValidTimeSlotWithOffsetTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String couponCodeData = rngHelper.checkMultiApplyCoupon(couponCode);
        System.out.println("=======>>>>" + couponCodeData);
        Assert.assertEquals(couponCodeData, couponCode);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("All", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }


    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPrivateValidTimeSlotWithOffsetTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String couponCodeData = rngHelper.checkMultiApplyCoupon(couponCode);
        System.out.println("=======>>>>" + couponCodeData);
        Assert.assertEquals(couponCodeData, couponCode);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "1000", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("All", "0", "2300", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }

    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPublicValidTimeSlotTimeOverLapOnSameDayTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String couponCodeData = rngHelper.checkMultiApplyCoupon(couponCode);
        System.out.println("=======>>>>" + couponCodeData);
        Assert.assertEquals(couponCodeData, couponCode);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2358", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "0", "1800", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }

    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue", dataProviderClass = CouponTimeSlotDP.class)
    public void couponPrivateValidTimeSlotTimeOverLapOnSameDayTest(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String couponCodeData = rngHelper.checkMultiApplyCoupon(couponCode);
        System.out.println("=======>>>>" + couponCodeData);
        Assert.assertEquals(couponCodeData, couponCode);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2358", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "0", "1800", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String descriptions = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(descriptions, "Your coupon" + " " + couponCode + " " + "is successfully applied. You will save Rs 50.0 with this coupon.");

    }


    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrueWithValidFromAsPreviousMonth", dataProviderClass = CouponTimeSlotDP.class)
    public void createPublicUserTypeCouponWithTimeSlotRestrictionTrueWithValidFromAsPreviousMonth(HashMap<String, Object> data) throws IOException {


        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String couponCodeData = rngHelper.checkMultiApplyCoupon(couponCode);
        System.out.println("=======>>>>" + couponCodeData);
        Assert.assertEquals(couponCodeData, couponCode);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("all", "1000", "2358", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "0", "1800", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, null);
    }

    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrueWithValidFromAsPreviousMonth", dataProviderClass = CouponTimeSlotDP.class)
    public void createPrivateUserTypeCouponWithTimeSlotRestrictionTrueWithValidFromAsPreviousMonth(HashMap<String, Object> data) throws IOException {


        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String couponCodeData = rngHelper.checkMultiApplyCoupon(couponCode);
        System.out.println("=======>>>>" + couponCodeData);
        Assert.assertEquals(couponCodeData, couponCode);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "1000", "2358", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("All", "0", "1800", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, null);
    }


    @Test(dataProvider = "createPublicUserTypeCouponWithTimeSlotRestrictionTrueWithOffsetZero", dataProviderClass = CouponTimeSlotDP.class)
    public void createPublicUserTypeCouponWithTimeSlotRestrictionTrueWithOffsetZero(HashMap<String, Object> data) throws IOException {


        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String couponCodeData = rngHelper.checkMultiApplyCoupon(couponCode);
        System.out.println("=======>>>>" + couponCodeData);
        Assert.assertEquals(couponCodeData, couponCode);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("All", "1000", "2358", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("all", "0", "1800", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, null);
    }

    @Test(dataProvider = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrueWithOffsetZero", dataProviderClass = CouponTimeSlotDP.class)
    public void createPrivateUserTypeCouponWithTimeSlotRestrictionTrueWithOffsetZero(HashMap<String, Object> data) throws IOException {


        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String couponCodeData = rngHelper.checkMultiApplyCoupon(couponCode);
        System.out.println("=======>>>>" + couponCodeData);
        Assert.assertEquals(couponCodeData, couponCode);
        List<CouponTimeSlotPOJO> payload1 = new ArrayList<CouponTimeSlotPOJO>();
        CouponTimeSlotPOJO couponTimeSlotPOJO = new CouponHelper().timeSlotMaping("ALL", "1000", "2358", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO);
        CouponTimeSlotPOJO couponTimeSlotPOJO1 = new CouponHelper().timeSlotMaping("ALL", "0", "1800", couponCode, Integer.valueOf(couponId));
        payload1.add(couponTimeSlotPOJO1);
        Processor processor = rngHelper.couponTimeSlotOverLap(payload1);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, null);
    }



    /*==============================================================MultiApplyGlobalOffset==============================================*/


    @Test(dataProvider = "createMultiApplyCouponForSingleTransaction", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForSingleTransaction(HashMap<String, Object> data) throws IOException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        String applyCoupon = new ApplyCouponPOJO(couponCode, "5660904", false, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, null);
    }

    @Test(dataProvider = "createMultiApplyCouponForSingleTransaction", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForSingleTransactionGlobalOffsetExpired(HashMap<String, Object> data) throws ParseException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(35)).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        Assert.assertEquals(couponError, "E_COUPON_EXPIRED", "Bug:Needs to be Fixed,Sent Future date in Apply api to expire the Global offset");
    }

    @Test(dataProvider = "createMultiApplyCouponForMultipleTransaction", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForMultipleTransactionOnNthGlobalOffset(HashMap<String, Object> data) throws ParseException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int userCount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.totalPerUser");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        for (int i = 1; i <= userCount; i++) {
            if (i == 1) {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction, getDateToSetInCouponUseCountDate(-29));
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
            }

        }
    }


    @Test(dataProvider = "createMultiApplyCouponForMultipleTransactionGlobalOffsetGreaterThanValidTill", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForMultipleTransactionGlobalOffsetGreaterThanValidTill(HashMap<String, Object> data) throws ParseException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 0, "Bug:Needs to be Fixed,Global Offset cannot cross the Valid till limit ");
    }


    @Test(dataProvider = "createMultiApplyCouponForMultipleTransaction", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForMultipleTransactionWithGlobalOffset(HashMap<String, Object> data) throws ParseException, IOException {
        List<String> couponList = new ArrayList<>();
        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int userCount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.totalPerUser");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        for (int i = 1; i <= userCount; i++) {
            if (i == 1) {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction, getDateToSetInCouponUseCountDate(-32));
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, "E_COUPON_EXPIRED");
                couponList.add(couponCode);
                NuxPOJO nuxPojo = new NuxPOJO(5660904, 1, couponList);
                Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
                String couponError1 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponError");
                String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].description");
                Assert.assertEquals(couponError1, "E_COUPON_EXPIRED", "GlobalOffset is not Expired");
                Assert.assertEquals(description, "Coupon expired");
            }

        }
    }


    @Test(dataProvider = "createMultiApplyCouponForMultipleTransaction", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForMultipleTransactionLessThanGlobalOffset(HashMap<String, Object> data) throws ParseException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int userCount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.totalPerUser");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        for (int i = 1; i <= userCount; i++) {
            if (i == 1) {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction, getDateToSetInCouponUseCountDate(-28));
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
            }

        }
    }


    @Test(dataProvider = "createMultiApplyCouponForMultipleTransactionWith60GlobalOffset", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForMultipleTransactionOffsetvalidityWithGlobalOffset(HashMap<String, Object> data) throws ParseException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int userCount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.totalPerUser");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        for (int i = 1; i <= userCount; i++) {
            if (i == 1) {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction, getDateToSetInCouponUseCountDate(-61));
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, "E_COUPON_EXPIRED");
            }
        }
    }


    @Test(dataProvider = "createMultiApplyCouponForMultipleTransactionWithinGlobalOffset", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForMultipleTransactionOffsetvalidityWithinGlobalOffset(HashMap<String, Object> data) throws ParseException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int userCount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.totalPerUser");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        for (int i = 1; i <= userCount; i++) {
            if (i == 1) {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction, getDateToSetInCouponUseCountDate(-59));
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
            }
        }

    }

    @Test(dataProvider = "createMultiApplyCouponForMultipleTransactionOffSetvalidityWithMultipleTransactionGlobalOffset", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffset(HashMap<String, Object> data) throws ParseException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int userCount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.totalPerUser");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        for (int i = 1; i <= userCount; i++) {
            if (i == 1) {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction1 = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction1, getDateToSetInCouponUseCountDate(-29));
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else if (i == 2) {

                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction2 = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction2, getDateToSetInCouponUseCountDate(-60));
                rngHelper.updateFirstTransactionCreatedDateCouponUseCount(orderIdTransaction2, getDateToSetInCouponUseCountDate(-37), couponCode);
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, "E_COUPON_EXPIRED");
            }
        }


    }

    @Test(dataProvider = "createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffsetDisable", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffsetDisable(HashMap<String, Object> data) throws ParseException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int userCount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.totalPerUser");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        for (int i = 1; i <= userCount; i++) {
            if (i == 1) {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction1 = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction1, getDateToSetInCouponUseCountDate(-29));
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else if (i == 2) {

                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction2 = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction2, getDateToSetInCouponUseCountDate(-35));
                rngHelper.updateFirstTransactionCreatedDateCouponUseCount(orderIdTransaction2, getDateToSetInCouponUseCountDate(-37), couponCode);
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, "E_COUPON_EXPIRED");
            }
        }


    }

    @Test(dataProvider = "createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffSetWithZeroOffSet", dataProviderClass = CouponGlobalOffsetDP.class)
    public void createMultiApplyCouponForMultipleTransactionOffSetValidityWithMultipleTransactionGlobalOffSetWithZeroOffSet(HashMap<String, Object> data) throws ParseException {

        GlobalOffsetCreateMultiApplyCouponPOJO payload = (GlobalOffsetCreateMultiApplyCouponPOJO) data.get("GlobalOffsetCreateMultiApplyCouponPOJO");
        Processor createResponse = rngHelper.createMultiApplyCouponGlobalOffset(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int userCount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.totalPerUser");
        Assert.assertNotEquals(response, null);
        Assert.assertEquals(statusCode, 1);
        String couponId = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        String couponCode = RngHelper.getMultiApplyCouponId(couponId);
        for (int i = 1; i <= userCount; i++) {
            if (i == 1) {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction1 = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction1, getDateToSetInCouponUseCountDate(-29));
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else if (i == 2) {

                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, null);
                Processor useCount = RngHelper.couponUsageCountGlobalOffset(couponCode, "5660904");
                int userId = useCount.RequestValidator.GetNodeValueAsInt("$.userId");
                String orderIdTransaction2 = useCount.RequestValidator.GetNodeValue("$.orderId");
                rngHelper.updateCreatedDateCouponUseCount(orderIdTransaction2, getDateToSetInCouponUseCountDate(-35));
                rngHelper.updateFirstTransactionCreatedDateCouponUseCount(orderIdTransaction2, getDateToSetInCouponUseCountDate(-37), couponCode);
                couponRedisHelper.flushPreviousDateFromRedis(couponCode);
                couponRedisHelper.flushCouponUsedCacheFromRedis(String.valueOf(userId));
                Processor guavaEviction = RngHelper.guavaEviction();
                int statusCode1 = guavaEviction.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                Assert.assertEquals(statusCode1, 1);
            } else {
                String applyCoupon1 = new ApplyCouponGlobalOffsetPOJO(couponCode, "5660904", getOneMonthAhead(0)).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon1, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                Assert.assertEquals(couponError, "E_COUPON_EXPIRED");
            }
        }

    }

    /*===================================================TD+Coupon Capping============================================================*/


    @Test(dataProvider = "cappingTDAsZero", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingWithTDAsZero(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        int discount_amount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.discount_amount");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 0.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + ".0" + " with this coupon.");
        Assert.assertEquals(discount_amount, 10);
    }

    @Test(dataProvider = "cappingTDAsMinusOne", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDAsMinusOne(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        int discount_amount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.discount_amount");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, -1.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + ".0" + " with this coupon.");
        Assert.assertEquals(discount_amount, 10);
    }


    @Test(dataProvider = "cappingTDWithDiscountTypeAsCashback", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDWithDiscountTypeAsCashback(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        Integer discount_amount = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.discount_amount");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        Assert.assertEquals(description, "Your cashback coupon" + " " + code + " " + "is successfully applied.", "Cashback coupon type is not applicable for Coupon and TD Capping");
    }


    @Test(dataProvider = "cappingTDWithDiscountTypeAsMarketing", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDWithDiscountTypeAsMarketing(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        String info_message = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.info_message");
        System.out.println(discount_amount);
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon." + " " + info_message);
        Assert.assertEquals(Double.valueOf(discount_amount), 78.0);
    }

    @Test(dataProvider = "cappingTDWithCappingPercentageAsZero", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDWithCappingPercentageAsZero(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon.");
        Assert.assertEquals(Double.valueOf(discount_amount), 90.0);
    }

    @Test(dataProvider = "cappingTDWithCappingPercentageAsMinusOne", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDWithCappingPercentageAsMinusOne(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon.");
        Assert.assertEquals(Double.valueOf(discount_amount), 90.0);
    }

    @Test(dataProvider = "cappingTDWithDiscountPercentageAsLessThanCappingPercentage", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDWithDiscountPercentageAsLessThanCappingPercentage(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon.");
        Assert.assertEquals(Double.valueOf(discount_amount), 15.0);
    }

    @Test(dataProvider = "cappingTDWithDiscountAmountAsLessThanCappingPercentageAndDiscountPercentage", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDWithDiscountAmountAsLessThanCappingPercentageAndDiscountPercentage(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon.");
        Assert.assertEquals(Double.valueOf(discount_amount), 10.0);
    }

    @Test(dataProvider = "cappingTDWithCappingPercentageAsHundredPercentage", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDWithCappingPercentageAsHundredPercentage(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon.");
        Assert.assertEquals(Double.valueOf(discount_amount), 15.0);
    }

    @Test(dataProvider = "cappingTDWithCappingPercentageAsOnePercentage", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDWithCappingPercentageAsOnePercentage(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        String info_message = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.info_message");
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon." + " " + info_message);
        Assert.assertEquals(Double.valueOf(discount_amount), 18.5, "If default hardcode value is change by dev than discount amount value will be changed");
    }

    @Test(dataProvider = "cappingTDWithCappingPercentageAsHundredAndTenPercentage", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDWithCappingPercentageAsHundredAndTenPercentage(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon.");
        Assert.assertEquals(Double.valueOf(discount_amount), 40.0);
    }

    @Test(dataProvider = "cappingTDMoreThanHundredWithCappingPercentageAsSeventy", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDMoreThanHundredWithCappingPercentageAsSeventy(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 200.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        String info_message = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.info_message");
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon." + " " + info_message);
        Assert.assertEquals(Double.valueOf(discount_amount), 150.0);
    }


    @Test(dataProvider = "cappingTDMoreThanHundredWithCappingPercentageAsSeventyWithDiscountAmountSameAsCappingCalculatedAmount", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDMoreThanHundredWithCappingPercentageAsSeventyWithDiscountAmountSameAsCappingCalculatedAmount(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 200.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon.");
        Assert.assertEquals(Double.valueOf(discount_amount), 150.0);
    }

    @Test(dataProvider = "cappingTDAsCartValue", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDAsCartValue(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 300.0, 300.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        String info_message = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.info_message");
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon." + " " + info_message);
        Assert.assertEquals(Double.valueOf(discount_amount), 120.0);
    }

    @Test(dataProvider = "cappingTDAsCartValue", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingTDAsCartValueZero(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 300.0, 0.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        String couponError = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.couponError");
        Assert.assertEquals(description, null);
        Assert.assertEquals(couponError, "E_COUPON_CART_MIN_AMOUNT_NOT_MET");
    }


    @Test(dataProvider = "cappingPercentageAsFifty", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingPercentageAsFifty(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 70.0, 230.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        String info_message = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.info_message");
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon." + " " + info_message);
        Assert.assertEquals(Double.valueOf(discount_amount), 80.0);
    }

    @Test(dataProvider = "cappingPercentageAsFiftyWithCouponDiscountAsFlat", dataProviderClass = TDAndCouponCappingDP.class)
    public void cappingPercentageAsFiftyWithCouponDiscountAsFlat(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 60.0, 240.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        String info_message = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.info_message");
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon." + " " + info_message);
        Assert.assertEquals(Double.valueOf(discount_amount), 90.0);
    }

    @Test(dataProvider = "cartTotalGreaterThanFiveHundred", dataProviderClass = TDAndCouponCappingDP.class)
    public void cartTotalGreaterThanFiveHundred(HashMap<String, Object> data) {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String response = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        String code = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(code);
        Assert.assertNotEquals(response, null);
        ApplyCouponCappingTDAndCouponPOJO applyCouponWithTDAsZero = new ApplyCouponCappingTDAndCouponPOJO(code, 60.0, 540.0);
        Processor applyCoupon = rngHelper.applyCoupon(String.valueOf(applyCouponWithTDAsZero), code);
        String description = applyCoupon.ResponseValidator.GetNodeValue("$.data.description");
        String discount_amount = applyCoupon.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        System.out.println(discount_amount);
        Assert.assertEquals(description, "Your coupon" + " " + code + " " + "is successfully applied. You will save Rs" + " " + discount_amount + " with this coupon.");
        Assert.assertEquals(Double.valueOf(discount_amount), 199.0);
    }




    /*=======================================================Verify+Nux=================================================*/


    List<String> couponListCityRestriction = new ArrayList<>();

    @Test(dataProvider = "createCouponWithCityRestrictionAsTrueAndFalse", dataProviderClass = VerifyNuxDP.class, description = "Error message validation on timeslot,city,offset,city restriction")
    public void verifyCityRestrictionAsTrueAndFalse(HashMap<String, Object> data) throws IOException {

        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        Processor cityMapProcessor = rngHelper.couponCityMap(couponCode, cityId, couponId);
        int cityId = cityMapProcessor.ResponseValidator.GetNodeValueAsInt("$.data[0].cityId");
        couponListCityRestriction.add(couponCode);
    }

    @Test(dependsOnMethods = "verifyCityRestrictionAsTrueAndFalse")
    public void verifyCityRestrictionTest() throws IOException {
        NuxPOJO nuxPojo = new NuxPOJO(123456, 2, couponListCityRestriction);
        Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
        SoftAssert softAssert = new SoftAssert();
        String couponCode1 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].code");
        String couponCode2 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].code");
        String couponCode3 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].code");
        String couponCode4 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].code");
        for (int i = 0; i < couponListCityRestriction.size(); i++) {
            if (couponListCityRestriction.get(i).equalsIgnoreCase(couponCode1)) {
                String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValue("$.data[0].couponError");
                softAssert.assertEquals(couponError, null, "City Restriction Failure which is made as False");
            } else if (couponListCityRestriction.get(i).equalsIgnoreCase(couponCode2)) {
                String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].couponError");
                String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].description");
                softAssert.assertEquals(couponError, "E_COUPON_INVALID_CITY", "City Restriction Failure which is made as True");
                softAssert.assertEquals(description, "Coupon is not applicable for this city");
            } else if (couponListCityRestriction.get(i).equalsIgnoreCase(couponCode3)) {
                String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].couponError");
                String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].description");
                softAssert.assertEquals(couponError, "E_COUPON_EXPIRED", "Valid Till is not Expired");
                softAssert.assertEquals(description, "Coupon expired");
            } else if (couponListCityRestriction.get(i).equalsIgnoreCase(couponCode4)) {
                String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].couponError");
                String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].description");
                softAssert.assertEquals(couponError, "E_COUPON_INVALID_DAY", "Within TimeSlot");
                softAssert.assertEquals(description, "This coupon is not valid for today");
            } else {
                String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].couponError");
                String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].description");
                softAssert.assertEquals(couponError, "E_COUPON_EXPIRED", "Expired offset is not Expired");
                softAssert.assertEquals(description, "Coupon expired");
            }


        }
        softAssert.assertAll();

    }


//    @Test(dataProvider = "createCouponWithFirstTimeUserRestrictionsAsTrueNegative", dataProviderClass = VerifyNuxDP.class)
//    public void createCouponWithFirstTimeUserRestrictionsAsTrueAndFalse(HashMap<String, Object> data) throws IOException {
//        List<String> couponUserRestriction = new ArrayList<>();
//        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
//        int customer_id1 = Utility.getRandom(90000, 140000);
//        int customer_id2 = Utility.getRandom(90000, 140000);
//        Processor createResponse = rngHelper.createCoupon(payload);
//        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
//        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
//        rngHelper.insertCustomerDeviceMapping(Utility.getRandom(9000, 140000),123456,String.valueOf(customer_id1));
//        Processor cityMapProcessor = rngHelper.couponCityMap(couponCode, cityId, couponId);
//        String applyCoupon = new ApplyCouponVerifyNuxPOJO(couponCode, String.valueOf(customer_id2), true,"123456").toString();
//        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
////        String couponError = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.couponError");
////        Assert.assertEquals(couponError,"E_COUPON_APPLICABLE_ONLY_ON_FIRST_ORDER");
//        int cityId = cityMapProcessor.ResponseValidator.GetNodeValueAsInt("$.data[0].cityId");
//        couponUserRestriction.add(couponCode);
//        NuxPOJO nuxPojo = new NuxPOJO(123456, cityId, couponUserRestriction);
//        Processor verifyNuxProcessor = rngHelper.verifyNux("123456", "ios", nuxPojo);
//        String couponError1 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponError");
//        String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].description");
//        Assert.assertEquals(couponError1, "E_COUPON_APPLICABLE_ONLY_ON_FIRST_ORDER", "First Time Order Restriction got Failed");
//        Assert.assertEquals(description, "This coupon is not valid for today");
//
//    }

//    @Test(dataProvider = "createCouponWithFirstTimeUserRestrictionsAsTruePositive", dataProviderClass = VerifyNuxDP.class)
//    public void createCouponWithFirstTimeUserRestrictionsAsTruePositive(HashMap<String, Object> data) throws IOException {
//        int customer_id = Utility.getRandom(90000, 140000);
//        List<String> couponUserRestriction = new ArrayList<>();
//        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
//        Processor createResponse = rngHelper.createCoupon(payload);
//        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
//        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
////        couponRedisHelper.deleteDormantUserRestMapping("123456");
//        Processor cityMapProcessor = rngHelper.couponCityMap(couponCode, cityId, couponId);
//        String applyCoupon = new ApplyCouponPOJO(couponCode, String.valueOf(customer_id), false, 200.0, 2).toString();
//        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
//        String couponError = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.couponError");
//        Assert.assertEquals(couponError,"E_COUPON_APPLICABLE_ONLY_ON_FIRST_ORDER");
//        int cityId = cityMapProcessor.ResponseValidator.GetNodeValueAsInt("$.data[0].cityId");
//        couponUserRestriction.add(couponCode);
////        NuxPOJO nuxPojo = new NuxPOJO(customer_id, cityId, couponUserRestriction);
////        Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
////        String couponError1 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponError");
////        String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].description");
////        Assert.assertEquals(couponError1, "E_COUPON_APPLICABLE_ONLY_ON_FIRST_ORDER", "First Time Order Restriction got Failed");
////        Assert.assertEquals(description, "This coupon is not valid for today");

//    }


    List<String> couponForPublicUser = new ArrayList<>();

    @Test(dataProvider = "createCouponForPublicUserSpecific", dataProviderClass = VerifyNuxDP.class, description = "Error message validation on Public and Private user restriction")
    public void createCouponForPublicUser(HashMap<String, Object> data) throws IOException {
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String userId = String.valueOf(new RandomNumber(900000, 999999).nextInt());
        Boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        if (isPrivate.equals(true)) {
            String couponCode1 = createResponse.ResponseValidator.GetNodeValue("$.data.code");
            System.out.println(couponCode);
            String couponId1 = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
            System.out.println(couponId);
            Processor cumProcessor = RngHelper.couponUserMap(couponCode1, userId, couponId1);

        }
        Processor cityMapProcessor = rngHelper.couponCityMap(couponCode, cityId, couponId);
        couponForPublicUser.add(couponCode);
    }


    @Test(dependsOnMethods = "createCouponForPublicUser")
    public void verifyPublicUserRestrictionTest() throws IOException {

        NuxPOJO nuxPojo = new NuxPOJO(5660904, 1, couponForPublicUser);
        Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
        SoftAssert softAssert = new SoftAssert();
        String couponCode1 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].code");
        String couponCode2 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].code");
        System.out.println(couponCode1);
        for (int i = 0; i < couponForPublicUser.size(); i++) {
            if (couponForPublicUser.get(i).equalsIgnoreCase(couponCode1)) {
                String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValue("$.data[0].couponError");
                softAssert.assertEquals(couponError, null, "Public coupon type is failed");
            } else if (couponForPublicUser.get(i).equalsIgnoreCase(couponCode2)) {
                String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValue("$.data[1].couponError");
                String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].description");
                softAssert.assertEquals(couponError, "E_COUPON_IS_NOT_APPLICABLE_FOR_THIS_USER", "Bug needs to fix:Private User Coupon Type Got Failed ");
                softAssert.assertEquals(description, "Coupon is not applicable for this User");
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponForPrivateUserSpecific", dataProviderClass = VerifyNuxDP.class, description = "private user validation:nux-verify")
    public void verifyCouponForPrivateUser(HashMap<String, Object> data) throws IOException {
        List<String> couponForPrivateUserList = new ArrayList<>();
        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        String userId = String.valueOf(new RandomNumber(900000, 999999).nextInt());
        Boolean isPrivate = createResponse.ResponseValidator.GetNodeValueAsBool("$.data.is_private");
        String couponCode1 = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId1 = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        Processor cumProcessor = RngHelper.couponUserMap(couponCode1, userId, couponId1);
        couponForPrivateUserList.add(couponCode);
        Processor cityMapProcessor = rngHelper.couponCityMap(couponCode, cityId, couponId);
        NuxPOJO nuxPojo = new NuxPOJO(5660904, 1, couponForPrivateUserList);
        Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
        String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValue("$.data[0].couponError");
        softAssert.assertEquals(couponError, null, "Private User Coupon Type Got Failed ");
        softAssert.assertAll();

    }

    @Test(dataProvider = "applyPublicDiscountTypeSuperCouponForUserTypeSuperData", dataProviderClass = CouponSuperDP.class, description = "Create public coupon of Discount type for coupon_user_type as Super (1) and Error check for non super user ")
    public void verifyCouponForSuperUserTest(HashMap<String, Object> data) throws IOException {

        List<String> couponForSuperUserList = new ArrayList<>();
        SoftAssert softAssert = new SoftAssert();
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor updateResponse = rngHelper.createCoupon(payload);
        int statusCode = updateResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponCode = updateResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponId = Integer.toString(updateResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        String code = payload.getCode();
        softAssert.assertEquals(statusCode, 1, "Fialure because --> Coupon not created for coupon_user_type-Super");
        String activeUserSubscription = SuperDbHelper.subscriptionCheckForUser(userId);
        System.out.print("active user id ------------->>  " + activeUserSubscription);
        if (!(activeUserSubscription.equals("No subscription found for user"))) {
            superHelper.cancelSubscriptionOfUser(activeUserSubscription);

        }
        superHelper.createSuperUserWithPublicPlan(userId);
        //String code, String userId,Boolean superUser, Double cartTotal, Integer cartPrice_Quantity)
        String applyCoupon = new ApplyCouponPOJO(code, userId, true, 200.0, 2).toString();
        Processor applyResponse = RngHelper.applyCoupon(applyCoupon, code);
        String codeInApplyResp = applyResponse.ResponseValidator.GetNodeValue("$.data.couponCode");
        String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
        String discountAmount = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount_amount");
        softAssert.assertEquals(codeInApplyResp, code);
        softAssert.assertEquals(couponError, null);
        softAssert.assertEquals(discountAmount, "20.0");
        couponForSuperUserList.add(couponCode);
        Processor cityMapProcessor = rngHelper.couponCityMap(couponCode, cityId, couponId);
        NuxPOJO nuxPojo = new NuxPOJO(5660904, 1, couponForSuperUserList);
        Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
        String couponError1 = verifyNuxProcessor.ResponseValidator.GetNodeValue("$.data[0].couponError");
        String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].description");
        softAssert.assertEquals(couponError1, "E_COUPON_NOT_APPLICABLE_FOR_USER", "Super User Coupon Type Got Failed ");
        softAssert.assertEquals(description, "Coupon Not Applicable for this user");
        softAssert.assertAll();

    }


    @Test(dataProvider = "useCountLimitCross", dataProviderClass = VerifyNuxDP.class, description = "Coupon Usages Count breach:Error message check")
    public void useCountLimitCross(HashMap<String, Object> data) throws IOException {
        List<String> userCountList = new ArrayList<>();
        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        int total_available = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.total_available");
        Processor cityMapProcessor = rngHelper.couponCityMap(couponCode, cityId, couponId);
        for (int i = 1; i <= total_available; i++) {
            RngHelper.couponUsageCount(couponCode, "5600769", String.valueOf(Utility.getRandom(1, 8)));
        }
        int cityId = cityMapProcessor.ResponseValidator.GetNodeValueAsInt("$.data[0].cityId");
        userCountList.add(couponCode);
        NuxPOJO nuxPojo = new NuxPOJO(5660904, cityId, userCountList);
        Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
        String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponError");
        String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].description");
        softAssert.assertEquals(couponError, "E_COUPON_USAGE_LIMIT_REACHED", "Coupon uses limit is not reached");
        softAssert.assertEquals(description, "Coupon usage limit has reached");
        softAssert.assertAll();
    }

    @Test(dataProvider = "customerRestriction", dataProviderClass = VerifyNuxDP.class, description = "Customer Restriction on coupon:Error Check")
    public void customerRestrictionTest(HashMap<String, Object> data) throws IOException {

        List<String> customerRestrictionList = new ArrayList<>();
        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        System.out.println(couponCode);
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        System.out.println(couponId);
        Processor cityMapProcessor = rngHelper.couponCityMap(couponCode, cityId, couponId);
        int cityId = cityMapProcessor.ResponseValidator.GetNodeValueAsInt("$.data[0].cityId");
        customerRestrictionList.add(couponCode);
        String userId = String.valueOf(new RandomNumber(900000, 999999).nextInt());
        NuxPOJO nuxPojo = new NuxPOJO(Integer.parseInt(userId), cityId, customerRestrictionList);
        Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
        String couponError = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponError");
        String description = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].description");
        softAssert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER", "Customer restriction is mapped as 0");
        softAssert.assertEquals(description, "Coupon Not Applicable for this user");
        softAssert.assertAll();
    }


    @Test(dataProvider = "applicableOnRestriction", dataProviderClass = VerifyNuxDP.class, description = "Transaction Level Restriction-ApplicableOn: Error Check")
    public void applicableOnTest(HashMap<String, Object> data) throws IOException {
        List<String> applicableOnList = new ArrayList<>();
        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        int customer_id = Utility.getRandom(90000, 140000);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponId = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        int total_available = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.total_available");
        Processor cityMapProcessor = rngHelper.couponCityMap(couponCode, cityId, couponId);
        for (int i = 1; i <= total_available; i++) {
            if (i == 1) {
                String applyCoupon = new ApplyCouponPOJO(couponCode, String.valueOf(customer_id), false, 200.0, 2).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.couponError");
                String description = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
                softAssert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER", "User transaction is mapped as 2");
                softAssert.assertEquals(description, null);
                applicableOnList.add(couponCode);
                NuxPOJO nuxPojo = new NuxPOJO(customer_id, Integer.parseInt(cityId), applicableOnList);
                Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
                String couponError1 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponError");
                String description1 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].description");
                softAssert.assertEquals(couponError1, "E_COUPON_NOT_APPLICABLE_FOR_USER", "User transaction is mapped as 2");
                softAssert.assertEquals(description1, "Coupon Not Applicable for this user");
            } else if (i == 2) {
                rngHelper.insertCustomerOrderCount(String.valueOf(Utility.getRandom(9000, 140000)), String.valueOf(customer_id), 1);
                int orderId = rngHelper.getCustomerOrderId(String.valueOf(customer_id));
                System.out.println(orderId);
                String applyCoupon = new ApplyCouponPOJO(couponCode, String.valueOf(customer_id), false, 200.0, 2).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValue("$.data.couponError");
                softAssert.assertEquals(couponError, null);
                applicableOnList.add(couponCode);
                NuxPOJO nuxPojo = new NuxPOJO(customer_id, Integer.parseInt(cityId), applicableOnList);
                Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
                String couponError1 = verifyNuxProcessor.ResponseValidator.GetNodeValue("$.data[0].couponError");
                softAssert.assertEquals(couponError1, null);
                rngHelper.updateCustomerOrderCount(String.valueOf(customer_id), 2);
                couponRedisHelper.flushOrderRelatedDataFromRedis("segmentationredis");
            } else {
                String applyCoupon = new ApplyCouponPOJO(couponCode, String.valueOf(customer_id), false, 200.0, 2).toString();
                Processor applyResponse = RngHelper.applyCoupon(applyCoupon, couponCode);
                String couponError = applyResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.couponError");
                String description = applyResponse.ResponseValidator.GetNodeValue("$.data.description");
                softAssert.assertEquals(couponError, "E_COUPON_NOT_APPLICABLE_FOR_USER", "User transaction is mapped as 2");
                softAssert.assertEquals(description, null);
                applicableOnList.add(couponCode);
                NuxPOJO nuxPojo = new NuxPOJO(customer_id, Integer.parseInt(cityId), applicableOnList);
                Processor verifyNuxProcessor = rngHelper.verifyNux("abcdef", "ios", nuxPojo);
                String couponError1 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponError");
                String description1 = verifyNuxProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].description");
                softAssert.assertEquals(couponError1, "E_COUPON_NOT_APPLICABLE_FOR_USER", "Customer restriction is mapped as 0");
                softAssert.assertEquals(description1, "Coupon Not Applicable for this user");
            }
        }
        softAssert.assertAll();


    }


    /*=============================================<<<<<>>>>>PreValidation-Coupon_Listing(validation based on cart_min value)<<<<<>>>>>=============================================*/

    @Test(dataProvider = "createCouponWithCartMinValueIsGreaterThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When Cart_min_Value is Greater than cart value passed as query param")
    public void getCouponUserCityOnCartMinValueGreaterThanCartValue(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        int cartMinValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.min_amount.cart");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "1", "1000");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        List<String> coupon_pre_validation_status_message = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.message");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                softAssert.assertEquals(coupon_pre_validation_status_message.get(i), "Applicable on orders above Rs." + cartMinValue + ".0");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsGreaterThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When cart value is  passed 1000f as query param")
    public void getCouponUserCityOnCartValueAsNumericWithSuffixF(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        int cartMinValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.min_amount.cart");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "1", "1000f");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        List<String> coupon_pre_validation_status_message = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.message");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                softAssert.assertEquals(coupon_pre_validation_status_message.get(i), "Applicable on orders above Rs." + cartMinValue + ".0");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsGreaterThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When cart value is  passed 1000d as query param")
    public void getCouponUserCityOnCartValueAsNumericWithSuffixD(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        int cartMinValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.min_amount.cart");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "1", "1000d");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        List<String> coupon_pre_validation_status_message = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.message");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                softAssert.assertEquals(coupon_pre_validation_status_message.get(i), "Applicable on orders above Rs." + cartMinValue + ".0");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsGreaterThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When cart value is  passed 1000&cart_value=300&cart_value=0.0 as query param")
    public void getCouponUserCityOnCartValueAsNumericWithSuffixMultipleQueryParamWithSameValue(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        int cartMinValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.min_amount.cart");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "1", "1000&cart_value=300&cart_value=0.0");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        List<String> coupon_pre_validation_status_message = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.message");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                softAssert.assertEquals(coupon_pre_validation_status_message.get(i), "Applicable on orders above Rs." + cartMinValue + ".0");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsGreaterThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When Cart Value is 9999.9999 passed as query param")
    public void getCouponUserCityOnCartValueAsDecimalTillFourthPlace(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "1", "9999.9999");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "APPLICABLE");
                softAssert.assertNotEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsGreaterThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When Cart Value is 0.0&cart_value=300&cart_value=0.0 passed as query param")
    public void getCouponUserCityOnCartValueWithSuffixMultipleData(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "1", "0.0&cart_value=300&cart_value=0.0");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "APPLICABLE");
                softAssert.assertNotEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsGreaterThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When CartValueGreaterThanCartMinValue")
    public void getCouponUserCityOnCartValueGreaterThanCartMinValue(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "1", "10000.01");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "APPLICABLE");
                softAssert.assertNotEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsGreaterThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When Cart Value is float")
    public void getCouponUserCityCartValueAsFloat(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        int cartMinValue = createResponse.ResponseValidator.GetNodeValueAsInt("$.data.min_amount.cart");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "1", "1000.00");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        List<String> coupon_pre_validation_status_message = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.message");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                softAssert.assertEquals(coupon_pre_validation_status_message.get(i), "Applicable on orders above Rs." + cartMinValue + ".0");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsLesserThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When Cart_min_Value is Lesser than cart value passed as query param")
    public void getCouponUserCityOnCartMinValueLesserThanCartValue(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1002", "1", "945");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "APPLICABLE");
                softAssert.assertNotEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsLesserThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When cart value is  passed as 0.0 ")
    public void getCouponUserCityOnCarValueAsZeroWithFloat(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1002", "1", "0.0");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "APPLICABLE");
                softAssert.assertNotEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsLesserThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When cart value is  passed as Null or Empty String")
    public void getCouponUserCityWithOutPassingCartValue(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1002", "1", "");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> coupon_pre_validation_status = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..coupon_pre_validation_status.status");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), coupon_pre_validation_status.get(i));
            if (codeDetails.containsKey(couponCode)) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), "APPLICABLE");
                softAssert.assertNotEquals(codeDetails.get(couponListingCode.get(i)), "NOT_APPLICABLE_CART_MIN");
                codeDetails.remove(couponListingCode.get(i));
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsLesserThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When Cart Value is alphanumeric")
    public void getCouponUserCityOnCartMinValueWithCartValueAsAlphaNumeric(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "7", "1000QWERTY");
        int status = processor.ResponseValidator.GetNodeValueAsInt("$.status");
        String error = processor.ResponseValidator.GetNodeValue("$.error");
        softAssert.assertEquals(status, 400, "Accepting alpha numeric value ");
        softAssert.assertEquals(error, "Bad Request", "Accepting alpha numeric value");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsLesserThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When Cart Value is Special Char")
    public void getCouponUserCityOnCartMinValueWithCartValueAsNumericSpecialChar(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "7", "!@#$%^&*");
        int status = processor.ResponseValidator.GetNodeValueAsInt("$.status");
        String error = processor.ResponseValidator.GetNodeValue("$.error");
        softAssert.assertEquals(status, 400, "Accepting alpha numeric value ");
        softAssert.assertEquals(error, "Bad Request", "Accepting alpha numeric value");
        softAssert.assertAll();
    }


    @Test(dataProvider = "createCouponWithCartMinValueIsLesserThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "When Cart Value is symbols")
    public void getCouponUserCityOnCartMinValueWithCartValueAsDifferentSymbols(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "7", "100Ω≈ç√∫˜µ≤≥÷");
        int status = processor.ResponseValidator.GetNodeValueAsInt("$.status");
        String error = processor.ResponseValidator.GetNodeValue("$.error");
        softAssert.assertEquals(status, 400, "Accepting alpha numeric value ");
        softAssert.assertEquals(error, "Bad Request", "Accepting alpha numeric value");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithCartMinValueIsGreaterThanCartValue", dataProviderClass = PreValidationCouponListingDP.class, description = "Cart_min_Value validation against Coupon code in DB")
    public void getCouponUserCityOnCartMinValueDBValidation(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        Processor processor = rngHelper.getCouponUserCityOnCartMinValue("1000", "1", "1000");
        HashMap<String, String> codeDetails = new HashMap<>();
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        List<String> cart_min_value = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..min_cart_amount");
        for (int i = 0; i < couponListingCode.size(); i++) {
            codeDetails.put(couponListingCode.get(i), cart_min_value.get(i));
            if (codeDetails.containsKey(couponListingCode.get(i))) {
                softAssert.assertEquals(codeDetails.get(couponListingCode.get(i)), rngHelper.getCartMinValueAgainstCouponCode(couponListingCode.get(i)));
            }
        }
        softAssert.assertAll();
    }


    /*=============================================<<<<<>>>>>Restaurant level visibility of coupon<<<<<>>>>>=============================================*/


    @Test(dataProvider = "createPrivateCouponWithRestaurantMapping", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "Wrong restaurant id is passed")
    public void getCouponUserCityWithWrongRestaurantMap(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", "1000");
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        softAssert.assertFalse(couponListingCode.contains(couponCode), "List contains the expected COUPON CODE");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPrivateCouponWithRestaurantMapping", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "correct restaurant id is passed")
    public void getCouponUserCityWithCorrectRestaurantMap(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", String.valueOf(restID));
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        softAssert.assertTrue(couponListingCode.contains(couponCode), "List dosen't contain the expected COUPON CODE");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPrivateCouponWithRestaurantMapping", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "wrong user id is passed")
    public void getCouponUserCityWithWrongUserIDAndCorrectRestaurantMap(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant("12345", "1", String.valueOf(restID));
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        softAssert.assertFalse(couponListingCode.contains(couponCode), "List contains the expected COUPON CODE");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPublicCouponWithWithRestaurantMapping", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "correct restaurant id is passed for Public type user")
    public void getCouponUserCityWithCorrectRestaurantMapAndPublicType(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", String.valueOf(restID));
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        softAssert.assertTrue(couponListingCode.contains(couponCode), "List dosen't contain the expected COUPON CODE");
        softAssert.assertAll();
    }
    @Test(dataProvider = "createPublicCouponWithWithRestaurantMapping", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "Wrong restaurant id is passed for Public type user")
    public void getCouponUserCityWithWrongRestaurantMapAndPublicType(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", "1000");
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        softAssert.assertFalse(couponListingCode.contains(couponCode), "List contains the expected COUPON CODE");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPublicCouponWithWithRestaurantMapping", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "Empty restaurant id is passed for Public type user")
    public void getCouponUserCityWithRestaurantMapAndEmptyRestIDIsPassedAndPublicType(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", "");
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        softAssert.assertFalse(couponListingCode.contains(couponCode), "List contains the expected COUPON CODE");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPublicCouponWithWithRestaurantMappingWithIsPrivate", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "correct restaurant id is passed for Public type user")
    public void getCouponUserCityWithCorrectRestaurantMapButIsPrivateAndPublicType(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", String.valueOf(restID));
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        softAssert.assertTrue(couponListingCode.contains(couponCode), "List dosen't contain the expected COUPON CODE");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createCouponWithoutCustomerRestrictionWithRestaurantMappingWithIsPrivate", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "Customer Restriction ")
    public void getCreateCouponWithoutCustomerRestrictionWithRestaurantMappingWithIsPrivate(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", "");
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        softAssert.assertFalse(couponListingCode.contains(couponCode), "List contains the expected COUPON CODE");
        softAssert.assertAll();
    }


    @Test(dataProvider = "createPublicCouponWithWithRestaurantMappingWithIsPrivate", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "Symbols are passed as restaurant id for Public type user")
    public void getCouponUserCityWithSymbolPassedAsRestID(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", "100Ω≈ç√∫˜µ≤≥÷");
        int status = processor.ResponseValidator.GetNodeValueAsInt("$.status");
        String error = processor.ResponseValidator.GetNodeValue("$.error");
        softAssert.assertEquals(status, 400, "Accepting alpha numeric value ");
        softAssert.assertEquals(error, "Bad Request", "Accepting alpha numeric value");
        softAssert.assertAll();
    }
    @Test(dataProvider = "createPublicCouponWithWithRestaurantMappingWithIsPrivate", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "alphanumeric are passed as restaurant id for Public type user")
    public void getCouponUserCityWithAlphaNumericPassedAsRestID(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", "100QWERTY");
        int status = processor.ResponseValidator.GetNodeValueAsInt("$.status");
        String error = processor.ResponseValidator.GetNodeValue("$.error");
        softAssert.assertEquals(status, 400, "Accepting alpha numeric value ");
        softAssert.assertEquals(error, "Bad Request", "Accepting alpha numeric value");
        softAssert.assertAll();
    }
    @Test(dataProvider = "createPublicCouponWithWithRestaurantMappingWithIsPrivate", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "Special Char are passed as restaurant id for Public type user")
    public void getCouponUserCityWithSpecialCharPassedAsRestID(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", "!@#$%^&*()");
        int status = processor.ResponseValidator.GetNodeValueAsInt("$.status");
        String error = processor.ResponseValidator.GetNodeValue("$.error");
        softAssert.assertEquals(status, 400, "Accepting alpha numeric value ");
        softAssert.assertEquals(error, "Bad Request", "Accepting alpha numeric value");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPublicCouponWithWithRestaurantMappingWithIsPrivate", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "Float Value is passed as restaurant id for Public type user")
    public void getCouponUserCityWithFloatPassedAsRestID(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", String.valueOf(restID)+".0");
        int status = processor.ResponseValidator.GetNodeValueAsInt("$.status");
        String error = processor.ResponseValidator.GetNodeValue("$.error");
        softAssert.assertEquals(status, 400, "Accepting alpha numeric value ");
        softAssert.assertEquals(error, "Bad Request", "Accepting alpha numeric value");
        softAssert.assertAll();
    }


    @Test(dataProvider = "createPrivateCouponWithRestaurantMapping", dataProviderClass = RestaurantCouponVisibilityDP.class, description = "Wrong restaurant id is passed")
    public void getCouponRestaurantMapInDBValidation(HashMap<String, Object> data) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        int userId = Utility.getRandom(1, 99900);
        int restID = Utility.getRandom(1, 99900);
        CreateCouponPOJO payload = (CreateCouponPOJO) data.get("createCouponPOJO");
        Processor createResponse = rngHelper.createCoupon(payload);
        String couponCode = createResponse.ResponseValidator.GetNodeValue("$.data.code");
        String couponID = String.valueOf(createResponse.ResponseValidator.GetNodeValueAsInt("$.data.id"));
        Processor userMapProcessor = rngHelper.couponUserMap(couponCode, String.valueOf(userId), couponID);
        Processor couponRestaurantMapProcessor = rngHelper.couponRestaurantMap(couponCode, String.valueOf(restID), couponID);
        String restaurantId = couponRestaurantMapProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restaurantId").replace("[","").replace("]","");
        Processor processor = rngHelper.getCouponUserCityOnRestaurant(String.valueOf(userId), "1", String.valueOf(restID));
        List<String> couponListingCode = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..code");
        softAssert.assertTrue(couponListingCode.contains(couponCode), "List doesn't contains the expected COUPON CODE");
        softAssert.assertEquals(restaurantId, rngHelper.getRestaurantID(couponCode),"restaurant mapped coupon is not present");
        softAssert.assertAll();
    }
}