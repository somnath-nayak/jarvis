package com.swiggy.api.sf.checkout.pojo;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartItem;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class OrderEditPOJO {

	@JsonProperty("order_id")
	private Long orderId;
	@JsonProperty("couponCode")
	private String couponCode;
	@JsonProperty("cartItems")
	private List<CartItem> cartItems = null;
	@JsonProperty("initiation_source")
	private Integer initiationSource;
	@JsonProperty("restaurantId")
	private Integer restaurantId;

	@JsonProperty("order_id")
	public Long getOrderId() {
		return orderId;
	}

	@JsonProperty("order_id")
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public OrderEditPOJO withOrderId(Long orderId) {
		this.orderId = orderId;
		return this;
	}

	@JsonProperty("couponCode")
	public String getCouponCode() {
		return couponCode;
	}

	@JsonProperty("couponCode")
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public OrderEditPOJO withCouponCode(String couponCode) {
		this.couponCode = couponCode;
		return this;
	}

	@JsonProperty("cartItems")
	public List<CartItem> getCartItems() {
		return cartItems;
	}

	@JsonProperty("cartItems")
	public void setCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
	}

	public OrderEditPOJO withCartItems(List<CartItem> cartItems) {
		this.cartItems = cartItems;
		return this;
	}

	@JsonProperty("initiation_source")
	public Integer getInitiationSource() {
		return initiationSource;
	}

	@JsonProperty("initiation_source")
	public void setInitiationSource(Integer initiationSource) {
		this.initiationSource = initiationSource;
	}

	public OrderEditPOJO withInitiationSource(Integer initiationSource) {
		this.initiationSource = initiationSource;
		return this;
	}

	@JsonProperty("restaurantId")
	public Integer getRestaurantId() {
		return restaurantId;
	}

	@JsonProperty("restaurantId")
	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}

	public OrderEditPOJO withRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
		return this;
	}

	public OrderEditPOJO setData(Integer restId, Integer quantity, String Menu_Item_Id, Long Order_Id , Integer Initiation_source) {
		ArrayList al = new ArrayList();
		CartItem cr = new CartItem().setData(Menu_Item_Id, quantity);
		al.add(cr);
		this.withCartItems(al).withRestaurantId(restId).withOrderId(Order_Id).withInitiationSource(Initiation_source);
		return this;

	}
}