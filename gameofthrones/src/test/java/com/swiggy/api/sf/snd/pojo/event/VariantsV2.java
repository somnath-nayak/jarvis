package com.swiggy.api.sf.snd.pojo.event;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class VariantsV2 {

    @JsonProperty("variant_groups")
    private List<VariantGroup> variantGroups = null;
    @JsonProperty("pricing_models")
    private List<PricingModel> pricingModels = null;

    public List<VariantGroup> getVariantGroups() {
        return variantGroups;
    }

    public void setVariantGroups(List<VariantGroup> variantGroups) {
        this.variantGroups = variantGroups;
    }

    public List<PricingModel> getPricingModels() {
        return pricingModels;
    }

    public void setPricingModels(List<PricingModel> pricingModels) {
        this.pricingModels = pricingModels;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("variant_groups", variantGroups).append("pricing_models", pricingModels).toString();
    }

}