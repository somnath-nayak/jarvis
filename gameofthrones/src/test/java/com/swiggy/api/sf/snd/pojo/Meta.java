package com.swiggy.api.sf.snd.pojo;


public class Meta {

        private String change_type;

        private String from;


    public String getChange_type() {
        return change_type;
    }

    public void setChange_type(String change_type) {
        this.change_type = change_type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }


    public Meta build(){
        setDefaultValues();
        return this;
    }
    public void setDefaultValues(){

        if(this.getChange_type()==null){
            this.setChange_type("UPDATE");
        }
        if(this.getFrom()==null){
            this.setFrom("CMS");
        }

    }

    @Override
    public String toString() {
        return "{" +
                "change_type='" + change_type + '\'' +
                ", from='" + from + '\'' +
                '}';
    }

}

