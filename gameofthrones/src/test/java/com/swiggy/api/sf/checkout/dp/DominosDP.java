package com.swiggy.api.sf.checkout.dp;

import com.swiggy.api.sf.checkout.pojo.*;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DominosDP {

    @DataProvider(name="userSingleOrder")
    public Object[][] userSingleOrder() throws IOException {
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "677829", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(273)
                .Address(782255)
                .paymentMethod("Cash")
                .orderComments("Test-Order")
                .build();
        return new Object[][]{
                {createOrder}
        };
    }
   
    @DataProvider(name="SingleOrder")
    public Object[][] SingleOrderPlace() throws IOException {
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "677829", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(273)
                .Address(782255)
                .paymentMethod("Cash")
                .orderComments("Test-Order")
                .build();
        return new Object[][]{
                {createOrder}
        };
    }

}
