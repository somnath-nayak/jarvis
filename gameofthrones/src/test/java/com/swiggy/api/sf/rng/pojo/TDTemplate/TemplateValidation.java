package com.swiggy.api.sf.rng.pojo.TDTemplate;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "templateIds"
})
public class TemplateValidation {

    @JsonProperty("templateIds")
    private List<Integer> templateIds = null;

    @JsonProperty("templateIds")
    public List<Integer> getTemplateIds() {
        return templateIds;
    }

    @JsonProperty("templateIds")
    public void setTemplateIds(List<Integer> templateIds) {
        this.templateIds = templateIds;
    }

}


