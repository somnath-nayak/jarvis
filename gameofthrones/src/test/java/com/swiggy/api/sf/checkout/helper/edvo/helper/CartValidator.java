package com.swiggy.api.sf.checkout.helper.edvo.helper;

import com.swiggy.api.sf.checkout.constants.EDVOConstants;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Group;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Item;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.MealItem;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CartValidator extends EDVOCartHelper {
    public void smokeCheck(int statusCode, String statusMessage, Processor processor) {
        SoftAssert softAssert = new SoftAssert();
        Reporter.log("Expected StatusCode = '" + statusCode + "' <==> Actual StatusCode = '" + processor.ResponseValidator.GetNodeValueAsInt("statusCode") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode, "StatusCode mismatch");
        Reporter.log("Expected StatusMessage = '" + statusMessage + "' <==> Actual StatusMessage = '" + processor.ResponseValidator.GetNodeValue("statusMessage") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage, "StatusMessage mismatch");
        softAssert.assertAll();
    }

    /**
     * Verify that items are added as it is to the cart
     *
     * @param processor
     * @param cart
     */
    public void checkItemsAddedCorrectly(Processor processor, Cart cart) {
        CartV2Response cartV2Response = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);

        // Check Cart Menu Items Are Added With Exact Quantity To Cart Successfully
        if (cart.getCartItems()!=null && cart.getCartItems().size() > 0) {
            for (int i = 0; i < cart.getCartItems().size(); i++) {
                // Verify Item Id is added to cart
                Assert.assertEquals(cartV2Response.getData().getCartMenuItems().get(i).getMenuItemId().toString(),
                        cart.getCartItems().get(i).getMenuItemId(), "Cart Menu Items || Item Id = "
                                + cart.getCartItems().get(i).getMenuItemId() + " is missing(not added) in cart..!!!");
                Reporter.log("Cart Menu Item || Item Id = " + cart.getCartItems().get(i).getMenuItemId()
                        + " is added successfully to cart", true);

                // Verify the Quantity if added item
                Assert.assertEquals(cartV2Response.getData().getCartMenuItems().get(i).getQuantity(),
                        cart.getCartItems().get(i).getQuantity(), "Quantity is verified for Cart Menu Item Id = "
                                + cart.getCartItems().get(i).getMenuItemId());
                Reporter.log("Cart Menu Item || Quantity is verified for Item Id = " + cart.getCartItems().get(i).getMenuItemId(), true);
            }
        }

        // Check Meal Items Are Added With Exact Quantity To Cart Successfully
        if (cart.getMealItems()!=null && cart.getMealItems().size() > 0) {
            for (int i = 0; i < cart.getMealItems().size(); i++) {
                if (cart.getMealItems().get(i).getGroups().size() > 0) {
                    for (int j = 0; j < cart.getMealItems().get(i).getGroups().size(); j++) {
                        // Verify group id added to respective meal in cart successfully
                        Assert.assertEquals(cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getGroupId(),
                                cart.getMealItems().get(i).getGroups().get(j).getGroupId(),
                                "Group Id = " + cart.getMealItems().get(i).getGroups().get(j).getGroupId()
                                        + " not found in Meal Id = " + cart.getMealItems().get(i).getMealId() + "..!!!");
                        Reporter.log("Group Id = " + cart.getMealItems().get(i).getGroups().get(j).getGroupId()
                                        + " verified for meal id = " + cart.getMealItems().get(i).getMealId() + "..!!!",
                                true);
                        if (cart.getMealItems().get(i).getGroups().get(j).getItems().size() > 0) {
                            for (int k = 0; k < cart.getMealItems().get(i).getGroups().get(j).getItems().size(); k++) {
                                // Verify if items are added to respective meal group
                                Assert.assertEquals(cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getMenuItemId()
                                        , cart.getMealItems().get(i).getGroups().get(j).getItems().get(k).getMenuItemId(),
                                        "Item Id = " + cart.getMealItems().get(i).getGroups().get(j).getItems().get(k).getMenuItemId()
                                                + " missing for group id = " + cart.getMealItems().get(i).getGroups().get(j).getGroupId()
                                                + " in meal id = " + cart.getMealItems().get(i).getMealId() + "..!!!");
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Verify the cart menu item amount in the cart @Generic level
     *
     * @param processor
     * @param cart
     */
    public HashMap<String, Double> checkCartMenuItemAmount(Processor processor, Cart cart) {
        CartV2Response cartV2Response = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        HashMap<String, Double> amount = new HashMap<>();
        amount.put("subtotal", 0.00);
        amount.put("finalPrice", 0.00);
        amount.put("subTotalTD", 0.00);
        if (cart.getCartItems()!=null && cart.getCartItems().size() > 0) {
            for (int i = 0; i < cart.getCartItems().size(); i++) {
                amount.put("subtotal", amount.get("subtotal") + cartV2Response.getData().getCartMenuItems().get(i).getSubtotal());
                amount.put("finalPrice", amount.get("finalPrice") + cartV2Response.getData().getCartMenuItems().get(i).getFinalPrice());
                amount.put("subTotalTD", amount.get("subTotalTD") + cartV2Response.getData().getCartMenuItems().get(i).getSubtotalTradeDiscount());

                Double subtotal = cartV2Response.getData().getCartMenuItems().get(i).getSubtotal();
                Double basePrice = cartV2Response.getData().getCartMenuItems().get(i).getBasePrice();
                Double addonsPrice = cartV2Response.getData().getCartMenuItems().get(i).getAddonsPrice();
                Double variantsPrice = cartV2Response.getData().getCartMenuItems().get(i).getVariantsPrice();
                Double subtotalTradeDiscount = cartV2Response.getData().getCartMenuItems().get(i).getSubtotalTradeDiscount();
                Double finalPrice = cartV2Response.getData().getCartMenuItems().get(i).getFinalPrice();
                Integer quantity = cartV2Response.getData().getCartMenuItems().get(i).getQuantity();
                Integer itemId = cartV2Response.getData().getCartMenuItems().get(i).getMenuItemId();

                //Initialize addonsPrice with 0.0 if found null in response
                if(addonsPrice == null) addonsPrice = 0.0;
                //Initialize variantsPrice with 0.0 if found null in response
                if(variantsPrice == null) variantsPrice = 0.0;

                //Verify if subtotal = (basePrice + addonPrice + variantPrice)* quantity || @CartMenuItemLevel
                Assert.assertEquals(subtotal.doubleValue(),
                        (basePrice.doubleValue() + addonsPrice.doubleValue() + variantsPrice.doubleValue()) * quantity.intValue(),
                        "Validation Failed for 'subtotal = (basePrice + addonPrice + variantPrice)* quantity' " +
                                "@CartMenuItem Level for Item ID = " + itemId.intValue());

                //Verify if finalPrice = subtotal -subtotalTradeDiscount || CartMenuItemLevel
                Assert.assertEquals(finalPrice.doubleValue(), subtotal.doubleValue() - subtotalTradeDiscount.doubleValue(),
                        "Validation Failed for 'finalPrice = subtotal -subtotalTradeDiscount' " +
                                "@CartMenuItem Level for Item ID = " + itemId.intValue());

                //Domino's Specific Check
                if (cartV2Response.getData().getRestaurantDetails().getName().contains("Domino")) {
                    //Verify packaging charge should be zero for Domino's
                    Assert.assertEquals(cartV2Response.getData().getCartMenuItems().get(i).getPackingCharge().intValue(),
                            0, "Packaging charge should be zero for Domino's for Item Id = " + itemId);
                }
            }
            //Domino's Specific Check
            if (cartV2Response.getData().getRestaurantDetails().getName().contains("Domino")) {
                //Verify delivery charge should be zero for Domino's
                Assert.assertEquals(cartV2Response.getData().getDeliveryCharges().intValue(),
                        0, "Delivery charge should be zero for Domino's at Cart level");
                for (int i = 0; i < cartV2Response.getData().getRenderingDetails().size(); i++) {
                    if (cartV2Response.getData().getRenderingDetails().get(i).getKey().equals("delivery_charges")) {
                        //Verify delivery charge should be zero for Domino's in Rendering Details
                        Assert.assertEquals(cartV2Response.getData().getRenderingDetails().get(i).getValue(),
                                "0.00", "Delivery charge should be zero for Domino's in Rendering Details");
                    }
                }
            }
        }
        return amount;
    }

    /**
     * Verify the meal item amount in the cart @Generic level
     *
     * @param processor
     * @param cart
     */
    public HashMap<String, Double> checkMealItemAmount(Processor processor, Cart cart) {
        CartV2Response cartV2Response = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        HashMap<String, Double> amount = new HashMap<>();
        amount.put("subtotal", 0.00);
        amount.put("finalPrice", 0.00);
        amount.put("subTotalTD", 0.00);
        Double subtotalSum = 0.0;
        Double tradeDiscountSum = 0.0;

        if (cart.getMealItems().size() > 0) {
            for (int i = 0; i < cart.getMealItems().size(); i++) {
                amount.put("subtotal", amount.get("subtotal") + cartV2Response.getData().getMealItems().get(i).getSubtotal());
                amount.put("finalPrice", amount.get("finalPrice") + cartV2Response.getData().getMealItems().get(i).getFinalPrice());
                amount.put("subTotalTD", amount.get("subTotalTD") + cartV2Response.getData().getMealItems().get(i).getSubtotalTradeDiscount());

                Double subtotalMeal = cartV2Response.getData().getMealItems().get(i).getSubtotal().doubleValue();
                Double subtotalTradeDiscountMeal = cartV2Response.getData().getMealItems().get(i).getSubtotalTradeDiscount().doubleValue();
                Double finalPriceMeal = cartV2Response.getData().getMealItems().get(i).getFinalPrice();
                Double totalMeal = cartV2Response.getData().getMealItems().get(i).getTotal();
                Double packagingChargeMeal = cartV2Response.getData().getMealItems().get(i).getPackingCharge();
                Integer quantityMeal = cartV2Response.getData().getMealItems().get(i).getQuantity();
                Integer mealId = cartV2Response.getData().getMealItems().get(i).getMealId();



                //Verify if finalprice = subtotal - subtotaltradediscount @MealLevel
                Assert.assertEquals(finalPriceMeal.doubleValue(),
                        subtotalMeal.doubleValue() - subtotalTradeDiscountMeal.doubleValue(),
                        "Validation failed for || finalprice = subtotal - subtotaltradediscount @MealLevel for meal id = " + mealId);

                //Verify if total = subtotal + packaging charge @MealLevel
                Assert.assertEquals(totalMeal.doubleValue(),
                        subtotalMeal.doubleValue() + packagingChargeMeal.doubleValue(),
                        "Validation failed for || total = subtotal + packaging charge @MealLevelfor meal id = " + mealId);

                //Domino's Specific Check
                if (cartV2Response.getData().getRestaurantDetails().getName().contains("Domino")) {
                    //Verify packaging charge should be zero for Domino's
                    Assert.assertEquals(cartV2Response.getData().getMealItems().get(i).getPackingCharge().intValue(), 0,
                            "Packaging charge should be zero for Domino's");
                }


                if (cart.getMealItems().get(i).getGroups().size() > 0) {
                    //Meal Group Level Check
                    for (int j = 0; j < cart.getMealItems().get(i).getGroups().size(); j++) {
                        if (cart.getMealItems().get(i).getGroups().get(j).getItems().size() > 0) {
                            //Meal Items Level Check
                            for (int k = 0; k < cart.getMealItems().get(i).getGroups().get(j).getItems().size(); k++) {

                                Double subtotal = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getSubtotal().doubleValue();
                                Double basePrice = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getBasePrice();
                                Double addonsPrice = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getAddonsPrice();
                                Double variantsPrice = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getVariantsPrice();
                                Double subtotalTradeDiscount = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getSubtotalTradeDiscount().doubleValue();
                                Double finalPrice = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getFinalPrice();
                                Integer quantity = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getQuantity();
                                Integer mealQuantity = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getMealQuantity();
                                Integer itemId = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getMenuItemId();
                                Double packagingCharge = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getPackingCharge();

                                subtotalSum = subtotalSum + subtotal;
                                tradeDiscountSum = tradeDiscountSum + subtotalTradeDiscount;

                                //Initialize addonsPrice with 0.0 if found null in response
                                if(addonsPrice == null) addonsPrice = 0.0;
                                //Initialize variantsPrice with 0.0 if found null in response
                                if(variantsPrice == null) variantsPrice = 0.0;

                                //Verify if subtotal = (basePrice + addonPrice + variantPrice)* quantity * mealQuantity|| MealItemLevel
                                Assert.assertEquals(subtotal.doubleValue(),
                                        (basePrice.doubleValue() + addonsPrice.doubleValue() + variantsPrice.doubleValue())
                                                * quantity.intValue() * mealQuantity,
                                        "Validation Failed for 'subtotal = (basePrice + addonPrice + variantPrice)* quantity' " +
                                                "@CartMenuItem Level for Item ID = " + itemId.intValue());

                                //Verify if finalPrice = subtotal -subtotalTradeDiscount || MealItemLevel
                                Assert.assertEquals(Math.round(finalPrice.doubleValue()),
                                        (Math.round(subtotal.doubleValue()) - Math.round(subtotalTradeDiscount.doubleValue())),
                                        "Validation Failed for 'finalPrice = subtotal -subtotalTradeDiscount' " +
                                                "@CartMenuItem Level for Item ID = " + itemId.intValue());

                                //Domino's Specific Check
                                if (cartV2Response.getData().getRestaurantDetails().getName().contains("Domino")) {
                                    //Verify packaging charge should be zero for Domino's
                                    Assert.assertEquals(packagingCharge.doubleValue(),0.0,
                                            "Packaging charge should be zero for Domino's at Cart Level");
                                }
                            }
                        }
                    }

                }
                //Verify that subtotal = sum(@meal item subtotal) @MealLevel
                Assert.assertEquals(subtotalSum, subtotalMeal,
                        "Verification failed for subtotal = sum(@meal item subtotal) @MealLevel");

                //Verify that subtotaTD = sum(item TD) * mealquantity * quantity @MealLevel
                Assert.assertEquals(Double.valueOf(Math.round(tradeDiscountSum * quantityMeal)), Double.valueOf(Math.round(subtotalTradeDiscountMeal * quantityMeal)),
                        "Verification failed for subtotaTD = sum(item TD) * mealquantity * quantity @MealLevel");

            }
            return amount;
        }

        //Domino's Specific Check
        if (cartV2Response.getData().getRestaurantDetails().getName().contains("Domino")) {
            //Verify delivery charge should be zero for Domino's at Cart Level
            Assert.assertEquals(cartV2Response.getData().getDeliveryCharges().intValue(),
                    0, "Delivery charge should be zero for Domino's at Cart Level");
            for (int i = 0; i < cartV2Response.getData().getRenderingDetails().size(); i++) {
                if (cartV2Response.getData().getRenderingDetails().get(i).getKey().equals("delivery_charges")) {
                    //Verify delivery charge should be zero for Domino's in Rendering Details
                    Assert.assertEquals(cartV2Response.getData().getRenderingDetails().get(i).getValue(),
                            "0.00", "Delivery charge should be zero for Domino's in Rendering Details");
                }
            }
        }
        return amount;
    }

    /**
     * Verify the cart amount in the cart @Generic level
     *
     * @param processor
     * @param cart
     */
    public void checkCartAmount(Processor processor, Cart cart){
        HashMap<String, Double> normalCartAmount = new HashMap<>();
        HashMap<String, Double> mealAmount = new HashMap<>();
        normalCartAmount.put("finalPrice", 0.00);
        normalCartAmount.put("subtotal", 0.00);
        normalCartAmount.put("subTotalTD", 0.00);
        mealAmount.put("finalPrice", 0.00);
        mealAmount.put("subtotal", 0.00);
        mealAmount.put("subTotalTD", 0.00);

        //Check amount @Cart Menu Item
        if(cart.getCartItems().size()>0){
            Reporter.log("Cart Menu Item Amount Check Started...", true);
            normalCartAmount = checkCartMenuItemAmount(processor, cart);
            Reporter.log("Cart Menu Item Amount Check Success...", true);
        }

        //Check amount @Meal Items level
        if(cart.getMealItems().size()>0){
            Reporter.log("Meal Item Amount Check Started...", true);
            mealAmount= checkMealItemAmount(processor, cart);
            Reporter.log("Meal Item Amount Check Success...", true);
        }

        CartV2Response cartV2Response = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(),
                CartV2Response.class);

        // Verify CartSubtotalWithoutPacking = sum(Subtotal) at Cart Level
        Assert.assertEquals(cartV2Response.getData().getCartSubtotalWithoutPacking().doubleValue(),
                (normalCartAmount.get("subtotal").doubleValue() + mealAmount.get("subtotal").doubleValue())/100);

        // Verify TradeDiscountTotal = sum(subtotalTD) at Cart Level
        Assert.assertEquals(cartV2Response.getData().getTradeDiscountTotal(),
                (normalCartAmount.get("subTotalTD").doubleValue() + mealAmount.get("subTotalTD").doubleValue())/100);
    }

    public void checkMealTradeDiscount(Processor processor, Cart cart, String[] mealTD, String rewardType, String tdType){
        CartV2Response cartV2Response = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        Integer discountSum = 0;
        Integer quantity = 0;
        for(String discount: mealTD){
            for(int i=0; i<cart.getMealItems().size(); i++) {
                quantity = cart.getMealItems().get(i).getQuantity();
                discountSum = discountSum + Integer.valueOf(discount)*quantity;
            }

        }

        // Verify Discount Total at Cart Level
        Assert.assertEquals(cartV2Response.getData().getTradeDiscountTotal(),
                discountSum,
                "Trade Discount Total at cart level is not equal to sum of subtotal trade discount expected on meal level");

        for(int i=0; i<cart.getMealItems().size();i++){
            Double td = 0.0;

            // Verify Subtotal Trade Discount Total at Meal Level
            Assert.assertEquals(cartV2Response.getData().getMealItems().get(i).getSubtotalTradeDiscount().intValue() / 100,
                    Integer.valueOf(mealTD[i]).intValue() * cart.getMealItems().get(i).getQuantity().intValue(),
                    "Actual TD and Expected TD are different for meal id = "
                            + cartV2Response.getData().getMealItems().get(i).getMealId());

            // Verify Reward Type at Meal Level
            Assert.assertEquals(cartV2Response.getData().getMealItems().get(i).getRewardType(),
                    rewardType, "Reward type mismatch for meal id = "
                            + cartV2Response.getData().getMealItems().get(i).getRewardType());

            //Meal Item level check
            if(cart.getMealItems().size()>0){
                for(int j=0; j<cart.getMealItems().get(i).getGroups().size();j++) {
                    List<Double> itemTD = new ArrayList<>();
                    for (int k = 0; k < cart.getMealItems().get(i).getGroups().get(j).getItems().size(); k++) {

                        // Verify Reward Type at Meal Item Level
                        Assert.assertEquals(cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getRewardType(),
                                rewardType, "Reward type mismatch for meal id = "
                                        + cartV2Response.getData().getMealItems().get(i).getMealId());

                        itemTD.add(k, cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getSubtotalTradeDiscount().doubleValue());
                        td = td + itemTD.get(k);
//                        double itemRatio = cartV2Response.getData().getMealItems().get(i).getGroups().get(j).getItems().get(k).getTotal().i
                        // Verify Subtotal Trade Discount at Meal Item Level
//                        Assert.assertEquals(itemTD.get(k).intValue(),Integer.valueOf(mealTD[i]).intValue()*100);
                    }
                }
            }
        }
    }

    //Validates Cart Response for Meals
    public void validateMealCart(CartV2Response cartV2Response, String tdType){
        List<MealItem> mealItems = cartV2Response.getData().getMealItems();
        Integer actual = 0;
        Integer expected = 1;
        switch (tdType) {
            case EDVOConstants.MEAL_TD_TYPE_01: // (buy x and y items, at Price P)
                for (MealItem mealItem : mealItems){
                    actual = mealItem.getSubtotalTradeDiscount();
                    expected = mealItem.getSubtotal() - (EDVOConstants.REWARD_VALUE_FINAL_PRICE * 100 * mealItem.getQuantity());
                    expected = expected > 0 ? expected : mealItem.getSubtotal();
                    Assert.assertEquals(actual, expected, "Trade Discount mismatch at Meal Level for meal id = '" + mealItem.getMealId() + "'");
                    Reporter.log("[Meal Discount Check Success at Meal Level for meal id = '" + mealItem.getMealId() + "']", true);
                }
                break;
            case EDVOConstants.MEAL_TD_TYPE_02: // (buy x,y with %P off)
                for (MealItem mealItem : mealItems){
                    actual = mealItem.getSubtotalTradeDiscount();
                    expected = mealItem.getSubtotal() - mealItem.getSubtotal() * mealItem.getQuantity() * (EDVOConstants.REWARD_VALUE_PERCENTAGE)/100;
                    expected = expected > 0 ? expected : mealItem.getSubtotal();
                    Assert.assertEquals(actual, expected, "Trade Discount mismatch at Meal Level for meal id = '" + mealItem.getMealId() + "'");
                    Reporter.log("[Meal Discount Check Success at Meal Level for meal id = '" + mealItem.getMealId() + "']", true);
                }
                break;
            case EDVOConstants.MEAL_TD_TYPE_03: // (buy x,y with  flat P off)
                for (MealItem mealItem : mealItems){
                    actual = mealItem.getSubtotalTradeDiscount();
                    expected = EDVOConstants.REWARD_VALUE_FLAT * 100;
                    expected = mealItem.getSubtotal() - expected > 0 ? expected : mealItem.getSubtotal();
                    Assert.assertEquals(actual, expected, "Trade Discount mismatch at Meal Level for meal id = '" + mealItem.getMealId() + "'");
                    Reporter.log("[Meal Discount Check Success at Meal Level for meal id = '" + mealItem.getMealId() + "']", true);
                }
                break;
            case EDVOConstants.MEAL_TD_TYPE_04: // (buy x and y items at Price P each)
                for (MealItem mealItem : mealItems){
                    actual = mealItem.getSubtotalTradeDiscount();
                    expected = 0;
                    for (Group group : mealItem.getGroups()) {
                        expected = expected + EDVOConstants.REWARD_VALUE_FINAL_PRICE * 100 * group.getItems().size();
                    }
                    expected = mealItem.getSubtotal() - expected;
                    expected = expected > 0 ? expected : mealItem.getSubtotal();
                    Assert.assertEquals(actual, expected, "Trade Discount mismatch at Meal Level for meal id = '" + mealItem.getMealId() + "'");
                    Reporter.log("[Meal Discount Check Success at Meal Level for meal id = '" + mealItem.getMealId() + "']", true);
                }
                break;
            case EDVOConstants.MEAL_TD_TYPE_05: // (buy x and y items with %P discount each)
                for (MealItem mealItem : mealItems){
                    actual = mealItem.getSubtotalTradeDiscount();
                    expected = mealItem.getSubtotal() - mealItem.getSubtotal() * (EDVOConstants.REWARD_VALUE_PERCENTAGE)/100;
                    expected = expected > 0 ? expected : mealItem.getSubtotal();
                    Assert.assertEquals(actual, expected, "Trade Discount mismatch at Meal Level for meal id = '" + mealItem.getMealId() + "'");
                    Reporter.log("[Meal Discount Check Success at Meal Level for meal id = '" + mealItem.getMealId() + "']", true);
                    for (Group group : mealItem.getGroups()){
                        for (Item item : group.getItems()){
                            actual = item.getSubtotalTradeDiscount();
                            expected = (item.getSubtotal() * (EDVOConstants.REWARD_VALUE_PERCENTAGE) / 100);
                            Assert.assertEquals(actual, expected, "Trade Discount mismatch at Item Level for meal id = '" + mealItem.getMealId() + "'");
                            Reporter.log("[Meal Discount Check Success at Item Level for meal id = '" + mealItem.getMealId() + "']", true);
                        }
                    }
                }
                break;
            case EDVOConstants.MEAL_TD_TYPE_06: // (buy x and y items with flat P discount each)
                for (MealItem mealItem : mealItems) {
                    actual = mealItem.getSubtotalTradeDiscount();
                    expected = EDVOConstants.REWARD_VALUE_FLAT * 100;
                    int count = 0;
                    for (Group group : mealItem.getGroups()) {
                        for (Item item : group.getItems()) {
                            expected = count > 0 ?
                                    (item.getSubtotal() > (EDVOConstants.REWARD_VALUE_FLAT * 100) ? expected + expected : expected + item.getSubtotal())
                                    : expected;
                            count++;
                        }
                    }
                    Assert.assertEquals(actual, expected, "Trade Discount mismatch at Item Level for meal id = '" + mealItem.getMealId() + "'");
                    Reporter.log("[Meal Discount Check Success at Item Level for meal id = '" + mealItem.getMealId() + "']", true);
                }
                break;
            case EDVOConstants.MEAL_TD_TYPE_07: // (buy x,y , get y item free(y is min among all) BOGO
                Integer minSubtotal = 0;
                for (MealItem mealItem : mealItems){
                    actual = mealItem.getSubtotalTradeDiscount();
                    for (Group group : mealItem.getGroups()){
                        for (Item item : group.getItems()){
                            minSubtotal = minSubtotal > 0 ?
                                    (minSubtotal > item.getSubtotal() ? item.getSubtotal() : minSubtotal)
                                    : item.getSubtotal();
                        }
                    }
                    Assert.assertEquals(actual, minSubtotal, "Trade Discount mismatch at Meal Level for meal id = '" + mealItem.getMealId() + "'");
                    Reporter.log("[Meal Discount Check Success at Meal Level for meal id = '" + mealItem.getMealId() + "']", true);
                }
                break;
            case EDVOConstants.MEAL_TD_TYPE_08: // (buy x, y and z, get x at %P1, y at %P2, z at %P3)
                for (MealItem mealItem : mealItems) {
                    for (Group group : mealItem.getGroups()) {
                        for (Item item : group.getItems()) {
                            actual = item.getSubtotalTradeDiscount();
                            expected = (item.getSubtotal() * EDVOConstants.REWARD_VALUE_PERCENTAGE) / 100;
                            Assert.assertEquals(actual, expected, "Trade Discount mismatch at Item Level for meal id = '" + mealItem.getMealId() + "'");
                            Reporter.log("[Meal Discount Check Success at Item Level for meal id = '" + mealItem.getMealId() + "']", true);
                        }
                    }
                }
                break;
            default:
                Assert.fail("[TEST CASE OUT OF SCOPE...!!]");
        }
    }
}