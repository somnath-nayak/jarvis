package com.swiggy.api.sf.rng.pojo.MultiTD.CreateSuperCampaignV2;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.MultiTD
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.redis.S;
import com.swiggy.api.sf.rng.helper.SuperMultiTDHelper;
import org.apache.commons.lang.time.DateUtils;
import org.apache.xpath.operations.Bool;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "benefits",
        "campaign_type",
        "commissionOnFullBill",
        "createdAt",
        "createdBy",
        "description",
        "discountCap",
        "dormant_user_type",
        "enabled",
        "firstOrderRestriction",
        "header",
        "namespace",
        "operationType",
        "restaurantFirstOrder",
        "restaurant_hit",
        "shortDescription",
        "surgeApplicable",
        "swiggy_hit",
        "taxesOnDiscountedBill",
        "timeSlotRestriction",
        "updatedBy",
        "userRestriction",
        "valid_from",
        "valid_till"
})
public class CreateSuperCampaignV2 {

    SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();

    @JsonProperty("benefits")
    private List<Benefit> benefits = null;
    @JsonProperty("campaign_type")
    private String campaign_type;
    @JsonProperty("commissionOnFullBill")
    private Boolean commissionOnFullBill;
    @JsonProperty("createdAt")
    private Long createdAt;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("description")
    private String description;
    @JsonProperty("discountCap")
    private Integer discountCap;
    @JsonProperty("dormant_user_type")
    private String dormant_user_type;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("firstOrderRestriction")
    private Boolean firstOrderRestriction;
    @JsonProperty("header")
    private String header;
    @JsonProperty("namespace")
    private String namespace;
    @JsonProperty("operationType")
    private String operationType;
    @JsonProperty("restaurantFirstOrder")
    private Boolean restaurantFirstOrder;
    @JsonProperty("restaurant_hit")
    private Integer restaurant_hit;
    @JsonProperty("shortDescription")
    private String shortDescription;
    @JsonProperty("surgeApplicable")
    private Boolean surgeApplicable;
    @JsonProperty("swiggy_hit")
    private Integer swiggy_hit;
    @JsonProperty("taxesOnDiscountedBill")
    private Boolean taxesOnDiscountedBill;
    @JsonProperty("timeSlotRestriction")
    private Boolean timeSlotRestriction;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("userRestriction")
    private Boolean userRestriction;
    @JsonProperty("valid_from")
    private Long valid_from;
    @JsonProperty("valid_till")
    private Long valid_till;
    @JsonProperty("copyOverridden")
    private Boolean copyOverridden = false;
    @JsonProperty("minCartAmount")
    private String minCartAmount = "99";

    @JsonProperty("benefits")
    public List<Benefit> getBenefits() {
        return benefits;
    }

    @JsonProperty("benefits")
    public void setBenefits(List<Benefit> benefits) {
        this.benefits = benefits;
    }

    @JsonProperty("campaign_type")
    public String getCampaign_type() {
        return campaign_type;
    }

    @JsonProperty("campaign_type")
    public void setCampaign_type(String campaign_type) {
        this.campaign_type = campaign_type;
    }

    @JsonProperty("commissionOnFullBill")
    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    @JsonProperty("commissionOnFullBill")
    public void setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
    }

    @JsonProperty("createdAt")
    public Long getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("createdAt")
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("discountCap")
    public Integer getDiscountCap() {
        return discountCap;
    }

    @JsonProperty("discountCap")
    public void setDiscountCap(Integer discountCap) {
        this.discountCap = discountCap;
    }

    @JsonProperty("dormant_user_type")
    public String getDormant_user_type() {
        return dormant_user_type;
    }

    @JsonProperty("dormant_user_type")
    public void setDormant_user_type(String dormant_user_type) {
        this.dormant_user_type = dormant_user_type;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("firstOrderRestriction")
    public Boolean getFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    @JsonProperty("firstOrderRestriction")
    public void setFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
    }

    @JsonProperty("header")
    public String getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(String header) {
        this.header = header;
    }

    @JsonProperty("namespace")
    public String getNamespace() {
        return namespace;
    }

    @JsonProperty("namespace")
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    @JsonProperty("operationType")
    public String getOperationType() {
        return operationType;
    }

    @JsonProperty("operationType")
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    @JsonProperty("restaurantFirstOrder")
    public Boolean getRestaurantFirstOrder() {
        return restaurantFirstOrder;
    }

    @JsonProperty("restaurantFirstOrder")
    public void setRestaurantFirstOrder(Boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
    }

    @JsonProperty("restaurant_hit")
    public Integer getRestaurant_hit() {
        return restaurant_hit;
    }

    @JsonProperty("restaurant_hit")
    public void setRestaurant_hit(Integer restaurant_hit) {
        this.restaurant_hit = restaurant_hit;
    }

    @JsonProperty("shortDescription")
    public String getShortDescription() {
        return shortDescription;
    }

    @JsonProperty("shortDescription")
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @JsonProperty("surgeApplicable")
    public Boolean getSurgeApplicable() {
        return surgeApplicable;
    }

    @JsonProperty("surgeApplicable")
    public void setSurgeApplicable(Boolean surgeApplicable) {
        this.surgeApplicable = surgeApplicable;
    }

    @JsonProperty("swiggy_hit")
    public Integer getSwiggy_hit() {
        return swiggy_hit;
    }

    @JsonProperty("swiggy_hit")
    public void setSwiggy_hit(Integer swiggy_hit) {
        this.swiggy_hit = swiggy_hit;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public Boolean getTaxesOnDiscountedBill() {
        return taxesOnDiscountedBill;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public void setTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
    }

    @JsonProperty("timeSlotRestriction")
    public Boolean getTimeSlotRestriction() {
        return timeSlotRestriction;
    }

    @JsonProperty("timeSlotRestriction")
    public void setTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
    }

    @JsonProperty("updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updatedBy")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @JsonProperty("userRestriction")
    public Boolean getUserRestriction() {
        return userRestriction;
    }

    @JsonProperty("userRestriction")
    public void setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
    }

    @JsonProperty("valid_from")
    public Long getValid_from() {
        return valid_from;
    }

    @JsonProperty("valid_from")
    public void setValid_from(Long valid_from) {
        this.valid_from = valid_from;
    }

    @JsonProperty("valid_till")
    public Long getValid_till() {
        return valid_till;
    }

    @JsonProperty("valid_till")
    public void setValid_till(Long valid_till) {
        this.valid_till = valid_till;
    }

    public String getMinCartAmount() {
        return minCartAmount;
    }

    public void setMinCartAmount(String minCartAmount) {
        this.minCartAmount = minCartAmount;
    }


    private void setDefaultValues (String benefitID, String type, String level, List<Integer> restID) {
        List<Benefit> list = new ArrayList<>();
        list.add(new Benefit().build(benefitID,type,level,restID));

        if (this.getBenefits() == null)
            this.setBenefits(list);
        if (this.getCampaign_type() == null)
            this.setCampaign_type(type);
        if (this.getOperationType() == null)
            this.setOperationType("SUPER");
            else
                    this.setOperationType("RESTAURANT");
        if (this.getCommissionOnFullBill() == null)
            this.setCommissionOnFullBill(true);
        if (this.getValid_from() == null)
            this.setValid_from(superMultiTDHelper.getCurrentEpochTimeInMillis());
        if (this.getValid_till() == null)
            this.setValid_till(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100);
        if (this.getCreatedBy() == null)
            this.setCreatedBy("Automation_Super_MultiTD");
        if (this.getCreatedAt() == null)
            this.setCreatedAt(superMultiTDHelper.getCurrentEpochTimeInMillis());
        if (this.getDescription() == null)
            this.setDescription("Testing SuperMultiTD for automation");
        if (this.getDiscountCap() == null)
            this.setDiscountCap(0);
        if (this.getDormant_user_type() == null)
            this.setDormant_user_type("ZERO_DAYS_DORMANT");
        if (this.getEnabled() == null)
            this.setEnabled(true);
        if (this.getNamespace() == null)
            this.setNamespace("super_test");
        if (this.getFirstOrderRestriction() == null)
            this.setFirstOrderRestriction(false);
        if (this.getHeader() == null)
            this.setHeader("Super TD");
        if (this.getRestaurantFirstOrder() == null)
            this.setRestaurantFirstOrder(false);
        if (this.getRestaurant_hit() == null)
            this.setRestaurant_hit(0);
        if (this.getSwiggy_hit() == null)
            this.setSwiggy_hit(100);
        if (this.getShortDescription() == null)
            this.setShortDescription("FreeDel/Freebie");
        if (this.getSurgeApplicable() == null)
            this.setSurgeApplicable(true);
        if (this.getTaxesOnDiscountedBill() == null)
            this.setTaxesOnDiscountedBill(false);
        if (this.getTimeSlotRestriction() == null)
            this.setTimeSlotRestriction(false);
        if (this.getUpdatedBy() == null)
            this.setUpdatedBy("Automation_SuperMultiTD");
        if (this.getUserRestriction() == null)
            this.setUserRestriction(false);

}

    public Boolean getCopyOverridden() {
        return copyOverridden;
    }
    public void setCopyOverridden(Boolean copyOverridden) {
        this.copyOverridden = copyOverridden;
    }
    public CreateSuperCampaignV2 withCopyOverridden(Boolean copyOverridden){
        this.copyOverridden = copyOverridden;
        return this;
    }

    public CreateSuperCampaignV2 build (String benefitID, String type, String level, List<Integer> restID) {
        setDefaultValues(benefitID,type,level,restID);
        return this;
    }

    public CreateSuperCampaignV2 withBenefit(List<Benefit> benefit){
        this.benefits = benefit;
        return this;
    }

    public  CreateSuperCampaignV2 withCampaignType(String type){
        this.campaign_type = type;
        return this;
    }
   public CreateSuperCampaignV2 withValidFrom(Long validFrom){
        this.valid_from = valid_from;
        return this;
   }
    public CreateSuperCampaignV2 withValidTill(Long validTill){
        this.valid_till = validTill;
        return this;
    }
    public CreateSuperCampaignV2 withUserRestriction(Boolean userRestriction){
        this.userRestriction = userRestriction;
        return this;
    }

    public CreateSuperCampaignV2 withMinCartAmount(String minCartAmount){
        this.minCartAmount = minCartAmount;
        return this;
    }

    public CreateSuperCampaignV2 withHeader(String header){
        this.header = header;
        return this;
    }

    public CreateSuperCampaignV2 withDescription(String description){
        this.description = description;
        return this;
    }
    public CreateSuperCampaignV2 withShortDescription(String shortDescription){
        this.shortDescription = shortDescription;
        return this;
    }

    public CreateSuperCampaignV2 createFreeDelTdWithCopy(Boolean copyOverridden, String benefitId, List<Integer> restID, String minCartAmount ){

        List<Benefit> list = new ArrayList<>();
        list.add(new Benefit().build(benefitId,"FREE_DELIVERY","SuperCart",restID));

        if (this.getBenefits() == null)
            this.setBenefits(list);
        if (this.getCampaign_type() == null)
            this.setCampaign_type("FREE_DELIVERY");
        if (this.getOperationType() == null)
            this.setOperationType("SUPER");
        else
            this.setOperationType("SUPER");
        if (this.getCommissionOnFullBill() == null)
            this.setCommissionOnFullBill(true);
        if (this.getValid_from() == null)
            this.setValid_from(superMultiTDHelper.getCurrentEpochTimeInMillis());
        if (this.getValid_till() == null)
            this.setValid_till(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100);
        if (this.getCreatedBy() == null)
            this.setCreatedBy("Ankita-CopyAutomation ");
        if (this.getCreatedAt() == null)
            this.setCreatedAt(superMultiTDHelper.getCurrentEpochTimeInMillis());
        if (this.getDescription() == null)
            this.setDescription("Default");
        if (this.getDiscountCap() == null)
            this.setDiscountCap(0);
        if (this.getDormant_user_type() == null)
            this.setDormant_user_type("ZERO_DAYS_DORMANT");
        if (this.getEnabled() == null)
            this.setEnabled(true);
        if (this.getNamespace() == null)
            this.setNamespace("copyTest");
        if (this.getFirstOrderRestriction() == null)
            this.setFirstOrderRestriction(false);
        if (this.getHeader() == null)
            this.setHeader("Default");
        if (this.getRestaurantFirstOrder() == null)
            this.setRestaurantFirstOrder(false);
        if (this.getRestaurant_hit() == null)
            this.setRestaurant_hit(0);
        if (this.getSwiggy_hit() == null)
            this.setSwiggy_hit(100);
        if (this.getShortDescription() == null)
            this.setShortDescription("Default");
        if (this.getSurgeApplicable() == null)
            this.setSurgeApplicable(true);
        if (this.getTaxesOnDiscountedBill() == null)
            this.setTaxesOnDiscountedBill(false);
        if (this.getTimeSlotRestriction() == null)
            this.setTimeSlotRestriction(false);
        if (this.getUpdatedBy() == null)
            this.setUpdatedBy("CopyResolver_Automation");
        if (this.getUserRestriction() == null)
            this.setUserRestriction(false);
            this.setCopyOverridden(copyOverridden);
            this.minCartAmount = minCartAmount;
        return this;
    }



//    @Override
//    public String toString() {
//        return new ToStringBuilder(this).append("benefits", benefits).append("campaign_type", campaign_type).append("commissionOnFullBill", commissionOnFullBill).append("createdAt", createdAt).append("createdBy", createdBy).append("description", description).append("discountCap", discountCap).append("dormant_user_type", dormant_user_type).append("enabled", enabled).append("firstOrderRestriction", firstOrderRestriction).append("header", header).append("namespace", namespace).append("operationType", operationType).append("restaurantFirstOrder", restaurantFirstOrder).append("restaurant_hit", restaurant_hit).append("shortDescription", shortDescription).append("surgeApplicable", surgeApplicable).append("swiggy_hit", swiggy_hit).append("taxesOnDiscountedBill", taxesOnDiscountedBill).append("timeSlotRestriction", timeSlotRestriction).append("updatedBy", updatedBy).append("userRestriction", userRestriction).append("valid_from", valid_from).append("valid_till", valid_till).toString();
//    }

}