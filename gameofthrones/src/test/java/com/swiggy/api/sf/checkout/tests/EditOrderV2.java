package com.swiggy.api.sf.checkout.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.EditTimeConstants;
import com.swiggy.api.sf.checkout.dp.EditOrderDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.sanityHelper.EditOrderHelper;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;



public class EditOrderV2   {

    CheckoutHelper helper = new CheckoutHelper();
    EditOrderHelper EditHelp = new EditOrderHelper();
    SoftAssert sc;



    @Test(dataProviderClass=EditOrderDP.class,dataProvider = "OrderEditAndConfirm")
    public void OrderPlace_Test(String Tid,String Token,Integer Rest_id , Integer Quantity,String Menu_Item_Id,Long Order_Id,Integer Init_Source ) throws Exception{
        sc=new SoftAssert();
        String EditOrderResponse = EditHelp.orderEdit(Tid,Token,Rest_id,Quantity,Menu_Item_Id,Order_Id,Init_Source).ResponseValidator.GetBodyAsText();
        int status_code = JsonPath.read(EditOrderResponse, "$.statusCode");
        sc.assertEquals(status_code, 0, "Status code  matching");
        String Status_message = JsonPath.read(EditOrderResponse,"$.statusMessage");
        sc.assertEquals(Status_message,"CART_UPDATED_SUCCESSFULLY", "Status code  matching");
        EditHelp.orderConfirm(Tid,Token,Integer.parseInt(EditTimeConstants.REST_ID),1,Menu_Item_Id, Order_Id,1).ResponseValidator.GetBodyAsText();
        sc.assertEquals(status_code, 0, "Status code  matching");


    }

}
