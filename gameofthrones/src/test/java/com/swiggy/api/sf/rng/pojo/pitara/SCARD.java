
package com.swiggy.api.sf.rng.pojo.pitara;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "count"
})
@JsonRootName(value = "SCARD")
public class SCARD {


    @JsonProperty("count")
    private Integer count;

    /**
     * No args constructor for use in serialization
     * 
     */
    public SCARD() {
    }

    /**
     * 
     * @param count
     */
    public SCARD(Integer count) {
        super();
        this.count = count;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    public SCARD withCount(Integer count) {
        this.count = count;
        return this;
    }

}
