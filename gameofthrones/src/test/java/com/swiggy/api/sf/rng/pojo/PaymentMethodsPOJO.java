package com.swiggy.api.sf.rng.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "payment_codes",
        "upi_request",
        "source"
})
public class PaymentMethodsPOJO {

    @JsonProperty("payment_codes")
    private List<String> paymentCodes = null;
    @JsonProperty("upi_request")
    private List<UpiRequestPOJO> upiRequest = null;
    @JsonProperty("source")
    private Integer source;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    /**
     *
     */
    public PaymentMethodsPOJO() {
        super();
        this.paymentCodes = paymentCodes;
        this.upiRequest = upiRequest;
        this.source = source;
    }

    @JsonProperty("payment_codes")
    public List<String> getPaymentCodes() {
        return paymentCodes;
    }

    @JsonProperty("payment_codes")
    public void setPaymentCodes(List<String> paymentCodes) {
        this.paymentCodes = paymentCodes;
    }

    public PaymentMethodsPOJO withPaymentCodes(List<String> paymentCodes) {
        this.paymentCodes = paymentCodes;
        return this;
    }

    @JsonProperty("upi_request")
    public List<UpiRequestPOJO> getUpiRequest() {
        return upiRequest;
    }

    @JsonProperty("upi_request")
    public void setUpiRequest(List<UpiRequestPOJO> upiRequest) {
        this.upiRequest = upiRequest;
    }

    public PaymentMethodsPOJO withUpiRequest(List<UpiRequestPOJO> upiRequest) {
        this.upiRequest = upiRequest;
        return this;
    }

    @JsonProperty("source")
    public Integer getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(Integer source) {
        this.source = source;
    }

    public PaymentMethodsPOJO withSource(Integer source) {
        this.source = source;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PaymentMethodsPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public PaymentMethodsPOJO setDefault() {

        UpiRequestPOJO upiRequest = new UpiRequestPOJO();
        upiRequest.setDefault();
        List<UpiRequestPOJO> list = new ArrayList<>();
        list.add(upiRequest);

        List<String> list2 = new ArrayList<>();
        list2.add("123456");
        list2.add("678901");

        return this.withPaymentCodes(list2)
                .withSource(source)
                .withUpiRequest(list);
    }
}
