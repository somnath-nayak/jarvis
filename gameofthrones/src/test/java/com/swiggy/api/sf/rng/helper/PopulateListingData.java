package com.swiggy.api.sf.rng.helper;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PopulateListingData {
	
	private static final String filePath="D:\\perf\\listing_perfBXGY.csv";

	private static int listingPayloadCount = 1;
	private static int maxRestIdsLimit = 300;

	private static String listingPayload = "{\"restaurantIds\":[<restIds>],\"userId\":3824967,\"minCartAmount\":0.0,\"userAgent\":\"IOS\",\"versionCode\":208,\"firstOrder\":false}\n";
	
	public void writeDataToFile(String filePath, Map<Integer, List> map) {

			File listingFile = new File(filePath);
			FileWriter fw;
			try {
				fw = new FileWriter(listingFile);

				for (int i = 0; i < map.size(); i++) {

					String payload = createPayload(map.get(i));
					System.out.println("payload " + payload);
					fw.write(payload);
				}
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	private String createPayload(List restIds) {
		String restaurantIds = restIds.size() > 1 ? StringUtils.join(restIds, ",") : String.valueOf(restIds.get(0));
		return listingPayload.replace("<restIds>", restaurantIds);
	}

	
	public void createListingPayload() {
		TDPerfTest tdPerfTest = new TDPerfTest();
		List restIds = tdPerfTest.getTdEnableRestIds();
		Map<Integer,List> map=new HashMap<>();
		for(int i = 0; i < listingPayloadCount; i++) {
			int restIdsRange = Utility.getRandom(100, maxRestIdsLimit);
			List randomRestIds = Utility.getRandomElementsFromList(restIds, restIdsRange);
			map.put(i,randomRestIds);
		}
		writeDataToFile(filePath, map);

	}

}