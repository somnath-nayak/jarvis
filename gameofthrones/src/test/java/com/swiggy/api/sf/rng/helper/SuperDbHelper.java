package com.swiggy.api.sf.rng.helper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.List;
import java.util.Map;

public class SuperDbHelper {

	static String deleteBenefit = "delete from benefits where id= '";
	static String endQuotes = "'";
	static String end = "';";
	static String subscriptionDB = "MarketingSubscription";
	static String deleteFreeDelBenefit = "delete from benefits where type= \"FREE_DELIVERY\";";
	static String deleteAllBenefit = "delete from benefits;";
	static String deleteFreebieBenefit = "delete from benefits where type= \"Freebie\";";
	static String selectIdOfUserSubsFromSubscription = "select id from subscription where user_id in (select user_id from user_subscription where subscription_id='";
	static String midOfSelectIdOfUserSubsFromSubscription = "') and plan_id='";
	static String endOfSelectIdOfUserSubsFromSubscription = "' and order_id='";
	static String updateUserSubsPlanValidTillDate = "UPDATE subscription SET valid_till ='";
	static String midOfUpdateValidTill = "' WHERE id='";
	static String endOfUpdateValidTill = "'and user_id='";
	static String setPresentSubsToPastSubsByValidFromValidTill = "UPDATE subscription SET valid_from = '";
	static String midOfsetPresentSubsToPast = "' , valid_till='";
	static String updateUserSubsPlanValidFromDate = "UPDATE subscription SET valid_till = '";
	static String getOrderIdByUserId = "select order_id from subscription where user_id='";
	static String getValidFromOfPlan = "select valid_from from subscription where plan_id=' ";
	static String midGetValidFrom = "' and order_id='";
	static String endOfValidFrom = "' and user_id='";
	static String getValidTillOfPlan = "select valid_till from subscription where plan_id=' ";
	static String updatePlanValidTill = "UPDATE plan SET valid_till ='";
	static String midUpdatePlanValidTill = "' where id='";
	static String updateTotalAvailableOfPlan = "update plan SET total_available ='";
	static String updateUsageCountOfPlan = " update plan SET usage_count ='";
	static String deletePlan = "delete from plan where id='";
	static String selectValidfromOfSubsFromSubscription = "select valid_from from subscription where user_id in (select user_id from user_subscription where subscription_id='";
	static String selectValidtillOfSubsFromSubscription = "select valid_till from subscription where user_id in (select user_id from user_subscription where subscription_id='";
	static String totalAvailableInPlan = "select total_available from plan where id='";
	static String updatePlanRenewalOffset = "UPDATE plan SET renewal_offset_days ='";
	static String endOfUpdatePlanRenewalWHERE = "' WHERE id='";
	static String checkUserSubscription = "select * from subscription where user_id = \"#1\" and enabled=1 and active=1;";
	static String getSubscriptionEnbledUserId = "select * from subscription where valid_from <= now() and valid_till > now() and enabled=1 and active= 1;";
    static String getSubscriptionOfTestUser= "select * from  user_subscription where user_id in (select user_id from subscription where user_id = \"#1\" and enabled=1 and active=1);";
static String getActiveBenefitId = "select id from benefits where type = \"#1\";";
	public static void deleteBenefitFromDB(String benefitId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		sqlTemplate.update(deleteBenefit + benefitId + endQuotes);

	}
	//The benefit has to uncomment after multiTD testing in UAT-3

	public static void deleteFreebieBenefit() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		sqlTemplate.execute(deleteFreebieBenefit);

	}

	public static void deleteFreeDelBenefit() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		sqlTemplate.execute(deleteFreeDelBenefit);
	}

	public static void deleteAllBenefit() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		sqlTemplate.execute(deleteAllBenefit);
	}


	public static String getIdFromSubscriptionForUserSubs(String subsId, String plan_id, String order_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(selectIdOfUserSubsFromSubscription
				+ subsId + midOfSelectIdOfUserSubsFromSubscription + plan_id
				+ endOfSelectIdOfUserSubsFromSubscription + order_id + end);
		System.out.println("Susbc. querry -->>" + selectIdOfUserSubsFromSubscription
				+ subsId + midOfSelectIdOfUserSubsFromSubscription + plan_id
				+ endOfSelectIdOfUserSubsFromSubscription + order_id + end);
		return (list.size() > 0) ? (list.get(0).get("id").toString())
				: "no id created in subscription for user subscription is DB";
	}

	public static String getValidfromFromSubscriptionForUserSubs(String subsId, String plan_id, String order_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(selectValidfromOfSubsFromSubscription
				+ subsId + midOfSelectIdOfUserSubsFromSubscription + plan_id
				+ endOfSelectIdOfUserSubsFromSubscription + order_id + end);
		System.out.println("Susbc. querry -->>" + selectValidfromOfSubsFromSubscription
				+ subsId + midOfSelectIdOfUserSubsFromSubscription + plan_id
				+ endOfSelectIdOfUserSubsFromSubscription + order_id + end);
		return (list.size() > 0) ? (list.get(0).get("valid_from").toString())
				: "no id created in subscription for user subscription is DB";
	}

	public static String getValidtillFromSubscriptionForUserSubs(String subsId, String plan_id, String order_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(selectValidtillOfSubsFromSubscription
				+ subsId + midOfSelectIdOfUserSubsFromSubscription + plan_id
				+ endOfSelectIdOfUserSubsFromSubscription + order_id + end);
		System.out.println("Susbc. querry -->>" + selectValidtillOfSubsFromSubscription
				+ subsId + midOfSelectIdOfUserSubsFromSubscription + plan_id
				+ endOfSelectIdOfUserSubsFromSubscription + order_id + end);
		return (list.size() > 0) ? (list.get(0).get("valid_till").toString())
				: "no id created in subscription for user subscription is DB";
	}

	public static void updateValidTillByIdInSusbscription(String valid_till, String id, String userId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		sqlTemplate.execute(updateUserSubsPlanValidTillDate + valid_till + midOfUpdateValidTill
				+ id + endOfUpdateValidTill + userId + end);
		System.out.println("update Valid Till querry " + updateUserSubsPlanValidTillDate + valid_till + midOfUpdateValidTill
				+ id + endOfUpdateValidTill + userId + end);
	}

	public static void updateValidTillByUserId(String valid_till, String userId) {
		String id = getSubscriptionIdOfTestUser(userId);
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		sqlTemplate.execute(updateUserSubsPlanValidTillDate + valid_till + midOfUpdateValidTill
				+ id + endOfUpdateValidTill + userId + end);
		System.out.println("update Valid Till querry " + updateUserSubsPlanValidTillDate + valid_till + midOfUpdateValidTill
				+ id + endOfUpdateValidTill + userId + end);
	}

	public static void updateValidFromByIdInSusbscription(String valid_from, String id, String userId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);

		sqlTemplate.execute(updateUserSubsPlanValidFromDate + valid_from + midOfUpdateValidTill
				+ id + endOfUpdateValidTill + userId + end);
//		System.out.println("update Valid Till querry " +SuperConstants.updateUserSubsPlanValidFromDate + valid_from+SuperConstants.midOfUpdateValidTill
//				+id+ SuperConstants.endOfUpdateValidTill+userId+SuperConstants.end);
	}

	public static void updateValidFromAndValidTillByIdInSusbscription(String valid_from, String valid_till, String id, String userId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		// List<Map<String, Object>> list =
		// sqlTemplate.queryForList(RngConstants.selectUser+ mobile);
		sqlTemplate.execute(setPresentSubsToPastSubsByValidFromValidTill + valid_from + midOfsetPresentSubsToPast + valid_till + midOfUpdateValidTill
				+ id + endOfUpdateValidTill + userId + end);
		System.out.println("update Valid Till querry " + setPresentSubsToPastSubsByValidFromValidTill + valid_from + midOfsetPresentSubsToPast + valid_till + midOfUpdateValidTill
				+ id + endOfUpdateValidTill + userId + end);
	}

	public static List getAllSubsOrderIdByUserId(String user) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(getOrderIdByUserId + user + endQuotes);
		return list;
	}

	public static String getValidTillOfPlan(String plan_id, String order_id, String user_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(getValidTillOfPlan
				+ plan_id
				+ midGetValidFrom + order_id + endOfValidFrom + user_id + end);
//		System.out.println("Susbc. querry -->>" + getValidFromOfPlan
//		+ plan_id + midOfSelectIdOfUserSubsFromSubscription + plan_id
//		+ midGetValidFrom + order_id + endOfValidFrom + user_id + end);
		return (list.size() > 0) ? (list.get(0).get("valid_till").toString())
				: "no id created in subscription for user subscription is DB";
	}


	public static String getValidFromOfPlan(String plan_id, String order_id, String user_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(getValidFromOfPlan
				+ plan_id
				+ midGetValidFrom + order_id + endOfValidFrom + user_id + end);
		return (list.size() > 0) ? (list.get(0).get("valid_from").toString())
				: "no id created in subscription for user subscription is DB";
	}


	public static void updateValidTillOfPlanByValidTill(String valid_till, String plan_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);

		sqlTemplate.execute(updatePlanValidTill + valid_till + midUpdatePlanValidTill
				+ plan_id + end);
//		System.out.println("update Valid Till querry " +SuperConstants.updateUserSubsPlanValidFromDate + valid_from+SuperConstants.midOfUpdateValidTill
//				+id+ SuperConstants.endOfUpdateValidTill+userId+SuperConstants.end);
	}

	public static void updateTotalAvailableOfPlan(String plan_id, String totalCount) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);

		sqlTemplate.execute(updateTotalAvailableOfPlan + totalCount + midUpdatePlanValidTill
				+ plan_id + end);
		System.out.println("update Valid Till querry " + updateTotalAvailableOfPlan + totalCount + midUpdatePlanValidTill
				+ plan_id + end);
	}

	public static void updateUsageCountOfPlan(String plan_id, String usageCount) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);

		sqlTemplate.execute(updateUsageCountOfPlan + usageCount + midUpdatePlanValidTill
				+ plan_id + end);
//		System.out.println("update Valid Till querry " +SuperConstants.updateUserSubsPlanValidFromDate + valid_from+SuperConstants.midOfUpdateValidTill
//				+id+ SuperConstants.endOfUpdateValidTill+userId+SuperConstants.end);
	}

	public static void deletePlan(String plan_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);

		sqlTemplate.execute(deletePlan + plan_id + end);
//		System.out.println("update Valid Till querry " +SuperConstants.updateUserSubsPlanValidFromDate + valid_from+SuperConstants.midOfUpdateValidTill
//				+id+ SuperConstants.endOfUpdateValidTill+userId+SuperConstants.end);
	}

	public static String getTotalAvailableOfPlan(String plan_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(totalAvailableInPlan
				+ plan_id + end);
		return (list.size() > 0) ? (list.get(0).get("total_available").toString())
				: "plan does not exist in DB";
	}

	public static void updateRenewalOffsetOfPlan(String plan_id, String renewalOffset) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);

		sqlTemplate.execute(updatePlanRenewalOffset + renewalOffset + endOfUpdatePlanRenewalWHERE
				+ plan_id + end);
		System.out.println("update renewal off days querry >>>>>  " + updatePlanRenewalOffset + renewalOffset + endOfUpdatePlanRenewalWHERE
				+ plan_id + end);
	}

  public static String subscriptionCheckForUser(String userId){
	SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
	TDCachingDbHelper.replaceOneWord( checkUserSubscription,  userId);
	List<Map<String, Object>> list = sqlTemplate.queryForList(checkUserSubscription);
	return (list.size()>0) ? (list.get(0).get("user_id").toString()) : "No subscription found for user";
  }

	public static String getSubscriptionIdOfTestUser(String userId){
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		getSubscriptionOfTestUser= TDCachingDbHelper.replaceOneWord( getSubscriptionOfTestUser,  userId);
		List<Map<String, Object>> list = sqlTemplate.queryForList(getSubscriptionOfTestUser);
		return (list.size()>0) ? (list.get(0).get("subscription_id").toString()) : "No subscription found for user";
	}

	public static String getSubscriptionEnabledUserId(){
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(getSubscriptionEnbledUserId);
		return (list.size()>0) ? (list.get(0).get("user_id").toString()) : "No active subscription found for any user";
	}

	public static String getActiveBenefitsFromDb(String type){
		String getActiveBenefit = TDCachingDbHelper.replaceOneWord(getActiveBenefitId,type);
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(getActiveBenefit);
	return (list.size()>0) ? (list.get(0).get("id").toString()) : "No active Free Delivery benefit found in DB";
	}

	public static String getActiveFreebieBenefitsFromDb(String type){
		String getActiveBenefit = TDCachingDbHelper.replaceOneWord(getActiveBenefitId,type);
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(subscriptionDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(getActiveBenefit);
		return (list.size()>0) ? (list.get(0).get("id").toString()) : "No active Freebie benefit found in DB";
	}
	
}
