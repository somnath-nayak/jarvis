package com.swiggy.api.sf.rng.tests;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.SuperMultiTDHelper;
import com.swiggy.api.sf.rng.pojo.CartEvaluateRequest;
import com.swiggy.api.sf.rng.pojo.MenuEvaluateRequest;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3.EvaluateCartV3;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3.ItemRequest;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class TD extends DP{

	Processor processor;
	RngHelper rngHelper = new RngHelper();
	JsonHelper jsonHelper=new JsonHelper();
	SuperMultiTDHelper helper = new SuperMultiTDHelper();
	EvaluateCartV3 evaluateCartV3 = new EvaluateCartV3();

public void validation(String type,ArrayList al,String tid,String TDType,String minCartAmount,String cartTD,String rewardList,boolean check)
	{
	if(type.equals("list"))
	{
			Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			System.out.println(resp);
			if(check) {
				boolean cidFlag = false;

				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, TDType, ""+TDType+" TD is not found");
				String[] get_tdil_campaignIds = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..campaignId").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim().split(",");
				for (String cid: get_tdil_campaignIds) {
					if (tid.equalsIgnoreCase(cid)) {
						cidFlag = true;
						break;
					}
				}
				Assert.assertTrue(cidFlag, "camp id is not visible");
				String get_tdil_mincartamt = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..minCartAmount").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_mincartamt, minCartAmount, "Mincart amout is not found");

			}else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
				//System.out.println(JsonPath.read(resp, "$..data..discountList[*]").toString());
				}


	}
		if(type.equals("menu"))
		{
			Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].campaignId").toString(), "["+tid+"]", "TD is not found");
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "["+"\""+TDType+"\""+"]", "TD is not percentage");
				Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), cartTD, "TD is not percentage");
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].minCartAmount").toString(),"["+minCartAmount+"]" , "TD is not percentage");
				Assert.assertEquals(JsonPath.read(resp, "$.data.rewardType").toString(),TDType , "TD is not percentage");
				Assert.assertEquals(JsonPath.read(resp, "$.data.rewardList").toString(),"[]" , "TD is not percentage");



			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
				Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "TD is not percentage");

			}
		}
		if(type.equals("cart"))
		{
			Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			//$.data..restaurants..tradeDiscountInfo..discountType
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..campaignId").toString(), "["+tid+"]", "TD is not found");
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..discountType").toString(), "["+"\""+TDType+"\""+"]", "TD is not percentage");
				Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), cartTD, "TD is not percentage");
				//Assert.assertEquals(JsonPath.read(resp, "$.data.rewardType").toString(),TDType , "TD is not percentage");
				//Assert.assertEquals(JsonPath.read(resp, "$.data.rewardList..itemId").toString(),rewardList , "TD is not percentage");

			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
				Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "TD is not percentage");

			}
		}
		if(type.equals("cart2")) {
			Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(), al.get(10).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if (check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Flat\"]", "TD is not percentage");
			} else {
				Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "tradeDiscountInfo object has discount");
			}
		}

	}

	@Test(dataProvider = "PercentageTD_EvaluationWithNoMinCartAtRestaurantLevel", groups = {"sanity","Gaurav"},description = "Listing Evaluation With Percentage At Restaurant level", enabled = true)
    public void PercentageTD_EvaluationWithNoMinCartAtRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
	String TDType = "Percentage";
	String minCartAmount = "0.0";
	String cartTD = "50.0";
	String rewardList = "[]";
	validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check); }

    @Test(dataProvider = "PercentageTD_EvaluationWithMinCartAtRestaurantLevel", groups = {"regression","Gaurav"},description = "Listing Evaluation With Percentage Min Limit At Restaurant level", enabled = true)
    public void PercentageTD_EvaluationWithMinCartAtRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Percentage";
		String minCartAmount = "100.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);
    }

    @Test(dataProvider = "PercentageTD_EvaluationWithSwiggyFirstOrderRestaurantLevel",description = "Listing Evaluation with Percentage , min Limit & swiggy first order at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void PercentageTD_EvaluationWithSwiggyFirstOrderRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Percentage";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);
    }

    @Test(dataProvider = "PercentageTD_EvaluationWithRestaurantFirstOrderRestaurantLevel",description = "Listing Evaluation with Percentage , min Limit & restaurant first order at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void PercentageTD_EvaluationWithRestaurantFirstOrderRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Percentage";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);
    }

    @Test(dataProvider = "PercentageTD_EvaluationWithUserRestrictionRestaurantLevel",description = "Listing Evaluation with Percentage , min Limit & User restriction at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void PercentageTD_EvaluationWithUserRestrictionRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Percentage";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "PercentageTD_EvaluationWithTimeSlotRestrictionRestaurantLevel",description = "Listing Evaluation with Percentage , min Limit & time slot restriction at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void PercentageTD_EvaluationWithTimeSlotRestrictionRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Percentage";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "PercentageTD_EvaluationWithThirtyDaysDormantRestaurantLevel", groups = {"regression","Gaurav"},description = "Listing Evaluation With Percentage At Restaurant level", enabled = true)
    public void PercentageTD_EvaluationWithThirtyDaysDormantRestaurantLevel(String type, ArrayList al,String tid,boolean check){
		String TDType = "Percentage";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

     }

    @Test(dataProvider = "PercentageTD_EvaluationWithSixtyDaysDormantRestaurantLevel", groups = {"regression","Gaurav"},description = "Listing Evaluation With Percentage At Restaurant level", enabled = true)
    public void PercentageTD_EvaluationWithSixtyDaysDormantRestaurantLevel(String type, ArrayList al,String tid,boolean check){
		String TDType = "Percentage";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

     }

    @Test(dataProvider = "PercentageTD_EvaluationWithNinetyDaysDormantRestaurantLevel", groups = {"regression","Gaurav"},description = "Listing Evaluation With Percentage At Restaurant level", enabled = true)
    public void PercentageTD_EvaluationWithNinetyDaysDormantRestaurantLevel(String type, ArrayList al,String tid,boolean check){
		String TDType = "Percentage";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

     }

/*    @Test(dataProvider = "menuEvaluationWithPercentageAtRestaurant", groups = {"sanity","Gaurav"},description = "Menu Evaluation With Percentage At Restaurant level", enabled = true)
    public void menuEvalutateWithPercentageAtRestaurant(String minCartAmount,String restaurantIds, String firstOrder, String userId, String userAgent, String versionCode) {
        Processor processor = rngHelper.menuEvaluate(minCartAmount,restaurantIds, firstOrder, userId, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
    }

    @Test(dataProvider = "menuEvaluationWithPercentageMinLimitAtRestaurant", groups = {"regression","Gaurav"},description = "Menu Evaluation With Percentage Min Limit At Restaurant level", enabled = true)
    public void menuEvaluationWithPercentageMinLimitAtRestaurant(String minCartAmount,String restaurantIds, String firstOrder, String userId, String userAgent, String versionCode) {
        Processor processor = rngHelper.menuEvaluate(minCartAmount,restaurantIds, firstOrder, userId, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
    }

    @Test(dataProvider = "menuEvaluationWithPercentageMinLimitSwiggyFirstAtRestaurant",description = "Menu Evaluation with Percentage , min Limit & swiggy first order at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void menuEvaluationWithPercentageMinLimitSwiggyFirstAtRestaurant(String minCartAmount,String restaurantIds, String firstOrder, String userId, String userAgent, String versionCode,boolean isCheck) {
        Processor processor = rngHelper.menuEvaluate(minCartAmount,restaurantIds, firstOrder, userId, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        if(isCheck) {
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
        }else{
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
        }
    }

    @Test(dataProvider = "menuEvaluationWithPercentageMinLimitRestaurantFirstAtRestaurant",description = "Menu Evaluation with Percentage , min Limit & restaurant first order at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void menuEvaluationWithPercentageMinLimitRestaurantFirstAtRestaurant(String minCartAmount,String restaurantIds, String firstOrder, String userId, String userAgent, String versionCode) {

    	RedisTemplate sqlTemplate = redisHelper.getRedisTemplate("rngredis", 1);
    	sqlTemplate.setKeySerializer(new StringRedisSerializer());
    	sqlTemplate.setHashKeySerializer(new StringRedisSerializer());
    	boolean isCheck= sqlTemplate.boundHashOps("user_restaurant_last_order:"+userId).entries().containsKey(restaurantIds);
        Processor processor = rngHelper.menuEvaluate(minCartAmount,restaurantIds, firstOrder, userId, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        if(isCheck==false) {
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
        }else{
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
        }
    }

    @Test(dataProvider = "menuEvaluationWithPercentageMinLimitWithUserRestrictiontAtRestaurant",description = "Menu Evaluation with Percentage , min Limit & User restriction at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void menuEvaluationWithPercentageMinLimitWithUserRestrictiontAtRestaurant(String minCartAmount,String restaurantIds, String firstOrder, String userId, String userAgent, String versionCode,String tidId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from campaign_user_map where campaign_id="+tidId+" and user_id="+userId);
        System.out.println("Key status"+ list.size());
        Processor processor = rngHelper.menuEvaluate(minCartAmount,restaurantIds, firstOrder, userId, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        if(list.size()>0) {
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
        }else{
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
        }
    }

    @Test(dataProvider = "menuEvaluationWithPercentageMinLimitWithTimeSlotRestrictionAtRestaurant",description = "Listing Evaluation with Percentage , min Limit & time slot restriction at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void menuEvaluationWithPercentageMinLimitWithTimeSlotRestrictionAtRestaurant(String minCartAmount,String restaurantIds, String firstOrder, String userId, String userAgent, String versionCode) {
        Processor processor = rngHelper.menuEvaluate(minCartAmount,restaurantIds, firstOrder, userId, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");

    }

    @Test(dataProvider = "menuEvalutateWithPercentageAtRestaurantWithDormantUser", groups = {"regression","Gaurav"},description = "Menu Evaluation With Percentage At Restaurant level for Dormant User", enabled = true)
    public void menuEvalutateWithPercentageAtRestaurantWithDormantUser(String minCart,String restaurantIds, String firstOrder, String userId, String userAgent, String versionCode) throws ParseException {
    	RedisTemplate sqlTemplate = redisHelper.getRedisTemplate("rngredis", 1);
    	sqlTemplate.setKeySerializer(new StringRedisSerializer());
    	sqlTemplate.setHashKeySerializer(new StringRedisSerializer());
    	boolean isCheck= sqlTemplate.boundHashOps("user_restaurant_last_order:"+userId).entries().containsKey(restaurantIds);
    	Processor processor = rngHelper.menuEvaluate(minCart,restaurantIds, firstOrder, userId, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
       if(isCheck)
       {

    	 String date = sqlTemplate.boundHashOps("user_restaurant_last_order:"+userId).entries().get(restaurantIds).toString();
    	 if(rngHelper.getDayDifference(date)>=30||rngHelper.getDayDifference(date)>=60||rngHelper.getDayDifference(date)>=90 )
    	 {
    	 Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
         }
    	 else

    	 {
    		 Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
    	 }
       }
       else
       {
    	   Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
       }
    }

   @Test(dataProvider = "cartEvaluationWithPercentageAtRestaurant", groups = {"sanity","Gaurav"},description = "Cart Evaluation With Percentage At Restaurant level", enabled = true)
    public void cartEvalutateWithPercentageAtRestaurant(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String cartAmount,  String userId, String firstOrder, String userAgent, String versionCode) {
       Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,cartAmount, userId,firstOrder, userAgent, versionCode);
       String resp = processor.ResponseValidator.GetBodyAsText();
       Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
       Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "20.0", "TD is not applied");

    }

    @Test(dataProvider = "cartEvaluationWithPercentageMinLimitAtRestaurant", groups = {"sanity","Gaurav"},description = "cart Evaluation With Percentage Min Limit At Restaurant level", enabled = true)
    public void cartEvaluationWithPercentageMinLimitAtRestaurant(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String cartAmount,  String userId, String firstOrder, String userAgent, String versionCode,String minCartAmount) {
        Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,cartAmount, userId,firstOrder, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        if (Integer.parseInt(cartAmount)>=Integer.parseInt(minCartAmount)) {

        	Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
            Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "20.0", "TD is not applied");

		}
        else
        {
        	Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "TD is not percentage");

        }
    }

    @Test(dataProvider = "cartEvaluationWithPercentageMinLimitSwiggyFirstAtRestaurant",description = "cart Evaluation with Percentage , min Limit & swiggy first order at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void cartEvaluationWithPercentageMinLimitSwiggyFirstAtRestaurant(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String cartAmount,  String userId, String firstOrder, String userAgent, String versionCode,String minCartAmount) {
        Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,cartAmount, userId,firstOrder, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        if(firstOrder=="true"&& Integer.parseInt(cartAmount)>Integer.parseInt(minCartAmount)) {
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");

        }else{
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");

        }
    }

    @Test(dataProvider = "cartEvaluationWithPercentageMinLimitRestaurantFirstAtRestaurant",description = "cart Evaluation with Percentage , min Limit & restaurant first order at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void cartEvaluationWithPercentageMinLimitRestaurantFirstAtRestaurant(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String cartAmount,  String userId, String firstOrder, String userAgent, String versionCode){

    	RedisTemplate sqlTemplate = redisHelper.getRedisTemplate("rngredis", 1);
    	sqlTemplate.setKeySerializer(new StringRedisSerializer());
    	sqlTemplate.setHashKeySerializer(new StringRedisSerializer());
    	boolean isCheck= sqlTemplate.boundHashOps("user_restaurant_last_order:"+userId).entries().containsKey(restaurantID);
        Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,cartAmount, userId,firstOrder, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
        if(isCheck==false && Integer.parseInt(cartAmount)>100) {
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
        }else{
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
        }
    }

    @Test(dataProvider = "cartEvaluationWithPercentageMinLimitWithUserRestrictiontAtRestaurant",description = "cart Evaluation with Percentage , min Limit & User restriction at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void cartEvaluationWithPercentageMinLimitWithUserRestrictiontAtRestaurant(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String cartAmount,  String userId, String firstOrder, String userAgent, String versionCodes,String tidId,String minCartAmount) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from campaign_user_map where campaign_id="+tidId+" and user_id="+userId);
        System.out.println("Key status"+ list.size());
        Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,cartAmount, userId,firstOrder, userAgent, versionCodes);
        String resp = processor.ResponseValidator.GetBodyAsText();
        if(list.size()>0 && Integer.parseInt(cartAmount)>Integer.parseInt(minCartAmount)) {
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
        }else{
            Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
        }
    }

    @Test(dataProvider = "cartEvaluationWithPercentageMinLimitWithTimeSlotRestrictionAtRestaurant",description = "cart Evaluation with Percentage , min Limit & time slot restriction at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void cartEvaluationWithPercentageMinLimitWithTimeSlotRestrictionAtRestaurant(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String cartAmount,  String userId, String firstOrder, String userAgent, String versionCodes) {
        Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,cartAmount, userId,firstOrder, userAgent, versionCodes);
        String resp = processor.ResponseValidator.GetBodyAsText();
        if(Integer.parseInt(cartAmount)>100)
        {
        Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");

        }
        else
        {
        	Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");

        }
    }

    @Test(dataProvider = "cartEvalutateWithPercentageAtRestaurantWithDormantUser", groups = {"regression","Gaurav"},description = "Cart Evaluation With Percentage At Restaurant level", enabled = true)
    public void cartEvalutateWithPercentageAtRestaurantWithDormantUser(String restaurantID,String categoryId,String subCategoryId,String itemId,String count,String price,String cartAmount,  String userId, String firstOrder, String userAgent, String versionCode,String minCartAmount) throws ParseException {
    	RedisTemplate sqlTemplate = redisHelper.getRedisTemplate("rngredis", 1);
    	sqlTemplate.setKeySerializer(new StringRedisSerializer());
    	sqlTemplate.setHashKeySerializer(new StringRedisSerializer());
    	boolean isCheck= sqlTemplate.boundHashOps("user_restaurant_last_order:"+userId).entries().containsKey(restaurantID);
    	Processor processor = rngHelper.cartEvaluate(restaurantID,categoryId,subCategoryId,itemId,count,price,cartAmount, userId,firstOrder, userAgent, versionCode);
        String resp = processor.ResponseValidator.GetBodyAsText();
       if(isCheck&Integer.parseInt(cartAmount)>=Integer.parseInt(minCartAmount))
       {

    	 String date = sqlTemplate.boundHashOps("user_restaurant_last_order:"+userId).entries().get(restaurantID).toString();
    	 if(rngHelper.getDayDifference(date)>=30||rngHelper.getDayDifference(date)>=60||rngHelper.getDayDifference(date)>=90)
    	 {
    	 Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
         }
    	 else

    	 {
    		 Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
    	 }
       }
       else
       {
    	   Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
       }
        }
*/


	@Test(dataProvider = "EvaluationForSubCategoryLevel", groups = {"sanity", "Gaurav"}, description = "TD at Sub Category Level", enabled = true)
	public void PercentageTD_EvaluateSubCategoryLevelTest(String type, Object al, boolean shouldTDApply, String amount,String tid,boolean isSameSubCate,boolean isCapApplied) throws IOException {

			if (type.equals("list")) {
				List list = (ArrayList) al;
				Processor processor = rngHelper.listEvalulateV3(list.get(0).toString(), list.get(1).toString(), list.get(2).toString(), list.get(3).toString(), list.get(4).toString());
				String resp = processor.ResponseValidator.GetBodyAsText();
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();

				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");

				String get_tdil_campid = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..campaignId").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();

				Assert.assertEquals(get_tdil_campid, tid, "TD is not percentage");
			}
			if (type.equals("menu")) {
				MenuEvaluateRequest menuEvaluateRequest = (MenuEvaluateRequest) al;
				Processor processor = rngHelper.menuEvaluateItemList(menuEvaluateRequest);
				String resp = processor.ResponseValidator.GetBodyAsText();
				if(shouldTDApply)
				{
					Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo..campaignId").toString(), "["+tid+"]", "TD is not percentage");
					Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
                if(isSameSubCate)
                     {
	               if(isCapApplied) {
	                	Assert.assertEquals(JsonPath.read(resp, "$.data.restaurantTradeDiscount").toString(), amount, "TD is not percentage");
	                              }else{
		                Assert.assertEquals(JsonPath.read(resp, "$.data.restaurantTradeDiscount").toString(), "10.0", "TD is not percentage");

	                  }
                 }else{
                	     Assert.assertEquals(JsonPath.read(resp, "$.data.swiggyTradeDiscount").toString(), "0.0", "TD is not percentage");
	                     Assert.assertEquals(JsonPath.read(resp, "$.data.restaurantTradeDiscount").toString(), "0.0", "TD is not percentage");
                        }
                 }
				else
				{
					Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo..campaignId").toString(), "[]", "TD is not percentage");
                    Assert.assertEquals(JsonPath.read(resp, "$.data.swiggyTradeDiscount").toString(), "0.0", "TD is not percentage");
					Assert.assertEquals(JsonPath.read(resp, "$.data.restaurantTradeDiscount").toString(), "0.0", "TD is not percentage");

				}

				}
			if (type.equals("cart")) {
				CartEvaluateRequest cartEvaluateRequest = (CartEvaluateRequest) al;
				Processor processor = rngHelper.cartEvaluateItemList(cartEvaluateRequest);
				String resp = processor.ResponseValidator.GetBodyAsText();
				if(shouldTDApply)
				{
					Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..campaignId").toString(), "["+tid+"]", "TD is not percentage");
					Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
					if(isSameSubCate)
					{
						if(isCapApplied) {
							Assert.assertEquals(JsonPath.read(resp, "$.data.restaurantTradeDiscount").toString(), amount, "TD is not percentage");
						}else{
							Assert.assertEquals(JsonPath.read(resp, "$.data.restaurantTradeDiscount").toString(), "10.0", "TD is not percentage");

						}
					}else
					{
						Assert.assertEquals(JsonPath.read(resp, "$.data.swiggyTradeDiscount").toString(), "0.0", "TD is not percentage");
						Assert.assertEquals(JsonPath.read(resp, "$.data.restaurantTradeDiscount").toString(), "0.0", "TD is not percentage");

					}


				}
				else
				{
					Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..campaignId").toString(), "[]", "TD is not percentage");

					Assert.assertEquals(JsonPath.read(resp, "$.data.swiggyTradeDiscount").toString(), "0.0", "TD is not percentage");
					Assert.assertEquals(JsonPath.read(resp, "$.data.restaurantTradeDiscount").toString(), "0.0", "TD is not percentage");

				}

			}
			}




	@Test(dataProvider = "PercentageTD_EvaluateCategoryLevelTest", groups = {"sanity", "Gaurav"}, description = "TD at Category Level", enabled = true)
    public void PercentageTD_EvaluateCategoryLevelTest(String type, Object al, boolean shouldTDApply, String amount,boolean check) throws IOException {
        if (shouldTDApply) {
            if (type.equals("list")) {
                List list = (ArrayList) al;
                Processor processor = rngHelper.listEvalulateV3(list.get(0).toString(), list.get(1).toString(), list.get(2).toString(), list.get(3).toString(), list.get(4).toString());
                String resp = processor.ResponseValidator.GetBodyAsText();
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
                Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not pecentage");
            }
            if (type.equals("menu")) {
                List menuList = (ArrayList) al;
                Processor processor = rngHelper.menuEvaluate(menuList.get(0).toString(), menuList.get(1).toString(), menuList.get(2).toString(), menuList.get(3).toString(), menuList.get(4).toString(), menuList.get(5).toString());
                String resp = processor.ResponseValidator.GetBodyAsText();
                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not flat");
            }
            if (type.equals("cart")) {
                CartEvaluateRequest cartEvaluateRequest = (CartEvaluateRequest) al;
                Processor processor = rngHelper.cartEvaluateItemList(cartEvaluateRequest);
                String resp = processor.ResponseValidator.GetBodyAsText();
                if(check) {
                    Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not flat");
                    Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), amount, "TD is not percentage");

                }else
                {
                    Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "TD is not percentage");

                }

            }
        } else {
            if (type.equals("cart")) {
                CartEvaluateRequest cartEvaluateRequest = (CartEvaluateRequest) al;
                Processor processor = rngHelper.cartEvaluateItemList(cartEvaluateRequest);
                String resp = processor.ResponseValidator.GetBodyAsText();
                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[]", "TD is not percentage");
            }
        }

    }

    @Test(dataProvider = "FlatTD_EvaluationWithNoMinCartAtRestaurantLevel", groups = {"sanity","Gaurav"},description = "FlatTD Evaluation At Restaurant level with NoMINCartAmount", enabled = true)
    public void FlatTD_EvaluationWithNoMinCartAtRestaurantLevel(String type, ArrayList al,String tid, boolean check) {
        String TDType = "Flat";
		String minCartAmount = "0.0";
		String cartTD = "200.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "FlatTD_EvaluationWithMinCartAtRestaurantLevel", groups = {"sanity","Gaurav"},description = "FlatTD Evaluation With MinCart ", enabled = true)
    public void FlatTD_EvaluationWithMinCartAtRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "500.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "FlatTD_EvaluationWithSwiggyFirstOrderRestaurantLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation with SwiggyFirstOrderRestriction ", enabled = true)
    public void FlatTD_EvaluationWithSwiggyFirstOrderRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "500.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "FlatTD_EvaluationWithRestaurantFirstOrderRestaurantLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation with RestaurantFirstOrderRestriction ", enabled = true)
    public void FlatTD_EvaluationWithRestaurantFirstOrderRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "500.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "FlatTD_EvaluationWithTimeSlotRestrictionAtRestaurantLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With TimeSlotRestriction ", enabled = true)
    public void FlatTD_EvaluationWithTimeSlotRestrictionAtRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "500.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);


    }

    @Test(dataProvider = "FlatTD_EvaluationWithUserRestrictionAtRestaurantLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With UserRestrition ", enabled = true)
    public void FlatTD_EvaluationWithUserRestrictionAtRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "500.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "FlatTD_EvaluationWithThirtyDayDormantAtRestaurantLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With ThirtyDaysDormant ", enabled = true)
    public void FlatTD_EvaluationWithThirtyDayDormantAtRestaurantLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "Flat";
		String minCartAmount = "500.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "FlatTD_EvaluationWithSixtyDayDormantAtRestaurantLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With SixtyDaysDormantRestriction ", enabled = true)
    public void FlatTD_EvaluationWithSixtyDayDormantAtRestaurantLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "Flat";
		String minCartAmount = "500.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "FlatTD_EvaluationWithNinetyDayDormantAtRestaurantLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With NinetyDaysDormantRestriction ", enabled = true)
    public void FlatTD_EvaluationWithNinetyDayDormantAtRestaurantLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "Flat";
		String minCartAmount = "500.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }

    @Test(dataProvider = "FlatTDEvaluationAtCategoryLevelDP", groups = {"sanity", "Gaurav"}, description = "FlatTD Evaluation at Category Level", enabled = true)
    public void FlatTD_EvaluateCategoryLevelTest(String type, Object al, boolean shouldTDApply, String flatTDDiscount,boolean flatTDCheck) throws IOException {

		if (shouldTDApply) {
			if (type.equals("list")) {
				List list = (ArrayList) al;
				Processor processor = rngHelper.listEvalulateV3(list.get(0).toString(), list.get(1).toString(), list.get(2).toString(), list.get(3).toString(), list.get(4).toString());
				String resp = processor.ResponseValidator.GetBodyAsText();
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Flat", "TD is not Flat");
			}
			if (type.equals("menu")) {
				List menuList = (ArrayList) al;
				Processor processor = rngHelper.menuEvaluate(menuList.get(0).toString(), menuList.get(1).toString(), menuList.get(2).toString(), menuList.get(3).toString(), menuList.get(4).toString(), menuList.get(5).toString());
				String resp = processor.ResponseValidator.GetBodyAsText();
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Flat\"]", "TD is not percentage");
			}
			if (type.equals("cart")) {
				CartEvaluateRequest cartEvaluateRequest = (CartEvaluateRequest) al;
				Processor processor = rngHelper.cartEvaluateItemList(cartEvaluateRequest);
				String resp = processor.ResponseValidator.GetBodyAsText();
			if(flatTDCheck)
			{
			  Assert.assertEquals(flatTDDiscount,"200","TD is applied but TD amount is not expected");

			}else
			{
				Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "TD is not percentage");

			}



			}
		} else {
			if (type.equals("cart")) {
				CartEvaluateRequest cartEvaluateRequest = (CartEvaluateRequest) al;
				Processor processor = rngHelper.cartEvaluateItemList(cartEvaluateRequest);
				String resp = processor.ResponseValidator.GetBodyAsText();
				Assert.assertEquals(JsonPath.read(resp, "$..data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
				System.out.println("done...");
				//processor.ResponseValidator.GetNodeValueAsJsonArray("$.data..restaurants..tradeDiscountInfo");
			}
		}
    }

	@Test(dataProvider = "FlatTDEvalutionAtSubCategoryLevel", groups = {"sanity", "Gaurav"}, description = "TD at Sub Category Level", enabled = true)
	public void FlatTD_EvaluateSubCategoryLevelTest(String type, Object al, boolean shouldTDApply, String flatTDDiscount,boolean flatTDCheck) throws IOException {
		if (shouldTDApply) {
			if (type.equals("list")) {
				List list = (ArrayList) al;
				Processor processor = rngHelper.listEvalulateV3(list.get(0).toString(), list.get(1).toString(), list.get(2).toString(), list.get(3).toString(), list.get(4).toString());
				String resp = processor.ResponseValidator.GetBodyAsText();
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Flat", "TD is not flat");
			}
			if (type.equals("menu")) {
				List menuList = (ArrayList) al;
				Processor processor = rngHelper.menuEvaluate(menuList.get(0).toString(), menuList.get(1).toString(), menuList.get(2).toString(), menuList.get(3).toString(), menuList.get(4).toString(), menuList.get(5).toString());
				String resp = processor.ResponseValidator.GetBodyAsText();
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Flat\"]", "TD is not percentage");
			}
			if (type.equals("cart")) {
				CartEvaluateRequest cartEvaluateRequest = (CartEvaluateRequest) al;
				Processor processor = rngHelper.cartEvaluateItemList(cartEvaluateRequest);
				String resp = processor.ResponseValidator.GetBodyAsText();
			if(flatTDCheck)
			{
			  Assert.assertEquals(flatTDDiscount,"20","TD is applied but TD amount is not expected");

			}else
			{
            	Assert.assertEquals(JsonPath.read(resp, "$.data.cartTradeDiscount").toString(), "0.0", "TD is not percentage");

			}



			}
		} else {
			if (type.equals("cart")) {
				CartEvaluateRequest cartEvaluateRequest = (CartEvaluateRequest) al;
				Processor processor = rngHelper.cartEvaluateItemList(cartEvaluateRequest);
				String resp = processor.ResponseValidator.GetBodyAsText();
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[]", "TD is not percentage");
			}
		}
	}

    @Test(dataProvider = "FlatTD_EvaluationWithSwiggyFirstOrderItemLevel",  groups = {"sanity","Gaurav"},description = "FlatTD Evaluation With SwiggyFirstOrder ", enabled = true)
	public void FlatTD_EvaluationWithSwiggyFirstOrderRItemLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

	}

	@Test(dataProvider = "FlatTD_EvaluationWithRestaurantFirstOrderItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With RestaurantFirstOrder ", enabled = true)
	public void FlatTD_EvaluationWithRestaurantFirstOrderItemLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);


	}

	@Test(dataProvider = "FlatTD_EvaluationWithTimeSlotRestrictionItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With TimeSlotRestriction ", enabled = true)
	public void FlatTD_EvaluationWithTimeSlotRestrictionItemLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "0.0";
		String cartTD = "50.0";
		String rewardList = "[]";
 		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

	}


    @Test(dataProvider = "FlatTD_EvaluationWithDisabledTdItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With TimeSlotRestriction ", enabled = true)
    public void FlatTD_EvaluationWithDisabledTdItemLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "100.0";
		String cartTD = "50.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

    }



    @Test(dataProvider = "FlatTD_EvaluationWithUserRestrictionAtItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With UserRestriction ", enabled = true)
	public void FlatTD_EvaluationWithUserRestrictionAtItemLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "Flat";
		String minCartAmount = "0.0";
		String cartTD = "20.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);



	}

	@Test(dataProvider = "FlatTD_EvaluationWithThirtyDayDormantAtItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With ThirtyDaysDormant ", enabled = true)
	public void FlatTD_EvaluationWithThirtyDayDormantAtItemLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "Flat";
		String minCartAmount = "0.0";
		String cartTD = "20.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);




	}

	@Test(dataProvider = "FlatTD_EvaluationWithSixtyDayDormantAtItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With SixtyDaysDormant ", enabled = true)
	public void FlatTD_EvaluationWithSixtyDayDormantAtItemLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "Flat";
		String minCartAmount = "0.0";
		String cartTD = "20.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);


	}

	@Test(dataProvider = "FlatTD_EvaluationWithNinetyDayDormantAtItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With NinetyDaysDormant ", enabled = true)
	public void FlatTD_EvaluationWithNinetyDayDormantAtItemLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "Flat";
		String minCartAmount = "0.0";
		String cartTD = "20.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);


	}

	@Test(dataProvider = "FlatTD_EvaluationWithNoMinCartItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With NinetyDaysDormant ", enabled = true)
	public void FlatTD_EvaluationWithNoMinCartItemLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "Flat";
		String minCartAmount = "0.0";
		String cartTD = "20.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);



		}

	@Test(dataProvider = "FlatTD_EvaluationWithMinCartItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With NinetyDaysDormant ", enabled = true)
	public void FlatTD_EvaluationWithMinCartItemLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "Flat";
		String minCartAmount = "100.0";
		String cartTD = "20.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);


	}


//    @Test(dataProvider = "FlatTD_EvaluationWithTimeSlotRestrictionItemLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With NinetyDaysDormant ", enabled = true)
//    public void FlatTD_EvaluationWithTimeSlotRestrictionItemLevel(String type, ArrayList al,boolean check) throws ParseException {
//        if (type.equals("list")) {
//            Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
//            String resp = processor.ResponseValidator.GetBodyAsText();
//            if (check) {
//                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Flat\"]", "TD is not percentage");
//            } else {
//                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
//            }
//        }
//        if (type.equals("menu")) {
//            Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString());
//            String resp = processor.ResponseValidator.GetBodyAsText();
//            if (check) {
//                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Flat\"]", "TD is not percentage");
//            } else {
//                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
//            }
//        }
//        if (type.equals("cart")) {
//            Processor processor = rngHelper.cartEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(), al.get(10).toString());
//            String resp = processor.ResponseValidator.GetBodyAsText();
//            if (check) {
//                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Flat\"]", "TD is not percentage");
//            } else {
//                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
//            }
//        }
//
//    }

    @Test(dataProvider = "FreebieTD_EvaluationforVersionCheck", groups = {"sanity","Gaurav"},description = "Freebie Evalution with NoMinCart At Restaurant level", enabled = true)
    public void FreebieTD_EvaluationforVersionCheck(String type, ArrayList al,String tid,String freebieitemId ,boolean check) {
		String TDType = "Freebie";
		String minCartAmount = "0.0";
		String cartTD = "0.0";
		String rewardList = "["+"\""+freebieitemId+"\""+"]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);


	}

	@Test(dataProvider = "FreebieTD_EvaluationWithNoMinCartAtRestaurantLevel", groups = {"sanity","Gaurav"},description = "Freebie Evalution with NoMinCart At Restaurant level", enabled = true)
    public void FreebieTD_EvaluationWithNoMinCartAtRestaurantLevel(String type, ArrayList al,String tid,String freebieItem ,boolean check) {
		String TDType = "Freebie";
		String minCartAmount = "0.0";
		String cartTD = "0.0";
		String rewardList = "["+"\""+freebieItem+"\""+"]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

}

    @Test(dataProvider = "FreebieTD_EvaluationWithMinCartAtRestaurantLevel", groups = {"regression","Gaurav"},description = "Freebie Evaluation with MinCart", enabled = true)
    public void FreebieTD_EvaluationWithMinCartAtRestaurantLevel(String type, ArrayList al,String tid,String freebieItem, boolean check) {
		String TDType = "Freebie";
		String minCartAmount = "100.0";
		String cartTD = "0.0";
		String rewardList = "["+"\""+freebieItem+"\""+"]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

}

   @Test(dataProvider = "FreebieTD_EvaluationWithSwiggyFirstOrderRestaurantLevel", groups = {"regression","Gaurav"},description = "Freebie Evaluation With SwiggyFirstOrder", enabled = true)
    public void FreebieTD_EvaluationWithSwiggyFirstOrderRestaurantLevel(String type, ArrayList al,String tid,String freebieItem,boolean check) {
	   String TDType = "Freebie";
	   String minCartAmount = "0.0";
	   String cartTD = "0.0";
	   String rewardList = "["+"\""+freebieItem+"\""+"]";
	   validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

}

   @Test(dataProvider = "FreebieTD_EvaluationWithRestaurantFirstOrderRestaurantLevel", groups = {"regression","Gaurav"},description = "Freebie Evaluation With RestaurantFirstOrder", enabled = true)
    public void FreebieTD_EvaluationWithRestaurantFirstOrderRestaurantLevel(String type, ArrayList al,String tid,String freebieItem,boolean check) {
	   String TDType = "Freebie";
	   String minCartAmount = "0.0";
	   String cartTD = "0.0";
	   String rewardList = "["+"\""+freebieItem+"\""+"]";
	   validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

}

    @Test(dataProvider = "FreebieTD_EvaluationWithUserRestrictionAtRestaurantLevel", groups = {"regression","Gaurav"},description = "Freebie Evaluation With UserRestriction", enabled = true)
    public void FreebieTD_EvaluationWithUserRestrictionAtRestaurantLevel(String type, ArrayList al,String tid,String freebieItem,boolean check) {
		String TDType = "Freebie";
		String minCartAmount = "0.0";
		String cartTD = "0.0";
		String rewardList = "["+"\""+freebieItem+"\""+"]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

}

    @Test(dataProvider = "FreebieTD_EvaluationWithTimeSlotRestrictionAtRestaurantLevel", groups = {"regression","Gaurav"},description = "FlatTD Evaluation With TimeSlotRestriction ", enabled = true)
    public void FreebieTD_EvaluationWithTimeSlotRestrictionAtRestaurantLevel(String type, ArrayList al,String tid,String freebieitemId ,boolean check) {
		String TDType = "Freebie";
		String minCartAmount = "0.0";
		String cartTD = "0.0";
		String rewardList = "["+"\""+freebieitemId+"\""+"]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

//
    }

    @Test(dataProvider = "FreebieTD_EvaluationWithThirtyDayDormantAtRestaurantLevel", groups = {"sanity","Gaurav"},description = "Freebie Evaluation With ThirtyDaysDormant", enabled = true)
    public void FreebieTD_EvaluationWithThirtyDayDormantAtRestaurantLevel(String type, ArrayList al,String tid,String freebieitemId,boolean check) throws ParseException {
		String TDType = "Freebie";
		String minCartAmount = "0.0";
		String cartTD = "0.0";
		String rewardList = "["+"\""+freebieitemId+"\""+"]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);


}

    @Test(dataProvider = "FreebieTD_EvaluationWithSixtyDayDormantAtRestaurantLevel", groups = {"regression","Gaurav"},description = "Freebie Evaluation With SixtyDaysDormant", enabled = true)
    public void FreebieTD_EvaluationWithSixtyDayDormantAtRestaurantLevel(String type, ArrayList al,String tid,String freebieitemId, boolean check) throws ParseException {
		String TDType = "Freebie";
		String minCartAmount = "0.0";
		String cartTD = "0.0";
		String rewardList = "["+"\""+freebieitemId+"\""+"]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

       }


    @Test(dataProvider = "FreebieTD_EvaluationWithNinetyDayDormantAtRestaurantLevel", groups = {"regression","Gaurav"},description = "Freebie Evaluation With NinetyDaysDormant", enabled = true)
    public void FreebieTD_EvaluationWithNinetyDayDormantAtRestaurantLevel(String type, ArrayList al,String tid,String freebieitemId,boolean check) throws ParseException {
		String TDType = "Freebie";
		String minCartAmount = "0.0";
		String cartTD = "0.0";
		String rewardList = "["+"\""+freebieitemId+"\""+"]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

}

    @Test(dataProvider = "FreeDeliveryTD_EvaluationWithNoMinCartAtRestaurantLevel", groups = {"sanity", "Gaurav"}, description = "FreeDelivery Evaluation With NoMinCartAmount", enabled = true)
	public void FreeDeliveryTD_EvaluationWithNoMinCartAtRestaurantLevel(String type, ArrayList al,String tid ,boolean check) {
		String TDType = "FREE_DELIVERY";
		String minCartAmount = "99.0";
		String cartTD = "0.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

	}

	@Test(dataProvider = "FreeDeliveryTD_EvaluationWithMinCartAtRestaurantLevel", groups = {"regression", "Gaurav"}, description = "FreeDelivery Evaluation With MinCartAmount", enabled = true)
	public void FreeDeliveryTD_EvaluationWithMinCartAtRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "FREE_DELIVERY";
		String minCartAmount = "99.0";
		String cartTD = "0.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

	}

    @Test(dataProvider = "FreeDeliveryTD_EvaluationWithMinCartSwiggyFirstOrderRestaurantLevel", groups = {"regression", "Gaurav"}, description = "FreeDelivery Evaluation With SwiggyFirstOrder", enabled = true)
	public void FreeDeliveryTD_EvaluationWithMinCartSwiggyFirstOrderRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "FREE_DELIVERY";
		String minCartAmount = "99.0";
		String cartTD = "0.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

	}

	@Test(dataProvider = "FreeDeliveryTD_EvaluationWithMinCartRestaurantFirstOrderRestaurantLevel", groups = {"regression", "Gaurav"}, description = "FreeDelivery Evaluation With FirstOrderRestriction", enabled = true)
	public void FreeDeliveryTD_EvaluationWithMinCartRestaurantFirstOrderRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "FREE_DELIVERY";
		String minCartAmount = "99.0";
		String cartTD = "0.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

	}

    @Test(dataProvider = "FreeDeliveryTD_EvaluationWithMinCartWithUserRestrictionRestaurantLevel", groups = {"regression", "Gaurav"}, description = "FreeDelivery Evaluation With UserRestriction", enabled = true)
	public void FreeDeliveryTD_EvaluationWithMinCartWithUserRestrictionRestaurantLevel(String type, ArrayList al,String tid,boolean check) {
		String TDType = "FREE_DELIVERY";
		String minCartAmount = "99.0";
		String cartTD = "0.0";
		String rewardList = "[]";
          		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);


	}

    @Test(dataProvider = "FreeDeliveryTD_EvaluationWithMinCartWithThirtyDaysDormantRestaurantLevel", groups = {"regression", "Gaurav"}, description = "FreeDelivery Evaluation ThirtyDaysDormant", enabled = true)
	public void FreeDeliveryTD_EvaluationWithMinCartWithThirtyDaysDormantRestaurantLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "FREE_DELIVERY";
		String minCartAmount = "99.0";
		String cartTD = "0.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

	}

	@Test(dataProvider = "FreeDeliveryTD_EvaluationWithMinCartWithSixtyDaysDormantRestaurantLevel", groups = {"regression", "Gaurav"}, description = "FreeDelivery Evaluation ThirtyDaysDormant", enabled = true)
	public void FreeDeliveryTD_EvaluationWithMinCartWithSixtyDaysDormantRestaurantLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "FREE_DELIVERY";
		String minCartAmount = "99.0";
		String cartTD = "0.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);
	}


	@Test(dataProvider = "FreeDeliveryTD_RestaurantBillAmount", groups = {"regression", "Gaurav"}, description = "Verifying FreeDelivery with restuarantbillamount", enabled = true)
	public void FreeDeliveryTD_RestaurantBillAmount(int restId, int minCart, int restBillAmt,int freedelMinCart) throws ParseException, IOException {

		verifyRestBillAmount(minCart,freedelMinCart,restBillAmt,"FREE_DELIVERY",restId);
	}
	@Test(dataProvider = "FreeBie_RestaurantBillAmount", groups = {"regression", "Gaurav"}, description = "Verifying FreeBie with restuarantbillamount", enabled = true)
	public void FreeBieTD_RestaurantBillAmount(int restId, int minCart, int restBillAmt,int freedelMinCart) throws ParseException, IOException {

		verifyRestBillAmount(minCart,freedelMinCart,restBillAmt,"Freebie",restId);
	}

	public Processor getEvalCart(int restBillAmt,int minCart,int restId) throws IOException
	{
		evaluateCartV3.setRestaurantBillAmount(restBillAmt);
		evaluateCartV3.setMinCartAmount(minCart);
		ItemRequest itemRequest = new ItemRequest();
		itemRequest.build(restId,121,111,Integer.parseInt("121"));
		List<ItemRequest> list = new ArrayList<>();
		list.add(itemRequest);
		evaluateCartV3.setItemRequests(list);
		return processor = helper.evaluateCartHelperPojo(jsonHelper.getObjectToJSON(evaluateCartV3));
	}


	public void verifyRestBillAmount(int minCart,int freedelMinCart,int restBillAmt,String tdType,int restId) throws IOException
	{
		Processor processor= getEvalCart(restBillAmt,minCart,restId);
		if(minCart>=freedelMinCart && restBillAmt>=freedelMinCart)
		{
			validatePresenceOfTD(processor,tdType);
		}
		else if(minCart<=freedelMinCart && restBillAmt>=freedelMinCart)
		{
			validatePresenceOfTD(processor,tdType);
		}
		else if(restBillAmt<=freedelMinCart && minCart>=freedelMinCart)
		{
			validateNoTDPresent(processor,tdType);
		}
		else if(restBillAmt<=freedelMinCart && minCart <=freedelMinCart)
		{
			validateNoTDPresent(processor,tdType);
		}
		else
			validateNoTDPresent(processor,tdType);
	}

	public void validatePresenceOfTD(Processor proccess,String TDType)
	{
		List<String> TDs = JsonPath.read(proccess.ResponseValidator.GetBodyAsText(),
				"$..data..restaurants[*]..tradeDiscountInfo[*]..discountType");
		if(TDs.isEmpty())
		{
			Assert.fail("TD "+TDType+ " is empty");
		}
		else
			Assert.assertEquals(TDs.get(0),TDType, TDType + " TD has not come in cart response");

	}
	public void validateNoTDPresent(Processor proccess, String TDType)
	{
		List<String> TDs = JsonPath.read(proccess.ResponseValidator.GetBodyAsText(),
				"$.data.restaurants[*].tradeDiscountInfo[*]");
		if(!TDs.isEmpty())
		{
			Assert.fail("TD has seen while giving restaurantBillAmount as  "+TDType);
		}
		else
			Assert.assertTrue(true);
		}


	@Test(dataProvider = "FreeDeliveryTD_EvaluationWithMinCartWithNINETYDaysDormantRestaurantLevel", groups = {"regression", "Gaurav"}, description = "FreeDelivery Evaluation ThirtyDaysDormant", enabled = true)
	public void FreeDeliveryTD_EvaluationWithMinCartWithNINETYDaysDormantRestaurantLevel(String type, ArrayList al,String tid,boolean check) throws ParseException {
		String TDType = "FREE_DELIVERY";
		String minCartAmount = "99.0";
		String cartTD = "0.0";
		String rewardList = "[]";
		validation(type,al,tid,TDType,minCartAmount,cartTD,rewardList,check);

	}
//
//	@Test(dataProvider = "FreeDeliveryTD_EvaluationWithMinCartWithTimeSlotRestrictionRestaurantLevel", groups = {"regression", "Gaurav"}, description = "FreeDelivery Evaluation ThirtyDaysDormant", enabled = true)
//	public void FreeDeliveryTD_EvaluationWithMinCartWithTimeSlotRestrictionRestaurantLevel(String type, ArrayList al,boolean check) throws ParseException {
//		if (type.equals("list")) {
//			Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
//			String resp = processor.ResponseValidator.GetBodyAsText();
//			if (check) {
//				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"FREE_DELIVERY\"]", "TD is not percentage");
//			} else {
//				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
//			}
//		}
//		if (type.equals("menu")) {
//			Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString());
//			String resp = processor.ResponseValidator.GetBodyAsText();
//			if (check) {
//				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"FREE_DELIVERY\"]", "TD is not percentage");
//			} else {
//				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
//			}
//		}
//		if (type.equals("cart")) {
//			Processor processor = rngHelper.cartEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(), al.get(10).toString());
//			String resp = processor.ResponseValidator.GetBodyAsText();
//			if (check) {
//				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"FREE_DELIVERY\"]", "TD is not percentage");
//
//			} else {
//				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
//
//			}
//		}
//	}new Integer(rm.nextInt()).toString();
//

	@Test(dataProvider = "FreeDeliveryTD_EvaluationWithSpecialFee", groups = {"sanity", "Gaurav"}, description = "FreeDelivery Evaluation With SpecialFee", enabled = true)
	public void FreeDeliveryTD_EvaluationWithSpecialFeeWITHSURGEFEE(String type, ArrayList al,boolean isApply) {
			if (type.equals("list")) {
				Processor processor = rngHelper.freedeliverylistevaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(),al.get(6).toString(),al.get(7).toString(),al.get(8).toString(),al.get(9).toString(),al.get(10).toString());
				String resp = processor.ResponseValidator.GetBodyAsText();
				if(isApply)
				{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList..tradeDiscountInfoList..discountType").toString(), "[\"FREE_DELIVERY\"]", "TD is not FREE_DELIVERY");
			    }
				else
			       {
			      	Assert.assertEquals(JsonPath.read(resp, "$..data..discountList..tradeDiscountInfoList..discountType").toString(), "[]", "TD is not FREE_DELIVERY");
					}
			}
			if (type.equals("menu")) {
				Processor processor = rngHelper.freedeliverymenuevaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(),al.get(6).toString(),al.get(7).toString(),al.get(8).toString(),al.get(9).toString(),al.get(10).toString(),al.get(11).toString());
				String resp = processor.ResponseValidator.GetBodyAsText();
				if(isApply)
				{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"FREE_DELIVERY\"]", "TD is not FREE_DELIVERY");
			    }
				else
			       {
			      	Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[]", "TD is not FREE_DELIVERY");
					}
				}
			if (type.equals("cart")) {
				Processor processor = rngHelper.freedeliverycartevaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(), al.get(10).toString(),al.get(11).toString(),al.get(12).toString(),al.get(13).toString(),al.get(14).toString(),al.get(15).toString(),al.get(16).toString());
				String resp = processor.ResponseValidator.GetBodyAsText();
				if(isApply)
				{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..discountType").toString(), "[\"FREE_DELIVERY\"]", "TD is not FREE_DELIVERY");
			    }
				else
			       {
			      	Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[]", "TD is not FREE_DELIVERY");
					}
			}
		}

	@Test(dataProvider = "FreeDeliveryTD_EvaluationWithSpecialFeeWithoutSURGEFEE", groups = {"sanity", "Gaurav"}, description = "FreeDelivery Evaluation With SpecialFee", enabled = true)
	public void FreeDeliveryTD_EvaluationWithSpecialFeeWithoutSURGEFEE(String type, ArrayList al,boolean isApply) {
		if (type.equals("list")) {
			Processor processor = rngHelper.freedeliverylistevaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(),al.get(6).toString(),al.get(7).toString(),al.get(8).toString(),al.get(9).toString(),al.get(10).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(isApply)
			{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList..tradeDiscountInfoList..discountType").toString(), "[\"FREE_DELIVERY\"]", "TD is not FREE_DELIVERY");
			}
			else
			{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList..tradeDiscountInfoList[*].discountType").toString(), "[]", "TD is not FREE_DELIVERY");
			}
		}
		if (type.equals("menu")) {
			Processor processor = rngHelper.freedeliverymenuevaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(),al.get(6).toString(),al.get(7).toString(),al.get(8).toString(),al.get(9).toString(),al.get(10).toString(),al.get(11).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(isApply)
			{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"FREE_DELIVERY\"]", "TD is not FREE_DELIVERY");
			}
			else
			{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[]", "TD is not FREE_DELIVERY");
			}
		}
		if (type.equals("cart")) {
			Processor processor = rngHelper.freedeliverycartevaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(), al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(), al.get(10).toString(),al.get(11).toString(),al.get(12).toString(),al.get(13).toString(),al.get(14).toString(),al.get(15).toString(),al.get(16).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(isApply)
			{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..discountType").toString(), "[\"FREE_DELIVERY\"]", "TD is not FREE_DELIVERY");
			}
			else
			{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[]", "TD is not FREE_DELIVERY");
			}
		}
	}


	@Test(dataProvider = "PercentageTD_EvaluationWithSwiggyFirstOrderItemLevel",  groups = {"sanity","regression","Gaurav"},description = "FlatTD Evaluation With MinCart ", enabled = true)
	public void PercentageTD_EvaluationWithSwiggyFirstOrderRItemLevel(String type, ArrayList al,boolean check) {
		if(type.equals("list"))
		{
			Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("menu"))
		{
			Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("cart"))
		{
			Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo..discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}

	}

	@Test(dataProvider = "PercentageTD_EvaluationWithRestaurantFirstOrderItemLevel", groups = {"sanity","regression","Gaurav"},description = "FlatTD Evaluation With MinCart ", enabled = true)
	public void PercentageTD_EvaluationWithRestaurantFirstOrderItemLevel(String type, ArrayList al,boolean check) {
		if(type.equals("list"))
		{
			Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");			}
		}
		if(type.equals("menu"))
		{
			Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("cart"))
		{
			Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
	}

	@Test(dataProvider = "PercentageTD_EvaluationWithUserRestrictionAtItemLevel", groups = {"sanity","regression","Gaurav"},description = "FlatTD Evaluation With MinCart ", enabled = true)
	public void PercentageTD_EvaluationWithUserRestrictionAtItemLevel(String type, ArrayList al,boolean check) {
		if(type.equals("list"))
		{
			Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("menu"))
		{
			Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("cart"))
		{
			Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}

	}

	@Test(dataProvider = "PercentageTD_EvaluationWithThirtyDayDormantAtItemLevel", groups = {"sanity","regression","Gaurav"},description = "FlatTD Evaluation With MinCart ", enabled = true)
	public void PercentageTD_EvaluationWithThirtyDayDormantAtItemLevel(String type, ArrayList al,boolean check) throws ParseException {
		if(type.equals("list"))
		{
			Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("menu"))
		{
			Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("cart"))
		{
			Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
	}

	@Test(dataProvider = "PercentageTD_EvaluationWithSixtyDayDormantAtItemLevel", groups = {"sanity","regression","Gaurav"},description = "FlatTD Evaluation With MinCart ", enabled = true)
	public void PercentageTD_EvaluationWithSixtyDayDormantAtItemLevel(String type, ArrayList al,boolean check) throws ParseException {
		if(type.equals("list"))
		{
			Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("menu"))
		{
			Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("cart"))
		{
			Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
	}

	@Test(dataProvider = "PercentageTD_EvaluationWithNinetyDayDormantAtItemLevel", groups = {"sanity","regression","Gaurav"},description = "FlatTD Evaluation With MinCart ", enabled = true)
	public void PercentageTD_EvaluationWithNinetyDayDormantAtItemLevel(String type, ArrayList al,boolean check) throws ParseException {
		if(type.equals("list"))
		{
			Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");			}
		}
		if(type.equals("menu"))
		{
			Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}
		if(type.equals("cart"))
		{
			Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
			String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
			}else{
				Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
			}
		}

	}


    @Test(dataProvider = "PercentageTD_EvaluationWithNoMinCartItemLevel", groups = {"sanity","regression","Gaurav"},description = "FlatTD Evaluation With MinCart ", enabled = true)
    public void PercentageTD_EvaluateWithNoMinCartAmountAtItemLevel(String type, ArrayList al,boolean check) throws ParseException
	{

        if(type.equals("list"))
        {
            Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            if(check) {
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");
            }else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
            }
        }
        if(type.equals("menu"))
        {
            Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            if(check) {
                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
            }else{
                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
            }
        }
        if(type.equals("cart"))
        {
            Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            if(check) {
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
            }else{
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
            }
        }



    }

    @Test(dataProvider = "PercentageTD_EvaluationWithMinCartItemLevel", groups = {"sanity","regression","Gaurav"},description = "FlatTD Evaluation With MinCart ", enabled = true)
    public void PercentageTD_EvaluateWithMinCartAmountAtItemLevel(String type, ArrayList al,boolean check) throws ParseException
    {

        if(type.equals("list"))
        {
            Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();

			if(check) {
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");
            }else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
            }
        }
        if(type.equals("menu"))
        {
            Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            if(check) {
                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
            }else{
                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
            }
        }
        if(type.equals("cart"))
        {
            Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            if(check) {
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
            }else{
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
            }
        }



    }

    @Test(dataProvider = "PercentageTD_EvaluationWithTimeSlotRestrictionItemLevel",description = "Listing Evaluation with Percentage , min Limit & time slot restriction at restaurant level", groups = {"regression","Gaurav"}, enabled = true)
    public void PercentageTD_EvaluationWithTimeSlotRestrictionItemLevel(String type, ArrayList al,boolean check) {


        if(type.equals("list"))
        {
            Processor processor = rngHelper.listEvalulateV3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
			if(check) {
				String get_tdil_discountType = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..data..discountList..tradeDiscountInfoList..discountType").replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
				Assert.assertEquals(get_tdil_discountType, "Percentage", "TD is not percentage");
            }else{
				Assert.assertEquals(JsonPath.read(resp, "$..data..discountList[*]").toString(), "[]", "tradeDiscountInfo object has discount");
            }
        }
        if(type.equals("menu"))
        {
            Processor processor = rngHelper.menuEvaluate(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            if(check) {
                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
            }else{
                Assert.assertEquals(JsonPath.read(resp, "$.data.tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
            }
        }
        if(type.equals("cart"))
        {
            Processor processor = rngHelper.cartEvaluatev3(al.get(0).toString(), al.get(1).toString(), al.get(2).toString(), al.get(3).toString(), al.get(4).toString(),al.get(5).toString(), al.get(6).toString(), al.get(7).toString(), al.get(8).toString(), al.get(9).toString(),al.get(10).toString());
            String resp = processor.ResponseValidator.GetBodyAsText();
            if(check) {
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*].discountType").toString(), "[\"Percentage\"]", "TD is not percentage");
            }else{
                Assert.assertEquals(JsonPath.read(resp, "$.data..restaurants..tradeDiscountInfo[*]").toString(), "[]", "tradeDiscountInfo object has discount");
            }
        }
    }


    @Test(dataProvider="PercentageTD_UpperCappingChanges" )
	public void PercentageTD_UpperCappingChanges(String type, Object al,boolean isMultiITemsCart,String discountOn1,String discountOn2,String totalTD) throws IOException
	{

		if(type.equals("cart"))
		{
			CartEvaluateRequest cartEvaluateRequest = (CartEvaluateRequest)al;
			Processor processor = rngHelper.cartEvaluateItemList(cartEvaluateRequest);
			String resp = processor.ResponseValidator.GetBodyAsText();

		if(isMultiITemsCart)
		{
			Assert.assertEquals(JsonPath.read(resp, "$.data.restaurants..itemList[0].restaurantTradeDiscount").toString(),"["+discountOn1+"]");

            Assert.assertEquals(JsonPath.read(resp, "$.data.restaurants..itemList[1].restaurantTradeDiscount").toString(),"["+discountOn2+"]");

			Assert.assertEquals(JsonPath.read(resp, "$.data..cartTradeDiscount").toString(),"["+totalTD+"]");



		}
		else
		{
			Assert.assertEquals(JsonPath.read(resp, "$.data.restaurants..itemList[0].restaurantTradeDiscount").toString(),"["+discountOn1+"]");
			Assert.assertEquals(JsonPath.read(resp, "$.data..cartTradeDiscount").toString(),"["+totalTD+"]");

		}

		}
	}
}