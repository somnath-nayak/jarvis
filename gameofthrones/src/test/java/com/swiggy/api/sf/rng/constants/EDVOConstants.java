package com.swiggy.api.sf.rng.constants;

public interface EDVOConstants {


	public static String namespace = "\"EDVO_Test\"";
	public static String header = "\"EDVO_Test_AUTOMATION\"";
	public static String description ="\"EDVO_Test_DESCRIPTION\"";
	public static String shortDescription = "\"EDVO_SHROT_DESCRIPTION\"";
	public static String campaign_type= "\"BXGY\"";
	public static String restaurant_hit= "100";
	public static String enabled= "true";
	public static String swiggy_hit= "0";
	public static String createdBy= "\"AY\"";
	public static String discountCap= "1000";
	public static String minCartAmount= "10";
	public static String commissionOnFullBill= "false";
	public static String taxesOnDiscountedBill= "false";
	public static String firstOrderRestriction= "false";
	public static String timeSlotRestriction= "false";
	public static String dormant_user_type= "ZERO_DAYS_DORMANT";
	public static String restaurantFirstOrder= "false";
	public static String userRestriction= "false";
	public static String selectEDVOCampShortDescription = "select * from campaign where id = '";
	public static String endWithSemiColon = ";" ;
	public static String endQuote = "'";
	 static int campValidTillTime = 86400000;
	 public static String eCampaignDatesInValid="E_CAMPAIGN_DATES_INVALID";
	 public static String alreadyRunningCampaignMessage="Already running Campaign for one of the given meals on another Restaurant( Restaurant already has an active campaign";
	public static String campTypeOfMealBXGYOnly="Campaign Type For Meals should be BXGY only";
	public static String deleteEDVOWithRestId="delete from campaign_restaurant_map where restaurant_id='";
	 String headerEDVOMultiTd = "EDVO_Test_AUTOMATION";
	 Integer edvoMinCartAmount = 99;

}
