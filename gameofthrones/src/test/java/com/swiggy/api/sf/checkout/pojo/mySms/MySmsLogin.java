package com.swiggy.api.sf.checkout.pojo.mySms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;


@JsonPropertyOrder({
        "msisdn",
        "password",
        "apiKey"
})
public class MySmsLogin {

    @JsonProperty("msisdn")
    private String msisdn;
    @JsonProperty("password")
    private String password;
    @JsonProperty("apiKey")
    private String apiKey;

    /**
     * No args constructor for use in serialization
     *
     */
    public MySmsLogin() {
    }

    /**
     *
     * @param msisdn
     * @param password
     * @param apiKey
     */
    public MySmsLogin(String msisdn, String password, String apiKey) {
        super();
        this.msisdn = msisdn;
        this.password = password;
        this.apiKey = apiKey;
    }

    @JsonProperty("msisdn")
    public String getMsisdn() {
        return msisdn;
    }

    @JsonProperty("msisdn")
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("apiKey")
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty("apiKey")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("msisdn", msisdn).append("password", password).append("apiKey", apiKey).toString();
    }

}