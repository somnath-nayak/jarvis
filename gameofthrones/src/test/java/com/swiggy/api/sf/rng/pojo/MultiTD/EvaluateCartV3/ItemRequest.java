package com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "categoryId",
        "count",
        "itemId",
        "itemKey",
        "price",
        "restaurantId",
        "subCategoryId"
})
public class ItemRequest {

    @JsonProperty("categoryId")
    private Integer categoryId;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("itemId")
    private Integer itemId;
    @JsonProperty("itemKey")
    private String itemKey;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("restaurantId")
    private Integer restaurantId;
    @JsonProperty("subCategoryId")
    private Integer subCategoryId;

    @JsonProperty("categoryId")
    public Integer getCategoryId() {
        return categoryId;
    }

    @JsonProperty("categoryId")
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("itemId")
    public Integer getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    @JsonProperty("itemKey")
    public String getItemKey() {
        return itemKey;
    }

    @JsonProperty("itemKey")
    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("restaurantId")
    public Integer getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurantId")
    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    @JsonProperty("subCategoryId")
    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    @JsonProperty("subCategoryId")
    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public void setDefaultValues(int restID, int categoryID, int subCatID, int itemID) {
        if (this.getCategoryId() == null)
            this.setCategoryId(categoryID);
        if (this.getCount() == null)
            this.setCount(2);
        if (this.getItemId() == null)
            this.setItemId(itemID);
        if (this.getPrice() == null)
            this.setPrice(100);
        if (this.getRestaurantId() == null)
            this.setRestaurantId(restID);
        if (this.getSubCategoryId() == null)
            this.setSubCategoryId(subCatID);
    }

    public ItemRequest build(int restID, int categoryID, int subCatID, int itemID) {
        setDefaultValues(restID, categoryID, subCatID, itemID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("categoryId", categoryId).append("count", count).append("itemId", itemId).append("itemKey", itemKey).append("price", price).append("restaurantId", restaurantId).append("subCategoryId", subCategoryId).toString();
    }

}
