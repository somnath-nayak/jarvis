package com.swiggy.api.sf.snd.tests;

import com.swiggy.api.sf.snd.helper.SANDHelper;
import org.testng.Reporter;
import org.testng.annotations.Test;

import framework.gameofthrones.JonSnow.Processor;

public class SmokeTest {
	String holidaySlotID;

	@Test (priority = 1)
	public void login(){
		String[] payload = new String[]{"8085798950","welcome123"};
		int statusCode = 0;
	    String statusMessage = "done successfully";
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.login(payload);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetNodeValue("data.juspay.cards.merchantId"));
	}

	@Test (priority = 2)
	public void getBlackZone(){
		String headersAuthorization = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
		int statusCode = 0;
	    String statusMessage = "done successfully";
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.getBlackZone(headersAuthorization);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 3)
	public void getListingV3(){
		String lat = "13.057878";
		String lng = "80.254562";
		String headersTID = "7d0d1e17-2488-4f90-9f3f-0035506bef3b";
		String headersToken = "3e27f6dc-9d1c-4534-835d-1a739d71a4d589b85264-8d71-48e1-80c7-7a1e791ee574";
		int statusCode = 0;
	    String statusMessage = "done successfully";
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.getListingV3(lat, lng, headersTID, headersToken);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 4)
	public void getListingV2(){
		String lat = "12.9275374";
		String lng = "80.25035100000002";
		String headersTID = "7d0d1e17-2488-4f90-9f3f-0035506bef3b";
		String headersToken = "3e27f6dc-9d1c-4534-835d-1a739d71a4d589b85264-8d71-48e1-80c7-7a1e791ee574";
		String headersDeviceID = "c440f731-795a-4549-859f-2137472c8cdf";
		int statusCode = 0;
	    String statusMessage = "done successfully";
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.getListingV2(lat, lng, headersTID, headersToken, headersDeviceID);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}
	
	@Test (priority = 5)
	public void listingDebug(){
		String lat = "28.46331920528186";
		String lng = "77.09471042766108";
		String restID = "1658";
		String headersXDebugAuth = "2mV36sXEW49X6hR8";
		int statusCode = 0;
	    String statusMessage = "done successfully";
	    
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.listingDebug(lat, lng, restID, headersXDebugAuth);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 6)
	public void aggregatorCall(){
		String restaurantListingLat = "12.933";
		String restaurantListingLng = "77.601";
		String collectionLat = "12.933";
		String collectionLng = "77.601";
		String carouselLat = "12.933";
		String carouselLng = "77.601";
		int statusCode = 0;
	    String statusMessage = "done successfully";
		
		String[] payload = {restaurantListingLat, restaurantListingLng, collectionLat, collectionLng, carouselLat, carouselLng};
//		String[] payload = new String[]{"12.933","77.601","12.933","77.601","12.933","77.601"};
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.aggregator(payload);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}
	
	@Test (priority = 7)
	public void aggregator(){
		String restaurantListingLat = "12.933";
		String restaurantListingLng = "77.601";
		String collectionLat = "12.933";
		String collectionLng = "77.601";
		String carouselLat = "12.933";
		String carouselLng = "77.601";
		int statusCode = 0;
	    String statusMessage = "done successfully";
	    
		String[] payload = {restaurantListingLat, restaurantListingLng};
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.aggregator(payload);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}
	
	@Test (priority = 8)
	public void aggregatorIBC(){
		String restaurantListingLat = "17.4484363";
		String restaurantListingLng = "78.3741361";
		String collectionLat = "17.4484363";
		String collectionLng = "78.3741361";
		String carouselLat = "17.4484363";
		String carouselLng = "78.3741361";
		int statusCode = 0;
	    String statusMessage = "done successfully";
		
		String[] payload = {restaurantListingLat, restaurantListingLng, collectionLat, collectionLng, carouselLat, carouselLng};
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.aggregator(payload);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}
	


	@Test (priority = 10)
	public void restaurantMenu(){
		String restID = "222";
		int statusCode = 0;
	    String statusMessage = "done successfully";
	    
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.getRestaurantMenu(restID);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

//	@Test (priority = 11)
	public void getPolygon(){
		String[] restaurantID = {"2590"}; //Query Param
		String headersToken = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
		int statusCode = 0;
	    String statusMessage = "Polygon Id found";
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.getPolygon(restaurantID, headersToken);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 12)
	public void getResraurantHolidaySlots(){
		String[] restaurantID = {"9990"}; //Query Param
		String headersAuthorization = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
		int statusCode = 0;
	    String statusMessage = "done successfully";
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.getResraurantHolidaySlots(restaurantID, headersAuthorization);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 13)
	public void createResraurantHolidaySlots(){
		String[] payload = {"9990", "2017-11-10", "00:00:00.0", "2017-11-25", "06:00:00.0"};
		String headersAuthorization = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
		int statusCode = 0;
	    String statusMessage = "done successfully";
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.createRestaurantHolidaySlots(payload, headersAuthorization);
		sh.smokeCheck(statusCode, statusMessage, processor);
		holidaySlotID = String.valueOf(processor.ResponseValidator.GetNodeValueAsInt("data.id"));
		Reporter.log("HID: " + processor.ResponseValidator.GetNodeValueAsInt("data.id"), true);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 14, dependsOnMethods = {"createResraurantHolidaySlots"})
	public void updateResraurantHolidaySlots(){
		String[] payload = {holidaySlotID,"9990", "2018-11-10", "00:00:00.0", "2018-11-25", "06:00:00.0"};
		String headersAuthorization = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
		int statusCode = 0;
	    String statusMessage = "done successfully";
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.updateRestaurantHolidaySlots(payload, headersAuthorization);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}
	
	@Test (priority = 15, dependsOnMethods = {"createResraurantHolidaySlots"})
	public void deleteResraurantHolidaySlots(){
		String[] holidaySlotID = {this.holidaySlotID};
		String headersAuthorization = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
		int statusCode = 0;
	    String statusMessage = "deleted successfully";
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.deleteRestaurantHolidaySlots(holidaySlotID, headersAuthorization);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 16)
	public void favorites(){
		String lat = "12.936515";
		String lng = "77.605567";
		String headersCustomerID = "201966";
		int statusCode = 0;
	    String statusMessage = "done successfully";
		
		SANDHelper sh = new SANDHelper();
		Processor processor = sh.favorites(lat, lng, headersCustomerID);
		sh.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

}
