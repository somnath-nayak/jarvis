package com.swiggy.api.sf.snd.constants;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.constants
 **/
public interface POPConstants {
    String lat = "12.9352156";
    String lng = "77.618562";
    String[] days = {"MON","TUE","WED","THU","FRI","SAT","SUN"};
    String city = "bangalore";
    String area_code = "1";
    String msg_doneSuccess = "done successfully";
    String msg_success = "SUCCESS";
    String serviceable_with_banner = "SERVICEABLE_WITH_BANNER";
    int status0 = 0;
    int status1 = 1;
    String polygonId = "2781";
    String restId = "10820";
    String cmsDB="cms";
    String deliveryDB = "deliverydb";
    String slot_type = "LUNCH";
    String serviceable_str = "SERVICEABLE";
    String serviceable_with_banner_str = "SERVICEABLE_WITH_BANNER";
    String menu_type = "POP_MENU";
    String search_str_item = "Test-Item";
    String search_str_rest = "Burrito";
    int openTime = 0;
    int closeTime = 2330;
    String get_area_schedule_id_open_close_time_db = "SELECT id, open_time, close_time FROM swiggy.area_schedule WHERE area_id=${AREA} AND day='${DAY}' AND type='POP_MENU'";
    String delete_conflicting_area_schedule_db = "DELETE FROM swiggy.area_schedule WHERE id=";
    String get_area_schedule_by_id = "SELECT * FROM swiggy.area_schedule WHERE area_id=${AREA} AND day='${DAY}'";
    String get_area_slot_db = "select * FROM swiggy.area_slots WHERE area_id=${AREA} AND day='${DAY}';";
    String delete_conflicting_area_slot_db = "DELETE FROM swiggy.area_slots WHERE id=";
    String get_area_slot_by_id_db = "SELECT * FROM swiggy.area_slots WHERE id=";
    String enable_created_restaurant = "UPDATE swiggy.restaurants SET enabled=1 WHERE id=";
    String get_rest_details = "SELECT * FROM swiggy.restaurants WHERE id=";
    String map_restaurant_polygon = "INSERT INTO delivery.restaurant_polygon_mapping (polygon_id, restaurant_id) VALUES(${POLYGON}, ${REST})";
    String get_data_restaurant_polygon_map = "SELECT * FROM delivery.restaurant_polygon_mapping where restaurant_id=${REST} AND polygon_id=${POLYGON}";
    String avail_item_schedule_item_redis = "[{\"id\":${ID},\"itemId\"::${ITEMID},\"areaScheduleId\":${AREASCHEDULE},\"cityScheduleId\":null,\"date\":\"${DATE}\"}]";
    int redis_index_0 = 0;
    String AVAILABILITY_ITEM_SCHEDULE_ITEM_ID = "AVAILABILITY_ITEM_SCHEDULE_ITEM_ID_";
    String AVAILABILITY_AREA_SCHEDULE = "AVAILABILITY_AREA_SCHEDULE_";
    String avail_area_schedule_redis = "[{\"id\":${ID},\"day\":\"${DAY}\",\"openTime\":${OPEN},\"closeTime\":${CLOSE},\"type\":\"POP_MENU\",\"areaId\":${AREAID}}]";
    String get_item_schedule_mapping = "SELECT * FROM swiggy.item_schedule_mapping WHERE item_id=";
    String get_menu_id_from_item_menu_map  = "SELECT menu_id FROM swiggy.item_menu_map WHERE item_id=";
}
