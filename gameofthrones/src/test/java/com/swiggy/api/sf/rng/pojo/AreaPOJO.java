package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;

import com.swiggy.api.sf.rng.pojo.couponcartevaluate.AreaEntity;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "areaEntity"
})
public class AreaPOJO {

    @JsonProperty("areaEntity")
    private AreaEntityPOJO areaEntity;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public AreaPOJO() {
    }

    /**
     *
     * @param areaEntity
     */
    public AreaPOJO(AreaEntityPOJO areaEntity) {
        super();
        this.areaEntity = areaEntity;
    }

    @JsonProperty("areaEntity")
    public AreaEntityPOJO getAreaEntity() {
        return areaEntity;
    }

    @JsonProperty("areaEntity")
    public void setAreaEntity(AreaEntityPOJO areaEntity) {
        this.areaEntity = areaEntity;
    }

    public AreaPOJO withAreaEntity(AreaEntityPOJO areaEntity) {
        this.areaEntity = areaEntity;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AreaPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public AreaPOJO setDefault() {
        AreaEntityPOJO areaEntityPOJO = new AreaEntityPOJO().setDefault();
        return this.withAreaEntity(areaEntityPOJO);
    }
}
