package com.swiggy.api.sf.rng.helper;

import com.swiggy.api.sf.rng.pojo.tdcaching.OperationModel;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Assert;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.swiggy.api.sf.rng.constants.CacheConstants.RESTAURANT_REDIS_KEY;

public class TdCachingHelper {

    static Initialize gameofthrones = new Initialize();
    RngHelper rngHelper = new RngHelper();

    private static String testRestaurantId = "0000000";

    public static String getKey(String prefix, String hashValue) {
        return prefix + hashValue;
    }

    public static String getRestaurantIds() {
        return String.valueOf(Utility.getRandom(10000000, 200000000));
    }

    public static String getCategoryIds() {
        return String.valueOf(Utility.getRandom(1000000, 20000000));
    }

    public static String getSubsCategoryIds() {
        return String.valueOf(Utility.getRandom(100000, 2000000));
    }
    public static String getItemIds() {
        return String.valueOf(Utility.getRandom(10000, 20000));
    }

    public static String getUserId() {
        return String.valueOf(Utility.getRandom(1, 2000000));
    }

    private static TDRedisHelper tdRedisHelper = new TDRedisHelper();


    public void evictKeyFromGuava() {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("td", "evictGuava", gameofthrones);
        Processor processor = new Processor(service);

    }

    public void createUserMapping(String testCampaignId, String testUserId) {
        String date = Utility.getPresentDate();
        TDCachingDbHelper.createUserMapping(testCampaignId, testUserId, date);

    }

    public void evaluateMenu(String restaurantId, String userId, String userAgent, String versionCode, String expectedResult) {
        String minCartAmount = "0";

        Processor processor = rngHelper.menuEvaluate(minCartAmount, restaurantId, "false", userId, userAgent, versionCode);
//        String resp = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

//        Assert.assertEquals(resp, expectedResult);
    }

    public void evaluateCart(String restaurantId, String userId, String userAgent,
                              String versionCode, String expectedResult) {
        Processor processor = rngHelper.cartEvaluate(restaurantId, "233", "2331", "12233", "1", "100",
                "100", userId, "false",   userAgent, versionCode);
        String resp = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount");

        Assert.assertEquals(resp, expectedResult);
    }

    public void checkRedisData(String restaurantRedisKey, String campaignId, String restaurantId) throws IOException {
        String redisValue = tdRedisHelper.getKey(getKey(restaurantRedisKey, restaurantId));
        ObjectMapper objectMapper = new ObjectMapper();
        List<OperationModel> result = Arrays.asList(objectMapper.readValue(redisValue, OperationModel[].class));

        Assert.assertEquals(1, result.size());
        if (result.size() > 0) {
            Assert.assertTrue(String.valueOf(result.get(0).getCampaignId()).equals(campaignId));
        }
    }

    public List<String> getRuleIdsByCampaignId(String campaignId) {
        return TDCachingDbHelper.getRuleIdsByCampaign(campaignId);
    }

    public void deleteDataFromCacheForRestaurant(String restaurantId) {
        evictKeyFromGuava();
        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, restaurantId));
    }
}
