
package com.swiggy.api.sf.rng.pojo.pitara;

import org.codehaus.jackson.annotate.JsonProperty;

public class LaunchCARD {

    @JsonProperty("count")
    private Integer count;

    public LaunchCARD() {
    }

    public LaunchCARD(Integer count) {
        super();
        this.count = count;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    public LaunchCARD withCount(Integer count) {
        this.count = count;
        return this;
    }

}
