package com.swiggy.api.sf.rng.helper;

import com.swiggy.api.sf.rng.pojo.SwiggySuper.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.apache.tomcat.util.buf.StringUtils;
import org.testng.Assert;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;

public class SuperHelper {
	static Initialize gameofthrones = new Initialize();
	SuperDbHelper superDbHelper = new SuperDbHelper();
	public String[] populatePlanPayload(HashMap<String, String> createPlanData, String numOfPlans) {
		String[] planPayload = new String[Integer.parseInt(numOfPlans)];
		for (int i = 0; i < Integer.parseInt(numOfPlans); i++) {
			planPayload[i] = new CreatePlan(createPlanData).toString();
		}

		return planPayload;
	}

	public String[] populateFuturePlanPayload(HashMap<String, String> createPlanData, String numOfPlans) {
		String[] planPayload = new String[Integer.parseInt(numOfPlans)];
		for (int i = 0; i < Integer.parseInt(numOfPlans); i++) {
			planPayload[i] = new CreatePlan(createPlanData.get("0"),createPlanData.get("1"),createPlanData.get("2"),createPlanData.get("3")).toString();
		}

		return planPayload;
	}

	public String[] populateActivePlanPayloadWithDefaultValues(HashMap<String, String> createPlan, String numOfPlans) {
		// validFromDate, numOfMonthValidity, planTenure, planPrice, renewalOffSetDays)
		String createPlanData = "";
		String[] planPayload = new String[Integer.parseInt(numOfPlans)];
		for (int i = 0; i < Integer.parseInt(numOfPlans); i++) {
			planPayload[i] = new CreatePlan(createPlan.get(Integer.toString(0)), createPlan.get(Integer.toString(1)),
					createPlan.get(Integer.toString(2)), createPlan.get(Integer.toString(3)),
					createPlan.get(Integer.toString(4))).toString();
		}
		return planPayload;
	}

	public String populateBenefitData(String type, String priority) {
		String benefitPayload = new Createbenefit(type, priority).toString();
		return benefitPayload;
	}

	public String[] populateMultipleIncentivePayload(HashMap<String, String> createIncentiveData,
													 String numOfIncentives) {
		String[] incentivesPayload = new String[Integer.parseInt(numOfIncentives)];
		for (int i = 0; i < Integer.parseInt(numOfIncentives); i++) {
			incentivesPayload[i] = new CreateIncentive(createIncentiveData.get(Integer.toString(0)),
					createIncentiveData.get(Integer.toString(1)), createIncentiveData.get(Integer.toString(2)),
					createIncentiveData.get(Integer.toString(3)), createIncentiveData.get(Integer.toString(4)),
					createIncentiveData.get(Integer.toString(5)), createIncentiveData.get(Integer.toString(6)))
					.toString();

		}
		return incentivesPayload;
	}

	public String populateIncentivePayload(HashMap<String, String> createIncentiveData) {

		return new CreateIncentive(createIncentiveData.get(Integer.toString(0)),
				createIncentiveData.get(Integer.toString(1)), createIncentiveData.get(Integer.toString(2)),
				createIncentiveData.get(Integer.toString(3)), createIncentiveData.get(Integer.toString(4)),
				createIncentiveData.get(Integer.toString(5)), createIncentiveData.get(Integer.toString(6))).toString();

	}

	public String populateMultiplePlanBenefitMappingPayload(HashMap<String, String> createPlanBenefitMapping,
															String numOfMapping) {
		// plan_id, benefit_id, created_by
		String benefitPayloadData = "";
		String benefitPayload = "";
		for (int i = 0; i < Integer.parseInt(numOfMapping); i++) {
			benefitPayloadData = new Createplanbenefitmapping(createPlanBenefitMapping.get(Integer.toString(0)),
					createPlanBenefitMapping.get(Integer.toString(1)),
					createPlanBenefitMapping.get(Integer.toString(2))).toString();
			benefitPayload = benefitPayload + benefitPayloadData;
			benefitPayloadData = "";
			if (!(i == Integer.parseInt(numOfMapping) - 1)) {
				benefitPayload = benefitPayload + ",";
			}
		}
		return "[" + benefitPayload + "]";
	}

	public String populatePlanBenefitMappingPayload(String plan_id, String benefit_id) {
		// plan_id, benefit_id
		String createdBy = "By-Super-Automation";
		String benefitPayloadData = new Createplanbenefitmapping(plan_id, benefit_id, createdBy).toString();

		return "[" + benefitPayloadData + "]";
	}

	public String populateMultiplePlanUserIncentiveMappingPayload(HashMap<String, String> planUserIncentiveData,
																  String numOfMapping) {
		// plan_id, userType-> USER or user_id, incentiveId- null or incentive_id
		String planUserIncentive = "";
		String planUserIncentivePayload = "";

		for (int i = 0; i <= Integer.parseInt(numOfMapping); i++) {
			planUserIncentive = new Createplanuserincentivemapping(planUserIncentiveData.get(Integer.toString(0)),
					planUserIncentiveData.get(Integer.toString(1)), planUserIncentiveData.get(Integer.toString(1)))
					.toString();

			planUserIncentivePayload = planUserIncentivePayload + planUserIncentive;
			planUserIncentive = "";
			if (!(i == Integer.parseInt(numOfMapping) - 1)) {
				planUserIncentivePayload = planUserIncentivePayload + ",";
			}
		}
		return "[" + planUserIncentivePayload + "]";
	}

	public String populatePlanUserIncentiveMappingPayload(String plan_id, String user_id, String incentive_id) {
		// plan_id, userType-> USER or user_id, incentiveId- null or incentive_id
		String planUserIncentive = "";
		if (!(user_id.equalsIgnoreCase("USER"))) {
			planUserIncentive = new Createplanuserincentivemapping(plan_id, user_id, incentive_id).MappingUserId();
		}

		else {
			planUserIncentive = new Createplanuserincentivemapping(plan_id, user_id, incentive_id).toString();
		}
		return "[" + planUserIncentive + "]";
	}

	public String populateCreateSubscriptionPayload(String plan_id, String user_id, String order_id) {
		// plan_id, user_id, order_id
		String createSubscriptionPayload = "";
		createSubscriptionPayload = new Createsubscription(plan_id, user_id, order_id).toString();

		return createSubscriptionPayload;
	}

	public String populateVerifyPlanPayload(String plan_id, String user_id) {
		// plan_id, user_id
		String createVerifyPlanPayload = "";
		createVerifyPlanPayload = new Verifyplan(plan_id, user_id).toString();

		return createVerifyPlanPayload;
	}

	public String populateCancelSubscriptionIdPayload(String cancelSubscriptionData) {
		String cancelSubscriptionPayload = "";
		// subscription_id

		cancelSubscriptionPayload = new Cancelsubscription(cancelSubscriptionData).cancelSubscriptionBySubscriptionId();
		return cancelSubscriptionPayload;
	}

	public String populateCancelSubscriptionByOrderIdAndUserIdPayload(String order_id, String user_id) {
		String cancelSubscriptionPayload = "";
		// order_id, user_id

		cancelSubscriptionPayload = new Cancelsubscription(order_id, user_id).CancelSubscriptionByUserIdOrderId();
		return cancelSubscriptionPayload;
	}

	public String populateCancelSubscriptionBySubscriptionIdOrderIdAndUserIdPayload(String subscription_id,
																					String order_id, String user_id) {
		String cancelSubscriptionPayload = "";
		// subscription_id, order_id, user_id

		cancelSubscriptionPayload = new Cancelsubscription(subscription_id, order_id, user_id).toString();

		return cancelSubscriptionPayload;
	}

	public String populatePatchPlanPayloadByPlanIdEnabled( String plan_id, boolean enabledStatus) {
		// plan_id, enabled status
		String patchPlanPayload = "";
		patchPlanPayload = new Patchplan(plan_id,enabledStatus).toStringDisbalePlan();

		return patchPlanPayload;
	}

	public String populatePatchPlanPayload(HashMap<String, String> patchPlanData) {
		// plan_id, enabled, valid_till, count
		String patchPlanPayload = "";
		patchPlanPayload = new Patchplan(patchPlanData.get(Integer.toString(0)), patchPlanData.get(Integer.toString(1)),
				patchPlanData.get(Integer.toString(2)), patchPlanData.get(Integer.toString(3))).toString();

		return patchPlanPayload;
	}

	public String populatePatchPlanPayloadForCount(String plan_id, String count) {
		// plan_id, count
		String patchPlanPayload = "";
		patchPlanPayload = new Patchplan(plan_id, count).toStringPlanCount();

		return patchPlanPayload;
	}

	public String populatePatchPlanPayloadByValidityCount( String plan_id,String validTill, String count) {
		// plan_id , valid_till , count
		String patchPlanPayload = "";
		patchPlanPayload = new Patchplan(plan_id, validTill,count).toStringPlanValidityAndCount();

		return patchPlanPayload;
	}

	public String createPlanHelper(String planPayload) {
		Processor planResponse = createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 1);
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertNotEquals(planId, null);
		return planId;
	}

	public String populatePatchIncentiveDisableEnabledPayload(HashMap<String, String> patchIncentiveData) {
		// incentive_id, enabled status
		String patchIncentivePayload = "";
		patchIncentivePayload = new PatchIncentive(patchIncentiveData.get(Integer.toString(0)),
				Boolean.parseBoolean(patchIncentiveData.get(Integer.toString(1)))).disableEnableIncentive();
		return patchIncentivePayload;
	}

	public String populatePatchIncentiveTenurePayload(HashMap<String, String> patchIncentiveData) {
		// incentive_id, tenure- 0 or -1
		String patchIncentivePayload = "";
		patchIncentivePayload = new PatchIncentive(patchIncentiveData.get(Integer.toString(0)),
				patchIncentiveData.get(Integer.toString(1))).updateIncentiveTenure();

		return patchIncentivePayload;
	}

	public String populatePatchIncentiveByEnableDisableTenurePayload(HashMap<String, String> patchIncentiveData) {
		// incentive_id, tenure, enabled
		String patchIncentivePayload = "";
		patchIncentivePayload = new PatchIncentive(patchIncentiveData.get(Integer.toString(0)),
				patchIncentiveData.get(Integer.toString(1)),Boolean.parseBoolean(patchIncentiveData.get(Integer.toString(2)))).toString();

		return patchIncentivePayload;
	}

	public Processor createPlan(String createPlanData) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "createplan", gameofthrones);
		String[] payload = new String[1];
		payload[0] = createPlanData;
		System.out.println(payload[0]);
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor createBenefit(String benefitPayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "createbenefit", gameofthrones);
		String[] payload = new String[1];
		payload[0] = benefitPayload;
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor createIncentive(String incentivePayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "createincentive", gameofthrones);
		String[] payload = new String[1];
		payload[0] = incentivePayload;
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor createPlanBenefitMapping(String planBenefitMappingPayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "createplanbenefitmapping", gameofthrones);
		String[] payload = new String[1];
		payload[0] = planBenefitMappingPayload;
		System.out.println(planBenefitMappingPayload);
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor createPlanUserIncentiveMapping(String planUserIncentiveMappingPayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "createplanuserincentivemapping",
				gameofthrones);
		String[] payload = new String[1];
		payload[0] = planUserIncentiveMappingPayload;
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor createSubscription(String createSubscriptionPayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "createsubscription", gameofthrones);
		String[] payload = new String[1];
		payload[0] = createSubscriptionPayload;
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor verifyPlan(String verifyPlanPayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "verifyplan", gameofthrones);
		String[] payload = new String[1];
		payload[0] = verifyPlanPayload;
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor getUserSubscription(String user_id, String benefitStatus) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "getusersubscription", gameofthrones);
		String[] urlparams = new String[] { user_id, benefitStatus };
		Processor processor = new Processor(service, requestHeader, null, urlparams);
		return processor;
	}

	public Processor getSubscriptionHistoryOfUser(String user_id) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "getsubscriptionhistoryofuser", gameofthrones);
		String[] urlparams = new String[] { user_id };
		Processor processor = new Processor(service, requestHeader, null, urlparams);
		return processor;
	}

	public Processor getPlanDetailsByUserIdAndPlanId(String plan_id, String user_id, String benefitStatus) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "getplandetailsbyuseridandplanid",
				gameofthrones);
		String[] urlparams = new String[] { plan_id, user_id, benefitStatus };
		Processor processor = new Processor(service, requestHeader, null, urlparams);
		return processor;
	}

	public Processor getPlansByUserId(String user_id, String benefitStatus) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "getplanbyuserid", gameofthrones);
		String[] urlparams = new String[] { user_id, benefitStatus };
		Processor processor = new Processor(service, requestHeader, null, urlparams);
		return processor;
	}

	public Processor cancelSubscription(String cancelSubscriptionPayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "cancelsubscription", gameofthrones);
		String[] payload = new String[1];
		payload[0] = cancelSubscriptionPayload;
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor patchPlan(String patchPlanPayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "patchplan", gameofthrones);
		String[] payload = new String[1];
		payload[0] = patchPlanPayload;
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor patchIncentive(String patchIncentivePayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "patchincentive", gameofthrones);
		String[] payload = new String[1];
		payload[0] = patchIncentivePayload;
		Processor processor = new Processor(service, requestHeader, payload);
		return processor;
	}

	public Processor getPlanbenefits(String plan_id) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("super", "getplanbenefits", gameofthrones);
		String[] urlparams = new String[] { plan_id };
		Processor processor = new Processor(service, requestHeader, null, urlparams);
		return processor;
	}

	public String[] subscriptionPlanBenefitIncentiveIDs(String planPayload, String benefitPayload,
														String incentivePayload) {

		String[] PlanBenefitIncentiveIds = new String[3];
		// create plan
		Processor planResponse = createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 1);
		PlanBenefitIncentiveIds[0] = Integer.toString(planId);
		// create benefit

		String benefitType = "FREE_DELIVERY";
		String id = superDbHelper.getActiveBenefitsFromDb(benefitType);
		if (id.equalsIgnoreCase("No active Free Delivery benefit found in DB")) {
			String benefitId = createSuperBenefit(benefitType);
			if (!benefitId.equalsIgnoreCase("fail")) {
				id = "" + benefitId;
			}
		}

//		Processor benefitResponse = createBenefit(benefitPayload);
//		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
//		Assert.assertEquals(benefitStatusCode, 1);
//		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		PlanBenefitIncentiveIds[1] = id;

		// create incentive(tenure zero only)
		Processor incectiveResp = createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);
		PlanBenefitIncentiveIds[2] = Integer.toString(incentiveId);

		return PlanBenefitIncentiveIds;
	}

	public String[] subscriptionPlanFreebieBenefitIncentiveIDs(String planPayload, String benefitPayload,
														String incentivePayload) {

		String[] PlanBenefitIncentiveIds = new String[3];
		// create plan
		Processor planResponse = createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 1);
		PlanBenefitIncentiveIds[0] = Integer.toString(planId);
		// create benefit

		String benefitType = "Freebie";
		String id = superDbHelper.getActiveBenefitsFromDb(benefitType);
		if (id.equalsIgnoreCase("No active Freebie benefit found in DB")) {
			String benefitId = createSuperBenefit(benefitType);
			if (!benefitId.equalsIgnoreCase("fail")) {
				id = "" + benefitId;
			}
		}

//		Processor benefitResponse = createBenefit(benefitPayload);
//		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
//		Assert.assertEquals(benefitStatusCode, 1);
//		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		PlanBenefitIncentiveIds[1] = id;

		// create incentive(tenure zero only)
		Processor incectiveResp = createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);
		PlanBenefitIncentiveIds[2] = Integer.toString(incentiveId);

		return PlanBenefitIncentiveIds;
	}

	public boolean mapPlanAndBenefit(String plan_id,String benefit_id) {
		boolean mappingStatus=false;
		//create plan benefit mapping
		String  planBenefitMapPayload= populatePlanBenefitMappingPayload(plan_id,benefit_id).toString();
		Processor planBenefitMapResponse  = createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping=planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

		String  dataPlanBenefitMappingResp= planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		if (dataPlanBenefitMappingResp.equalsIgnoreCase("success") && statusCodeOfPlanBenefitMapping== 1);{
			mappingStatus=true;
		}

		return mappingStatus ;
	}

	public boolean mapPlanIncentiveAndUserMapping(String plan_id,String incentive, String user_type) {
		boolean mappingStatus=false;

		//plan user incentive mapping
		String planUserIncentiveMapPayload=populatePlanUserIncentiveMappingPayload(plan_id, user_type,incentive).toString();
		Processor planUserIncentiveMapResponse =createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap=planUserIncentiveMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		//Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap=planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		// Assert.assertEquals(dataPlanUserIncenMap,"success" );
		if (dataPlanUserIncenMap.equalsIgnoreCase("success") && statusCodePlanUserIncentiveMap== 1);{
			mappingStatus=true;
		}

		return mappingStatus ;
	}

	public void setPresentSubscriptionToPastSubsByValidFromValidTillDate(String subsId,String userId, String order_id, String plan_id,String subscription_tenure) {
		String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, plan_id, order_id);
		String validFrom= Utility.getPastMonthDate(1+ Integer.parseInt(subscription_tenure));
		String validTill= Utility.getPastMonthDate(1);
		System.out.println("valid from---  "+ validFrom  +"  and valid til  " +validTill +" tneure is  "+subscription_tenure);
		SuperDbHelper.updateValidFromAndValidTillByIdInSusbscription(validFrom,validTill,id,userId);

	}

	public void setPresentSubscriptionToExpiredSubsByValidTillDate(String subsId,String order_id,String user_id, String plan_id) {
		String id =SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, plan_id, order_id);
		String validTill= Utility.getPastTime(1);

		SuperDbHelper.updateValidTillByIdInSusbscription(validTill, id, user_id);
	}

	public void setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(String subsId,String order_id,String user_id, String plan_id,String plan_renewalOffSet){
		String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, plan_id, order_id);
		//String presentDate= Utility.getPresentTimeInMilliSeconds();
		String validfrom= Utility.getCustomDaysInDateFormat(Integer.parseInt(plan_renewalOffSet));
		SuperDbHelper.updateValidFromByIdInSusbscription(validfrom, id, user_id);
	}

	public void setPresentSubscriptionToAboutToExpireByValidTillDate(String subsId,String order_id,String user_id, String plan_id) {
		String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, plan_id, order_id);
		//String presentDate= Utility.getPresentTimeInMilliSeconds();
		String validTill= Utility.convertDateToString(Utility.getDatePlusDays(new Date(), 1));
		SuperDbHelper.updateValidTillByIdInSusbscription(validTill, id, user_id);
	}


	public String createSubscripionResponse(String planId, String userId,String orderId) {
		//create subscription
		String subscriptionPayload= populateCreateSubscriptionPayload(planId,userId,orderId );
		Processor subsResponse= createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp =subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		if( statusCodeOfSubsResp==1) {
			String userSubsID= subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
			int userIDInSubsResp= subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
			Assert.assertEquals(userIDInSubsResp, Integer.parseInt(userId));
			return   userSubsID;
		}

		return "Subscription not created";
	}


	public void setActivePlanToDisableByValidTillDate(String plan_id) {
		// String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, plan_id, order_id);
		//String presentDate= Utility.getPresentTimeInMilliSeconds();
		String validTill= Utility.getPastTimeForPlan(-90000);
		System.out.println(validTill + " <<<<<<");
		SuperDbHelper.updateValidTillOfPlanByValidTill(validTill, plan_id);
	}
	public void updateValidTillOfThePlanByDays(String plan_id,String days) throws ParseException {
		// String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, plan_id, order_id);
		//String presentDate= Utility.getPresentTimeInMilliSeconds();
		String valid_till= Utility.getCustomDaysInDateFormat(Integer.parseInt(days));
		System.out.println(valid_till + " <<<<<<");
		SuperDbHelper.updateValidTillOfPlanByValidTill(valid_till, plan_id);
	}

	public HashMap createSuperUserWithPublicPlan(String userId) {

//		SuperDbHelper.deleteFreeDelBenefit();
//		SuperDbHelper.deleteFreebieBenefit();

		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String planPayload=new CreatePlan().toString();
		String benefitPaload=new Createbenefit().toString();
		String incentivePayload=new CreateIncentive().toString();
		String[] allIds=subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPaload,incentivePayload);
		String planId=allIds[0];
		String benefitId=allIds[1];
		String incentiveId=allIds[2];

		String planBenefitMappingPayload=populatePlanBenefitMappingPayload(planId,  benefitId);
		String planUserIncentiveMappingPayload=populatePlanUserIncentiveMappingPayload( planId,  "USER",  incentiveId);

		createPlanBenefitMapping( planBenefitMappingPayload);
		createPlanUserIncentiveMapping( planUserIncentiveMappingPayload);

		String createSubscriptionPayload= populateCreateSubscriptionPayload( planId,  userId,  orderId);
		Processor subscriptionResp=createSubscription(createSubscriptionPayload);
		int statusCodeOfSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subscriptionResp.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		//String[] planBenefitIncentiveUserOrderIds={planId,benefitId,incentiveId,userId,orderId};
		HashMap<String, String> planBenefitIncentiveUserOrderIds=new HashMap<String, String>();
		planBenefitIncentiveUserOrderIds.put("planId", planId);
		planBenefitIncentiveUserOrderIds.put("benefitid", benefitId);
		planBenefitIncentiveUserOrderIds.put("incentiveId", incentiveId);
		planBenefitIncentiveUserOrderIds.put("userId", userId);
		planBenefitIncentiveUserOrderIds.put("orderId", orderId);
		planBenefitIncentiveUserOrderIds.put("subscriptionId", userSubsID);
		return planBenefitIncentiveUserOrderIds ;
	}

	public HashMap createSuperUserWithFreedelAndFreebiePlan(String userId) {

//		SuperDbHelper.deleteFreeDelBenefit();
//		SuperDbHelper.deleteFreebieBenefit();

		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String planPayload=new CreatePlan().toString();
		String benefitPaload=new Createbenefit().toString();
		String incentivePayload=new CreateIncentive().toString();
		String[] allIds=subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPaload,incentivePayload);
		String freebieBenefitId = createSuperBenefit("Freebie");
		if(freebieBenefitId.equalsIgnoreCase("fail")){
			freebieBenefitId = superDbHelper.getActiveFreebieBenefitsFromDb("Freebie");
		}
		String planId=allIds[0];
		String benefitId=allIds[1];
		String incentiveId=allIds[2];

		String planBenefitMappingPayload=populatePlanBenefitMappingPayload(planId,  benefitId);
		String planFreebieBenefitMappingPayload=populatePlanBenefitMappingPayload(planId,  freebieBenefitId);
		String planUserIncentiveMappingPayload=populatePlanUserIncentiveMappingPayload( planId,  "USER",  incentiveId);

		createPlanBenefitMapping( planBenefitMappingPayload);
		createPlanBenefitMapping( planFreebieBenefitMappingPayload);
		createPlanUserIncentiveMapping( planUserIncentiveMappingPayload);

		String createSubscriptionPayload= populateCreateSubscriptionPayload( planId,  userId,  orderId);
		Processor subscriptionResp=createSubscription(createSubscriptionPayload);
		int statusCodeOfSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subscriptionResp.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		//String[] planBenefitIncentiveUserOrderIds={planId,benefitId,incentiveId,userId,orderId};
		HashMap<String, String> planBenefitIncentiveUserOrderIds=new HashMap<String, String>();
		planBenefitIncentiveUserOrderIds.put("planId", planId);
		planBenefitIncentiveUserOrderIds.put("benefitid", benefitId);
		planBenefitIncentiveUserOrderIds.put("incentiveId", incentiveId);
		planBenefitIncentiveUserOrderIds.put("userId", userId);
		planBenefitIncentiveUserOrderIds.put("orderId", orderId);
		planBenefitIncentiveUserOrderIds.put("subscriptionId", userSubsID);
		return planBenefitIncentiveUserOrderIds ;
	}

	public HashMap createSuperUserWithFreebiePublicPlan(String userId) {

//		SuperDbHelper.deleteFreeDelBenefit();
//		SuperDbHelper.deleteFreebieBenefit();

		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String planPayload=new CreatePlan().toString();
		String benefitPaload=new Createbenefit().freebie();
		String incentivePayload=new CreateIncentive().toString();
		String[] allIds=subscriptionPlanFreebieBenefitIncentiveIDs(planPayload, benefitPaload,incentivePayload);
		String planId=allIds[0];
		String benefitId=allIds[1];
		String incentiveId=allIds[2];

		String planBenefitMappingPayload=populatePlanBenefitMappingPayload(planId,  benefitId);
		String planUserIncentiveMappingPayload=populatePlanUserIncentiveMappingPayload( planId,  "USER",  incentiveId);

		createPlanBenefitMapping( planBenefitMappingPayload);
		createPlanUserIncentiveMapping( planUserIncentiveMappingPayload);

		String createSubscriptionPayload= populateCreateSubscriptionPayload( planId,  userId,  orderId);
		Processor subscriptionResp=createSubscription(createSubscriptionPayload);
		int statusCodeOfSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subscriptionResp.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		//String[] planBenefitIncentiveUserOrderIds={planId,benefitId,incentiveId,userId,orderId};
		HashMap<String, String> planBenefitIncentiveUserOrderIds=new HashMap<String, String>();
		planBenefitIncentiveUserOrderIds.put("planId", planId);
		planBenefitIncentiveUserOrderIds.put("benefitid", benefitId);
		planBenefitIncentiveUserOrderIds.put("incentiveId", incentiveId);
		planBenefitIncentiveUserOrderIds.put("userId", userId);
		planBenefitIncentiveUserOrderIds.put("orderId", orderId);
		planBenefitIncentiveUserOrderIds.put("subscriptionId", userSubsID);
		return planBenefitIncentiveUserOrderIds ;
	}

	public HashMap createSuperUserWithPublicPlanWithBenefit(String userId, String benefitIdAsString) {

		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String planPayload=new CreatePlan().toString();
		String benefitPaload=new Createbenefit().toString();
		String incentivePayload=new CreateIncentive().toString();
		String[] allIds=subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPaload,incentivePayload);
		String planId=allIds[0];
		String benefitId=benefitIdAsString;
		String incentiveId=allIds[2];

		String planBenefitMappingPayload=populatePlanBenefitMappingPayload(planId,  benefitId);
		String planUserIncentiveMappingPayload=populatePlanUserIncentiveMappingPayload( planId,  "USER",  incentiveId);

		createPlanBenefitMapping( planBenefitMappingPayload);
		createPlanUserIncentiveMapping( planUserIncentiveMappingPayload);

		String createSubscriptionPayload= populateCreateSubscriptionPayload( planId,  userId,  orderId);
		Processor subscriptionResp=createSubscription(createSubscriptionPayload);
		int statusCodeOfSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subscriptionResp.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subscriptionResp.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		//String[] planBenefitIncentiveUserOrderIds={planId,benefitId,incentiveId,userId,orderId};
		HashMap<String, String> planBenefitIncentiveUserOrderIds=new HashMap<String, String>();
		planBenefitIncentiveUserOrderIds.put("planId", planId);
		planBenefitIncentiveUserOrderIds.put("benefitid", benefitId);
		planBenefitIncentiveUserOrderIds.put("incentiveId", incentiveId);
		planBenefitIncentiveUserOrderIds.put("userId", userId);
		planBenefitIncentiveUserOrderIds.put("orderId", orderId);
		planBenefitIncentiveUserOrderIds.put("subscriptionId", userSubsID);
		return planBenefitIncentiveUserOrderIds ;
	}


	public void cancelUserSubscription(String subscription_id,String order_id, String user_id) {
		String cancelSubscriptionPayload=populateCancelSubscriptionBySubscriptionIdOrderIdAndUserIdPayload( subscription_id,order_id,  user_id);
		cancelSubscription( cancelSubscriptionPayload);
	}

  public  String createSuperBenefit(String type){
	  String status = "fail";
	  HashMap<String,String> benefit =  new HashMap<String, String>();
	  String payload=populateBenefitData(type, "100");
	   Processor response  =createBenefit(payload);
	   int benefitStatusCode = response.ResponseValidator.GetNodeValueAsInt("$.statusCode");
	   if(benefitStatusCode== 1){
		   String benefitId = Integer.toString(response.ResponseValidator.GetNodeValueAsInt("$.data"));
		   benefit.put("benefit_Id" ,benefitId );
		   return benefit.get("benefit_Id");
	   };
	return status;
  }
   	public void cancelSubscriptionOfUser(String user_id)
   	{
		try {
	 	 String subs_id = SuperDbHelper.getSubscriptionIdOfTestUser(user_id);
		cancelUserSubscription(subs_id, "123", "123");
		}catch (NullPointerException e){
		System.out.println("No subscription found for the user");
		}
   	}
}