package com.swiggy.api.sf.snd.pojo.meals.createMeal;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "title",
        "description",
        "group"
})
public class Screen {

    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("group")
    private Group group=null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Screen() {
    }

    /**
     *
     * @param title
     * @param description
     * @param group
     */
    public Screen(String title, String description, Group group) {
        this.title = title;
        this.description = description;
        group = group;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("group")
    public Group getGroup() {
        return this.group;
    }

    @JsonProperty("group")
    public void setGroup(Group group) {
        this.group = group;
    }

}