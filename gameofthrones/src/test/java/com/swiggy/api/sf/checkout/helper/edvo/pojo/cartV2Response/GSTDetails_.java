package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "packaging_SGST",
        "packaging_CGST",
        "cart_SGST",
        "cart_CGST"
})
public class GSTDetails_ {

    @JsonProperty("packaging_SGST")
    private Double packagingSGST;
    @JsonProperty("packaging_CGST")
    private Double packagingCGST;
    @JsonProperty("cart_SGST")
    private Double cartSGST;
    @JsonProperty("cart_CGST")
    private Double cartCGST;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     */
    public GSTDetails_() {
    }

    /**
     * @param cartCGST
     * @param packagingSGST
     * @param cartSGST
     * @param packagingCGST
     */
    public GSTDetails_(Double packagingSGST, Double packagingCGST, Double cartSGST, Double cartCGST) {
        super();
        this.packagingSGST = packagingSGST;
        this.packagingCGST = packagingCGST;
        this.cartSGST = cartSGST;
        this.cartCGST = cartCGST;
    }

    @JsonProperty("packaging_SGST")
    public Double getPackagingSGST() {
        return packagingSGST;
    }

    @JsonProperty("packaging_SGST")
    public void setPackagingSGST(Double packagingSGST) {
        this.packagingSGST = packagingSGST;
    }

    @JsonProperty("packaging_CGST")
    public Double getPackagingCGST() {
        return packagingCGST;
    }

    @JsonProperty("packaging_CGST")
    public void setPackagingCGST(Double packagingCGST) {
        this.packagingCGST = packagingCGST;
    }

    @JsonProperty("cart_SGST")
    public Double getCartSGST() {
        return cartSGST;
    }

    @JsonProperty("cart_SGST")
    public void setCartSGST(Double cartSGST) {
        this.cartSGST = cartSGST;
    }

    @JsonProperty("cart_CGST")
    public Double getCartCGST() {
        return cartCGST;
    }

    @JsonProperty("cart_CGST")
    public void setCartCGST(Double cartCGST) {
        this.cartCGST = cartCGST;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("packagingSGST", packagingSGST).append("packagingCGST", packagingCGST).append("cartSGST", cartSGST).append("cartCGST", cartCGST).append("additionalProperties", additionalProperties).toString();
    }
}