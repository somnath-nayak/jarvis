package com.swiggy.api.sf.checkout.helper;

import com.swiggy.api.sf.checkout.constants.CancellationFeeConstants;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderCancelCheck.OrderCancelCheckRequest;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.OrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.revertCancellationFee.RevertCancellationFeeRequest;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.checkout.helper.rts.validator.GenericValidator;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.Reporter;

public class CancellationFeeHelper {
    EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
    GenericValidator genericValidator = new GenericValidator();

    public Processor orderCancelCheck(String orderId, boolean cancellationFeeApplicability){
        OrderCancelCheckRequest orderCancelCheckRequest = new OrderCancelCheckRequest(orderId, cancellationFeeApplicability);
        return edvoCartHelper.orderCancelCheck(Utility.jsonEncode(orderCancelCheckRequest));
    }

    public Processor orderCancelCheck(String payload){
        return edvoCartHelper.orderCancelCheck(payload);
    }

    public Processor revertCancellationFee(String orderId, String reason, String amount, String createdBy){
        RevertCancellationFeeRequest revertCancellationFeeRequest = new RevertCancellationFeeRequest(orderId, reason, amount, createdBy);
        return edvoCartHelper.revertCancellationFee(Utility.jsonEncode(revertCancellationFeeRequest));
    }

    public Processor revertCancellationFee(String payload){
        return edvoCartHelper.revertCancellationFee(payload);
    }

    public Processor placeOrder(String restaurantId){
        if(edvoCartHelper.restaurantServiceabilityCheck(restaurantId)){
            return edvoCartHelper.getPlaceOrder(null, restaurantId, false, false, true);
        }
        Assert.fail("Restaurants ID [" + restaurantId + "] is not serviceable. Hence, cannot place order with this restaurant..");
        return null;
    }

    public Processor getOrderCancelCheck(String restaurantId, boolean cancellationFeeApplicability){
        Processor processor = placeOrder(restaurantId);
        OrderResponse orderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        Long orderId = orderResponse.getData().getOrderId();
        return orderCancelCheck(String.valueOf(orderId), cancellationFeeApplicability);
    }

    public Processor getOrderCancelCheck(String restaurantId){
        Processor processor = placeOrder(restaurantId);
        OrderResponse orderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        Long orderId = orderResponse.getData().getOrderId();
        return orderCancelCheck(String.valueOf(orderId), Boolean.valueOf(CancellationFeeConstants.CANCELLATION_FEE_APPLICABILITY));
    }

    public void cancelAllOrders(){
        edvoCartHelper.cancelAllOrders();
    }

    public Processor getOrderCancel(String restaurantId){
        Processor processor = placeOrder(restaurantId);
        OrderResponse orderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        Long orderId = orderResponse.getData().getOrderId();
        return edvoCartHelper.cancelOrder(String.valueOf(orderId));
    }

    public Processor cancelOrder(String restaurantId, String reason, String cancellationFeeApplicability, String cancellationFee){
        Processor processor = placeOrder(restaurantId);
        OrderResponse orderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        Long orderId = orderResponse.getData().getOrderId();
        return edvoCartHelper.cancelOrder(String.valueOf(orderId), reason, cancellationFeeApplicability, cancellationFee);
    }

    public Processor getRevertCancellationFee(String restaurantId){
        Processor processor = cancelOrder(restaurantId, CancellationFeeConstants.REASON,
                CancellationFeeConstants.CANCELLATION_FEE_APPLICABILITY, CancellationFeeConstants.CANCELLATION_FEE);
        OrderResponse orderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        Long orderId =orderResponse.getData().getOrderId();
        Reporter.log("[ORDER_ID] --> " + orderId, true);
        return revertCancellationFee(String.valueOf(orderId), CancellationFeeConstants.REASON,
                CancellationFeeConstants.CANCELLATION_FEE, "SP");
    }
}
