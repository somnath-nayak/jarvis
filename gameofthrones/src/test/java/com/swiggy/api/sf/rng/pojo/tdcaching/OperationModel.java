package com.swiggy.api.sf.rng.pojo.tdcaching;


import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonPOJOBuilder
public class OperationModel {
    private Long campaignId = 1l;
    List<OperationPojo> operationPojoList = new ArrayList<>();

    public OperationModel() {}

    public OperationModel(Long campaignId, List<OperationPojo> operationPojoList) {
        this.campaignId = campaignId;
        this.operationPojoList = operationPojoList;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public void setOperationPojoList(List<OperationPojo> operationPojoList) {
        this.operationPojoList = operationPojoList;
    }

    public Long getCampaignId() {
        return this.campaignId;
    }

    public List<OperationPojo> getOperationPojoList() {
        return this.operationPojoList;
    }
}


