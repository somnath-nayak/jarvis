package com.swiggy.api.sf.checkout.tests.sanityTests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.SchemaValidationConstants;
import com.swiggy.api.sf.checkout.dp.ChekoutPricingSchemaDP;

import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutPricingHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.*;


public class PricingSchemaValidatorTest extends ChekoutPricingSchemaDP {

    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    CheckoutHelper helper=new CheckoutHelper();
    CheckoutPricingHelper pricingHelper = new CheckoutPricingHelper();


    String tid;
    String token;
    HashMap<String, String> hashMap;
    String schemaPath = SchemaValidationConstants.SCHEMA_PATH;

    @BeforeClass
    public void login() {
        hashMap = helper.TokenData("8197982351", "welcome123");
        tid = hashMap.get("Tid");
        token = hashMap.get("Token");
    }


    @Test(dataProvider = "FetchPricingSchemaValidator", groups = {"schemaValidator"})
    public void fetchPricingSchemaValidatorTest(String userAgent, String versionCode,
                                                String cityID,String restID,String distance,String rainMode,
                                                String fileName)
            throws IOException, ProcessingException {

        String fetchPricing=setFetchPricing(userAgent,versionCode,cityID,restID,distance,rainMode);
        System.out.println(fetchPricing);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println(expectedSchema);
        validateSchema(fetchPricing,expectedSchema,"Fetch Pricing Regular Cart");

    }

    @Test(dataProvider = "thresholdFee", groups = {"schemaValidator"})
    public void thesholdPricingSchemaValidatorTest(String userAgent,String versionCode,String restId,String startTime,String endTime,String fixFee,String DefaultFee,String cartTotalRangeFrom,String cartTotalRangeTo,String cartFee,String fileName)


            throws IOException, ProcessingException {

        String fetchPricing=updateThreshholeFee(userAgent,versionCode,restId,startTime,endTime,fixFee,DefaultFee,cartTotalRangeFrom, cartTotalRangeTo,cartFee);
        System.out.println(fetchPricing);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println(expectedSchema);
        validateSchema(fetchPricing,expectedSchema,"Update Threshold Cart");

    }

    @Test(dataProvider = "distanceFee", groups = {"schemaValidator"})
    public void distancePricingSchemaValidatorTest(String userAgent,String versionCode,String restID,String startTime,String endTime,String defaultFee,String distanceFrom,String distanceTo,String cartFee,String fileName)


            throws IOException, ProcessingException {

        String fetchPricing=updateDistanceFee(userAgent,versionCode,restID,startTime,endTime, defaultFee,distanceFrom,distanceTo,cartFee);
        System.out.println(fetchPricing);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println(expectedSchema);
        validateSchema(fetchPricing,expectedSchema,"Update Distance Price Cart");

    }




    public String setFetchPricing(String userAgent, String versionCode,
                                  String cityID, String restID, String distance, String rainMode){
        HashMap<String, String> header=setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  pricingHelper.setFetchPricing(header,cityID,restID,distance,rainMode).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }

    public String updateThreshholeFee(String userAgent, String versionCode,String restId,String startTime,String endTime,String fixFee,String DefaultFee,String cartTotalRangeFrom,String cartTotalRangeTo,String cartFee)
                                  {
        HashMap<String, String> header=setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  pricingHelper.setThresholdFee(header,restId,startTime,endTime,fixFee,DefaultFee,cartTotalRangeFrom, cartTotalRangeTo,cartFee).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }

    public String updateDistanceFee(String userAgent, String versionCode,String restID,String startTime,String endTime,String defaultFee,String distanceFrom,String distanceTo,String cartFee)
    {
        HashMap<String, String> header=setCompleteHeader(tid, token, userAgent, versionCode);
        String cartResponse=  pricingHelper.setDistanceFee(header,restID,startTime,endTime, defaultFee,distanceFrom,distanceTo,cartFee).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }






    public HashMap<String, String> setCompleteHeader(String tid, String token,
                                                     String userAgent,String versionCode) {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Authorization", CheckoutConstants.authorization);
        requestheaders.put("tid", tid);
        requestheaders.put("token", token);
        requestheaders.put("User-Agent", token);
        requestheaders.put("Version-Code", token);
        return requestheaders;
    }


    public void validateSchema(String response,String expectedSchema,String message) throws IOException, ProcessingException{
        System.out.println("Expected jsonschema   $$$$" + expectedSchema);
        List missingNodeList = schemaValidatorUtils.validateServiceSchema(expectedSchema, response);
        Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For "+message+" API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes=>" + isEmpty);
        Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
    }




    public String getSchemafromFile(String schemaPath,String fileName) throws IOException, ProcessingException{
        return new ToolBox().readFileAsString(System.getProperty("user.dir") + schemaPath+fileName);
    }


}







