
package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "order_id",
        "status"
})
public class OrderDelivery {

    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("status")
    private String status;

    /**
     * No args constructor for use in serialization
     */
    public OrderDelivery() {
    }

    /**
     * @param status
     * @param orderId
     */
    public OrderDelivery(String orderId, String status) {
        super();
        this.orderId = orderId;
        this.status = status;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

}