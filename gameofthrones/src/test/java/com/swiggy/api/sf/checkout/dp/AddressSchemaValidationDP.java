package com.swiggy.api.sf.checkout.dp;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.AddressConstants;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

public class AddressSchemaValidationDP {


        EDVOCartHelper eDVOCartHelper=new EDVOCartHelper();
        SANDHelper sandHelper = new SANDHelper();
        static String restId;
        String[] latLog;
        static Cart cartPayload;

        @BeforeClass
        public void getRestData() {

            latLog=new String[]{"12.935215600000001","77.6199608"};

		/*String aggregatorRes=sandHelper.aggregator(latLog).ResponseValidator.GetBodyAsText();
		String[] restList=JsonPath.read(aggregatorRes, "$.data.restaurants[*].id").toString().replace("[","").replace("]","").split(",");
		for(int i=0;i<restList.length;i++){
		String serviceability=JsonPath.read(aggregatorRes, "$.data.restaurants["+restList[i]+"]..sla..serviceability")
				                      .toString().replace("[","").replace("]","").replace("\"","");
		String isOpened=JsonPath.read(aggregatorRes, "$.data.restaurants["+restList[i]+"]..availability..opened")
				                .toString().replace("[","").replace("]","");
		if (isOpened.equalsIgnoreCase("true") && serviceability.equalsIgnoreCase("SERVICEABLE")) {
				restId=restList[i].replace("\"","");
				break;
			}}
		cartPayload=eDVOCartHelper.getCartPayload1(null, restId, false, false, true);*/
            //cartPayload=eDVOCartHelper.getCartPayload1(null, "16197", false, false, true);
        }

        @DataProvider(name = "GetAllAddressSchemaValidator")
        public static Object[][] GetAllAddressSchemaValidator() {
            return new Object[][]{
                    {"Swiggy-Android", "300","getAllAddressAndroid.txt"},
                    {"Swiggy-iOS", "300","getAllAddressIos.txt"},
                    {"Web", "300","getAllAddressWeb.txt"}

            };
        }

    @DataProvider(name = "AddNewAddressSchemaValidator")
    public static Object[][] AddNewAddressSchemaValidator() {
        return new Object[][]{
               {"Swiggy-Android", "300", CheckoutConstants.mobile1,CheckoutConstants.password1, "Rest123", "7338471171", "199, Near Sai Baba Temple, 7th Block, Koramangala", "Udupi garden", "Swiggy Test", "12.911282", "77.67624999999998", "78212", "Bangalore", "WORK",AddressConstants.statusmessage, AddressConstants.validStatusCode,"addnewAddress.txt"},
                {"Swiggy-iOS", "300",CheckoutConstants.mobile1,CheckoutConstants.password1, "Rest1234", "7338471171", "199, Near Sai Baba Temple, 7th Block, Koramangala", "Udupi garden", "Swiggy Test", "12.911282", "77.67624999999998", "78212", "Bangalore", "WORK",AddressConstants.statusmessage, AddressConstants.validStatusCode,"addAddressNewIos.txt"},
               {"Web", "300",CheckoutConstants.mobile1,CheckoutConstants.password1, "Rest1234", "7338471171", "199, Near Sai Baba Temple, 7th Block, Koramangala", "Udupi garden", "Swiggy Test", "12.911282", "77.67624999999998", "78212", "Bangalore", "WORK",AddressConstants.statusmessage, AddressConstants.validStatusCode,"addAddressNewWeb.txt"}

        };
    }

    @DataProvider(name = "isAddressServiceableSchemaValidator")
    public static Object[][] isAddressServiceableSchemaValidator() {
        return new Object[][]{
                {"Swiggy-Android", "300","12.934533", "77.626579","isAddressServiceableAndroid.txt"},
                {"Swiggy-iOS", "300","12.934533", "77.626579","isAddressServiceableIos.txt"},
                {"Web", "300","12.934533", "77.626579","isAddressServiceableWeb.txt"}

        };
    }

    @DataProvider(name = "deleteAddressSchemaValidator")
    public static Object[][] deleteAddressSchemaValidator() {
        return new Object[][]{
                {"Swiggy-Android", "300","deleteAddressAndroid.txt"},
                {"Swiggy-iOS", "300","deleteAddressIos.txt"},
                {"Web", "300","deleteAddressWeb.txt"}

        };
    }

    @DataProvider(name = "updateAddressSchemaValidator")
    public static Object[][] updateAddressSchemaValidator() {
        return new Object[][]{
                {"Swiggy-Android", "300","Rest123", "7338471171", "199, BTM layout, 2nd stage", "office Land Mark", "Kormangala", "12.911282", "77.67", "78212", "Hyderabad", "WORK","updateAddressAndroid.txt"},
                {"Swiggy-iOS", "300","Rest123", "7338471171", "199, BTM layout, 2nd stage", "office Land Mark", "Kormangala", "12.911282", "77.67", "78212", "Hyderabad", "WORK","updateAddressIos.txt"},
                {"Web", "300","Rest123", "7338471171", "199, BTM layout, 2nd stage", "office Land Mark", "Kormangala", "12.911282", "77.67", "78212", "Hyderabad", "WORK","updateAddressWeb.txt"}

        };
    }


        }



