package com.swiggy.api.sf.rng.pojo.edvo;

import io.advantageous.boon.json.annotations.JsonProperty;

public class EDVOCartMealItemRequest {

	@JsonProperty("mealId")
	String mealId;
	@JsonProperty("itemId")
	String itemId;
	@JsonProperty("restaurantId")
	String restId;
	@JsonProperty("price")
	String price;
	@JsonProperty("count")
	String count;
	@JsonProperty("groupId")
	String groupId;

	public EDVOCartMealItemRequest() {
		this.mealId = null;
	}

	public EDVOCartMealItemRequest(String mealId, String itemId, String restaurantId, String price, String count,
			String groupId) {

		this.mealId = mealId;
		this.itemId = itemId;
		this.restId = restaurantId;
		this.price = price;
		this.count = count;
		this.groupId = groupId;
	}
	
	public String getmealId() {
		return this.mealId;
	}
	
	public void setMealId(String mealId) {
		this.mealId= mealId;
	}
	public String getGroupId() {
		return this.groupId;
	}
	
	public void setGroupId(String groupId) {
		this.groupId= groupId;
	}
	
	public String getItemId() {
		return this.itemId;
	}
	
	public void setItemId(String itemId) {
		this.itemId= itemId;
	}
	
	public String getRestaurantId() {
		return this.restId;
	}
	public void setRestaurantId(String restId) {
		this.restId= restId;
	}
	
	public String getPrice() {
		
		return this.price= price;
	}
	public void setPrice(String price) {
		this.price= price;
	}
	
	public String getCount() {
		
		return this.count;
	}
	public void setCount(String count) {
		this.count= count;
	}
	public String toString() {
		if (this.mealId != null) {
			return 
					"{"
					+"\"mealId\"" + ":" + mealId + "," + "\"itemId\"" + ":" + itemId + "," + "\"restaurantId\"" + ":"
					+ restId + "," + "\"price\"" + ":" + price + "," + "\"count\"" + ":" + count + "," + "\"groupId\""
					+ ":" + groupId 
		 + "}";
		} else {
			return "";
		}

	}

}