package com.swiggy.api.sf.snd.pojo;

public class crossSelling {

    private String category;

    private ItemSet[] itemSets;

    public crossSelling() {
    }

    public crossSelling(String category, ItemSet[] itemSets) {
        this.category = category;
        this.itemSets = itemSets;
    }

    public String getcategory ()
    {
        return category;
    }

    public void setcategory (String category)
    {
        this.category = category;
    }

    public ItemSet[] getItemSets() {
        return itemSets;
    }

    public void setItemSets(ItemSet[] itemSets) {
        this.itemSets = itemSets;
    }


}
