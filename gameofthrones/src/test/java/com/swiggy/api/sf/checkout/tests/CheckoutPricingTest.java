package com.swiggy.api.sf.checkout.tests;

import java.util.HashMap;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import com.swiggy.api.sf.checkout.dp.CheckoutPricingDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutPricingHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.snd.helper.SANDHelper;

public class CheckoutPricingTest extends CheckoutPricingDP {

    CheckoutHelper helper= new CheckoutHelper();
    CheckoutPricingHelper pricingHelper= new CheckoutPricingHelper();
    EDVOCartHelper eDVOCartHelper=new EDVOCartHelper();
    SANDHelper sandHelper=new SANDHelper();
	String tid;
	String token;
	
	@BeforeTest
	public void login(){
		HashMap<String, String> hashMap=helper.TokenData(CheckoutConstants.mobile1, CheckoutConstants.password1);
	    tid=hashMap.get("Tid");
	    token=hashMap.get("Token");
	}
    
   @Test(dataProvider="thresholdFee", groups = {"Regression","Smoke","chandan"}, description = "set pricing for cart")
   public void createThresholdFee(String n,String lat, String lng,String startTime,String endTime,String fixFee,String DefaultFee,
		   String cartTotalRangeFrom,String cartTotalRangeTo,String thresholdFee){
       
	   String restId=getRestid(lat,lng);
	   Cart cartPayload=getCartPayload(restId);
	    
	   String thresholdFeeRes= pricingHelper.setThresholdFee(restId,startTime,endTime,fixFee,DefaultFee,
    		   cartTotalRangeFrom,cartTotalRangeTo,thresholdFee).ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(thresholdFeeRes);
       
       String cartResponse=helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
	   helper.validateApiResStatusData(cartResponse);
	   String getCartResAfterThresholdFee= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
       
       if(fixFee.equals(PricingConstants.PRICING_ZERO_VALUE)){
    	   pricingHelper.validateThresholdFeeDataFromResponse(getCartResAfterThresholdFee,restId,thresholdFee);
       }else{
       pricingHelper.validateThresholdFeeDataFromResponse(getCartResAfterThresholdFee,restId,fixFee);}
   }
  
   @Test(dataProvider="thresholdFeeCity", groups = {"Regression","Smoke","chandan"}, description = "set pricing city wise")
   public void createThresholdFeeCity(String n,String lat, String lng,String startTime,String endTime,String cityID,String fixFee,
		   String defaultFee,String cartTotalRangeFrom,String cartTotalRangeTo,String cartFee){
      
	   String restId=getRestid(lat,lng);
	   Cart cartPayload=getCartPayload(restId);
	   
	   String thresholdFeeCityRes= pricingHelper.setThresholdFeeCity(cityID,startTime,endTime,fixFee,defaultFee,cartTotalRangeFrom,
    		   cartTotalRangeTo,cartFee).ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(thresholdFeeCityRes);
          
       String thresholdFeeRes= pricingHelper.setThresholdFee(restId,startTime,endTime,fixFee,defaultFee,
    		   cartTotalRangeFrom,cartTotalRangeTo,cartFee).ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(thresholdFeeRes);
       
       helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
       String getCartResAfterThresholdFee= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
       
       if(fixFee.equals(PricingConstants.PRICING_ZERO_VALUE)){
    	   pricingHelper.validateThresholdFeeDataFromResponse(getCartResAfterThresholdFee,restId,cartFee);
       }else{
       pricingHelper.validateThresholdFeeDataFromResponse(getCartResAfterThresholdFee,restId,fixFee);}
   }
   
   @Test(dataProvider="distanceFee", groups = {"Regression","Smoke","chandan"}, description = "set pricing as per distance")
   public void createDistanceFee(String n,String lat, String lng,String startTime,String endTime,String defaultFee,String distanceFrom,String distanceTo,String cartFee){
	   
	   String restId=getRestid(lat,lng);
	   Cart cartPayload=getCartPayload(restId);

       String distanceFeeRes= pricingHelper.setDistanceFee(restId,startTime,endTime,defaultFee,distanceFrom,distanceTo,cartFee)
    		   .ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(distanceFeeRes);
       helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
       
       String getCartResAfterDistanceFee= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
       pricingHelper.validateDistanceFeeDataFromResponse(getCartResAfterDistanceFee,restId,cartFee);
   }
   
   @Test(dataProvider="distanceFeeCity", groups = {"Regression","Smoke","chandan"}, description = "set pricing as per distance,city wise")
   public void createDistanceFeeCity(String n,String lat, String lng,String cityID,String startTime,String endTime,
		   String defaultFee,String distanceFrom,String distanceTo,String cartFee){
     
       String distanceFeeCityRes= pricingHelper.setDistanceFeeCity(cityID,startTime,endTime,defaultFee,
    		   distanceFrom,distanceTo,cartFee).ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(distanceFeeCityRes);
       
       String restId=getRestid(lat,lng);
	   Cart cartPayload=getCartPayload(restId);
       
       String distanceFeeRes= pricingHelper.setDistanceFee(restId,startTime,endTime,defaultFee,distanceFrom,distanceTo,cartFee)
    		   .ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(distanceFeeRes);
       
       helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
       
       String getCartResAfterDistanceFee= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
       pricingHelper.validateDistanceFeeDataFromResponse(getCartResAfterDistanceFee,restId,cartFee);
   }
   
   @Test(dataProvider="timeFee", groups = {"Regression","Smoke","chandan"}, description = "set pricing based on time")
   public void createTimeFee(String n,String lat, String lng,String startTime,String endTime,String timeFee){
	         
	   String restId=getRestid(lat,lng);
	   Cart cartPayload=getCartPayload(restId);
	   
       String timeFeeRes= pricingHelper.setTimeFee(restId,startTime,endTime,timeFee).ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(timeFeeRes);
       
       helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
       String getCartResAfterTimeFee= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
       
       pricingHelper.validateTimeFeeDataFromResponse(getCartResAfterTimeFee,restId,timeFee);
       }
   
   @Test(dataProvider="timeFeeCity", groups = {"Regression","Smoke","chandan"}, description = "set pricing for night order")
   public void createTimeFeeCity(String n,String lat, String lng,String cityID,String startTime,String endTime,String timeFee){
       
	   String timeFeeCityRes= pricingHelper.setTimeFeeCity(cityID,startTime,endTime,timeFee)
			   .ResponseValidator.GetBodyAsText();
	   helper.validateApiResStatusData(timeFeeCityRes);
       
	   String restId=getRestid(lat,lng);
	   Cart cartPayload=getCartPayload(restId);
	   
       String timeFeeRes= pricingHelper.setTimeFee(restId,startTime,endTime,timeFee).ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(timeFeeRes);
       
       helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
       String getCartResAfterTimeFee= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
       
       pricingHelper.validateTimeFeeDataFromResponse(getCartResAfterTimeFee,restId,timeFee);
      }
   
   @Test(dataProvider="specialFeeCity", groups = {"Regression","Smoke","chandan"}, description = "set special pricing for fesival/events")
   public void createSpecialFeeCity(String n,String lat, String lng,String cityID,String rainMode,String specialFee,
		   String priority,String fromDate,String toDate){
	   
       String specialFeeCityRes= pricingHelper.setSpecialFeeCity(cityID,rainMode,specialFee,priority,fromDate,toDate)
    		   .ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(specialFeeCityRes);
       
       String restId=getRestid(lat,lng);
	    Cart cartPayload=getCartPayload(restId);
       
       String specialFeeRes= pricingHelper.setSpecialFee(restId,rainMode,specialFee,priority,fromDate,toDate)
    		   .ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(specialFeeRes);
       
       helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
       String getCartResAfterTimeFee= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
     
       pricingHelper.validateSpecialFeeDataFromResponse(getCartResAfterTimeFee,restId,specialFee);
   }
   
   @Test(dataProvider="specialFee", groups = {"Regression","Smoke","chandan"}, description = "set special pricing for fesival/events")
   public void createSpecialFee(String n,String lat, String lng,String rainMode,String specialFee,String priority,String fromDate,String toDate)
	 { 
	    String restId=getRestid(lat,lng);
	    Cart cartPayload=getCartPayload(restId);
       
       String specialFeeRes= pricingHelper.setSpecialFee(restId,rainMode,specialFee,priority,fromDate,toDate)
    		   .ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(specialFeeRes);
       
       helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
       String getCartResAfterTimeFee= helper.GetCart(tid,token).ResponseValidator.GetBodyAsText();
   
       if(rainMode.equals("true")){
    	   pricingHelper.validateSpecialFeeDataFromResponse(getCartResAfterTimeFee,restId,PricingConstants.PRICING_ZERO_VALUE);
       } else{
    	   pricingHelper.validateSpecialFeeDataFromResponse(getCartResAfterTimeFee,restId,specialFee);
   }
       }
   
   @Test(dataProvider="updatePricingMessage", groups = {"Smoke","chandan"}, description = "update pricing message")
   public void updatePricingMessage(String lat, String lng,String apiMessage)
   {
	   String restId=getRestid(lat,lng);
       String updatePricingMessageRes= pricingHelper.updatePricingMessage(restId,apiMessage).ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(updatePricingMessageRes);
   }
   
   @Test(dataProvider="fetchPricing", groups = {"Smoke","chandan"}, description = "update pricing message")
   public void fetchPricing(String lat, String lng,String cityID,String distance,String rainMode){   
	   String restId=getRestid(lat,lng);
       String fetchPricingRes= pricingHelper.setFetchPricing(cityID,restId,distance,rainMode)
    		   .ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(fetchPricingRes);
      }
   
   public String getRestid(String lat,String lng) {
	   String restId="";
		String[] latLog=new String[]{lat,lng};
		String aggregatorRes=sandHelper.aggregator(latLog).ResponseValidator.GetBodyAsText();
		String[] restList=JsonPath.read(aggregatorRes, "$.data.restaurants[*].id").toString().replace("[","").replace("]","").split(",");
		for(int i=0;i<restList.length;i++){
		String serviceability=JsonPath.read(aggregatorRes, "$.data.restaurants["+restList[i]+"]..sla..serviceability").toString().replace("[","").replace("]","").replace("\"","");
		String isOpened=JsonPath.read(aggregatorRes, "$.data.restaurants["+restList[i]+"]..availability..opened").toString().replace("[","").replace("]","");
		if (isOpened.equalsIgnoreCase("true") && serviceability.equalsIgnoreCase("SERVICEABLE")) {
		restId=restList[i].replace("\"","");}}
		return restId;
  }
   
   public Cart getCartPayload(String restId) {
	   return eDVOCartHelper.getCartPayload1(null, restId, false, false, true);
 }
   
}