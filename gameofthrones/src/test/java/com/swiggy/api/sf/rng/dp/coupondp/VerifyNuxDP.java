package com.swiggy.api.sf.rng.dp.coupondp;

import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.TimeSlot;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.HashMap;

public class VerifyNuxDP {

    @DataProvider(name = "createCouponWithCityRestrictionAsTrueAndFalse")
    public static Object[][] createCouponWithCityRestrictionAsTrueAndFalse() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        HashMap<String, Object> data1 = new HashMap<String, Object>();
        HashMap<String, Object> data2 = new HashMap<String, Object>();
        HashMap<String, Object> data3 = new HashMap<String, Object>();
        HashMap<String, Object> data4 = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withDiscountPercentage(0)
                .withDiscountAmount(0)
                .withFreeShipping(0)
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        CreateCouponPOJO createCouponPOJO1 = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(1)
                .withDiscountPercentage(0)
                .withDiscountAmount(0)
                .withFreeShipping(0)
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data1.put("createCouponPOJO", createCouponPOJO1);
        CreateCouponPOJO createCouponPOJO2 = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getYesterDayDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withDiscountPercentage(0)
                .withDiscountAmount(0)
                .withFreeShipping(0)
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data2.put("createCouponPOJO", createCouponPOJO2);
        CreateCouponPOJO createCouponPOJO3 = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withDiscountPercentage(0)
                .withDiscountAmount(0)
                .withFreeShipping(0)
                .withCouponUserTypeId(0)
                .withSlotRestriction(1)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withTimeSlot(new ArrayList<TimeSlot>() {{
                    add(new TimeSlot().setDefaultData().withOpenTime(0).withCloseTime(1));
                }})
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data3.put("createCouponPOJO", createCouponPOJO3);
        CreateCouponPOJO createCouponPOJO4 = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getYesterDayDate())
                .withValidTill(Utility.getYesterDayDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withDiscountPercentage(0)
                .withDiscountAmount(0)
                .withFreeShipping(0)
                .withCouponUserTypeId(0)
                .withExpiryOffset(1)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data4.put("createCouponPOJO", createCouponPOJO4);
        return new Object[][]{{data}, {data1}, {data2}, {data3}, {data4}};
    }


    @DataProvider(name = "createCouponWithFirstTimeUserRestrictionsAsTruePositive")
    public static Object[][] createCouponWithFirstTimeUserRestrictionsAsTruePositive() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withFirstOrderRestriction(1)
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createCouponWithFirstTimeUserRestrictionsAsTrueNegative")
    public static Object[][] createCouponWithFirstTimeUserRestrictionsAsTrueNegative() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withFirstOrderRestriction(1)
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createCouponForPublicUserSpecific")
    public static Object[][] createCouponForPublicUserSpecific() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        HashMap<String, Object> data1 = new HashMap<>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withCouponUserTypeId(2)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        CreateCouponPOJO createCouponPOJO1 = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withFirstOrderRestriction(0)
                .withCouponUserTypeId(2)
                .withIsPrivate(1)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data1.put("createCouponPOJO", createCouponPOJO1);
        return new Object[][]{{data},{data1}};
    }
    @DataProvider(name = "createCouponForPrivateUserSpecific")
    public static Object[][] createCouponForPrivateUserSpecific() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        HashMap<String, Object> data1 = new HashMap<>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withIsPrivate(1)
                .withCouponUserTypeId(2)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createCouponForSuperUserSpecific")
    public static Object[][] createCouponForSuperUserSpecific() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withCouponUserTypeId(1)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }
    @DataProvider(name = "useCountLimitCross")
    public static Object[][] useCountLimitReached() {
        HashMap<String, Object> data = new HashMap<String, Object>();

        CreateCouponPOJO createCouponPOJO1 = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withFirstOrderRestriction(0)
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO1);
        return new Object[][]{{data}};
    }
    @DataProvider(name = "customerRestriction")
    public static Object[][] customerRestriction() {
        HashMap<String, Object> data = new HashMap<String, Object>();

        CreateCouponPOJO createCouponPOJO1 = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withCustomerRestriction(1)
                .withFirstOrderRestriction(0)
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO1);
        return new Object[][]{{data}};
    }
    @DataProvider(name = "applicableOnRestriction")
    public static Object[][] applicableOnRestriction() {
        HashMap<String, Object> data = new HashMap<String, Object>();

        CreateCouponPOJO createCouponPOJO1 = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withTotalPerUser(3)
                .withUser_transaction(2)
                .withFirstOrderRestriction(0)
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO1);
        return new Object[][]{{data}};
    }
}