package com.swiggy.api.sf.rng.pojo;

import edu.emory.mathcs.backport.java.util.Arrays;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collections;
import java.util.List;

public class CouponMeta {
    private int usageCount;
    private String couponType;
    private int cartMinAmount;
    private int cartMinQty;
    private double discountAmount;
    private double discountPercent;
    private int upperCap;
    private boolean freeShipping;
    private long userClient;
    private int discountItem;
    private String description;
    private String title;
    private List<String> tnc;
    private String logoId;
    private boolean firstOrderRestriction;
    private List<String> preferredPaymentMethod;
    private String userAgent;
    private int offset;

    public int getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }

    public CouponMeta withUsageCount(int usageCount) {
        this.usageCount = usageCount;
        return this;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public CouponMeta withCouponType(String couponType) {
        this.couponType = couponType;
        return this;
    }

    public int getCartMinAmount() {
        return cartMinAmount;
    }

    public void setCartMinAmount(int cartMinAmount) {
        this.cartMinAmount = cartMinAmount;
    }

    public CouponMeta withCartMinAmount(int cartMinAmount) {
        this.cartMinAmount = cartMinAmount;
        return this;
    }

    public int getCartMinQty() {
        return cartMinQty;
    }

    public void setCartMinQty(int cartMinQty) {
        this.cartMinQty = cartMinQty;
    }

    public CouponMeta withCartMinQty(int cartMinQty) {
        this.cartMinQty = cartMinQty;
        return this;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public CouponMeta withDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
        return this;
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public CouponMeta withDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
        return this;
    }

    public int getUpperCap() {
        return upperCap;
    }

    public void setUpperCap(int upperCap) {
        this.upperCap = upperCap;
    }

    public CouponMeta withUpperCap(int upperCap) {
        this.upperCap = upperCap;
        return this;
    }

    public boolean isFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    public CouponMeta withFreeShipping(boolean freeShipping) {
        this.freeShipping = freeShipping;
        return this;
    }

    public long getUserClient() {
        return userClient;
    }

    public void setUserClient(long userClient) {
        this.userClient = userClient;
    }

    public CouponMeta withUserClient(long userClient) {
        this.userClient = userClient;
        return this;
    }

    public int getDiscountItem() {
        return discountItem;
    }

    public void setDiscountItem(int discountItem) {
        this.discountItem = discountItem;
    }

    public CouponMeta withDiscountItem(int discountItem) {
        this.discountItem = discountItem;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CouponMeta withDescription(String description) {
        this.description = description;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CouponMeta withTitle(String title) {
        this.title = title;
        return this;
    }

    public List<String> getTnc() {
        return tnc;
    }

    public void setTnc(List<String> tnc) {
        this.tnc = tnc;
    }

    public CouponMeta withTnc(List<String> tnc) {
        this.tnc = tnc;
        return this;
    }

    public String getLogoId() {
        return logoId;
    }

    public void setLogoId(String logoId) {
        this.logoId = logoId;
    }

    public CouponMeta withLogoId(String logoId) {
        this.logoId = logoId;
        return this;
    }

    public boolean isFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    public void setFirstOrderRestriction(boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
    }

    public CouponMeta withFirstOrderRestriction(boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
        return this;
    }

    public List<String> getPreferredPaymentMethod() {
        return preferredPaymentMethod;
    }

    public void setPreferredPaymentMethod(List<String> preferredPaymentMethod) {
        this.preferredPaymentMethod = preferredPaymentMethod;
    }

    public CouponMeta withPreferredPaymnetMethod(List<String> preferredPaymentMethod) {
        this.preferredPaymentMethod = preferredPaymentMethod;
        return this;

    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public CouponMeta withUserAgent(String userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public CouponMeta withOffset(int offset) {
        this.offset = offset;
        return this;
    }

    public CouponMeta withDefaultValues() {
        return this.withUsageCount(1)
                .withCouponType("Discount")
                .withCartMinAmount(1)
                .withCartMinQty(1)
                .withDiscountAmount(20)
                .withDiscountPercent(0)
                .withUpperCap(100)
                .withFreeShipping(false)
                .withUserClient(0)
                .withDiscountAmount(0)
                .withDescription("Super-Coupon-Automation")
                .withTitle("Automation-Test")
                .withTnc(Collections.emptyList())
                .withLogoId("ps5t0cxxfwhgftt2dodm1")
                .withFirstOrderRestriction(false)
                .withPreferredPaymnetMethod(Collections.singletonList("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb"))
                .withUserAgent("ANDROID")
                .withOffset(30);
    }

}
