package com.swiggy.api.sf.snd.pojo;

public class Variant_groups
{
    private String name;

    private String group_id;

    private Variations[] variations;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getGroup_id ()
    {
        return group_id;
    }

    public void setGroup_id (String group_id)
    {
        this.group_id = group_id;
    }

    public Variations[] getVariations ()
    {
        return variations;
    }

    public void setVariations (Variations[] variations)
    {
        this.variations = variations;
    }

    public Variant_groups build(){
        setDefaultValues();
        return this;
    }
    public void setDefaultValues(){

        Variations variations = new Variations();
        variations.build();

        if(this.getName()==null){
            this.setName("Quantity");
        }
        if(this.getGroup_id()==null){
            this.setGroup_id("302819");
        }

    }

    @Override
    public String toString()
    {
        return "ClassPojo [name = "+name+", group_id = "+group_id+", variations = "+variations+"]";
    }
}