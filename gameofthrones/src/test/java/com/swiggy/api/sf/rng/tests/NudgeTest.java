package com.swiggy.api.sf.rng.tests;
import com.swiggy.api.sf.rng.dp.NudgeDP;
import com.swiggy.api.sf.rng.dp.SuperMultiTDDP;
import com.swiggy.api.sf.rng.helper.SuperDbHelper;
import com.swiggy.api.sf.rng.helper.SuperMultiTDHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.tests
 **/
public class NudgeTest extends NudgeDP {

    SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();
    @BeforeClass(alwaysRun = true)
    public void getBenefitPlanUserMapped () throws IOException, InterruptedException {


//        SuperDbHelper db = new SuperDbHelper();
//        db.updateValidTillByUserId
        superMultiTDHelper.deactiveAllFreeDelBenifits();
        superMultiTDHelper.deactiveAllSubscription();
        superMultiTDHelper.createPlanBenifitWithMapingSuper();
       // superMultiTDHelper.createSubscription();

    }


    @Test(dataProvider = "nudgecaseForMappedUser", priority = 2,groups = {"multitdRegression"},description = "nudgecaseForMappedUser")
    public void nudgeBusinessCases(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String,HashMap<String,List<String>>> eval, String caseType,String description) throws Exception {
        boolean flag= false;
        flag = superMultiTDHelper.caseHandlerForNudge(cases, actionAssert, eval, caseType);
        Assert.assertTrue(flag, "it has not found");

    }
    @Test(dataProvider = "nudgeCaseForSuperUser", priority = 2,groups = {"multitdRegression"},description = "nudgeCaseForSuperUser")
    public void nudegeSuperCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String,HashMap<String,List<String>>> eval, String caseType,String description) throws Exception {
        boolean flag= false;
        flag = superMultiTDHelper.caseHandlerForNudge(cases, actionAssert, eval, caseType);
        Assert.assertTrue(flag, "it has not found");

    }


    @Test(dataProvider = "nudgeCaseForNonSuperUser", priority = 2,groups = {"multitdRegression"},description = "nudgeCaseForNonSuperUser")
    public void nudgeNonsuper(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String,HashMap<String,List<String>>> eval, String caseType,String description) throws Exception {
        boolean flag= false;
        flag = superMultiTDHelper.caseHandlerForNudge(cases, actionAssert, eval, caseType);
        Assert.assertTrue(flag, "it has not found");

    }

}
