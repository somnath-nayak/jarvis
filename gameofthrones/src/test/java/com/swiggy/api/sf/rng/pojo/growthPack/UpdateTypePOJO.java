package com.swiggy.api.sf.rng.pojo.growthPack;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "meta",
        "updated_by"
})
public class UpdateTypePOJO {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("meta")
    private TypeMetaPOJO meta;
    @JsonProperty("updated_by")
    private String updated_by;

    /**
     * No args constructor for use in serialization
     *
     */
    public UpdateTypePOJO() {
    }

    /**
     *
     * @param id
     * @param name
     * @param meta
     * @param updated_by
     */
    public UpdateTypePOJO(Integer id, String name, TypeMetaPOJO meta, String updated_by) {
        super();
        this.id = id;
        this.name = name;
        this.meta = meta;
        this.updated_by = updated_by;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("meta")
    public TypeMetaPOJO getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(TypeMetaPOJO meta) {
        this.meta = meta;
    }

    @JsonProperty("updated_by")
    public String getUpdated_by() {
        return updated_by;
    }

    @JsonProperty("updated_by")
    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("meta", meta).append("updated_by", updated_by).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(name).append(meta).append(updated_by).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof UpdateTypePOJO) == false) {
            return false;
        }
        UpdateTypePOJO rhs = ((UpdateTypePOJO) other);
        return new EqualsBuilder().append(id, rhs.id).append(name, rhs.name).append(meta, rhs.meta).append(updated_by, rhs.updated_by).isEquals();
    }

    public UpdateTypePOJO withName(String name) {
        this.name=name;
        return this;
    }
    public UpdateTypePOJO withTypeMeta(TypeMetaPOJO meta) {
        this.meta=meta;
        return this;
    }

    public UpdateTypePOJO withUpdated_by(String updated_by) {
        this.updated_by =updated_by;
        return this;
    }


    public UpdateTypePOJO withId(int id) {
        this.id =id;
        return this;
    }

    public UpdateTypePOJO setDefault() {
        if (getId()== null){
            setId(1);
        }
        if(getName()==null){
            setName("TurboType");
        }
        if(getUpdated_by()==null){
            setUpdated_by(("Binit.ANAND@swiggy.in"));
        }
        if(getMeta()==null){
            new TypeMetaPOJO().setDefault();
        }
        return this;
    }

}
