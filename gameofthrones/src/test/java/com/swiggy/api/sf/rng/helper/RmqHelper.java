package com.swiggy.api.sf.rng.helper;

import com.rabbitmq.client.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.QueueData;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RmqHelper {


    Initialize init = new Initialize();
    String msg = "";
    String user_name ;
    String password ;
    JsonHelper jsonHelper = new JsonHelper();


    public RmqHelper (String username,String password){
        this.user_name= username;
        this.password=password;

    }
    
    public String getMessage(String hostName, String queueName) {
        String message=null;
        try {
            ConnectionFactory factory = new ConnectionFactory();
            QueueData qdetails = init.EnvironmentDetails.setup.GetQueueDetails(hostName, queueName);
            if((System.getProperty("sit-env") != null)) {
                factory.setUsername(user_name);
                factory.setPassword(password);
            }
            factory.setHost(qdetails.HostUrl);
            factory.setPort(qdetails.Port);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(qdetails.Name, true, false, false, null);
            GetResponse response= channel.basicGet(qdetails.Name, false);
            message = new String(response.getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }
    
    public void pushMessageToExchange(String hostName, String queueName, AMQP.BasicProperties.Builder amp, Object message) throws IOException, TimeoutException {

            ConnectionFactory factory = new ConnectionFactory();
            QueueData qdetails = init.EnvironmentDetails.setup.GetQueueDetails(hostName, queueName);
            if ((System.getProperty("sit-env") != null)) {
                factory.setUsername(user_name);
                factory.setPassword(password);
            }
            factory.setHost(qdetails.HostUrl);
            factory.setPort(qdetails.Port);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            System.out.println("Queue Name" + qdetails.Name);
            channel.basicPublish(qdetails.Exchangekey, qdetails.Routingkey, amp.build(), jsonHelper.getObjectToJSON(message).getBytes());
            System.out.println(" [x] Sent '" + message + "'");
            channel.close();
            connection.close();

    }
}
