package com.swiggy.api.sf.rng.dp.coupondp;

import com.swiggy.api.sf.rng.helper.CouponHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import org.testng.annotations.DataProvider;

import java.util.HashMap;

public class CouponTimeSlotDP {



    RngHelper rngHelper = new RngHelper();

    @DataProvider(name = "createPublicDiscountTypeCouponForUserTypeNonSuperData")
    public static Object[][] createPublicDiscountTypeCouponForUserTypeNonSuperData() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                    .setDefaultData()
                    .withValidFrom(Utility.getCurrentDate())
                    .withValidTill(Utility.getFutureDate())
                    .withCouponType("Discount")
                    .withDiscountPercentage(0)
                    .withDiscountAmount(20)
                    .withCouponUserTypeId(1)
                    .withPreferredPaymentMethod("AmazonPay,Sodexo")
                    .withDescription("Test")
                    .withCode("AUTOMATE" + Utility.getRandomPostfix());
            data.put("createCouponPOJO", createCouponPOJO);
            return new Object[][]{{data}};

        }
    }

    @DataProvider(name = "createPrivateDiscountTypeCouponForUserTypeNonSuperData")
    public static Object[][] createPrivateDiscountTypeCouponForUserTypeNonSuperData() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                    .setDefaultData()
                    .withValidFrom(Utility.getCurrentDate())
                    .withValidTill(Utility.getFutureDate())
                    .withCouponType("Discount")
                    .withIsPrivate(1)
                    .withDiscountPercentage(0)
                    .withDiscountAmount(20)
                    .withCouponUserTypeId(1)
                    .withPreferredPaymentMethod("AmazonPay,Sodexo")
                    .withDescription("Test")
                    .withCode("AUTOMATE" + Utility.getRandomPostfix());
            data.put("createCouponPOJO", createCouponPOJO);
            return new Object[][]{{data}};

        }
    }

    @DataProvider(name = "createPublicDiscountTypeWithFreeDelCouponForUserTypeNonSuperData")
    public static Object[][] createPublicDiscountTypeWithFreeDelCouponForUserTypeNonSuperData() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                    .setDefaultData()
                    .withValidFrom(Utility.getCurrentDate())
                    .withValidTill(Utility.getFutureDate())
                    .withCouponType("Discount")
                    .withIsPrivate(1)
                    .withFreeShipping(1)
                    .withDiscountPercentage(0)
                    .withDiscountAmount(20)
                    .withCouponUserTypeId(1)
                    .withPreferredPaymentMethod("AmazonPay,Sodexo")
                    .withDescription("Test")
                    .withCode("AUTOMATE" + Utility.getRandomPostfix());
            data.put("createCouponPOJO", createCouponPOJO);
            return new Object[][]{{data}};

        }
    }

    @DataProvider(name = "createPrivateDiscountTypeWithFreeDelCouponForUserTypeNonSuperData")
    public static Object[][] createPrivateDiscountTypeWithFreeDelCouponForUserTypeNonSuperData() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                    .setDefaultData()
                    .withValidFrom(Utility.getCurrentDate())
                    .withValidTill(Utility.getFutureDate())
                    .withCouponType("Discount")
                    .withIsPrivate(1)
                    .withFreeShipping(1)
                    .withDiscountPercentage(0)
                    .withDiscountAmount(20)
                    .withCouponUserTypeId(1)
                    .withPreferredPaymentMethod("AmazonPay,Sodexo")
                    .withDescription("Test")
                    .withCode("AUTOMATE" + Utility.getRandomPostfix());
            data.put("createCouponPOJO", createCouponPOJO);
            return new Object[][]{{data}};

        }
    }

    @DataProvider(name = "createPrivateDiscountTypeWithFreeDelCoupon")
    public static Object[][] createPublicDiscountTypeWithFreeDelCoupon() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                    .setDefaultData()
                    .withValidFrom(Utility.getCurrentDate())
                    .withValidTill(Utility.getFutureDate())
                    .withCouponType("Discount")
                    .withIsPrivate(1)
                    .withFreeShipping(1)
                    .withDiscountPercentage(0)
                    .withDiscountAmount(20)
                    .withCouponUserTypeId(1)
                    .withPreferredPaymentMethod("AmazonPay,Sodexo")
                    .withDescription("Test")
                    .withCode("AUTOMATE" + Utility.getRandomPostfix());
            data.put("createCouponPOJO", createCouponPOJO);
            return new Object[][]{{data}};

        }
    }


    @DataProvider(name = "createPublicUserTypeCouponWithTimeSlotRestrictionTrue")
    public static Object[][] createPublicUserTypeCouponWithTimeSlotRestrictionTrue() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().createCouponWithTimeSlotWithDayAllTrue(0,30,1);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrue")
    public static Object[][] createPrivateUserTypeCouponWithTimeSlotRestrictionTrue() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().createCouponWithTimeSlotWithDayAllTrue(1,30,1);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }


    @DataProvider(name = "createPublicUserTypeCouponWithTimeSlotRestrictionFalse")
    public static Object[][] createPublicUserTypeCouponWithTimeSlotRestrictionFalse() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().createCouponWithTimeSlotWithDayAllTrue(0,-1,0);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createPrivateUserTypeCouponWithTimeSlotRestrictionFalse")
    public static Object[][] createPrivateUserTypeCouponWithTimeSlotRestrictionFalse() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().createCouponWithTimeSlotWithDayAllTrue(1,-1,0);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createPublicUserTypeCouponWithTimeSlotRestrictionTrueWithValidFromAsPreviousMonth")
    public static Object[][] createPublicUserTypeCouponWithTimeSlotRestrictionTrueWithValidFromAsPreviousMonth() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().createCouponWithValidFromPreviousMonthTrue(0,30,1);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }


    @DataProvider(name = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrueWithValidFromAsPreviousMonth")
    public static Object[][] createPrivateUserTypeCouponWithTimeSlotRestrictionTrueWithValidFromAsPreviousMonth() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().createCouponWithValidFromPreviousMonthTrue(1,30,1);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createPublicUserTypeCouponWithTimeSlotRestrictionTrueWithOffsetZero")
    public static Object[][] createPublicUserTypeCouponWithTimeSlotRestrictionTrueWithOffsetZero() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().createCouponWithValidFromPreviousMonthTrue(0,0,1);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createPrivateUserTypeCouponWithTimeSlotRestrictionTrueWithOffsetZero")
    public static Object[][] createPrivateUserTypeCouponWithTimeSlotRestrictionTrueWithOffsetZero() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CouponHelper().createCouponWithValidFromPreviousMonthTrue(1,0,1);
        data.put("createCouponPOJO",createCouponPOJO);
        return new Object[][]{{data}};
    }



}
