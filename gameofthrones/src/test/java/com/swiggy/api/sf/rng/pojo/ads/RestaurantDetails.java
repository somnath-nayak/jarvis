package com.swiggy.api.sf.rng.pojo.ads;

import org.codehaus.jackson.annotate.JsonProperty;

public class RestaurantDetails {
    @JsonProperty("rest_id")
    private Integer restId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("cityId")
    private Integer cityId;
    @JsonProperty("areaId")
    private Integer areaId;
    @JsonProperty("area")
    private String area;
    @JsonProperty("city")
    private String city;
    @JsonProperty("exclusive")
    private Boolean exclusive;
    @JsonProperty("parentId")
    private Integer parentId;
    @JsonProperty("promoted")
    private Boolean promoted;
    @JsonProperty("position")
    private Integer position;

    @JsonProperty("rest_id")
    public Integer getRestId() {
        return restId;
    }

    @JsonProperty("rest_id")
    public void setRestId(Integer restId) {
        this.restId = restId;
    }

    public RestaurantDetails withRestId(Integer restId) {
        this.restId = restId;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public RestaurantDetails withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("cityId")
    public Integer getCityId() {
        return cityId;
    }

    @JsonProperty("cityId")
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public RestaurantDetails withCityId(Integer cityId) {
        this.cityId = cityId;
        return this;
    }

    @JsonProperty("areaId")
    public Integer getAreaId() {
        return areaId;
    }

    @JsonProperty("areaId")
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public RestaurantDetails withAreaId(Integer areaId) {
        this.areaId = areaId;
        return this;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    public RestaurantDetails withArea(String area) {
        this.area = area;
        return this;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    public RestaurantDetails withCity(String city) {
        this.city = city;
        return this;
    }

    @JsonProperty("exclusive")
    public Boolean getExclusive() {
        return exclusive;
    }

    @JsonProperty("exclusive")
    public void setExclusive(Boolean exclusive) {
        this.exclusive = exclusive;
    }

    public RestaurantDetails withExclusive(Boolean exclusive) {
        this.exclusive = exclusive;
        return this;
    }

    @JsonProperty("parentId")
    public Integer getParentId() {
        return parentId;
    }

    @JsonProperty("parentId")
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public RestaurantDetails withParentId(Integer parentId) {
        this.parentId = parentId;
        return this;
    }

    @JsonProperty("promoted")
    public Boolean getPromoted() {
        return promoted;
    }

    @JsonProperty("promoted")
    public void setPromoted(Boolean promoted) {
        this.promoted = promoted;
    }

    public RestaurantDetails withPromoted(Boolean promoted) {
        this.promoted = promoted;
        return this;
    }

    @JsonProperty("position")
    public Integer getPosition() {
        return position;
    }

    @JsonProperty("position")
    public void setPosition(Integer position) {
        this.position = position;
    }

    public RestaurantDetails withPosition(Integer position) {
        this.position = position;
        return this;
    }

    public RestaurantDetails setDefault(){
        return this
                .withRestId(212)
                .withCityId(1)
                .withAreaId(2)
                .withArea("Koramangala")
                .withCity("Bangalore")
                .withName("TestDevkaran")
                .withExclusive(false)
//                .withParentId(20)
                .withPromoted(true)
                .withPosition(1);


    }

}

