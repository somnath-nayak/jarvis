package com.swiggy.api.sf.rng.pojo;
import java.util.List;

public class MenuEvaluateRequest {


        private String minCartAmount;

        private String firstOrder;

        private String userId;

        private String userAgent;

        private String versionCode;

        private List<ItemRequest> itemRequests;

        public void setMinCartAmount(String minCartAmount){
            this.minCartAmount = minCartAmount;
        }
        public String getMinCartAmount(){
            return this.minCartAmount;
        }
        public void setFirstOrder(String firstOrder){
            this.firstOrder = firstOrder;
        }
        public String getFirstOrder(){
            return this.firstOrder;
        }
        public void setUserId(String userId){
            this.userId = userId;
        }
        public String getUserId(){
            return this.userId;
        }
        public void setUserAgent(String userAgent){
            this.userAgent = userAgent;
        }
        public String getUserAgent(){
            return this.userAgent;
        }
        public void setVersionCode(String versionCode){
            this.versionCode = versionCode;
        }
        public String getVersionCode(){
            return this.versionCode;
        }
        public void setItemRequests(List<ItemRequest> itemRequests){
            this.itemRequests = itemRequests;
        }
        public List<ItemRequest> getItemRequests(){
            return this.itemRequests;
        }
    public MenuEvaluateRequest(String minCartAmount, String userId, String firstOrder, String userAgent, String versionCode, List<ItemRequest> itemRequests) {
        this.minCartAmount = minCartAmount;
        this.userId = userId;
        this.firstOrder = firstOrder;
        this.userAgent = userAgent;
        this.versionCode = versionCode;
        this.itemRequests = itemRequests;
    }
    public String toString() {
        return "{" +
                "minCartAmount='" + minCartAmount + '\'' +
                ", userId='" + userId + '\'' +
                ", firstOrder='" + firstOrder + '\'' +
                ", userAgent='" + userAgent + '\'' +
                ", versionCode='" + versionCode + '\'' +
                ", itemRequests=" + itemRequests +
                '}';
    }

}
