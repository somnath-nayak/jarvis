package com.swiggy.api.sf.rng.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.dp.Freebie_DP;
import com.swiggy.api.sf.rng.helper.RngHelper;

import framework.gameofthrones.JonSnow.Processor;

public class CreateTDWithListingRestaurants extends DP {

	RngHelper rngHelper = new RngHelper();
	
	
	@Test(dataProvider = "createPercentageTD", groups = "sanity", enabled = true,priority = 2)
	public void createPercentageTradeDiscount(String payload) {
		Processor processor=rngHelper.createTD(payload);
		String response =	processor.ResponseValidator.GetBodyAsText();rngHelper.TDstatusChecker(response);
		Assert.assertEquals(JsonPath.read(response,"$.statusMessage").toString(), "done");
		Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");
	}

	@Test(dataProvider = "createFlatTDLevel", groups = "sanity", enabled = true,priority = 2)
	public void createFlatTradeDiscount(String payload) {
		Processor processor=rngHelper.createTD(payload);
		String response =	processor.ResponseValidator.GetBodyAsText();rngHelper.TDstatusChecker(response);
		Assert.assertEquals(JsonPath.read(response,"$.statusMessage").toString(), "done");
		Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");
	}

	@Test(dataProvider = "createFreebieTD", dataProviderClass = Freebie_DP.class, groups = "regression", enabled = true,priority = 3)
	public void createFreebieTradeDiscount(String payload) {
		System.out.println("payload"+payload);
		Processor processor=rngHelper.createTD(payload);
		String response =	processor.ResponseValidator.GetBodyAsText();
		rngHelper.TDstatusChecker(response);
		Assert.assertEquals(JsonPath.read(response,"$.statusMessage").toString(), "done");
		Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");
	}

	@Test(dataProvider = "createFreeDeliveryTD", groups = "regression", enabled = true,priority = 4)
	public void createFreeDeliveryTradeDiscount(String payload) {
		System.out.println("payload"+payload);
		Processor processor=rngHelper.createTD(payload);
		String response =	processor.ResponseValidator.GetBodyAsText();
		rngHelper.TDstatusChecker(response);
		Assert.assertEquals(JsonPath.read(response,"$.statusMessage").toString(), "done");
		Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");
	}

}
