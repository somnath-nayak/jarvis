package com.swiggy.api.sf.rng.pojo.TDTemplate;

import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.helper.TDHelper.TDTemplateHelper;

public class TemplateCreationBuilder {

    public TemplateCreation createTemplate;

    public TemplateCreationBuilder() {
        createTemplate = new TemplateCreation();
    }

    TDTemplateHelper tdTemplateHelper = new TDTemplateHelper();


    public TemplateCreationBuilder withTemplateName(String templateName) {
        createTemplate.setTemplateName(templateName);
        return this;
    }

    public TemplateCreationBuilder withName(String name) {
        createTemplate.setName(name);
        return this;
    }

    public TemplateCreationBuilder withHeader(String header) {
        createTemplate.setHeader(header);
        return this;
    }

    public TemplateCreationBuilder withDescription(String description) {
        createTemplate.setDescription(description);
        return this;
    }

    public TemplateCreationBuilder withShortDescription(String shortDescription) {
        createTemplate.setShortDescription(shortDescription);
        return this;
    }

    public TemplateCreationBuilder withValidFrom(long validFrom) {
        createTemplate.setValidFrom(validFrom);
        return this;
    }

    public TemplateCreationBuilder withValidTill(long validTill) {
        createTemplate.setValidTill(validTill);
        return this;
    }

    public TemplateCreationBuilder withCampaignType(String campaignType) {
        createTemplate.setCampaignType(campaignType);
        return this;
    }


    public TemplateCreationBuilder withTimeSpanType(String timeSpanType) {
        createTemplate.setTimeSpanType(timeSpanType);
        return this;
    }

    public TemplateCreationBuilder withTimeSpanLength(int timeSpanLength) {
        createTemplate.setTimeSpanLength(timeSpanLength);
        return this;
    }


    public TemplateCreationBuilder withRewardValue(String rewardValue) {
        createTemplate.setRewardValue(rewardValue);
        return this;
    }


    public TemplateCreationBuilder withCreatedBy(String createdBy) {
        createTemplate.setCreatedBy(createdBy);
        return this;
    }


    public TemplateCreationBuilder withIsFirstOrder(boolean isFirstOrder) {
        createTemplate.setIsFirstOrder(isFirstOrder);
        return this;
    }

    public TemplateCreationBuilder withMinCartAmount(int minCartAmount) {
        createTemplate.setMinCartAmount(minCartAmount);
        return this;
    }

    public TemplateCreationBuilder withDiscountCap(int discountCap) {
        createTemplate.setDiscountCap(discountCap);
        return this;
    }

    public TemplateCreationBuilder withUserRestriction(Boolean userRestriction) {
        createTemplate.setUserRestriction(userRestriction);
        return this;
    }


    public TemplateCreationBuilder withTimeSlotRestriction(boolean timeSlotRestriction) {
        createTemplate.setTimeSlotRestriction(timeSlotRestriction);
        return this;
    }

    public TemplateCreationBuilder withCommissionOnFullBill(boolean commissionOnFullBill) {
        createTemplate.setCommissionOnFullBill(commissionOnFullBill);
        return this;
    }

    public TemplateCreationBuilder withTaxesOnDiscountedBill(boolean taxesOnDiscountedBill) {
        createTemplate.setTaxesOnDiscountedBill(taxesOnDiscountedBill);
        return this;
    }

    public TemplateCreationBuilder withDormantUserType(String  dormantUserType) {
        createTemplate.setDormantUserType(dormantUserType);
        return this;
    }



    public TemplateCreationBuilder withRestaurantFirstOrder(Boolean rfo) {
        createTemplate.setIsRestaurantFirstOrder(rfo);
        return this;
    }



    public TemplateCreation build() {
        getDefaultValue();
        return createTemplate;
    }

    private void getDefaultValue() {
        if (createTemplate.getTemplateName() == null) {
            createTemplate.setTemplateName("Test-Template");
        }
        if (createTemplate.getName() == null) {
            createTemplate.setName("TD_Name");
        }
        if (createTemplate.getHeader() == null) {

            createTemplate.setHeader("25 off");
        }

        if (createTemplate.getDescription() == null) {
            createTemplate.setDescription("Flat 25 off");
        }
        if (createTemplate.getShortDescription() == null) {
            createTemplate.setShortDescription("Flat 25 off");
        }
        if (createTemplate.getValidFrom() == null) {
           long validfrom=Long.parseLong(( tdTemplateHelper.getFutureTimeForMinutesInMilliSecondsFromCurrentDate(1)));
            createTemplate.setValidFrom(validfrom);
        }
        if (createTemplate.getValidTill() == null) {
            long validtill=Long.parseLong(( tdTemplateHelper.getFutureTimeForMonthsInMilliSecondsFromCurrentDate(1)));
            createTemplate.setValidFrom(validtill);
        }

        if (createTemplate.getCampaignType() == null) {

            createTemplate.setCampaignType(RngConstants.FREE_DELIVERY);
        }

        if (createTemplate.getTimeSpanType() == null) {

            createTemplate.setTimeSpanType("SUGGESTED");
        }
        if (createTemplate.getTimeSpanLength() == null) {

            createTemplate.setTimeSpanLength(20000);
        }
        if (createTemplate.getRewardValue() == null) {

            createTemplate.setRewardValue("25");
        }
        if (createTemplate.getCreatedBy() == null) {

            createTemplate.setCreatedBy(RngConstants.CREATED_BY);
        }

        if (createTemplate.getIsFirstOrder() == null) {

            createTemplate.setIsFirstOrder(Boolean.TRUE);
        }

        if (createTemplate.getMinCartAmount() == null) {

            createTemplate.setMinCartAmount(0);
        }
        if (createTemplate.getDiscountCap() == null) {

            createTemplate.setDiscountCap(1000);
        }
        if (createTemplate.getUserRestriction() == null) {

            createTemplate.setUserRestriction(Boolean.FALSE);
        }
        if (createTemplate.getTimeSlotRestriction() == null) {

            createTemplate.setTimeSlotRestriction(Boolean.FALSE);
        }
        if (createTemplate.getCommissionOnFullBill() == null) {

            createTemplate.setCommissionOnFullBill(Boolean.FALSE);
        }
        if (createTemplate.getTaxesOnDiscountedBill() == null) {

            createTemplate.setTaxesOnDiscountedBill(Boolean.FALSE);
        }
        if(createTemplate.getIsRestaurantFirstOrder() ==null){
            createTemplate.setIsRestaurantFirstOrder(Boolean.FALSE);
        }



    }















}
