package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cmsAttributes.Attributes;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "rewardType",
        "mealQuantity",
        "menu_item_id",
        "name",
        "attributes",
        "base_price",
        "variants_price",
        "addons_price",
        "quantity",
        "subtotal",
        "subtotal_trade_discount",
        "packing_charge",
        "total",
        "final_price",
        "is_customized",
        "id_hash",
        "valid_addons",
        "valid_variants",
        "valid_variants_v2",
        "is_veg",
        "addons",
        "variants",
        "item_taxes",
        "GST_details",
        "cloudinaryImageId",
        "in_stock",
        "inventory",
        "inventory_message",
        "inventory_insufficient_message",
        "added_by_user_id",
        "added_by_user_name"
})
public class CartMenuItem {

    @JsonProperty("rewardType")
    private Object rewardType;
    @JsonProperty("mealQuantity")
    private Integer mealQuantity;
    @JsonProperty("menu_item_id")
    private Integer menuItemId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("attributes")
    private Attributes attributes;
    @JsonProperty("base_price")
    private Double basePrice;
    @JsonProperty("variants_price")
    private Double variantsPrice;
    @JsonProperty("addons_price")
    private Double addonsPrice;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("subtotal")
    private Double subtotal;
    @JsonProperty("subtotal_trade_discount")
    private Double subtotalTradeDiscount;
    @JsonProperty("packing_charge")
    private Integer packingCharge;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("final_price")
    private Double finalPrice;
    @JsonProperty("is_customized")
    private Object isCustomized;
    @JsonProperty("id_hash")
    private String idHash;
    @JsonProperty("valid_addons")
    private List<Object> validAddons = null;
    @JsonProperty("valid_variants")
    private Object validVariants;
    @JsonProperty("valid_variants_v2")
    private Object validVariantsV2;
    @JsonProperty("is_veg")
    private String isVeg;
    @JsonProperty("addons")
    private List<Addon> addons = null;
    @JsonProperty("variants")
    private List<Variant> variants = null;
    @JsonProperty("item_taxes")
    private ItemTaxes itemTaxes;
    @JsonProperty("GST_details")
    private GSTDetails gSTDetails;
    @JsonProperty("cloudinaryImageId")
    private String cloudinaryImageId;
    @JsonProperty("in_stock")
    private Integer inStock;
    @JsonProperty("inventory")
    private Integer inventory;
    @JsonProperty("inventory_message")
    private Object inventoryMessage;
    @JsonProperty("inventory_insufficient_message")
    private Object inventoryInsufficientMessage;
    @JsonProperty("added_by_user_id")
    private Integer addedByUserId;
    @JsonProperty("added_by_user_name")
    private String addedByUserName;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public CartMenuItem() {
    }

    /**
     *
     * @param total
     * @param validVariantsV2
     * @param addonsPrice
     * @param addedByUserName
     * @param variantsPrice
     * @param finalPrice
     * @param cloudinaryImageId
     * @param isVeg
     * @param name
     * @param idHash
     * @param variants
     * @param quantity
     * @param mealQuantity
     * @param itemTaxes
     * @param menuItemId
     * @param inventoryInsufficientMessage
     * @param packingCharge
     * @param addons
     * @param validAddons
     * @param subtotal
     * @param addedByUserId
     * @param rewardType
     * @param isCustomized
     * @param validVariants
     * @param gSTDetails
     * @param inventory
     * @param inventoryMessage
     * @param inStock
     * @param basePrice
     * @param subtotalTradeDiscount
     */
    public CartMenuItem(Object rewardType, Integer mealQuantity, Integer menuItemId, String name, Attributes attributes, Double basePrice, Double variantsPrice, Double addonsPrice, Integer quantity, Double subtotal, Double subtotalTradeDiscount, Integer packingCharge, Integer total, Double finalPrice, Object isCustomized, String idHash, List<Object> validAddons, Object validVariants, Object validVariantsV2, String isVeg, List<Addon> addons, List<Variant> variants, ItemTaxes itemTaxes, GSTDetails gSTDetails, String cloudinaryImageId, Integer inStock, Integer inventory, Object inventoryMessage, Object inventoryInsufficientMessage, Integer addedByUserId, String addedByUserName) {
        super();
        this.rewardType = rewardType;
        this.mealQuantity = mealQuantity;
        this.menuItemId = menuItemId;
        this.name = name;
        this.attributes = attributes;
        this.basePrice = basePrice;
        this.variantsPrice = variantsPrice;
        this.addonsPrice = addonsPrice;
        this.quantity = quantity;
        this.subtotal = subtotal;
        this.subtotalTradeDiscount = subtotalTradeDiscount;
        this.packingCharge = packingCharge;
        this.total = total;
        this.finalPrice = finalPrice;
        this.isCustomized = isCustomized;
        this.idHash = idHash;
        this.validAddons = validAddons;
        this.validVariants = validVariants;
        this.validVariantsV2 = validVariantsV2;
        this.isVeg = isVeg;
        this.addons = addons;
        this.variants = variants;
        this.itemTaxes = itemTaxes;
        this.gSTDetails = gSTDetails;
        this.cloudinaryImageId = cloudinaryImageId;
        this.inStock = inStock;
        this.inventory = inventory;
        this.inventoryMessage = inventoryMessage;
        this.inventoryInsufficientMessage = inventoryInsufficientMessage;
        this.addedByUserId = addedByUserId;
        this.addedByUserName = addedByUserName;
    }

    @JsonProperty("rewardType")
    public Object getRewardType() {
        return rewardType;
    }

    @JsonProperty("rewardType")
    public void setRewardType(Object rewardType) {
        this.rewardType = rewardType;
    }

    @JsonProperty("mealQuantity")
    public Integer getMealQuantity() {
        return mealQuantity;
    }

    @JsonProperty("mealQuantity")
    public void setMealQuantity(Integer mealQuantity) {
        this.mealQuantity = mealQuantity;
    }

    @JsonProperty("menu_item_id")
    public Integer getMenuItemId() {
        return menuItemId;
    }

    @JsonProperty("menu_item_id")
    public void setMenuItemId(Integer menuItemId) {
        this.menuItemId = menuItemId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("base_price")
    public Double getBasePrice() {
        return basePrice;
    }

    @JsonProperty("base_price")
    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    @JsonProperty("variants_price")
    public Double getVariantsPrice() {
        return variantsPrice;
    }

    @JsonProperty("variants_price")
    public void setVariantsPrice(Double variantsPrice) {
        this.variantsPrice = variantsPrice;
    }

    @JsonProperty("addons_price")
    public Double getAddonsPrice() {
        return addonsPrice;
    }

    @JsonProperty("addons_price")
    public void setAddonsPrice(Double addonsPrice) {
        this.addonsPrice = addonsPrice;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("subtotal")
    public Double getSubtotal() {
        return subtotal;
    }

    @JsonProperty("subtotal")
    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    @JsonProperty("subtotal_trade_discount")
    public Double getSubtotalTradeDiscount() {
        return subtotalTradeDiscount;
    }

    @JsonProperty("subtotal_trade_discount")
    public void setSubtotalTradeDiscount(Double subtotalTradeDiscount) {
        this.subtotalTradeDiscount = subtotalTradeDiscount;
    }

    @JsonProperty("packing_charge")
    public Integer getPackingCharge() {
        return packingCharge;
    }

    @JsonProperty("packing_charge")
    public void setPackingCharge(Integer packingCharge) {
        this.packingCharge = packingCharge;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    @JsonProperty("final_price")
    public Double getFinalPrice() {
        return finalPrice;
    }

    @JsonProperty("final_price")
    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    @JsonProperty("is_customized")
    public Object getIsCustomized() {
        return isCustomized;
    }

    @JsonProperty("is_customized")
    public void setIsCustomized(Object isCustomized) {
        this.isCustomized = isCustomized;
    }

    @JsonProperty("id_hash")
    public String getIdHash() {
        return idHash;
    }

    @JsonProperty("id_hash")
    public void setIdHash(String idHash) {
        this.idHash = idHash;
    }

    @JsonProperty("valid_addons")
    public List<Object> getValidAddons() {
        return validAddons;
    }

    @JsonProperty("valid_addons")
    public void setValidAddons(List<Object> validAddons) {
        this.validAddons = validAddons;
    }

    @JsonProperty("valid_variants")
    public Object getValidVariants() {
        return validVariants;
    }

    @JsonProperty("valid_variants")
    public void setValidVariants(Object validVariants) {
        this.validVariants = validVariants;
    }

    @JsonProperty("valid_variants_v2")
    public Object getValidVariantsV2() {
        return validVariantsV2;
    }

    @JsonProperty("valid_variants_v2")
    public void setValidVariantsV2(Object validVariantsV2) {
        this.validVariantsV2 = validVariantsV2;
    }

    @JsonProperty("is_veg")
    public String getIsVeg() {
        return isVeg;
    }

    @JsonProperty("is_veg")
    public void setIsVeg(String isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("addons")
    public List<Addon> getAddons() {
        return addons;
    }

    @JsonProperty("addons")
    public void setAddons(List<Addon> addons) {
        this.addons = addons;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    @JsonProperty("item_taxes")
    public ItemTaxes getItemTaxes() {
        return itemTaxes;
    }

    @JsonProperty("item_taxes")
    public void setItemTaxes(ItemTaxes itemTaxes) {
        this.itemTaxes = itemTaxes;
    }

    @JsonProperty("GST_details")
    public GSTDetails getGSTDetails() {
        return gSTDetails;
    }

    @JsonProperty("GST_details")
    public void setGSTDetails(GSTDetails gSTDetails) {
        this.gSTDetails = gSTDetails;
    }

    @JsonProperty("cloudinaryImageId")
    public String getCloudinaryImageId() {
        return cloudinaryImageId;
    }

    @JsonProperty("cloudinaryImageId")
    public void setCloudinaryImageId(String cloudinaryImageId) {
        this.cloudinaryImageId = cloudinaryImageId;
    }

    @JsonProperty("in_stock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("in_stock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("inventory")
    public Integer getInventory() {
        return inventory;
    }

    @JsonProperty("inventory")
    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    @JsonProperty("inventory_message")
    public Object getInventoryMessage() {
        return inventoryMessage;
    }

    @JsonProperty("inventory_message")
    public void setInventoryMessage(Object inventoryMessage) {
        this.inventoryMessage = inventoryMessage;
    }

    @JsonProperty("inventory_insufficient_message")
    public Object getInventoryInsufficientMessage() {
        return inventoryInsufficientMessage;
    }

    @JsonProperty("inventory_insufficient_message")
    public void setInventoryInsufficientMessage(Object inventoryInsufficientMessage) {
        this.inventoryInsufficientMessage = inventoryInsufficientMessage;
    }

    @JsonProperty("added_by_user_id")
    public Integer getAddedByUserId() {
        return addedByUserId;
    }

    @JsonProperty("added_by_user_id")
    public void setAddedByUserId(Integer addedByUserId) {
        this.addedByUserId = addedByUserId;
    }

    @JsonProperty("added_by_user_name")
    public String getAddedByUserName() {
        return addedByUserName;
    }

    @JsonProperty("added_by_user_name")
    public void setAddedByUserName(String addedByUserName) {
        this.addedByUserName = addedByUserName;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("rewardType", rewardType).append("mealQuantity", mealQuantity).append("menuItemId", menuItemId).append("name", name).append("basePrice", basePrice).append("variantsPrice", variantsPrice).append("addonsPrice", addonsPrice).append("quantity", quantity).append("subtotal", subtotal).append("subtotalTradeDiscount", subtotalTradeDiscount).append("packingCharge", packingCharge).append("total", total).append("finalPrice", finalPrice).append("isCustomized", isCustomized).append("idHash", idHash).append("validAddons", validAddons).append("validVariants", validVariants).append("validVariantsV2", validVariantsV2).append("isVeg", isVeg).append("addons", addons).append("variants", variants).append("itemTaxes", itemTaxes).append("gSTDetails", gSTDetails).append("cloudinaryImageId", cloudinaryImageId).append("inStock", inStock).append("inventory", inventory).append("inventoryMessage", inventoryMessage).append("inventoryInsufficientMessage", inventoryInsufficientMessage).append("addedByUserId", addedByUserId).append("addedByUserName", addedByUserName).append("additionalProperties", additionalProperties).toString();
    }

}