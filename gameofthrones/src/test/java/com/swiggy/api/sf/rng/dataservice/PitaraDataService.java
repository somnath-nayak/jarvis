package com.swiggy.api.sf.rng.dataservice;

import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.pojo.pitara.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Cersei.DataGenerator;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/*
This class is used to create all comobination of data in two dimension array which will help to provide the data to our
dataprovider method. This will save lots of time to prepare your dataset creation.
 */

public class PitaraDataService {

    /*
    The below method returns all the possible combination of data
     */

    UserOrderSegment uos;
    UserSegment us;
    Payload pl,pl2,pl3;
    NUX_LISTING nx,nx2;
    SCARD sc;
    Collection cl,cl2;
    RAIN_NUDGE_CARD rnc,rnc2,rnc3;
    Mode mode1,mode2;
    LCARD lcard;
    XLCARD xlcard;


    public PitaraDataService() {

        uos = new UserOrderSegment();
        us = new UserSegment();
        pl = new Payload();
        pl2 = new Payload();
        nx = new NUX_LISTING();
        nx2 = new NUX_LISTING();
        cl = new Collection();
        cl2= new Collection();

    }

    static Initialize gameofthrones = new Initialize();

    DataGenerator generator = new DataGenerator();


    public Object[][] getNuxCardData(boolean validation) {


        List<UserOrderSegment> userOrderSegment = new ArrayList<UserOrderSegment>();
        uos.setFirstOrderCityId(2);
        uos.setLastOrderCityId(1);
        uos.setLastOrderTime(15163609);
        uos.setOrderCount(1);
        userOrderSegment.add(uos);


        List<UserSegment> userSegment = new ArrayList<UserSegment>();
        us.setNewUser(0);
        us.setUserId(3074533);

        userSegment.add(us);

        List<Collection> collection = new ArrayList<Collection>();
        List<Collection> collection2 = new ArrayList<Collection>();
//        if (!validation) {
//            cl.setCount(0);
//            cl.setId(566);
//            collection.add(cl);
//        }
        cl.setCount(0);
        cl.setId(566);
        collection.add(cl);



        nx.setType("listing");
        nx.setCount(-1);
        nx.setCollections(collection);

        List<NUX_LISTING> nuxListing = new ArrayList<NUX_LISTING>();
        nuxListing.add(nx);


        List<Payload> payLoads = new ArrayList<Payload>();
        pl.setNUXLISTING(nx);

        payLoads.add(pl);

        collection2.add(cl2);
        nx2.setCollections(collection2);
        pl2.setNUXLISTING(nx2);
        payLoads.add(pl2);

        return generator.generatevariants2(userOrderSegment, userSegment, payLoads);
    }




    public Object[][] getCouponCard(boolean validation) {


        List<UserOrderSegment> userOrderSegment = new ArrayList<UserOrderSegment>();
        uos.setFirstOrderCityId(2);
        uos.setLastOrderCityId(1);
        uos.setLastOrderTime(15163609);
        uos.setOrderCount(1);
        userOrderSegment.add(uos);


        List<UserSegment> userSegment = new ArrayList<UserSegment>();
        us.setNewUser(0);
        us.setUserId(3074533);

        userSegment.add(us);



        List<Collection> collection2 = new ArrayList<Collection>();
//        if (!validation) {
//            cl.setCount(0);
//            cl.setId(566);
//            collection.add(cl);
//        }




        cl.setCount(-1);
        cl.setId(566);

        List<Payload> payLoads = new ArrayList<Payload>();
        collection2.add(cl);
        nx2.setCollections(collection2);
        nx2.setCount(-1);
        pl2.setNUXLISTING(nx2);

        payLoads.add(pl2);

        return generator.generatevariants2(userOrderSegment, userSegment, payLoads);
    }

//    public enum CardType {
//        nuxcard(RngConstants.nuxCard),
//        lcard(RngConstants.lCard),
//        scard(RngConstants.sCard),
//        xlcard(RngConstants.xlCard),
//        scardv2(RngConstants.sCardV2),
//        rain(RngConstants.rainCard),
//        launchcard(RngConstants.launchCard),
//        smallnudgecard(RngConstants.smallNudgeCard),
//        bannercard(RngConstants.bannerCard);
//        private final String name;
//
//        private CardType(String card) {
//            this.name = card;
//        }
//
//        public boolean equalsName(String otherName) {
//            // (otherName == null) check is not needed because name.equals(null) returns false
//            return name.equals(otherName);
//        }
//
//        public String toString() {
//            return this.name;
//        }
//    }
//
//    public List<String> createCard(CardType type,int times) {
//
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        List<Map<String, Object>> list ;
//        List<String> ids=new ArrayList<String>();
//        HashMap<String, String> card = new HashMap<>();
//        for (int i=0;i<times;i++)
//        {
//            ids.add(sqlTemplate.queryForList(type.toString()).toString());
//        }
//
//        return ids;
//    }
//
//    public String createCard(CardType type) {
//
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        List<Map<String, Object>> list = sqlTemplate.queryForList(type.toString());
//        return list.get(0).get("id").toString();
//    }
//
//    public HashMap<String, String> createAllCards() {
//        HashMap<String, String> allCards = new HashMap<>();
//        for (CardType status : CardType.values()) {
//            allCards.put(status.name(), createCard(status));
//        }
//        return allCards;
//    }


    public enum ValidationType {
        collection(RngConstants.nuxCard),
        coupon(RngConstants.lCard),
        showrate(RngConstants.showRateValidation);

        private final String name;

        ValidationType(String card) {
            this.name = card;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
        }


    public String collectionValidationNew(String collectionID) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        String query = String.format(RngConstants.collectionValidation, collectionID);
        System.out.println(query + "===============================>");
        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
        return list.get(0).get("id").toString();
    }
//    }
//    public String collectionValidation(String collectionID)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.collectionValidation+collectionID+RngConstants.endCollectionValidation;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String couponValidation(String couponCode)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.couponValidation+couponCode+RngConstants.endCouponValidation;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String showRateAppCardValidation(boolean isTrue)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = showRateValidation+isTrue+RngConstants.endShowRateValidation;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String isPreOrderEnabledValidation(boolean isTrue)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.isPreOrderEnabledValidation+isTrue+RngConstants.endIsPreOrderEnabledValidaiton;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String HasPlacedPreOrderValidation(boolean isTrue)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.hasPlacePreOrderValidation+isTrue+RngConstants.endHasPlacePreOrerValidtion;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String isPreOrderValidation(boolean isTrue)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.isPreOrderValidation+isTrue+RngConstants.endIsPreOrdeValiation;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String isLongDistanceOrderValidation(boolean isTrue)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.isLongDistanceOrder+isTrue+RngConstants.endIsLongDistanceOrder;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String hasPlacedLongDistanceOrderValidation(boolean isTrue)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.hasPlacedLongDistanceOrder+isTrue+RngConstants.endHasPlacedLongDistanceOrder;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String cafeAvailabledValidation(boolean isTrue)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.cafeAvailableValidation+isTrue+RngConstants.endCafeAvailableValidation;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String cafeCorporateAvailableValidation(boolean IsTrue)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.cafeAvailableValidation+isTrue+RngConstants.endCafeAvailableValidation;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }
//    public String rainCardValiadation(String type)
//    {
//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
//        String query = RngConstants.cafeAvailableValidation+isTrue+RngConstants.endCafeAvailableValidation;
//        System.out.println(query+"===============================>");
//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
//        return list.get(0).get("id").toString();
//    }




    public Object[][] getScardCardData(boolean validation) {
        List<String> urlParam =new ArrayList<String>();
        urlParam.add("0");
        urlParam.add("1");
        List<UserOrderSegment> userOrderSegment = new ArrayList<UserOrderSegment>();
        uos.setFirstOrderCityId(2);
        uos.setLastOrderCityId(1);
        uos.setLastOrderTime(15163609);
        uos.setOrderCount(1);
        userOrderSegment.add(uos);


        List<UserSegment> userSegment = new ArrayList<UserSegment>();
        us.setNewUser(0);
        us.setUserId(3074533);

        userSegment.add(us);
        List<Payload> payLoads = new ArrayList<Payload>();
        sc = new SCARD();
        sc.setCount(-1);
        pl.setS_CARD(sc);
        payLoads.add(pl);

        return generator.generatevariants2(userOrderSegment, userSegment, payLoads,urlParam);
    }

    public Object[][] getRainCardData(boolean validation) {
        List<String> urlParam =new ArrayList<String>();
        urlParam.add("0");
        urlParam.add("1");
        List<UserOrderSegment> userOrderSegment = new ArrayList<UserOrderSegment>();
        uos.setFirstOrderCityId(2);
        uos.setLastOrderCityId(1);
        uos.setLastOrderTime(15163609);
        uos.setOrderCount(1);
        userOrderSegment.add(uos);


        List<UserSegment> userSegment = new ArrayList<UserSegment>();
        us.setNewUser(0);
        us.setUserId(3074533);

        userSegment.add(us);
        List<Payload> payLoads = new ArrayList<Payload>();
        rnc = new RAIN_NUDGE_CARD();
        rnc2 = new RAIN_NUDGE_CARD();rnc3 = new RAIN_NUDGE_CARD();
        rnc.setCount(-1);
        rnc.setType("pop");
        rnc2.setCount(-1);
        rnc2.setType("pop");
        rnc3.setCount(-1);
        rnc3.setType("pop");

        mode1 = new Mode();
        mode2 = new Mode();
        mode1.setType("RAIN");
        mode1.setValue("EXTREME");
        mode2.setType("RAIN");
        mode2.setValue("NORMAL");

        List<Mode> modes = new ArrayList<>();
        List<Mode> modes2 = new ArrayList<>();
        modes.add(mode1);
        modes2.add(mode2);
        //modes.add(mode2);
        rnc.setModes(modes);
        rnc2.setModes(modes2);

        pl.setRAIN_NUDGE_CARD(rnc);
        pl2.setRAIN_NUDGE_CARD(rnc2);
        payLoads.add(pl);
        payLoads.add(pl2);

        return generator.generatevariants2(userOrderSegment, userSegment, payLoads);
    }

    public Object[][] getLCardData(boolean validation) {

        List<String> urlParam =new ArrayList<String>();
        urlParam.add("1");
        urlParam.add("0");

        List<UserOrderSegment> userOrderSegment = new ArrayList<UserOrderSegment>();
        uos.setFirstOrderCityId(2);
        uos.setLastOrderCityId(1);
        uos.setLastOrderTime(15163609);
        uos.setOrderCount(1);
        userOrderSegment.add(uos);


        List<UserSegment> userSegment = new ArrayList<UserSegment>();
        us.setNewUser(0);
        us.setUserId(3074533);

        userSegment.add(us);
        List<Payload> payLoads = new ArrayList<Payload>();
        lcard = new LCARD();
        lcard.setCount(-1);
        pl.setLCARD(lcard);
        payLoads.add(pl);

        return generator.generatevariants2(userOrderSegment, userSegment, payLoads,urlParam);
    }



    public Object[][] getXLCardData(boolean validation) {
        List<String> urlParam =new ArrayList<String>();
        urlParam.add("0");
        urlParam.add("1");
        List<UserOrderSegment> userOrderSegment = new ArrayList<UserOrderSegment>();
        uos.setFirstOrderCityId(2);
        uos.setLastOrderCityId(1);
        uos.setLastOrderTime(15163609);
        uos.setOrderCount(1);
        userOrderSegment.add(uos);


        List<UserSegment> userSegment = new ArrayList<UserSegment>();
        us.setNewUser(0);
        us.setUserId(3074533);

        userSegment.add(us);
        List<Payload> payLoads = new ArrayList<Payload>();
        xlcard = new XLCARD();
        xlcard.setCount(-1);
        pl.setXLCARD(xlcard);
        payLoads.add(pl);

        return generator.generatevariants2(userOrderSegment, userSegment, payLoads);
    }

    public Object[][] getLaunchCardData(boolean validation) {
        List<String> urlParam =new ArrayList<String>();
        urlParam.add("0");
        urlParam.add("1");
        List<UserOrderSegment> userOrderSegment = new ArrayList<UserOrderSegment>();
        uos.setFirstOrderCityId(2);
        uos.setLastOrderCityId(1);
        uos.setLastOrderTime(15163609);
        uos.setOrderCount(1);
        userOrderSegment.add(uos);


        List<UserSegment> userSegment = new ArrayList<UserSegment>();
        us.setNewUser(0);
        us.setUserId(3074533);

        userSegment.add(us);
        List<Payload> payLoads = new ArrayList<Payload>();
       LaunchCARD launchCard = new LaunchCARD();
        launchCard.setCount(-1);
        pl.setLaunchCARD(launchCard);
        payLoads.add(pl);

        return generator.generatevariants2(userOrderSegment, userSegment, payLoads);
    }




    public Object[][] getSuperCard(boolean validation) {


        List<UserOrderSegment> userOrderSegment = new ArrayList<UserOrderSegment>();
        uos.setFirstOrderCityId(2);
        uos.setLastOrderCityId(1);
        uos.setLastOrderTime(15163609);
        uos.setOrderCount(1);
        userOrderSegment.add(uos);


        List<UserSegment> userSegment = new ArrayList<UserSegment>();
        us.setNewUser(0);
        us.setUserId(3074533);

        userSegment.add(us);


        List<Collection> collection2 = new ArrayList<Collection>();

        List<Payload> payLoads = new ArrayList<Payload>();
        collection2.add(cl2);
        nx2.setCollections(collection2);
        nx2.setCount(1);
        pl2.setNUXLISTING(nx2);
        SUPER_LISTING_CARD sl = new SUPER_LISTING_CARD();
        sl.setCount(1);
        pl2.setSUPER_LISTING_CARD(sl);
        payLoads.add(pl2);



        return generator.generatevariants2(userOrderSegment, userSegment, payLoads);
    }







}
