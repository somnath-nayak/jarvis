
package com.swiggy.api.sf.rng.pojo.pitara;


import org.codehaus.jackson.annotate.JsonProperty;

import java.util.HashMap;

public class PitaraNuxPojo {

    @JsonProperty("userOrderSegment")
    private UserOrderSegment userOrderSegment;
    @JsonProperty("userSegment")
    private UserSegment userSegment;
    @JsonProperty("payload")
    private Payload payload;
    @JsonProperty("cardsMeta")
    private HashMap<String,Object> cardsMeta;


    /**
     * No args constructor for use in serialization
     * 
     */
    public PitaraNuxPojo() {
    }

    /**
     * 
     * @param userSegment
     * @param payload
     * @param userOrderSegment
     */
    public PitaraNuxPojo(UserOrderSegment userOrderSegment, UserSegment userSegment, Payload payload) {
        super();
        this.userOrderSegment = userOrderSegment;
        this.userSegment = userSegment;
        this.payload = payload;
    }

    @JsonProperty("userOrderSegment")
    public UserOrderSegment getUserOrderSegment() {
        return userOrderSegment;
    }

    @JsonProperty("userOrderSegment")
    public void setUserOrderSegment(UserOrderSegment userOrderSegment) {
        this.userOrderSegment = userOrderSegment;
    }

    public PitaraNuxPojo withUserOrderSegment(UserOrderSegment userOrderSegment) {
        this.userOrderSegment = userOrderSegment;
        return this;
    }

    @JsonProperty("userSegment")
    public UserSegment getUserSegment() {
        return userSegment;
    }

    @JsonProperty("userSegment")
    public void setUserSegment(UserSegment userSegment) {
        this.userSegment = userSegment;
    }

    public PitaraNuxPojo withUserSegment(UserSegment userSegment) {
        this.userSegment = userSegment;
        return this;
    }

    @JsonProperty("payload")
    public Payload getPayload() {
        return payload;
    }

    @JsonProperty("payload")
    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public PitaraNuxPojo withPayload(Payload payload) {
        this.payload = payload;
        return this;
    }

    @JsonProperty("cardsMeta")
    public HashMap<String,Object> getCardsMeta() {
        return cardsMeta;
    }

    @JsonProperty("cardsMeta")
    public void setCardsMeta(HashMap<String,Object> cardsMeta) {
        this.cardsMeta = cardsMeta;
    }

    public PitaraNuxPojo withCardsMeta(String cardID, Integer clickCount, Integer seenCount){
        HashMap<String,Integer> cardCounts = new HashMap<>();
        cardCounts.put("clickCount", clickCount);
        cardCounts.put("seenCount",seenCount);
        HashMap<String,Object> cardsCount = new HashMap<>();
        cardsCount.put(cardID,cardCounts);
        HashMap<String,Object> cardIdCardMetaMap = new HashMap<>();
        cardIdCardMetaMap.put("cardIdCardMetaMap",cardsCount);
        setCardsMeta(cardIdCardMetaMap);
        return this;
    }

    public PitaraNuxPojo withCardCount(Integer count){

        return this;
    }


}
