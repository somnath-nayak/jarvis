package com.swiggy.api.sf.snd.pojo.DD;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DDBuilder {
    private DD dd;
    Targetting targetting= new Targetting();
    View view = new View();
    ViewData vd= new ViewData();
    Coc coc= new Coc();

    public DDBuilder(){
        dd = new DD();
    }

    public DDBuilder desc(String des) {
        dd.setDescription(des);
        return this;
    }

    public DDBuilder name(String name ) {
        dd.setName(name);
        return this;
    }

    public DDBuilder slug(String slug) {
        dd.setSlug(slug);
        return this;
    }

    public DDBuilder enabled(Boolean type) {
        dd.setEnabled(type);
        return this;
    }

    public DDBuilder enabledFilters(EnabledFilters ef) {
        dd.setEnabledFilters(ef);
        return this;
    }

    public DDBuilder slots(List<Slot> slot) {
        dd.setSlots(slot);
        return this;
    }

    public DDBuilder startDate(String date) {
        dd.setStartDate(date);
        return this;
    }

    public DDBuilder endDate(String date) {
        dd.setEndDate(date);
        return this;
    }

    public DDBuilder cities(List<Integer> cities) {
        dd.setCities(cities);
        return this;
    }

    public DDBuilder sort(String sort) {
        dd.setSortBy(sort);
        return this;
    }

    public DDBuilder target(List<Targetting> t) {
        dd.setTargetting(t);
        return this;
    }

    public DDBuilder view(List<View> view) {
        dd.setView(view);
        return this;
    }
    public DDBuilder coc(List<Coc> coc) {
        dd.setCoc(coc);
        return this;
    }

    public DDBuilder collectable_type(String collectable) {
        dd.setCollectableType(collectable);
        return this;
    }

    public DD build() throws IOException {
        defaultDD();
        return dd;
    }

    private List<Slot> listSlot(){
        Slot s= new Slot();
        List<Slot> slot= new ArrayList<>();
        if(s.getOpenTime()==null)
            s.setOpenTime(0100);
        if(s.getCloseTime()==null)
            s.setCloseTime(2359);
        if(s.getDay()==null)
            s.setDay("MON");
        slot.add(s);
        return slot;

    }

    private List<Coc> listCoc() {
        if (coc.getEnabled() == null)
            coc.setEnabled(true);
        if (coc.getId() == null)
            coc.setId(1);
        if (coc.getPage() == null)
            coc.setPage("LISTING");
        if (coc.getPosition() == null)
            coc.setPosition(0);

        List<Coc> Coc = new ArrayList<>();
        Coc.add(coc);
        return Coc;
    }

    private List<Targetting> listTargetting() {
        if (targetting.getManual() == null) {
            targetting.setManual(true);
        }
        if (targetting.getRelevanceGroup() == null) {
            targetting.setRelevanceGroup("itemdish");
        }
        if (targetting.getRelevanceScore() == null) {
            targetting.setRelevanceScore(1);
        }
        if (targetting.getRelevanceValue() == null) {
            targetting.setRelevanceValue("samosa");
        }

        List<Targetting> target = new ArrayList<>();
        target.add(targetting);
        return target;

    }



    private void defaultDD() throws IOException{
        EnabledFilters enabledFilters= new EnabledFilters();
        Filters filters = new Filters();
        RestaurantDiscountType restaurantDiscountType= new RestaurantDiscountType();
        RestaurantDiscountOpType restaurantDiscountOpType= new RestaurantDiscountOpType();
        if(restaurantDiscountType.getType()==null)
            restaurantDiscountType.setType("STRING_EQUALS_FILTER");
        if(restaurantDiscountType.getValue()==null)
            restaurantDiscountType.setValue("freebie");

        if(restaurantDiscountType.getId()==null)
            restaurantDiscountType.setId("restaurantDiscountType");
        if(restaurantDiscountType.getQueryId()==null)
            restaurantDiscountType.setQueryId("restaurantDiscountType0");

        List<RestaurantDiscountType> restaurantDiscountTypes= new ArrayList<>();
        restaurantDiscountTypes.add(restaurantDiscountType);

        if(restaurantDiscountOpType.getType()==null)
            restaurantDiscountOpType.setType("STRING_EQUALS_FILTER");
        if(restaurantDiscountOpType.getValue()==null)
            restaurantDiscountOpType.setValue("super");

        if(restaurantDiscountOpType.getId()==null)
            restaurantDiscountOpType.setId("restaurantDiscountOpType");
        if(restaurantDiscountOpType.getQueryId()==null)
            restaurantDiscountOpType.setQueryId("restaurantDiscountOpType0");

        List<RestaurantDiscountOpType> restaurantDiscountOpTypes= new ArrayList<>();
        restaurantDiscountOpTypes.add(restaurantDiscountOpType);

        if(filters.getRestaurantDiscountOpType()==null)
            filters.setRestaurantDiscountOpType(restaurantDiscountOpTypes);
        if(filters.getRestaurantDiscountType()==null)
            filters.setRestaurantDiscountType(restaurantDiscountTypes);


        RestaurantmultiTD restaurantmultiTD= new RestaurantmultiTD();

        if(restaurantmultiTD.getType()==null)
            restaurantmultiTD.setType("MATCH_EXPRESSION_IN_ANY_MAP_FIELD_FILTER");
        if(restaurantmultiTD.getQuery()==null)
            restaurantmultiTD.setQuery("restaurantDiscountType0 AND restaurantDiscountOpType0");
        if(restaurantmultiTD.getId()==null)
            restaurantmultiTD.setId("restaurantmultiTD");
        if(restaurantmultiTD.getType()==null)
            restaurantmultiTD.setType("restaurantmultiTD0");
        if(restaurantmultiTD.getFilters()==null)
            restaurantmultiTD.setFilters(filters);

        List<RestaurantmultiTD> restaurantmultiTDS= new ArrayList<>();
        restaurantmultiTDS.add(restaurantmultiTD);

        if(enabledFilters.getRestaurantmultiTD()== null){
            enabledFilters.setRestaurantmultiTD(restaurantmultiTDS);
        }



        RestaurantId restaurantId= new RestaurantId();
        if(restaurantId.getId()==null)
            restaurantId.setId("restaurantId");
        if(restaurantId.getType()==null)
            restaurantId.setType("NUMBER_IN_RANGE_FILTER");
        if(restaurantId.getMax()==null)
            restaurantId.setMax(223);
        if(restaurantId.getMin()==null)
            restaurantId.setMax(229);
        List<RestaurantId> RestaurantIdList= new ArrayList<>();
        RestaurantIdList.add(restaurantId);

        if(enabledFilters.getRestaurantId()== null){
            enabledFilters.setRestaurantId(RestaurantIdList);
        }

        Tag tag= new Tag();
        if(tag.getId()==null)
            tag.setId("tags");
        if(tag.getType()==null)
            tag.setType("TAG_EQUALS_FILTER");
        if(tag.getTagGroup()==null)
            tag.setTagGroup("Chaats_all");
        if(tag.getTagValue()==null)
            tag.setTagValue("Merch_tag");
        List<Tag> tagList= new ArrayList<>();
        tagList.add(tag);


        if(enabledFilters.getTags()== null){
            enabledFilters.setTags(tagList);
        }
        ItemPrice itemPrice= new ItemPrice();
        if(itemPrice.getId()==null)
            itemPrice.setId("itemPrice");
        if(itemPrice.getType()==null)
            itemPrice.setType("NUMBER_IN_RANGE_FILTER");
        if(itemPrice.getMin()==null)
            itemPrice.setMin(1);
        if(itemPrice.getMax()==null)
            itemPrice.setMax(10);
        List<ItemPrice> itemPriceList= new ArrayList<>();
        itemPriceList.add(itemPrice);

        if(enabledFilters.getItemPrice()==null)
            enabledFilters.setItemPrice(itemPriceList);

        List<Integer> city= new ArrayList<>();
        city.add(1);

        if(vd.getMainPageDescription()==null){
            vd.setMainPageDescription("DDDDDD");
        }

        if(vd.getMinRestaurants()==null){
            vd.setMinRestaurants("1");
        }

        if(vd.getMaxRestaurants()==null){
            vd.setMaxRestaurants("3");
        }

        if(vd.getMinItemsPerRestaurant()==null){
            vd.setMinItemsPerRestaurant("1");
        }

        if(vd.getMaxItemsPerRestaurant()==null){
            vd.setMaxItemsPerRestaurant("3");
        }
        if(vd.getSubTitle()==null){
            vd.setSubTitle("RKO");
        }
        if(vd.getTitle()==null){
            vd.setTitle("RKO1");
        }

        List<View> v= new ArrayList<>();
        v.add(view);

        if(view.getId()==null){
            view.setId(2);
        }
        if(view.getPage()==null){
            view.setPage("OFFERS");
        }
        if(view.getViewType()==null){
            view.setViewType("RESTAURANT_OFFERS_BRANDED_HORIZONTAL_LIST");
        }
        if(view.getViewData()==null){
            view.setViewData(vd);
        }
        List<String> sw= new ArrayList<>();
        sw.add("$COLLECTION_MIN_PRICE");

        if(view.getViewVariables()==null){
            view.setViewVariables(sw);
        }

        List<View> views= new ArrayList<>();
        views.add(view);

        if(dd.getDescription()==null){
            dd.setDescription("New Coll");
        }

        if(dd.getName()==null) {
            dd.setName("Collection");
        }
        if(dd.getSlug()==null){
            dd.setSlug("CCCCCCC");
        }
        if(dd.getEnabled()==null){
            dd.setEnabled(true);
        }
        if(dd.getSlug()==null){
            dd.setSlug("CCCCCCC");
        }
        if(dd.getCollectableType()==null){
            dd.setCollectableType("RESTAURANT");
        }

        if(dd.getStartDate()==null) {
            dd.setStartDate("2018-09-01");
        }
        if(dd.getEndDate()==null){
            dd.setEndDate("2018-09-30");
        }

        if(dd.getSortBy()==null){
            dd.setSortBy("rating");
        }
        if(dd.getEnabledFilters()==null){
            dd.setEnabledFilters(enabledFilters);
        }
        if(dd.getSlots()==null){
            dd.setSlots(listSlot());
        }

        if(dd.getCities()==null){
            dd.setCities(city);
        }

        if(dd.getTargetting()==null){
            dd.setTargetting(listTargetting());
        }

        if(dd.getView()==null){
            dd.setView(views);
        }

        if(dd.getCoc()==null){
            dd.setCoc(listCoc());
        }

    }





}
