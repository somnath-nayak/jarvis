package com.swiggy.api.sf.rng.pojo.MultiTD.CreateSuperCampaignV2;
import org.codehaus.jackson.annotate.JsonProperty;

public class RemoveFreebie {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("updatedBy")
    private String updatedBy;

    /**
     * No args constructor for use in serialization
     *
     */
    public RemoveFreebie() {
    }

    /**
     *
     * @param id
     * @param enabled
     * @param updatedBy
     */
    public RemoveFreebie(Integer id, Boolean enabled, String updatedBy) {
        super();
        this.id = id;
        this.enabled = enabled;
        this.updatedBy = updatedBy;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public RemoveFreebie withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public RemoveFreebie withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updatedBy")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public RemoveFreebie withUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }
    
}