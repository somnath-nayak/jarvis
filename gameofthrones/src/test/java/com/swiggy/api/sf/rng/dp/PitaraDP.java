package com.swiggy.api.sf.rng.dp;
import com.swiggy.api.sf.rng.dataservice.PitaraDataService;
import org.testng.annotations.DataProvider;

public class PitaraDP{
    /*
    The data provider class always pulls data from service class which gives data to data provider.
    Try to avoid the data generation inside the data provider method.
     */
    @DataProvider(name = "fetchNuxCard")
    public Object[][] fetchNuxCard() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getNuxCardData(false);

    }

    @DataProvider(name = "couponCard")
    public Object[][] couponCard() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getCouponCard(false);

    }

    @DataProvider(name = "fetchNuxCardValidation")
    public Object[][] fetchNuxCardValidation() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getNuxCardData(true);
    }

    @DataProvider(name = "fetchScardCard")
    public Object[][] fetchScardCard() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getScardCardData(false);

    }

    @DataProvider(name = "fetchScardCard_seen")
    public Object[][] fetchScardCard_seen() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getScardCardData(false);

    }

    @DataProvider(name = "rainCard")
    public Object[][] rainCard() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getRainCardData(false);

    }



    @DataProvider(name = "lCard")
    public Object[][] lCard() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getLCardData(false);

    }


    @DataProvider(name = "xlCard")
    public Object[][] xlCard() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getXLCardData(false);

    }


    @DataProvider(name = "launchCard")
    public Object[][] launchCard() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getXLCardData(false);

    }

    @DataProvider(name = "cafeCard")
    public Object[][] cafeCard() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getLaunchCardData(false);

    }



    @DataProvider(name = "superCard")
    public Object[][] superCard() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getSuperCard(false);

    }


    @DataProvider(name = "superCardTrack")
    public Object[][] superCardTrack() {

        PitaraDataService dataService=new PitaraDataService();
        return dataService.getSuperCard(false);

    }


}
