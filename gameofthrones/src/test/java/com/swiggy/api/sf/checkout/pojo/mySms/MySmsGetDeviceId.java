package com.swiggy.api.sf.checkout.pojo.mySms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;


@JsonPropertyOrder({
        "apiKey",
        "authToken",
        "os",
        "pushRegistrationId"
})
public class MySmsGetDeviceId {

    @JsonProperty("apiKey")
    private String apiKey;
    @JsonProperty("authToken")
    private String authToken;
    @JsonProperty("os")
    private Integer os;
    @JsonProperty("pushRegistrationId")
    private String pushRegistrationId;

    /**
     * No args constructor for use in serialization
     *
     */
    public MySmsGetDeviceId() {
    }

    /**
     *
     * @param os
     * @param authToken
     * @param pushRegistrationId
     * @param apiKey
     */
    public MySmsGetDeviceId(String apiKey, String authToken, Integer os, String pushRegistrationId) {
        super();
        this.apiKey = apiKey;
        this.authToken = authToken;
        this.os = os;
        this.pushRegistrationId = pushRegistrationId;
    }

    @JsonProperty("apiKey")
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty("apiKey")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonProperty("authToken")
    public String getAuthToken() {
        return authToken;
    }

    @JsonProperty("authToken")
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @JsonProperty("os")
    public Integer getOs() {
        return os;
    }

    @JsonProperty("os")
    public void setOs(Integer os) {
        this.os = os;
    }

    @JsonProperty("pushRegistrationId")
    public String getPushRegistrationId() {
        return pushRegistrationId;
    }

    @JsonProperty("pushRegistrationId")
    public void setPushRegistrationId(String pushRegistrationId) {
        this.pushRegistrationId = pushRegistrationId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("apiKey", apiKey).append("authToken", authToken).append("os", os).append("pushRegistrationId", pushRegistrationId).toString();
    }

}