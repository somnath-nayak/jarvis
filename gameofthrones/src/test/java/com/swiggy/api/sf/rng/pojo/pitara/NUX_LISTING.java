
package com.swiggy.api.sf.rng.pojo.pitara;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "count",
    "collections"
})

@JsonRootName("NUX_LISTING")
public class NUX_LISTING {

    @JsonProperty("type")
    private String type;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("collections")
    private List<Collection> collections = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public NUX_LISTING() {
    }

    /**
     * 
     * @param count
     * @param type
     * @param collections
     */
    public NUX_LISTING(String type, Integer count, List<Collection> collections) {
        super();
        this.type = type;
        this.count = count;
        this.collections = collections;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public NUX_LISTING withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    public NUX_LISTING withCount(Integer count) {
        this.count = count;
        return this;
    }

    @JsonProperty("collections")
    public List<Collection> getCollections() {
        return collections;
    }

    @JsonProperty("collections")
    public void setCollections(List<Collection> collections) {
        this.collections = collections;
    }

    public NUX_LISTING withCollections(List<Collection> collections) {
        this.collections = collections;
        return this;
    }

}
