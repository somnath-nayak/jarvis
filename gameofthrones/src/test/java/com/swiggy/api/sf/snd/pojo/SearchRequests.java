package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class SearchRequests {

    private List<SearchRequest> searchRequests = null;
    private RequestContext requestContext;

    /**
     * No args constructor for use in serialization
     *
     */
    public SearchRequests() {
    }

    /**
     *
     * @param requestContext
     * @param searchRequests
     */
    public SearchRequests(List<SearchRequest> searchRequests, RequestContext requestContext) {
        super();
        this.searchRequests = searchRequests;
        this.requestContext = requestContext;
    }

    public List<SearchRequest> getSearchRequests() {
        return searchRequests;
    }

    public void setSearchRequests(List<SearchRequest> searchRequests) {
        this.searchRequests = searchRequests;
    }

    public RequestContext getRequestContext() {
        return requestContext;
    }

    public void setRequestContext(RequestContext requestContext) {
        this.requestContext = requestContext;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("searchRequests", searchRequests).append("requestContext", requestContext).toString();
    }

}