package com.swiggy.api.sf.snd.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SnDHelper {
	//static Initialize initilizer=new Initialize();
    static Initialize initilizer =Initializer.getInitializer();
	static RedisHelper redisHelper = new RedisHelper();

	//post
	public static Processor consumerSignUp(String name, String mobile,String email,String password) {
		GameOfThronesService service = new GameOfThronesService("sand", "signupv2", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		Processor p = new Processor(service, headers,new String[] { name,mobile, email,password });
		return p;
	}

	//get
	public static Processor sendOTP(String mobile) {
		GameOfThronesService service = new GameOfThronesService("sand", "otpsendv2", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>(){{put("Content-Type","application/json");}};
		return new Processor(service, headers,null,new String[] { mobile});
	}
	//post
	public static Processor verifyOTP(String otp,String tid) {
		GameOfThronesService service = new GameOfThronesService("sand", "otpverifyv1", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>(){{put("Content-Type", "application/json");put("tid", tid);}};
		return new Processor(service, headers,null,new String[]{otp});
	}

	//PUT
	public static Processor callAuth(String mobile,String tid,String token) {
		GameOfThronesService service = new GameOfThronesService("sand", "callauthv1", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token", token);
		Processor p = new Processor(service, headers,null,new String[] { mobile});
		return p;
	}

	//GET
	public static Processor callVerify(String tid) {
		GameOfThronesService service = new GameOfThronesService("sand", "callverifyv1", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		Processor p = new Processor(service, headers);
		return p;
	}

	public static Processor userProfile(String tid,String token) {
		GameOfThronesService service = new GameOfThronesService("sand", "userprofilev1", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token", token);
		Processor p = new Processor(service, headers);
		return p;
	}

	//POST
	public static Processor setPassword(String newpassword,String tid,String token) {
		GameOfThronesService service = new GameOfThronesService("sand", "passwordsetv2", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token", token);
		Processor p = new Processor(service, headers,new String[] { newpassword});
		return p;
	}

	//post
	public static Processor consumerLogin(String mobile, String password) {
		GameOfThronesService service = new GameOfThronesService("sand", "consumerloginv2", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("deviceId", "");
		Processor p = new Processor(service, headers, new String[] { mobile, password });
		return p;
	}



	//post
	public static Processor consumerLoginCheck(String mobile) {
		GameOfThronesService service = new GameOfThronesService("sand", "logincheckv2", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		Processor p = new Processor(service, headers,null, new String[] { mobile});
		return p;
	}

	//post
	public static Processor logout(String tid, String token) {
		GameOfThronesService service = new GameOfThronesService("sand", "consumerloginv2", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		Processor p = new Processor(service, headers);
		return p;
	}

	public static Processor menuV4RestId(String restID, String lat, String lng) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("sand", "menuV4", initilizer);
		Processor processor = new Processor(service, requestHeader, null, new String[]{restID, lat, lng});
		return processor;
	}

	public static Processor menuV3(String restID, String lat, String lng, String tid, String token) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		if (null != tid) {
			requestHeader.put("tid", tid);
		}
		if (null != token) {
			requestHeader.put("token", token);
		}

		GameOfThronesService service = new GameOfThronesService("sand", "menuv3", initilizer);

		Processor processor = new Processor(service, requestHeader, null, lat!=null&&lng!=null?new String[] { restID, lat, lng }:new String[] { restID});
		return processor;
	}

	public static Processor menuV3RestId(String restID) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("sand", "menuv3RestId", initilizer);
		Processor processor = new Processor(service, requestHeader, null,new String[] { restID});
		return processor;
	}


	public Processor getAggregatorDetails(String lat, String lng) {
		GameOfThronesService service = new GameOfThronesService("sand", "aggregator", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		Processor processer = new Processor(service, headers, new String[] { lat, lng });
		return processer;
	}

	public Processor createHolidaySlots(Integer restId, String fromTime, String toTime) {
		GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "addholidayslots", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		//headers.put("Authorization", SANDConstants.bAuth);
		Processor processer = new Processor(service, headers,
				new String[] { String.valueOf(restId), fromTime, toTime });

		return processer;
	}

	public Processor getPopAggregatorDetails(String lat,String lng)
	{
		GameOfThronesService service=new GameOfThronesService("sand", "popaggregator", initilizer);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		Processor processer=new Processor(service,headers,new String[]{lat,lng});
		return processer;
	}


	public static String getOTP(String tid)
	{
		return JsonPath.read(redisHelper.getValue("sandredisstage",0,tid).toString(), "$.user.otp");
	}

	public static List<Map<String, Object>> getUserdetails(String mobile) {
		List<Map<String, Object>> userObject = SystemConfigProvider.getTemplate(CmsConstants.cmsDB).queryForList(CmsConstants.getUserId + mobile);
		return userObject;
	}



	public static long getUserId(String mobile) {
		List<Map<String, Object>> userDetails = getUserdetails(mobile);
		System.out.println(userDetails);
		if(userDetails.isEmpty()) {
			return -999;
		}
		Map<String, Object> userData = userDetails.get(0);
		return Long.parseLong(userData.get("ID").toString());
	}

	public static boolean deleteUserEntriesFromCatalogDB(long userId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		sqlTemplate.execute(CmsConstants.deleteUserWPUsersHistory + userId);
		sqlTemplate.execute(CmsConstants.deleteAddresses + userId);
		sqlTemplate.execute(CmsConstants.deleteUserWPUsers + userId);
		return true;
	}

	public static boolean deleteAddressesFromCatalogDB(long userId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		sqlTemplate.execute(CmsConstants.deleteAddresses + userId);
		return true;
	}


	public static List<Map<String, Object>>  getItemData(List<String> itemList) {
		String sql=CmsConstants.getItemsInfo+itemList;
		System.out.println(sql.replace("[", "(").replace("]", ")"));
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> userObject = sqlTemplate.queryForList(sql.replace("[", "(").replace("]", ")"));
		System.out.println(userObject);
		return userObject;
	}

	public static JsonNode convertStringtoJSON(String response)
			throws
            Exception {
		JsonNode obj1 = null;
		try {
			JsonFactory factory = new JsonFactory();
			JsonParser jp1 = factory.createJsonParser(response);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
			mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS,
					true);
			obj1 = mapper.readTree(jp1);
		} catch (Exception e) {

		}

		return obj1;
	}
	public static Processor menuV4(String restID, String lat, String lng, String tid, String token,HashMap<String,String> headers) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		if (null != tid) {
			requestHeader.put("tid", tid);
		}
		if (null != token) {
			requestHeader.put("token", token);
		}
		requestHeader.putAll(headers);

		GameOfThronesService service = new GameOfThronesService("sand", "menuv4", initilizer);

		Processor processor = new Processor(service, requestHeader, null, lat!=null&&lng!=null?new String[] { restID, lat, lng }:new String[] { restID});
		return processor;
	}
	public static Processor storyBoard(String tid, String token,String collectionId,String lat, String lng,String type,String parentCollectionId ) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		if (null != tid) {
			requestHeader.put("tid", tid);
		}
		if (null != token) {
			requestHeader.put("token", token);
		}
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "storyBoard", initilizer);
		Processor processor = new Processor(service, requestHeader, null, new String[] {collectionId,lat, lng,type,parentCollectionId });
		return processor;
	}
	public static Processor popListing(String tid, String token,String collectionId,String lat, String lng,String type,String parentCollectionId ) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		if (null != tid) {
			requestHeader.put("tid", tid);
		}
		if (null != token) {
			requestHeader.put("token", token);
		}
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "storyBoard", initilizer);
		Processor processor = new Processor(service, requestHeader, null, new String[] {collectionId,lat, lng,type,parentCollectionId });
		return processor;
	}


	public List<Map<String, Object>> rainMode(String delivery){
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(delivery);
		List<Map<String, Object>> list = sqlTemplate.queryForList("");
		return list;
	}


	public void changeSLA(String delivery, String maxSla, String minSla, String zoneId){
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(delivery);
		int a= sqlTemplate.update(SANDConstants.updateSla+maxSla+SANDConstants.updateSla1+minSla+SANDConstants.whereId+zoneId);
		System.out.println(a);
	}

	public void rainModeType(String delivery, String rainModeType, String zoneId){
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(delivery);
		int a= sqlTemplate.update(SANDConstants.rainModeType+rainModeType+SANDConstants.whereId+zoneId);
		System.out.println(a);
	}

	public void zoneRainParams(String delivery, String lastMile, String zoneId){
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(delivery);
		int a= sqlTemplate.update(SANDConstants.zoneRainParams+SANDConstants.whereId+zoneId);
		System.out.println(a);
	}




}

