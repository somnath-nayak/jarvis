package com.swiggy.api.sf.snd.pojo.event;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class Data {

    @JsonProperty("event_type")
    private String serviceTax;
    private String vat;
    private Integer timestamp;
    private String name;
    @JsonProperty("is_perishable")
    private Integer isPerishable;
    @JsonProperty("addon_free_limit")
    private Integer addonFreeLimit;
    @JsonProperty("eligible_for_long_Distance")
    private Integer eligibleForLongDistance;
    private String category;
    @JsonProperty("is_discoverable")
    private Boolean isDiscoverable;
    @JsonProperty("sub_category_desc")
    private String subCategoryDesc;
    @JsonProperty("third_party_sub_category_name")
    private String thirdPartySubCategoryName;
    private Float price;
    @JsonProperty("restaurant_id")
    private Integer restaurantId;
    @JsonProperty("is_veg")
    private Integer isVeg;
    @JsonProperty("sub_category_id")
    private Integer subCategoryId;
    @JsonProperty("third_party_id")
    private String thirdPartyId;
    @JsonProperty("menu_id")
    private Integer menuId;
    private Integer id;
    @JsonProperty("category_id")
    private Integer categoryId;
    @JsonProperty("sub_category")
    private String subCategory;
    private Integer enabled;
    @JsonProperty("variants_v2")
    private VariantsV2 variantsV2;
    @JsonProperty("sub_category_order")
    private Integer subCategoryOrder;
    @JsonProperty("packing_slab_count")
    private Integer packingSlabCount;
    @JsonProperty("category_desc")
    private String categoryDesc;
    private Integer order;
    @JsonProperty("in_stock")
    private Integer inStock;
    @JsonProperty("packing_charges")
    private Float packingCharges;
    private Boolean recommended;
    private String description;
    @JsonProperty("category_order")
    private Integer categoryOrder;
    @JsonProperty("service_charges")
    private String serviceCharges;
    @JsonProperty("meta")
    private Meta meta;
    @JsonProperty("event_timestamp")
    private Integer eventTimestamp;
    @JsonProperty("gst_details")
    private GstDetails gstDetails;
    private String version;
    @JsonProperty("addon_limit")
    private Integer addonLimit;
    @JsonProperty("crop_choices")
    private Integer cropChoices;
    @JsonProperty("item_suggestion")
    private List<ItemSuggestion> itemSuggestion = null;
    private String type;
    @JsonProperty("serves_how_many")
    private Integer servesHowMany;
    @JsonProperty("is_spicy")
    private Integer isSpicy;

    public String getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsPerishable() {
        return isPerishable;
    }

    public void setIsPerishable(Integer isPerishable) {
        this.isPerishable = isPerishable;
    }

    public Integer getAddonFreeLimit() {
        return addonFreeLimit;
    }

    public void setAddonFreeLimit(Integer addonFreeLimit) {
        this.addonFreeLimit = addonFreeLimit;
    }

    public Integer getEligibleForLongDistance() {
        return eligibleForLongDistance;
    }

    public void setEligibleForLongDistance(Integer eligibleForLongDistance) {
        this.eligibleForLongDistance = eligibleForLongDistance;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean getIsDiscoverable() {
        return isDiscoverable;
    }

    public void setIsDiscoverable(Boolean isDiscoverable) {
        this.isDiscoverable = isDiscoverable;
    }

    public String getSubCategoryDesc() {
        return subCategoryDesc;
    }

    public void setSubCategoryDesc(String subCategoryDesc) {
        this.subCategoryDesc = subCategoryDesc;
    }

    public String getThirdPartySubCategoryName() {
        return thirdPartySubCategoryName;
    }

    public void setThirdPartySubCategoryName(String thirdPartySubCategoryName) {
        this.thirdPartySubCategoryName = thirdPartySubCategoryName;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getThirdPartyId() {
        return thirdPartyId;
    }

    public void setThirdPartyId(String thirdPartyId) {
        this.thirdPartyId = thirdPartyId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public VariantsV2 getVariantsV2() {
        return variantsV2;
    }

    public void setVariantsV2(VariantsV2 variantsV2) {
        this.variantsV2 = variantsV2;
    }

    public Integer getSubCategoryOrder() {
        return subCategoryOrder;
    }

    public void setSubCategoryOrder(Integer subCategoryOrder) {
        this.subCategoryOrder = subCategoryOrder;
    }

    public Integer getPackingSlabCount() {
        return packingSlabCount;
    }

    public void setPackingSlabCount(Integer packingSlabCount) {
        this.packingSlabCount = packingSlabCount;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public Float getPackingCharges() {
        return packingCharges;
    }

    public void setPackingCharges(Float packingCharges) {
        this.packingCharges = packingCharges;
    }

    public Boolean getRecommended() {
        return recommended;
    }

    public void setRecommended(Boolean recommended) {
        this.recommended = recommended;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCategoryOrder() {
        return categoryOrder;
    }

    public void setCategoryOrder(Integer categoryOrder) {
        this.categoryOrder = categoryOrder;
    }

    public String getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Integer getEventTimestamp() {
        return eventTimestamp;
    }

    public void setEventTimestamp(Integer eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    public GstDetails getGstDetails() {
        return gstDetails;
    }

    public void setGstDetails(GstDetails gstDetails) {
        this.gstDetails = gstDetails;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAddonLimit() {
        return addonLimit;
    }

    public void setAddonLimit(Integer addonLimit) {
        this.addonLimit = addonLimit;
    }

    public Integer getCropChoices() {
        return cropChoices;
    }

    public void setCropChoices(Integer cropChoices) {
        this.cropChoices = cropChoices;
    }

    public List<ItemSuggestion> getItemSuggestion() {
        return itemSuggestion;
    }

    public void setItemSuggestion(List<ItemSuggestion> itemSuggestion) {
        this.itemSuggestion = itemSuggestion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getServesHowMany() {
        return servesHowMany;
    }

    public void setServesHowMany(Integer servesHowMany) {
        this.servesHowMany = servesHowMany;
    }

    public Integer getIsSpicy() {
        return isSpicy;
    }

    public void setIsSpicy(Integer isSpicy) {
        this.isSpicy = isSpicy;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("service_tax", serviceTax).append("vat", vat).append("timestamp", timestamp).append("name", name).append("is_perishable", isPerishable).append("addon_free_limit", addonFreeLimit).append("eligible_for_long_Distance", eligibleForLongDistance).append("category", category).append("is_discoverable", isDiscoverable).append("sub_category_desc", subCategoryDesc).append("third_party_sub_category_name", thirdPartySubCategoryName).append("price", price).append("restaurant_id", restaurantId).append("is_veg", isVeg).append("sub_category_id", subCategoryId).append("third_party_id", thirdPartyId).append("menu_id", menuId).append("id", id).append("category_id", categoryId).append("sub_category", subCategory).append("enabled", enabled).append("variants_v2", variantsV2).append("sub_category_order", subCategoryOrder).append("packing_slab_count", packingSlabCount).append("category_desc", categoryDesc).append("order", order).append("in_stock", inStock).append("packing_charges", packingCharges).append("recommended", recommended).append("description", description).append("category_order", categoryOrder).append("service_charges", serviceCharges).append("meta", meta).append("event_timestamp", eventTimestamp).append("gst_details", gstDetails).append("version", version).append("addon_limit", addonLimit).append("crop_choices", cropChoices).append("item_suggestion", itemSuggestion).append("type", type).append("serves_how_many", servesHowMany).append("is_spicy", isSpicy).toString();
    }

}