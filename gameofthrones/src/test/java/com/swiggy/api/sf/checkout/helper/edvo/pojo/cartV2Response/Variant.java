package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cmsAttributes.Attributes;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "attributes",
        "variation_id",
        "group_id",
        "name",
        "price",
        "external_choice_id",
        "external_group_id",
        "variant_tax_charges"
})
public class Variant {

    @JsonProperty("attributes")
    private Attributes attributes;
    @JsonProperty("variation_id")
    private Integer variationId;
    @JsonProperty("group_id")
    private Integer groupId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("external_choice_id")
    private String externalChoiceId;
    @JsonProperty("external_group_id")
    private String externalGroupId;
    @JsonProperty("variant_tax_charges")
    private VariantTaxCharges variantTaxCharges;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Variant() {
    }

    /**
     *
     * @param groupId
     * @param variationId
     * @param price
     * @param variantTaxCharges
     * @param name
     * @param externalGroupId
     * @param externalChoiceId
     */
    public Variant(Attributes attributes, Integer variationId, Integer groupId, String name, Integer price, String externalChoiceId, String externalGroupId, VariantTaxCharges variantTaxCharges) {
        super();
        this.attributes = attributes;
        this.variationId = variationId;
        this.groupId = groupId;
        this.name = name;
        this.price = price;
        this.externalChoiceId = externalChoiceId;
        this.externalGroupId = externalGroupId;
        this.variantTaxCharges = variantTaxCharges;
    }


    @JsonProperty("attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("variation_id")
    public Integer getVariationId() {
        return variationId;
    }

    @JsonProperty("variation_id")
    public void setVariationId(Integer variationId) {
        this.variationId = variationId;
    }

    @JsonProperty("group_id")
    public Integer getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("external_choice_id")
    public String getExternalChoiceId() {
        return externalChoiceId;
    }

    @JsonProperty("external_choice_id")
    public void setExternalChoiceId(String externalChoiceId) {
        this.externalChoiceId = externalChoiceId;
    }

    @JsonProperty("external_group_id")
    public String getExternalGroupId() {
        return externalGroupId;
    }

    @JsonProperty("external_group_id")
    public void setExternalGroupId(String externalGroupId) {
        this.externalGroupId = externalGroupId;
    }

    @JsonProperty("variant_tax_charges")
    public VariantTaxCharges getVariantTaxCharges() {
        return variantTaxCharges;
    }

    @JsonProperty("variant_tax_charges")
    public void setVariantTaxCharges(VariantTaxCharges variantTaxCharges) {
        this.variantTaxCharges = variantTaxCharges;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("variationId", variationId).append("groupId", groupId).append("name", name).append("price", price).append("externalChoiceId", externalChoiceId).append("externalGroupId", externalGroupId).append("variantTaxCharges", variantTaxCharges).append("additionalProperties", additionalProperties).toString();
    }

}