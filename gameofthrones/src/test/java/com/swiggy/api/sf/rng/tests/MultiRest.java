package com.swiggy.api.sf.rng.tests;

import com.swiggy.api.sf.rng.dp.SuperMultiTDDP;
import com.swiggy.api.sf.rng.helper.SuperMultiTDHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.tests
 **/
public class MultiRest extends SuperMultiTDDP {

    SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();
    private static Logger log = LoggerFactory.getLogger(MultiRest.class);
    int count = 1;

    @BeforeClass(alwaysRun = true)
    public void getBenefitPlanUserMapped() throws IOException, InterruptedException {
        superMultiTDHelper.createRandomUser();
        // superMultiTDHelper.deactiveAllFreeDelBenifits();
        // superMultiTDHelper.createPlanBenifitWithMapingSuper();
        // superMultiTDHelper.deactiveAllMaping();
        //superMultiTDHelper.createPlanBenifitWithMapingSuper();

    }

    @Test(dataProvider = "multiRestFreedel", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with Freedel")
    public void multiRestCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        log.info("TESTCASE ---> " + description);
        log.info("TESTCASE NUMBER: " + count++);
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 2);
        softAssert.assertTrue(flag);
        softAssert.assertAll();
    }

    @Test(dataProvider = "multiRestFreebie", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with Freebie")
    public void multiRestFreebie(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        log.info("TESTCASE ---> " + description);
        log.info("TESTCASE NUMBER: " + count++);
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 2);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }

    @Test(dataProvider = "multiRestPercentage", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with Percentage")
    public void multiRestPercentageCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        log.info("TESTCASE ---> " + description);
        log.info("TESTCASE NUMBER: " + count++);
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 2);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }

    @Test(dataProvider = "multiRestFlat", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with Flat")
    public void multiRestFlatCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        log.info("TESTCASE ---> " + description);
        log.info("TESTCASE NUMBER: " + count++);
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 2);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }

    @Test(dataProvider = "multiRestEdvo", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with EDVO")
    public void multiRestEdvoCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        log.info("TESTCASE ---> " + description);
        log.info("TESTCASE NUMBER: " + count++);
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 2);
        softAssert.assertTrue(flag);
        softAssert.assertAll();
    }


    @Test(dataProvider = "multiRestFreedelVisited", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with Freedel - R1 visited, R2 not visited")
    public void multiRestFreedelVisitedCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 1);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }

    @Test(dataProvider = "multiRestFreebieVisited", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with Freebie - R1 visited, R2 not visited")
    public void multiRestFreebieVisitedCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 1);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }

    @Test(dataProvider = "multiRestPercentageVisited", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with Percentage - R1 visited, R2 not visited")
    public void multiRestPercentageVisitedCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 1);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }

    @Test(dataProvider = "multiRestFlatVisited", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with Flat - R1 visited, R2 not visited")
    public void multiRestFlatVisitedCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 1);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }

    @Test(dataProvider = "multiRestCampaign", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant- With three campaign type")
    public void multiRestCampaignCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 2);
        softAssert.assertTrue(flag);
        softAssert.assertAll();
    }

    @Test(dataProvider = "multiRestCampaign2", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with two campaign type")
    public void multiRestCampaign2Case(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 2);
        softAssert.assertTrue(flag);
        softAssert.assertAll();
    }


    @Test(dataProvider = "multiRestFlatmultiUserCut", priority = 2, groups = {"multiRestCase"}, description = "Multi restaurant with SFO, RFO user cut")
    public void multiRestFlatmultiUserCutCase(ArrayList<List<String>> cases, ArrayList<Boolean> actionAssert, HashMap<String, HashMap<String, List<String>>> eval, String caseType, String description) throws Exception {
        SoftAssert softAssert = new SoftAssert();
        boolean flag = false;
        flag = superMultiTDHelper.caseHandlerMultiRest(cases, actionAssert, eval, caseType, 2);
        softAssert.assertTrue(flag);
        softAssert.assertAll();

    }
}