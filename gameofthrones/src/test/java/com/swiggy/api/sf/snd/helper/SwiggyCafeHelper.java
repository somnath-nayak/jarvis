package com.swiggy.api.sf.snd.helper;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SwiggyCafeHelper {
    Initialize gameofthrones = new Initialize();
    CheckoutHelper checkoutHelper= new CheckoutHelper();

    /***
     * Authentication
     * @param userId
     * @return
     */

    public Processor authenticateCorporate(String mobile, String password,String corporateId, String passcode)
    {
        HashMap<String, String> login = checkoutHelper.TokenData(mobile, password);
        String tid = login.get("Tid");
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("tid", tid);
        GameOfThronesService service = new GameOfThronesService("sand", "authenticateCorporate", gameofthrones);
        String[] payloadparams = {corporateId,passcode};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }


    public Processor getCorporateByLatLong(String mobile, String password, String lat, String lng)
    {
        HashMap<String, String> login = checkoutHelper.TokenData(mobile, password);
        String tid = login.get("Tid");
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("tid", tid);
        GameOfThronesService service = new GameOfThronesService("sand", "getCorporateByLatLong", gameofthrones);
        String[] urlparams = {lat,lng};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }


    public Processor getCafeListing( String corporateId, String passcode, String password, String mobile)
    {
        HashMap<String, String> login = checkoutHelper.TokenData(mobile, password);
        String tid = login.get("Tid");
        String token = login.get("Token");
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("tid", tid);
        requestheaders.put("token", tid);
        GameOfThronesService service = new GameOfThronesService("sand", "getCafeListing", gameofthrones);
        String[] urlparams = {corporateId,passcode};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }
    /*
    cafe helpers
     */

    public Processor getAccessibleCorporates(String userId)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "accessiblecorporates", gameofthrones);
        String[] urlparams = {userId};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }

    public Processor accessibleLatestCorporates(String userId)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "accessiblelatestcorporate", gameofthrones);
        String[] urlparams = {userId};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }

    public Processor authenticate(String corporateId, String passcode, String userId)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "authenticate", gameofthrones);
        String[] payloadparams = {corporateId,passcode, userId};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor accessToCafe(String cafeId, String userId, String passcode)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "accesstocafe", gameofthrones);
        String[] urlparams = {cafeId, userId, passcode};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }

    public Processor accessToCorporate(String corporateId, String userId, String passcode)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "accesstocorporate", gameofthrones);
        String[] urlparams = {corporateId, userId, passcode};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }

    public Processor accessToRestaurant(String corporateId, String userId, String passcode)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "accesstorestaurant", gameofthrones);
        String[] urlparams = {corporateId, userId, passcode};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }

    /***
     * feature controller
     */

    public Processor isCafeEnabled(String latLng)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "cafeenabled", gameofthrones);
        String[] urlparams = {latLng};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }

/***
 * Listing Controller
 */

    public Processor getCafes(String corporateId, String userId, String passcode)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "getcafe", gameofthrones);
        String[] urlparams = {corporateId, userId, passcode};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }

    public Processor getCorporateById(String corporateId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "getcorporatebyid", gameofthrones);
        String[] urlparams = {corporateId};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }

    public Processor getCorporatesList(String latLng) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "getcorporateslist", gameofthrones);
        String[] urlparams = {latLng};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }

    public Processor getRestaurantList(String cafeId, String userId, String passcode) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "getrestaurantlist", gameofthrones);
        String[] urlparams = {cafeId, userId, passcode};
        Processor processor = new Processor(service, requestheaders, null, urlparams);
        return processor;
    }
    public Processor clearCacheCafe(){
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Authorization", SANDConstants.cafeClearCache);
        GameOfThronesService service = new GameOfThronesService("swiggycafesand", "clearCache", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null,null);
        return processor;
    }

    /***
     * DB Methods
     */

    public List<Map<String, Object>> cafeZone() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(SANDConstants.cafeZone);
        return list;
    }

    public List<Map<String, Object>> corporateUserMapping() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(SANDConstants.corporateUserMapping);
        return list;
    }

    public List<Map<String, Object>> orders() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(SANDConstants.orders);
        return list;
    }

    public List<Map<String, Object>> cafeRestaurantMapping() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(SANDConstants.cafeRestaurantMapping);
        return list;
    }


    public List<Map<String, Object>> cafes() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(SANDConstants.cafes);
        return list;
    }

    public List<Map<String, Object>> corporateCafeMapping() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(SANDConstants.corporateCafeMapping);
        return list;
    }

    public void enableCafe(String cafeId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(SANDConstants.enableCafe+cafeId);
    }

    public List<Map<String, Object>> dbs() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from swiggy.area");
        return list;
    }

    public void enableCorporate(String corporateId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(SANDConstants.enableCorporate+corporateId);

    }

    public void disableCafe(String cafeId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(SANDConstants.disableCafe+cafeId);
    }

    public void disableCorporate(String corporateId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(SANDConstants.disableCorporate+corporateId);
    }

}
