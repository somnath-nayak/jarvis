package com.swiggy.api.sf.rng.tests.TradeDiscount;

import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.dp.TradeDiscount.TemplateCreationDp;
import com.swiggy.api.sf.rng.helper.TDHelper.TDTemplateHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.mortbay.util.ajax.JSON;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.*;



// TEST Class contains CREATE, DISABLE , GET TEMPLATE & GET TEMPLATE LIST -> BY ENABLED & TEMPLATE_NAME

public class TemplateTest extends TemplateCreationDp {
    TDTemplateHelper tdTemplateHelper = new TDTemplateHelper();
    SoftAssert softAssert = new SoftAssert();

    String statuscode_jsonpath = "$.statusCode";
    String template_creation_id = "$.data.id";
    String doramantuser_type ="$.data.dormantUserType";
    String template_error_status ="$.status";
    String template_error= "$.error";
    String response_data ="$.data";
    String statusMessage="$.statusMessage";
    String campaign_type = "$.data.campaignType";
    String total_template ="$.data.totalElements";
    String data_data="$.data.data";
    String templatename ="templateName";




    /********CREATE TEMPLATE TESTCASES**************/

    @Test(dataProvider ="createValidTemplateDp" ,description = "Create Template with valid data  ")
    public void createTemplate(HashMap<String,String > requestmap) throws IOException {
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);
        //DB Validation
        Map<String ,Object> dbresults = tdTemplateHelper.getTemplateFromDB(template_id);
        softAssert.assertFalse(dbresults.isEmpty(), "Template is not found in DB");
        softAssert.assertAll();
    }

    @Test(dataProvider ="createValidTemplateDp" ,description = "Create template with the same payload repetitively - > Both the template should be able to create")
    public void createTemplateRepitivelyWithSameData(HashMap<String,String > requestmap) throws IOException {
        //New Template
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode,"1");

        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertNotNull(template_id);

        //Same template create again
        Processor response1 = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode1 = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode1,"1");
        String template_id1 =  JSON.toString(response1.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertNotEquals(template_id,template_id1, "Both request as the same TEMPLATE ID");

        softAssert.assertAll();
    }

    @Test(dataProvider ="createTemplateForDifferentCampaignTypeDp" ,description = "Create Template for FLAT/Percentage/Free-Del/Freebie")
    public void createTemplateForDifferentCampaignType(HashMap<String,String > requestmap) throws IOException {
        //New Template
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode,"1");

        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertNotNull(template_id);

        softAssert.assertAll();
    }

    @Test(dataProvider ="createTemplateInDifferentCaseDp" ,description = "Create Template for FLAT/Percentage/Free-Del/Freebie -> with invalid case in Campaign types")
    public void createTemplateInDifferentCaseAtCampaignTypes(HashMap<String,String > requestmap) throws IOException {
        //New Template
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_error_status));
        softAssert.assertEquals(response_statuscode,"400");

        String response_template_error =  JSON.toString(response.ResponseValidator.GetNodeValue(template_error));
        softAssert.assertTrue(response_template_error.contains("Bad Request"));

        softAssert.assertAll();
    }

    @Test(dataProvider ="createTemplateForDifferentCampaignTypeDp" ,description = "Create Template for FLAT/Percentage/Free-Del/Freebie -> VALID_FROM with valid timestamp")
    public void createTemplateForDifferentCampaignTypeWithValidTimeStamp(HashMap<String,String > requestmap) throws IOException {
        //New Template
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode,"1");

        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertNotNull(template_id);

        softAssert.assertAll();
    }

    @Test(dataProvider ="createTemplateForDifferentCampaignTypeWithValidTimeStampWithCurrentStampDp" ,description = "Create Template for FLAT/Percentage/Free-Del/Freebie -> VALID_FROM with valid CURRENT TIMESTAMP")
    public void createTemplateForDifferentCampaignTypeWithValidTimeStampWithCurrentStamp(HashMap<String,String > requestmap) throws IOException {
        //New Template
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode,"1");

        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertNotNull(template_id);

        softAssert.assertAll();
    }


    @Test(dataProvider ="createTemplateForDifferentCampaignTypWithFirstOrderEnabledDp" ,description = "Create Template for FLAT/Percentage/Free-Del/Freebie -> FIRST_ORDER with enabled TRUE/FALSE & DORMANT USER  ")
    public void createTemplateForDifferentCampaignTypWithFirstOrderEnabled(HashMap<String,String > requestmap) throws IOException {
        //New Template
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode,"1");

        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertNotNull(template_id);

        softAssert.assertAll();
    }

    @Test(dataProvider ="createTemplateForDifferentCampaignTypWithoutDormantUserDp" ,description = "Create Template for FLAT/Percentage/Free-Del/Freebie -> FIRST_ORDER with enabled TRUE/FALSE &  DORMANT USER as NULL  ")
    public void createTemplateForDifferentCampaignTypWithDormnantUserAsNull(HashMap<String,String > requestmap) throws IOException {
        //New Template
        Processor response = tdTemplateHelper.createTemplateWithDormantUserNull(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode,"1");

        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertNotNull(template_id);

        String dormant_user_type =   JSON.toString(response.ResponseValidator.GetNodeValue(doramantuser_type));
        softAssert.assertTrue(dormant_user_type.contains(RngConstants.ZERO_DAYS_DORMANT));

        softAssert.assertAll();
    }


    /********DISABLE TEMPLATE TESTCASES**************/

    @Test(dataProvider = "disableValidTemplateIdDp",description = "Disable Valid Template ID - Single ID  & Multiple ID's")
    public void disableValidTemplateId(HashMap<String,String > requestmap,List<Integer> templateIds) throws IOException {
        //Create
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);

        //Disable
        Processor response1 = tdTemplateHelper.disableTemplate(templateIds);
        String response_statuscode1 = JSON.toString(response1.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode1,"1", "INVALID Response Code");

        Boolean data =Boolean.valueOf(response1.ResponseValidator.GetNodeValueAsBool(response_data));
        softAssert.assertTrue(data, "RESPONSE DATA is not TRUE");

        String status_mesasge = (response1.ResponseValidator.GetNodeValue(statusMessage));
        softAssert.assertNull(status_mesasge, "STATUS MESSAGE is not NULL");

        softAssert.assertAll();
    }


    @Test(dataProvider = "disableValidTemplateIdAndVerifyDbDp",description = "Disable Valid Template ID - Single ID, Multiple ID's and Verify Database")
    public void disableValidTemplateIdAndVerifyDb(HashMap<String,String > requestmap,List<Integer> templateIds) throws IOException {

        //Create
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);

        //Disable Template
        Processor response1 = tdTemplateHelper.disableTemplate(templateIds);
        String response_statuscode1 = JSON.toString(response1.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode1,"1", "INVALID Response Code");

        Boolean data =Boolean.valueOf(response1.ResponseValidator.GetNodeValueAsBool(response_data));
        softAssert.assertTrue(data, "RESPONSE DATA is not TRUE");

        String status_mesasge = (response1.ResponseValidator.GetNodeValue(statusMessage));
        softAssert.assertNull(status_mesasge, "STATUS MESSAGE is not NULL");

        for (Integer id : templateIds){
            softAssert.assertFalse(Boolean.valueOf(tdTemplateHelper.getEnabledForGivenTemplateIdFromDB(String.valueOf(id)).get("enabled").toString())); // Validation after disabled
        }

        softAssert.assertAll();
    }



    /********GET TEMPLATE BY ID TESTCASES**************/

    @Test(dataProvider = "createTemplateForDifferentCampaignTypeDp",description = "Get VALID template by ID")
    public void getValidTemplateId(HashMap<String,String > requestmap) throws IOException {
        //Create TEMPLATE
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);


        //GET TEMPLATE BY ID
        Processor gettemplate_response = tdTemplateHelper.getTemplateById(template_id);
        String status_code =  JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String campaignType = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValue(campaign_type));
        String templateId = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(template_creation_id));

        softAssert.assertEquals(status_code,"1", "INVALID Response Code");
        softAssert.assertTrue(campaignType.contains(requestmap.get(RngConstants.CAMPAIGN_TYPE)), "CAMPAIGN TYPE are not same in the GET Request compared with Create Template CAMPAIGN TYPE");
        softAssert.assertEquals(templateId,template_id, "TEMPLATE ID are not same in the GET Request compared with Create Template response");

        softAssert.assertAll();

    }


    @Test(dataProvider = "createTemplateForDifferentCampaignTypeDp",description = "Get VALID DISABLED template by ID")
    public void getValidDisabledTemplateId(HashMap<String,String > requestmap) throws IOException {
        //Create TEMPLATE
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);


        //Disable
        List<Integer> templateids = new ArrayList<>();
        templateids.add(Integer.valueOf(template_id));
        System.out.println(templateids);
        Processor response1 = tdTemplateHelper.disableTemplate(templateids);
        String response_statuscode1 = JSON.toString(response1.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode1,"1", "INVALID Response Code");

        Boolean data =Boolean.valueOf(response1.ResponseValidator.GetNodeValueAsBool(response_data));
        softAssert.assertTrue(data, "RESPONSE DATA is not TRUE");

        String status_mesasge = (response1.ResponseValidator.GetNodeValue(statusMessage));
        softAssert.assertNull(status_mesasge, "STATUS MESSAGE is not NULL");


        //GET TEMPLATE BY ID
        Processor gettemplate_response = tdTemplateHelper.getTemplateById(template_id);
        String status_code =  JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String campaignType = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValue(campaign_type));
        String templateId = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(template_creation_id));

        softAssert.assertEquals(status_code,"1", "INVALID Response Code");
        softAssert.assertTrue(campaignType.contains(requestmap.get(RngConstants.CAMPAIGN_TYPE)), "CAMPAIGN TYPE are not same in the GET Request compared with Create Template CAMPAIGN TYPE");
        softAssert.assertEquals(templateId,template_id, "TEMPLATE ID are not same in the GET Request compared with Create Template response");

        softAssert.assertAll();

    }

    @Test(dataProvider = "createTemplateForDifferentCampaignTypeDp",description = "Get random template by ID which doesn't exist in DB")
    public void getRandomTemplateId(HashMap<String,String > requestmap) throws IOException {

        Random rn = new Random();
        int range = 9999999 - 777777 + 1;
        int randomNum =  rn.nextInt(range) + 1;


        //GET TEMPLATE BY ID
        Processor gettemplate_response = tdTemplateHelper.getTemplateById(String.valueOf(randomNum));
        String status_code =  JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String data = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValue(response_data));

        softAssert.assertEquals(status_code,"1", "INVALID Response Code");
        softAssert.assertEquals(data,"null", "RESPONSE DATA is not NULL");

        softAssert.assertAll();

    }



    @Test(dataProvider = "getExpiredTemplateIdDp",description = "Get VALID template by ID")
    public void getExpiredTemplateId(HashMap<String,String > requestmap) throws IOException {
        //Create TEMPLATE
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);



        //GET TEMPLATE BY ID
        Processor gettemplate_response = tdTemplateHelper.getTemplateById(template_id);
        String status_code =  JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String campaignType = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValue(campaign_type));
        String templateId = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(template_creation_id));

        softAssert.assertEquals(status_code,"1", "INVALID Response Code");
        softAssert.assertTrue(campaignType.contains(requestmap.get(RngConstants.CAMPAIGN_TYPE)), "CAMPAIGN TYPE are not same in the GET Request compared with Create Template CAMPAIGN TYPE");
        softAssert.assertEquals(templateId,template_id, "TEMPLATE ID are not same in the GET Request compared with Create Template response");

        softAssert.assertAll();

    }



    /********GET TEMPLATE LIST -> BY ENABLED & TEMPLATE_NAME  TESTCASES**************/


    @Test(dataProvider = "getTemplateByTemplateNameDp",description = "Get Template count for the request ENABLED & TEMPLATE NAME ")
    public void getTemplateByTemplateName(HashMap<String,String > requestmap,String enabled,String templateName) throws IOException {
        //Create TEMPLATE
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);

        //Get COUNT IN DB
        String template_count_in_db= tdTemplateHelper.getEnabledTemplate(enabled,templateName).get("count(*)").toString();

        //GET TEMPLATE BY ID
        Processor gettemplate_response = tdTemplateHelper.getTemplateList(enabled,templateName);
        String totalTemplates = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(total_template));
        String statuscode= JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));

        softAssert.assertEquals(statuscode,"1");
        softAssert.assertEquals(totalTemplates,template_count_in_db,"TOTAL TEMPLATE Count is not matching between DB & API RESPONSE");

        int lengthOfResponseArray =gettemplate_response.ResponseValidator.GetNodeValueAsJsonArray(data_data).size();
        for(int i=0; i<=lengthOfResponseArray-1;i++){
            String actual_template_name = gettemplate_response.ResponseValidator.GetNodeValue(data_data+"["+i+"]."+templatename).toLowerCase();

            softAssert.assertTrue(actual_template_name.contains(templateName.toLowerCase()),
                    "Response TEMPLATE NAME doesn't contain requested TEMPLATE NAME"); // actual & expected response is kept in lowercase because api also ignores the case sensitivity
        }

        softAssert.assertAll();
    }

    @Test(dataProvider = "getDisabledTemplateByTemplateNameDp",description = "Get Template count for the request ENABLED & TEMPLATE NAME ")
    public void getDisabledTemplateByTemplateName(HashMap<String,String > requestmap,String enabled,String templateName) throws IOException {
        //Create TEMPLATE
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);

        //Disable
        List<Integer> templateids = new ArrayList<>();
        templateids.add(Integer.valueOf(template_id));
        System.out.println(templateids);
        Processor response1 = tdTemplateHelper.disableTemplate(templateids);
        String response_statuscode1 = JSON.toString(response1.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(response_statuscode1,"1", "INVALID Response Code");

        Boolean data =Boolean.valueOf(response1.ResponseValidator.GetNodeValueAsBool(response_data));
        softAssert.assertTrue(data, "RESPONSE DATA is not TRUE");

        String status_mesasge = (response1.ResponseValidator.GetNodeValue(statusMessage));
        softAssert.assertNull(status_mesasge, "STATUS MESSAGE is not NULL");


        //Get COUNT IN DB
        String template_count_in_db= tdTemplateHelper.getEnabledTemplate(enabled,templateName).get("count(*)").toString();

        //GET TEMPLATE BY ID
        Processor gettemplate_response = tdTemplateHelper.getTemplateList(enabled,templateName);
        String totalTemplates = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(total_template));
        String statuscode= JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));

        softAssert.assertEquals(statuscode,"1");
        softAssert.assertEquals(totalTemplates,template_count_in_db,"TOTAL TEMPLATE Count is not matching between DB & API RESPONSE");

         int lengthOfResponseArray =gettemplate_response.ResponseValidator.GetNodeValueAsJsonArray(data_data).size();
         for(int i=0; i<=lengthOfResponseArray-1;i++){
             String actual_template_name = gettemplate_response.ResponseValidator.GetNodeValue(data_data+"["+i+"]."+templatename).toLowerCase();

             softAssert.assertTrue(actual_template_name.contains(templateName.toLowerCase()),
                     "Response TEMPLATE NAME doesn't contain requested TEMPLATE NAME"); // actual & expected response is kept in lowercase because api also ignores the case sensitivity
         }
        softAssert.assertAll();
    }



    @Test(dataProvider = "getTemplateByTemplateNameWithoutEnabledDp",description = "Get Template count for the request  TEMPLATE NAME - > without enabled ")
    public void getTemplateByTemplateNameWithoutEnabled(HashMap<String,String > requestmap,String templateName) throws IOException {
        //Create TEMPLATE
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);

        //Get COUNT IN DB
        String template_count_in_db= tdTemplateHelper.getAllTemplate().get("count(*)").toString();

        //GET TEMPLATE BY ID
        Processor gettemplate_response = tdTemplateHelper.getTemplateList(templateName);
        String totalTemplates = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(total_template));
        String statuscode= JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));

        softAssert.assertEquals(statuscode,"1");
        softAssert.assertEquals(totalTemplates,template_count_in_db,"TOTAL TEMPLATE Count is not matching between DB & API RESPONSE");

        int lengthOfResponseArray =gettemplate_response.ResponseValidator.GetNodeValueAsJsonArray(data_data).size();
        for(int i=0; i<=lengthOfResponseArray-1;i++){
            String actual_template_name = gettemplate_response.ResponseValidator.GetNodeValue(data_data+"["+i+"]."+templatename).toLowerCase();

            softAssert.assertTrue(actual_template_name.contains(templateName.toLowerCase()),
                    "Response TEMPLATE NAME doesn't contain requested TEMPLATE NAME"); // actual & expected response is kept in lowercase because api also ignores the case sensitivity
        }

        softAssert.assertAll();

    }


    @Test(dataProvider = "getTemplateByEnabledDp",description = "Get Template count for the request ENABLED- > without TEMPLATE NAME ")
    public void getTemplateByEnabled(HashMap<String,String > requestmap,String enabled) throws IOException {
        //Create TEMPLATE
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);

        //Get COUNT IN DB
        String template_count_in_db= tdTemplateHelper.getEnabledTemplate(enabled).get("count(*)").toString();

        //GET TEMPLATE BY ID
        Processor gettemplate_response = tdTemplateHelper.getTemplateListOnlyEnabled(enabled);
        String totalTemplates = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(total_template));
        String statuscode= JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));

        softAssert.assertEquals(statuscode,"1");
        softAssert.assertEquals(totalTemplates,template_count_in_db,"TOTAL TEMPLATE Count is not matching between DB & API RESPONSE");


        softAssert.assertAll();

    }

    @Test(dataProvider = "getTemplateWithoutAnyParamDp",description = "Get Template count for the request without TNAME & Enabled Param in the request ")
    public void getTemplateWithoutAnyParam(HashMap<String,String > requestmap) throws IOException {
        //Create TEMPLATE
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));
        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);

        //Get COUNT IN DB
        String template_count_in_db= tdTemplateHelper.getAllTemplate().get("count(*)").toString();

        //GET TEMPLATE BY ID
        Processor gettemplate_response = tdTemplateHelper.getTemplateListWithoutAnyParam();
        String totalTemplates = JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(total_template));
        String statuscode= JSON.toString(gettemplate_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));

        softAssert.assertEquals(statuscode,"1");
        softAssert.assertEquals(totalTemplates,template_count_in_db,"TOTAL TEMPLATE Count is not matching between DB & API RESPONSE");


        softAssert.assertAll();
    }



    /******ONBOARDING TEMPLATE  TESTCASES******/


    @Test(dataProvider = "onboardTemplateWithValidTemplateAndRestIdsDp",description = "Onboard Template, validate Campaign table  & campaign_restaurant_map table ")
    public void onboardTemplateWithValidTemplateAndRestIds(HashMap<String,String > requestmap,List<Integer> restIds) throws IOException {

        //Disable all ACTIVE existing CAMPAIGNS
        tdTemplateHelper.disableAllCampaigns();

        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);
        //DB Validation
        Map<String ,Object> dbresults = tdTemplateHelper.getTemplateFromDB(template_id);
        softAssert.assertFalse(dbresults.isEmpty(), "Template is not found in DB");



        //Onboard TEMPLATE
        Processor onboard_response = tdTemplateHelper.onboaringTemplate(restIds,Integer.valueOf(template_id));
        String statuscode = JSON.toString(onboard_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(statuscode,"1", "Status code is not 1");

        //Campaign table validation
        String campaign_id = JSON.toString(onboard_response.ResponseValidator.GetNodeValueAsInt(response_data));
        softAssert.assertFalse(tdTemplateHelper.getCampaignById(campaign_id).isEmpty(),"CAMPAIGN ID is not present in the CAMPAIGN table....");

        //campaign_restaurant_map table validation
        for( Integer li : restIds){
           softAssert.assertTrue( tdTemplateHelper.getRestaurantIdByCampaignId(campaign_id).contains(String.valueOf(li)),"DB is not having the mapping for the restaurant id "+li);
        }

        softAssert.assertAll();
    }


    @Test(dataProvider = "onboardTemplateWithOptionalParamsDp",description ="Onboard TEMPLATE, ingestion_source field validation & Re-Onboard the same restaurants while already active campaigns running")
    public void onboardTemplateWithOptionalParams(HashMap<String, String> requestmap, List<Integer> restIds,
                                                  String duration, String startdate, String ingestionSource) throws IOException {

        //Disable all ACTIVE existing CAMPAIGNS
        tdTemplateHelper.disableAllCampaigns();
        //Create Template
        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);
        //DB Validation
        Map<String ,Object> dbresults = tdTemplateHelper.getTemplateFromDB(template_id);
        softAssert.assertFalse(dbresults.isEmpty(), "Template is not found in DB");



        //Onboard TEMPLATE
        Processor onboard_response = tdTemplateHelper.onboaringTemplate(restIds,Integer.valueOf(template_id),duration,startdate,ingestionSource);
        String statuscode = JSON.toString(onboard_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(statuscode,"1", "Status code is not 1");

        //Campaign table validation
        String campaign_id = JSON.toString(onboard_response.ResponseValidator.GetNodeValueAsInt(response_data));
        softAssert.assertFalse(tdTemplateHelper.getCampaignById(campaign_id).isEmpty(),"CAMPAIGN ID is not present in the CAMPAIGN table....");
        //Campaign table validation - ingestion_source field validation
        if(ingestionSource != null){
            softAssert.assertEquals(tdTemplateHelper.getCampaignById(campaign_id).get("ingestion_source"),ingestionSource," ingestion_source is not as expected in the CAMPAIGN table...." );
        }
        else if(ingestionSource == null){
            softAssert.assertEquals(tdTemplateHelper.getCampaignById(campaign_id).get("ingestion_source"),RngConstants.INGESTIONSOURCE_GROWTH_PACK," ingestion_source is not as expected in the CAMPAIGN table...." );
        }

        //campaign_restaurant_map table validation
        for( Integer li : restIds){
            softAssert.assertTrue( tdTemplateHelper.getRestaurantIdByCampaignId(campaign_id).contains(String.valueOf(li)),"DB is not having the mapping for the restaurant id "+li);
        }

        //Re-Onboard the same restaurants while already active campaigns running
        Processor onboard_response1 = tdTemplateHelper.onboaringTemplate(restIds,Integer.valueOf(template_id),duration,startdate,ingestionSource);
        String statuscode1 = JSON.toString(onboard_response1.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(statuscode1,"0", "Status code is not 0");

        String status_message= JSON.toString(onboard_response1.ResponseValidator.GetNodeValue(statusMessage));
        softAssert.assertTrue(status_message.contains("Restaurant already has an campaign "+campaign_id));



        softAssert.assertAll();
    }







    @Test(dataProvider = "onboardTemplateWithValidTemplateAndRestIdsDp")
    public void onboardTemplateWithInvalidTemplateAndValidRestIds(HashMap<String,String > requestmap,List<Integer> restIds) throws IOException {

        Processor response = tdTemplateHelper.createTemplate(requestmap.get(RngConstants.CAMPAIGN_TYPE),requestmap.get(RngConstants.VALID_FROM),
                requestmap.get(RngConstants.VALID_TILL),requestmap.get(RngConstants.MIN_CART_AMOUNT),requestmap.get(RngConstants.FIRST_ORDER),
                requestmap.get(RngConstants.DORMANT_USER_TYPE),requestmap.get(RngConstants.USER_RESTRICTION));

        String response_statuscode = JSON.toString(response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        String template_id =  JSON.toString(response.ResponseValidator.GetNodeValueAsInt(template_creation_id));
        softAssert.assertEquals(response_statuscode,"1", "INVALID Response Code");
        softAssert.assertNotNull(template_id);
        //DB Validation
        Map<String ,Object> dbresults = tdTemplateHelper.getTemplateFromDB(template_id);
        softAssert.assertFalse(dbresults.isEmpty(), "Template is not found in DB");

        //Disable all ACTIVE CAMPAIGNS
        tdTemplateHelper.disableAllCampaigns();

        //Onboard TEMPLATE
        int invalid_template_id=Integer.valueOf(template_id)+1;
        Processor onboard_response = tdTemplateHelper.onboaringTemplate(restIds,invalid_template_id);
        String statuscode = JSON.toString(onboard_response.ResponseValidator.GetNodeValueAsInt(statuscode_jsonpath));
        softAssert.assertEquals(statuscode,"0", "Status code is not 0");

        String status_message = JSON.toString(onboard_response.ResponseValidator.GetNodeValue(statusMessage));
        softAssert.assertEquals(status_message,"\"Invalid template id\"");

        softAssert.assertAll();
    }




    @AfterClass(description = "only in successful execution of testclass it will executed if not created templates can be used for debugging any regression")
    public void removeAllAutomationTempateCreated() {

        tdTemplateHelper.deleteAllTemplate(); //created by user is hardcode as "AUTOMATION in the RNGCONSTANTS"

    }


}
