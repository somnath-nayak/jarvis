package com.swiggy.api.sf.rng.pojo.SwiggySuper;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.SwiggySuper
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "created_by",
        "description",
        "logo_id",
        "name",
        "priority",
        "title",
        "type",
        "updated_by"
})

public class CreateBenefits {

    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("description")
    private String description;
    @JsonProperty("logo_id")
    private String logo_id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("priority")
    private Integer priority;
    @JsonProperty("title")
    private String title;
    @JsonProperty("type")
    private String type;
    @JsonProperty("updated_by")
    private String updated_by;


    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("logo_id")
    public String getLogo_id() {
        return logo_id;
    }

    @JsonProperty("logo_id")
    public void setLogo_id(String logo_id) {
        this.logo_id = logo_id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("priority")
    public Integer getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("updated_by")
    public String getUpdated_by() {
        return updated_by;
    }

    @JsonProperty("updated_by")
    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public void setDefaultValues(String type) {
        if (this.getCreated_by() == null)
            this.setCreated_by("Automation_Super_MultiTD");
        if (this.getDescription() == null)
            this.setDescription("Testing Benefits for automation");
        if (this.getLogo_id() == null)
            this.setLogo_id("Dummy_logo_id");
        if (this.getName() == null)
            this.setName("Super MultiTD Benefits");
        if (this.getPriority() == null)
            this.setPriority(0);
        if (this.getTitle() == null)
            this.setTitle("Testing Benefits");
        if (this.getType() == null)
            this.setType(type);
        if (this.getUpdated_by() == null)
            this.setUpdated_by("AutomationSuite_SuperMultiTD");
    }

    public CreateBenefits build(String type) {
        setDefaultValues(type);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("created_by", created_by).append("description", description).append("logo_id", logo_id).append("name", name).append("priority", priority).append("title", title).append("type", type).append("updated_by", updated_by).toString();
    }

}

