package com.swiggy.api.sf.checkout.dp;

import org.testng.annotations.DataProvider;

public class FriedManDP {

	
    @DataProvider(name = "friedMan")
    public static Object[][] restData() {
    	   return new Object[][]{
   //1.userAgent,2.versionCode,3.cartType,4.isCouponApplied,5.TDType,6.isGSTApplied,7.isPackagingCharge,8.isDelFee,
  //9.isthresholdFee,10.isTimeFee,11.isDistanceFee,12.specialFee,13.swiggyMoney,14.cancellationFee


//case-1 PRE-ORDER
    /*{"Swiggy-Android", "311","preorder",true,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "235","preorder",true,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "311","preorder",true,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "309","preorder",true,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "233","preorder",true,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "309","preorder",true,"flatTD",true,true,true,true,true,true,true,10.0,10.0},*/
    
  //case-1 coupon & TD   
    
    {"Swiggy-Android", "311","regular",false,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "235","regular",false,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "311","regular",false,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "309","regular",false,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "233","regular",false,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "309","regular",false,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "311","regular",false,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "235","regular",false,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "311","regular",false,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "309","regular",false,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "233","regular",false,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "309","regular",false,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "311","regular",true,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "235","regular",true,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "311","regular",true,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "309","regular",true,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "233","regular",true,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "309","regular",true,"noTD",true,true,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "311","regular",true,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "235","regular",true,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "311","regular",true,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "309","regular",true,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "233","regular",true,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    {"Web", "309","regular",true,"flatTD",true,true,true,true,true,true,true,10.0,10.0},
    
//case-2 Packaging changes & GST
    {"Swiggy-Android", "311","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "235","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Web", "311","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "309","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "233","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Web", "309","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    
    {"Swiggy-Android", "311","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "235","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Web", "311","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
   
    {"Swiggy-Android", "309","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "233","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Web", "309","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    
   {"Swiggy-Android", "311","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   {"Swiggy-iOS", "235","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   {"Web", "311","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   
   {"Swiggy-Android", "309","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   {"Swiggy-iOS", "233","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   {"Web", "309","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   
   {"Swiggy-Android", "311","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   {"Swiggy-iOS", "235","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   {"Web", "311","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   
   {"Swiggy-Android", "309","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   {"Swiggy-iOS", "233","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
   {"Web", "309","regular",false,"FreeDelTD",true,true,true,true,true,true,true,10.0,10.0},
//case-3 Surge Fee
    {"Swiggy-Android", "311","regular",false,"noTD",false,false,true,true,false,false,false,10.0,10.0},
    {"Swiggy-iOS", "235","regular",false,"noTD",false,false,true,true,false,false,false,10.0,10.0},
    {"Web", "311","regular",false,"noTD",false,false,true,true,false,false,false,10.0,10.0},
    
    {"Swiggy-Android", "311","regular",false,"noTD",false,false,true,false,true,false,false,10.0,10.0},
    {"Swiggy-iOS", "235","regular",false,"noTD",false,false,true,false,true,false,false,10.0,10.0},
    {"Web", "311","regular",false,"noTD",false,false,true,false,true,false,false,10.0,10.0},
    
    {"Swiggy-Android", "311","regular",false,"noTD",false,false,true,false,false,true,false,10.0,10.0},
    {"Swiggy-iOS", "235","regular",false,"noTD",false,false,true,false,false,true,false,10.0,10.0},
    {"Web", "311","regular",false,"noTD",false,false,true,false,false,true,false,10.0,10.0},
    
    {"Swiggy-Android", "311","regular",false,"noTD",false,false,true,false,false,false,true,10.0,10.0},
    {"Swiggy-iOS", "235","regular",false,"noTD",false,false,true,false,false,false,true,10.0,10.0},
    {"Web", "311","regular",false,"noTD",false,false,true,false,false,false,true,10.0,10.0},
    
    {"Swiggy-Android", "311","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "235","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Web", "311","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    
  //old flow  
    {"Swiggy-Android", "309","regular",false,"noTD",false,false,true,true,false,false,false,10.0,10.0},
    {"Swiggy-iOS", "233","regular",false,"noTD",false,false,true,true,false,false,false,10.0,10.0},
    {"Web", "309","regular",false,"noTD",false,false,true,true,false,false,false,10.0,10.0},

    {"Swiggy-Android", "309","regular",false,"noTD",false,false,true,false,true,false,false,10.0,10.0},
    {"Swiggy-iOS", "233","regular",false,"noTD",false,false,true,false,true,false,false,10.0,10.0},
    {"Web", "309","regular",false,"noTD",false,false,true,false,true,false,false,10.0,10.0},

    {"Swiggy-Android", "309","regular",false,"noTD",false,false,true,false,false,true,false,10.0,10.0},
    {"Swiggy-iOS", "233","regular",false,"noTD",false,false,true,false,false,true,false,10.0,10.0},
    {"Web", "309","regular",false,"noTD",false,false,true,false,false,true,false,10.0,10.0},

    {"Swiggy-Android", "309","regular",false,"noTD",false,false,true,false,false,false,true,10.0,10.0},
    {"Swiggy-iOS", "233","regular",false,"noTD",false,false,true,false,false,false,true,10.0,10.0},
    {"Web", "309","regular",false,"noTD",false,false,true,false,false,false,true,10.0,10.0},

    {"Swiggy-Android", "309","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Swiggy-iOS", "233","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
    {"Web", "309","regular",false,"noTD",false,false,true,true,true,true,true,10.0,10.0},
//case-4 Super Secondary Flow
    
    {"Swiggy-Android", "311","superSec",false,"noTD",true,true,true,false,false,false,false,0.0,0.0},
    {"Swiggy-iOS", "235","superSec",false,"noTD",true,true,true,false,false,false,false,0.0,0.0},
    {"Web", "311","superSec",false,"noTD",true,true,true,false,false,false,false,0.0,0.0},
    
   {"Swiggy-Android", "311","superSec",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   {"Swiggy-iOS", "235","superSec",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   {"Web", "311","superSec",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   
   //old flow
   {"Swiggy-Android", "309","superSec",false,"noTD",true,true,true,false,false,false,false,0.0,0.0},
   {"Swiggy-iOS", "233","superSec",false,"noTD",true,true,true,false,false,false,false,0.0,0.0},
   {"Web", "309","superSec",false,"noTD",true,true,true,false,false,false,false,0.0,0.0},
    
   {"Swiggy-Android", "309","superSec",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   {"Swiggy-iOS", "233","superSec",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   {"Web", "309","superSec",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   
//case-5 Swiggy Money & Cancellation Fee
   {"Swiggy-Android", "311","regular",false,"noTD",true,true,true,false,false,false,false,10.0,0.0},
   {"Swiggy-iOS", "235","regular",false,"noTD",true,true,true,false,false,false,false,10.0,0.0},
   {"Web", "311","regular",false,"noTD",true,true,true,false,false,false,false,10.0,0.0},
   
   {"Swiggy-Android", "311","regular",false,"noTD",true,true,true,false,false,false,false,0.0,10.0},
   {"Swiggy-iOS", "235","regular",false,"noTD",true,true,true,false,false,false,false,0.0,10.0},
   {"Web", "311","regular",false,"noTD",true,true,true,false,false,false,false,0.0,10.0},
   
   {"Swiggy-Android", "311","regular",false,"noTD",true,true,true,false,false,false,false,10.0,10.0},
   {"Swiggy-iOS", "235","regular",false,"noTD",true,true,true,false,false,false,false,10.0,10.0},
   {"Web", "311","regular",false,"noTD",true,true,true,false,false,false,false,10.0,10.0},
   
   {"Swiggy-Android", "311","regular",false,"noTD",true,true,true,false,false,false,false,11.0,10.0},
   {"Swiggy-iOS", "235","regular",false,"noTD",true,true,true,false,false,false,false,11.0,10.0},
   {"Web", "311","regular",false,"noTD",true,true,true,false,false,false,false,11.0,10.0},
   
   {"Swiggy-Android", "311","regular",false,"noTD",true,true,true,false,false,false,false,10.0,11.0},
   {"Swiggy-iOS", "235","regular",false,"noTD",true,true,true,false,false,false,false,10.0,11.0},
   {"Web", "311","regular",false,"noTD",true,true,true,false,false,false,false,10.0,11.0},
   
   {"Swiggy-Android", "311","regular",false,"noTD",true,true,true,true,true,true,true,0.0,10.0},
   {"Swiggy-iOS", "235","regular",false,"noTD",true,true,true,true,true,true,true,0.0,10.0},
   {"Web", "311","regular",false,"noTD",true,true,true,true,true,true,true,0.0,10.0},
   
   //old flow
   
   {"Swiggy-Android", "309","regular",false,"noTD",true,true,true,false,false,false,false,10.0,0.0},
   {"Swiggy-iOS", "233","regular",false,"noTD",true,true,true,false,false,false,false,10.0,0.0},
   {"Web", "309","regular",false,"noTD",true,true,true,false,false,false,false,10.0,0.0},

   {"Swiggy-Android", "309","regular",false,"noTD",true,true,true,false,false,false,false,0.0,10.0},
   {"Swiggy-iOS", "233","regular",false,"noTD",true,true,true,false,false,false,false,0.0,10.0},
   {"Web", "309","regular",false,"noTD",true,true,true,false,false,false,false,0.0,10.0},

   {"Swiggy-Android", "309","regular",false,"noTD",true,true,true,false,false,false,false,10.0,10.0},
   {"Swiggy-iOS", "233","regular",false,"noTD",true,true,true,false,false,false,false,10.0,10.0},
   {"Web", "309","regular",false,"noTD",true,true,true,false,false,false,false,10.0,10.0},

   {"Swiggy-Android", "309","regular",false,"noTD",true,true,true,false,false,false,false,11.0,10.0},
   {"Swiggy-iOS", "233","regular",false,"noTD",true,true,true,false,false,false,false,11.0,10.0},
   {"Web", "309","regular",false,"noTD",true,true,true,false,false,false,false,11.0,10.0},

   {"Swiggy-Android", "309","regular",false,"noTD",true,true,true,false,false,false,false,10.0,11.0},
   {"Swiggy-iOS", "233","regular",false,"noTD",true,true,true,false,false,false,false,10.0,11.0},
   {"Web", "309","regular",false,"noTD",true,true,true,false,false,false,false,10.0,11.0},

   {"Swiggy-Android", "309","regular",false,"noTD",true,true,true,true,true,true,true,0.0,10.0},
   {"Swiggy-iOS", "233","regular",false,"noTD",true,true,true,true,true,true,true,0.0,10.0},
   {"Web", "309","regular",false,"noTD",true,true,true,true,true,true,true,0.0,10.0},
   
//Case-6 Del Fee & Can fee==0
   {"Swiggy-Android", "311","regular",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   {"Swiggy-iOS", "235","regular",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   {"Web", "311","regular",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
  //old flow 
   {"Swiggy-Android", "311","regular",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   {"Swiggy-iOS", "233","regular",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
   {"Web", "311","regular",false,"noTD",true,true,false,false,false,false,false,0.0,0.0},
//case-7  TD -Free DEL & Del>0 & Con Fee>0
  {"Swiggy-Android", "311","regular",false,"FreeDelTD",true,true,true,true,true,true,true,0.0,0.0},
  {"Swiggy-iOS", "235","regular",false,"FreeDelTD",true,true,true,true,true,true,true,0.0,0.0},
  {"Web", "311","regular",false,"FreeDelTD",true,true,true,true,true,true,true,0.0,0.0},
  //old flow
  {"Swiggy-Android", "309","regular",false,"FreeDelTD",true,true,true,true,true,true,true,0.0,0.0},
  {"Swiggy-iOS", "233","regular",false,"FreeDelTD",true,true,true,true,true,true,true,0.0,0.0},
  {"Web", "309","regular",false,"FreeDelTD",true,true,true,true,true,true,true,0.0,0.0}
    		   };
 }}
