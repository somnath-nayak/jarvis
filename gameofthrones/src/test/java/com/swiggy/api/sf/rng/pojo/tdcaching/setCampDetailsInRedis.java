package com.swiggy.api.sf.rng.pojo.tdcaching;

import java.util.Date;

public class setCampDetailsInRedis {

    private Long id;

    private String name;

    private String header;

    private String description;

    private String shortDescription;

    private Date validFrom;

    private Date validTill;

    private String discountType;

    private String dormantUserType;

    private double restaurantHit;

    private double swiggyHit;

    private boolean enabled;

    private String createdBy;

    private String updatedBy;

    private boolean isFirstOrder;

    private double minCartAmount;

    private double discountCap;

    private Boolean userRestriction;

    private Boolean timeSlotRestriction;

    private Boolean commissionOnFullBill;

    private Boolean taxesOnDiscountedBill;

    private boolean restaurantFirstOrder;

    private Date createdAt;

    private Date updatedAt;

    private Long templateId;

    private boolean isSurgeApplicable;

    private boolean isCopyOverridden;

    private String levelPlaceHolder;

    private String freebiePlaceHolder;

    private Double commissionLevy = 0d;

    private String shareType = "Percentage";

    private Double shareValue = 100d;


//    public void setCampDetailsInRedis(int campaignId, String header, String description) {
//        this.id = campaignId;
//        this.header = header;
//        this.description = description;
//
//    }
//
//    public void setCampDetailsInRedis(int id, String header, String description, String validFrom, String validTill,
//                                      String campaignType, String dormantUserType, boolean isFirstOrder, double minCartAmount,
//                                      double discountCap, boolean userRestriction, boolean timeSlotRestriction, boolean restaurantFirstOrder,
//                                      String campaignSlotEntities) {
//        this.id = id;
//        this.header = header;
//        this.description = description;
//        this.validFrom = validFrom;
//        this.validTill = validTill;
//        this.campaignType = campaignType;
//        this.dormantUserType = dormantUserType;
//        this.isFirstOrder = isFirstOrder;
//        this.minCartAmount = minCartAmount;
//        this.discountCap = discountCap;
//        this.userRestriction = userRestriction;
//        this.timeSlotRestriction = timeSlotRestriction;
//        this.restaurantFirstOrder = restaurantFirstOrder;
//        this.campaignSlotEntities = campaignSlotEntities;
//
//    }
//
//    public void setCampDetailsInRedis(int id, String header, String description, String validFrom, String validTill) {
//        this.id = id;
//        this.header = header;
//        this.description = description;
//        this.validFrom = validFrom;
//        this.validTill = validTill;
//
//    }
//
//    public void setCampDetailsInRedis(int id, String header, String description, boolean enabled) {
//        this.id = id;
//        this.header = header;
//        this.description = description;
//        this.enabled = enabled;
//
//    }

//    @Override
//    public String toString() {
//
//        return
//                "{"
//                        + "\"id\" : " + id + ";"
//                        + "\"name\" : " + name + ";"
//                        + "\"description\" : " + description + ";"
//                        + "\"shortDescription\" : " + shortDescription + ";"
//                        + "\"validFrom\" : " + validFrom + ";"
//                        + "\"validTill\" : " + validTill + ";"
//                        + "\"campaignType\"" + campaignType + ";"
//                        + "\"dormantUserType\" : " + dormantUserType + ";"
//                        + "\"restaurantHit\" : " + restaurantHit + ";"
//                        + "\"swiggyHit\" : " + swiggyHit + ";"
//                        + "\"enabled\"" + enabled + ";"
//                        + "\"createdBy\" : " + createdBy + ";"
//                        + "\"updatedBy\" : " + updatedBy + ";"
//                        + "\"isFirstOrder\" : " + isFirstOrder + ";"
//                        + "\"minCartAmount\" : " + minCartAmount + ";"
//                        + "\"discountCap\" : " + discountCap + ";"
//                        + "\"userRestriction\" : " + userRestriction + ";"
//                        + "\timeSlotRestriction\" : " + timeSlotRestriction + ";"
//                        + "\"commissionOnFullBill\" : " + commissionOnFullBill + ";"
//                        + "\"taxesOnDiscountedBill\" : " + taxesOnDiscountedBill + ";"
//                        + "\"campaignSlotEntities\" : " + campaignSlotEntities + ";"
//                        + "\"taxesOnDiscountedBill\" : " + restaurantFirstOrder + ";"
//                        + "\"createdAt\" : " + createdAt + ";"
//                        + "\"updatedAt\" : " + updatedAt + ";"
//                        + "}";
//    }

}