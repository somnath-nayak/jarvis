package com.swiggy.api.sf.snd.pojo.meals.createMeal;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "itemId",
        "addons",
        "variants",
        "maxQuantity"
})
public class Item {

    @JsonProperty("itemId")
    private Integer itemId;
    @JsonProperty("addons")
    private Object addons;
    @JsonProperty("variants")
    private Object variants;
    @JsonProperty("maxQuantity")
    private Integer maxQuantity;

    @JsonProperty("itemId")
    public Integer getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    @JsonProperty("addons")
    public Object getAddons() {
        return addons;
    }

    @JsonProperty("addons")
    public void setAddons(Object addons) {
        this.addons = addons;
    }

    @JsonProperty("variants")
    public Object getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(Object variants) {
        this.variants = variants;
    }

    @JsonProperty("maxQuantity")
    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    @JsonProperty("maxQuantity")
    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

}