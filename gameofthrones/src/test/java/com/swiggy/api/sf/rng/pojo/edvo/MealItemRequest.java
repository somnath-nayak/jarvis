package com.swiggy.api.sf.rng.pojo.edvo;

import com.redis.S;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.MealItem;
import io.gatling.commons.stats.assertion.In;
import org.codehaus.jackson.annotate.JsonProperty;

public class MealItemRequest {

    @JsonProperty("mealId")
    private String mealId;
    @JsonProperty("itemId")
    private String itemId;
    @JsonProperty("restaurantId")
    private String restaurantId;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("groupId")
    private String groupId;

    public MealItemRequest(){

    }

    public MealItemRequest(String mealId, String itemId, String restaurantId, Integer price, Integer count, String groupId){
        super();
        this.mealId = mealId;
        this.groupId = groupId;
        this.itemId = itemId;
        this.restaurantId = restaurantId;
        this.price = price;
        this.count = count;
    }

    public String getMealId() {
        return mealId;
    }

    public void setMealId(String mealId) {
        this.mealId = mealId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

}
