package com.swiggy.api.sf.checkout.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class CheckoutAPIHelper {
    Initialize gameofthrones = new Initialize();

    public Processor updateCartV2(String payload, HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "createcartv2edvo", gameofthrones);
        return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor getMeals(String mealId, String restaurantId){
        String[] queryParam = new String[]{mealId, restaurantId};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "getmeals", gameofthrones);
        return new Processor(gots, requestHeader, null, queryParam);
    }

    public Processor getMenuv4(String restaurantId){
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        String[] queryParam = {restaurantId};
        GameOfThronesService gots = new GameOfThronesService("sand", "menuv4withoutlatlng", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    public Processor getMenuV4WithLatLng(String restaurantId, String lat, String lng){
        String[] queryParam = {restaurantId, lat, lng};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "menuv4", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    public Processor flushCart(HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "deletecart", gameofthrones);
        return new Processor(gots, headers, null, null);
    }

    public Processor createMinimalCart(String payload, HashMap<String, String> requestHeader){
        GameOfThronesService gots = new GameOfThronesService("checkout", "edvominimalcartv2", gameofthrones);
        return new Processor(gots, requestHeader, new String[]{payload}, null);
    }

    public Processor getCart(HashMap<String, String> header){
        GameOfThronesService service = new GameOfThronesService("checkout", "getcart", gameofthrones);
        Processor processor = new Processor(service, header, null, null);
        return processor;
    }

    public Processor checkTotalCart(String payload, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "checktotalv2edvo", gameofthrones);
        return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor checkBankListAll(HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "checkBankListAll", gameofthrones);
        return new Processor(gots, headers);
        //return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor checkGetInventory(HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "checkGetInventory", gameofthrones);
        return new Processor(gots, headers);
        //return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor checkSettingsWalletEmailUpdateURL(HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "checkSettingsWalletEmailUpdateURL", gameofthrones);
        return new Processor(gots, headers);
        //return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor checkGetAllPayment(HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "checkGetAllPayment", gameofthrones);
        return new Processor(gots, headers);
        //return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor checkCardsJuspay(HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "checkCardsJuspay", gameofthrones);
        return new Processor(gots, headers);
        //return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor checkApplyCoupon(String payload, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "checkApplyCoupon", gameofthrones);
        return new Processor(gots, headers, new String[]{payload}, null);
        //return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor checkRemoveCoupon(String payload, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "checkRemoveCoupon", gameofthrones);
        return new Processor(gots, headers, new String[]{payload}, null);
        //return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor placeOrder(String addressId, String paymentMethod, String orderComments, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        return placeOrder(addressId, paymentMethod, orderComments, headers);
    }

    public Processor editOrderCheck(String[] payload, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "editordercheckv2", gameofthrones);
        return new Processor(gots, headers, payload, null);
    }

    public Processor editOrderConfirm(String[] payload, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "editorderconfirmv2", gameofthrones);
        return new Processor(gots, headers, payload, null);
    }

    public Processor cancelOrder(String orderId, String reason, String cancellationFeeApplicability, String cancellationFee, HashMap<String, String>  headers){
        headers.put("Content-Type", "application/json");
        String[] payloadparams = {reason , cancellationFeeApplicability, cancellationFee, reason};
        String[] urlParams = new String[]{orderId};
        GameOfThronesService gots = new GameOfThronesService("checkout", "ordercancel", gameofthrones);
        Processor processor = new Processor(gots, headers, payloadparams, urlParams);
        return  processor;
    }

    public Processor getAllOrders(HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "getallorder", gameofthrones);
        return new Processor(gots, headers, null, null);
    }

    public Processor cloneOrderCheck(String[] payload, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "cloneordercheck", gameofthrones);
        return new Processor(gots, headers, payload, null);

    }

    public Processor cloneOrderConfirm(String[] payload, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "cloneorderconfirm", gameofthrones);
        return new Processor(gots, headers, payload, null);

    }

    public Processor singleOrder(String orderId, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "getsingleorder", gameofthrones);
        return new Processor(gots, headers, null, new String[]{orderId});

    }

    public Processor getLastOrder(HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "getlastorder", gameofthrones);
        return new Processor(gots, headers, null, null);
    }

    public Processor reserveCart(String address, String sla, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "reservecart", gameofthrones);
        return new Processor(gameOfThronesService, headers, null, new String[]{address, sla});
    }

    public Processor unreserveCart(HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "unreservecart", gameofthrones);
        return new Processor(gameOfThronesService, headers, null, null);
    }

    public Processor orderCancelCheck(String payload, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "cancelcheck", gameofthrones);
        return new Processor(gameOfThronesService, headers, new String[]{payload}, null);
    }

    public Processor revertCancellationFee(String payload, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "revertcancellationfee", gameofthrones);
        return new Processor(gameOfThronesService, headers, new String[]{payload}, null);
    }

    public Processor trackOrder(String orderId, HashMap<String, String> headers){
        headers.put("Content-Type", "application/json");
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout1", "track", gameofthrones);
        return new Processor(gameOfThronesService, headers, null, new String[]{orderId});
    }

    public Processor trackOrderMinimal(String orderKey){
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout1", "trackminimalorder", gameofthrones);
        return new Processor(gameOfThronesService, null, null, new String[]{orderKey});
    }
}


