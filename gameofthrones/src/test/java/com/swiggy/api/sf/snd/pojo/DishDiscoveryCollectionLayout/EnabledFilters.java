package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class EnabledFilters {

    private ItemPrice itemPrice;
    private ItemEnabled itemEnabled;
    private OrdersCount ordersCount;
    private RestaurantName restaurantName;
    private RestaurantLocation restaurantLocation;
    private Dish dish;
    private FilterTags filtertags;

    /**
     * No args constructor for use in serialization
     *
     */
    public EnabledFilters() {
    }

    /**
     *
     * @param dish
     * @param filtertags
     * @param itemEnabled
     * @param itemPrice
     * @param restaurantName
     * @param restaurantLocation
     * @param ordersCount
     */
    public EnabledFilters(ItemPrice itemPrice, ItemEnabled itemEnabled, OrdersCount ordersCount, RestaurantName restaurantName, RestaurantLocation restaurantLocation, Dish dish, FilterTags filtertags) {
        super();
        this.itemPrice = itemPrice;
        this.itemEnabled = itemEnabled;
        this.ordersCount = ordersCount;
        this.restaurantName = restaurantName;
        this.restaurantLocation = restaurantLocation;
        this.dish = dish;
        this.filtertags = filtertags;
    }

    public ItemPrice getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(ItemPrice itemPrice) {
        this.itemPrice = itemPrice;
    }

    public ItemEnabled getItemEnabled() {
        return itemEnabled;
    }

    public void setItemEnabled(ItemEnabled itemEnabled) {
        this.itemEnabled = itemEnabled;
    }

    public OrdersCount getOrdersCount() {
        return ordersCount;
    }

    public void setOrdersCount(OrdersCount ordersCount) {
        this.ordersCount = ordersCount;
    }

    public RestaurantName getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(RestaurantName restaurantName) {
        this.restaurantName = restaurantName;
    }

    public RestaurantLocation getRestaurantLocation() {
        return restaurantLocation;
    }

    public void setRestaurantLocation(RestaurantLocation restaurantLocation) {
        this.restaurantLocation = restaurantLocation;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public FilterTags getFiltertags() {
        return filtertags;
    }

    public void setFiltertags(FilterTags filtertags) {
        this.filtertags = filtertags;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("itemPrice", itemPrice).append("itemEnabled", itemEnabled).append("ordersCount", ordersCount).append("restaurantName", restaurantName).append("restaurantLocation", restaurantLocation).append("dish", dish).append("filtertags", filtertags).toString();
    }

}