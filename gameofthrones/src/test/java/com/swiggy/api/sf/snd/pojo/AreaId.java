package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class AreaId {

    private String type;
    private Double value;

    /**
     * No args constructor for use in serialization
     *
     */
    public AreaId() {
    }

    /**
     *
     * @param value
     * @param type
     */
    public AreaId(String type, Double value) {
        super();
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("value", value).toString();
    }

}