package com.swiggy.api.sf.snd.pojo.event;

import org.apache.commons.lang.builder.ToStringBuilder;

public class GstDetails {

    private String sgst;
    private String cgst;
    private String igst;
    private Boolean inclusive;

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public Boolean getInclusive() {
        return inclusive;
    }

    public void setInclusive(Boolean inclusive) {
        this.inclusive = inclusive;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sgst", sgst).append("cgst", cgst).append("igst", igst).append("inclusive", inclusive).toString();
    }

}
