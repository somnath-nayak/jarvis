package com.swiggy.api.sf.rng.pojo.MenuMerch;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class SubCategories {
    @JsonProperty("id")
    private String id;
    @JsonProperty("menu")
    private List<Menu> menu;
    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    public String getId ()
    {
        return id;
    }
    @JsonProperty("id")
    public void setId (String id)
    {
        this.id = id;
    }

    public SubCategories withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("menu")
    public List<Menu> getMenu ()
    {
        return menu;
    }
    @JsonProperty("menu")
    public void setMenu (List<Menu> menu)
    {
        this.menu = menu;
    }

    public SubCategories withMenu(List<Menu> menu) {
        this.menu = menu;
        return this;
    }

    @JsonProperty("name")
    public String getName ()
    {
        return name;
    }
    @JsonProperty("name")
    public void setName (String name)
    {
        this.name = name;
    }

    public SubCategories withName(String name) {
        this.name = name;
        return this;
    }

    public SubCategories setDefault(){
        return this.withId("12345")
                .withName("SubCategoryId")
                .withMenu(getMenu());
    }

    public SubCategories(){

    }

    public SubCategories(String subCatId,String name, List<Menu> menu)
    {
        this.id=subCatId;
        this.menu=menu;
        this.name="SubCategory";
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("menu",menu).toString();
    }
}
