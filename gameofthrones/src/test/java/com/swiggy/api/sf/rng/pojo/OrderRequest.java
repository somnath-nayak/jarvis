package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class OrderRequest {
	@JsonProperty("orderEdited")
	private Boolean orderEdited;
	@JsonProperty("orderEditSource")
	private Integer orderEditSource;
	@JsonProperty("campaignIds")
	private List<Integer> campaignIds = null;

	@JsonProperty("orderEdited")
	public Boolean getOrderEdited() {
	return orderEdited;
	}

	@JsonProperty("orderEdited")
	public void setOrderEdited(Boolean orderEdited) {
	this.orderEdited = orderEdited;
	}

	public OrderRequest withOrderEdited(Boolean orderEdited) {
	this.orderEdited = orderEdited;
	return this;
	}
	@JsonProperty("orderEditSource")
	public Integer getOrderEditedSource() {
	return orderEditSource;
	}
	@JsonProperty("orderEditSource")
	public void setOrderEditedSource(Integer orderEditSource) {
	this.orderEditSource = orderEditSource;
	}

	public OrderRequest withOrderEditedSource(Integer orderEditSource) {
	this.orderEditSource = orderEditSource;
	return this;
	}

	@JsonProperty("campaignIds")
	public List<Integer> getCampaignIds() {
	return campaignIds;
	}

	@JsonProperty("campaignIds")
	public void setCampaignIds(List<Integer> campaignIds) {
	this.campaignIds = campaignIds;
	}

	public OrderRequest withCampaignIds(List<Integer> campaignIds) {
	this.campaignIds = campaignIds;
	return this;
	}
}
