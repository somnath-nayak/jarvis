package com.swiggy.api.sf.rng.pojo.ads;

import com.swiggy.api.sf.rng.helper.Utility;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FetchChargesPOJO {
    @JsonProperty("instrumentType")
    private String instrumentType;
    @JsonProperty("startDate")
    private String startDate;
    @JsonProperty("endDate")
    private String endDate;
    @JsonProperty("timeSlots")
    private List<String> timeSlots = null;
    @JsonProperty("restaurantDetails")
    private HashMap<String,RestaurantDetails> restaurantDetails;

    @JsonProperty("instrumentType")
    public String getInstrumentType() {
        return instrumentType;
    }

    @JsonProperty("instrumentType")
    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public FetchChargesPOJO withInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
        return this;
    }

    @JsonProperty("startDate")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("startDate")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public FetchChargesPOJO withStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    @JsonProperty("endDate")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("endDate")
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public FetchChargesPOJO withEndDate(String endDate) {
        this.endDate = endDate;
        return this;
    }

    @JsonProperty("timeSlots")
    public List<String> getTimeSlots() {
        return timeSlots;
    }

    @JsonProperty("timeSlots")
    public void setTimeSlots(List<String> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public FetchChargesPOJO withTimeSlots(List<String> timeSlots) {
        this.timeSlots = timeSlots;
        return this;
    }

    @JsonProperty("restaurantDetails")
    public HashMap<String,RestaurantDetails>  getRestaurantDetails() {
        return restaurantDetails;
    }

    @JsonProperty("restaurantDetails")
    public void setRestaurantDetails(HashMap<String,RestaurantDetails>  restaurantDetails) {
        this.restaurantDetails = restaurantDetails;
    }

    public FetchChargesPOJO withRestaurantDetails(HashMap<String,RestaurantDetails>  restaurantDetails) {
        this.restaurantDetails = restaurantDetails;
        return this;
    }
    public List<String> setAllDaysTimeSlot(){
        List<String> slots =  new ArrayList<String>();
        slots.add("LATE_NIGHT");
        slots.add("BREAKFAST");
        slots.add("LUNCH");
        slots.add("SNACKS");
        slots.add("DINNER");
        return slots;

    }
    public List<String> setLateNightTimeSlot(){
        List<String> slots =  new ArrayList<String>();
        slots.add("LATE_NIGHT");
        slots.add(null);
        slots.add(null);
        slots.add(null);
        slots.add(null);
        return slots;

    }

    public FetchChargesPOJO setDefault(){
        this.restaurantDetails = new HashMap<>();
        return this
                .withInstrumentType("HIGH_PRIORITY")
                .withStartDate(Utility.getFutureDate(2,"yyyy-MM-dd"))
                .withEndDate(Utility.getFutureDate(4,"yyyy-MM-dd"))
                .withTimeSlots(setAllDaysTimeSlot());
    }
    public FetchChargesPOJO setRestDetailsMap(HashMap<String,RestaurantDetails> restaurantDetails){
        return this.withRestaurantDetails(restaurantDetails);
    }
}
