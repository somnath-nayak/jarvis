package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Root {
    private String event_type;

    private Data data;

    private String meta;

    private int id;

    private String op_type;

    private String version;

    private int event_timestamp;

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_type() {
        return this.event_type;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return this.data;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getMeta() {
        return this.meta;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setOp_type(String op_type) {
        this.op_type = op_type;
    }

    public String getOp_type() {
        return this.op_type;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return this.version;
    }

    public void setEvent_timestamp(int event_timestamp) {
        this.event_timestamp = event_timestamp;
    }

    public int getEvent_timestamp() {
        return this.event_timestamp;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("event_type", event_type).append("data", data).append("meta", meta).append("id", id).append("op_type", op_type).append("version", version).append("event_timestamp", event_timestamp).toString();
    }
}
