package com.swiggy.api.sf.rng.pojo.ads;

import com.swiggy.api.sf.rng.helper.Utility;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PrioritySlotsPOJO {

    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("end_time")
    private String endTime;
    @JsonProperty("timeSlots")
    private List<String> timeSlots = null;
    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("charges")
    private Integer charges;
//    @JsonProperty("target_clicks_per_day")
    private Integer targetClicksPerDay;
    @JsonProperty("updated_by")
    private String updatedBy;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("is_pre_paid")
    private Boolean isPrePaid;
    @JsonProperty("restaurantDetails")
    private HashMap<String,RestaurantDetails> restaurantDetails;

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public PrioritySlotsPOJO withStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public PrioritySlotsPOJO withEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    @JsonProperty("timeSlots")
    public List<String> getTimeSlots() {
        return timeSlots;
    }

    @JsonProperty("timeSlots")
    public void setTimeSlots(List<String> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public PrioritySlotsPOJO withTimeSlots(List<String> timeSlots) {
        this.timeSlots = timeSlots;
        return this;
    }

    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    public PrioritySlotsPOJO withActive(Boolean active) {
        this.active = active;
        return this;
    }

    @JsonProperty("charges")
    public Integer getCharges() {
        return charges;
    }

    @JsonProperty("charges")
    public void setCharges(Integer charges) {
        this.charges = charges;
    }

    public PrioritySlotsPOJO withCharges(Integer charges) {
        this.charges = charges;
        return this;
    }

    @JsonProperty("target_clicks_per_day")
    public Integer getTargetClicksPerDay() {
        return targetClicksPerDay;
    }

    @JsonProperty("target_clicks_per_day")
    public void setTargetClicksPerDay(Integer targetClicksPerDay) {
        this.targetClicksPerDay = targetClicksPerDay;
    }

    public PrioritySlotsPOJO withTargetClicksPerDay(Integer targetClicksPerDay) {
        this.targetClicksPerDay = targetClicksPerDay;
        return this;
    }

    @JsonProperty("updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updated_by")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public PrioritySlotsPOJO withUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    public PrioritySlotsPOJO withComment(String comment) {
        this.comment = comment;
        return this;
    }

    @JsonProperty("is_pre_paid")
    public Boolean getIsPrePaid() {
        return isPrePaid;
    }

    @JsonProperty("is_pre_paid")
    public void setIsPrePaid(Boolean isPrePaid) {
        this.isPrePaid = isPrePaid;
    }

    public PrioritySlotsPOJO withIsPrePaid(Boolean isPrePaid) {
        this.isPrePaid = isPrePaid;
        return this;
    }

    @JsonProperty("restaurantDetails")
    public HashMap<String,RestaurantDetails> getRestaurantDetails() {
        return restaurantDetails;
    }

    @JsonProperty("restaurantDetails")
    public void setRestaurantDetails(HashMap<String,RestaurantDetails> restaurantDetails) {
        this.restaurantDetails = restaurantDetails;
    }

    public PrioritySlotsPOJO withRestaurantDetails(HashMap<String,RestaurantDetails> restaurantDetails) {
        this.restaurantDetails = restaurantDetails;
        return this;
    }

    public List<String> setAllDaysTimeSlot(){
        List<String> slots =  new ArrayList<String>();
        slots.add("LATE_NIGHT");
        slots.add("BREAKFAST");
        slots.add("LUNCH");
        slots.add("SNACKS");
        slots.add("DINNER");
        return slots;

    }
    public PrioritySlotsPOJO setDefault(){
        this.restaurantDetails = new HashMap<>();
        return this
                .withStartTime(Utility.getFutureDate(2,"dd/MM/yyyy"))
                .withEndTime(Utility.getFutureDate(3,"dd/MM/yyyy"))
                .withTimeSlots(setAllDaysTimeSlot())
                .withActive(true)
                .withCharges(5000)
                .withTargetClicksPerDay(20)
                .withUpdatedBy("dev")
                .withComment("multi")
                .withIsPrePaid(true);

    }

    public PrioritySlotsPOJO setRestDetailsMap(HashMap<String,RestaurantDetails> restaurantDetails){
        return this.withRestaurantDetails(restaurantDetails);
    }

}