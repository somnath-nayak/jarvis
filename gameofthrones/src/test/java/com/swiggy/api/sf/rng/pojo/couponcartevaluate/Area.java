package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class Area {
	@JsonProperty("areaEntity")
	private AreaEntity areaEntity;
	@JsonProperty("areaEntity")
	public AreaEntity getAreaEntity() {
		return areaEntity;
	}
	@JsonProperty("areaEntity")
	public void setAreaEntity(AreaEntity areaEntity) {
		this.areaEntity = areaEntity;
	}
	public Area withAreaEntity(AreaEntity areaEntity) {
		this.areaEntity = areaEntity;
		return this;
	}
}
