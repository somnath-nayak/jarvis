package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "campaign_id",
        "reward_type",
        "discount_amount",
        "is_super",
        "discount_breakup",
        "seeding_info"
})
public class TradeDiscountBreakup {




    @JsonProperty("campaign_id")
    private Integer campaignId;
    @JsonProperty("reward_type")
    private String rewardType;
    @JsonProperty("discount_amount")
    private Integer discountAmount;
    @JsonProperty("is_super")
    private Boolean isSuper;
    @JsonProperty("discount_breakup")
    private Object discountBreakup;
    @JsonProperty("seeding_info")
    private SeedingInfo seedingInfo;

    @JsonProperty("campaign_id")
    public Integer getCampaignId() {
        return campaignId;
    }

    @JsonProperty("campaign_id")
    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    @JsonProperty("reward_type")
    public String getRewardType() {
        return rewardType;
    }

    @JsonProperty("reward_type")
    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    @JsonProperty("discount_amount")
    public Integer getDiscountAmount() {
        return discountAmount;
    }

    @JsonProperty("discount_amount")
    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

    @JsonProperty("is_super")
    public Boolean getIsSuper() {
        return isSuper;
    }

    @JsonProperty("is_super")
    public void setIsSuper(Boolean isSuper) {
        this.isSuper = isSuper;
    }

    @JsonProperty("discount_breakup")
    public Object getDiscountBreakup() {
        return discountBreakup;
    }

    @JsonProperty("discount_breakup")
    public void setDiscountBreakup(Object discountBreakup) {
        this.discountBreakup = discountBreakup;
    }

    @JsonProperty("seeding_info")
    public SeedingInfo getSeedingInfo() {
        return seedingInfo;
    }

    @JsonProperty("seeding_info")
    public void setSeedingInfo(SeedingInfo seedingInfo) {
        this.seedingInfo = seedingInfo;
    }

}