package com.swiggy.api.sf.rng.pojo.ads.Segmentation;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.codehaus.jackson.annotate.JsonProperty;
import java.util.Date;

public class OrderCreateMessage {
    
    @JsonProperty("restaurant_city_code")
    private int cityId =1;

    @JsonProperty("restaurant_area_code")
    private int areaId =1;

    @JsonProperty("customer_id")
    private long customerId;

    @JsonProperty("restaurant_id")
    private long restaurantId;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Kolkata")
    @JsonProperty("order_time")
    private String orderTime;

    @JsonProperty("payment_txn_status")
    private String paymentTxnStatus;

    @JsonProperty("order_type")
    private String orderType="Regular"; // regular, promo, express, etc

    @JsonProperty("sharedOrder")
    private boolean sharedOrder = false;

    @JsonProperty("payment_method")
    private String paymentMethod ;

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("order_status")
    private String status;
    
    public int getCityId() {
        return cityId;
    }
    
    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
    
    public int getAreaId() {
        return areaId;
    }
    
    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }
    
    public long getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }
    
    public String getPaymentTxnStatus() {
        return paymentTxnStatus;
    }
    
    public void setPaymentTxnStatus(String paymentTxnStatus) {
        this.paymentTxnStatus = paymentTxnStatus;
    }
    
    public String getOrderType() {
        return orderType;
    }
    
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
    
    public boolean isSharedOrder() {
        return sharedOrder;
    }
    
    public void setSharedOrder(boolean sharedOrder) {
        this.sharedOrder = sharedOrder;
    }
    
    public String getPaymentMethod() {
        return paymentMethod;
    }
    
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    public String getOrderId() {
        return orderId;
    }
    
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    public long getRestaurantId() {
        return restaurantId;
    }
    
    public String getOrderTime() {
        return orderTime;
    }
    
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }
    
    
    public void setRestaurantId(long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
