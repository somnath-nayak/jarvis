package com.swiggy.api.sf.checkout.helper.edvo.pojo.mealTDRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "restaurant",
        "operationType",
        "meals"
})
public class Discount {

    @JsonProperty("restaurant")
    private Restaurant restaurant;
    @JsonProperty("operationType")
    private String operationType;
    @JsonProperty("meals")
    private List<Meal> meals = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Discount() {
    }

    /**
     *
     * @param meals
     * @param operationType
     * @param restaurant
     */
    public Discount(Restaurant restaurant, String operationType, List<Meal> meals) {
        super();
        this.restaurant = restaurant;
        this.operationType = operationType;
        this.meals = meals;
    }

    @JsonProperty("restaurant")
    public Restaurant getRestaurant() {
        return restaurant;
    }

    @JsonProperty("restaurant")
    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @JsonProperty("operationType")
    public String getOperationType() {
        return operationType;
    }

    @JsonProperty("operationType")
    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    @JsonProperty("meals")
    public List<Meal> getMeals() {
        return meals;
    }

    @JsonProperty("meals")
    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("restaurant", restaurant).append("operationType", operationType).append("meals", meals).toString();
    }

}