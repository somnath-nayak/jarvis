package com.swiggy.api.sf.checkout.pojo;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
		"name",
		"mobile",
		"address",
		"landmark",
		"area",
		"lat",
		"lng",
		"flat_no",
		"city",
		"annotation"
})

public class AddressPOJO {
	@JsonProperty("name")
	private String name;
	@JsonProperty("mobile")
	private String mobile;
	@JsonProperty("address")
	private String address;
	@JsonProperty("landmark")
	private String landmark;
	@JsonProperty("area")
	private String area;
	@JsonProperty("lat")
	private String lat;
	@JsonProperty("lng")
	private String lng;
	@JsonProperty("flat_no")
	private String flatNo;
	@JsonProperty("city")
	private String city;
	@JsonProperty("annotation")
	private String annotation;

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}
	public AddressPOJO withName(String name) {
		this.name = name;
		return this;
	}
	@JsonProperty("mobile")
	public String getMobile() {
		return mobile;
	}
	@JsonProperty("mobile")
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public AddressPOJO withMobile(String mobile) {
		this.mobile = mobile;
		return this;
	}
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}
	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}
	public AddressPOJO withAddress(String address) {
		this.address = address;
		return this;
	}
	@JsonProperty("landmark")
	public String getLandmark() {
		return landmark;
	}
	@JsonProperty("landmark")
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public AddressPOJO withLandmark(String landmark) {
		this.landmark = landmark;
		return this;
	}
	@JsonProperty("area")
	public String getArea() {
		return area;
	}
	@JsonProperty("area")
	public void setArea(String area) {
		this.area = area;
	}
	public AddressPOJO withArea(String area) {
		this.area = area;
		return this;
	}
	@JsonProperty("lat")
	public String getLat() {
		return lat;
	}
	@JsonProperty("lat")
	public void setLat(String lat) {
		this.lat = lat;
	}
	public AddressPOJO withLat(String lat) {
		this.lat = lat;
		return this;
	}
	@JsonProperty("lng")
	public String getLng() {
		return lng;
	}
	@JsonProperty("lng")
	public void setLng(String lng) {
		this.lng = lng;
	}
	public AddressPOJO withLng(String lng) {
		this.lng = lng;
		return this;
	}
	@JsonProperty("flat_no")
	public String getFlatNo() {
		return flatNo;
	}
	@JsonProperty("flat_no")
	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}
	public AddressPOJO withFlatNo(String flatNo) {
		this.flatNo = flatNo;
		return this;
	}
	@JsonProperty("city")
	public String getCity() {
		return city;
	}
	@JsonProperty("city")
	public void setCity(String city) {
		this.city = city;
	}
	public AddressPOJO withCity(String city) {
		this.city = city;
		return this;
	}
	@JsonProperty("annotation")
	public String getAnnotation() {
		return annotation;
	}
	@JsonProperty("annotation")
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
	public AddressPOJO withAnnotation(String annotation) {
		this.annotation = annotation;
		return this;
	}
	public AddressPOJO setDefaultData() {
		return this.withName("Dev's Home")
		.withAddress("#509, 2nd floor")
		.withLandmark("near muthoot finance")
		.withArea("Koramangala 5th block")
		.withLat("12.9279")
		.withLng("77.6271")
		.withFlatNo("")
		.withCity("Bangalore")
		.withAnnotation("Automation_Test");
	}

	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("mobile", mobile)
				.append("address", address).append("landmark", landmark)
				.append("area", area).append("lat", lat)
				.append("lng", lng).append("flat_no", flatNo)
				.append("city", city).append("annotation", annotation).toString();
	}
}
