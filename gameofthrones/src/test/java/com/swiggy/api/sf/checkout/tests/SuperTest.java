package com.swiggy.api.sf.checkout.tests;

import java.util.HashMap;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.dp.SuperDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.HelpHelper;
import com.swiggy.api.sf.checkout.helper.SuperHelper;

import framework.gameofthrones.Aegon.Initialize;

public class SuperTest extends SuperDP {

    Initialize gameofthrones = new Initialize();
    CheckoutHelper helper= new CheckoutHelper();
    SuperHelper superHelper=new SuperHelper();
    HelpHelper helpHelper=new HelpHelper();
    String tid;
    String token;
    
    @BeforeTest()
    public void login(){
    	HashMap<String, String> hashMap=helper.TokenData(CheckoutConstants.mobile1, CheckoutConstants.password1);
    	tid=hashMap.get("Tid");
        token=hashMap.get("Token");
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "validateRegularCart")
    public void validateRegularCart(String menu_item_id,String quantity, String restId){
    	String cartResponse=superHelper.callCreateCart(tid,token,menu_item_id, quantity, restId,"","");
       superHelper.validateSubscriptionNudge(cartResponse,superConstant.NUDGE_CTA_MESSAGE);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "validateRegularplaceOrder")
    public void validateRegularplaceOrder(String menu_item_id,String quantity, String restId){
    	String orderRes=createNplaceOrder(menu_item_id, quantity, restId, "", "");
    	superHelper.sanityResponse(orderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "trackRegularOrder")
    public void trackRegularOrder(String menu_item_id,String quantity, String restId){
    	String orderRes=createNplaceOrder(menu_item_id, quantity, restId, "", "");
    	String trackRes=helpHelper.track(tid, token, superHelper.getOrderId(orderRes)).ResponseValidator.GetBodyAsText();
    	superHelper.sanityResponse(trackRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "cancelRegularOrder")
    public void cancelRegularOrder(String menu_item_id,String quantity, String restId){
    	String orderRes=createNplaceOrder(menu_item_id, quantity, restId, "", "");
    	String cancelOrderRes=cancelOrder(orderRes);
    	superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "refundRegularOrder")
    public void refundRegularOrder(String menu_item_id,String quantity, String restId){
    	String orderRes=createNplaceOrder(menu_item_id, quantity, restId, "", "");
    	String refundRes=callFullRefund(orderRes);
    	superHelper.sanityResponse(refundRes,PricingConstants.STATUS_CODE_ONE,PricingConstants.REFUND_MESSAGE);
    	String cancelOrderRes=callOrderCancel(orderRes);
    	superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "validateCartWithFoodNSubscription")
    public void validateCartWithFoodNSubscription(String menu_item_id,String quantity, String restId){
    	String cartResponse=superHelper.createCart(tid,token,menu_item_id,quantity,restId,"","");
        String planId=superHelper.getSubscriptionID(cartResponse)[superConstant.INT_ZERO];
        String superCartRes=superHelper.callCreateCart(tid,token,menu_item_id, quantity, restId, planId, superConstant.SUB_QUANTITY);
        superHelper.validateFoodNSuperCartRes(superCartRes, planId,superConstant.ZERO_AS_STRING);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "validatePlaceOrderWithFoodNSubscription")
    public void validatePlaceOrderWithFoodNSubscription(String menu_item_id,String quantity, String restId){
    	String cartResponse=superHelper.createCart(tid,token,menu_item_id,quantity,restId,"","");
        String planId=superHelper.getSubscriptionID(cartResponse)[superConstant.INT_ZERO];
    	String placeOrderRes=createNplaceOrder(menu_item_id, quantity, restId, planId, superConstant.SUB_QUANTITY);
    	superHelper.validateFoodNSuperPlaceOrder(placeOrderRes, planId);
    	String cancelOrderRes=callSuperOrderCancel(placeOrderRes);
    	superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "trackOrderWithFoodNSubscription")
    public void ValidateTrackOrderWithFoodNSubscription(String menu_item_id,String quantity, String restId){
    	String cartResponse=superHelper.createCart(tid,token,menu_item_id,quantity,restId,"","");
        String planId=superHelper.getSubscriptionID(cartResponse)[superConstant.INT_ZERO];
    	String placeOrderRes=createNplaceOrder(menu_item_id, quantity, restId, planId, superConstant.SUB_QUANTITY);
        String trackRes=helpHelper.track(tid, token, superHelper.getOrderId(placeOrderRes)).ResponseValidator.GetBodyAsText();
   	    superHelper.sanityResponse(trackRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    	String cancelOrderRes=callSuperOrderCancel(placeOrderRes);
 	    superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
  
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "cancel Order With Food and Subscription")
    public void ValidateCancelOrderWithFoodNSubscription(String menu_item_id,String quantity, String restId){
    	String cartResponse=superHelper.createCart(tid,token,menu_item_id,quantity,restId,"","");
        String planId=superHelper.getSubscriptionID(cartResponse)[superConstant.INT_ZERO];
    	String orderRes=createNplaceOrder(menu_item_id, quantity, restId, planId, superConstant.SUB_QUANTITY);
   	   String cancelOrderRes=callOrderCancel(orderRes);
       superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
       superHelper.validateCancelOrderStatus(cancelOrderRes, superConstant.CANCEL_STATUS);
       superHelper.validateOrderTag(cancelOrderRes, superConstant.CART_TYPE_SUPER);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "refund Order With Food and Subscription")
    public void refundOrderWithFoodNSubscription(String menu_item_id,String quantity, String restId){
    	String cartResponse=superHelper.createCart(tid,token,menu_item_id,quantity,restId,"","");
        String planId=superHelper.getSubscriptionID(cartResponse)[superConstant.INT_ZERO];
    	String orderRes=createNplaceOrder(menu_item_id, quantity, restId, planId, superConstant.SUB_QUANTITY);
    	//String refundRes=callFullRefund(orderRes);
       //superHelper.sanityResponse(refundRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
       String cancelOrderRes=callOrderCancel(orderRes);
       superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "validatePlaceOrderWithFoodNSubscription")
    public void validateEditOrderFoodNSubscription(String menu_item_id,String quantity, String restId){
    	String cartResponse=superHelper.createCart(tid,token,menu_item_id,quantity,restId,"","");
        String planId=superHelper.getSubscriptionID(cartResponse)[superConstant.INT_ZERO];
    	String placeOrderRes=createNplaceOrder(menu_item_id, quantity, restId, planId, superConstant.SUB_QUANTITY);
    	superHelper.sanityResponse(placeOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    	int quant=Integer.parseInt(quantity);
    	int tempQuantity=++quant;
    	String newQuantity=tempQuantity+"";
    	String editOrderRes=helper.editOrderCheck(tid, token, superHelper.getOrderId(placeOrderRes),
    			superConstant.SUB_QUANTITY, menu_item_id,newQuantity,restId).ResponseValidator.GetBodyAsText();
    	superHelper.sanityResponse(editOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.CART_UPDATED_SUCCESSFULLY);
    	String editConfirmOrderRes=helper.editOrderConfirm(tid, token, superHelper.getOrderId(placeOrderRes),
    			superConstant.SUB_QUANTITY, menu_item_id,newQuantity,restId).ResponseValidator.GetBodyAsText();
    	superHelper.sanityResponse(editConfirmOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    	String cancelOrderRes=callOrderCancel(placeOrderRes);
        superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
     
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "create Cart Only Subscription")
    public void validateCartOnlySubscription(String menu_item_id,String quantity, String restId){
    	String[] resArray=createCartOnlySubscription(menu_item_id,quantity,restId);
    	superHelper.validateCartResOnlySubscription(resArray[0],resArray[1],superConstant.EXPECTED_FALSE_FLAG);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "place Order Only Subscription Using wallet")
    public void placeOrderOnlySubscriptionUsingWallet(String menu_item_id,String quantity, String restId){
       String orderRes=callCreateNPlaceOrderOnlySuper(menu_item_id, quantity, restId);
       superHelper.sanityResponse(orderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
       superHelper.validateOrderDelCharge(orderRes,superConstant.ZERO_AS_STRING);
       String cancelOrderRes=callSuperOrderCancel(orderRes);
       superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
   
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "validatePlaceOrderWithFoodNSubscription")
    public void validatePlaceOrderAsSuper(String menu_item_id,String quantity, String restId){
    	String cartResponse=superHelper.createCart(tid,token,menu_item_id,quantity,restId,"","");
        String planId=superHelper.getSubscriptionID(cartResponse)[superConstant.INT_ZERO];
    	String placeOrderRes=createNplaceOrder(menu_item_id, quantity, restId, planId, superConstant.SUB_QUANTITY);
    	superHelper.validateFoodNSuperPlaceOrder(placeOrderRes, planId);
    	String superCartRes=superHelper.callCreateCart(tid,token,menu_item_id, quantity, restId,"","");
    	superHelper.validateCartDelCharge(superCartRes, superConstant.ZERO_AS_STRING);
    	superHelper.validateResFees(superCartRes, superConstant.ZERO_AS_STRING);
 	    superHelper.validateOrderTotalInCart(superCartRes);
 	    superHelper.validateTradeDiscountRewardList(superCartRes);
    	
    	String addressId=helper.getAddressIDfromCartResponse(tid, token, superCartRes);
 	    String orderRes= placeRegularOrder(menu_item_id, quantity, restId, addressId);
    	superHelper.sanityResponse(orderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    
    	String cancelOrderRes=callSuperOrderCancel(placeOrderRes);
    	superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "cancel Order Only Subscription")
    public void cancelOrderOnlySubscription(String menu_item_id,String quantity, String restId){
    	String orderRes=callCreateNPlaceOrderOnlySuper(menu_item_id, quantity, restId);
    	String cancelOrderRes=callSuperOrderCancel(orderRes);
       //superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    }
    
    @Test(dataProvider="super", groups = {"Smoke","chandan"}, description = "refund Only Subscription")
    public void refundOnlySubscription(String menu_item_id,String quantity, String restId){
    	String orderRes=callCreateNPlaceOrderOnlySuper(menu_item_id, quantity, restId);
    	String refundRes=callFullRefund(orderRes);
    	superHelper.sanityResponse(refundRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    	String cancelOrderRes=callSuperOrderCancel(orderRes);
       //superHelper.sanityResponse(cancelOrderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
    }
    
    public String placeRegularOrder(String menu_item_id,String quantity, String restId,String addressId){
    	return helper.orderPlaceV2(tid, token, addressId,superConstant.PAYMENT_METHOD_CASH,superConstant.DEF_STRING)
        		.ResponseValidator.GetBodyAsText();
    }
    
    public String createNplaceOrder(String menu_item_id,String quantity, String restId,String planId,String Planquantity){
       String cartResponse=superHelper.callCreateCart(tid,token,menu_item_id, quantity, restId,planId,Planquantity);
	   String addressId=helper.getAddressIDfromCartResponse(tid, token, cartResponse);
	return placeRegularOrder(menu_item_id, quantity, restId, addressId);
	}
    
   
    public String cancelOrder(String orderRes){
    return helper.OrderCancel(tid, token, CheckoutConstants.authorization, superConstant.DEF_STRING,
            superConstant.EXPECTED_TRUE_FLAG, superConstant.ZERO_AS_STRING, superConstant.DEF_STRING, superHelper.getOrderId(orderRes))
            .ResponseValidator.GetBodyAsText();
    }
    
    public String callFullRefund(String orderRes){
    	return helper.fullRefund(tid,token,superHelper.getOrderId(orderRes), "PG", superConstant.DEF_STRING,superConstant.DEF_STRING)
                .ResponseValidator.GetBodyAsText();
    }
    
    public String callOrderCancel(String orderRes){
    	return helper.OrderCancel(tid, token, CheckoutConstants.authorization, superConstant.DEF_STRING,
                superConstant.EXPECTED_TRUE_FLAG, superConstant.ZERO_AS_STRING, superConstant.DEF_STRING, superHelper.getOrderId(orderRes))
                .ResponseValidator.GetBodyAsText();
    }
    
    public String callSuperOrderCancel(String orderRes){
    	return superHelper.superOrderCancel(tid, token,superConstant.DEF_STRING,
                superConstant.EXPECTED_TRUE_FLAG, superConstant.ZERO_AS_STRING,superConstant.DEF_STRING,superConstant.ORDER_TYPE, superHelper.getOrderId(orderRes))
                .ResponseValidator.GetBodyAsText();
    }
    
    public void validateCartOnlySuper(String cartRes ,String planId){
        superHelper.validateCartResOnlySubscription(cartRes,planId,superConstant.EXPECTED_FALSE_FLAG);
        String orderRes= helper.placeSpecialOrder(tid, token, superConstant.PAYMENT_METHOD_CASH, superConstant.DEF_STRING,superConstant.ORDER_TYPE).ResponseValidator.GetBodyAsText();
        superHelper.sanityResponse(orderRes,PricingConstants.VALID_STATUS_CODE,PricingConstants.DONE_SUCCESSFULLY_MESSAGE);
        superHelper.validateCartType(cartRes, superConstant.CART_TYPE_SUPER);
    }
    
    public String callCreateNPlaceOrderOnlySuper(String menu_item_id,String quantity, String restId){
    	String planId=superHelper.getPlanIDfromCartResponse(tid,token,menu_item_id, quantity, restId);
    	superHelper.callCreateCartOnlySuper(tid,token,planId);
    	String orderRes=helper.placeSpecialOrder(tid, token, superConstant.MOBIKWIK_SSO, superConstant.DEF_STRING,superConstant.ORDER_TYPE).ResponseValidator.GetBodyAsText();
        return orderRes;
    }
    
    public String[] createCartOnlySubscription(String menu_item_id,String quantity, String restId){
    	String planId=superHelper.getPlanIDfromCartResponse(tid,token,menu_item_id, quantity, restId);
    	String cartRes=superHelper.callCreateCartOnlySuper(tid,token,planId);
    	String[] array={cartRes,planId};
    	return array;
    }
 }