package com.swiggy.api.sf.rng.pojo.carousel;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class CarouselTradeDiscountInfoSet {
    @JsonProperty("discountType")
    private String discountType;
    @JsonProperty("tradeDiscountIds")
    private List<String> tradeDiscountIds = null;

    @JsonProperty("discountType")
    public String getDiscountType() {
        return discountType;
    }

    @JsonProperty("discountType")
    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public CarouselTradeDiscountInfoSet withDiscountType(String discountType) {
        this.discountType = discountType;
        return this;
    }

    @JsonProperty("tradeDiscountIds")
    public List<String> getTradeDiscountIds() {
        return tradeDiscountIds;
    }

    @JsonProperty("tradeDiscountIds")
    public void setTradeDiscountIds(List<String> tradeDiscountIds) {
        this.tradeDiscountIds = tradeDiscountIds;
    }

    public CarouselTradeDiscountInfoSet withTradeDiscountIds(List<String> tradeDiscountIds) {
        this.tradeDiscountIds = tradeDiscountIds;
        return this;
    }
}
