package com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "minCartAmount",
        "firstOrder",
        "userId",
        "userAgent",
        "versionCode",
        "itemRequests"
})
public class EvaluateMenu {

    @JsonProperty("minCartAmount")
    private Integer minCartAmount;
    @JsonProperty("firstOrder")
    private Boolean firstOrder;
    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("userAgent")
    private String userAgent;
    @JsonProperty("versionCode")
    private String versionCode;
    @JsonProperty("itemRequests")
    private List<ItemRequests> itemRequests = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public EvaluateMenu() {
    }

    /**
     *
     * @param itemRequests
     * @param userAgent
     * @param firstOrder
     * @param userId
     * @param versionCode
     * @param minCartAmount
     */
    public EvaluateMenu(Integer minCartAmount, Boolean firstOrder, Integer userId, String userAgent, String versionCode, List<ItemRequests> itemRequests) {
        super();
        this.minCartAmount = minCartAmount;
        this.firstOrder = firstOrder;
        this.userId = userId;
        this.userAgent = userAgent;
        this.versionCode = versionCode;
        this.itemRequests = itemRequests;
    }

    @JsonProperty("minCartAmount")
    public Integer getMinCartAmount() {
        return minCartAmount;
    }

    @JsonProperty("minCartAmount")
    public void setMinCartAmount(Integer minCartAmount) {
        this.minCartAmount = minCartAmount;
    }

    public EvaluateMenu withMinCartAmount(Integer minCartAmount) {
        this.minCartAmount = minCartAmount;
        return this;
    }

    @JsonProperty("firstOrder")
    public Boolean getFirstOrder() {
        return firstOrder;
    }

    @JsonProperty("firstOrder")
    public void setFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
    }

    public EvaluateMenu withFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
        return this;
    }

    @JsonProperty("userId")
    public Integer getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public EvaluateMenu withUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    @JsonProperty("userAgent")
    public String getUserAgent() {
        return userAgent;
    }

    @JsonProperty("userAgent")
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public EvaluateMenu withUserAgent(String userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    @JsonProperty("versionCode")
    public String getVersionCode() {
        return versionCode;
    }

    @JsonProperty("versionCode")
    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public EvaluateMenu withVersionCode(String versionCode) {
        this.versionCode = versionCode;
        return this;
    }

    @JsonProperty("itemRequests")
    public List<ItemRequests> getItemRequests() {
        return itemRequests;
    }

    @JsonProperty("itemRequests")
    public void setItemRequests(List<ItemRequests> itemRequests) {
        this.itemRequests = itemRequests;
    }

    public EvaluateMenu withItemRequests(List<ItemRequests> itemRequests) {
        this.itemRequests = itemRequests;
        return this;
    }
    public void build(int userId,int restId,boolean firstOrder)
    {
        ItemRequests itr =new ItemRequests();
        itr.setRestaurantId(restId);
        List<ItemRequests> itemRequests=new ArrayList<ItemRequests>();
        itemRequests.add(0,itr);
        withUserId(userId).withFirstOrder(firstOrder).withMinCartAmount(300).withItemRequests(itemRequests).withUserAgent("WEB").withVersionCode("300");
    }

}
