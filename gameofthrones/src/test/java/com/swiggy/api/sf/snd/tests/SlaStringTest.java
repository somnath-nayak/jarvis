package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SlaStringTest extends SnDDp {
    SANDHelper helper =new  SANDHelper();

    List<String> restIds= new ArrayList<>();


    String[] latLng =  {"12.9165757","77.6101163"};


    @BeforeClass
    public void getServiceableAndOpenRestaurant(){
        restIds = helper.aggregatorRestListSearch(latLng);
        Assert.assertFalse(restIds.isEmpty(),"Assertion Failed :: No serviceable and open rest found.");
    }

   @Test(dataProvider = "menuV4WithUA", description = "Verify sla range for android and ios for version code above 435 ")
    public void menuV4AboveVersion( String lat, String lng, String version, String userAgent) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestIdWithVersionAndUserAgent(restId, lat, lng, version, userAgent);
        String response = processor.ResponseValidator.GetBodyAsText();
        int minTime = JsonPath.read(response, "$.data.sla.minDeliveryTime");
        int maxTime = JsonPath.read(response, "$.data.sla.maxDeliveryTime");
        String slaString = JsonPath.read(response, "$.data.sla.slaString");
        Assert.assertEquals(slaString,minTime+"-"+maxTime+" "+"MINS");
    }

    @Test(dataProvider = "menuV4WithUAOnVersion", description = "Verify sla range for android and ios for version code  435 ")
    public void menuV4OnVersion( String lat, String lng, String version, String userAgent) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestIdWithVersionAndUserAgent(restId, lat, lng, version, userAgent);
        String response = processor.ResponseValidator.GetBodyAsText();
        int deliveryTime = JsonPath.read(response, "$.data.sla.deliveryTime");
        String slaString = JsonPath.read(response, "$.data.sla.slaString");
        Assert.assertEquals(slaString,+deliveryTime+" "+"MINS");
    }

    @Test(dataProvider = "menuV4WithUABelowVersion", description = "Verify sla range for android and ios for version code below 435 ")
    public void menuV4BelowVersion( String lat, String lng, String version, String userAgent) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestIdWithVersionAndUserAgent(restId, lat, lng, version, userAgent);
        String response = processor.ResponseValidator.GetBodyAsText();
        int deliveryTime = JsonPath.read(response, "$.data.sla.deliveryTime");
        String slaString = JsonPath.read(response, "$.data.sla.slaString");
        Assert.assertEquals(slaString,+deliveryTime+" "+"MINS");
    }



}
