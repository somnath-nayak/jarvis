package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "SGST",
        "CGST",
        "IGST"
})
public class GSTDetails {

    @JsonProperty("SGST")
    private Integer sGST;
    @JsonProperty("CGST")
    private Integer cGST;
    @JsonProperty("IGST")
    private Integer iGST;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public GSTDetails() {
    }

    /**
     *
     * @param iGST
     * @param cGST
     * @param sGST
     */
    public GSTDetails(Integer sGST, Integer cGST, Integer iGST) {
        super();
        this.sGST = sGST;
        this.cGST = cGST;
        this.iGST = iGST;
    }

    @JsonProperty("SGST")
    public Integer getSGST() {
        return sGST;
    }

    @JsonProperty("SGST")
    public void setSGST(Integer sGST) {
        this.sGST = sGST;
    }

    @JsonProperty("CGST")
    public Integer getCGST() {
        return cGST;
    }

    @JsonProperty("CGST")
    public void setCGST(Integer cGST) {
        this.cGST = cGST;
    }

    @JsonProperty("IGST")
    public Integer getIGST() {
        return iGST;
    }

    @JsonProperty("IGST")
    public void setIGST(Integer iGST) {
        this.iGST = iGST;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sGST", sGST).append("cGST", cGST).append("iGST", iGST).append("additionalProperties", additionalProperties).toString();
    }

}