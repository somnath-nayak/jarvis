package com.swiggy.api.sf.checkout.helper.paas;


import com.swiggy.api.sf.checkout.constants.PaasConstant;
import com.swiggy.api.sf.checkout.pojo.mySms.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;

public class MySmsHelper {

    Initialize gameofthrones=new Initialize();
    JsonHelper jsonHelper=new JsonHelper();


    public static HashMap<String,String> getDefaultHeader(){
        HashMap<String, String> headers = new HashMap<>();
        headers.put(PaasConstant.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(PaasConstant.HEADER_CONNECTION, "keep-alive");
        return headers;
    }


    public Processor mySmsLogin(String msisdn, String password, String apiKey){
        Processor processor=null;
        try {
            GameOfThronesService service = new GameOfThronesService("checkout1", "mySmsLogin", gameofthrones);
            MySmsLogin mySmsLogin = new MySmsLogin(msisdn, password, apiKey);
            String payload=jsonHelper.getObjectToJSON(mySmsLogin);
            processor = new Processor(service, getDefaultHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }

    public Processor mySmsGetConversation(String apiKey, String authToken, String address, Integer offset, Integer limit){
        Processor processor=null;
        try {
            GameOfThronesService service = new GameOfThronesService("checkout1", "mySmsGetConversation", gameofthrones);
            MySmsGetConversations mySmsLogin = new MySmsGetConversations(apiKey,authToken,address,offset,limit);
            String payload=jsonHelper.getObjectToJSON(mySmsLogin);
            processor = new Processor(service, getDefaultHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }

    public Processor mySmsGetAllConversation(String apiKey, String authToken){
        Processor processor=null;
        try {
            GameOfThronesService service = new GameOfThronesService("checkout1", "mySmsGetAllConversation", gameofthrones);
            MySmsGetAllconversations mySmsGetAllConversations = new MySmsGetAllconversations(apiKey,authToken);
            String payload=jsonHelper.getObjectToJSON(mySmsGetAllConversations);
            processor = new Processor(service, getDefaultHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }

    public Processor mySmsGetDeviceId(String apiKey, String authToken, Integer os, String pushRegistrationId){
        Processor processor=null;
        try {
            GameOfThronesService service = new GameOfThronesService("checkout1", "mySmsGetDeviceId", gameofthrones);
            MySmsGetDeviceId mySmsGetDeviceId = new MySmsGetDeviceId(apiKey,authToken,os,pushRegistrationId);
            String payload=jsonHelper.getObjectToJSON(mySmsGetDeviceId);
            processor = new Processor(service, getDefaultHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }

    public Processor mySmsDeleteMessage(String apiKey, String authToken, String address){
        Processor processor=null;
        try {
            GameOfThronesService service = new GameOfThronesService("checkout1", "mySmsDeleteMessage", gameofthrones);
            MySmsDeleteMessage mySmsDeleteMessage = new MySmsDeleteMessage(apiKey,authToken,address);
            String payload=jsonHelper.getObjectToJSON(mySmsDeleteMessage);
            processor = new Processor(service, getDefaultHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }

    public Processor mySmsDeleteAllMessage(String apiKey, String authToken, List<String> addresses){
        Processor processor=null;
        try {
            GameOfThronesService service = new GameOfThronesService("checkout1", "mySmsDeleteAllMessage", gameofthrones);
            MySmsDeleteAllMessages mySmsDeleteAllMessages = new MySmsDeleteAllMessages(apiKey,authToken,addresses);
            String payload=jsonHelper.getObjectToJSON(mySmsDeleteAllMessages);
            processor = new Processor(service, getDefaultHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }

    public Processor mySmsSyncMessage(String apiKey, String authToken, Integer deviceId, Integer syncLimit){
        Processor processor=null;
        try {
            GameOfThronesService service = new GameOfThronesService("checkout1", "mySmsSyncMessage", gameofthrones);
            MySmsSyncMessage mySmsSyncMessage = new MySmsSyncMessage( apiKey,authToken,deviceId,syncLimit);
            String payload=jsonHelper.getObjectToJSON(mySmsSyncMessage);
            processor = new Processor(service, getDefaultHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }


}
