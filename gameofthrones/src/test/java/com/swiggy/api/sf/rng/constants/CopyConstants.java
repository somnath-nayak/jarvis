package com.swiggy.api.sf.rng.constants;

import com.swiggy.api.sf.rng.helper.Utility;
import io.advantageous.boon.core.Str;

public interface CopyConstants {
    String[] type = {"Flat", "Percentage", "Freebie", "FREE_DELIVERY"};
    String[] discountLevel = {"Restaurant", "Category", "Subcategory", "Item"};
    String minCartAmount = "99";
    String cap = "10";
    String defaultMaxCap = "5000";
    String[] benefits = {"FREE_DELIVERY","Freebie"};
    String nonSuperUserId = "101010101";
    String superUserId = "2222220";
    String automationLevelPlaceHolder = " Automation-Default-CopyFalse";
    String automationLevelPlaceHolderWithCopyTrue = " Automation-Default-CopyTrue";
    String userAgent[] = {"ANDROID","IOS"};
    String userVersion = "303";
    String[] campaignType = {"R.Flat","R.Percentage","R.Freebie","R.Freedel","S.Freebie","S.Freedel"};
    String statusFail = "fail";
    String headerPercentOffAndMore = "10% off & more";
    String headerFreedelAndMore = "[\"Free Delivery & more\"]";
    String shortDescriptionFreedelWithMOV = "Free Delivery on order above ₹99";
    String shortDescriptionFreedelWithNoMOV = "Free Delivery on all orders";
    String shortDescriptionPercentageWithMOV = "10% off on orders above ₹99";
    String shortDescriptionPercentageWithNOMOV = "10% off on all orders";
    String shortDescriptionPercentageWithNoMOVAndNoMax = "10% off on all orders";
    String shortDescriptionPercentageWithMOVAndMax = "10% off on orders above ₹99";
    String shortDescriptionFreeDelightWithMOV = "Free delight on orders above ₹99";
    String percentDescriptionWithMOVAndMaxDiscount = "10% off on orders above ₹99 up to ₹10";
    String percentDescriptionWithNoMOVAndNoMaxDiscount = "10% off on all orders";
    String percentDescriptionWithMOVAndNoMaxDiscount = "10% off on orders above ₹99";
    String percentDescriptionWithNoMOVAndMaxDiscount = "10% off on all orders up to ₹10";
    String freebieWithMOV ="Free delight on orders above ₹99";
    String superStatusMeta = "{\"meta\":{\"schemaVersion\":\"1.0.0\"},\"value\":{\"superStatus\":\"$1\"}}";
    String  superOperationType = "SUPER";
    String freeDelWithNoMOV = "Free Delivery on all orders";
    String freeDelWithMOV = "Free Delivery on orders above ₹99";
    String freebieWithNoMOV = "Free delight on all orders";
    String listingFreedel = "Free Delivery";
    String listingPercent = "10% off";
    String listingFreebieWithNoMOV = "[\"Free delight on all orders\"]";
    String listingFreebiePlaceHolderNoMOV = "Free Automation-Default-CopyTrue on all orders";
    String listingFreebiePlaceHolderNoMOVOnlySuper = "Free Automation-Default-CopyTrue only for SUPERs";
    String listingFreebiePlaceHolderNoMOVWithCopyFalse = "Free Automation-Default-CopyFalse on all orders";
    String listingFreebiePlaceHolderWithMOV = "Free Automation-Default-CopyTrue on orders above ₹99";
    String descriptionFreebiePlaceHolderWithNoMOV = "Free Automation-Default-CopyTrue on all orders";
    String descriptionFreebiePlaceHolderWithNoMOVOnlySuper = "Free Automation-Default-CopyTrue orders only for SUPERs";
    String descriptionFreebiePlaceHolderWithNoMOVCopyFalse = "Free Automation-Default-CopyFalse on all orders";
    String descriptionFreebiePlaceHolderWithMOV = "Free Automation-Default-CopyTrue on orders above ₹99";
    String descriptionFreebiePlaceHolderWithMOVCopyFalse = "Free Automation-Default-CopyFalse orders above ₹99";
    String flatDescriptionWithNoMOV = "₹20 off on all orders";
    String flatDescriptionWithMOV = "₹20 off on orders above ₹99";
    String listingFreedelWithFlatShortDescription = "Free Delivery, ₹20 off";
    String listingFlat = " ₹20 off";
    String listingFlatSHortDescriptionWithMOV = "₹20 off on orders above ₹99";
    String listingFlatSHortDescriptionWithNoMOV = "₹20 off on all orders";
    String headerFlatAndMore = "₹20 off & more";


}
