package com.swiggy.api.sf.checkout.constants;

public interface EditTimeConstants {

    String mobile1="8197982351";
    String password1="swiggy123";
    String lat="12.936358";
    String lng="77.61514599999998";
    Integer Quantity=10;
    String REST_ID = "226";
    String mobile="7406734416";//"9886379321";
    String password="rkonowhere";
    String CART_UPDATED_SUCCESSFULLY="CART_UPDATED_SUCCESSFULLY";
    String ERROR_MSG = "Invalid quantity for item";
    int HTTP_CODE=200;
    String STATUS_MESSAGE="done successfully";
    String STATUS_CODE="0";
    String ERROR_CODE="1";
    String CONFIRM_ERROR_MSG="Updated Cart is empty.";
    String restInitSource="1";
    String custInitSource="2";
    String defaultInitSource="0";
    String VERSION_CODE = "300";
    String USER_AGENT = "Swiggy-Android";
    String AUTHORIZATION = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";

    //TD Constants
    String MIN_CART_AMOUNT = "1";
    boolean FIRST_ORDER_RESTRICTION = false;
    boolean RESTAURANT_FIRST_ORDER = false;
    boolean USER_RESTRICTION = false;
    String DORMANT_USER_TYPE = "ZERO_DAYS_DORMANT";

    //Cancel Order Data
    String CANCELLATION_REASON = "Automation Testing";
    String CANCELLATION_FEE_APPLICABILITY = "true";
    String CANCELLATION_FEE = "0";



}