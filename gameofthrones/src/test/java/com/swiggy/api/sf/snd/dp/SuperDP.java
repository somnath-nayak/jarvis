package com.swiggy.api.sf.snd.dp;

import com.swiggy.api.sf.snd.constants.SANDConstants;
import org.testng.annotations.DataProvider;

public class SuperDP {

    @DataProvider(name = "userProfileSuper" )
    public Object[][] userProfileSuper() {
        return new Object[][] {
                {"12.9279", "77.6271", SANDConstants.fal, SANDConstants.fal,SANDConstants.isSuper, SANDConstants.superDetails},
        };
    }

    @DataProvider(name = "userProfileSuperWithoutToken" )
    public Object[][] userProfileSuperWithoutToken() {
        return new Object[][] {
                {SANDConstants.name, SANDConstants.password,"12.9279", "77.6271", SANDConstants.fal, SANDConstants.fal,SANDConstants.isSuper, SANDConstants.superDetails},
        };
    }


    @DataProvider(name = "userProfileNonSuper" )
    public Object[][] userProfileNonSuper() {
        return new Object[][] {
                {SANDConstants.name,SANDConstants.password, SANDConstants.fal, SANDConstants.fal,SANDConstants.isSuper},
        };
    }

    @DataProvider(name = "userProfileWithoutOptional" )
    public Object[][] userProfileWithoutOptional() {
        return new Object[][] {
                {"12.9279", "77.6271", SANDConstants.fal, SANDConstants.fal},
        };
    }

    @DataProvider(name = "userProfileValidNNonValid" )
    public Object[][] userProfileValidNNonValid() {
        return new Object[][] {
                {"12.9279", "77.6271", SANDConstants.fal, SANDConstants.fal, SANDConstants.superKey,SANDConstants.msg, SANDConstants.superDetails},
        };
    }

    @DataProvider(name = "userProfileAdminSuper" )
    public Object[][] userProfileAdminSuper() {
        return new Object[][] {
                {"12.9279", "77.6271",SANDConstants.user_id,SANDConstants.isSuper, SANDConstants.superDetails},
        };
    }

    @DataProvider(name = "userProfileAdmin1Optional" )
    public Object[][] userProfileAdmin1Optional() {
        return new Object[][] {
                {"12.9279", "77.6271",SANDConstants.user_id,SANDConstants.isSuper},
                {"12.9279", "77.6271",SANDConstants.user_id,SANDConstants.superDetails},
        };
    }

    @DataProvider(name = "userProfileAdminSuperWithoutOptional" )
    public Object[][] userProfileAdminSuperWithoutOptional() {
        return new Object[][] {
                {"12.9279", "77.6271",SANDConstants.user_id},
                {"12.9279", "77.6271",SANDConstants.user_id},
                {"12.9279", "77.6271",SANDConstants.user_id},
        };
    }

    @DataProvider(name = "userProfileInternal" )
    public Object[][] userProfileInternal() {
        return new Object[][] {
                {"12.9279", "77.6271",SANDConstants.id, SANDConstants.isSuper, SANDConstants.superDetails},
                {"12.9279", "77.6271",SANDConstants.mobileId,SANDConstants.isSuper, SANDConstants.superDetails},
                {"12.9279", "77.6271",SANDConstants.email,SANDConstants.isSuper, SANDConstants.superDetails}
        };
    }

    @DataProvider(name = "userProfileInternalOptional" )
    public Object[][] userProfileInternalOptional() {
        return new Object[][] {
                {"12.9279", "77.6271",SANDConstants.isSuper, SANDConstants.superDetails},
                {"12.9279", "77.6271",SANDConstants.isSuper, SANDConstants.superDetails},
                { "12.9279", "77.6271",SANDConstants.isSuper, SANDConstants.superDetails}
        };
    }

    @DataProvider(name = "userInternalOptional" )
    public Object[][] userInternalOptional() {
        return new Object[][] {
                {"12.9279", "77.6271",SANDConstants.isSuper, SANDConstants.superDetails}
        };
    }

    @DataProvider(name = "deleteCache" )
    public Object[][] deleteCache() {
        return new Object[][] {
                {"12.9279", "77.6271",SANDConstants.isSuper, SANDConstants.superDetails}
        };
    }

    @DataProvider(name = "addSchema" )
    public Object[][] addSchema() {
        return new Object[][] {
                {SANDConstants.name, SANDConstants.password}
        };
    }

    @DataProvider(name = "latLong1")
    public Object[][] latLong1(){
        return new Object[][] {
                { "12.9279","77.6271","free_delivery" }
        };
    }



    @DataProvider(name = "percentageMenu")
    public Object[][] percentageMenu(){
        return new Object[][] {
                { "12.9326", "77.6036",SANDConstants.freeDel, SANDConstants.percent, SANDConstants.rest }
        };
    }

    @DataProvider(name = "flatMenu")
    public Object[][] flatMenu(){
        return new Object[][] {
                { "12.9279","77.6271",SANDConstants.freeDel, SANDConstants.flat, SANDConstants.rest }
        };
    }

    @DataProvider(name = "freebieMenu")
    public Object[][] freebieMenu(){
        return new Object[][] {
                { "12.9279","77.6271" , SANDConstants.freeDel, SANDConstants.freebie, SANDConstants.rest}
        };
    }

    @DataProvider(name = "percentMenu")
    public Object[][] percentMenu(){
        return new Object[][] {
                { "12.9279","77.6271" , SANDConstants.freeDel, SANDConstants.percent, SANDConstants.rest}
        };
    }

    @DataProvider(name = "freeDelMenu")
    public Object[][] freeDelMenu(){
        return new Object[][] {
                { "12.9166", "77.6101", SANDConstants.freeDel, SANDConstants.superKey, SANDConstants.rest}
        };
    }


    @DataProvider(name = "freeDelAgg")
    public Object[][] freeDelAgg(){
        return new Object[][] {
                { "12.9166", "77.6101", "free_delivery", SANDConstants.freeDel, SANDConstants.superKey}
        };
    }

    @DataProvider(name = "percentageAgg")
    public Object[][] percentageAgg(){
        return new Object[][] {
                { "12.9326", "77.6036", SANDConstants.freeDel, SANDConstants.superKey, SANDConstants.percent}
        };
    }

    @DataProvider(name = "freeAgg")
    public Object[][] freeAgg(){
        return new Object[][] {
                { "12.9279","77.6271", "free_delivery", SANDConstants.freeDel, SANDConstants.superKey}
        };
    }


    @DataProvider(name = "freeBieAgg")
    public Object[][] freeBieAgg(){
        return new Object[][] {
                { "12.9279","77.6271", SANDConstants.freeDel, SANDConstants.superKey, SANDConstants.freebie}
        };
    }

    @DataProvider(name = "percentageCol")
    public Object[][] percentageCol(){
        return new Object[][] {
                { "12.9326", "77.6036", "discount_percent_greater_than"}
        };
    }

    @DataProvider(name = "percentGreaterFirstCol")
    public Object[][] percentGreaterFirstCol(){
        return new Object[][] {
                { "12.9081", "77.6476", "discount_percent_greater_than_first_user"}
        };
    }


    @DataProvider(name = "percentageDiscountLess")
    public Object[][] percentageDiscountLess(){
        return new Object[][] {
                { "12.9166", "77.6101", "discount_percent_less_than"}
        };
    }

    @DataProvider(name = "costForTwo")
    public Object[][] costForTwo(){
        return new Object[][] {
                { "12.9166", "77.6101", "cost_for_two"}
        };
    }

    @DataProvider(name = "percentageColGreaterThan")
    public Object[][] percentageColGreaterThan(){
        return new Object[][] {
                { "12.9326", "77.6036", "discount_percent_greater_than_first_user"}
        };
    }

    @DataProvider(name = "slaCollection")
    public Object[][] slaCollection(){
        return new Object[][] {
                { "12.9166", "77.6101", "delivery_time"}
        };
    }


    @DataProvider(name = "VegOnly")
    public Object[][] VegOnly(){
        return new Object[][] {
                { "12.9279","77.6271", "veg_only"}
        };
    }

    @DataProvider(name = "longDistance")
    public Object[][] longDistance(){
        return new Object[][] {
                { "12.9166", "77.6101", "long_distance"}
        };
    }

    @DataProvider(name = "freebieSearch")
    public Object[][] freebieSearch(){
        return new Object[][] {
                { "12.9279","77.6271", SANDConstants.freeDel, SANDConstants.freebie,SANDConstants.superKey,SANDConstants.rest}
        };
    }

    @DataProvider(name = "percentSearch")
    public Object[][] percentSearch(){
        return new Object[][] {
                { "12.9326", "77.6036", SANDConstants.freeDel, SANDConstants.percent,SANDConstants.superKey,SANDConstants.rest}
        };
    }

    @DataProvider(name = "flatSearch")
    public Object[][] flatSearch(){
        return new Object[][] {
                { "12.9279","77.6271", SANDConstants.freeDel, SANDConstants.flat,SANDConstants.superKey,SANDConstants.rest}
        };
    }

    @DataProvider(name = "freeDelSearch")
    public Object[][] freeDelSearch(){
        return new Object[][] {
                { "12.9166", "77.6101", SANDConstants.freeDel, SANDConstants.freeDel,SANDConstants.superKey,SANDConstants.rest}
        };
    }
}
