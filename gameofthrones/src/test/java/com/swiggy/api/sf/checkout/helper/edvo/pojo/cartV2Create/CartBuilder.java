package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CartBuilder {
    private Cart cart;
    public CartBuilder(){
        cart = new Cart();
    }

    public CartBuilder address(Integer addressId){
            cart.setAddressId(addressId);
            return this;
    }

    public CartBuilder cartItems(List<CartItem> cartItems)
    {
        cart.setCartItems(cartItems);
        return this;
    }

    public CartBuilder mealItems(List<MealItem> mealItems){
        cart.setMealItems(mealItems);
        return this;
    }

    public CartBuilder restaurantId(String restaurantId){
        cart.setRestaurantId(restaurantId);
        return this;
    }
    
    public CartBuilder couponCode(String couponCode){
        cart.setCouponCode(couponCode);
        return this;
    }

    public  CartBuilder cartType(String cartType){
        cart.setCartType(cartType);
        return this;
    }
    
    public CartBuilder subscriptionItems(List<SubscriptionItem> subscriptionItems)
    {
        cart.setSubscriptionItems(subscriptionItems);
        return this;
    }
    
    public CartBuilder preorderSlot(PreorderSlot preorderSlot) {
    	cart.setPreorderSlot(preorderSlot);
    	return this;
    }
    
    public Cart build(){
        return cart;
    }

    private void defaultCart() {
        JsonHelper jh = new JsonHelper();
        try {
            jh.getObjectToJSON(cart);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        if(cart.getAddressId()==null){
//            cart.setAddressId(123);
//        }
        List<CartItem> cartItems = new ArrayList<>();
        CartItem cartItem = new CartItem();
        if(cart.getCartItems()==null) {
            if (cartItem.getAddons() == null) {
                cartItem.setAddons(new ArrayList<Addon>());
            }
            if(cartItem.getVariants()==null){
                cartItem.setVariants(new ArrayList<Variant>());
            }
            if(cartItem.getMenuItemId()==null){
                cartItem.setMenuItemId("123");
            }
            if(cartItem.getQuantity()==null){
                cartItem.setQuantity(1);
            }

        }
    }

    public Cart buildCart(){
        defaultCart();
        return cart;
    }

}
