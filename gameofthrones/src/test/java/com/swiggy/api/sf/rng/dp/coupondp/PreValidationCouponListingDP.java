package com.swiggy.api.sf.rng.dp.coupondp;

import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.MinAmount;
import org.testng.annotations.DataProvider;

import java.util.HashMap;

public class PreValidationCouponListingDP {

    @DataProvider(name = "createCouponWithCartMinValueIsGreaterThanCartValue")
    public static Object[][] createCouponWithCartMinValueIsGreaterThanCartValue() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withDiscountPercentage(0)
                .withDiscountAmount(0)
                .withFreeShipping(0)
                .withMinAmount(new MinAmount().withCart(10000))
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createCouponWithCartMinValueIsLesserThanCartValue")
    public static Object[][] createCouponWithCartMinValueIsLesserThanCartValue() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withDiscountPercentage(0)
                .withDiscountAmount(0)
                .withFreeShipping(0)
                .withMinAmount(new MinAmount().withCart(800))
                .withCouponUserTypeId(0)
                .withPreferredPaymentMethod("AmazonPay,Sodexo")
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }
}
