package com.swiggy.api.sf.rng.pojo;

import org.apache.tomcat.util.buf.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class RestaurantIds {
    
    @JsonProperty("restaurant_ids")
    private String restaurantIds;
    
    public RestaurantIds(){
    
    }
    
    public String createRestaurantList(String[] restaurantIds){
        List<String> allList = new ArrayList<>();
        for (int i = 0; i<restaurantIds.length; i++){
            allList.add(i, restaurantIds[i]);
        }
        return   StringUtils.join(allList, ',');
    }
    
    @Override
    public String toString(){
        return "{" +
                       "\"restaurant_ids\":" + "[" + restaurantIds + "]"
                       
                       +"}";
    }
    
}
