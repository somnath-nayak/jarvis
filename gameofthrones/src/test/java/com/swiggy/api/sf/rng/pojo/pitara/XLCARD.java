
package com.swiggy.api.sf.rng.pojo.pitara;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "count"
})
public class XLCARD {

    @JsonProperty("count")
    private Integer count;

    /**
     * No args constructor for use in serialization
     * 
     */
    public XLCARD() {
    }

    /**
     * 
     * @param count
     */
    public XLCARD(Integer count) {
        super();
        this.count = count;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    public XLCARD withCount(Integer count) {
        this.count = count;
        return this;
    }

}
