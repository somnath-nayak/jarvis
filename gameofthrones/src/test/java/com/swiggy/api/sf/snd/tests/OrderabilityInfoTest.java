package com.swiggy.api.sf.snd.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.ServiceablilityHelper;
import com.swiggy.api.sf.snd.dp.OrderabilityInfoDataProvider;
import com.swiggy.api.sf.snd.helper.OrderabilityInfoHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class OrderabilityInfoTest extends OrderabilityInfoDataProvider {

    String tid ;
    String token;

    OrderabilityInfoHelper orderabilityInfoHelper = new OrderabilityInfoHelper();
    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();
    ServiceablilityHelper serviceablilityHelper = new ServiceablilityHelper();


    @Test(dataProvider = "validLatLong")
    public void verifyOrderabilityInfo(String mobile, String password, String lat, String lng) {

        HashMap<String, String> tidToken = orderabilityInfoHelper.getTidToken(mobile, password);
        tid = tidToken.get("tid");
        token = tidToken.get("token");
        Processor orderabilityInfo = orderabilityInfoHelper.getOrderabilityInfo(tid, token, lat, lng);
        int i = orderabilityInfo.ResponseValidator.GetResponseCode();
        Assert.assertEquals(i,200);
    }

    @Test(dataProvider = "validLatLong")
    public void verifyOrderabilityInfoJsonSchema(String mobile, String password, String lat, String lng) throws IOException, ProcessingException {

        HashMap<String, String> tidToken = orderabilityInfoHelper.getTidToken(mobile, password);
        tid = tidToken.get("tid");
        token = tidToken.get("token");

        Processor processor = orderabilityInfoHelper.getOrderabilityInfo(tid, token, lat,lng);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/orderability.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For Orderability Info API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "invalidLatLong")
    public void verifyOrderabilityInfoForInvalidLatLng(String mobile, String password, String lat, String lng) {

        HashMap<String, String> tidToken = orderabilityInfoHelper.getTidToken(mobile, password);
        tid = tidToken.get("tid");
        token = tidToken.get("token");
        Processor orderabilityInfo = orderabilityInfoHelper.getOrderabilityInfo(tid, token, lat, lng);
        int i = orderabilityInfo.ResponseValidator.GetResponseCode();
        Assert.assertEquals(i,200);

        String s = orderabilityInfo.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cityInfo.bannerMessage");
        Assert.assertEquals(s," Service unavailable");

        boolean s1 = orderabilityInfo.ResponseValidator.GetNodeValueAsBool("$.data.cityInfo.isOpen");
        Assert.assertEquals(s1,false);
    }

    @Test(dataProvider = "CityClosedLatLong")
    public void verifyCityInfoWhileCityIsClosed(String lat, String lng){

        Processor orderabilityInfo = orderabilityInfoHelper.getOrderabilityInfo( lat, lng);
        int i = orderabilityInfo.ResponseValidator.GetResponseCode();
        boolean s1 = orderabilityInfo.ResponseValidator.GetNodeValueAsBool("$.data.cityInfo.isOpen");
        Assert.assertEquals(i,200);
        Assert.assertEquals(s1,false);
    }

    /*
    black zone
     */

    @Test(dataProvider = "BlackZoneLatLong")
    public void verifyOrderabilityInfoForBlackZone( String lat, String lng) {

        Processor orderabilityInfo = orderabilityInfoHelper.getOrderabilityInfo( lat, lng);
        int i = orderabilityInfo.ResponseValidator.GetResponseCode();
        Assert.assertEquals(i,200);

        boolean b = orderabilityInfo.ResponseValidator.DoesNodeExists("$.data..blackZoneInfo.blackZoneTitle");
        boolean b1 = orderabilityInfo.ResponseValidator.DoesNodeExists("$.data.blackZoneInfo.blackZoneMessage");
        Assert.assertEquals(b,true);
        Assert.assertEquals(b1,true);

        int s = orderabilityInfo.ResponseValidator.GetNodeValueAsInt("$.data.orderableInfo.orderableCount");
        int s1 = orderabilityInfo.ResponseValidator.GetNodeValueAsInt("$.data.orderableInfo.totalCount");

        Assert.assertEquals(s,0);
        Assert.assertEquals(s1,0);

    }


    /*
    rain pop is enable
     */

    @Test(dataProvider = "rainPopEnabledLatLong")
    public void verifyOrderabilityInfoForRainPop( String lat, String lng) {

        Processor orderabilityInfo = orderabilityInfoHelper.getOrderabilityInfo( lat, lng);
        int i = orderabilityInfo.ResponseValidator.GetResponseCode();
        Assert.assertEquals(i,200);

        Processor processor = serviceablilityHelper.cerebroListing(lat, lng, "1");
        int s = processor.ResponseValidator.GetNodeValueAsInt("$.type");

        int i1 = orderabilityInfo.ResponseValidator.GetNodeValueAsInt("$.data.popInfo.popType");

        Assert.assertEquals(i1,s);
    }

    /*
    verify the count of orderable restaurants

    TBD
     */

    @Test(dataProvider = "validLatLong2")
    public void verifyOrderabilityInfoForCount( String lat, String lng) {

        Processor orderabilityInfo = orderabilityInfoHelper.getOrderabilityInfo( lat, lng);
        int i = orderabilityInfo.ResponseValidator.GetResponseCode();
        Assert.assertEquals(i,200);

    }


}




