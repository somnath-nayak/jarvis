package com.swiggy.api.sf.rng.pojo.edvo;

import com.swiggy.api.sf.rng.helper.Utility;

import java.util.HashMap;

public class updateCamp {


 private String id = "";
 private String namespace = "Automation";
	private String header = "Test_Header";
	private String description = "Automation_Description";
	private String shortDescription = "Auto_Short_Desc.";
	private String valid_from = Utility.getPresentTimeInMilliSeconds();
	private String valid_till = Utility.getFuturetimeInMilliSeconds(86400000);
	private String campaign_type = "Flat.";
	private double restaurant_hit = 60;
	private double swiggy_hit = 40;
	private boolean enabled = true;
	private String createdBy = "Automation_Ankita";
	private double discountCap = 0;
	private double minCartAmount = 10;
	private String discounts = null;
	private String slots = null;
	private boolean commissionOnFullBill = false;
	private boolean taxesOnDiscountedBill = false;
	private boolean firstOrderRestriction = false;
	private boolean timeSlotRestriction = false;
	private String dormant_user_type = "ZERO_DAYS_DORMANT";
	private boolean restaurantFirstOrder = false;
	private boolean userRestriction = false;

	public updateCamp(){

	}

	public updateCamp(String campaignId , HashMap<String , String > updateCampData){

	this.id = campaignId;
	this.namespace = updateCampData.get("0");
	this.header = updateCampData.get("1");
	this.description = updateCampData.get("2");
	this.shortDescription = updateCampData.get("3");
	this.valid_from = updateCampData.get("4");
	this.valid_till = updateCampData.get("5");
	this.campaign_type = updateCampData.get("6");
	this.restaurant_hit = Double.parseDouble(updateCampData.get("7"));
	this.enabled = Boolean.parseBoolean(updateCampData.get("8"));
	this.swiggy_hit = Double.parseDouble(updateCampData.get("9"));
	this.createdBy = updateCampData.get("10");
	this.discountCap = Double.parseDouble(updateCampData.get("11"));
	this.minCartAmount = Double.parseDouble(updateCampData.get("12"));
	this.discounts = updateCampData.get("13");
	this.slots = updateCampData.get("14");
	this.commissionOnFullBill = Boolean.parseBoolean(updateCampData.get("15"));
	this.taxesOnDiscountedBill = Boolean.parseBoolean(updateCampData.get("16"));
	this.firstOrderRestriction = Boolean.parseBoolean(updateCampData.get("17"));
	this.timeSlotRestriction = Boolean.parseBoolean(updateCampData.get("18"));
	this.dormant_user_type = updateCampData.get("19");
	this.restaurantFirstOrder = Boolean.parseBoolean(updateCampData.get("20"));
	this.userRestriction = Boolean.parseBoolean(updateCampData.get("21"));

	}

	public updateCamp(String campaignId, String header, String valid_till){
		this.id = campaignId;
		this.header = header;
		this.valid_till = valid_till;

	}

	public updateCamp(String campaignId, String header){
		this.id = campaignId;
		this.header = header;
	}

	public updateCamp(String campaignId){
		this.id = campaignId;
	}

	public updateCamp(String campaignId, boolean enabled){
		this.id = campaignId;
		this.enabled = enabled;
	}
 public updateCamp(int campaignId, String discounts){
		this.id = campaign_type.toString();
		this.discounts = discounts;
 }


@Override
	public  String toString() {
	return
			"{"
               + "\"namespace\" : " + namespace + ";"
			   + "\"header\" : " + header + ";"
			   + "\"description\" : " + description + ";"
			   + "\"shortDescription\" : " + shortDescription + ";"
			   + "\"valid_from\" : " + valid_from + ";"
			   + "\"valid_till\" : " + valid_till + ";"
			   + "\"campaign_type\" : " + campaign_type + ";"
			   + "\"restaurant_hit\" : " + restaurant_hit + ";"
			   + "\"enabled\" : " + enabled + ";"
			   + "\"swiggy_hit\" : " + swiggy_hit + ";"
			   + "\"createdBy\" : " + createdBy + ";"
			   + "\"discountCap\""  + discountCap + ";"
			   + "\"minCartAmount\" : " + minCartAmount + ";"
			   + "\"discounts\" : " + discounts + ";"
			   + "\"slots\" : " +  slots + ";"
			   + "\"commissionOnFullBill\" : " + commissionOnFullBill + ";"
			   + "\"taxesOnDiscountedBill\" : " + taxesOnDiscountedBill + ";"
			   + "\"firstOrderRestriction\" : " + firstOrderRestriction + ";"
			   + "\"timeSlotRestriction\" : " + timeSlotRestriction + ";"
			   + "\"dormant_user_type\" : " + dormant_user_type + ";"
			   + "\"restaurantFirstOrder\" : " + restaurantFirstOrder + ";"
			   + "\"userRestriction\" : " +  userRestriction + ";"
			+ "}"
			;
}



//	{
//		"namespace": ${0},
//		"header": ${1},
//		"description": ${2},
//		"shortDescription" : ${3},
//		"valid_from": ${4},
//		"valid_till": ${5},
//		"campaign_type": ${6},
//		"restaurant_hit": ${7},
//		"enabled": ${8},
//		"swiggy_hit": ${9},
//		"createdBy": ${10},
//		"discountCap" : ${11},
//		"minCartAmount":${12},
//		"discounts": [
//		${13}
//  ],
//		"slots": ${14},
//		"commissionOnFullBill": ${15},
//		"taxesOnDiscountedBill": ${16},
//		"firstOrderRestriction": ${17},
//		"timeSlotRestriction":${18},
//		"dormant_user_type" : "${19}",
//			"restaurantFirstOrder" :${20},
//		"userRestriction": ${21}
	//}

}