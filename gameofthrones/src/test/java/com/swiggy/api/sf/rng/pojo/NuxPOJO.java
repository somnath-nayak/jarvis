package com.swiggy.api.sf.rng.pojo;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "userId",
        "cityId",
        "codes"
})
public class NuxPOJO {

    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("cityId")
    private Integer cityId;
    @JsonProperty("codes")
    private List<String> codes;

    /**
     * No args constructor for use in serialization
     *
     */
    public NuxPOJO() {
    }

    /**
     *
     * @param cityId
     * @param userId
     * @param codes
     */
    public NuxPOJO(Integer userId, Integer cityId, List<String> codes) {
        super();
        this.userId = userId;
        this.cityId = cityId;
        this.codes = codes;
    }

    @JsonProperty("userId")
    public Integer getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @JsonProperty("cityId")
    public Integer getCityId() {
        return cityId;
    }

    @JsonProperty("cityId")
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    @JsonProperty("codes")
    public List<String> getCodes() {
        return codes;
    }

    @JsonProperty("codes")
    public void setCodes(List<String> codes) {
        this.codes = codes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("cityId", cityId).append("codes", codes).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(cityId).append(userId).append(codes).toHashCode();
    }

    public NuxPOJO withUserId(int userId) {
         this.userId = userId;
        return this;
    }

    public NuxPOJO withCityId(int cityId) {
        this.cityId = cityId;
        return this;
    }
    public NuxPOJO withCodes(List<String> codes) {
        this.codes = codes;
        return this;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof NuxPOJO) == false) {
            return false;
        }
        NuxPOJO rhs = ((NuxPOJO) other);
        return new EqualsBuilder().append(cityId, rhs.cityId).append(userId, rhs.userId).append(codes, rhs.codes).isEquals();
    }

    public NuxPOJO setDefaultData(int userId,int cityId,List<String>  codes) {

        return  this.withUserId(userId)
                .withCityId(cityId)
                .withCodes(codes);
    }

}