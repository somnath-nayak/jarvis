package com.swiggy.api.sf.rng.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class CreateFlatTdEntry {

    @JsonProperty("namespace")
    private String namespace;
    @JsonProperty("header")
    private String header;
    @JsonProperty("description")
    private String description;
    @JsonProperty("valid_from")
    private String validFrom;
    @JsonProperty("valid_till")
    private String validTill;
    @JsonProperty("campaign_type")
    private String campaignType;
    @JsonProperty("restaurant_hit")
    private String restaurantHit;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("swiggy_hit")
    private String swiggyHit;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("discountLevel")
    private String discountLevel;
    @JsonProperty("restaurantList")
    private List<RestaurantList> restaurantList = null;
    @JsonProperty("ruleDiscount")
    private FlatRuleDiscount flatRuleDiscount;
    @JsonProperty("slots")
    private List<Slot> slots = null;
    @JsonProperty("commissionOnFullBill")
    private Boolean commissionOnFullBill;
    @JsonProperty("taxesOnDiscountedBill")
    private Boolean taxesOnDiscountedBill;
    @JsonProperty("firstOrderRestriction")
    private Boolean firstOrderRestriction;
    @JsonProperty("timeSlotRestriction")
    private Boolean timeSlotRestriction;
    @JsonProperty("userRestriction")
    private Boolean userRestriction;
    @JsonProperty("dormant_user_type")
    private String dormantUserType;
    @JsonProperty("restaurantFirstOrder")
    private Boolean restaurantFirstOrder;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("copyOverridden")
    private Boolean copyOverridden;
    @JsonProperty("isSuper")
    private Boolean isSuper;
    @JsonProperty("levelPlaceHolder")
    private String levelPlaceHolder = "Default Flat-TD place holder";
    @JsonProperty("shortDescription")
    private String shortDescription;

    /**
     * No args constructor for use in serialization
     */
    public CreateFlatTdEntry() {
    }

    /**
     * @param ruleDiscount
     * @param timeSlotRestriction
     * @param enabled
     * @param dormantUserType
     * @param campaignType
     * @param restaurantList
     * @param commissionOnFullBill
     * @param userRestriction
     * @param swiggyHit
     * @param restaurantHit
     * @param validTill
     * @param header
     * @param namespace
     * @param firstOrderRestriction
     * @param createdBy
     * @param slots
     * @param description
     * @param validFrom
     * @param discountLevel
     * @param taxesOnDiscountedBill
     */
    public CreateFlatTdEntry(String namespace, String header, String description, String validFrom, String validTill, String campaignType, String restaurantHit, Boolean enabled, String swiggyHit, String createdBy, String discountLevel, List<RestaurantList> restaurantList, RuleDiscount ruleDiscount, List<Slot> slots, Boolean commissionOnFullBill, Boolean taxesOnDiscountedBill, Boolean firstOrderRestriction, Boolean timeSlotRestriction, Boolean userRestriction, String dormantUserType) {
        super();
        this.namespace = namespace;
        this.header = header;
        this.description = description;
        this.validFrom = validFrom;
        this.validTill = validTill;
        this.campaignType = campaignType;
        this.restaurantHit = restaurantHit;
        this.enabled = enabled;
        this.swiggyHit = swiggyHit;
        this.createdBy = createdBy;
        this.discountLevel = discountLevel;
        this.restaurantList = restaurantList;
        this.flatRuleDiscount = flatRuleDiscount;
        this.slots = slots;
        this.commissionOnFullBill = commissionOnFullBill;
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        this.firstOrderRestriction = firstOrderRestriction;
        this.timeSlotRestriction = timeSlotRestriction;
        this.userRestriction = userRestriction;
        this.dormantUserType = dormantUserType;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public String getRestaurantHit() {
        return restaurantHit;
    }

    public void setRestaurantHit(String restaurantHit) {
        this.restaurantHit = restaurantHit;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getSwiggyHit() {
        return swiggyHit;
    }

    public void setSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDiscountLevel() {
        return discountLevel;
    }

    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    public List<RestaurantList> getRestaurantList() {
        return restaurantList;
    }

    public void setRestaurantList(List<RestaurantList> restaurantList) {
        this.restaurantList = restaurantList;
    }

    public FlatRuleDiscount getRuleDiscount() {
        return flatRuleDiscount;
    }

    public void setFlatRuleDiscount(FlatRuleDiscount flatRuleDiscount) {
        this.flatRuleDiscount = flatRuleDiscount;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    public void setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
    }

    public Boolean getTaxesOnDiscountedBill() {
        return taxesOnDiscountedBill;
    }

    public void setTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
    }

    public Boolean getFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    public void setFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
    }

    public Boolean getTimeSlotRestriction() {
        return timeSlotRestriction;
    }

    public void setTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
    }

    public Boolean getUserRestriction() {
        return userRestriction;
    }

    public void setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
    }

    public String getDormantUserType() {
        return dormantUserType;
    }

    public void setDormantUserType(String dormantUserType) {
        this.dormantUserType = dormantUserType;
    }

    public Boolean getRestaurantFirstOrder() {
        return restaurantFirstOrder;
    }

    public void setRestaurantFirstOrder(Boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
    }

    public Boolean getCopyOverridden() {
        return copyOverridden;
    }

    public void setCopyOverridden(Boolean copyOverridden) {
        this.copyOverridden = copyOverridden;
    }

    public Boolean getSuper() {
        return isSuper;
    }

    public void setSuper(Boolean aSuper) {
        isSuper = aSuper;
    }

    public String getLevelPlaceHolder() {
        return levelPlaceHolder;
    }

    public void setLevelPlaceHolder(String levelPlaceHolder) {
        this.levelPlaceHolder = levelPlaceHolder;
    }
    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("namespace", namespace).append("header", header).append("description", description).append("validFrom", validFrom).append("validTill", validTill).append("campaignType", campaignType).append("restaurantHit", restaurantHit).append("enabled", enabled).append("swiggyHit", swiggyHit).append("createdBy", createdBy).append("discountLevel", discountLevel).append("restaurantList", restaurantList).append("flatRuleDiscount", flatRuleDiscount).append("slots", slots).append("commissionOnFullBill", commissionOnFullBill).append("taxesOnDiscountedBill", taxesOnDiscountedBill).append("firstOrderRestriction", firstOrderRestriction).append("timeSlotRestriction", timeSlotRestriction).append("userRestriction", userRestriction).append("dormantUserType", dormantUserType).append("restaurantFirstOrder", restaurantFirstOrder).toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

}