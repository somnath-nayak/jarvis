package com.swiggy.api.sf.rng.pojo.carousel;

import com.swiggy.api.sf.rng.helper.RngHelper;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class CarouselPOJO {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public CarouselPOJO withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("bannerName")
    private String bannerName;
    @JsonProperty("startDateTime")
    private String startDateTime;
    @JsonProperty("endDateTime")
    private String endDateTime;
    @JsonProperty("priority")
    private String priority;
    @JsonProperty("channel")
    private String channel;
    @JsonProperty("metatypes")
    private String metatypes;
    @JsonProperty("type")
    private String type;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("timeSlots")
    private List<TimeSlot> timeSlots;
    @JsonProperty("restaurantVisibility")
    private Boolean restaurantVisibility;
    @JsonProperty("userRestriction")
    private Boolean userRestriction;
    @JsonProperty("cityId")
    private String cityId;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("addPolygonIds")
    private List<Integer> addPolygonIds = null;
    @JsonProperty("disablePolygons")
    private List<Object> disablePolygons = null;
    @JsonProperty("creativeId")
    private String creativeId;
    @JsonProperty("redirectLink")
    private String redirectLink;
    @JsonProperty("carouselTradeDiscountInfoSet")
    private List<CarouselTradeDiscountInfoSet> carouselTradeDiscountInfoSet = null;
    @JsonProperty("superType")
    private String superType;
    @JsonProperty("bgColor")
    private String bgColor;
    @JsonProperty("gradientStart")
    private String gradientStart;
    @JsonProperty("gradientEnd")
    private String gradientEnd;
    @JsonProperty("fontColor")
    private String fontColor;
    @JsonProperty("restaurantId")
    private Integer restaurantId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("creativeThumbnail")
    private String creativeThumbnail;
    @JsonProperty("superType")
    public String getSuperType() {
        return superType;
    }
    @JsonProperty("superType")
    public void setSuperType(String superType) {
        this.superType = superType;
    }

// super user : "SUPER"
// expired super user : WAS_SUPER
// user eligible for super "NOT_SUPER"
//    super type : NULL for all

    @JsonProperty("bannerName")
    public String getBannerName() {
        return bannerName;
    }

    @JsonProperty("bannerName")
    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    public CarouselPOJO withBannerName(String bannerName) {
        this.bannerName = bannerName;
        return this;
    }

    @JsonProperty("startDateTime")
    public String getStartDateTime() {
        return startDateTime;
    }

    @JsonProperty("startDateTime")
    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public CarouselPOJO withRestaurantId(Integer restaurantId){
        this.restaurantId = restaurantId;
        return this;
    }
    public CarouselPOJO withStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
        return this;
    }

    @JsonProperty("endDateTime")
    public String getEndDateTime() {
        return endDateTime;
    }

    @JsonProperty("endDateTime")
    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public CarouselPOJO withEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
        return this;
    }

    @JsonProperty("priority")
    public String getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(String priority) {
        this.priority = priority;
    }

    public CarouselPOJO withPriority(String priority) {
        this.priority = priority;
        return this;
    }

    public CarouselPOJO withDescription(String description){
        this.description = description;
        return this;
    }

    public CarouselPOJO withCreativeThumbnail(String creativeThumbnail){
        this.creativeThumbnail = creativeThumbnail;
        return this;
    }

    @JsonProperty("channel")
    public String getChannel() {
        return channel;
    }

    @JsonProperty("channel")
    public void setChannel(String channel) {
        this.channel = channel;
    }

    public CarouselPOJO withChannel(String channel) {
        this.channel = channel;
        return this;
    }

    @JsonProperty("metatypes")
    public String getMetatypes() {
        return metatypes;
    }

    @JsonProperty("metatypes")
    public void setMetatypes(String metatypes) {
        this.metatypes = metatypes;
    }

    public CarouselPOJO withMetatypes(String metatypes) {
        this.metatypes = metatypes;
        return this;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public CarouselPOJO withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public CarouselPOJO withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("timeSlots")
    public List<TimeSlot> getTimeSlots() {
        return timeSlots;
    }

    @JsonProperty("timeSlots")
    public void setTimeSlots(List<TimeSlot> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public CarouselPOJO withTimeSlots(List<TimeSlot> timeSlots) {
        this.timeSlots = timeSlots;
        return this;
    }

    @JsonProperty("restaurantVisibility")
    public Boolean getRestaurantVisibility() {
        return restaurantVisibility;
    }

    @JsonProperty("restaurantVisibility")
    public void setRestaurantVisibility(Boolean restaurantVisibility) {
        this.restaurantVisibility = restaurantVisibility;
    }

    public CarouselPOJO withRestaurantVisibility(Boolean restaurantVisibility) {
        this.restaurantVisibility = restaurantVisibility;
        return this;
    }

    @JsonProperty("userRestriction")
    public Boolean getUserRestriction() {
        return userRestriction;
    }

    @JsonProperty("userRestriction")
    public void setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
    }

    public CarouselPOJO withUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
        return this;
    }

    @JsonProperty("cityId")
    public String getCityId() {
        return cityId;
    }

    @JsonProperty("cityId")
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public CarouselPOJO withCityId(String cityId) {
        this.cityId = cityId;
        return this;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public CarouselPOJO withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    @JsonProperty("addPolygonIds")
    public List<Integer> getAddPolygonIds() {
        return addPolygonIds;
    }

    @JsonProperty("addPolygonIds")
    public void setAddPolygonIds(List<Integer> addPolygonIds) {
        this.addPolygonIds = addPolygonIds;
    }

    public CarouselPOJO withAddPolygonIds(List<Integer> addPolygonIds) {
        this.addPolygonIds = addPolygonIds;
        return this;
    }

    @JsonProperty("disablePolygons")
    public List<Object> getDisablePolygons() {
        return disablePolygons;
    }

    @JsonProperty("disablePolygons")
    public void setDisablePolygons(List<Object> disablePolygons) {
        this.disablePolygons = disablePolygons;
    }

    public CarouselPOJO withDisablePolygons(List<Object> disablePolygons) {
        this.disablePolygons = disablePolygons;
        return this;
    }

    @JsonProperty("creativeId")
    public String getCreativeId() {
        return creativeId;
    }

    @JsonProperty("creativeId")
    public void setCreativeId(String creativeId) {
        this.creativeId = creativeId;
    }

    public CarouselPOJO withCreativeId(String creativeId) {
        this.creativeId = creativeId;
        return this;
    }

    @JsonProperty("redirectLink")
    public String getRedirectLink() {
        return redirectLink;
    }

    @JsonProperty("redirectLink")
    public void setRedirectLink(String redirectLink) {
        this.redirectLink = redirectLink;
    }

    public CarouselPOJO withRedirectLink(String redirectLink) {
        this.redirectLink = redirectLink;
        return this;
    }

    @JsonProperty("carouselTradeDiscountInfoSet")
    public List<CarouselTradeDiscountInfoSet> getCarouselTradeDiscountInfoSet() {
        return carouselTradeDiscountInfoSet;
    }

    @JsonProperty("carouselTradeDiscountInfoSet")
    public void setCarouselTradeDiscountInfoSet(List<CarouselTradeDiscountInfoSet> carouselTradeDiscountInfoSet) {
        this.carouselTradeDiscountInfoSet = carouselTradeDiscountInfoSet;
    }

    public CarouselPOJO withCarouselTradeDiscountInfoSet(List<CarouselTradeDiscountInfoSet> carouselTradeDiscountInfoSet) {
        this.carouselTradeDiscountInfoSet = carouselTradeDiscountInfoSet;
        return this;
    }

    @JsonProperty("bgColor")
    public String getBgColor() {
        return bgColor;
    }

    @JsonProperty("bgColor")
    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public CarouselPOJO withBgColor(String bgColor) {
        this.bgColor = bgColor;
        return this;
    }

    @JsonProperty("gradientStart")
    public String getGradientStart() {
        return gradientStart;
    }

    @JsonProperty("gradientStart")
    public void setGradientStart(String gradientStart) {
        this.gradientStart = gradientStart;
    }

    public CarouselPOJO withGradientStart(String gradientStart) {
        this.gradientStart = gradientStart;
        return this;
    }

    @JsonProperty("gradientEnd")
    public String getGradientEnd() {
        return gradientEnd;
    }

    @JsonProperty("gradientEnd")
    public void setGradientEnd(String gradientEnd) {
        this.gradientEnd = gradientEnd;
    }

    public CarouselPOJO withGradientEnd(String gradientEnd) {
        this.gradientEnd = gradientEnd;
        return this;
    }

    @JsonProperty("fontColor")
    public String getFontColor() {
        return fontColor;
    }

    @JsonProperty("fontColor")
    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public CarouselPOJO withFontColor(String fontColor) {
        this.fontColor = fontColor;
        return this;
    }

    public CarouselPOJO setDefault(){
        return this.withBannerName("AutomationTesting")
                .withPriority("22")
                .withChannel("All")
                .withStartDateTime(RngHelper.getCurrentDate("yyyy-MM-dd HH:mm:ss"))
                .withEndDateTime(RngHelper.getFutureDate("yyyy-MM-dd HH:mm:ss"))
                .withMetatypes("Legacy Carousels")
                .withType("restaurant")
                .withEnabled(true)
                .withRestaurantVisibility(true)
                .withUserRestriction(false)
                .withCityId("1")
                .withCreatedBy("dev")
                .withCreativeId("mhe1e5qqpogynjltjjjc")
                .withTimeSlots(getAllDaysTimeSlot());
    }

    public CarouselPOJO setDefaultCarouselData(){
        return this.withBannerName("TestCarousel")
                .withPriority("22")
                .withChannel("All")
                .withStartDateTime(RngHelper.getCurrentDate("yyyy-MM-dd HH:mm:ss"))
                .withEndDateTime(RngHelper.getFutureDate("yyyy-MM-dd HH:mm:ss"))
                .withMetatypes("TopCarousel")
                .withType("item")
                .withEnabled(true)
                .withRestaurantVisibility(true)
                .withUserRestriction(false)
                .withCityId("1")
                .withCreatedBy("Srishty")
                .withCreativeId("mhe1e5qqpogynjltjjjc")
                .withDescription("test Carousel")
                .withFontColor("#ffffff")
                .withTimeSlots(getAllDaysTimeSlot());
    }

    public CarouselPOJO setColor(){
        return this.withBgColor("#121211")
                .withGradientStart("#121211")
                .withGradientEnd("#121211")
                .withFontColor("#121211")
                .withDescription("Test Carousel")
                .withCreativeThumbnail("mhe1e5qqpogynjltjjjc");

    }

    @JsonIgnore
    public List<TimeSlot> getAllDaysTimeSlot(){
        timeSlots = new ArrayList<>();
        TimeSlot timeSlot ;
        String[] days = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
        for(String day: days) {
            timeSlot = new TimeSlot();
            timeSlots.add(timeSlot.withCloseTime(2359).withStartTime(0).withDay(day));
        }
        return timeSlots;
    }


    public CarouselPOJO withCarouselTradeDiscountInfoSet(String type){
        List<CarouselTradeDiscountInfoSet> carouselTradeDiscountInfoSet = new ArrayList<>();
        CarouselTradeDiscountInfoSet carouselTradeDiscountInfoSet1 = new CarouselTradeDiscountInfoSet();
        carouselTradeDiscountInfoSet1.setDiscountType(type);
        carouselTradeDiscountInfoSet.add(carouselTradeDiscountInfoSet1);
        this.carouselTradeDiscountInfoSet = carouselTradeDiscountInfoSet;
        return this;
    }

    public CarouselPOJO withCarouselTradeDiscountInfoSet(String type, List<String> campaignID){
        List<CarouselTradeDiscountInfoSet> carouselTradeDiscountInfoSet = new ArrayList<>();

            CarouselTradeDiscountInfoSet carouselTradeDiscountInfoSet1 = new CarouselTradeDiscountInfoSet();
            carouselTradeDiscountInfoSet1.setDiscountType(type);
            carouselTradeDiscountInfoSet1.setTradeDiscountIds(campaignID);
            carouselTradeDiscountInfoSet.add(carouselTradeDiscountInfoSet1);

        this.carouselTradeDiscountInfoSet = carouselTradeDiscountInfoSet;
        return this;
    }
}