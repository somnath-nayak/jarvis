package com.swiggy.api.sf.rng.tests.coupon;

import com.ctc.wstx.msv.RelaxNGSchema;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.dp.coupondp.CouponSuperDP;
import com.swiggy.api.sf.rng.helper.*;
import com.swiggy.api.sf.rng.pojo.*;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.DBHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

public class CouponUPIHandle extends DP {

    HashMap<String, String> requestHeaders = new HashMap<String, String>();
    GameOfThronesService service;
    Processor processor;
    RngHelper rngHelper = new RngHelper();
    DBHelper dbHelper = new DBHelper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();

    UpiPaymentHelper helper = new UpiPaymentHelper();

    //===========================================Map coupon - UPI Handle================================================

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Create and Map coupon for public user type with payment Code okicici and type UPI_HANDLE")
    public void mapCouponForUserTypePublicOKICICTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code, RngConstants.okicici, RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode,RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);
        Assert.assertNotEquals(paymentCode, RngConstants.okhdfc);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePrivate", dataProviderClass = CouponSuperDP.class, description = "Map coupon for private user type with payment Code okicici and type UPI_HANDLE")
    public void mapCouponForUserTypePrivateOKICICITest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);
        Assert.assertNotEquals(paymentCode, RngConstants.okhdfc);
    }

    @Test(dataProvider = "UPICouponMapAndDeleteSuper", dataProviderClass = CouponSuperDP.class, description = "Map coupon for super user type with payment Code okicici and type UPI_HANDLE")
    public void mapCouponForUserTypeSuperOKICICITest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);
        Assert.assertNotEquals(paymentCode, RngConstants.okhdfc);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon for public user type with payment Code okhdfc and type UPI_HANDLE")
    public void mapCouponForPaymentCodePublicOKHDFCTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePrivate", dataProviderClass = CouponSuperDP.class, description = "Map coupon for private user type with payment Code okhdfc and type UPI_HANDLE")
    public void mapCouponForPaymentCodePrivateOKHDFCTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);

    }

    @Test(dataProvider = "UPICouponMapAndDeleteSuper", dataProviderClass = CouponSuperDP.class, description = "Map coupon for super user type with payment Code okhdfc and type UPI_HANDLE")
    public void mapCouponForPaymentCodeSuperOKHDFCTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon for public user type with payment Code Tez and type UPI_BRAND")
    public void mapCouponForPaymentCodeTEZTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.Tez,RngConstants.UPI_BRAND);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.Tez);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_BRAND);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePrivate", dataProviderClass = CouponSuperDP.class, description = "Map coupon for private user type  with payment Code Tez and type UPI_BRAND")
    public void mapCouponForPaymentCodePrivateUserTEZTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.Tez,RngConstants.UPI_BRAND);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.Tez);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_BRAND);

    }

    @Test(dataProvider = "UPICouponMapAndDeleteSuper", dataProviderClass = CouponSuperDP.class, description = "Map coupon for super user type with payment Code Tez and type UPI_BRAND")
    public void mapCouponForPaymentCodeSuperUserTEZTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.Tez,RngConstants.UPI_BRAND);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.Tez);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_BRAND);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon for public user type with payment Code Bhim and type UPI_BRAND")
    public void mapCouponForPaymentCodeBHIMTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.Bhim,RngConstants.UPI_BRAND);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.Bhim);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_BRAND);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePrivate", dataProviderClass = CouponSuperDP.class, description = "Map coupon for private user type with payment Code Bhim and type UPI_BRAND")
    public void mapCouponForPaymentCodePrivateUserBHIMTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.Bhim,RngConstants.UPI_BRAND);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.Bhim);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_BRAND);
    }

    @Test(dataProvider = "UPICouponMapAndDeleteSuper", dataProviderClass = CouponSuperDP.class, description = "Map coupon for super user type with payment Code Bhim and type UPI_BRAND")
    public void mapCouponForPaymentCodeSuperUserBHIMTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.Bhim,RngConstants.UPI_BRAND);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.Bhim);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_BRAND);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon for public user type with payment Code okicici and type UPI_VPA")
    public void mapCouponForPaymentCodeUPI_VPATest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);
        Assert.assertNotEquals(paymentCode, RngConstants.okhdfc);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePrivate", dataProviderClass = CouponSuperDP.class, description = "Map coupon for private user type with payment Code okicici and type UPI_VPA")
    public void mapCouponForPaymentCodePrivateUserUPI_VPATest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);
        Assert.assertNotEquals(paymentCode, RngConstants.okhdfc);

    }

    @Test(dataProvider = "UPICouponMapAndDeleteSuper", dataProviderClass = CouponSuperDP.class, description = "Map coupon for super user type with payment Code okicici and type UPI_VPA")
    public void mapCouponForPaymentCodeSuperUserUPI_VPATest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);
        Assert.assertNotEquals(paymentCode, RngConstants.okhdfc);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code as okhdfc and type as UPI_VPA FOR public user")
    public void mapCouponForPaymentCodeUPIVPAhdfcTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePrivate", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code as okhdfc and type as UPI_VPA FOR private user")
    public void mapCouponForPaymentCodePrivateUserUPIVPAhdfcTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);

    }

    @Test(dataProvider = "UPICouponMapAndDeleteSuper", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code as okhdfc and type as UPI_VPA FOR super user")
    public void mapCouponForPaymentCodeSuperUserUPIVPAhdfcTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code 123456 and  type BANK_DISCOUNT for Public user")
    public void mapCouponBANK_DISCOUNTPublicTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode123456,RngConstants.BANK_DISCOUNT);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.paymentCode123456);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.BANK_DISCOUNT);

        Processor updateResponse01 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode678901,RngConstants.BANK_DISCOUNT);
        String paymentCode01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode01,couponCode01,type01);

        Assert.assertEquals(paymentCode01, RngConstants.paymentCode678901);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode01, code);
        Assert.assertEquals(type01, RngConstants.BANK_DISCOUNT);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePrivate", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code 123456 and type BANK_DISCOUNT for Private user")
    public void mapCouponBANK_DISCOUNTPrivateTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode123456,RngConstants.BANK_DISCOUNT);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.paymentCode123456);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.BANK_DISCOUNT);

        Processor updateResponse01 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode678901,RngConstants.BANK_DISCOUNT);
        String paymentCode01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode01,couponCode01,type01);

        Assert.assertEquals(paymentCode01, RngConstants.paymentCode678901);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode01, code);
        Assert.assertEquals(type01, RngConstants.BANK_DISCOUNT);

    }

    @Test(dataProvider = "UPICouponMapAndDeleteSuper", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code 123456 and  type BANK_DISCOUNT for Super user")
    public void mapCouponBANK_DISCOUNTSuperTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode123456,RngConstants.BANK_DISCOUNT);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.paymentCode123456);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.BANK_DISCOUNT);

        Processor updateResponse01 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode678901,RngConstants.BANK_DISCOUNT);
        String paymentCode01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode01,couponCode01,type01);

        Assert.assertEquals(paymentCode01, RngConstants.paymentCode678901);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode01, code);
        Assert.assertEquals(type01, RngConstants.BANK_DISCOUNT);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Muliple coupon validation")
    public void mapCouponMulitpleMapTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);

        Processor updateResponse01 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_HANDLE);
        String paymentCode01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode01 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode01,couponCode01,type01);

        Assert.assertEquals(paymentCode01, RngConstants.okhdfc);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode01, code);
        Assert.assertEquals(type01, RngConstants.UPI_HANDLE);

        Processor updateResponse02 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.Tez,RngConstants.UPI_HANDLE);
        String paymentCode02 = String.valueOf(updateResponse02.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type02 = String.valueOf(updateResponse02.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode02 = String.valueOf(updateResponse02.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode02,couponCode02,type02);

        Assert.assertEquals(paymentCode02, RngConstants.Tez);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode02, code);
        Assert.assertEquals(type02, RngConstants.UPI_HANDLE);

        Processor updateResponse03 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode678901,RngConstants.UPI_HANDLE);
        String paymentCode03 = String.valueOf(updateResponse03.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type03 = String.valueOf(updateResponse03.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode03 = String.valueOf(updateResponse03.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode03,couponCode03,type03);

        Assert.assertEquals(paymentCode03, RngConstants.paymentCode678901 );
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode03, code);
        Assert.assertEquals(type03, RngConstants.UPI_HANDLE);

        Processor updateResponse04 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode123456,RngConstants.UPI_HANDLE);
        String paymentCode04 = String.valueOf(updateResponse04.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type04 = String.valueOf(updateResponse04.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode04 = String.valueOf(updateResponse04.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode04,couponCode04,type04);

        Assert.assertEquals(paymentCode04, RngConstants.paymentCode123456);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode04, code);
        Assert.assertEquals(type04, RngConstants.UPI_HANDLE);

        Processor updateResponse05 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.Tez,RngConstants.UPI_BRAND);
        String paymentCode05 = String.valueOf(updateResponse05.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type05 = String.valueOf(updateResponse05.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode05 = String.valueOf(updateResponse05.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode05,couponCode05,type05);

        Assert.assertEquals(paymentCode05, RngConstants.Tez);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode05, code);
        Assert.assertEquals(type05, RngConstants.UPI_BRAND);

        Processor updateResponse06 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String paymentCode06 = String.valueOf(updateResponse06.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type06 = String.valueOf(updateResponse06.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode06 = String.valueOf(updateResponse06.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode06,couponCode06,type06);

        Assert.assertEquals(paymentCode06, RngConstants.okhdfc);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode06, code);
        Assert.assertEquals(type06, RngConstants.UPI_VPA);

        Processor updateResponse07 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_VPA);
        String paymentCode07 = String.valueOf(updateResponse07.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type07 = String.valueOf(updateResponse07.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode07 = String.valueOf(updateResponse07.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode07,couponCode07,type07);

        Assert.assertEquals(paymentCode07, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode07, code);
        Assert.assertEquals(type07, RngConstants.UPI_VPA);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "DB validations")
    public void dbDetailsForUPIHandleTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));

        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);

        String DBPaymentType = rngHelper.selectPaymentCode(couponCode);
        Assert.assertEquals(paymentCode,DBPaymentType);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Duplicate Coupon used for Public user")
    public void duplicateCouponPublicMapForUPIHandleTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));

        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);

        Processor updateResponse01 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String statusMessage = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));

        Assert.assertEquals(statusMessage, "Duplicate entry already present");

    }

    @Test(dataProvider = "UPICouponMapAndDeletePrivate", dataProviderClass = CouponSuperDP.class, description = "Duplicate Coupon used for Private user")
    public void duplicateCouponPrivateMapForUPIHandleTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));

        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);

        Processor updateResponse01 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String statusMessage = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));

        Assert.assertEquals(statusMessage, "Duplicate entry already present");

    }

    @Test(dataProvider = "UPICouponMapAndDeleteSuper", dataProviderClass = CouponSuperDP.class, description = "Duplicate Coupon used for Super user")
    public void duplicateCouponSuperMapForUPIHandleTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));

        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okhdfc);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_VPA);

        Processor updateResponse01 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okhdfc,RngConstants.UPI_VPA);
        String statusMessage = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));

        Assert.assertEquals(statusMessage, "Duplicate entry already present");

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code as 1223")
    public void mapCouponForUserTypePublicTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode1223,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.paymentCode1223);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);
        Assert.assertNotEquals(paymentCode, RngConstants.okhdfc);

        Processor updateResponse02 = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode1223,RngConstants.UPI_VPA);
        String paymentCode02 = String.valueOf(updateResponse02.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type02 = String.valueOf(updateResponse02.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode02 = String.valueOf(updateResponse02.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode02, RngConstants.paymentCode1223);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode02, code);
        Assert.assertEquals(type02, RngConstants.UPI_VPA);
        Assert.assertNotEquals(paymentCode02, RngConstants.okhdfc);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Payment code length validation from DB")
    public void paymentCodeLengthCheckTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor = rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======" + code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO, code, RngConstants.okhdfc, RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode, couponCode, type);

        String DBPaymentType = rngHelper.selectPaymentCode(couponCode);
        Assert.assertEquals(paymentCode.length(),DBPaymentType.length());

        Assert.assertNotEquals(DBPaymentType.length(),7);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "valid_brands validation from wp_options table")
    public void paymentCodeWpOptionsTableTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor = rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======" + code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO, code, RngConstants.Bhim, RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode, couponCode, type);

        String DBPaymentType = rngHelper.selectBrandForUPIHandleFromWPOptionsTable();
        System.out.println("=================" +DBPaymentType);
        Assert.assertEquals(DBPaymentType, RngConstants.Bhim);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Payment Code null validation")
    public void paymentCodeLNullTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor = rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======" + code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO, code, "", RngConstants.UPI_HANDLE);
        String statusMessage = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));

        Assert.assertEquals(statusMessage,"paymentCode is Invalid");

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Payment Code invalid validation")
    public void paymentCodeLInvalidTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor = rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======" + code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO, code, "", RngConstants.UPI_HANDLE);
        String statusMessage = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));

        Assert.assertEquals(statusMessage,"paymentCode is Invalid");

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Payment Code empty validation")
    public void paymentCodeBANK_DISCOUNTTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor = rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======" + code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO, code, RngConstants.okhdfc, RngConstants.BANK_DISCOUNT);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode, couponCode, type);

        String DBPaymentType = rngHelper.selectPaymentCode(couponCode);
        Assert.assertEquals(paymentCode.length(),6);

//        String DBPayment= rngHelper.selectDetailsForUPIHandleFromPaymentCodeMappingAudTable(couponCode);
//        Assert.assertEquals(paymentCode.length(), 6);
        Assert.assertEquals(DBPaymentType.length(), 6);

    }

    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code as 559404 and type as BANK_DISCOUNT FOR public user")
    public void mapCouponPaymentCodeAndTypeTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.paymentCode559404,RngConstants.BANK_DISCOUNT);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.paymentCode559404);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.BANK_DISCOUNT);

    }

//    ==================================================== Apply Coupon - UPI Handle ======================================================

    @Test(dataProvider = "UPIApplyPublic", dataProviderClass = CouponSuperDP.class, description = "Create,map and apply coupon with payment code as okicici and type as UPI_VPA FOR public user with handle mapped to coupon")
    public void applyCouponWithHandleMappedTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO, CouponApplyPOJO couponApplyPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);

        Processor updateResponse01 = helper.applyCouponForUsers(couponApplyPOJO, code, 1);
        int statusCode01 = updateResponse01.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode01, 1);

    }

    @Test(dataProvider = "UPIApplyPublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code as okicici and type as UPI_VPA FOR public user with handle not mapped to coupon")
    public void applyCouponWithHandleNotMappedTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO,CouponApplyPOJO couponApplyPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);

        Processor updateResponse01 = helper.applyCouponForUsers(couponApplyPOJO, code, 1);
        int statusCode01 = updateResponse01.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode01, 1);

    }

    @Test(dataProvider = "UPIApplyPublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code as okhdfc and type as UPI_VPA FOR public user with mapped vpa for coupon")
    public void applyCouponWithMappedVpaTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO,CouponApplyPOJO couponApplyPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);

        Processor updateResponse01 = helper.applyCouponForUsers(couponApplyPOJO, code, 1);
        int statusCode01 = updateResponse01.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponCode02 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValue("$.data.couponCode"));
        Assert.assertEquals(statusCode01, 1);
        Assert.assertEquals(couponCode02,code);

    }

    @Test(dataProvider = "UPIApplyPublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code as okhdfc and type as UPI_VPA FOR public user with source set as 0")
    public void applyCouponWithSource0Test(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO,CouponApplyPOJO couponApplyPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);

        Processor updateResponse01 = helper.applyCouponForUsers(couponApplyPOJO, code, 0);
        int statusCode01 = updateResponse01.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponCode02 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValue("$.data.couponCode"));
        Assert.assertEquals(statusCode01, 1);
        Assert.assertEquals(couponCode02,code);

    }

    @Test(dataProvider = "UPIApplyPublic", dataProviderClass = CouponSuperDP.class, description = "Without Mapping a coupon to a user , valid brands while applying should be null")
    public void applyCouponWithoutMappingTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO,CouponApplyPOJO couponApplyPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse01 = helper.applyCouponForUsers(couponApplyPOJO, code, 0);
        int statusCode01 = updateResponse01.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponCode02 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValue("$.data.couponCode"));
        String validbrands = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.upi_response.valid_brands"));
        Assert.assertEquals(statusCode01, 1);
        Assert.assertEquals(couponCode02,code);
        Assert.assertEquals(validbrands, "[]");

    }

    @Test(dataProvider = "UPIApplyPublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon with payment code as okicici and type as UPI_VPA FOR public user with source set as null")
    public void applyCouponWithSourceNullTest(CreateCouponPOJO createCouponPOJO,UPIPaymentContractPOJO upiPaymentContractPOJO,CouponApplyPOJO couponApplyPOJO) throws Exception {
        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
        System.out.println("======"+code);

        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_HANDLE);
        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);

        Assert.assertEquals(paymentCode, RngConstants.okicici);
        Assert.assertNotNull(code);
        Assert.assertEquals(couponCode, code);
        Assert.assertEquals(type, RngConstants.UPI_HANDLE);

        Processor updateResponse01 = helper.applyCouponForUsers(couponApplyPOJO, code, null);
        int statusCode01 = updateResponse01.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String couponCode02 = String.valueOf(updateResponse01.ResponseValidator.GetNodeValue("$.data.couponCode"));
        Assert.assertEquals(statusCode01, 1);
        Assert.assertEquals(couponCode02,code);

    }


//  ==================================================GET - UPI Handle===========================================================================

    @Test(dataProvider = "GetDetailsPublic", dataProviderClass = CouponSuperDP.class, description = "GET coupons by userId and cityId")
    public void getDetailsTest(HashMap<String, Object> data) throws Exception {

        Processor getByUserIdCityId = rngHelper.getDetailsForCoupons("1","1");
        String id = String.valueOf(getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].id"));
        String Couponcode = String.valueOf(getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].code"));
        String type = String.valueOf(getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        String valid_Payment_methods = String.valueOf(getByUserIdCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].valid_payment_methods"));
        Assert.assertNotNull(Couponcode);
        Assert.assertNotNull(id);
        Assert.assertNotNull(type);
        Assert.assertNotNull(valid_Payment_methods);


    }

//    ==================================================== Delete Payment - UPI Handle======================================================

//    DELETE Payment testcases on hold - The DELETE operation is done using a body which doesn't support in the existing framework.
//    To be clarified



//    @Test(dataProvider = "UPICouponMapAndDeletePublic", dataProviderClass = CouponSuperDP.class, description = "Map coupon for super user type")
//    public void deletePaymentPublicokiciciTest(CreateCouponPOJO createCouponPOJO, UPIPaymentContractPOJO upiPaymentContractPOJO) throws Exception {
////        create coupon
//        Processor processor =  rngHelper.createCoupon(createCouponPOJO);
//        String code = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.code"));
//        System.out.println("======"+code);
//
////        map coupon
//        Processor updateResponse = helper.mapCouponForUsers(upiPaymentContractPOJO,code,RngConstants.okicici,RngConstants.UPI_HANDLE);
//        String paymentCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
//        String type = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
//        String couponCode = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
//        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);
//
//        Assert.assertEquals(paymentCode, RngConstants.okicici);
//        Assert.assertEquals(couponCode, code);
//        Assert.assertEquals(type, RngConstants.UPI_HANDLE);
//
////        delete coupon
//        Processor updateResponse01 = helper.deletePaymentForUsers(upiPaymentContractPOJO,code,RngConstants.okicici, RngConstants.UPI_HANDLE);
//        String coupon = String.valueOf(updateResponse01.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].couponCode"));
//        String paymentType = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].paymentCode"));
//        String type2 = String.valueOf(updateResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
//        upiPaymentContractPOJO.setDefault(paymentCode,couponCode,type);
//
//        Assert.assertEquals(paymentType, RngConstants.okicici);
//        Assert.assertEquals(coupon, code);
//        Assert.assertEquals(type2, RngConstants.UPI_HANDLE);
//        Assert.assertNotEquals(paymentType, RngConstants.okhdfc);
//
//    }


}
