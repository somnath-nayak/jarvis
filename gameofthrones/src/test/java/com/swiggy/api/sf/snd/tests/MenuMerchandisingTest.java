package com.swiggy.api.sf.snd.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.helper.ServiceablilityHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.MenuMerch.CreateFlatTDItemLevelPOJO;
import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import com.swiggy.api.sf.snd.dp.MenuMerchandisingDP;
import com.swiggy.api.sf.snd.helper.MenuMerchandisingHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.SqlTemplate;
import net.minidev.json.JSONArray;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class MenuMerchandisingTest extends MenuMerchandisingDP{

    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();
    MenuMerchandisingHelper menuMerchandisingHelper= new MenuMerchandisingHelper();
    ServiceablilityHelper serviceablewithbanner = new ServiceablilityHelper();
    CMSHelper cmsHelper = new CMSHelper();
    RngHelper rngHelper = new RngHelper();
    SANDHelper helper = new SANDHelper();
    com.swiggy.api.sf.rng.helper.EDVOHelper deleteTdHelper = new com.swiggy.api.sf.rng.helper.EDVOHelper();

    @Test(dataProvider = "getRestId",groups = {"sanity","smoke","srishty","menuMerchandising"},description = "Verify merchandising response")
    public void verifyMerchandisingInfo(String layoutType,String restId) throws InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);

        Thread.sleep(2000);
        Processor processor = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = processor.ResponseValidator.GetResponseCode();
        assertEquals(statusCode, 200);
    }

    @Test (dataProvider = "getRestId",groups= {"schemaValidation","srishty","menuMerchandising"}, description="Verify schema check for merchandising API")
    public void merchandisingJSONSchemavalidation(String layoutType,String restId) throws IOException, ProcessingException, InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);

        Thread.sleep(2000);
        Processor processor = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/Sand/merchandising.txt");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        System.out.println("missingNodeList==="+missingNodeList.size());
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For Merchandising API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");

    }

    /*  Verify the order of the layout for a rest having layout_type as HIGH TOUCH
        Verify the order of the layout w.r.t the widgets order while the rest id open and serviceable
        Verify the merchandising and menu show the layout as high_touch for newer version i.e. above 5000,
        Verify the merchandising and menu does not show the layout as high_touch for older version i.e. below 5000
    */
    @Test(dataProvider = "menuWithHighTouchValid",groups= {"functional","srishty","menuMerchandising"}, description="Verify the order of the layout for a rest having layout_type as HIGH TOUCH")
    public void verifyHighTouchConfig(String layoutType,String restId, String lat,String lng) throws InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);

        Thread.sleep(2000);
        Processor processor = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String layoutTypeAPI = processor.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Integer restIdFromJSON = processor.ResponseValidator.GetNodeValueAsInt("$.restaurantId");
        Assert.assertEquals(layoutTypeAPI,layoutType);
        Assert.assertEquals(restIdFromJSON.toString(),restId);

        Processor processor1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String layoutTypeInMenu = processor1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertEquals(layoutTypeInMenu,layoutType);

        // Version check

        Processor processor2 = menuMerchandisingHelper.getMerchandisingInfoVersionCheck(restId);
        String layoutTypeVersion = processor2.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Assert.assertNotEquals(layoutTypeVersion,layoutType);
        Assert.assertEquals(layoutTypeVersion,"normal");

        Processor processor3 = menuMerchandisingHelper.getMenuV4VersionCheck(restId,lat,lng);
        String layoutTypeInMenuVersion = processor3.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertNotEquals(layoutTypeInMenuVersion,layoutType);
        Assert.assertEquals(layoutTypeInMenuVersion,"normal");

    }

    /* Verify the order of the layout for a rest having layout_type as NORMAL */
    @Test(dataProvider = "menuWithNormalLayout",groups= {"functional","srishty","menuMerchandising"},description = "Verify the order of the layout for a rest having layout_type as NORMAL")
    public void verifyNormalConfig(String layoutType,String restId, String lat,String lng) throws InterruptedException {

        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);

        Thread.sleep(2000);
        Processor processor = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String layoutTypeAPI = processor.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Integer restIdFromJSON = processor.ResponseValidator.GetNodeValueAsInt("$.restaurantId");
        Assert.assertEquals(layoutTypeAPI,layoutType);
        Assert.assertEquals(restIdFromJSON.toString(),restId);

        Processor processor1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String layoutTypeInMenu = processor1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertEquals(layoutTypeInMenu,layoutType);
    }

    /* verify addition of new section */
    @Test(dataProvider = "getRestId",groups= {"functional","srishty","menuMerchandising"},description = "verify addition of new section")
    public void verifyAddingNewSection(String layoutType,String restId) throws InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);
        //read current section size
        HashMap<String, String> sectionConfigFromRedis = menuMerchandisingHelper.getSectionConfigFromRedis();
        int size = sectionConfigFromRedis.size();

        // add one more section
        menuMerchandisingHelper.addNewSectionConfig(size);

        // verify the menu merch API for the new section
        Thread.sleep(2000);
        Processor merchandisingInfo = menuMerchandisingHelper.getMerchandisingInfo(restId);
        JSONArray valueAsJsonArray = merchandisingInfo.ResponseValidator.GetNodeValueAsJsonArray("$.sectionConfigList..id");

        Assert.assertEquals(valueAsJsonArray.size(),size);
        Assert.assertEquals(valueAsJsonArray.contains(size-1),true);

    }

    /* verify priority of the section */
    @Test(dataProvider = "getRestId",groups= {"functional","srishty","menuMerchandising"},description = "verify priority of the section")
    public void verifyPriorityForSection(String layoutType, String restId) throws JSONException, InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);

        List prioritiesList = new ArrayList();

        HashMap<String, String> sectionConfigFromRedis = menuMerchandisingHelper.getSectionConfigFromRedis();
        int size = sectionConfigFromRedis.size();
        for (int i = 0 ; i <size;i++)
        {
            String valuefromRedisForGivenSection = menuMerchandisingHelper.getValuefromRedisForGivenSection(i);
            String priority = JsonPath.read(valuefromRedisForGivenSection, "$.priority").toString();
            prioritiesList.add(priority);
        }

        Processor merchandisingInfo = menuMerchandisingHelper.getMerchandisingInfo(restId);
        JSONArray valueAsJsonArray = merchandisingInfo.ResponseValidator.GetNodeValueAsJsonArray("$.sectionConfigList..priority");
        int size1 = valueAsJsonArray.size();
        for (int j=0;j<size1;j++){
            assertEquals(prioritiesList.get(j).toString(),valueAsJsonArray.get(j).toString());
        }

    }

    /* verify the transition of the layoutType from high_touch to normal.
        verify transition of the layoutType for normal to high_touch
    */
    @Test(dataProvider = "menuWithLayoutTransition",groups= {"functional","srishty","menuMerchandising"},description = "verify the transition of the layoutType from high_touch to normal and vice-versa")
    public void verifyTransitionOfLayoutType(String layoutType,String layoutType2,String restId, String lat,String lng) throws InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);

        // verify high_touch to normal
        menuMerchandisingHelper.setLayoutInRedis(layoutType2,restId);

        Thread.sleep(3000);
        Processor processorNormal = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String layoutTypeNormal = processorNormal.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Integer restIdFromJSONNormal = processorNormal.ResponseValidator.GetNodeValueAsInt("$.restaurantId");
        Assert.assertEquals(layoutTypeNormal,layoutType2);
        Assert.assertEquals(restIdFromJSONNormal.toString(),restId);

        Processor processorNormal1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String layoutTypeInMenuNormal = processorNormal1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertEquals(layoutTypeInMenuNormal,layoutType2);

        // verify normal to high_touch
        menuMerchandisingHelper.setLayoutInRedis(layoutType, restId);

        Thread.sleep(3000);
        Processor processor1 = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String layoutTypeAPI = processor1.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Integer restIdFromJSON = processor1.ResponseValidator.GetNodeValueAsInt("$.restaurantId");
        Assert.assertEquals(layoutTypeAPI,layoutType);
        Assert.assertEquals(restIdFromJSON.toString(),restId);

        Processor processor2 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String layoutTypeInMenu = processor2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertEquals(layoutTypeInMenu,layoutType);

        // Version check

        Processor processor3 = menuMerchandisingHelper.getMerchandisingInfoVersionCheck(restId);
        String layoutTypeVersion = processor3.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Assert.assertNotEquals(layoutTypeVersion,layoutType);
        Assert.assertEquals(layoutTypeVersion,layoutType2);

        Processor processor4 = menuMerchandisingHelper.getMenuV4VersionCheck(restId,lat,lng);
        String layoutTypeInMenuVersion = processor4.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertNotEquals(layoutTypeInMenuVersion,layoutType);
        Assert.assertEquals(layoutTypeInMenuVersion,layoutType2);

    }

    /* verify the response while high touch is enabled but sections config is not present
      */
    @Test(dataProvider = "menuWithHighTouchValid",groups= {"functional","srishty","menuMerchandising"},description = "verify the response while high touch is enabled but sections config is not present")
    public void verifyHighTouchWithNoSectionConfig(String layoutType,String restId, String lat,String lng) throws InterruptedException {

        // Set High Touch for a rest
        menuMerchandisingHelper.setLayoutInRedis(layoutType, restId);
        Thread.sleep(2000);

        //Delete all section Config
        menuMerchandisingHelper.deleteAllSections();
        Thread.sleep(2000);
        // Hit merch config API and Menu API to verify the same
        Processor processor = menuMerchandisingHelper.getMerchandisingInfo(restId);
        JSONArray valueAsJsonArray = processor.ResponseValidator.GetNodeValueAsJsonArray("$.sectionConfigList..id");
        Assert.assertEquals(valueAsJsonArray.size(),0);

        Processor processor1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String valueAsJsonArray1 = processor1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.menu..menuCarousels").replace("]","").replace("[","");
        Assert.assertEquals(valueAsJsonArray1,"null");

    }

    /*
    verify the min number of carousels
     */

    @Test(dataProvider = "createCarouselWithEnabled",groups= {"functional","srishty","menuMerchandising"},description = "verify the min number of carousels")
    public void verifyMinOfSection(String layoutType,String restId, String lat,String lng,CarouselPOJO carouselPOJO) throws IOException, InterruptedException {

        //set high_touch for a rest and set default HT section config
        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);
        Processor merchandisingInfoResp = menuMerchandisingHelper.getMerchandisingInfo(restId);
        int min = Integer.valueOf(merchandisingInfoResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.sectionConfigList[?(@.name=='menuCarousels')].min").replace("]","").replace("[","").replace("\"",""));

        /*
        delete the existing carousels from redis
        */
        menuMerchandisingHelper.deleteAllCarousels(restId);
        Thread.sleep(2000);
        /* create carousel less than min number of carousel and check
         */
        List<String> bannerIdList = new ArrayList<>();

        for (int i=1; i<min;i++){
            Processor carousel = menuMerchandisingHelper.createCarousel(carouselPOJO);
            String bannerID = carousel.ResponseValidator.GetNodeValue("$.data");
            bannerIdList.add(bannerID);
        }

        /*
        Verify Menu response, carousel shall not show
         */
        Processor menuV4Response = menuMerchandisingHelper.getMenuV4(restId, lat, lng);
        String resp = menuV4Response.ResponseValidator.GetBodyAsText();
        JSONArray jsonArray = menuV4Response.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels");
        int size1 = jsonArray.size();
        assertEquals(size1,0);
        bannerIdList.clear();

        /*
        delete the existing carousels from redis
        */
        menuMerchandisingHelper.deleteAllCarousels(restId);
        Thread.sleep(2000);
        /* create carousel equal to min number of carousel and check
         */
        for (int i=1; i<=min;i++){
            Processor carousel = menuMerchandisingHelper.createCarousel(carouselPOJO);
            String bannerID = String.valueOf(carousel.ResponseValidator.GetNodeValueAsInt("$.data"));
            bannerIdList.add(bannerID);
        }

        /* create min number of carousel and check */

        Processor processor2 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray type = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')].type");
        JSONArray valueAsJsonArray1 = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')]..bannerId");
        int size = valueAsJsonArray1.size();
        for (int i=0;i<size;i++)
        {
            boolean contains = bannerIdList.contains(valueAsJsonArray1.get(i).toString());
            assertEquals(contains,true);
        }
    }

    /*
    verify the max number of carousels
     */
    @Test(dataProvider = "createCarouselWithEnabled",groups= {"functional","srishty","menuMerchandising"},description = "verify the max number of carousels")
    public void verifyMaxOfSection(String layoutType,String restId, String lat,String lng,CarouselPOJO carouselPOJO) throws IOException, InterruptedException {

        List<String> bannerIdList = new ArrayList<>();

        //set high_touch for a rest and set default HT section config
        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);

        Thread.sleep(2000);
        Processor merchandisingInfoResp = menuMerchandisingHelper.getMerchandisingInfo(restId);

        //delete the existing carousels from redis
        menuMerchandisingHelper.deleteAllCarousels(restId);
        Thread.sleep(2000);
        // create carousels as per the max number of carousels and check
        menuMerchandisingHelper.createMaxCarousels(carouselPOJO);

        // hit menu V4 and read the current carousel count
        Processor menuV4Response = menuMerchandisingHelper.getMenuV4(restId, lat, lng);
        String resp = menuV4Response.ResponseValidator.GetBodyAsText();
        boolean b= menuV4Response.ResponseValidator.DoesNodeExists("$.data..menu..menuCarousels",resp);

        Assert.assertEquals(b,true);
        JSONArray valueAsJsonArray1 = menuV4Response.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')]..bannerId");
        int size = valueAsJsonArray1.size();
        for (int i=0;i<size;i++)
        {
            boolean contains = bannerIdList.contains(valueAsJsonArray1.get(i).toString());
            assertEquals(contains,true);
        }

        //create carousel more than the max limit and check
        bannerIdList.clear();
        Processor carousel = menuMerchandisingHelper.createCarousel(carouselPOJO);
        String bannerIDnew = String.valueOf(carousel.ResponseValidator.GetNodeValueAsInt("$.data"));
        bannerIdList.add(bannerIDnew);

        Processor menuV4Response2 = menuMerchandisingHelper.getMenuV4(restId, lat, lng);
        JSONArray valueAsJsonArray2 = menuV4Response2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')]..bannerId");
        boolean contains = valueAsJsonArray2.contains(bannerIDnew);
        assertEquals(contains,false);

    }

    /*
    verify disabling the carousel
     */
    @Test(dataProvider = "createCarouselWithDisabled",groups= {"functional","srishty","menuMerchandising"},description = " verify disabling the carousel")
    public void verifyDisablingCarousel(String layoutType,String restId, String lat,String lng,CarouselPOJO carouselPOJO) throws IOException, InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);

        CarouselPOJO carouselPOJOEnabled = carouselPOJO.withEnabled(true);

        //creating min enabled carousels as test data
        menuMerchandisingHelper.createMinimumCarousels(carouselPOJOEnabled);
        Processor menuV4ResponseEnabled = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String firstWidgetTypeEnabled = menuV4ResponseEnabled.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.menu.widgets[0]..type").replace("]","").replace("[","").replace("\"","");
        String resp = menuV4ResponseEnabled.ResponseValidator.GetBodyAsText();
        boolean isMenuCarouselPresentEnabled = menuV4ResponseEnabled.ResponseValidator.DoesNodeExists("$.data.menu.menuCarousels",resp);

        Assert.assertEquals(firstWidgetTypeEnabled,"menuCarousels");
        Assert.assertEquals(isMenuCarouselPresentEnabled,true);


        // creating disabled carousel for itemId
        CarouselPOJO carouselPOJODisabled = carouselPOJO.withEnabled(false);

        Processor carouselResponse = menuMerchandisingHelper.createCarousel(carouselPOJODisabled);
        String bannerId = String.valueOf(carouselResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

        Processor menuV4ResponseDisabled = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray bannerIdArray = menuV4ResponseDisabled.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..data.bannerId");
        boolean isBannerPresent = bannerIdArray.contains(bannerId);


        Assert.assertEquals(isBannerPresent,false);

    }

    /*
    Verify the carousel out of Slot
    */
    @Test(dataProvider = "createCarouselOutOfSlot",groups= {"functional","srishty","menuMerchandising"},description = " Verify the carousel out of Slot")
    public void verifyCarouselOutOfSlot(String layoutType,String restId, String lat,String lng,CarouselPOJO carouselPOJO ) throws IOException, JSONException, InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);

        Thread.sleep(2000);
        //creating min enabled carousels as test data
        menuMerchandisingHelper.createMinimumCarousels(carouselPOJO);

//        //delete the existing carousels from redis
//        menuMerchandisingHelper.deleteAllCarousels(restId);

        carouselPOJO.getAllDaysTimeSlot().remove(menuMerchandisingHelper.getCurrentDay()-1);

        // creating carousel out of slot
        Processor carouselResponse = menuMerchandisingHelper.createCarousel(carouselPOJO);
        String bannerId = String.valueOf(carouselResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

        //validating in redis
        String carouselFromRedis = menuMerchandisingHelper.getCarouselFromRedis(restId, bannerId);
        String bannerIdFromRedis = JsonPath.read(carouselFromRedis, "$.id").toString();
        Assert.assertEquals(bannerIdFromRedis,bannerId);

        //validating in Menu response
        Processor menuV4Response1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray bannerIdArray = menuV4Response1.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..data.bannerId");
        boolean isBannerPresent = bannerIdArray.contains(bannerId);

        Assert.assertEquals(isBannerPresent,false);
    }

    /*
    Verify the carousel does not show if item OOS
     */
    @Test(dataProvider = "createCarouselOutOfStock",groups= {"functional","srishty","menuMerchandising"},description = "Verify the carousel does not show if item OOS")
    public void verifyCarouselForOOS(String layoutType,String restId, String lat,String lng,String itemIdToBeOOS,CarouselPOJO carouselPOJO) throws IOException, JSONException, InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);
        //deleting existing carousels
        menuMerchandisingHelper.deleteAllCarousels(restId);
        Thread.sleep(2000);
        //creating min enabled carousels as test data
        menuMerchandisingHelper.createMinimumCarousels(carouselPOJO);

        CarouselPOJO carouselPOJOoos = carouselPOJO.withRedirectLink(itemIdToBeOOS);
        // creating carousel
        Processor carouselResponse = menuMerchandisingHelper.createCarousel(carouselPOJOoos);
        String bannerId = String.valueOf(carouselResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

        String itemId = carouselPOJOoos.getRedirectLink();

        //validating in redis
        Thread.sleep(2000);
        String carouselFromRedis = menuMerchandisingHelper.getCarouselFromRedis(restId, bannerId);
        String bannerIdFromRedis = JsonPath.read(carouselFromRedis, "$.id").toString();
        Assert.assertEquals(bannerIdFromRedis,bannerId);

        //mark item OOS
        cmsHelper.setItemInStock(itemIdToBeOOS,0);

        //validating Menu response
        Processor menuV4Response = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray bannerIdArray = menuV4Response.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..data.bannerId");
        boolean isBannerPresent = bannerIdArray.contains(bannerId);

        Assert.assertEquals(isBannerPresent,false);

        //mark item in stock
        cmsHelper.setItemInStock(itemIdToBeOOS,1);
    }

    @Test(dataProvider = "createCarouselWithEnabled",groups= {"functional","srishty","menuMerchandising"},description = "Verify while rest is closed")
    public void verifyRestClosed(String layoutType,String restId, String lat,String lng, CarouselPOJO carouselPOJO) throws IOException, InterruptedException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);
        //creating min enabled carousels as test data
        menuMerchandisingHelper.createMinimumCarousels(carouselPOJO);
        Thread.sleep(2000);
        //validate the menu
        Processor menuV4Response = menuMerchandisingHelper.getMenuV4(restId, lat, lng);
        String firstWidgetType = (menuV4Response.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.widgets[0]..type")).get(0).toString();
        String resp = menuV4Response.ResponseValidator.GetBodyAsText();
        boolean isMenuCarouselPresent = menuV4Response.ResponseValidator.DoesNodeExists("$.data.menu.menuCarousels",resp);

        Assert.assertEquals(firstWidgetType,"menuCarousels");
        Assert.assertEquals(isMenuCarouselPresent,true);

        //mark rest as closed
        int openTime = menuMerchandisingHelper.getOpenTime();
        cmsHelper.setRestaurantOpenTime(restId,openTime);

        //validate no carousels
        Processor menuV4ResponseNew = menuMerchandisingHelper.getMenuV4(restId, lat, lng);
        String firstWidgetTypeNew = (menuV4ResponseNew.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.widgets[0]..type")).get(0).toString();
        String respnew = menuV4ResponseNew.ResponseValidator.GetBodyAsText();
        boolean isMenuCarouselPresentNew = menuV4ResponseNew.ResponseValidator.DoesNodeExists("$.data.menu.menuCarousels",respnew);
        JSONArray valueAsJsonArray = menuV4ResponseNew.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels");

        Assert.assertEquals(valueAsJsonArray.isEmpty(),true);
        Assert.assertNotEquals(firstWidgetTypeNew,"menuCarousels");
        Assert.assertEquals(isMenuCarouselPresentNew,true);


    }

    @Test(dataProvider = "serviceableWithBanner",groups= {"functional","srishty","menuMerchandising"},description = "Verify while rest is serviceable with banner")
    public void verifyRestServiceableWithBanner(String layoutType,String restId, String lat,String lng,String cityId,CarouselPOJO carouselPOJO) throws InterruptedException, IOException {

        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);
        //creating min enabled carousels as test data
        menuMerchandisingHelper.createMinimumCarousels(carouselPOJO);
        Thread.sleep(2000);
        //validate the menu
        Processor menuV4Response = menuMerchandisingHelper.getMenuV4(restId, lat, lng);
        String firstWidgetType = (menuV4Response.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.widgets[0]..type")).get(0).toString();
        String resp = menuV4Response.ResponseValidator.GetBodyAsText();
        boolean isMenuCarouselPresent = menuV4Response.ResponseValidator.DoesNodeExists("$.data.menu.menuCarousels",resp);

        Assert.assertEquals(firstWidgetType,"menuCarousels");
        Assert.assertEquals(isMenuCarouselPresent,true);

        //mark the rest as serviceable with banner
        serviceablewithbanner.serviceablewithbanner(lat, lng, cityId, restId);

        // validate menu v4 for no carousels
        Processor menuV4Response2 = menuMerchandisingHelper.getMenuV4(restId, lat, lng);
        String firstWidgetType2 = (menuV4Response2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.widgets[0]..type")).get(0).toString();
        String respnew = menuV4Response2.ResponseValidator.GetBodyAsText();
        boolean isMenuCarouselPresent2 = menuV4Response2.ResponseValidator.DoesNodeExists("$.data.menu.menuCarousels",respnew);

        Assert.assertNotEquals(firstWidgetType2,"menuCarousels");
        Assert.assertEquals(isMenuCarouselPresent2,false);

    }

    /*
    Verify the ItemCarousel shall show up in the respective TD slot
     */
    @Test(dataProvider = "createCarouselItemTDEnabled",groups = "",description = "Verify the ItemCarousel shall show up in the respective TD slot")
    public void verifyMenuMerchWithTdSlot(String layoutType,String restId, String lat, String lng, CarouselPOJO carouselPOJO, CreateFlatTDItemLevelPOJO createFlatTDItemLevelPOJO) throws IOException, JSONException, InterruptedException {

        //set High touch config for rest
        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);
        //verify change of layout type in menu response
        Processor processor = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String layoutTypeAPI = processor.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Integer restIdFromJSON = processor.ResponseValidator.GetNodeValueAsInt("$.restaurantId");
        Assert.assertEquals(layoutTypeAPI,layoutType);
        Assert.assertEquals(restIdFromJSON.toString(),restId);

        Processor processor1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String layoutTypeInMenu = processor1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertEquals(layoutTypeInMenu,layoutType);

        //delete existing carousels from redis
        menuMerchandisingHelper.deleteAllCarousels(restId);
        Thread.sleep(2000);
        //delete existing tds for the rest
        com.swiggy.api.sf.rng.helper.EDVOHelper.deleteUserByMobile(restId);

        //create minimum carousels and verify in menu response
        menuMerchandisingHelper.createMinimumCarousels(carouselPOJO);
        Thread.sleep(2000);
        List<String> bannerIdList = new ArrayList<>();
        Processor processor2 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray type = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')].type");
        JSONArray valueAsJsonArray1 = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')]..bannerId");
        int size = valueAsJsonArray1.size();
        for (int i=0;i<size;i++)
        {
            boolean contains = bannerIdList.contains(valueAsJsonArray1.get(i).toString());
            assertEquals(contains,true);
        }

        //delete existing tds for the rest
        com.swiggy.api.sf.rng.helper.EDVOHelper.deleteUserByMobile(restId);

        //create td for item id
        Processor itemTD = rngHelper.createItemTD(createFlatTDItemLevelPOJO);
        String tdId = Integer.toString(itemTD.ResponseValidator.GetNodeValueAsInt("$.data"));
        List<String> tdList = new ArrayList<>();
        tdList.add(tdId);

        //create new carousel with td id
        carouselPOJO.withCarouselTradeDiscountInfoSet("item",tdList);
        Processor carousel = menuMerchandisingHelper.createCarousel(carouselPOJO);
        Thread.sleep(2000);
        String bannerID = carousel.ResponseValidator.GetNodeValue("$.data");

        String carouselFromRedis = menuMerchandisingHelper.getCarouselFromRedis(restId, bannerID);
        String bannerIdFromRedis = JsonPath.read(carouselFromRedis, "$.id").toString();
        Assert.assertEquals(bannerIdFromRedis,bannerID);

        // hit menu V4 and validate the carousel banner
        Processor menuV4Response = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray bannerIdArray = menuV4Response.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..data.bannerId");
        boolean isBannerPresent = bannerIdArray.contains(bannerID);

        Assert.assertEquals(isBannerPresent,true);

    }
    /*
    Verify the ItemCarousel shall not show up outside the TD slot
     */
    @Test(dataProvider = "createCarouselItemTDOutsideSlot",groups = "",description = "Verify the ItemCarousel shall not show up outside the TD slot")
    public void verifyMenuMerchOutsideTDSlot(String layoutType,String restId, String lat, String lng, CarouselPOJO carouselPOJO, CreateFlatTDItemLevelPOJO createFlatTDItemLevelPOJO) throws IOException, JSONException, InterruptedException {

        //set High touch config for rest
        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis("high_touch",restId);
        Thread.sleep(2000);

        //verify change of layout type in menu response
        Processor processor = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String layoutTypeAPI = processor.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Integer restIdFromJSON = processor.ResponseValidator.GetNodeValueAsInt("$.restaurantId");
        Assert.assertEquals(layoutTypeAPI,layoutType);
        Assert.assertEquals(restIdFromJSON.toString(),restId);

        Processor processor1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String layoutTypeInMenu = processor1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertEquals(layoutTypeInMenu,layoutType);

        //delete existing carousels from redis
        menuMerchandisingHelper.deleteAllCarousels(restId);
        Thread.sleep(2000);

        //create minimum carousels and verify in menu response
        menuMerchandisingHelper.createMinimumCarousels(carouselPOJO);
        Thread.sleep(2000);
        List<String> bannerIdList = new ArrayList<>();
        Processor processor2 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray type = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')].type");
        JSONArray valueAsJsonArray1 = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')]..bannerId");
        int size = valueAsJsonArray1.size();
        for (int i=0;i<size;i++)
        {
            boolean contains = bannerIdList.contains(valueAsJsonArray1.get(i).toString());
            assertEquals(contains,true);
        }

        //delete existing tds for the rest
        com.swiggy.api.sf.rng.helper.EDVOHelper.deleteUserByMobile(restId);

        //create td for item id with slot excluding the current day
        createFlatTDItemLevelPOJO.getAllDaysTimeSlot().remove(menuMerchandisingHelper.getCurrentDay());
        Processor itemTD = rngHelper.createItemTD(createFlatTDItemLevelPOJO);
        String tdId = Integer.toString(itemTD.ResponseValidator.GetNodeValueAsInt("$.data"));
        List<String> tdList = new ArrayList<>();
        tdList.add(tdId);

        //create new carousel with td id and verify in redis
        carouselPOJO.withCarouselTradeDiscountInfoSet("item",tdList);
        Processor carousel = menuMerchandisingHelper.createCarousel(carouselPOJO);
        String bannerID = carousel.ResponseValidator.GetNodeValue("$.data");

        String carouselFromRedis = menuMerchandisingHelper.getCarouselFromRedis(restId, bannerID);
        String bannerIdFromRedis = JsonPath.read(carouselFromRedis, "$.id").toString();
        Assert.assertEquals(bannerIdFromRedis,bannerID);

        // hit menu V4 and validate the carousel banner data
        Processor menuV4Response1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray bannerIdArray1 = menuV4Response1.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..data.bannerId");
        boolean isBannerPresent1 = bannerIdArray1.contains(bannerID);

        Assert.assertEquals(isBannerPresent1,false);

    }

    /*
    Verify the menu Response when tdCarousels have any td campaign mapped to it.
     */
    @Test(dataProvider = "createCarouselItemTDEnabled",groups = "",description = "Verify the menu Response when tdCarousels have any td campaign mapped to it.")
    public void verifyMenuMerchWithMappedTdEnabled(String layoutType,String restId, String lat, String lng, CarouselPOJO carouselPOJO, CreateFlatTDItemLevelPOJO createFlatTDItemLevelPOJO) throws IOException, JSONException, InterruptedException {

        //set High touch config for rest
        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);
        //verify change of layout type in menu response
        Processor processor = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String layoutTypeAPI = processor.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Integer restIdFromJSON = processor.ResponseValidator.GetNodeValueAsInt("$.restaurantId");
        Assert.assertEquals(layoutTypeAPI,layoutType);
        Assert.assertEquals(restIdFromJSON.toString(),restId);

        Processor processor1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String layoutTypeInMenu = processor1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertEquals(layoutTypeInMenu,layoutType);

        //delete existing carousels from redis
        menuMerchandisingHelper.deleteAllCarousels(restId);
        Thread.sleep(2000);

        //delete existing tds for the rest
        com.swiggy.api.sf.rng.helper.EDVOHelper.deleteUserByMobile(restId);

        //create minimum carousels and verify in menu response
        menuMerchandisingHelper.createMinimumCarousels(carouselPOJO);
        Thread.sleep(2000);

        List<String> bannerIdList = new ArrayList<>();
        Processor processor2 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray type = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')].type");
        JSONArray valueAsJsonArray1 = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')]..bannerId");
        int size = valueAsJsonArray1.size();
        for (int i=0;i<size;i++)
        {
            boolean contains = bannerIdList.contains(valueAsJsonArray1.get(i).toString());
            assertEquals(contains,true);
        }

        //delete existing campaign
        com.swiggy.api.sf.rng.helper.EDVOHelper.deleteUserByMobile(restId);

        //create td for item id
        Processor itemTD = rngHelper.createItemTD(createFlatTDItemLevelPOJO);
        String tdId = Integer.toString(itemTD.ResponseValidator.GetNodeValueAsInt("$.data"));
        List<String> tdList = new ArrayList<>();
        tdList.add(tdId);

        //create new carousel with td id
        carouselPOJO.withCarouselTradeDiscountInfoSet("item",tdList);
        Processor carousel = menuMerchandisingHelper.createCarousel(carouselPOJO);
        String bannerID = carousel.ResponseValidator.GetNodeValue("$.data");

        String carouselFromRedis = menuMerchandisingHelper.getCarouselFromRedis(restId, bannerID);
        String bannerIdFromRedis = JsonPath.read(carouselFromRedis, "$.id").toString();
        Assert.assertEquals(bannerIdFromRedis,bannerID);

        // hit menu V4 and validate the carousel banner
        Processor menuV4Response = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray bannerIdArray = menuV4Response.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..data.bannerId");
        boolean isBannerPresent = bannerIdArray.contains(bannerID);

        Assert.assertEquals(isBannerPresent,true);

    }
    /*
    verify the tdCarousels response while the td attached to the campaign is disabled
     */
    @Test(dataProvider = "createCarouselItemTDEnabled",groups = "",description = " verify the tdCarousels response while the td attached to the campaign is disabled")
    public void verifyMenuMerchWithTDMappedIsDisabled(String layoutType,String restId, String lat, String lng, CarouselPOJO carouselPOJO, CreateFlatTDItemLevelPOJO createFlatTDItemLevelPOJO) throws IOException, JSONException, InterruptedException {

//        restId= "23798";

        //set High touch config for rest
        menuMerchandisingHelper.setSectionsForHighTouch();
        menuMerchandisingHelper.setLayoutInRedis(layoutType,restId);
        Thread.sleep(2000);
        //verify change of layout type in menu response
        Processor processor = menuMerchandisingHelper.getMerchandisingInfo(restId);
        String layoutTypeAPI = processor.ResponseValidator.GetNodeValue("$.restaurantLayoutType");
        Integer restIdFromJSON = processor.ResponseValidator.GetNodeValueAsInt("$.restaurantId");
        Assert.assertEquals(layoutTypeAPI,layoutType);
        Assert.assertEquals(restIdFromJSON.toString(),restId);

        Processor processor1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        String layoutTypeInMenu = processor1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..layoutType").replace("]","").replace("[","").replace("\"","");
        Assert.assertEquals(layoutTypeInMenu,layoutType);

        //delete existing carousels from redis
        menuMerchandisingHelper.deleteAllCarousels(restId);
        Thread.sleep(2000);
        //create minimum carousels and verify in menu response
        menuMerchandisingHelper.createMinimumCarousels(carouselPOJO);
        Thread.sleep(2000);
        List<String> bannerIdList = new ArrayList<>();
        Processor processor2 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray type = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')].type");
        JSONArray valueAsJsonArray1 = processor2.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..[?(@.type=='TopCarousel')]..bannerId");
        int size = valueAsJsonArray1.size();
        for (int i=0;i<size;i++)
        {
            boolean contains = bannerIdList.contains(valueAsJsonArray1.get(i).toString());
            assertEquals(contains,true);
        }

        //delete existing tds for the rest
        com.swiggy.api.sf.rng.helper.EDVOHelper.deleteUserByMobile(restId);

        //create td for item id
        Processor itemTD = rngHelper.createItemTD(createFlatTDItemLevelPOJO);
        String tdId = Integer.toString(itemTD.ResponseValidator.GetNodeValueAsInt("$.data"));
        List<String> tdList = new ArrayList<>();
        tdList.add(tdId);

        //create new carousel with td id
        carouselPOJO.withCarouselTradeDiscountInfoSet("item",tdList);
        Processor carousel = menuMerchandisingHelper.createCarousel(carouselPOJO);
        Thread.sleep(2000);
        String bannerID = carousel.ResponseValidator.GetNodeValue("$.data");

        String carouselFromRedis = menuMerchandisingHelper.getCarouselFromRedis(restId, bannerID);
        String bannerIdFromRedis = JsonPath.read(carouselFromRedis, "$.id").toString();
        Assert.assertEquals(bannerIdFromRedis,bannerID);

        // hit menu V4 and validate the carousel banner
        Processor menuV4Response = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray bannerIdArray = menuV4Response.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..data.bannerId");
        boolean isBannerPresent = bannerIdArray.contains(bannerID);

        Assert.assertEquals(isBannerPresent,true);

        // mark Td disabled
        RngHelper.disableEnableTd(tdId,false);

        // hit menu V4 and validate the carousel banner data
        Processor menuV4Response1 = menuMerchandisingHelper.getMenuV4(restId,lat,lng);
        JSONArray bannerIdArray1 = menuV4Response1.ResponseValidator.GetNodeValueAsJsonArray("$.data.menu.menuCarousels..data.bannerId");
        boolean isBannerPresent1 = bannerIdArray1.contains(bannerID);

        Assert.assertEquals(isBannerPresent1,false);
    }

    @Test(dataProvider = "sectionConfig",groups = {"sanity","smoke","nevia","presentational"},description = "Verify section config response")
    public void sectionConfig( String name, String title, String min, String max, String priority, String enabled, String [] layoutType) {
        Processor processor = menuMerchandisingHelper.sectionConfig(new String[] {name,title,min,max,priority,enabled,t(layoutType)});
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode= JsonPath.read(resp,"$.statusCode");
        assertEquals(statusCode, 0);
        String statusMsg= JsonPath.read(resp,"$.statusMessage");
        assertEquals(statusMsg, "successfully done");
        String data= JsonPath.read(resp,"$.data");
        assertEquals(data, "Updated Successfully");
    }

    @Test(dataProvider = "sectionConfigInvalid",groups = {"sanity","smoke","nevia","presentational"},description = "Verify section config invalid response")
    public void sectionConfigInvalid( String name, String title, String min, String max, String priority, String enabled, String [] layoutType) {
        Processor processor = menuMerchandisingHelper.sectionConfig(new String[] {name,title,min,max,priority,enabled,t(layoutType)});
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode= JsonPath.read(resp,"$.statusCode");
        assertEquals(statusCode, 0);
        String statusMsg= JsonPath.read(resp,"$.statusMessage");
        assertEquals(statusMsg, "successfully done");
        String data= JsonPath.read(resp,"$.data");
        assertEquals(data, "Updated Successfully");
    }

    @Test(dataProvider = "sectionConfig",groups = {"sanity","smoke","nevia","presentational"},description = "Verify section config response in redis, also check for layout mapping")
    public void sectionConfigRedis( String name, String title, String min, String max, String priority, String enabled, String [] layoutType) {
        HashMap<String, String> sectionConfigFromRedis = menuMerchandisingHelper.getSectionConfigFromRedis();
        int size = sectionConfigFromRedis.size();
        HashMap<String, String> layoutMappingFromRedis = menuMerchandisingHelper.getMerchLayoutMappingConfigFromRedis(t(layoutType));
        int sizeM = layoutMappingFromRedis.size();
        Processor processor = menuMerchandisingHelper.sectionConfig(new String[] {name,title,min,max,priority,enabled,t(layoutType)});
        String resp = processor.ResponseValidator.GetBodyAsText();
        HashMap<String, String> sectionConfigFromRedisAfter = menuMerchandisingHelper.getSectionConfigFromRedis();
        int sizeAfter = sectionConfigFromRedisAfter.size();
        Assert.assertTrue(sizeAfter>size,"Section is not getting added");
        HashMap<String, String> layoutMappingFromRedisAfter = menuMerchandisingHelper.getMerchLayoutMappingConfigFromRedis(t(layoutType));
        int sizeMAfter = layoutMappingFromRedisAfter.size();
        Assert.assertTrue(sizeMAfter>sizeM,"Layout is not getting added");

    }

    @Test(dataProvider = "readLayoutType",groups = {"sanity","smoke","nevia","presentational"},description = "Verify read layout response")
    public void verifyLayout( String lat , String lng) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = menuMerchandisingHelper.readLayoutType(new String[]{restId});
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode= JsonPath.read(resp,"$.statusCode");
        assertEquals(statusCode, 0);
        String statusMsg= JsonPath.read(resp,"$.statusMessage");
        assertEquals(statusMsg, "successfully done");
    }

    @Test(dataProvider = "readLayoutType",groups = {"sanity","smoke","nevia","presentational"},description = "Verify read layout response updates in cache")
    public void verifyLayoutRedis( String lat , String lng) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = menuMerchandisingHelper.readLayoutType(new String[]{restId});
        String resp = processor.ResponseValidator.GetBodyAsText();
        String layoutType= JsonPath.read(resp,"$.data..layoutType").toString().replace("[","").replace("]","").replace("\"", "");
        String Redis = menuMerchandisingHelper.getLayoutTypeFromRedis(restId);
        String layoutTypeRedis = JsonPath.read(Redis,"$.layoutType");
        Assert.assertEquals(layoutType,layoutTypeRedis,"Layout type does not matches");
    }

    @Test(dataProvider = "setLayoutType",groups = {"sanity","smoke","nevia","presentational"},description = "Verify smoke response")
    public void setLayout( String lat , String lng, String [] layoutType , String updatedBy) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = menuMerchandisingHelper.setLayoutType(new String[]{restId,t(layoutType),updatedBy});
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode= JsonPath.read(resp,"$.statusCode");
        assertEquals(statusCode, 0);
        String statusMsg= JsonPath.read(resp,"$.statusMessage");
        assertEquals(statusMsg, "successfully done");
        String data= JsonPath.read(resp,"$.data");
        assertEquals(data, "Layouts are successfully set");
    }

    @Test(dataProvider = "setLayoutType",groups = {"sanity","smoke","nevia","presentational"},description = "Verify  response of api updates in cache")
    public void verifyLayoutRedis( String lat , String lng, String [] layoutType , String updatedBy) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = menuMerchandisingHelper.setLayoutType(new String[]{restId,t(layoutType),updatedBy});
        String resp = processor.ResponseValidator.GetBodyAsText();
        String Redis= menuMerchandisingHelper.getLayoutTypeFromRedis(restId);
        String layoutTypeRedis = JsonPath.read(Redis,"$.layoutType");
        Assert.assertEquals(t(layoutType),layoutTypeRedis,"Layout type does not matches");
    }

    @Test(dataProvider = "sectionConfigLayoutType",groups = {"sanity","smoke","nevia","presentational"},description = "Verify smoke test ")
    public void sectionConfigLayoutType(  String  layoutType ) {
        Processor resp = menuMerchandisingHelper.readSectionConfigAsPerLayoutType(layoutType);
        String response = resp.ResponseValidator.GetBodyAsText();
        int statusCode= JsonPath.read(response,"$.statusCode");
        assertEquals(statusCode, 0);
        String statusMsg= JsonPath.read(response,"$.statusMessage");
        assertEquals(statusMsg, "successfully done");
    }


    @Test(dataProvider = "sectionConfigLayoutType",groups = {"sanity","smoke","nevia","presentational"},description = "Verify api response updates data in cache ")
    public void sectionConfigLayoutTypeRedis(  String  layoutType ) {
        Processor resp = menuMerchandisingHelper.readSectionConfigAsPerLayoutType(layoutType);
        String response = resp.ResponseValidator.GetBodyAsText();
        HashMap<String, String> layoutConfigFromRedis = menuMerchandisingHelper.getMerchLayoutMappingConfigFromRedis(layoutType);
        int size = layoutConfigFromRedis.size();
        List<String> apiResp = Arrays.asList(JsonPath.read(response,"$.data.response..id").toString().replace("[","").replace("]",""));
        int sizeApi = apiResp.size();
        Assert.assertEquals(size,sizeApi);
    }

    @Test(dataProvider = "disableLayout",groups = {"sanity","smoke","nevia","presentational"},description = "Verify smoke test ")
    public void disableLayoutType( String  layoutType,String sectionId ) {
        menuMerchandisingHelper.addNewSectionConfig(9999);
        Processor resp = menuMerchandisingHelper.disableLayout(layoutType,sectionId);
        String response = resp.ResponseValidator.GetBodyAsText();
        int statusCode= JsonPath.read(response,"$.statusCode");
        assertEquals(statusCode, 0);
        String statusMsg= JsonPath.read(response,"$.statusMessage");
        assertEquals(statusMsg, "successfully done");
        String data= JsonPath.read(response,"$.data");
        assertEquals(data, "Disabling Done");
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute("delete from menumerch.merchandise_section_config where id = ("+ sectionId+")");
    }

    @Test(dataProvider = "disableLayoutMapping",groups = {"sanity","smoke","nevia","presentational"},description = "Verify layout mapping gets deleted ")
    public void disableLayoutTypeRedis(String lat, String lng, String layoutType, String updatedBy, String updatedOn,String sectionId ,String name, String title, String min, String max, String priority, String enabled) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        menuMerchandisingHelper.setRestLayout(restId,layoutType,updatedBy,updatedOn);
        menuMerchandisingHelper.setSectionConfig(sectionId,name,title,min,max,priority,enabled);
        menuMerchandisingHelper.addNewSectionConfig(9999);
        Processor respo = menuMerchandisingHelper.readSectionConfigAsPerLayoutType(layoutType);
        String response = respo.ResponseValidator.GetBodyAsText();
        HashMap<String, String> layoutConfigFromRedis = menuMerchandisingHelper.getMerchLayoutMappingConfigFromRedis(layoutType);
        int size = layoutConfigFromRedis.size();
        Processor resp = menuMerchandisingHelper.disableLayout(layoutType,sectionId);
        List<String> apiResp = Arrays.asList(JsonPath.read(response,"$.data.response..id").toString().replace("[","").replace("]",""));
        int sizeApi = apiResp.size();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute("delete from menumerch.merchandise_section_config where id = ("+ sectionId+")");
        sqlTemplate.execute("delete from menumerch.restaurant_layout where restaurant_id = ("+ restId+")");
        Assert.assertTrue(size>sizeApi,"Layout mapping did not get deleted");
    }


    @Test(dataProvider = "layoutRefresh",groups = {"sanity","smoke","nevia","presentational"},description = "Verify api response, smoke test")
    public void layoutRefresh( String lat, String lng) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = menuMerchandisingHelper.layoutRefresh(new String[]{restId});
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode= JsonPath.read(resp,"$.statusCode");
        assertEquals(statusCode, 0);
        String statusMsg= JsonPath.read(resp,"$.statusMessage");
        assertEquals(statusMsg, "successfully done");
        String data= JsonPath.read(resp,"$.data");
        assertEquals(data, "Successfully refreshed");
    }

   @Test(dataProvider = "layoutRefresht",groups = {"sanity","smoke","nevia","presentational"},description = "Verify any changes done in db reflects in redis")
    public void layoutRefreshDbRedis( String lat, String lng,String layout_type,String updatedBy, String updatedOn) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        menuMerchandisingHelper.setRestLayout(restId,layout_type,updatedBy,  updatedOn);
        Processor processor = menuMerchandisingHelper.layoutRefresh(new String[]{restId});
        String resp = processor.ResponseValidator.GetBodyAsText();
        String Redis= menuMerchandisingHelper.getLayoutTypeFromRedis(restId);
        String layoutTypeRedis = JsonPath.read(Redis,"$.layoutType");
        Assert.assertEquals(layout_type,layoutTypeRedis);
       SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
       sqlTemplate.execute("delete from menumerch.restaurant_layout where restaurant_id = ("+ restId+")");
    }

    @Test(dataProvider = "sectionConfigLayoutType",groups = {"sanity","smoke","nevia","presentational"},description = "Verify smoke test ")
    public void refreshsectionConfigLayoutType(  String  layoutType ) {
        Processor resp = menuMerchandisingHelper.refreshSectionConfigAsPerLayoutType(layoutType);
        String response = resp.ResponseValidator.GetBodyAsText();
        int statusCode= JsonPath.read(response,"$.statusCode");
        assertEquals(statusCode, 0);
        String statusMsg= JsonPath.read(response,"$.statusMessage");
        assertEquals(statusMsg, "successfully done");
        String data= JsonPath.read(response,"$.data");
        assertEquals(data, "Refreshed Successfully");
    }

    @Test(dataProvider = "sectionConfigLayoutTypeRedis",groups = {"sanity","smoke","nevia","presentational"},description = "Verify any changes done in db reflects in redis ")
    public void refreshsectionConfigLayoutTypeDbRedis(  String  layoutType ,String sectionId,String name, String title, String min, String max, String priority, String enabled) {
        menuMerchandisingHelper.setSectionConfig(sectionId,name,title, min,  max,  priority, enabled);
        Processor resp = menuMerchandisingHelper.refreshSectionConfigAsPerLayoutType(layoutType);
        String response = resp.ResponseValidator.GetBodyAsText();
        String redis =menuMerchandisingHelper.getMerchSectionConfigFromRedis(sectionId);
        System.out.println(redis);
        String id = JsonPath.read(redis,"$.id");
        Assert.assertEquals(sectionId,id);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute("delete from menumerch.merchandise_section_config where id = ("+ sectionId+")");
    }


    public String t(String[] str) {
        String res = str[0];
        for(int i=1;i<str.length;i++) {
            if(str[i] != null)
            res += "\",\"" + str[i];
        }
        return res;
    }


}
