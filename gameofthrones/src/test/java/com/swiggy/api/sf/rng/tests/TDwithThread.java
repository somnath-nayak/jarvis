package com.swiggy.api.sf.rng.tests;

import java.util.Iterator;
import java.util.List;

import org.testng.Assert;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.helper.RngHelper;

import framework.gameofthrones.JonSnow.Processor;

public class TDwithThread extends Thread {

	List l = null;
RngHelper rngHelper = new RngHelper();
	public TDwithThread(List l)
 {
		this.l = l;
 }
	public void run()
	{
		Iterator it = l.iterator();
        while(it.hasNext()){
            Processor processor=rngHelper.createTD(it.next().toString());
			 String response =	processor.ResponseValidator.GetBodyAsText();rngHelper.TDstatusChecker(response);
			 Assert.assertEquals(JsonPath.read(response,"$.statusMessage").toString(), "done");
			 Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");
	
            try {
                Thread.sleep(7000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
	}
}
}
