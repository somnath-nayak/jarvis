package com.swiggy.api.sf.snd.pojo.Daily.DeliveryListing;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.pojo.Daily.DeliveryListing
 **/
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "start_time",
        "end_time"
})
public class Time_slot {

    @JsonProperty("start_time")
    private Long start_time;
    @JsonProperty("end_time")
    private Long end_time;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("start_time")
    public Long getStart_time() {
        return start_time;
    }

    @JsonProperty("start_time")
    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    @JsonProperty("end_time")
    public Long getEnd_time() {
        return end_time;
    }

    @JsonProperty("end_time")
    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    private void setDefaultValues (long start_time, long end_time) {
        if(this.getStart_time() == null)
            this.setStart_time(start_time);
        if(this.getEnd_time() == null)
            this.setEnd_time(end_time);

    }

    public Time_slot build(long start_time, long end_time) {
        setDefaultValues(start_time,end_time);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("start_time", start_time).append("end_time", end_time).append("additionalProperties", additionalProperties).toString();
    }

}
