package com.swiggy.api.projectX.grouporder.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.*;

public class GroupOrderHelper {
    static Initialize gameofthrones = new Initialize();
    static ArrayList<String> tid = new ArrayList<>();
    static ArrayList<String> sid = new ArrayList<>();
    static ArrayList<String> token = new ArrayList<>();
    static ArrayList<String> notLoggedIn = new ArrayList<>();

    SANDHelper sandHelper = new SANDHelper();
    CheckoutHelper checkoutHelper = new CheckoutHelper();


    public Processor login(String mobile, String password) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        String[] payload = {mobile, password};
        GameOfThronesService gots = new GameOfThronesService("sand", "loginV22", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor updateCart(String tid,String sid,String token,String parentCartKey, String cartKey, String restId, int index) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid);
        headers.put("sid", sid);
        headers.put("token", token);
        String[] menu = getMenuItems(restId);
        System.out.println("Menu Size for the restaurant: " + menu.length);
        GameOfThronesService got = new GameOfThronesService("grouporder", "updatecart", gameofthrones);
        String[] payload = {cartKey, parentCartKey, menu[index], restId};
        Processor processor = new Processor(got, headers, payload);
        return processor;
            }

    public void loginUsers(String mobile, String password) {
        //String[] tid_sid_token = {};
        Processor processor = login(mobile, password);
        String body = processor.ResponseValidator.GetBodyAsText();
        String tid_user = JsonPath.read(body, "$..tid").toString().replace("[", "").replace("]", "").replace("\"", "");
        String sid_user = JsonPath.read(body, "$..sid").toString().replace("[", "").replace("]", "").replace("\"", "");
        String token_user = JsonPath.read(body, "$..data.token").toString().replace("[", "").replace("]", "").replace("\"", "");
        int status = JsonPath.read(body, "$.statusCode");
        System.out.println(status);
        if (status == 0) {
//            tid_sid_token[0] = tid_user;
//            tid_sid_token[1] = sid_user;
//            tid_sid_token[2] = token_user;
            tid.add(tid_user);
            sid.add(sid_user);
            token.add(token_user);
        }
        // Debugger
        else
            notLoggedIn.add(mobile);
        System.out.println(tid);
        System.out.println(sid);
        System.out.println(token);
        System.out.println("Users Unable To Log In::: " + notLoggedIn);
    }

   /* public String getOpenRestaurantI(String lat, String lng) {
        String restaurantId = restId(lat,lng);
        System.out.println(restaurantId);
        return restaurantId;

    }*/

    public String[] getMenuItems(String restID) {

        String rest = sandHelper.menuV3(restID).ResponseValidator.GetBodyAsText();
        String[] menu_items = JsonPath.read(rest, "$..data..menu..items[*]..id").toString().replace("[", "").replace("]", "").replace("\"", "").trim().split(",");
        String[] item_prices = JsonPath.read(rest, "$..data..menu..items[*]..price").toString().replace("[", "").replace("]", "").replace("\"", "").trim().split(",");
        ArrayList<String> menu = new ArrayList<>(Arrays.asList(menu_items));
        //ArrayList prices = new ArrayList<>(Arrays.asList(item_prices));
        for (int i = 0; i < menu_items.length; i++) {
            if (Double.parseDouble(item_prices[i]) < 15000.00)
                menu.add(menu_items[i]);
        }
        //debugger
        /*for (String m: menuList) {
            System.out.print("MenuItems:::: "+m);
        }*/
        String[] filtered_menu = new String[menu.size()];
        for (int i = 0; i < menu.size(); i++) {
            filtered_menu[i] = menu.get(i);
        }
        System.out.println("MENU LENGTH FOR ITEMS WITH VALUE < INR 150 ::: " + filtered_menu.length);
        return filtered_menu;
    }

    public Processor initiateCart(String restId, String lat, String lng) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(0));
        System.out.println(tid.get(0));
        headers.put("sid", sid.get(0));
        headers.put("token", token.get(0));
        GameOfThronesService got = new GameOfThronesService("grouporder", "initiate", gameofthrones);
        String[] payload = {restId, lat, lng};
        Processor processor = new Processor(got, headers, payload);
        return processor;
    }

    public Processor joinCart(String parentCartKey, int index) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(index));
        headers.put("sid", sid.get(index));
        headers.put("token", token.get(index));
        GameOfThronesService got = new GameOfThronesService("grouporder", "joincart", gameofthrones);
        String[] payload = {parentCartKey};
        Processor processor = new Processor(got, headers, payload);
        return processor;

    }

    public int totalUsersLoggedIn() {
        // User 0 Is always the host
        return token.size();
    }

    public Processor userCartMapping(String parentCartKey, int index) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(index));
        headers.put("sid", sid.get(index));
        headers.put("token", token.get(index));
        GameOfThronesService got = new GameOfThronesService("grouporder", "usercartmapping", gameofthrones);
        String[] queryparams = {parentCartKey};
        Processor processor = new Processor(got, headers, null, queryparams);
        return processor;
    }

    public String[] getTidTokenSid(int index) {
        String[] userDetails = {tid.get(index), token.get(index), sid.get(index)};
        return userDetails;
    }

    public Processor updateCart(String parentCartKey, String cartKey, String restId, int index) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(index));
        headers.put("sid", sid.get(index));
        headers.put("token", token.get(index));
        String[] menu = getMenuItems(restId);
        System.out.println("--tid: " + tid.get(index) + " --sid: " + sid.get(index) + " --token: " + token.get(index));
        System.out.println("Menu Size for the restaurant: " + menu.length);
        GameOfThronesService got = new GameOfThronesService("grouporder", "updatecart", gameofthrones);
        String[] payload = {cartKey, parentCartKey, menu[index], restId};
        Processor processor = new Processor(got, headers, payload);
        return processor;
    }


    public String getOpenRestaurantId(String lat, String lng) {
        String[] p = new String[]{lat, lng};
        String resp = sandHelper.aggregator(p).ResponseValidator.GetBodyAsText();
        String restIds = JsonPath.read(resp, "$.data.restaurants[*].id").toString().replace("[", "").replace("]", "")
                .replace("\"", "");
        // System.out.println(restIds);
        List<String> list = Arrays.asList(restIds.split(","));
        List<String> openRest = new ArrayList<>();
        String open = JsonPath.read(resp, "$.data.restaurants[*].availability.opened").toString().replace("[", "")
                .replace("]", "");
        List<String> openList = Arrays.asList(open.split(","));
        for (int i = 0; i < openList.size(); i++) {
            if (openList.get(i).equalsIgnoreCase("true")) {
                openRest.add(list.get(i));
            }
        }
        //int random = new Random().nextInt(openRest.size());
        //return openRest.get(random);
        int i = 0;
        while (i <= openRest.size()) {
            if (checkoutHelper.menuItemsList(openRest.get(i)) <= 5) {
                openRest.remove(i);
                i++;
            } else {
                break;
            }
        }
        return openRest.get(new Random().nextInt(openRest.size()));
        //return openRest.get(0);
    }

    public Processor closeCart(String parentCartKey, String cartKey) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(0));
        headers.put("sid", sid.get(0));
        headers.put("token", token.get(0));
        System.out.println("--tid: " + tid.get(0) + " --sid: " + sid.get(0) + " --token: " + token.get(0));
        GameOfThronesService got = new GameOfThronesService("grouporder", "closecart", gameofthrones);
        String[] payload = {cartKey, parentCartKey};
        Processor processor = new Processor(got, headers, payload);
        return processor;
    }
    //@Overload
    public Processor closeCart(String tid,String sid,String token,String parentCartKey, String cartKey) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid);
        headers.put("sid", sid);
        headers.put("token", token);
        GameOfThronesService got = new GameOfThronesService("grouporder", "closecart", gameofthrones);
        String[] payload = {cartKey, parentCartKey};
        Processor processor = new Processor(got, headers, payload);
        return processor;
    }

    public Processor placeOrder(String addressId, String parentCartKey) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(0));
        headers.put("sid", sid.get(0));
        headers.put("token", token.get(0));
        System.out.println("--tid: " + tid.get(0) + " --sid: " + sid.get(0) + " --token: " + token.get(0));
        System.out.println("Address ID in constants ---> " + addressId + " -- new ParentCartKey ---> " + parentCartKey);
        GameOfThronesService got = new GameOfThronesService("grouporder", "placeorder", gameofthrones);
        String[] payload = {addressId, parentCartKey};
        Processor processor = new Processor(got, headers, payload);
        return processor;
    }

    public Processor getOrderDetails(String orderId, int index) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(index));
        headers.put("sid", sid.get(index));
        headers.put("token", token.get(index));
        System.out.println("--tid: " + tid.get(index) + " --sid: " + sid.get(index) + " --token: " + token.get(index));
        System.out.println("OrderID is ---> " + orderId);
        GameOfThronesService got = new GameOfThronesService("grouporder", "orderdetails", gameofthrones);
        String[] queryparams = {orderId};
        Processor processor = new Processor(got, headers, null, queryparams);
        return processor;
    }

    public Processor trackOrder(String parentCartKey, String orderId, int index) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(index));
        headers.put("sid", sid.get(index));
        headers.put("token", token.get(index));
        System.out.println("--tid: " + tid.get(index) + " --sid: " + sid.get(index) + " --token: " + token.get(index));
        System.out.println("OrderID is ---> " + orderId + "new ParentCartKey ---> " + parentCartKey);
        GameOfThronesService got = new GameOfThronesService("grouporder", "trackorder", gameofthrones);
        String[] queryparams = {parentCartKey, orderId};
        Processor processor = new Processor(got, headers, null, queryparams);
        return processor;
    }

    public Processor cartStatus(String parentCartKey, int index) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(index));
        headers.put("sid", sid.get(index));
        headers.put("token", token.get(index));
        System.out.println("--tid: " + tid.get(index) + " --sid: " + sid.get(index) + " --token: " + token.get(index));
        System.out.println("ParentCartKey ---> " + parentCartKey);
        GameOfThronesService got = new GameOfThronesService("grouporder", "cartstatus", gameofthrones);
        String[] queryparams = {parentCartKey};
        Processor processor = new Processor(got, headers, null, queryparams);
        return processor;
    }

    public Processor discardCart(String parentCartKey, int index) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(index));
        headers.put("token", token.get(index));
        System.out.println("--tid: " + tid.get(index) + " --sid: " + sid.get(index) + " --token: " + token.get(index));
        System.out.println("ParentCartKey ---> " + parentCartKey);
        GameOfThronesService got = new GameOfThronesService("grouporder", "discard", gameofthrones);
        String[] params = {parentCartKey};
        Processor processor = new Processor(got, headers, params);
        return processor;
    }

    public Processor leaveCart(String parentCartKey, String cartKey, int index) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("tid", tid.get(index));
        headers.put("token", token.get(index));
        System.out.println("--tid: " + tid.get(index) + " --sid: " + sid.get(index) + " --token: " + token.get(index));
        System.out.println("ParentCartKey ---> " + parentCartKey);
        GameOfThronesService got = new GameOfThronesService("grouporder", "leavecart", gameofthrones);
        String[] params = {cartKey,parentCartKey};
        Processor processor = new Processor(got, headers, params);
        return processor;
    }






}

