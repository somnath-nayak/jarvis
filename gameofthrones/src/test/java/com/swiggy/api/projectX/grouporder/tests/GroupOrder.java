package com.swiggy.api.projectX.grouporder.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.projectX.grouporder.constants.GroupOrderConstants;
import com.swiggy.api.projectX.grouporder.dp.GroupOrderDP;
import com.swiggy.api.projectX.grouporder.helper.GroupOrderHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GroupOrder extends GroupOrderDP {

    GroupOrderHelper groupOrderHelper = new GroupOrderHelper();
    private static Logger log = LoggerFactory.getLogger(GroupOrder.class);

    String[] menuItemslist;
    String restaurantId;
    String parentCartKey;
    String orderId;
    String billAmount;
    List<String> cartKey = new ArrayList<>();
    List<String> users = new ArrayList<>();
    List<String> g_cart_items = new ArrayList<>();
    

    @Test(dataProvider = "login", priority = 1, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"})
    public void loginUsers(String mobile, String password) {
        groupOrderHelper.loginUsers(mobile, password);
    }

    @Test(priority = 2, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"loginUsers"}, enabled = true)
    public void initiateCart() {
        SoftAssert softAssert = new SoftAssert();
        //Login All Users and Collect Data
        restaurantId = groupOrderHelper.getOpenRestaurantId(GroupOrderConstants.lat,GroupOrderConstants.lng);
        //menuItemslist = groupOrderHelper.getMenuItems(restaurantId);
        //Initiate Cart And Validate
        String body = groupOrderHelper.initiateCart(restaurantId,GroupOrderConstants.lat,GroupOrderConstants.lng).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body,"$.statusCode");
        String message = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String restId = JsonPath.read(body, "$..data..groupCart.restId").toString().replace("[","").replace("]","").trim();
        String cartStatus = JsonPath.read(body, "$..data..groupCart..cartStatus").toString().replace("[","").replace("]","").trim();
        parentCartKey = JsonPath.read(body,"$..data..groupCart..parentCartKey").toString().replace("[","").replace("]","").replace("\"","").trim();
        String cartKey_host = JsonPath.read(body,"$..data..groupCart..cartKey").toString().replace("[","").replace("]","").replace("\"","").trim();
        String getUser = JsonPath.read(body, "$..data..groupCart..userName").toString().replace("[","").replace("]","").replace("\"","").trim();
        users.add(getUser);
        cartKey.add(cartKey_host);
        parentCartKey = JsonPath.read(body,"$..data..groupCart..parentCartKey").toString().replace("[","").replace("]","").replace("\"","").trim();
        log.info("ParentCartKey::: "+parentCartKey);
        String sharingUrl = JsonPath.read(body,"$..data..groupCart..sharingUrl").toString().replace("[","").replace("]","").trim();
        softAssert.assertEquals(status,GroupOrderConstants.statusCode, "Status Code is not Zero");
        softAssert.assertEquals(message,"Cart initiated for group ordering");
        softAssert.assertEquals(cartStatus,GroupOrderConstants.openCartStatus,"Host - Cart Status is not Marked Open");
        softAssert.assertNotNull(sharingUrl, "Sharing URL is null");
        softAssert.assertEquals(restId,restaurantId,"Restaurant ID not similar");
        //debugger
        softAssert.assertAll();
        log.info("Users After InitiateCart: "+users);

    }

    @Test(priority = 3, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"initiateCart"}, enabled = true)
    public void joinCart() {
        SoftAssert softAssert = new SoftAssert();
        int totalUsers = groupOrderHelper.totalUsersLoggedIn();
        //User index 0 is always the host so guest starts from 1
        //Debugger
        //log.info("totalusers:::"+totalUsers);
        //log.info("ParentCartKey::: "+parentCartKey);
        for (int i = 1; i < totalUsers; i++) {
            Processor processor = groupOrderHelper.joinCart(parentCartKey,i);
            String body = processor.ResponseValidator.GetBodyAsText();
            int status = JsonPath.read(body,"$.statusCode");
            String message = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
            String restId = JsonPath.read(body, "$..data..groupCart..restId").toString().replace("[","").replace("]","").trim();
            String cartStatus = JsonPath.read(body, "$..data..groupCart..cartStatus").toString().replace("[","").replace("]","").trim();
            String cartKey_guest = JsonPath.read(body,"$..data..groupCart..cartKey").toString().replace("[","").replace("]","").replace("\"","").trim();
            //String cartKey_host = JsonPath.read(body,"$..data..groupCart..cartKey").toString().replace("[","").replace("]","").trim();
            String getUser = JsonPath.read(body, "$..data..groupCart..userName").toString().replace("[","").replace("]","").replace("\"","").trim();
            cartKey.add(cartKey_guest);
            users.add(getUser);
            String sharingUrl = JsonPath.read(body,"$..data..groupCart..sharingUrl").toString().replace("[","").replace("]","").trim();
            softAssert.assertEquals(status,GroupOrderConstants.statusCode, "Status Code is not Zero");
            softAssert.assertEquals(message,"done successfully");
            softAssert.assertEquals(cartStatus,GroupOrderConstants.openCartStatus,"Guest "+getUser+": - Cart Status is not Marked Open");
            softAssert.assertNotNull(sharingUrl, "Sharing URL is null");
            softAssert.assertEquals(restId,restaurantId,"Restaurant ID not similar");
            //debugger
            log.info("CartKeys: ----> "+cartKey);
            log.info("Users After Join Cart: "+users);
        }
        softAssert.assertAll();
    }

    @Test(priority = 4, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"joinCart"}, enabled = true)
    public void validateUsersInGroup() {
        SoftAssert softAssert = new SoftAssert();
        int totalUsers = groupOrderHelper.totalUsersLoggedIn();
        Processor processor = groupOrderHelper.joinCart(parentCartKey,(totalUsers-1));
        String body = processor.ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body,"$.statusCode");
        softAssert.assertEquals(status,GroupOrderConstants.statusCode, "Status Code is not Zero");
        String[] allgroupMembers = JsonPath.read(body,"$..data..groupCartMetaData.users[*]").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");
        //debugger
        int len = allgroupMembers.length;
        softAssert.assertEquals(len,totalUsers,"Total No of Users in the group are not same");
        log.info("USERS......:"+users);
        String[] userList = users.toArray(new String[users.size()]);
        Arrays.sort(userList);
        for (int i = 0; i < len ; i++) {
           log.info("allGroupMembers::: "+allgroupMembers[i]);
           int index = Arrays.binarySearch(userList,allgroupMembers[i]);
           softAssert.assertTrue((index >= 0));
           // log.info("index----------->>>"+index);
        }
        softAssert.assertAll();
    }

    @Test(priority = 5, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"initiateCart"}, enabled = true)
    public void guestJoinCartMultipleTimes() {
        SoftAssert softAssert = new SoftAssert();
        int totalUsers = groupOrderHelper.totalUsersLoggedIn();
        int count = 0;
        if (count < 3) {
            Processor processor = groupOrderHelper.joinCart(parentCartKey, (totalUsers - 1));
            String body = processor.ResponseValidator.GetBodyAsText();
            int status = JsonPath.read(body, "$.statusCode");
            String restId = JsonPath.read(body, "$..data..groupCart..restId").toString().replace("[", "").replace("]", "").trim();
            String cartStatus = JsonPath.read(body, "$..data..groupCart..cartStatus").toString().replace("[", "").replace("]", "").trim();
            String cartKey_guest = JsonPath.read(body, "$..data..groupCart..cartKey").toString().replace("[", "").replace("]", "").replace("\"","").trim();
            String sharingUrl = JsonPath.read(body,"$..data..groupCart..sharingUrl").toString().replace("[","").replace("]","").trim();
            String parentKey = JsonPath.read(body,"$..data..groupCart..parentCartKey").toString().replace("[","").replace("]","").replace("\"","").trim();
            softAssert.assertEquals(restId,restaurantId,"restaurant ID changed");
            softAssert.assertEquals(status, GroupOrderConstants.statusCode, "Status Code is not Zero");
            softAssert.assertNotNull(sharingUrl, "Sharing URL is null");
            softAssert.assertEquals(cartStatus,GroupOrderConstants.openCartStatus,"MultiHit scenario - Guest Cart Status is not Marked Open");
            softAssert.assertEquals(cartKey_guest,cartKey.get(totalUsers - 1), "cart key is different");
            softAssert.assertEquals(parentKey,parentCartKey, "parent cart key changed");
            log.info("Multiple Guest JoinCart Hit Count : "+count);
            ++count;
        }
        softAssert.assertAll();
    }

    @Test(priority = 6, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"loginUsers","initiateCart"}, enabled = true)
    public void closeCart() {
        SoftAssert softAssert = new SoftAssert();
        String body = groupOrderHelper.closeCart(parentCartKey, cartKey.get(0)).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body, "$.statusCode");
        String message = JsonPath.read(body, "$..statusMessage").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
        softAssert.assertEquals(status, GroupOrderConstants.statusCode, "status code did not match");
        softAssert.assertEquals(message, "done successfully", "Message did not match");
        //debugger
        log.info("Cart Closed -- Please re Initiate Cart and Join");
        softAssert.assertAll();
    }

    @Test(priority = 7, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"loginUsers","initiateCart"}, enabled = true)
    public void guestUpdatesAClosedCart() {
        SoftAssert softAssert = new SoftAssert();
        String body = groupOrderHelper.updateCart(parentCartKey, cartKey.get(1),restaurantId,1).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body, "$.statusCode");
        String message = JsonPath.read(body, "$..statusMessage").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
        softAssert.assertEquals(status, GroupOrderConstants.status1115, "status code did not match");
        softAssert.assertEquals(message, "This group order has been closed by host", "Message did not match");
        //debugger
        log.info("Guest tries to update a cart closed by host");
        softAssert.assertAll();
    }



    @Test(priority = 8, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"initiateCart", "loginUsers"}, enabled = true)
    public void reInitiateAndJoinCart() {
        SoftAssert softAssert = new SoftAssert();
        //clearing array list and re-initiating
        users.clear();
        cartKey.clear();
        initiateCart();
        joinCart();
        log.info("Re Initiated & joined Users List ::: "+users);
        log.info("New ParentCartKey ::: "+ parentCartKey);
        log.info("Re Initiated User's CartKeys ::: "+cartKey);
        int len = groupOrderHelper.totalUsersLoggedIn();
        softAssert.assertNotNull(users.size(),"Users list empty");
        softAssert.assertNotNull(cartKey.size(),"CartKey list empty");
        softAssert.assertEquals(users.size(),cartKey.size(), "All cart CartKeys logged");
        log.info("::: Total Logged in users: "+len + " -- Total re-joined users: "+users.size());
        softAssert.assertAll();
    }


  @Test(priority = 9, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"reInitiateAndJoinCart"}, enabled = true)
    public void updateCart() {
      SoftAssert softAssert = new SoftAssert();
      int totalUsers = groupOrderHelper.totalUsersLoggedIn();
      log.info("TOTAL USERS ::: "+totalUsers);
      log.info(" -- ONLY 3 USERS WILL UPDATE CART INCLUDING HOST -- ");
      for (int i = 0; i < 3; i++) {
          String body = groupOrderHelper.updateCart(parentCartKey, cartKey.get(i), restaurantId, i).ResponseValidator.GetBodyAsText();
          int status = JsonPath.read(body, "$.statusCode");
          String message = JsonPath.read(body, "$..statusMessage").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
          String restId = JsonPath.read(body, "$..data..userCartList[*]..cart..restaurant_details.id").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
          String[] cartStatus = JsonPath.read(body, "$..data..userCartList[*]..cartStatus").toString().replace("[", "").replace("]", "").trim().split(",");
          String[] get_parentCartKey = JsonPath.read(body, "$..data..userCartList[*]..parentCartKey").toString().replace("[", "").replace("]", "").replace("\"", "").trim().split(",");
          String[] get_cartKey = JsonPath.read(body, "$..data..userCartList[*]..cartKey").toString().replace("[", "").replace("]", "").replace("\"", "").trim().split(",");
          String mergedCart = JsonPath.read(body, "$..data..mergedCart").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
          String[] cart_menu_items = JsonPath.read(body, "$..data..userCartList[*]..cart..cart_menu_items[*]..menu_item_id").toString().replace("[", "").replace("]", "").replace("\"", "").trim().split(",");
          String[] userName = JsonPath.read(body, "$..data..userCartList[*]..userName").toString().replace("[", "").replace("]", "").replace("\"", "").trim().split(",");
          billAmount = JsonPath.read(body, "$..data..mergedCart..bill_total").toString().replace("[","").replace("]","").replace("\"","").trim();
          //debugger
          log.info("parentCartKey -- "+parentCartKey);
          log.info("cartKey -- "+cartKey.get(i));
          log.info("restId -- "+restId);
          log.info(userName[i] + "-->" + parentCartKey + "-->" + cartKey.get(i) + "-->" + restId);
          for (String s : cart_menu_items) {
              log.info("Cart_Menu_Items::: " + s);
              g_cart_items.add(s);
          }
          for (int j=0; j < get_parentCartKey.length; j++) {
              softAssert.assertEquals(get_parentCartKey[j],parentCartKey);
          }

          softAssert.assertEquals(status, GroupOrderConstants.statusCode, "status code did not match");
          softAssert.assertEquals(message, "Done Successfully", "Message did not match");
          softAssert.assertNotNull(mergedCart);
          softAssert.assertEquals(cartStatus[i], GroupOrderConstants.openCartStatus);
      }
      log.info("TOTAL BILL AMOUNT ::: "+billAmount);
      softAssert.assertAll();
  }

  @Test(priority = 10, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"reInitiateAndJoinCart"}, enabled = true)
    public void getUserCartMapping() {
      SoftAssert softAssert = new SoftAssert();
        int totalUsers = users.size();
        // Hit Api as a host
        String body = groupOrderHelper.userCartMapping(parentCartKey,0).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body, "$.statusCode");
        String message = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        //tid, token, sid
        //String[] userDetails = groupOrderHelper.getTidTokenSid(0);
        String[] allUsersCartStatus = JsonPath.read(body, "$..data..userCartList[*]..cartStatus").toString().replace("[","").replace("]","").trim().split(",");
        String[] usersInGroup = JsonPath.read(body,"$..data..userCartList[*]..userName").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");
        String groupCartMetaData = JsonPath.read(body, "$..data..groupCartMetaData").toString().replace("[","").replace("]","").trim();
        String mergedCart = JsonPath.read(body, "$..data..mergedCart..cart").toString().replace("[","").replace("]","").trim();
        String mergedCart_hostUser = JsonPath.read(body, "$..data..mergedCart..userName").toString().replace("[","").replace("]","").replace("\"","").trim();
        String mergedCart_RestId = JsonPath.read(body, "$..data..mergedCart..cart..restaurant_details..id").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status,GroupOrderConstants.statusCode, "Status Not same");
        softAssert.assertEquals(message,"Done Successfully", "Message not same");
        softAssert.assertEquals(totalUsers,usersInGroup.length, "Total users in group different from users in UserCartMapping API");
        String[] userList = users.toArray(new String[users.size()]);
        Arrays.sort(userList);
        for (int i = 0; i < totalUsers; i++) {
            log.info("Users in the Group::: " + usersInGroup[i]);
            int index = Arrays.binarySearch(userList,usersInGroup[i]);
            softAssert.assertTrue((index >= 0));
            softAssert.assertEquals(GroupOrderConstants.openCartStatus,allUsersCartStatus[i],"Cart is not in OPEN State");
        }
        softAssert.assertNotNull(groupCartMetaData);
        softAssert.assertNotNull(mergedCart);
        softAssert.assertEquals(mergedCart_hostUser,users.get(0),"Merged Cart Host user is different");
        //debugger
        log.info("Restaurant ID:::: "+ restaurantId);
        softAssert.assertEquals(mergedCart_RestId,restaurantId, "Restaurant ID is different in merged Cart");
      softAssert.assertAll();
    }

    @Test(priority = 11, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"reInitiateAndJoinCart","updateCart"}, enabled = true)
    public void placeOrder() {
        SoftAssert softAssert = new SoftAssert();
            String body = groupOrderHelper.placeOrder(GroupOrderConstants.addressId, parentCartKey).ResponseValidator.GetBodyAsText();
            int status = JsonPath.read(body, "$.statusCode");
            String message = JsonPath.read(body, "$..statusMessage").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            orderId = JsonPath.read(body, "$..data..order_id").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String get_address_id = JsonPath.read(body, "$..delivery_address..id").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String get_address_lat = JsonPath.read(body, "$..delivery_address..lat").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String get_address_lng = JsonPath.read(body, "$..delivery_address..lng").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String get_parent_cart_key = JsonPath.read(body, "$..data..parentCartKey").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String get_parent_user_display_name = JsonPath.read(body, "$..data..parentUserDisplayName").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String get_payment_status = JsonPath.read(body, "$..data..payment").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String get_payment_method = JsonPath.read(body, "$..data..payment_method").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String get_success_Message = JsonPath.read(body, "$..data..success_message").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String get_payment_txn_status = JsonPath.read(body, "$..data..payment_txn_status").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
            String[] get_order_items = JsonPath.read(body, "$..data..order_items[*]..item_id").toString().replace("[", "").replace("]", "").replace("\"", "").trim().split(",");
            String[] get_added_by_username = JsonPath.read(body, "$..data..order_items[*]..added_by_username").toString().replace("[", "").replace("]", "").replace("\"", "").trim().split(",");
            int order_items_len = get_order_items.length;
            int get_added_by_username_len = get_added_by_username.length;
            log.info("order_items_len ::: " + order_items_len + " get_added_by_username_len ::: " + get_added_by_username_len);
            softAssert.assertEquals(status, GroupOrderConstants.statusCode, "status code did not match");
            softAssert.assertEquals(message, "done successfully", "Message did not match");
            softAssert.assertEquals(get_address_id, GroupOrderConstants.addressId);
            softAssert.assertEquals(get_address_lat, GroupOrderConstants.lat);
            softAssert.assertEquals(get_address_lng, GroupOrderConstants.lng);
            softAssert.assertEquals(get_parent_cart_key, parentCartKey);
            softAssert.assertEquals(get_parent_user_display_name, users.get(0));
            softAssert.assertEquals(get_payment_method, "Cash", "Payment method not right");
            softAssert.assertEquals(get_payment_status, "successful", "payment status issue");
            softAssert.assertEquals(get_success_Message, "Your order will be delivered shortly. Keep Swiggying!");
            softAssert.assertEquals(get_payment_txn_status, "success", "payment txn status issue");
            softAssert.assertTrue((order_items_len >= 1));
            softAssert.assertTrue((get_added_by_username_len >= 1));
            softAssert.assertEquals(get_added_by_username_len, order_items_len);
            log.info("orderId ::: " + orderId);
        softAssert.assertAll();
        }

    @Test(priority = 12, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"reInitiateAndJoinCart","placeOrder"}, enabled = true)
    public void getOrderDetails() {
        SoftAssert softAssert = new SoftAssert();
        String body = groupOrderHelper.getOrderDetails(orderId,0).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body, "$.statusCode");
        String get_message = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_parent_user_display_name = JsonPath.read(body,"$..data..groupOrderDisplayTexts.parentDisplayName").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_total_bill_amnt= JsonPath.read(body,"$..data..groupOrderDisplayTexts..totalBill").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_total_amnt_owed_by_guests= JsonPath.read(body,"$..data..groupOrderDisplayTexts..owedByGuests").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_total_amnt_owed_by_parent= JsonPath.read(body,"$..data..groupOrderDisplayTexts..owedByParent").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_total_items= JsonPath.read(body,"$..data..groupOrderDisplayTexts..totalItems").toString().replace("[","").replace("]","").replace("\"","").trim();
        String[] get_added_by_username = JsonPath.read(body,"$..data..orderDetails[*]..orderCartItems[*]..added_by_username").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");
        String[] get_order_items = JsonPath.read(body,"$..data..order_items[*]..item_id").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");
        int order_items_len =  get_order_items.length;
        int get_added_by_username_len = get_added_by_username.length;
        log.info("orderId ::: "+orderId);
        double bill_amount = Double.parseDouble(get_total_bill_amnt);
        double owed_by_guests = Double.parseDouble(get_total_amnt_owed_by_guests);
        double owed_by_parent = Double.parseDouble(get_total_amnt_owed_by_parent);
        int total_items = Integer.parseInt(get_total_items);
        softAssert.assertEquals(status, GroupOrderConstants.statusCode, "status code did not match");
        softAssert.assertEquals(get_message, "done successfully,Done successfully", "Message did not match");
        softAssert.assertTrue((order_items_len >=1));
        softAssert.assertTrue((get_added_by_username_len >=1));
        softAssert.assertTrue((bill_amount>0.00));
        softAssert.assertTrue((owed_by_guests>0.00));
        softAssert.assertTrue((owed_by_parent>0.00));
        softAssert.assertTrue((total_items>0));
        softAssert.assertEquals(get_parent_user_display_name,users.get(0));
        softAssert.assertAll();

    }

    @Test(priority = 13, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, dependsOnMethods = {"reInitiateAndJoinCart","placeOrder"}, enabled = true)
    public void trackOrder() {
        SoftAssert softAssert = new SoftAssert();
        String body = groupOrderHelper.trackOrder(parentCartKey,orderId,0).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body, "$.statusCode");
        String message = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_lat_add = JsonPath.read(body, "$..data..billing_address..lat").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_lng_add = JsonPath.read(body, "$..data..billing_address..lng").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_lat_rest = JsonPath.read(body, "$..data..restaurant..lat").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_lng_rest = JsonPath.read(body, "$..data..restaurant..lat").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status, GroupOrderConstants.statusCode, "status code did not match");
        softAssert.assertEquals(message, "done successfully", "Message did not match");
        softAssert.assertEquals(get_lat_add,GroupOrderConstants.lat);
        softAssert.assertEquals(get_lng_add,GroupOrderConstants.lng);
        softAssert.assertNotNull(get_lat_rest);
        softAssert.assertNotNull(get_lng_rest);
        softAssert.assertNotEquals(get_lat_rest,"");
        softAssert.assertNotEquals(get_lng_rest,"");
        log.info("ADD_LAT :::  "+ get_lat_add + "  ADD_LNG ::: "+get_lng_add);
        softAssert.assertAll();
    }

    @Test(priority = 14, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, enabled = true)
    public void leaveCart() {
        SoftAssert softAssert = new SoftAssert();
        reInitiateAndJoinCart();
        //guest leave
        String body1 = groupOrderHelper.leaveCart(parentCartKey, cartKey.get(1),1).ResponseValidator.GetBodyAsText();
        int status1 = JsonPath.read(body1, "$.statusCode");
        String message1 = JsonPath.read(body1, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field1 = JsonPath.read(body1, "$..data").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status1, GroupOrderConstants.statusCode, "status code did not match");
        softAssert.assertEquals(message1, "done successfully", "Message did not match");
        softAssert.assertEquals(data_field1,"null");
        //host leave
        String body2 = groupOrderHelper.leaveCart(parentCartKey, parentCartKey,0).ResponseValidator.GetBodyAsText();
        int status2 = JsonPath.read(body2, "$.statusCode");
        String message2 = JsonPath.read(body2, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field2 = JsonPath.read(body2, "$..data").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status2, GroupOrderConstants.status1125, "status code 1125 did not match");
        softAssert.assertEquals(message2, "You are not permitted to leave this cart", "Message did not match");
        softAssert.assertEquals(data_field2,"null");
        //same guest tries to again leave
        String body3 = groupOrderHelper.leaveCart(parentCartKey, cartKey.get(1),1).ResponseValidator.GetBodyAsText();
        int status3 = JsonPath.read(body3, "$.statusCode");
        String message3 = JsonPath.read(body3, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field3 = JsonPath.read(body3, "$..data").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status3, GroupOrderConstants.status1115, "status code 1115 did not match");
        softAssert.assertEquals(message3, "Cart is already closed", "Message did not match");
        softAssert.assertEquals(data_field3,"null");
        //left guest tries to update cart
        String body_update = groupOrderHelper.updateCart(parentCartKey,cartKey.get(1),restaurantId,1).ResponseValidator.GetBodyAsText();
        int status_update = JsonPath.read(body_update, "$.statusCode");
        String message_update = JsonPath.read(body_update, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status_update, GroupOrderConstants.status1123, "status code 1123 did not match");
        softAssert.assertEquals(message_update, "you have left this swiggy social order, join again", "Message did not match");

    }

    @Test(priority = 15, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, enabled = true)
    public void discardCart() {
        SoftAssert softAssert = new SoftAssert();
        reInitiateAndJoinCart();
        //discard by guest
        String body1 = groupOrderHelper.discardCart(parentCartKey,1).ResponseValidator.GetBodyAsText();
        int status1 = JsonPath.read(body1, "$.statusCode");
        String message1 = JsonPath.read(body1, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status1, GroupOrderConstants.status1125, "status code 1125 did not match");
        softAssert.assertEquals(message1, "You are not permitted to flush this cart", "Message did not match");
        //discard by host
        String body2 = groupOrderHelper.discardCart(parentCartKey,0).ResponseValidator.GetBodyAsText();
        int status2 = JsonPath.read(body2, "$.statusCode");
        String message2 = JsonPath.read(body2, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field2 = JsonPath.read(body2, "$..data").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status2, GroupOrderConstants.statusCode, "status code did not match");
        softAssert.assertEquals(message2, "done successfully", "Message did not match");
        softAssert.assertEquals(data_field2,"null");
    }


    @Test(priority = 16, groups = {"projectX","grouporder","aviral","groupordersanity", "grouporderregression"}, enabled = true)
    public void cartStatus() {
        SoftAssert softAssert = new SoftAssert();
        reInitiateAndJoinCart();
        updateCart();
        //status from host
        String body = groupOrderHelper.cartStatus(parentCartKey,0).ResponseValidator.GetBodyAsText();
        int status_host = JsonPath.read(body, "$.statusCode");
        String message_host = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String status_active_host = JsonPath.read(body, "$..data.status").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status_host, GroupOrderConstants.statusCode, "status code did not match");
        softAssert.assertEquals(message_host, "done successfully", "Message did not match");
        softAssert.assertEquals(status_active_host,"2","status not active");
        //status from guest
        String body_g = groupOrderHelper.cartStatus(parentCartKey,1).ResponseValidator.GetBodyAsText();
        int status_guest = JsonPath.read(body_g, "$.statusCode");
        String message_guest = JsonPath.read(body_g, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String status_active_guest = JsonPath.read(body_g, "$..data.status").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status_active_guest,"2","guest status not active");
        softAssert.assertEquals(status_guest, GroupOrderConstants.statusCode, "guest status code did not match");
        softAssert.assertEquals(message_guest, "done successfully", "guest Message did not match");
        //place order
        placeOrder();
        String body1 = groupOrderHelper.cartStatus(parentCartKey,0).ResponseValidator.GetBodyAsText();
        int status1 = JsonPath.read(body1, "$.statusCode");
        String message1 = JsonPath.read(body1, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String status_placed_host = JsonPath.read(body1, "$..data.status").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status_placed_host,"1","cart status is not placed order - 1");
        softAssert.assertEquals(status1, GroupOrderConstants.statusCode, "guest status code did not match");
        softAssert.assertEquals(message1, "done successfully", "guest Message did not match");
        //discard status
        discardCart();
        String body2 = groupOrderHelper.cartStatus(parentCartKey,0).ResponseValidator.GetBodyAsText();
        int status2 = JsonPath.read(body2, "$.statusCode");
        String message2 = JsonPath.read(body2, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String status_discard_host = JsonPath.read(body2, "$..data.status").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status_discard_host,"0","cart status is not discarded order - 0");
        softAssert.assertEquals(status2, GroupOrderConstants.statusCode, "guest status code did not match");
        softAssert.assertEquals(message2, "done successfully", "guest Message did not match");
        softAssert.assertAll();

    }


}
