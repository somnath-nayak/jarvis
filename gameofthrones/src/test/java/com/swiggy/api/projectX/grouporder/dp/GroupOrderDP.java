package com.swiggy.api.projectX.grouporder.dp;

import org.testng.annotations.DataProvider;

public class GroupOrderDP {
    @DataProvider(name = "login")
    public Object[][] login() {
        return new Object[][] {
                {"9008738199", "swiggy"},
                {"8431137043", "srishty16"},
                {"7406734416", "rkonowhere"},
                {"9886373389", "passw0rd"},
                {"7760677755", "swiggy"}
        };
    }


}
