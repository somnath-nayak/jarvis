package com.swiggy.api;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.List;
import java.util.Map;

public class DBHelp {
    public List<Map<String, Object>> dbs(String cms) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("SELECT DISTINCT TABLE_NAME, INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS order by table_name;");
        return list;
    }

    public List<Map<String, Object>> schema(String cms, String tableName) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("SELECT * FROM INFORMATION_SCHEMA.STATISTICS where index_name ='PRIMARY' and table_name ='"+tableName);
        return list;
    }

    public List<Map<String, Object>> INFORMATION_SCHEMAS(String cms) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("SELECT * FROM INFORMATION_SCHEMA.STATISTICS;");
        return list;
    }

    public List<Map<String,Object>> Schema(String cms){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("SELECT DISTINCT TABLE_SCHEMA FROM INFORMATION_SCHEMA.STATISTICS;");
        return list;
    }

    public List<Map<String,Object>> tableNAme(String cms, String db){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("SELECT DISTINCT table_name FROM INFORMATION_SCHEMA.STATISTICS where TABLE_SCHEMA='"+db+"'");
        return list;
    }

    public List<Map<String,Object>> SchemaAndTable(String cms, String db, String tableName){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("SELECT * FROM INFORMATION_SCHEMA.STATISTICS where TABLE_SCHEMA='"+db+"'"+" and table_name='"+tableName+"'");
        return list;
    }

    public List<Map<String,Object>> SchemaAndColumn(String cms, String db, String tableName){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.STATISTICS where TABLE_SCHEMA='"+db+"'"+" and table_name='"+tableName+"'");
        return list;
    }

    public List<Map<String,Object>> IndexData(String cms){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("SELECT * FROM INFORMATION_SCHEMA.STATISTICS GROUP BY TABLE_NAME, INDEX_NAME;");
        return list;
    }

    public List<Map<String, Object>> UserPrivlege(String cms){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("select distinct grantee from INFORMATION_SCHEMA.user_privileges;");
        return list;
    }

    public List<Map<String, Object>> Users(String cms){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(cms);
        List<Map<String, Object>> list = sqlTemplate.queryForList("select distinct grantee from INFORMATION_SCHEMA.user_privileges where grantee not like '%''swiggy''%';");
        return list;
    }

}
