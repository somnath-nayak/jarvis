package com.swiggy.api.erp.ff.tests.verificationService;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.constants.VerificationServiceConstants;
import com.swiggy.api.erp.ff.dp.verificationService.LiveOrderDetailsData;
import com.swiggy.api.erp.ff.helper.ContextualCancellationHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.VerificationServiceHelper;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;

public class LiveOrderDetailsTests extends LiveOrderDetailsData implements VerificationServiceConstants {

	VerificationServiceHelper helper = new VerificationServiceHelper();
	OMSHelper omsHelper = new OMSHelper();
	LOSHelper losHelper = new LOSHelper();
	ContextualCancellationHelper contextHelper = new ContextualCancellationHelper();
	
	@AfterMethod
	public void sleep() throws InterruptedException{
		System.out.println("######### Giving a sleep ##########");
		Thread.sleep(6000);
		System.out.println("######### sleep time over ##########");
	}
	
	@Test(priority = 1, groups = {"sanity", "regression"},description = "Create a manual order and verify details in fact table")
	public void manualOrderDetailsTest() throws Exception{
		System.out.println("***************************************** manualOrderDetailsTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("manual");
		Assert.assertEquals(helper.getExpectedCustomerId(Long.parseLong((order_id))), helper.getActualCustomerid(Long.parseLong(order_id)), "Customer ID in live order details table did not match with the expected customer ID");
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		
		System.out.println("######################################### manualOrderDetailsTest compleated #########################################");
	}
	
	@Test(priority = 2, groups = {"sanity", "regression"},description = "Create a partner order and verify details in fact table")
	public void partnerOrderDetailsTest() throws Exception{
		System.out.println("***************************************** partnerOrderDetailsTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("partner");
		Assert.assertEquals(helper.getExpectedCustomerId(Long.parseLong(order_id)), helper.getActualCustomerid(Long.parseLong(order_id)), "Customer ID in live order details table did not match with the expected customer ID");
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		
		System.out.println("######################################### partnerOrderDetailsTest compleated #########################################");
	}
	
	@Test(priority = 3, groups = {"sanity", "regression"},description = "Create a third party order and verify details in fact table")
	public void thirdPartyOrderDetailsTest() throws Exception{
		System.out.println("***************************************** thirdPartyOrderDetailsTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("thirdparty");
		Assert.assertEquals(helper.getExpectedCustomerId(Long.parseLong(order_id)), helper.getActualCustomerid(Long.parseLong(order_id)), "Customer ID in live order details table did not match with the expected customer ID");
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		
		System.out.println("######################################### thirdPartyOrderDetailsTest compleated #########################################");
	}
	
	@Test(priority = 4, groups = {"sanity", "regression"},description = "Create a long distance order and verify details in fact table")
	public void longDistanceOrderDetailsTest() throws Exception{
		System.out.println("***************************************** longDistanceOrderDetailsTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("longdistance");
		Assert.assertEquals(helper.getExpectedCustomerId(Long.parseLong(order_id)), helper.getActualCustomerid(Long.parseLong(order_id)), "Customer ID in live order details table did not match with the expected customer ID");
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		
		System.out.println("######################################### longDistanceOrderDetailsTest compleated #########################################");
	}
	
	@Test(priority = 5, groups = {"sanity", "regression"},description = "Create a swiggy assured order and verify details in fact table")
	public void swiggyAssuredOrderDetailsTest() throws Exception{
		System.out.println("***************************************** swiggyAssuredOrderDetailsTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("swiggyassured");
		Assert.assertEquals(helper.getExpectedCustomerId(Long.parseLong(order_id)), helper.getActualCustomerid(Long.parseLong(order_id)), "Customer ID in live order details table did not match with the expected customer ID");
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		
		System.out.println("######################################### swiggyAssuredOrderDetailsTest compleated #########################################");
	}
	
	@Test(priority = 6, groups = {"sanity", "regression"},description = "Create a new user order and verify details in fact table")
	public void newUserOrderDetailsTest() throws Exception{
		System.out.println("***************************************** newUserOrderDetailsTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("newuser");
		Assert.assertEquals(helper.getExpectedCustomerId(Long.parseLong(order_id)), helper.getActualCustomerid(Long.parseLong(order_id)), "Customer ID in live order details table did not match with the expected customer ID");
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		
		System.out.println("######################################### newUserOrderDetailsTest compleated #########################################");
	}
	
	@Test(priority = 7, groups = {"sanity", "regression"},description = "Create a pop order and verify details in fact table")
	public void popOrderDetailsTest() throws Exception{
		System.out.println("***************************************** popOrderDetailsTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("pop");
		Assert.assertEquals(helper.getExpectedCustomerId(Long.parseLong(order_id)), helper.getActualCustomerid(Long.parseLong(order_id)), "Customer ID in live order details table did not match with the expected customer ID");
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		
		System.out.println("######################################### popOrderDetailsTest compleated #########################################");
	}
	
	@Test(priority = 8, groups = {"sanity", "regression"},description = "Deliver a manual order and check whether it's entry is deleted from fact table")
	public void manualOrderDetailsAfterDeliveredTest() throws Exception{
		System.out.println("***************************************** manualOrderDetailsAfterDeliveredTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("manual");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		helper.markOrderAsDelivered(order_id);
		Thread.sleep(1000);
		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
		
		System.out.println("######################################### manualOrderDetailsAfterDeliveredTest compleated #########################################");
	}
	
	@Test(priority = 9, groups = {"sanity", "regression"},description = "Deliver a partner order and check whether it's entry is deleted from fact table")
	public void partnerOrderDetailsAfterDeliveredTest() throws Exception{
		System.out.println("***************************************** partnerOrderDetailsAfterDeliveredTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("partner");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		helper.markOrderAsDelivered(order_id);
		Thread.sleep(1000);
		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
		
		System.out.println("######################################### partnerOrderDetailsAfterDeliveredTest compleated #########################################");
	}
	
	@Test(priority = 10, groups = {"sanity", "regression"},description = "Deliver a third party order and check whether it's entry is deleted from fact table")
	public void thirdPartyOrderDetailsAfterDeliveredTest() throws Exception{
		System.out.println("***************************************** thirdPartyOrderDetailsAfterDeliveredTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("thirdparty");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		helper.markOrderAsDelivered(order_id);
		Thread.sleep(1000);
		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
		
		System.out.println("######################################### thirdPartyOrderDetailsAfterDeliveredTest compleated #########################################");
	}
	
	@Test(priority = 11, groups = {"sanity", "regression"},description = "Deliver a long distance order and check whether it's entry is deleted from fact table")
	public void longDistanceOrderDetailsAfterDeliveredTest() throws Exception{
		System.out.println("***************************************** longDistanceOrderDetailsAfterDeliveredTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("longdistance");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		helper.markOrderAsDelivered(order_id);
		Thread.sleep(1000);
		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
		
		System.out.println("######################################### longDistanceOrderDetailsAfterDeliveredTest compleated #########################################");
	}
	
	@Test(priority = 12, groups = {"sanity", "regression"},description = "Deliver a new user order and check whether it's entry is deleted from fact table")
	public void newUserOrderDetailsAfterDeliveredTest() throws Exception{
		System.out.println("***************************************** newUserOrderDetailsAfterDeliveredTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("newuser");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		helper.markOrderAsDelivered(order_id);
		Thread.sleep(1000);
		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
		
		System.out.println("######################################### newUserOrderDetailsAfterDeliveredTest compleated #########################################");
	}
	
	@Test(priority = 13, groups = {"sanity", "regression"},description = "Deliver a pop order and check whether it's entry is deleted from fact table")
	public void popOrderDetailsAfterDeliveredTest() throws Exception{
		System.out.println("***************************************** popOrderDetailsAfterDeliveredTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("pop");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		helper.markOrderAsDelivered(order_id);
		Thread.sleep(1000);
		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
		
		System.out.println("######################################### popOrderDetailsAfterDeliveredTest compleated #########################################");
	}
	
	@Test(priority = 14, groups = {"sanity", "regression"},description = "Deliver a swiggy assured order and check whether it's entry is deleted from fact table")
	public void swiggyAssuredDetailsAfterDeliveredTest() throws Exception{
		System.out.println("***************************************** swiggyAssuredDetailsAfterDeliveredTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("swiggyassured");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the live order details table");
		helper.markOrderAsDelivered(order_id);
		Thread.sleep(1000);
		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
		
		System.out.println("######################################### swiggyAssuredDetailsAfterDeliveredTest compleated #########################################");
	}

//	@Test(priority = 15, groups = {"sanity", "regression"}, dataProvider = "orderFromBackend" ,description = "Cancel a manual order and check whether it's entry is deleted from fact table")
//	public void manualOrderDetailsAfterCancellationTest(CreateMenuEntry payload, String disposition, String subDisposition, String cancellationFee, String CancellationFeeApplicability) throws Exception{
//		System.out.println("***************************************** manualOrderDetailsAfterCancellationTest started *****************************************");
//
//		String order_id = omsHelper.getMeAnOrder(payload);
//		System.out.println(order_id);
//		Thread.sleep(1000);
//		contextHelper.cancelOrder(String.valueOf(order_id), disposition, subDisposition, cancellationFee, CancellationFeeApplicability);
//		Thread.sleep(1000);
//		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
//
//		System.out.println("######################################### manualOrderDetailsAfterCancellationTest compleated #########################################");
//	}
//
//	@Test(priority = 16, groups = {"sanity", "regression"}, dataProvider = "orderFromBackend" ,description = "Cancel a partner order and check whether it's entry is deleted from fact table")
//	public void partnerOrderDetailsAfterCancellationTest(CreateMenuEntry payload, String disposition, String subDisposition, String cancellationFee, String CancellationFeeApplicability) throws Exception{
//		System.out.println("***************************************** partnerOrderDetailsAfterCancellationTest started *****************************************");
//
//		String order_id = omsHelper.getMeAnOrder(payload);
//		Thread.sleep(1000);
//		contextHelper.cancelOrder(String.valueOf(order_id), disposition, subDisposition, cancellationFee, CancellationFeeApplicability);
//		Thread.sleep(1000);
//		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
//
//		System.out.println("######################################### partnerOrderDetailsAfterCancellationTest compleated #########################################");
//	}
//
//	@Test(priority = 17, groups = {"sanity", "regression"}, dataProvider = "orderFromBackend" ,description = "Cancel a third party order and check whether it's entry is deleted from fact table")
//	public void thirdPartyOrderDetailsAfterCancellationTest(CreateMenuEntry payload, String disposition, String subDisposition, String cancellationFee, String CancellationFeeApplicability) throws Exception{
//		System.out.println("***************************************** thirdPartyOrderDetailsAfterCancellationTest started *****************************************");
//
//		String order_id = omsHelper.getMeAnOrder(payload);
//		Thread.sleep(1000);
//		contextHelper.cancelOrder(String.valueOf(order_id), disposition, subDisposition, cancellationFee, CancellationFeeApplicability);
//		Thread.sleep(1000);
//		Assert.assertFalse(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order details are not deleted from the live order details table");
//
//		System.out.println("######################################### thirdPartyOrderDetailsAfterCancellationTest compleated #########################################");
//	}
	
	@Test(priority = 18, groups = {"sanity", "regression"},description = "Create a duplicate manual order and check for it's availability in waiting for verifier queue")
	public void duplicateManualOrderTest() throws Exception{
		System.out.println("***************************************** duplicateManualOrderTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("manual");
		String duplicateOrder_id = losHelper.getAnOrder("manual");
		Thread.sleep(1000);
		Assert.assertTrue(helper.queryOrdersInQueue(duplicateOrder_id, getOeRole("Verifier")), "Order is not present in waiting for verifier queue");
		
		System.out.println("######################################### duplicateManualOrderTest compleated #########################################");
	}


	@Test(priority = 19, groups = {"sanity", "regression"},description = "Create a duplicate partner order and check for it's availability in waiting for verifier queue")
	public void duplicatePartnerOrderTest() throws Exception{
		System.out.println("***************************************** duplicatePartnerOrderTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("partner");
		String duplicateOrder_id = losHelper.getAnOrder("partner");
		Thread.sleep(1000);
		Assert.assertTrue(helper.queryOrdersInQueue(duplicateOrder_id, getOeRole("Verifier")), "Order is not present in waiting for verifier queue");
		
		System.out.println("######################################### duplicatePartnerOrderTest compleated #########################################");
	}
	
	@Test(priority = 20, groups = {"sanity", "regression"},description = "Create a duplicate third party order and check for it's availability in waiting for verifier queue")
	public void duplicateThirdPartyOrderTest() throws Exception{
		System.out.println("***************************************** duplicateThirdPartyOrderTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("thirdparty");
		String duplicateOrder_id = losHelper.getAnOrder("thirdparty");
		Thread.sleep(1000);
		Assert.assertTrue(helper.queryOrdersInQueue(duplicateOrder_id, getOeRole("Verifier")), "Order is not present in waiting for verifier queue");
		
		System.out.println("######################################### duplicateThirdPartyOrderTest compleated #########################################");
	}
	
	@Test(priority = 21, groups = {"sanity", "regression"},description = "Create a duplicate long distance order and check for it's availability in waiting for verifier queue")
	public void duplicateLongDistanceOrderTest() throws Exception{
		System.out.println("***************************************** duplicateLongDistanceOrderTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("longdistance");
		String duplicateOrder_id = losHelper.getAnOrder("longdistance");
		Thread.sleep(1000);
		Assert.assertTrue(helper.queryOrdersInQueue(duplicateOrder_id, getOeRole("Verifier")), "Order is not present in waiting for verifier queue");
		
		System.out.println("######################################### duplicateLongDistanceOrderTest compleated #########################################");
	}
	
	@Test(priority = 22, groups = {"sanity", "regression"},description = "Create a new user order and check for it's availability in waiting for verifier queue")
	public void newUserOrderTest() throws Exception{
		System.out.println("***************************************** duplicateLongDistanceOrderTest started *****************************************");
		
//		String order_id = losHelper.getAnOrder("newuser");
		String duplicateOrder_id = losHelper.getAnOrder("newuser");
		Thread.sleep(1000);
		Assert.assertTrue(helper.queryOrdersInQueue(duplicateOrder_id, getOeRole("Verifier")), "Order is not present in waiting for verifier queue");
		
		System.out.println("######################################### duplicateLongDistanceOrderTest compleated #########################################");
	}
	
	@Test(priority = 23, groups = {"sanity", "regression"},description = "Create a duplicate swiggy pop order and check for it's availability in waiting for verifier queue")
	public void duplicatePopOrderTest() throws Exception{
		System.out.println("***************************************** duplicatePopOrderTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("pop");
		String duplicateOrder_id = losHelper.getAnOrder("pop");
		Thread.sleep(1000);
		Assert.assertTrue(helper.queryOrdersInQueue(duplicateOrder_id, getOeRole("Verifier")), "Order is not present in waiting for verifier queue");
		
		System.out.println("######################################### duplicatePopOrderTest compleated #########################################");
	}
	
	@Test(priority = 24, groups = {"sanity", "regression"},description = "Create a duplicate swiggy assured order and check for it's availability in waiting for verifier queue")
	public void duplicateSwiggyAssuredOrderTest() throws Exception{
		System.out.println("***************************************** duplicateSwiggyAssuredOrderTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("swiggyassured");
		String duplicateOrder_id = losHelper.getAnOrder("swiggyassured");
		Thread.sleep(1000);
		Assert.assertTrue(helper.queryOrdersInQueue(duplicateOrder_id, getOeRole("Verifier")), "Order is not present in waiting for verifier queue");
		
		System.out.println("######################################### duplicateSwiggyAssuredOrderTest compleated #########################################");
	}

	@Test(priority = 32, groups = {"sanity", "regression"},description = "Create a duplicateManualOrderTestfor Different Item Idsa manual order and check for it's availability in waiting for verifier queue")
	public void dublicateCheckForOrderWithDifferentItemId() throws Exception{
		System.out.println("***************************************** duplicateManualOrderTestfor Different Item Ids started *****************************************");
        helper.clearFactTable(VerificationServiceConstants.customerIds);
		String order_id = losHelper.getAnOrder("manual");
		String duplicateWithDifferentItemOrder_id = losHelper.getAnOrder("manual2");
		Thread.sleep(1000);
		String metadata = helper.getMetaData(Long.parseLong(duplicateWithDifferentItemOrder_id));
		Assert.assertTrue(metadata.contains("\"verificationRequired\":false")&& metadata.contains("\"duplicateValidation\":false"));
		System.out.println("######################################### duplicateManualOrderTestfor Different Item Ids compleated #########################################");
	}

	@Test(priority = 33, groups = {"sanity", "regression"},description = "dublicateCheckForOrderWithMoreTimeColleapseInbitween")
	public void dublicateCheckForOrderWithMoreTimeColleapseInbitween() throws Exception{
		System.out.println("***************************************** duplicateManualOrderTest for more than configure Time started *****************************************");
		helper.clearFactTable(VerificationServiceConstants.customerIds);
		String order_id = losHelper.getAnOrder("manual");
		Thread.sleep(100);
		String duplicateOrder_id = losHelper.getAnOrder("manual");
		String metadata = helper.getMetaData(Long.parseLong(duplicateOrder_id));
		Assert.assertTrue(metadata.contains("\"verificationRequired\":false")&& metadata.contains("\"duplicateValidation\":false"));
		System.out.println("######################################### duplicateManualOrderTest for more than configure Time compleated #########################################");
	}

	@Test(priority = 25, groups = {"sanity", "regression"},description = "Verify a manual order and check for it's availability in waiting for L1 queue")
	public void manualOrderAfterVerificationTest() throws Exception{
		System.out.println("***************************************** manualOrderAfterVerificationTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("manual");
		Thread.sleep(1000);
		omsHelper.verifyOrder(order_id, verifyAction);
		Thread.sleep(1000);
		Assert.assertTrue(helper.queryOrdersInQueue(order_id, getOeRole("L1Placer")), "Order is not present in  waiting for L1 queue");
		
		System.out.println("######################################### manualOrderAfterVerificationTest compleated #########################################");
	}
	
	@Test(priority = 26, groups = {"sanity", "regression"},description = "Verify a partner order and check if it's relayed to vendor")
	public void partnerOrderAfterVerificationTest() throws Exception{
		System.out.println("***************************************** partnerOrderAfterVerificationTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("partner");
		Thread.sleep(1000);
		omsHelper.verifyOrder(order_id, verifyAction);
		Thread.sleep(1000);
		Assert.assertFalse(helper.queryOrdersInQueue(order_id, "L1Placer"), "Partner order is present in  waiting for L1 queue after verification");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderRelayedToRMS(Long.parseLong(order_id)), "Order has not been relayed to RMS");
		
		System.out.println("######################################### partnerOrderAfterVerificationTest compleated #########################################");
	}
	
	@Test(priority = 27, groups = {"sanity", "regression"},description = "Verify a third party order and check if it's relayed to vendor")
	public void thirdPartyOrderAfterVerificationTest() throws Exception{
		System.out.println("***************************************** thirdPartyOrderAfterVerificationTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("thirdparty");
		Thread.sleep(1000);
		omsHelper.verifyOrder(order_id, verifyAction);
		Thread.sleep(1000);
		Assert.assertFalse(helper.queryOrdersInQueue(order_id, "L1Placer"), "Third party order is present in  waiting for L1 queue after verification");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderRelayedToRMS(Long.parseLong(order_id)), "Order has not been relayed to RMS");
		
		System.out.println("######################################### thirdPartyOrderAfterVerificationTest compleated #########################################");
	}
	
	@Test(priority = 28, groups = {"sanity", "regression"},description = "Verify a long distance order and check if it's relayed to vendor")
	public void longDistanceOrderAfterVerificationTest() throws Exception{
		System.out.println("***************************************** longDistanceOrderAfterVerificationTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("longdistance");
		Thread.sleep(1000);
		omsHelper.verifyOrder(order_id, verifyAction);
		Thread.sleep(1000);
		Assert.assertFalse(helper.queryOrdersInQueue(order_id, "L1Placer"), "Long distance order is present in  waiting for L1 queue after verification");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderRelayedToRMS(Long.parseLong(order_id)), "Order has not been relayed to RMS");
		
		System.out.println("######################################### longDistanceOrderAfterVerificationTest compleated #########################################");
	}
	
	@Test(priority = 29, groups = {"sanity", "regression"},description = "Verify a new user order and check if it's relayed to vendor")
	public void newUserOrderAfterVerificationTest() throws Exception{
		System.out.println("***************************************** newUserOrderAfterVerificationTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("newuser");
		Thread.sleep(1000);
		omsHelper.verifyOrder(order_id, verifyAction);
		Thread.sleep(1000);
		Assert.assertFalse(helper.queryOrdersInQueue(order_id, "L1Placer"), "new user order is present in  waiting for L1 queue after verification");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderRelayedToRMS(Long.parseLong(order_id)), "Order has not been relayed to RMS");
		
		System.out.println("######################################### newUserOrderAfterVerificationTest compleated #########################################");
	}
	
	@Test(priority = 30, groups = {"sanity", "regression"},description = "Verify a swiggy pop order and check if it's relayed to vendor")
	public void swiggyPopOrderAfterVerificationTest() throws Exception{
		System.out.println("***************************************** swiggyPopOrderAfterVerificationTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("pop");
		Thread.sleep(1000);
		omsHelper.verifyOrder(order_id, verifyAction);
		Thread.sleep(1000);
		Assert.assertFalse(helper.queryOrdersInQueue(order_id, "L1Placer"), "swiggy pop order is present in  waiting for L1 queue after verification");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderRelayedToRMS(Long.parseLong(order_id)), "Order has not been relayed to RMS");
		
		System.out.println("######################################### swiggyPopOrderAfterVerificationTest compleated #########################################");
	}
	
	@Test(priority = 31, groups = {"sanity", "regression"},description = "Verify a swiggy assured order and check if it's relayed to vendor")
	public void swiggyAssuredOrderAfterVerificationTest() throws Exception{
		System.out.println("***************************************** swiggyAssuredOrderAfterVerificationTest started *****************************************");
		
		String order_id = losHelper.getAnOrder("swiggyassured");
		Thread.sleep(1000);
		omsHelper.verifyOrder(order_id, verifyAction);
		Thread.sleep(1000);
		Assert.assertFalse(helper.queryOrdersInQueue(order_id, "L1Placer"), "swiggy assured order is present in  waiting for L1 queue after verification");
		Thread.sleep(1000);
		Assert.assertTrue(helper.isOrderRelayedToRMS(Long.parseLong(order_id)), "Order has not been relayed to RMS");
		
		System.out.println("######################################### swiggyAssuredOrderAfterVerificationTest compleated #########################################");
	}
	
	
}
