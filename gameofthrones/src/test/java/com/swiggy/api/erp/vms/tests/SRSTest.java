package com.swiggy.api.erp.vms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.vms.dp.SRSDP;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.erp.vms.helper.SRSHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import net.minidev.json.JSONArray;
import org.bouncycastle.math.ec.ScaleYPointMap;
import org.primefaces.extensions.component.switchcase.Switch;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SRSTest extends SRSDP {

    SRSHelper helper = new SRSHelper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();


    @Test(dataProvider = "createRest", groups = {
            "sanity" }, description = "POST /restaurant/ Create restaurant through SRS API")
    public void createRest(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail)
            throws Exception {
        Processor processor = helper.createRest(name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy, locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState, invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail, ownerName, smEmail, asmEmail);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), "done successfully");
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 1);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "success");
        Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsInt("data"));

    }

    @Test(dataProvider = "createRestRegression1", groups = {
            "regression" }, description = "POST /restaurant/ Create restaurant with incorrect values")
    public void createRestRegression1(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail, String statusMessage, String field, String value)
            throws Exception {
        Processor processor = helper.createRest(name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy, locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState, invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail, ownerName, smEmail, asmEmail);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 0);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), statusMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors..field").replace("[\"","").replace("\"]",""), field );
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors..value").replace("[\"","").replace("\"]",""), value );

    }

    @Test(dataProvider = "createRestRegression2", groups = {
            "regression" }, description = "POST /restaurant/ Create restaurant with null values")
    public void createRestRegression2(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail, String statusMessage, String field, String value)
            throws Exception {
        Processor processor = helper.createRest(name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy, locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState, invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail, ownerName, smEmail, asmEmail);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 0);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), statusMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors..field").replace("[\"", "").replace("\"]", ""), field);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors..value").replace("[\"", "").replace("\"]", ""), value);
    }


    @Test(dataProvider = "updateRest", groups = {
            "sanity" }, description = "PUT /restaurant/update/{restId} update restaurant through SRS API")
    public void updateRest(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail, String menuId, String restId)
            throws Exception {
        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.updateRest(name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy,
                locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState,
                invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail,
                ownerName, smEmail, asmEmail, menuId, restId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), "done successfully", "message do not match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 1, "status do not match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "success", "code do not match");

        List<Map<String, Object>> list = helper.getRestaurantDetails(restId);

        if(list.size() >0) {
            System.out.println(list);
            softAssert.assertEquals("\""+list.get(0).get("name")+"\"", name, "Name do not match");
            softAssert.assertEquals("\""+list.get(0).get("area_code")+"\"", area, "area_code do not match");
            softAssert.assertEquals("\""+list.get(0).get("city_code")+"\"", city, "city_code do not match");
            softAssert.assertEquals(list.get(0).get("agreement_type").toString(), "1", "agreement_type do not match");
            softAssert.assertEquals("\""+list.get(0).get("locality")+"\"", locality, "locality do not match");
            softAssert.assertEquals("\""+list.get(0).get("address")+"\"", address, "address do not match");
            softAssert.assertEquals("\""+list.get(0).get("commission_exp")+"\"", commissionExp, "commission_exp do not match");
            if(payBySystemValue.equals("\"1\"")) {
                softAssert.assertEquals(list.get(0).get("pay_by_system_value").toString(), "true", "pay_by_system_value do not match");
            } else
                softAssert.assertEquals(list.get(0).get("pay_by_system_value").toString(), "false", "pay_by_system_value do not match");
            softAssert.assertEquals("\""+list.get(0).get("menu_id")+"\"", menuId, "menu_id do not match");
        }

        List<Map<String, Object>> fin = helper.getRestaurantFinanceDetails(restId);
        System.out.println("FINNNNNNNNN" + fin);

        if(fin.size() >0) {
            softAssert.assertEquals("\""+fin.get(0).get("gst_state")+"\"", gstState, "gst_state do not match");
            softAssert.assertEquals("\""+fin.get(0).get("invoice_entity_name")+"\"", invoicingName, "invoice_entity_name do not match");
            softAssert.assertEquals("\""+fin.get(0).get("invoice_email_id")+"\"", invoicingEmail, "invoice_email_id do not match");

            String payment = fin.get(0).get("payment_instrument").toString();
            System.out.println("PAYMENTTTTTT"+ payment);
            JsonHelper jh = new JsonHelper();
            Map<String, Object> m = jh.getJSONToMap(payment);

            softAssert.assertEquals("\""+m.get("url")+"\"", cancelledCheque, "cancelledCheque do not match");
            softAssert.assertEquals("\""+m.get("bank_city")+"\"", bankCity, "bankCity do not match");
            softAssert.assertEquals("\""+m.get("mou_url")+"\"", mou, "mou do not match");
            softAssert.assertEquals("\""+m.get("bank_name")+"\"", bankName, "bankName do not match");
            softAssert.assertEquals("\""+m.get("ifsc_code")+"\"", bankIfscCode, "bankIfscCode do not match");
            softAssert.assertEquals("\""+m.get("account_no")+"\"", bankAccountNo, "bankAccountNo do not match");
            softAssert.assertEquals("\""+m.get("tan_number")+"\"", tanNo, "tanNo do not match");
            softAssert.assertEquals("\""+m.get("pan_number")+"\"", panNo, "panNo do not match");
            softAssert.assertEquals("\""+m.get("benificary_name")+"\"", benificiaryName, "benificiaryName do not match");

            /*
            //Enable This block onve we have Vymo Adapter db setup in all UAT

            String vymo = helper.getVymoRestMap(restId);
            System.out.println("VYMOOOOOO" + vymo);
            softAssert.assertEquals("\""+vymo+"\"", vymoId, "vymoId do not match");
            */

            //Don't know where they are going
            //softAssert.assertEquals(list.get(0).get("compId"),compId, "compId do not match");
            //softAssert.assertEquals(list.get(0).get("ownerEmail"),ownerEmail, "ownerEmail do not match");
            //softAssert.assertEquals(list.get(0).get("ownerMobile"),ownerMobile, "ownerMobile do not match");
            //softAssert.assertEquals(list.get(0).get("smEmail"),smEmail, "smEmail do not match");
            //softAssert.assertEquals(list.get(0).get("asmEmail"),asmEmail, "asmEmail do not match");
            //softAssert.assertEquals(list.get(0).get("updated_by"), updatedBy, "updated_by do not match");
        }

        softAssert.assertAll();

    }

    @Test(dataProvider = "updateRestRegression1", groups = {
            "regression" }, description = "PUT /restaurant/update/{restId} update restaurant through SRS API")
    public void updateRestRegression1(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail, String menuId, String restId, String statusMessage, String field, String value)
            throws Exception {
        Processor processor = helper.updateRest(name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy, locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState, invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail, ownerName, smEmail, asmEmail, menuId, restId);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 0);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), statusMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors..field").replace("[\"","").replace("\"]",""), field );
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors..value").replace("[\"","").replace("\"]",""), value );

    }

    @Test(dataProvider = "updateRestRegression2", groups = {
            "regression" }, description = "PUT /restaurant/update/{restId} update restaurant through SRS API")
    public void updateRestRegression2(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail, String menuId, String restId, String statusMessage, String field, String value)
            throws Exception {
        Processor processor = helper.updateRest(name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy, locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState, invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail, ownerName, smEmail, asmEmail, menuId, restId);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 0);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), statusMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors..field").replace("[\"","").replace("\"]",""), field );
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors..value").replace("[\"","").replace("\"]",""), value );

    }

    @Test(dataProvider = "updateRestRegression3", groups = {
            "regression" }, description = "PUT /restaurant/update/{restId} update restaurant through SRS API")
    public void updateRestRegression3(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail, String menuId, String restId, String statusMessage)
            throws Exception {
        Processor processor = helper.updateRest(name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy, locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState, invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail, ownerName, smEmail, asmEmail, menuId, restId);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 0);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), statusMessage);
    }

    @Test(dataProvider = "updateRestRegression4", groups = {
            "regression" }, description = "Create restaurant through SRS API")
    public void updateRestRegression4(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail, String menuId, String restId, String statusMessage, String scenario)
            throws Exception {
        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.updateRest(name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy, locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState, invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail, ownerName, smEmail, asmEmail, menuId, restId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 1);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), statusMessage);

        switch(scenario)
        {
            case "1":
                List<Map<String, Object>> list = helper.getRestaurantDetails(restId);
                if(list.size() >0) {
                    System.out.println(list);
                    softAssert.assertNotNull(list.get(0).get("name"), "Name is Null");
                    softAssert.assertNotNull(list.get(0).get("area_code"), "area_code is Null");
                    softAssert.assertNotNull(list.get(0).get("city_code"),  "city_code is Null");
                    softAssert.assertNotNull(list.get(0).get("agreement_type").toString(), "agreement_type is Null");
                    softAssert.assertNotNull(list.get(0).get("locality"), "locality is Null");
                    softAssert.assertNotNull(list.get(0).get("address"), "address is Null");
                    softAssert.assertNotNull(list.get(0).get("commission_exp"), "commission_exp is Null");
                }
                break;
            case "2":
                List<Map<String, Object>> fin = helper.getRestaurantFinanceDetails(restId);
                System.out.println("FINNNNNNNNN" + fin);
                if(fin.size() >0) {
                    softAssert.assertNotNull(fin.get(0).get("gst_state"), "gst_state is Null");
                    softAssert.assertNotNull(fin.get(0).get("invoice_entity_name"), "invoice_entity_name is Null");
                    softAssert.assertNotNull(fin.get(0).get("invoice_email_id"), "invoice_email_id is Null");

                    String payment = fin.get(0).get("payment_instrument").toString();
                    System.out.println("PAYMENTTTTTT" + payment);
                    JsonHelper jh = new JsonHelper();
                    Map<String, Object> m = jh.getJSONToMap(payment);

                    softAssert.assertNotNull(m.get("url"), "cancelledCheque is Null");
                    softAssert.assertNotNull(m.get("bank_city"), "bankCity i Null");
                    softAssert.assertNotNull(m.get("mou_url"), "mou is Null");
                    softAssert.assertNotNull(m.get("bank_name"), "bankName is Null");
                    softAssert.assertNotNull(m.get("ifsc_code"), "bankIfscCode is Null");
                    softAssert.assertNotNull(m.get("account_no"), "bankAccountNo is Null");
                    softAssert.assertNotNull(m.get("tan_number"), "tanNo is Null");
                    softAssert.assertNotNull(m.get("pan_number"), "panNo is Null");
                    softAssert.assertNotNull(m.get("benificary_name"), "benificiaryName is Null");
                }
                break;

            case "3":
                /*
                    //Enable This block once we have Vymo Adapter db setup in all UAT
                    String vymo = helper.getVymoRestMap(restId);
                    System.out.println("VYMOOOOOO" + vymo);
                    softAssert.assertNotNull(vymo,"vymoId is Null");
                    */
                break;

            case "4":
                //TBD
                }
        softAssert.assertAll();

    }

    @Test(dataProvider = "restToggleTnC", groups = {"sanity" }, description = "POST /restaurant/toggle-tnc Toggle enable_tnc flag")
    public void restToggleTnC(String enableRestId, String disableRestId)
            throws Exception {

        Processor processor = helper.restToggleTnC(enableRestId, disableRestId);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 1);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "SUCCESS");

        List<Map<String, Object>> enableRest = helper.getRestaurantDetails(enableRestId);
        if (enableRest.size() > 0) {
            System.out.println(enableRest);
            Assert.assertEquals(enableRest.get(0).get("enable_tnc").toString(), "1", "restaurant tnc not enabled");
        }

        List<Map<String, Object>> disableRest = helper.getRestaurantDetails(disableRestId);
        if (disableRest.size() > 0) {
            System.out.println(disableRest);
            Assert.assertEquals(disableRest.get(0).get("enable_tnc").toString(), "0", "restaurant tnc not disabled");
        }
    }

        @Test(dataProvider = "restToggleTnCRegression", groups = {"regression" }, description = "POST /restaurant/toggle-tnc Toggle enable_tnc flag")
        public void restToggleTnCRegression(String restId1, String restId2, String enableFlag, boolean error)
            throws Exception {

        Processor processor;

        if(enableFlag.equals("1"))
            processor = helper.restToggleTnC(restId1+","+restId2, "");
        else
            processor = helper.restToggleTnC("",restId1+","+restId2);

        if(!error) {
            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 1);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "SUCCESS");

            List<Map<String, Object>> Rest1 = helper.getRestaurantDetails(restId1);
            if (Rest1.size() > 0) {
                System.out.println(Rest1);
                Assert.assertEquals(Rest1.get(0).get("enable_tnc").toString(), enableFlag, "restaurant tnc flag not as expected");
            }

            List<Map<String, Object>> Rest2 = helper.getRestaurantDetails(restId2);
            if (Rest2.size() > 0) {
                System.out.println(Rest2);
                Assert.assertEquals(Rest2.get(0).get("enable_tnc").toString(), enableFlag, "restaurant tnc not as expected");
            }
        } else {
            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 0);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "FAILURE");
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), "There was some error while processing this request.");

            }
    }

    @Test(dataProvider = "restFindBy", groups = {"sanity" }, description = "POST /restaurant/find/by Find rest by Id")
    public void restFindBy(String restId1, String restId2, int status, String code, boolean error)
            throws Exception {

        Processor processor = helper.restFindBy(restId1+","+restId2);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code);

        if(!error){
            String resp = processor.ResponseValidator.GetBodyAsText();
            String path = System.getProperty("user.dir");
            String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/restFindBy");
            List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
            Assert.assertTrue(missingNodeList.isEmpty(),
                    missingNodeList + " Nodes Are Missing Or Not Matching For API");
            boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
            System.out.println("Contain empty nodes => " + isEmpty);
        }
    }

    @Test(dataProvider = "updateRestDetails", groups = {"sanity" }, description = "PUT /restaurant/{id} update some details for rest")
    public void updateRestDetails(String restId, String name, String packing_charges, String tier,String veg_classifier, String halal_classifier, String cutlery, String updated_by)
            throws Exception {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.updateRestDetails(restId, name, packing_charges, tier, veg_classifier, halal_classifier, cutlery, updated_by);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 1, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "SUCCESS", "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.name"), name, "name doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.packingCharges"), packing_charges, "packingCharges doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tier"), tier, "tier doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.attributes.restaurant_id"), restId, "restaurant_id doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.attributes.veg_classifier"), veg_classifier, "veg_classifier doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.attributes.halal_classifier"), halal_classifier, "halal_classifier doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.attributes.cutlery"), cutlery, "cutlery doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.attributes.updated_by"), updated_by, "updated_by doesn't match");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.attributes.updated_at"), "updated_at doesn't match");

        softAssert.assertAll();
    }

    @Test(dataProvider = "updateRestDetailsRegression", groups = {"regression" }, description = "PUT /restaurant/{id} update some details for rest")
    public void updateRestDetailsRegression(String restId, String name, String packing_charges, String tier,String veg_classifier, String halal_classifier, String cutlery, String updated_by, String field, String value)
            throws Exception {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.updateRestDetails(restId, name, packing_charges, tier, veg_classifier, halal_classifier, cutlery, updated_by);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 0, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "FAILURE", "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors.[0].field"), field, "error field doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.field_validation_errors.[0].value"), value, "error value doesn't match");

        softAssert.assertAll();
    }

    @Test(dataProvider = "updateRestDetailsRegression1", groups = {"regression" }, description = "PUT /restaurant/{id} update some details for rest")
    public void updateRestDetailsRegression1(String restId, String name, String packing_charges, String tier,String veg_classifier, String halal_classifier, String cutlery, String updated_by)
            throws Exception {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.updateRestDetails(restId, name, packing_charges, tier, veg_classifier, halal_classifier, cutlery, updated_by);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 0, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "FAILURE", "code doesn't match");
        softAssert.assertAll();
    }

    @Test(dataProvider = "getByAreaIds", groups = {"sanity" }, description = "GET /restaurant/getByAreaIds get resturants by area ids")
    public void getByAreaIds(String areaId1, String areaId2)
            throws Exception {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getByAreaIds(areaId1, areaId2);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 1, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "success", "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), "done successfully", "message doesn't match");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].id"), "id field doesn't exist");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.[0].name"), "name field doesn't exist");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.[0].phoneNumbers"), "phoneNumbers field doesn't exist");

        softAssert.assertAll();
    }

    @Test(dataProvider = "getByRestIdsAndParentIds", groups = {"sanity" }, description = "GET /restaurant/getByRestIdsAndParentIds get resturants by city, parent, rest ids")
    public void getByRestIdsAndParentIds(String cityId, String restId, String parentId)
            throws Exception {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getByRestIdsAndParentIds(cityId, restId, parentId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 1, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "success", "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), "done successfully", "message doesn't match");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].id"), "id field doesn't exist");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.[0].name"), "name field doesn't exist");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].areaCode"), "phoneNumbers field doesn't exist");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].codLimit"), "codLimit field doesn't exist");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].assured"), "assured field doesn't exist");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].cityCode"), cityId, "city doesn't match");
        if(!parentId.isEmpty())
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].parentId"), parentId, "parentId doesn't match");

        softAssert.assertAll();
    }

    @Test(dataProvider = "getByRestIdsAndParentIdsRegression", groups = {"regression" }, description = "GET /restaurant/getByRestIdsAndParentIds get resturants by city, parent, rest ids")
    public void getByRestIdsAndParentIdsRegression(String cityId, String restId, String parentId)
            throws Exception {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getByRestIdsAndParentIds(cityId, restId, parentId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 0, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "FAILURE", "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), "Invalid argument type", "message doesn't match");

        softAssert.assertAll();
    }

    @Test(dataProvider = "getRestIdsByAreaId", groups = {"sanity" }, description = "GET /restaurant/id/area/{area-id} get resturants by area ids")
    public void getRestIdsByAreaId(String areaId, int status, String code, String message) {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getRestIdsByAreaId(areaId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");

        softAssert.assertAll();
    }

    @Test(dataProvider = "getRestIdsByCityId", groups = {"sanity" }, description = "GET /restaurant/id/city/{city-id} get resturants by city ids")
    public void getRestIdsByCityId(String cityId, int status, String code, String message) {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getRestIdsByCityId(cityId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");

        softAssert.assertAll();
    }

    @Test(dataProvider = "isRestaurantOpen", groups = {"sanity" }, description = "GET /restaurant/is-restaurant-open/{restaurant_id} get if resturants is enabled")
    public void isRestaurantOpen(String restId, int status, String code, String message, String data, boolean error) {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.isRestaurantOpen(restId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        if(!error) {
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsBool("data").toString(), data, "data doesn't match");
        }
        softAssert.assertAll();

        /* We need to add more test cases for enabled one taking into consideration rest slots and holiday slots*/
    }

    @Test(groups = {"sanity" }, description = "GET /restaurant/last-id/{last_id}/get-next-ids-page get resturants after given id")
    public void getNextPageOfRestIds() {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getNextPageOfRestIds("9990");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), 1, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), "success", "code doesn't match");

        List<Integer> list = (JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data"));
        System.out.println("Rest List Size::::" + list.size());
        softAssert.assertTrue(list.size() >= 200, "rest list size is greater than 200 ");
        for(int i=0; i<list.size(); i++) {
            Map<String, Object> m = helper.isRestaurantEnabled(list.get(i).toString());
            softAssert.assertTrue(m.get("enabled").equals("1"), "rest not enable for restId" +list.get(i).toString());
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "getRestAreaId", groups = {"sanity" }, description = "GET /restaurant/{id}/area-id get resturants area ids")
    public void getRestAreaId(String restId, int status, String code, String message) {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getRestAreaId(restId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");
        softAssert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data"), "data doesn't match");

        softAssert.assertAll();
    }

    @Test(dataProvider = "getRestAreaId", groups = {"sanity" }, description = "GET /restaurant/{id}/city-id get resturants city ids")
    public void getRestCityId(String restId, int status, String code, String message) {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getRestCityId(restId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");
        softAssert.assertTrue(processor.ResponseValidator.DoesNodeExists("data"), "data doesn't match");

        softAssert.assertAll();
    }

    @Test(dataProvider = "getRestContacts", groups = {"sanity" }, description = "GET /restaurant/{id}/contacts get resturants contacts")
    public void getRestContacts(String restId, int status, String code, String message, boolean error) {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getRestContacts(restId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");

        if(!error) {
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"), restId, "id doesn't match");
            softAssert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.name"), "name doesn't match");
            softAssert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data..orderNotifyEmails"), "orderNotifyEmails doesn't match");
            softAssert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data..orderNotifyNumbers"), "orderNotifyNumbers doesn't match");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "getRestPartner", groups = {"sanity" }, description = "GET /restaurant/{id}/partner get resturants contacts")
    public void getRestPartner(String restId, int status, String code, String message, boolean error) {

        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.getRestPartner(restId);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");

        if(!error) {
            softAssert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.partnerType"), "partnerType doesn't match");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "restSuggestV2", groups = {"sanity" }, description = "GET /restaurant/v2/suggest get restaurant list filted on area, city, partner ")
    public void restSuggestV2(String cityId, String areaId, String partner, int status, String code, String message) {

        SoftAssert softAssert = new SoftAssert();

        Processor processor = helper.restSuggestV2(cityId, areaId, partner, "restSuggestV2");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");

        JSONArray ja = processor.ResponseValidator.GetNodeValueAsJsonArray("data");
        System.out.println("DATA from JsonArray::" +ja);
        List<Map<String, Object>> data = helper.getRestSuggestV2FromDB(cityId, areaId, partner);
        int jaSize = ja.size();
        int dataSize = data.size();
        int count = 0;
        if(jaSize != dataSize) {
            softAssert.assertTrue(false, "number of rest from DB and API doesn't match");
        }

        for(int i=0; i<dataSize; i++) {
            for (int j=0; j<jaSize; j++) {
                if(ja.get(j).toString().equals(data.get(i).get("id").toString())) {
                    count++;
                    System.out.println("REST From DB count"+i+ "::" +data.get(i).get("id"));
                    break;
                }
            }
        }

        softAssert.assertAll();
    }

    @Test(dataProvider = "restSuggestV2Regression", groups = {"regression" }, description = "GET /restaurant/v2/suggest get restaurant list filted on area, city, partner ")
    public void restSuggestV2Regression(String Id1, int status, String code, String message, String apiUnderTest) {

        SoftAssert softAssert = new SoftAssert();

        Processor processor = helper.restSuggestV2(Id1, apiUnderTest);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");

        JSONArray ja = processor.ResponseValidator.GetNodeValueAsJsonArray("data");

        if(ja.size()<=0) {
            softAssert.assertFalse(false, "No rest returned in API suggest");
        }

        softAssert.assertAll();
    }

    @Test(dataProvider = "restSuggestV2Regression1", groups = {"regression" }, description = "GET /restaurant/v2/suggest get restaurant list filted on area, city, partner ")
    public void restSuggestV2Regression1(String Id1, String Id2, int status, String code, String message, String apiUnderTest) {

        SoftAssert softAssert = new SoftAssert();

        Processor processor = helper.restSuggestV2(Id1, Id2, apiUnderTest);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("message"), message, "message doesn't match");

        JSONArray ja = processor.ResponseValidator.GetNodeValueAsJsonArray("data");

        if(ja.size()<=0) {
            softAssert.assertFalse(false, "No rest returned in API suggest");
        }

        softAssert.assertAll();
    }

    @Test(dataProvider = "restSuggestV2Regression2", groups = {"regression" }, description = "GET /restaurant/v2/suggest get restaurant list filted on area, city, partner ")
    public void restSuggestV2Regression2(String cityId, String areaId, String partner, int status, String code) {

        SoftAssert softAssert = new SoftAssert();

        Processor processor = helper.restSuggestV2(cityId, areaId, partner, "restSuggestV2");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), status, "status doesn't match");
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("code"), code, "code doesn't match");
        softAssert.assertAll();
    }

}
