
package com.swiggy.api.erp.cms.pojo.PricingService;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.constants.CatalogV2Constants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sku_id",
    "store_id"
})
public class Meta {

    @JsonProperty("sku_id")
    private String sku_id;
    @JsonProperty("store_id")
    private String store_id;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("sku_id")
    public String getSku_id() {
        return sku_id;
    }

    @JsonProperty("sku_id")
    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public Meta withSku_id(String sku_id) {
        this.sku_id = sku_id;
        return this;
    }

    @JsonProperty("store_id")
    public String getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public Meta withStore_id(String store_id) {
        this.store_id = store_id;
        return this;
    }
    public Meta setDefaultValue(){
        this.withSku_id(CatalogV2Constants.sku_id).withStore_id(CatalogV2Constants.store_id);
        return this;
    }


}
