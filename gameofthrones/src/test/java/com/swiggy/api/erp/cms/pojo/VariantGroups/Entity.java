package com.swiggy.api.erp.cms.pojo.VariantGroups;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/19/18.
 */
public class Entity
{
    private String id;

    private int order;

    private String item_id;

    private String name;

    private String third_party_id;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getThird_party_id() {
        return third_party_id;
    }

    public void setThird_party_id(String third_party_id) {
        this.third_party_id = third_party_id;
    }

    public Entity build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        if(this.getId() == null)
            this.setId(MenuConstants.items_id);
        if(this.getName() == null)
            this.setName(MenuConstants.items_name);
        if(this.getItem_id() == null)
            this.setItem_id(MenuConstants.items_id);
        if(this.getThird_party_id() == null)
            this.setThird_party_id("");
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", order = "+order+", item_id = "+item_id+", name = "+name+"]";
    }
}
