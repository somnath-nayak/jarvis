package com.swiggy.api.erp.ff.helper;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.constants.OmtConstants;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SwiggyAssuredRefundsHelper implements OmtConstants {
	
	public String getOrderTimeEarlier () throws JSONException {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) - 1);
		String orderTimeEarlier = dateFormat.format(cal.getTime());
		return orderTimeEarlier;
	}
	
	public String getOrderTimeNow () throws JSONException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
		String orderTimeNow = dateFormat.format(cal.getTime());
		return orderTimeNow;
	}
	
	public String getEpochTime () throws JSONException {
		Instant instant = Instant.now ();
		Instant instantHourEarlier = instant.minus ( 1 , ChronoUnit.HOURS );
		String epochTime = String.valueOf(instantHourEarlier.getEpochSecond());  
		return epochTime;
	}	
	
	public String getOrderId () throws JSONException {
		Date date = new Date();
		DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String orderId = order_idFormat.format(date);
		return orderId;
	}	
		
	public int getSwiggyAssuredFlag(long order_id) throws JSONException{
		List<Map<String, Object>> swiggyAssured = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id +";");
		String payload = (String)swiggyAssured.get(0).get("payload");
		JSONObject jsonObj = new JSONObject(payload.toString());
		return jsonObj.getInt("is_assured");
	}
	
	public String getPgResponseTime(long order_id) throws JSONException{
		List<Map<String, Object>> swiggyAssured = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id +";");
		String payload = (String)swiggyAssured.get(0).get("payload");
		JSONObject jsonObj = new JSONObject(payload.toString());
		return jsonObj.getString("pg_response_time");
	}
	
	public String getDeliveredTime(long order_id) throws JSONException{
		List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("Select delivered_time from oms_order oo FULL OUTER JOIN oms_orderstatus os ON (oo.status_id=os.id) where order_id = " +order_id + " ;");
		return String.valueOf(postgresOrders.get(0).get("delivered_time"));
	}
	
	public String setOrderAsEdited(long order_id) throws JSONException{
		int postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).update("update oms_refundstatus set edited_status = 'customer_edit' where order_id = " +order_id + " ;");
		return "true";
	}
	
	public String getRefundStatus(long order_id) throws JSONException{
		List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("Select refund_status from oms_refundstatus where order_id = " +order_id + " ;");
		return String.valueOf(postgresOrders.get(0).get("refund_status"));
	}
	
	public String getCustomerEditFlag(long order_id) throws JSONException{
		List<Map<String, Object>> getCustomerEditFlag = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_refundstatus where order_id = "+ order_id +";");
		try {
			String payload = (String)getCustomerEditFlag.get(0).get("payload");
			JSONObject jsonObj = new JSONObject(payload.toString());
			return jsonObj.getString("edited_status");	

		} catch (NullPointerException e) {
			// TODO: handle exception
			return null;
		}		
	}
}
