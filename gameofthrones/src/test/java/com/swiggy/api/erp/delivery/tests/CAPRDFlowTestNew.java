package com.swiggy.api.erp.delivery.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.AutoAssignmentDataProvider;
import com.swiggy.api.erp.delivery.helper.*;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import io.gatling.charts.component.AssertionsTableComponent;
import org.apache.tomcat.jni.Proc;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import java.util.*;

/**
 * Created by preetesh.sharma on 10/04/18.
 */
public class CAPRDFlowTestNew {


    Integer zone_id = 34;
    String restlat;
    String restlon;
    String app_version;
    DeliveryDataHelper deliveryDataHelper;
    String de_id;
    Map<String, String> orderDefaultMap;
    String restaurant_id;
    Integer areacode;
    AutoassignHelper autoassignHelper;
    DeliveryServiceHelper deliveryServiceHelper;
    DeliveryHelperMethods deliveryHelperMethods;
    String de_auth;


    public CAPRDFlowTestNew() {


    }

    @BeforeSuite
    public void setData()
    {
        orderDefaultMap = new HashMap<>();
        deliveryDataHelper = new DeliveryDataHelper();
        autoassignHelper = new AutoassignHelper();
        deliveryServiceHelper = new DeliveryServiceHelper();
        deliveryHelperMethods = new DeliveryHelperMethods();
        de_id = deliveryDataHelper.CreateDE(zone_id);
        //de_id = "87526";
        app_version = "1.9";
        areacode = Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select id from area where zone_id=" + zone_id).get("id").toString());
        String latlon = getRestLatLon();
        restlat = latlon.split(",")[0];
        restlon = latlon.split(",")[1];
        //restaurant_id = "10820";
        //orderDefaultMap.put("restaurant_id", restaurant_id);
        orderDefaultMap.put("restaurant_lat_lng", latlon);
        orderDefaultMap.put("order_time", deliveryDataHelper.getcurrentDateTimefororderTime());
        orderDefaultMap.put("order_type", "regular");
        orderDefaultMap.put("restaurant_area_code", "4");
        Object rest=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select r.id from restaurant r inner join area a on r.area_code=a.id  where a.zone_id="+zone_id+" and a.enabled=1 and r.with_de=0 and r.partner_id is not null order by 1 desc limit 1;");
        if(rest!=null)
            restaurant_id=rest.toString();
        else
            Assert.assertTrue(false, "Cannot Find restaurant in Zone  "+zone_id);
    }

    public void loginDEandUpdateLocation()
    {
        autoassignHelper.delogin(de_id, app_version);
        Integer deotp = deliveryServiceHelper.getOtp(de_id, app_version);
        String response = autoassignHelper.deotp(de_id, deotp.toString()).ResponseValidator.GetBodyAsText();
        try {
            de_auth = ((JSONObject) ((new JSONObject(response)).get("data"))).getString("Authorization");
            System.out.println("de Auth is " + de_auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        autoassignHelper.locupdate(restlat, restlon, de_auth);

    }

    public String getOrderID(Map<String, String> orderDefaultMap) {
        String orderjson = deliveryDataHelper.returnOrderJson(orderDefaultMap);
        String order_id = null;
        if (orderjson != null) {
            try {
                order_id = new JSONObject(orderjson).get("order_id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderjson)) {
                return order_id;
            }
        }
        return null;
    }

    public String getRestLatLon() {
        String path = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select path from zone where id=" + zone_id).get("path").toString();
        List<String> latlonllist = Arrays.asList(path.split(" "));
        String latlon = deliveryDataHelper.getCentroid(latlonllist);
        return latlon;
    }

    public Boolean checkAssignmentStatus(Processor processor, String order_id, String de_id) {
        if ((processor.ResponseValidator.GetResponseCode()) == 200) {
            String query = "Select de_id from trips where order_id=" + order_id;
            String deIdAssigned = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap(query).get("de_id").toString();
            return de_id.equalsIgnoreCase(deIdAssigned);
        }
        return false;
    }

/*    public Boolean checkDeFreeStatus(String order_id,String de_id )
    {

    }*/

    @Test(groups = "Regression", description = "Verify DE is able to Login to system")
    public void CAPRDDP332() {
        //need to put all de related stuff at one place
        Processor processor = autoassignHelper.delogin(de_id, app_version);
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200);
        // Assert.assertTrue(processor.ResponseValidator.GetBodyAsText().);
        //Ambigous to check otp sent status leavint it for now
        //            Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
        //            .toString(), "OTP Sent");
    }


    @Test(groups = "Regression", description = "Verify DE is able to login to app with OTP recieved")
    public void CAPRDDP333() {
        autoassignHelper.delogin(de_id, app_version);
        Integer deotp = deliveryServiceHelper.getOtp(de_id, app_version);
        String response = autoassignHelper.deotp(de_id, deotp.toString()).ResponseValidator.GetBodyAsText();
        try {
            de_auth=((JSONObject)((new JSONObject(response)).get("data"))).getString("Authorization");
            System.out.println("de Auth is "+de_auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(JsonPath.read(response, "$.data.Authorization").toString().length()>1 );
    }

    @Test(groups = "Regression", description = "Verify order is assigned to DE if DE is active and free")
    public void CAPRDDP299() {
        //put poll db in deliveryData helper
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Processor processor = null;
        if (defreestatus)
            processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        else
            Assert.assertTrue(false, "Cant make de Free");
        Boolean assignmentStatus = checkAssignmentStatus(processor, order_id, de_id);
        Assert.assertTrue(assignmentStatus, "Order " + order_id + " Not Assigned to " + de_id);
    }

    @Test(groups = "Regression", description = "Verify that DE is not able to login to system if DE status is not working with swiggy")
    public void CAPRDDP253() {
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update delivery_boys set employment_status=2 where id=" + de_id);
        Processor processor = autoassignHelper.delogin(de_id, app_version);
        String resp = processor.ResponseValidator.GetBodyAsText();
        if ((processor.ResponseValidator.GetResponseCode()) == 403) {
            Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(),
                    "1");
            Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
                    .toString(), "DE who is currently not working at Swiggy, is trying to login.");
        } else {
            Assert.assertTrue(false);
        }
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update delivery_boys set employment_status=1 where id=" + de_id);
    }

    @Test(groups = "Regression", description = "Verify DE is able to mark order confirmed once order is assigned to DE")
    public void CAPRDDP300() {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor = null;
        boolean freestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Assert.assertTrue(freestatus, "DE cannot ne make free");
        processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignstatus = checkAssignmentStatus(processor, order_id, de_id);
        Assert.assertTrue(assignstatus, "Order could not be assigned");
        try {
            Boolean confirmstatus = deliveryServiceHelper.zipDialConfirm(order_id);
            Thread.sleep(5000);
            Assert.assertTrue(confirmstatus, "Status not marked as confirm");
            //polldb here for the confirmed time
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test(groups = "Regression", description = "Verify DE is not able to mark confirmed if order is not assigned to DE")
    public void CAPRDDP301() {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Assert.assertTrue(defreestatus, "DE cannot ne make free");
        Processor processor = deliveryServiceHelper.zipDialConfirmDE(order_id);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200);
        System.out.println("Printing JSON Path"+JsonPath.read(resp, "$.statusMessage").toString());
        Assert.assertTrue(JsonPath.read(resp, "$.statusMessage").toString().equalsIgnoreCase("can  not update the state from null to confirmed"));

    }


    @Test(groups = "Regression", description = "Verify DE is not able to mark order arrived/pickedup/reached/delivered if current order status is assigned")
    public void CAPRDDP302() {
        List<String> statelist = new ArrayList<>();
        statelist.add("arrived");
        statelist.add("pickedup");
        statelist.add("reached");
        statelist.add("delivered");
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Assert.assertTrue(defreestatus, "DE cannot ne make free");
        Processor processor1 = deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignStatus = checkAssignmentStatus(processor1, order_id, de_id);
        Assert.assertTrue(assignStatus, "Order could not be assigned");
        for (String state : statelist) {
            switch (state) {
                case "arrived": {
                    deliveryDataHelper.waitInterval(5);
                    Processor processor = deliveryServiceHelper.zipDialArrivedDE(order_id);
                    Assert.assertFalse(confirmZipDialStatus(state, order_id), "order status not changed in db");
                    String resp = processor.ResponseValidator.GetBodyAsText();
                    Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                    Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from assigned to arrived");
                    break;
                }
                case "pickedup": {
                    deliveryDataHelper.waitInterval(5);
                    Processor processor = deliveryServiceHelper.zipDialPickedUpDE(order_id);
                    Assert.assertFalse(confirmZipDialStatus(state, order_id), "order status not changed in db");
                    String resp = processor.ResponseValidator.GetBodyAsText();
                    Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                    Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from assigned to pickedup");
                    break;
                }
                case "reached": {
                    deliveryDataHelper.waitInterval(5);
                    Processor processor = deliveryServiceHelper.zipDialReachedDE(order_id);
                    Assert.assertFalse(confirmZipDialStatus(state, order_id), "order status not changed in db");
                    String resp = processor.ResponseValidator.GetBodyAsText();
                    Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                    Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from assigned to reached");
                    break;
                }
                case "delivered": {
                    deliveryDataHelper.waitInterval(5);
                    Processor processor = deliveryServiceHelper.zipDialDeliveredDE(order_id);
                    Assert.assertFalse(confirmZipDialStatus(state, order_id), "order status not changed in db");
                    String resp = processor.ResponseValidator.GetBodyAsText();
                    Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                    Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from assigned to delivered");
                    break;
                }
            }
        }


    }
    @Test(groups = "Regression", description = "Verify DE is able to mark order arrived once order is confirmed by DE")
    public void CAPRDDP303() {
        //need to check wether de is logged in or not
        String order_id = getOrderID(orderDefaultMap);
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Processor processor = null;
        Assert.assertTrue(defreestatus, "DE cannot ne make free");
        processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignmentStatus = checkAssignmentStatus(processor, order_id, de_id);
        Assert.assertTrue(assignmentStatus, "Order could not be assigned");
        deliveryDataHelper.waitInterval(3);
        boolean confirmstatus = deliveryServiceHelper.zipDialConfirm(order_id);
        Assert.assertTrue(confirmZipDialStatus("confirmed", order_id), "order status not changed in db");
        Assert.assertTrue(confirmstatus, "Order could not be confirmed");
        deliveryDataHelper.waitInterval(3);
        Processor processor1 = deliveryServiceHelper.zipDialArrivedDE(order_id);
        Assert.assertTrue(confirmZipDialStatus("arrived", order_id), "order status not changed in db");
        String resp = processor1.ResponseValidator.GetBodyAsText();
        if ((processor.ResponseValidator.GetResponseCode()) == 200) {
            Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "0");
            Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "Successfully pushed to queue");
        }
        else
        {
            Assert.assertTrue(false,"Response code is other than  400");
        }
    }

    @Test(groups = "Regresssion", description = "Verify DE is able to mark order pickedup once order is arrived by DE")
    public void CAPRDDP305() {
        String order_id=getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor=null;
        boolean freestatus=deliveryServiceHelper.makeDEFree(de_id,app_version);
        Assert.assertTrue(freestatus, "DE cannot ne make free");        {
            processor =deliveryServiceHelper.assignOrder(order_id, de_id);
            Boolean assignstatus=checkAssignmentStatus(processor,order_id,de_id);
            Assert.assertTrue(assignstatus, "Order could not be assigned");
            deliveryDataHelper.waitInterval(3);
            Boolean confirmstatus=deliveryServiceHelper.zipDialConfirm(order_id);
            Assert.assertTrue(confirmZipDialStatus("confirmed",order_id),"order status not changed in db");
            Assert.assertTrue(confirmstatus, "Status not marked as confirm");
            deliveryDataHelper.waitInterval(3);
            Boolean arrivedstatus=deliveryServiceHelper.zipDialArrived(order_id);
            Assert.assertTrue(confirmZipDialStatus("arrived",order_id),"order status not changed in db");
            Assert.assertTrue(arrivedstatus, "Status not marked as arrived");
            deliveryDataHelper.waitInterval(3);
            Boolean pickedUpstatus=deliveryServiceHelper.zipDialPickedUp(order_id);
            Assert.assertTrue(confirmZipDialStatus("pickedup",order_id),"order status not changed in db");
            Assert.assertTrue(pickedUpstatus, "Status not marked as pickedup");

        }}




    @Test(groups = "Regression", description = "Verify DE is not able to mark order pickedup/reached/delivered if current order status is confirmed")
    public void CAPRDDP304()
    {
        String order_id=getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor=null;
        boolean freestatus=deliveryServiceHelper.makeDEFree(de_id,app_version);
        Assert.assertTrue(freestatus, "DE cannot ne make free");
        processor =deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignstatus=checkAssignmentStatus(processor,order_id,de_id);
        Assert.assertTrue(assignstatus, "Order could not be assigned");

        deliveryDataHelper.waitInterval(3);
        Boolean confirmstatus=deliveryServiceHelper.zipDialConfirm(order_id);
        Assert.assertTrue(confirmstatus, "Status not marked as confirm");
        Assert.assertTrue(confirmZipDialStatus("confirmed",order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean pickedUpstatus=deliveryServiceHelper.zipDialPickedUp(order_id);
        //Assert.assertTrue(pickedUpstatus, "Status marked as pickedup without making de Arrived");
        Assert.assertFalse(confirmZipDialStatus("pickedup",order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean reachedstatus=deliveryServiceHelper.zipDialReached(order_id);
        //Assert.assertTrue(reachedstatus, "Status marked as reached,without making de Arrived");
        Assert.assertFalse(confirmZipDialStatus("reached",order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean deliveredstatus=deliveryServiceHelper.zipDialDelivered(order_id);
        deliveryDataHelper.waitInterval(3);
        //Assert.assertFalse(deliveredstatus, "Status marked as delivered,without making de Arrived");
        Assert.assertFalse(confirmZipDialStatus("delivered",order_id));


    }

    @Test(groups = "Regresssion", description = "Verify DE is not able to mark order reached/delivered if current order status is arrived")
    public void CAPRDDP306() {
        List<String> statelist = new ArrayList<>();
        statelist.add("arrived");
        statelist.add("pickedup");
        statelist.add("reached");
        statelist.add("delivered");
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        deliveryServiceHelper.makeDEFree(de_id);
        for (String state : statelist) {

            deliveryDataHelper.waitInterval(3);
            switch (state) {
                case "arrived": {
                    Processor processor = deliveryServiceHelper.zipDialArrivedDE(order_id);
                    String resp = processor.ResponseValidator.GetBodyAsText();
                    Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                    Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from null to arrived");
                    Assert.assertFalse(confirmZipDialStatus(state, order_id));

                    break;
                }
                case "pickedup": {
                    Processor processor = deliveryServiceHelper.zipDialPickedUpDE(order_id);
                    String resp = processor.ResponseValidator.GetBodyAsText();
                    Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                    Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from null to pickedup");
                    Assert.assertFalse(confirmZipDialStatus(state, order_id));

                    break;
                }
                case "reached": {
                    Processor processor = deliveryServiceHelper.zipDialReachedDE(order_id);
                    String resp = processor.ResponseValidator.GetBodyAsText();
                    Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                    Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from null to reached");
                    Assert.assertFalse(confirmZipDialStatus(state, order_id));

                    break;
                }
                case "delivered": {
                    Processor processor = deliveryServiceHelper.zipDialDeliveredDE(order_id);
                    String resp = processor.ResponseValidator.GetBodyAsText();
                    Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                    Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from null to delivered");
                    Assert.assertFalse(confirmZipDialStatus(state, order_id));
                    break;
                }
            }
        }
    }

    @Test(groups = "Regresssion", description = "Verify DE is able to mark order reached once order is pickedup by DE")
    public void CAPRDDP307() {
        String order_id=getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor=null;
        boolean freestatus=deliveryServiceHelper.makeDEFree(de_id,app_version);
        Assert.assertTrue(freestatus, "DE cannot ne make free");

        processor =deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignstatus=checkAssignmentStatus(processor,order_id,de_id);
        Assert.assertTrue(assignstatus, "Order could not be assigned");

        deliveryDataHelper.waitInterval(3);
        Boolean confirmstatus=deliveryServiceHelper.zipDialConfirm(order_id);
        Assert.assertTrue(confirmZipDialStatus("confirmed",order_id));
        Assert.assertTrue(confirmstatus, "Status not marked as confirm");

        deliveryDataHelper.waitInterval(3);
        Boolean arrivedstatus=deliveryServiceHelper.zipDialArrived(order_id);
        Assert.assertTrue(arrivedstatus, "Status not marked as arrived");
        Assert.assertTrue(confirmZipDialStatus("arrived",order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean pickedUpstatus=deliveryServiceHelper.zipDialPickedUp(order_id);
        Assert.assertTrue(pickedUpstatus, "Status not marked as pickedup");
        Assert.assertTrue(confirmZipDialStatus("pickedup",order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean reachedstatus=deliveryServiceHelper.zipDialReached(order_id);
        Assert.assertTrue(reachedstatus, "Status not marked as reached");
        Assert.assertTrue(confirmZipDialStatus("reached",order_id));

    }


    @Test(groups = "Regresssion", description = "Verify DE is able to mark order delivered if current order status is pickedup")
    public void CAPRDDP308() {
        String order_id=getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor=null;
        boolean freestatus=deliveryServiceHelper.makeDEFree(de_id,app_version);
        Assert.assertTrue(freestatus, "DE cannot ne make free");
        processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignstatus = checkAssignmentStatus(processor, order_id, de_id);
        Assert.assertTrue(assignstatus, "Order could not be assigned");
        deliveryDataHelper.waitInterval(3);
        Boolean confirmstatus = deliveryServiceHelper.zipDialConfirm(order_id);
        Assert.assertTrue(confirmZipDialStatus("confirmed", order_id));
        Assert.assertTrue(confirmstatus, "Status not marked as confirm");

        deliveryDataHelper.waitInterval(3);
        Boolean arrivedstatus = deliveryServiceHelper.zipDialArrived(order_id);
        Assert.assertTrue(arrivedstatus, "Status not marked as arrived");
        Assert.assertTrue(confirmZipDialStatus("arrived", order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean pickedUpstatus = deliveryServiceHelper.zipDialPickedUp(order_id);
        Assert.assertTrue(pickedUpstatus, "Status not marked as pickedup");
        Assert.assertTrue(confirmZipDialStatus("pickedup", order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean reachedstatus = deliveryServiceHelper.zipDialReached(order_id);
        Assert.assertTrue(reachedstatus, "Status not marked as reached");
        Assert.assertTrue(confirmZipDialStatus("reached", order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean deliveredstatus = deliveryServiceHelper.zipDialDelivered(order_id);
        Assert.assertTrue(deliveredstatus, "Status not marked as reached");
        Assert.assertTrue(confirmZipDialStatus("delivered", order_id));

    }



    @Test(groups = "Regression", description = "Verify that order status becomes unassigned once order is rejected/re-assigned from DE ")
    public void CAPRDDP310() {
        String order_id = getOrderID(orderDefaultMap);
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Processor processor = null;
        Assert.assertTrue(defreestatus, "DE cannot ne make free");
        processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignstatus = checkAssignmentStatus(processor, order_id, de_id);
        Assert.assertTrue(assignstatus, "Order could not be assigned");
        Processor processor1 = deliveryServiceHelper.systemRejectOrder(de_id, order_id);
        deliveryDataHelper.waitInterval(3);
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200, "Cannot reject order");
        String query = "Select * from trips where order_id=" + order_id;
        Object assignedde = deliveryHelperMethods.dbhelperget(query, "de_id");
        Assert.assertTrue(null == assignedde || !(assignedde.toString().equalsIgnoreCase(de_id)), "Order was not unassigned");
    }


    @Test(groups = "Regression", description = "Verify cancelled order is not assigned to DE ")
    public void cancellOrderIsNotAssignedToDe() {
        String order_id = getOrderID(orderDefaultMap);
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Processor processor = null;
        if (defreestatus)
            processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        else
            Assert.assertFalse(true,"Cant Make DE free");
        processor=deliveryServiceHelper.ordercancel(order_id);
        deliveryDataHelper.waitInterval(3);
        //need to poll here
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200,"could not cancel order");
        deliveryDataHelper.waitInterval(3);
        Processor processor1 = deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignstatus=checkAssignmentStatus(processor1,order_id,de_id);
        Assert.assertFalse(assignstatus,"assignment worked");
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode()==400, "Wrong status code returned");
        //Assert.assertTrue(JsonPath.read(resp, "$.statusMessage").toString().equalsIgnoreCase("Trying to assign cancelled order " + order_id));
        }

    @Test(groups = "Regression", description = "Verify delivered order is not assigned to DE")
    public void deliveredordernotgettingassigned(){
        String order_id=getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor=null;
        boolean freestatus=deliveryServiceHelper.makeDEFree(de_id,app_version);
        Assert.assertTrue(freestatus, "DE cannot ne make free");
        processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignstatus = checkAssignmentStatus(processor, order_id, de_id);
        Assert.assertTrue(assignstatus, "Order could not be assigned");
        deliveryDataHelper.waitInterval(3);
        Boolean confirmstatus = deliveryServiceHelper.zipDialConfirm(order_id);
        Assert.assertTrue(confirmZipDialStatus("confirmed", order_id));
        Assert.assertTrue(confirmstatus, "Status not marked as confirm");

        deliveryDataHelper.waitInterval(3);
        Boolean arrivedstatus = deliveryServiceHelper.zipDialArrived(order_id);
        Assert.assertTrue(arrivedstatus, "Status not marked as arrived");
        Assert.assertTrue(confirmZipDialStatus("arrived", order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean pickedUpstatus = deliveryServiceHelper.zipDialPickedUp(order_id);
        Assert.assertTrue(pickedUpstatus, "Status not marked as pickedup");
        Assert.assertTrue(confirmZipDialStatus("pickedup", order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean reachedstatus = deliveryServiceHelper.zipDialReached(order_id);
        Assert.assertTrue(reachedstatus, "Status not marked as reached");
        Assert.assertTrue(confirmZipDialStatus("reached", order_id));

        deliveryDataHelper.waitInterval(3);
        Boolean deliveredstatus = deliveryServiceHelper.zipDialDelivered(order_id);
        Assert.assertTrue(deliveredstatus, "Status not marked as reached");
        Assert.assertTrue(confirmZipDialStatus("delivered", order_id));
        processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertTrue(JsonPath.read(resp, "$.statusCode").toString().equalsIgnoreCase( "3"));
        Assert.assertTrue(JsonPath.read(resp, "$.statusMessage").toString().equalsIgnoreCase("Batch already assigned to " + de_id));

    }


        public boolean confirmZipDialStatus(String status,String order_id)
        {
           Boolean orderstatus =false;
           String regex="\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}";
            switch (status) {
                case "confirmed": {
                    String query = DeliveryConstant.zipconfirmquery + order_id;
                    orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "confirmed_time", regex, 2, 20);
                    break;
                }
                case "arrived": {
                    String query = DeliveryConstant.ziparrivequery + order_id;
                    orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "arrived_time", regex, 2, 20);
                    break;
                }
                case "reached": {
                    String query = DeliveryConstant.zipreachedquery + order_id;
                    orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "reached_time", regex, 2, 20);
                    break;
                }
                case "pickedup": {
                    String query = DeliveryConstant.zippickedupquery + order_id;
                    orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "pickedup_time", regex, 2, 20);
                    break;
                }
                case "delivered": {
                    String query = DeliveryConstant.zipdeliveredquery + order_id;
                    orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "delivered_time", regex, 2, 20);
                    break;
                }
            }
            return orderstatus;

        }


    @Test
    public void createorder()
{
   // System.out.println(getOrderID(orderDefaultMap));
    confirmZipDialStatus("confirm","1523755653");
}
}