package com.swiggy.api.erp.crm.foodissues.pojo.CCResolution;

public class Example {

private String orderId;
private String issueType;
private String fullOrder;
private String containerCount;

public Example setOrderId(String orderId) {
this.orderId = orderId;
return this;
}

public Example setIssueType(String issueType) {
this.issueType = issueType;
return this;
}

public Example setFullOrder(String fullOrder) {
this.fullOrder = fullOrder;
return this;
}


public Example setContainerCount(String containerCount) {
this.containerCount = containerCount;
return this;
}
}
