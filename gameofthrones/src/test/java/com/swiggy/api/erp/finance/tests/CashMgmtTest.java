package com.swiggy.api.erp.finance.tests;

import com.swiggy.api.erp.finance.constants.CashMgmtConstant;
import com.swiggy.api.erp.finance.dp.CashDataProvider;
import com.swiggy.api.erp.finance.helper.CashMgmtDBhelper;
import com.swiggy.api.erp.finance.helper.CashMgmtHelper;
import com.swiggy.api.erp.finance.helper.finHelper;
import framework.gameofthrones.JonSnow.Processor;

import org.testng.Assert;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class CashMgmtTest extends CashDataProvider {
    Boolean alreadyExecuted=false;
    String de_id;
  //  String de_id="87607";
    String zone_id;
    CashMgmtHelper cmh=new CashMgmtHelper();
    CashMgmtDBhelper cmdh=new CashMgmtDBhelper();
    finHelper fhelp= new finHelper();
    private static boolean isDeCreated;


    @BeforeMethod
    public void setUp() {
        if (isDeCreated) {
            return;
        }
        de_id=fhelp.createZoneAndDE().get("de_id");
        // zone_id=fhelp.createZoneAndDE().get("zone_id");
        System.out.println( "DE id is ==> "+de_id);
      //  System.out.println("Zone id is ==> "+zone_id);
        isDeCreated = true;
    }





    //Creating session for Active DE
   @Test
    public void createSessionForActiveDE() throws InterruptedException {

        Object state=cmdh.getSessionStateFromDB(de_id);
        Object session_id=cmdh.getSessionIdFromDB(de_id);
        System.out.println("#####################"+state);
        if(state.equals("OPEN")){
            System.out.println("Session is already OPEN");
            Processor process = cmh.createSessionHelper(de_id);
            String createSessionResponse = process.ResponseValidator.GetBodyAsText();
            boolean stateflag=createSessionResponse.contains("A session is already open for the delivery boy.");

            System.out.println(stateflag);

           Assert.assertEquals(true,stateflag);
            System.out.println("Session id is  ="+session_id);

        }
       else {
            Processor process = cmh.createSessionHelper(de_id);
            int createSessionResponse = process.ResponseValidator.GetResponseCode();
            System.out.println(createSessionResponse);
            Assert.assertEquals(200,createSessionResponse,"Expected 200 status code");

            System.out.println("Session created successfully");


        }

    }

    @Test
    public void createSessionAlreadyOpen() throws InterruptedException {
        Object state=cmdh.getSessionStateFromDB(de_id);
        Object session_id=cmdh.getSessionIdFromDB(de_id);

        System.out.println("##################### state is "+state);
        if(state.equals("OPEN")){
            System.out.println("Session is already OPEN");
            Processor process = cmh.createSessionHelper(de_id);
            String createSessionResponse = process.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
            boolean stateflag=createSessionResponse.equalsIgnoreCase("A session is already open for the delivery boy.");

            System.out.println(stateflag);

            Assert.assertEquals(true,stateflag);
            System.out.println("Session id is  ="+session_id);

        }
        else {
            System.out.println("verifying already open session");
            System.out.println(" DE id is "+de_id);
            Processor process = cmh.createSessionHelper(de_id);
            int createSessionResponse = process.ResponseValidator.GetResponseCode();
            System.out.println(createSessionResponse);
            Assert.assertEquals(200,createSessionResponse,"Expected 200 status code");

            System.out.println("Session created successfully");
            Processor process2 = cmh.createSessionHelper(de_id);
            String createSessionResponse2 = process2.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");

            System.out.println("createSessionResponse2");
            boolean stateflag=createSessionResponse2.equalsIgnoreCase("A session is already open for the delivery boy.");

            System.out.println(stateflag);

            Assert.assertEquals(true,stateflag);
            System.out.println("Session id is  ="+session_id);



        }

    }
    //Creating session with inactive DE
    @Test(dataProvider ="getInactiveDEId",description = "Create session with inactive DE")
    public void createSessionForInactiveDE(String de_iid) throws InterruptedException {

        Processor process = cmh.createSessionHelper(de_iid);
        String createSessionResponse = process.ResponseValidator.GetBodyAsText();
        boolean stateflag = createSessionResponse.contains("No de found with the given id");

        System.out.println(stateflag);

        Assert.assertEquals(true, stateflag,"Expected Response not received for inactive DE");




    }

    //Creating session with invalid DE

    @Test(dataProvider ="getInvalidDEId",description = "Create session with invalid DE")
    public void createSessionForInvalidDE(String de_iid) {

        Processor process = cmh.createSessionHelper(de_iid);
        String createSessionResponse = process.ResponseValidator.GetBodyAsText();
        boolean stateflag = createSessionResponse.contains("No de found with the given id");

        Assert.assertEquals(true, stateflag,"Expected Response not received for invalid DE");


    }
     // Test to get session for active DE and validate the Response
    @Test
    public void deSessionsActiveDE(){

        System.out.println("Validate get sessions for de id =  "+de_id);

        Processor process = cmh.getSessionsHelper(de_id,"0","500");
        String getdesessionresp=process.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.deId");


        System.out.println("###########################################################");
        Assert.assertEquals(de_id,getdesessionresp);
        String sessioncountDB=cmdh.getSessionCount(de_id).toString();
        String getsessioncount=process.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.totalSessions");
        System.out.println("Validating session count received in get session api");

        Assert.assertEquals(sessioncountDB,getsessioncount,"Session count not matched");
    }

    //Test to get session for Inactive DE and validate the Response
    @Test (dataProvider= "getInactiveDEId")
    public void deSessionsInactiveDE(String de_iid ){
        System.out.println("Validate get sessions for de id =  "+de_iid);
        Processor process = cmh.getSessionsHelper(de_iid,"0","500");
        String getsessionresp=process.ResponseValidator.GetNodeValue("statusMessage");

        System.out.println("###########################################################");
        Assert.assertEquals("Completed",getsessionresp);

    }


    //Test to get session for invalid DE and validate the Response
    @Test (dataProvider= "getInvalidDEId")
    public void deSessionsInvalidDE(String de_iid ){

        System.out.println("Validate get sessions for de id =  "+de_iid);
        Processor process = cmh.getSessionsHelper(de_iid,"0","500");
        String getsessionresp=process.ResponseValidator.GetNodeValue("data");

        System.out.println("###########################################################");
        Assert.assertEquals(null,getsessionresp);

    }

    @Test
    public void getDESessionDetailsActiveDE() throws InterruptedException {

        String sid=cmdh.getSessionIdFromDB(de_id).toString();
        System.out.println("Validating get session details for DE "+de_id);
        Processor process= cmh.getSessionDetailHelper(sid,de_id,"500","0");
        System.out.println("Validating session id from DB and get session detail API");
        String sidfromapi=process.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.sessionId");
        Assert.assertEquals(sid,sidfromapi,"Session id not matched ");

        String ordercountfromDB=cmdh.getOrderCountFromDB(de_id,sid).toString();
        Thread.sleep(2000);
        String ordercountapi=process.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.orderCount");
        System.out.println("Validating order count from DB and api");
        Assert.assertEquals(ordercountfromDB,ordercountapi,"Order Count Not matched ");

        String fcfromdb=cmdh.getFloatingCashFromDB(de_id).toString();
        System.out.println("Floating Cash From DB :: "+fcfromdb);
        String fcfromapi=process.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.floatingCash");
        System.out.println("Floating Cash From API :: "+fcfromapi);
        System.out.println("Validating Floating cash from DB and API");
        Assert.assertEquals(fcfromdb,fcfromapi,"Floating cash not matched  ");


    }
    // validate get session details if session id is not belong to de
    @Test
    public  void getSessionDetailsForWrongMappedSession() throws InterruptedException {

        System.out.println("validating get session details for DE "+de_id);
        String sid=cmdh.getSessionIdFromDB(de_id).toString();

        Processor process= cmh.getSessionDetailHelper(sid,CashMgmtConstant.INACTIVE_DE,"500","0");
        String statusmsg=process.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
        Assert.assertEquals(statusmsg,"Trying to get a de cash session, that does not belong to the given de. deId: "+CashMgmtConstant.INACTIVE_DE+", sessionId: "+sid,"Expected Response Not received");

    }

    @Test
    public void getSessionDetailsInvalidSession(){

        System.out.println("validating get session details for DE "+de_id);
        Processor process= cmh.getSessionDetailHelper(CashMgmtConstant.INVALID_SESSION,de_id,"500","0");
        String statusmsg=process.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
        Assert.assertEquals(statusmsg,"Did not find any session with id:"+CashMgmtConstant.INVALID_SESSION,"Expected Response Not received");


    }




// get order count for active DE
    @Test
    public void getOrderCountDB() throws InterruptedException {

        //getting the session id from DB
       String sid=cmdh.getSessionIdFromDB(de_id).toString();
       if(sid.equals("0")){
           System.out.println("order count is = "+sid);
       }
        else {
           Object ordercount = cmdh.getOrderCountFromDB(de_id, sid);
           System.out.println(ordercount);
           Object sessioncount = cmdh.getSessionCount(de_id);
           System.out.println("session count is =" + sessioncount);
           Object a = cmdh.getFloatingCashFromDB(de_id);
           System.out.println(a);
           String b = cmdh.getSessionIdFromDB(de_id).toString();
           System.out.println(b);
           Object c = cmdh.getSessionStateFromDB(de_id);
           System.out.println(c);

           Object d = cmdh.getTransactionDB(de_id, b);
           System.out.println(d);
       }


    }






    @Test
    public void addCommentWithValidSessionIdValidDE() throws InterruptedException {

        System.out.println("_____________________________");
        System.out.println("Add comment for DE id =>"+de_id);
        String sid=cmdh.getSessionIdFromDB(de_id).toString();
        System.out.println("Session id is =>"+sid);
        if(sid.equals("0")) {
            Processor process = cmh.addCommentHelper(de_id, sid);
            int getaddcommentresponsecode = process.ResponseValidator.GetResponseCode();
            System.out.println("Response code of Add comment is =>" + getaddcommentresponsecode);

            Assert.assertEquals( getaddcommentresponsecode,500);
        }
        else
        {
            Processor process = cmh.addCommentHelper(de_id, sid);
            int getaddcommentresponsecode = process.ResponseValidator.GetResponseCode();
            System.out.println("Response code of Add comment is =>" + getaddcommentresponsecode);

            Assert.assertEquals( getaddcommentresponsecode,200);
        }

    }

// Adding comment with invalid session id
    @Test
    public void addCommentWithInvalidSessionId() throws InterruptedException {

        System.out.println("_____________________________");
        System.out.println("Add comment for DE id =>"+de_id);

        Processor process=cmh.addCommentHelper(de_id,CashMgmtConstant.INVALID_SESSION);
        int getaddcommentresponsecode=process.ResponseValidator.GetResponseCode();
        System.out.println("Response code of Add comment is =>"+getaddcommentresponsecode);
        Assert.assertEquals(500,getaddcommentresponsecode);

    }


    //validating get comment with valid session
    @Test
    public void  getCommentWithValidSession() throws InterruptedException {

        String sid=cmdh.getSessionIdFromDB(de_id).toString();
        System.out.println("get session id for de "+de_id);
        if (sid.equals("0")){
            Processor process = cmh.createSessionHelper(de_id);
            int createSessionResponse = process.ResponseValidator.GetResponseCode();
            System.out.println(createSessionResponse);
            Assert.assertEquals(200,createSessionResponse,"Expected 200 status code");

            Processor process2=cmh.getCommentHelper(sid);
            String deidfromgetcomment=process2.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.sessionId");
            Assert.assertEquals(sid,deidfromgetcomment,"get comment with valid session id is not working");


        }
       else {
            Processor process = cmh.getCommentHelper(sid);
            String deidfromgetcomment = process.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.sessionId");
            Assert.assertEquals(sid, deidfromgetcomment, "get comment with valid session id is not working");
        }
    }

    @Test
    public void getCommentWithInvalidSession(){
        Processor process=cmh.getCommentHelper(CashMgmtConstant.INVALID_SESSION);
        int getCommmentResponseCode=process.ResponseValidator.GetResponseCode();
        Assert.assertEquals(500,getCommmentResponseCode);

    }

    @Test
    public void getChannelDetailsInvalidDEInvalidChannelID(){
        Processor processor= cmh.channelDetailsHelper(CashMgmtConstant.INVALID_CHANNELID,CashMgmtConstant.INVALID_DE);
        String channel_message=processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
        Assert.assertEquals("Channel not found",channel_message,"Channel details not working");
    }


    @Test
    public void getChannelDetailsActiveDEInvalidChannelID(){
        Processor processor= cmh.channelDetailsHelper(CashMgmtConstant.INVALID_CHANNELID,de_id);
        String channel_message=processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
        Assert.assertEquals("Channel not found",channel_message,"Channel details not working");
    }

    @Test
    public void getChannelDetailsInvalidDEValidChannelID(){
        Processor processor= cmh.channelDetailsHelper(CashMgmtConstant.VALID_CHANNELID,CashMgmtConstant.INVALID_DE);
        String channel_message=processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
        Assert.assertEquals("DE Id not found",channel_message,"Channel details not working");
    }

    @Test
    public void getChannelDetailsActiveDEValidChannel(){

        Processor processor= cmh.channelDetailsHelper(CashMgmtConstant.VALID_CHANNELID,de_id);
        String channel_message=processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.steps[2].dynamic_code");
        boolean cdm_id=channel_message.endsWith(de_id);
        Assert.assertEquals(cdm_id,true,"response not matched");
    }

    @Test
    public void getChannelOptionsInvalidDE(){
        Processor processor=cmh.channelOptionHelper(CashMgmtConstant.INVALID_DE);
        String channelOptionResponse=processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
        Assert.assertEquals(channelOptionResponse,"DE Id not found");

    }
    @Test
    public void getChannelOptionsActiveDE(){
        Processor processor=cmh.channelOptionHelper(de_id);
        String channelOptionResponse=processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
        Assert.assertEquals(channelOptionResponse,"Success");

    }





}
