package com.swiggy.api.erp.vms.td.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

/***
 * 
 * @author ramzi
 *
 */
public class Category {

    private String id;
    private String name;
    private List<SubCategory> subCategories = null;
    private ArrayList<Menu> menu=null;

   	/**
     * No args constructor for use in serialization
     *
     */
    public Category() {
    }

    /**
    *
    * @param id
    * @param menu
    * @param name
    */
    public Category(String id, String name, ArrayList<Menu> menu) {
        super();
        this.id = id;
        this.name = name;
        this.menu = menu;
    }
   
    /**
     *
     * @param id
     * @param subCategories
     * @param name
     */
    public Category(String id, String name,  List<SubCategory> subCategories) {
        super();
        this.id = id;
        this.name = name;
        this.subCategories = subCategories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }
    
    public ArrayList<Menu> getMenu() {
		return menu;
	}

	public void setMenu(ArrayList<Menu> menu) {
		this.menu = menu;
	}

     
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("subCategories", subCategories).append("menu",menu).toString();
    }

}
