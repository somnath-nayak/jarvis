package com.swiggy.api.erp.crm.tests.IntegrationServicesTests;

import com.swiggy.api.erp.crm.constants.CRMConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertTrue;

public  class IntegrationIssueTypeMT {

    Initialize gameofthrones = new Initialize();

    @Test(description = "Issue Listing Verification")
    public void integrationCancellationFlow() {

        List<String> expectedIssueTypes = Arrays.asList(CRMConstants.ORDERISSUETYPE, CRMConstants.GENERALISSUETYPE, CRMConstants.LEGALISSUETYPE,
                CRMConstants.FAQISSUETYPE, CRMConstants.SUPERFAQISSUETYPE, CRMConstants.DASHORDERISSUETYPE);

        HashMap<String, String> requestheaders_IssueListing = new HashMap<String, String>();
        requestheaders_IssueListing.put("Content-Type","application/json");
        List<String> testing = Arrays.asList(CRMConstants.POSITIVETEST,CRMConstants.NEGATIVETEST);
        for(int i=0; i<testing.size(); i++) {

            if (testing.get(i) == "positive") {
                requestheaders_IssueListing.put("tenantId", "eefdeac62db1ef01652053dd85f474ef626f3710277a63e77e109fc35cdaff6e");
                GameOfThronesService issuelistinginbound = new GameOfThronesService("crm", "getOrderIssues", gameofthrones);

                Processor issuelistinginbound_response = new Processor(issuelistinginbound,
                        requestheaders_IssueListing, null, null);
                Assert.assertEquals(issuelistinginbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");
                String issueLists = issuelistinginbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..issueType").toString().replace("[", "").replace("]", "");
                issueLists = issueLists.replace("\"","");
                List<String> actualIssueList = Arrays.asList(issueLists.split(","));
                System.out.println("actualIssueList " + actualIssueList);
                System.out.println("expectedIssueTypes " + expectedIssueTypes);
                assertTrue(actualIssueList.containsAll(expectedIssueTypes), "Actual result doesn't match with Expected ");
            } else if(testing.get(i) == "negative"){
                requestheaders_IssueListing.put("tenantId", "eefeac62db1ef01652053dd85f474ef626f3710277a63e77e109fc35cdaff6e");
                GameOfThronesService issuelistinginbound = new GameOfThronesService("crm", "getOrderIssues", gameofthrones);

                Processor issuelistinginbound_response = new Processor(issuelistinginbound,
                        requestheaders_IssueListing, null, null);
                Assert.assertEquals(issuelistinginbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");
                String expectedErrorMessage = "is not registered with us. Please contact support to register the tenant with regression.";
                expectedErrorMessage = expectedErrorMessage.replace("\"","");
                String actualErrorMessage = issuelistinginbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception..errorMessage").toString().replace("[", "").replace("]", "");
                System.out.println("Actual Error Message  " + actualErrorMessage);
                assertTrue(actualErrorMessage.contains(expectedErrorMessage),"Actual error message doesn't match with expected ");
            }
        }
    }

}
