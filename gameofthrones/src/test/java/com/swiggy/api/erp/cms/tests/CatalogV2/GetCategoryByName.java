package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetCategoryByName {
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
    String catName;
    @Test(description = "Get Category by Name")
    public void getCategoryByName(){
        String response=catalogV2Helper.createCategoryPojo().ResponseValidator.GetBodyAsText();
        catName=JsonPath.read(response,"$.name");
        String response1=catalogV2Helper.getCategoryByName(catName).ResponseValidator.GetBodyAsText();
        System.out.println(response1);
        String catName2=JsonPath.read(response1,"$.[0].name");
        Assert.assertNotNull(catName2);
    }
}
