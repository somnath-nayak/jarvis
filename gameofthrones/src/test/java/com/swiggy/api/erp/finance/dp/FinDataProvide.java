package com.swiggy.api.erp.finance.dp;

import com.swiggy.api.erp.finance.constants.FinConstants;
import com.swiggy.api.erp.finance.constants.FinanceConstants;
import com.swiggy.api.erp.finance.helper.FinanceHelper;
import framework.gameofthrones.JonSnow.DateHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import com.swiggy.api.erp.ff.helper.LOSHelper;


public class FinDataProvide {

    static LOSHelper losHelper = new LOSHelper();
    public static FinanceHelper financeHelper =  new FinanceHelper();
    DateHelper dateHelper = new DateHelper();
    public static String restID = financeHelper.createRestaurantOfGivenAgreementType("pre_paid");

    @DataProvider(name="getOrderId")
    public Object[][] getOrderId() throws IOException, InterruptedException {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        String order_time = dateFormat.format(date);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        String status = "delivered";

        //Create the manual order
        losHelper.createManualOrder(order_id,order_time,epochTime);

        //Update the status of the created order to delivered
        losHelper.statusUpdate(order_id, order_time, status);

        return new Object[][]{
                {order_id}
        };
    }

    @DataProvider(name="getOrder")
    public Object[][] getOnlyOrder() throws IOException, InterruptedException {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        String order_time = dateFormat.format(date);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());

        //Create the manual order
        losHelper.createManualOrder(order_id,order_time,epochTime);
        losHelper.statusUpdate(order_id, order_time, FinConstants.deliveredstatus);

        return new Object[][]{
                {order_id}
        };
    }

    @DataProvider(name="cancelorder")
    public Object[][] createAndCancelOrder() throws IOException, InterruptedException {
        String[] order_id = new String[2];
        String[] order_time = new String[2];
        String[] epochTime = new String[2];
        for(int i=0;i<2;i++) {
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            order_id[i] = order_idFormat.format(date);
            order_time[i] = dateFormat.format(date);
            epochTime[i] = String.valueOf(Instant.now().getEpochSecond());
            losHelper.createManualOrder(order_id[i], order_time[i], epochTime[i]);
            losHelper.cancelOrder(order_id[i], FinConstants.food_prepared[i]);
        }

        return new Object[][]{
                {order_id[0], FinConstants.food_prepared[0]},
                {order_id[1], FinConstants.food_prepared[1]}
        };
    }

    @DataProvider(name="update_de_Status")
    public Object[][] destatus() throws IOException, InterruptedException {
        String[] order_id = new String[2];
        String[] order_time = new String[2];
        String[] epochTime = new String[2];
        for(int i=0;i<2;i++) {
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            order_id[i] = order_idFormat.format(date);
            order_time[i] = dateFormat.format(date);
            epochTime[i] = String.valueOf(Instant.now().getEpochSecond());
            losHelper.createManualOrder(order_id[i], order_time[i], epochTime[i]);
            if(i == 0)losHelper.statusUpdate(order_id[i], order_time[i], FinConstants.deliveredstatus);
            else losHelper.deliveryStatusUpdate(order_id[i], order_time[i], "7", "7", "7");
        }
        return new Object[][]{
                {order_id[0], FinConstants.food_prepared[0]},
                {order_id[1], FinConstants.food_prepared[1]},
        };
    }

    @DataProvider(name="submitorder")
    public Object[][] submit() throws IOException, InterruptedException {
        String order_id = new String();
        String order_time = new String();
        String epochTime = new String();
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            order_id = order_idFormat.format(date);
            order_time= dateFormat.format(date);
            epochTime = String.valueOf(Instant.now().getEpochSecond());
            losHelper.createManualOrder(order_id, order_time, epochTime);
            losHelper.deliveryStatusUpdate(order_id, order_time, "7", "7", "7");
        return new Object[][]{
                {order_id}
        };
    }

    @DataProvider(name = "invoicejob")
    public Object[][] invoicejob() {
        String toDate = dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss();
        String fromDate = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(-1);
       String[] orderTime = financeHelper.createOrderOfAGivenType(restID,"Prepaid","false","[bill_without_taxes]*0.05");
        return new Object[][]{
               {fromDate, toDate, FinanceConstants.user_id,orderTime[0]}
        };
    }

    @DataProvider(name = "invoicecancel")
    public Object[][] invoicecancel() {
        return new Object[][] {
                {FinConstants.task_id, FinConstants.user}
        };
    }

    @DataProvider(name = "invoiceemail")
    public Object[][] invoiceemail() {
        return new Object[][] {
                {FinConstants.task_id, FinConstants.testemail}
        };
    }

    @DataProvider(name = "reconconfig")
    public Object[][] reconconfig() {
        return new Object[][] {
                {FinConstants.key, FinConstants.value}
        };
    }

    @DataProvider(name = "manualinvoice")
    public Object[][] manualainvoice() {
        return new Object[][] {
                {FinConstants.fileurl, FinConstants.user}
        };
    }

    @DataProvider(name = "currentpayout")
    public Object[][] currentpayout() {
        String toDate;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        toDate = dateFormat.format(date);
        return new Object[][] {
                {FinConstants.from_time, toDate, FinConstants.user}
        };
    }

    @DataProvider(name = "reconlogout")
    public Object[][] reconlogout() {
        return new Object[][] {
                {FinConstants.fileurl, FinConstants.user}
        };
    }

    @DataProvider(name = "addquickrecon")
    public Object[][] addquickrecon() {
        return new Object[][] {
                {FinConstants.fileurl, FinConstants.user}
        };
    }

    @DataProvider(name = "autoassignment")
    public Object[][] autoassignment() {
        return new Object[][] {
                {FinConstants.user}
        };
    }

    @DataProvider(name = "addannexure")
    public Object[][] addannexure() {
        return new Object[][] {
                {FinConstants.fileurl, FinConstants.user}
        };
    }

    @DataProvider(name = "restaurantpendingapproval")
    public Object[][] restaurantpendingapproval() {
        return new Object[][] {
                {FinConstants.pageno}
        };
    }

    @DataProvider(name = "restaurantidpendingapproval")
    public Object[][] restaurantidpendingapproval() {
        return new Object[][] {
                {FinConstants.pageno, FinConstants.restaurantId}
        };
    }

    @DataProvider(name = "createreports")
    public Object[][] createreports() {
        String toDate;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        toDate = dateFormat.format(date);
        return new Object[][] {
                {FinConstants.from_time_formatted, toDate, FinConstants.user, FinConstants.type}
        };
    }

    @DataProvider(name = "swiggypendingapproval")
    public Object[][] swiggypendingapproval() {
        return new Object[][] {
                {FinConstants.pageno}
        };
    }

    @DataProvider(name = "restaurantpendingpayout")
    public Object[][] restaurantpendingpayout() {
        String toDate;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        toDate = dateFormat.format(date);
        return new Object[][] {
                {FinConstants.from_time, toDate, FinConstants.excludelist}
        };
    }

    @DataProvider(name = "restaurantreleasepayout")
    public Object[][] restaurantreleasepayout() {
        String toDate;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        toDate = dateFormat.format(date);
        return new Object[][] {
                {FinConstants.from_time, toDate, FinConstants.user, FinConstants.restaurantId}
        };
    }

    @DataProvider(name = "swiggyreleasepayout")
    public Object[][] swiggyreleasepayout() {
        String toDate;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        toDate = dateFormat.format(date);
        return new Object[][] {
                {FinConstants.from_time, toDate, FinConstants.user, FinConstants.restaurantId}
        };
    }

    @DataProvider(name = "swiggypendingpayout")
    public Object[][] swiggypendingpayout() {
        String toDate;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        toDate = dateFormat.format(date);
        return new Object[][] {
                {FinConstants.from_time, toDate, FinConstants.pageno, FinConstants.pagesize, FinConstants.excludelist}
        };
    }

    @DataProvider(name = "nodalpayout")
    public Object[][] nodalpayout() {
        String toDate;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        toDate = dateFormat.format(date);
        return new Object[][] {
                {FinConstants.cityid, FinConstants.from_time, toDate}
        };
    }

}
