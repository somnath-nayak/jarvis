package com.swiggy.api.erp.ff.tests.fraudService;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.POJO.CouponFraudCheck.CouponCheck;
import com.swiggy.api.erp.ff.POJO.CouponFraudCheck.Coupon_details;
import com.swiggy.api.erp.ff.POJO.CouponFraudCheck.Customer_details;
import com.swiggy.api.erp.ff.constants.FraudServiceConstants;
import com.swiggy.api.erp.ff.dp.FraudServiceDP.CouponAbusesDP;
import com.swiggy.api.erp.ff.helper.FraudServiceHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.List;

public class CouponAbuses {

    JsonHelper j = new JsonHelper();
    FraudServiceHelper fsh = new FraudServiceHelper();
    CouponCheck cc;
    Processor processor;
    RedisHelper redisHelper = new RedisHelper();

    @BeforeClass
    public void setRedis(){


        fsh.addCouponThreshold(FraudServiceConstants.category1,FraudServiceConstants.value2);
        fsh.addCouponThreshold(FraudServiceConstants.category2,FraudServiceConstants.value0);
        fsh.addCouponThreshold(FraudServiceConstants.category3,FraudServiceConstants.value3);
        fsh.addCouponThreshold(FraudServiceConstants.category4,FraudServiceConstants.value4);
        fsh.addCouponThreshold(FraudServiceConstants.category6,FraudServiceConstants.value6);
        fsh.addCouponThreshold(FraudServiceConstants.category7,FraudServiceConstants.value7);
        fsh.addCouponThreshold(FraudServiceConstants.category12,FraudServiceConstants.value12);
        fsh.addCouponThreshold(FraudServiceConstants.category14,FraudServiceConstants.value14);
        fsh.addCouponThreshold(FraudServiceConstants.category15,FraudServiceConstants.value15);
        fsh.addCouponThreshold(FraudServiceConstants.category16,FraudServiceConstants.value16);




        fsh.addCouponDeviceCount(FraudServiceConstants.category1,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_01);
        fsh.addCouponDeviceCount(FraudServiceConstants.category3,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_03);
        fsh.addCouponDeviceCount(FraudServiceConstants.category4,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_04);
        fsh.addCouponDeviceCount(FraudServiceConstants.category10,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_10);
        fsh.addCouponDeviceCount(FraudServiceConstants.category11,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_11);
        fsh.addCouponDeviceCount(FraudServiceConstants.category12,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_12);
        fsh.addCouponDeviceCount(FraudServiceConstants.category16,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_16);
        fsh.addCouponDeviceCount(FraudServiceConstants.category6,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_06);
        fsh.addCouponDeviceCount(FraudServiceConstants.category7,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_07);
        fsh.addCouponDeviceCount(FraudServiceConstants.category9,FraudServiceConstants.deviceId1,FraudServiceConstants.category_count_0);


    }

    @Test(dataProvider= "CouponCheck",dataProviderClass = CouponAbusesDP.class,description="Coupon fraud check validations")
    public void couponAbusesFraudCheck(Customer_details cus, Coupon_details cou) throws IOException {
        cc = new CouponCheck(cus,cou);
        String body = j.getObjectToJSON(cc);
        processor = fsh.getCouponAbusesProcessor(body);
        String response = processor.ResponseValidator.GetBodyAsText();
        fsh.statusCheck(processor);
        String deviceId = cus.getDevice_id();
        List coupon_cat_details = JsonPath.read(response,"$..coupon_cat_details.*");
        couponCheckbasedVerification(deviceId,coupon_cat_details,response);

    }

    @Test(dataProvider= "multipleCouponCategories",dataProviderClass = CouponAbusesDP.class,description="Coupon fraud check validations")
    public void multipleCouponCheck(Customer_details cus, Coupon_details cou)throws IOException {
        cc = new CouponCheck(cus,cou);
        String body = j.getObjectToJSON(cc);
        processor = fsh.getCouponAbusesProcessor(body);
        String response = processor.ResponseValidator.GetBodyAsText();
        fsh.statusCheck(processor);
        String deviceId = cus.getDevice_id();
        List coupon_cat_details = JsonPath.read(response,"$..coupon_cat_details.*");
        couponCheckbasedVerification(deviceId,coupon_cat_details,response);

    }

    @Test(dataProvider= "withoutCouponDetails",dataProviderClass = CouponAbusesDP.class,description="Coupon fraud check validations")
    public void withoutCouponDetails(Customer_details cus) throws IOException {
        cc = new CouponCheck(cus);
        String body = j.getObjectToJSON(cc);
        processor = fsh.getCouponAbusesProcessor(body);
        String response = processor.ResponseValidator.GetBodyAsText();
        fsh.statusCheck(processor);
        String deviceId = cus.getDevice_id();
        List coupon_cat_details = JsonPath.read(response,"$..coupon_cat_details.*");
        couponCheckbasedVerification(deviceId,coupon_cat_details,response);


    }

    @Test(dataProvider= "fieldLevelValidation",dataProviderClass = CouponAbusesDP.class,description="Coupon fraud check validations")
    public void couponAbusesFieldLevelCheck(Customer_details cus, Coupon_details cou) throws IOException {
        cc = new CouponCheck(cus,cou);
        String body = j.getObjectToJSON(cc);
        processor = fsh.getCouponAbusesProcessor(body);
        String response = processor.ResponseValidator.GetBodyAsText();
        int statusCode = processor.ResponseValidator.GetResponseCode();
        String message = JsonPath.read(response,"$.message");
        Assert.assertEquals(statusCode,400);
        Assert.assertEquals(message,"invalid_request");

    }


    @Test(dataProvider= "rmqOrderCancellationPaymentConfirmation",dataProviderClass = CouponAbusesDP.class,description="Coupon fraud check validations")
    public void rmqValidationForPostPaymentAndCancelOrder(String deviceId, String categoryId) throws IOException {
        if(categoryId.equalsIgnoreCase("CAT10"))
        {
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterPaymentConfirmationStaus,deviceId,categoryId);
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterCancelOrderStatus,deviceId,categoryId);

        }
        if(categoryId.equalsIgnoreCase("CAT11"))
        {
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterCancelOrderStatus,deviceId,categoryId);
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterPaymentConfirmationStaus,deviceId,categoryId);


        }
        String value = fsh.getCouponDeviceCount(deviceId,categoryId);
        if(deviceId.equalsIgnoreCase(FraudServiceConstants.deviceId1))
        {
            switch (categoryId)
            {
                case "CAT10":  Assert.assertEquals(value,"10","Count is not updated in redis");  break;
                case "CAT11":  Assert.assertEquals(value,"11","Count is not updated in redis");  break;
            }
        }


    }

    @Test(dataProvider= "apiResponsePaymentConfirmation",dataProviderClass = CouponAbusesDP.class,description="Coupon fraud check validations")
    public void apiResponseValidationWithPaymentConfirmation(String deviceId, String category) throws IOException {

        processor = fsh.getCouponAbusesAPIProcessor(deviceId,category);

        String response = processor.ResponseValidator.GetBodyAsText();
        fsh.statusCheck(processor);
        boolean is_fraudulent = JsonPath.read(response, "$.data.coupon_cat_details[0].is_fraudulent");

        if(category.equalsIgnoreCase("CAT5"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterPaymentConfirmationStaus,FraudServiceConstants.deviceId1, FraudServiceConstants.category5);

        }
        if(category.equalsIgnoreCase("CAT2"))
        {
            Assert.assertEquals(is_fraudulent, true, "Fraudulent is coming true in this case");
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterPaymentConfirmationStaus,FraudServiceConstants.deviceId1, FraudServiceConstants.category2);

        }

        if(category.equalsIgnoreCase("CAT6"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterPaymentConfirmationStaus,FraudServiceConstants.deviceId1, FraudServiceConstants.category6);

        }
        if(category.equalsIgnoreCase("CAT7"))
        {
            Assert.assertEquals(is_fraudulent, true, "Fraudulent is coming true in this case");
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterPaymentConfirmationStaus,FraudServiceConstants.deviceId1, FraudServiceConstants.category7);

        }
        if(category.equalsIgnoreCase("CAT8"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterPaymentConfirmationStaus,FraudServiceConstants.deviceId1, FraudServiceConstants.category8);

        }
        if(category.equalsIgnoreCase("CAT9"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
            fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterPaymentConfirmationStaus,FraudServiceConstants.deviceId1, FraudServiceConstants.category9);

        }



        processor = fsh.getCouponAbusesAPIProcessor(deviceId,category);
        response = processor.ResponseValidator.GetBodyAsText();
        fsh.statusCheck(processor);
        is_fraudulent = JsonPath.read(response, "$.data.coupon_cat_details[0].is_fraudulent");

        if(category.equalsIgnoreCase("CAT2"))
        {
            Assert.assertEquals(is_fraudulent, true, "Fraudulent is coming true in this case");
        }
        if(category.equalsIgnoreCase("CAT5"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
        }

        if(category.equalsIgnoreCase("CAT6"))
        {
            Assert.assertEquals(is_fraudulent, true, "Fraudulent is coming true in this case");
        }
        if(category.equalsIgnoreCase("CAT7"))
        {
            Assert.assertEquals(is_fraudulent, true, "Fraudulent is coming true in this case");
        }
        if(category.equalsIgnoreCase("CAT8"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
        }
        if(category.equalsIgnoreCase("CAT9"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
        }
    }

    @Test(dataProvider= "apiResponsePaymentCancellation",dataProviderClass = CouponAbusesDP.class,description="Coupon fraud check validations")
    public void apiResponseValidationWithPaymentCancellation(String deviceId, String category) throws IOException {


        Processor processor = fsh.getCouponAbusesAPIProcessor(deviceId,category);

        String response = processor.ResponseValidator.GetBodyAsText();

        fsh.statusCheck(processor);
        boolean is_fraudulent = JsonPath.read(response, "$.data.coupon_cat_details[0].is_fraudulent");

        if(category.equalsIgnoreCase("CAT16"))
        {
            Assert.assertEquals(is_fraudulent, true, "Fraudulent is coming true in this case");
        }

        if(category.equalsIgnoreCase("CAT13"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
        }

        if(category.equalsIgnoreCase("CAT14"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
        }
        if(category.equalsIgnoreCase("CAT15"))
        {
            Assert.assertEquals(is_fraudulent, true, "Fraudulent is coming true in this case");
        }


        fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterCancelOrderStatus,FraudServiceConstants.deviceId1, FraudServiceConstants.category12);
        fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterCancelOrderStatus,FraudServiceConstants.deviceId1, FraudServiceConstants.category13);
        fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterCancelOrderStatus,FraudServiceConstants.deviceId1, FraudServiceConstants.category14);
        fsh.pushMessageToRMQForCouponCount(FraudServiceConstants.afterCancelOrderStatus,FraudServiceConstants.deviceId1, FraudServiceConstants.category15);

        processor = fsh.getCouponAbusesAPIProcessor(deviceId,category);
        response = processor.ResponseValidator.GetBodyAsText();
        fsh.statusCheck(processor);
        is_fraudulent = JsonPath.read(response, "$.data.coupon_cat_details[0].is_fraudulent");

        if(category.equalsIgnoreCase("CAT12"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
        }
        if(category.equalsIgnoreCase("CAT13"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
        }
        if(category.equalsIgnoreCase("CAT14"))
        {
            Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");
        }
        if(category.equalsIgnoreCase("CAT15"))
        {
            Assert.assertEquals(is_fraudulent, true, "Fraudulent is coming true in this case");
        }

    }

    public void couponCheckbasedVerification(String deviceId,List coupon_cat_details,String response) {

        int status = JsonPath.read(response, "$.status");
        String message = JsonPath.read(response, "$.message");

        Assert.assertEquals(status, 0, "Status in response is different");
        Assert.assertNull(message, "Message in reponse in not coming null");

        for (int i = 0; i < coupon_cat_details.size(); i++) {
            String category = JsonPath.read(response, "$.data.coupon_cat_details["+i+"].coupon_cat");
            boolean is_fraudulent = JsonPath.read(response, "$.data.coupon_cat_details["+i+"].is_fraudulent");
            String reason = JsonPath.read(response, "$.data.coupon_cat_details["+i+"].reason");

            System.out.println(category+"======================");

            if (deviceId.equalsIgnoreCase(FraudServiceConstants.deviceId1)) {
                if(category.equalsIgnoreCase( "CAT1")||category.equalsIgnoreCase( "CAT5")||category.equalsIgnoreCase( "CAT10")||category.equalsIgnoreCase( "CAT11")||category.equalsIgnoreCase( "CAT12")||category.equalsIgnoreCase( "CAT9"))
                {
                    Assert.assertNull(reason, "Reason is not null incase of fraudulent is false");
                    Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");

                }else
                {
                    Assert.assertEquals(reason, "coupon_limit_for_device_breached", "Wrong reason is coming in reponse");
                    Assert.assertEquals(is_fraudulent, true, "Fraudlent is coming as false");

                }
            } else {
                switch(category)
                {
                    case "CAT2":
                        Assert.assertEquals(reason, "coupon_limit_for_device_breached", "Wrong reason is coming in reponse");
                        Assert.assertEquals(is_fraudulent, true, "Fraudlent is coming as false");
                        break;
                    default:  Assert.assertNull(reason, "Reason is not null incase of fraudulent is false");
                        Assert.assertEquals(is_fraudulent, false, "Fraudulent is coming true in this case");


                }
            }
        }

    }


}
