package com.swiggy.api.erp.crm.foodissues.pojo.CCResolution;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"resolutionRequestData",
"resolutions"
})
public class CCResolutionPOJO {

@JsonProperty("resolutionRequestData")
private ResolutionRequestData resolutionRequestData;
@JsonProperty("resolutions")
private List<Resolution> resolutions = null;

@JsonProperty("resolutionRequestData")
public ResolutionRequestData getResolutionRequestData() {
return resolutionRequestData;
}

@JsonProperty("resolutionRequestData")
public void setResolutionRequestData(ResolutionRequestData resolutionRequestData) {
this.resolutionRequestData = resolutionRequestData;
}

@JsonProperty("resolutions")
public List<Resolution> getResolutions() {
return resolutions;
}

@JsonProperty("resolutions")
public void setResolutions(List<Resolution> resolutions) {
this.resolutions = resolutions;
}

    public CCResolutionPOJO(ResolutionRequestData resolutionRequestData, List<Resolution> resolutions) {
        this.resolutionRequestData = resolutionRequestData;
        this.resolutions = resolutions;
    }

    public CCResolutionPOJO() {
    }

    @Override
public String toString(){
    return getResolutionRequestData() + ", "+getResolutions();
}


}