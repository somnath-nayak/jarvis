package com.swiggy.api.erp.ff.tests.fraudService;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.POJO.*;
import com.swiggy.api.erp.ff.constants.FraudServiceConstants;
import com.swiggy.api.erp.ff.dp.FraudServiceDP.CODStatusDP;
import com.swiggy.api.erp.ff.dp.FraudServiceDP.CODStatusData;
import com.swiggy.api.erp.ff.dp.FraudServiceDP.FraudServiceData;
import com.swiggy.api.erp.ff.helper.FraudServiceHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;
//import org.codehaus.groovy.runtime.powerassert.SourceText;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class CODStatus {

    CODCheckPayload ch;
    JsonHelper j = new JsonHelper();
    FraudServiceHelper fsh = new FraudServiceHelper();
    Processor processor;
    WireMockHelper wireMockHelper = new WireMockHelper();

//    @BeforeClass
//    public void mockServerStart()
//    {
//        fsh.clearRedisKeys(FraudServiceConstants.CityId_5);//delete redis Keys first
//        fsh.clearRedisKeys(FraudServiceConstants.CityId_6);
//        wireMockHelper.startMockServer(FraudServiceConstants.mockPort);
//        fsh.AddCityConfigurations(FraudServiceConstants.CityId_5, FraudServiceConstants.startTime, FraudServiceConstants.endTime, FraudServiceConstants.Cityfee_300);
//        fsh.AddCityConfigurations(FraudServiceConstants.CityId_6, FraudServiceConstants.startTime, FraudServiceConstants.endTime, FraudServiceConstants.Cityfee_0);
//        fsh.AddToExceptionList(FraudServiceConstants.userID1); //added customerid to exclusion list
//        String previousHour = fsh.getPreviousHour();
//        fsh.AddCityConfigurations(FraudServiceConstants.CityId_7, FraudServiceConstants.startTime, previousHour, FraudServiceConstants.Cityfee_300);
//
//    }

    @Test(dataProvider = "exclustionList", dataProviderClass = CODStatusData.class,description = "COD check for Exclusion list")
    public void codStatus_exclustionList(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        String customerId = customerDetails.getCustomerId().toString();
        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();
        Processor codStatusProcessor = fsh.getCODStatusProcessor(customerId);
        fsh.statusCheck(processor);
        String codCheck = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.cod_enabled").toString();
        System.out.println(codCheck+"<=========================");
        fsh.statusCheck(codStatusProcessor);
        Assert.assertNull(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(),"$.message"));
        String codStatus = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.cod_enabled").toString();
        Assert.assertEquals(codCheck,codStatus);
        Assert.assertEquals(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.reason").toString(),"cod_exclusion_check");


    }



    @Test(dataProvider = "PendingCancellationFee", dataProviderClass = CODStatusData.class,description = "COD status for PendingCancellationFee")
    public void codStatus_pendingCancellationFee(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        String customerId = customerDetails.getCustomerId().toString();
        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();
        Processor codStatusProcessor = fsh.getCODStatusProcessor(customerId);
        fsh.statusCheck(processor);
        String codCheck = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.cod_enabled").toString();
        System.out.println(codCheck+"<=========================");
        fsh.statusCheck(codStatusProcessor);
        String codStatus = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.cod_enabled").toString();
        Assert.assertEquals(codCheck,codStatus);
        Assert.assertNull(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(),"$.message"));

        if(codCheck.equalsIgnoreCase("true"))
        {
            Assert.assertEquals(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.reason").toString(), "cod_model_check");

        }else {
            Assert.assertEquals(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.reason").toString(), "pending_cancellation_fee_check");
        }

    }





    @Test(dataProvider = "CityConfigurations", dataProviderClass = CODStatusData.class,description = "COD status for CityConfigurations")
    public void codStatus_cityConfigurations(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        String customerId = customerDetails.getCustomerId().toString();
        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();
        Processor codStatusProcessor = fsh.getCODStatusProcessor(customerId);
        fsh.statusCheck(processor);
        String codCheck = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.cod_enabled").toString();
        System.out.println(codCheck+"<=========================");
        fsh.statusCheck(codStatusProcessor);
        String codStatus = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.cod_enabled").toString();
        Assert.assertEquals(codCheck,codStatus);
        Assert.assertNull(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(),"$.message"));

        if(codCheck.equalsIgnoreCase("true"))
        {
            Assert.assertEquals(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.reason").toString(), "cod_model_check");

        }else {
            Assert.assertEquals(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.reason").toString(), "late_night_fee_check");
        }

    }





    @Test(dataProvider = "DSCheck", dataProviderClass = CODStatusData.class,description = "COD status for DS True")
    public void codStatus_DSCheck_True(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        String customerId = customerDetails.getCustomerId().toString();
        fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
        fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0,FraudServiceConstants.status_1);


        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        Processor codStatusProcessor = fsh.getCODStatusProcessor(customerId);
        String codCheck = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.cod_enabled").toString();
        String codCheckMsg = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.reason").toString();

        System.out.println(codCheck+"<=========================");
        fsh.statusCheck(codStatusProcessor);
        String codStatus = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.cod_enabled").toString();
        String codStatusMsg = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.reason").toString();
        Assert.assertNull(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(),"$.message"));

        Assert.assertEquals(codCheck,codStatus);
        Assert.assertEquals(codStatusMsg,codCheckMsg);

    }




    @Test(dataProvider = "DSCheck", dataProviderClass = CODStatusData.class,description = "COD status for DS False")
    public void codStatus_DSCheck_False(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        String customerId = customerDetails.getCustomerId().toString();
        fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
        fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_1,FraudServiceConstants.status_1);


        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        Processor codStatusProcessor = fsh.getCODStatusProcessor(customerId);
        String codCheck = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.cod_enabled").toString();
        String codCheckMsg = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.reason").toString();

        System.out.println(codCheck+"<=========================");
        fsh.statusCheck(codStatusProcessor);
        String codStatus = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.cod_enabled").toString();
        String codStatusMsg = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.reason").toString();
        Assert.assertNull(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(),"$.message"));
        Assert.assertEquals(codCheck,codStatus);
        Assert.assertEquals(codStatusMsg,codCheckMsg);

    }



    @Test(dataProvider = "fieldLevelValidation", dataProviderClass = CODStatusData.class,description = "COD status Field level validation")
    public void codStatus_FieldLevelValidations(String customerId) {

        Processor codStatusProcessor = fsh.getCODStatusProcessor(customerId);
        String response = codStatusProcessor.ResponseValidator.GetBodyAsText();
   if(customerId.equalsIgnoreCase(""))
    {
       Assert.assertEquals(codStatusProcessor.ResponseValidator.GetResponseCode(),400);
       Assert.assertEquals(JsonPath.read(response,"$.message"),"getUserCodStatus.customerId must not be empty");

    }
    else
   {
       Assert.assertEquals(codStatusProcessor.ResponseValidator.GetResponseCode(),404);
       Assert.assertEquals(JsonPath.read(response,"$.message"),"No COD properties found");
       Assert.assertNull(JsonPath.read(response,"$.data"));

   }


    }




    @Test(dataProvider = "DSCheck", dataProviderClass = CODStatusData.class,description = "COD status for DS False")
    public void codStatus_DSPFallback_status_0(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        String customerId = customerDetails.getCustomerId().toString();
        fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
        fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_1,FraudServiceConstants.status_0);


        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        Processor codStatusProcessor = fsh.getCODStatusProcessor(customerId);
        String codCheck = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.cod_enabled").toString();
        String codCheckMsg = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.reason").toString();

        System.out.println(codCheck+"<=========================");
        fsh.statusCheck(codStatusProcessor);
        String codStatus = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.cod_enabled").toString();
        String codStatusMsg = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.reason").toString();
        Assert.assertNull(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(),"$.message"));

        Assert.assertEquals(codCheck,codStatus);
        Assert.assertEquals(codStatusMsg,codCheckMsg);

    }



    @Test(dataProvider = "DSCheck", dataProviderClass = CODStatusData.class,description = "COD status for DS False")
    public void codStatus_DSPFallback_With_Delay(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        String customerId = customerDetails.getCustomerId().toString();
        fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
        fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_1,FraudServiceConstants.status_1,FraudServiceConstants.delay);


        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        Processor codStatusProcessor = fsh.getCODStatusProcessor(customerId);
        String codCheck = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.cod_enabled").toString();
        String codCheckMsg = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.reason").toString();

        System.out.println(codCheck+"<=========================");
        fsh.statusCheck(codStatusProcessor);
        String codStatus = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.cod_enabled").toString();
        String codStatusMsg = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(), "$.data.reason").toString();
        int status = JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(),"$.status");
        Assert.assertEquals(status,0);
        Assert.assertEquals(codCheck,codStatus);
        Assert.assertEquals(codStatusMsg,codCheckMsg);
        Assert.assertNull(JsonPath.read(codStatusProcessor.ResponseValidator.GetBodyAsText(),"$.message"));

    }



//
//    @AfterClass
//    public void mockServerStop() {
//
////        try {
////            Thread.sleep(1000000000);
////        } catch (InterruptedException e) {
////            e.printStackTrace();
////        }
//
//        wireMockHelper.stopMockServer();
//
//    }




}
