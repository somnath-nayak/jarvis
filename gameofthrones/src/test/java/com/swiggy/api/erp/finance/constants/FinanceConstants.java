package com.swiggy.api.erp.finance.constants;

public interface FinanceConstants {

    String hostName = "oms";
    String finDB = "financeDB";
    String createOrdersQueue = "swiggy.orders";
    String cancelOrdersQueue = "swiggy.order_cancel";
    String city_id = "15";
    String rest_id_prepaid_newmou = "212";
    //String restid = "restaurant_id";
    String rest_id_postpaid_newmou = "1234";

    String rest_id_prepaid_oldmou = "9990";
    String rest_id_postpaid_oldmou = "0001";

    String[] status = {"assigned","confirmed", "arrived", "pickedup", "reached", "delivered"};

    String responsible_id_swiggy = "1";
    String responsible_id_restaurant = "2";
    String responsible_id_customer = "3";

    String food_prepared_t = "true";
    String food_prepared_f = "false";


    String undelivered_schema = "/Data/SchemaSet/Json/Finance/undelivered";
    String cancel_schema = "/Data/Payloads/JSON/finance_cancel";
    String submit_schema = "/Data/SchemaSet/Json/Finance/submitrecon";
    String reconhealth = "/Data/SchemaSet/Json/Finance/reconhealth";
    String reconversion = "/Data/SchemaSet/Json/Finance/reconversion";
    String restcurrentpayout = "/Data/SchemaSet/Json/Finance/finance_currentpayout";
    String reconlogout = "/Data/SchemaSet/Json/Finance/finance_logout";
    String quickrecon = "/Data/SchemaSet/Json/Finance/finance_quickrecon";
    String autoassign = "/Data/SchemaSet/Json/Finance/finance_autoassignment";
    String addannexure = "/Data/SchemaSet/Json/Finance/finance_addanexure";
    String restpendingapproval = "/Data/SchemaSet/Json/Finance/finance_restaurantpendingapproval";
    String restidpendingapproval = "/Data/SchemaSet/Json/Finance/finance_restaurantidpendingapproval";
    String createreports = "/Data/SchemaSet/Json/Finance/finance_createreports";
    String deliveredstatus = "delivered";
    String[] food_prepared = {"false","true"};
    String autoreconciled = "AUTORECONCILED";
    String unreconciled = "UNRECONCILED";
    String restaurants[] = new String[]{"7","7","7"};
    String cancelled = "CANCELLED";
    String reconciled = "RECONCILED";
    String packing_charges = "3.0";
    Double packe_charges = 3.0;
    String packaging_reason = "4";
    String item_total = "4";
    String mouquery = "SELECT restaurant_id FROM finance.restaurants where mou_type=";
    String oldmou = "0000";
    String newmou = "0001";
    String finance_db = "finance";
    String restid = "restaurant_id";
    int statuscode = 200;
    String invoice_job_check = "select * from recon_invoice_jobs where to_date='";
    String endstring = "'";
    String from_time = "2017-01-01 00s:00:00";
    String from_time_formatted = "2017-01-01";
    String user = "test";
    String testemail = "test@test.com";
    String task_id = "165";
    String key = "key";
    String value = "value";
    String countmanualjob = "select count(*) from manual_invoice_jobs";
    String fileurl = "test/test";
    String pageno = "10";
    String pagesize = "0";
    String restaurantId = "270";
    String type = "12";
    String excludelist = "11";
    String cityid = "12";
    String oms_queue = "oms";
    String zone_exchange = "swiggy.zone_update";
    String zone_queue = "fin_zone_update";
    String transaction_auth = "Basic R0dZU1dJOjIwMTVTVyFHR1k=";
    String get_order_details_from_recon_raw_orders = "SELECT * FROM finance.recon_raw_orders where order_id=";
    String get_order_details_from_master = "SELECT * FROM finance.master where order_id=";
    String get_master_entities_from_master_tinyint = "SELECT cast(${*} as char) as ${*} FROM finance.master where order_id=\"${order_id}\"";
    String get_master_entities_from_master = "SELECT ${*} FROM finance.master where order_id=\"${order_id}\"";
    String get_order_details_from_recon_raw_order_details = "SELECT * FROM finance.recon_raw_order_detail where order_id=";
   // String commission_expression="select * from finance.commission_expression where order_id=";
    String get_details_from_recon_de_dm_entries = "SELECT * FROM finance.recon_de_dm_entries where order_id=";
    String get_auto_recon_status = "SELECT auto_recon_status FROM finance.recon_raw_orders where order_id=";
    String get_recon_reconciliation_status ="SELECT * FROM finance.recon_reconciliation_status WHERE order_id=";
    String get_recon_order_actuals= "SELECT * FROM finance.recon_order_actuals WHERE order_id=";
    String get_details_restaurant_current_payout_details = "SELECT * FROM finance.restaurant_current_payout_detail WHERE payout_id=";
    String get_details_restaurant_current_payout = "SELECT * FROM finance.restaurant_current_payouts WHERE task_id=";
    String get_details_recon_current_payout_jobs = "SELECT * FROM finance.recon_current_payout_jobs WHERE id=";
    String get_commission_rest_earnings = "SELECT commission, restaurant_earnings FROM finance.recon_order_actuals WHERE order_id=";
    String get_details_from_master = "SELECT * FROM finance.master WHERE order_id=";
    String get_details_from_orders = "SELECT * FROM finance.orders WHERE order_id=";
    String get_task_id_from_recon_invoice_jobs = "SELECT id FROM finance.recon_invoice_jobs ORDER BY id DESC LIMIT 1";
    String get_status_from_recon_invoice_jobs = "SELECT status FROM finance.recon_invoice_jobs WHERE id=";
    String get_details_from_subtask = "SELECT * FROM finance.subtask WHERE task_id =";
    String get_details_from_recon_invoices = "SELECT * FROM finance.recon_invoices WHERE task_id=";
    String get_id_from_recon_master_file_job = "SELECT * FROM finance.recon_master_file_jobs ORDER BY id DESC LIMIT 1";
    String recon_status_auto_reconciled = "AUTORECONCILED";
    String user_id = "automation@swiggy.in";
    int statusOne = 1;
    int statusZero = 0;
    String msg_job_created = "Job created successfully";
   // String insert_details_in_deduction_configurations = "INSERT INTO finance.deduction_configurations (restaurant_id, type, detail, go_live_date, expire_date, created_by, updated_by, created_at, updated_at, is_active) VALUES (${RESTID}, 2, '{\"gmv_threshold_percentage\":10.0,\"pre_pickedup_swiggy_share\":10.0,\"post_pickedup_swiggy_share\":90.0}', '2017-10-02 00:00:00', '2020-10-03 00:00:00', 'rahul.sharma@swiggy.in', NULL,  '2020-07-30 16:14:42', NULL, 1 )";
    String insert_details_in_deduction_configurations = "INSERT INTO finance.deduction_configurations (restaurant_id, type, detail, go_live_date, expire_date, created_by, updated_by, created_at, updated_at, is_active) VALUES (${RESTID}, 2, '{\"cancellation_policy\":\"POLICY_MFR\",\"fallback_cancellation_policy\":\"POLICY_OLD_MOU\",\"gmv_threshold_percentage\":10.0,\"pre_pickedup_swiggy_share\":10.0,\"post_pickedup_swiggy_share\":90.0}', '2017-10-02 00:00:00', '2020-10-03 00:00:00', 'rahul.sharma@swiggy.in', NULL,  '2020-07-30 16:14:42', NULL, 1 )";

    String update_restaurant_to_new_mou = "UPDATE finance.restaurants SET mou_type=1 WHERE restaurant_id=";
    String get_task_id_from_recon_current_payout_job = " SELECT * FROM finance.recon_current_payout_jobs WHERE from_date=\"${FROM_DATETIME}\" AND to_date=\"${TO_DATETIME}\"";
    String get_payout_id_from_restaurant_current_payouts = "SELECT id FROM finance.restaurant_current_payouts WHERE task_id=";
    String get_id_from_restaurant_current_payout_details = "SELECT id FROM finance.restaurant_current_payout_detail WHERE payout_id=";
    String msg_invoice_job_created ="Invoice job created successfully.";
    String msg_successfully_added_job = "Successfully added job";
    String file_path_csv = System.getProperty("user.dir") + "/Data/invoice.csv";
    String file_path_xlsx = System.getProperty("user.dir") + "/Data/annexure.xlsx";
    String get_details_from_recon_master_file_job = "SELECT * FROM recon_master_file_jobs WHERE id=";
    String placedOrdersQueue = "vendor.placing-fsm.state-transitions";
    String db = "finance_cash_db";
    String get_zone = "select * from zone";
    String get_area = "select * from area";
    String get_zone_details = "select * from zone where id=";
    String create = "insert";
    String update = "update";
    String get_de = "select * from delivery_boys limit 100;";
    String de_details = "select * from delivery_boys where id=";
    String multi_td_online_json="../Data/Payloads/JSON/fin2_CreateMultiTDOrder_Online.json";
    String Multi_td_cash_json="../Data/Payloads/JSON/fin2_CreateMultiTDOrder_Cash.json";
    String pop_order_online_json="../Data/Payloads/JSON/fin2_CreatePopOrder_Online.json";
    String pop_order_cash_json="../Data/Payloads/JSON/fin2_CreatePopOrder_Cash.json";
    String super_order_online_json="../Data/Payloads/JSON/fin2_CreatePopOrder_Online.json";
    String super_order_cash_json="../Data/Payloads/JSON/fin2_CreateSuperOrder_Cash.json";
    String online_order_json="../Data/Payloads/JSON/fin2_CreateOnlineOrder2.json";
    String placed_json="../Data/Payloads/JSON/fin2_placed.json";
    String pickedup_json="../Data/Payloads/JSON/fin2_pickedup.json";
    String delivered_json="../Data/Payloads/JSON/fin2_delivered.json";
    String rule1="prePickedUpCancelFoodPreparedTrueResponsible1";
    String rule2="prePickedUpCancelFoodPreparedTrueResponsible2";
    String rule3="prePickedUpCancelFoodPreparedTrueResponsible3";
    String rule4="postPickedUpCancelFoodPreparedTrueResponsible1";
    String rule5="postPickedUpCancelFoodPreparedTrueResponsible2";
    String rule6="postPickedUpCancelFoodPreparedTrueResponsible3";
    String rule7="prePickedUpCancelFoodPreparedFalseResponsible1";
    String rule8="prePickedUpCancelFoodPreparedFalseResponsible2";
    String rule9="prePickedUpCancelFoodPreparedFalseResponsible3";
    String rule10="postPickedUpCancelFoodPreparedFalseResponsible1";
    String rule11="postPickedUpCancelFoodPreparedFalseResponsible2";
    String rule12="postPickedUpCancelFoodPreparedFalseResponsible3";
    String rule13="notPlacedPrePickedUpCancelFoodPreparedFalseResponsible3";
    String rule14="notPlacedPostPickedUpCancelFoodPreparedFalseResponsible3";
    String rule15="mfrEnabled";
    String rule16="oldMoUFallback";
    String rule17="newMoUFallback";
    String updateMfrEnabledWithoutFallback= "UPDATE finance.seller_master SET cancellation_policy='POLICY_MFR' WHERE seller_master_id=";
    String updateMfrEnabledWithFallbackNewMou= "UPDATE finance.seller_master SET cancellation_policy='POLICY_MFR POLICY_NEW_MOU ' WHERE seller_master_id=";
    String updateMfrEnabledWithFallbackOldMou= "UPDATE finance.seller_master SET cancellation_policy='POLICY_MFR POLICY_OLD_MOU' WHERE seller_master_id=";






}
