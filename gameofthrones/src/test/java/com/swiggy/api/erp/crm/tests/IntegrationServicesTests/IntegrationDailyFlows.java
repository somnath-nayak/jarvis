package com.swiggy.api.erp.crm.tests.IntegrationServicesTests;

import com.swiggy.api.daily.preorder.helper.DailyPreorderHelper;
import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.CRMUpstreamValidation;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class IntegrationDailyFlows extends MockServiceDP {

    FlowMapper flowMapper = new FlowMapper();
    Initialize gameofthrones = new Initialize();
    DailyPreorderHelper dailyPreorderHelper = new DailyPreorderHelper();

    public void verifyFlows(String[] flow, String userId, String orderId, String conversationId){
        int lengthofflow = flow.length;
        String queryparam[] = new String[1];
        System.out.println("lengthofflow \t" + lengthofflow);

        for (int i = 0; i < lengthofflow - 1; i++) {

            String nodeId = flow[i];
            if (nodeId == "1" || nodeId == "2") {
                continue;
            } else {
                HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
                requestheaders_cancellationinbound.put("Content-Type", "application/json");
                System.out.println("length of flow" + lengthofflow);
                queryparam[0] = nodeId;
                String[] paylaodparam = new String[]{userId, orderId, conversationId};

                requestheaders_cancellationinbound.put("tenantId", CRMConstants.DAILY_TENANT_ID);
                GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "validChildrenNew", gameofthrones);

                Processor cancellationinbound_response = new Processor(cancellationinbound,
                        requestheaders_cancellationinbound, paylaodparam, queryparam);
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");
                String addOnGroupList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
                List<String> addOnlist = Arrays.asList(addOnGroupList.split(","));

                System.out.println("addonlist" + addOnlist);
                System.out.println("flowname[i+1]" + flow[i + 1]);

                assertTrue(addOnlist.contains(flow[i + 1]));
                int index = addOnlist.indexOf(flow[i + 1]);

                if (i < (lengthofflow - 2)) {
                    Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "false");
                } else {
                    Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "true");
                }
            }
        }
    }

    @Test(dataProvider = "dailyPreOrderWorkFlows", groups = {"regression"}, description = "Daily pre order flows")
    public void integrationPreOrderFlows(HashMap<String, String> flowDetails) throws IOException, InterruptedException {

        String orderType = flowDetails.get("orderType").toString();
        String ffStatus = flowDetails.get("ffStatus").toString();
        String deliveryStatus = flowDetails.get("deliveryStatus").toString();
        String flowName = flowDetails.get("flow").toString();
        String conversationId, deviceId;
        String[] flow = flowMapper.setFlow().get(flowName);
        String orderId = null;

        Processor createPreOrderResponse = dailyPreorderHelper.createPreorder();
        orderId = createPreOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.order_job_id");
        Assert.assertNotNull(orderId);
        conversationId = orderId;
        String userId = "5770026";
        Assert.assertNotNull(orderId);
        verifyFlows(flow, userId, orderId, conversationId);
    }

    @Test(dataProvider = "dailySubMealWorkFlows", groups = {"regression"}, description = "Daily pre order flows")
    public void integrationSubscriptionMealOrderFlows(HashMap<String, String> flowDetails) throws IOException, InterruptedException {

        String orderType = flowDetails.get("orderType").toString();
        String ffStatus = flowDetails.get("ffStatus").toString();
        String deliveryStatus = flowDetails.get("deliveryStatus").toString();
        String flowName = flowDetails.get("flow").toString();
        String conversationId, deviceId;
        String[] flow = flowMapper.setFlow().get(flowName);
        String orderId = null;

        Processor createPreOrderResponse = dailyPreorderHelper.createPreorder();
        orderId = createPreOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.order_job_id");
        Assert.assertNotNull(orderId);
        conversationId = orderId;
        String userId = "5770026";
        Assert.assertNotNull(orderId);
        verifyFlows(flow, userId, orderId, conversationId);
    }

}
