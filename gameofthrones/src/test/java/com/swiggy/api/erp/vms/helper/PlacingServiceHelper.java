package com.swiggy.api.erp.vms.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class PlacingServiceHelper extends RMSConstants{

	

	Initialize initializer = Initializer.getInitializer();
	

	public Processor createEvent(String eventType,String updatedBy){

		GameOfThronesService services=new GameOfThronesService("placingservice", "createEvent", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");	
		Processor processor= new Processor(services,requestheaders,new String[]{eventType,updatedBy});

		return processor;
	}

	public Processor getAllEvents(){

		GameOfThronesService services=new GameOfThronesService("placingservice", "getAllEvents", initializer);
		//HashMap<String, String> requestheaders = new HashMap<String, String>();
		//requestheaders.put("Content-Type", "application/json");	
		Processor processor= new Processor(services,null,null,null);

		return processor;
	}
	
	public Processor deleteEvent(int eventId){

		GameOfThronesService services=new GameOfThronesService("placingservice", "deleteEvent", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");	
		Processor processor= new Processor(services,requestheaders,null,new String[] { String.valueOf(eventId) });

		return processor;
	}
	
	
	public Processor createState(String stateType,String updatedBy){

		GameOfThronesService services=new GameOfThronesService("placingservice", "createState", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");	
		Processor processor= new Processor(services,requestheaders,new String[]{stateType,updatedBy});

		return processor;
	}

	public Processor getAllStates(){

		GameOfThronesService services=new GameOfThronesService("placingservice", "getAllStates", initializer);
		Processor processor= new Processor(services,null,null,null);

		return processor;
	}
	
	public Processor deleteState(int stateId){

		GameOfThronesService services=new GameOfThronesService("placingservice", "deleteState", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");	
		Processor processor= new Processor(services,requestheaders,null,new String[] { String.valueOf(stateId) });

		return processor;
	}
	
	public Processor addStateTransition(int fromStateId, int eventId, int toStateId, String updatedBy){

		GameOfThronesService services=new GameOfThronesService("placingservice", "addStateTransition", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");	
		Processor processor= new Processor(services,requestheaders,new String[] { String.valueOf(fromStateId), String.valueOf(eventId), String.valueOf(toStateId), updatedBy });

		return processor;
	}

	public Processor getAllStateTransition(){

		GameOfThronesService services=new GameOfThronesService("placingservice", "getAllStateTransition", initializer);
		Processor processor= new Processor(services,null,null,null);

		return processor;
	}

	public Processor makeTransition(String eventId, String eventType, Long eventTimestamp, long orderId, String updatedBy, String restaurantId){

		GameOfThronesService services=new GameOfThronesService("placingservice", "makeTransition", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");	
		Processor processor= new Processor(services,requestheaders,new String[] {eventId, eventType, String.valueOf(eventTimestamp), String.valueOf(orderId), updatedBy, restaurantId });

		return processor;
	}

	public String getEventId(String eventId) {
		Long a = Long.valueOf(eventId) + 1l ;
		return String.valueOf(a);
	}
	
	public Processor getPlacingState(long orderId){

		GameOfThronesService services=new GameOfThronesService("placingservice", "getPlacingState", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");	
		
		Processor processor= new Processor(services,requestheaders,null,new String[] { String.valueOf(orderId) });

		return processor;
	}
	
	
	public Processor deleteStateTransition(int trnasId){

		GameOfThronesService services=new GameOfThronesService("placingservice", "deleteStateTransition", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");	
		Processor processor= new Processor(services,requestheaders,null,new String[] { String.valueOf(trnasId) });

		return processor;
	}
	
	public Processor healthCheck(String status){
		GameOfThronesService services=new GameOfThronesService("placingservice", "getHealthCheck", initializer);
		Processor processor= new Processor(services,null,null,new String[]{status});
		return processor;
	}

	public void deleteEvents() {
		Processor processor = getAllEvents();
		String[] ids = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..id").toString().replace("[", "").replace("]", "").split(",");
		for(String id : ids) {
			if(!id.isEmpty() && ids.length > 0 )
				deleteEvent(Integer.parseInt(id));
		}
	}

	public void deleteStates() {
		Processor processor = getAllStates();
		String[] ids = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data..id").toString().replace("[", "").replace("]", "").split(",");
		for(String id : ids)
			if(ids.length > 0 && !id.isEmpty())
				deleteState(Integer.parseInt(id));
	}
}
