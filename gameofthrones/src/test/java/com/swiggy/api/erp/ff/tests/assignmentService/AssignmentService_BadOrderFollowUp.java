package com.swiggy.api.erp.ff.tests.assignmentService;


import com.swiggy.api.erp.ff.constants.AssignmentServiceConstants;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.AssignmentServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.ManualTaskServiceHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class AssignmentService_BadOrderFollowUp {

    OMSHelper omshelper = new OMSHelper();
    LOSHelper losHelper  = new LOSHelper();
    ManualTaskServiceHelper mtsh = new ManualTaskServiceHelper();
    AssignmentServiceHelper assignmentServiceHelper = new AssignmentServiceHelper();
    DBHelper dbHelper = new DBHelper();

    @BeforeMethod
    public void badOrderFollowUp()
    {
        omshelper.redisFlushAll(OMSConstants.REDIS);
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+ AssignmentServiceConstants.badOrderFollowup_id[0]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.badOrderFollowup_id[1]+";");

    }
   @Test(description="Create a Placing Delay bad order for 1 Bad Order followup OE")
   public void createPlacingDelayOrder_1OE() throws IOException, InterruptedException {
    Long orderId = Long.parseLong(losHelper.getAnOrder("manual"));
    String order = String.valueOf(orderId);
    createManualTask(order, AssignmentServiceConstants.reason_PlacingDelay,AssignmentServiceConstants.subreason_RESTAURANT_POTENTIALLY_CLOSED);

       //1st OE login
    assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.badOrderFollowup_id[0], AssignmentServiceConstants.badOrderFollowup_role_id);


    //Checking whether order is assigned to OE in mysql table
    Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId + " and oe_id = " +AssignmentServiceConstants.badOrderFollowup_id[0]+ ";","order_id",order,1,100),"Orderid is not found in DB");

}

    @Test(description="Create a Cancellation bad order for 1 Bad Order followup OE")
    public void createCancellationRequiredOrder_1OE() throws IOException, InterruptedException {
        Long orderId = Long.parseLong(losHelper.getAnOrder("manual"));
        String order = String.valueOf(orderId);
        createManualTask(order, AssignmentServiceConstants.reason_CANCELLATION_REQUIRED,AssignmentServiceConstants.subreason_RESTAURANT_CLOSED);


        //1st OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.badOrderFollowup_id[0], AssignmentServiceConstants.badOrderFollowup_role_id);

        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId + " and oe_id = " +AssignmentServiceConstants.badOrderFollowup_id[0]+ ";","order_id",order,1,100),"Orderid is not found in DB");

    }


    @Test(description="Create 2 bad orders for 1 Bad Order followup OE")
    public void create2Orders_1OE() throws IOException, InterruptedException {
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String order1 = String.valueOf(orderId1);
        Long orderId2 = Long.parseLong(losHelper.getAnOrder("manual"));
        String order2 = String.valueOf(orderId2);

        createManualTask(order1, AssignmentServiceConstants.reason_CANCELLATION_REQUIRED,AssignmentServiceConstants.subreason_RESTAURANT_CLOSED);

        createManualTask(order2, AssignmentServiceConstants.reason_CANCELLATION_REQUIRED,AssignmentServiceConstants.subreason_RESTAURANT_CLOSED);

        //1st OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.badOrderFollowup_id[0], AssignmentServiceConstants.badOrderFollowup_role_id);

        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +AssignmentServiceConstants.badOrderFollowup_id[0]+ ";","order_id",order1,1,100),"Orderid is not found in DB");

    }


    @Test(description="Create a bad order for 1 Bad Order followup OE but by batch assignment")
    public void OELoggedIn_Created1Order_BatchJob() throws IOException, InterruptedException {

        //1st OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.badOrderFollowup_id[0], AssignmentServiceConstants.badOrderFollowup_role_id);

        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String order1 = String.valueOf(orderId1);

        createManualTask(order1, AssignmentServiceConstants.reason_CANCELLATION_REQUIRED,AssignmentServiceConstants.subreason_RESTAURANT_CLOSED);

        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +AssignmentServiceConstants.badOrderFollowup_id[0]+ ";","order_id",order1,1,100),"Orderid is not found in DB");

    }


    @Test(description="Create 2 bad orders for 1 Bad Order followup OE by Batch assignment")
    public void OELoggedIn_Created2Order_BatchJob() throws IOException, InterruptedException {
        //1st OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.badOrderFollowup_id[0], AssignmentServiceConstants.badOrderFollowup_role_id);
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String order1 = String.valueOf(orderId1);
        Long orderId2 = Long.parseLong(losHelper.getAnOrder("manual"));
        String order2 = String.valueOf(orderId2);

        createManualTask(order1, AssignmentServiceConstants.reason_CANCELLATION_REQUIRED,AssignmentServiceConstants.subreason_RESTAURANT_CLOSED);
        createManualTask(order2, AssignmentServiceConstants.reason_CANCELLATION_REQUIRED,AssignmentServiceConstants.subreason_RESTAURANT_CLOSED);

        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +AssignmentServiceConstants.badOrderFollowup_id[0]+ ";","order_id",order1,5,100),"Orderid is not found in DB");

    }


    @Test(description="Create 2 bad orders for 2 Bad Order followup OE Batch Assignment")
    public void OE2LoggedIn_Created2Order_BatchJob() throws IOException, InterruptedException {
        //1st OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.badOrderFollowup_id[0], AssignmentServiceConstants.badOrderFollowup_role_id);
        Thread.sleep(3000);
        //2nd OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.badOrderFollowup_id[1], AssignmentServiceConstants.badOrderFollowup_role_id);
        Thread.sleep(3000);


        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String order1 = String.valueOf(orderId1);
        Long orderId2 = Long.parseLong(losHelper.getAnOrder("manual"));
        String order2 = String.valueOf(orderId2);

        createManualTask(order1, AssignmentServiceConstants.reason_CANCELLATION_REQUIRED,AssignmentServiceConstants.subreason_RESTAURANT_CLOSED);
        Thread.sleep(2000);
        createManualTask(order2, AssignmentServiceConstants.reason_CANCELLATION_REQUIRED,AssignmentServiceConstants.subreason_RESTAURANT_CLOSED);

        System.out.println("Order1====="+order1);
        System.out.println("Order2====="+order2);
        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +AssignmentServiceConstants.badOrderFollowup_id[0]+ ";","order_id",order1,1,100),"Orderid is not found in DB");
       //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId2 + " and oe_id = " +AssignmentServiceConstants.badOrderFollowup_id[1]+ ";","order_id",order2,1,100),"Orderid is not found in DB");

    }

    @Test(description="Create a bad order for 1 Bad Order followup OE and Logout scenario")
    public void Created1Order_OELoggedOut() throws IOException, InterruptedException {

        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String order1 = String.valueOf(orderId1);
        //created a manual task
        createManualTask(order1, AssignmentServiceConstants.reason_CANCELLATION_REQUIRED,AssignmentServiceConstants.subreason_RESTAURANT_CLOSED);

        //1st OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.badOrderFollowup_id[0], AssignmentServiceConstants.badOrderFollowup_role_id);
        Thread.sleep(3000);

        //OE1 logout
        assignmentServiceHelper.oeLogoutRMQ(AssignmentServiceConstants.badOrderFollowup_id[0],AssignmentServiceConstants.badOrderFollowup_role_id, AssignmentServiceConstants.Bad_logout_new_state_id);

        Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId1,AssignmentServiceConstants.badOrder),orderId1+"==== is not present in OrdersInQueueTable");

        //1st OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.badOrderFollowup_id[1], AssignmentServiceConstants.badOrderFollowup_role_id);


        System.out.println("Order1====="+order1);

        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +AssignmentServiceConstants.badOrderFollowup_id[1]+ ";","order_id",order1,1,100),"Orderid is not found in DB");

    }


    public void createManualTask(String orderId,String reason,String subreason)
    {
       Processor processor = mtsh.createTask(orderId,reason,subreason,"test");
       Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),AssignmentServiceConstants.expectedCode);
     }


}
