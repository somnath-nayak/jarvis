package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Addon_groups_vo {
	private Addon_group addon_group;

    private String[] delete_addon_by_ids;

    private Addons_vo[] addons_vo;

    private String delete_addon_by_id_error_map;

    public Addon_group getAddon_group ()
    {
        return addon_group;
    }

    public void setAddon_group (Addon_group addon_group)
    {
        this.addon_group = addon_group;
    }

    public String[] getDelete_addon_by_ids ()
    {
        return delete_addon_by_ids;
    }

    public void setDelete_addon_by_ids (String[] delete_addon_by_ids)
    {
        this.delete_addon_by_ids = delete_addon_by_ids;
    }

    public Addons_vo[] getAddons_vo ()
    {
        return addons_vo;
    }

    public void setAddons_vo (Addons_vo[] addons_vo)
    {
        this.addons_vo = addons_vo;
    }

    public String getDelete_addon_by_id_error_map ()
    {
        return delete_addon_by_id_error_map;
    }

    public void setDelete_addon_by_id_error_map (String delete_addon_by_id_error_map)
    {
        this.delete_addon_by_id_error_map = delete_addon_by_id_error_map;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [addon_group = "+addon_group+", delete_addon_by_ids = "+delete_addon_by_ids+", addons_vo = "+addons_vo+", delete_addon_by_id_error_map = "+delete_addon_by_id_error_map+"]";
    }
}
			
			