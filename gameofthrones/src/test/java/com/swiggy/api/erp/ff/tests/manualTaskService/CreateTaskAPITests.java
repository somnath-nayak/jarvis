package com.swiggy.api.erp.ff.tests.manualTaskService;

import com.swiggy.api.erp.ff.dp.manualTaskService.CreateTaskAPIData;
import com.swiggy.api.erp.ff.helper.ManualTaskServiceHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;

public class CreateTaskAPITests extends CreateTaskAPIData {

    ManualTaskServiceHelper helper = new ManualTaskServiceHelper();
    OMSHelper omsHelper = new OMSHelper();


    @Test(priority = 1, dataProvider = "createTaskData", groups = {"sanity", "regression"},description = "End to end tests for new user verification")
    public void createTaskAPITests(String orderId, String reason, int reasonId, String reasonOE, String subReason, int subReasonid, String subReasonOE, String source, HashMap<String, String> expectedResponseMap) throws InterruptedException {
        System.out.println("***************************************** createTaskAPITests started *****************************************");

        Processor response = helper.createTask(orderId, reason, subReason, source);


        helper.assertCreateTaskApiResponses(response, expectedResponseMap);
        Thread.sleep(1000);
        if (response.ResponseValidator.GetResponseCode() == 200){
            String taskId = response.ResponseValidator.GetNodeValue("data.task_id");
            helper.assertDataAfterTaskCreation(orderId, source, taskId, reasonId, subReasonid, reasonOE, subReasonOE);
        }

        System.out.println("######################################### createTaskAPITests compleated #########################################");
    }
}
