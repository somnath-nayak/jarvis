package com.swiggy.api.erp.vms.constants;

import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class RMSConstants {

	public static final String TEST_ORDER_COMMENTS = "RamziEndToEndQATestOrder";
	public static final String PAYMENTMODE_CASH = "Cash";
//	public static final String consumerAppPassword = "swiggy";
	public static final String consumerAppPassword = "123456";
//	public static final long consumerAppMobile = 7899772315l;
	public static final long consumerAppMobile = 8618869769l;
	
	public static final String OMS_UAT_PASSWORD = "Swiggy@1234";
	public static final String OMS_UAT_USERNAME = "murthy@swiggy.in";
	public static final String VENDOR_UAT_PASSWORD = "El3ment@ry";
	public static final String VENDOR_UAT_USERNAME = "9738948943";
	public static String auth = "";





	public enum PrepType {
		GLOBAL,
		SLOTWISE,
        TEST
	}

    static RMSHelper rmsh = new RMSHelper();

	public static String password="imran@1234";
	//public static String username="9036285925";
	//public static String username="5271";
	//public static String username="9990";

//	public static String username="9742471472";
	public static String username="9990";
	public static int successresponseCode=200;
	public static int successstatusCode=0;
	public static int failedstatusCode=-1;
	public static int failedstatusCode_alreadyExist=-2;
	public static int failedstatusCode_invalidSession=-3;
	public static String data=null; 
//	public static String restaurantId= "9990";
	public static String restaurantId;
	//public static String restaurantId= "213";
	public static String restaurantId1= "5688";	
	public static String orderPollMessage= "Success";
	public static String token=null;
	
	//1.Login	
	public static String username_VI="9742471472";
	public static String invalidusername="9738948944";
	public static String password1="imran@1234";
	public static String userName="9742471472";
	
	
	/*public static String username="5555588888";
	public static String invalidusername="55555288888";
	public static String password="123456";
	public static String loginValidMessage="Login Successful";
	public static String loginInValidMessage="Login failed";*/
	
	//2.POST- get restaurant
	public static String rest_ids="30751";
//	public static String group_by="daily";
	public static String invalidrest_ids="2";
	public static String getrestaurantValidMessage="Fetched Insights restaurants Successfully";
	
	public static String getrestaurantInValidMessage="Failed to fetch Restaurants";
	
//3.POST - OrderForRest- Daily	
	public static String user_id ="68174";
	public static String rest_id="9990";
	public static String invalidrest_id="2";
	public static String start_date=startDate(-100);
	public static String end_date=startDate(-1);
	public static String group_by="daily";
	public static String group_by_week="week";
	public static String orderForRestValidMessage="Fetched Insights ordersForRestaurant Successfully";
	public static String inValidSessionMessage="Invalid Session";
	public static String validMessage="successful";
	public static String savepreptime = "/Data/SchemaSet/Json/VMS/savePreptime.txt";
	public static String saveprepmulti= "/Data/SchemaSet/Json/VMS/savePreptimeMulti.txt";
	public static String updatepreptime = "/Data/SchemaSet/Json/VMS/updatepreptime";
		   
 
	
	public static String loginValidMessage="Login Successful";
	public static String loginInValidMessage="Login failed";
	
	public static String logoutSuccessMessage="Logout Successful";

	public static String portalUser="7899772315";
	public static String portalPassword="swiggy";
	public static String itemId = "564238";
	public static String quantity = "2";
	public static String preptimeMessage="SUCCESS";
//	public static String prepTimeRestaurantId="5271";

	public static String prepTimeRestaurantId="9990";
	public static String prep_time = "15";

	public static String slotId[] = null;//rmsh.fetchSlotWiseId(prepTimeRestaurantId);

	public static String dayId[] = null;//rmsh.fetchDayWiseId();
	public static String errorstatus = "500";
	public static String errorstatus2 = "404";
	public static String preperrormessage = "FAILURE";
	public static String invalidprepid = "99999999";
	
	//Food Issues
	
	//foodIssuesGetConfig_DP
	public static String FIGC_restaurantId ="9990";
	public static String FFIGC_statusMessage ="fetched config successfully";
	
	
	// FoodIssuesVerifyBill_DP API
//	public static String FIVB_orderId ="7741123400";
//	public static String FIVB_inputValue ="45";
	public static String FIVB_inputType ="BILL";
	public static String FIVB_billProvided ="true";
	public static String FIVB_statusMessage ="verified successfully";
	
	//FoodIssuesSaveFinalBill_DP API
	public static String FISFB_issueId ="115";
	public static String FISFB_orderId ="7741098889";
	public static String FISFB_finalBillValue ="85";
	public static String FISFB_statusMessage ="invalid input params";
	
	//FoodIssuesRectifybill_DP API
	
	public static String FIRB_issueId ="115";
	public static String FIRB_resolution ="WRONG_BILL";
	public static String FIRB_FI_statusMessage ="invalid input params";
	
	//restaurant_Tier_mertics_of_Restaurant
	
	public static String RTMR_restaurantId ="9990"; 
	public static String RTMR_statusMessage ="success";
	
	//restaurant_Tier_Benifits_For_All_Tiers_DP API
	
	public static String RTBFA_cityId ="3";
	public static String RTBFA_restId ="9990";
	public static String RTBFA_statusMessage ="success";
	
	//Self Serve Menu GET Menu API
	public static String SSMM_statusMessage ="Successfully fetched items";
	
	//Self Serve Menu ADD NEW ITEM API
	public static String SSMANI_statusMessage = "Item added Successfully";
	public static String SSMANI_name ="test automation";
	public static String SSMANI_is_veg ="1";
	public static String SSMANI_packing_charges ="1";
	public static String SSMANI_description ="";
	public static String SSMANI_item_change_request ="";
	public static String SSMANI_addons_and_variant_change_request ="";

	public static String SSMANI_markInStock ="1";

	public static String markInStock ="1";

	public static String SSMANI_s3_image_url ="";
	public static String SSMANI_price ="10";

	
	
	// CALCUALTE SERVICE API
	
	public static String SSMCS_statusMessage = "Calculate Service Successfully";
	public static String SSMCS_packagingCharges ="10";
	public static String SSMCS_price ="10";

	// self_Serve_Menu_Revison_History API

	public static String SSMIT_statusMessage ="Item Ticket Fetch Successfully";
	
	
	//self_Serve_Menu_ItemTicket API
	
	public static String SSMRH_statusMessage = "Revision Search Successfully";

	//fetch_Broadcast_Info_DP
	
	public static String FBI_statusMessage = "Fetched Broadcast Successfully";
	public static String FBI_restaurantId ="9990";
	
	//DS callout API
	
	public static String DS_statusMessage ="Successfully created ds callout";
	public static String DS_restaurantId ="9990";
	public static String DS_orderId ="15396847";
	
	
	//self_Serve_Menu Edit item API

	public static String SSMEI_description ="Chicken Biryani + Chicken Starter + Kheers";
	public static String SSMEI_seller_tier ="Untiered";
	public static String SSMEI_statusMessage="Item edited Successfully";
	
	// self_Serve_Menu_DeletE_item API
	
	public static String SSMDI_statusMessage ="Item Deleted Successfully";

	//public static String itemSubCatId ="254880";
	
	//self_Serve_Menu_Cancel_Ticket_DP
	
	public static String SSMCT_statusMessage ="Cancel ticket Successfully";

	// self_Serve_Menu_Submit_Pending_Tickets_DP
	
	public static String SSMSPT_statusMessage ="Pending Tickets Submission Successfully";
	
	//self_Serve_Menu_delete_Cart_Image_DP API
	
	public static String SSMDCI_statusMessage ="Catlogue Image Deleted Successfully";

	//itemOOS
	public static int markOOStock = 0;
	public static String itemIdOos ="5897902";
	public static String hours = "2";
	public static String itemOOSMessage ="Done Successfully";
	public static String itemInstockMessage ="Done Successfully";
	
	public static String variantItemIdOos ="16046343";
	public static String variantId ="5253090";
	public static String variantGroupId ="1514902";
	public static String variantOOSMessage ="Done Successfully";
	public static String variantInStockSMessage ="Done Successfully";
	
	
	
//    public static String end_date=DateUtils.addDays(new Date(), -16).toInstant().toString();
//	public static String end_date= DateUtils.addMonths(new Date(), -16).toInstant().toString();

	//4.HelpContext
	public static String helpContextValidMessage="Fetched Insights helpContext Successfully";
	
	
	//5.Order Feedback
	public static String order_id = "14472081396";
	public static String orderFeedbackValidMessage="Fetched Insights orderFeedback Successfully";
	public static String orderFeedbackInValidMessage="Invalid Session";
	
	//6.Last updatedTime
	public static String lastUpdatedTimeValidMessage="Fetched Insights lastUpdatedTime Successfully";
	public static String lastUpdatedTimeInValidMessage="Invalid Session";
	
	//7.Confirmation Time - PeakTime
	public static String group_by1="peak-wise";
	public static String confirmationTimeValidMessage="Fetched Insights confirmationTime Successfully";
	
	//8. POST- OrderForRest - Weekly
	public static String rest_id1="30751";
	public static String rest_id2="220";
//	public static String rest_id2= rest_id1.concat(rest_id);	
	public static String start_date_weekly=startDate(-30);
	public static String end_date_weekly=startDate(-15);
	public static String group_by_weekly="weekly";
	
	//9.POST - OrderForRest - Monthly
	  public static String group_by_mothly="monthly";
	  public static String start_date_monthly=startDate(-30);
		public static String end_date_monthly=startDate(-15);
	  
	//10. POST - ops-metrics-for -rest ----> 
	public static String metricsForRestValidMessage="Fetched Insights opsMetricsForRestaurant Successfully";
	
	//11.POST - Rating
	public static String ratingsForRestValidMessage= "Fetched Insights ratingsv2 Successfully";
		
	//12. GET -OrdersForRatings
	 public static String in_ratings="1";
	 public static String in_ratings1="5";
	 public static  String orderForRatingsValidMessage="Fetched Insights ordersForRatings Successfully";
	 
	 //13.POST- OrdersForItems
	 public static String ordersforitemsValidMessage="Fetched Insights ordersForItems Successfully";
	 public static String days[] = {"MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY","SUNDAY"};
     public static String restaurantIds[] = new String[2];
	 //14.POST - OrdersForCategories
	 public static String ordersforcategoriesValidMessage="Fetched Insights ordersForCategories Successfully";
	 
	 
	 //15.POST - Cancellation Reasons
	 public static String cancellationReasonsValidMessage="Fetched Insights cancellationReasons Successfully";
	 
	 //16.POST - OOSOrdersAndReasons
	 public static String oosOrdersAndReasonsReasonsValidMessage="Fetched Insights OOSOrdersAndReasons Successfully";
	 
	 //17. GET - Registration
	 public static String mobileno="9738948943";
	 public static String invalidmobileno1="9638948943";
	 public static String invalidmobileno2="97389489";
	 public static String registrationValidMessage="...........";
	 public static String registrationInValidMessage="Invalid Mobile Number";
	 public static String registrationAlreadyRegisteredMessage="User already registered with this mobile number";
	 public static String userIdRegistered = "9990";
	 public static String userIdNotRegistered = "2147483647";
	 public static String registrationSuccessMessage = "success";
	 public static String registrationInvalidInput = "Invalid Input";
	 public static String prep_auth = "Basic Y2hlY2s6Y2hlY2s=";
	 

	 
	 //18. Restaurant Info
	 public static String restUsersInfoMessage="User Details Fetched";
	 public static String userInfoMessage="User Info Fetched Successfully";
	 public static String restInfoMessage = "Restaurant Details Fetched";
	 public static int userRole_id = 1;
	 public static int userRole_id2 = 2;
	 public static int userRole_id3 = 3;
	 public static String userPhone_no = "0000008899";
	 public static String newUser = "TestUser";
	 public static String userEmail = "null" ;
	 public static String newUserNull = "null";
	 public static String userRest_id = "4993" ;
	 public static String invalid_userRest_id = "0000" ;
	 public static String newPassword = "Swiggy@123";
	 public static String confirmPassword = "Swiggy@123";
	 
	 public static String is_enable = "true" ;
	 public static String userRest_ids = "11691" ;
	 public static String is_disable = "false" ;
//	 public static String is_enable = "true" ;

	 public static String invalid_session = "c42ae6d4" ;
	 public static String role_name = "Store Manager" ;
	 public static String role_name1 = "Order Manager" ;
	 public static int updatedRole_id = 2 ;
	 public static int updatedRole_id1 = 3 ;
	 public static String updatedName = "Updated Name" ;
	 public static String updatedName1 = "Updated Name1" ;
	 public static String updatedEmail = "testemail1@email.com" ;
	 public static String updatedEmail1 = "testemail2@email.com" ;
	 public static String registration_complete = "false" ;
	 public static String isActive = "false" ;
	 public static String createUserMessage = "User Details Inserted";
	 public static String updateUserMessage = "User Details Updated" ;
	 public static String disableUserMessage = "User disabled";
	 public static String enableUserMessage = "User enabled";
	 public static String createTDMessage = "Successfully created Restaurant Level discount";
	 public static String editTDMessage = "Successfully Edited Restaurant Level discount";
	 public static String getDiscountbyRestMessage ="Fetched Campaign Successfully";
	 public static String getMetatMessage = "Successfully Fetched Metadata";
	 public static String getDiscountDetailsMessage = "Successfully Fetched Discount Details";
	 public static String signUpMessage = "Sign up successful";
	 
	 
	 public static String fetchItemsMessage = "Successfully fetched items";

	 //19. GET - Contact Reasons
	// public static String cookie = "Swiggy_Session-alpha=bda03a35-02e0-438a-bcf0-eb5c00e87e81";
	 public static String validMessage_contactReasons = "null";

	 //19. POST - Contact Us Ticket
	 public static int restaurantId_contactusticket= 223;

	 public static String subject="test";
	 public static String message= "test";
	 public static String category="DE Related Issue";
	 public static String category_Invalid="test";
	 public static String category_Empty=" ";
	 public static String city= "Bangalore";
	 public static String validMessage_contactUsTicket="Freshdesk Ticket Created Successfully";
	 public static String inValidMessage_contactUsTicket="Freshdesk Ticket Creation Failed";
	 
	 //21. GET - Find partner type
	 public static String partnertype_statusMessage="Decision Engine Successful";
	 public static String partnertype_comment="Decision Engine: No issues found";
	 public static String rest_id_finpartnertype="223";
	 public static String order_id_finpartnertype="7740763942";
	 public static String partnertype_invalidMessage="Invalid Input";
	 public static String invalid_order_id_finpartnertype="23";
	 public static String invalid_comment="Decision Engine: Partner info unavailable";
	 
	 // GET HealthCheck
	 
	 //Post - Restaurant OnOff
	 public static boolean isRestaurantOnOff_valid=true;
	 public static boolean isRestaurantOnOff_inValid=false;
	// public static int rest_id11=223;
	 public static int rest_id11=9990;
	 
	 
	 public static String rest_id12= "9990";
	 public static String un_restOn="9742471472";

	 
	 //POST - createholidat slot
	 public static String restaurantId_Invalid= "112";
	// public static int restaurantId_Invalid1= 112;
	 public static int restaurantId_Invalid1= 112;

	 public static String fromTime= startDate(1)+ " "+ "16:51:26";
	 public static String toTime= endDate(2)+" " +"16:55:26";
	 public static String toTime1= endDate(2);
	 public static String createholidatslot_inValidMessage="Action Failed";
	 public static String isRequestedToOpen = "true";
	 public static String isRequestedToClose = "false";
	public static String fromDate= startDate(-7)+"%2010:51:26";
	public static String toDate= endDate(0)+"%2018:55:26";
	 
	 //public static String fromTime ="2018-02-14 16:31:26";
	 //public static String toTime="2018-02-14 16:45:26";
	 public static String createholidayslotValidMessage="Done Successfully";
	 public static String createholidayslotInValidMessage="Invalid Session";
	 
	 //omeETE - deleteholiday slot
	 public static String slotId1="428073";
	 
	 //POST - Fetch Order	 
//	 public static String username_login = "5271";

	 public static String username_login = "5271";
	 public static String password_login = "$w199y@zolb";
	 public static String restaurantTimeMap ="lastUpdatedTime\":\"2017-02-07T08:15:29";
	 //public static String restaurantId= "5271";
	 
    //Get Bussiness Matrix Overview
	 public static String validMessage_overview="Fetched Insights Overview Successfully";
	 public static String rest_id_overview="9990";
	 public static int rest_id_overview1=9990;
	 //GET Bussiness Matrix Revenue
	 public static String validMessage_revenue="Fetched Revenue Insights Successfully";
	 public static String startdate=startDate(-8);
	 public static String enddate=endDate(-1);
	 public static String startdate1=startDate(-2);
	 public static String enddate1=endDate(-1);
	 public static String errorMessage="Unexpected error:<type 'exceptions.KeyError'>";
	 
	 //GET Bussiness Matrix Rating
	 public static String validMessage_ratings="Fetched Ratings Insights Successfully";
	 public static String invalidMesage_ratings="Something Went Wrong";
	 
	 
	 
	 //GET Bussiness Matrix Operations
	 public static String validMessage_operations="Fetched Operations Insights Successfully";

	//GET Mfr Daily Report
	public static String validMessage_mfrReport="success";
	public static String invalidMessage_mfrReport="error processing request";
	public static String invalidRest1= "error processing request";
	public static String invalidRest2= "There was some error while processing this request.";
	public static String invalidRest3 = "session key/restaurants are invalid";
	public static String validMessage_rmsMfrReport="Mfr metrics fetched successfully";
	public static String invalidRestMessage_mfrReport="don't have permission for requested restaurants";
	public static String invalidTimeMessage_mfrReport="Error fetching mfr report metrics";



	//Placing Service
	 
	 public static String updatedBy = "ravi.brahmam@swiggy.in";
	 public static String addeventMessage = "Event added successfully";
	 public static String geteventMessage = "Fetched all events";
	 public static String deleteeventMessage = "Event deleted successfully";
	 public static String addstateMessage = "State added successfully";
	 public static String getstatetMessage = "Fetched all states";
	 public static String deletestateMessage = "State deleted successfully";
	 public static String addstateTransitionMessage = "State Transition added successfully";
	 public static String getstatetTransitionMessage = "State Transition Map fetched successfully";
	 public static String makeTramsitionMessage = "Placing Event captured successfully";
	 public static String getPlacingStateMessage = "Placing State fetched successfully";
	 public static String updatedService = "VVO_PLACER";
	 public static Long eventTime= System.currentTimeMillis();
	 public static long order = 1234567899;
	 public static int event = 20;
	 public static String deleteTransition = "State Transition Map deleted successfully";
	 public static String healthCheckMessage = "UP";
	 
	 
	public static String startDate(int dif){
		Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, dif);
	    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(cal.getTimeInMillis())); 		
	    System.out.println("startDateeeeee :"+startDate);
	    //start_date=startDate;
	    return startDate;
	}
	
	
	public static String endDate(int dif){
		Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, dif);
	    String endDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(cal.getTimeInMillis()));
	    System.out.println("endDateeeeee :"+endDate);
	    //end_date=endDate;
	    return endDate;
	}
	
	 public static HashMap getLoginAPIResponseConstants(){
	    	
			HashMap<String, String> keyvalueresp = new HashMap<String, String>();
			
			keyvalueresp.put("rest_id", "223");
			keyvalueresp.put("rest_name","Truffles Ice & Spice");
			keyvalueresp.put("area_id","1");
			keyvalueresp.put("area_name","Koramangala");
			keyvalueresp.put("city_id","1");
			keyvalueresp.put("city_name","Bangalore");
			keyvalueresp.put("assured", "false");
			keyvalueresp.put("rating","4.06");
			keyvalueresp.put("enabled","true");
			keyvalueresp.put("mobile","9738948943");
			keyvalueresp.put("rid","223");
			keyvalueresp.put( "name", "Truffles Ice & Spice");
			keyvalueresp.put("city","Bangalore");
		    	
			return keyvalueresp;
	    }

    public static String invalid_prep_message = "Prep time entry does not exists";
	public static String method_not_allowed = "405";
	public static String auth_error = "401";
	public static String invalid_auth = "test";
	public static String invalid_preptime = "112312313123";
	public static String bad_request = "400";
	public static String invalid_day = "monday";
	public static String[] event_types = {"ROUTED","WITH_RELAYER","RELAYED","PLACED","FOOD_PREPARED","CALL_PARTNER","OUT_OF_STOCK"};
	public static String delete_user_pass = "delete from user_password where user_id=";
	public static String delete_user = "delete from users where id=";
	public static String delete_user_map = "delete from user_restaurant_map where user_id=";
	public static String rms_db = "rms_db";
	public static String user_by_name = "select id from users where name like '%";
	public static String order_by_id = "select with_partner_timestamp from order_info where order_id=";
	public static String userMap_by_id = "select is_primary from user_restaurant_map where user_id=";
	public static String setRegisTrue = "update users set REGISTRATION_COMPLETE=1 where id=";
	public static String firstAcceptance = "select first_acceptance_timestamp,first_acceptance_event from order_info where order_id=";
	public static String disp = "select order_id from rest_callbacks where disposition!='DEFAULT_OLD_CALLBACK_REQUEST' limit 1;";
	public static String end_like = "%'";
	public static String[] transition_types = {"ROUTED","WITH_RELAYER","RELAYED","PLACED","FOOD_PREPARED","CALL_PARTNER"};

	public static String vname = "\"Vname\"";
	public static String Uname = "\"Uname\"";
	public static String vnamelong = "\"namenamenamenamenamenamenamenamenamenamenamenamename\"";
	public static String vymoId = "\"VId\"";
	public static String UvymoId = "\"UId\"";
	public static String vymoIdlong = "\"VIdVIdVIdVIdVIdVIdVId\"";
	public static String cancelledCheque = "\"Vtest\"";
	public static String UcancelledCheque = "\"Utest\"";
	public static String compId = "\"Vtest\"";
	public static String UcompId = "\"Utest\"";
	public static String compIdlong = "\"VtestVtestVtestVtestV\"";
	public static String varea = "\"1\"";
	public static String Uarea = "\"2\"";
	public static String varealong = "\"11111111111\"";
	public static String vcity = "\"1\"";
	public static String Ucity = "\"2\"";
	public static String vcitylong = "\"11111111111\"";
	public static String agreementType = "\"post_paid\"";
	public static String vupdatedBy = "\"test\"";
	public static String UupdatedBy = "\"Utest\"";
	public static String locality = "\"VLocality\"";
	public static String Ulocality = "\"ULocality\"";
	public static String localitylong = "\"VLocalityyVLocalityyVLocalityyVLocalityyVLocalityyVLocalityyVLocalityyVLocalityyVLocalityyVLocalityyV\"";
	public static String bankCity = "\"VCity\"";
	public static String UbankCity = "\"UCity\"";
	public static String bankCitylong = "\"VCityVCityVCityVCityVCityVCityVCityVCityVCityVCityV\"";
	public static String ownerEmail = "\"test@gmail.com\"";
	public static String UownerEmail = "\"Utest@gmail.com\"";
	public static String ownerEmaillong = "\"test1test1test1test1test1test1test1test1t@gmail.com\"";
	public static String ownerMobile = "\"0099009999\"";
	public static String UownerMobile = "\"1099009999\"";
	public static String ownerMobilelong = "\"00990099999\"";
	public static String vaddress = "\"Vaddress\"";
	public static String Uaddress = "\"Uaddress\"";
	public static String vaddresslong = "\"VaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddressssVaddresssss\"";
	public static String mou = "\"Vtest\"";
	public static String Umou = "\"Utest\"";
	public static String commissionExp = "\"Vtest\"";
	public static String UcommissionExp = "\"Utest\"";
	public static String payBySystemValue = "\"0\"";
	public static String UpayBySystemValue = "\"1\"";
	public static String gstState = "\"Vtest\"";
	public static String UgstState = "\"Utest\"";
	public static String gstStatelong = "\"Vtesttttt\"";
	public static String invoicingName = "\"Vname\"";
	public static String UinvoicingName = "\"Uname\"";
	public static String invoicingNamelong = "\"VnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameVnameV\"";
	public static String bankName = "\"Vname\"";
	public static String UbankName = "\"Uname\"";
	public static String bankNamelong = "\"VnameVnameVnameVnameVnameVnameVnameVnameVnameVnameV\"";
	public static String bankIfscCode = "\"Vcode12\"";
	public static String UbankIfscCode = "\"Ucode12\"";
	public static String bankIfscCodelong = "\"Vcode12345Vcode12345Vcode12345V\"";
	public static String bankAccountNo = "\"1030897708\"";
	public static String UbankAccountNo = "\"2030897708\"";
	public static String bankAccountNolong = "\"1030897708103089770810308977081\"";
	public static String tanNo = "\"1371987hg\"";
	public static String UtanNo = "\"2371987hg\"";
	public static String tanNolong = "\"1371987hg61371987hg61371987hg61\"";
	public static String panNo = "\"17397FGGKH\"";
	public static String UpanNo = "\"27397FGGKH\"";
	public static String panNolong = "\"17397FGGKH17397FGGKH17397FGGKH1\"";
	public static String benificiaryName = "\"Vtest\"";
	public static String UbenificiaryName = "\"Utest\"";
	public static String benificiaryNamelong = "\"VtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestV\"";
	public static String invoicingEmail = "\"Vtest\"";
	public static String UinvoicingEmail = "\"Utest\"";
	public static String invoicingEmaillong = "\"VtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestVtestV\"";
	public static String ownerName = "\"Vtest\"";
	public static String UownerName = "\"Utest\"";
	public static String ownerNamelong = "\"VtestVtestVtestVtestVtestVtestVtestVtestVtestVtestV\"";
	public static String smEmail = "\"Vtest@swiggy.in\"";
	public static String UsmEmail = "\"Utest@swiggy.in\"";
	public static String smEmaillong = "\"VtestVtestVtestVtestVtestVtestVtestVtestV@swiggy.in\"";
	public static String asmEmail = "\"Vtest@swiggy.in\"";
	public static String UasmEmail = "\"Utest@swiggy.in\"";
	public static String asmEmaillong = "\"VtestVtestVtestVtestVtestVtestVtestVtestV@swiggy.in\"";
	public static String menuId = "\"254877\"";
	public static String UmenuId = "\"123\"";
	public static String menuIdlong = "\"254877254877\"";
	public static String restId = "75554";

	public static String cmsDB = "cms";
	public static String rmsVymoDB = "rmsVymo_db";
	public static String getRest_query = "Select * from swiggy.restaurants where id=";
	public static String getRestEnabled_query = "Select enabled from swiggy.restaurants where id=";
	public static String getRestFin_query = "Select * from swiggy.restaurants_finance_detail where rest_id=";
	public static String getVymoRest_query = "Select vymo_id from vymo_adapter_db.vymo_restaurant_map where rest_id=";
	public static String getRestSuggestV2FromDB_query = "select restaurants.id, restaurants.area_code, restaurants.city_code, restaurants_partner_map.type_of_partner from restaurants LEFT JOIN restaurants_partner_map on restaurants.id = restaurants_partner_map.rest_id where restaurants.area_code=160 AND restaurants.city_code=3 AND restaurants_partner_map.type_of_partner=3";


	static {
		try {
			if (RMSCommonHelper.getEnv().equalsIgnoreCase("stage1")) {
				restaurantIds[0] = "4993";
				restaurantIds[1] = "9990";
				restaurantId = "4993";
				auth = PrepTimeConstants.auth_password;
			} else if (RMSCommonHelper.getEnv().equalsIgnoreCase("stage2")) {
				restaurantIds[0] = "4993";
				restaurantIds[1] = "9990";
				restaurantId = "4993";
				auth = PrepTimeConstants.auth_password;
			} else if (RMSCommonHelper.getEnv().equalsIgnoreCase("stage3")) {
				restaurantIds[0] = "4993";
				restaurantIds[1] = "9990";
				restaurantId = "4993";
				auth = PrepTimeConstants.auth_password;
			} else if (RMSCommonHelper.getEnv().equalsIgnoreCase("prod")) {
				restaurantIds[0] = "4993";
				restaurantIds[1] = "9990";
				restaurantId = "4993";
				auth = PrepTimeConstants.auth;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
