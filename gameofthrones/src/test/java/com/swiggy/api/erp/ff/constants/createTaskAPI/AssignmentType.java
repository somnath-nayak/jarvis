package com.swiggy.api.erp.ff.constants.createTaskAPI;

public enum AssignmentType {

    AUTOMATIC(1), MANUAL(2);

    private final int id;

    AssignmentType(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }
}

