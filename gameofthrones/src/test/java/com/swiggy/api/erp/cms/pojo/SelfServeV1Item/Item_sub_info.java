package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Item_sub_info {
	  private String[] sub_info;

	    public String[] getSub_info ()
	    {
	        return sub_info;
	    }

	    public void setSub_info (String[] sub_info)
	    {
	        this.sub_info = sub_info;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [sub_info = "+sub_info+"]";
	    }
	}
			