package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/8/18.
 */
public class Addon_combination {

    private String addon_group_id;

    private String addon_id;

    public String getAddon_group_id() {
        return addon_group_id;
    }

    public void setAddon_group_id(String addon_group_id) {
        this.addon_group_id = addon_group_id;
    }

    public String getAddon_id() {
        return addon_id;
    }

    public void setAddon_id(String addon_id) {
        this.addon_id = addon_id;
    }

    public Addon_combination build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        if(this.getAddon_group_id() == null)
            this.setAddon_group_id(MenuConstants.addon_group_id);
        if(this.getAddon_id() == null)
            this.setAddon_id(MenuConstants.addonid);
    }

    @Override
    public String toString() {
        return "{" +
                "addon_group_id='" + addon_group_id + '\'' +
                ", addon_id='" + addon_id + '\'' +
                '}';
    }
}
