package com.swiggy.api.erp.delivery.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.erp.delivery.Pojos.Coordinate;
import com.swiggy.api.erp.delivery.Pojos.CreateCluster;
import com.swiggy.api.erp.delivery.Pojos.Polygon;
import com.swiggy.api.erp.delivery.constants.ClusterConstants;
import com.swiggy.api.erp.delivery.dp.ServiceabilityDataProvider;
import com.swiggy.api.erp.delivery.helper.*;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.Validator;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.dao.EmptyResultDataAccessException;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.*;
import java.util.Optional;

public class CerebroClusterSmokeTest extends ServiceabilityDataProvider
{
    ClusterServiceHelper clusterHelper = new ClusterServiceHelper();
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    ServiceablilityHelper serviceablilityHelper = new ServiceablilityHelper();
    Serviceability_Helper servHelper = new Serviceability_Helper();
    RedisHelper redisHelper = new RedisHelper();
    DeliveryDataBaseUtils dbUtils = new DeliveryDataBaseUtils();
    DegradationHelper degradationHelper = new DegradationHelper();

    static String ItemId, selfDelItemId, item_refid, selfDelRefId;
    int numRetries = 10, pollInt = 2000;
    String customerClusterName = "Automation_BLR_Koramangala_customer", restaurantClusterName = "Automation_BLR_Koramangala_restaurant", clusterNameForGettingDetailsFromDB = "BLR_BTM"/*"BLR_Koramangala"*/;
    String cgName = "SMOKE-CONNECTION-GROUP";

    static String connectionGroup = null;
    static String connectionId = null;
    static String custClusterId, restClusterId;
    static String name = "AutomationTest";


    String[] CustomerLatLng;

    String esmeEntityId = dbUtils.getEntityIdFromEsme(clusterNameForGettingDetailsFromDB);
    String esmeEntityRefId = dbUtils.getEntityRefIdFromEsme(clusterNameForGettingDetailsFromDB), transitionWindow = "0";

    @BeforeTest
    public void beforeClass()
    {
        clusterHelper.disableConnectionGroupAndConnection();
        if (clusterHelper.ifTupleExistsInDB("connection_groups", "name", cgName)) {
            try {
                String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + cgName + "'").get("id").toString();
                clusterHelper.deleteQuery("connection_groups", "name", cgName);
                if (cgId != null)
                    clusterHelper.deleteQuery("connections", "group_id", cgId);
            } catch (EmptyResultDataAccessException e) {
            }
        }
        custClusterId = clusterHelper.getCustomerCluster(name);
        restClusterId = clusterHelper.getRestaurantCluster(name);
        connectionGroup = clusterHelper.getConnectionGroup(name);
        connectionId = clusterHelper.getConnection(name);

        System.out.println("connectionGroup = " + connectionGroup);
        System.out.println("connectionId = " + connectionId);
        System.out.println("customerClusterName = " + custClusterId);
        System.out.println("restClusterId = " + restClusterId);

        item_refid = ClusterConstants.ref_id;
        ItemId = clusterHelper.createItem_andGetId(item_refid);

        clusterHelper.addItem_toCluster(restClusterId, ItemId);

        selfDelRefId = ClusterConstants.selfDelItem_ref_id;

        while (clusterHelper.ifItemExists(selfDelRefId)) {
            selfDelRefId = String.valueOf(Integer.sum(Integer.parseInt(selfDelRefId), 1000));
        }

        selfDelItemId = clusterHelper.createSelfDelItem_andGetId(selfDelRefId);
        clusterHelper.addItem_toCluster(restClusterId, selfDelItemId);
        CustomerLatLng = deliveryDataHelper.getCentroid(clusterHelper.getClusterLatLong(custClusterId)).split(",");
    }


    @AfterTest
    public void afterClass() {
        System.out.println("---------------after------------------");
        clusterHelper.deleteAfterTest(connectionId, connectionGroup, ItemId, selfDelItemId, custClusterId, restClusterId);
        clusterHelper.enableConnectionGroupAndConnection();
        clusterHelper.deleteQuery("items", "id", selfDelItemId);
        clusterHelper.deleteQuery("cluster_items", "item_id", selfDelItemId);
    }


    @BeforeGroups(groups = {"longDistance"})
    public void beforeGroup()
    {
        clusterHelper.enableConnectionGroupAndConnection();
    }

    @AfterGroups(groups = {"longDistance"})
    public void afterGroup()
    {
        clusterHelper.disableConnectionGroupAndConnection();
        clusterHelper.updateQuery("connection_groups", "enabled", "1", "id", connectionGroup);
        clusterHelper.updateQuery("connections", "enabled", "1", "id", connectionId);

    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_01 :- Verify items are serviceable when distance is less than the config at connection")
    public void distanceltCheck() {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        String[] listingPayload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"};
        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
        }});

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};
        Processor itemSearch = clusterHelper.Item_Search(payload);//clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        String distance_check = "", reason = "", item_list = "";
        try {
            String itemResponseAsText = itemSearch.ResponseValidator.GetBodyAsText();
            item_list = itemSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");
            String itemId = JsonPath.read(itemResponseAsText, "$.itemList[*].item.id")
                    .toString().replace("[", "").replace("]", "");

            distance_check = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertNotEquals(item_list, "[]");
        Assert.assertEquals(distance_check, "DISTANCE_CHECK");
        Assert.assertEquals(reason, "true");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(itemSearch.ResponseValidator.GetResponseCode(), 200);
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_02 :- Verify items are not serviceable when distance is greater than the config at connection")
    public void distancegtCheck() throws Exception {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        double distance = deliveryDataHelper.getDistanceBtwnTwoLatLongs(Double.parseDouble(CustomerLatLng[0]), Double.parseDouble(CustomerLatLng[1]), Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng));
        clusterHelper.connectionUpdateWithDistance(String.valueOf(distance - 500), custClusterId, restClusterId, connectionId);

        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
        }});

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};
        clusterHelper.deleteClusterCache();
        Processor itemSearch = clusterHelper.Item_Search(payload);



        clusterHelper.deleteClusterCache();
        String item_list = itemSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");
        String ignored_item_list = itemSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ignoredItemList");

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        clusterHelper.connectionUpdateWithDistance(ClusterConstants.last_mile, custClusterId, restClusterId, connectionId);

        Assert.assertEquals(ignored_item_list, "[]");
        Assert.assertEquals(item_list, "[]");
        Assert.assertNull(listingMappedResponse.get("serviceability"), "serviceability");
    }



    //DISTANCE CHECK FAILING BCOZ OF RESTAURANT LM..
    @Test(groups = "rashmi", priority=2, description = "CSTC_02 :- Verify items are not serviceable when distance is greater than the config at connection")
    public void distancegtCheckDueToLessRestLM() throws Exception {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        double distance = deliveryDataHelper.getDistanceBtwnTwoLatLongs(Double.parseDouble(CustomerLatLng[0]), Double.parseDouble(CustomerLatLng[1]), Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng));
        clusterHelper.connectionUpdateWithDistance(String.valueOf(distance + 2000), custClusterId, restClusterId, connectionId);

        clusterHelper.updateItemUsingPOJO(ItemId,"maxSecondMile", "0.1");

        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
        }});

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};
        clusterHelper.deleteClusterCache();
        Processor itemSearch = clusterHelper.Item_Search(payload);

        clusterHelper.deleteClusterCache();
        String item_list = itemSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");
        String ignored_item_list = itemSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ignoredItemList");

        String itemId = "", distance_check = "", reason = "";
        try {
            itemId = JsonPath.read(itemSearch.ResponseValidator.GetBodyAsText(), "$.ignoredItemList[*].item.id")
                    .toString().replace("[", "").replace("]", "");
            distance_check = JsonPath.read(itemSearch.ResponseValidator.GetBodyAsText(), "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemSearch.ResponseValidator.GetBodyAsText(), "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}

        clusterHelper.updateItemUsingPOJO(ItemId,"maxSecondMile", clusterHelper.itemHashMap().get("maxSecondMile"));
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        clusterHelper.connectionUpdateWithDistance(ClusterConstants.last_mile, custClusterId, restClusterId, connectionId);

        Assert.assertEquals(ignored_item_list, "[]");
        Assert.assertEquals(item_list, "[]");
       // Assert.assertEquals(distance_check, "DISTANCE_CHECK");
       // Assert.assertEquals(reason, "false");
        Assert.assertNull(listingMappedResponse.get("serviceability"), "serviceability");
        //Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "0", "non_serviceable_reason");
    }

    @Test(groups = "rashmi", priority=2, description = "CSTC_07 :- Verify items are serviceable when current BF is less than the closeBF config at connection")
    public void bannerltCheck() throws Exception{
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        String banner = "0.5";
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, banner);
        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
            put("banner_factor", banner);
        }});

        clusterHelper.deleteClusterCache();
        Processor itemSearch = clusterHelper.Item_Search(payload);
        String itemResponseAsText = itemSearch.ResponseValidator.GetBodyAsText();
        String itemId = JsonPath.read(itemResponseAsText, "$.itemList[*].item.id")
                .toString().replace("[", "").replace("]", "");

        String distance_check = "", reason = "";
        try {
            distance_check = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(distance_check, "BANNER_CHECK");
        Assert.assertEquals(reason, "true");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority=2, description = "Verify items are not serviceable when current BF is greater than the closeBF config at connection")
    public void bannergtCheck() {
        String current_banner_factor = "1234";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, current_banner_factor);

        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "1");
            put("non_serviceable_reason", "7");
            put("banner_factor", current_banner_factor);
        }});

        clusterHelper.deleteClusterCache();
        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, current_banner_factor, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};
        Processor itemSearch = clusterHelper.Item_Search(payload);
        //Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], current_banner_factor,
                //ClusterConstants.sla_extra_time, item_refid);

        String itemResponseAsText = itemSearch.ResponseValidator.GetBodyAsText();
        String itemId = JsonPath.read(itemResponseAsText, "$.ignoredItemList..item.id")
                .toString().replace("[", "").replace("]", "");

        String distance_check = "", reason = "";
        try {
            distance_check = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(distance_check, "BANNER_CHECK");
        Assert.assertEquals(reason, "false");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "7", "non_serviceable_reason");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_13 :- Verify items are serviceable if sla is less than the max_sla config at connection")
    public void slaltCheck() throws InterruptedException {
        String banner = "0.5";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, banner);
        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);                                                        //setting max_sla=70..
        List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select filter_group from connections where id='" + connectionId + "'");

        String maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
                .toString().replace("[", "").replace("]", "");
        String item_list = null;
        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
            put("banner_factor", banner);
        }});

        if (maxSla.equals(ClusterConstants.max_sla)) {
            String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};
            clusterHelper.deleteClusterCache();
            item_list = clusterHelper.Item_Search(payload).ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");
            //item_list = clusterHelper.fetchItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);
        }
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        Assert.assertEquals(maxSla, ClusterConstants.max_sla);
        Assert.assertNotEquals(item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_14 :- Verify items are not serviceable if sla is greater than the max_sla config at connection")
    public void slaCheckGt() throws InterruptedException {
        String max_sla = "10";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        clusterHelper.connectionUpdate(max_sla, custClusterId, restClusterId, connectionId);                                    //setting max-sla=10..
        List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select filter_group from connections where id='" + connectionId + "'");

        String maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
                .toString().replace("[", "").replace("]", "");
        String ignored_item_list = null;

        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "0");
            put("non_serviceable_reason", "1");
        }});
        if (maxSla.equals(max_sla)) {
            String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

            clusterHelper.deleteClusterCache();
            ignored_item_list = clusterHelper.Item_Search(payload).ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ignoredItemList");
            /*ignored_item_list = clusterHelper.fetchIgnoredItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1],
                    ClusterConstants.currentBF, ClusterConstants.is_cluster_zone_open, item_refid);*/
        }



        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);
        Assert.assertEquals(maxSla, max_sla);
        Assert.assertNotEquals(ignored_item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "0", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }

    //
    @Test(groups = "rashmi", priority=2, description = "CSTC_15 :- Verify item is serviceable/unserviceable depending upon the connections of corresponding degradation state")
    public void rainModegtCheck() throws InterruptedException
    {
        String[] payload1 = {clusterNameForGettingDetailsFromDB, transitionWindow, esmeEntityRefId};
        degradationHelper.updateEntity(payload1, esmeEntityId);
        String response = degradationHelper.getEntityCurrentState(esmeEntityId).ResponseValidator.GetBodyAsText();
        String currentStateId = JsonPath.read(response, "$..stateId").toString().replace("[", "").replace("]", "");
        String activatedMachineId = JsonPath.read(response, "$..machineId").toString().replace("[", "").replace("]", "");
        //changing current state to heavy..
        String to_state = "2";//String.valueOf(Integer.sum(Integer.parseInt(currentStateId), 1));
        System.out.println("*********** to state ************ " + to_state);
        System.out.println("*********** activatedMachineId ************ " + activatedMachineId);
        System.out.println("*********** currentStateId ************ " + currentStateId);
        degradationHelper.changeMachineState(Integer.parseInt(esmeEntityId), Integer.parseInt(activatedMachineId), Integer.parseInt(to_state));
        String max_sla = "10";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "2.5");
        String rainModeInZone = clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, "1");
        String connection_id_rainMode = clusterHelper.createConnectionWithDegradationState(connectionGroup,
                custClusterId, restClusterId, ClusterConstants.last_mile,
                max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, "1",
                ClusterConstants.connection_type, ClusterConstants.is_connection_enabled,
                "heavy").ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");

        Thread.sleep(10000);
        //update cluster type to heavy to execute heavy rain mode connection type..
        String clusterName = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + restClusterId + "' and tags='restaurant'").get("name").toString();
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "heavy");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "heavy", "restaurant"}, restClusterId);

        System.out.println("********* cluster attributes ********** " + SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from clusters where id='" + restClusterId + "' and tags='restaurant'").get("attributes"));

        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "0");
            put("non_serviceable_reason", "1");
        }});

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

        clusterHelper.deleteClusterCache();

        Processor itemSearch = clusterHelper.Item_Search(payload);
        //Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        String itemResponseAsText = itemSearch.ResponseValidator.GetBodyAsText();
        String itemId = JsonPath.read(itemResponseAsText, "$.itemList[*].item.id")
                .toString().replace("[", "").replace("]", "");

        List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select filter_group from connections where id='" + connection_id_rainMode + "'");

        String maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
                .toString().replace("[", "").replace("]", "");
        String ignored_item_list = null;


        if (maxSla.equals(max_sla)) {
            clusterHelper.deleteClusterCache();
            ignored_item_list = clusterHelper.fetchIgnoredItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1],
                    ClusterConstants.currentBF, ClusterConstants.is_cluster_zone_open, item_refid);
        }
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        //update cluster type to heavy to execute normal rain mode connection type..
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "normal");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "normal", "restaurant"}, restClusterId);
        degradationHelper.changeMachineState(Integer.parseInt(esmeEntityId), Integer.parseInt(activatedMachineId), 1);
        clusterHelper.deleteQuery("connections", "id", connection_id_rainMode);
        clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, rainModeInZone);

        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);


        Assert.assertEquals(maxSla, max_sla);
        Assert.assertNotEquals(ignored_item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "0", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }


    //serviceability through both the connections..
    @Test(groups = "rashmi", priority=2, description = "CSTC_15 :- Verify item is serviceable/unserviceable depending upon the connections of corresponding degradation state")
    public void serviceableDueToHeavyRainModeConnection() throws InterruptedException
    {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        //update cluster type to heavy to execute heavy rain mode connection type..
        String clusterName = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + restClusterId + "' and tags='restaurant'").get("name").toString();
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "heavy");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "heavy", "restaurant"}, restClusterId);

        String rainModeInZone = clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, "4");
        String connection_id_rainMode = clusterHelper.createConnectionWithDegradationState(connectionGroup, custClusterId, restClusterId, ClusterConstants.last_mile,
                ClusterConstants.max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, "1",
                ClusterConstants.connection_type, ClusterConstants.is_connection_enabled,
                "heavy").ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");

        System.out.println("********* cluster attributes ********** " + SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from clusters where id='" + restClusterId + "' and tags='restaurant'").get("attributes"));

        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
        }});

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

        clusterHelper.deleteClusterCache();
        String item_list = clusterHelper.Item_Search(payload).ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");//String item_list = clusterHelper.fetchItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        //update cluster type to normal..
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "normal");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "normal", "restaurant"}, restClusterId);
        clusterHelper.deleteQuery("connections", "id", connection_id_rainMode);
        clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, rainModeInZone);

        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);

        Assert.assertNotEquals(item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
    }

    @Test(groups= "longDistance", description = "CSTC_07 :- Verify items are serviceable when current BF is less than the closeBF config at connection")
    public void longDistanceFlagSyncCheck() {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        Boolean flag = false;

        clusterHelper.deleteClusterCache();
        String response = servHelper.listingCallWithoutRestaurantId(new String[]{"22.644822", "88.347903", ClusterConstants.cityId, "true"}).ResponseValidator.GetBodyAsText();
        Map<String, String> listingWithCluster = servHelper.getServiceabiltyResponseMapWithoutRestaurantId(response);

        clusterHelper.deleteClusterCache();
        String response1 = servHelper.listingCallWithoutRestaurantId(new String[]{"22.644822", "88.347903", ClusterConstants.cityId, "false"}).ResponseValidator.GetBodyAsText();
        Map<String, String> listingWithCerebro = servHelper.getServiceabiltyResponseMapWithoutRestaurantId(response1);
        System.out.println("*******  MAP CLUSTER ******** " + listingWithCluster);
        System.out.println("*******  MAP CEREBRO ******** " + listingWithCerebro);

        Map<String, String> restaurantsBothInClusterAndCerebroWithSyncedLDflag = new LinkedHashMap<>();
        Map<String, String> restaurantsInBothWithAsyncedLDflag_ClusterFlag = new LinkedHashMap<>();
        Map<String, String> restaurantsInBothWithAsyncedLDflag_CerebroFlag = new LinkedHashMap<>();
        Map<String, String> restaurantsInClusterOnly = new LinkedHashMap<>();
        Map<String, String> restaurantsInCerebroOnly = new LinkedHashMap<>();

        flag = (listingWithCerebro.size() == 0 || listingWithCluster.size() == 0) ? false : true;
        if(!flag) {
            for (Map.Entry<String, String> entry : listingWithCerebro.entrySet()) {
                for (Map.Entry<String, String> entry1 : listingWithCluster.entrySet()) {
                    if (entry.getKey().equalsIgnoreCase(entry1.getKey()) && entry.getValue().equalsIgnoreCase(entry1.getValue())) {
                        flag = true;
                        restaurantsBothInClusterAndCerebroWithSyncedLDflag.put(entry.getKey(), entry.getValue());
                    } else if (entry.getKey().equalsIgnoreCase(entry1.getKey()) && !entry.getValue().equalsIgnoreCase(entry1.getValue())) {

                        restaurantsInBothWithAsyncedLDflag_CerebroFlag.put(entry.getKey(), entry.getValue());
                        restaurantsInBothWithAsyncedLDflag_ClusterFlag.put(entry1.getKey(), entry1.getValue());
                    } else if(!entry.getKey().equalsIgnoreCase(entry1.getKey())){

                        restaurantsInCerebroOnly.put(entry.getKey(), entry.getValue());
                        restaurantsInClusterOnly.put(entry1.getKey(), entry1.getValue());

                        if (entry1.getValue().equals("1"))
                            System.out.println("******* entry1 for cluster = " + entry1);
                        if (entry.getValue().equals("1"))
                            System.out.println("****** entry for cerebro = " + entry);
                    }
                }
            }
        }
        System.out.println("restaurantsInCerebroOnly = " + restaurantsInCerebroOnly);
        System.out.println("restaurantsInClusterOnly = " + restaurantsInClusterOnly);
        System.out.println("restaurantsInBothWithAsyncedLDflag_CerebroFlag = " + restaurantsInBothWithAsyncedLDflag_CerebroFlag);
        System.out.println("restaurantsInBothWithAsyncedLDflag_ClusterFlag = " + restaurantsInBothWithAsyncedLDflag_ClusterFlag);
        System.out.println("restaurantsBothInClusterAndCerebroWithSyncedLDflag = " + restaurantsBothInClusterAndCerebroWithSyncedLDflag);

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertTrue(flag, "LD flag is not in sync or response is empty. Response");
    }

    @Test(groups = "rashmi", priority = 1, description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
    public void selfDeliveryServiceableCheck()
    {
        String current_banner = "0";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, current_banner);

        /*LinkedHashMap<String, String> itemMap = clusterHelper.selfDelItemAttributes();
        itemMap.put("selfDeliveryOverriden", "0");
        itemMap.put("deliveryPartnerActive", "1");
        itemMap.put("serviceable", "1");
        itemMap.put("partnerId", "12");*/

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);

       /* String[] payload = clusterHelper.getSelfDelItemElements(String.valueOf(clusterHelper.restaurantLat(selfDelRefId)), String.valueOf(clusterHelper.restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
        clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);*/
        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, selfDelRefId, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
        }});

        clusterHelper.deleteClusterCache();
        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, selfDelRefId};
        Validator response = clusterHelper.Item_Search(payload).ResponseValidator;
        //Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(CustomerLatLng[0], CustomerLatLng[1], selfDelRefId)).ResponseValidator;
        String response1 = response.GetBodyAsText();
        int responseCode = response.GetResponseCode();
        String itemList = JsonPath.read(response1, "$.itemList").toString();
        String minSla = JsonPath.read(response1, "$.itemList..local_attributes.minSla").toString().replace("[", "").replace("]", "");
        String maxSla = JsonPath.read(response1, "$.itemList..local_attributes.maxSla").toString().replace("[", "").replace("]", "");


        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(responseCode, 200);
        Assert.assertNotEquals(itemList, "[]");
        Assert.assertEquals(minSla, maxSla);
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
    }
    //ignored item list
    @Test(groups = "rashmi", priority = 1, description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
    public void selfDeliveryUnServiceableDueToBannerCheck()
    {
       /* LinkedHashMap<String, String> itemMap = clusterHelper.selfDelItemAttributes();
        itemMap.put("selfDeliveryOverriden", "0");
        itemMap.put("deliveryPartnerActive", "1");
        itemMap.put("serviceable", "1");
        itemMap.put("partnerId", "12");*/

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);

        String current_banner = "1234";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, current_banner);

        /*String[] payload = clusterHelper.getSelfDelItemElements(String.valueOf(clusterHelper.restaurantLat(selfDelRefId)), String.valueOf(clusterHelper.restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
        clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);*/
        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, selfDelRefId, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "1");
            put("non_serviceable_reason", "7");
        }});

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, current_banner, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, selfDelRefId};
        clusterHelper.deleteClusterCache();
        Validator response = clusterHelper.Item_Search(payload).ResponseValidator;
        // Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(CustomerLatLng[0], CustomerLatLng[1], selfDelRefId, current_banner)).ResponseValidator;
        String response1 = response.GetBodyAsText();
        int responseCode = response.GetResponseCode();
        String itemList = JsonPath.read(response1, "$.itemList").toString();
        String minSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.minSla").toString().replace("[", "").replace("]", "");
        String maxSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.maxSla").toString().replace("[", "").replace("]", "");


        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(responseCode, 200);
        Assert.assertEquals(itemList, "[]");
        Assert.assertEquals(minSla, maxSla);
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "7", "non_serviceable_reason");
    }


    //PENDING..
    @Test(groups = "rashmi", priority = 1, description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
    public void selfDeliveryUnServiceableDueToRainMode()
    {
       /* LinkedHashMap<String, String> itemMap = clusterHelper.selfDelItemAttributes();
        itemMap.put("selfDeliveryOverriden", "0");
        itemMap.put("deliveryPartnerActive", "1");
        itemMap.put("serviceable", "1");
        itemMap.put("partnerId", "12");*/

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);

        String current_banner = "1";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, current_banner);

        String clusterName = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + restClusterId + "' and tags='restaurant'").get("name").toString();
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "heavy");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "heavy", "restaurant"}, restClusterId);

        String rainModeInZone = clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, "4");
        String connection_id_rainMode = clusterHelper.createConnectionWithDegradationState(connectionGroup, custClusterId, restClusterId, ClusterConstants.last_mile,
                "1", ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, "1",
                ClusterConstants.connection_type, ClusterConstants.is_connection_enabled,
                "heavy").ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");

        /*String[] payload = clusterHelper.getSelfDelItemElements(String.valueOf(clusterHelper.restaurantLat(selfDelRefId)), String.valueOf(clusterHelper.restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
        clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);*/
        clusterHelper.deleteClusterCache();
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, selfDelRefId, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "0");
            put("non_serviceable_reason", "1");
        }});

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, selfDelRefId};
        clusterHelper.deleteClusterCache();
        Validator response = clusterHelper.Item_Search(payload).ResponseValidator;
        //Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(CustomerLatLng[0], CustomerLatLng[1], selfDelRefId, current_banner)).ResponseValidator;
        String response1 = response.GetBodyAsText();
        int responseCode = response.GetResponseCode();
        String itemList = JsonPath.read(response1, "$.itemList").toString();
        String minSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.minSla").toString().replace("[", "").replace("]", "");
        String maxSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.maxSla").toString().replace("[", "").replace("]", "");


        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "normal");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "normal", "restaurant"}, restClusterId);
        clusterHelper.deleteQuery("connections", "id", connection_id_rainMode);
        clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, rainModeInZone);

        Assert.assertEquals(responseCode, 200);
        Assert.assertEquals(itemList, "[]");
        Assert.assertEquals(minSla, maxSla);
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "0", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }
    //vj
    @Test(priority=3,dataProvider = "deliveryListing_Simple1",invocationCount = 1)
    public void deliveryDataAndType_CerebroVsCluster(String lat, String lng, String city_id, String cluster_debug_enabled){
        Processor processor1 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,"true"});
        Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
        Assert.assertTrue(serviceablilityHelper.verifyClusterResponseIsFromFallback(processor1.ResponseValidator.GetBodyAsText()));

        Processor processor2 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,"false"});
        Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);

        //This is comparing whole response between cerebro and cluster
//		serviceablilityHelper.jsonCompareByString(processor1.ResponseValidator.GetBodyAsText(),processor2.ResponseValidator.GetBodyAsText(),"response1","response2");

        //This is asserting all the response are having datatype as per old contract
        boolean result = serviceablilityHelper.verifyJsonDataTypeForListingAPi(processor1.ResponseValidator.GetBodyAsText(),processor2.ResponseValidator.GetBodyAsText());
        System.out.println(ServiceablilityHelper.errorMessage);
        Assert.assertTrue(result);

        //serviceablilityHelper.ComparewithExpectedResponse(processor2.ResponseValidator.GetBodyAsText(),processor1.ResponseValidator.GetBodyAsText(),null);

    }

    @Test(priority=3,dataProvider = "deliveryListing_Simple1",invocationCount = 1)
    public void deliveryDuplicateCheck_CerebroVsCluster(String lat, String lng, String city_id, String cluster_debug_enabled){
        Processor processor1 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,"true"});
        Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);

        Assert.assertTrue(!serviceablilityHelper.verifyClusterResponseIsFromFallback(processor1.ResponseValidator.GetBodyAsText()));

        Processor processor2 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,"false"});
        Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);

        // This is asserting any restaurant Id is repeating in the response
        serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor1.ResponseValidator.GetBodyAsText());
        serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor2.ResponseValidator.GetBodyAsText());
        Assert.assertTrue(serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor1.ResponseValidator.GetBodyAsText()));
        Assert.assertTrue(serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor2.ResponseValidator.GetBodyAsText()));

    }

    @Test(priority=3,dataProvider = "deliveryListingResponseCountCheck")
    public void deliveryListingResponseCountCheck(String lat, String lng, String city_id, String cluster_debug_enabled, int acceptablePercentage){
        Processor processor1 = null;
        Integer[] size = new Integer[10];
        for(int i = 0 ; i < 10 ; i++)
        {
            processor1 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,cluster_debug_enabled});
            Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
            if(Boolean.getBoolean(cluster_debug_enabled))
            {
                Assert.assertTrue(!serviceablilityHelper.verifyClusterResponseIsFromFallback(processor1.ResponseValidator.GetBodyAsText()));
            }
            int currentSize = serviceablilityHelper.getTotalRestaurantsCountIdnListingAPi(processor1.ResponseValidator.GetBodyAsText());
            System.out.println("currentSize = " + currentSize);
            size[i] = currentSize;
            //Assert.assertTrue(serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor1.ResponseValidator.GetBodyAsText()));

        }
        Optional<Integer> max = Arrays.stream(size).max(Integer::compareTo);
        Optional<Integer> min = Arrays.stream(size).min(Integer::compareTo);
        System.out.println("min = " + min.toString());
        System.out.println("max = " + max.toString());
        Assert.assertTrue(min.equals(max));
//        int percentageOfMiss = Math.abs(((max -min)/max)*100);
//        Assert.assertTrue(percentageOfMiss<acceptablePercentage);
    }

    @Test(priority=3,dataProvider = "distanceFilterCheck")
    public void distanceFilterCheck(String lat, String lng, String city_id, String cluster_debug_enabled, double distance){
        Processor processor1 = null;
        processor1 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,cluster_debug_enabled});
        Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
        if(Boolean.getBoolean(cluster_debug_enabled))
        {
            Assert.assertTrue(!serviceablilityHelper.verifyClusterResponseIsFromFallback(processor1.ResponseValidator.GetBodyAsText()));
        }
        Assert.assertTrue(serviceablilityHelper.getTotalRestaurantsCountServiceableDistance(processor1.ResponseValidator.GetBodyAsText(),distance));
        Assert.assertTrue(serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor1.ResponseValidator.GetBodyAsText()));

    }

    @Test(priority=3,dataProvider = "slaFilterCheck")
    public void slaFilterCheck(String lat, String lng, String city_id, String cluster_debug_enabled, Integer SLA){
        Processor processor1 = null;
        processor1 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,cluster_debug_enabled});
        Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
        if(Boolean.getBoolean(cluster_debug_enabled))
        {
            Assert.assertTrue(!serviceablilityHelper.verifyClusterResponseIsFromFallback(processor1.ResponseValidator.GetBodyAsText()));
        }
        Assert.assertTrue(serviceablilityHelper.getTotalRestaurantsCountServiceableSLA(processor1.ResponseValidator.GetBodyAsText(), SLA));
        Assert.assertTrue(serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor1.ResponseValidator.GetBodyAsText()));

    }

    @Test(priority=3,dataProvider = "bannerFactorCheck")
    public void bannerFactorCheck(String lat, String lng, String city_id, String cluster_debug_enabled, Double bannerFactor){
        Processor processor1 = null;
        processor1 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,cluster_debug_enabled});
        Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
        if(Boolean.getBoolean(cluster_debug_enabled))
        {
            Assert.assertTrue(!serviceablilityHelper.verifyClusterResponseIsFromFallback(processor1.ResponseValidator.GetBodyAsText()));
        }
        Assert.assertTrue(serviceablilityHelper.getTotalRestaurantsCountServiceableBF(processor1.ResponseValidator.GetBodyAsText(), bannerFactor));
        Assert.assertTrue(serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor1.ResponseValidator.GetBodyAsText()));

    }

    @Test(priority=3,dataProvider = "cacheCheck")
    public void cacheTest(String lat, String lng, String city_id, String cluster_debug_enabled, Double bannerFactor){
        Processor processor1 = null;
        Processor processor2 = null;
        Integer[] size = new Integer[10];
        Integer[] size2 = new Integer[10];
        Integer[] size3 = new Integer[10];

        processor2 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,cluster_debug_enabled});
        Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
        if(Boolean.getBoolean(cluster_debug_enabled))
        {
            Assert.assertTrue(!serviceablilityHelper.verifyClusterResponseIsFromFallback(processor2.ResponseValidator.GetBodyAsText()));
        }
        int sizeBeforeUpdate = serviceablilityHelper.getTotalRestaurantsCountIdnListingAPi(processor2.ResponseValidator.GetBodyAsText());
        System.out.println("sizeBeforeUpdate = " + sizeBeforeUpdate);
        clusterHelper.connectionUpdateWithDistance("2000", custClusterId, restClusterId, connectionId);
        for(int i = 0 ; i < 10 ; i++)
        {
            processor1 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,cluster_debug_enabled});
            Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
            if(Boolean.getBoolean(cluster_debug_enabled))
            {
                Assert.assertTrue(!serviceablilityHelper.verifyClusterResponseIsFromFallback(processor1.ResponseValidator.GetBodyAsText()));
            }
            int currentSize = serviceablilityHelper.getTotalRestaurantsCountIdnListingAPi(processor1.ResponseValidator.GetBodyAsText());
            System.out.println("currentSize = " + currentSize);
            //Assert.assertTrue(serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor1.ResponseValidator.GetBodyAsText()));
            size[i] = currentSize;

        }
        Optional<Integer> max = Arrays.stream(size).max(Integer::compareTo);
        Optional<Integer> min = Arrays.stream(size).min(Integer::compareTo);
        System.out.println("min = " + min.toString());
        System.out.println("max = " + max.toString());
        Assert.assertTrue(min.equals(max));

        clusterHelper.connectionUpdateBF("2", custClusterId, restClusterId, connectionId);
        for(int i = 0 ; i < 10 ; i++)
        {
            processor1 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,cluster_debug_enabled});
            Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
            if(Boolean.getBoolean(cluster_debug_enabled))
            {
                Assert.assertTrue(!serviceablilityHelper.verifyClusterResponseIsFromFallback(processor1.ResponseValidator.GetBodyAsText()));
            }
            int currentSize = serviceablilityHelper.getTotalRestaurantsCountIdnListingAPi(processor1.ResponseValidator.GetBodyAsText());
            size2[i] = currentSize;
            //Assert.assertTrue(serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor1.ResponseValidator.GetBodyAsText()));

        }
        Optional<Integer> max2 = Arrays.stream(size2).max(Integer::compareTo);
        Optional<Integer> min2 = Arrays.stream(size2).min(Integer::compareTo);
        System.out.println("min 2 = " + min2.toString());
        System.out.println("max 2 = " + max2.toString());
        Assert.assertTrue(min2.equals(max2));

        clusterHelper.connectionUpdate("50", custClusterId, restClusterId, connectionId);
        for(int i = 0 ; i < 10 ; i++)
        {
            processor1 = serviceablilityHelper.listing(new String[] {lat, lng, city_id,cluster_debug_enabled});
            Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
            if(Boolean.getBoolean(cluster_debug_enabled))
            {
                Assert.assertTrue(!serviceablilityHelper.verifyClusterResponseIsFromFallback(processor1.ResponseValidator.GetBodyAsText()));
            }
            int currentSize = serviceablilityHelper.getTotalRestaurantsCountIdnListingAPi(processor1.ResponseValidator.GetBodyAsText());
            size3[i] = currentSize;
            //Assert.assertTrue(serviceablilityHelper.verifyJsonDuplicateRestaurantIdListingAPi(processor1.ResponseValidator.GetBodyAsText()));

        }
        Optional<Integer> max3 = Arrays.stream(size3).max(Integer::compareTo);
        Optional<Integer> min3 = Arrays.stream(size3).min(Integer::compareTo);
        System.out.println("min 3 = " + min3.toString());
        System.out.println("max 3 = " + max3.toString());
        Assert.assertTrue(min3.equals(max3));


    }



    //*********************************************************** CART ***************************************************

    String listing_item_count = "1", total_item_count = "1", restaurantName = "just_testing";

    @Test(groups = "rashmi", priority=2, description = "CSTC_01 :- Verify items are serviceable when distance is less than the config at connection")
    public void distanceltCheckCart() {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");

        String[] cartPayload = new String[]
                {listing_item_count, total_item_count, item_refid, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]};

        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String,String>(){{
            put("serviceable", "2");
        }});
        Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        String distance_check = "", reason = "";
        try {
            String itemResponseAsText = item_check_response.ResponseValidator.GetBodyAsText();
            String itemId = JsonPath.read(itemResponseAsText, "$.itemList[*].item.id")
                    .toString().replace("[", "").replace("]", "");

            distance_check = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(distance_check, "DISTANCE_CHECK");
        Assert.assertEquals(reason, "true");
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_02 :- Verify items are not serviceable when distance is greater than the config at connection")
    public void distancegtCheckCart() {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        String[] listingPayload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"};
        double distance = deliveryDataHelper.getDistanceBtwnTwoLatLongs(Double.parseDouble(CustomerLatLng[0]), Double.parseDouble(CustomerLatLng[1]), Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng));
        clusterHelper.connectionUpdateWithDistance(String.valueOf(distance - 500), custClusterId, restClusterId, connectionId);

        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(10, pollInt, new String[]
                {listing_item_count, total_item_count, item_refid, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "0");
            put("non_serviceable_reason", "10");
        }});
        Processor response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);
        String item_list = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");
        String ignored_item_list = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ignoredItemList");

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        clusterHelper.connectionUpdateWithDistance(ClusterConstants.last_mile, custClusterId, restClusterId, connectionId);
        System.out.println("*********** serviceability" + listingMappedResponse);

        Assert.assertEquals(ignored_item_list, "[]");
        Assert.assertEquals(item_list, "[]");
        //empty response from cluster leads to cerebro fallback..
        if(!dbUtils.isRestaurantPresentInDeliveryDB(item_refid)) {
            Assert.assertEquals(listingMappedResponse.get("serviceable"), "0", "serviceability");
            Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "10", "non_serviceable_reason");
        }
        else
            Assert.assertTrue(dbUtils.isRestaurantPresentInDeliveryDB(item_refid), "cerebro fallback");
    }

    @Test(groups = "rashmi", priority=2, description = "CSTC_07 :- Verify items are serviceable when current BF is less than the closeBF config at connection")
    public void bannerltCheckCart() {
        String banner = "0";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, banner);
        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, new String[]
                {listing_item_count, total_item_count, item_refid, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "2");
            put("banner_factor", banner);
        }});

        String distance_check = "", reason = "";
        try {
            Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

            String itemResponseAsText = item_check_response.ResponseValidator.GetBodyAsText();
            String itemId = JsonPath.read(itemResponseAsText, "$.itemList[*].item.id")
                    .toString().replace("[", "").replace("]", "");

            distance_check = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}


        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(distance_check, "BANNER_CHECK");
        Assert.assertEquals(reason, "true");
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority=2, description = "Verify items are not serviceable when current BF is greater than the closeBF config at connection")
    public void bannergtCheckCart() {
        String current_banner_factor = "1234";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, current_banner_factor);
        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, new String[]
                {listing_item_count, total_item_count, item_refid, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "1");
            put("non_serviceable_reason", "7");
            put("banner_factor", current_banner_factor);
        }});

        Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], current_banner_factor,
                ClusterConstants.sla_extra_time, item_refid);

        String distance_check = "", reason = "";
        try {
            String itemResponseAsText = item_check_response.ResponseValidator.GetBodyAsText();
            String itemId = JsonPath.read(itemResponseAsText, "$.ignoredItemList..item.id")
                    .toString().replace("[", "").replace("]", "");

            distance_check = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}


        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(distance_check, "BANNER_CHECK");
        Assert.assertEquals(reason, "false");
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "1", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "7", "non_serviceable_reason");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_13 :- Verify items are serviceable if sla is less than the max_sla config at connection")
    public void slaltCheckCart() throws InterruptedException {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);                                                        //setting max_sla=70..
        List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select filter_group from connections where id='" + connectionId + "'");

        String maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
                .toString().replace("[", "").replace("]", "");
        String item_list = null;
        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, new String[]
                {listing_item_count, total_item_count, item_refid, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "2");
        }});

        if (maxSla.equals(ClusterConstants.max_sla)) {
            item_list = clusterHelper.fetchItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        }

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);


        Assert.assertEquals(maxSla, ClusterConstants.max_sla);
        Assert.assertNotEquals(item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_14 :- Verify items are not serviceable if sla is greater than the max_sla config at connection")
    public void slagtCheckCart() throws InterruptedException {
        String max_sla = "10";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        clusterHelper.connectionUpdate(max_sla, custClusterId, restClusterId, connectionId);                                    //setting max-sla=10..
        List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select filter_group from connections where id='" + connectionId + "'");

        String maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
                .toString().replace("[", "").replace("]", "");
        String ignored_item_list = null;
        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, new String[]
                {listing_item_count, total_item_count, item_refid, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "0");
            put("non_serviceable_reason", "1");
        }});
        if (maxSla.equals(max_sla)) {
            ignored_item_list = clusterHelper.fetchIgnoredItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1],
                    ClusterConstants.currentBF, ClusterConstants.is_cluster_zone_open, item_refid);
        }

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);
        Assert.assertEquals(maxSla, max_sla);
        Assert.assertNotEquals(ignored_item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "0", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_15 :- Verify item is serviceable/unserviceable depending upon the connections of corresponding degradation state")
    public void rainModegtCheckCart() throws InterruptedException
    {
        String max_sla = "10";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        String rainModeInZone = clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, "4");
        String connection_id_rainMode = clusterHelper.createConnectionWithDegradationState(connectionGroup, custClusterId, restClusterId, ClusterConstants.last_mile,
                max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, "1",
                ClusterConstants.connection_type, ClusterConstants.is_connection_enabled,
                "heavy").ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
        //update cluster type to heavy to execute heavy rain mode connection type..
        String clusterName = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + restClusterId + "' and tags='restaurant'").get("name").toString();
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "heavy");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "heavy", "restaurant"}, restClusterId);

        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, new String[]
                {listing_item_count, total_item_count, item_refid, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "0");
            put("non_serviceable_reason", "1");
        }});
        Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        String itemResponseAsText = item_check_response.ResponseValidator.GetBodyAsText();
        String itemId = JsonPath.read(itemResponseAsText, "$.itemList[*].item.id")
                .toString().replace("[", "").replace("]", "");

        List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select filter_group from connections where id='" + connection_id_rainMode + "'");

        String maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
                .toString().replace("[", "").replace("]", "");
        String ignored_item_list = null;
        if (maxSla.equals(max_sla)) {
            ignored_item_list = clusterHelper.fetchIgnoredItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1],
                    ClusterConstants.currentBF, ClusterConstants.is_cluster_zone_open, item_refid);
        }


        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        //update cluster type to heavy to execute normal rain mode connection type..
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "normal");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "normal", "restaurant"}, restClusterId);
        clusterHelper.deleteQuery("connections", "id", connection_id_rainMode);
        clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, rainModeInZone);

        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);
        Assert.assertEquals(maxSla, max_sla);
        Assert.assertNotEquals(ignored_item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "0", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }


    @Test(groups = "rashmi", priority=1, description = "CSTC_15 :- Verify item is serviceable/unserviceable depending upon the connections of corresponding degradation state")
    public void serviceableDueToHeavyRainModeConnectionCart() throws InterruptedException
    {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        //update cluster type to heavy to execute heavy rain mode connection type..
        String clusterName = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + restClusterId + "' and tags='restaurant'").get("name").toString();
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "heavy");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "heavy", "restaurant"}, restClusterId);

        String rainModeInZone = clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, "4");
        String connection_id_rainMode = clusterHelper.createConnectionWithDegradationState(connectionGroup, custClusterId, restClusterId, ClusterConstants.last_mile,
                ClusterConstants.max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, "1",
                ClusterConstants.connection_type, ClusterConstants.is_connection_enabled,
                "heavy").ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");

        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, new String[]
                {listing_item_count, total_item_count, item_refid, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "2");
        }});
        String item_list = clusterHelper.fetchItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        //update cluster type to normal..
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "normal");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "normal", "restaurant"}, restClusterId);
        clusterHelper.deleteQuery("connections", "id", connection_id_rainMode);
        clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, rainModeInZone);

        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);

        Assert.assertNotEquals(item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority = 1, description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
    public void selfDeliveryServiceableCheckCart()
    {
        String current_banner = "0";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, current_banner);

        /*LinkedHashMap<String, String> itemMap = clusterHelper.selfDelItemAttributes();
        itemMap.put("selfDeliveryOverriden", "0");
        itemMap.put("deliveryPartnerActive", "1");
        itemMap.put("serviceable", "1");
        itemMap.put("partnerId", "12");*/

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);

       /* String[] payload = clusterHelper.getSelfDelItemElements(String.valueOf(clusterHelper.restaurantLat(selfDelRefId)), String.valueOf(clusterHelper.restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
        clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);*/
        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, new String[]
                {listing_item_count, total_item_count, selfDelRefId, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "2");
        }});
        Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(CustomerLatLng[0], CustomerLatLng[1], selfDelRefId)).ResponseValidator;
        String response1 = response.GetBodyAsText();
        int responseCode = response.GetResponseCode();
        String itemList = JsonPath.read(response1, "$.itemList").toString();
        String minSla = JsonPath.read(response1, "$.itemList..local_attributes.minSla").toString().replace("[", "").replace("]", "");
        String maxSla = JsonPath.read(response1, "$.itemList..local_attributes.maxSla").toString().replace("[", "").replace("]", "");

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(responseCode, 200);
        Assert.assertNotEquals(itemList, "[]");
        Assert.assertEquals(minSla, maxSla);
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "2", "serviceability");
    }

    @Test(groups = "rashmi", priority = 1, description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
    public void selfDeliveryUnServiceableDueToBannerCheckCart()
    {
       /* LinkedHashMap<String, String> itemMap = clusterHelper.selfDelItemAttributes();
        itemMap.put("selfDeliveryOverriden", "0");
        itemMap.put("deliveryPartnerActive", "1");
        itemMap.put("serviceable", "1");
        itemMap.put("partnerId", "12");*/

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);

        String current_banner = "1234";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "10");

        /*String[] payload = clusterHelper.getSelfDelItemElements(String.valueOf(clusterHelper.restaurantLat(selfDelRefId)), String.valueOf(clusterHelper.restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
        clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);*/
        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, new String[]
                {listing_item_count, total_item_count, selfDelRefId, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "1");
            put("non_serviceable_reason", "7");
        }});

        Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(CustomerLatLng[0], CustomerLatLng[1], selfDelRefId, current_banner)).ResponseValidator;
        String response1 = response.GetBodyAsText();
        int responseCode = response.GetResponseCode();
        String itemList = JsonPath.read(response1, "$.itemList").toString();
        String minSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.minSla").toString().replace("[", "").replace("]", "");
        String maxSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.maxSla").toString().replace("[", "").replace("]", "");


        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(responseCode, 200);
        Assert.assertEquals(itemList, "[]");
        Assert.assertEquals(minSla, maxSla);
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "1", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "7", "non_serviceable_reason");
    }
    //PENDING..
    @Test(groups = "rashmi", priority = 1, description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
    public void selfDeliveryUnServiceableDueToRainModeCheckCart()
    {
       /* LinkedHashMap<String, String> itemMap = clusterHelper.selfDelItemAttributes();
        itemMap.put("selfDeliveryOverriden", "0");
        itemMap.put("deliveryPartnerActive", "1");
        itemMap.put("serviceable", "1");
        itemMap.put("partnerId", "12");*/

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);

        String current_banner = "10";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "10");

        String clusterName = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + restClusterId + "' and tags='restaurant'").get("name").toString();
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "heavy");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "heavy", "restaurant"}, restClusterId);

        String rainModeInZone = clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, "4");
        String connection_id_rainMode = clusterHelper.createConnectionWithDegradationState(connectionGroup, custClusterId, restClusterId, ClusterConstants.last_mile,
                "1", ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, "1",
                ClusterConstants.connection_type, ClusterConstants.is_connection_enabled,
                "heavy").ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");

        /*String[] payload = clusterHelper.getSelfDelItemElements(String.valueOf(clusterHelper.restaurantLat(selfDelRefId)), String.valueOf(clusterHelper.restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
        clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);*/
        Map<String,String> listingMappedResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, new String[]
                {listing_item_count, total_item_count, selfDelRefId, ClusterConstants.area_id, ClusterConstants.cityId, restaurantName, ClusterConstants.item_lat, ClusterConstants.item_lng, "true", CustomerLatLng[0], CustomerLatLng[1]}, new LinkedHashMap<String,String>(){{
            put("serviceable", "1");
            put("non_serviceable_reason", "7");
        }});

        Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(CustomerLatLng[0], CustomerLatLng[1], selfDelRefId, current_banner)).ResponseValidator;
        String response1 = response.GetBodyAsText();
        int responseCode = response.GetResponseCode();
        String itemList = JsonPath.read(response1, "$.itemList").toString();
        String minSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.minSla").toString().replace("[", "").replace("]", "");
        String maxSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.maxSla").toString().replace("[", "").replace("]", "");

        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "normal");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "normal", "restaurant"}, restClusterId);
        clusterHelper.deleteQuery("connections", "id", connection_id_rainMode);
        clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, rainModeInZone);

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(responseCode, 200);
        Assert.assertEquals(itemList, "[]");
        Assert.assertEquals(minSla, maxSla);
        Assert.assertEquals(listingMappedResponse.get("serviceable"), "1", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "7", "non_serviceable_reason");
    }



    //self delivery unserviceable due to rain case is pending..
    //************************************** LISTING WITHOUT CHECK ***************************************************

    @Test(groups = "rashmi", priority=2, description = "CSTC_01 :- Verify items are serviceable when distance is less than the config at connection")
    public void distanceltCheckListingWithoutServiceability() {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        String[] listingPayload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"};

        clusterHelper.deleteClusterCache();

        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
        }});
        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

        Processor itemSearch = clusterHelper.Item_Search(payload);//Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        String distance_check = "", reason = "";
        try {
            String itemResponseAsText = itemSearch.ResponseValidator.GetBodyAsText();
            String itemId = JsonPath.read(itemResponseAsText, "$.itemList[*].item.id")
                    .toString().replace("[", "").replace("]", "");

            distance_check = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(distance_check, "DISTANCE_CHECK");
        Assert.assertEquals(reason, "true");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_02 :- Verify items are not serviceable when distance is greater than the config at connection")
    public void distancegtCheckListingWithoutServiceability() {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        //String[] listingPayload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"};
        double distance = deliveryDataHelper.getDistanceBtwnTwoLatLongs(Double.parseDouble(CustomerLatLng[0]), Double.parseDouble(CustomerLatLng[1]), Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng));
        clusterHelper.connectionUpdateWithDistance(String.valueOf(distance - 500), custClusterId, restClusterId, connectionId);

        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
        }});
        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

        clusterHelper.deleteClusterCache();

        Processor itemSearch = clusterHelper.Item_Search(payload);//Processor response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);
        String item_list = itemSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");
        String ignored_item_list = itemSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ignoredItemList");

        String itemId = "", distance_check = "", reason = "";
        try {
            itemId = JsonPath.read(itemSearch.ResponseValidator.GetBodyAsText(), "$.ignoredItemList[*].item.id")
                    .toString().replace("[", "").replace("]", "");
            distance_check = JsonPath.read(itemSearch.ResponseValidator.GetBodyAsText(), "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemSearch.ResponseValidator.GetBodyAsText(), "$.executionAudit.audits." + itemId.replace("\"", "") + "[0].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        clusterHelper.connectionUpdateWithDistance(ClusterConstants.last_mile, custClusterId, restClusterId, connectionId);
        System.out.println("*********** serviceability" + listingMappedResponse);

        Assert.assertEquals(ignored_item_list, "[]");
        Assert.assertEquals(item_list, "[]");
        //Assert.assertEquals(distance_check, "DISTANCE_CHECK");
        //Assert.assertEquals(reason, "true");
        Assert.assertNull(listingMappedResponse.get("serviceability"), "serviceability");
    }

    @Test(groups = "rashmi", priority=2, description = "CSTC_07 :- Verify items are serviceable when current BF is less than the closeBF config at connection")
    public void bannerltCheckListingWithoutServiceability() {
        String banner = "0";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, banner);
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
            put("banner_factor", banner);
        }});
        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

        clusterHelper.deleteClusterCache();
        Processor itemSearch = clusterHelper.Item_Search(payload);//Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        String distance_check = "", reason = "";
        try {
            String itemResponseAsText = itemSearch.ResponseValidator.GetBodyAsText();
            String itemId = JsonPath.read(itemResponseAsText, "$.itemList[*].item.id")
                    .toString().replace("[", "").replace("]", "");

            distance_check = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(distance_check, "BANNER_CHECK");
        Assert.assertEquals(reason, "true");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority=2, description = "Verify items are not serviceable when current BF is greater than the closeBF config at connection")
    public void bannergtCheckListingWithoutServiceability() {
        String current_banner_factor = "1234";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, current_banner_factor);
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "1");
            put("non_serviceable_reason", "7");
            put("banner_factor", current_banner_factor);
        }});
        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, current_banner_factor, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

        clusterHelper.deleteClusterCache();
        Processor itemSearch = clusterHelper.Item_Search(payload);
        /*Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], current_banner_factor,
                ClusterConstants.sla_extra_time, item_refid);*/
        String distance_check = "", reason = "";
        try{
        String itemResponseAsText = itemSearch.ResponseValidator.GetBodyAsText();
        String itemId = JsonPath.read(itemResponseAsText, "$.ignoredItemList..item.id")
                .toString().replace("[", "").replace("]", "");

            distance_check = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].reason")
                    .toString().replace("[", "").replace("]", "");
            reason = JsonPath.read(itemResponseAsText, "$.executionAudit.audits." + itemId.replace("\"", "") + "[2].included")
                    .toString().replace("[", "").replace("]", "");
        }catch (PathNotFoundException e){e.printStackTrace();}

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(distance_check, "BANNER_CHECK");
        Assert.assertEquals(reason, "false");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "7", "non_serviceable_reason");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_13 :- Verify items are serviceable if sla is less than the max_sla config at connection")
    public void slaltCheckListingWithoutServiceability() throws InterruptedException {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);                                                        //setting max_sla=70..
        List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select filter_group from connections where id='" + connectionId + "'");

        String maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
                .toString().replace("[", "").replace("]", "");
        String item_list = null;
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
        }});
        if (maxSla.equals(ClusterConstants.max_sla)) {
            String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

            clusterHelper.deleteClusterCache();
            item_list = clusterHelper.Item_Search(payload).ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");//item_list = clusterHelper.fetchItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        }
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);


        Assert.assertEquals(maxSla, ClusterConstants.max_sla);
        Assert.assertNotEquals(item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_14 :- Verify items are not serviceable if sla is greater than the max_sla config at connection")
    public void slaCheckGtListingWithoutServiceability() throws InterruptedException {
        String max_sla = "10";

        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        clusterHelper.connectionUpdate(max_sla, custClusterId, restClusterId, connectionId);                                    //setting max-sla=10..
        List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select filter_group from connections where id='" + connectionId + "'");

        String maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
                .toString().replace("[", "").replace("]", "");
        String ignored_item_list = null;
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "0");
            put("non_serviceable_reason", "1");
        }});
        if (maxSla.equals(max_sla)) {
            String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

            clusterHelper.deleteClusterCache();
            ignored_item_list = clusterHelper.Item_Search(payload).ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ignoredItemList");
            /*ignored_item_list = clusterHelper.fetchIgnoredItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1],
                    ClusterConstants.currentBF, ClusterConstants.is_cluster_zone_open, item_refid);*/
        }
        // clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);
        Assert.assertEquals(maxSla, max_sla);
        Assert.assertNotEquals(ignored_item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "0", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }


    @Test(groups = "rashmi", priority=2, description = "CSTC_15 :- Verify item is serviceable/unserviceable depending upon the connections of corresponding degradation state")
    public void rainModegtCheckListingWithoutServiceability() throws InterruptedException
    {
        String max_sla = "10";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        String rainModeInZone = clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, "4");
        String connection_id_rainMode = clusterHelper.createConnectionWithDegradationState(connectionGroup, custClusterId, restClusterId, ClusterConstants.last_mile,
                max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, "1",
                ClusterConstants.connection_type, ClusterConstants.is_connection_enabled,
                "heavy").ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
        //update cluster type to heavy to execute heavy rain mode connection type..
        String clusterName = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + restClusterId + "' and tags='restaurant'").get("name").toString();
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "heavy");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "heavy", "restaurant"}, restClusterId);

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

        clusterHelper.deleteClusterCache();
        Processor itemSearch = clusterHelper.Item_Search(payload);

        //Processor item_check_response = clusterHelper.item_check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        String itemResponseAsText = itemSearch.ResponseValidator.GetBodyAsText();
        String itemId = JsonPath.read(itemResponseAsText, "$.itemList[*].item.id")
                .toString().replace("[", "").replace("]", "");

        List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select filter_group from connections where id='" + connection_id_rainMode + "'");

        String maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
                .toString().replace("[", "").replace("]", "");
        String ignored_item_list = null;
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "0");
            put("non_serviceable_reason", "1");
        }});
        if (maxSla.equals(max_sla)) {
            ignored_item_list = clusterHelper.fetchIgnoredItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1],
                    ClusterConstants.currentBF, ClusterConstants.is_cluster_zone_open, item_refid);
        }
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        //update cluster type to heavy to execute normal rain mode connection type..
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "normal");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "normal", "restaurant"}, restClusterId);
        clusterHelper.deleteQuery("connections", "id", connection_id_rainMode);
        clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, rainModeInZone);

        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);
        Assert.assertEquals(maxSla, max_sla);
        Assert.assertNotEquals(ignored_item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "0", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }



    @Test(groups = "rashmi", priority=2, description = "CSTC_15 :- Verify item is serviceable/unserviceable depending upon the connections of corresponding degradation state")
    public void serviceableDueToHeavyRainModeConnectionListingWithoutServiceability() throws InterruptedException
    {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "0");
        //update cluster type to heavy to execute heavy rain mode connection type..
        String clusterName = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + restClusterId + "' and tags='restaurant'").get("name").toString();
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "heavy");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "heavy", "restaurant"}, restClusterId);

        String rainModeInZone = clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, "4");
        String connection_id_rainMode = clusterHelper.createConnectionWithDegradationState(connectionGroup, custClusterId, restClusterId, ClusterConstants.last_mile,
                ClusterConstants.max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, "1",
                ClusterConstants.connection_type, ClusterConstants.is_connection_enabled,
                "heavy").ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");

        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, item_refid, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
        }});
        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid};

        clusterHelper.deleteClusterCache();
        String item_list = clusterHelper.Item_Search(payload).ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");//String item_list = clusterHelper.fetchItemList_fromItem_Check_response(CustomerLatLng[0], CustomerLatLng[1], item_refid);

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);
        //update cluster type to normal..
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "normal");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "normal", "restaurant"}, restClusterId);
        clusterHelper.deleteQuery("connections", "id", connection_id_rainMode);
        clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, rainModeInZone);

        clusterHelper.connectionUpdate(ClusterConstants.max_sla, custClusterId, restClusterId, connectionId);

        Assert.assertNotEquals(item_list, "[]");
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(groups = "rashmi", priority = 1, description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
    public void selfDeliveryServiceableListingWithoutServiceabilityCheck()
    {
        String current_banner = "0";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, current_banner);

        /*LinkedHashMap<String, String> itemMap = clusterHelper.selfDelItemAttributes();
        itemMap.put("selfDeliveryOverriden", "0");
        itemMap.put("deliveryPartnerActive", "1");
        itemMap.put("serviceable", "1");
        itemMap.put("partnerId", "12");*/

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);

       /* String[] payload = clusterHelper.getSelfDelItemElements(String.valueOf(clusterHelper.restaurantLat(selfDelRefId)), String.valueOf(clusterHelper.restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
        clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);*/
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, selfDelRefId, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "2");
        }});
        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, selfDelRefId};

        clusterHelper.deleteClusterCache();

        Validator response = clusterHelper.Item_Search(payload).ResponseValidator;//Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(CustomerLatLng[0], CustomerLatLng[1], selfDelRefId)).ResponseValidator;
        String response1 = response.GetBodyAsText();
        int responseCode = response.GetResponseCode();
        String itemList = JsonPath.read(response1, "$.itemList").toString();
        String minSla = JsonPath.read(response1, "$.itemList..local_attributes.minSla").toString().replace("[", "").replace("]", "");
        String maxSla = JsonPath.read(response1, "$.itemList..local_attributes.maxSla").toString().replace("[", "").replace("]", "");


        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(responseCode, 200);
        Assert.assertNotEquals(itemList, "[]");
        Assert.assertEquals(minSla, maxSla);
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "2", "serviceability");
    }

    @Test(groups = "rashmi", priority = 1, description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
    public void selfDeliveryUnServiceableDueToBannerListingWithoutServiceabilityCheck()
    {
       /* LinkedHashMap<String, String> itemMap = clusterHelper.selfDelItemAttributes();
        itemMap.put("selfDeliveryOverriden", "0");
        itemMap.put("deliveryPartnerActive", "1");
        itemMap.put("serviceable", "1");
        itemMap.put("partnerId", "12");*/

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);

        String current_banner = "1234";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "10");

        /*String[] payload = clusterHelper.getSelfDelItemElements(String.valueOf(clusterHelper.restaurantLat(selfDelRefId)), String.valueOf(clusterHelper.restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
        clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);*/
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, selfDelRefId, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "1");
            put("non_serviceable_reason", "7");
        }});

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, selfDelRefId};

        clusterHelper.deleteClusterCache();
        Validator response = clusterHelper.Item_Search(payload).ResponseValidator;//Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(CustomerLatLng[0], CustomerLatLng[1], selfDelRefId, current_banner)).ResponseValidator;
        String response1 = response.GetBodyAsText();
        int responseCode = response.GetResponseCode();
        String itemList = JsonPath.read(response1, "$.itemList").toString();
        String ignoredItemList = JsonPath.read(response1, "$.ignoredItemList").toString();
        String minSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.minSla").toString().replace("[", "").replace("]", "");
        String maxSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.maxSla").toString().replace("[", "").replace("]", "");


        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(responseCode, 200);
        Assert.assertEquals(itemList, "[]");
        Assert.assertNotEquals(ignoredItemList, "[]");
        Assert.assertEquals(minSla, maxSla);
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "7", "non_serviceable_reason");
    }


    @Test(groups = "rashmi", priority = 1, description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
    public void selfDeliveryUnServiceableDueToRainModeListingWithoutServiceability()
    {
       /* LinkedHashMap<String, String> itemMap = clusterHelper.selfDelItemAttributes();
        itemMap.put("selfDeliveryOverriden", "0");
        itemMap.put("deliveryPartnerActive", "1");
        itemMap.put("serviceable", "1");
        itemMap.put("partnerId", "12");*/

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);

        String current_banner = "10";
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + ClusterConstants.zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, "10");

        String clusterName = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + restClusterId + "' and tags='restaurant'").get("name").toString();
        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "heavy");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "heavy", "restaurant"}, restClusterId);

        String rainModeInZone = clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, "4");
        String connection_id_rainMode = clusterHelper.createConnectionWithDegradationState(connectionGroup, custClusterId, restClusterId, ClusterConstants.last_mile,
                "1", ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, "1",
                ClusterConstants.connection_type, ClusterConstants.is_connection_enabled,
                "heavy").ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");

        /*String[] payload = clusterHelper.getSelfDelItemElements(String.valueOf(clusterHelper.restaurantLat(selfDelRefId)), String.valueOf(clusterHelper.restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
        clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);*/
        Map<String,String> listingMappedResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.cityId, selfDelRefId, "true"}, new LinkedHashMap<String,String>(){{
            put("serviceability", "1");
            put("non_serviceable_reason", "7");
        }});

        String[] payload = new String[]{CustomerLatLng[0], CustomerLatLng[1], ClusterConstants.zone_id, ClusterConstants.currentBF, ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, selfDelRefId};
        clusterHelper.deleteClusterCache();
        Validator response = clusterHelper.Item_Search(payload).ResponseValidator;//Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(CustomerLatLng[0], CustomerLatLng[1], selfDelRefId, current_banner)).ResponseValidator;
        String response1 = response.GetBodyAsText();
        int responseCode = response.GetResponseCode();
        String itemList = JsonPath.read(response1, "$.itemList").toString();
        String ignoredItemList = JsonPath.read(response1, "$.ignoredItemList").toString();
        String minSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.minSla").toString().replace("[", "").replace("]", "");
        String maxSla = JsonPath.read(response1, "$.ignoredItemList..local_attributes.maxSla").toString().replace("[", "").replace("]", "");

        clusterHelper.updateCluster(clusterName, "restaurant", "degradedState", "normal");//clusterHelper.updateCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", customerClusterName, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_range_width, ClusterConstants.item_count_sla_beefUp, "normal", "restaurant"}, restClusterId);
        clusterHelper.deleteQuery("connections", "id", connection_id_rainMode);
        clusterHelper.updateRainModeInZoneAndZoneRainParams(ClusterConstants.zone_id, rainModeInZone);

        clusterHelper.makeSDserviceable(selfDelItemId, selfDelRefId);
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+ClusterConstants.zone_id, currentBF);

        Assert.assertEquals(responseCode, 200);
        Assert.assertEquals(itemList, "[]");
        Assert.assertNotEquals(ignoredItemList, "[]");
        Assert.assertEquals(minSla, maxSla);
        Assert.assertEquals(listingMappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(listingMappedResponse.get("non_serviceable_reason"), "7", "non_serviceable_reason");
    }


}
