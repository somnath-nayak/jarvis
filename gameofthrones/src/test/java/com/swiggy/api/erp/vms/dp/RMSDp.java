package com.swiggy.api.erp.vms.dp;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.erp.delivery.helper.AlchemistCommands;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceApiHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

public class RMSDp extends RMSCommonHelper {

	SnDHelper sndHelper = new SnDHelper();
	RMSCommonHelper commonhelper = new RMSCommonHelper();
	RMSHelper helper = new RMSHelper();
	CheckoutHelper checkouthelper = new CheckoutHelper();
	OMSHelper omsHelper = new OMSHelper();
	static HashMap<String, String> m = new HashMap<>();
	static HashMap<String, String> m2 = new HashMap<>();

	static String restaurantId = null;
	static String restaurantId1 = null;
	static String TCrestaurantId = null;
	public static String VENDOR_PASSWORD = null;
	public static String VENDOR_USERNAME = null;
	public static String consumerAppPassword = null;
	public static long consumerAppMobile = 0l;
	public static String OMS_USERNAME = null;
	public static String OMS_PASSWORD = null;
	static String OrderId = null;
	static String oldPassword = null;
	static String itemId = null;
	static String variantId = null;
	static String variantGroupId = null;

	static {
		try {
			if (getEnv().equalsIgnoreCase("stage1")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "rkonowhere";
				consumerAppMobile = 7406734416l;
				// restaurantId = "4993";
				restaurantId1 = "4993";
				restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
				oldPassword = "imran@123";
				itemId = "5748192";
				variantId = "1637694";
				variantGroupId = "481253";
				TCrestaurantId = "11691";
			} else if (getEnv().equalsIgnoreCase("stage2")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "9990";
				consumerAppPassword = "rkonowhere";
				consumerAppMobile = 7406734416l;
				restaurantId = "9990";
				restaurantId1 = "4993";
				// restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
				oldPassword = "imran@123";
				itemId = "5748192";
				variantId = "1637694";
				variantGroupId = "481253";
				TCrestaurantId = "11691";
			} else if (getEnv().equalsIgnoreCase("stage3")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "rkonowhere";
				consumerAppMobile = 7406734416l;
				// restaurantId = "9990";
				restaurantId1 = "4993";
				restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
				oldPassword = "imran@123";
				itemId = "5748192";
				variantId = "1637694";
				variantGroupId = "481253";
				TCrestaurantId = "11691";
			} else if (getEnv().equalsIgnoreCase("prod")) {
				VENDOR_PASSWORD = "$w199y@zolb";
				VENDOR_USERNAME = "9738948943";
				consumerAppPassword = "swiggy";
				consumerAppMobile = 7899772315l;
				restaurantId = "9990";
				restaurantId1 = "4993";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
				oldPassword = "imran@1234";
				itemId = "1837858";
				variantId = "12";
				variantGroupId = "9";
				TCrestaurantId = "11691";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@DataProvider(name = "getUserNameAndPassword")
	public static Object[][] getUserNameAndPassword() {

		return new Object[][] { { VENDOR_USERNAME, VENDOR_PASSWORD, RMSConstants.loginValidMessage } };
	}

	@DataProvider(name = "logout")
	public static Object[][] logout() {

		return new Object[][] {
				{ VENDOR_USERNAME, VENDOR_PASSWORD, RMSConstants.successstatusCode, RMSConstants.logoutSuccessMessage }

		};
	}

	@DataProvider(name = "logoutRegression")
	public static Object[][] logoutRegression() {
		RMSHelper helper = new RMSHelper();
		Processor processor = helper.getLogin(VENDOR_USERNAME, VENDOR_PASSWORD);
		if (processor.ResponseValidator.GetNodeValue("statusMessage").equals(RMSConstants.loginValidMessage)) {

			return new Object[][] {
					{ VENDOR_USERNAME, processor.ResponseValidator.GetNodeValue("$.data.access_token"),
							RMSConstants.successstatusCode, RMSConstants.logoutSuccessMessage },
					{ VENDOR_USERNAME, "15b6b641-026c-4fae-80bc-7ccedcc66643",
							RMSConstants.failedstatusCode_invalidSession, RMSConstants.inValidSessionMessage },
					{ "9886379321",  processor.ResponseValidator.GetNodeValue("$.data.access_token"),
								RMSConstants.failedstatusCode_invalidSession, RMSConstants.inValidSessionMessage },
					{ VENDOR_USERNAME,  null,
									RMSConstants.failedstatusCode_invalidSession, RMSConstants.inValidSessionMessage },
					{ null,  processor.ResponseValidator.GetNodeValue("$.data.access_token"),
										RMSConstants.failedstatusCode_invalidSession, RMSConstants.inValidSessionMessage },
					{ "-12345",  processor.ResponseValidator.GetNodeValue("$.data.access_token"),
											RMSConstants.failedstatusCode_invalidSession, RMSConstants.inValidSessionMessage },
							
			};
		} else {
			Assert.fail("login Failed..Cannot Checkout logout now");
		}
		return null;
	}

	@DataProvider(name = "orderPoll")
	public static Object[][] orderPoll() {
		m = RMSCommonHelper.getRMSSessionData(RMSConstants.username, RMSConstants.password1);

		return new Object[][] { { RMSConstants.restaurantId, m.get("accessToken"), RMSConstants.orderPollMessage } };

	}

	@DataProvider(name = "rmslogin")
	public static Object[][] rmslogin() {

		return new Object[][] { { VENDOR_USERNAME, VENDOR_PASSWORD, RMSConstants.loginInValidMessage } };

	}

	@DataProvider(name = "slotWisePrepTime")
	public static Object[][] slotWisePrepTime() {

		return new Object[][] { { RMSConstants.prepTimeRestaurantId, RMSConstants.preptimeMessage } };

	}

	@DataProvider(name = "slotWisePrepTimeID")
	public static Object[][] slotWisePrepTimeID() {

		return new Object[][] {};

	}

	@DataProvider(name = "dayWisePrepTime")
	public static Object[][] dayWisePrepTime() {

		return new Object[][] { { RMSConstants.prepTimeRestaurantId, RMSConstants.preptimeMessage } };

	}

	@DataProvider(name = "multiRestPrepTime")
	public static Object[][] multiRestPrepTime() {

		return new Object[][] { { RMSConstants.prepTimeRestaurantId, RMSConstants.preptimeMessage } };

	}

	@DataProvider(name = "savePrepTime")
	public static Iterator<Object[]> savepreptime() {
		String toDate, fromDate, days;
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		fromDate = dateFormat.format(-1);
		toDate = dateFormat.format(date);
		List<Object[]> obj = new ArrayList<>();
		for (int j = 0; j < RMSConstants.restaurantIds.length; j++) {
			days = "\"" + RMSConstants.days[0] + "\"";
			for (int i = 1; i < RMSConstants.days.length; i++) {
				days += "," + "\"" + RMSConstants.days[i] + "\"";
				obj.add(new Object[] { RMSConstants.restaurantIds[j], fromDate.toString(), toDate.toString(),
						RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), days,
						RMSConstants.preptimeMessage, true });
				obj.add(new Object[] { RMSConstants.restaurantIds[j], fromDate.toString(), toDate.toString(),
						RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days,
						RMSConstants.preptimeMessage, true });
				obj.add(new Object[] { RMSConstants.restaurantIds[j], fromDate.toString(), toDate.toString(),
						RMSConstants.prep_time, RMSConstants.PrepType.TEST.toString(), days, RMSConstants.errorstatus,
						false });
			}
		}
		return obj.iterator();

	}

	@DataProvider(name = "savePrepTimeMulti")
	public static Iterator<Object[]> savePrepTimeMulti() {
		String toDate, fromDate, days, rests;
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		fromDate = dateFormat.format(-1);
		toDate = dateFormat.format(date);
		List<Object[]> obj = new ArrayList<>();
		rests = RMSConstants.restaurantIds[0];
		for (int j = 0; j < RMSConstants.restaurantIds.length; j++) {
			days = "\"" + RMSConstants.days[0] + "\"";
			for (int i = 1; i < RMSConstants.days.length; i++) {
				days += "," + "\"" + RMSConstants.days[i] + "\"";
				obj.add(new Object[] { rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time,
						RMSConstants.PrepType.GLOBAL.toString(), days, RMSConstants.preptimeMessage, true });
				obj.add(new Object[] { rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time,
						RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.preptimeMessage, true });
				obj.add(new Object[] { rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time,
						RMSConstants.PrepType.TEST.toString(), days, RMSConstants.errorstatus, false });
			}
			if (j < RMSConstants.restaurantIds.length - 1)
				rests += "," + RMSConstants.restaurantIds[j + 1];
		}
		return obj.iterator();

	}

	@DataProvider(name = "fetchDayPrepTimeMulti")
	public static Object[][] fetchDayPrepTimeMulti() {

		return new Object[][] { { RMSConstants.prepTimeRestaurantId, RMSConstants.preptimeMessage } };

	}

	@DataProvider(name = "fetchSlotPrepTimeMulti")
	public static Object[][] fetchSlotPrepTimeMulti() {

		return new Object[][] { { RMSConstants.prepTimeRestaurantId, RMSConstants.preptimeMessage } };

	}

	@DataProvider(name = "index")
	public static Object[][] index() {

		return new Object[][] { { RMSConstants.preptimeMessage } };

	}

	@DataProvider(name = "healthCheck")
	public static Object[][] healthCheck() {

		return new Object[][] { { RMSConstants.preptimeMessage } };

	}

	@DataProvider(name = "getRestUsers")
	public Object[][] getRestUsers() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), RMSConstants.rest_id, RMSConstants.restUsersInfoMessage } };

	}

	@DataProvider(name = "getRestUsers1")
	public Object[][] getRestUsers1() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), "0", -3, "Invalid Session" },
				{ m.get("accessToken"), "", -1, "Invalid Input" },
				{ m.get("accessToken"), "-9990", -3, "Invalid Session" },
				{ m.get("accessToken"), "9812646124719274", -3, "Invalid Session" }, };

	}

	@DataProvider(name = "getUserInfo")
	public Object[][] getUserInfo() throws IOException {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), RMSConstants.userInfoMessage } };

	}

	@DataProvider(name = "getUserInfoRegression")
	public Object[][] getUserInfoRegression() throws IOException {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { RMSConstants.invalid_session, "Invalid Session" } };

	}

	@DataProvider(name = "getRestInfo")
	public Object[][] getRestInfo() throws IOException {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), RMSConstants.rest_id, RMSConstants.restInfoMessage } };

	}

	@DataProvider(name = "getRestInfo1")
	public Object[][] getRestInfo1() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), "0", -3, "Invalid Session" },
				{ m.get("accessToken"), "", -1, "Invalid Input" },
				{ m.get("accessToken"), "-9990", -3, "Invalid Session" },
				{ m.get("accessToken"), "9812646124719274", -3, "Invalid Session" }, };

	}

	@DataProvider(name = "createUser")
	public Object[][] createUser() throws IOException {
		String[] permissions5 = new String[] { "1", "2", "3", "4", "5" };
		String[] permissions4 = new String[] { "1", "2", "3", "4" };
		String[] permissions3 = new String[] { "1", "2", "3" };
		String[] permissions2 = new String[] { "1", "2" };
		String[] permissions1 = new String[] { "1" };
		String[] userRest_ids = new String[] { RMSConstants.userRest_id, RMSConstants.userRest_ids };
		String[] phoneNo = new String[10];
		String mobile_no = "3039546901";
		List<String> users = new ArrayList<String>();

		for (int i = 0; i < 10; i++) {
			mobile_no = incrementPhoneNo(mobile_no);
			phoneNo[i] = mobile_no;
			String user_id = helper.getUserByName(RMSConstants.newUser + "-" + phoneNo[i]);
			if(user_id != null && !user_id.isEmpty())
				users.add(user_id);
		}


		for(int i = 0;i<users.size();i++) {
			helper.deleteUserIds(users.get(i));
		}


		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.userRole_id, phoneNo[0], RMSConstants.newUser + "-" + phoneNo[0],
						phoneNo[0], permissions5, new String[] { RMSConstants.userRest_id },
						RMSConstants.createUserMessage },
				{ m.get("accessToken"), RMSConstants.userRole_id, phoneNo[1], RMSConstants.newUser+ "-" + phoneNo[1],
						phoneNo[1], permissions5, userRest_ids, RMSConstants.createUserMessage },
				{ m.get("accessToken"), RMSConstants.userRole_id2, phoneNo[2], RMSConstants.newUser+ "-" + phoneNo[2],
						phoneNo[2], permissions4, new String[] { RMSConstants.userRest_id },
						RMSConstants.createUserMessage },
				{ m.get("accessToken"), RMSConstants.userRole_id, phoneNo[3], RMSConstants.newUser+ "-" + phoneNo[3],
						phoneNo[3], permissions4, userRest_ids, RMSConstants.createUserMessage },
				{ m.get("accessToken"), RMSConstants.userRole_id2, phoneNo[4], RMSConstants.newUser+ "-" + phoneNo[4],
						phoneNo[4], permissions3, new String[] { RMSConstants.userRest_id },
						RMSConstants.createUserMessage },
				{ m.get("accessToken"), RMSConstants.userRole_id, phoneNo[5], RMSConstants.newUser+ "-" + phoneNo[5],
						phoneNo[5], permissions3, userRest_ids, RMSConstants.createUserMessage },
				{ m.get("accessToken"), RMSConstants.userRole_id3, phoneNo[6], RMSConstants.newUser+ "-" + phoneNo[6],
						phoneNo[6], permissions2, new String[] { RMSConstants.userRest_id },
						RMSConstants.createUserMessage },
				{ m.get("accessToken"), RMSConstants.userRole_id, phoneNo[7], RMSConstants.newUser+ "-" + phoneNo[7],
						phoneNo[7], permissions2, userRest_ids, RMSConstants.createUserMessage },
				{ m.get("accessToken"), RMSConstants.userRole_id3, phoneNo[8], RMSConstants.newUser+ "-" + phoneNo[8],
						phoneNo[8] , permissions1, new String[] { RMSConstants.userRest_id },
						RMSConstants.createUserMessage },
				{ m.get("accessToken"), RMSConstants.userRole_id, phoneNo[9], RMSConstants.newUser+ "-" + phoneNo[9],
						phoneNo[9], permissions1, userRest_ids, RMSConstants.createUserMessage }

		};
	}

	@DataProvider(name = "createUser1")
	public Object[][] createUser1() throws IOException {
		String[] permissions5 = new String[] { "1", "2", "3", "4", "5" };
		String[] permissions4 = new String[] { "1", "2", "3", "4" };
		String[] permissions3 = new String[] { "1", "2", "3" };
		String[] permissions2 = new String[] { "1", "2" };
		String[] permissions1 = new String[] { "1" };
		String[] permissions0 = new String[] { "" };
		String[] userRest_ids = new String[] { RMSConstants.userRest_id, RMSConstants.userRest_ids };
		String noRestaurants = "null";
		String[] phoneNo = new String[11];

		for (int i = 0; i < 11; i++) {
			phoneNo[i] = RMSCommonHelper.generatePhoneNo();
		}
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), 0, phoneNo[0], RMSConstants.newUser, phoneNo[0], permissions3,
						userRest_ids, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.userRole_id, phoneNo[1], RMSConstants.newUser,
						phoneNo[1], permissions0, new String[] { RMSConstants.userRest_id }, -1,
						"Unable to create user" },
				{ m.get("accessToken"), RMSConstants.userRole_id, phoneNo[2], RMSConstants.newUser,
						phoneNo[2] , permissions0, userRest_ids, -1, "Unable to create user" },
				{ m.get("accessToken"), RMSConstants.userRole_id2, phoneNo[3], RMSConstants.newUser,
						phoneNo[3], permissions4, new String[] { noRestaurants }, -3,
						"Invalid Session" },
				{ m.get("accessToken"), RMSConstants.userRole_id3, "", RMSConstants.newUser, phoneNo[4],
						permissions2, new String[] { RMSConstants.userRest_id }, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.userRole_id3, "", RMSConstants.newUser, phoneNo[5],
						permissions2, userRest_ids, -1, "Invalid Input" },
				{ m.get("accessToken"), -1, phoneNo[6], RMSConstants.newUser, phoneNo[6], permissions1,
						new String[] { RMSConstants.userRest_id }, -1, "User Details Inserted" },
				{ m.get("accessToken"), -1, phoneNo[7], RMSConstants.newUser, phoneNo[7], permissions1,
						userRest_ids, -1, "User Details Inserted" },
				{ m.get("accessToken"), -1, phoneNo[8], RMSConstants.newUser, phoneNo[8], permissions5,
						new String[] { RMSConstants.userRest_id }, -1, "User Details Inserted" },
				{ m.get("accessToken"), 5, phoneNo[9], RMSConstants.newUser, phoneNo[9], permissions5,
						userRest_ids, -1, "User Details Inserted" },
				{ m.get("accessToken"), 5, phoneNo[10], RMSConstants.newUserNull, phoneNo[10],
						permissions5, userRest_ids, -1, "User Details Inserted" }

		};
	}

	@DataProvider(name = "createUser2")
	public Object[][] createUser2() throws IOException {
		String[] permissions5 = new String[] { "1", "2", "3", "4", "5" };
		String[] permissions4 = new String[] { "1", "2", "3", "4" };
		String[] permissions3 = new String[] { "1", "2", "3" };
		String[] permissions2 = new String[] { "1", "2" };
		String[] permissions1 = new String[] { "1" };
		String[] userRest_ids = new String[] { RMSConstants.userRest_id, RMSConstants.userRest_ids };

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {

				{ m.get("accessToken"), 1, RMSConstants.username_VI, RMSConstants.newUser,
						RMSCommonHelper.generatePhoneNo() + "@email.com", permissions1, userRest_ids, -2,
						"Phone Number already exists" },
				{ m.get("accessToken"), 1, RMSConstants.username_VI, RMSConstants.newUser,
						RMSCommonHelper.generatePhoneNo() + "@email.com", permissions2,
						new String[] { RMSConstants.userRest_id }, -2, "Phone Number already exists" },
				{ m.get("accessToken"), 1, RMSConstants.username_VI, RMSConstants.newUser,
						RMSCommonHelper.generatePhoneNo() + "@email.com", permissions3, userRest_ids, -2,
						"Phone Number already exists" },
				{ m.get("accessToken"), 1, RMSConstants.username_VI, RMSConstants.newUser,
						RMSCommonHelper.generatePhoneNo() + "@email.com", permissions4,
						new String[] { RMSConstants.userRest_id }, -2, "Phone Number already exists" },
				{ m.get("accessToken"), 1, RMSConstants.username_VI, RMSConstants.newUser,
						RMSCommonHelper.generatePhoneNo() + "@email.com", permissions5, userRest_ids, -2,
						"Phone Number already exists" } };
	}

	@DataProvider(name = "updateUser")
	public Object[][] updateUser() throws IOException {
		String[] permissions5 = new String[] { "1", "2", "3", "4", "5" };
		String[] permissions4 = new String[] { "1", "2", "3", "4" };
		String[] permissions3 = new String[] { "1", "2", "3" };
		String[] permissions2 = new String[] { "1", "2" };
		String[] permissions1 = new String[] { "1" };
		String[] userRest_ids = new String[] { RMSConstants.userRest_id, RMSConstants.userRest_ids };

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.updatedRole_id, RMSConstants.role_name, RMSConstants.userPhone_no,
						RMSConstants.updatedEmail, RMSConstants.updatedName, permissions5,
						new String[] { RMSConstants.userRest_id }, RMSConstants.userRole_id, RMSConstants.userEmail,
						permissions4, new String[] { RMSConstants.userRest_id }, RMSConstants.registration_complete,
						RMSConstants.isActive, RMSConstants.updateUserMessage },
				{ m.get("accessToken"), RMSConstants.updatedRole_id1, RMSConstants.role_name1,
						RMSConstants.userPhone_no, RMSConstants.updatedEmail1, RMSConstants.updatedName1, permissions4,
						userRest_ids, RMSConstants.userRole_id, RMSConstants.userEmail, permissions3,
						new String[] { RMSConstants.userRest_id }, RMSConstants.registration_complete,
						RMSConstants.isActive, RMSConstants.updateUserMessage },
				{ m.get("accessToken"), RMSConstants.updatedRole_id, RMSConstants.role_name, RMSConstants.userPhone_no,
						RMSConstants.updatedEmail, RMSConstants.updatedName, permissions3,
						new String[] { RMSConstants.userRest_id }, RMSConstants.userRole_id, RMSConstants.userEmail,
						permissions2, new String[] { RMSConstants.userRest_id }, RMSConstants.registration_complete,
						RMSConstants.isActive, RMSConstants.updateUserMessage },
				{ m.get("accessToken"), RMSConstants.updatedRole_id1, RMSConstants.role_name1,
						RMSConstants.userPhone_no, RMSConstants.updatedEmail1, RMSConstants.updatedName1, permissions2,
						new String[] { RMSConstants.userRest_id }, RMSConstants.userRole_id, RMSConstants.userEmail,
						permissions1, new String[] { RMSConstants.userRest_id }, RMSConstants.registration_complete,
						RMSConstants.isActive, RMSConstants.updateUserMessage },
				{ m.get("accessToken"), RMSConstants.updatedRole_id, RMSConstants.role_name, RMSConstants.userPhone_no,
						RMSConstants.updatedEmail, RMSConstants.updatedName, permissions1, userRest_ids,
						RMSConstants.userRole_id, RMSConstants.userEmail, permissions1,
						new String[] { RMSConstants.userRest_id }, RMSConstants.registration_complete,
						RMSConstants.isActive, RMSConstants.updateUserMessage }, };

	}

	@DataProvider(name = "updateUser1")
	public Object[][] updateUser1() throws IOException {
		String[] permissions = new String[] { "1", "2", "3", "4", "5" };
		String[] noPermissions = new String[] { "null" };
		String noRestaurants = "null";
		String[] userRest_ids = new String[] { RMSConstants.userRest_id, RMSConstants.userRest_ids };

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), 0, RMSConstants.role_name, RMSConstants.userPhone_no, RMSConstants.updatedEmail,
						RMSConstants.updatedName, permissions, new String[] { RMSConstants.userRest_id },
						RMSConstants.userRole_id, RMSConstants.userEmail, permissions,
						new String[] { RMSConstants.userRest_id }, RMSConstants.registration_complete,
						RMSConstants.isActive, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.updatedRole_id, RMSConstants.role_name, RMSConstants.userPhone_no,
						RMSConstants.updatedEmail, RMSConstants.updatedName, permissions,
						new String[] { noRestaurants }, RMSConstants.userRole_id, RMSConstants.userEmail, permissions,
						new String[] { RMSConstants.userRest_id }, RMSConstants.registration_complete,
						RMSConstants.isActive, -1, "Not Authorized" },
				{ m.get("accessToken"), RMSConstants.updatedRole_id, RMSConstants.role_name, RMSConstants.userPhone_no,
						RMSConstants.updatedEmail, RMSConstants.updatedName, permissions,
						new String[] { RMSConstants.userRest_id }, 0, RMSConstants.userEmail, permissions,
						new String[] { RMSConstants.userRest_id }, RMSConstants.registration_complete,
						RMSConstants.isActive, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.updatedRole_id, RMSConstants.role_name, RMSConstants.userPhone_no,
						RMSConstants.updatedEmail, RMSConstants.updatedName, noPermissions, userRest_ids,
						RMSConstants.userRole_id, RMSConstants.userEmail, permissions,
						new String[] { RMSConstants.userRest_id }, RMSConstants.registration_complete,
						RMSConstants.isActive, -1, "Unable to update users" }

		};

	}

	@DataProvider(name = "disableEnableUser")
	public Object[][] disableEnableUser() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.userPhone_no, RMSConstants.is_disable,
						RMSConstants.disableUserMessage },
				{ m.get("accessToken"), RMSConstants.userPhone_no, RMSConstants.is_enable,
						RMSConstants.enableUserMessage } };

	}

	@DataProvider(name = "disableEnableUserRegression")
	public Object[][] disableEnableUserRegression() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { {m.get("accessToken"), "8143200438", RMSConstants.is_disable, 0, "User disabled", "[0,[]]"},
				{m.get("accessToken"), "8143200438", RMSConstants.is_enable, 0, "User enabled","can enable"}
		};

	}

	@DataProvider(name = "fetchItems")
	public Object[][] fetchItems() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), RMSConstants.rest_id, RMSConstants.fetchItemsMessage, true},
								{m.get("accessToken"), RMSConstants.invalid_userRest_id, RMSConstants.inValidSessionMessage, false}
		};

	}

	@DataProvider(name = "fetchItems1")
	public Object[][] fetchItems1() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), "0", -3, "Invalid Session" },
				{ m.get("accessToken"), "", -1, "Invalid Input" },
				{ m.get("accessToken"), "-9990", -3, "Invalid Session" },
				{ m.get("accessToken"), "9812646124719274", -3, "Invalid Session" }, };
	}

	@DataProvider(name = "itemOOS")
	public Object[][] itemOOS() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		Processor p = helper.fetchItems(m.get("accessToken"), VENDOR_USERNAME);
		List<Integer> items = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories..menu[?(@.inStock == 1)].id");
		String item_id = Integer.toString(items.get(0));

		return new Object[][] { { m.get("accessToken"), RMSConstants.markOOStock, item_id,
				VENDOR_USERNAME, RMSConstants.hours, RMSConstants.itemOOSMessage } };

	}

	@DataProvider(name = "itemOOS1")
	public Object[][] itemOOS1() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.markOOStock, "null", RMSConstants.rest_id, "2", -1,
						"Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.itemIdOos, VENDOR_USERNAME, "0", -1,
						"Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.itemIdOos, VENDOR_USERNAME, "-2",
						-1, "Action Failed" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.itemIdOos, "-9990", RMSConstants.hours,
						-3, "Invalid Session" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.itemIdOos, "0", RMSConstants.hours, -1,
						"Invalid Input" } };

	}

	@DataProvider(name = "itemOOSV2")
	public Object[][] itemOOSV2() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		Processor p = helper.fetchItems(m.get("accessToken"), VENDOR_USERNAME);
		List<Integer> items = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories..menu[?(@.inStock == 1)].id");
		String item_id = Integer.toString(items.get(0));

		return new Object[][] { { m.get("accessToken"), RMSConstants.markOOStock, item_id,
				VENDOR_USERNAME, RMSConstants.hours, RMSConstants.itemOOSMessage } };

	}

	@DataProvider(name = "itemOOS1V2")
	public Object[][] itemOOS1V2() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.markOOStock, "null", RMSConstants.rest_id, "2", -1,
						"Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.itemIdOos, VENDOR_USERNAME, "0", -1,
						"Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.itemIdOos, VENDOR_USERNAME, "-2",
						-1, "Done Successfully" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.itemIdOos, "-9990", RMSConstants.hours,
						-3, "Invalid Session" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.itemIdOos, "0", RMSConstants.hours, -1,
						"Invalid Input" } };

	}

	@DataProvider(name = "itemInstock")
	public Object[][] itemInstock() throws Exception {

		String item_id = "123";
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		Processor p = helper.fetchItems(m.get("accessToken"), VENDOR_USERNAME);
		List<Integer> items = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories..menu[?(@.inStock == 0)].id");
		if(items.size()!=0)
			item_id = Integer.toString(items.get(0));

		return new Object[][] {
				{ m.get("accessToken"), "1", item_id, restaurantId, "2", RMSConstants.itemInstockMessage } };
		// {{RMSConstants.markInStock, RMSConstants.itemIdOos, RMSConstants.rest_id,
		// RMSConstants.hours, RMS Constants.itemInstockMessage}};

	}

	@DataProvider(name = "itemInstock1")
	public Object[][] itemInstock1() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.markInStock, "null", RMSConstants.rest_id, "2", -1,
						"Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markInStock, RMSConstants.itemIdOos, "-9990", RMSConstants.hours,
						-3, "Invalid Session" },
				{ m.get("accessToken"), RMSConstants.markInStock, RMSConstants.itemIdOos, "0", RMSConstants.hours, -1,
						"Invalid Input" } };

	}

	@DataProvider(name = "variantOOS")
	public Object[][] variantOOS() throws Exception {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		Processor p = helper.fetchItems(m.get("accessToken"), VENDOR_USERNAME);
		HashMap<String, String> list = RMSCommonHelper.getInStockVariantFromFetchItems(p);
		String item_id = list.get("itemId");
		String variantgroup_id = list.get("variantGroupId");
		String variant_id = list.get("variantId");

		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.markOOStock, item_id, variant_id,
						variantgroup_id, VENDOR_USERNAME, RMSConstants.variantOOSMessage } };

	}

	@DataProvider(name = "variantOOS1")
	public Object[][] variantOOS1() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, "-1728775",
						RMSConstants.variantGroupId, VENDOR_USERNAME, -2, "Action failed" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, "0",
						RMSConstants.variantGroupId, RMSConstants.rest_id, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, "0", RMSConstants.variantId,
						RMSConstants.variantGroupId, RMSConstants.rest_id, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, "null", RMSConstants.variantId,
						RMSConstants.variantGroupId, RMSConstants.rest_id, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						"-506618", VENDOR_USERNAME, -2, "Action failed" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						"0", VENDOR_USERNAME, -2, "Action failed" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						RMSConstants.variantGroupId, "-30751", -3, "Invalid Session" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						RMSConstants.variantGroupId, "0", -3, "Invalid Input" } };

	}

	@DataProvider(name = "variantInStock")
	public Object[][] variantInStock() throws Exception {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		Processor p = helper.fetchItems(m.get("accessToken"), VENDOR_USERNAME);
		HashMap<String, String> list = RMSCommonHelper.getInStockVariantFromFetchItems(p);
		String item_id = list.get("itemId");
		String variantgroup_id = list.get("variantGroupId");
		String variant_id = list.get("variantId");

		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.markInStock, item_id, variant_id,
						variantgroup_id, VENDOR_USERNAME, RMSConstants.variantInStockSMessage }};

	}

	@DataProvider(name = "variantInStock1")
	public Object[][] variantInStock1() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.markInStock, RMSConstants.variantItemIdOos, "-1728775",
						RMSConstants.variantGroupId, VENDOR_USERNAME, -2, "Action failed" },
				{ m.get("accessToken"), RMSConstants.markInStock, RMSConstants.variantItemIdOos, "0",
						RMSConstants.variantGroupId, RMSConstants.rest_id, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markInStock, "0", RMSConstants.variantId,
						RMSConstants.variantGroupId, RMSConstants.rest_id, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markInStock, "null", RMSConstants.variantId,
						RMSConstants.variantGroupId, RMSConstants.rest_id, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markInStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						"-506618", VENDOR_USERNAME, -2, "Action failed" },
				{ m.get("accessToken"), RMSConstants.markInStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						"0", VENDOR_USERNAME, -2, "Action failed" },
				{ m.get("accessToken"), RMSConstants.markInStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						RMSConstants.variantGroupId, "-30751", -3, "Invalid Session" },
				{ m.get("accessToken"), RMSConstants.markInStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						RMSConstants.variantGroupId, "0", -3, "Invalid Input" } };
	}

	@DataProvider(name = "variantOOSV2")
	public Object[][] variantOOSV2() throws Exception {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		Processor p = helper.fetchItems(m.get("accessToken"), VENDOR_USERNAME);
		HashMap<String, String> list = RMSCommonHelper.getInStockVariantFromFetchItems(p);
		String item_id = list.get("itemId");
		String variantgroup_id = list.get("variantGroupId");
		String variant_id = list.get("variantId");

		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.markOOStock, item_id, variant_id,
						variantgroup_id, VENDOR_USERNAME, RMSConstants.variantOOSMessage } };

	}

	@DataProvider(name = "variantOOS1V2")
	public Object[][] variantOOS1V2() throws IOException {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, "-1728775",
						RMSConstants.variantGroupId, VENDOR_USERNAME, 0, "Done Successfully" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, "0",
						RMSConstants.variantGroupId, RMSConstants.rest_id, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, "0", RMSConstants.variantId,
						RMSConstants.variantGroupId, RMSConstants.rest_id, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, "null", RMSConstants.variantId,
						RMSConstants.variantGroupId, RMSConstants.rest_id, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						"-506618", VENDOR_USERNAME, 0, "Done Successfully" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						"0", VENDOR_USERNAME, 0, "Done Successfully" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						RMSConstants.variantGroupId, "-30751", -3, "Invalid Session" },
				{ m.get("accessToken"), RMSConstants.markOOStock, RMSConstants.variantItemIdOos, RMSConstants.variantId,
						RMSConstants.variantGroupId, "0", -3, "Invalid Input" }
		};

	}

	@DataProvider(name = "getContactReasons_sanity")
	public static Object[][] getContactReasons_sanity() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken") } };
		// "Invoice Request or Payment Issue","Finance - Payment or Invoice Issues"
	}

	@DataProvider(name = "contactUsTicket")
	public static Object[][] contactUsTicket() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

		if (getEnv().equalsIgnoreCase("prod")) {
			return new Object[][] {
					{ m.get("accessToken"), restaurantId, "test", "test", "DE Related Issue", "Bangalore", 0,
							"Freshdesk Ticket Created Successfully" },
					{ m.get("accessToken"), restaurantId1, "test", "test", "DE Related Issue", "Bangalore", 0,
							"Freshdesk Ticket Created Successfully" } };

		}

		return new Object[][] { { m.get("accessToken"), restaurantId, "test", "test", "DE Related Issue", "Bangalore",
				0, "Freshdesk Ticket Created Successfully" } };
		// {m.get("accessToken"),restaurantId1,"test","test","DE Related
		// Issue","Bangalore",0,"Freshdesk Ticket Created Successfully"}};
	}

	@DataProvider(name = "contactUsTicket_regression")
	public static Object[][] contactUsTicket_regression() {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

		return new Object[][] {
				{ m.get("accessToken"), "112", "test", "test", "DE Related Issue", "Bangalore", -3, "Invalid Session" },
				{ m.get("accessToken"), "9990", "test", "test", "test", "Bangalore", -1,
						"Freshdesk Ticket Creation Failed" },
				{ m.get("accessToken"), "112", "test", "test", "", "Bangalore", -3, "Invalid Session" },
				{ m.get("accessToken"), "9990", "test", "test", "", "Bangalore", -1,
						"Freshdesk Ticket Creation Failed" },
				{ m.get("accessToken"), "9990", "test", " ", "DE Related Issue", "Bangalore", 0,
						"Freshdesk Ticket Created Successfully" },
				{ m.get("accessToken"), "9990", " ", "test", "DE Related Issue", "Bangalore", 0,
						"Freshdesk Ticket Created Successfully" },
				{ m.get("accessToken"), "9990", "test", "test", "DE Related Issue", " ", -1,
						"Freshdesk Ticket Creation Failed" } };
	}

	@DataProvider(name = "restauranttoggleonoff")
	public static Object[][] restauranttoggleonoff() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
			return new Object[][] { { m.get("accessToken"), "9990", "false", "VMS", 0, "Done successfully" },
					{ m.get("accessToken"), restaurantId, "true", "VMS", 0, "Done successfully" } };
	}

	@DataProvider(name="restauranttoggleonoff1")
       public static Object[][] restauranttoggleonoff1() throws Exception{
		m=RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
			return new Object[][] {{m.get("accessToken"),restaurantId,"false","VMS", RMSConstants.endDate(2),0,"Done successfully"}};
	}

	@DataProvider(name = "restaurantOnOff_regression")
	public static Object[][] restaurantOnOff_regression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), "112", "false", "VMS", -3, "Invalid Session" },
				{ m.get("accessToken"), "112", "true", "VMS", -3, "Invalid Session" } };
	}

	@DataProvider(name = "getRestaurantHolidaySlots")
	public static Object[][] getRestaurantHolidaySlots() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

			return new Object[][] {
					{ m.get("accessToken"), restaurantId, 0, "Successfully fetched restaurant holiday slots" } };
	}

	@DataProvider(name = "getRestaurantHolidaySlots_regression")
	public static Object[][] getRestaurantHolidaySlots_regression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), "112", -3, "Invalid Session" }};
	}

	@DataProvider(name = "createRestaurantHolidaySlots")
	public static Object[][] createRestaurantHolidaySlots() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

		if (getEnv().equalsIgnoreCase("prod")) {
			return new Object[][] {
					{ m.get("accessToken"), restaurantId, RMSConstants.endDate(2), RMSConstants.isRequestedToClose, 0,
							"Done successfully" },
					
					{ m.get("accessToken"), restaurantId, RMSConstants.endDate(2), RMSConstants.isRequestedToOpen, 0,
					"Done successfully" } };
}
		return new Object[][] {
				{ m.get("accessToken"), restaurantId, RMSConstants.endDate(2), RMSConstants.isRequestedToClose, 0,
						"Done successfully" },
				
				{ m.get("accessToken"), restaurantId, RMSConstants.endDate(2), RMSConstants.isRequestedToOpen, 0,
						"Done successfully" } };

	}

	@DataProvider(name = "createRestaurantHolidaySlots_regression")
	public static Object[][] createRestaurantHolidaySlots_regression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "112", RMSConstants.endDate(2), RMSConstants.isRequestedToClose, -3,"Invalid Session" },
				{ m.get("accessToken"), "113", " ", RMSConstants.endDate(2), -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.rest_id12, RMSConstants.fromTime, " ", -1, "Invalid Input" },
				{ m.get("accessToken"), "112", RMSConstants.isRequestedToClose, " ", -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.rest_id12, " ", " ", -1, "Invalid Input" },
				{ m.get("accessToken"), "112", " ", " ", -1, "Invalid Input" } ,
				{ m.get("accessToken"), "112", RMSConstants.isRequestedToClose,RMSConstants.endDate(2) ,-1,"Invalid Input" }};

	}

	@DataProvider(name = "deleteRestaurantHolidaySlots")
	public static Object[][] deleteRestaurantHolidaySlots() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), restaurantId, RMSConstants.slotId1,
				RMSConstants.createholidayslotValidMessage } };

	}

	@DataProvider(name = "fetchFeatureGate")
	public static Object[][] fetchFeatureGate() throws Exception {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

		if (getEnv().equalsIgnoreCase("prod")) {
			return new Object[][] {
					{ m.get("accessToken"), restaurantId, 0, "Fetched Refresh Feature Gates Successfully" },
					{ m.get("accessToken"), restaurantId1, 0, "Fetched Refresh Feature Gates Successfully" } };

		}
		return new Object[][] {
				{ m.get("accessToken"), restaurantId, 0, "Fetched Refresh Feature Gates Successfully" } };
		// {m.get("accessToken"),restaurantId1,0,"Fetched Refresh Feature Gates
		// Successfully"}};
	}

	@DataProvider(name = "fetchFeatureGate_regression")
	public static Object[][] fetchFeatureGate_regression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), "123", -3, "Invalid Session" } };
	}

	@DataProvider(name = "toggleitemsoos")
	public Object[][] toggleitemsoos() throws Exception {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		Processor p = helper.fetchItems(m.get("accessToken"), VENDOR_USERNAME);
		List<Integer> items = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories..menu[?(@.inStock == 1)].id");
		String item_id = Integer.toString(items.get(0));

		return new Object[][] { { m.get("accessToken"), "0", item_id, restaurantId, "", "", "", "2",
				0, "Done Successfully" } };
		// {"0",instockItems2.get(0).toString(),"35750","","","","2",0,"Done
		// Successfully"}
		// {"0",instockItems.get(0).toString(),"5688","","","","2",0,"Done
		// Successfully"}

	}

	@DataProvider(name = "toggleitemsoos_regression")
	public static Object[][] toggleitemsoos_regression() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

		return new Object[][] {
				{ m.get("accessToken"), "-1", "8819246", restaurantId1, "", "", "", "2", 0, "Done Successfully" },
				{ m.get("accessToken"), "1", "8819246", "123", "", "", "", "2", -3, "Invalid Session" },
				{ m.get("accessToken"), "1", "8819246", "9990", "", "", "", "2", 0, "Done Successfully" },
				{ m.get("accessToken"), "1", "1234", "9990", "", "", "", "2", 0, "Done Successfully" },
				{ m.get("accessToken"), "1 ", "8819246", restaurantId1, "", "", "", "0", 0, "Done Successfully" } };
	}

	@DataProvider(name = "fetch_Broadcast_Info_DP")
	public static Object[][] fetch_Broadcast_Info_DP() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), RMSConstants.FBI_restaurantId, RMSConstants.FBI_statusMessage } };
	}

	@DataProvider(name = "fetch_Broadcast_Info_DP1")
	public static Object[][] fetch_Broadcast_Info_DP1() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "0", -3, "Invalid Session" },
				{ m.get("accessToken"), "-1", -3, "Invalid Session" },
				{ m.get("accessToken"), "8997687996576786879", -3, "Invalid Session" },
				{ m.get("accessToken"), "", -1, "Invalid Input" }
		};
	}

	@DataProvider(name = "restaurant_Tier_mertics_of_Restaurant_DP")
	public static Object[][] restaurant_Tier_mertics_of_Restaurant_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken"), RMSConstants.RTMR_restaurantId, RMSConstants.RTMR_statusMessage } };
	}

	@DataProvider(name = "restaurant_Tier_mertics_of_Restaurant_DP1")
	public static Object[][] restaurant_Tier_mertics_of_Restaurant_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
		        { m.get("accessToken"), "0", -3, "Invalid Session" },
                { m.get("accessToken"), "-1", -3, "Invalid Session" },
				{ m.get("accessToken"), "8997687996576786879", -3, "Invalid Session" },
                { m.get("accessToken"), "", -3, "Invalid Session" },

		};
	}

	@DataProvider(name = "restaurant_Tier_Benifits_For_All_Tiers_DP")
	public static Object[][] restaurantTierBenifitsForAllTiers_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.RTBFA_cityId, RMSConstants.RTBFA_restId, RMSConstants.RTBFA_statusMessage } };
	}

	@DataProvider(name = "restaurant_Tier_Benifits_For_All_Tiers_DP1")
	public static Object[][] restaurantTierBenifitsForAllTiers_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
		        { m.get("accessToken"), "", RMSConstants.RTBFA_restId, -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.RTBFA_cityId, "-1", -3, "Invalid Session" },
				{ m.get("accessToken"), RMSConstants.RTBFA_cityId, "899768888", -3, "Invalid Session" },
				{ m.get("accessToken"), RMSConstants.RTBFA_cityId, "", -1, "Invalid Input" },
				{ m.get("accessToken"), RMSConstants.RTBFA_cityId, "0", -3, "Invalid Session" },
				{ m.get("accessToken"), "", "", -1, "Invalid Input" },
                { m.get("accessToken"), "899768888", "899768888", -3, "Invalid Session" },
				{ m.get("accessToken"), "-1", "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "0", "0", -3, "Invalid Session" },

		};
	}

	@DataProvider(name = "restaurantOnOff")
	public static Object[][] restaurantOnOff() {
		return new Object[][] { { RMSConstants.rest_id11, RMSConstants.isRestaurantOnOff_valid, "Done successfully" },
				{ RMSConstants.restaurantId_Invalid, RMSConstants.isRestaurantOnOff_inValid,
						RMSConstants.inValidSessionMessage },
				{ RMSConstants.rest_id11, RMSConstants.isRestaurantOnOff_inValid, "Done successfully" },
				{ RMSConstants.restaurantId_Invalid, RMSConstants.isRestaurantOnOff_valid,
						RMSConstants.inValidSessionMessage } };
	}

	@DataProvider(name = "registerAndMapUser")
	public Object[][] createNewUser() throws IOException {
		String[] permissions5 = new String[] { "1", "2", "3", "4", "5" };
		String[] permissions4 = new String[] { "1", "2", "3", "4" };
		String[] permissions3 = new String[] { "1", "2", "3" };
		String[] permissions2 = new String[] { "1", "2" };
		String[] permissions1 = new String[] { "1" };
		String[] userRest_ids = new String[] { RMSConstants.userRest_id, RMSConstants.userRest_ids };

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		String phoneNo = RMSCommonHelper.generatePhoneNo();
		return new Object[][] { { m.get("accessToken"), phoneNo, phoneNo, RMSConstants.newUser,
				RMSConstants.newPassword, RMSConstants.confirmPassword, RMSConstants.userRole_id, permissions5,
				new String[] { RMSConstants.FBI_restaurantId }, RMSConstants.createUserMessage, 0,
				RMSConstants.signUpMessage }

		};
	}

	@DataProvider(name="registrationStatus")
	public static Object[][] registrationStatus(){
		return new Object[][] {{RMSConstants.mobileno,RMSConstants.registrationAlreadyRegisteredMessage},
				{RMSConstants.invalidmobileno1,RMSConstants.registrationInValidMessage},
				{RMSConstants.invalidmobileno2,RMSConstants.registrationInValidMessage}};
	}

	@DataProvider(name = "billMaskingDP")
	public static Object[][] billMaskingDP() {

		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] { { m.get("accessToken") } };
	}

	@DataProvider(name = "changePasswordRegression")
	public static Object[][] changePasswordRegression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), VENDOR_USERNAME, oldPassword, oldPassword, -1, "New password should be different from current password."},
				{ m.get("accessToken"), VENDOR_USERNAME, RMSConstants.invalid_session, VENDOR_PASSWORD, -1, "Current password is incorrect"},
				{ RMSConstants.invalid_session, restaurantId, VENDOR_PASSWORD, VENDOR_PASSWORD, -3, "Invalid Session"}
		};
	}

	@DataProvider(name = "getDispositionsDp")
	public Iterator<Object[]> getDispositionsDp() {
		List<Object[]> data = new ArrayList<>();
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		data.add(new Object[]{"1", VENDOR_USERNAME, m.get("accessToken"), true});
		data.add(new Object[]{"1", VENDOR_USERNAME, "123", false});

		return data.iterator();
	}

	@DataProvider(name = "getVariantStatusV2")
	public static Object[][] getVariantStatusV2() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		RMSHelper helper = new RMSHelper();
		Processor p = helper.fetchItems(m.get("accessToken"), restaurantId);
		HashMap<String, String> list = RMSCommonHelper.getItemListFromFetchItemsWithVariants(p);
		String item_id = list.get("itemId");
		String variantgroup_id = list.get("variantGroupId");
		String variant_id = list.get("variantId");
		return new Object[][] {{ m.get("accessToken"), variant_id, variantgroup_id, item_id, restaurantId, "Done Successfully"}};
	}

	@DataProvider(name = "getVariantStatusV2Regression")
	public static Object[][] getVariantStatusV2Regression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ "ygs389u23e23h", variantId, variantGroupId, itemId, restaurantId, "Invalid Session"},
				{ m.get("accessToken"), "", variantGroupId, itemId, restaurantId, "Invalid Input"},
				{ m.get("accessToken"), variantId, "", itemId, restaurantId, "Invalid Input"},
				{ m.get("accessToken"), variantId, variantGroupId, "", restaurantId, "Invalid Input"},
				{ m.get("accessToken"), variantId, variantGroupId, itemId, "", "Invalid Input"},
				{ m.get("accessToken"), "abc", variantGroupId, itemId, restaurantId, "Action failed"},
				{ m.get("accessToken"), variantId, "abc", itemId, restaurantId, "Action failed"}
		};
	}

	@DataProvider(name = "getVariantStatus")
	public static Object[][] getVariantStatus() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		RMSHelper helper = new RMSHelper();
		Processor p = helper.fetchItems(m.get("accessToken"), restaurantId);
		HashMap<String, String> list = RMSCommonHelper.getItemListFromFetchItemsWithVariants(p);
		String item_id = list.get("itemId");
		String variantgroup_id = list.get("variantGroupId");
		String variant_id = list.get("variantId");
		return new Object[][] {{ m.get("accessToken"), variant_id, variantgroup_id, item_id, restaurantId, "Done Successfully"}};
	}

	@DataProvider(name = "getVariantStatusRegression")
	public static Object[][] getVariantStatusRegression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ "ygs389u23e23h", variantId, variantGroupId, itemId, restaurantId, "Invalid Session"},
				{ m.get("accessToken"), "", variantGroupId, itemId, restaurantId, "Invalid Input"},
				{ m.get("accessToken"), variantId, "", itemId, restaurantId, "Invalid Input"},
				{ m.get("accessToken"), variantId, variantGroupId, "", restaurantId, "Invalid Input"},
				{ m.get("accessToken"), variantId, variantGroupId, itemId, "", "Invalid Input"},
				{ m.get("accessToken"), "abc", variantGroupId, itemId, restaurantId, "Action failed"},
				{ m.get("accessToken"), variantId, "abc", itemId, restaurantId, "Action failed"}
		};
	}

	@DataProvider(name = "fetchAlternatives")
	public static Object[][] fetchAlternatives() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

		return new Object[][] {{ m.get("accessToken"), restaurantId, "Successfully fetched alternatives"}};
	}

	@DataProvider(name = "fetchAlternativesRegression")
	public static Object[][] fetchAlternativesRegression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ "yqb783dysqs", itemId, restaurantId, "Invalid Session"},
				{ m.get("accessToken"), "", restaurantId, "Invalid Input"},
				{ m.get("accessToken"), itemId, "", "Invalid Input"},
				{ m.get("accessToken"), "abc", restaurantId, "Successfully fetched alternatives"},
				{ m.get("accessToken"), itemId, "abc", "Invalid Session"}
		};
	}

	@DataProvider(name = "fetchAlternativesV2")
	public static Object[][] fetchAlternativesV2() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), restaurantId, "Successfully fetched items for alternatives"}};
	}

	@DataProvider(name = "fetchAlternativesV2Regression")
	public static Object[][] fetchAlternativesV2Regression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ "yqb783dysqs", restaurantId, "Invalid Session"},
				{ m.get("accessToken"), "", "Invalid Input"}
		};
	}

	@DataProvider(name = "getInvoiceDocument")
	public static Object[][] getInvoiceDocument() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), restaurantId, RMSConstants.startDate(-30),
						RMSConstants.endDate(-1), "invoices fetched Successfully" }};
	}

	@DataProvider(name = "getInvoiceDocumentRegression")
	public static Object[][] getInvoiceDocumentRegression() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "", RMSConstants.startDate(-30),
						RMSConstants.endDate(-1), "Invalid Input" },
				{ m.get("accessToken"), restaurantId, "",
						RMSConstants.endDate(-1), "Invalid Input" },
				{ m.get("accessToken"), restaurantId, RMSConstants.startDate(-30),
						"", "Invalid Input" },
				{ "Yhdhgwe=dgu3", restaurantId, RMSConstants.startDate(-30),
						RMSConstants.endDate(-1), "Invalid Session" },
				{ m.get("accessToken"), restaurantId, RMSConstants.startDate(-30),
						"2018-10", "Get invoices Failed" }

		};
	}

	@DataProvider(name = "getNonPollingRestaurants")
    public static Object[][] getNonPollingRestaurants() {
        return new Object[][] {
                { "1", "Successfully fetched restaurants" },
                { "", "Action Failed" }
        };
    }

    @DataProvider(name = "getTDEnabledCities")
    public static Object[][] getTDEnabledCities() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), restaurantId, "Fetched TD enabled cities" },
                { "", restaurantId, "Invalid Session" },
				{ m.get("accessToken"), "", "Invalid Input" }
        };
    }

	@DataProvider(name = "getRestInfoById")
	public static Object[][] getRestInfoById() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), restaurantId, "Successfully fetched restaurants" }};
	}

	@DataProvider(name = "getRestInfoByIdRegression")
	public static Object[][] getRestInfoByIdRegression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "" , "Invalid Input : Empty restaurant ids" },
				{ "Yhdehwj=whew", restaurantId, "Invalid Session" },
				{ m.get("accessToken"), "-1", "Invalid Session" }
		};
	}

    @DataProvider(name = "getGSTFeatureFlag")
    public static Object[][] getGSTFeatureFlag() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), 1, "Feature flag for gst fetched" },
                { "", -1, "Invalid Input" }
        };
    }

    @DataProvider(name = "getGSTStaticInfo")
    public static Object[][] getGSTStaticInfo() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{ m.get("accessToken"), 1, "Fetched GST Static Info Successfully" }};
    }

    @DataProvider(name = "getGSTDetails")
    public static Object[][] getGSTDetails() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), restaurantId, 1, "GST Details Fetched" },
                //{ m.get("accessToken"), restaurantId1, 1, "GST Details Fetched" },
                //{ "", -1, "Invalid Input" }
        };
    }

	@DataProvider(name="registrationStatusV2")
	public static Object[][] registrationStatusV2(){
		return new Object[][] {
				{RMSConstants.userIdRegistered,RMSConstants.registrationAlreadyRegisteredMessage},
				{RMSConstants.userIdNotRegistered,RMSConstants.registrationSuccessMessage},
				{RMSConstants.invalidmobileno1,RMSConstants.registrationInValidMessage},
				{RMSConstants.invalidmobileno2,RMSConstants.registrationInValidMessage},
				{"",RMSConstants.registrationInvalidInput}
		};
	}

	@DataProvider(name = "getRestaurantInfo")
	public static Object[][] getRestaurantInfo() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), restaurantId, "Successfully fetched restaurants" }};
	}

	@DataProvider(name = "getRestaurantInfoRegression")
	public static Object[][] getRestaurantInfoRegression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "" , "Invalid Input" },
				{ "Yhdehwj=whew", restaurantId, "Invalid Session" },
				{ m.get("accessToken"), "-1", "Invalid Session" }
		};
	}

    @DataProvider(name = "getUsers")
    public static Object[][] getUsers() {

        return new Object[][] {{ "5271", "", "", "", "", "", "User Details Fetched"}};
    }

    @DataProvider(name = "getUsersLimit")
    public static Object[][] getUsersLimit() {

        return new Object[][] {{ "5271", "", "", "", "", "1", "User Details Fetched"}};
    }

    @DataProvider(name = "getUsersbyUserId")
    public static Object[][] getUsersbyUserId() {

        return new Object[][] {{ "", "5271", "", "", "", "", "User Details Fetched"}};
    }

    @DataProvider(name = "getUsersbyEmail")
    public static Object[][] getUsersbyEmail() {

        return new Object[][] {{ "", "", "abhishek.pr@swiggy.in", "", "", "", "User Details Fetched"}};
    }

    @DataProvider(name = "getUsersbyMobile")
    public static Object[][] getUsersbyMobile() {

        return new Object[][] {{ "", "", "", "Abhishek", "", "", "User Details Fetched"}};
    }

    @DataProvider(name = "getUsersbyName")
	public static Object[][] getUsersbyName() {

		return new Object[][] {{ "", "", "", "", "abhishek", "", "User Details Fetched"}};
	}

	@DataProvider(name = "getUserByRest")
	public static Object[][] getUserByRest() {

		return new Object[][] {{ "5271", "", "", "", "User Details Fetched"}};
	}

	@DataProvider(name = "getUserByRestWithRole")
	public static Object[][] getUserByRestWithRole() {

		return new Object[][] {
				{ "5271", "3", "", "", "User Details Fetched"},
				{ "5271", "2", "", "", "User Details Fetched"},
				{ "5271", "1", "", "", "User Details Fetched"},
		};
	}

	@DataProvider(name = "getUserByRestWithPartnerType")
	public static Object[][] getUserByRestWithPartnerType() {

		return new Object[][] {
				{ "5271", "", "3", "", "User Details Fetched"},
				{ "5271", "3", "3", "", "User Details Fetched"},
				{ "5271", "", "2", "", "User Details Fetched"}
		};
	}

	@DataProvider(name = "getUserByRestMultiMapping")
	public static Object[][] getUserByRestMultiMapping() {

		List<String> restList = new ArrayList<>();
		restList.add(0, "5271");
		restList.add(1, "9990");
		restList.add(2, "111");

		return new Object[][] {{ restList, "3", "", "multi", "User Details Fetched"}};
	}

	@DataProvider(name = "getUserByRestSingleMapping")
	public static Object[][] getUserByRestSingleMapping() {

		return new Object[][] {{ "5271", "3", "", "single", "User Details Fetched"}};
	}

    @DataProvider(name = "getDispFromFFRegression")
    public static Object[][] getDispFromFFRegression() {

        return new Object[][] {
                { "", "Invalid Input"},
                { "abc", "Invalid Input"}
        };
    }

	@DataProvider(name = "findPartnerType")
	public static Object[][] findPartnerType() throws Exception {
		m = RMSCommonHelper.getRMSSessionData("5271", VENDOR_PASSWORD);
		String order_id = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] { { m.get("accessToken"), order_id, "5271", 0, "Decision Engine Successful",
				"Decision Engine: No issues found" } };
	}

	@DataProvider(name = "findPartnerType_regression")
	public static Object[][] findPartnerType_regression() {
		m = RMSCommonHelper.getRMSSessionData("5271", VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "112", "9990", 0, "Decision Engine Successful"},
				{ m.get("accessToken"), "123", "897", 0, "Decision Engine Successful"},
				{ m.get("accessToken"), "", "9990", -1, "Invalid Input"},
				{ m.get("accessToken"), "", "876", -1, "Invalid Input"},
				{ m.get("accessToken"), "7740763942", "", -1, "Invalid Input"},
				{ m.get("accessToken"), "123", "", -1, "Invalid Input"},
				{ m.get("accessToken"), "", "", -1, "Invalid Input"}
		};
	}

	@DataProvider(name = "getRoleByID")
	public static Object[][] getRoleByID() {
		return new Object[][] {
				{ "1", "Owner"},
				{ "2", "Store Manager"},
				{ "3", "Order Manager"}
		};
	}

	@DataProvider(name = "updateUserFromROP")
	public static Object[][] updateUserFromROP() {
		List<String> mobile = new LinkedList<>();
		mobile.add("1111111111");
		mobile.add("2222222222");
		mobile.add("3333333333");

		return new Object[][] {{ mobile, "test", "1@email.com", "1452", "1", "true", "NA", "", "script",
				"Processed successfully", "Other Primary Users are present. Action cannot be fulfilled"}};
	}

	@DataProvider(name = "updateUserFromROPRegression")
	public static Object[][] updateUserFromROPRegression() {

		return new Object[][] {
				{ "test", "111111111", "1@email.com", "1452", "1", "true", "NA", "", "", "Bad Request", -2 },
				{ "test", "", "1@email.com", "1452", "1", "true", "NA", "", "", "Bad Request", -2 },
				{ "test", "1111111111", "email", "1452", "1", "true", "NA", "", "", "Bad Request", -2 },
				{ "test", "1111111111", "", "1452", "1", "true", "NA", "", "", "Bad Request", -2 },
				{ "test", "1111111111", "1@email.com", "1452", "5", "true", "NA", "", "", "Role not found", -2 },
				{ "test", "1111111111", "1@email.com", "1452", "\"a\"", "true", "NA", "", "", "Bad Request", -2 },
				{ "test", "1111111111", "1@email.com", "\"\"", "1", "true", "NA", "", "", "Bad Request", -2 },
				{ "test", "1111111111", "1@email.com", "1452", "1", "\"abc\"", "NA", "", "", "Bad Request", -2 },
				{ "test", "1111111111", "1@email.com", "1452", "1", "true", "abc", "", "", "Bad Request", -2 },
				{ "test", "1111111111", "1@email.com", "1452", "1", "true", "replace", "", "", "User to be replaced not found.", -3 }
		};
	}

	@DataProvider(name = "updateUserROPSecondary")
	public static Object[][] updateUserROPSecondary() {

		return new Object[][] {{ "1111111111", "test", "1@email.com", "1452", "1", "false", "NA", "", "", "Processed successfully"}};
	}

	@DataProvider(name = "updateExistingUserROPSecondary")
	public static Object[][] updateExistingUserROPSecondary() {

		return new Object[][] {
				{ "1111111111", "test", "1@email.com", "1452", "1", "false", "add", "", "", "Processed successfully"},
				{ "1111111111", "test", "1@email.com", "1452", "1", "false", "NA", "", "script", "Processed successfully"}
		};
	}

	@DataProvider(name = "updateUserROPPrimaryThenSecondary")
	public static Object[][] updateUserROPPrimaryThenSecondary() {
		List<String> mobile = new LinkedList<>();
		mobile.add("1111111111");
		mobile.add("2222222222");

		return new Object[][] {{ mobile, "test", "1@email.com", "1452", "1", "true", "NA", "", "", "Processed successfully"}};
	}

	@DataProvider(name = "updateSecondaryToPrimaryActionNA")
	public static Object[][] updateSecondaryToPrimaryActionNA() {
		List<String> mobile = new LinkedList<>();
		mobile.add("1111111111");
		mobile.add("2222222222");

		return new Object[][] {{ mobile, "test", "1@email.com", "1452", "1", "true", "NA", "", "",
				"Processed successfully", "Other Primary Users are also present. Please add or replace with some user"}};
	}

	@DataProvider(name = "updateSecondaryToPrimaryActionAdd")
	public static Object[][] updateSecondaryToPrimaryActionAdd() {
		List<String> mobile = new LinkedList<>();
		mobile.add("1111111111");
		mobile.add("2222222222");

		return new Object[][] {{ mobile, "test", "1@email.com", "1452", "1", "true", "add", "", "", "Processed successfully"}};
	}

	@DataProvider(name = "updateUserROPPrimaryExistingPrimaryActionNA")
	public static Object[][] updateUserROPPrimaryExistingPrimaryActionNA() {
		List<String> mobile = new LinkedList<>();
		mobile.add("1111111111");
		mobile.add("2222222222");

		return new Object[][] {{ mobile, "test", "1@email.com", "1452", "1", "true", "NA", "", "",
				"Processed successfully", "Other Primary Users are also present. Please add or replace with some user"}};
	}

	@DataProvider(name = "updateUserROPPrimaryExistingPrimaryActionAdd")
	public static Object[][] updateUserROPPrimaryExistingPrimaryActionAdd() {
		List<String> mobile = new LinkedList<>();
		mobile.add("1111111111");
		mobile.add("2222222222");

		return new Object[][] {{ mobile, "test", "1@email.com", "1452", "1", "true", "add", "", "", "Processed successfully"}};
	}

	@DataProvider(name = "updateUserROPPrimaryExistingPrimaryActionReplace")
	public static Object[][] updateUserROPPrimaryExistingPrimaryActionReplace() {
		List<String> mobile = new LinkedList<>();
		mobile.add("1111111111");
		mobile.add("2222222222");

		return new Object[][] {{ mobile, "test", "1@email.com", "1452", "1", "true", "replace", "", "", "Processed successfully"}};
	}

	@DataProvider(name = "updateUserROPReplaceRegression")
	public static Object[][] updateUserROPReplaceRegression() {

		return new Object[][] {
				{ "1111111111", "test", "1@email.com", "1452", "1", "true", "replace", "", "", "Processed successfully", 0, "1452"},
				{ "1111111111", "test", "1@email.com", "1452", "1", "true", "replace", "", "", "User to be replaced not found.", -3, "5721"},
		};
	}

	@DataProvider(name = "getAllCities")
	public static Object[][] getAllCities() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), restaurantId, 0, "Cities fetched" }};
	}

	@DataProvider(name = "getAllCitiesRegression")
	public static Object[][] getAllCitiesRegression() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "123", -3, "Invalid Session" },
				{ m.get("accessToken"), "", -1, "Invalid Input" },
		};
	}

	@DataProvider(name = "getCities")
	public static Object[][] getCities() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), restaurantId, "3", 0, "City details fetched" }};
	}

	@DataProvider(name = "getCitiesRegression")
	public static Object[][] getCitiesRegression() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "123", "3",-3, "Invalid Session" },
				{ m.get("accessToken"), "", "3",-1, "Invalid Input" },
				{ m.get("accessToken"), restaurantId, "",-4, "Unable to fetch city details" },
		};
	}

	@DataProvider(name = "getTC")
	public static Object[][] getTC() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), TCrestaurantId, 0, "Latest Terms and Conditions fetched" }};
	}

	@DataProvider(name = "getTCRegression")
	public static Object[][] getTCRegression() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "123", -3, "Invalid Session" },
				{ m.get("accessToken"), "", -1, "Invalid Input" }
		};
	}

	@DataProvider(name = "getTCAcceptance")
	public static Object[][] getTCAcceptance() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), TCrestaurantId, 0, "Acceptance fetched" }};
	}

	@DataProvider(name = "getTCAcceptanceRegression")
	public static Object[][] getTCAcceptanceRegression() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "123", -3, "Invalid Session" },
				{ m.get("accessToken"), "", -1, "Invalid Input" }
		};
	}

	@DataProvider(name = "getTCLastAcceptance")
	public static Object[][] getTCLastAcceptance() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), TCrestaurantId, 0, "Last accepted Terms and Conditions fetched" }};
	}

	@DataProvider(name = "getTCLastAcceptanceRegression")
	public static Object[][] getTCLastAcceptanceRegression() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "123", -3, "Invalid Session" },
				{ m.get("accessToken"), "", -1, "Invalid Input" }
		};
	}

	@DataProvider(name = "acceptTC")
	public static Object[][] acceptTC() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), TCrestaurantId, 0, "Accepted"},
				{ m.get("accessToken"), "123", -3, "Invalid Session"},
				{ m.get("accessToken"), "\"\"", -1, "Invalid Input"},
		};
	}

	@DataProvider(name = "enabledWebviewTC")
	public static Object[][] enabledWebviewTC() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "["+TCrestaurantId+"]", 0, "Last accepted Terms and Conditions fetched"},
				{ m.get("accessToken"), "[1]", -4, "Last accepted Terms and Conditions fetched"},
				{ m.get("accessToken"), "[]", -2, "Unable to fetch last accepted Terms and Conditions"},
				{ m.get("accessToken"), "\"\"", -1, "Invalid Input"},

		};
	}

	@DataProvider(name = "acceptWebviewTC")
	public static Object[][] acceptWebviewTC() throws Exception {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ m.get("accessToken"), "["+TCrestaurantId+"]", -4, "Latest Terms and Conditions fetched"},
				{ m.get("accessToken"), "[1]", -4, "Latest Terms and Conditions fetched"},
				{ m.get("accessToken"), "[]", -2, "Unable to fetch latest Terms and Conditions"},
				{ m.get("accessToken"), "\"\"", -1, "Invalid Input"},

		};
	}

	@DataProvider(name = "getMfrReport")
	public static Object[][] getMfrReport() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{  m.get("accessToken"), restaurantId, RMSConstants.fromDate,
						RMSConstants.toDate, 0, RMSConstants.validMessage_rmsMfrReport }
		};

	}

	@DataProvider(name = "getMfrReportRegression")
	public static Object[][] getMfrReportRegression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{  m.get("accessToken"), "9", RMSConstants.fromDate,
						RMSConstants.toDate, -5, RMSConstants.invalidRestMessage_mfrReport },
				{  m.get("accessToken"), restaurantId, RMSConstants.fromDate,
						RMSConstants.toDate+1, -2, RMSConstants.invalidTimeMessage_mfrReport },
				{  m.get("accessToken"), "-99", RMSConstants.fromDate,
						RMSConstants.toDate+1, -1, RMSConstants.invalidRest3 },
				{  m.get("accessToken"), "abc", RMSConstants.fromDate,
						RMSConstants.toDate+1, -1, RMSConstants.invalidRest3 },
				{  m.get("accessToken"), "%20", RMSConstants.fromDate,
						RMSConstants.toDate+1, -1, RMSConstants.invalidRest3 },
				{  m.get("accessToken"), "@$", RMSConstants.fromDate,
						RMSConstants.toDate+1, -1, RMSConstants.invalidRest3}
		};

	}


}
