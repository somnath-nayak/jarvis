package com.swiggy.api.erp.ff.POJO.WatchDog;
public class Body
{
    private String workflowId;

    private String order_id;

    public String getWorkflowId ()
    {
        return workflowId;
    }

    public void setWorkflowId (String workflowId)
    {
        this.workflowId = workflowId;
    }

    public String getOrder_id ()
    {
        return order_id;
    }

    public void setOrder_id (String order_id)
    {
        this.order_id = order_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [workflowId = "+workflowId+", order_id = "+order_id+"]";
    }
}
