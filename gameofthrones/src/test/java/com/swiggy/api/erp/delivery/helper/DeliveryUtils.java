package com.swiggy.api.erp.delivery.helper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFRow.CellIterator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * @author preetesh
 * @project swiggy_test
 * @createdon 19/09/18
 */
public class DeliveryUtils {

    public static final String filepath="../Data/Payloads/JSON/alchemistFuncTestDataSheet.xlsx";
    // System.out.println(path);

    public static LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> readExcelInMap(String filepath) {
        FileInputStream fs = null;
        XSSFWorkbook workbook = null;
        XSSFSheet sheet = null;
        int sheetcount = 0;
        LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> baapMap = new LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>>();

        try {
            fs = new FileInputStream(new File(filepath));
            workbook = new XSSFWorkbook(fs);
            int numsheets = workbook.getNumberOfSheets();

            for (int i = 0; i < numsheets; i++) {
                sheet = workbook.getSheetAt(i);
                String sheetname = sheet.getSheetName();
                //int rownum=sheet.getLastRowNum();
                Iterator<Row> rowIt = sheet.rowIterator();
                Row firstrow = rowIt.next();
                List<String> firstrowlist = new ArrayList<String>();
                Iterator<Cell> firstrowcell = firstrow.cellIterator();
                while (firstrowcell.hasNext()) {
                    Cell cell = firstrowcell.next();
                    firstrowlist.add(cell.getStringCellValue());
                }

                LinkedHashMap<String, LinkedHashMap<String, String>> testmap = new LinkedHashMap<String, LinkedHashMap<String, String>>();

                while (rowIt.hasNext()) {
                    LinkedHashMap<String, String> rowmap = new LinkedHashMap<String, String>();
                    Row testrow = rowIt.next();
                    Iterator<Cell> cellIt = testrow.cellIterator();
                    while (cellIt.hasNext()) {
                        Cell testcell = cellIt.next();
                        int colnu = testcell.getColumnIndex();
                        testcell.setCellType(Cell.CELL_TYPE_STRING);
                        String cellvalue = testcell.getStringCellValue();
                        //System.out.println("Cell value is"+cellvalue);
                        rowmap.put(firstrowlist.get(colnu), cellvalue);
                    }
                    testmap.put(rowmap.get(firstrowlist.get(0)), rowmap);

                    for (String str : rowmap.keySet()) {
                        //System.out.println(str+"#############"+rowmap.get(str));
                    }
                }

                baapMap.put(sheetname, testmap);
            }
            //last map goes here
        } catch (Exception e) {
            e.printStackTrace();
        }

        return baapMap;

    }
    public static Object[][] getTestsInSheet(String sheetname) {
        LinkedHashMap<String, LinkedHashMap<String, LinkedHashMap<String, String>>> allTestMap = readExcelInMap(filepath);
        LinkedHashMap<String, LinkedHashMap<String, String>> sheetMap = allTestMap.get(sheetname);
        int numelement = sheetMap.size();
        Object[][] data = new Object[numelement][2];
        int row = 0;
        int col = 0;
        for (String str : sheetMap.keySet()) {
            data[row][col] = str;
            col = col + 1;
            data[row][col] = sheetMap.get(str);
            row = row + 1;
            col = 0;
        }
        return data;
    }

}
