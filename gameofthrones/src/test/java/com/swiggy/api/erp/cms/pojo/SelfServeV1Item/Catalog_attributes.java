package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Catalog_attributes {
	private Quantity quantity;

    private String spice_level;

    private String veg_classifier;

    private String cutlery;

    private String[] accompaniments;

    private String packaging;

    private String prep_style;

    private String serves_how_many;

    public Quantity getQuantity ()
    {
        return quantity;
    }

    public void setQuantity (Quantity quantity)
    {
        this.quantity = quantity;
    }

    public String getSpice_level ()
    {
        return spice_level;
    }

    public void setSpice_level (String spice_level)
    {
        this.spice_level = spice_level;
    }

    public String getVeg_classifier ()
    {
        return veg_classifier;
    }

    public void setVeg_classifier (String veg_classifier)
    {
        this.veg_classifier = veg_classifier;
    }

    public String getCutlery ()
    {
        return cutlery;
    }

    public void setCutlery (String cutlery)
    {
        this.cutlery = cutlery;
    }

    public String[] getAccompaniments ()
    {
        return accompaniments;
    }

    public void setAccompaniments (String[] accompaniments)
    {
        this.accompaniments = accompaniments;
    }

    public String getPackaging ()
    {
        return packaging;
    }

    public void setPackaging (String packaging)
    {
        this.packaging = packaging;
    }

    public String getPrep_style ()
    {
        return prep_style;
    }

    public void setPrep_style (String prep_style)
    {
        this.prep_style = prep_style;
    }

    public String getServes_how_many ()
    {
        return serves_how_many;
    }

    public void setServes_how_many (String serves_how_many)
    {
        this.serves_how_many = serves_how_many;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [quantity = "+quantity+", spice_level = "+spice_level+", veg_classifier = "+veg_classifier+", cutlery = "+cutlery+", accompaniments = "+accompaniments+", packaging = "+packaging+", prep_style = "+prep_style+", serves_how_many = "+serves_how_many+"]";
    }
}
			
			