package com.swiggy.api.erp.finance.dp;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.finance.helper.CashMgmtDBhelper;
import com.swiggy.api.erp.finance.helper.CashMgmtHelper;
import com.swiggy.api.erp.finance.helper.CashMgmtRegressionHelper;
import com.swiggy.api.erp.finance.helper.ZoneHelper;
import com.swiggy.api.erp.finance.pojo.AddBulkTransactionWithErrorReport;
import com.swiggy.api.erp.finance.pojo.AddHubTransaction;
import com.swiggy.api.erp.finance.pojo.BlockUnblockDe;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import io.gatling.core.json.Json;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.dp
 **/
public class CashMgmtRegressionDP {

    CashMgmtRegressionHelper helper = new CashMgmtRegressionHelper();
    DateHelper dateHelper = new DateHelper();
    public static DeliveryDataHelper deliverydatahelper = new DeliveryDataHelper();
    CashMgmtHelper cashMgmtHelper = new CashMgmtHelper();
    CashMgmtDBhelper cashMgmtDBhelper = new CashMgmtDBhelper();
    LOSHelper losHelper = new LOSHelper();

    public static ZoneHelper zoneHelper = new ZoneHelper();
    public static String g_zoneID,g_deID;

    public static String[] createZoneAndDE() throws IOException {
        g_zoneID = zoneHelper.createZone();
        g_deID = deliverydatahelper.CreateDE(1);
        System.out.println("--ZONEID ::: "+g_zoneID);
        System.out.println("--DEID ::: "+g_deID);
        String[] returnVals = {g_zoneID,g_deID};
        System.out.println();
        return returnVals;
    }

    @DataProvider(name = "getFloatingCashDP")
    public Object[][] getFloatingCashDP() {
        List<Map<String,Object>> list;
        list = helper.getDECashSessionDetailsFromDB(g_deID);
        String fc = list.get(0).get("floating_cash").toString();
        String inactiveDE = helper.getInactiveDEId();
        List<Map<String,Object>> list1 = helper.getDECashSessionDetailsFromDB(inactiveDE);
        String fc_inactive_de = list1.get(0).get("floating_cash").toString();
        return new Object[][] {
                {g_deID,fc,"true"},
                {"0",fc,"false"},
                {inactiveDE,fc_inactive_de,"true"}
        };
    }

    @DataProvider(name = "zoneLevelFloatingCashDP")
    public Object[][] zoneLevelFloatingCashDP() {
        List<Map<String,Object>> list;
        list = helper.getZoneLevelFCDeIdFromDB(g_zoneID);
        String zone = g_zoneID;
        Integer fc_total = 0;
        if (list.size() == 0) {
            zone = "2"; // fallback for test
            list = helper.getZoneLevelFCDeIdFromDB(zone);
        }
        System.out.println("--zoneLevelFloatingCash list size ::: "+list.size());
        String[] deID = new String[list.size()];
        for (int i = 0; i <list.size() ; i++) {
            String fc = list.get(i).get("floating_cash").toString();
            fc_total = fc_total + Integer.parseInt(fc);
            deID[i] =  list.get(i).get("de_id").toString();
        }

        return new Object[][] {
                {"0",String.valueOf(fc_total),deID, "false"},
                {zone,String.valueOf(fc_total),deID,"true"},
        };
    }

    @DataProvider(name = "zoneCashOverviewDP")
    public Object[][] zoneCashOverviewDP() {
        List<Map<String,Object>> list;
        String zone = g_zoneID;
        list = helper.getZoneCashOverviewFromDB(zone);
        if (list.size() == 0) {
            zone = "1"; // fallback for test
            list = helper.getZoneCashOverviewFromDB(zone);
        }
        String[] zoneCashID = new String[list.size()];
        String[] openingbalances = new String[list.size()];
        String[] closingbalances = new String[list.size()];
        String[] issuesToHub = new String[list.size()];
        String[] depositByHub = new String[list.size()];
        System.out.println("--zoneCashOverview list size:"+list.size());
        for (int i = 0; i < list.size(); i++) {
            zoneCashID[i] = list.get(i).get("id").toString();
            openingbalances[i] = list.get(i).get("opening_balance").toString();
            closingbalances[i] = list.get(i).get("closing_balance").toString();
            issuesToHub[i] = list.get(i).get("issued_to_hub").toString();
            depositByHub[i] = list.get(i).get("deposit_by_hub").toString();
        }

        return new Object[][] {
                {zone,zoneCashID,openingbalances,closingbalances,issuesToHub,depositByHub,"true"},
                {"0",zoneCashID,openingbalances,closingbalances,issuesToHub,depositByHub,"false"}
        };
    }

    @DataProvider(name = "zoneCashLogDP")
    public Object[][] zoneCashLogDP() {
        List<Map<String,Object>> list;
        list = helper.getZoneCashAuditFromDB(g_zoneID);
        String zone =g_zoneID;
        String[] auditValue = new String[list.size()];
        if (auditValue.length == 0) {
            list = helper.getZoneCashAuditFromDB("1");
            zone = "1"; //fallback for test
            auditValue = new String[list.size()];
            System.out.println("--zoneCashLog zone id ::: 1");
        }
        System.out.println("--zoneCashLog list size :::" + auditValue.length);
        String[] userEmail = new String[list.size()];
        String[] userID = new String[list.size()];
        String[] timestamp = new String[list.size()];
        for (int i = 0; i < list.size() ; i++) {
            auditValue[i] = list.get(i).get("audit_value").toString();
            userEmail[i] = list.get(i).get("user_email").toString();
            userID[i] = list.get(i).get("user_id").toString();
            timestamp[i] = list.get(i).get("created_at").toString();
        }

        return new Object[][] {
                {zone, auditValue,userEmail,userID,timestamp, "true"},
                {"0",auditValue,userEmail,userID,timestamp, "false"}
        };
    }

    @DataProvider(name = "zoneCashHubTransactionDP")
    public Object[][] zoneCashHubTransactionDP() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        AddHubTransaction addHubTransaction = new AddHubTransaction();
        int deposit = 500;
        int issued = 1000;
        addHubTransaction.build(Integer.parseInt(g_zoneID),Integer.parseInt(g_deID),0,deposit,"deposit");
        AddHubTransaction addHubTransaction1 = new AddHubTransaction();
        addHubTransaction1.build(Integer.parseInt(g_zoneID),Integer.parseInt(g_deID),issued,0, "issued");
        AddHubTransaction addHubTransaction2 = new AddHubTransaction();
        addHubTransaction2.build(0,Integer.parseInt(g_deID),0,deposit,"deposit");
        AddHubTransaction addHubTransaction3 = new AddHubTransaction();
        addHubTransaction3.build(Integer.parseInt(g_zoneID),Integer.parseInt(g_deID),issued,0, "invalid");

        return new Object[][] {
                {jsonHelper.getObjectToJSON(addHubTransaction), g_zoneID, String.valueOf(deposit), "deposit","true"},
                {jsonHelper.getObjectToJSON(addHubTransaction1), g_zoneID, String.valueOf(issued), "issued","true"},
                {jsonHelper.getObjectToJSON(addHubTransaction2), "0", String.valueOf(deposit), "deposit","false"},
                {jsonHelper.getObjectToJSON(addHubTransaction3), g_zoneID, String.valueOf(issued), "invalid","false"}
        };
    }

    @DataProvider(name = "reportCityHubDP")
    public Object[][] reportCityHubDP() {
        String zone = g_zoneID;
        String date = dateHelper.getCurrentDateIn_yyyy_mm_dd();
        String cityID = helper.getCityIDFromDB(zone);
        List<Map<String,Object>> all_zones = helper.getAllZonesForCityIDFromDB(cityID);
        if (all_zones.size() == 0) {
            zone = "1"; //fallback
            cityID = helper.getCityIDFromDB(zone);
            all_zones = helper.getAllZonesForCityIDFromDB(cityID);
        }
        return new Object[][] {
                {date,cityID,all_zones,"true"},
               {date,"0",all_zones,"false"}
        };
    }

    @DataProvider(name = "pocketingDP")
    public Object[][] pocketingDP() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        List<Map<String,Object>> list;
        list = helper.getDECashSessionDetailsFromDB(g_deID);
        String fc = list.get(0).get("floating_cash").toString();
        AddBulkTransactionWithErrorReport addBulkTransactionWithErrorReport = new AddBulkTransactionWithErrorReport();
        //String orderID = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderID = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
        String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
        losHelper.createManualOrder(orderID,orderTime,epochTime);
        addBulkTransactionWithErrorReport.build(Integer.parseInt(g_deID),100,0,orderID,"pocket_tx");
        AddBulkTransactionWithErrorReport addBulkTransactionWithErrorReport1 = new AddBulkTransactionWithErrorReport();
        addBulkTransactionWithErrorReport1.build(Integer.parseInt("0"),100,0,orderID,"pocket_tx");
        return new Object[][] {
                {jsonHelper.getObjectToJSON(addBulkTransactionWithErrorReport),g_deID,"100","0", orderID, fc, "true"},
                {jsonHelper.getObjectToJSON(addBulkTransactionWithErrorReport1),"0","100","0", orderID, fc, "false"},
        };
    }

    @DataProvider(name = "dePayoutAdjustmentDP")
    public Object[][] dePayoutAdjustmentDP() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        List<Map<String,Object>> list;
        list = helper.getDECashSessionDetailsFromDB(g_deID);
        String fc = list.get(0).get("floating_cash").toString();
        AddBulkTransactionWithErrorReport addBulkTransactionWithErrorReport = new AddBulkTransactionWithErrorReport();
        String orderID = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        addBulkTransactionWithErrorReport.build(Integer.parseInt(g_deID),0,100,orderID,"payout_adjustment_tx");
        AddBulkTransactionWithErrorReport addBulkTransactionWithErrorReport1 = new AddBulkTransactionWithErrorReport();
        addBulkTransactionWithErrorReport1.build(Integer.parseInt("0"),0,100,orderID,"payout_adjustment_tx");
        return new Object[][] {
                {jsonHelper.getObjectToJSON(addBulkTransactionWithErrorReport),g_deID,"0","100", fc, "true"},
                {jsonHelper.getObjectToJSON(addBulkTransactionWithErrorReport1),"0","0","100", fc, "false"},
        };
    }

    @DataProvider(name = "blockUnblockDeDP")
    public Object[][] blockUnblockDeDP() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        Integer[] deID = {Integer.parseInt(g_deID)};
        Integer[] invalidDE = {0};
        BlockUnblockDe blockUnblockDe = new BlockUnblockDe();
        blockUnblockDe.build(deID);
        BlockUnblockDe blockUnblockDe1 = new BlockUnblockDe();
        blockUnblockDe1.build(invalidDE);
        String fc_limit = helper.getFloatingCashDetailsHelper(g_deID).ResponseValidator.GetNodeValue("$.data.floatingCashLimit");
        System.out.println("--FLOATING CASH LIMIT ::: "+fc_limit);
        return new Object[][] {
                {jsonHelper.getObjectToJSON(blockUnblockDe),g_deID,fc_limit,"fc less-than equal","block","true"},
                {jsonHelper.getObjectToJSON(blockUnblockDe),g_deID,fc_limit,"fc less-than equal","unblock","true"},
                {jsonHelper.getObjectToJSON(blockUnblockDe),g_deID,fc_limit,"fc greater-than","block","true"},
                {jsonHelper.getObjectToJSON(blockUnblockDe),g_deID,fc_limit,"fc greater-than","unblock","true"},
                {jsonHelper.getObjectToJSON(blockUnblockDe1),"0",fc_limit,"invalid de","block","false"},
                {jsonHelper.getObjectToJSON(blockUnblockDe1),"0",fc_limit,"invalid de","unblock","false"},

        };
    }

    @DataProvider(name = "closeSessionDP")
    public Object[][] closeSessionDP() {
        return new Object[][] {
                {"0", "false"},
                {g_deID, "true"}
        };
    }

    @DataProvider(name = "dynamicFloatingCashDP")
    public Object[][] dynamicFloatingCashDP() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        String order1 = helper.createManualOrderAndGetOrderId();
        String order2 =  helper.createManualOrderAndGetOrderId();
        String deID1 = g_deID;
        String zoneID1 = g_zoneID;
        String zoneID2 = "1";
       String deID2 = deliverydatahelper.CreateDE(Integer.parseInt(zoneID2));
       Processor p1 = cashMgmtHelper.createSessionHelper(deID2);
       int addTxnStatusCode = p1.ResponseValidator.GetResponseCode();
       if (!(addTxnStatusCode == 200)) {
           System.out.println("--Retrying Create Session for DEID ::: "+deID2);
           p1 = cashMgmtHelper.createSessionHelper(deID2);
       }
        Processor p = cashMgmtHelper.addTransactionHelper(deID2,"Adding txn for dynamicFCLimit",order2,"100","0","disburse_cash_tx");
        int get_addtxn_responseCode = p.ResponseValidator.GetResponseCode();
        if((get_addtxn_responseCode == 200)) {
            System.out.println("--Retrying add txn for DEID ::: "+deID2);
            p = cashMgmtHelper.addTransactionHelper(deID2,"Adding txn for dynamicFCLimit",String.valueOf(dateHelper.getCurrentEpochTimeInSeconds()),"100","0","disburse_cash_tx");
        }
        String sessionID = "";
        try {
            sessionID = cashMgmtDBhelper.getSessionIdFromDB(deID2).toString();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //String deID2 ="1";
        //int validateResponse_getSessionDetails = cashMgmtHelper.getSessionDetailHelper(sessionID,deID2,500,0).ResponseValidator.GetResponseCode();

        String fromTime = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(-2);
        String toTime = dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss();
        if (!order1.equalsIgnoreCase("FAILED") && !order2.equalsIgnoreCase("FAILED")) {
            HashMap<String, String> hm1 = helper.getTimeStampsForTripsTable(-3);
            HashMap<String, String> hm2 = helper.getTimeStampsForTripsTable(-1);
            helper.updateTimestampsAndDeIdInDeliveryTripsTable(hm1,order1,deID1);
            helper.updateTimestampsAndDeIdInDeliveryTripsTable(hm2,order2,deID2);
        } else {
            HashMap<String, String> hm1 = helper.getTimeStampsForTripsTable(-3);
            HashMap<String, String> hm2 = helper.getTimeStampsForTripsTable(-1);
            helper.updateTimestampsAndDeIdInDeliveryTripsTable(hm1,order1,deID1);
            helper.updateTimestampsAndDeIdInDeliveryTripsTable(hm2,order2,deID2);
        }
        return new Object[][] {
                {fromTime,toTime,deID1,zoneID1, "0"},
                {fromTime,toTime,deID2,zoneID2, "1"}
        };
    }



 }
