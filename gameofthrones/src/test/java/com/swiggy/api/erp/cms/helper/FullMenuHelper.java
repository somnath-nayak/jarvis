package com.swiggy.api.erp.cms.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.FullMenu.*;
import com.swiggy.api.erp.cms.pojo.VariantGroups.VariantGroup;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.automation.api.rest.RestTestUtil;
import edu.emory.mathcs.backport.java.util.Arrays;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.codehaus.jackson.JsonNode;

import java.io.IOException;
import java.time.Instant;
import java.util.*;

/**
 * Created by kiran.j on 2/2/18.
 */
public class FullMenuHelper {

    Initialize gameofthrones = new Initialize();
    private static HashMap<String, List<String>> variantGroupIds = new HashMap<>();
    private static HashMap<String, List<String>> addonGroupIds = new HashMap<>();
    private static HashMap<String, String> token_ids = new HashMap<>();
    CMSHelper cmsHelper = new CMSHelper();
    static {
        setTokens();
    }

    public static void setTokens() {
        if(token_ids.size() == 0) {
            CMSHelper cmsHelper = new CMSHelper();
            String[] rests = MenuConstants.rests;
            for (String rest : rests) {
                token_ids.put(rest, cmsHelper.getTokenForRestaurant(rest));
            }
        }
    }

    public static HashMap<String, String> getToken_ids() {
        return token_ids;
    }

    public static void setToken_ids(HashMap<String, String> token_ids) {
        FullMenuHelper.token_ids = token_ids;
    }

    public FullMenuPojo getFullMenuObject(Entity entity) {
        FullMenuPojo fullMenuPojo = new FullMenuPojo();
        fullMenuPojo.setEntity(entity);
        return fullMenuPojo;
    }

    public Entity getEntity(Items items[], Main_categories main_categories[]) {
        Entity entity = new Entity();
        entity.setItems(items);
        entity.setMain_categories(main_categories);
        return entity;
    }

    public Processor createFullMenu(String rest_id, String fullMenuPojo) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", MenuConstants.token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","addfullmenu", gameofthrones);
        return new Processor(service, headers, new String[]{fullMenuPojo.toString()}, new String[] {rest_id});
    }

    public Processor createVariantGroup(String rest_id, String variantgrouppojo) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", MenuConstants.token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","addvariantgroup", gameofthrones);
        return new Processor(service, headers, new String[]{variantgrouppojo}, new String[] {rest_id});
    }

    public Processor updateVariantGroup(String rest_id, VariantGroup variantgrouppojo) throws IOException {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", MenuConstants.token_id);
        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updatevariantgroup", gameofthrones);
        return new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(variantgrouppojo)}, new String[] {rest_id, variantgrouppojo.getEntity().getId()});
    }

    public Processor createMainCategory(String rest_id, String json, String token_id) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","addmaincategory", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateMainCategory(String rest_id, String json, String token_id, String cat_id) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updatemaincategory", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id, cat_id});
    }

    public Processor createSubCategory(String rest_id, String json, String token_id) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","addsubcategory", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateSubCategory(String rest_id, String json, String token_id, String cat_id) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updatesubcategory", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id, cat_id});
    }

    public Processor createBulkMainCategory(String rest_id, String json, String token_id) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","addbulkmaincategory", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateBulkMainCategory(String rest_id, String json, String token_id, String cat_id) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updatebulkmaincategory", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id, cat_id});
    }

    public Processor bulkCreateVariantGroup(String rest_id, String json, String token_id) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","bulkaddvariantgroup", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor bulkUpdateVariantGroup(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","bulkupdatevariantgroup", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor createVariant(String rest_id, String json, String token_id) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","addvariant", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateVariant(String rest_id, String json, String token_id, String id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updatevariant", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id, id});
    }

    public Processor createAddonGroup(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","createaddongroup", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateAddonGroup(String rest_id, String json, String token_id, String id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updateaddongroup", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id, id});
    }

    public Processor createBulkAddonGroup(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","bulkcreateaddongroup", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateBulkAddonGroup(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","bulkupdateaddongroup", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor createBulkVariant(String rest_id, String json, String token_id) {
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","bulkaddvariant", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateBulkVariant(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","bulkupdatevariant", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor createAddon(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","createaddon", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateAddon(String rest_id, String json, String token_id, String id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updateaddon", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id, id});
    }

    public Processor createBulkAddon(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","createbulkaddon", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateBulkAddon(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updatebulkaddon", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor createItem(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","createitem", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateItem(String rest_id, String json, String token_id, String id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updateitem", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id,id});
    }

    public Processor createBulkItem(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","createbulkitem", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateBulkItem(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updatebulkitem", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor createBulkSubcategory(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","createbulksubcategories", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public Processor updateBulkSubcategory(String rest_id, String json, String token_id){
        HashMap<String, String> headers = defaultHeaders();
        if(rest_id != "")
            headers.put("tokenid", FullMenuHelper.getToken_ids().get(rest_id));
        else
            headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","updatebulksubcategories", gameofthrones);
        return new Processor(service, headers, new String[]{json}, new String[] {rest_id});
    }

    public HashMap<String, String> defaultHeaders() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", MenuConstants.content_type);
        //headers.put("tokenid", MenuConstants.token_id);
        headers.put("Authorization", MenuConstants.auth);
        return headers;
    }

    public boolean verifyEntityResponse(Processor processor, MenuConstants.negativejson e) {
        boolean success = false;
        String response = processor.ResponseValidator.GetBodyAsText();
        String res;
        switch (e) {
            case empty:  success = processor.ResponseValidator.GetNodeValue("code").toString().equalsIgnoreCase(MenuConstants.code);
                         res = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("errors").replace("[","").replace("]","");
                         success = verifyNegativeEntityResponse(res);
                         break;
            case entity: success = processor.ResponseValidator.GetNodeValue("code").toString().equalsIgnoreCase(MenuConstants.code);
            			 res = processor.ResponseValidator.GetBodyAsText();
            			 success = verifyNegativeMaincategoryResponse(JsonPath.read(res, "$.errors").toString().replace("[","").replace("]",""));
            			 success = verifyNegativeItemResponse(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("errors"), 1);
            			 
            			 break;
            case maincategory:
                         success = processor.ResponseValidator.GetNodeValue("code").toString().equalsIgnoreCase(MenuConstants.code);
                         res = processor.ResponseValidator.GetBodyAsText();
                         success = verifyNegativeMaincategoryResponse(JsonPath.read(res, "$.errors").toString().replace("[","").replace("]",""));
                         success = verifyNegativeItemResponse(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("errors"), 1);
                        
                         break;

            case items: success = processor.ResponseValidator.GetNodeValue("code").toString().equalsIgnoreCase(MenuConstants.code);
                        res = processor.ResponseValidator.GetBodyAsText();
                        success = verifyNegativeMaincategoryResponse(JsonPath.read(res, "$.errors").toString().replace("[","").replace("]",""));
                        success = verifyNegativeItemResponse(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("errors"), 0);
                        break;

        }
        return success;
    }

    public boolean verifyNegativeItemResponse(String json, int offset) {
        boolean success = false;
        System.out.println(json);
        success = JsonPath.read(json, "$["+offset+"].error_field").toString().equalsIgnoreCase(MenuConstants.item_errm);
        success = JsonPath.read(json, "$["+offset+"].message").toString().equalsIgnoreCase(MenuConstants.item_message);
        return success;
    }

    public boolean verifyNegativeMaincategoryResponse(String json) {
        boolean success = false;
        success = JsonPath.read(json, "$.error_field").toString().equalsIgnoreCase(MenuConstants.main_categories_err);
        success = JsonPath.read(json, "$.message").toString().equalsIgnoreCase(MenuConstants.main_category_message);
        return success;
    }

    public boolean verifyNegativeEntityResponse(String json) {
        boolean success = false;
        success = JsonPath.read(json, "$.error_field").toString().equalsIgnoreCase(MenuConstants.entity);
        success = JsonPath.read(json, "$.message").toString().equalsIgnoreCase(MenuConstants.entities_error_message);
        return success;
    }

    public boolean verifyZeroItemsResponse(String json) {
        boolean success = false;
        success = JsonPath.read(json, "$.error_field").toString().equalsIgnoreCase(MenuConstants.items_zeroerror) &&
                JsonPath.read(json, "$.rejected_value").toString().equalsIgnoreCase(MenuConstants.items_rejected_value) &&
                JsonPath.read(json, "$.message").toString().equalsIgnoreCase(MenuConstants.items_zero_message);
        return success;
    }

    
    
    
    public List<String> bitGenerator() {
        List<String> list = new ArrayList<>();
        for(int i=0;i<16;i++) {
            String s = formatter(Integer.toBinaryString(i));
            System.out.println(s);
            list.add(s);
        }
        return list;
    }

    public String formatter(String s) {
        String ss = "";
        if(s.length() == 1)
             ss = "000";
        else if(s.length() == 2)
            ss = "00";
        else if(s.length() == 3)
            ss = "0";
        return ss+s;
    }

    public FullMenuPojo createPriceCombinations(String combinations) {
        FullMenuPojo fullMenuPojo = null;
        
        try {
            fullMenuPojo = createDefaultFullMenuPojo();

            fullMenuPojo = setPrice(fullMenuPojo, combinations.charAt(0));

            fullMenuPojo = setAddonPrice(fullMenuPojo, combinations.charAt(1));

            fullMenuPojo = setVariantPrice(fullMenuPojo, combinations.charAt(2));

            fullMenuPojo = setPricingCombinations(fullMenuPojo, combinations.charAt(3));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fullMenuPojo;
    }

    public FullMenuPojo createDefaultFullMenuPojo() throws IOException {
        FullMenuPojo fullMenuPojo = new FullMenuPojo();
        Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        Items items = new Items();
        Variant_groups variant_groups = new Variant_groups();
        Variants variants = new Variants();
        Addon_Groups addon_groups = new Addon_Groups();
        addon_groups.build();
        
        Addons addons = new Addons();
        Pricing_combinations pricing_combinations = new Pricing_combinations();
        addons.build();
        addon_groups.setAddons(new Addons[]{addons});
        pricing_combinations.build();
        variants.build();
        variant_groups.setVariants(new Variants[]{variants});
        variant_groups.build();
        items.setVariant_groups(new Variant_groups[]{variant_groups});
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        items.setAddon_groups(new Addon_Groups[]{addon_groups});
        main_categories.build();
        items.build();
        entity.setItems(new Items[]{items});
        entity.setMain_categories(new Main_categories[]{main_categories});
        fullMenuPojo.setEntity(entity);
        JsonHelper jsonHelper = new JsonHelper();
        System.out.println(jsonHelper.getObjectToJSON(fullMenuPojo));
        
        
        return fullMenuPojo;
        
    }

    public FullMenuPojo setPrice(FullMenuPojo fullMenuPojo, char c) {
        Entity entity = fullMenuPojo.getEntity();
        Items[] items = fullMenuPojo.getEntity().getItems();
        Items item = items[0];
        if(c == '0')
            item.setPrice(MenuConstants.zero_price);
        else
            item.setPrice(MenuConstants.default_price);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        return fullMenuPojo;
    }

    public FullMenuPojo setAddonPrice(FullMenuPojo fullMenuPojo, char c) {
        Entity entity = fullMenuPojo.getEntity();
        Items[] items = fullMenuPojo.getEntity().getItems();
        Items item = items[0];
        Addon_Groups[] addon_groups = item.getAddon_groups();
        Addon_Groups addon_group = addon_groups[0];
        Addons[] addons = addon_group.getAddons();
        Addons addon = addons[0];
        addon.setPrice((c == '0')?MenuConstants.zero_price: MenuConstants.default_price);
        addon_group.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_group});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        return fullMenuPojo;
    }

    public Addons[] getAddons() {
        Addons addons = new Addons();
        addons.build();
        return new Addons[]{addons};
    }

    public Addon_Groups[] getAddonGroups() {
        Addon_Groups addon_groups = new Addon_Groups();
        addon_groups.build();
        addon_groups.setAddons(getAddons());
        return new Addon_Groups[]{addon_groups};
    }

    public FullMenuPojo setVariantPrice(FullMenuPojo fullMenuPojo, char c) {
        Entity entity = fullMenuPojo.getEntity();
        Items[] items = fullMenuPojo.getEntity().getItems();
        Items item = items[0];
        Variant_groups variant_groups = item.getVariant_groups()[0];
        Variants variants = variant_groups.getVariants()[0];
        variants.setPrice((c == '0')?MenuConstants.zero_price: MenuConstants.default_price);
        variant_groups.setVariants(new Variants[]{variants});
        item.setVariant_groups(new Variant_groups[]{variant_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        return fullMenuPojo;
    }


    public FullMenuPojo setPricingCombinations(FullMenuPojo fullMenuPojo, char c) {
        Entity entity = fullMenuPojo.getEntity();
        Items[] items = fullMenuPojo.getEntity().getItems();
        Items item = items[0];
        Pricing_combinations pricing_combinations = item.getPricing_combinations()[0];
        pricing_combinations.setPrice((c == '0')?MenuConstants.zero_price: MenuConstants.default_price);
        item.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        return fullMenuPojo;
    }

    public List<FullMenuPojo> createPricingData() {
        List<FullMenuPojo> pojos = new ArrayList<>();
        List<String> combinations = bitGenerator();
        for(String str: combinations)
            pojos.add(createPriceCombinations(str));
        return  pojos;
    }

    public boolean verifyEntityResponseForPositive(Processor processor) {
        boolean success = false;
        success =processor.ResponseValidator.GetNodeValue("statusMessage").toString().equalsIgnoreCase(MenuConstants.Success_message) &&
                (processor.ResponseValidator.GetNodeValueAsInt("statusCode")==(MenuConstants.SuccessStatusCode))&&
                processor.ResponseValidator.GetNodeValue("code").toString().equalsIgnoreCase(MenuConstants.Successcode);
        return success;
    }

    public boolean verifyEntityResponseForPositive(String rest_id, Processor processor, List<String> ids) {
        boolean success = false;
        success =processor.ResponseValidator.GetNodeValue("statusMessage").toString().equalsIgnoreCase(MenuConstants.Success_message) &&
                (processor.ResponseValidator.GetNodeValueAsInt("statusCode")==(MenuConstants.SuccessStatusCode))&&
                processor.ResponseValidator.GetNodeValue("code").toString().equalsIgnoreCase(MenuConstants.Successcode) && validateItems(ids, rest_id);
        return success;
    }

    public boolean verifyIds(String store_id , List<String> ids) {
            CMSHelper cmsHelper = new CMSHelper();
            boolean success = false;
            try {
                Thread.sleep(5000);
                success = (cmsHelper.getCategoryActive(store_id, ids.get(0)) == 1);
                success &= (cmsHelper.getCategoryActive(store_id, ids.get(1)) == 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return success;
    }

    public boolean verifyMainCatForNegative(Processor processor,MenuConstants.negativejson e) {
        boolean success = false;
        success =processor.ResponseValidator.GetNodeValue("statusMessage").toString().equalsIgnoreCase(MenuConstants.statusMessage) &&
                (processor.ResponseValidator.GetNodeValueAsInt("statusCode")==(MenuConstants.failure_status_code));
        return success;
    }
    
    
    public boolean verifyEntityResponseForNegativePrice(Processor processor) {
        boolean success = false;
        String res = processor.ResponseValidator.GetBodyAsText();
        res = JsonPath.read(res, "$.errors").toString().replace("[","").replace("]","");
        success = JsonPath.read(res, "$.error_field").toString().equalsIgnoreCase(MenuConstants.failure_pricing_combinations) &&
                JsonPath.read(res, "$.message").toString().equalsIgnoreCase(MenuConstants.failure_pricing_message);
        return success;
    }

    public boolean validateResponse(Processor processor, boolean flag) {
        return (!flag) ? verifyEntityResponseForNegativePrice(processor) : verifyEntityResponseForPositive(processor);
    }

    public boolean verifyNegativeMainCategoryResponse(String json) {
        boolean success = false;
        success = JsonPath.read(json, "$.error_field").toString().equalsIgnoreCase(MenuConstants.entity);
        success = JsonPath.read(json, "$.statusMessage").toString().equalsIgnoreCase(MenuConstants.statusMessage);
        return success;
    }
    
    public boolean validatePricingResponse(Processor processor, boolean flag) {
        return (!flag) ? verifyZeroItemsResponse(getErrors(processor)) : verifyEntityResponseForPositive(processor);
    }

    public String getErrors(Processor processor) {
        return JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.errors").toString().replace("[","").replace("]","");
    }

    public String getError(Processor processor) {
        return JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.errors");
    }

    public boolean validateErrors(Processor processor, String[] validations) {
        boolean success = false;
        String res = "";
            if (validations.length == 0)
                success = verifyEntityResponseForPositive(processor);
            else {
                res = getErrors(processor);
                System.out.println("text is " + JsonPath.read(res, "error_field").toString());
                success = JsonPath.read(res, "$.error_field").toString().equalsIgnoreCase(validations[0]) &&
                        JsonPath.read(res, "$.message").toString().equalsIgnoreCase(validations[1]);
            }
        return success;
    }

    public boolean validateError(Processor processor, String[] validations) {
        boolean success = false;
        String res = "";
        if (validations.length == 0)
            success = verifyEntityResponseForPositive(processor);
        else {
            res = processor.ResponseValidator.GetBodyAsText();
            System.out.println("text is " + JsonPath.read(res, "errors.error_field").toString());
            success = JsonPath.read(res, "errors.error_field").toString().equalsIgnoreCase(validations[0]) &&
                    JsonPath.read(res, "errors.message").toString().equalsIgnoreCase(validations[1]);
        }
        return success;
    }

    public JsonNode setValues(JsonNode jsonnode, String key, String value) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(key, value);
        return RestTestUtil.updateRequestDataBeforeSend(jsonnode,  map);
    }


    public String generateId() {
        return String.valueOf(Instant.now().getEpochSecond());
    }

    public FullMenuPojo setItemId(FullMenuPojo fullMenuPojo, String id, int index) {
        Entity entity = fullMenuPojo.getEntity();
        if(entity.getItems().length >= index) {
            Items item = entity.getItems()[index];
            item.setId(id);
            Items[] items = entity.getItems();
            items[index] = item;
            entity.setItems(items);
            fullMenuPojo.setEntity(entity);
        }

        return fullMenuPojo;
    }

    public FullMenuPojo setCategoryId(FullMenuPojo fullMenuPojo, String id, int index) {
        Entity entity = fullMenuPojo.getEntity();
        if(entity.getMain_categories().length >= index) {
            Main_categories main_category = entity.getMain_categories()[index];
            main_category.setId(id);
            Main_categories[] main_categories = entity.getMain_categories();
            main_categories[index] = main_category;
            entity.setMain_categories(main_categories);
            Items items = entity.getItems()[0];
            items.setCategory_id(id);
            entity.setItems(new Items[]{items});
            fullMenuPojo.setEntity(entity);
        }
        return fullMenuPojo;
    }

    public Main_categories setCategoryDetails(Main_categories main_categories, String id, String name) {
            main_categories.setId(id);
            main_categories.setName(name);
            return main_categories;
    }

    public Main_categories getDefaultMainCategories() throws InterruptedException {
        Main_categories main_categories = new Main_categories();
        main_categories.build();
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        main_categories.setName(epochTime);
        main_categories.setId(epochTime);
        main_categories.getSub_categories()[0].setCategory_id(epochTime);
        Thread.sleep(1000);
        epochTime = String.valueOf(Instant.now().getEpochSecond());
        main_categories.getSub_categories()[0].setId(epochTime);
        return main_categories;
    }

    public Items getDefaultItems() {
        Items items = new Items();
        items.build();
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        items.setId(epochTime);
        items.setName("item_"+epochTime);
        return items;
    }

    public Items getRandomItems(String store_id) {
        Items items = new Items();
        items.build();
        CMSHelper cmsHelper = new CMSHelper();
        List<Map<String, Object>> l = cmsHelper.getFullMenuItems(store_id);
        int random = Utility.getRandom(0, l.size()-1);
        items.setId(l.get(random).get("external_item_id").toString());
        return items;
    }


    public Main_categories getRandomMainCategories(String store_id) {
        Main_categories main_categories = new Main_categories();
        main_categories.build();
        CMSHelper cmsHelper = new CMSHelper();
        List<String> l = getCategoryList(store_id);
        int random = Utility.getRandom(0, l.size()-1);
        String str = l.get(random);
        main_categories.setId(str);
        main_categories.setName(l.get(random));
        main_categories.getSub_categories()[0].setCategory_id(str);
        return main_categories;
    }

    public FullMenuPojo setSubcategoryId(FullMenuPojo fullMenuPojo, String id, int cat_index, int subcat_index) {
        Entity entity = fullMenuPojo.getEntity();
        if(entity.getMain_categories().length >= cat_index) {
            Main_categories main_category = entity.getMain_categories()[cat_index];
            if(main_category.getSub_categories().length >= subcat_index) {
                Sub_categories sub_category = main_category.getSub_categories()[subcat_index];
                sub_category.setId(id);
                Sub_categories[] sub_categories = main_category.getSub_categories();
                sub_categories[subcat_index] = sub_category;
                main_category.setSub_categories(sub_categories);
                Main_categories[] main_categories = entity.getMain_categories();
                main_categories[cat_index] = main_category;
                entity.setMain_categories(main_categories);

                Items items = entity.getItems()[0];
                items.setSub_category_id(id);
                entity.setItems(new Items[]{items});

                fullMenuPojo.setEntity(entity);
            }
        }
        return fullMenuPojo;
    }

    public Addons getAddon() {
        Addons addons = new Addons();
        addons.build();
        String epoc = getEpocId();
        addons.setId(epoc);
        addons.setName(addons.getName()+epoc);
        return addons;
    }

    public Variants getVariant() {
        Variants variants = new Variants();
        variants.build();
        String epoc = getEpocId();
        variants.setId(epoc);
        variants.setName(variants.getName()+epoc);
        return variants;
    }

    public Addon_Groups getAddonsGroups() {
        Addon_Groups addon_groups = new Addon_Groups();
        addon_groups.build();
        String epoc = getEpocId();
        addon_groups.setId(epoc);
        addon_groups.setName(addon_groups.getName()+epoc);
        return addon_groups;
    }

    public Variant_groups getVariantsGroups() {
        Variant_groups variant_groups = new Variant_groups();
        variant_groups.build();
        String epoc = getEpocId();
        variant_groups.setId(epoc);
        variant_groups.setName(variant_groups.getName()+epoc);
        return variant_groups;
    }

    public String getEpocId() {
        return Long.toString(Instant.now().getEpochSecond());
    }

    public Addons[] getNAddons(int n) {
        Addons[] addons = new Addons[n];
        for(int i=0;i<n;i++) {
            addons[i] = getAddon();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return addons;
    }

    public Variants[] getNVariants(int n) {
        Variants[] variants = new Variants[n];
        for(int i=0;i<n;i++) {
            variants[i] = getVariant();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return variants;
    }

    public Addon_Groups[] getNAddonsGroups(int n) {
        Addon_Groups[] addon_groups = new Addon_Groups[n];
        for(int i=0;i<n;i++) {
            addon_groups[i] = getAddonsGroups();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return addon_groups;
    }

    public Variant_groups[] getNVariantsGroups(int n) {
        Variant_groups[] variant_groups = new Variant_groups[n];
        for(int i=0;i<n;i++) {
            variant_groups[i] = getVariantsGroups();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return variant_groups;
    }

    public Variant_groups[] createnVariantsGroupswithmVariants(int variant_group_count, int variant_count) throws IOException {
        Variant_groups[] variant_groups = getNVariantsGroups(variant_group_count);
        JsonHelper jsonHelper = new JsonHelper();
        for(int i=0;i<variant_group_count;i++) {
            Variant_groups vg = variant_groups[i];
            String key = JsonPath.read(jsonHelper.getObjectToJSON(vg),"$.id");
            Variants[] variants = getNVariants(variant_count);
            String[] variant_ids = JsonPath.read(jsonHelper.getObjectToJSON(variants),"$..id").toString().replace("[","").replace("]","").replace("\"","").split(",");
            variantGroupIds.put(key, new ArrayList<>(Arrays.asList(variant_ids)));
            vg.setVariants(variants);
        }
        variant_groups = setDefaultVariants(variant_groups);
      
        return variant_groups;
    }

    public Variant_groups[] setDefaultVariants(Variant_groups[] variant_groups) throws IOException {
        int i=0;
        JsonHelper jsonHelper = new JsonHelper();
        for(i=0;i<variant_groups.length-1;i++) {
            Variant_groups vg = variant_groups[i];
            Variant_groups vg_next = variant_groups[i+1];
            Variants[] variants = vg.getVariants();
            String key = JsonPath.read(jsonHelper.getObjectToJSON(vg_next),"$.id");
            String[] variant_ids = JsonPath.read(jsonHelper.getObjectToJSON(vg_next.getVariants()),"$..id").toString().replace("[","").replace("]","").replace("\"","").split(",");
            for(int j=0;j<variants.length;j++) {
                variants[j].setDefault_dependent_variant_group_id(key);
                variants[j].setDefault_dependent_variant_id(variant_ids[j]);
            }
        }
        return variant_groups;
    }

    public Addon_Groups[] createnAddonGroupswithmVariants(int addon_group_count, int addon_count) throws IOException {
        Addon_Groups[] addon_groups = getNAddonsGroups(addon_group_count);
        JsonHelper jsonHelper = new JsonHelper();
        for(int i=0;i<addon_group_count;i++) {
            Addon_Groups vg = addon_groups[i];
            String key = JsonPath.read(jsonHelper.getObjectToJSON(vg),"$.id");
            Addons[] addons = getNAddons(addon_count);
            String[] addon_ids = JsonPath.read(jsonHelper.getObjectToJSON(addons),"$..id").toString().replace("[","").replace("]","").replace("\"","").split(",");
            addonGroupIds.put(key, new ArrayList<>(Arrays.asList(addon_ids)));
            vg.setAddons(addons);
        }
        return addon_groups;
    }

    public String[] getGroupIds(String json) {
        return JsonPath.read(json, "$[*].id").toString().replace("["," ").replace("]","").split(",");
    }

    public String[] getIds(String json) {
        return JsonPath.read(json, "$[*]..id").toString().replace("["," ").replace("]","").split(",");
    }

    public List<String> subractionOfArrays(String[] first, String[] second) {
       List<String> firstlist = new ArrayList<String>(Arrays.asList(first));
       List<String> secondlist = new ArrayList<String>(Arrays.asList(second));
       secondlist.removeAll(firstlist);
       return secondlist;
    }

    public FullMenuPojo createFullMenuPojo(int variant_group_count, int variant_count, int addon_group_count, int addon_count, int addon_combinations_count, int variant_combinations_count) throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuPojo fullMenuPojo = createDefaultFullMenuPojo();
        Variant_groups[] variant_groups = createnVariantsGroupswithmVariants(variant_group_count, variant_count);
        Addon_Groups[] addon_groups = createnAddonGroupswithmVariants(addon_group_count, addon_count);
        Entity entity = fullMenuPojo.getEntity();
        Items item = entity.getItems()[0];
        item.setVariant_groups(variant_groups);
        item.setAddon_groups(addon_groups);
        Pricing_combinations pricing_combinations = item.getPricing_combinations()[0];
        List<HashMap<String, String>> variant_list = getListOfMap(variantGroupIds);
        List<HashMap<String, String>> addon_list = getListOfMap(addonGroupIds);
        Variant_combination[] variant_combinations = addVariants(variant_list);
        Addon_combination[] addon_combinations = addAddons(addon_list);
        pricing_combinations.setAddon_combination((Addon_combination[]) Arrays.copyOfRange(addon_combinations, 0, addon_combinations_count));
        pricing_combinations.setVariant_combination((Variant_combination[]) Arrays.copyOfRange(variant_combinations, 0 , variant_combinations_count));
        item.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        return fullMenuPojo;
    }


    public List<HashMap<String, String>> getListOfMap(HashMap<String, List<String>> map_list) {
        List<HashMap<String, String>> list = new ArrayList<>();
        for (Iterator it = map_list.keySet().iterator(); it.hasNext(); ) {
            String key = (String) it.next();
            List<String> l = map_list.get(key);
            for(int i=0;i<l.size();i++) {
                HashMap<String, String> h = new HashMap<>();
                h.put(key, l.get(i));
                list.add(h);
            }
        }
        return list;
    }

    public Variant_combination[] addVariants(List<HashMap<String, String>> list) {
        int n=list.size();
        Variant_combination[] variant_combinations = new Variant_combination[n];
        for(int i=0;i<list.size();i++) {
            Variant_combination variant_combination = new Variant_combination();
            variant_combination.build();
            HashMap<String, String> m = list.get(i);
            String key = m.keySet().iterator().next();
            variant_combination.setVariant_group_id(key);
            variant_combination.setVariant_id(m.get(key));

            variant_combinations[i] = variant_combination;
        }
        return variant_combinations;
    }

    public Addon_combination[] addAddons(List<HashMap<String, String>> list) {
        int n=list.size();
        Addon_combination[] addon_combinations = new Addon_combination[n];
        for(int i=0;i<list.size();i++) {
            Addon_combination addon_combination = new Addon_combination();
            addon_combination.build();
            HashMap<String, String> m = list.get(i);
            String key = m.keySet().iterator().next();
            addon_combination.setAddon_group_id(key);
            addon_combination.setAddon_id(m.get(key));

            addon_combinations[i] = addon_combination;
        }
        return addon_combinations;
    }

    public List<List<HashMap<String, String>>> generateAllSubsets(List<HashMap<String, String>> list) throws IOException {
        List<List<HashMap<String, String>>> subset = new ArrayList<>();
        int n = list.size(), i = 0, j = 0;
        for(i = 0;i < (1 << n);i++) {
            List<HashMap<String, String>> temp = new ArrayList<HashMap<String, String>>();
            for(j = 0;j < n;j++) {
                if ((i & (1 << j)) > 0) {
                    temp.add(list.get(j));
                }
            }
            subset.add(i, temp);
        }
        return subset;
    }


 /*   public Pricing_combinations[] generateAllPricingCombinations(List<HashMap<String, String>> list1, List<HashMap<String, String>> list2, int variant_group_count) throws IOException {
        List<List<HashMap<String, String>>> subset1 = generateAllSubsets(list1);
        List<List<HashMap<String, String>>> subset2 = generateAllSubsets(list2);
        return combineList(subset1, list2, variant_group_count);
    public Pricing_combinations[] generateAllPricingCombinations(List<HashMap<String, String>> list1, List<HashMap<String, String>> list2, int variant_group_count, int addon_group_count) throws IOException {
        List<List<HashMap<String, String>>> subset1 = generateAllSubsets(list1);
        List<List<HashMap<String, String>>> subset2 = generateAllSubsets(list2);
        return combineList(subset1, list2, variant_group_count, addon_group_count);
   }

    public Pricing_combinations[] combineList(List<List<HashMap<String, String>>> list1, List<HashMap<String, String>> list2, int variant_group_count, int addon_group_count) {
        List<List<HashMap<String, String>>> list = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        int i=0,j=0,k=0;
        int price = MenuConstants.defaultpojo_price;
        Pricing_combinations[] pricing_combinations = new Pricing_combinations[10001];
        for(i = 0; i < list1.size();i++) {
            Variant_combination[] variant_combinations = addVariants(list1.get(i));
            try {
                System.out.println(jsonHelper.getObjectToJSON(variant_combinations));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if(variant_combinations.length == variant_group_count && getVariantCountIds(variant_combinations) == variant_group_count) {
                    Pricing_combinations pricing_combination = new Pricing_combinations();
                    Addon_combination[] addon_combinations = addAddons(list2);
   					Arrays.sort(addon_combinations, new Comparator<Addon_combination>() {
                        @Override
                        public int compare(Addon_combination o1, Addon_combination o2) {
                            return o1.getAddon_group_id().compareTo(o2.getAddon_group_id());
                        }
                    });
                    Arrays.sort(variant_combinations, new Comparator<Variant_combination>() {
                        @Override
                        public int compare(Variant_combination o1, Variant_combination o2) {
                            return o1.getVariant_group_id().compareTo(o2.getVariant_group_id());
                        }
                    });
                    pricing_combination.setVariant_combination(variant_combinations);
                    pricing_combination.setAddon_combination(addon_combinations);
                    pricing_combination.setPrice(price+10);
                    price += 10;
                    pricing_combinations[k++] = pricing_combination;
        			pricing_combination.setVariant_combination(variant_combinations);
                    pricing_combination.setAddon_combination(addon_combinations);

                    pricing_combination.setPrice(price+10);
                    price += 10;
                    pricing_combinations[k++] = pricing_combination;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            System.out.println(jsonHelper.getObjectToJSON((Pricing_combinations[])Arrays.copyOfRange(pricing_combinations, 0, k)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ((Pricing_combinations[])Arrays.copyOfRange(pricing_combinations, 0, k));
    }
    
    public FullMenuPojo createFullMenuPojo(String category_id, String subcategory_id, String item_id, int variant_group_count, int variant_count, int addon_group_count, int addon_count, int pricing_combinations_count) throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuPojo fullMenuPojo = createDefaultFullMenuPojo();

        if(category_id != "" && category_id != null)
            setCategoryId(fullMenuPojo, category_id, 0);
        if(subcategory_id != "" && subcategory_id != null)
            setSubcategoryId(fullMenuPojo, subcategory_id, 0, 0);
        if(item_id != "" && item_id != null)
            setItemId(fullMenuPojo, item_id, 0);

        Variant_groups[] variant_groups = createnVariantsGroupswithmVariants(variant_group_count, variant_count);
        
        Addon_Groups[] addon_groups = createnAddonGroupswithmVariants(addon_group_count, addon_count);
        Entity entity = fullMenuPojo.getEntity();
        Items item = entity.getItems()[0];
        item.setVariant_groups(variant_groups);
        item.setAddon_groups(addon_groups);
        List<HashMap<String, String>> variant_list = getListOfMap(variantGroupIds);
        List<HashMap<String, String>> addon_list = getListOfMap(addonGroupIds);
        Pricing_combinations[] pricing_combinations = generateAllPricingCombinations(variant_list, addon_list, variant_group_count, addon_group_count);
        pricing_combinations_count = Math.min(pricing_combinations_count, pricing_combinations.length);
        pricing_combinations = (Pricing_combinations[])Arrays.copyOfRange(pricing_combinations, 0, pricing_combinations_count);
        item.setPricing_combinations(pricing_combinations);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        return fullMenuPojo;
    }
*/
    public int getVariantCountIds(Variant_combination[] variant_combinations) throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        String json = jsonHelper.getObjectToJSON(variant_combinations);
        String[] s = JsonPath.read(json, "$[*].variant_group_id").toString().replace("[","").replace("]","").replace("\"","").split(",");
        List<String> ls = new ArrayList<>(Arrays.asList(s));
        return (int)ls.stream().distinct().count();
    }


    
    public Main_categories getMainCategories() {
        Main_categories mainCat = new Main_categories();
        mainCat.build();
        String epoc = getEpocId();
        mainCat.setId(epoc);
        mainCat.setName(mainCat.getName()+epoc);
        return mainCat;
    }
    
    
    public Sub_categories getsubCategories() {
    	Sub_categories subCat = new Sub_categories();
        subCat.build();
        String epoc = getEpocId();
        subCat.setId(epoc);
        subCat.setName(subCat.getName()+epoc);
        return subCat;
    }


    
    public Pricing_combinations[] generateAllPricingCombinations(List<HashMap<String, String>> list1, List<HashMap<String, String>> list2, int variant_group_count, int addon_group_count) throws IOException {
        List<List<HashMap<String, String>>> subset1 = generateAllSubsets(list1);
        List<List<HashMap<String, String>>> subset2 = generateAllSubsets(list2);
        return combineList(subset1, list2, variant_group_count, addon_group_count);
    }
    public Pricing_combinations[] combineList(List<List<HashMap<String, String>>> list1, List<HashMap<String, String>> list2, int variant_group_count, int addon_group_count) throws IOException {
        List<List<HashMap<String, String>>> list = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        int i=0,j=0,k=0;
        int price = MenuConstants.defaultpojo_price;
        Pricing_combinations[] pricing_combinations = new Pricing_combinations[10001];
        for(i = 0; i < list1.size();i++) {
            Variant_combination[] variant_combinations = addVariants(list1.get(i));
           
                if(variant_combinations.length == variant_group_count && getVariantCountIds(variant_combinations) == variant_group_count) {
                    Pricing_combinations pricing_combination = new Pricing_combinations();
                    Addon_combination[] addon_combinations = addAddons(list2);
                    Arrays.sort(addon_combinations, new Comparator<Addon_combination>() {
                        @Override
                        public int compare(Addon_combination o1, Addon_combination o2) {
                            return o1.getAddon_group_id().compareTo(o2.getAddon_group_id());
                        }
                    });
                    Arrays.sort(variant_combinations, new Comparator<Variant_combination>() {
                        @Override
                        public int compare(Variant_combination o1, Variant_combination o2) {
                            return o1.getVariant_group_id().compareTo(o2.getVariant_group_id());
                        }
                    });
                    pricing_combination.setVariant_combination(variant_combinations);
                    pricing_combination.setAddon_combination(addon_combinations);
                    pricing_combination.setPrice(price+10);
                    price += 10;
                    pricing_combinations[k++] = pricing_combination;
           }
        }
        return ((Pricing_combinations[])Arrays.copyOfRange(pricing_combinations, 0, k));
    }
    public FullMenuPojo createFullMenuPojo(String category_id, String subcategory_id, String item_id, int variant_group_count, int variant_count, int addon_group_count, int addon_count, int pricing_combinations_count) throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
       variantGroupIds = new HashMap<>();
       addonGroupIds = new HashMap<>();
        FullMenuPojo fullMenuPojo = createDefaultFullMenuPojo();
        if(category_id != "" && category_id != null)
            setCategoryId(fullMenuPojo, category_id, 0);
        if(subcategory_id != "" && subcategory_id != null)
            setSubcategoryId(fullMenuPojo, subcategory_id, 0, 0);
        if(item_id != "" && item_id != null)
            setItemId(fullMenuPojo, item_id, 0);
        Variant_groups[] variant_groups = createnVariantsGroupswithmVariants(variant_group_count, variant_count);
        Addon_Groups[] addon_groups = createnAddonGroupswithmVariants(addon_group_count, addon_count);
        Entity entity = fullMenuPojo.getEntity();
        Items item = entity.getItems()[0];
        item.setVariant_groups(variant_groups);
        item.setAddon_groups(addon_groups);
        List<HashMap<String, String>> variant_list = getListOfMap(variantGroupIds);
        List<HashMap<String, String>> addon_list = getListOfMap(addonGroupIds);
        Pricing_combinations[] pricing_combinations = generateAllPricingCombinations(variant_list, addon_list, variant_group_count, addon_group_count);
        pricing_combinations_count = Math.min(pricing_combinations_count, pricing_combinations.length);
        pricing_combinations = (Pricing_combinations[])Arrays.copyOfRange(pricing_combinations, 0, pricing_combinations_count);
        item.setPricing_combinations(pricing_combinations);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        return fullMenuPojo;
    }
    

    public Processor getRequestTracker(String request_id, String token_id) {
        HashMap<String, String> headers = defaultHeaders();
        headers.put("tokenid", token_id);
        GameOfThronesService service = new GameOfThronesService("cms_cloud_menu","requesttracker", gameofthrones);
        return new Processor(service, headers, null, new String[]{request_id});
    }

    public List<String> getCategoryList(String store_id) {
        List<String> categories = new ArrayList<>();
        CMSHelper cmsHelper = new CMSHelper();
        List<Map<String, Object>> map = cmsHelper.getCategoriesByStoreId(store_id);
        for(int i=0;i<map.size();i++)
            categories.add(map.get(i).get("external_id").toString());
        return categories;
    }

    public List<String> getCatIds(Main_categories[] main_categories) {
        List<String> list = new ArrayList<>();
        for(int i=0;i<main_categories.length;i++)
            list.add(main_categories[i].getId());

        return list;
    }

    public List<String> getItemIds(Items[] item) {
        List<String> list = new ArrayList<>();
        for(int i=0;i<item.length;i++)
            list.add(item[i].getId());
        return list;
    }

    public boolean validateItems(List<String> item_ids, String store_id) {
        CMSHelper cmsHelper = new CMSHelper();
        boolean success = false;
        List<Map<String, Object>> list = cmsHelper.getItemsCreated(store_id);
        for(int i=0;i<list.size();i++) {
            Map<String, Object> map = list.get(i);
            if(item_ids.contains(map.get("external_item_id")))
                success &= ((int)map.get("active") == 1) && ((int)map.get("enabled") == 1);
            else
                success &= ((int)map.get("active") == 1) && ((int)map.get("enabled") == 0);
        }
        return success;
    }

    
    public boolean verifyEntityResponseForPositive1(String rest_id, Processor processor, List<String> ids,Items[] items,boolean itemPassOrFail) throws Exception {
        boolean success = false;
        success =processor.ResponseValidator.GetNodeValue("statusMessage").toString().equalsIgnoreCase(MenuConstants.Success_message) &&
                (processor.ResponseValidator.GetNodeValueAsInt("statusCode")==(MenuConstants.SuccessStatusCode))&&
                processor.ResponseValidator.GetNodeValue("code").toString().equalsIgnoreCase(MenuConstants.Successcode) && validatedetailsItems(ids, rest_id,items,itemPassOrFail);
      return success;
    }
    
    public boolean validatedetailsItems(List<String> item_ids, String store_id,Items[] items,boolean itemPassOrFail) throws  Exception
    {
       Thread.sleep(1000);
        boolean success = false;
        int size=items.length;
        String itemIdFM=null;
        
         for( int i=0;i<size;i++)
       {
        	
         Map<String, Object> map = cmsHelper.getItemsCreated1(store_id,items[i].getId());
       	 itemIdFM=items[i].getId().toString();
       	 if(itemPassOrFail==true)
       		 {
       	 if(map!=null) 
       	 { 
       		 
       	 if(item_ids.contains(map.get("external_item_id")))
        	{
       		 success = ((Float)map.get("price")==items[i].getPrice()) 
       				 &&((int)map.get("enabled") == 1)
    				&&(map.get("name").toString().equalsIgnoreCase(items[i].getName())) 
    			    &&(map.get("description").toString().equalsIgnoreCase(items[i].getDescription()))
    				&&((int)map.get("is_veg")==items[i].getIs_veg()) 
    				&&((int)map.get("in_stock")==1) 
    				&&((int)map.get("active") == 1) ;
//       		 if(items[i].getImage_url()!=null)
//       			 success &=map.get("s3_image_url").toString().equalsIgnoreCase(items[i].getImage_url());
        	  if (items[i].getVariant_groups()!=null && success==true)
                 success &= validateVariantsGroups(map, store_id,items,success,i);
              if (items[i].getAddon_groups()!=null && success==true)
                  success &= validateAddonGroups(map, store_id,items,success,i);
              if (items[i].getPricing_combinations()!=null && success==true)
            	  success &= validatePringCombination(map, store_id,items,success,i);
              if(items[i].getItem_slots()!=null && success==true)
             success &=validateDetailedItemsSlots(map, store_id,items,success,i);
              if(items[i].getGst_details()!=null && success==true)
                  success &=validateDetailedGst(items,success,i,"ITEM",map.get("id").toString());
              System.out.println("gst");
           	}
           else
               success &= ((int)map.get("active") == 1) && ((int)map.get("enabled") == 0);
      
       	 }
       	 else
       	 success&=false;
       	 }
         else
          if((int)map.get("enabled") == 0);
       	 success =true;
        
       }
      		
        return success;
    }



public boolean validateVariantsGroups(Map<String, Object> map, String store_id,Items[] items,boolean success,int itemCount) throws  Exception{
	Thread.sleep(1000);
    String[] variantGroup_id_map1 = new String[101];
    String[] variantGroupExternalIdMap = new String[101];
    
  	 if (items[itemCount].getVariant_groups()!=null)
        	{
    	     	Variant_groups[] vg=items[itemCount].getVariant_groups();
    	 	    int vgCount=0;
    		    for (int j=0;j<vg.length;j++)
    		      {
    			     List<Map<String, Object>> map1 = cmsHelper.getVariantGroupsCreated(store_id,items[itemCount].getId());
                     for( int p=0;p<map1.size();p++)
    			     {
                    	 variantGroup_id_map1[p] = map1.get(p).get("id").toString();
                    	 variantGroupExternalIdMap[p]=map1.get(p).get("external_id").toString();
    			     }
                    
    			     String[] variantGID1 = JsonPath.read(map.get("variants_v2").toString(),"$.variant_groups[*].group_id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
    			    if(variantGID1!=null)
                      {
                    	 for(int l=0;l<variantGID1.length;l++)
         				 { 
                    	  for(int m=0;m<vg.length;m++)
         					{
                    		 if(variantGID1[l].toString().equalsIgnoreCase(variantGroup_id_map1[m].toString()) && variantGroupExternalIdMap[m].toString().equalsIgnoreCase(vg[l].getId().toString()))
         					   {
                    			   if(vg[l].getName().toString().equalsIgnoreCase(map1.get(l).get("name").toString()) && (vg[l].getOrder()==Integer.parseInt(map1.get(l).get("order").toString())) && (int)(map1.get(l).get("active"))==1)
         						    vgCount++;
         					   }
                    	  
                    		if (success==true)
                		    success&= validateDetailedVariants(map,store_id,items,success,itemCount,map1,l,vg[l].getId(),variantGID1[m]);
                		    }
         				 }
         				  	
         		   	  }
    	
 			  		if(vgCount==vg.length)
 			  			success&=true;
 			  		System.out.println("length"+vgCount+"requestpayload"+vg.length);
    			   }
        		}
   return success;
}



public boolean validateDetailedVariants(Map<String, Object> map, String store_id,Items[] items,boolean success,int itemCount,List<Map<String, Object>> map1,int variantGroupCount,String  variantGIdofrequest, String  variantGIdofItem) throws  Exception{
	Thread.sleep(1000);
    int size=items.length;
    int variantcount=0;
    String[] variant_id_map1 = new String[101];
    String[] variant1 = new String[101];
    String[] variantId= new String[101];
    System.out.println("SIZE"+size);
	Variant_groups[] vg=items[itemCount].getVariant_groups();
	Variants[] v=vg[variantcount].getVariants();
	System.out.println("variiants list individual variant tables1"+variant1);
	  
    variant1 = JsonPath.read(map.get("variants_v2").toString(),"$.variant_groups["+variantGroupCount+"].variations[*].id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
    				
    List<Map<String, Object>> variantsIdsfromdb = cmsHelper.getVariantCreated(store_id,items[itemCount].getId(),variantGIdofrequest);
    			       
    					  for( int i=0;i<variant1.length;i++)
            			  { 
    						 variantId[i] = variantsIdsfromdb.get(i).get("external_variant_id").toString();
    			        	 for(int j=0;j<variant1.length;j++)
    			        	  { 
    			        	    if(variant1[i].toString().equalsIgnoreCase(variantsIdsfromdb.get(j).get("id").toString())&& variantsIdsfromdb.get(j).get("external_variant_id").toString().equals(v[j].getId().toString()))
    			        	  	  {
    			        		    if(  v[i].getId().toString().equalsIgnoreCase(variantsIdsfromdb.get(j).get("id").toString())&&
    			        				  v[i].getName().toString().equalsIgnoreCase(variantsIdsfromdb.get(j).get("name").toString())&&
    			        				  v[i].getPrice()==Double.parseDouble(variantsIdsfromdb.get(j).get("price").toString())&&
    			        				  v[i].getOrder()==Integer.parseInt(variantsIdsfromdb.get(j).get("order").toString())&&
    			        				  v[i].getIn_stock()==(Boolean)(variantsIdsfromdb.get(j).get("in_stock"))&&
    			        				  (int)variantsIdsfromdb.get(j).get("active")==1 &&
    			        				  v[i].getIs_veg()==(Boolean)(variantsIdsfromdb.get(j).get("is_veg"))&&
    			        				  v[i].getDefault()== variantsIdsfromdb.get(j).get("default") &&
    			        				  variantGIdofItem.equalsIgnoreCase(variantsIdsfromdb.get(j).get("variant_group_id").toString())
    			        			)
    			        		    { 
    			        		        if(v[i].getGst_details()!=null && success==true)
    			        	                  success &=validateDetailedGst(items,success,i,"VARIANT",variantsIdsfromdb.get(j).get("id").toString());			        	         
    			        		    	success&=true;
    			        		    }
    			        			  for (int l=0;l<variant1.length;l++)
    			        				  {
    			        				      if(variant1[l].equalsIgnoreCase(variantsIdsfromdb.get(j).get("id").toString()));
    			        				 	  success&=true;
    			        				  }
    			        			 		  
    			        		  variantcount++;
    			        	  	}	
    			        	  else System.out.println("not equal");
    			        	  }
    			          }
 	  success&= variant1.length==variantcount;
 	  return success;
  }



public boolean validateAddonGroups(Map<String, Object> map, String store_id,Items[] items,boolean success,int itemCount) throws  Exception{
	 String[] AddonGroupIdsfrmDB;
	 String[] addonGroup_id_map1 = new String[101];
     String[] addonGroupExternalIdMap = new String[101];
	    
	 if (items[itemCount].getAddon_groups()!=null)
		    {
	     	Addon_Groups[] ag=items[itemCount].getAddon_groups();
	 	    int agCount=0;
		    for (int j=0;j<ag.length;j++)
		      {
			     List<Map<String, Object>> map1 = cmsHelper.getAddonGroupCreated(store_id,items[itemCount].getId());
       
			     AddonGroupIdsfrmDB = JsonPath.read(map.get("addons").toString(),"$..group_id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
			 	
			     for( int p=0;p<map1.size();p++)
			     {
                	addonGroup_id_map1[p] = map1.get(p).get("id").toString();
                	addonGroupExternalIdMap[p]=map1.get(p).get("external_id").toString();
			     }
             if(AddonGroupIdsfrmDB!=null)
               {
             	 for(int l=0;l<AddonGroupIdsfrmDB.length;l++)
  				 {for(int m=0;m<AddonGroupIdsfrmDB.length;m++)
  					{
             				    
             		 if(AddonGroupIdsfrmDB[l].toString().equalsIgnoreCase(addonGroup_id_map1[m].toString()) && addonGroupExternalIdMap[m].toString().equalsIgnoreCase(ag[l].getId().toString()))
  					   {
             			   if(ag[l].getName().toString().equalsIgnoreCase(map1.get(l).get("name").toString()) 
             					   && (ag[l].getOrder()==Integer.parseInt(map1.get(l).get("order").toString())) 
             					   && (int)(map1.get(l).get("active"))==1 
             					  // && (ag[l].getAddon_free_limit()==Integer.parseInt(map1.get(l).get("addon_free_limit").toString()))
             			          // && (ag[l].getAddon_min_limit()==Integer.parseInt(map1.get(l).get("addon_min_imit").toString()))
            			      //     && (ag[l].getAddon_free_limit()==Integer.parseInt(map1.get(l).get("addon_free_limit").toString()))
             			           
             				  )agCount++;
  					   }
             		 if(success==true)
             	   success&= validateDetailedAddons(map,store_id,items,success,itemCount,map1,l,AddonGroupIdsfrmDB[l],ag[l].getId());
			  		}
  				 }
  				  	
  		   	  }
	
		  		if(agCount==ag.length)
		  			success &=true;
		  	  }
 		}
	  
		 
   return success;
}

public boolean validateDetailedAddons(Map<String, Object> map, String store_id,Items[] items,boolean success,int itemCount,List<Map<String, Object>> map1,int addonGroupCount,String addonExternalId,String  addonGId) throws  Exception{
        Thread.sleep(1000);
	    int size=items.length;
	    int addoncount=0;
	    String[] addon_id_map1 = new String[101];
	    String[] addons1 = new String[101];
	    String[] addonId= new String[101];
	    System.out.println("SIZE"+size);
		Addon_Groups[] ag=items[itemCount].getAddon_groups();
		Addons[] a=ag[addonGroupCount].getAddons();
			addons1 = JsonPath.read(map.get("addons").toString(),"$."+addonExternalId+".choices[*].id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
		 	for( int i=0;i<addons1.length;i++)
	            			  { 
	    						  List<Map<String, Object>> addonsIdsfromdb = cmsHelper.getaddonCreated(store_id,items[itemCount].getId(),a[i].getId(),addonGId);
	   	    			       
	    			        	  addonId[i] = addonsIdsfromdb.get(i).get("external_addon_id").toString();
	    			        	 for(int j=0;j<addons1.length;j++)
	    			        	  { 
	    			        	  if(addons1[i].toString().equalsIgnoreCase(addonsIdsfromdb.get(j).get("id").toString())&& addonsIdsfromdb.get(j).get("external_addon_id").toString().equals(a[j].getId().toString()))
	    			        	  
	    			        	  	{
	    			        		  if( a[i].getId().toString().equalsIgnoreCase(addonsIdsfromdb.get(j).get("external_addon_id").toString())&&
	    			        				  a[i].getName().toString().equalsIgnoreCase(addonsIdsfromdb.get(j).get("name").toString())&&
		    			        			  a[i].getPrice()==Double.parseDouble(addonsIdsfromdb.get(j).get("price").toString())&&
	    			        				  a[i].getOrder()==Integer.parseInt(addonsIdsfromdb.get(j).get("order").toString())&&
	    			        				  a[i].isIn_stock()==(Boolean)(addonsIdsfromdb.get(j).get("in_stock"))&&
	    			        				  (int)addonsIdsfromdb.get(j).get("active")==1 )
	    			        		  {	  System.out.println("valid success");
	    			        		  addoncount++;
	    			        		  if(a[i].getGst_details()!=null && success==true)
			        	                  success &=validateDetailedGst(items,success,i,"ADDON",addonsIdsfromdb.get(j).get("id").toString());
			        	         
    			        		  }
	    			        	  	}	
	    			        	  
	    			        	  }
	    			          }
		 	
		 	 if(addons1.length==addoncount)
		 			 success&=true;
return success;
}

public boolean validatePringCombination(Map<String, Object> map, String store_id,Items[] items,boolean success,int itemCount) throws  Exception{
	 String[] pricingCombinationFrmDb;
	 String[] pCVariant,pCVariantGIdCombo,pricingCombinationPriceFrmDb; 
     String[] pCAddonIdCombo,pCAddonGIdCombo,pCVariantionLength = null,pCaddonLength = null;
     Pricing_combinations[] pc=items[itemCount].getPricing_combinations();
    int ip = JsonPath.read(map.get("variants_v2").toString(),"$.pricing_models").toString().replace("[","").replace("]","").replace("\"","").split("}").length; 
//     List<String> list = Arrays.asList(JsonPath.read(map.get("variants_v2").toString(),"$.pricing_models").toString().replace("[","").replace("]","").split(","));
//     System.out.println("list size" + list.size());
     
     Integer l = JsonPath.read(map.get("variants_v2").toString(), "$.pricing_models.length()");
     System.out.println("list size"+l);
     
    
  
     
    if (items[itemCount].getPricing_combinations()!=null)
	 {
    	for(int i=0;i<l;i++)
    	{	
    		 Integer addonlength = JsonPath.read(map.get("variants_v2").toString(), "$.pricing_models["+i+"].addon_combinations.length()");
    	     Integer variantlength = JsonPath.read(map.get("variants_v2").toString(), "$.pricing_models["+i+"].variations.length()");
    	     
    		   Variant_combination[] vc= pc[i].getVariant_combination();
               Addon_combination[] ac=pc[i].getAddon_combination();
               if(variantlength!=0)
               pCVariantionLength= JsonPath.read(map.get("variants_v2").toString(),"$.pricing_models["+i+"].variations["+i+"].third_party_group_id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
                
               if(addonlength!=0)
               pCaddonLength= JsonPath.read(map.get("variants_v2").toString(),"$.pricing_models["+i+"].addon_combinations["+i+"].third_party_group_id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
       		 
              if(pCVariantionLength.length!=0) 
              {  
               for(int j=0;j<pCVariantionLength.length;j++)
    	   	{	
    	   		int vgCount=0,variantionCount = 0;
    	   		pCVariantGIdCombo = JsonPath.read(map.get("variants_v2").toString(),"$.pricing_models["+i+"].variations["+j+"].third_party_group_id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
    		    pCVariant= JsonPath.read(map.get("variants_v2").toString(),"$.pricing_models["+i+"].variations["+j+"].third_party_variation_id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
    		    pricingCombinationPriceFrmDb = JsonPath.read(map.get("variants_v2").toString(),"$.pricing_models["+i+"].price").toString().replace("[","").replace("]","").replace("\"","").split(","); 
               
                for(int m = 0;m<vc.length;m++)
                   {
                	if(pCVariantGIdCombo.equals(vc[m].getVariant_group_id()) && pc[j].getPrice()==Double.parseDouble(pricingCombinationPriceFrmDb[m]));
                	{System.out.println("test===");vgCount++;
                    }
                    if(pCVariant[m].equals(vc[m].getVariant_id()))
                    {System.out.println("test"); variantionCount++;}
                   }
               
                if(vgCount==vc.length && variantionCount==vc.length)
                	success &=true;
    	   	}
             } 
               
               if(pCaddonLength!=null)
               {
    	 	for(int j=0;j<pCaddonLength.length;j++)
    	   	{
    	 		int agCount=0,addonCount=0;
    	    pCAddonGIdCombo = JsonPath.read(map.get("variants_v2").toString(),"$.pricing_models["+i+"].addon_combinations["+j+"].third_party_group_id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
		    pCAddonIdCombo = JsonPath.read(map.get("variants_v2").toString(),"$.pricing_models["+i+"].addon_combinations["+j+"].third_party_addon_id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
		   
		    for(int m = 0;m<vc.length;m++)
            { 
          	if(pCAddonGIdCombo[m].equals(ac[m].getAddon_group_id()));
          	{System.out.println("test");agCount++;}
          	if(pCAddonIdCombo[m].equals(ac[m].getAddon_id()))
          	{System.out.println("test");addonCount++;}
          
             }
          
		     if(agCount==vc.length && addonCount==vc.length)
             	success &=true;
    	   	}
    	   	}
    	}
    	}
	
		 
  return success;
}



public boolean validateDetailedItemsSlots(Map<String, Object> map, String store_id,Items[] items,boolean success,int itemCount)  throws  Exception{
	 List<Map<String, Object>> mapSlot = cmsHelper.getItemSlot(store_id,items[itemCount].getId());
	int sloutdb_count=0;
	
	 // id,item_id,day,open_time,close_time,active
	 Item_Slots[] itemSlots=items[itemCount].getItem_slots();
		String day=null;
	 if (items[itemCount].getItem_slots()!=null)
	 {
			
			 for (int i=0;i<itemSlots.length;i++)
		      {
				 if(itemSlots[i].getDay_of_week()==1)day="mon";
				 else if(itemSlots[i].getDay_of_week()==2)day = "Tue";
				 else if(itemSlots[i].getDay_of_week()==3)day = "Wed";
				 else if(itemSlots[i].getDay_of_week()==4)day = "Thu";
				 else if(itemSlots[i].getDay_of_week()==5) day = "Fri";
				 else if(itemSlots[i].getDay_of_week()==6)day = "Sat";
				 else if(itemSlots[i].getDay_of_week()==7)day = "Sun";
			     else if(itemSlots[i].getDay_of_week()==8)day = "Sat";
			     else day = null;
			    
				 if(mapSlot.get(i).get("open_time").equals(itemSlots[i].getOpen_time()) && mapSlot.get(i).get("close_time").equals(itemSlots[i].getClose_time())
						&& mapSlot.get(i).get("day").equals(day) && (int)mapSlot.get(i).get("active")==1 )
					 sloutdb_count++;
		      }
				
			 if(itemSlots.length==sloutdb_count)
				 success &=true;
 }
	
return success;
}


//success &=validateDetailedGst(map, store_id,items,success,i,"ITEM");
public boolean validateDetailedGst(Items[] items,boolean success,int itemCount,String entityType,String externalId)  throws  Exception{
	 List<Map<String, Object>> mapGst = cmsHelper.getCatlogGstDetails(externalId);
	Gst_Details gstdetails;
	if(entityType=="ITEM")
		 gstdetails=items[itemCount].getGst_details();
	else if (entityType=="VARIANT")
		 gstdetails=items[itemCount].getGst_details();
	else if (entityType=="ADDON")
		 gstdetails=items[itemCount].getGst_details();
	else
		gstdetails=null;
 if (items[itemCount].getGst_details()!=null)
	 {
		 
		 
				 if(mapGst.get(0).get("cgst").toString().equals(Double.toString(gstdetails.getCgst())+"*[SUBTOTAL]") && mapGst.get(0).get("igst").equals(Double.toString(gstdetails.getIgst())+"*[SUBTOTAL]")
						&& mapGst.get(0).get("sgst").equals(Double.toString(gstdetails.getSgst())+"*[SUBTOTAL]")  && (int)mapGst.get(0).get("active")==1 
								&& mapGst.get(0).get("inclusive").equals(gstdetails.isInclusive())
						&& (mapGst.get(0).get("entity_type").equals(entityType))
								)
				success &= true;
	    
      }
	
return success;
}
}


