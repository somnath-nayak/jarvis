package com.swiggy.api.erp.cms.helper;

import java.util.HashMap;

import com.swiggy.api.erp.cms.constants.SelfServeConstants;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

public class AgentHelper {
	 Initialize gameofthrones = new Initialize();

	    private HashMap<String, String> getAgentControllerHeaders(String appSecret) {
	        HashMap<String, String> headers =  new HashMap<>();
	        headers.put("Content-Type", "application/json");
	        headers.put("Authorization",SelfServeConstants.agent_authorization_key);
	        headers.put("app-secret",SelfServeConstants.app_secret);
	        return headers;
	    }

	    private HashMap<String, String> getAgentControllerHeader(String auth_key) {
	        HashMap<String, String> headers =  new HashMap<>();
	        headers.put("Content-Type", "application/json");
	        headers.put("Authorization",SelfServeConstants.agent_authorization_key);
	        headers.put("app-secret",SelfServeConstants.app_secret);
	        headers.put("auth-key",auth_key);
	        return headers;
	    }

	    public Processor createAgent(String agent_type,String auth_user_id,String auth_key,String appSecret) {
	        HashMap<String, String> requestheaders =getAgentControllerHeaders(appSecret);
	        GameOfThronesService service = new GameOfThronesService("cmsagentservice", "createAgent", gameofthrones);
	        String[] payload={agent_type,auth_user_id,auth_key};
	        Processor processor = new Processor(service, requestheaders,payload);
	        return processor;
	    }
	    
	    public Processor availableUnavailableAgentByAuthUserID(String auth_user_id, boolean available,String authKey,String appSecret) {
	        String[] params = {auth_user_id, String.valueOf(available)};
	        GameOfThronesService gots = new GameOfThronesService("cmsagentservice", "availableunavailableagent", gameofthrones);
	        Processor processor = new Processor(gots,getAgentControllerHeader(authKey) ,null,params);
	        return processor;
	    }

}

