package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "pricing_models",
        "variant_groups"
})
public class Variants_v2 {

    @JsonProperty("pricing_models")
    private List<Pricing_model> pricing_models = null;
    @JsonProperty("variant_groups")
    private List<Variant_group_> variant_groups = null;

    @JsonProperty("pricing_models")
    public List<Pricing_model> getPricing_models() {
        return pricing_models;
    }

    @JsonProperty("pricing_models")
    public void setPricing_models(List<Pricing_model> pricing_models) {
        this.pricing_models = pricing_models;
    }

    @JsonProperty("variant_groups")
    public List<Variant_group_> getVariant_groups() {
        return variant_groups;
    }

    @JsonProperty("variant_groups")
    public void setVariant_groups(List<Variant_group_> variant_groups) {
        this.variant_groups = variant_groups;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("pricing_models", pricing_models).append("variant_groups", variant_groups).toString();
    }

}
