package com.swiggy.api.erp.cms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/*
 * Area scheduling can be created from this class and same details can be updated for area-code=3 (HSR)
 */

public class POP_Restaurant_Menu_Map_Helper {
	Initialize gameofthrones = new Initialize();

	public HashMap<String, String> setHeaders() throws JSONException {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		JSONObject json = new JSONObject();
		json.put("source", "cms");
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("user-meta", json.toString());
		return requestheaders;
	}

	public String[] setPayload1(int restaurant_id, int menu_id, String menu_type) throws JSONException {
		return new String[] { "" + restaurant_id, "" + menu_id, menu_type };
	}

	public Processor createRestaurantMenuMapRegular(int restaurant_id, int menu_id, String menu_type)
			throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "restaurantmenumapcreate",
				gameofthrones);
		Processor p = new Processor(service1, setHeaders(), setPayload1(restaurant_id, menu_id, menu_type), null);
		return p;
	}

	public Processor createRestaurantMenuMapPop(int restaurant_id, int menu_id, String menu_type) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "restaurantmenumapcreate",
				gameofthrones);
		Processor p = new Processor(service1, setHeaders(), setPayload1(restaurant_id, menu_id, menu_type), null);
		return p;
	}

	public Processor getRestaurantMenuMapRegular(int restaurantMenuMapRegularId) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "restaurantmenumapget",
				gameofthrones);
		Processor p1 = new Processor(service1, setHeaders(), null, new String[] { "" + restaurantMenuMapRegularId });
		return p1;
	}

	public Processor getRestaurantMenuMapPop(int restaurantMenuMapPopId) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "restaurantmenumapget",
				gameofthrones);
		Processor p1 = new Processor(service1, setHeaders(), null, new String[] { "" + restaurantMenuMapPopId });
		return p1;
	}

	public Processor deleteRestaurantMenuMapRegular(int restaurantMenuMapRegularId) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "restaurantmenumapdelete",
				gameofthrones);
		Processor p1 = new Processor(service1, setHeaders(), null, new String[] { "" + restaurantMenuMapRegularId });
		return p1;
	}

	public Processor deleteRestaurantMenuMapPop(int restaurantMenuMapPopId) throws JSONException {
		// String getId = Long.toString(id);
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "restaurantmenumapdelete",
				gameofthrones);
		Processor p1 = new Processor(service1, setHeaders(), null, new String[] { "" + restaurantMenuMapPopId });
		return p1;
	}

	public Processor createRestaurantMenuMapPop1(String menu_type, int parent_category_id, int restId, String type,
			String name) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "restaurantmenumapcreate1",
				gameofthrones);
		System.out.println("test" + name);
		Processor p = new Processor(service1, setHeaders(),
				setPayload2(menu_type, parent_category_id, restId, type, name), null);
		return p;
	}

	public String[] setPayload2(String menu_type, int parent_category_id, int restaurant_id, String type, String name)
			throws JSONException {
		return new String[] { menu_type, Integer.toString(parent_category_id), Integer.toString(restaurant_id), type,
				name };
	}

}
