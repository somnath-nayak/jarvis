package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.dash.Tripmanager.helper.TripManagerHelper;
import com.swiggy.api.erp.delivery.constants.DEAlchemistConstants;
import com.swiggy.api.erp.delivery.helper.*;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;

public class SingleCAPRDTest {
    DeliveryServiceApiHelper deliveryServiceApiHelper = new DeliveryServiceApiHelper();
    DeliveryServiceApiWrapperHelper deliveryServiceApiWrapperHelper = new DeliveryServiceApiWrapperHelper();
    DeliveryDataHelper dataHelper = new DeliveryDataHelper();

    @Test(description = "Verfiy CAPRD for order pushed in swiggy_orders queue", enabled = true)
    public void verifyZipDialCAPRD() {
        //String de_id=deliveryServiceApiHelper.createDEandmakeDEActive(4);
        String de_id = "98125";
        HashMap<String, String> orderjson = new HashMap<>();
        String order_id = deliveryServiceApiHelper.createNewOrder(orderjson);
        dataHelper.updateDELocationInRedis(de_id);
        //deliveryServiceApiHelper.assignDE(order_id, de_id);
        //deliveryServiceApiHelper.assignDE(order_id, de_id);
        //dataHelper.doCAPRD(order_id,de_id,"confirmed","delivered");
    }

    @Test(description = "Verfiy CAPRD for order pushed in swiggy_orders queue", enabled = true)
    public void verifyServiceCAPRD() {
        String de_id = deliveryServiceApiHelper.createDEandmakeDEActive(4);
        HashMap<String, String> orderjson = new HashMap<>();
        String order_id = deliveryServiceApiHelper.createNewOrder(orderjson);
        dataHelper.updateDELocationInRedis(de_id);
        String auth = deliveryServiceApiWrapperHelper.returnAuthForDE(de_id);
        deliveryServiceApiHelper.assignDE(order_id, de_id);
        deliveryServiceApiWrapperHelper.statusUpdate(order_id, "delivered", auth);
    }

    @Test(description = "Verfiy CAPRD for order pushed in dc.orders_delivery queue", enabled = true)
    public void verifyZipDialCAPRDFORSLD() {
        //String de_id=deliveryServiceApiHelper.createDEandmakeDEActive(4);
        String de_id = "88651";
        HashMap<String, String> orderjson = new HashMap<>();
        String order_id = deliveryServiceApiHelper.createNewOrderSLD(orderjson);
        dataHelper.updateDELocationInRedis(de_id);
       // deliveryServiceApiHelper.assignDE(order_id,de_id);

        dataHelper.doCAPRD(order_id,de_id,"confirmed","delivered");
    }
    @Test(description = "Verfiy CAPRD for order pushed in swiggy_orders queue", enabled = true)
    public void verifyZipDialCAPRDFORSLDNEW() {
        TripManagerHelper tripManagerHelper=new TripManagerHelper();
        tripManagerHelper.getTaskLegBoBasedOnClientOrderId("17165");
    }
}
