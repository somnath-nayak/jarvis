package com.swiggy.api.erp.crm.dp.foodissues;


import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.constants.MockServiceConstants;
import com.swiggy.api.erp.crm.foodissues.pojo.CCResolution.*;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import framework.gameofthrones.Tyrion.RedisHelper;
import com.swiggy.api.erp.crm.foodissues.pojo.CCResolution.resolutionRequestBuilder;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;

public class GratificationDP {

    public static Initialize gameofthrones = new Initialize();
    public static resolutionRequestBuilder resolutionRequestBuilder = new resolutionRequestBuilder();
    public static MockServiceConstants mockServiceConstants = new MockServiceConstants();

    public static HashMap<Object,Object> couponCreate() throws Exception{
        String orderId = "895292";
        System.out.println("orderId marked as deliverd    " +orderId);

        HashMap<String, String> requestheaders_igcccoupon = new HashMap<String, String>();
        requestheaders_igcccoupon.put("Content-Type", "application/json");

        GameOfThronesService igcccoupon = new GameOfThronesService("crmigcc", "coupon", gameofthrones);

        Date date = new Date();
        DateFormat coupondFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String coupon = coupondFormat.format(date);

        String[] payload = new String[]{"igcc"+coupon+"", "IGCC"+coupon+""};

        Processor igcccoupon_response = new Processor(igcccoupon, requestheaders_igcccoupon, payload);
        System.out.println("igcc coupon response  " + igcccoupon_response);

        Assert.assertEquals(igcccoupon_response.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
        Assert.assertNotNull(igcccoupon_response.ResponseValidator.GetNodeValueAsInt("data.id"));

        int couponId = igcccoupon_response.ResponseValidator.GetNodeValueAsInt("data.id");
        String couponCode = igcccoupon_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.code");
        String couponDescription = igcccoupon_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.description");
        int couponAmount = igcccoupon_response.ResponseValidator.GetNodeValueAsInt("data.discount_amount");

        HashMap<Object,Object> couponData = new HashMap<>();

        couponData.put("couponId",couponId);
        couponData.put("couponCode",couponCode);
        couponData.put("couponDescription",couponDescription);
        couponData.put("couponAmount",couponAmount);

        return couponData;
    }

    @DataProvider(name = "replicateRequest")
    public static Object[][] replicateRequest() throws Exception {

        String itemId=mockServiceConstants.getUniqueItemId();
        String restId=mockServiceConstants.getUniquerestaurantId();
        String orderId=mockServiceConstants.getUniqueOrderId().substring(0, Math.min(13, 10));
        String cost = "1";
        String order_status="delivered";

        List<CartItem> cartItems = new ArrayList<CartItem>();
        cartItems.add(new CartItem(itemId, 1));
        return new Object[][]{
                {resolutionRequestBuilder.replicateRequest(Integer.parseInt(orderId),393325,restId,cartItems),orderId,restId,itemId,cost,"regular",order_status,"TAKEAWAY"}
        };
    }

    @DataProvider(name = "couponRequest")
    public static Object[][] couponRequest() throws Exception {

        HashMap<Object,Object> getCouponData = couponCreate();
        return new Object[][]{
                {resolutionRequestBuilder.couponRequest(895292,387395,CRMConstants.userIDForCoupon, getCouponData.get("couponCode").toString(),getCouponData.get("couponDescription").toString(),
                        (Integer) getCouponData.get("couponId"),(Integer) getCouponData.get("couponAmount"))}
        };
    }

    @DataProvider(name = "refundRequest")
    public static Object[][] refundRequest() throws Exception {

        return new Object[][]{
                {resolutionRequestBuilder.refundRequest(895292,387395,CRMConstants.userIDForCoupon,"PG","test refund",1)}
        };
    }
}
