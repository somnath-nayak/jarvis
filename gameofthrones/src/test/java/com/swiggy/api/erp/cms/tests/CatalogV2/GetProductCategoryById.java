package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetProductCategoryById  {
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
    String catId;
    @Test(description = "Get Category by ID")
    public void getCategoryById(){
        String response=catalogV2Helper.createCategoryPojo().ResponseValidator.GetBodyAsText();
        catId=JsonPath.read(response,"$.id");
        String response1=catalogV2Helper.getCategoryById(catId).ResponseValidator.GetBodyAsText();
        String catId2=JsonPath.read(response1,"$.id");
        Assert.assertNotNull(catId2);
    }

}
