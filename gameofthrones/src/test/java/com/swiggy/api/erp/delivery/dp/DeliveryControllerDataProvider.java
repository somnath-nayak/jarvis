package com.swiggy.api.erp.delivery.dp;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

public class DeliveryControllerDataProvider {

	static String order_id;

	@DataProvider(name = "zonepartnerupdate")
	public static Object[][] partnermappingupdatezone() {

		return new Object[][] {

			{ "2", "15", "10", 500},
			{ "2", "12", " ", 500},
			{ "2", " ", "9", 200},
			{ " ", "12", "9", 200},
			{ " ", " ", " ", 500},
			{ "2", "12","9", 200}

		};
	}

	@DataProvider(name = "citypartnerupdate")
	public static Object[][] partnermappingupdatecity() {

		return new Object[][] {

			{ "1", "12", "8","100","8093292223",200},
			{ " ", "12", "8","100","8093292223",200},
			{ "1", " ", "8","100","8093292223",200},
			{ "1", "12", "1000","100","8093292223",500},
			{ "1", "12", " ","100","8093292223",500},
			{ "1", "12", "8"," ","8093292223",1},
			{ "1", "12", "8","100.0","8093292223",1},
			{ "1", "12", "8","100"," ",200}
		};
	}

	@DataProvider
	public static Object[][] AddPartnerData() {
		return new Object[][] {

			{"1", "PizzaHut"},
			{"0", "Dominos"}

		};
	}

	public static String createOrder() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems();
		Processor processor = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return order_id = processor.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}

	public static Object[][] cartItems() {
		return new Object[][] {

		   { "1064243", "1", "6118" }
	   };
	}

	@DataProvider
	public static Object[][] ReAssignData() throws Exception {

		order_id = createOrder();

		return new Object[][] {

			{order_id,"1", "1", 0},
			{" ", "1", "1", 500},
			{order_id, " ", "1", 500},
			{order_id, "1", " ", 500},
			{" ", " "," ",500},
			{"12234253453","1","1",500}
		};
	}
		 @DataProvider(name = "zonepartneradd")
		    public static Object[][] AddPartnerZone() {

		        return new Object[][]
		        	{
		        	     { "2", "15", "10", 200},
		                 { "2", "12", "''" , 500},
		                 { "2", "' '", "9", 500},
		                 { "'' ", "12", "9", 500},
		                 { "'' ", " ", " ", 500},
		                 { "2", "12","9", 200}
		        		
			        };
		         }
			
			@DataProvider(name = "citypartneradd")
		    public static Object[][] AddPartnerCity() {

		        return new Object[][]
		        		{
		        	{ "1", "12", "8","100","8093292223",200},
	             { "''", "12", "8","100","8093292223",500},
	             { "1", "''", "8","100","8093292223",500},
	             { "1", "12", "1000","100","8093292223",200},
	             { "1", "12", "''","100","8093292223",500},
	             { "1", "12", "8"," ","8093292223",1},
	             { "1", "12", "8","100.0","8093292223",1},
	             { "1", "12", "8","100","",200}
		        		
			            };
		         }
			@DataProvider(name = "zonepartnerdelete")
		    public static Object[][] DeletePartnerZone() {

		        return new Object[][]
		        	{
		        	{ "10",500},
		        	{ " ",500},
		        	{ "1.0",500},
			        };
		         }
			
			@DataProvider(name = "citypartnerdelete")
		    public static Object[][] DeletePartnerCity() {

		        return new Object[][]
		        		{
		        	{ "8",200},
		        	{ " ",500},
		        	{ "1.0",500},        
	             
		          };
		         }
	}



