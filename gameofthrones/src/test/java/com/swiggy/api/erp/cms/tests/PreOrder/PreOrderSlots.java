package com.swiggy.api.erp.cms.tests.PreOrder;

import com.swiggy.api.erp.cms.dp.PreOrderDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class PreOrderSlots extends PreOrderDP {
    CMSHelper cmsHelper= new CMSHelper();
    SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("cms");

        @Test(dataProvider = "preorderslots",description = "Adding pre order slots to restaurant")
        public void preOrderSlots(String rest_id, String day, String openTime, String closeTime){
        cmsHelper.preorderSlots(rest_id,day,openTime,closeTime);
        sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
        List<Map<String, Object>> sched=sqlTemplateCI.queryForList("select `rest_id`,`type` from `restaurants_schedule` where `rest_id`="+rest_id+"");
        Assert.assertNotNull(sched);

    }
}
