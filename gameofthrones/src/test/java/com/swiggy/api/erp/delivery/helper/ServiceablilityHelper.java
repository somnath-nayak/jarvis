package com.swiggy.api.erp.delivery.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.Serviceability_Constant;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.apache.noggit.JSONUtil;
import org.apache.solr.common.SolrDocumentList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.AssertionFailedError;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.unitils.reflectionassert.ReflectionAssert;
import org.unitils.reflectionassert.ReflectionComparatorMode;

import java.io.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class ServiceablilityHelper implements Serviceability_Constant {
	//Initialize gameofthrones = new Initialize();
	Initialize gameofthrones =Initializer.getInitializer();
	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();

	Serviceability_Helper serviceablility_Helper = new Serviceability_Helper();
	SnDHelper sndhelp = new SnDHelper();
	CheckoutHelper chelper = new CheckoutHelper();
	AddressHelper ahelper=new AddressHelper();
	CMSHelper cmsHelper = new CMSHelper();
	DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
	SolrHelper solrHelper = new SolrHelper();
	RedisHelper redis = new RedisHelper();
	String token = null;
	String tid = null;
	String RecheckServiceability = null;
	public static String errorMessage = "";

	public boolean serviceable(String lat, String lng, String city_id, String restaurant_ids)
			throws InterruptedException {
		String query = "select * from restaurant r inner join area a on r.area_code=a.id where r.id in ("
				+ restaurant_ids + ")" + ";";
		Object obj = delmeth.dbhelperget(query, "zone_id");
		if (null == obj) {
			return false;
		} else {
			String zoneid = obj.toString();
			delmeth.redisconnectsetdouble(DeliveryConstant.bannerrediskey, zoneid, DeliveryConstant.redisdb,
					DeliveryConstant.bf,"deliveryredis");
			GameOfThronesService service = new GameOfThronesService("deliverycerebro", "newlisting", gameofthrones);
			HashMap<String, String> requestheaders = delmeth.singleheader();
			String[] payloadparams = new String[] { lat, lng, city_id, restaurant_ids };
			Thread.sleep(20000);
			Processor processor = new Processor(service, requestheaders, payloadparams);
            return (processor.ResponseValidator.GetResponseCode()) == 200;
		}
	}
	public Processor listing(String[] payload)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","listingCluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
		return processor;
	}

	public Processor listingDaily(String[] payload)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","listings_daily",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
		return processor;
	}

	public Processor listingWithRestaurant(String[] payload)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","listingViaCluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
		return processor;
	}

	public Processor DSserviceable(String lat, String lng, String city_id, String restaurant_ids)
			throws InterruptedException {
		String query = "select * from restaurant r inner join area a on r.area_code=a.id where r.id in ("
				+ restaurant_ids + ")" + ";";
		Object obj = delmeth.dbhelperget(query, "zone_id");
		if (null == obj) {
			return null;
		} else {
			String zoneid = obj.toString();
			delmeth.redisconnectsetdouble(DeliveryConstant.bannerrediskey, zoneid, DeliveryConstant.redisdb,
					DeliveryConstant.bf,"deliveryredis");
			GameOfThronesService service = new GameOfThronesService("deliverycerebro", "newlisting", gameofthrones);
			HashMap<String, String> requestheaders = delmeth.singleheader();
			String[] payloadparams = new String[] { lat, lng, city_id, restaurant_ids };
			Thread.sleep(20000);
			Processor processor = new Processor(service, requestheaders, payloadparams);
			return processor;
		}

	}

	public boolean serviceablewithbanner(String lat, String lng, String city_id, String restaurant_ids)
			throws InterruptedException {
		String zoneid = null;
		String query = "select * from restaurant r inner join area a on r.area_code=a.id where r.id in ("
				+ restaurant_ids + ")" + ";";
		Object obj = delmeth.dbhelperget(query, "zone_id");
		if (null == obj) {
			return false;
		} else {
			zoneid = obj.toString();

			String query1 = "update zone set is_open=0 where id=" + zoneid + ";";
			delmeth.dbhelperupdate(query1);
			Thread.sleep(20000);
			GameOfThronesService service = new GameOfThronesService("deliverycerebro", "newlisting", gameofthrones);
			HashMap<String, String> requestheaders = delmeth.singleheader();
			String[] payloadparams = new String[] { lat, lng, city_id, restaurant_ids };
			Processor processor = new Processor(service, requestheaders, payloadparams);
			String query2 = "update zone set is_open=1 where id=" + zoneid + ";";
			delmeth.dbhelperupdate(query2);
            return (processor.ResponseValidator.GetResponseCode()) == 200;
		}
	}


	public void updateLastMileForZone(int mile, int zoneId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE zone SET last_mile_cap="+mile+" WHERE id="+zoneId+"");
	}

	public void updateLastMileForArea(int mile, int areaId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE area SET last_mile_cap="+mile+" WHERE id="+areaId+"");
	}

	public void updateDeliveryTimeForZone(int time, int zoneId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET max_delivery_time="+time+" WHERE id="+zoneId+"");
	}
	public void updateEnabledForZone(int enabled, int zoneId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET enabled="+enabled+" WHERE id="+zoneId+"");
	}
	public void updateEnabledForArea(int enabled, int areaId){
		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.area SET enabled="+enabled+" WHERE id="+areaId+"");
	}
	public void updateMaxItemsInBatchForZone(int enabled, int zoneId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET enabled="+enabled+" WHERE id="+zoneId+"");
	}
	public void updateEnableTempLastMile(int enabled, int zoneId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET temp_lm_enabled="+enabled+" WHERE id="+zoneId+"");
	}
	public void updateZoneTempLastMileForZone(int mile, int zoneId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET temp_last_mile="+mile+" WHERE id="+zoneId+"");
	}
	public void updateRainModeTypeForZone(int rainModeType, int zoneId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET rain_mode_type="+rainModeType+" WHERE id="+zoneId+"");
	}
	public void updateRainModeTypeForRestaurant(int rainModeType, int restId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.restaurant SET rain_mode_type="+rainModeType+" WHERE id="+restId+"");
	}
	public void updateEnabledForRestaurant(int enabled, String restId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.restaurant SET enabled="+enabled+" WHERE id="+restId+"");
	}
	public void updateLastMileForRestaurant(int mile, int restId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.restaurant SET last_mile="+mile+" WHERE id="+restId+"");
	}
	public void updateMaxSecondMileForRestaurant(int enabled, String restId){

		SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.restaurant SET max_second_mile="+enabled+" WHERE id="+restId+"");
	}
	/**
	 * Delivery Listing
	 * @param lat
	 * @param lng
	 * @param resIds
	 * @return Processor
	 */
	public Processor serviceabilityListing(String lat, String lng, String city_id, String[] resIds){
		GameOfThronesService service = new GameOfThronesService(
				"deliverycerebro", "listing", gameofthrones);
		HashMap<String, String> requestHeaders = delmeth.singleheader();
		String[] payloadParams = new String[] { lat, lng, city_id, String.join(",", resIds),  "0", "0", "0", "0", "0", "C", "0", "H"};
		Processor processor = new Processor(service, requestHeaders, payloadParams);
		return processor;
	}

	/**
	 * Serviceability Cart SLA
	 * @param itemCount
	 * @param is_long_distance_enabled
	 * @param restaurant_id
	 * @param area_id
	 * @param city_id
	 * @param restaurant_type
	 * @param lat_long
	 * @param address_lat_long
	 * @param value_segment
	 * @param premium_segment
	 * @param high_eng_segment
	 * @param contact_affinity
	 * @param cancellation_probability
	 * @param churn_probability
	 * @param marketing_segment
	 * @param new_user
	 * @return {Processor}
	 */
	public Processor serviceabilityCartSLA(String itemCount, String is_long_distance_enabled, String restaurant_id,
										   String area_id, String city_id, String restaurant_type, String lat_long,
										   String address_lat_long, String value_segment, String premium_segment,
										   String high_eng_segment, String contact_affinity, String cancellation_probability,
										   String churn_probability, String marketing_segment, String new_user){
		GameOfThronesService service = new GameOfThronesService(
				"deliverycerebro", "cartsla", gameofthrones);
		HashMap<String, String> requestHeaders = delmeth.singleheader();
		String[] payloadParams = new String[] {itemCount, is_long_distance_enabled, restaurant_id, area_id, city_id,
				restaurant_type, lat_long, address_lat_long, value_segment, premium_segment, high_eng_segment,
				contact_affinity, cancellation_probability, churn_probability, marketing_segment, new_user};
		Processor processor = new Processor(service, requestHeaders, payloadParams);
		return processor;
	}


	public Processor serviceabilityCartSLAMinParam(String restaurant_id,
												   String area_id, String city_id, String lat_long,
												   String address_lat_long){
		GameOfThronesService service = new GameOfThronesService(
				"deliverycerebro", "cartsla", gameofthrones);
		HashMap<String, String> requestHeaders = delmeth.singleheader();
		String[] payloadParams = new String[] {"1", "true", restaurant_id, area_id, city_id,
				"F", lat_long, address_lat_long, "H", "0", "0",
				"0", "0", "0", "A", "0"};
		Processor processor = new Processor(service, requestHeaders, payloadParams);
		return processor;
	}

	/**
	 * Serviceability Menu
	 * @param resId
	 * @param lat
	 * @param lng
	 * @return Processor
	 */
	public Processor serviceabilitySandMenu(String resId, String lat, String lng){

		GameOfThronesService service = new GameOfThronesService(
				"deliverycerebro", "sandmenu", gameofthrones);
		HashMap<String, String> requestHeaders = delmeth.singleheader();
		String[] urlParams = new String[] { resId, lat, lng};
		Processor processor = new Processor(service, requestHeaders, null, urlParams);
		return processor;
	}


	/**
	 * Delivery Serviceability Aggregator
	 * @param lat
	 * @param lng
	 * @retun Processor
	 */
	public Processor serviceabilityAggregator(String lat, String lng){
		GameOfThronesService service = new GameOfThronesService(
				"sand", "aggregator", gameofthrones);
		HashMap<String, String> requestheaders = delmeth.singleheader();
		String[] payloadParams = new String[] { lat+","+lng };
		Processor processor = new Processor(service, requestheaders, payloadParams);
		return processor;
	}





	// Listing API
	public String serviceabilityCheckListing(String lat, String lng, String restaurantId) throws Exception {
		System.out.println("******************************ListingTest is started******************************");
		Processor processor = sndhelp.getAggregatorDetails(lat, lng);
		List<String> listRestId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
				"$.data.restaurants[*].sla.restaurantId");
		List<String> listServiceability = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
				"$.data.restaurants[*].sla.serviceability");
		HashMap<Object, Object> SndHashMap = new HashMap();

		for (int i = 0; i < listRestId.size(); i++) {
			SndHashMap.put(listRestId.get(i), listServiceability.get(i));
		}

		if(SndHashMap.containsKey(restaurantId)){
			return SndHashMap.get(restaurantId).toString();
		}
		return null;
	}

	public Processor getAddressId() {
		/*String Mobile_no = sandHelper.getMobile();
		HashMap<String, String> userDetails = sandHelper.createUser(DeliveryConstant.user_name, Mobile_no, DeliveryConstant.user_email, DeliveryConstant.user_password);
		tid = userDetails.get("tid");
		token = userDetails.get("token");
		String[] test= new String[]{DeliveryConstant.user_mobile,DeliveryConstant.user_password};
		Processor loginResponse = sandHelper.login(test);*/
		Processor loginResponse = SnDHelper.consumerLogin(DeliveryConstant.user_mobile, DeliveryConstant.user_password);
		tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		Processor addressID = ahelper.NewAddress(tid, token, "Somnath", DeliveryConstant.user_mobile, "Marthalli Bridge ", "Marthalli", "6", "12.957213", "77.702837", "1", "Bangalore", "Home");
		return addressID;
	}

	// Cart API
	public HashMap<String,Integer> serviceabilityCheckCart(String restaurantId, String area_id, String itemId, String quantity) throws Exception {
		System.out.println("******************************Cart Serviceabilty Check******************************");

		Processor loginResponse = SnDHelper.consumerLogin(DeliveryConstant.mobile2, DeliveryConstant.password2);
		tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		//ahelper.NewAddress(tid, token, "Somnath", DeliveryConstant.mobile, "Marthalli Bridge ", "Marthalli", "6", "12.9569","77.7015", "1", "Bangalore", "Home");
		Processor cart = chelper.invokeCreateCartAPI(tid, token, itemId, quantity, restaurantId);
		List<Integer> listServiceability = JsonPath.read(cart.ResponseValidator.GetBodyAsText(),
				"$..delivery_valid");
		List<String> listAddressId=JsonPath.read(cart.ResponseValidator.GetBodyAsText(),
				"$..addresses..id");
		HashMap<String,Integer> resDetails =new HashMap();

		for (int i=0;i<listAddressId.size();i++)
		{
			resDetails.put(listAddressId.get(i),listServiceability.get(i));
		}

		return resDetails;

	}

	// Menu API
	public String serviceabilityCheckMenu(String lat, String lng, String restaurantId, String area_id)
			throws Exception {
		System.out.println("******************************Menu Serviceabilty check started******************************");

		Processor loginResponse = SnDHelper.consumerLogin(DeliveryConstant.mobile2, DeliveryConstant.password2);
		tid = loginResponse.ResponseValidator.GetNodeValue("tid");
		token = loginResponse.ResponseValidator.GetNodeValue("data.token");
		Processor cartResponse = SnDHelper.menuV3(restaurantId, lat, lng, tid, token);
		String Serviceability = JsonPath.read(cartResponse.ResponseValidator.GetBodyAsText(),
				"$..serviceability");
		return Serviceability;

	}

	public Processor cerebroListingWithRestId(String lat, String lng, String restaurant_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","jarvisListing",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, DeliveryConstant.headers(), new String[] {lat, lng, restaurant_id});
		return processor;
	}

	public Processor cerebroListing(String lat, String lng, String city_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","listing",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, DeliveryConstant.headers(), new String[] {lat, lng, city_id});
		return processor;
	}


	public void makeServiceable(String restaurant_id)
	{
		//String[] RestLatLng = cmsHelper.getLatLngByRestID(restaurant_id).split(",");
		//String[] RestLatLng = getRestaurantLatlng(restaurant_id).split(",");
		String[] RestLatLng = solrHelper.getAttributeValueFromSolr("core_listing", "id:" + restaurant_id, "place").replace("\"", "").split(",");
		String[] ApproxCustomerLatLng = deliveryDataHelper.getApproxLatLongAtADistance(Double.parseDouble(RestLatLng[0]), Double.parseDouble(RestLatLng[1]), 1.0 ).split(",");
		Processor processor = cerebroListingWithRestId(ApproxCustomerLatLng[0], ApproxCustomerLatLng[1], restaurant_id);
		String response = processor.ResponseValidator.GetBodyAsText();
		String serviceability = JsonPath.read(response,"$..listingServiceabilityResponse.serviceability").toString();
		System.out.println("Serviceability is"+ serviceability);
		if(!serviceability.equals("[2]")){
			String non_serviceable_reason = JsonPath.read(response, "$..listingServiceabilityResponse.non_serviceable_reason").toString();
			System.out.println("Non-Serviceable Reason is"+ non_serviceable_reason);
			int iteration=1;
			do{
				String area_id =null;
				String zone_id =null;
				String city_id =null;

				Map<String,Object> restMap= SystemConfigProvider.getTemplate("deliverydb").queryForMap("select a.id as area_id,a.city_id,a.zone_id from restaurant r inner join area a on a.id=r.area_code where r.id="+restaurant_id);
				if (restMap!=null){
					area_id = restMap.get("area_id").toString();
					zone_id = restMap.get("zone_id").toString();
					city_id = restMap.get("city_id").toString();
				}
				else
				{
					System.out.println("********** Restaurant Not Found **********");
					return;
				}
				switch (non_serviceable_reason) {
					case "[0]":
						System.out.println("Restaurant is non-serviceable because of LAST_MILE");
						SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.area SET last_mile_cap = 10.00 WHERE id ="+area_id);
						SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET temp_lm_enabled=0,rain_mode_type=3,rain_mode=0 WHERE id="+zone_id);
						serviceablility_Helper.clearCache("area", area_id);
						serviceablility_Helper.clearCache("zone", zone_id);
						//Thread.sleep(5000);
						threadWait(5000);
						break;

					case "[1]":
						System.out.println("Restaurant is non-serviceable because of MAX_SLA");
						SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.city SET max_delivery_time=85 WHERE id ="+city_id);
						SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET max_delivery_time=100,rain_mode=0,rain_mode_type=3 WHERE id ="+zone_id);
						serviceablility_Helper.clearCache("city", city_id);
						serviceablility_Helper.clearCache("zone", zone_id);
						//Thread.sleep(5000);
						threadWait(5000);
						break;

					case "[2]":
						System.out.println("Restaurant is non-serviceable because of ITEMS_EXCEED");
						System.out.println("*************** listing_item_count limit is exceeded ***************");
						break;

					case "[3]":
						System.out.println("Restaurant is non-serviceable because of RAIN");
						SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET rain_mode_type=3,rain_mode=0 WHERE id="+zone_id);
						serviceablility_Helper.clearCache("zone", zone_id);
						//Thread.sleep(5000);
						threadWait(5000);
						break;

					case "[4]":
						System.out.println("Restaurant is non-serviceable because of TEMP_LAST_MILE");
						SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET temp_lm_enabled=0 WHERE id="+zone_id);
						serviceablility_Helper.clearCache("zone", zone_id);
						//Thread.sleep(5000);
						threadWait(5000);
						break;

					case "[5]":
						System.out.println("Restaurant is non-serviceable because of ZONE_NOT_OPEN");
						SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET open_time=0,close_time=2359 WHERE id="+zone_id);
						serviceablility_Helper.clearCache("zone", zone_id);
						//Thread.sleep(5000);
						threadWait(5000);
						break;

					case "[6]":
						System.out.println("Restaurant is non-serviceable because of RESTAURANT_NOT_OPEN");
						SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.restaurant SET enabled=1,serviceable=1 WHERE id="+restaurant_id);
						serviceablility_Helper.clearCache("restaurant", restaurant_id);
						//Thread.sleep(7000);
						threadWait(7000);
						break;

					case "[7]":
						System.out.println("Restaurant is non-serviceable because of BANNER_FACTOR");
						redis.setValue("deliveryredis",1,"BL_BANNER_FACTOR_ZONE_"+zone_id,"0.8");
						SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET is_open=1 WHERE id="+zone_id);
						serviceablility_Helper.clearCache("zone", zone_id);
						//Thread.sleep(5000);
						threadWait(5000);
						break;

					case "[8]":
						System.out.println("Restaurant is non-serviceable because of NON_BATCHABLE_ACTIVE_ORDER");
						System.out.println("*************** There is no implementation against this reason ***************");
						break;

					case "[9]":
						System.out.println("Restaurant is non-serviceable because of BLACK_ZONE");
						System.out.println("*************** Restaurants are showing non-serviceable because customer lies under Black Zone ***************");
						break;

					case "[10]":
						System.out.println("Restaurant is non-serviceable because of TIMEOUT");
						System.out.println("*************** Restaurants are showing non-serviceable because its getting timeout just increase hystrix timeout ***************");
						break;

					default:
						System.out.println("Non-Serviceable Reason is Unknown");
				}
				Processor new_processor = cerebroListingWithRestId(ApproxCustomerLatLng[0], ApproxCustomerLatLng[1], restaurant_id);
				String new_response = new_processor.ResponseValidator.GetBodyAsText();
				RecheckServiceability = JsonPath.read(new_response,"$..listingServiceabilityResponse.serviceability").toString();
				iteration++;


			} while (!RecheckServiceability.equals("[2]") && iteration<=7);
			if (RecheckServiceability.equals("[2]")){
				System.out.println("********** Restaurant "+restaurant_id+" is serviceable now **********");
			}
		}else{System.out.println("*************** Restaurant is already Serviceable ***************");}
	}

	public String getRestaurantLatlng(String restaurant_id){
		String solrUrl = delmeth.getSolrUrl();
		SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl, "core_listing", "id:"+ restaurant_id +"");
		String jsonDoc = JSONUtil.toJSON(solrDocuments);
		System.out.println(jsonDoc);
		//String[] restLatLng = JsonPath.read(jsonDoc, "$[*].place").toString().replace("[\"", "").replace("\"]", "").split(",");
		String restLatLng = JsonPath.read(jsonDoc, "$[*].place").toString().replace("[\"", "").replace("\"]", "");
		return restLatLng;
	}

	public void threadWait(long milliseconds){
		try {
			Thread.sleep(milliseconds);
		}catch (Exception e){e.printStackTrace();}

	}
	public boolean verifyJsonDataTypeForListingAPi(String clusterContent, String cerebroContent) {
		boolean result = true;
		try {

			//String content1 = FileUtils.readFileToString(new File("/Users/vijay.s/LocalRepo/Experiment/src/main/java/actual.json"), org.apache.commons.lang3.CharEncoding.UTF_8);
//                String content = processor.ResponseValidator.GetBodyAsText();
			JSONObject jObj = new JSONObject(cerebroContent);
			Object aObj = jObj.get("serviceableRestaurants");
			int totalSize = ((JSONArray)aObj).length();
			System.out.println("Cerebro Total Size : "+totalSize);

			//picking up the random json and verifying the datatype
			int randomNum = ThreadLocalRandom.current().nextInt(0, totalSize);
			System.out.println("randomNum = " + randomNum);
//			JSONObject firstObject = ((JSONArray) aObj).getJSONObject(randomNum);
//			verifyJsonDataTypeForListingAPI(((JSONArray) aObj).getJSONObject(i));
//			System.out.println("Object.toString() = " + firstObject.toString());

			JSONObject clusterContentjObj = new JSONObject(clusterContent);
			Object clusterContentaObj = clusterContentjObj.get("serviceableRestaurants");
			int clusterContenttotalSize = ((JSONArray)clusterContentaObj).length();
			System.out.println("Cluster Total Size : "+clusterContenttotalSize);

			boolean clusterResponseIsFromFallback = verifyClusterResponseIsFromFallback(((JSONArray) clusterContentaObj).getJSONObject(randomNum));
			if(clusterResponseIsFromFallback)
			{
				System.out.println("clusterResponse is coming from fallback = " + clusterResponseIsFromFallback);
				return false;
			}

			if(totalSize==0)
			{
				result= false;
				System.out.println("Cerebro returning empty response = " + totalSize);
				return result;
			}
			if(clusterContenttotalSize==0)
			{
				result= false;
				System.out.println("Cluster Service returning empty response = " + clusterContenttotalSize);
				return result;
			}

			//verifying datatype check for all the json's
			for(int i =0 ; i <totalSize ;i++)
			{
				boolean cerebroServiceable = checkIfRestaurantIsServiceable(((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse"));

				if(cerebroServiceable)
				{
					boolean individualResult = verifyJsonDataTypeForListingAPI(((JSONArray) aObj).getJSONObject(i));

					if(!individualResult)
					{
						int cerebroRestId = (Integer)((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("restaurant_id");
						for(int j =0 ; j <clusterContenttotalSize; j++)
						{
							int clusterRestId = (Integer)((JSONArray) clusterContentaObj).getJSONObject(j).getJSONObject("listingServiceabilityResponse").get("restaurant_id");
							if(cerebroRestId==clusterRestId)
							{
								boolean clusterServiceable = checkIfRestaurantIsServiceable(((JSONArray) clusterContentaObj).getJSONObject(j).getJSONObject("listingServiceabilityResponse"));

								if(cerebroServiceable && clusterServiceable)
								{
									verifyBothJsonObjectValues(((JSONArray) aObj).getJSONObject(i),((JSONArray) clusterContentaObj).getJSONObject(j));
								}else if(!clusterServiceable && !cerebroServiceable)
								{
									System.out.println("both clusterService and cerebro is unServiceable for rest ID= " + clusterRestId + " "+ cerebroRestId);

								}else if (!clusterServiceable)
								{
									System.out.println("cluster is unServiceable for rest ID= " + clusterRestId);
								}else {
									System.out.println("cerebro is unServiceable for rest ID= " + cerebroRestId);
								}

							}
						}
						//System.out.println("The datatype validation failed restID : "+((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("restaurant_id"));
						result = false;
						//	break;
					}else {
						System.out.println("cerebro is unServiceable for rest ID= " + (Integer)((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("restaurant_id"));
					}
				}


			}
		} catch (JSONException je) {
			je.printStackTrace();
//			errorMessage = errorMessage+ jObj.getJSONObject("listingServiceabilityResponse").get("restaurant_id")+" : "+je.toString()+"\n";
			result = false;
			return result;

		} catch (Exception e) {

			e.printStackTrace();
			result = false;
			return result;
		}
		return result;

	}

	private void verifyBothJsonObjectValues(JSONObject cerebroObject, JSONObject clusterObject) {
		//todo update all the checks
		try{
			jsonCompareByString(cerebroObject.toString(),clusterObject.toString(),String.valueOf(cerebroObject.getJSONObject("listingServiceabilityResponse").get("restaurant_id")),String.valueOf(clusterObject.getJSONObject("listingServiceabilityResponse").get("restaurant_id")));
		}catch (JSONException je)
		{
			je.toString();
		}

	}

	private boolean verifyClusterResponseIsFromFallback(JSONObject listingJsonObject)
	{
		boolean result = false;
		try{

			JSONObject jsonObj = listingJsonObject.getJSONObject("listingServiceabilityResponse");
			if (jsonObj == null) {
				System.out.println("jsonObj is null ");
			}
			try{
				Object jsonObjThirtyMinsFree = jsonObj.get("thirty_mins_or_free");
				if (!(jsonObjThirtyMinsFree instanceof Boolean)) {
					System.out.println("data type error in thirty_mins_or_free " + jsonObjThirtyMinsFree);
				}
				result = true;
				return result;
			}catch (JSONException je)
			{
				result = false;
				je.printStackTrace();
			}

		}catch (JSONException je)
		{
			je.printStackTrace();
		}

		return result;
	}

	public boolean verifyClusterResponseIsFromFallback(String responseContent)
	{
		boolean result = false;
		try{
			JSONObject clusterContentjObj = new JSONObject(responseContent);
			Object clusterContentaObj = clusterContentjObj.get("serviceableRestaurants");
			JSONObject jsonObj = ((JSONArray) clusterContentaObj).getJSONObject(0).getJSONObject("listingServiceabilityResponse");
			if (jsonObj == null) {
				System.out.println("jsonObj is null ");
			}
			try{
				Object jsonObjThirtyMinsFree = jsonObj.get("thirty_mins_or_free");
				if (!(jsonObjThirtyMinsFree instanceof Boolean)) {
					System.out.println("data type error in thirty_mins_or_free " + jsonObjThirtyMinsFree);
				}
				result = true;
				System.out.println("Results are coming from cerebro fallback");
				return result;
			}catch (JSONException je)
			{
				result = false;
				je.printStackTrace();
			}

		}catch (JSONException je)
		{
			System.out.println("Empty results in verifyClusterResponseIsFromFallback");
			result = false;
			je.printStackTrace();
		}

		return result;
	}

	private boolean verifyJsonDataTypeForListingAPI(JSONObject listingJsonObject)
	{
		boolean result = true;

		try{

			// get top level serviceableRestaurants[0] keys and values and verified their datatype
			Object finalAreaObj = listingJsonObject.get("areaId");
			if (!(finalAreaObj instanceof Integer)) {
				System.out.println("data type error in areaId " + finalAreaObj);
			}
			Object finalCityObj = listingJsonObject.get("cityId");
			if (!(finalCityObj instanceof Integer)) {
				System.out.println("data type error in cityId " + finalCityObj);
			}
			Object finalRestaurantObj = listingJsonObject.get("restaurantId");
			if (!(finalRestaurantObj instanceof String)) {
				System.out.println("data type error in restaurantId " + finalRestaurantObj);
			}

			// get all listingServiceabilityResponse keys and values and verified their datatype
			JSONObject firstLSRResObj = listingJsonObject.getJSONObject("listingServiceabilityResponse");
			if (firstLSRResObj == null) {
				System.out.println("firstLSRResObj is null ");
			}

			Object finalLSRRestaruantIdObj = firstLSRResObj.get("restaurant_id");
			if (!(finalLSRRestaruantIdObj instanceof Integer)) {
				System.out.println("data type error in restaurant_id " + finalLSRRestaruantIdObj);
			}else
			{
				if((int)finalLSRRestaruantIdObj <= 0)
				{
					System.out.println("data semantic error in restaurant_id = " + finalLSRRestaruantIdObj);
				}
			}

			Object finalLSRDeliveryTimeObj = firstLSRResObj.get("delivery_time");
			if (!(finalLSRDeliveryTimeObj instanceof Integer)) {
				System.out.println("data type error in delivery_time " + finalLSRDeliveryTimeObj);
			}else
			{
				if((int)finalLSRDeliveryTimeObj < 0)
				{
					System.out.println("data semantic error in delivery_time = " + finalLSRDeliveryTimeObj);
				}
			}

			Object finalLSRMinDeliveryTimeObj = firstLSRResObj.get("min_delivery_time");
			if (!(finalLSRMinDeliveryTimeObj instanceof Integer)) {
				System.out.println("data type error in min_delivery_time " + finalLSRMinDeliveryTimeObj);
			}else
			{
				if((int)finalLSRMinDeliveryTimeObj < 0)
				{
					System.out.println("data semantic error in min_delivery_time = " + finalLSRMinDeliveryTimeObj);
				}
			}

			Object finalLSRMaxDeliveryTimeObj = firstLSRResObj.get("max_delivery_time");
			if (!(finalLSRMaxDeliveryTimeObj instanceof Integer)) {
				System.out.println("data type error in max_delivery_time " + finalLSRMaxDeliveryTimeObj);
			}else
			{
				if((int)finalLSRMaxDeliveryTimeObj < 0)
				{
					System.out.println("data semantic error in max_delivery_time = " + finalLSRMaxDeliveryTimeObj);
				}
			}


			Object finalLSRLastMileObj = firstLSRResObj.get("last_mile_travel");
			if (!(finalLSRLastMileObj instanceof Double)) {
				System.out.println("data type error in last_mile_travel " + finalLSRLastMileObj);
			}else
			{
				if((double)finalLSRLastMileObj <= 0.0)
				{
					System.out.println("data semantic error in last_mile_travel = " + finalLSRLastMileObj);
				}
			}

			//This may be optional
			Object finalLSRDistanceCalulationMethodObj = firstLSRResObj.get("distance_calculation_method");
			if (!(finalLSRDistanceCalulationMethodObj instanceof String)) {
				System.out.println("data type error in distance_calculation_method " + finalLSRDistanceCalulationMethodObj);
			}

			Object finalLSRServiceabilityObj = firstLSRResObj.get("serviceability");
			if (!(finalLSRServiceabilityObj instanceof Integer)) {
				System.out.println("data type error in serviceability " + finalLSRServiceabilityObj);
			}
			Object finalLSRRainModeObj = firstLSRResObj.get("rain_mode");
			if (!(finalLSRRainModeObj instanceof String)) {
				System.out.println("data type error in rain_mode " + finalLSRRainModeObj);
			}

			Object finalLSRLongDistObj = firstLSRResObj.get("long_distance");
			if (!(finalLSRLongDistObj instanceof Integer)) {
				System.out.println("data type error in long_distance " + finalLSRLongDistObj);
			}

			Object finalLSRBatchableObj = firstLSRResObj.get("batchable");
			if (!(finalLSRBatchableObj instanceof Boolean)) {
				System.out.println("data type error in batchable " + finalLSRBatchableObj);
			}

			// get all listingServiceabilityResponse--> Degradation keys and values and verified their datatype
			JSONObject firstLSRDegradationObj = firstLSRResObj.getJSONObject("degradation");

			if(firstLSRDegradationObj!=null)
			{
				Object finalLSRDegradationModeObj = firstLSRDegradationObj.get("mode");
				if (!(finalLSRDegradationModeObj instanceof Integer)) {
					System.out.println("data type error in mode " + finalLSRDegradationModeObj);
				}
				Object finalLSRDegradationSupplyStateObj = firstLSRDegradationObj.get("supply_state");
				if (!(finalLSRDegradationSupplyStateObj instanceof Integer)) {
					System.out.println("data type error in supply_state " + finalLSRDegradationSupplyStateObj);
				}
				Object finalLSRDegradationCauseObj = firstLSRDegradationObj.get("cause");
				if (!(finalLSRDegradationCauseObj instanceof String)) {
					System.out.println("data type error in cause " + finalLSRDegradationCauseObj);
				}

			}

			Object finalLSRStartBannerFactorObj = firstLSRResObj.get("start_banner_factor");
			if (!(finalLSRStartBannerFactorObj instanceof Double)) {
				System.out.println("data type error in start_banner_factor " + finalLSRStartBannerFactorObj);
			}
			Object finalLSRStopBannerFactorObj = firstLSRResObj.get("stop_banner_factor");
			if (!(finalLSRStopBannerFactorObj instanceof Double)) {
				System.out.println("data type error in stop_banner_factor " + finalLSRStopBannerFactorObj);
			}
			Object finalLSRBanerFactorObj = firstLSRResObj.get("banner_factor");
			if (!(finalLSRBanerFactorObj instanceof Double)) {
				System.out.println("data type error in banner_factor " + finalLSRBanerFactorObj);
			}
			Object finalLSRBanerFactorDiffObj = firstLSRResObj.get("banner_factor_diff");
			if (!(finalLSRBanerFactorDiffObj instanceof Double)) {
				System.out.println("data type error in banner_factor_diff " + finalLSRBanerFactorDiffObj);
			}
			Object finalLSRDemandShapingFactorObj = firstLSRResObj.get("demand_shaping_factor");
			if (!(finalLSRDemandShapingFactorObj instanceof Double)) {
				System.out.println("data type error in demand_shaping_factor " + finalLSRDemandShapingFactorObj);
			}

			Object finalLSRZoneBeefUpObj = firstLSRResObj.get("zone_beefup");
			if (!(finalLSRZoneBeefUpObj instanceof Integer)) {
				System.out.println("data type error in zone_beefup " + finalLSRZoneBeefUpObj);
			}
			Object finalLSRStressFactorObj = firstLSRResObj.get("stress_factor");
			if (!(finalLSRStressFactorObj instanceof Double)) {
				System.out.println("data type error in stress_factor " + finalLSRStressFactorObj);
			}
			Object finalLSRRainBeefUpObj = firstLSRResObj.get("rain_beefup");
			if (!(finalLSRRainBeefUpObj instanceof Integer)) {
				System.out.println("data type error in rain_beefup " + finalLSRRainBeefUpObj);
			}
			Object finalLSRThirtyMinsOrFreeObj = firstLSRResObj.get("thirty_mins_or_free");
			if (!(finalLSRThirtyMinsOrFreeObj instanceof Boolean)) {
				System.out.println("data type error in thirty_mins_or_free " + finalLSRThirtyMinsOrFreeObj);
			}

		}catch (JSONException je){
			try{
				errorMessage = errorMessage+ listingJsonObject.getJSONObject("listingServiceabilityResponse").get("restaurant_id")+" : "+je.toString()+"\n";
				System.out.println("errorMessage = " + errorMessage);
				result = false;
			}catch (JSONException jes)
			{
				jes.toString();
			}
//			je.toString();



		} catch (Exception e) {

			e.printStackTrace();
			result = false;

		}
		return result;

	}

	private boolean verifyDistanceFilterCheck(JSONObject listingJsonObject, double distance)
	{
		boolean result = true;

		try{

			// get all listingServiceabilityResponse keys and values and verified their datatype
			JSONObject firstLSRResObj = listingJsonObject.getJSONObject("listingServiceabilityResponse");
			if (firstLSRResObj == null) {
				System.out.println("firstLSRResObj is null ");
			}

			Object finalLSRRestaruantIdObj = firstLSRResObj.get("restaurant_id");
			Object finalLSRLastMileObj = firstLSRResObj.get("last_mile_travel");
			Object finalLSRServiceabilityObj = firstLSRResObj.get("serviceability");
			int serviceability = (Integer) finalLSRServiceabilityObj;
			double lastMile = (Double) finalLSRLastMileObj;
			if(lastMile > distance && serviceability==2)
			{
				System.out.println("Rest Id which breached serviceability check 1= " + finalLSRRestaruantIdObj.toString());
				result = false;
			}
			if(serviceability!=2)
			{
				try{
					Object nonServiceableReasonObj = firstLSRResObj.get("non_serviceable_reason");
				}catch (JSONException je)
				{
					System.out.println("Rest Id which breached serviceability check 2= " + finalLSRRestaruantIdObj.toString());
				}

			}


		}catch (JSONException je){
			try{
				errorMessage = errorMessage+ listingJsonObject.getJSONObject("listingServiceabilityResponse").get("restaurant_id")+" : "+je.toString()+"\n";
				System.out.println("errorMessage = " + errorMessage);
				result = false;
			}catch (JSONException jes)
			{
				jes.toString();
			}
//			je.toString();



		} catch (Exception e) {

			e.printStackTrace();
			result = false;

		}
		return result;

	}


	private boolean checkIfRestaurantIsServiceable(JSONObject listingJsonObject)
	{
		boolean result = false;

		try{

			// get all listingServiceabilityResponse keys and values and verified their datatype
			JSONObject firstLSRResObj = listingJsonObject.getJSONObject("listingServiceabilityResponse");
			if (firstLSRResObj == null) {
				System.out.println("firstLSRResObj is null ");
			}

			Object finalLSRRestaruantIdObj = firstLSRResObj.get("restaurant_id");
			Object finalLSRServiceabilityObj = firstLSRResObj.get("serviceability");
			int serviceability = (Integer) finalLSRServiceabilityObj;
			if(serviceability==2)
			{
				System.out.println("This restaurant is serviceable = " + finalLSRRestaruantIdObj.toString());
				result = true;
			}

		}catch (JSONException je){
			try{
				errorMessage = errorMessage+ listingJsonObject.getJSONObject("listingServiceabilityResponse").get("restaurant_id")+" : "+je.toString()+"\n";
				result = false;
			}catch (JSONException jes)
			{
				jes.toString();
			}
//			je.toString();

		} catch (Exception e) {

			e.printStackTrace();
			result = false;

		}
		return result;

	}

	private boolean verifySLAFilterCheck(JSONObject listingJsonObject, Integer SLA)
	{
		boolean result = true;

		try{

			// get all listingServiceabilityResponse keys and values and verified their datatype
			JSONObject firstLSRResObj = listingJsonObject.getJSONObject("listingServiceabilityResponse");
			if (firstLSRResObj == null) {
				System.out.println("firstLSRResObj is null ");
			}

			Object finalLSRRestaruantIdObj = firstLSRResObj.get("restaurant_id");
			Object finalLSRDeliveryTimeObj = firstLSRResObj.get("delivery_time");
			Object finalLSRServiceabilityObj = firstLSRResObj.get("serviceability");
			int serviceability = (Integer) finalLSRServiceabilityObj;
			double deliveryTime = (Integer) finalLSRDeliveryTimeObj;
			if(deliveryTime > SLA && serviceability==2)
			{
				System.out.println("Rest Id which breached serviceability check 1= " + finalLSRRestaruantIdObj.toString());
				result = false;
			}
			if(serviceability!=2)
			{
				try{
					Object nonServiceableReasonObj = firstLSRResObj.get("non_serviceable_reason");
				}catch (JSONException je)
				{
					System.out.println("Rest Id which breached serviceability check 2= " + finalLSRRestaruantIdObj.toString());
					result = false;
				}

			}


		}catch (JSONException je){
			try{
				errorMessage = errorMessage+ listingJsonObject.getJSONObject("listingServiceabilityResponse").get("restaurant_id")+" : "+je.toString()+"\n";
				result = false;
			}catch (JSONException jes)
			{
				jes.toString();
			}
//			je.toString();



		} catch (Exception e) {

			e.printStackTrace();
			result = false;

		}
		return result;

	}

	private boolean verifyBFFilterCheck(JSONObject listingJsonObject, Double SLA)
	{
		boolean result = true;

		try{

			// get all listingServiceabilityResponse keys and values and verified their datatype
			JSONObject firstLSRResObj = listingJsonObject.getJSONObject("listingServiceabilityResponse");
			if (firstLSRResObj == null) {
				System.out.println("firstLSRResObj is null ");
			}

			Object finalLSRRestaruantIdObj = firstLSRResObj.get("restaurant_id");
			Object finalLSRBFObj = firstLSRResObj.get("banner_factor");
			Object finalLSRServiceabilityObj = firstLSRResObj.get("serviceability");
			int serviceability = (Integer) finalLSRServiceabilityObj;
			double BF = (Double) finalLSRBFObj;
			if(BF > SLA && serviceability==2)
			{
				System.out.println("Rest Id which breached serviceability check 1= " + finalLSRRestaruantIdObj.toString());
				result = false;
			}
			if(serviceability!=2)
			{
				try{
					Object nonServiceableReasonObj = firstLSRResObj.get("non_serviceable_reason");
				}catch (JSONException je)
				{
					System.out.println("Rest Id which breached serviceability check 2= " + finalLSRRestaruantIdObj.toString());
					result = false;
				}

			}


		}catch (JSONException je){
			try{
				errorMessage = errorMessage+ listingJsonObject.getJSONObject("listingServiceabilityResponse").get("restaurant_id")+" : "+je.toString()+"\n";
				System.out.println("errorMessage = " + errorMessage);
				result = false;
			}catch (JSONException jes)
			{
				jes.toString();
			}
//			je.toString();



		} catch (Exception e) {

			e.printStackTrace();
			result = false;

		}
		return result;

	}


	public boolean verifyJsonDuplicateRestaurantIdListingAPi(String content) {
		boolean result = true;
		try {

			//String content = FileUtils.readFileToString(new File("/Users/vijay.s/LocalRepo/Experiment/src/main/java/actual.json"), org.apache.commons.lang3.CharEncoding.UTF_8);
//                String content = processor.ResponseValidator.GetBodyAsText();
			JSONObject jObj = new JSONObject(content);
			Object aObj = jObj.get("serviceableRestaurants");
			int totalSize = ((JSONArray)aObj).length();
			System.out.println("SIZE : "+totalSize);
			Set<Object> set = new HashSet<Object>();
			int j =0;
			for(int i= 0; i <totalSize ; i++)
			{
				try{
					if(!set.contains(((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("restaurant_id")))
					{
						set.add(((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("restaurant_id"));
					}else if(((Integer)((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("restaurant_id") == 0))
					{
						j++;
					}else
					{
						System.out.println("Index: "+(i+1)+" rest Id : "+((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("restaurant_id"));
					}

				}catch (Exception e)
				{
					System.out.println("There is a duplicate in : "+((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("restaurant_id"));
					result = false;
					e.printStackTrace();
				}

			}
			if(set.size() != totalSize)
			{
				System.out.println("There are duplicate restaurants");
				System.out.println("Restaurant Id duplicates with 0 as restaurantId= " + j);
				System.out.println(set.size()+"!="+totalSize);
				result = false;
			}

		} catch (JSONException je) {
			je.printStackTrace();
			result = false;
			return result;

		} catch (Exception e) {

			e.printStackTrace();
			result = false;
			return result;
		}
		return result;

	}

	public int getTotalRestaurantsCountIdnListingAPi(String content) {
		int totalSize =0;

		try {

			JSONObject jObj = new JSONObject(content);
			Object aObj = jObj.get("serviceableRestaurants");
			totalSize = ((JSONArray) aObj).length();
			System.out.println("SIZE : " + totalSize);
			return totalSize;
		}catch (JSONException jse)
		{
			jse.printStackTrace();

		}
		return totalSize;
	}

	public int getPolygonIdFromResponse(String content) {
		int polygonId =0;

		try {

			JSONObject jObj = new JSONObject(content);
			Object aObj = jObj.get("statusMessage");
			System.out.println("Polygon id is : " + aObj.toString());
			polygonId = (Integer) aObj;
			return polygonId;
		}catch (JSONException jse)
		{
			jse.printStackTrace();

		}
		return polygonId;
	}

	public int getPolygonCreationStatus(String content) {
		int polygonId =0;

		try {

			JSONObject jObj = new JSONObject(content);
			Object aObj = jObj.get("statusCode");
			System.out.println("Polygon id is : " + aObj.toString());
			polygonId = (Integer) aObj;
			return polygonId;
		}catch (JSONException jse)
		{
			jse.printStackTrace();

		}
		return polygonId;
	}

	public boolean getTotalRestaurantsCountServiceableDistance(String content, double distance) {
		int totalSize =0;
		boolean result = true;
		boolean flag = true;

		try {

			JSONObject jObj = new JSONObject(content);
			Object aObj = jObj.get("serviceableRestaurants");
			totalSize = ((JSONArray) aObj).length();
			System.out.println("SIZE : " + totalSize);
			for(int i=0; i< totalSize; i++)
			{
				result = verifyDistanceFilterCheck(((JSONArray) aObj).getJSONObject(i),distance);
				if(!result)
				{
					flag = false;
				}

			}
		}catch (JSONException jse)
		{
			jse.printStackTrace();

		}
		return flag;
	}

	public String getOneServiceableRestaurant(String content) {
		Integer totalSize =0;
		String serviceableRest ="";
		boolean result = false;

		try {

			JSONObject jObj = new JSONObject(content);
			Object aObj = jObj.get("serviceableRestaurants");
			totalSize = ((JSONArray) aObj).length();
			System.out.println("SIZE : " + totalSize);
			for(int i=0; i< totalSize&& result==false; i++ )
			{
				result = checkIfRestaurantIsServiceable(((JSONArray) aObj).getJSONObject(i));
				serviceableRest = (String)((JSONArray) aObj).getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("restaurant_id");

			}
			return serviceableRest;
		}catch (JSONException jse)
		{
			jse.printStackTrace();

		}
		return serviceableRest;
	}

	public boolean getTotalRestaurantsCountServiceableSLA(String content, Integer SLA) {
		int totalSize =0;
		boolean result = true;
		boolean flag  = true;

		try {

			JSONObject jObj = new JSONObject(content);
			Object aObj = jObj.get("serviceableRestaurants");
			totalSize = ((JSONArray) aObj).length();
			System.out.println(" getTotalRestaurantsCountServiceableSLA SIZE : " + totalSize);
			for(int i=0; i< totalSize; i++)
			{
				result = verifySLAFilterCheck(((JSONArray) aObj).getJSONObject(i),SLA);
				if(!result)
				{
					flag = false;
				}

			}
		}catch (JSONException jse)
		{
			jse.printStackTrace();

		}
		return flag;
	}

	public boolean getTotalRestaurantsCountServiceableBF(String content, Double BF) {
		int totalSize =0;
		boolean result = true;
		boolean flag = true;

		try {

			JSONObject jObj = new JSONObject(content);
			Object aObj = jObj.get("serviceableRestaurants");
			totalSize = ((JSONArray) aObj).length();
			System.out.println(" getTotalRestaurantsCountServiceableSLA SIZE : " + totalSize);
			for(int i=0; i< totalSize; i++)
			{
				result = verifyBFFilterCheck(((JSONArray) aObj).getJSONObject(i),BF);
				if(!result)
				{
					flag = false;
				}

			}
		}catch (JSONException jse)
		{
			jse.printStackTrace();

		}
		return flag;
	}

	public static void jsonCompareByString(String jsonObject1,String jsonObject2, String url1, String url2)
	{
//        CompareJson.execute(jsonObject1, jsonObject2, true);
		PrintStream writeToLog = null;
		try {
			JSONObject actualResponseJson = new JSONObject(jsonObject1);
			JSONObject expectedResponseJson = new JSONObject(jsonObject2);

			ReflectionAssert.assertReflectionEquals(expectedResponseJson, actualResponseJson, ReflectionComparatorMode.LENIENT_DATES);
			try{
				writeToLog = new PrintStream(new FileOutputStream("/Users/vijay.s/LocalRepo/swiggy_test/gameofthrones/src/test/java/com/swiggy/api/erp/delivery/helper/json_results.log", true));
				writeToLog.append(url1+" equals "+url2);
				writeToLog.append("\n");
			}catch (IOException e1) {
				e1.printStackTrace();
			}


		} catch (JSONException e) {
			try {
				writeToLog = new PrintStream(new FileOutputStream("/Users/vijay.s/LocalRepo/swiggy_test/gameofthrones/src/test/java/com/swiggy/api/erp/delivery/helper/json_results.log", true));
				writeToLog.append("\n");

				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				writeToLog.append("\n-----------------");
				writeToLog.append("\nDifferences START\n");
				writeToLog.append(url1 +" not equals "+url2);
				writeToLog.append("\n-----------------");
				String title = StringUtils.substringBetween(sw.toString(), "--- Found following differences ---", "--- Difference detail");
				writeToLog.append(title);
				writeToLog.append("-----------------");
				writeToLog.append("\nDifferences   END");
				writeToLog.append("\n-----------------\n\n");
				writeToLog.close();

			} catch (IOException e1) {
				e1.printStackTrace();
			}

		} catch (AssertionFailedError e) {
			try {
				writeToLog = new PrintStream(
						new FileOutputStream("/Users/vijay.s/LocalRepo/swiggy_test/gameofthrones/src/test/java/com/swiggy/api/erp/delivery/helper/json_results.log", true));
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				writeToLog.append("\n-----------------");
				writeToLog.append("\nDifferences START\n");
				writeToLog.append(url1 +" not equals "+url2);
				writeToLog.append("\n-----------------");
				String title = StringUtils.substringBetween(sw.toString(), "--- Found following differences ---", "--- Difference detail");
				writeToLog.append(title);
				writeToLog.append("-----------------");
				writeToLog.append("\nDifferences   END");
				writeToLog.append("\n-----------------\n\n");
				writeToLog.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}
	}

}
