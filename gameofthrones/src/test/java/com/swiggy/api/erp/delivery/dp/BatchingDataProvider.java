package com.swiggy.api.erp.delivery.dp;

import java.util.HashMap;
import java.util.Map;

import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

public class BatchingDataProvider {
	CheckoutHelper chelper=new CheckoutHelper();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	DeliveryServiceHelper helpdel=new DeliveryServiceHelper();
	DeliveryDataHelper deldata=new DeliveryDataHelper();

	@DataProvider(name = "QE-256")
	public Object[][] AutoassigntestDP256() throws Exception {
		delmeth.dbhelperupdate(DeliveryConstant.updatebatch);
		delmeth.dbhelperupdate(DeliveryConstant.assigndetounassinged);
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_single,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-264")
	public Object[][] BatchingDP264() throws Exception {
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-265")
	public Object[][] AutoassigntestDP265() throws Exception {
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		
		return new Object[][] {
				{ order_id1,order_id2}
		};
	}
	 @DataProvider(name = "QE-266")
		public Object[][] batchingTest266DP() throws Exception {
		 String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
			String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
			
			return new Object[][] {
					{ order_id1,order_id2}
			};
		}
	 @DataProvider(name = "QE-267")
		public Object[][] batchingTest267DP() throws Exception {
		 String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
			String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
			
			return new Object[][] {
					{ order_id1,order_id2}
			};
		}
	 @DataProvider(name = "QE-268")
		public Object[][] batchingTest268DP() throws Exception {
		 String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
			String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
			
			return new Object[][] {
					{ order_id1,order_id2}
			};
		}
	
	@DataProvider(name = "QE-269")
	public Object[][] Autoassigntest269() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
		
	@DataProvider(name = "QE-270")
	public Object[][] AutoassigntestDP270() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_single,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-271")
	public Object[][] AutoassigntestDP271() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_less,
				DeliveryConstant.batchingVersion_single,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-272")
	public Object[][] AutoassigntestDP272() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_single,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_less,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-273")
	public Object[][] AutoassigntestDP273() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_single,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_less,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,DeliveryConstant.single_rest_cust_Dist_200m_lat,DeliveryConstant.single_rest_cust_Dist_200m_lng).toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-275")
	public Object[][] BatchingDP275() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_single,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_less,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		Thread.sleep(65000);
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-276")
	public Object[][] BatchingDP276() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_single,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	
	@DataProvider(name = "QE-277")
	public Object[][] batchingTest277DP() throws Exception {
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {{order_id1, order_id2}};
	}
	@DataProvider(name = "QE-278")
	public Object[][] BatchingDP278() throws Exception {
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	 @DataProvider(name= "QE-279")
		public Object[][] batchingTest279DP() throws Exception {
		 String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
			String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"","").toString();
			return new Object[][] {
			{ order_id1,order_id2}
			};
		}
	 @DataProvider(name= "QE-280")
		public Object[][] batchingTest280DP() throws Exception {
		 String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
			return new Object[][] 
					{
					{ order_id1}
					};
				}
	 
	 @DataProvider(name= "QE-281")
	 public Object[][] batchingTest281DP() throws Exception {
		 String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
			return new Object[][] 
					{
					{ order_id1}
					};
				}
	 @DataProvider(name= "QE-359")
		public Object[][] batchingTest359DP() throws Exception {
		 String query = DeliveryConstant.batching_disabled + DeliveryConstant.multi_rest_batching_rest1 + ";";
		 delmeth.dbhelperupdate(query);
		 String order_id1 = createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area, "", "").toString();
		 String order_id2 = createorderjson(DeliveryConstant.multi_rest_batching_rest1, DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area, "", "").toString();
		 return new Object[][]
				 {
						 {order_id1, order_id2}
				 };
	 }

				
public Object createorderjson(String restaurant_id,String restaurant_lat_lng,String restaurant_customer_distance,String restaurant_area_code,String lat,String lng) throws JSONException
	{
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",restaurant_id);
		defMap.put("restaurant_lat_lng",restaurant_lat_lng);
		defMap.put("restaurant_customer_distance", restaurant_customer_distance);
		defMap.put("restaurant_area_code", restaurant_area_code);
	if(lat!="")
	{
		defMap.put("lat", lat);
	}
	if(lng!="")
	{
		defMap.put("lng",lng);
	}
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
			
		String orderJson=deldata.returnOrderJson(defMap);
		boolean order_status=deldata.pushOrderJsonToRMQAndValidate(orderJson);
		Object order_id=null;
		System.out.println(orderJson);
		if(order_status)
		{
			order_id=(new JSONObject(orderJson)).get("order_id").toString();
		}
		return order_id;
	}
	
	
	
}

