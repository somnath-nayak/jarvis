package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Pricing_models {
	 private Addon_combinations[] addon_combinations;

	    private String price;

	    private Variations[] variations;

	    public Addon_combinations[] getAddon_combinations ()
	    {
	        return addon_combinations;
	    }

	    public void setAddon_combinations (Addon_combinations[] addon_combinations)
	    {
	        this.addon_combinations = addon_combinations;
	    }

	    public String getPrice ()
	    {
	        return price;
	    }

	    public void setPrice (String price)
	    {
	        this.price = price;
	    }

	    public Variations[] getVariations ()
	    {
	        return variations;
	    }

	    public void setVariations (Variations[] variations)
	    {
	        this.variations = variations;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [addon_combinations = "+addon_combinations+", price = "+price+", variations = "+variations+"]";
	    }
	}
				
				