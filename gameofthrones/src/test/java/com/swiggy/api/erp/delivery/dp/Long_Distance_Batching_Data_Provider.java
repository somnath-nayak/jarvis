package com.swiggy.api.erp.delivery.dp;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;

public class Long_Distance_Batching_Data_Provider {
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	DeliveryDataHelper deldata=new DeliveryDataHelper();
	DeliveryServiceHelper delserhel=new DeliveryServiceHelper();
	
	@DataProvider(name = "LDB_1")
	public Object[][] LDB_1() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.closeallexisitingbatches,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_random_de_id, DeliveryConstant.databaseName);
		String order_id1=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		String order_id2=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		return new Object[][] {
		{ order_id1, order_id2}

		};
	}
	@DataProvider(name = "LDB_2")
	public Object[][] LDB_2() throws Exception {
		String order_id1=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_much_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		String order_id2=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		return new Object[][] {
	{ order_id1, order_id2}
		};
	}
	@DataProvider(name = "LDB_3")
	public Object[][] LDB_3() throws Exception {
		String order_id1=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		String order_id2=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_much_more, RTSConstants.long_distance_disabled, RTSConstants.long_distance_area_code_string).toString();
		return new Object[][] {

		{ order_id1, order_id2}

		};
	}
	@DataProvider(name = "LDB_4")
	public Object[][] LDB_4() throws Exception {
		String order_id1=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		delserhel.assignOrder(order_id1, RTSConstants.de_id1);
		Thread.sleep(10000);
		delserhel.zipDialConfirm(order_id1);
		Thread.sleep(10000);
		delserhel.zipDialArrived(order_id1);
		String order_id2=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		return new Object[][] {

		{ order_id1, order_id2}

		};
	}
	@DataProvider(name = "LDB_5")
	public Object[][] LDB_5() throws Exception {
		String order_id1=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_less, RTSConstants.long_distance_disabled, RTSConstants.long_distance_area_code_string).toString();
		delserhel.assignOrder(order_id1, RTSConstants.de_id1);
		delserhel.zipDialConfirm(order_id1);
		delserhel.zipDialArrived(order_id1);
		String order_id2=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		return new Object[][] {
		{ order_id1, order_id2}

		};
	}
	@DataProvider(name = "LDB_6")
	public Object[][] LDB_6() throws Exception {
		String order_id1=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		String order_id2=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_much_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		return new Object[][] {
		{ order_id1, order_id2}

		};
	}
	@DataProvider(name = "LDB_7")
	public Object[][] LDB_7() throws Exception {
		String order_id1=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		String order_id2=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_more, RTSConstants.long_distance_enabled, RTSConstants.long_distance_area_code_string).toString();
		return new Object[][] {
		{ order_id1, order_id2}

		};
	}
	public Object createorderjson(String restaurant_id,String restaurant_lat_lng,String restaurant_customer_distance,String is_long_distance,String restaurant_area_code) throws JSONException
	{
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",restaurant_id);
		defMap.put("restaurant_lat_lng",restaurant_lat_lng);
		defMap.put("restaurant_customer_distance", restaurant_customer_distance);
		defMap.put("is_long_distance", is_long_distance);
		defMap.put("restaurant_area_code", restaurant_area_code);
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
		String orderJson=deldata.returnOrderJson(defMap);
		boolean order_status=deldata.pushOrderJsonToRMQAndValidate(orderJson);
		Object order_id=null;
		System.out.println(orderJson);
		if(order_status)
		{
			order_id=(new JSONObject(orderJson)).get("order_id").toString();
		}
		return order_id;
	}

	}
