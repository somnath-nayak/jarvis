package com.swiggy.api.erp.vms.editservice.pojo;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"requested_timestamp",
"requested_by"
})
public class Meta_data {

@JsonProperty("requested_timestamp")
private String requested_timestamp;
@JsonProperty("requested_by")
private String requested_by;

/**
* No args constructor for use in serialization
* 
*/
public Meta_data() {
}

/**
* 
* @param requested_timestamp
* @param requested_by
*/
public Meta_data(String requested_timestamp, String requested_by) {
super();
this.requested_timestamp = requested_timestamp;
this.requested_by = requested_by;
}

@JsonProperty("requested_timestamp")
public String getRequested_timestamp() {
return requested_timestamp;
}

@JsonProperty("requested_timestamp")
public void setRequested_timestamp(String requested_timestamp) {
this.requested_timestamp = requested_timestamp;
}

public Meta_data withRequested_timestamp(String requested_timestamp) {
this.requested_timestamp = requested_timestamp;
return this;
}

@JsonProperty("requested_by")
public String getRequested_by() {
return requested_by;
}

@JsonProperty("requested_by")
public void setRequested_by(String requested_by) {
this.requested_by = requested_by;
}

public Meta_data withRequested_by(String requested_by) {
this.requested_by = requested_by;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("requested_timestamp", requested_timestamp).append("requested_by", requested_by).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(requested_timestamp).append(requested_by).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Meta_data) == false) {
return false;
}
Meta_data rhs = ((Meta_data) other);
return new EqualsBuilder().append(requested_timestamp, rhs.requested_timestamp).append(requested_by, rhs.requested_by).isEquals();
}

}