package com.swiggy.api.erp.cms.pojo.BulkSubCategory;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/20/18.
 */
public class Entities
{
    private String id;

    private int order;

    private String description;

    private String name;

    private int enable;

    private String category_id;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public int getEnable ()
    {
        return enable;
    }

    public void setEnable (int enable)
    {
        this.enable = enable;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public void setCategory_id (String category_id)
    {
        this.category_id = category_id;
    }

    public Entities build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        if(this.getId() == null)
            this.setId(MenuConstants.sub_category_id);
        if(this.getName() == null)
            this.setName(MenuConstants.sub_category_name);
        if(this.getDescription() == null)
            this.setDescription(MenuConstants.items_description);
        if(this.getCategory_id() == null)
            this.setCategory_id(MenuConstants.main_category_id);
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", order = "+order+", description = "+description+", name = "+name+", enable = "+enable+", category_id = "+category_id+"]";
    }
}
