package com.swiggy.api.erp.finance.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.finance.constants.FinanceConstants;
import com.swiggy.api.erp.finance.constants.TransactionConstants;
import com.swiggy.api.erp.finance.dp.TransactionDp;
import com.swiggy.automation.common.utils.APIUtils;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.SqlTemplate;

import javax.crypto.Cipher;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;
import org.codehaus.jackson.JsonNode;


/**
 * Created by kiran.j on 7/12/18.
 */
public class TransactionsHelper {

    Initialize gameofthrones = new Initialize();

    public Processor getTransactions(String de_id, String session_id, String transaction_id) {
        HashMap<String, String> headers = getDefaultHeaders();
        GameOfThronesService service = new GameOfThronesService("cashmanagement", "get_txn_detail_api", gameofthrones);
        return new Processor(service, headers, null, new String[]{de_id, session_id, transaction_id});
    }

    public Processor addTransactionHelper(String de_id, String description, String order_id, String de_incoming, String de_outgoing, String txntype){
        HashMap<String,String> headers= getDefaultHeaders();
        String[] payload=new String[]{de_id,description,order_id,de_incoming,de_outgoing,txntype};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","add_txn_api",gameofthrones);
        return new Processor(service, headers, payload);
    }

    public Processor addPocketingAndPayoutTxn(String de_id, String description, String order_id, String de_incoming, String de_outgoing, String txntype){
        HashMap<String,String> headers= getDefaultHeaders();

        String[] payload=new String[]{de_id,description,order_id,de_incoming,de_outgoing,txntype};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","add_bulk_txn_api",gameofthrones);
        return new Processor(service, headers, payload);
    }

    public Processor addPocketing(String de_id, String city_id, String zone, String from_date, String to_date){
        HashMap<String,String> headers= getDefaultHeaders();
        String[] payload=new String[]{de_id,city_id,zone,from_date,to_date};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","pocketing_fin_api",gameofthrones);
        return new Processor(service, headers, payload);
    }

    public Processor submitRevertRequest(String transaction_id, String reason, String user_id, String email){
        HashMap<String,String> headers= getDefaultHeaders();
        String[] payload=new String[]{transaction_id, reason, user_id, email};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","pocketing_submit_revert_api",gameofthrones);
        return new Processor(service, headers, payload);
    }

    public Processor updateTxn(String de_id, String transaction_id, String inconsitency){
        HashMap<String,String> headers= getDefaultHeaders();
        String[] payload=new String[]{de_id, inconsitency, transaction_id};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","update_txn_api",gameofthrones);
        return new Processor(service, headers, payload);
    }

    public Processor acceptRevertRequest(String transaction_id, String reason, String user_id, String email){
        HashMap<String,String> headers= getDefaultHeaders();
        String[] payload=new String[]{transaction_id, reason, user_id, email};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","pocketing_accept_revert_api",gameofthrones);
        return new Processor(service, headers, payload);
    }

    public Processor denyRevertRequest(String transaction_id, String reason, String user_id, String email){
        HashMap<String,String> headers= getDefaultHeaders();
        String[] payload=new String[]{transaction_id, reason, user_id, email};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","pocketing_deny_revert_api",gameofthrones);
        return new Processor(service, headers, payload);
    }

    public Processor depositNovoPay(String mobile_no, String amount, String np_txn_id){
        HashMap<String,String> headers= getDefaultHeaders();
        String[] payload=new String[]{mobile_no, amount, np_txn_id};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","deposit_novopay_api",gameofthrones);
        return new Processor(service, headers, payload);
    }
    public Processor getNovoPay(String mobile_no){
        HashMap<String,String> headers= getDefaultHeaders();
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","validate_novopay_api",gameofthrones);
        return new Processor(service, headers, null, new String[]{mobile_no});
    }


    public HashMap<String, String> getDefaultHeaders() {
        HashMap<String, String> map = new HashMap<>();
        map.put("Authorization", FinanceConstants.transaction_auth);
        return  map;
    }

    public boolean verifyTransactionResponse(String de_id, String session_id, Processor processor, String[] errors) {
        boolean success = false;
        if(errors.length > 0) {
            success = processor.ResponseValidator.GetNodeValueAsInt("statusCode") == Integer.parseInt(errors[0])
                    && processor.ResponseValidator.GetNodeValue("statusMessage").contains(errors[1]);
        } else {
            success = verifyTransactions(processor, de_id, session_id);
        }
        return success;
    }
/*
    public boolean verifyTransactions(Processor processor, String de_id, String session_id) {
        boolean success = false;
        String json = processor.ResponseValidator.GetBodyAsText();
        System.out.println(JsonPath.read(json, "$.data.transaction[?(@.orderId != null)]").toString());
        System.out.println(getTransactionsFromDb(de_id, session_id));
        return success;
    }
*/

    public boolean verifyTransactions(Processor processor, String de_id, String session_id){
        boolean success = false;
        String json = processor.ResponseValidator.GetBodyAsText();
        json = JsonPath.read(json, "$.data.transaction[?(@.orderId != null)]").toString();
        JsonNode node = null;
        try {
            node = APIUtils.convertStringtoJSON(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, Object> txn = getTransactionsFromDb(de_id, session_id).get(0);
        success = txn.get("de_incoming").toString().equalsIgnoreCase(node.get(0).get("deIncoming").toString())
                && txn.get("de_outgoing").toString().equalsIgnoreCase(node.get(0).get("deOutgoing").toString())
                && txn.get("session_id").toString().equalsIgnoreCase(node.get(0).get("sessionId").toString());
        return success;
    }

    public List<Map<String, Object>> getTransactionsFromDb(String de_id, String session_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.cash_transaction_start + de_id + TransactionConstants.cash_transaction_middle + session_id + TransactionConstants.cash_transaction_end);
        return list;
    }

    public boolean verifyStatusCode(Processor processor, int status_code) {
        return (processor.ResponseValidator.GetNodeValueAsInt("statusCode") == status_code);
    }

    public boolean verifyStatusCode(Processor processor, String[] status) {
        return (processor.ResponseValidator.GetNodeValue("status").equalsIgnoreCase(status[0]) && processor.ResponseValidator.GetNodeValue("status_msg").equalsIgnoreCase(status[1]));
    }

    public boolean verifyRevertStatusCode(Processor processor, String[] status) {
        return (processor.ResponseValidator.GetNodeValueAsInt("statusCode") == Integer.parseInt(status[0]) && processor.ResponseValidator.GetNodeValue("statusMessage").equalsIgnoreCase(status[1]));
    }

    public String getInvalidDeMob() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.invalid_de_mob + "1");
        return list.get(0).get("mobile").toString();
    }

    public String getValidDeMob() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.valid_de_mob + "1");
        return list.get(0).get("mobile").toString();
    }

    public Map<String, Object> getValidDedetails() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.valid_de_details + "1");
        return list.get(0);
    }

    public int getFloatingCash(String de_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.floating_cash + de_id);
        return Integer.parseInt(list.get(0).get("floating_cash").toString());
    }

    public List<Map<String, Object>> getCashSessions(String de_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.floating_cash + de_id);
        return list;
    }

    public List<Map<String, Object>>  getTransactions(String order_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.cash_transactions + order_id + TransactionConstants.order_by);
        return list;
    }

    public List<Map<String, Object>> getPocketingTxn(String txn_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.pocketingtxn + txn_id);
        return list;
    }

    public Processor revertTxnHelper(String de_id,String  txn_id){
        HashMap<String,String> headers= getDefaultHeaders();
        GameOfThronesService service = new GameOfThronesService("cashmanagement","revert_txn_api",gameofthrones);
        Processor p = new Processor(service,headers, new String[]{de_id, txn_id});
        return  p;
    }

    public List<Map<String, Object>> deCashAudit(String txn_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.de_cash_audit + txn_id);
        return list;
    }

    public boolean verifyTxnList(Processor processor) {
        List<String> list = Arrays.asList(JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.pocketTransactionViewList").toString().replace("[","").replace("]",""));
        return list.size() <= 1;
    }

    public List<Map<String, Object>> getPocketingTxnDB(String de_id, String from_date, String to_date) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.pocketing_fin + de_id + TransactionConstants.pocketin_fin_middle + from_date + "' and '" + to_date +"' group by t2.order_id");
        return list;
    }

    public boolean getPocketingTxns(Processor processor, String de_id, String from, String to) {
        List<List<Object>> list = new ArrayList<>();
        List<Object> sub_list = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.pocketTransactionViewList");
        for(Object l : sub_list) {
            System.out.println("list value is" + l);
        }
        List<Map<String, Object>> map = getPocketingTxnDB(de_id, from, to);
        System.out.println("db value is"+map);
        return verifyTxns(map, sub_list);
    }

    public Processor addPocketingOps(String de_id, String city_id, String zone, String from_date, String to_date){
        HashMap<String,String> headers= getDefaultHeaders();
        GameOfThronesService service = new GameOfThronesService("cashmanagement","pocketing_ops_api",gameofthrones);
        return new Processor(service, headers, new String[]{de_id, city_id, zone, from_date, to_date});
    }

    public boolean verifyTxns(List<Map<String, Object>> db_list, List<Object> list) {
        boolean success = false;
        for(Map<String, Object> map : db_list) {
            String order_id = map.get("order_id").toString();
            for(int i=0;i<list.size();i++) {
                List<Object> l = (List)list.get(i);
                if(l.get(4).toString().equalsIgnoreCase(order_id)) {
                    success = l.get(1).toString().equalsIgnoreCase(map.get("id").toString()) &&
                            l.get(2).toString().equalsIgnoreCase(map.get("de_id").toString()) &&
                            l.get(3).toString().equalsIgnoreCase(map.get("session_id").toString()) &&
                            l.get(5).toString().equalsIgnoreCase(map.get("de_incoming").toString()) &&
                            l.get(6).toString().equalsIgnoreCase(map.get("de_outgoing").toString()) &&
                            l.get(7).toString().equalsIgnoreCase(map.get("description").toString());
                }
            }
        }
        return success;
    }

    public static String encrypt(PublicKey publicKey, String message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encrypted_string = cipher.doFinal(message.getBytes());
        String str = org.apache.commons.codec.binary.Base64.encodeBase64String(encrypted_string);
        System.out.println("string is"+str);
        return str;
    }

    public static byte[] decrypt(PublicKey publicKey, byte [] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        byte[] encrypted_string = cipher.doFinal(encrypted);
        String str = new String(encrypted_string, StandardCharsets.UTF_8);
        System.out.println("descrypted string is"+str);
        return encrypted_string;
    }

    public PublicKey getPublicKey() throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        String key = TransactionConstants.pub_key;
        byte[] decode = org.apache.commons.codec.binary.Base64.decodeBase64(key);
        X509EncodedKeySpec spec =
                new X509EncodedKeySpec(decode);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    static Key readKeyFromFile(String keyFileName) throws IOException {
        InputStream in = new FileInputStream(keyFileName);
        ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(
                in));
        try {
            BigInteger m = (BigInteger) oin.readObject();
            BigInteger e = (BigInteger) oin.readObject();
            KeyFactory fact = KeyFactory.getInstance("RSA");
            return fact.generatePublic(new RSAPublicKeySpec(m, e));
        } catch (Exception e) {
            throw new RuntimeException("Spurious serialisation error", e);
        } finally {
            oin.close();
            System.out.println("Closed reading file.");
        }
    }

    public int getRandomNo(int minimum,int maximum){
        Random rn = new Random();
        int range = maximum - minimum + 1;
        int randomNum =  rn.nextInt(range) + minimum;
        return randomNum;
    }

    public Processor cdmiciciDeposit(String payload) {
        GameOfThronesService service = new GameOfThronesService("cashmanagement","deposit_cdm_api",gameofthrones);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "*/*");
        return new Processor(service, headers, new String[]{payload});
    }

    public Processor cdmiciciValidate(String payload) {
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","validate_cdm_api",gameofthrones);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "*/*");
        return new Processor(service, headers, new String[]{payload});
    }

    public Processor upiiciciDeposit(String payload) {
        GameOfThronesService service = new GameOfThronesService("cashmanagement","deposit_upi_api",gameofthrones);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "*/*");
        return new Processor(service, headers, new String[]{payload});
    }

    public List<Map<String, Object>> deCashAuditByDe(String de_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.de_cash_audit_de + de_id + TransactionConstants.last_txn_end);
        return list;
    }

    public List<Map<String, Object>> getLastTransactions(String de_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(TransactionConstants.cash_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(TransactionConstants.last_txn + de_id + TransactionConstants.last_txn_end);
        return list;
    }

}
