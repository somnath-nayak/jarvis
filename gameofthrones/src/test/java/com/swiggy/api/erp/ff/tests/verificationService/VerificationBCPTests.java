package com.swiggy.api.erp.ff.tests.verificationService;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.constants.VerificationServiceConstants;
import com.swiggy.api.erp.ff.dp.verificationService.VerificationBCP_Data;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.VerificationServiceHelper;

import framework.gameofthrones.Tyrion.DBHelper;

public class VerificationBCPTests extends VerificationBCP_Data implements VerificationServiceConstants {

	VerificationServiceHelper helper = new VerificationServiceHelper();
	OMSHelper omsHelper = new OMSHelper();
	LOSHelper losHelper = new LOSHelper();
	DBHelper dbHelper=new DBHelper(); 
	Logger log = Logger.getLogger(VerificationBCPTests.class);
	
	
	//force verify test
	@Test(groups = {"sanity", "regression"},description = "Create a partner order")
	public void partnerOrderDetailsForceVerifiedTest() throws Exception{
		log.info("         partnerOrderDetails_Force_verified_Test started             ");
		String order_id = losHelper.getAnOrder_force_verify("partner");
		int BCP_state= (VerificationServiceConstants.verificationBCP_Pending);
		int BCP_state_force_verified= (VerificationServiceConstants.verificationBCP_Force_Verified);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the Fact table");
		Assert.assertTrue(helper.isOrderIdPresentInVerificationTable(Long.parseLong(order_id)), "Order id is not present in the verification_status table");
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state), "Order state is pending in the verification_status table");
	   
		Assert.assertFalse(dbHelper.pollDB(verificationDBHost,"SELECT state FROM verification.verification_status where order_id = " + Long.parseLong(order_id) + ";","state", "3",60,600),"Order status is not found in DB");
		Thread.sleep(1000);
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state_force_verified), "Order state is not changing in the verification_status table");
		log.info("         partnerOrderDetails_Force_verified_Test completed             ");

	}
	
	@Test(groups = {"sanity", "regression"},description = "Create a manual order")
	public void manualOrderDetailsForceVerifiedTest() throws Exception{
		log.info("         manualOrderDetails_Force_verified_Test started             ");

		String order_id = losHelper.getAnOrder_force_verify("manual");
		int BCP_state= VerificationServiceConstants.verificationBCP_Pending;
		int BCP_state_force_verified= (VerificationServiceConstants.verificationBCP_Force_Verified);

		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the Fact table");
		Assert.assertTrue(helper.isOrderIdPresentInVerificationTable(Long.parseLong(order_id)), "Order id is not present in the verification_status table");
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state), "Order state is pending in the verification_status table");
		Assert.assertFalse(dbHelper.pollDB(verificationDBHost,"SELECT state FROM verification.verification_status where order_id = " + Long.parseLong(order_id) + ";","state", "3",60,600),"Order status is not found in DB");

		Thread.sleep(1000);
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state_force_verified), "Order state is not changing in the verification_status table");
		log.info("         manualOrderDetails_Force_verified_Test completed             ");

	}
	
	
	
	@Test(groups = {"sanity", "regression"},description = "Create a third party order and verify details in fact table")
	public void thirdPartyOrderDetailsForceVerifiedTest() throws Exception{
		log.info("         thirdpartyOrderDetails_Force_verified_Test started             ");

		String order_id = losHelper.getAnOrder_force_verify("thirdparty");
		int BCP_state= VerificationServiceConstants.verificationBCP_Pending;
		int BCP_state_force_verified= (VerificationServiceConstants.verificationBCP_Force_Verified);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the Fact table");
		Assert.assertTrue(helper.isOrderIdPresentInVerificationTable(Long.parseLong(order_id)), "Order id is not present in the verification_status table");
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state), "Order state is pending in the verification_status table");
		Assert.assertFalse(dbHelper.pollDB(verificationDBHost,"SELECT state FROM verification.verification_status where order_id = " + Long.parseLong(order_id) + ";","state", "3",60,600),"Order status is not found in DB");

		Thread.sleep(1000);
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state_force_verified), "Order state is not changing in the verification_status table");
		log.info("         thirdpartyOrderDetails_Force_verified_Test completed             ");

	}
	
	@Test(groups = {"sanity", "regression"},description = "Create a long distance order and verify details in fact table")
	public void longDistanceOrderDetailsForceVerifiedTest() throws Exception{
		log.info("         longDistanceOrderDetails_Force_verified_Test started             ");

		String order_id = losHelper.getAnOrder_force_verify("longdistance");
		int BCP_state= VerificationServiceConstants.verificationBCP_Pending;
		int BCP_state_force_verified= (VerificationServiceConstants.verificationBCP_Force_Verified);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the Fact table");
		Assert.assertTrue(helper.isOrderIdPresentInVerificationTable(Long.parseLong(order_id)), "Order id is not present in the verification_status table");
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state), "Order state is pending in the verification_status table");
		Assert.assertFalse(dbHelper.pollDB(verificationDBHost,"SELECT state FROM verification.verification_status where order_id = " + Long.parseLong(order_id) + ";","state", "3",60,600),"Order status is not found in DB");

		Thread.sleep(1000);
	    Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state_force_verified), "Order state is not changing in the verification_status table");
		log.info("         longDistanceOrderDetails_Force_verified_Test completed             ");

	}
	
	@Test(groups = {"sanity", "regression"},description = "Create a swiggy assured order and verify details in fact table")
	public void swiggyAssuredOrderDetailsForceVerifiedTest() throws Exception{
		
		log.info("        swiggyAssuredOrderDetails_Force_verified_Test started             ");
		String order_id = losHelper.getAnOrder_force_verify("swiggyassured");
		int BCP_state= VerificationServiceConstants.verificationBCP_Pending;
		int BCP_state_force_verified= (VerificationServiceConstants.verificationBCP_Force_Verified);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the Fact table");
		Assert.assertTrue(helper.isOrderIdPresentInVerificationTable(Long.parseLong(order_id)), "Order id is not present in the verification_status table");
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state), "Order state is pending in the verification_status table");
		Assert.assertFalse(dbHelper.pollDB(verificationDBHost,"SELECT state FROM verification.verification_status where order_id = " + Long.parseLong(order_id) + ";","state", "3",60,600),"Order status is not found in DB");

		Thread.sleep(1000);
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state_force_verified), "Order state is not changing in the verification_status table");
		log.info("        swiggyAssuredOrderDetails_Force_verified_Test completed             ");

	}
	
	@Test(groups = {"sanity", "regression"},description = "Create a new user order and verify details in fact table")
	public void newUserOrderDetailsForceVerifiedTest() throws Exception{
		log.info("        newUserOrderDetails_Force_verified_Test started             ");
		String order_id = losHelper.getAnOrder_force_verify("newuser");
		int BCP_state= VerificationServiceConstants.verificationBCP_Pending;
		int BCP_state_force_verified= (VerificationServiceConstants.verificationBCP_Force_Verified);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the Fact table");
		Assert.assertTrue(helper.isOrderIdPresentInVerificationTable(Long.parseLong(order_id)), "Order id is not present in the verification_status table");
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state), "Order state is pending in the verification_status table");
		Assert.assertFalse(dbHelper.pollDB(verificationDBHost,"SELECT state FROM verification.verification_status where order_id = " + Long.parseLong(order_id) + ";","state", "3",60,600),"Order status is not found in DB");

		Thread.sleep(1000);
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state_force_verified), "Order state is not changing in the verification_status table");
		log.info("        newUserOrderDetails_Force_verified_Test completed             ");

	}
	
	@Test(groups = {"sanity", "regression"},description = "Create a pop order and verify details in fact table")
	public void popOrderDetailsForceVerifiedTest() throws Exception{
		log.info("        popOrderDetails_Force_verified_Test started             ");

		String order_id = losHelper.getAnOrder_force_verify("pop");
		int BCP_state= VerificationServiceConstants.verificationBCP_Pending;
		int BCP_state_force_verified= (VerificationServiceConstants.verificationBCP_Force_Verified);
		Assert.assertTrue(helper.isOrderIdPresentInFactTable(Long.parseLong(order_id)), "Order id is not present in the Fact table");
		Assert.assertTrue(helper.isOrderIdPresentInVerificationTable(Long.parseLong(order_id)), "Order id is not present in the verification_status table");
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state), "Order state is pending in the verification_status table");
		Assert.assertFalse(dbHelper.pollDB(verificationDBHost,"SELECT state FROM verification.verification_status where order_id = " + Long.parseLong(order_id) + ";","state", "3",60,600),"Order status is not found in DB");

		Thread.sleep(1000);
		Assert.assertTrue(helper.StateOfOrderInVerificationTable(Long.parseLong(order_id),BCP_state_force_verified), "Order state is not changing in the verification_status table");
		
		log.info("        popOrderDetails_Force_verified_Test completed             ");

    }
	
	
	
	
	
	@AfterMethod
	public void sleep() throws InterruptedException{
		System.out.println("######### Giving a wait ##########");
		Thread.sleep(1500);
		System.out.println("######### waiting time over ##########");
	}
}
