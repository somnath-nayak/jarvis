package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.dp.BatchingDataProvider;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.RTSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BatchingTest extends BatchingDataProvider{
	
	DeliveryServiceHelper helpdel=new DeliveryServiceHelper();
	AutoassignHelper hepauto=new AutoassignHelper();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	CheckoutHelper chelper=new CheckoutHelper();
	RTSHelper rtshelp = new RTSHelper();
	DeliveryDataHelper deldatahelp=new DeliveryDataHelper();
	static Object de_id;
	static String bathcing_de_id;
	
	@BeforeClass
    public void enablebatching() {
	delmeth.dbhelperupdate(DeliveryConstant.batching_enabled_query);
	delmeth.dbhelperupdate(DeliveryConstant.single_rest_batching_enabled_query);
	String query = RTSConstants.enable_resturant_batching
			+ DeliveryConstant.multi_rest_batching_rest1;
	delmeth.dbhelperupdate(query);
	String query1=RTSConstants.enable_resturant_batching
			+ DeliveryConstant.multi_rest_batching_rest2;
	delmeth.dbhelperupdate(query1);
	delmeth.dbhelperupdate(RTSConstants.set_random_de_id);
	de_id=deldatahelp.CreateDE(DeliveryConstant.zone_id_int);
	if(null!=de_id)
	{
		bathcing_de_id=DeliveryConstant.mayur_autoassign_de;
	}
	else
	{
		bathcing_de_id=de_id.toString();
	}
	}
	@AfterMethod
	public void closeallbatches()
	{
		delmeth.dbhelperupdate(DeliveryConstant.updatebatch);
		delmeth.dbhelperupdate(DeliveryConstant.assigndetounassinged);
	}
	@Test(dataProvider = "QE-256",priority=1, enabled = true, groups = {"batchingregression"}, description = "Verify that if the order is a replicated order than it's not considered for batching and also, there is no delay added for this order")
	public void batchingtest256(String order_id1, String order_id2) throws InterruptedException {
		String query=DeliveryConstant.replicate_order+"'"+order_id1+"'";
		delmeth.dbhelperupdate(query);
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertNotEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}

	}
	@Test(dataProvider = "QE-264",priority=2, groups = {"assignmentregression"}, description = "Verify whether two orders from same restaurants under same zone are batched only if both of these orders are non with_de")
	public void Batching264(String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-265",priority=3, enabled = true, groups = {"batchingregression"}, description = "Verify that if the order is a replicated order than it's not considered for batching and also, there is no delay added for this order")
	public void batchingtest265(String order_id1, String order_id2) throws InterruptedException {
		String query=DeliveryConstant.replicate_order+order_id1;
		delmeth.dbhelperupdate(query);
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertNotEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-266",priority=4, enabled = true, groups = "batchingregression", description = "Verify whether batch is spiltted if DE has not yet pickedup for the order which is marked WITH_DE")
	public void batchingtest266(String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			if(batch_id3.equals(batch_id4))
			{
				helpdel.markOrderWithDE(order_id1);
				Thread.sleep(5000);
				batch_id3=delmeth.dbhelperget(query1, "batch_id");
				batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			Assert.assertNotEquals(batch_id3, batch_id4);
			}
		}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-267",priority=5, enabled = true, groups = "batchingregression", description = "Verify whether batch is not spiltted if DE has marked pickedup for the order which is marked WITH_DE")
	public void batchingtest267(String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			if(batch_id3.equals(batch_id4))
			{
				deldatahelp.delocationupdate(DeliveryConstant.single_rest_cust_Dist_200m_lat, DeliveryConstant.single_rest_cust_Dist_200m_lng, bathcing_de_id, DeliveryConstant.delivery_app_version);
				helpdel.assignOrder(order_id1, bathcing_de_id);
				Thread.sleep(10000);
				helpdel.zipDialConfirmDE(order_id1);
				Thread.sleep(10000);
				helpdel.zipDialArrivedDE(order_id1);
				Thread.sleep(10000);
				helpdel.zipDialPickedUpDE(order_id1);
				Thread.sleep(10000);
				helpdel.markOrderWithDE(order_id1);
				Thread.sleep(10000);
    			batch_id3=delmeth.dbhelperget(query1, "batch_id");
				batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			Assert.assertEquals(batch_id3, batch_id4);
			}
		}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-268",priority=6, enabled = true, groups = "batchingregression", description = "Verify whether batch is not spiltted if DE has marked pickedup for the order which is marked WITH_DE")
	public void batchingtest268(String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			if(batch_id3.equals(batch_id4))
			{
				deldatahelp.delocationupdate(DeliveryConstant.single_rest_cust_Dist_200m_lat, DeliveryConstant.single_rest_cust_Dist_200m_lng, bathcing_de_id, DeliveryConstant.delivery_app_version);
				helpdel.assignOrder(order_id1, bathcing_de_id);
				Thread.sleep(10000);
				helpdel.zipDialConfirmDE(order_id1);
				helpdel.zipDialConfirmDE(order_id2);
				Thread.sleep(10000);
				helpdel.zipDialArrivedDE(order_id1);
				helpdel.zipDialArrivedDE(order_id2);
				Thread.sleep(10000);
				helpdel.zipDialPickedUpDE(order_id1);
				Thread.sleep(10000);
				helpdel.markOrderWithDE(order_id1);
				helpdel.markOrderWithDE(order_id2);
				Thread.sleep(10000);
				Assert.assertNotEquals(batch_id3,batch_id4);
				batch_id3=delmeth.dbhelperget(query1, "batch_id");
				batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			Assert.assertNotEquals(batch_id3, batch_id4);
			}
		}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-269",priority=7, enabled = true, groups = {"batchingregression"}, description = "	Verify whether two orders from same restaurants are getting merged even if multi restaurant batching is enabled for a zone")
	public void batchingtest269(String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-270",priority=8, enabled = true, groups = {"batchingregression"}, description = "Verify order is not getting merged with already exisiting batch consisting of two orders if maximum orders in batch is configured as two at zone level")
	public void batchingtest270(String order_id1, String order_id2)
			throws InterruptedException, JSONException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		if (batch_id1 != null && batch_id2!= null ) {
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		if(batch_id3.toString().equalsIgnoreCase(batch_id4.toString()))
		{
			String order_id3= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
			System.out.println(order_id3);
			rtshelp.runassignment(RTSConstants.cityid);
			String query3 = DeliveryConstant.batch_id + order_id3 + ";";
			Object batch_id5 = delmeth.dbhelperget(query3, "batch_id");
			Assert.assertNotEquals(batch_id5, batch_id3);
			Assert.assertNotEquals(batch_id5, batch_id4);
		}}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-271",priority=9, enabled = true, groups = {"batchingregression"}, description = "Verify orders are not getting merged if SLA exceeds max sla of batch configured at zone level")
	public void batchingtest271(String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertNotEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-272",priority=10, enabled = true, groups = {"batchingregression"}, description = "Verify orders are not getting merged if number of items exceeds max item in batch confgiured at zone level")
	public void batchingtest272(String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertNotEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-273",priority=11, enabled = true, groups = {"batchingregression"}, description = "Verify orders are not getting merged if distance between customers exceeds max customer distance in batch configured at zone level")
	public void batchingtest273(String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertNotEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-275",priority=12, groups = "batchingregression", description = "verify order are not eligble for merging if order elapsed time of any of the order exceeds max batch elapsed Time configured at zone level")
	public void Batching275(String order_id1,String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertNotEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-276",priority=13, groups = "batchingregression", description = "Verify two order are not merged again if they were already merged and batch broked because one order got rejected by DE")
	public void Batching276(String order_id1,String order_id2) throws InterruptedException {
	Assert.assertNotNull(order_id1,"Order is not created through order json");
	Assert.assertNotNull(order_id2,"Order is not created through order json");
	String query1 = DeliveryConstant.batch_id + order_id1 + ";";
	String query2 = DeliveryConstant.batch_id + order_id2 + ";";
	Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
	Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
	rtshelp.runassignment(RTSConstants.cityid);
	if (batch_id1 != null && batch_id2!= null ) {
		Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id4 = delmeth.dbhelperget(query2,"batch_id");
		if(batch_id3.toString().equalsIgnoreCase(batch_id4.toString()))
		{
			deldatahelp.delocationupdate(DeliveryConstant.single_rest_cust_Dist_200m_lat, DeliveryConstant.single_rest_cust_Dist_200m_lng, bathcing_de_id, DeliveryConstant.delivery_app_version);
			helpdel.assignOrder(order_id1, bathcing_de_id);
			Thread.sleep(5000);
			helpdel.systemRejectOrder(bathcing_de_id, order_id1);
			Thread.sleep(5000);
			rtshelp.runassignment(RTSConstants.cityid);
			String query3 = DeliveryConstant.batch_id + order_id1 + ";";
			Object batch_id5 = delmeth.dbhelperget(query3, "batch_id");
			Assert.assertNotEquals(batch_id5, batch_id3);
			}
		}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-277",priority=14, enabled = true, groups = "batchingregression", description = "Verify two order are not merged again if they were already merged and batch broked because one order got auto-rejected")
	public void batchingTest277 (String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		if (batch_id1 != null && batch_id2!= null ) {
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2,"batch_id");
			if(batch_id3.toString().equalsIgnoreCase(batch_id4.toString()))
			{
				deldatahelp.delocationupdate(DeliveryConstant.single_rest_cust_Dist_200m_lat, DeliveryConstant.single_rest_cust_Dist_200m_lng, bathcing_de_id, DeliveryConstant.delivery_app_version);
				helpdel.assignOrder(order_id1, bathcing_de_id);
				Thread.sleep(5000);
				helpdel.systemRejectOrder(de_id.toString(), order_id1);
				Thread.sleep(5000);
				rtshelp.runassignment(RTSConstants.cityid);
				String query3 = DeliveryConstant.batch_id + order_id1 + ";";
				Object batch_id5 = delmeth.dbhelperget(query3, "batch_id");
				Assert.assertNotEquals(batch_id5, batch_id3);
				}
			}
			else {
				System.out.println("Batch id not present in Batch table");
				Assert.assertTrue(false);
			}
		}
	@Test(dataProvider = "QE-278",priority=15, groups = "batchingregression", description = "Verify two order are not merged again if they were already merged and batch broked because one order got reassigned from FF")
	public void Batching278(String order_id1,String order_id2) throws InterruptedException
	{
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		if (batch_id1 != null && batch_id2!= null ) {
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2,"batch_id");
			if(batch_id3.toString().equalsIgnoreCase(batch_id4.toString()))
			{
				deldatahelp.delocationupdate(DeliveryConstant.single_rest_cust_Dist_200m_lat, DeliveryConstant.single_rest_cust_Dist_200m_lng, bathcing_de_id, DeliveryConstant.delivery_app_version);
				helpdel.assignOrder(order_id1,bathcing_de_id);
				Thread.sleep(5000);
				helpdel.reassign(order_id1, DeliveryConstant.re_assign_code, DeliveryConstant.re_assing_city_id);
				Thread.sleep(5000);
				rtshelp.runassignment(RTSConstants.cityid);
				String query3 = DeliveryConstant.batch_id + order_id1 + ";";
				Object batch_id5 = delmeth.dbhelperget(query3, "batch_id");
				Assert.assertNotEquals(batch_id5, batch_id3);
				}
			}
			else {
				System.out.println("Batch id not present in Batch table");
				Assert.assertTrue(false);
			}
		}
	@Test(dataProvider = "QE-279",priority=16, enabled = true, groups = "batchingregression", description = "Verify that order is not merged if any one of the orders considered for batching is cancelled")
	public void batchingTest279(String order_id1,String order_id2) throws Exception {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		helpdel.ordercancel(order_id1);
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertNotEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-280",priority=17, enabled = true, groups ="batchingregression",description = "Verify Order is merged with exisitng order assigned to some DE if order status is not marked pickedup")
	public void batchingTest280(String order_id1) throws Exception {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		if (batch_id1 != null) {
			deldatahelp.delocationupdate(DeliveryConstant.single_rest_cust_Dist_200m_lat, DeliveryConstant.single_rest_cust_Dist_200m_lng, bathcing_de_id, DeliveryConstant.delivery_app_version);
			helpdel.assignOrder(order_id1, bathcing_de_id);
			Thread.sleep(10000);
			helpdel.zipDialConfirmDE(order_id1);
			Thread.sleep(10000);
			helpdel.zipDialArrivedDE(order_id1);
			 String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
				String query2 = DeliveryConstant.batch_id + order_id2 + ";";
				Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
				if(batch_id2 !=null)
				{
					rtshelp.runassignment(RTSConstants.cityid);
					Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
					Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
					Assert.assertEquals(batch_id3,batch_id4);
				}
				else
				{
					Assert.assertNotNull(order_id2,"Order is not created through order json");
				}
		}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-281",priority=18, enabled = true, groups = "batchingregression", description = "Verify Order is not merged with exisitng order assigned to some DE if order status is marked pickedup")
	public void batchingTest281(String order_id1) throws Exception 
	{
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		if (batch_id1 != null) {
			deldatahelp.delocationupdate(DeliveryConstant.single_rest_cust_Dist_200m_lat, DeliveryConstant.single_rest_cust_Dist_200m_lng, bathcing_de_id, DeliveryConstant.delivery_app_version);
			helpdel.assignOrder(order_id1, bathcing_de_id);
			Thread.sleep(5000);
			helpdel.zipDialConfirmDE(order_id1);
			Thread.sleep(5000);
			helpdel.zipDialArrivedDE(order_id1);
			Thread.sleep(5000);
			helpdel.zipDialPickedUpDE(order_id1);
			 String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
				String query2 = DeliveryConstant.batch_id + order_id2 + ";";
				Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
				if(batch_id2 !=null)
				{
					rtshelp.runassignment(RTSConstants.cityid);
					Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
					Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
					Assert.assertNotEquals(batch_id3,batch_id4);
				}
				else
				{
					Assert.assertNotNull(order_id2,"Order is not created through order json");
				}
		}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-359",priority=19, enabled = true, groups = {"batchingregression"}, description = "Verify Orders are not getting merged if restaurant is not enabled for batching")
	public void batchingtest359(String order_id1, String order_id2) throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through order json");
		Assert.assertNotNull(order_id2,"Order is not created through order json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		if (batch_id1 != null && batch_id2!= null ) {
			rtshelp.runassignment(RTSConstants.cityid);
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertNotEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
}