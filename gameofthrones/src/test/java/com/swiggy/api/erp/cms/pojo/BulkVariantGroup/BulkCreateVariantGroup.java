package com.swiggy.api.erp.cms.pojo.BulkVariantGroup;

/**
 * Created by kiran.j on 2/21/18.
 */
public class BulkCreateVariantGroup
{
    private Entities[] entities;

    public Entities[] getEntities ()
    {
        return entities;
    }

    public void setEntities (Entities[] entities)
    {
        this.entities = entities;
    }

    public BulkCreateVariantGroup build() {
        Entities entities = new Entities();
        entities.build();
        if(this.getEntities() == null)
            this.setEntities(new Entities[]{entities});
        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [entities = "+entities+"]";
    }
}
