package com.swiggy.api.erp.finance.pojo;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.pojo
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ops",
        "zoneId",
        "issuedAmount",
        "depositAmount",
        "collectedBy",
        "comments",
        "txType",
        "depositedTo"
})

public class AddHubTransaction {

    @JsonProperty("ops")
    private Ops ops;
    @JsonProperty("zoneId")
    private Integer zoneId;
    @JsonProperty("issuedAmount")
    private Integer issuedAmount;
    @JsonProperty("depositAmount")
    private Integer depositAmount;
    @JsonProperty("collectedBy")
    private String collectedBy;
    @JsonProperty("comments")
    private String comments;
    @JsonProperty("txType")
    private String txType;
    @JsonProperty("depositedTo")
    private String depositedTo;

    @JsonProperty("ops")
    public Ops getOps() {
        return ops;
    }

    @JsonProperty("ops")
    public void setOps(Ops ops) {
        this.ops = ops;
    }

    @JsonProperty("zoneId")
    public Integer getZoneId() {
        return zoneId;
    }

    @JsonProperty("zoneId")
    public void setZoneId(Integer zoneId) {
        this.zoneId = zoneId;
    }

    @JsonProperty("issuedAmount")
    public Integer getIssuedAmount() {
        return issuedAmount;
    }

    @JsonProperty("issuedAmount")
    public void setIssuedAmount(Integer issuedAmount) {
        this.issuedAmount = issuedAmount;
    }

    @JsonProperty("depositAmount")
    public Integer getDepositAmount() {
        return depositAmount;
    }

    @JsonProperty("depositAmount")
    public void setDepositAmount(Integer depositAmount) {
        this.depositAmount = depositAmount;
    }

    @JsonProperty("collectedBy")
    public String getCollectedBy() {
        return collectedBy;
    }

    @JsonProperty("collectedBy")
    public void setCollectedBy(String collectedBy) {
        this.collectedBy = collectedBy;
    }

    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    @JsonProperty("txType")
    public String getTxType() {
        return txType;
    }

    @JsonProperty("txType")
    public void setTxType(String txType) {
        this.txType = txType;
    }

    @JsonProperty("depositedTo")
    public String getDepositedTo() {
        return depositedTo;
    }

    @JsonProperty("depositedTo")
    public void setDepositedTo(String depositedTo) {
        this.depositedTo = depositedTo;
    }

    private void setDefaultValues(int zoneID,int DE_ID, int issueAmount, int depositAmount, String txnType) {
        Ops ops = new Ops();
        ops.build(DE_ID);
        if (this.getIssuedAmount() == null)
            this.setIssuedAmount(issueAmount);
        if (this.getDepositAmount() == null)
            this.setDepositAmount(depositAmount);
        if (this.getComments() == null)
            this.setComments("validating txn via automation");
        if (this.getCollectedBy() == null)
            this.setCollectedBy("Automation_CashMgmt");
        if (this.getTxType() == null)
            this.setTxType(txnType);
        if (this.getDepositedTo() == null)
            this.setDepositedTo("Automation_CashMgmt");
        if (this.getZoneId() == null)
            this.setZoneId(zoneID);
        if (this.getOps() == null)
            this.setOps(ops);
    }

    public AddHubTransaction build(int zoneID,int DE_ID, int issueAmount, int depositAmount, String txnType) {
        setDefaultValues(zoneID,DE_ID, issueAmount, depositAmount, txnType);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("ops", ops).append("zoneId", zoneId).append("issuedAmount", issuedAmount).append("depositAmount", depositAmount).append("collectedBy", collectedBy).append("comments", comments).append("txType", txType).append("depositedTo", depositedTo).toString();
    }

}
