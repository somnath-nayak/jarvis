package com.swiggy.api.erp.cms.dp;

import io.advantageous.boon.core.Sys;
import org.testng.annotations.DataProvider;

import java.time.Instant;

public class PreOrderDP {
    @DataProvider(name = "preorderenable")
    public Object[][] preorderEnable(){


        return new Object[][]{
                {"450","preorder_enabled","1"
                        }
        };
    }
    @DataProvider(name = "preorderenabledjenkins")
    public Object[][] preorderEnableJenkins(){


       return new Object[][]{
               {System.getenv("rest_Id"),System.getenv("attribute"),System.getenv("value")}
        };

    }
    @DataProvider(name = "preorderslots")
    public Object[][] preorderSlots(){


        return new Object[][]{
                {"450","Wed","0100","2359"}
        };

    }

    @DataProvider(name = "preorderslotsjenkins")
    public Object[][] preorderSlotsJenkins(){



        return new Object[][]{
                {System.getenv("rest_id"),System.getenv("day"),System.getenv("openTime"),System.getenv("closeTime")}
        };

    }



    @DataProvider(name = "preorderdisable")
    public Object[][] preorderDisable(){


        return new Object[][]{
                {"450","preorder_enabled","0"}
        };
    }


    @DataProvider(name = "areaschedulecreate")
    public Object[][] preorderAreaScheduleCreate(){


        return new Object[][]{
                {"1","PRE_ORDER","WED","PREORDER","1000","2330"}
        };
    }

    @DataProvider(name = "areaschedulecreatejenkins")
    public Object[][] preorderAreaScheduleCreateJen(){


        return new Object[][]{
                {System.getenv("areaId"),System.getenv("menuType"),System.getenv("day"),System.getenv("slotType"),System.getenv("openTime"),System.getenv("closeTime")}
        };
    }
}
