package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.FullMenu.*;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 2/2/18.
 */
public class FullMenuDp {

    @DataProvider(name = "negativeentity")
    public Iterator<Object[]> negativentity() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        FullMenuPojo fullMenuPojo = new FullMenuPojo();
        Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        JsonHelper jsonHelper = new JsonHelper();
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), MenuConstants.negativejson.empty});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), MenuConstants.negativejson.entity});
        entity.setMain_categories(new Main_categories[0]);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), MenuConstants.negativejson.maincategory});
        main_categories.build();
        entity.setMain_categories(new Main_categories[]{main_categories.build()});
        entity.setItems(new Items[0]);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), MenuConstants.negativejson.items});
        return obj.iterator();
    }

    @DataProvider(name = "negativeitems")
    public Iterator<Object[]> negativeitems() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuPojo fullMenuPojo = new FullMenuPojo();
        Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        Items items = new Items();
        main_categories.build();
        items.build();
        items.setPrice(0);
        items.setPricing_combinations(null);
        entity.setMain_categories(new Main_categories[]{main_categories});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo)});
        return obj.iterator();
    }

    @DataProvider(name = "pricingcombinations")
    public Iterator<Object[]> pricingcombinations() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        System.out.println("nnnn");
        List<FullMenuPojo> lists = fullMenuHelper.createPricingData();
        int count = 1;
        boolean flag = false;
        for(FullMenuPojo f: lists) {
        	 flag = count % 2 == 0;
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(f),fullMenuHelper.getItemIds(f.getEntity().getItems()),f.getEntity().getItems(), flag});
          //  obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(f), flag});
            count++;
        }
        return obj.iterator();
    }
    
    
    @DataProvider(name = "negativeOnlyCat")
    public Object[][] negativeOnlyCat() throws IOException {
        FullMenuPojo fullMenuPojo = new FullMenuPojo();
        Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        Items items = new Items();
        main_categories.setId("Automation_CM_MainCatID");
        main_categories.setName("Automation_CM_MainCatName");
        Main_categories main_categoriesValuesToArray[] = new Main_categories[1];
        main_categoriesValuesToArray[0] = main_categories;
        entity.setMain_categories(main_categoriesValuesToArray);
        
        entity.setItems(new Items[0]);
        fullMenuPojo.setEntity(entity);
        JsonHelper jsonHelper = new JsonHelper();
        
        return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo),MenuConstants.negativejson.items}};
    }
    

    @DataProvider(name = "NegativeCategoryIDandNameNull")
    public Object[][] NegativeCategoryIDandNameNull() throws IOException {
        FullMenuPojo fullMenuPojo = new FullMenuPojo();
        Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        Sub_categories subCategories= new Sub_categories();
        Items items = new Items();

        items.build();
        subCategories.build();
        main_categories.setId("");
        main_categories.setName("");   

        Main_categories main_categoriesValuesToArray[] = new Main_categories[1];
        main_categoriesValuesToArray[0] = main_categories;
        entity.setMain_categories(main_categoriesValuesToArray);
        Items ItemValuesToArray[] = new Items[1];
        ItemValuesToArray[0] = items;
        entity.setItems(ItemValuesToArray);
        fullMenuPojo.setEntity(entity);
        JsonHelper jsonHelper = new JsonHelper();
        System.out.println("final json"+jsonHelper.getObjectToJSON(fullMenuPojo));
        
        return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo),MenuConstants.negativejson.maincategory}};
        
       
    }

    @DataProvider(name = "pricing")
    public Iterator<Object[]>  pricing() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = new FullMenuPojo();
        Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        JsonHelper jsonHelper = new JsonHelper();
        main_categories.build();
        entity.setMain_categories(new Main_categories[]{main_categories.build()});
        Items items = new Items();
        items.build();
        items.setPrice(MenuConstants.default_price);
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        System.out.println("price 10 pricing combination has value");
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo),fullMenuHelper.getItemIds(fullMenuPojo.getEntity().getItems()),fullMenuPojo.getEntity().getItems(), true});
     
        items.setPrice(MenuConstants.default_price);
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        items.setPrice(0);
        System.out.println("price 0 but pricing combo has objects with price");
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo),fullMenuHelper.getItemIds(fullMenuPojo.getEntity().getItems()),fullMenuPojo.getEntity().getItems(), true});
     
        items.setPrice(0);
        items.setPricing_combinations(null);
        items.setVariant_groups(null);
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        System.out.println("price 0 and  pricing combiination null");
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo),fullMenuHelper.getItemIds(fullMenuPojo.getEntity().getItems()),fullMenuPojo.getEntity().getItems(), false});
     
        return obj.iterator();
        
        
    }

    @DataProvider(name = "nullvalues")
    public Iterator<Object[]>  nullvalues() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        try {
            FullMenuHelper fullMenuHelper = new FullMenuHelper();
            JsonHelper jsonHelper = new JsonHelper();
            CMSHelper cmsHelper = new CMSHelper();
            FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
            Addon_Groups[] addon_groups = fullMenuHelper.getAddonGroups();
            String rest_id = MenuConstants.rest_id;
            Entity entity = fullMenuPojo.getEntity();
            Addon_Groups addon_group = addon_groups[0];
            addon_group.build();
            addon_group.setAddon_min_limit(-1);
            Items[] items = entity.getItems();
            Items item = items[0];
            item.setPrice(MenuConstants.default_price);
            item.setAddon_groups(new Addon_Groups[]{addon_group});
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);
            System.out.println("addon minlimit -1");
          obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.addonlimit, new ArrayList<>(),entity.getItems(),false});

            Addon_Groups[] addon_groups1 = fullMenuHelper.getAddonGroups();
            Addon_Groups addon_group1 = addon_groups[0];
            addon_group1.build();
            addon_group.setId(null);
            Entity entity1 = fullMenuPojo.getEntity();
            Items[] items1 = entity1.getItems();
            Items item1 = items[0];
            item.setPrice(MenuConstants.default_price);
            item.setAddon_groups(new Addon_Groups[]{addon_group1});
            entity.setItems(new Items[]{item1});
            fullMenuPojo.setEntity(entity1);
            System.out.println("addon_group  id null");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.addongroup_id, new ArrayList<>(),entity.getItems(),false});

            Addon_Groups[] addon_groups2 = fullMenuHelper.getAddonGroups();
            Addon_Groups addon_group2 = addon_groups[0];
            addon_group2.build();
            addon_group2.setName(null);
            Entity entity2 = fullMenuPojo.getEntity();
            Items[] items2 = entity1.getItems();
            Items item2 = items[0];
            item.setPrice(MenuConstants.default_price);
            item.setAddon_groups(new Addon_Groups[]{addon_group2});
            entity.setItems(new Items[]{item2});
            fullMenuPojo.setEntity(entity2);
            System.out.println("addon_group name null");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.addongroup_name, new ArrayList<>(),entity.getItems(),false});

            Addon_Groups[] addon_groups3 = fullMenuHelper.getAddonGroups();
            Addon_Groups addon_group3 = addon_groups3[0];
            addon_group3.build();
            Addons[] addons3 = addon_group3.getAddons();
            Addons addon3 = addons3[0];
            addon3.setId(null);
            Entity entity3 = fullMenuPojo.getEntity();
            Items[] items3 = entity1.getItems();
            Items item3 = items3[0];
            item.setPrice(MenuConstants.default_price);
            item.setIs_veg(MenuConstants.is_veg[0]);
            item.setId(MenuConstants.item_id[0]);
            addon_group3.setAddons(new Addons[]{addon3});
            item.setAddon_groups(new Addon_Groups[]{addon_group3});
            entity.setItems(new Items[]{item3});
            fullMenuPojo.setEntity(entity3);
            System.out.println("addon id null");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.addon_id, new ArrayList<>(),entity.getItems(),false});

            Addon_Groups[] addon_groups4 = fullMenuHelper.getAddonGroups();
            Addon_Groups addon_group4 = addon_groups4[0];
            addon_group4.build();
            Addons[] addons4 = addon_group4.getAddons();
            Addons addon4 = addons4[0];
            addon4.setName(null);
            Entity entity4 = fullMenuPojo.getEntity();
            Items[] items4 = entity1.getItems();
            Items item4 = items4[0];
            item.setPrice(MenuConstants.default_price);
            addon_group4.setAddons(new Addons[]{addon4});
            item.setAddon_groups(new Addon_Groups[]{addon_group4});
            entity.setItems(new Items[]{item4});
            fullMenuPojo.setEntity(entity4);
            System.out.println("addon name null");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.addon_name, new ArrayList<>(),entity.getItems(),false});

            Entity entity5 = fullMenuPojo.getEntity();
            Items[] items5 = entity5.getItems();
            Items item5 = items5[0];
            item5.setId(null);
            entity5.setItems(new Items[]{item5});
            fullMenuPojo.setEntity(entity5);
            System.out.println("item id null");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.item_id, new ArrayList<>(),entity.getItems(),false});

            Entity entity6 = fullMenuPojo.getEntity();
            Items[] items6 = entity6.getItems();
            Items item6 = new Items();
            item6.build();
            item6.setName(null);
            entity6.setItems(new Items[]{item6});
            fullMenuPojo.setEntity(entity6);
            System.out.println("item name null");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.item_name, new ArrayList<>(),entity.getItems(),false});

            Entity entity7 = fullMenuPojo.getEntity();
            Items[] items7 = entity7.getItems();
            Items item7 = new Items();
            item6.build();
            item6.setName(null);
            entity6.setItems(new Items[]{item6});
            fullMenuPojo.setEntity(entity6);
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.item_name, new ArrayList<>(),entity.getItems(),false});

            Entity entity8 = fullMenuPojo.getEntity();
            Items[] items8 = entity8.getItems();
            Items item8 = new Items();
            item8.build();
            Pricing_combinations[] pricing_combinations8 = item8.getPricing_combinations();
            Pricing_combinations pricing_combination8 = pricing_combinations8[0];
            Variant_combination[] variant_combinations8 = pricing_combination8.getVariant_combination();
            Variant_combination variant_combination8 = variant_combinations8[0];
            variant_combination8.setVariant_id(null);
            pricing_combination8.setVariant_combination(new Variant_combination[]{variant_combination8});
            item8.setPricing_combinations(new Pricing_combinations[]{pricing_combination8});
            entity8.setItems(new Items[]{item8});
            fullMenuPojo.setEntity(entity8);
            System.out.println("variant id null in pricing combinations");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.variant_id, new ArrayList<>(),entity.getItems(),false});

            Entity entity9 = fullMenuPojo.getEntity();
            Items[] items9 = entity8.getItems();
            Items item9 = new Items();
            item9.build();
            Pricing_combinations[] pricing_combinations9 = item9.getPricing_combinations();
            Pricing_combinations pricing_combination9 = pricing_combinations9[0];
            Variant_combination[] variant_combinations9 = pricing_combination9.getVariant_combination();
            Variant_combination variant_combination9 = variant_combinations9[0];
            variant_combination9.setVariant_group_id(null);
            pricing_combination9.setVariant_combination(new Variant_combination[]{variant_combination9});
            item9.setPricing_combinations(new Pricing_combinations[]{pricing_combination9});
            entity9.setItems(new Items[]{item9});
            fullMenuPojo.setEntity(entity9);
            System.out.println("Variant_group id is null pricing combination");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.variantgroup_id, new ArrayList<>(),entity.getItems(),false});

            Entity entity10 = fullMenuPojo.getEntity();
            Items[] items10 = entity10.getItems();
            Items item10 = new Items();
            item10.build();
            Pricing_combinations[] pricing_combinations10 = item10.getPricing_combinations();
            Pricing_combinations pricing_combination10 = pricing_combinations10[0];
            Addon_combination[] addon_combinations10 = pricing_combination10.getAddon_combination();
            Addon_combination addon_combination10 = addon_combinations10[0];
            addon_combination10.setAddon_id(null);
            entity10.setItems(new Items[]{item10});
            fullMenuPojo.setEntity(entity10);
            System.out.println("addonId is null pricing combination");
           obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.addonid_pricing, new ArrayList<>(),entity.getItems(),false});

            Entity entity11 = fullMenuPojo.getEntity();
            Items[] items11 = entity11.getItems();
            Items item11 = new Items();
            item11.build();
            Pricing_combinations[] pricing_combinations11 = item11.getPricing_combinations();
            Pricing_combinations pricing_combination11 = pricing_combinations11[0];
            Addon_combination[] addon_combinations11 = pricing_combination11.getAddon_combination();
            Addon_combination addon_combination11 = addon_combinations11[0];
            addon_combination11.setAddon_group_id(null);
            entity11.setItems(new Items[]{item11});
            fullMenuPojo.setEntity(entity11);
            System.out.println("addonGroupId is null pricing combination");
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.addongroupid_pricing, new ArrayList<>(),entity.getItems(),false});

            Entity entity12 = fullMenuPojo.getEntity();
            Items[] items12 = entity12.getItems();
            Items item12 = new Items();
            item12.build();
            Variant_groups[] variant_groups12 = item12.getVariant_groups();
            Variant_groups variant_group12 = variant_groups12[0];
            variant_group12.setId(null);
            item12.setVariant_groups(new Variant_groups[]{variant_group12});
            entity12.setItems(new Items[]{item12});
            fullMenuPojo.setEntity(entity12);

            System.out.println("VariantGroup id null");
           obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.variantgroupid, new ArrayList<>(),entity.getItems(),false});

            Entity entity13 = fullMenuPojo.getEntity();
            Items[] items13 = entity13.getItems();
            Items item13 = new Items();
            item13.build();
            Variant_groups[] variant_groups13 = item13.getVariant_groups();
            Variant_groups variant_group13 = variant_groups13[0];
            variant_group13.setName(null);
            item13.setVariant_groups(new Variant_groups[]{variant_group13});
            entity13.setItems(new Items[]{item13});
            fullMenuPojo.setEntity(entity13);

            System.out.println("Variant Group Name null");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.variantgroupname, new ArrayList<>(),entity.getItems(),false});

            FullMenuPojo fullMenuPojo1 = fullMenuHelper.createDefaultFullMenuPojo();

            Entity entity14 = fullMenuPojo1.getEntity();
            Items[] items14 = entity14.getItems();
            Items item14 = new Items();
            item14.build();
            Variant_groups variant_groups = item14.getVariant_groups()[0];
            variant_groups.setId(null);
            item14.setVariant_groups(new Variant_groups[]{variant_groups});
            entity14.setItems(new Items[]{item14});
            fullMenuPojo.setEntity(entity14);
            System.out.println("VariantID  null");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo1), MenuConstants.variantid, new ArrayList<>(),entity.getItems(),false});

            Entity entity15 = fullMenuPojo1.getEntity();
            Items[] items15 = entity15.getItems();
            Items item15 = new Items();
            item15.build();
            Variant_groups variant_groups1 = item15.getVariant_groups()[0];
            variant_groups1.setName(null);
            item14.setVariant_groups(new Variant_groups[]{variant_groups1});
            entity15.setItems(new Items[]{item15});
            fullMenuPojo.setEntity(entity15);
            System.out.println("VariantName  null");

         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo1), MenuConstants.variantname, new ArrayList<>(),entity.getItems(),false});

          //  positive case with successs
            FullMenuPojo fullMenuPojo2 = fullMenuHelper.createDefaultFullMenuPojo();
            Addon_Groups[] addon_groups16 = fullMenuHelper.getAddonGroups();
            Addon_Groups addon_group16 = addon_groups[0];
            addon_group16.build();
            addon_group16.setAddon_min_limit(0);
            Entity entity16 = fullMenuPojo2.getEntity();
            Items[] items16 = entity16.getItems();
            Items item16 = new Items();
            item16.setPrice(MenuConstants.default_price);
            item16.setAddon_groups(new Addon_Groups[]{addon_group16});
            Main_categories new_cat, old_cat;
            new_cat = fullMenuHelper.getDefaultMainCategories();
            old_cat = fullMenuHelper.getRandomMainCategories(rest_id);
            Thread.sleep(1000);
            Items new_item, old_item;
            new_item = fullMenuHelper.getDefaultItems();
            old_item = fullMenuHelper.getRandomItems(rest_id);
            String old_cat_id=null, old_subcat_id=null;

            old_cat_id = cmsHelper.getFullMenuCategory(old_item.getId(), rest_id).get(0).get("external_id").toString();
            old_subcat_id = cmsHelper.getFullMenuSubcategory(old_item.getId(), rest_id).get(0).get("external_id").toString();

            new_item.setCategory_id(new_cat.getId());
            new_item.setSub_category_id(new_cat.getSub_categories()[0].getId());

            old_item.setCategory_id(old_cat_id);
            old_item.setSub_category_id(old_subcat_id);

            old_cat.setId(old_cat_id);
            old_cat.getSub_categories()[0].setCategory_id(old_cat_id);
            old_cat.getSub_categories()[0].setId(old_subcat_id);

            item16.setCategory_id(new_cat.getId());
            item16.setSub_category_id(new_cat.getSub_categories()[0].getId());
            item16.setId(String.valueOf(Instant.now().getEpochSecond()));
            
            if (old_subcat_id==null)
            {
            	entity16.setItems(new Items[]{new_item, new_item});
                entity16.setMain_categories(new Main_categories[]{new_cat, new_cat});
              
            }
            else
            { entity16.setItems(new Items[]{new_item, old_item});
            //TODO: still working on it

            entity16.setMain_categories(new Main_categories[]{new_cat, old_cat});
            }
            fullMenuPojo2.setEntity(entity16);
            Thread.sleep(1000);
            obj.add(new Object[]{rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo2), MenuConstants.catIdBlank, fullMenuHelper.getItemIds(entity16.getItems()),entity16.getItems(),false});

            Addon_Groups[] addon_groups17 = fullMenuHelper.getAddonGroups();
            Addon_Groups addon_group17 = addon_groups[0];
            addon_group17.build();
            addon_group17.setAddon_min_limit(70);
            Entity entity17 = fullMenuPojo2.getEntity();
            Items item17 = new Items();
            item17.build();
            item17.setPrice(MenuConstants.default_price);
            item17.setAddon_groups(new Addon_Groups[]{addon_group17});
            entity17.setItems(new Items[]{item17});
            fullMenuPojo2.setEntity(entity17);

            System.out.println("Addon limit >50");
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo2), MenuConstants.addonlimit, new ArrayList<>(),entity.getItems(),false});

            Addon_Groups[] addon_groups18 = fullMenuHelper.getAddonGroups();
            Addon_Groups addon_group18 = addon_groups[0];
            addon_group18.build();
            addon_group18.setAddon_free_limit(0);
            Entity entity18 = fullMenuPojo2.getEntity();
            Items item18 = new Items();
            item18.build();
            item18.setPrice(MenuConstants.default_price);
            item18.setAddon_groups(new Addon_Groups[]{addon_group18});
            entity18.setItems(new Items[]{item18});
            fullMenuPojo2.setEntity(entity18);

            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo2), MenuConstants.addonlimit, new ArrayList<>(),entity.getItems(),false});

            Addon_Groups[] addon_groups19 = fullMenuHelper.getAddonGroups();
            Addon_Groups addon_group19 = addon_groups[0];
            addon_group19.build();
            addon_group19.setAddon_free_limit(70);
            Entity entity19 = fullMenuPojo2.getEntity();
            Items item19 = new Items();
            item19.build();
            item19.setPrice(MenuConstants.default_price);
            item19.setAddon_groups(new Addon_Groups[]{addon_group19});
            entity19.setItems(new Items[]{item19});
            fullMenuPojo2.setEntity(entity19);

            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo2), MenuConstants.addonfreelimit, new ArrayList<>(),entity.getItems(),false});

            Addon_Groups[] addon_groups20 = fullMenuHelper.getAddonGroups();
            Addon_Groups addon_group20 = addon_groups[0];
            addon_group20.build();
            addon_group20.setAddon_free_limit(-1);
            addon_group20.setAddon_min_limit(0);
            Entity entity20 = fullMenuPojo2.getEntity();
            Items item20 = new Items();
            item20.build();
            item20.setPrice(MenuConstants.default_price);
            item20.setId(String.valueOf(Instant.now().getEpochSecond()));
            item20.setAddon_groups(new Addon_Groups[]{addon_group20});
            new_cat = fullMenuHelper.getDefaultMainCategories();
            old_cat = fullMenuHelper.getRandomMainCategories(rest_id);

            new_item = fullMenuHelper.getDefaultItems();
            old_item = fullMenuHelper.getRandomItems(rest_id);

            new_item.setCategory_id(new_cat.getId());
            new_item.setSub_category_id(new_cat.getSub_categories()[0].getId());

            old_item.setCategory_id(old_cat.getId());
            old_item.setSub_category_id(old_cat.getSub_categories()[0].getId());
            item20.setCategory_id(new_cat.getId());
            item20.setSub_category_id(new_cat.getSub_categories()[0].getId());
            item20.setId(String.valueOf(Instant.now().getEpochSecond()));
            entity20.setItems(new Items[]{new_item, old_item});
            entity20.setMain_categories(new Main_categories[]{new_cat, old_cat});
            fullMenuPojo2.setEntity(entity20);
            Thread.sleep(1000);
           obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo2), new String[]{}, fullMenuHelper.getCatIds(fullMenuPojo2.getEntity().getMain_categories()),true});
           obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo2), MenuConstants.addonfreelimit, fullMenuHelper.getItemIds(entity20.getItems()),entity20.getItems(),true});
            
           
        } catch(Exception e) {
            e.printStackTrace();
        }
        return obj.iterator();
    }

    @DataProvider(name = "combinationprice")
    public Iterator<Object[]> combinationprice() throws IOException, InterruptedException {
        List<Object[]> obj = new ArrayList<>();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuPojo fullMenuPojo = new FullMenuPojo();
         Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        Items items = new Items();
        main_categories.build();
        items.build();
        Addon_Groups addon_groups = new Addon_Groups();
        addon_groups.build();
        items.setAddon_groups(new Addon_Groups[]{addon_groups});
        items.setPrice(MenuConstants.default_price);
        items.setPricing_combinations(new Pricing_combinations[]{});
        items.setVariant_groups(new Variant_groups[]{});
        entity.setMain_categories(new Main_categories[]{main_categories});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        Entity entity1 = new Entity();
        Main_categories main_categories1 = new Main_categories();
        Items items1 = new Items();
        main_categories1.build();
        items1.build();
        items1.setPrice(MenuConstants.default_price);
        items1.setPricing_combinations(new Pricing_combinations[]{});
        items1.setVariant_groups(new Variant_groups[]{});
        entity1.setMain_categories(new Main_categories[]{main_categories1});
        entity1.setItems(new Items[]{items1});
        fullMenuPojo.setEntity(entity1);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity1.getItems()),entity1.getItems(),true});


        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        Pricing_combinations pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        Addon_combination addon_combination = pricing_combinations.getAddon_combination()[0];
        Variant_groups variantgroups=new Variant_groups();
        variantgroups.build();
        addon_combination.setAddon_group_id(null);
        pricing_combinations.setAddon_combination(new Addon_combination[]{addon_combination});
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        items.setVariant_groups(new Variant_groups[] {variantgroups});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.addongroupid_pricing, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        addon_combination = pricing_combinations.getAddon_combination()[0];
        addon_combination.setAddon_id(null);
        pricing_combinations.setAddon_combination(new Addon_combination[]{addon_combination});
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.addonid_pricing, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        pricing_combinations.setPrice(0);
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.pricing_price,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        pricing_combinations.setAddon_combination(null);
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.pricing_addon,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        pricing_combinations.setVariant_combination(null);
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.pricing_variant, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        String variantgroup_id1 = fullMenuHelper.generateId();
        Variant_combination variant_combination = new Variant_combination();
        variant_combination.build().setVariant_group_id(variantgroup_id1);
        Thread.sleep(2000);
        pricing_combinations.setVariant_combination(new Variant_combination[]{variant_combination});
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        Variant_groups variant_groups = new Variant_groups();
       // variant_id1 = fullMenuHelper.generateId();
        variant_groups.build().setId(variantgroup_id1);
        items.setVariant_groups(new Variant_groups[]{variant_groups});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
      obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        String variant_id1 = fullMenuHelper.generateId();
        variant_combination = new Variant_combination();
        variant_combination.build().setVariant_id(variant_id1);
        Thread.sleep(2000);
        pricing_combinations.setVariant_combination(new Variant_combination[]{variant_combination});
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        variant_groups = new Variant_groups();
        variant_groups.build();
        Variants variants = variant_groups.getVariants()[0];
        variants.setId(variant_id1);
        variant_groups.setVariants(new Variants[]{variants});
        items.setVariant_groups(new Variant_groups[]{variant_groups});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
      obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        String addonGroup_id1 = fullMenuHelper.generateId();
        Addon_combination addon_combination1 = new Addon_combination();
        addon_combination1.build().setAddon_group_id(addonGroup_id1);
        Thread.sleep(2000);
        pricing_combinations.setAddon_combination(new Addon_combination[]{addon_combination1});
        pricing_combinations.setVariant_combination(new Variant_combination[]{variant_combination});     
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        Addon_Groups addonGroups = new Addon_Groups();
        addonGroups.build().setId(addonGroup_id1);
        items.setAddon_groups(new Addon_Groups[]{addonGroups});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        String addon_id1 = fullMenuHelper.generateId();
        addon_combination = new Addon_combination();
        addon_combination.build().setAddon_id(addon_id1);
        Variant_groups vg=new Variant_groups();
        vg.build();
        Thread.sleep(2000);
        pricing_combinations.setAddon_combination(new Addon_combination[]{addon_combination});
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        addon_groups  = new Addon_Groups();
        addon_groups.build();
        Addons addons = addon_groups.getAddons()[0];
        addons.build().setId(addon_id1);
        addon_groups.setAddons(new Addons[]{addons});
        items.setAddon_groups(new Addon_Groups[]{addon_groups});
        items.setVariant_groups(new Variant_groups[] {vg});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        variant_id1 = fullMenuHelper.generateId();
        variant_combination = new Variant_combination();
        variant_combination.build().setVariant_group_id(variant_id1);
        Thread.sleep(2000);
        pricing_combinations.setVariant_combination(new Variant_combination[]{variant_combination});
        items.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        
        variant_groups = new Variant_groups();
        variant_groups.build();
        variant_groups.setId(variant_id1);
        Addon_Groups ag  = new Addon_Groups();
        ag.build();
        items.setAddon_groups(new Addon_Groups[] {ag});
        items.setVariant_groups(new Variant_groups[]{variant_groups});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
     obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        variant_groups = items.getVariant_groups()[0];
        variant_groups.setOrder(1);
        items.setVariant_groups(new Variant_groups[]{variant_groups});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
   obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        entity = fullMenuPojo.getEntity();
        items = entity.getItems()[0];
        variant_groups = items.getVariant_groups()[0];
        variants = variant_groups.getVariants()[0];
        variants.setOrder(1);
        variant_groups.setVariants(new Variants[]{variants});
        items.setVariant_groups(new Variant_groups[]{variant_groups});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
      obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        return obj.iterator();
    }

    @DataProvider(name = "imageformat")
    public Iterator<Object[]> imageformat() throws IOException {
            List<Object[]> obj = new ArrayList<>();
            FullMenuHelper fullMenuHelper = new FullMenuHelper();
            JsonHelper jsonHelper = new JsonHelper();
            FullMenuPojo fullMenuPojo = new FullMenuPojo();
            Entity entity = new Entity();
            Main_categories main_categories = new Main_categories();
            Items items = new Items();
            main_categories.build();
            items.build();
            items.setImage_url(MenuConstants.jpg);
            entity.setMain_categories(new Main_categories[]{main_categories});
            entity.setItems(new Items[]{items});
            fullMenuPojo.setEntity(entity);
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

            Entity entity1 = fullMenuPojo.getEntity();
            Main_categories main_categories1 = new Main_categories();
            Items items1 = new Items();
            main_categories1.build();
            items1.build();
            items1.setImage_url(MenuConstants.jpeg);
            entity1.setMain_categories(new Main_categories[]{main_categories1});
            entity1.setItems(new Items[]{items1});
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity1.getItems(),true});

            Entity entity2 = fullMenuPojo.getEntity();
            Main_categories main_categories2 = new Main_categories();
            Items items2 = new Items();
            main_categories2.build();
            items2.build();
            items2.setImage_url(MenuConstants.png);
            entity2.setMain_categories(new Main_categories[]{main_categories2});
            entity2.setItems(new Items[]{items2});
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity2.getItems(),true});
            return obj.iterator();
    }

    @DataProvider(name = "itemslots")
    public Iterator<Object[]> itemslots() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        int[] days = MenuConstants.invalid_day_week;
        int[] openclose = MenuConstants.open_close;
        for(int i=0;i<days.length;i++) {
            Entity entity = fullMenuPojo.getEntity();
            Items[] items = entity.getItems();
            Items item = items[0];
            Item_Slots item_slots = new Item_Slots();
            item_slots.setDay_of_week(days[i]);
            item.setItem_slots(new Item_Slots[]{item_slots});
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);
          obj.add(new Object[]{MenuConstants.rest_id,jsonHelper.getObjectToJSON(fullMenuPojo) , MenuConstants.invalid_day_errors, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }

        for(int i=0;i<openclose.length;i++) {
            Entity entity = fullMenuPojo.getEntity();
            Items item = new Items();
            item.build();
            Item_Slots item_slots = item.getItem_slots()[0];
            item_slots.setOpen_time(openclose[i]);
            item_slots.setClose_time(openclose[i]);
            item.setItem_slots(new Item_Slots[]{item_slots});
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);
            obj.add(new Object[]{MenuConstants.rest_id,jsonHelper.getObjectToJSON(fullMenuPojo) , MenuConstants.invalid_slot, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }

        Entity entity = fullMenuPojo.getEntity();
        Items item = new Items();
        item.build();
        Item_Slots item_slots = item.getItem_slots()[0];
        item_slots.setOpen_time(1200);
        item_slots.setClose_time(600);
        item.setItem_slots(new Item_Slots[]{item_slots});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);

       obj.add(new Object[]{MenuConstants.rest_id,jsonHelper.getObjectToJSON(fullMenuPojo) , MenuConstants.greateropentime, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        entity = fullMenuPojo.getEntity();
        Items item1 = new Items();
        item1.build();
        item1.setItem_slots(null);
        entity.setItems(new Items[]{item1});
        fullMenuPojo.setEntity(entity);

     obj.add(new Object[]{MenuConstants.rest_id,jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo) , new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        return obj.iterator();
    }

    @DataProvider(name = "addoncombinations")
    public Iterator<Object[]> addoncombinations() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuHelper.setTokens();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        JsonHelper jsonHelper = new JsonHelper();
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(fullMenuPojo.getEntity().getItems()),fullMenuPojo.getEntity().getItems(),true});

        Addons[] addons = fullMenuHelper.getAddons();
        Addons addon = addons[0];
        addon.setIn_stock(false);
        Entity entity = fullMenuPojo.getEntity();
        Items item = entity.getItems()[0];
        Addon_Groups addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        System.out.println("Addons is" + jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo));
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setIn_stock(true);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        System.out.println("Addons is" + jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo));
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setIs_veg(0);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setIs_veg(1);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setIs_veg(2);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setIs_veg(3);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setIs_default(false);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setIs_default(true);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setOrder(0);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setOrder(1);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups = item.getAddon_groups()[0];
        addon_groups.setAddons(new Addons[]{addon});
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setIs_default(true);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups.build();
        addon_groups.setOrder(0);
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        addons = fullMenuHelper.getAddons();
        addon = addons[0];
        addon.setIs_default(true);
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        addon_groups.build();
        addon_groups.setOrder(1);
        item.setAddon_groups(new Addon_Groups[]{addon_groups});
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        int[] limit = MenuConstants.limitcombinations;

        for(int i=0; i<(limit.length/2);i++) {
            entity = fullMenuPojo.getEntity();
            item = entity.getItems()[0];
            addon_groups.build();
            addon_groups.setAddon_min_limit(limit[i]);
            addon_groups.setAddon_limit(limit[i+1]);
            item.setAddon_groups(new Addon_Groups[]{addon_groups});
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }

        for(int i=0; i<(limit.length/2);i++) {
            entity = fullMenuPojo.getEntity();
            item = entity.getItems()[0];
            addon_groups.build();
            addon_groups.setAddon_free_limit(limit[i]);
            addon_groups.setAddon_limit(limit[i+1]);
            item.setAddon_groups(new Addon_Groups[]{addon_groups});
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }

        return obj.iterator();
    }

    @DataProvider(name = "gstdp")
    public Iterator<Object[]> gstdp() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Items item = entity.getItems()[0];
        item.setGst_details(null);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        Gst_Details gst_details = new Gst_Details();
        gst_details.build();
        item.setGst_details(gst_details);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        gst_details = new Gst_Details();
        gst_details.build();
        item.setGst_details(gst_details);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        gst_details = new Gst_Details();
        gst_details.build();
        gst_details.setIgst(MenuConstants.invalid_igst);
        gst_details.setCgst(MenuConstants.invalid_cgst);
        gst_details.setSgst(MenuConstants.invalid_sgst);
        item.setGst_details(gst_details);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.gst,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        gst_details = new Gst_Details();
        gst_details.build();
        gst_details.setIgst(MenuConstants.negative_igst);
        gst_details.setCgst(MenuConstants.negative_cgst);
        gst_details.setSgst(MenuConstants.negative_sgst);
        item.setGst_details(gst_details);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.gst,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        gst_details = new Gst_Details();
        gst_details.build();
        gst_details.setInclusive(false);
        item.setGst_details(gst_details);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        gst_details = new Gst_Details();
        gst_details.build();
        gst_details.setInclusive(true);
        item.setGst_details(gst_details);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});


        return obj.iterator();
    }

    @DataProvider(name = "catcombinations")
    public Iterator<Object[]> catcombinations() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Main_categories main_categories = entity.getMain_categories()[0];
        main_categories.setId(null);
        entity.setMain_categories(new Main_categories[]{main_categories});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.catid,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        entity = fullMenuPojo.getEntity();
        main_categories = entity.getMain_categories()[0];
        main_categories.setName(null);
        entity.setMain_categories(new Main_categories[]{main_categories});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.catname,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        entity = fullMenuPojo.getEntity();
        main_categories = entity.getMain_categories()[0];
        Sub_categories sub_categories = main_categories.getSub_categories()[0];
        sub_categories.setId(null);
        main_categories.setSub_categories(new Sub_categories[]{sub_categories});
        entity.setMain_categories(new Main_categories[]{main_categories});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.subcatid,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        entity = fullMenuPojo.getEntity();
        main_categories = entity.getMain_categories()[0];
        sub_categories = main_categories.getSub_categories()[0];
        sub_categories.setName(null);
        main_categories.setSub_categories(new Sub_categories[]{sub_categories});
        entity.setMain_categories(new Main_categories[]{main_categories});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.subcatname,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        return obj.iterator();
    }


    @DataProvider(name = "itemcombinations")
    public Iterator<Object[]> itemcombinations() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        JsonHelper jsonHelper = new JsonHelper();
        int [] veg = MenuConstants.is_veg;
        FullMenuPojo fullMenuPojo;
        Entity entity;
        Items item;

        for(int i = 0; i < veg.length; i++) {
            fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
            entity = fullMenuPojo.getEntity();
            item = entity.getItems()[0];
            item.setIs_veg(veg[i]);
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);

            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }

        boolean[] enable = MenuConstants.enable;

        for(int i = 0; i < enable.length; i++) {
            fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
            entity = fullMenuPojo.getEntity();
            item = entity.getItems()[0];
            item.setEnable(enable[i]);
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);

            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }

        boolean[] in_stock = MenuConstants.in_stock;

        for(int i = 0; i < in_stock.length; i++) {
            fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
            entity = fullMenuPojo.getEntity();
            item = entity.getItems()[0];
            item.setIn_stock(in_stock[i]);
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);

            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }

        fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        item.setAddon_limit(-1);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        item.setAddon_free_limit(-1);
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

        fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        entity = fullMenuPojo.getEntity();
        item = entity.getItems()[0];
        item.setPacking_charges("-10");
        entity.setItems(new Items[]{item});
        fullMenuPojo.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.packaging_Charges,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        int[] price = MenuConstants.limitcombinations;
        for(int i=0; i<(price.length)/2; i++) {
            fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
            entity = fullMenuPojo.getEntity();
            item = entity.getItems()[0];
            item.setPacking_charges(Integer.toString(price[i]));
            item.setPrice(Double.parseDouble(Integer.toString(price[i+1])));
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);

            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }

        Addon_Groups addon_groups = fullMenuHelper.getAddonGroups()[0];
        for(int i=0; i<(price.length)/2; i++) {
                entity = fullMenuPojo.getEntity();
                item = entity.getItems()[0];
                addon_groups.build();
                addon_groups.setAddon_free_limit(price[i+1]);
                item.setAddon_groups(new Addon_Groups[]{addon_groups});
                item.setAddon_free_limit(price[i]);
                entity.setItems(new Items[]{item});
                fullMenuPojo.setEntity(entity);
                obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }

        for(int i=0; i<(price.length)/2; i++) {
            entity = fullMenuPojo.getEntity();
            item = entity.getItems()[0];
            addon_groups.build();
            addon_groups.setAddon_limit(price[i+1]);
            item.setAddon_groups(new Addon_Groups[]{addon_groups});
            item.setAddon_limit(price[i]);
            entity.setItems(new Items[]{item});
            fullMenuPojo.setEntity(entity);
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        }


       
        return obj.iterator();
    }
    
    
    @DataProvider(name = "NegativeItemsWithAddonsNullinPc")
    public Object[][] NegativeItemsWithVariantsAndAddonsNullinPc() throws IOException {
       JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Items items = entity.getItems()[0];
        Pricing_combinations pricing_combinations = items.getPricing_combinations()[0];
        pricing_combinations.setAddon_combination(new Addon_combination[] {});
        items.setPricing_combinations(new Pricing_combinations[] {pricing_combinations});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
     return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true}};
    }

    @DataProvider(name = "NegativeItemsWithVGwithoutV")
    public Object[][] NegativeItemsWithVGwithoutV() throws IOException {
       JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Items items = entity.getItems()[0];
        Variant_groups VG=items.getVariant_groups()[0];
        VG.setVariants(null);
        items.setVariant_groups(new Variant_groups[] {VG});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        
     return new Object[][] {{MenuConstants.rest_id,jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.variantNullInVG, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false}};
    }
    
    
    @DataProvider(name = "NegativeItemsWithAGwithoutA")
    public Object[][] NegativeItemsWithAGwithoutA() throws IOException {
       JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Items items = entity.getItems()[0];
        Addon_Groups AG=items.getAddon_groups()[0];
        AG.setAddons(null);
        items.setAddon_groups(new Addon_Groups[] {AG});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        
     return new Object[][] {{MenuConstants.rest_id,jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.VandAdoonsNull,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false}};
    }
    
    
    //** Created by HemaGovindaraj **//
    
    @DataProvider(name = "NegativeItemWithVariantsPricingComboWithPricewithoutVariant")
    public Iterator<Object[]> NegativeItemWithVariantsPricingComboWithPricewithoutVariant() throws IOException {
    	 List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Items items = entity.getItems()[0];
        Pricing_combinations pricing_combinations = items.getPricing_combinations()[0];
        pricing_combinations.setVariant_combination(null);
        items.setPricing_combinations(new Pricing_combinations[] {pricing_combinations});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.variantPriceerror,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

        
     return obj.iterator();
     }

    
    
    @DataProvider(name = "Multiple_variant_cases")
    public Iterator<Object[]>  Multiple_variant_cases() throws IOException {
    	 
    	 List<Object[]> obj = new ArrayList<>();
         FullMenuHelper fullMenuHelper = new FullMenuHelper();
         JsonHelper jsonHelper = new JsonHelper();
         FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
         Entity entity = fullMenuPojo.getEntity();
         Items items = new Items();
         Variant_groups variantGroup=new Variant_groups();
         Variants variant=new Variants();
         items.build();
         variantGroup.build();
         variant.build();
         variant.setDefault(true);  
         variantGroup.setVariants(new Variants[] {variant});
         items.setVariant_groups(new Variant_groups[]{variantGroup});
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
         System.out.println("variants default flag true");
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),  new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

         
      
         items.build();
         variantGroup.build();
         variant.build();
         variant.setDefault(false);  
         variantGroup.setVariants(new Variants[] {variant});
         items.setVariant_groups(new Variant_groups[]{variantGroup});
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
         System.out.println("variant default flag false");
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),  new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});

         items.build();
         variantGroup.build();
         variant.build();
         variant.setDefault(null); 
         variantGroup.setVariants(new Variants[] {variant});
         items.setVariant_groups(new Variant_groups[]{variantGroup});
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
         System.out.println("variant default flag false");
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), MenuConstants.variantDefaultFlagNull, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

         
         items.build();
         variantGroup.build();
         variant.build();
         variant.setId("variant_id_MissMatch with PC");
         variantGroup.setVariants(new Variants[] {variant});
         items.setVariant_groups(new Variant_groups[]{variantGroup});
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
         System.out.println(" Pricing Combo variant default dependent variable missmatch flag false");
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), MenuConstants.variantId_inPCMissMatch, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});
         
         items.build();
         variantGroup = new Variant_groups();
         variantGroup.build();
         variantGroup.setId("VariantGroup_Id_miss_match_WithPC");
         variant.build();
         variantGroup.setVariants(new Variants[] {variant});
         items.setVariant_groups(new Variant_groups[]{variantGroup});
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
         System.out.println(" Pricing Combo variant default dependent variable missmatch flag false");
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), MenuConstants.variantgroupId_inPCMissMatch, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});
         
         return obj.iterator();
         
    }
    
    
    
    @DataProvider(name = "Multiple_Addon_cases")
    public Iterator<Object[]>  Multiple_Addon_cases() throws IOException {
    	 
    	 List<Object[]> obj = new ArrayList<>();
         FullMenuHelper fullMenuHelper = new FullMenuHelper();
         JsonHelper jsonHelper = new JsonHelper();
         FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
         Entity entity = fullMenuPojo.getEntity();
         Items items = new Items();
         items.build();
         Addon_Groups ag=new Addon_Groups();
         ag.build();
         ag.setId("AddonGroupId Differ from PC");
         items.setAddon_groups(new Addon_Groups[] {ag});
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
         System.out.println("Addon Group id different from ag id in pc");
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),   MenuConstants.addongroupId_inPCMissMatch,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

         ag=new Addon_Groups();
         Addons addon=new Addons();
         ag.build();
         addon.build();
         addon.setId("AddonId Differ from PC");
         ag.setAddons(new Addons[] {addon});
         items.setAddon_groups(new Addon_Groups[] {ag});
         fullMenuPojo.setEntity(entity);
         System.out.println("Addon Group id different from ag id in pc");
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),   MenuConstants.addonId_inPCMissMatch,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});

         return obj.iterator();
         
    }

   
    @DataProvider(name = "PricingCombination_onInput")
    public Iterator<Object[]>  PricingCombination_onInput() throws IOException {
    	List<Object[]> obj = new ArrayList<>();
    	  FullMenuHelper fullMenuHelper = new FullMenuHelper();
          JsonHelper jsonHelper = new JsonHelper();
          FullMenuPojo fullMenuPojo = fullMenuHelper.createFullMenuPojo("Cat_id", "Subcat_id", "Item_id",3, 2, 1, 2, 100);
         Entity entity = fullMenuPojo.getEntity();
         System.out.println(jsonHelper.getObjectToJSON(fullMenuHelper.createFullMenuPojo("Cat_id", "Subcat_id", "Item_id",3, 2, 1, 2, 100)));
        
        // obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),   new String[]{}});
          obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
          
          
          return obj.iterator();
         
    }

   


}
