package com.swiggy.api.erp.cms.pojo.AttributeItem;

import com.swiggy.api.erp.cms.constants.AttributeConstants;

public class Quantity {
    private String unit;
    private  int value;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Quantity build(){
        setDefaultValues();
        return this;
    }
    public void setDefaultValues(){

        if(this.getUnit()==null){
            this.setUnit(AttributeConstants.unit);
        }
        if(this.getValue()==0){
            this.setValue(AttributeConstants.value);
        }

    }
    @Override
    public String toString()
    {
        return "ClassPojo [unit = "+unit+",value = "+value+"]";
    }
}
