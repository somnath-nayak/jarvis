package com.swiggy.api.erp.ff.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.constants.VerificationServiceConstants;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class LOSHelper implements LosConstants{
	
	Initialize gameofthrones = new Initialize();


	RabbitMQHelper rmqHelper = new RabbitMQHelper();
	OMSHelper omsHelper = new OMSHelper();

    public String createManualOrder(String orderId, String orderTime, String epochTime) throws IOException {
        File file = new File("../Data/Payloads/JSON/losCreateManualOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }

    public String createManualOrder(String orderId, String orderTime, String epochTime,String item_id) throws IOException {
        File file = new File("../Data/Payloads/JSON/losCreateManualOrder2.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${item_id}",item_id);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }

    public String createManualOrder(String orderId, String orderTime, String epochTime,String item_id,String restId,String customerId,String bill) throws IOException {
        File file = new File("../Data/Payloads/JSON/losCreateManualOrder3.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${item_id}",item_id).replace("${restId}",restId).replace("${customerId}",customerId).replace("${bill}",bill);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }


    public String createCheckoutOrder(String orderTime, String epochTime) throws IOException {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CheckoutConstants.checkout);
        sqlTemplate.execute(OMSConstants.OrderInsert);
        List<Map<String, Object>> lId =  sqlTemplate.queryForList(OMSConstants.OrderQuery);
        String orderId = lId.get(0).toString().substring(4,(lId.get(0).toString().length()-1));
        System.out.println(">>>>>>>>>>>>++++>>>>>>>"+orderId);
        File file = new File("../Data/Payloads/JSON/losCreateOrderCheckout.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }




    public String createOrderWithDifferentPaymentMethods(String paymentMethod, String txnId) throws IOException, InterruptedException {

        Thread.sleep(1000);
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String orderId = order_idFormat.format(date);
        String orderTime = dateFormat.format(date);
        System.out.println("Order time = " + orderTime);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        System.out.println("Epoch time = " + epochTime);

        File file = new File("../Data/Payloads/JSON/los_createOrderWithDifferentPaymentMethods.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${txnId}", txnId).replace("${paymentMethod}", paymentMethod);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return orderId;
    }

    public String createOrderWithPartner(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateOrderWithPartner.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createOrderWithPartnerJIT(String orderId, String orderTime, String epochTime) throws IOException{
        File file = new File("../Data/Payloads/JSON/losCreateOrderWithPartnerJIT.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }
    
    public String createThirdPartyOrder(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateOrderWithThirdParty.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }
    
    public String createLongDistanceOrder(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateLongDistanceOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createSwiggyAssuredOrder(String orderId, String orderTime, String epochTime) throws IOException{
        File file = new File("../Data/Payloads/JSON/losCreateSwiggyAssuredOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createDominosOrder(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateDominosOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }
    
    public String createNewUsersFirstOrder(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateNewUsersFirstOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createPOPOrder(String orderId, String orderTime, String epochTime) throws IOException{
        File file = new File("../Data/Payloads/JSON/losCreatePopOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createOrderWithWrongJson(String orderId, String orderTime, String epochTime) throws IOException, TimeoutException, InterruptedException{
    	rmqHelper.purgeQueue("oms", "ff-new-order-exchange-dead-letters");
    	System.out.println("********** Dead-letter queue is purged **********");
    	Thread.sleep(100);
    	File file = new File("../Data/Payloads/JSON/losFailedOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        Thread.sleep(10000);
        return createOrder;
    }
    
    public String createSwiggyCafeOrder(String orderId, String orderTime, String epochTime,String order_type) throws IOException {
        File file = new File("../Data/Payloads/JSON/loscreateSwiggyCafeOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${order_type}", order_type);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }

    public void deIncoming(String order_type, String order_id, String incoming, String order_time, String epochTime) throws IOException, InterruptedException{
    	if(order_type.equals("with_partner")){
    		createOrderWithPartner(order_id, order_time, epochTime);
    	}else if(order_type.equals("manual") ){
    		createManualOrder(order_id, order_time, epochTime);
    	}else if(order_type.equals("thirdParty") ){
    		createThirdPartyOrder(order_id, order_time, epochTime);
    	}else{
    		System.out.println("############### order_type = "+ order_type +" is not supported ###############");
    	}
    	Thread.sleep(10000);
    	File file = new File("../Data/Payloads/JSON/losDEIncoming.json");
        String deIncoming = FileUtils.readFileToString(file);
        deIncoming = deIncoming.replace("${order_id}", order_id).replace("${incoming}",incoming);
        rmqHelper.pushMessageToExchange("oms", "swiggy.order_collect", new AMQP.BasicProperties().builder().contentType("application/json"), deIncoming);
    }


    public void relayManualOrderForVerification(String orderId, String orderTime, String epochTime) throws IOException{
    	rmqHelper.pushMessageToExchange("oms", "swiggy.ff_order_verification", new AMQP.BasicProperties().builder().contentType("application/json"), createManualOrder(orderId, orderTime, epochTime));
    }
    
    public void relayPartnerOrderForVerification(String orderId, String orderTime, String epochTime) throws IOException{
    	rmqHelper.pushMessageToExchange("oms", "swiggy.ff_order_verification", new AMQP.BasicProperties().builder().contentType("application/json"), createOrderWithPartner(orderId, orderTime, epochTime));
    }
    
    public void relayThirdPartyOrderForVerification(String orderId, String orderTime, String epochTime) throws IOException{
    	rmqHelper.pushMessageToExchange("oms", "swiggy.ff_order_verification", new AMQP.BasicProperties().builder().contentType("application/json"), createThirdPartyOrder(orderId, orderTime, epochTime));
    }
    
    
//  public void rmsUpdate(String is_json = correct/wrong, String order_id, String is_edited = true/false, String status = true/false, String external_order_id = int/null)
    public String rmsUpdate(String is_json, String order_id, String is_edited, String status, String external_order_id, String order_time, String epochTime) throws IOException{
    	createOrderWithPartner(order_id, order_time, epochTime);
    	File file = new File("../Data/Payloads/JSON/losRmsUpdate.json");
    	String rmsUpdate = FileUtils.readFileToString(file);
    	rmsUpdate = rmsUpdate.replace("${order_id}", order_id).replace("${is_edited}", is_edited).replace("${status}", status).replace("${external_order_id}", external_order_id);
    	if(is_json == "correctJson"){
    		rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.rmsUpdatePartnerQueue, new AMQP.BasicProperties().builder().contentType("application/json"), rmsUpdate);
    	}else{
    		rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.rmsUpdatePartnerQueue, new AMQP.BasicProperties().builder().contentType("application/json"), rmsUpdate+"This is wrong Json");
    	}

    	return rmsUpdate;
    }

    public String statusUpdate(String orderId, String orderTime, String status) throws IOException, InterruptedException {
        //verifyOrder(orderId);
        Thread.sleep(100);
        File file = new File("../Data/Payloads/JSON/losOrderStatusUpdate.json");
        String statusUpdate = FileUtils.readFileToString(file);
        statusUpdate = statusUpdate.replace("${order_id}", orderId).replace("${batch_id}",orderId.substring(8)).replace("${time}", orderTime).replace("${status}", status);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.statusUpdateQueue, new AMQP.BasicProperties().builder().contentType("application/json"), statusUpdate);
        return statusUpdate;
    }

    public void statusUpdateWrongJson() throws IOException, InterruptedException {
        //verifyOrder(orderId);
        Thread.sleep(100);
        File file = new File("../Data/Payloads/JSON/losOrderStatusUpdate.json");
        String statusUpdate = FileUtils.readFileToString(file);
        rmqHelper.pushMessageToExchange("oms", "swiggy.order_status", new AMQP.BasicProperties().builder().contentType("application/json"), statusUpdate);
    }

    public void deliveryStatusUpdate(String orderId, String orderTime, String restauant_bill, String de_spending, String de_incoming) throws IOException, InterruptedException {
        //verifyOrder(orderId);
        Thread.sleep(100);
        File file = new File("../Data/Payloads/JSON/losdeliveryupdatejson");
        String statusUpdate = FileUtils.readFileToString(file);
        statusUpdate = statusUpdate.replace(LosConstants.order_id, orderId).replace(LosConstants.batch_id,orderId.substring(8)).replace(LosConstants.time, orderTime).replace(LosConstants.restaurant_bill, restauant_bill).replace(LosConstants.de_spending,de_spending).replace(LosConstants.de_incoming,de_incoming);
        rmqHelper.pushMessageToExchange("oms", "swiggy.order_status", new AMQP.BasicProperties().builder().contentType("application/json"), statusUpdate);
    }

    public void cancelOrder(String orderId, String food_prepared) {
        try {
            File file = new File("../Data/Payloads/JSON/losCancelOrder");
            String statusUpdate = FileUtils.readFileToString(file);
            statusUpdate = statusUpdate.replace(LosConstants.order_id, orderId).replace(LosConstants.food_prepared, food_prepared);
            rmqHelper.pushMessageToExchange("oms", "swiggy.order_cancel", new AMQP.BasicProperties().builder().contentType("application/json"), statusUpdate);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public String createMouOrder(String orderId, String orderTime, String epochTime, String restaurant_id) throws IOException {
        File file = new File("../Data/Payloads/JSON/mou_order");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${restaurant_id}",restaurant_id);
        rmqHelper.pushMessageToExchange("oms", "swiggy.orders", new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }
    
    public void verifyOrder(String order_id){
        ArrayList<String> sessiondata = new ArrayList<String>();
        sessiondata = omsHelper.getOmsSession();
        String csrf_token = sessiondata.get(0);
        String cookie = sessiondata.get(1);
        HashMap<String, String> requestheaders_verifyOrder = new HashMap<String, String>();
        requestheaders_verifyOrder.put("Content-Type", "application/json");
        requestheaders_verifyOrder.put("X-CSRFToken", csrf_token);
//        requestheaders_verifyOrder.put("Cookie", cookie);
        String[] payload = new String[]{order_id};
        GameOfThronesService verifyOrder = new GameOfThronesService("oms","verifyorder", gameofthrones);
        Processor verifyorder_response = new Processor(verifyOrder, requestheaders_verifyOrder, payload);
        
    }
    
    public String createOrderForVerificationService(String orderId, String orderTime, String epochTime, String customerId, String isLongDistance, String isFirstOrderDelivered, String isPartnerEnabled, String typeOfPartner, String restaurantCustomrDistance, String isReplicated, String orderIncoming) throws IOException {
        File file = new File("../Data/Payloads/JSON/losCreateOrderForVerificationService.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${isLongDistance}", isLongDistance).replace("${customerId}", customerId).replace("${isFirstOrderDelivered}", isFirstOrderDelivered).replace("${isPartnerEnabled}", isPartnerEnabled).replace("${typeOfPartner}", typeOfPartner).replace("${restaurantCustomerDistance}", restaurantCustomrDistance).replace("${isReplicated}", isReplicated).replace("${orderIncoming}", orderIncoming);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }
    
    public String getAnOrder(String orderType) throws IOException, InterruptedException {
        Thread.sleep(1000);
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        String order_time = dateFormat.format(date);
        System.out.println("Order time = " + order_time);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        System.out.println("Epoch time = " + epochTime);
        String item_id = VerificationServiceConstants.item_id;


        
        if(orderType.equals("manual")){
        	createManualOrder(order_id, order_time, epochTime);
        	return order_id;
        }else if (orderType.equals("partner")) {
        	createOrderWithPartner(order_id, order_time, epochTime);
        	return order_id;
		}else if (orderType.equalsIgnoreCase("thirdparty")) {
			createThirdPartyOrder(order_id, order_time, epochTime);
			return order_id;
		}else if (orderType.contains("longdistance")) {
			createLongDistanceOrder(order_id, order_time, epochTime);
			return order_id;
		}else if (orderType.contains("newuser")) {
			createNewUsersFirstOrder(order_id, order_time, epochTime);
			return order_id;
		}else if (orderType.contains("pop")) {
			createPOPOrder(order_id, order_time, epochTime);
			return order_id;
		}else if (orderType.contains("JIT")) {
            createOrderWithPartnerJIT(order_id, order_time, epochTime);
			return order_id;
        }else if (orderType.contains("swiggyassured")) {
            createSwiggyAssuredOrder(order_id, order_time, epochTime);
            return order_id;
		}else if (orderType.contains("dominos")) {
			createDominosOrder(order_id, order_time, epochTime);
			return order_id;
		} else if (orderType.contains("invalid")){
			return order_id+"1";
		}else if(orderType.contains("cafe")){
            createSwiggyCafeOrder(order_id, order_time, epochTime, orderType);
            return order_id;
        }else if(orderType.equals("manual2")){
            createManualOrder(order_id, order_time, epochTime,item_id);
            return order_id;
        }else if(orderType.equals("checkoutOrder")){
            createCheckoutOrder(order_time, epochTime);
            return order_id;
        }else {
            return null;
        }
    }

    public String getAnOrder(String restId, String itemId, String customerId,String bill) throws IOException {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        String order_time = dateFormat.format(date);
        System.out.println("Order time = " + order_time);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        System.out.println("Epoch time = " + epochTime);
        createManualOrder(order_id, order_time, epochTime,restId,itemId,customerId,bill);
        return order_id;

    }

    public void partnerFirstAcceptance(){

    }

    /**
     * Validate Orders Present in DB
     * @param orderID
     * @param delayToCheck
     * @return
     */
    public boolean validateOrderExistInOMSOrderTable(String orderID, int delayToCheck) {
        boolean validateStatus = false;
        try {
            for (int i = 0; i < delayToCheck; i++) {
                List orders = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select * from oms_order where order_id = "+ orderID  +";");
                if(orders.size() > 0){
                    return true;
                }else{
                    Thread.sleep(5000L);
                }
                System.out.println("waiting for Orders  in oms_order table "+ i);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return validateStatus;
    }


    /**
     * Validate Orders Status in DB
     * @param orderID
     * @param delayToCheck
     * @return
     */
    public boolean validateOrderStatusInOMSDB(String orderID, String status, int delayToCheck) {
        boolean validateStatus = false;
        String orderStatus = null;
        try {
            for (int i = 0; i < delayToCheck; i++) {
                List<Map<String, Object>> orders = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select delivery_status from oms_orderstatus where id in (select status_id from oms_order where order_id = " + orderID + ");");
                if(orders.size() > 0){
                    orderStatus = orders.get(0).get("delivery_status").toString();
                    if(orderStatus.equals(status))
                        return true;
                    else
                        Thread.sleep(5000);
                }else{
                    Thread.sleep(5000);
                }
                System.out.println("Current Order Status is  "+ i + "   : " + orderStatus);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return validateStatus;
    }

    public String createManualOrder_verify(String orderId, String orderTime, String epochTime) throws IOException {
        File file = new File("../Data/Payloads/JSON/losCreateManualOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.verification_queue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }
    
    public String createOrderWithPartner_verify(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateOrderWithPartner.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.verification_queue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createOrderWithPartnerJIT_verify(String orderId, String orderTime, String epochTime) throws IOException{
        File file = new File("../Data/Payloads/JSON/losCreateOrderWithPartnerJIT.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.verification_queue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }
    
    public String createThirdPartyOrder_verify(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateOrderWithThirdParty.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.verification_queue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }
    
    public String createLongDistanceOrder_verify(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateLongDistanceOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.verification_queue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createSwiggyAssuredOrder_verify(String orderId, String orderTime, String epochTime) throws IOException{
        File file = new File("../Data/Payloads/JSON/losCreateSwiggyAssuredOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.verification_queue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createDominosOrder_verify(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateDominosOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.verification_queue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }
    
    public String createNewUsersFirstOrder_verify(String orderId, String orderTime, String epochTime) throws IOException{
    	File file = new File("../Data/Payloads/JSON/losCreateNewUsersFirstOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createPOPOrder_verify(String orderId, String orderTime, String epochTime) throws IOException{
        File file = new File("../Data/Payloads/JSON/losCreatePopOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.verification_queue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        return createOrder;
    }

    public String createOrderWithWrongJson_verify(String orderId, String orderTime, String epochTime) throws IOException, TimeoutException, InterruptedException{
    	rmqHelper.purgeQueue("oms", "ff-new-order-exchange-dead-letters");
    	System.out.println("********** Dead-letter queue is purged **********");
    	Thread.sleep(100);
    	File file = new File("../Data/Payloads/JSON/losFailedOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.verification_queue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        Thread.sleep(10000);
        return createOrder;
    }
    

    
    
    
    public String getAnOrder_force_verify(String orderType) throws IOException, InterruptedException {
        Thread.sleep(3000);
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        String order_time = dateFormat.format(date);
        System.out.println("Order time = " + order_time);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        System.out.println("Epoch time = " + epochTime);
        
        switch (orderType){
        case "manual":
            createManualOrder_verify(order_id, order_time, epochTime);
            return order_id;
        case "partner":
            createOrderWithPartner_verify(order_id, order_time, epochTime);
            return order_id;
        case "thirdparty":
            createThirdPartyOrder_verify(order_id, order_time, epochTime);
            return order_id;
        case "newuser":
            createNewUsersFirstOrder_verify(order_id, order_time, epochTime);
            return order_id;
        case "longdistance":
            createLongDistanceOrder_verify(order_id, order_time, epochTime);
            return order_id;
        case "pop":
            createPOPOrder_verify(order_id, order_time, epochTime);
            return order_id;
        case "JIT":
            createOrderWithPartnerJIT_verify(order_id, order_time, epochTime);
            return order_id;
        case "swiggyassured":
            createSwiggyAssuredOrder_verify(order_id, order_time, epochTime);
            return order_id;
        case "dominos":
            createDominosOrder_verify(order_id, order_time, epochTime);
            return order_id;
        case "invalid":
            return order_id+"1";
        default :
            return null;

    }
    }
    
    
    
    
    
    
    
    
    
}
