package com.swiggy.api.erp.ff.constants.createTaskAPI;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MapConstants {

    public static final Map<RequestReason, AgentRole> requestReasonRoleMap;
    public static final Map<RequestSubReason, AgentRole> requestSubReasonRoleMap;
    public static final Map<RequestReason, TaskType> requestTaskTypeMap;

    static {
        Map<RequestReason, AgentRole> requestReasonAgentRoleHashMap = new HashMap<>();
        populateRequestReasonRoleMap(requestReasonAgentRoleHashMap);
        requestReasonRoleMap = Collections.unmodifiableMap(requestReasonAgentRoleHashMap);

        Map<RequestSubReason, AgentRole> requestSubReasonAgentRoleHashMap = new HashMap<>();
        populateRequestSubReasonRoleMap(requestSubReasonAgentRoleHashMap);
        requestSubReasonRoleMap = Collections.unmodifiableMap(requestSubReasonAgentRoleHashMap);

        Map<RequestReason, TaskType> requestReasonTaskTypeHashMap = new HashMap<>();
        populateRequestTaskTypeMap(requestReasonTaskTypeHashMap);
        requestTaskTypeMap = Collections.unmodifiableMap(requestReasonTaskTypeHashMap);
    }

    private MapConstants() {
    }

    private static void populateRequestTaskTypeMap(Map<RequestReason, TaskType> requestReasonTaskTypeHashMap) {
        requestReasonTaskTypeHashMap.put(RequestReason.MANUAL_PLACING_ORDER, TaskType.PLACING);
        requestReasonTaskTypeHashMap.put(RequestReason.PLACING_DELAY, TaskType.PLACING);
        requestReasonTaskTypeHashMap.put(RequestReason.CANCELLATION_REQUIRED, TaskType.PLACING);
        requestReasonTaskTypeHashMap.put(RequestReason.PARTNER_CALL_REQUEST, TaskType.PLACING);
        requestReasonTaskTypeHashMap.put(RequestReason.ORDER_DECLINED_BY_RESTAURANT, TaskType.PLACING);
        requestReasonTaskTypeHashMap.put(RequestReason.VENDOR_UNABLE_TO_ACCEPT_ORDER, TaskType.PLACING);
        requestReasonTaskTypeHashMap.put(RequestReason.PLACING_STATE_MISMATCH, TaskType.PLACING);
        requestReasonTaskTypeHashMap.put(RequestReason.VENDOR_UNAVAILABLE, TaskType.PLACING);
    }

    private static void populateRequestReasonRoleMap(Map<RequestReason, AgentRole> requestReasonAgentRoleHashMap) {
        requestReasonAgentRoleHashMap.put(RequestReason.MANUAL_PLACING_ORDER, AgentRole.L1_PLACER);
        requestReasonAgentRoleHashMap.put(RequestReason.PLACING_DELAY, AgentRole.L1_PLACER);
        requestReasonAgentRoleHashMap.put(RequestReason.CANCELLATION_REQUIRED, AgentRole.L2_PLACER);
        requestReasonAgentRoleHashMap.put(RequestReason.PARTNER_CALL_REQUEST, AgentRole.L2_PLACER);
        requestReasonAgentRoleHashMap.put(RequestReason.VENDOR_UNABLE_TO_ACCEPT_ORDER, AgentRole.L1_PLACER);
        requestReasonAgentRoleHashMap.put(RequestReason.PLACING_STATE_MISMATCH, AgentRole.L2_PLACER);
        requestReasonAgentRoleHashMap.put(RequestReason.VENDOR_UNAVAILABLE, AgentRole.L1_PLACER);
        requestReasonAgentRoleHashMap.put(RequestReason.ORDER_DECLINED_BY_RESTAURANT, AgentRole.L2_PLACER);
    }

    private static void populateRequestSubReasonRoleMap(Map<RequestSubReason, AgentRole> requestSubReasonAgentRoleHashMap) {
        requestSubReasonAgentRoleHashMap.put(RequestSubReason.REMINDER_ATTEMPTS_UNSUCCESSFUL, AgentRole.L1_PLACER);
        requestSubReasonAgentRoleHashMap.put(RequestSubReason.UNABLE_TO_ATTEMPT_REMINDERS, AgentRole.L1_PLACER);
        requestSubReasonAgentRoleHashMap.put(RequestSubReason.ITEM_NOT_AVAILABLE, AgentRole.L2_PLACER);
        requestSubReasonAgentRoleHashMap.put(RequestSubReason.RESTAURANT_POTENTIALLY_CLOSED, AgentRole.L2_PLACER);
        requestSubReasonAgentRoleHashMap.put(RequestSubReason.RESTAURANT_CLOSED, AgentRole.L2_PLACER);
    }
}

