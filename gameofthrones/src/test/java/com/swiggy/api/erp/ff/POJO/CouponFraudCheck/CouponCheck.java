package com.swiggy.api.erp.ff.POJO.CouponFraudCheck;

public class CouponCheck {



        private Customer_details customer_details;

        private Coupon_details coupon_details;


        public CouponCheck(Customer_details customer_details,Coupon_details coupon_details )
        {
            this.coupon_details = coupon_details;
            this.customer_details = customer_details;
        }


    public CouponCheck(Customer_details customer_details)
    {
        this.customer_details = customer_details;
    }



    public Customer_details getCustomer_details ()
        {
            return customer_details;
        }

        public void setCustomer_details (Customer_details customer_details)
        {
            this.customer_details = customer_details;
        }

        public Coupon_details getCoupon_details ()
        {
            return coupon_details;
        }

        public void setCoupon_details (Coupon_details coupon_details)
        {
            this.coupon_details = coupon_details;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [customer_details = "+customer_details+", coupon_details = "+coupon_details+"]";
        }
    }

