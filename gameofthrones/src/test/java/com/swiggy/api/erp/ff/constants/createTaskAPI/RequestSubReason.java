package com.swiggy.api.erp.ff.constants.createTaskAPI;

import java.util.Arrays;
import java.util.Optional;

public enum RequestSubReason {

    REMINDER_ATTEMPTS_UNSUCCESSFUL("Reminder attempts unsuccessful", 1, RequestReason.PLACING_DELAY),
    UNABLE_TO_ATTEMPT_REMINDERS("Unable to attempt reminders", 2, RequestReason.PLACING_DELAY),
    RESTAURANT_POTENTIALLY_CLOSED("Restaurant potentially closed", 3, RequestReason.PLACING_DELAY),
    RESTAURANT_CLOSED("Restaurant Closed", 4, RequestReason.CANCELLATION_REQUIRED),
    ITEM_NOT_AVAILABLE("Item not available", 5, RequestReason.VENDOR_UNABLE_TO_ACCEPT_ORDER);

    private final String description;
    private final int id;
    private final RequestReason reason;

    RequestSubReason(String description, int id, RequestReason reason) {
        this.description = description;
        this.id = id;
        this.reason = reason;
    }

    public static Optional<RequestSubReason> findById(int id) {

        return Arrays.stream(RequestSubReason.values())
                .filter(x -> x.getId() == id)
                .findFirst();
    }

    public static Optional<RequestSubReason> getEnum(String subReason) {
        for (RequestSubReason requestSubReason : RequestSubReason.values()) {
            if (requestSubReason.name().equals(subReason)) {
                return Optional.of(requestSubReason);
            }
        }
        return Optional.empty();
    }

    public String toString() {
        return this.description;
    }

    public int getId() {
        return this.id;
    }

    public RequestReason getReason() {
        return reason;
    }
}
