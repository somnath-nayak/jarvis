
package com.swiggy.api.erp.cms.pojo.Inventory;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder({
    "inventories",
    "attributes"
})
public class Inventory_construct {

    @JsonProperty("inventories")
    private Inventories inventories;
    @JsonProperty("attributes")
    private Attributes attributes;

    @JsonProperty("inventories")
    public Inventories getInventories() {
        return inventories;
    }

    @JsonProperty("inventories")
    public void setInventories(Inventories inventories) {
        this.inventories = inventories;
    }

    public Inventory_construct withInventories(Inventories inventories) {
        this.inventories = inventories;
        return this;
    }

    @JsonProperty("attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public Inventory_construct withAttributes(Attributes attributes) {
        this.attributes = attributes;
        return this;
    }

    public Inventory_construct setValue(Inventories inv,Attributes ab){
        this.withInventories(inv).withAttributes(ab);
        return this;
    }


}
