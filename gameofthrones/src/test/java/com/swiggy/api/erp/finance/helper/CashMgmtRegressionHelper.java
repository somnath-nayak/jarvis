package com.swiggy.api.erp.finance.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.finance.constants.CashMgmtConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.io.*;
import java.util.*;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.helper
 **/
public class CashMgmtRegressionHelper {

    CashMgmtHelper cashMgmtHelper = new CashMgmtHelper();
    static Initialize gameofthrones = new Initialize();
    DeliveryDataHelper deliverydatahelper = new DeliveryDataHelper();
    DateHelper dateHelper = new DateHelper();
    ZoneHelper zoneHelper = new ZoneHelper();
    String g_deID, g_zoneID;


    public HashMap<String, String> getDefaultHeader() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        //headers.put("Authorization","Basic R0dZU1dJOjIwMTVTVyFHR1k=");
        return headers;
    }

    public Processor getZoneLevelFloatingCashHelper(String zoneid) {
        String[] params = {zoneid};
        GameOfThronesService gots = new GameOfThronesService("financecashmanagement", "zone_level_fc_api", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(),null,params);
        return processor;
    }

    public Processor getFloatingCashDetailsHelper(String deID) {
        String[] params = {deID};
        GameOfThronesService gots = new GameOfThronesService("cashmanagement", "cash_detail_api", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), null, params);
        return processor;
    }

    public Processor getZoneCashOverviewHelper(String zoneID) {
        String[] params = {zoneID};
        GameOfThronesService gots = new GameOfThronesService("financecashmanagement", "zone_cash_overview_api", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), null, params);
        return processor;
    }

    public Processor getZoneCashLogHelper(String zoneID) {
        String[] params = {zoneID};
        GameOfThronesService gots = new GameOfThronesService("financecashmanagement", "zone_cash_log", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), null, params);
        return processor;
    }

    public Processor addHubTransactionHelper(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("financecashmanagement", "add_hub_transaction", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public Processor getCityHubReportHelper(String date, String cityID) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "text/csv");
        headers.put("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
        String[] params = {date, cityID};
        GameOfThronesService gots = new GameOfThronesService("cashmanagement", "city_hub_report_api", gameofthrones);
        Processor processor = new Processor(gots, headers, null, params);
        return processor;
    }

    public Processor addBulkTransactionHelper(String json) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("cashmanagement", "add_bulk_txn_api_pojo", gameofthrones);
        Processor processor = new Processor(gots, headers, payload);
        return processor;
    }

    public Processor dePayoutAdjustmentHelper(String json) {
        String[] payload = {json};
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
        GameOfThronesService gots = new GameOfThronesService("cashmanagement", "de_payout_adjustment_api", gameofthrones);
        Processor processor = new Processor(gots, headers, payload);
        return processor;
    }

    //returns: LFCL,UFCL
    public String[] getUpperLowerFCLimitFromDB(String zoneID) {
        String[] returnVals = new String[2];
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_LFCL_UFLC + zoneID;
        System.out.println("--Query ::: " + q);
        List<Map<String, Object>> list = sqlTemplate.queryForList(q);
        if (!list.isEmpty()) {
            String ufcl = list.get(0).get("UFCL").toString();
            String lfcl = list.get(0).get("LFCL").toString();
            System.out.println("--UFCL: " + ufcl + "  --LFCL: " + lfcl);
            returnVals[0] = ufcl;
            returnVals[1] = lfcl;
        }
        return returnVals;
    }


    public String createDEHelper(int zoneID) {
        String de_id = deliverydatahelper.CreateDE(zoneID);
        return de_id;
    }

    public int getRandomOrderId() {
            Random random = new Random();
            int x = random.nextInt(999999) + 10000;
            return x;
    }

    public List<Map<String, Object>> getDECashSessionDetailsFromDB(String deID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_de_cash_session_details.replace("${DE_ID}", deID);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public String getInactiveDEId() {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_inactive_de_id;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        String deID = list.get(0).get("id").toString();
        return deID;
    }

    public List<Map<String, Object>> getZoneLevelFCDeIdFromDB(String zoneID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_zone_level_fc_de_id.replace("${ZONEID}", zoneID);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public List<Map<String, Object>> getZoneCashOverviewFromDB(String zoneID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_zone_cash_overview + zoneID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public List<Map<String, Object>> getZoneCashAuditFromDB(String zoneID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_zone_cash_log_from_zoneCashAudit.replace("${ZONEID}", zoneID);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public String getDeForceLogoutStatusFromDB(String deID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_force_logout_status_of_de + deID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        String forceLogoutStatus = list.get(0).get("force_logout").toString();
        System.out.println("--force logout status ::: " + forceLogoutStatus);
        return forceLogoutStatus;
    }

    public List<Map<String, Object>> getDeCashTransactionDetailsFromDB(String deID, String orderID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_de_cash_transactions.replace("${DEID}", deID).replace("${ORDERID}", orderID);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public Processor blockDeHelper(String json) {
        String[] payload = {json};
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
        GameOfThronesService gots = new GameOfThronesService("deliveryservice", "blockdeliveryAgent", gameofthrones);
        Processor processor = new Processor(gots, headers, payload);
        return processor;
    }

    public Processor unblockDeHelper(String json) {
        String[] payload = {json};
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
        GameOfThronesService gots = new GameOfThronesService("deliveryservice", "unblockdeliveryAgent", gameofthrones);
        Processor processor = new Processor(gots, headers, payload);
        return processor;
    }

    public String getZoneFloatingCashLimitFromDB(String zoneID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_floating_cash_limit_zone + zoneID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        String floatingCashLimit = list.get(0).get("floating_cash_limit").toString();
        return floatingCashLimit;
    }

    public String CalculateStartDateForUnpaidOrderCount(String yyyy_mm_dd) {
        String current_day = dateHelper.getDayForAGivenDate(yyyy_mm_dd);
        System.out.println("--Current Day ::: " + current_day);
        int count = 0;
        int current_DOW_numeric = dateHelper.getDayForAGivenDateAsNumeric(yyyy_mm_dd);
        if (current_DOW_numeric >= 1 && current_DOW_numeric <= 5) {
            count = 7 + (current_DOW_numeric - 1);
        } else if (current_DOW_numeric == 6 || current_DOW_numeric == 7) {
            count = current_DOW_numeric - 1;
        }
        String date_time = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss((0 - count));
        String[] datetime = date_time.split("\\s+");
        System.out.println("--Date after calculating ==> " + datetime[0]);
        //System.out.println("--Time ==> "+datetime[1]);
        return datetime[0];
    }

    public void deleteAllCreatedTmpFiles(String path) {
        File file = new File(path);
        if (file.delete()) {
            System.out.println("File deleted successfully ===> " + path);
        } else {
            System.out.println("No_File_To_Delete/Failed_To_Delete the file ===> " + path);
        }
    }

    public void readCSVResponseAndCreateTxt(Processor p, String path) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(path));
            writer.write(p.ResponseValidator.GetBodyAsText());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public HashMap<String, String[]> parseTxtFileToHashMap(String path) throws IOException {
        File file = new File(path);
        List<String> list = new ArrayList<>();
        HashMap<String, String[]> hm = new HashMap<>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String st;
        while ((st = in.readLine()) != null) {
            list.add(st);
            //debug
            System.out.println("--Line ::: " + st);
        }
        System.out.println("--Size of list in parseTxtFileToHashMap ::: " + list.size());
        for (int i = 0; i < list.size(); i++) {
            String[] vals = list.get(i).split(",");
            for (int j = 0; j < vals.length; j++) {
                //debugger
                System.out.println("--Values ::: " + vals[j]);

            }
            hm.put(vals[1], vals);
        }
        // System.out.println(hm.entrySet());
        return hm;
    }

    public String getCityIDFromDB(String zoneID) {
        List<Map<String, Object>> list;
        String cityID = "null";
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_city_id + zoneID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size() > 0)
            cityID = list.get(0).get("city_id").toString();
        return cityID;
    }

    //gives zone name and zone id
    public List<Map<String, Object>> getAllZonesForCityIDFromDB(String cityID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_all_zones_for_city_id + cityID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public List<Map<String, Object>> getCityHubReportFromDB(String zoneID, String date) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_hub_city_report.replace("${ZONEID}", zoneID).replace("${DATE}", date);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    //returns: orderedTime,recievedTime,placedTime,assignedTime,confirmedTime,arrivedTIme,pickedupTime,deliveredTime
    public HashMap<String, String> getTimeStampsForTripsTable(int days) {
        HashMap<String, String> hm = new HashMap<>();
        String getDateTime = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(days);
        String orderedTime, recievedTime, placedTime, assignedTime, confirmedTime, arrivedTime, pickedupTime, deliveredTime;
        orderedTime = dateHelper.getMinutesPlusOrMinusToAGivenDateTime_yyyy_mm_dd_hh_mm_ss(getDateTime, -30);
        recievedTime = dateHelper.getMinutesPlusOrMinusToAGivenDateTime_yyyy_mm_dd_hh_mm_ss(getDateTime, -25);
        placedTime = dateHelper.getMinutesPlusOrMinusToAGivenDateTime_yyyy_mm_dd_hh_mm_ss(getDateTime, -20);
        assignedTime = dateHelper.getMinutesPlusOrMinusToAGivenDateTime_yyyy_mm_dd_hh_mm_ss(getDateTime, -15);
        confirmedTime = dateHelper.getMinutesPlusOrMinusToAGivenDateTime_yyyy_mm_dd_hh_mm_ss(getDateTime, -12);
        arrivedTime = dateHelper.getMinutesPlusOrMinusToAGivenDateTime_yyyy_mm_dd_hh_mm_ss(getDateTime, -10);
        pickedupTime = dateHelper.getMinutesPlusOrMinusToAGivenDateTime_yyyy_mm_dd_hh_mm_ss(getDateTime, -6);
        deliveredTime = dateHelper.getMinutesPlusOrMinusToAGivenDateTime_yyyy_mm_dd_hh_mm_ss(getDateTime, 0);
        hm.put("orderedTime", orderedTime);
        hm.put("receivedTime", recievedTime);
        hm.put("placedTime", placedTime);
        hm.put("assignedTime", assignedTime);
        hm.put("confirmedTime", confirmedTime);
        hm.put("arrivedTime", arrivedTime);
        hm.put("pickedUpTime", pickedupTime);
        hm.put("deliveredTime", deliveredTime);
        return hm;
    }

    public String createManualOrderAndGetOrderId() {
        JsonHelper jsonHelper = new JsonHelper();
        HashMap<String, String> hm = new HashMap<>();
        String json = deliverydatahelper.returnOrderJson(hm);
        String orderID = JsonPath.read(json,"$.order_id").toString().replace("{","").replace("[","").replace("}","").replace("]","").replace("\"","").trim();
        System.out.println("--ORDERID ::: "+orderID);
        boolean val = deliverydatahelper.pushOrderJsonToRMQAndValidate(json);
        if(val)
            return orderID;
        else
            return "FAILED";

    }

    public void updateTimestampsAndDeIdInDeliveryTripsTable(HashMap<String,String> hm, String orderID, String deID) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.deliveryDB);
        String q = CashMgmtConstant.update_timestamps_in_trips_table
                .replace("${ORDEREDTIME}",hm.get("orderedTime"))
                .replace("${RECEIVEDTIME}",hm.get("receivedTime"))
                .replace("${PLACEDTIME}",hm.get("placedTime"))
                .replace("${ASSIGNEDTIME}",hm.get("assignedTime"))
                .replace("${CONFIRMEDTIME}",hm.get("confirmedTime"))
                .replace("${ARRIVEDTIME}",hm.get("arrivedTime"))
                .replace("${PICKEDUPTIME}",hm.get("pickedUpTime"))
                .replace("${DELIVEREDTIME}",hm.get("deliveredTime"))
                .replace("${DEID}",deID)
                .replace("${ORDERID}",orderID);
        System.out.println("--Query ::: " + q);
        sqlTemplate.update(q);
    }

    public Processor getDailyOrderDetailsHelper(String json) {
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("deliveryservice", "dailyorderdetails", gameofthrones);
        Processor processor = new Processor(gots, getDefaultHeader(), payload);
        return processor;
    }

    public String getPayPerOrderFromDB(String zoneID) {
        List<Map<String, Object>> list;
        String cityID = getCityIDFromDB(zoneID);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CashMgmtConstant.cashDB);
        String q = CashMgmtConstant.get_pay_per_order + cityID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        String ppo = list.get(0).get("payout_per_order").toString();
        return ppo;
    }
}
