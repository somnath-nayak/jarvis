package com.swiggy.api.erp.cms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;


public class TimeSlotsHelper {
    Initialize gameofthrones = new Initialize();

    /*===============================================================TimeSlots==================================================================================*/

    public Processor postTimeSlots(String restId, String openTime, String closeTime, String day) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "addtimeslots", gameofthrones);
        String[] payloadparams = {restId,openTime,closeTime,day};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor getTimeSlots(String restId) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "gettimeslots", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor delTimeSlots(String txId) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "deltimeslots", gameofthrones);
        String[] urlParams={txId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor putTimeSlots(String restId, String openTime, String closeTime, String day, String TxId) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "updatetimeslots", gameofthrones);
        String[] payloadparams = {restId,openTime,closeTime,day};
        String[] urlParams={TxId};
        Processor processor = new Processor(service, requestheaders, payloadparams, urlParams);
        return processor;
    }
    /*===============================================================Holiday TimeSlots==================================================================================*/
    public Processor postHolidayTimeSlots(String payload) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "addholidayslots", gameofthrones);
        String[] payloadparams = {payload};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor getHolidayTimeSlots(String restId) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "getholidayslots", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor delHolidayTimeSlots(String txId) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "delholidayslots", gameofthrones);
        String[] urlParams={txId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor putHolidayTimeSlots(String payload, String TxId) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "updateholidayslots", gameofthrones);
        String[] payloadparams = {payload};
        String[] urlParams={TxId};
        Processor processor = new Processor(service, requestheaders, payloadparams, urlParams);
        return processor;
    }

    /*================================================================item-slot=================================================================================*/

    public Processor postItemSlots(String item_id, String dayOfWeek,String open_time, String close_time)  {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "additemtimeslots", gameofthrones);
        String[] payloadparams = {item_id,dayOfWeek,open_time, close_time};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor getItemSlots(String itemId) {
        HashMap<String, String> requestheaders =new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "getitemtimeslots", gameofthrones);
        String[] urlParams={itemId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor delItemSlots(String txId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "delitemtimeslots", gameofthrones);
        String[] urlParams={txId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor putItemSlots(String id,String item_id, String dayOfWeek,String open_time, String close_time) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "updateitemtimeslots", gameofthrones);
        String[] payloadparams = {id,item_id, dayOfWeek,open_time, close_time};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    /*================================================================item-Holiday-slot=================================================================================*/
    public Processor postItemHolidaySlots(String itemId, String fromTime, String toTime) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "additemholidayslots", gameofthrones);
        String[] payloadparams = {itemId, fromTime,toTime};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor getItemHolidaySlots(String restId) {
        HashMap<String, String> requestheaders =new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "getitemholidayslots", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor delItemHolidaySlots(String txId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "delitemholidayslots", gameofthrones);
        String[] urlParams={txId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor putItemHolidaySlots(String TxId, String itemId, String fromTime, String toTime) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "updateitemholidayslots", gameofthrones);
        String[] payloadparams = {TxId,itemId,fromTime,toTime};
        String[] urlParams={TxId};
        Processor processor = new Processor(service, requestheaders, payloadparams, urlParams);
        return processor;
    }


    public HashMap<String, String> requestHeaders()
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("user_meta","{\"source\": \"CMS\", \"user\":\"cms-tester\"}");
        return requestheaders;
    }








}
