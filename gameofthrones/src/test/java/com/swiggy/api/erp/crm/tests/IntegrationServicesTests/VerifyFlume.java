package com.swiggy.api.erp.crm.tests.trackingScreen;


import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.delivery.helper.OrderStatus;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.time.Instant;

import java.util.*;
import java.util.concurrent.TimeoutException;

public class VerifyFlume extends MockServiceDP {

    Initialize gameofthrones = new Initialize();
    LOSHelper losHelper = new LOSHelper();
    RedisHelper redisHelper = new RedisHelper();

    public static Map<String, String> convertResponseToMap(String response){
        Map<String, String> map = new HashMap<String, String>();
        for(String keyValue : response.split(",")){
            String[] pairs = keyValue.split(":");
            System.out.println("keyValue "+keyValue);
            map.put(pairs[0], pairs.length == 1 ? "" :pairs[1]);
        }
        return map;
    }

    @Test(dataProvider = "fetchTrackingScreenData", groups = {"regression"}, description = "End to end tests for cancellation flow")
    public void testVerifyFlume(HashMap<String, String>  flowDetails) throws IOException, InterruptedException, TimeoutException {
        System.out.println("flowDetails "+flowDetails);
        String order = flowDetails.get("orderType").toString();
        String orderStatus = flowDetails.get("deliveryStatus").toString();
        String orderId = null;
        Date orderCreationDate = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String, String> map = new HashMap<String, String>();
        orderId = losHelper.getAnOrder(order);
        System.out.println("Order Id "+orderId);

        if(orderId != null) {

            Date orderUpdationDate = new Date();
            losHelper.statusUpdate(orderId, dateFormat.format(orderUpdationDate), orderStatus);
            //deleting redis entry for order and oe
            // omshelper.redisFlushAll(OMSConstants.REDIS);
            String sortedListOrder = redisHelper.Zrange(CRMConstants.redisTrackingScreen, 0, "order.state.checkout." + orderId, 0, 1000).toString();
            sortedListOrder.replaceAll("\\\\", "");
            System.out.println(sortedListOrder);
            Assert.assertEquals(sortedListOrder.contains("status" + orderStatus), true);

            String OrderDetails = redisHelper.getValue(CRMConstants.redisTrackingScreen, 0, "order.details." + orderId).toString();
            System.out.println(OrderDetails);
            OrderDetails = OrderDetails.replace("\"","");
            System.out.println(OrderDetails);
            map = convertResponseToMap(OrderDetails);
            System.out.println("map "+map);
            String actualOrderId = map.get("order_id");
            System.out.println("orderId "+orderId);
            System.out.println("actualOrderId "+actualOrderId);
            Assert.assertEquals(orderId, actualOrderId, "Actual orderid doesn't match with expected");
            }
        }

}
