package com.swiggy.api.erp.delivery.constants;


public interface DSPrediskeyConstant {
	String redis_box_key="dsredis";
	String key_prep_time_avg_hour="prep_time_avg_hour#";
	String key_rest_place_order="rest_place_orders#";
	String key_rest_delivered_orders="rest_delivered_orders#";
	String key_rest_active_orders="rest_active_orders#";
	String key_rest_de_ratings="rest_de_ratings#";
	String key_prep_time_avg_7_days="prep_time_avg_7_days#";
	String key_prep_time_avg_21_days="prep_time_avg_21_days#";
	String key_rest_wait_time_avg_hour="rest_wait_time_avg_hour#";
	String key_zone_undelivered_orders="zone_undelivered_orders#";
	String set_prep_time_avg_hour_more="{\"value\":80.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_prep_time_avg_hour_less="{\"value\":30.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_place_order_more="{\"value\":80.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_place_order_less="{\"value\":30.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_delivered_orders_more="{\"value\":80.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_delivered_orders_less="{\"value\":30.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_active_orders_more="{\"value\":80.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_active_orders_less="{\"value\":30.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_de_ratings_more="{\"value\":80.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_de_ratings_less="{\"value\":30.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_prep_time_avg_7_days_more="{\"value\":80.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_prep_time_avg_7_days_less="{\"value\":30.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_prep_time_avg_21_days_more="{\"value\":80.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_prep_time_avg_21_days_less="{\"value\":30.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_wait_time_avg_hour_more="{\"value\":80.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_rest_wait_time_avg_hour_less="{\"value\":30.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_zone_undelivered_orders_more="{\"value\":80.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String set_zone_undelivered_orders_less="{\"value\":30.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";



}



		
		

	
