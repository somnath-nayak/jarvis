package com.swiggy.api.erp.vms.td.pojo;

/***
 * 
 * @author ramzi
 *
 */
public class RuleDiscount
{
    private int percentDiscountAbsoluteCap;

    private int percentDiscount;

    private int minCartAmount;

    private String discountLevel;

    private String type;
    
    private int flatDiscountAmount;
    
    private int itemId;    
    

	public int getFlatDiscountAmount() {
		return flatDiscountAmount;
	}

	public void setFlatDiscountAmount(int flatDiscountAmount) {
		this.flatDiscountAmount = flatDiscountAmount;
	}

	public int getPercentDiscountAbsoluteCap ()
    {
        return percentDiscountAbsoluteCap;
    }

    public void setPercentDiscountAbsoluteCap (int percentDiscountAbsoluteCap)
    {
        this.percentDiscountAbsoluteCap = percentDiscountAbsoluteCap;
    }

    public int getPercentDiscount ()
    {
        return percentDiscount;
    }

    public void setPercentDiscount (int percentDiscount)
    {
        this.percentDiscount = percentDiscount;
    }

    public int getMinCartAmount ()
    {
        return minCartAmount;
    }

    public void setMinCartAmount (int minCartAmount)
    {
        this.minCartAmount = minCartAmount;
    }

    public String getDiscountLevel ()
    {
        return discountLevel;
    }

    public void setDiscountLevel (String discountLevel)
    {
        this.discountLevel = discountLevel;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
/*    public int getItemid() {
		return itemId;
	}

	public void setItemid(int itemId) {
		this.itemId = itemId;
	}*/
	
    @Override
    public String toString()
    {
        return "ClassPojo [percentDiscountAbsoluteCap = "+percentDiscountAbsoluteCap+", percentDiscount = "+percentDiscount+", minCartAmount = "+minCartAmount+",flatDiscountAmount="+flatDiscountAmount+",discountLevel = "+discountLevel+", type = "+type+",itemId = "+itemId+"]";
    }
}