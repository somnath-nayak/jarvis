package com.swiggy.api.erp.delivery.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;

import java.util.HashMap;

public class DeliveryServiceAssiatanceHelper {
	
	Initialize gameofthrones =Initializer.getInitializer();
	DeliveryHelperMethods del = new DeliveryHelperMethods();
	DBHelper dbHelper = new DBHelper();
	DeliveryServiceHelper dsh = new DeliveryServiceHelper();
	
	
	
	
	HashMap<String, String> requestheaders ;
	// TODO Auto-generated constructor stub
    public DeliveryServiceAssiatanceHelper(String auth)
    {

        gameofthrones= new Initialize();
        requestheaders= new HashMap<>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Authorization", auth);
    }

    public DeliveryServiceAssiatanceHelper()
    {
        gameofthrones= new Initialize();
    }
	
	

	
	public Processor deAssistance(String auth,String order_id, String action, String reason)
			throws InterruptedException {
		//HashMap<String, String> requestheaders = del.doubleheader();
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "assiatanceHelp", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		String[] payloadparams = new String[]{order_id,action,reason};
				Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}
	
	

}
