package com.swiggy.api.erp.cms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.HashMap;

public class CmsCloudMenuHelper {
	
	 Initialize gameofthrones = new Initialize();
	 
	 public String  getUniqueID()
	 {
		 String id = null;
		 id="CloudMenuID_Automation"+Instant.now().getEpochSecond();		 
		 return id;
	 }
	 
	 public String  getUniqueName()
	 {
		 String name = null;
		 name="CloudMenuName_Automation"+Instant.now().getEpochSecond();		 
		 return name;
	 }
	 
	 
	public HashMap<String, String>  getHeader()
	 {
		String tokenId="1ksue6a7593ks7s2";
		HashMap<String,String> headers = new HashMap<>();
		 headers.put("Content-Type", "application/json");
		 headers.put("tokenid",tokenId);
		 headers.put("Authorization","Basic dXNlcjpjaGVjaw==");
		 return headers;
	 }
	 
	
	
	
	 
	/* Content-Type:application/json
	 tokenid:1ksue6a7593ks7s2
	 Authorization:Basic dXNlcjpjaGVjaw==
	 */
	 
	 @DataProvider(name = "CreateCategory")
	    public Object[][] restTime() {
	        return new Object[][] {
	        	{"KF0002",getUniqueID(),getUniqueName()}
	        };
	    }
	 
	 @Test(dataProvider = "CreateCategory")
	 public void test(String restId, String catId, String catName) {
		 
		 //serviceaneme,API anme,GOT 
		 GameOfThronesService service = new GameOfThronesService("cms_cloud_menu", "createMainCat", gameofthrones);
		 String payload[] = {catId,catName};
		 String urlparam[] = {restId};
		 Processor processor = new Processor(service, getHeader(), payload, urlparam);
		 
	 }

}
