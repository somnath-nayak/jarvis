package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "delete_by_ids",
        "delete_by_third_party_ids",
        "items"
})
public class BulkItems {

    @JsonProperty("delete_by_ids")
    private List<Integer> delete_by_ids = null;
    @JsonProperty("delete_by_third_party_ids")
    private List<String> delete_by_third_party_ids = null;
    @JsonProperty("items")
    private List<Item> items = null;

    @JsonProperty("delete_by_ids")
    public List<Integer> getDelete_by_ids() {
        return delete_by_ids;
    }

    @JsonProperty("delete_by_ids")
    public void setDelete_by_ids(List<Integer> delete_by_ids) {
        this.delete_by_ids = delete_by_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public List<String> getDelete_by_third_party_ids() {
        return delete_by_third_party_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public void setDelete_by_third_party_ids(List<String> delete_by_third_party_ids) {
        this.delete_by_third_party_ids = delete_by_third_party_ids;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("delete_by_ids", delete_by_ids).append("delete_by_third_party_ids", delete_by_third_party_ids).append("items", items).toString();
    }

    public void setDefaultValues() {
        if(getDelete_by_ids() == null)
            this.setDelete_by_ids(new ArrayList<>());
        if(this.getItems() == null)
            this.setItems(new ArrayList<>());
        if(getDelete_by_third_party_ids() == null)
            this.setDelete_by_third_party_ids(new ArrayList<>());
    }

    public BulkItems build() {
        setDefaultValues();
        return this;
    }

}
