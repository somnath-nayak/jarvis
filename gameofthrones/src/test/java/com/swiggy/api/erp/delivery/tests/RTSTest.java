package com.swiggy.api.erp.delivery.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.dp.RTSDataProvider;
import com.swiggy.api.erp.delivery.helper.CartSLA_Helper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.RTSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.Processor;

public class RTSTest extends RTSDataProvider {

	Initialize init = Initializer.getInitializer();
	CheckoutHelper checkhelp = new CheckoutHelper();
	RTSHelper rtshelp = new RTSHelper();
	static DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	DeliveryServiceHelper delhelp=new DeliveryServiceHelper();
    DeliveryDataHelper deldatahelp=new DeliveryDataHelper();
    CartSLA_Helper serhelp = new CartSLA_Helper();
	static String auth1;
    int address_id,long_distance_address_id;
	static Object de_id1=90449,de_id2=90450;
	
	@BeforeClass
	public void setup() throws InterruptedException
	{
		delmeth.dbhelperupdate("update area set last_mile_cap=4 where id=3");
		delmeth.dbhelperupdate("update zone set max_delivery_time=100 where id=3");
		delmeth.dbhelperupdate(RTSConstants.enable_rts);
		delmeth.dbhelperupdate(RTSConstants.Disable_rts_shadow_mode);
		delmeth.dbhelperupdate("update delivery_boys set enabled=0");
		delmeth.dbhelperupdate(RTSConstants.set_rts_zone);
		delmeth.dbhelperupdate(RTSConstants.set_rts_area);
		delmeth.dbhelperupdate(RTSConstants.rts_start_banerfactor);
		delmeth.dbhelperupdate(RTSConstants.rts_stop_banerfactor);
		delmeth.dbhelperupdate(RTSConstants.rts_stale_order_more);
		delmeth.dbhelperupdate(RTSConstants.set_only_last_mile_DE);
		delmeth.redisconnectsetdoubleStrinvalue(RTSConstants.get_bf, RTSConstants.rts_area_id, 1, RTSConstants.rts_bf_value, RTSConstants.delivery_redis);
		address_id=rtshelp.checkaddressexistence(RTSConstants.address_id,RTSConstants.cust_address_id_lat,RTSConstants.cust_address_id_lng, DeliveryConstant.mobile, DeliveryConstant.password);
		long_distance_address_id=rtshelp.checkaddressexistence(RTSConstants.long_distance_address_id, RTSConstants.long_distance_lat, RTSConstants.long_distance_lng, DeliveryConstant.mobile, DeliveryConstant.password);
		//rtshelp.checklongdistancepolygonexistence(RTSConstants.long_distance_polygon_id_query, RTSConstants.long_distance_polygon_id);
		String query1=RTSConstants.Zone_Config_max_first_mile_query+RTSConstants.zone_max_first_mile_more+" where zone_id="+RTSConstants.single_assignedZones;
		delmeth.dbhelperupdate(query1);
		String query2=RTSConstants.Zone_Config_max_sla_query+RTSConstants.zone_max_SLA_More+" where zone_id="+RTSConstants.single_assignedZones;
		delmeth.dbhelperupdate(query2);
		//de_id1=deldatahelp.CreateDE(RTSConstants.single_assignedZones_int);
		//de_id2=deldatahelp.CreateDE(RTSConstants.single_assignedZones_int);
		if(!(null==de_id1))
		{
			 delhelp.markDEActive(de_id1.toString());
			rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,
				RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,
				RTSConstants.de_enabled, RTSConstants.cityid,true);
			deldatahelp.addDEzonemap(de_id1.toString(), RTSConstants.single_assignedZones_int);
				delhelp.makeDEFree(de_id1.toString());
			 delhelp.makeDEActive(de_id1.toString());
			
		}
		if(!(null==de_id2))
		{
			delhelp.markDEActive(de_id2.toString());
			rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,
					RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,
					RTSConstants.de_enabled, RTSConstants.cityid, true);
			deldatahelp.addDEzonemap(de_id2.toString(), RTSConstants.single_assignedZones_int);
			delhelp.makeDEFree(de_id2.toString());
			
			}
		
		delhelp.makeDEFree(de_id1.toString());
		delhelp.makeDEFree(de_id2.toString());
	}
	//@Test(dataProvider = "", groups = "RTS",priority=2, description = "Verify whether order is getting assigned to DE if first mile distance is less than configred limit")
	
	@AfterMethod
	public void markordersdelivered() throws InterruptedException
	{
		delmeth.dbhelperupdate(RTSConstants.set_delivered_time);
	}
	
	@Test(groups = "RTS",priority=2, description = "Verify whether order is getting placed and DE is reserved if first mile distance is less than configred limit")
	public void RTS1() throws InterruptedException, IOException {
		Thread.sleep(10000);
		String order_id=rtshelp.generaterandomorderid();
		rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"", RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		Processor processor=rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id, RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		if (null == processor) {
			Assert.assertTrue(false);
		} else {
			
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			String serviceablity=rtshelp.getserviceability(processor);
			Assert.assertEquals(serviceablity, "2");
			Assert.assertNotNull(de_id);
			//Assert.assertEquals(de_id1.toString(), de_id.toString());
		}
	}

	@Test(groups = "RTS",priority=3, description = "Verify that RTS logic work only when it is enabled for that zone")
	public void RTS2() throws InterruptedException, IOException {
		Thread.sleep(10000);
		String order_id=rtshelp.generaterandomorderid();
		rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"",RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		Processor processor=rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id,RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		if (null == processor) {
			Assert.assertTrue(false);
		} else {
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			String serviceablity=rtshelp.getserviceability(processor);
			Assert.assertEquals(serviceablity, "2");
			Assert.assertNotNull(de_id);
			Assert.assertEquals(de_id1.toString(), de_id.toString());					
		}
	}
   @Test(dataProvider = "RTS_3", groups = "RTS",priority=4, description = "Verify that RTS logic does not work and cart should work according to cerebro logic if it is not enabled for that zone")
	public void RTS3() throws InterruptedException, IOException {
		String order_id=rtshelp.generaterandomorderid();
		System.out.println("order_id for RTS_3 is "+order_id);
		rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"",RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		Processor processor=rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id,RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		if (null == processor) {
			Assert.assertTrue(false);
		} else {
			
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			String serviceablity=rtshelp.getserviceability(processor);
			rtshelp.cancelTask(order_id.toString());
			Assert.assertEquals(serviceablity, "2");
			Assert.assertNull(de_id);
			}
	}
	@Test(dataProvider = "RTS_4", groups = "RTS",priority=5, description = "Verify user is able to place order if DE is available against which calculated SLA is less than RTS zone max SLA")
	public void RTS4() throws InterruptedException, IOException {
		String order_id=rtshelp.generaterandomorderid();
		System.out.println("order_id for RTS_4 is "+order_id);
		rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"",RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		Processor processor=rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id,RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		if (null == processor) {
			Assert.assertTrue(false);
		} else {
	    	String sla=rtshelp.getsla(processor);
	    	String query=RTSConstants.zone_config_query+RTSConstants.single_assignedZones;
			Object max_sla=delmeth.dbhelperget(query, "max_sla");
			if((Double.parseDouble(sla)*60)<=(Double.parseDouble(max_sla.toString())))
			{
				String serviceability=rtshelp.getserviceability(processor);
				rtshelp.cancelTask(order_id.toString());
				Assert.assertEquals(serviceability, "2");
				Assert.assertTrue(true);
			}
				else
				{
			rtshelp.cancelTask(order_id.toString());
					Assert.assertTrue(false);
					}
		}
		}
	
	@Test(dataProvider = "RTS_5", groups = "RTS",priority=6, description = "Verify user is not able to place order if DE is not available agianst which calculated SLA is less than RTS zone max SLA")
	public void RTS5() throws InterruptedException, IOException {
		String order_id=rtshelp.generaterandomorderid();
		System.out.println("order_id for RTS_5 is "+order_id);
		rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"342432423",RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		Processor processor=rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id,RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		if (null == processor) {
			Assert.assertTrue(false);
		} else {
	    	String sla=rtshelp.getsla(processor);
	    	String query=RTSConstants.zone_config_query+RTSConstants.single_assignedZones;
			Object max_sla=delmeth.dbhelperget(query, "max_sla");
			if((Double.parseDouble(sla)*60)>=(Double.parseDouble(max_sla.toString())))
			{
				String serviceability=rtshelp.getserviceability(processor);
				String non_service_reason=rtshelp.getnonserviceabilityreason(processor);
				rtshelp.cancelTask(order_id.toString());
				Assert.assertEquals(serviceability, "1");
				Assert.assertEquals(non_service_reason, "1");				
			}
				else
				{
			rtshelp.cancelTask(order_id.toString());
					Assert.assertTrue(false);
					}
		}
		}
	@Test(dataProvider = "RTS_6",enabled=true, groups = "RTS",priority=7, description = "Verify that DE is soft tagged with order after order is placed")
	public void RTS6() throws InterruptedException, IOException {
		String order_id=rtshelp.generaterandomorderid();
		rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"",RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		Processor processor=rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id,RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		if (order_id == null) {
			Assert.assertTrue(false);

		} else {
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			String serviceablity=rtshelp.getserviceability(processor);
			Assert.assertEquals(serviceablity, "2");
			Assert.assertNotNull(de_id);
		}
}
	@Test(enabled=true, groups = "RTS",priority=8, description = "Verify soft tagging of DE with order is deleted if order is assigned to some other DE")
	public void RTS7() throws InterruptedException, IOException {
		//getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"");
		//getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id);
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		delmeth.dbhelperupdate(RTSConstants.rts_stale_order_less);
		String orderid = rtshelp.CreateRTSOrder(address_id, RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");
		if (order_id == null) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);
		} else {
			System.out.println("Order_id for RTS7 is"+order_id.toString());
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
		if (de_id!=null &&(de_id1.toString().equalsIgnoreCase(de_id.toString()))) {
				System.out.println("DE is soft tagged to with order after order is placed");
				delhelp.assignOrder(order_id.toString(), de_id2.toString());
				Thread.sleep(2000);
				rtshelp.assignmentResolve(RTSConstants.cityid, RTSConstants.single_assignedZones);
				Thread.sleep(2000);
				Object de_idd=delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
				rtshelp.cancelTask(order_id.toString());
				delhelp.makeDEFree(de_id2.toString());
			    Assert.assertEquals(de_idd.toString(), de_id2.toString(),"Order_id is"+order_id.toString());
				
			}
			else {
				rtshelp.cancelTask(order_id.toString());
				delhelp.makeDEFree(de_id2.toString());
				Assert.assertTrue(false,"Order_id is "+order_id.toString());
		}
		}	
}
	
	@Test(enabled=true, groups = "RTS",priority=9, description = "Verify soft tagging of DE with order is deleted if some other order is assigned to that DE")
	public void RTS8() throws InterruptedException, IOException {
		String orderid,orderid1;
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		delmeth.dbhelperupdate(RTSConstants.rts_stale_order_more);
		Thread.sleep(5000);
	orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");	
		if (order_id == null) {
		System.out.println("Order is not created");
			Assert.assertTrue(false);
		} else {
			System.out.println("Order_id for RTS8 is"+order_id.toString());
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			if (de_id!=null &&(de_id1.toString().equalsIgnoreCase(de_id.toString()))) {
				System.out.println("DE is soft tagged to with order after order is placed");
				orderid1 = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
				Object order_id1=JsonPath.read(orderid1,"$.data.order_id").toString().replace("[","").replace("]","");	
				if (order_id1 == null) {
					System.out.println("Order is not created");
					Assert.assertTrue(false);
				}
				else{
				System.out.println("Order_id1 for RTS8 is"+order_id1.toString());
				Thread.sleep(10000);
				delhelp.assignOrder(order_id1.toString(),de_id1.toString());
				Thread.sleep(2000);
				rtshelp.assignmentResolve(RTSConstants.cityid, RTSConstants.single_assignedZones);
				Thread.sleep(2000);
				Object de_idd=delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id1.toString(), 0);
				rtshelp.cancelTask(order_id.toString());
				rtshelp.cancelTask(order_id1.toString());
				delmeth.dbhelperupdate(RTSConstants.rts_stale_order_less);
				delhelp.makeDEFree(de_id1.toString());
				Assert.assertEquals(de_idd.toString(), de_id1.toString(),"Order_id is"+order_id.toString());
				}
				}	
			else {
				rtshelp.cancelTask(order_id.toString());
     			delmeth.dbhelperupdate(RTSConstants.rts_stale_order_less);
				delhelp.makeDEFree(de_id1.toString());
				Assert.assertTrue(false,"Order_id is "+order_id.toString());
			}
		}
		
		}
	@Test(enabled=true, groups = "RTS",priority=10, description = "Verify soft tagging of DE with order is not deleted if order is assigned to same DE")
	public void RTS9() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		delmeth.dbhelperupdate(RTSConstants.rts_stale_order_more);
		Thread.sleep(5000);
		String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");
		if (order_id == null) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);
		} else {
			System.out.println("Order_id for RTS9 is"+order_id.toString());
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			if (de_id!=null &&(de_id1.toString().equalsIgnoreCase(de_id.toString()))) {
				System.out.println("DE is soft tagged to with order after order is placed");
				Thread.sleep(10000);
				delhelp.assignOrder(order_id.toString(), de_id1.toString());
				Thread.sleep(2000);
				rtshelp.assignmentResolve(RTSConstants.cityid, RTSConstants.single_assignedZones);
				Thread.sleep(2000);
				Object de_idd=delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
				rtshelp.cancelTask(order_id.toString());
				delmeth.dbhelperupdate(RTSConstants.rts_stale_order_less);
				delhelp.makeDEFree(de_id1.toString());
				Assert.assertEquals(de_idd.toString(), de_id1.toString(),"Order_id is"+order_id.toString());
				}
			else {
				rtshelp.cancelTask(order_id.toString());
				delmeth.dbhelperupdate(RTSConstants.rts_stale_order_less);
				delhelp.makeDEFree(de_id1.toString(),"Order_id is"+order_id.toString());
				Assert.assertTrue(false,"Order_id is "+order_id.toString());
			}
	}
}
	@Test(enabled=true, groups = "RTS",priority=11, description = "Verify soft tagging of DE with order is deleted if order is cancelled")
	public void RTS10() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");
		if (order_id == null) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);

		} else {
			System.out.println("Order_id for RTS10 is"+order_id.toString());
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			if (de_id!=null &&(de_id1.toString().equalsIgnoreCase(de_id.toString()))) {
				System.out.println("DE is soft tagged to with order after order is placed");
				Thread.sleep(10000);
				delhelp.ordercancel(order_id.toString());
				System.out.println("RTS10 order_id="+order_id.toString());
			//	checkhelp.OrderCancel(DeliveryConstant.mobile, DeliveryConstant.password, order_id.toString());
				rtshelp.assignmentResolve(RTSConstants.cityid, RTSConstants.single_assignedZones);
				Thread.sleep(2000);
				Object de_idd=delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
				rtshelp.cancelTask(order_id.toString());
				delhelp.makeDEFree(de_id1.toString());
				Assert.assertEquals(de_idd,null,"Order_id is"+order_id.toString());
			}
				else {
				delhelp.makeDEFree(de_id1.toString());
				rtshelp.cancelTask(order_id.toString());
				Assert.assertTrue(false,"Order_id is "+order_id.toString());
			}

		}
}
	@Test(enabled=true, groups = "RTS",priority=12, description = "Verify soft tagging of DE with order is deleted if DE becomes inactive(Logout)")
	public void RTS11() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		Thread.sleep(5000);
		String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");
		if (order_id == null) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);
		} 
		else
		{
			System.out.println("Order_id for RTS11 is"+order_id.toString());
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			if (de_id!=null &&(de_id1.toString().equalsIgnoreCase(de_id.toString()))) {
				System.out.println("DE is soft tagged to with order after order is placed");
				delhelp.makeDEFree(de_id1.toString());
				delhelp.markDEInActive(de_id1.toString());
				Thread.sleep(5000);
				rtshelp.assignmentResolve(RTSConstants.cityid, RTSConstants.single_assignedZones);
				Thread.sleep(2000);
				Object de_idd=delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
				rtshelp.cancelTask(order_id.toString());
				Assert.assertEquals(de_idd ,de_id1.toString(),"Order_id is "+order_id.toString());
				}
			else {
				rtshelp.cancelTask(order_id.toString());
				delhelp.makeDEFree(de_id1.toString());
				delhelp.makeDEActive(de_id1.toString());
				Assert.assertTrue(false,"Order_id is "+order_id.toString());
			}
		}
}
	@Test(dataProvider = "RTS_12",enabled=true, groups = "RTS",priority=13, description = "Verify whether only DE within first mile distance are searched for availability for RTS")
	public void RTS12() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.RTS_rest_288_far_lat,RTSConstants.RTS_rest_288_far_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.RTS_rest_288_far_lat,	RTSConstants.RTS_rest_288_far_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object status_code=JsonPath.read(orderid,"$..statusCode").toString().replace("[","").replace("]","");
		Assert.assertEquals(status_code.toString(),"1");
		}
	@Test(dataProvider = "RTS_13",enabled=true, groups = "RTS",priority=14, description = "Verify order is tagged with DE who can fulfill the order in mimimum SLA time if multiple DE are eligible within max SLA")
	public void RTS13() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
    	String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");
		if (order_id == null) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);

		} else {
			System.out.println("Order_id for RTS13 is"+order_id.toString());
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			Assert.assertNotNull(de_id);
			Assert.assertEquals(de_id1.toString(), de_id.toString(),"Order_id is "+order_id.toString());
			
		}
	}
	@Test(dataProvider = "RTS_14",enabled=true, groups = "RTS",priority=15, description = "Verify user is able to place long distance order if DE is available against which calculated SLA is less than long distance max SLA")
	public void RTS14() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		String orderid = rtshelp.CreateRTSOrder(long_distance_address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");
		if (order_id == null) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);

		} else {
			System.out.println("Order_id for RTS14 is"+order_id.toString());
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			Assert.assertNotNull(de_id);
			Assert.assertEquals(de_id1.toString(), de_id.toString(),"Order_id is "+order_id.toString());
			
		}
	}
	@Test(dataProvider = "RTS_15",enabled=true, groups = "RTS",priority=16, description = "Verify user is not able to place long distance order if DE is not available against which calculated SLA is more than long distance max SLA")
	public void RTS15() throws InterruptedException, IOException {
		String orderid = rtshelp.CreateRTSOrder(long_distance_address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		rtshelp.cancelTask(orderid);
		Object status_code=JsonPath.read(orderid,"$..statusCode").toString().replace("[","").replace("]","");
		Assert.assertEquals(status_code.toString(),"1");
		}
	@Test(dataProvider = "RTS_16",enabled=true, groups = "RTS",priority=17, description = "Verify user is able to place long distance order from restaurant which fall under zone where RTS is not enabled if its satisfies polygon max SLA and last mile")
	public void RTS16() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
    	String orderid = rtshelp.CreateRTSOrder(long_distance_address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
    	Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");
		if (order_id!= null) {
			System.out.println("Order is created");
			Assert.assertTrue(true,"Order_id is "+order_id.toString());
		}	
		}
	@Test(dataProvider = "RTS_17",enabled=true, groups = "RTS",priority=18, description = "Verify user is able to place order even if BF crossed zone stop BF but some DE is available who can fullfill order within max SLA")
	public void RTS17() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
    	String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");
		if (order_id == null) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);

		} else {
			System.out.println("Order_id for RTS17 is"+order_id.toString());
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			Assert.assertNotNull(de_id);
			Assert.assertEquals(de_id1.toString(), de_id.toString(),"Order_id is "+order_id.toString());
			
		}
	}
	@Test(dataProvider = "RTS_18",enabled=true, groups = "RTS",priority=19, description = "Verify order DE soft tagged is broken when Order stale time is more than configured limit")
	public void RTS18() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_disabled, RTSConstants.cityid, false);
		Thread.sleep(5000);
		String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");	
		if (order_id == null) {
		System.out.println("Order is not created");
			Assert.assertTrue(false);

		} else {
			System.out.println("Order_id for RTS18 is"+order_id.toString());
			rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_disabled, RTSConstants.cityid,false);
			Thread.sleep(12000);
			rtshelp.assignmentResolve(RTSConstants.cityid, RTSConstants.single_assignedZones);
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			Assert.assertEquals(de_id.toString(), de_id1.toString(),"Order_id is "+order_id.toString());
		
	}
	}
	@Test(dataProvider = "RTS_19",enabled=true, groups = "RTS",priority=20, description = "Verify soft tagged DE is not considered for next order until it is assinged to same order it is tagged with or with other order")
	public void RTS19() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_disabled, RTSConstants.cityid, false);
		Thread.sleep(5000);
		String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");	
		if (order_id == null) {
		System.out.println("Order is not created");
			Assert.assertTrue(false);

		} else {
			System.out.println("Order_id for RTS19 is"+order_id.toString());
			String orderid1 = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
			Object status_code=JsonPath.read(orderid1,"$..statusCode").toString().replace("[","").replace("]","");
			rtshelp.cancelTask(orderid);
			Assert.assertEquals(status_code.toString(),"1");
	}
	}
	@Test(dataProvider = "RTS_20",enabled=true, groups = "RTS",priority=21, description = "Verify that DE whose service zone is B are not soft tagged even if it is within first mile if rest is of zone A and RTS is enabled for zone A")
	public void RTS20() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_disabled, RTSConstants.cityid, false);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,	RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid,true);
		deldatahelp.deleteDEzonemap(de_id2.toString(),RTSConstants.single_assignedZones_int);
		String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object status_code=JsonPath.read(orderid,"$..statusCode").toString().replace("[","").replace("]","");
		Assert.assertEquals(status_code.toString(),"1");
	}
	@Test(dataProvider = "RTS_21",enabled=true, groups = "RTS",priority=22, description = "Verify that only DE whose service zone is A are soft tagged if RTS is enabled for zone A")
	public void RTS21() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		deldatahelp.deleteDEzonemap(de_id2.toString(),RTSConstants.single_assignedZones_int);
		rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.non_RTS_zone,RTSConstants.de_enabled, RTSConstants.cityid, true);
		Thread.sleep(5000);
		String orderid = rtshelp.CreateRTSOrder(address_id,RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
		Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");	
		if (order_id == null) {
		System.out.println("Order is not created");
			Assert.assertTrue(false);

		} else {
			System.out.println("Order_id for RTS21 is"+order_id.toString());
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			Assert.assertEquals(de_id.toString(), de_id1.toString(),"Order_id is "+order_id.toString());
		
				}
	}
	@Test(groups = "RTS",priority=23, description = "Verify user is able to place dominos irrespective of whether RTS is enabled")
	public void RTS22() throws InterruptedException, IOException {
		String order_id=rtshelp.generaterandomorderid();
		rtshelp.getdatadominos(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"", RTSConstants.dominos_rest_id, RTSConstants.dominos_rest_lat_lng, RTSConstants.rts_area_id, RTSConstants.dominos_cust_lat_lng);
		Processor processor=rtshelp.getdatadominos(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id,RTSConstants.dominos_rest_id, RTSConstants.dominos_rest_lat_lng, RTSConstants.rts_area_id, RTSConstants.dominos_cust_lat_lng);
		if (null == processor) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);
		} else {
			
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			String serviceablity=rtshelp.getserviceability(processor);
			Assert.assertEquals(serviceablity, "2");
			Assert.assertNull(de_id);
			}
	}
	@Test(groups = "RTS",priority=24, description = "Verify user is able to place dominos irrespective of whether RTS is enabled")
	public void RTS23() throws InterruptedException, IOException {
		String order_id=rtshelp.generaterandomorderid();
		rtshelp.getdatadominos(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"",RTSConstants.dominos_rest_id, RTSConstants.dominos_rest_lat_lng, RTSConstants.rts_area_id, RTSConstants.dominos_cust_lat_lng);
		Processor processor=rtshelp.getdatadominos(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id,RTSConstants.dominos_rest_id, RTSConstants.dominos_rest_lat_lng, RTSConstants.rts_area_id, RTSConstants.dominos_cust_lat_lng);
		if (null == processor) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);
		} else {
			
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			String sla=rtshelp.getsla(processor);
			Assert.assertEquals(sla, "30");
			Assert.assertNull(de_id);
			}
	}
	@Test(dataProvider = "RTS_24",enabled=true, groups = "RTS",priority=25, description = "Verify RTS call is made by cerbro only when Banner factor is between rts_start_bfactor and rts_stop_bfactor")
	public void RTS24() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		String order_id=rtshelp.generaterandomorderid();
		Thread.sleep(5000);
		rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"",RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		Processor processor=rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id,RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		if (null == processor) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);
		} else {
			
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.cancelTask(order_id.toString());
			String serviceablity=rtshelp.getserviceability(processor);
			Assert.assertEquals(serviceablity, "2");
			Assert.assertNotNull(de_id);
		}
	}
	@Test(dataProvider = "RTS_25",enabled=true, groups = "RTS",priority=26, description = "Verify RTS call is made by cerbro only when Banner factor is between rts_start_bfactor and rts_stop_bfactor")
	public void RTS25() throws InterruptedException, IOException {
		rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,RTSConstants.de_enabled, RTSConstants.cityid, true);
		String order_id=rtshelp.generaterandomorderid();
		Thread.sleep(5000);
		rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,"",RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		Processor processor=rtshelp.getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less, RTSConstants.cart_state_place_order,order_id,RTSConstants.rts_rest_id_String, RTSConstants.rts_rest_288_lat_long, RTSConstants.rts_area_id, RTSConstants.rts_cust_288_lat_long);
		if (null == processor) {
			System.out.println("Order is not created");
			Assert.assertTrue(false);
		} else {
			
			Object de_id = delmeth.redisconnectget(RTSConstants.redis_get_task_de,
					order_id.toString(), 0);
			rtshelp.updatstartandstopbf(RTSConstants.startbf, RTSConstants.stopbf, RTSConstants.bannerfactor_more);
			rtshelp.cancelTask(order_id.toString());
			String serviceablity=rtshelp.getserviceability(processor);
			Assert.assertEquals(serviceablity, "2");
			Assert.assertNull(de_id);
		}
	}
	@AfterClass
	public void setotherzone()
	{
		delmeth.dbhelperupdate(RTSConstants.set_rts_zone_4);
		delmeth.dbhelperupdate(RTSConstants.set_rts_area_4);
		 String query="update delivery_boys set enabled=1 where id="+de_id1;
		 delmeth.dbhelperupdate(query);
		 String query2="update delivery_boys set enabled=1 where id="+de_id2;
		 delmeth.dbhelperupdate(query2);
		 try {
			rtshelp.updateDERTS(de_id1.toString(), RTSConstants.de_id1_lat,
						RTSConstants.de_id1_lng, RTSConstants.single_assignedZones,
						RTSConstants.de_disabled, RTSConstants.cityid, false);
			rtshelp.updateDERTS(de_id2.toString(), RTSConstants.de_id2_lat,
					RTSConstants.de_id2_lng, RTSConstants.single_assignedZones,
					RTSConstants.de_disabled, RTSConstants.cityid, false);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	}