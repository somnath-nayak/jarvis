package com.swiggy.api.erp.ff.helper;

import com.jayway.jsonpath.JsonPath;
import com.mysql.jdbc.ResultSetMetaData;
import com.sun.jna.StringArray;
import com.swiggy.api.erp.ff.constants.ContextualCancellationConstants;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.dp.ContextualCancellationData;
import com.swiggy.api.erp.ff.tests.ContextualCancellationTest;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.testng.annotations.Test;

public class ContextualCancellationHelper implements ContextualCancellationConstants {
	
	static Initialize gameofthrones = new Initialize();

	public HashMap<String, String> getHeaders(){
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		return headers;
	}

	public HashMap<String, String> getAuthHeaders(){
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization", ContextualCancellationConstants.basic_auth);
		return headers;
	}
	
	public Processor cancelOrder(String order_id, String disposition, String subDisposition, String cancellationFee, String CancellationFeeApplicability){
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "ffcancellationapi", gameofthrones);
		String[] urlParam = {order_id};
		String[] payload = {disposition, subDisposition, cancellationFee, CancellationFeeApplicability};
		Processor processor = new Processor(service, null, payload, urlParam);
		return processor;
	}
	
	
	
	
	   public static List getGroupsName(){

	        
	            List<Map<String, Object>> group = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT name FROM contextual_cancellations_group" +";");
	            
	            List<String> groupname = new ArrayList<String>();
	            for (Map<String, Object> map: group)
	            {
	                System.out.println(map.get("name").toString());

	                groupname.add(map.get("name").toString());
	            }
	            return groupname;
	    }
	   
	   
	
	  
	public static List getDispositionsNameByGroup(int groupId) {

		List<Map<String, Object>> group = SystemConfigProvider.getTemplate(OMSConstants.SERVICE)
				.queryForList("SELECT name FROM contextual_cancellations_disposition WHERE group_id =" + groupId);

		List<String> Groupdata = new ArrayList<String>();
		for (Map<String, Object> map : group) {
			System.out.println(map.get("name").toString());

			Groupdata.add(map.get("name").toString());
		}
		return Groupdata;
	}
	   
	   public List getsubDispositionsNameByDisposition(int dispositionId){
		  
	            List<Map<String, Object>> group = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT name FROM contextual_cancellations_subdisposition WHERE disposition_id = "+dispositionId);

	    		List<String> data = new ArrayList<String>();
	    		for (Map<String, Object> map : group) {
	    			System.out.println(map.get("name").toString());

	    			data.add(map.get("name").toString());
	    		}
	    		return data;
	    }
	   
	   public static List getAllDispositionsName() {

			List<Map<String, Object>> group = SystemConfigProvider.getTemplate(OMSConstants.SERVICE)
					.queryForList("SELECT name FROM contextual_cancellations_disposition");

			List<String> Dispositionsdata = new ArrayList<String>();
			for (Map<String, Object> map : group) {
				System.out.println(map.get("name").toString());

				Dispositionsdata.add(map.get("name").toString());
			}
			return Dispositionsdata;
		}
		   
		   public static List getAllSubDispositionsName(){
			  
		            List<Map<String, Object>> group = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT name FROM contextual_cancellations_subdisposition");

		    		List<String> subDispositionsdata = new ArrayList<String>();
		    		for (Map<String, Object> map : group) {
		    			System.out.println(map.get("name").toString());

		    			subDispositionsdata.add(map.get("name").toString());
		    		}
		    		return subDispositionsdata;
		    }
	   
	 
	  
		   
		   
		   
		   public static List getAllDispositionsId() {

				List<Map<String, Object>> group = SystemConfigProvider.getTemplate(OMSConstants.SERVICE)
						.queryForList("SELECT id FROM contextual_cancellations_disposition");

				List<String> Dispositionsdata = new ArrayList<String>();
				for (Map<String, Object> map : group) {
					System.out.println(map.get("id").toString());

					Dispositionsdata.add(map.get("id").toString());
				}
				return Dispositionsdata;
			}
		  
		   
			   public static List getAllSubDispositionsId(){
				  
			            List<Map<String, Object>> group = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT distinct id FROM contextual_cancellations_subdisposition");

			    		List<String> subDispositionsdata = new ArrayList<String>();
			    		for (Map<String, Object> map : group) {
			    			System.out.println(map.get("id").toString());

			    			subDispositionsdata.add(map.get("id").toString());
			    		}
			    		return subDispositionsdata;
			    }
			  //@To Compare Two Lists
			   public static <T> boolean listEqualsIgnoreOrder(List<T> list1, List<T> list2) {
				    return new HashSet<>(list1).equals(new HashSet<>(list2));
				}
	   
	
			   
			   
			   public static HashMap getDeliveryStatusConfig()
			   {
				   HashMap<String, Integer> deliveryConfigMap = new HashMap<>();
				     List<Map<String, Object>> deliveryConfigs = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT * FROM contextual_cancellations_deliverystatus_config");
		    		for (Map<String, Object> map : deliveryConfigs) {
		    			System.out.println(map.get("name").toString()+" "+map.get("id").toString());
		    			deliveryConfigMap.put((String) map.get("name"), (Integer) (map.get("id")));
		    		}
		    System.out.println("key and value"+deliveryConfigMap);
		    		return deliveryConfigMap;        
			   } 
			   
			   public static HashMap getPlacingStatusConfig()
			   {
				   HashMap<String, Integer> placingConfigMap = new HashMap<>();
				     List<Map<String, Object>> placingConfigs = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT * FROM contextual_cancellations_placingstatus_config");
		    		for (Map<String, Object> map : placingConfigs) {
		    			System.out.println(map.get("name").toString()+" "+map.get("id").toString());
		    			placingConfigMap.put((String) map.get("name"), (Integer) (map.get("id")));
		    		}
		    System.out.println("key and value"+placingConfigs);
		    		return placingConfigMap;        
			   } 
			   
			 //@Delivery status Placing status Verification status 
			   public static List<Integer> getAllDispositionsByGroup(int Verification_status_config,int Delivery_status_config,int Placing_status_config, int groupId ){
					  
		            List<Map<String, Object>> dispositions = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT * FROM contextual_cancellations_disposition where verification_status_config like '%"+Verification_status_config+"%' AND delivery_status_config like '%"+Delivery_status_config+"%' AND placing_status_config like '%"+Placing_status_config+"%' AND group_id="+groupId);


		    		List<Integer> dispositionsList = new ArrayList<Integer>();
		    		for (Map<String, Object> map1 : dispositions) {
		    			dispositionsList.add((Integer) map1.get("id"));
		    		}
		    		return dispositionsList;
		        }
			   
			   
                public static List<Integer> getAllStatus(int Verification_status_config)
                {
        		
	            List<Map<String, Object>> verification_status = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT * FROM contextual_cancellations_disposition where verification_status_config like '%"+Verification_status_config+"%'");

	        	List<Integer> verification_status_List = new ArrayList<Integer>();
	    		for (Map<String, Object> map1 : verification_status) {
	    			verification_status_List.add((Integer) map1.get("id"));
	    		}
	    		return verification_status_List;
        	
                }
                
                private void changeVerificationStatusInDB(String orderId, String verificationStatus) {
                	SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("UPDATE oms_orderverification SET verification_status = '"+verificationStatus+"' WHERE order_id = "+orderId+";");
                }
                
                private void deleteVerificationStatusFromDB(String orderId) {
                	SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("DELETE FROM oms_orderverification WHERE order_id = "+orderId+";");
                }
                
                private void changeDeliveryAndPlacingStatusInDB(String orderId, String placingStatus, String deliveryStatus) {
                	SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("UPDATE oms_orderstatus SET status = '"+placingStatus+"', delivery_status = '"+deliveryStatus+"' WHERE id IN (SELECT status_id FROM oms_order WHERE order_id = "+orderId+");");
                }
                
                public void changeDeliveryPlacingAndVerificationStatusInDB(String scenario, String orderId) throws SQLException, IOException, InterruptedException {
                	String[] statusArray = scenario.split(" ");
                	
                	String verificationStatus = statusArray[0];
                	String deliveryStatus = statusArray[1];
                	String placingStatus = statusArray[2];
                	
                	if(verificationStatus.equals("none"))
                	{
                		deleteVerificationStatusFromDB(orderId);
                	}else {
                		changeVerificationStatusInDB(orderId, verificationStatus);
					}
                	
                	changeDeliveryAndPlacingStatusInDB(orderId, placingStatus, deliveryStatus);
                	
                }
		
                
                public List ElapsedSLA(int orderId)
                {
                	
                	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
                    Date date = new Date();
                    long currentTimeInMillis = date.getTime();
                    
                    System.out.println(formatter.format(date)); 
                    String current= formatter.format(date);
                //    Date date1 = StringToDate(formatter.format(date));
                //	System.out.println(date1);
                    List<Map<String, Object>> verification_status = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT pg_response_time FROM oms_order where order_id="+orderId+";");

                    List<Date> verification_status_List = new ArrayList<Date>();
    	    		for (Map<String, Object> map1 : verification_status) {
    	    		verification_status_List.add((Date) map1.get("pg_response_time"));
                    System.out.println(map1);
                    
                    Date pg_response = (Date) map1.get("pg_response_time");
                    System.out.println(pg_response+"pg response");
                    long pg_response_in_millis=pg_response.getTime();
                    
                    long difference_millis=currentTimeInMillis-pg_response_in_millis;
                    
                    List<Map<String, Object>> SLA_interval_list = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("SELECT est_total_time FROM oms_ordercalculation WHERE id IN (SELECT calculations_id FROM oms_order WHERE order_id ="+orderId+")");

                    List<Double> SLA_status_List = new ArrayList<Double>();
    	    		for (Map<String, Object> map2 : SLA_interval_list) {
    	    			SLA_status_List.add((Double) map2.get("est_total_time"));
                    System.out.println(map2);
                    
                    Double est_response = (Double) map2.get("est_total_time");
                    System.out.println(est_response+"est_response");
                    long est_response_in_millis=(new Double(est_response)).longValue();
                    
                    
                    
                    if(difference_millis>est_response_in_millis)
                    {
                    	System.out.println("SLA Breach");
                    }
                    else
                    {
                    	System.out.println("Not SLA Breach");

                    }
                    
    	    		}
    	    		System.out.println(verification_status_List);
               	}
					return verification_status_List;
                }
                
              
}
