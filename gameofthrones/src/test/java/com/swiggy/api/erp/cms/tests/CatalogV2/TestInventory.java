package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.CatalogV2PricingInventoryDP;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created-By:Ashiwani
 */

public class TestInventory extends CatalogV2PricingInventoryDP {
    CatalogV2Helper cms=new CatalogV2Helper();



    @Test(dataProvider = "createinventorydp",description = "This test will create a inventory for Store-Sku")
    public void createInventory(String key,long quantity) throws Exception{
        String response=cms.createInventory(key,quantity).ResponseValidator.GetBodyAsText();
        String keyr= JsonPath.read(response,"$.data..key").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(keyr,key,"Something Wrong");
        Assert.assertNotNull(JsonPath.read(response,"$.data"));
    }

    @Test(description = "This test will verify negative case for inventory Create")
    public void createInventoryNegative() throws Exception{
        String response=cms.createInventoryNegative().ResponseValidator.GetBodyAsText();
        String statusmsg=JsonPath.read(response,"$.status_message").toString().replace("[","").replace("]","");
        Assert.assertEquals(statusmsg,"Malformed Request");
    }

    @Test(dataProvider = "createinventorydp",description = "This test will fetch inventory for given Store-Sku")
    public void getInventory(String key,long quantity) throws Exception{
        String responsecreate=cms.createInventory(key,quantity).ResponseValidator.GetBodyAsText();
        String key1= JsonPath.read(responsecreate,"$.data..key").toString().replace("[","").replace("]","").replace("\"","");
        String response=cms.getInventory(key1).ResponseValidator.GetBodyAsText();
        String keyact= JsonPath.read(response,"$.data..key").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertNotNull(JsonPath.read(response,"$.data"));
        Assert.assertEquals(keyact,key,"Some thing wrong");
    }

    @Test(dataProvider = "createinventorydp",description = "This test will clear inventory for Store-Sku")
    public void clearInventory(String key,long quantity) throws Exception{
        String responsecreate=cms.createInventory(key,quantity).ResponseValidator.GetBodyAsText();
        String key1= JsonPath.read(responsecreate,"$.data..key").toString().replace("[","").replace("]","");
        String response=cms.clearInventory(key1).ResponseValidator.GetBodyAsText();
        String quan= JsonPath.read(response,"$.data..inventories.quantity").toString().replace("[","").replace("]","");
        Assert.assertEquals(quan,"0","Something Wrong");
    }

    @Test(dataProvider = "clearinventorynegativedp",description = "This test will verify non-clear inventory for Store-Sku")
    public void clearInventoryNegative(String key) throws Exception{
        String response=cms.clearInventoryNegative(key).ResponseValidator.GetBodyAsText();
        String status_message= JsonPath.read(response,"$.status_message").toString().replace("[","").replace("]","");
        Assert.assertEquals(status_message,"Failed to clear.","Something Wrong");
    }

    @Test(dataProvider = "getinventorydpnegative")
    public void getInventoryNegativeCase(String store_sku){
        String response=cms.getInventory(store_sku).ResponseValidator.GetBodyAsText();
        List<String> keyact= JsonPath.read(response,"$.data");
        Assert.assertEquals(keyact.size(),0,"Size should be zero");

    }

}
