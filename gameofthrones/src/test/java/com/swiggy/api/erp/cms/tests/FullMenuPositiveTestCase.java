package com.swiggy.api.erp.cms.tests;

import java.util.List;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.swiggy.api.erp.cms.dp.FullMenuPositiveDp;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.FullMenu.Items;

import framework.gameofthrones.JonSnow.Processor;

/**
 * Created by HemaGovinadaraj on 14/2/18.
 */
public class FullMenuPositiveTestCase extends FullMenuPositiveDp {

	@Test(dataProvider = "positiveItemsMaptoCat")
    public void positiveItemsMaptoCat(String rest_id, String json_payload, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
     softAssert.assertAll();
    }
   
    @Test(dataProvider = "positiveItemsMaptoSubCat")
    public void positiveItemsMaptoSubCat(String rest_id, String json, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
     if(errors.length == 0)
      	softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
     else
     softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
     softAssert.assertAll();
     
     
     
    }
    //start here
    
    @Test(dataProvider = "positiveItemsWithVarinats")
    public void positiveItemsWithVarinats(String rest_id, String json_payload, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
     softAssert.assertAll();
    }
    
    @Test(dataProvider = "positiveItemsWithAddons")
    public void positiveItemsWithAddons(String rest_id, String json_payload, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception{
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
     softAssert.assertAll();
    }
	
    
    @Test(dataProvider = "positiveItemsWithVariantsAndPricingCombination")
    public void positiveItemsWithVariantsAndPricingCombination(String rest_id, String json_payload, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
     softAssert.assertAll();
    }
	

  
    @Test(dataProvider = "positiveItemsWithVariantsAndPCwithAAndV",description = "pricing module with both variants and  Addons")
    public void positiveItemsWithVariantsAndPCwithAAndV(String rest_id, String json_payload, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception{
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
     softAssert.assertAll();
    }

    @Test(dataProvider = "positiveItemsWithVAndPCAdoonEmptyArray",description = "pricing module with both variants and PC, Addons as empty Array")
    public void positiveItemsWithVAndPCAdoonEmptyArray(String rest_id, String json_payload, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor,ids,items,ItemPassOrFail));
     softAssert.assertAll();
    }
    
    @Test(dataProvider = "positiveItemsWithGST",description = "Items level GST Vaues")
    public void positiveItemsWithGST(String rest_id, String json_payload, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception{
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
     softAssert.assertAll();
    }
   
    @Test(dataProvider = "positiveItemsWithVariantGST",description = "Variants level GST Vaues")
    public void positiveItemsWithVariantGST(String rest_id, String json_payload, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception{
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
     softAssert.assertAll();
    }

    
    @Test(dataProvider = "positiveItemsWithItemSlots",description = "Items having slots")
    public void positiveItemsWithItemSlots(String rest_id, String json_payload, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception{
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
     softAssert.assertAll();
    }
  
}
