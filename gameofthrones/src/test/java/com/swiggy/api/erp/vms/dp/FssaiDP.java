package com.swiggy.api.erp.vms.dp;

import java.io.IOException;
import java.util.HashMap;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.vms.fssaiservice.pojo.FssaiACKBuilder;
import com.swiggy.api.erp.vms.fssaiservice.pojo.FssaiLicenseUpdateBuilder;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;

import framework.gameofthrones.Tyrion.JsonHelper;

public class FssaiDP extends RMSCommonHelper{

	static HashMap<String, String> rmsSessionData = null;


	static String restaurantId = null;
	static String restaurantId1 = null;
	public static String VENDOR_PASSWORD = null;
	public static String VENDOR_USERNAME = null;
	public static String consumerAppPassword = null;
	public static long consumerAppMobile = 0l;
	public static String OMS_USERNAME = null;
	public static String OMS_PASSWORD = null;
	static String OrderId = null;
	static String oldPassword = null;
	static String itemId = null;
	static String variantId = null;
	static String variantGroupId = null;

	static {
		try {
			if (getEnv().equalsIgnoreCase("stage1")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "rkonowhere";
				consumerAppMobile = 7406734416l;
				// restaurantId = "4993";
				restaurantId1 = "4993";
				restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
				oldPassword = "imran@123";
				itemId = "5748192";
				variantId = "1637694";
				variantGroupId = "481253";
			} else if (getEnv().equalsIgnoreCase("stage2")) {
				VENDOR_PASSWORD = "imran@123";
				VENDOR_USERNAME = "9990";
				consumerAppPassword = "rkonowhere";
				consumerAppMobile = 7406734416l;
				restaurantId = "9990";
				restaurantId1 = "4993";
				// restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
				oldPassword = "imran@123";
				itemId = "5748192";
				variantId = "1637694";
				variantGroupId = "481253";
			} else if (getEnv().equalsIgnoreCase("stage3")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "rkonowhere";
				consumerAppMobile = 7406734416l;
				restaurantId1 = "4993";
				restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
				oldPassword = "imran@123";
				itemId = "5748192";
				variantId = "1637694";
				variantGroupId = "481253";
			} else if (getEnv().equalsIgnoreCase("prod")) {
				VENDOR_PASSWORD = "$w199y@zolb";
				VENDOR_USERNAME = "9738948943";
				consumerAppPassword = "swiggy";
				consumerAppMobile = 7899772315l;
				restaurantId = "9990";
				restaurantId1 = "4993";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
				oldPassword = "imran@1234";
				itemId = "1837858";
				variantId = "12";
				variantGroupId = "9";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		rmsSessionData = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

	}

	
	@DataProvider(name="getFssaiAlert")
	public static Object[][] getFssaiAlert()
	{
		return new Object[][] {{rmsSessionData.get("accessToken"),0,"Successfully fetched FSSAI alert details"},
			{"a87ac2ae-4022-4b1e-8186-d53b32e74bcf",-3,"Invalid Session"}};
	}
	
	
	
	@DataProvider(name="getFssaiInfo")
	public static Object[][] getFssaiInfo()
	{
		return new Object[][] {{rmsSessionData.get("accessToken"),restaurantId}};
	}
	
	@DataProvider(name="fssaicompliance")
	public static Object[][] fssaiCompliance()
	{
		return new Object[][] {{rmsSessionData.get("accessToken"),restaurantId,0,"Fetched compliance details"},
			{rmsSessionData.get("accessToken"),"123",-3,"Invalid Session"}
		};
	}
	
	
	@DataProvider(name="fssaiackUpdate")
	public static Object[][] fssaiackUpdate() throws IOException
	{
		JsonHelper jsonHelper = new JsonHelper();
		FssaiACKBuilder builderObject=new FssaiACKBuilder();
		builderObject.setRegistered_name("Test1234");
		builderObject.setAcknowledgement_reference("43762377853783");
		builderObject.setAddress_line_1("address line 1 new ");
		builderObject.setAddress_line_2("sdsdsd sdsd");
		builderObject.setOfficial_city_id(1l);
		builderObject.setZipcode("560029");
		builderObject.setOwner_name("khrhadfg");
		builderObject.setOwner_contact_number("9876543210");
		builderObject.setApplication_date("05-09-2018");
		builderObject.setRestaurant_id(9990l);
		builderObject.setUser_id("9886379332");
		builderObject.setUser_type("te");
		builderObject.setDocument_url("https://s3-ap-southeast-1.amazonaws.com/cms-file-uploads-cuat/uploads/da4d02ed-2629-4243-86eb-95a963877f98-fssaiUpload.png");
		return new Object[][] {{rmsSessionData.get("accessToken"),jsonHelper.getObjectToJSON(builderObject)}};
	}
	
	@DataProvider(name="fssailicenseUpdate")
	public static Object[][] fssailicenseUpdate() throws IOException
	{
		JsonHelper jsonHelper = new JsonHelper();
		FssaiLicenseUpdateBuilder fssaiLicenseUpdateObject=new FssaiLicenseUpdateBuilder();
		fssaiLicenseUpdateObject.setId(1234l);
		fssaiLicenseUpdateObject.setUser_id("9738948943");
		fssaiLicenseUpdateObject.setUser_type("te");
		fssaiLicenseUpdateObject.setUser_meta("");
		fssaiLicenseUpdateObject.setRestaurant_id(9990l);
		fssaiLicenseUpdateObject.setRegistered_name("Test");
		fssaiLicenseUpdateObject.setAddress_line_1("tryety");
		fssaiLicenseUpdateObject.setAddress_line_2("ktfjhg");
		fssaiLicenseUpdateObject.setOfficial_city("Bangalore");
		
		fssaiLicenseUpdateObject.setOfficial_city_id(1l);
		fssaiLicenseUpdateObject.setZipcode("560040");
		fssaiLicenseUpdateObject.setDocument_url("https://s3-ap-southeast-1.amazonaws.com/cms-file-uploads-cuat/uploads/244452c7-d870-4e1f-8aad-20deae1b46a8-fssaiUpload.png");
		fssaiLicenseUpdateObject.setStatus(0l);
		fssaiLicenseUpdateObject.setFssai_licence_number("12345678912347");
		fssaiLicenseUpdateObject.setFssai_licence_validity("02-10-2017");
		fssaiLicenseUpdateObject.setLast_updated_at("04-11-2018");
		fssaiLicenseUpdateObject.setAcknowledgement_reference("12345678954324");
		fssaiLicenseUpdateObject.setApplication_date("02-10-2018");
		fssaiLicenseUpdateObject.setFssai_status("ok");
		fssaiLicenseUpdateObject.setFssai_licence_validity("01-10-2017");
		fssaiLicenseUpdateObject.setOwner_contact_number("1235566666");
		fssaiLicenseUpdateObject.setOwner_name("test");
		return new Object[][] {{rmsSessionData.get("accessToken"),jsonHelper.getObjectToJSON(fssaiLicenseUpdateObject)}};
	}
	
	
	

}
