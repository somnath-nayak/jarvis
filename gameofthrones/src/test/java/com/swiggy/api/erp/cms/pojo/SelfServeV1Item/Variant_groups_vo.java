package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Variant_groups_vo {
	 private String[] delete_variant_by_ids;

	    private Variants_vo[] variants_vo;

	    private String delete_by_variant_id_error_map;

	    private Variant_group variant_group;

	    public String[] getDelete_variant_by_ids ()
	    {
	        return delete_variant_by_ids;
	    }

	    public void setDelete_variant_by_ids (String[] delete_variant_by_ids)
	    {
	        this.delete_variant_by_ids = delete_variant_by_ids;
	    }

	    public Variants_vo[] getVariants_vo ()
	    {
	        return variants_vo;
	    }

	    public void setVariants_vo (Variants_vo[] variants_vo)
	    {
	        this.variants_vo = variants_vo;
	    }

	    public String getDelete_by_variant_id_error_map ()
	    {
	        return delete_by_variant_id_error_map;
	    }

	    public void setDelete_by_variant_id_error_map (String delete_by_variant_id_error_map)
	    {
	        this.delete_by_variant_id_error_map = delete_by_variant_id_error_map;
	    }

	    public Variant_group getVariant_group ()
	    {
	        return variant_group;
	    }

	    public void setVariant_group (Variant_group variant_group)
	    {
	        this.variant_group = variant_group;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [delete_variant_by_ids = "+delete_variant_by_ids+", variants_vo = "+variants_vo+", delete_by_variant_id_error_map = "+delete_by_variant_id_error_map+", variant_group = "+variant_group+"]";
	    }
	}
				
				