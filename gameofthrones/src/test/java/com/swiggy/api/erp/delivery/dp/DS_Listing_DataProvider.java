package com.swiggy.api.erp.delivery.dp;

import java.io.IOException;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DSListingConstant;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.DSListingHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;

public class DS_Listing_DataProvider {
	
	static DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	DSListingHelper dellist=new DSListingHelper();
	@DataProvider(name = "DS_1")
	public Object[][] DS1() throws IOException, InterruptedException {
		delmeth.dbhelperupdate("Update zone set is_open=0 where id=2");
		delmeth.redisconnectsetboolean(DSListingConstant.is_zone_serviveable, DSListingConstant.DS_zone,
				DeliveryConstant.redisdb,true);
		Thread.sleep(10000);
		return new Object[][] { {} };
	}
	@DataProvider(name = "DS_2")
	public Object[][] DS2() throws IOException, InterruptedException {
        delmeth.dbhelperupdate("Update zone set is_open=1 where id=2");
        delmeth.redisconnectsetboolean(DSListingConstant.is_zone_serviveable, DSListingConstant.DS_zone,
				DeliveryConstant.redisdb,false);
        Thread.sleep(10000);
	return new Object[][] { {} };
	}
	@DataProvider(name = "DS_3")
	public Object[][] DS3() throws IOException, InterruptedException {
		 delmeth.dbhelperupdate("Update zone set is_open=1 where id=3");
		 Thread.sleep(10000);
		 return new Object[][] { {} };
	}
	
	@DataProvider(name = "DS_4")
	public Object[][] DS4() throws IOException, InterruptedException {
		delmeth.dbhelperupdate("Update zone set is_open=1 where id=3");
		Thread.sleep(10000);
		return new Object[][] { {} };
	}
	@DataProvider(name = "DS_5")
	public Object[][] DS5() throws IOException, InterruptedException {
		 delmeth.redisconnectsetboolean(DSListingConstant.is_zone_serviveable, DSListingConstant.DS_zone,
					DeliveryConstant.redisdb,true);
		 Thread.sleep(10000);
		//delmeth.redisconnectsetdoubleStrinvalue(DSListingConstant.DS_avgA2D_key, DSListingConstant.DS_zone,0, DSListingConstant.DS_set_lessA2D,"dsredis");
		return new Object[][] { {} };
	}
	@DataProvider(name = "DS_6")
	public Object[][] DS6() throws IOException, InterruptedException {
		delmeth.redisconnectsetboolean(DSListingConstant.is_zone_serviveable, DSListingConstant.DS_zone,
				DeliveryConstant.redisdb,false);
		 Thread.sleep(10000);
		//delmeth.redisconnectsetdoubleStrinvalue(DSListingConstant.DS_avgA2D_key, DSListingConstant.DS_zone,0, DSListingConstant.DS_set_moreA2D,"dsredis");
		return new Object[][] { {} };
	}
	@DataProvider(name = "DS_7")
	public Object[][] DS7() throws IOException, InterruptedException {
		delmeth.redisconnectsetdouble(DeliveryConstant.bannerrediskey, DSListingConstant.DS_zone,
				DeliveryConstant.redisdb, 1.0,"deliveryredis");
		delmeth.redisconnectsetdoubleStrinvalue(DSListingConstant.DS_avgA2D_key, DSListingConstant.DS_zone,0, DSListingConstant.DS_set_moreA2D,"dsredis");
		dellist.maxOrderVolumeResolve();
		dellist.maxOrderCapacityResolve();
		return new Object[][] { {} };
	}
	@DataProvider(name = "DS_8")
	public Object[][] DS8() throws IOException, InterruptedException {
		delmeth.redisconnectsetdoubleStrinvalue(DSListingConstant.DS_avgA2D_key, DSListingConstant.DS_zone,0, DSListingConstant.DS_set_moreA2D,"dsredis");
		dellist.maxOrderVolumeResolve();
		dellist.maxOrderCapacityResolve();
		return new Object[][] { {} };
	}
	@DataProvider(name = "DS_20")
	public Object[][] DS20() throws IOException, InterruptedException {
		delmeth.redisconnectsetdouble(DeliveryConstant.bannerrediskey, DSListingConstant.DS_zone_2,
				DeliveryConstant.redisdb, 1234.0,"deliveryredis");
		Thread.sleep(70000);
		return new Object[][] { {} };
	}
	

}
