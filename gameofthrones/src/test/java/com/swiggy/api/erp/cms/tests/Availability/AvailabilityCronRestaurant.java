package com.swiggy.api.erp.cms.tests.Availability;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.annotations.Test;

public class AvailabilityCronRestaurant {
    CMSHelper cmsHelper=new CMSHelper();
    @Test(description = "sends restaurant availability events to snd kafka")
    public void restaurantAvailability(){
        cmsHelper.availabilityRestaurant();
    }
}
