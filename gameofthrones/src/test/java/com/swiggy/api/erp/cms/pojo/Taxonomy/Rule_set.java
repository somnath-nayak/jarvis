
package com.swiggy.api.erp.cms.pojo.Taxonomy;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "rules"
})
public class Rule_set {

    @JsonProperty("rules")
    private List<Rule> rules = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Rule_set() {
        setDefaultData();
    }

    /**
     * 
     * @param rules
     */
    public Rule_set(List<Rule> rules) {
        super();
        this.rules = rules;
    }

    @JsonProperty("rules")
    public List<Rule> getRules() {
        return rules;
    }

    @JsonProperty("rules")
    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    public Rule_set withRules(List<Rule> rules) {
        this.rules = rules;
        return this;
    }
    public Rule_set setDefaultData(){
        this.withRules(new ArrayList<>(Arrays.asList(new Rule())));
        return this;
    }


}
