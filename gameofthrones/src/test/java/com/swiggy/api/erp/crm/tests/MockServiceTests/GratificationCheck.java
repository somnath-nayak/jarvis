package com.swiggy.api.erp.crm.tests.MockServiceTests;

        import com.swiggy.api.erp.crm.constants.CRMConstants;
        import com.swiggy.api.erp.crm.dp.foodissues.GratificationDP;
        import com.swiggy.api.erp.crm.foodissues.pojo.CCResolution.CCResolutionPOJO;
        import framework.gameofthrones.Aegon.Initialize;
        import framework.gameofthrones.JonSnow.GameOfThronesService;
        import framework.gameofthrones.JonSnow.Processor;
        import framework.gameofthrones.Tyrion.WireMockHelper;
        import com.swiggy.api.erp.crm.helper.MockServicesHelper;
        import com.swiggy.api.erp.crm.constants.MockServiceConstants;
        import org.testng.Assert;
        import org.testng.annotations.AfterClass;
        import org.testng.annotations.BeforeClass;
        import org.testng.annotations.Test;

        import java.io.IOException;
        import java.text.DateFormat;
        import java.text.SimpleDateFormat;
        import java.util.Date;
        import java.util.HashMap;

public class GratificationCheck {

    Initialize gameofthrones = new Initialize();
    GratificationDP gratificationDP = new GratificationDP();
    CCResolutionPOJO po = new CCResolutionPOJO();
    WireMockHelper wireMockHelper = new WireMockHelper();
    MockServicesHelper mockServicesHelper = new MockServicesHelper();
    MockServiceConstants mockServiceConstants = new MockServiceConstants();


    public int cloneCheck(String orderId, String itemId, String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService cloneTest = new GameOfThronesService("cconeview", "cloneCheck", gameofthrones);
        String[] payload = new String[]{orderId, restId, itemId};

        Processor cloneTestResponse = new Processor(cloneTest, requestheaders, payload);
        int res = cloneTestResponse.ResponseValidator.GetNodeValueAsInt("statusCode");
        return res;

    }

    @BeforeClass
    public void startMockServer() {
        wireMockHelper.startMockServer(9005);
    }

    @AfterClass
    public void stopMockServer() {
        wireMockHelper.stopMockServer();
    }

    @Test(enabled = true, dataProviderClass = GratificationDP.class, dataProvider = "refundRequest", description = "Gratification - Replicate order")
    public void gratificationReplicate(String Payload,String orderId,String restId,String itemId,String cost,String orderType,String order_status,String restaurant_Type) throws InterruptedException, IOException {
        String userId= "2143";

        String valueSegment = "H";
        String igcc_segment = "L";
        //Generate a new order id for replicated order

        String newOrderId=mockServiceConstants.getUniqueOrderId();

        //Mock the dependent services

        //Following it to mock order details
        mockServicesHelper.stubOrderDetailAPIResponse(orderId,restId,itemId,cost,orderType,order_status,restaurant_Type);

        // Order details call to checkout
        mockServicesHelper.stubOrderDetailAPIResponse2(orderId,restId,itemId,cost,orderType,order_status,restaurant_Type);


        //Following is to mock clone check api
        mockServicesHelper.stubCloneCheckAPI(orderId,restId,itemId,cost,orderType);

        //Follwing is to mock order confirm api
        mockServicesHelper.stubCloneConfirmAPI(newOrderId,restId,itemId,cost,orderType,orderId);

        //Following is to mock user segment api
        mockServicesHelper.stubGetUserSegmentApiResponse(userId, valueSegment, igcc_segment);


        //First check the order can be replicated, clone check api
        int clonePossible = cloneCheck(orderId, itemId, restId);
        System.out.println(clonePossible);
        Assert.assertEquals(clonePossible,0);

        //Payload for the replicate resolution api
        String[] payload=new String[]{Payload};

        //Headers for resolution api
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("initiator-id", "212");
        requestheaders.put("initiator-type", "AGENT");

        //Replicate the order
        GameOfThronesService replicateOrder = new GameOfThronesService("cconeview", "gratification", gameofthrones);

        Processor replicateOrderResponse = new Processor(replicateOrder, requestheaders, payload);
        System.out.println("Replicate order response  " + replicateOrderResponse);

        //Validate the status, isResolved Flag and baseOrderId
        Assert.assertEquals(replicateOrderResponse.ResponseValidator.GetNodeValue("status"), "SUCCESS");
        Assert.assertEquals(replicateOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..isResolved").replace("[", "").replace("]", ""), "true");
        Assert.assertEquals(replicateOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..type").replace("[", "").replace("]", ""), "\"REPLICATE_ORDER\"");

        String baseOrderId = replicateOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..data.baseOrderId").replace("[", "").replace("]", "");
        Assert.assertEquals(baseOrderId, orderId);

    }


}
