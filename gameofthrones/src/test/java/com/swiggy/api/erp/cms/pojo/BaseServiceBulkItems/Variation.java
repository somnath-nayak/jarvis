package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "default",
        "externalVariantId",
        "id",
        "inStock",
        "isVeg",
        "name",
        "order",
        "price",
        "variantGroupId"
})
public class Variation {

    @JsonProperty("default")
    private Integer _default;
    @JsonProperty("externalVariantId")
    private String externalVariantId;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("inStock")
    private Integer inStock;
    @JsonProperty("isVeg")
    private Integer isVeg;
    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("variantGroupId")
    private Integer variantGroupId;

    @JsonProperty("default")
    public Integer getDefault() {
        return _default;
    }

    @JsonProperty("default")
    public void setDefault(Integer _default) {
        this._default = _default;
    }

    @JsonProperty("externalVariantId")
    public String getExternalVariantId() {
        return externalVariantId;
    }

    @JsonProperty("externalVariantId")
    public void setExternalVariantId(String externalVariantId) {
        this.externalVariantId = externalVariantId;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("inStock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("inStock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("isVeg")
    public Integer getIsVeg() {
        return isVeg;
    }

    @JsonProperty("isVeg")
    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("variantGroupId")
    public Integer getVariantGroupId() {
        return variantGroupId;
    }

    @JsonProperty("variantGroupId")
    public void setVariantGroupId(Integer variantGroupId) {
        this.variantGroupId = variantGroupId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("_default", _default).append("externalVariantId", externalVariantId).append("id", id).append("inStock", inStock).append("isVeg", isVeg).append("name", name).append("order", order).append("price", price).append("variantGroupId", variantGroupId).toString();
    }

}