package com.swiggy.api.erp.vms.helper;

import java.util.HashMap;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

public class FssaiServicesHelper {

	Initialize initializer = Initializer.getInitializer();
	
	public Processor getFssaiAlert(String accessToken)
	{
		GameOfThronesService services=new GameOfThronesService("rms", "getfssaialert", initializer);
		HashMap<String, String> requestHeaders=new HashMap<String, String>();
		requestHeaders.put("accessToken", accessToken);
		requestHeaders.put("Content-Type","application/json");
		Processor processor=new Processor(services,requestHeaders);
		return processor;
	}
	
	
	public Processor getFssaiInfo(String accessToken,String restaurantId)
	{
		GameOfThronesService services=new GameOfThronesService("rms", "getfssaiinfo", initializer);
		HashMap<String, String> requestHeaders=new HashMap<String, String>();
		requestHeaders.put("accessToken", accessToken);
		requestHeaders.put("Content-Type","application/json");
		Processor processor=new Processor(services,requestHeaders,null,new String[] {restaurantId});
		return processor;
	}
	
	public Processor updateFssaiLicense(String accessToken,String payload)
	{
		GameOfThronesService services=new GameOfThronesService("rms", "fssaiupdatelicense", initializer);
		HashMap<String, String> requestHeaders=new HashMap<String, String>();
		requestHeaders.put("accessToken", accessToken);
		requestHeaders.put("Content-Type","application/json");
		Processor processor=new Processor(services,requestHeaders,new String[] {payload});
		return processor;
	}
	
	
	public Processor fssaiackUpdate(String accessToken,String payload)
	{
		GameOfThronesService services=new GameOfThronesService("rms", "fssaiacknowledgeupdate", initializer);
		HashMap<String, String> requestHeaders=new HashMap<String, String>();
		requestHeaders.put("accessToken", accessToken);
		requestHeaders.put("Content-Type","application/json");
		Processor processor=new Processor(services,requestHeaders,new String[] {payload});
		return processor;
	}
	
	
	public Processor fssaiCompliance(String accessToken,String restaurantId)
	{
		GameOfThronesService services=new GameOfThronesService("rms", "fssaicompliance", initializer);
		HashMap<String, String> requestHeaders=new HashMap<String, String>();
		requestHeaders.put("accessToken", accessToken);
		requestHeaders.put("Content-Type","application/json");
		Processor processor=new Processor(services,requestHeaders,new String[] {restaurantId});
		return processor;
	}
	
	
}
