package com.swiggy.api.erp.finance.helper;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.finance.constants.FinConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class finHelper {

    Initialize gameofthrones = new Initialize();
    SchemaValidatorUtils schema = new SchemaValidatorUtils();
    ZoneHelper zoneHelper = new ZoneHelper();
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();

    public Processor getReconOrder(String orderId)
    {
        GameOfThronesService service = new GameOfThronesService("fin","getReconOrder",gameofthrones);
        HashMap<String,String> headers = new HashMap<String, String>();
        String[] params = {orderId};
        headers.put("Content-Type","application/json");
        Processor p = new Processor(service,headers,null,params);
        return p;
    }

    public boolean validateUndeliveredRecon(Processor processor) {
        return processor.ResponseValidator.GetNodeValue("recon_status") == null;
    }

    public String getReconStatus(Processor processor) {
        String str;
        try {
            str =  (processor.ResponseValidator.GetNodeValue(("recon_status")) != null) ? processor.ResponseValidator.GetNodeValue(("recon_status.status")) : null;
        } catch(ClassCastException e) {
            str = processor.ResponseValidator.GetNodeValue(("recon_status.status"));
        }
        return str;
    }

    public boolean validateSchema(Processor processor, String schema_path) {
        boolean valid = false;
        try {
            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + schema_path);
            List<String> missingNodeList = schema.validateServiceSchema(jsonschema, processor.ResponseValidator.GetBodyAsText());
            valid = missingNodeList.isEmpty();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return valid;
    }

    public Processor cancelRecon(String order_id) {
        GameOfThronesService service = new GameOfThronesService("fin","cancelReconOrder",gameofthrones);
        HashMap<String,String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        return new Processor(service, headers, new String[]{order_id});
    }

    public Processor submitRecon(String[] payloadparams) {
        GameOfThronesService service = new GameOfThronesService("fin","submitReconOrder",gameofthrones);
        HashMap<String,String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        return new Processor(service, headers, payloadparams);
    }

    public void waitForReconStatus(String orderId, String status) {
        Processor processor;
        try {
            for(int i=0;i<20;i++) {
                processor = getReconOrder(orderId);
                if(getReconStatus(processor) != null) {
                    if(getReconStatus(processor).equalsIgnoreCase(status))
                    break;
                }
                Thread.sleep(5000);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> getDefaultHeaders() {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        return header;
    }

    public Long getOldMouRestaurants() {
        DBHelper dbHelper = new DBHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinConstants.finance_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(FinConstants.mouquery+ FinConstants.oldmou);
        return (list.size() > 0)? (Long) (list.get(0).get(FinConstants.restid)) : 0;
    }

    public Long getNewMouRestaurants() {
        DBHelper dbHelper = new DBHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinConstants.finance_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(FinConstants.mouquery+ FinConstants.newmou);
        return (list.size() > 0)? (Long) (list.get(0).get(FinConstants.restid)) : 0;
    }

    public String createMouOrder(String restaurant_id) {
        String orderid = "";
        try {
            LOSHelper losHelper = new LOSHelper();
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            String order_id = order_idFormat.format(date);
            String order_time = dateFormat.format(date);
            String epochTime = String.valueOf(Instant.now().getEpochSecond());
            orderid = losHelper.createMouOrder(order_id, order_time, epochTime, restaurant_id);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return orderid;
    }

    public Processor getCurrentPayout(String from, String to, String userId, String cityID) {
        GameOfThronesService service = new GameOfThronesService("fin","currentpayout",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{from, to, userId, cityID});
    }

    public Processor generateMasterFile(String file_url, String userId) {
        GameOfThronesService service = new GameOfThronesService("fin","generatemasterfile",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{file_url, userId});
    }

    public Processor getRestaurantPendingPayout(String from, String to, String excluded) {
        GameOfThronesService service = new GameOfThronesService("fin2","restaurantpendingpayout",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{from, to, excluded});
    }

    public Processor getRestaurantReleasePayout(String from, String to, String user_id, String restList) {
        GameOfThronesService service = new GameOfThronesService("fin2","restaurantreleasepayout",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{from, to, user_id, restList});
    }

    public Processor restaurantPendingApproval(String pageno) {
        GameOfThronesService service = new GameOfThronesService("fin2","restaurantpendingapproval",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{pageno});
    }

    public Processor restaurantPendingApprovalRestid(String pageno, String restId) {
        GameOfThronesService service = new GameOfThronesService("fin2","restaurantpendingapprovalrestid",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{pageno}, new String[]{restId});
    }

    public Processor getSwiggyPendingPayout(String from, String to, String cityId, String areaList, String excluded) {
        GameOfThronesService service = new GameOfThronesService("fin2","swiggypendingpayout",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{from, to, cityId, areaList, excluded});
    }

    public Processor getSwiggyReleasePayout(String from, String to, String user_id, String restList) {
        GameOfThronesService service = new GameOfThronesService("fin2","swiggyreleasepayout",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{from, to, user_id, restList});
    }

    public Processor getReconConfig(String key, String value) {
        GameOfThronesService service = new GameOfThronesService("fin","reconconfig",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{key, value});
    }

    public Processor getReconAutoAssignment(String userId) {
        GameOfThronesService service = new GameOfThronesService("fin","reconautoassignment",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{userId});
    }

    public Processor getRestaurantNodalPayments(String cityId, String from, String to) {
        GameOfThronesService service = new GameOfThronesService("fin","restaurantnodalpayouts",gameofthrones);
        return new Processor(service, getDefaultHeaders(), null, new String[]{cityId, from, to});
    }

    public Processor createReports(String from, String to, String userId, String type) {
        GameOfThronesService service = new GameOfThronesService("fin2","createreports",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{from, to, userId, type});
    }

    public Processor swiggyPendingApproval(String pageno) {
        GameOfThronesService service = new GameOfThronesService("fin2","swiggypendingapproval",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{pageno});
    }

    public Processor getReconHealthCheck() {
        GameOfThronesService service = new GameOfThronesService("fin","reconhealthcheck",gameofthrones);
        return new Processor(service, getDefaultHeaders(),null);
    }

    public Processor getReconVersion() {
        GameOfThronesService service = new GameOfThronesService("fin","reconversion",gameofthrones);
        return new Processor(service, getDefaultHeaders(),null);
    }

    public Processor specialistLogout(String file_url, String user_id) {
        GameOfThronesService service = new GameOfThronesService("fin","specialistlogout",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{file_url, user_id});
    }

    public Processor payoutRestaurantApprove(String user_id, String payout_list, String secret_key, String request_id) {
        GameOfThronesService service = new GameOfThronesService("fin","restaurantapprove",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{user_id, payout_list, secret_key, request_id});
    }

    public Processor payoutSwiggyReject(String user_id, String payout_list, String secret_key, String request_id) {
        GameOfThronesService service = new GameOfThronesService("fin","swiggyreject",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{user_id, payout_list, secret_key, request_id});
    }

    public Processor addQuickRecon(String file_url, String user_id) {
        GameOfThronesService service = new GameOfThronesService("fin","addquickrecon",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{file_url, user_id});
    }

    public Processor reconOrdersAutoAssign(String user_id) {
        GameOfThronesService service = new GameOfThronesService("fin","reconautoassign",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{user_id});
    }

    public Processor fileUpload(String user_id, String file_url, String type) {
        GameOfThronesService service = new GameOfThronesService("fin","reconuploadfile",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{user_id, file_url, type});
    }

    public Processor fileUpload(String file_url, String user_id) {
        GameOfThronesService service = new GameOfThronesService("fin","manualinvoice",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{file_url, user_id});
    }

    public Processor invoiceAddAnexure(String file_url, String user_id) {
        GameOfThronesService service = new GameOfThronesService("fin","invoiceaddanexure",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{file_url, user_id});
    }

    public Processor invoiceCancel(String taskid, String user_id) {
        GameOfThronesService service = new GameOfThronesService("fin","invoicecancel",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{taskid, user_id});
    }

    public Processor invoiceEmail(String task_id, String email) {
        GameOfThronesService service = new GameOfThronesService("fin","invoiceemail",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{task_id, email});
    }

    public Processor manualInvoiceDownload(String task_id, String email) {
        GameOfThronesService service = new GameOfThronesService("fin","manualinvoicedownload",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{task_id, email});
    }

    public Processor invoiceJob(String from, String to, String user_id,String payout_id,String city_id) {
        GameOfThronesService service = new GameOfThronesService("fin","createinvoicejob",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{from, to, user_id,payout_id,city_id});
    }

    public boolean isInvoiceJobPresent(String to_date) {
        DBHelper dbHelper = new DBHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinConstants.finance_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(FinConstants.invoice_job_check+to_date+ FinConstants.endstring);
        return list.size() >= 1;
    }

    public int getManualInvoiceCount() {
        DBHelper dbHelper = new DBHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinConstants.finance_db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(FinConstants.countmanualjob);
        return (int)list.get(0).get(0);
    }

    public Processor nodalpayout(String cityId, String from, String to) {
        GameOfThronesService service = new GameOfThronesService("fin2","getnodalpayput",gameofthrones);
        return new Processor(service, getDefaultHeaders(), null, new String[]{cityId, from, to});
    }

    public HashMap<String, String> createZoneAndDE(){
        String zone_id = zoneHelper.createZone();
      //  String de_id = deliveryDataHelper.CreateDE(Integer.parseInt(zone_id));
        String de_id = deliveryDataHelper.CreateDE(Integer.parseInt("1"));
        HashMap<String, String> map = new HashMap<>();
        map.put("zone_id","1");
        map.put("de_id", de_id);
        return map;
    }
}
