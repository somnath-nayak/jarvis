package com.swiggy.api.erp.cms.tests.Restaurant;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.RestaurantDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateRestaurantSlots extends RestaurantDP {
    CMSHelper cmsHelper= new CMSHelper();

    @Test(dataProvider = "restslotscreate",description = "Create the restaurant slots and push to kafka")
    public void createrestSlots(String day,String restaurant_id, String open_time, String close_time){
        String response=cmsHelper.restaurantSlotsCreate(day,restaurant_id,open_time,close_time).ResponseValidator.GetBodyAsText();
        int status=JsonPath.read(response,"$.status");
        Assert.assertEquals(status,1);
    }

}
