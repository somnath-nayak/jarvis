package com.swiggy.api.erp.delivery.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.constants.Serviceability_Constant;
import com.swiggy.api.erp.delivery.dp.CerebroEnhancementsDataProvider;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.ServiceablilityHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import org.apache.http.util.Asserts;
import org.apache.solr.common.SolrDocumentList;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.apache.noggit.JSONUtil;
import com.jayway.jsonpath.PathNotFoundException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CerebroEnhancementsTest extends CerebroEnhancementsDataProvider {
    DeliveryHelperMethods deliveryHelperMethods = new DeliveryHelperMethods();
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    ServiceablilityHelper serviceablilityHelper = new ServiceablilityHelper();
    Initialize initialize = new Initialize();

    /*@BeforeTest
    public void makeServiceable(){
        helper.makeServiceable(Serviceability_Constant.restaurant_id);
    }*/

    @Test(dataProvider = "DP", description = "Verify that \"customer_zone_id\" is reflecting in listing response based on customer lat lng")
    public void CustomerInfoTest(String lat, String lng, String city_id) {
        String solrUrl = deliveryHelperMethods.getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl, "user_zone", "polygon:\"Intersects(" + lat + " " + lng + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        if (!jsonDoc.equals("[]")){
            String zone_id = JsonPath.read(jsonDoc, "$[*].id").toString().replace("[\"", "").replace("\"]", "");
            Processor processor = serviceablilityHelper.cerebroListing(lat, lng, city_id);
            String response = processor.ResponseValidator.GetBodyAsText();
            String customer_zone_id = JsonPath.read(response, "$.customerInfo.customer_zone_id").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(customer_zone_id, zone_id);
        }
        else {
            System.out.println("********** Customer lat lng is not lying under zone polygon **********");
            Assert.fail();

        }
    }

    @Test(dataProvider = "DP", description = "Verify that \"customer_zone_raining\" is reflecting as false in listing if rain_mode is off for the particular zone based on customer lat lng")
    public void CustomerInfoTest1(String lat, String lng, String city_id) throws Exception {
        String solrUrl = deliveryHelperMethods.getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl, "user_zone", "polygon:\"Intersects(" + lat + " " + lng + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        if (!jsonDoc.equals("[]")) {
            String zone_id = JsonPath.read(jsonDoc, "$[*].id").toString().replace("[\"", "").replace("\"]", "");
            SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET rain_mode_type=3,rain_mode=0 WHERE id=" + zone_id);
            Thread.sleep(5000);
            Processor processor = serviceablilityHelper.cerebroListing(lat, lng, city_id);
            String response = processor.ResponseValidator.GetBodyAsText();
            String customer_zone_raining = JsonPath.read(response, "$.customerInfo.customer_zone_raining").toString().replace("[", "").replace("]", "");
            Assert.assertFalse(false, customer_zone_raining);
        }
        else {
            System.out.println("********** Customer lat lng is not lying under zone polygon **********");
            Assert.fail();
        }
    }

    @Test(dataProvider = "DP", description = "Verify that \"customer_zone_raining\" is reflecting as true in listing if rain_mode is on for the particular zone based on customer lat lng")
    public void CustomerInfoTest2(String lat, String lng, String city_id) throws Exception {
        String solrUrl = deliveryHelperMethods.getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl, "user_zone", "polygon:\"Intersects(" + lat + " " + lng + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        if (!jsonDoc.equals("[]")) {
            String zone_id = JsonPath.read(jsonDoc, "$[*].id").toString().replace("[\"", "").replace("\"]", "");
            SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET rain_mode_type=1,rain_mode=1 WHERE id=" + zone_id);
            Thread.sleep(5000);
            Processor processor = serviceablilityHelper.cerebroListing(lat, lng, city_id);
            String response = processor.ResponseValidator.GetBodyAsText();
            String customer_zone_raining = JsonPath.read(response, "$.customerInfo.customer_zone_raining").toString().replace("[", "").replace("]", "");
            Assert.assertTrue(true, customer_zone_raining);
        }
        else {
            System.out.println("********** Customer lat lng is not lying under zone polygon **********");
            Assert.fail();
        }
    }

    @Test(dataProvider = "DP1", description = "Verify that \"customer_zone_raining\": false if it's raining in restaurant zone not in cutomer zone ")
    public void CustomerInfoTest3(String lat, String lng, String restaurant_id) throws Exception {
        String solrUrl = deliveryHelperMethods.getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl, "user_zone", "polygon:\"Intersects(" + lat + " " + lng + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        if (!jsonDoc.equals("[]")) {
            String customer_zone_id = JsonPath.read(jsonDoc, "$[*].id").toString().replace("[\"", "").replace("\"]", "");
            SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET rain_mode_type=3,rain_mode=0 WHERE id=" + customer_zone_id);
            Thread.sleep(3000);
            Map<String, Object> restMap = SystemConfigProvider.getTemplate("deliverydb").queryForMap("select a.id as area_id,a.city_id,a.zone_id from restaurant r inner join area a on a.id=r.area_code where r.id=" + restaurant_id);
            String restaurant_zone_id;
            if (restMap != null) {
                restaurant_zone_id = restMap.get("zone_id").toString();
            } else {
                System.out.println("********** Restaurant Not Found **********");
                return;
            }
            SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET rain_mode_type=1,rain_mode=1 WHERE id=" + restaurant_zone_id);
            Thread.sleep(3000);
            Processor processor = serviceablilityHelper.cerebroListingWithRestId(lat, lng, restaurant_id);
            String response = processor.ResponseValidator.GetBodyAsText();
            String customer_zone_raining = JsonPath.read(response, "$.customerInfo.customer_zone_raining").toString().replace("[", "").replace("]", "");
            String restaurant_zone_raining = JsonPath.read(response, "$..listingServiceabilityResponse.rain_mode").toString().replace("[", "").replace("]", "");
            if (customer_zone_raining.equals("false") && !restaurant_zone_raining.equals("0")) {
                Assert.assertTrue(true);
            } else {
                Assert.assertFalse(false);
            }
        }
        else {
            System.out.println("********** Customer lat lng is not lying under zone polygon **********");
            Assert.fail();
        }
    }

    @Test(dataProvider = "DP2", description = "Verify that \"customerInfo\" is not reflecting in listing response if customer lat lng is not under zone polygon")
    public void CustomerInfoTest4(String lat, String lng, String city_id) throws Exception {
        String solrUrl = deliveryHelperMethods.getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl, "user_zone", "polygon:\"Intersects(" + lat + " " + lng + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        if (jsonDoc.equals("[]")) {
            Processor processor = serviceablilityHelper.cerebroListing(lat, lng, city_id);
            String response = processor.ResponseValidator.GetBodyAsText();
            try {
                String customer_info = JsonPath.read(response, "$.customerInfo").toString();
            } catch (PathNotFoundException e) {
                Assert.assertTrue(true);
            }
        } else {
            System.out.println("********** Customer lat lng is lying under zone polygon **********");
            Assert.fail();
        }
    }
}


