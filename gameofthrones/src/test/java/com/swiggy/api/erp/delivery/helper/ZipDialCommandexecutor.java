package com.swiggy.api.erp.delivery.helper;

import framework.gameofthrones.JonSnow.Processor;

interface zip
{
    Processor executeZip(String order_id);
}

class zipConfirm implements zip{

    Processor processor;
    @Override
    public Processor executeZip(String order_id) {
       processor=  new DeliveryServiceHelper().zipDialConfirmDE(order_id);
       return processor;
    }
}
class zipArrived implements zip{
    Processor processor;
    @Override
    public Processor executeZip(String order_id) {
        processor=  new DeliveryServiceHelper().zipDialArrivedDE(order_id);
        return processor;
    }

}
class zipPickedUp implements zip{
    Processor processor;
    @Override
    public Processor executeZip(String order_id) {
        processor=  new DeliveryServiceHelper().zipDialPickedUpDE(order_id);
        return processor;
    }


}
class zipReached implements zip{
    Processor processor;
    @Override
    public Processor executeZip(String order_id) {
        processor=  new DeliveryServiceHelper().zipDialReachedDE(order_id);
        return processor;
    }


}
class zipDelivered implements zip{
    Processor processor;
    @Override
    public Processor executeZip(String order_id) {
        processor=  new DeliveryServiceHelper().zipDialDeliveredDE(order_id);
        return processor;
    }

}


public class ZipDialCommandexecutor {

    public Processor invokeOperation(zip zipObject, String order_id)
    {
        return zipObject.executeZip(order_id);
    }

}


