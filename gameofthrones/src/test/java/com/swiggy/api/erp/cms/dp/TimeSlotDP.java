package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.pojo.*;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.AddItemToRestaurant;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.RestBuilder;
import com.swiggy.api.erp.cms.pojo.ItemHolidaySlot.*;
import com.swiggy.api.erp.cms.pojo.ItemHolidaySlot.Data;
import com.swiggy.api.erp.cms.pojo.ItemHolidaySlot.Meta;
import com.swiggy.api.erp.cms.pojo.ItemHolidaySlot.UserMeta;
import com.swiggy.api.erp.cms.pojo.ItemData;

import framework.gameofthrones.Tyrion.JsonHelper;
import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TimeSlotDP {

    @DataProvider(name = "addTimeSlot")
    public Object[][] addTimeSlot() {
        return new Object[][]{{"281","1200","1230", getCurrentDay()},
                {"281","930","1230", getFutureDay()},
                {"281","2350","2359", getCurrentDay()},
                {"281","0000","0010", getCurrentDay()},
                {"281","2350","2355", getCurrentDay()},
                {"281","2358","2360", getCurrentDay()}};
    }

    @DataProvider(name = "addNegativeTimeSlot")
    public Object[][] addNegativeTimeSlot() {
        return new Object[][]{{"273","","1230", getCurrentDay()},
                {"273","","1230", getFutureDay()},
                {"273","2350","", getCurrentDay()},
                {"","0000","0010", getCurrentDay()}};
    }

    public String getFutureDay()
    {
        Calendar thatDay = Calendar.getInstance();
        thatDay.add(Calendar.DAY_OF_WEEK, 1);
        String t= new SimpleDateFormat("EE", Locale.ENGLISH).format(thatDay.getTime()).toUpperCase();
        return t;
    }

    public String getCurrentDay()
    {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String t= new SimpleDateFormat("EE", Locale.ENGLISH).format(date.getTime()).toUpperCase();
        return t;
    }

    @DataProvider(name = "addTimeSlotNight")
    public Object[][] addTimeSlotNight() {
        return new Object[][]{ {"281","2350","2360", getFutureDay()},
                {"281","2350","2400", getFutureDay()},
                {"273","","1230", getCurrentDay()},
                {"273","","1230", getFutureDay()},
                {"273","2350","", getCurrentDay()},
                {"","0000","0010", getCurrentDay()}
        };
    }

    @DataProvider(name = "updateTimeSlot")
    public Object[][] updateTimeSlot() {
        return new Object[][]{ {"281","1230","1235", getCurrentDay(), "1240"},
                {"281","650","655", getFutureDay(), "700"}};
    }

    @DataProvider(name = "updateTimeSlots")
    public Object[][] updateTimeSlots() {
        return new Object[][]{ {"281","1230","1235", getCurrentDay(), getFutureDay(),"1240"},
                {"281","650","655", getFutureDay(), getFutureDay(),"700"}};
    }

    @DataProvider(name = "updateDaySlot")
    public Object[][] updateDaySlot() {
        return new Object[][]{ {"281","2350","2360", getCurrentDay(), "1240"},
                {"281","2355","2358", getFutureDay(), "0"},
                {"281","2350","2354", getFutureDay(), "1"}
        };
    }

    @DataProvider(name = "deleteSlot")
    public Object[][] deleteSlot() {
        return new Object[][]{{"281"}};
    }

    @DataProvider(name = "addHolidaySlot")
    public Object[][] addHolidaySlot() throws IOException {
        JsonHelper jsonHelper= new JsonHelper();
        HolidaySlot holidaySlot= new SlotBuilder()
                .restaurantId(273)
                .fromTime(dateTime())
                .toTime(extendedTime(5))
                .buildHolidaySlot();

        HolidaySlot holidaySlot1= new SlotBuilder()
                .restaurantId(273)
                .fromTime(dateTime())
                .toTime(extendedTime(60))
                .buildHolidaySlot();

        HolidaySlot holidaySlot2= new SlotBuilder()
                .restaurantId(273)
                .fromTime(extendedTime(70))
                .toTime(extendedTime(3600))
                .buildHolidaySlot();

        HolidaySlot holidaySlot3= new SlotBuilder()
                .restaurantId(273)
                .fromTime(date()+"23:55:00")
                .toTime(date()+"23:59:00")
                .buildHolidaySlot();

        HolidaySlot holidaySlot4= new SlotBuilder()
                .restaurantId(281)
                .fromTime(date()+"23:55:00")
                .toTime(date()+"23:59:59")
                .buildHolidaySlot();

        HolidaySlot holidaySlot5= new SlotBuilder()
                .restaurantId(281)
                .fromTime(date()+"00:00:00")
                .toTime(date()+"00:05:59")
                .buildHolidaySlot();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(holidaySlot)},
                {jsonHelper.getObjectToJSON(holidaySlot1)},
                {jsonHelper.getObjectToJSON(holidaySlot2)},
                {jsonHelper.getObjectToJSON(holidaySlot3)},
                {jsonHelper.getObjectToJSON(holidaySlot4)},
                {jsonHelper.getObjectToJSON(holidaySlot5)}};
    }

    @DataProvider(name = "updateHolidaySlot")
    public Object[][] updateHolidaySlot() throws IOException {
        JsonHelper jsonHelper= new JsonHelper();
        HolidaySlot holidaySlot= new SlotBuilder()
                .restaurantId(273)
                .fromTime(dateTime())
                .toTime(extendedTime(5))
                .buildHolidaySlot();

        HolidaySlot holidaySlot1= new SlotBuilder()
                .restaurantId(273)
                .fromTime(dateTime())
                .toTime(extendedTime(60))
                .buildHolidaySlot();

        HolidaySlot holidaySlot2= new SlotBuilder()
                .restaurantId(273)
                .fromTime(extendedTime(1))
                .toTime(extendedTime(3))
                .buildHolidaySlot();

        HolidaySlot holidaySlot3= new SlotBuilder()
                .restaurantId(273)
                .fromTime(extendedTime(70))
                .toTime(extendedTime(70))
                .buildHolidaySlot();

        HolidaySlot holidaySlot4= new SlotBuilder()
                .restaurantId(281)
                .fromTime(date()+"23:55:00")
                .toTime(date()+"23:59:59")
                .buildHolidaySlot();

        HolidaySlot holidaySlot5= new SlotBuilder()
                .restaurantId(281)
                .fromTime(futureDate()+"00:00:00")
                .toTime(futureDate()+"00:05:59")
                .buildHolidaySlot();

        HolidaySlot holidaySlot6= new SlotBuilder()
                .restaurantId(281)
                .fromTime(futureDate()+"11:00:00")
                .toTime(futureDate()+"11:05:59")
                .buildHolidaySlot();

        HolidaySlot holidaySlot7= new SlotBuilder()
                .restaurantId(281)
                .fromTime(futureDate()+"15:00:00")
                .toTime(futureDate()+"15:05:59")
                .buildHolidaySlot();

        return new Object[][]{
                {jsonHelper.getObjectToJSON(holidaySlot), jsonHelper.getObjectToJSON(holidaySlot1)},
                {jsonHelper.getObjectToJSON(holidaySlot2),jsonHelper.getObjectToJSON(holidaySlot3)},
                {jsonHelper.getObjectToJSON(holidaySlot4),jsonHelper.getObjectToJSON(holidaySlot5)},
                {jsonHelper.getObjectToJSON(holidaySlot6),jsonHelper.getObjectToJSON(holidaySlot7)}};
    }

    @DataProvider(name = "deleteHolidaySlot")
    public Object[][] deleteHolidaySlot() {
        return new Object[][]{{"281"},{"273"}};
    }

    /*@DataProvider(name = "addItemSlot")
    public Object[][] addItemSlot() throws IOException {
        JsonHelper jsonHelper= new JsonHelper();
        ItemSlot itemSlot= new ItemSlotBuilder()
                .data()

    }*/
    public String dateTime()
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        String formatTime = new SimpleDateFormat("HH:mm:ss").format(date);
        String DT=modifiedDate+"T"+formatTime;
        return DT;
    }


    public String extendedTime(int i)
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        LocalDateTime d = LocalDateTime.now();
        String dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss").format(d.plusMinutes(i));
        String DT=modifiedDate+"T"+dateFormat;
        return DT;
    }

    public String date()
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        return modifiedDate+"T";
    }

    public String futureDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        Date currentDatePlusOne = c.getTime();
        return dateFormat.format(currentDatePlusOne)+"T";
        //modifiedDate+"T";
    }

    public String currentTime()
    {
        Date date=new Date();
        String modifiedDate=new SimpleDateFormat("HHmm").format(date);
        return modifiedDate;
    }

    public String futureTime()
    {
        DateFormat dateFormat=new SimpleDateFormat("HHmm");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, 5);
        Date currentDatePlusOne = c.getTime();
        return dateFormat.format(currentDatePlusOne);
    }

    @DataProvider(name = "currentTimeSlot")
    public Object[][] currentTimeSlot() {
        return new Object[][]{{"281",currentTime(),futureTime(), getCurrentDay()}};
    }

    @DataProvider(name = "addAHolidaySlot")
    public Object[][] addAHolidaySlot() throws IOException {
        JsonHelper jsonHelper= new JsonHelper();
        HolidaySlot holidaySlot= new SlotBuilder()
                .restaurantId(273)
                .fromTime(dateTime())
                .toTime(extendedTime(5))
                .buildHolidaySlot();
        return new Object[][]{
                {jsonHelper.getObjectToJSON(holidaySlot)}};
    }

    @DataProvider(name = "addTime")
    public Object[][] addTime() throws IOException{
        JsonHelper jsonHelper= new JsonHelper();
        ItemHolidaySlot holidaySlot= new ItemHolidaySlotBuilder()
                .Data(new Data(222222,"23233333","332233"))
                .userMeta(new UserMeta("cms",new Meta("cms-tester")))
                .buildItemHolidaySlot();
        return new Object[][]{{jsonHelper.getObjectToJSON(holidaySlot)}};
    }

    @DataProvider(name = "addItemSlot")
    public Object[][] addItemSlot() throws IOException{
        return new Object[][]{{"6538122","1","930","1230"},
                {"6538122","0","930","1230"},
                {"6538122","8","1030","1200"},
                {"6538122","1","0","10"},
                {"6538122","1","2355","2360"},
                {"6538122","1","2360","10"},
                {"6538122","3","2359","10"}};
    }

    @DataProvider(name = "updateItemSlot")
    public Object[][] updateItemSlot() throws IOException{
        return new Object[][]{{"6538122","1","930","1230", "945"},
                {"6538122","0","930","1230", "1020"},
                {"6538122","8","1030","1200","1030"},
                {"6538122","1","0","10", "1200"},
                {"6538122","1","2355","2360", "2359"},
                {"6538122","1","2360","10", "0"},
                {"6538122","3","2359","10","1"}};
    }

    public String timeInEpochMilli(int t)
    {
        String time=String.valueOf(DateUtils.addMinutes(new Date(), t).toInstant().getEpochSecond() * 1000);
        return time;
    }

    @DataProvider(name = "addItemHoliSlot")
    public Object[][] addItemHolidaySlot() throws IOException{
        return new Object[][]{{"545197",timeInEpochMilli(0),timeInEpochMilli(5)},{"545197",timeInEpochMilli(0),timeInEpochMilli(5)},
                {"545197",nextMidNightEpoch(1),nextMidNightEpoch(5)},{"545197",nextMidNightEpoch(-1),nextMidNightEpoch(50)},
                {"545197",nextMidNightEpoch(0),nextMidNightEpoch(5)}, {"545197",nextMidNightEpoch(-2),nextMidNightEpoch(5)}
        };
    }

    @DataProvider(name = "updateItemHoliSlot")
    public Object[][] updateItemHolidaySlot() throws IOException{
        return new Object[][]{{"545197",timeInEpochMilli(0),timeInEpochMilli(5),timeInEpochMilli(3)},
                {"545197",timeInEpochMilli(0),timeInEpochMilli(5),timeInEpochMilli(7)},
                {"545197",timeInEpochMilli(0),timeInEpochMilli(5),timeInEpochMilli(-2)},
                {"545197",timeInEpochMilli(0),timeInEpochMilli(5),timeInEpochMilli(0)},
                {"545197",nextMidNightEpoch(0),nextMidNightEpoch(5),nextMidNightEpoch(-2)},
                {"545197",nextMidNightEpoch(-2),nextMidNightEpoch(5),nextMidNightEpoch(-3)}

        };
    }

    public String nextMidNightEpoch(int minutes)
    {
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT+05:30"));
        c.set(Calendar.HOUR,0);
        c.set(Calendar.MINUTE,minutes);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);
        c.add(Calendar.DAY_OF_MONTH, 1);
        long midnightUnixTimestamp = c.getTime().getTime();
        String epoc=Long.toString(midnightUnixTimestamp);
        return epoc;
    }


}






