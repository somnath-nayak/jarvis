package com.swiggy.api.erp.cms.dp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.testng.annotations.DataProvider;
import com.swiggy.api.erp.cms.catalogV2.pojos.CatalogV2PricingPojo;
import com.swiggy.api.erp.cms.catalogV2.pojos.SkuAttributes;
import com.swiggy.api.erp.cms.catalogV2.pojos.SkuPojo;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;

import framework.gameofthrones.Tyrion.JsonHelper;

public class CatalogV2SkuDP {
	CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
	String catId,spinId,storeId,skuId;

	@DataProvider(name = "CreateSKU")
	public Iterator<Object[]> CreateSKU() throws IOException, InterruptedException {
		List<Object[]> obj = new ArrayList<>();
		JsonHelper jsonHelper = new JsonHelper();
		SkuPojo skuPojo = new SkuPojo();
		CatalogV2PricingPojo p=new CatalogV2PricingPojo();
		SkuAttributes attritutes =new SkuAttributes();
		attritutes.build();
		skuPojo.setSkuAttributes(attritutes);
		p.build();
		skuPojo.setPricing(p);
		skuPojo.build();
		catId=skuPojo.getCategory_id();
		spinId=skuPojo.getSpin();
		storeId=skuPojo.getStore_id();
		skuId=skuPojo.getSku_id();
		obj.add(new Object[]{jsonHelper.getObjectToJSONallowNullValues(skuPojo),skuPojo.getSku_id(),true,null});

		//category null==>500 internal server
		skuPojo = new SkuPojo("S00011");
		p=new CatalogV2PricingPojo();
		p.build();
		attritutes=new SkuAttributes();
		attritutes.build();
		skuPojo.setSkuAttributes(attritutes);
		skuPojo.setPricing(p);
		skuPojo.build();
		skuPojo.setCategory_id(null);
		skuPojo.setSpin(spinId);
		obj.add(new Object[]{jsonHelper.getObjectToJSONallowNullValues(skuPojo),skuPojo.getSku_id(),false,null});


		//enabled false
		skuPojo = new SkuPojo("S001");
		p=new CatalogV2PricingPojo();
		p.build();
		attritutes=new SkuAttributes();;
		attritutes.build();
		skuPojo.setSkuAttributes(attritutes);
		skuPojo.setPricing(p);
		skuPojo.setEnabled(false);
		skuPojo.build();
		skuPojo.setCategory_id(catId);
		skuPojo.setSpin(spinId);
		obj.add(new Object[]{jsonHelper.getObjectToJSONallowNullValues(skuPojo),skuPojo.getSku_id(),true,null});

		//enabled true
		skuPojo = new SkuPojo("S002");
		p=new CatalogV2PricingPojo();
		p.build();
		attritutes=new SkuAttributes();;
		attritutes.build();
		skuPojo.setSkuAttributes(attritutes);
		skuPojo.setPricing(p);
		skuPojo.setEnabled(true);
		skuPojo.build();
		skuPojo.setCategory_id(catId);
		skuPojo.setSpin(spinId);
		obj.add(new Object[]{jsonHelper.getObjectToJSONallowNullValues(skuPojo),skuPojo.getSku_id(),true,null});

		//pricing null 
		skuPojo = new SkuPojo("S003");
		attritutes=new SkuAttributes();
		attritutes.build();
		skuPojo.setSkuAttributes(attritutes);
		skuPojo.setPricing(null);
		skuPojo.setEnabled(false);
		skuPojo.build();
		skuPojo.setCategory_id(catId);
		skuPojo.setSpin(spinId);
		obj.add(new Object[]{jsonHelper.getObjectToJSONallowNullValues(skuPojo),skuPojo.getSku_id(),true,null});

		//attributes null
		Thread.sleep(1000);
		skuPojo = new SkuPojo("S004");
		p=new CatalogV2PricingPojo();
		p.build();
		skuPojo.build();
		
		skuPojo.setPricing(p);
		skuPojo.setCategory_id(catId);
		skuPojo.setSpin(spinId);
        skuPojo.setSkuAttributes(new SkuAttributes());
		obj.add(new Object[]{jsonHelper.getObjectToJSON(skuPojo),skuPojo.getSku_id(),true,null});
		

		//Product sku with spin already exists
		Thread.sleep(1000);
		skuPojo = new SkuPojo("S0001");
		p=new CatalogV2PricingPojo();
		p.build();
		attritutes=new SkuAttributes();
		attritutes.build();
		skuPojo.setSkuAttributes(attritutes);
		skuPojo.setPricing(p);
		skuPojo.build();
		skuPojo.setCategory_id(catId);
		skuPojo.setSpin(spinId);
		skuPojo.setSku_id(skuId);
		skuPojo.setStore_id(storeId);
		obj.add(new Object[]{jsonHelper.getObjectToJSONallowNullValues(skuPojo),skuPojo.getSku_id(),false,spinId});

		
		return obj.iterator();
	}

	@DataProvider(name = "getSkuByIds")
	public Object[][] getSkuByIds() throws IOException {
		return new Object[][] { 
			//storeId,skuId,categoryId  
			{storeId,skuId,catId,true},
			{storeId,skuId,"",false},
			{storeId,"",catId,false},
			{"",skuId,catId,false},
			// store id  mismatch with skuID
			{storeId,"X",catId,false},
			// store id  mismatch with CategoryId
			{storeId,skuId,"X",false},
			// Skuid  mismatch with CategoryId
			{"X",skuId,catId,false},
		};
	} 



	@DataProvider(name = "getSkuByStoreId")
	public Object[][] getSkuByStoreId() throws IOException {
		return new Object[][] { 
			//storeId,skuId,categoryId
			{storeId,true},
			{"",false},
			{"X",false},
		};
	} 


	@DataProvider(name = "disableSkuId")
	public Object[][] disableSkuId() throws IOException {
		return new Object[][] { 
			//storeId,skuIds
			{storeId,skuId,true},
			{storeId,"-",false},
			{"-",skuId,false},
			//Sku not belonging to store
			{storeId,"X",false}
		};
	} 

	@DataProvider(name = "enableSkuId")
	public Object[][] enableSkuId() throws IOException {
		return new Object[][] { 
			//storeId,skuIds
			{storeId,skuId,true},
			{storeId,"-",false},
			{"-",skuId,false},
			//Sku not belonging to store
			{storeId,"X",false}
		};
	} 

	@DataProvider(name = "getSkuBySKuId")
	public Object[][] getSkuBySKuId() throws IOException {
		return new Object[][] { 
			//storeId,skuId
			{storeId,skuId,true},
			{"-",skuId,false},
			{storeId,"-",false},
			{storeId,"X",false}
		};
	} 

}


