package com.swiggy.api.erp.ff.dp.manualTaskService;

import com.swiggy.api.erp.ff.constants.ManualTaskServiceConstants;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import org.bouncycastle.cert.ocsp.Req;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CreateTaskAPIData implements ManualTaskServiceConstants {

    LOSHelper losHelper = new LOSHelper();

    @DataProvider(name = "createTaskData")
    public Object[][] createTaskData() throws IOException, InterruptedException {

        HashMap<String, String> AgentRole = new HashMap<>();
        AgentRole.put("L1_PLACER", "L1_PLACER");
        AgentRole.put("L2_PLACER", "L2_PLACER");

        HashMap<String, String> RequestReason = new HashMap<>();
        RequestReason.put("ORDER_DECLINED_BY_RESTAURANT", "ORDER_DECLINED_BY_RESTAURANT");
        RequestReason.put("MANUAL_PLACING_ORDER", "MANUAL_PLACING_ORDER");
        RequestReason.put("CANCELLATION_REQUIRED", "CANCELLATION_REQUIRED");

        HashMap<String, String> successCase = new HashMap<>();
        successCase.put("responseCode", "200");
        successCase.put("statusCode", "0");
        successCase.put("statusMessage", "Success");
        successCase.put("taskId", "should not be null");
        successCase.put("taskStatus", "active");

        HashMap<String, String> badRequestCase = new HashMap<>();
        badRequestCase.put("expectedResponse", "400");
        badRequestCase.put("statusCode", "0");
        badRequestCase.put("statusMessage", "message");
        badRequestCase.put("task_Id", "message");
        badRequestCase.put("taskStatus", "message");

        return new Object[][]{
                //orderId, reason, reasonId, reasonOE, subReason, subReasonId, subReasonOE, source, responseMap
                {losHelper.getAnOrder("manual"), REASON_MANUAL_PLACING_ORDER_L1, REASON_MANUAL_PLACING_ORDER_L1_ID, "L1", "null", -1, "null", "testsource", successCase},
                {losHelper.getAnOrder("manual"), REASON_PARTNER_DELAY_L1, REASON_PARTNER_DELAY_L1_ID, "L1", SUBREASON_REMINDER_ATTEMPTS_UNSUCCESSFUL_L1, SUBREASON_REMINDER_ATTEMPTS_UNSUCCESSFUL_L1_ID, "L1", "testsource", successCase},
                {losHelper.getAnOrder("manual"), REASON_PARTNER_DELAY_L1, REASON_PARTNER_DELAY_L1_ID, "L1", SUBREASON_UNABLE_TO_ATTEMPT_REMINDERS_L1, SUBREASON_UNABLE_TO_ATTEMPT_REMINDERS_L1_ID, "L1", "testsource", successCase},

//                {losHelper.getAnOrder("manual"), REASON_VENDOR_UNABLE_TO_ACCEPT_ORDER_L1, REASON_VENDOR_UNABLE_TO_ACCEPT_ORDER_L1_ID, "L1", SUBREASON_ITEM_NOT_AVAILABLE_L2, SUBREASON_ITEM_NOT_AVAILABLE_L2_ID, "L2", "testsource", successCase},
//                {losHelper.getAnOrder("manual"), REASON_PARTNER_DELAY_L1, REASON_PARTNER_DELAY_L1_ID, "L1", "null", -1, "null", "testsource", successCase},

//                {losHelper.getAnOrder("partner"), REASON_MANUAL_PLACING_ORDER_L1, REASON_MANUAL_PLACING_ORDER_L1_ID, "null", "testsource", successCase},
//                {losHelper.getAnOrder("partner"), REASON_PARTNER_DELAY_L1, SUBREASON_REMINDER_ATTEMPTS_UNSUCCESSFUL_L1, "testsource", successCase},
//                {losHelper.getAnOrder("partner"), REASON_PARTNER_DELAY_L1, SUBREASON_UNABLE_TO_ATTEMPT_REMINDERS_L1, "testsource", successCase},
//                {losHelper.getAnOrder("partner"), REASON_PARTNER_DELAY_L1, "null", "testsource", successCase},
//
//                {losHelper.getAnOrder("thirdparty"), REASON_MANUAL_PLACING_ORDER_L1, "null", "testsource", successCase},
//                {losHelper.getAnOrder("thirdparty"), REASON_PARTNER_DELAY_L1, SUBREASON_REMINDER_ATTEMPTS_UNSUCCESSFUL_L1, "testsource", successCase},
//                {losHelper.getAnOrder("thirdparty"), REASON_PARTNER_DELAY_L1, SUBREASON_UNABLE_TO_ATTEMPT_REMINDERS_L1, "testsource", successCase},
//                {losHelper.getAnOrder("thirdparty"), REASON_PARTNER_DELAY_L1, "null", "testsource", successCase},


                {losHelper.getAnOrder("invalid"), REASON_MANUAL_PLACING_ORDER_L1, REASON_MANUAL_PLACING_ORDER_L1_ID, "L1", "null", -1, "null",  "testsource", successCase},
                {losHelper.getAnOrder("partner"), REASON_MANUAL_PLACING_ORDER_L1, REASON_MANUAL_PLACING_ORDER_L1_ID, "L1", "invalidSubReason", -1, "null",  "testsource", badRequestCase},
                {losHelper.getAnOrder("partner"), "InvalidReason", -1, "null", SUBREASON_REMINDER_ATTEMPTS_UNSUCCESSFUL_L1, SUBREASON_REMINDER_ATTEMPTS_UNSUCCESSFUL_L1_ID, "L1",  "testsource", badRequestCase},
                {losHelper.getAnOrder("partner"), "InvalidReason", -1, "null", "invalidSubReason", -1, "null",  "testsource", badRequestCase},
                {losHelper.getAnOrder("partner"), "123", -1, "null", "321", -1, "null", "testsource", badRequestCase},
                {losHelper.getAnOrder("partner"), REASON_MANUAL_PLACING_ORDER_L1, REASON_MANUAL_PLACING_ORDER_L1_ID, "L1", SUBREASON_REMINDER_ATTEMPTS_UNSUCCESSFUL_L1, SUBREASON_REMINDER_ATTEMPTS_UNSUCCESSFUL_L1_ID, "L1", 123, successCase},

        };
    }
}
