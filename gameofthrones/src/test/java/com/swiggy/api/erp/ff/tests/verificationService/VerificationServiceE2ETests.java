package com.swiggy.api.erp.ff.tests.verificationService;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.constants.VerificationServiceConstants;
import com.swiggy.api.erp.ff.dp.verificationService.VerificationServiceApiTestData;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.VerificationServiceHelper;

public class VerificationServiceE2ETests extends VerificationServiceApiTestData implements VerificationServiceConstants{
	
	VerificationServiceHelper helper = new VerificationServiceHelper();
	OMSHelper omsHelper =  new OMSHelper();
	
	@Test(dataProvider = "newUserE2ETestData", groups = {"sanity", "regression"},description = "End to end tests for new user verification")
	public void newUserVerificationEndToEndTests(boolean newUser, boolean longDistance, boolean crossAreaVerification, boolean isOrderReplicated, String orderType) throws IOException, InterruptedException{
		System.out.println("***************************************** newUserVerificationEndToEndTests started *****************************************");
		
		boolean duplicateCheck = false;
		String orderId = helper.createOrderForVerificationService(newUser, longDistance, crossAreaVerification, isOrderReplicated, orderType);
		Thread.sleep(2500);
		helper.assertNewUserCases(Long.parseLong(orderId), longDistance, crossAreaVerification, isOrderReplicated);
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.getVerifyAction(newUser, crossAreaVerification, duplicateCheck)), "Failed to verify order ID = "+orderId);
		Thread.sleep(2000);
//		Assert.assertFalse(helper.queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Order is not removed from waiting for verifier queue "+orderId);
		helper.assertCurrentStateInVerificationDBAfterVerification(Long.parseLong(orderId));
		Assert.assertTrue(helper.isOrderRelayedToRMS(Long.parseLong(orderId)), "Order has not been relayed to vendor and delivery after verification, "+orderId);
		
		System.out.println("######################################### newUserVerificationEndToEndTests compleated #########################################");
	}
	
	@Test(dataProvider = "crossAreaE2ETestData", groups = {"sanity", "regression"},description = "End to end tests for cross area verification")
	public void crossAreaVerificationEndToEndTests(boolean newUser, boolean longDistance, boolean crossAreaVerification, boolean isOrderReplicated, String orderType) throws IOException, InterruptedException{
		System.out.println("***************************************** crossAreaVerificationEndToEndTests started *****************************************");

		boolean duplicateCheck = false;
		helper.clearFactTable(customerIdForVerificationService);
		Thread.sleep(3000);
		String orderId = helper.createOrderForVerificationService(newUser, longDistance, crossAreaVerification, isOrderReplicated, orderType);
		Thread.sleep(2500);
		helper.assertCrossAreaCases(Long.parseLong(orderId), longDistance, crossAreaVerification, isOrderReplicated);
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.getVerifyAction(newUser, crossAreaVerification, duplicateCheck)), "Failed to verify order ID = "+orderId);
		Thread.sleep(2000);
//		Assert.assertFalse(helper.queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Order is not removed from waiting for verifier queue "+orderId);
		helper.assertCurrentStateInVerificationDBAfterVerification(Long.parseLong(orderId));
		Assert.assertTrue(helper.isOrderRelayedToRMS(Long.parseLong(orderId)), "Order has not been relayed to vendor and delivery after verification, "+orderId);
		
		System.out.println("######################################### crossAreaVerificationEndToEndTests compleated #########################################");
	}

//	@Test(dataProvider = "duplicateOrderE2ETestData", groups = {"sanity", "regression"},description = "End to end tests for duplicate order verification")
	public void duplicateOrderVerificationEndToEndTests(boolean newUser, boolean longDistance, boolean crossAreaVerification, boolean isOrderReplicated, String orderType) throws IOException, InterruptedException{
		System.out.println("***************************************** duplicateOrderVerificationEndToEndTests started *****************************************");

		boolean duplicateCheck = true;
		helper.clearFactTable(customerIdForVerificationService);
		Thread.sleep(3000);
		String orderId = helper.createOrderForVerificationService(newUser, longDistance, crossAreaVerification, isOrderReplicated, orderType);
		Thread.sleep(1000);
		String duplicateOrderId = helper.createOrderForVerificationService(newUser, longDistance, crossAreaVerification, isOrderReplicated, orderType);
		Thread.sleep(2500);
		helper.assertCrossAreaCases(Long.parseLong(orderId), longDistance, crossAreaVerification, isOrderReplicated);
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.getVerifyAction(newUser, crossAreaVerification, duplicateCheck)), "Failed to verify order ID = "+orderId);
		Thread.sleep(2000);
//		Assert.assertFalse(helper.queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Order is not removed from waiting for verifier queue "+orderId);
		helper.assertCurrentStateInVerificationDBAfterVerification(Long.parseLong(orderId));
		Assert.assertTrue(helper.isOrderRelayedToRMS(Long.parseLong(orderId)), "Order has not been relayed to vendor and delivery after verification, "+orderId);

		System.out.println("######################################### duplicateOrderVerificationEndToEndTests compleated #########################################");
	}

}
