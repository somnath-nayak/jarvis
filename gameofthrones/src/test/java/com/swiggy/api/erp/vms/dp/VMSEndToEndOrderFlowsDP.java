package com.swiggy.api.erp.vms.dp;

import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.td.pojo.RMSCreateTDEntry;
import com.swiggy.api.erp.vms.td.pojo.RMSTDBuilder;
import com.swiggy.api.erp.vms.td.pojo.Restaurant;
import com.swiggy.api.erp.vms.td.pojo.RuleDiscount;
import com.swiggy.api.sf.checkout.pojo.Addon;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.Variant;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.apache.commons.lang3.time.DateUtils;
import org.testng.annotations.DataProvider;

import java.util.*;

public class VMSEndToEndOrderFlowsDP {
	static RMSCommonHelper r = new RMSCommonHelper();
	static String restaurantId = null;
	public static String VENDOR_PASSWORD = null;
	public static String VENDOR_USERNAME = null;
	public static String consumerAppPassword = null;
	public static long consumerAppMobile = 0l;
	public static String OMS_USERNAME = null;
	public static String OMS_PASSWORD = null;
	static HashMap<String, String> omsSessionData = null;
	static HashMap<String, String> rmsSessionData = null;

	static {
		try {
			if (RMSCommonHelper.getEnv().equalsIgnoreCase("stage1")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "test1234";
				consumerAppMobile = 9886379321l;
				restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
			} else if (RMSCommonHelper.getEnv().equalsIgnoreCase("stage2")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "test1234";
				consumerAppMobile = 9886379321l;
				restaurantId = "9990";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
			} else if (RMSCommonHelper.getEnv().equalsIgnoreCase("stage3")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "9990";
				consumerAppPassword = "test1234";
				consumerAppMobile = 7022026831l;
				restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
			} else if (RMSCommonHelper.getEnv().equalsIgnoreCase("prod")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "test1234";
				consumerAppMobile = 9886379321l;
				restaurantId = "9990";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
			}

			omsSessionData = RMSCommonHelper.getOMSSessionData(OMS_USERNAME, OMS_PASSWORD);
			rmsSessionData = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws Exception {
		String restaurantId = "9990";
		String[] orderReponse = RMSCommonHelper.createOrder(restaurantId, RMSConstants.consumerAppMobile,
				RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
		System.out.println(orderReponse[0]);
		System.out.println(orderReponse[1]);

	}

	@DataProvider(name = "AAA")
	public static Object[][] aaa() throws Exception {
		String restaurantId = "9990";
		RMSCommonHelper.createOrder(restaurantId, RMSConstants.consumerAppMobile, RMSConstants.consumerAppPassword,
				RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS, 1);

		return null;
	}

	@DataProvider(name = "verifyOrderAtRMS")
	public static Object[][] verifyOrderAtRMS() throws Exception {
		return new Object[][] {
				{ placeNormalOrder(restaurantId, consumerAppMobile, consumerAppPassword, RMSConstants.PAYMENTMODE_CASH,
						RMSConstants.TEST_ORDER_COMMENTS) },
//				{ placeNormalOrderWithMultipleItems(restaurantId, consumerAppMobile, consumerAppPassword,
//						RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS) },
//				{ placerOrderWithVariant(restaurantId, consumerAppMobile, consumerAppPassword,
//						RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS) },
//				{ placerOrderWithAddons(restaurantId, consumerAppMobile, consumerAppPassword,
//						RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS) },
//				{ placerOrderWithItemHavingAddonsNVariants(restaurantId, consumerAppMobile, consumerAppPassword,
//						RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS) },
//
//				{ placerOrderWithDifferentAddonsNVariantsItems(restaurantId, consumerAppMobile, consumerAppPassword,
//						RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS) },
//				{ placerOrderWithCombinationOfAddonNvariantsNNormalItems(restaurantId, consumerAppMobile,
//						consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS) }

		};
	}

	public static HashMap<String, Object> placeNormalOrder(String restaurantId, long consumerAppMobile,
			String consumerAppPassword, String paymentMode, String comments) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		items.add(new Cart(null, null, String.valueOf(itemList.get(0)), 1));
		String orderReponse[] = RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng, items,
				restaurantId, paymentMode, comments);
		if (RMSCommonHelper.isOrderAvailableAtFF(orderReponse[0])) {
			boolean flg = omsHelper.verifyOrder(orderReponse[0], "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
		} else {
			throw new Exception("Order Not Reached FF...So DownStream Dependency Failed For FF");
		}
		return prepareOrderInfo(orderReponse, restaurantId);
	}

	public static HashMap<String, Object> placeNormalOrderWithMultipleItems(String restaurantId, long consumerAppMobile,
			String consumerAppPassword, String paymentMode, String comments) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		items.add(new Cart(null, null, String.valueOf(itemList.get(0)), 1));
		items.add(new Cart(null, null, String.valueOf(itemList.get(1)), 1));
		String orderReponse[] = RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng, items,
				restaurantId, paymentMode, comments);
		if (RMSCommonHelper.isOrderAvailableAtFF(orderReponse[0])) {
			boolean flg = omsHelper.verifyOrder(orderReponse[0], "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
		} else {
			throw new Exception("Order Not Reached FF...So DownStream Dependency Failed For FF");
		}
		return prepareOrderInfo(orderReponse, restaurantId);
	}

	public static HashMap<String, Object> placerOrderWithVariant(String restaurantId, long consumerAppMobile,
			String consumerAppPassword, String paymentMode, String comments) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		Variant v1 = new Variant();
		// need to get the data dynamically.code will be updated Ramzi
		v1.setgroup_id(481257);
		v1.setvariation_id(1637717);
		v1.setPrice(0.0);
		Variant v2 = new Variant();
		v2.setgroup_id(481259);
		v2.setvariation_id(1637729);
		v2.setPrice(0.0);
		ArrayList<Variant> variantData = new ArrayList<>();
		variantData.add(v1);
		variantData.add(v2);
		items.add(new Cart(null, variantData, "5748194", 1));
		String orderReponse[] = RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng, items,
				restaurantId, paymentMode, comments);
		if (RMSCommonHelper.isOrderAvailableAtFF(orderReponse[0])) {
			boolean flg = omsHelper.verifyOrder(orderReponse[0], "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
		} else {
			throw new Exception("Order Not Reached FF...So DownStream Dependency Failed For FF");
		}
		return prepareOrderInfo(orderReponse, restaurantId);
	}

	public static HashMap<String, Object> placerOrderWithAddons(String restaurantId, long consumerAppMobile,
			String consumerAppPassword, String paymentMode, String comments) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		Addon addon = new Addon();
		// need to get the data dynamically.code will be updated Ramzi
		addon.setgroup_id("1115663");
		addon.setchoice_id("3295833");
		addon.setName("Veggie Delite.");
		addon.setPrice(4000);
		List<Addon> a = new ArrayList<Addon>();
		a.add(addon);
		items.add(new Cart(a, null, "5748192", 1));
		String orderReponse[] = RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng, items,
				restaurantId, paymentMode, comments);
		if (RMSCommonHelper.isOrderAvailableAtFF(orderReponse[0])) {
			boolean flg = omsHelper.verifyOrder(orderReponse[0], "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
		} else {
			throw new Exception("Order Not Reached FF...So DownStream Dependency Failed For FF");
		}
		return prepareOrderInfo(orderReponse, restaurantId);
	}

	public static HashMap<String, Object> placerOrderWithItemHavingAddonsNVariants(String restaurantId,
			long consumerAppMobile, String consumerAppPassword, String paymentMode, String comments) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		Addon addon = new Addon();
		// need to get the data dynamically.code will be updated Ramzi
		addon.setgroup_id("1115663");
		addon.setchoice_id("3295833");
		addon.setName("Veggie Delite.");
		addon.setPrice(4000);
		List<Addon> a = new ArrayList<Addon>();
		a.add(addon);

		Variant v1 = new Variant();
		// need to get the data dynamically.code will be updated Ramzi
		v1.setgroup_id(481257);
		v1.setvariation_id(1637717);
		v1.setPrice(0.0);
		Variant v2 = new Variant();
		v2.setgroup_id(481259);
		v2.setvariation_id(1637729);
		v2.setPrice(0.0);
		ArrayList<Variant> variantData = new ArrayList<>();
		variantData.add(v1);
		variantData.add(v2);

		items.add(new Cart(a, variantData, "5748196", 1));
		String orderReponse[] = RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng, items,
				restaurantId, paymentMode, comments);
		if (RMSCommonHelper.isOrderAvailableAtFF(orderReponse[0])) {
			boolean flg = omsHelper.verifyOrder(orderReponse[0], "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
		} else {
			throw new Exception("Order Not Reached FF...So DownStream Dependency Failed For FF");
		}
		return prepareOrderInfo(orderReponse, restaurantId);
	}

	public static HashMap<String, Object> placerOrderWithDifferentAddonsNVariantsItems(String restaurantId,
			long consumerAppMobile, String consumerAppPassword, String paymentMode, String comments) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		Addon addon = new Addon();
		// need to get the data dynamically.code will be updated Ramzi
		addon.setgroup_id("1115663");
		addon.setchoice_id("3295833");
		addon.setName("Veggie Delite.");
		addon.setPrice(4000);
		List<Addon> addonData = new ArrayList<Addon>();
		addonData.add(addon);
		Variant v1 = new Variant();
		// need to get the data dynamically.code will be updated Ramzi
		v1.setgroup_id(481257);
		v1.setvariation_id(1637717);
		v1.setPrice(0.0);
		Variant v2 = new Variant();
		v2.setgroup_id(481259);
		v2.setvariation_id(1637729);
		v2.setPrice(0.0);
		ArrayList<Variant> variantData = new ArrayList<>();
		variantData.add(v1);
		variantData.add(v2);
		items.add(new Cart(null, variantData, "5748194", 1));
		items.add(new Cart(addonData, null, "5748192", 1));
		String orderReponse[] = RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng, items,
				restaurantId, paymentMode, comments);
		if (RMSCommonHelper.isOrderAvailableAtFF(orderReponse[0])) {
			boolean flg = omsHelper.verifyOrder(orderReponse[0], "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
		} else {
			throw new Exception("Order Not Reached FF...So DownStream Dependency Failed For FF");
		}
		return prepareOrderInfo(orderReponse, restaurantId);
	}

	public static HashMap<String, Object> placerOrderWithCombinationOfAddonNvariantsNNormalItems(String restaurantId,
			long consumerAppMobile, String consumerAppPassword, String paymentMode, String comments) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		Addon addon = new Addon();
		// need to get the data dynamically.code will be updated Ramzi
		addon.setgroup_id("1115663");
		addon.setchoice_id("3295833");
		addon.setName("Veggie Delite.");
		addon.setPrice(4000);
		List<Addon> addonData = new ArrayList<Addon>();
		addonData.add(addon);
		Variant v1 = new Variant();
		// need to get the data dynamically.code will be updated Ramzi
		v1.setgroup_id(481257);
		v1.setvariation_id(1637717);
		v1.setPrice(0.0);
		Variant v2 = new Variant();
		v2.setgroup_id(481259);
		v2.setvariation_id(1637729);
		v2.setPrice(0.0);
		ArrayList<Variant> variantData = new ArrayList<>();
		variantData.add(v1);
		variantData.add(v2);
		items.add(new Cart(null, variantData, "5748194", 1));
		items.add(new Cart(addonData, null, "5748192", 1));
		items.add(new Cart(addonData, null, "5748191", 1));
		String orderReponse[] = RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng, items,
				restaurantId, paymentMode, comments);
		if (RMSCommonHelper.isOrderAvailableAtFF(orderReponse[0])) {
			boolean flg = omsHelper.verifyOrder(orderReponse[0], "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
		} else {
			throw new Exception("Order Not Reached FF...So DownStream Dependency Failed For FF");
		}
		return prepareOrderInfo(orderReponse, restaurantId);
	}

	private static HashMap<String, Object> prepareOrderInfo(String[] orderReponse, String restaurantId) {
		HashMap<String, Object> orderInfo = new HashMap<>();
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		orderInfo.put("orderReponse", orderReponse[1]);
		orderInfo.put("orderId", orderReponse[0]);
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("orderData", orderInfo);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("rmsSessionData", rmsSessionData);
		return myFetchOrderData;
	}

	@DataProvider(name = "verifyFetchOrdersAtRMS")
	public static Object[][] a() throws Exception {
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		String restaurantId = "9990";
		String[] orderReponse = RMSCommonHelper.createOrder(restaurantId, RMSConstants.consumerAppMobile,
				RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
		HashMap<String, Object> orderInfo = new HashMap<>();
		orderInfo.put("orderReponse", orderReponse[1]);
		orderInfo.put("orderId", orderReponse[0]);
		myFetchOrderData.put("orderData", orderInfo);
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("omsSessionData",
				RMSCommonHelper.getOMSSessionData(RMSConstants.OMS_UAT_USERNAME, RMSConstants.OMS_UAT_PASSWORD));
		myFetchOrderData.put("rmsSessionData",
				RMSCommonHelper.getRMSSessionData(RMSConstants.VENDOR_UAT_USERNAME, RMSConstants.VENDOR_UAT_PASSWORD));
		return new Object[][] { { myFetchOrderData } };
	}

	@DataProvider(name = "verifyFetchOrdersAtRMSAndConfirmTheOrder")
	public static Object[][] verifyFetchOrdersAtRMSAndConfirmTheOrder() throws Exception {
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		String restaurantId = "9990";
		String[] orderReponse = RMSCommonHelper.createOrder(restaurantId, RMSConstants.consumerAppMobile,
				RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
		HashMap<String, Object> orderInfo = new HashMap<>();
		orderInfo.put("orderReponse", orderReponse[1]);
		orderInfo.put("orderId", orderReponse[0]);
		myFetchOrderData.put("orderData", orderInfo);
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("omsSessionData",
				RMSCommonHelper.getOMSSessionData(RMSConstants.OMS_UAT_USERNAME, RMSConstants.OMS_UAT_PASSWORD));
		myFetchOrderData.put("rmsSessionData",
				RMSCommonHelper.getRMSSessionData(RMSConstants.VENDOR_UAT_USERNAME, RMSConstants.VENDOR_UAT_PASSWORD));
		return new Object[][] { { myFetchOrderData } };
	}

	@DataProvider(name = "verifyFetchOrdersAtRMSAndMarkItemOutOfStock")
	public static Object[][] verifyFetchOrdersAtRMSAndMarkItemOutOfStock() throws Exception {
		String restaurantId = "9990";
		return new Object[][] { 	{ placeNormalOrder(restaurantId, consumerAppMobile, consumerAppPassword, RMSConstants.PAYMENTMODE_CASH,
				RMSConstants.TEST_ORDER_COMMENTS)  } };
	}

	@DataProvider(name = "verifyOOSBySendingMultipleAlertnatives")
	public static Object[][] verifyOOSBySendingMultipleAlertnatives() throws Exception {
		String restaurantId = "9990";
		return new Object[][] { 	{ placeNormalOrder(restaurantId, consumerAppMobile, consumerAppPassword, RMSConstants.PAYMENTMODE_CASH,
				RMSConstants.TEST_ORDER_COMMENTS)  } };
	}

	@DataProvider(name = "verifyUpdateCallBackRequestFromFF")
	public static Object[][] verifyUpdateCallBackRequestFromFF() throws Exception {
		String restaurantId = "9990";
		return new Object[][] { 	{ placeNormalOrder(restaurantId, consumerAppMobile, consumerAppPassword, RMSConstants.PAYMENTMODE_CASH,
				RMSConstants.TEST_ORDER_COMMENTS)  } };
	}

	@DataProvider(name = "verifyRequestCallBack")
	public static Object[][] verifyRequestCallBack() throws Exception {
		String restaurantId = "9990";
		return new Object[][] { 	{ placeNormalOrder(restaurantId, consumerAppMobile, consumerAppPassword, RMSConstants.PAYMENTMODE_CASH,
				RMSConstants.TEST_ORDER_COMMENTS)  } };
	}

	// TD DPs

	@DataProvider(name = "verifyFetchOrderWithPercentageTD")
	public static Object[][] verifyFetchOrderWithPercentageTD() throws Exception {
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		String restaurantId = "9990";

		JsonHelper jsonHelper = new JsonHelper();
		ArrayList<Integer> restIds1 = new ArrayList<>();
		restIds1.add(Integer.parseInt(restaurantId));
		ArrayList<Restaurant> restList1 = new ArrayList<>();
		Restaurant r1 = new Restaurant();
		r1.setId(Integer.parseInt(restaurantId));
		r1.setName("Myramzi");
		r1.setCity(219);
		r1.setArea("MyRamziArea");
		r1.setRecommended(0);
		r1.setCostForTwo(0);
		r1.setDeliveryCharge(0);
		r1.setMinimumOrder(0);
		r1.setOpened(0);
		r1.setDeliveryTime(0);
		r1.setTmpClosed(0);
		r1.setPostalCode(0);
		r1.setCityDeliveryCharge(0);
		r1.setThreshold(0);
		r1.setDiscountType(0);
		r1.setTradeCampaignHeaders(new ArrayList());
		r1.setSelect("false");
		r1.setNew("false");
		r1.setCategories(new ArrayList());
		restList1.add(r1);
		RuleDiscount rd = new RuleDiscount();
		rd.setType("Percentage");
		rd.setDiscountLevel("Restaurant");
		rd.setPercentDiscount(20);
		rd.setMinCartAmount(0);
		rd.setPercentDiscountAbsoluteCap(0);

		RMSCreateTDEntry entry1 = new RMSTDBuilder().setEnabled("true").setRestaurantIds(restIds1)
				.setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli()))
				.setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli()))
				.setCampaignType("Percentage").setRuleDiscountType("Percentage").setCreatedBy(9886379321l)
				.setDiscountLevel("Restaurant").setRestaurantList(restList1).setSlots(new ArrayList<>())
				.setFirstOrderRestriction("false").setUserRestriction("false").setRestaurantFirstOrder("false")
				.setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false")
				.setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		HashMap<String, Object> orderInfo = new HashMap<>();
		orderInfo.put("orderReponse", null);
		orderInfo.put("orderId", null);
		myFetchOrderData.put("orderData", orderInfo);
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("omsSessionData",
				RMSCommonHelper.getOMSSessionData(RMSConstants.OMS_UAT_USERNAME, RMSConstants.OMS_UAT_PASSWORD));
		myFetchOrderData.put("rmsSessionData",
				RMSCommonHelper.getRMSSessionData(RMSConstants.VENDOR_UAT_USERNAME, RMSConstants.VENDOR_UAT_PASSWORD));
		return new Object[][] { { myFetchOrderData, jsonHelper.getObjectToJSON(entry1) } };
	}

	@DataProvider(name = "verifyFetchOrderWhenOrderAmountLessThanTDMin")
	public static Object[][] verifyFetchOrderWhenOrderAmountLessThanTDMin() throws Exception {
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		String restaurantId = "9990";

		JsonHelper jsonHelper = new JsonHelper();
		ArrayList<Integer> restIds1 = new ArrayList<>();
		restIds1.add(Integer.parseInt(restaurantId));
		ArrayList<Restaurant> restList1 = new ArrayList<>();
		Restaurant r1 = new Restaurant();
		r1.setId(Integer.parseInt(restaurantId));
		r1.setName("Myramzi");
		r1.setCity(219);
		r1.setArea("MyRamziArea");
		r1.setRecommended(0);
		r1.setCostForTwo(0);
		r1.setDeliveryCharge(0);
		r1.setMinimumOrder(0);
		r1.setOpened(0);
		r1.setDeliveryTime(0);
		r1.setTmpClosed(0);
		r1.setPostalCode(0);
		r1.setCityDeliveryCharge(0);
		r1.setThreshold(0);
		r1.setDiscountType(0);
		r1.setTradeCampaignHeaders(new ArrayList());
		r1.setSelect("false");
		r1.setNew("false");
		r1.setCategories(new ArrayList());
		restList1.add(r1);
		RuleDiscount rd = new RuleDiscount();
		rd.setType("Percentage");
		rd.setDiscountLevel("Restaurant");
		rd.setPercentDiscount(2);
		rd.setMinCartAmount(0);
		rd.setPercentDiscountAbsoluteCap(0);

		RMSCreateTDEntry entry1 = new RMSTDBuilder().setEnabled("true").setRestaurantIds(restIds1)
				.setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli()))
				.setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli()))
				.setCampaignType("Percentage").setRuleDiscountType("Percentage").setCreatedBy(9886379321l)
				.setDiscountLevel("Restaurant").setRestaurantList(restList1).setSlots(new ArrayList<>())
				.setFirstOrderRestriction("false").setUserRestriction("false").setRestaurantFirstOrder("false")
				.setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false")
				.setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		HashMap<String, Object> orderInfo = new HashMap<>();
		orderInfo.put("orderReponse", null);
		orderInfo.put("orderId", null);
		myFetchOrderData.put("orderData", orderInfo);
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("omsSessionData",
				RMSCommonHelper.getOMSSessionData(RMSConstants.OMS_UAT_USERNAME, RMSConstants.OMS_UAT_PASSWORD));
		myFetchOrderData.put("rmsSessionData",
				RMSCommonHelper.getRMSSessionData(RMSConstants.VENDOR_UAT_USERNAME, RMSConstants.VENDOR_UAT_PASSWORD));
		return new Object[][] { { myFetchOrderData, jsonHelper.getObjectToJSON(entry1) } };
	}

	@DataProvider(name = "verifyFetchOrderWhenOrderAmountMoreThanTDMin")
	public static Object[][] verifyFetchOrderWhenOrderAmountMoreThanTDMin() throws Exception {
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		String restaurantId = "9990";

		JsonHelper jsonHelper = new JsonHelper();
		ArrayList<Integer> restIds1 = new ArrayList<>();
		restIds1.add(Integer.parseInt(restaurantId));
		ArrayList<Restaurant> restList1 = new ArrayList<>();
		Restaurant r1 = new Restaurant();
		r1.setId(Integer.parseInt(restaurantId));
		r1.setName("Myramzi");
		r1.setCity(219);
		r1.setArea("MyRamziArea");
		r1.setRecommended(0);
		r1.setCostForTwo(0);
		r1.setDeliveryCharge(0);
		r1.setMinimumOrder(0);
		r1.setOpened(0);
		r1.setDeliveryTime(0);
		r1.setTmpClosed(0);
		r1.setPostalCode(0);
		r1.setCityDeliveryCharge(0);
		r1.setThreshold(0);
		r1.setDiscountType(0);
		r1.setTradeCampaignHeaders(new ArrayList());
		r1.setSelect("false");
		r1.setNew("false");
		r1.setCategories(new ArrayList());
		restList1.add(r1);
		RuleDiscount rd = new RuleDiscount();
		rd.setType("Percentage");
		rd.setDiscountLevel("Restaurant");
		rd.setPercentDiscount(2);
		rd.setMinCartAmount(0);
		rd.setPercentDiscountAbsoluteCap(0);

		RMSCreateTDEntry entry1 = new RMSTDBuilder().setEnabled("true").setRestaurantIds(restIds1)
				.setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli()))
				.setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli()))
				.setCampaignType("Percentage").setRuleDiscountType("Percentage").setCreatedBy(9886379321l)
				.setDiscountLevel("Restaurant").setRestaurantList(restList1).setSlots(new ArrayList<>())
				.setFirstOrderRestriction("false").setUserRestriction("false").setRestaurantFirstOrder("false")
				.setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false")
				.setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		HashMap<String, Object> orderInfo = new HashMap<>();
		orderInfo.put("orderReponse", null);
		orderInfo.put("orderId", null);
		myFetchOrderData.put("orderData", orderInfo);
		myFetchOrderData.put("orderData", orderInfo);
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("omsSessionData",
				RMSCommonHelper.getOMSSessionData(RMSConstants.OMS_UAT_USERNAME, RMSConstants.OMS_UAT_PASSWORD));
		myFetchOrderData.put("rmsSessionData",
				RMSCommonHelper.getRMSSessionData(RMSConstants.VENDOR_UAT_USERNAME, RMSConstants.VENDOR_UAT_PASSWORD));
		return new Object[][] { { myFetchOrderData, jsonHelper.getObjectToJSON(entry1) } };
	}

	@DataProvider(name = "verifyFetchOrderWithFlatTD")
	public static Object[][] verifyFetchOrderWithFlatTD() throws Exception {
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		String restaurantId = "9990";
		JsonHelper jsonHelper = new JsonHelper();

		ArrayList<Integer> restIds2 = new ArrayList<>();
		restIds2.add(9990);
		ArrayList<Restaurant> restList2 = new ArrayList<>();
		Restaurant r2 = new Restaurant();
		r2.setId(Integer.parseInt(restaurantId));
		r2.setName("Myramzi2");
		r2.setCity(219);
		r2.setArea("MyRamziArea2");
		r2.setRecommended(0);
		r2.setCostForTwo(0);
		r2.setDeliveryCharge(0);
		r2.setMinimumOrder(0);
		r2.setOpened(0);
		r2.setDeliveryTime(0);
		r2.setTmpClosed(0);
		r2.setPostalCode(0);
		r2.setCityDeliveryCharge(0);
		r2.setThreshold(0);
		r2.setDiscountType(0);
		r2.setTradeCampaignHeaders(new ArrayList());
		r2.setSelect("false");
		r2.setNew("false");
		r2.setCategories(new ArrayList());
		restList2.add(r2);
		RuleDiscount rd2 = new RuleDiscount();
		rd2.setType("Flat");
		rd2.setDiscountLevel("Restaurant");
		rd2.setFlatDiscountAmount(300);
		rd2.setMinCartAmount(0);

		RMSCreateTDEntry entry2 = new RMSTDBuilder().setEnabled("true").setRestaurantIds(restIds2)
				.setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli()))
				.setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli()))
				.setCampaignType("Flat").setRuleDiscountType("Flat").setCreatedBy(9886379321l)
				.setDiscountLevel("Restaurant").setRestaurantList(restList2).setSlots(new ArrayList<>())
				.setFirstOrderRestriction("false").setUserRestriction("false").setRestaurantFirstOrder("false")
				.setRuleDiscount(rd2).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false")
				.setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		HashMap<String, Object> orderInfo = new HashMap<>();
		orderInfo.put("orderReponse", null);
		orderInfo.put("orderId", null);
		myFetchOrderData.put("orderData", orderInfo);
		myFetchOrderData.put("orderData", orderInfo);
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("omsSessionData",
				RMSCommonHelper.getOMSSessionData(RMSConstants.OMS_UAT_USERNAME, RMSConstants.OMS_UAT_PASSWORD));
		myFetchOrderData.put("rmsSessionData",
				RMSCommonHelper.getRMSSessionData(RMSConstants.VENDOR_UAT_USERNAME, RMSConstants.VENDOR_UAT_PASSWORD));
		return new Object[][] { { myFetchOrderData, jsonHelper.getObjectToJSON(entry2) } };
	}

	@DataProvider(name = "verifyFetchOrderWithFlatTDLessthanMinAmount")
	public static Object[][] verifyFetchOrderWithFlatTDLessthanMinAmount() throws Exception {
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		String restaurantId = "9990";
		JsonHelper jsonHelper = new JsonHelper();

		ArrayList<Integer> restIds2 = new ArrayList<>();
		restIds2.add(9990);
		ArrayList<Restaurant> restList2 = new ArrayList<>();
		Restaurant r2 = new Restaurant();
		r2.setId(Integer.parseInt(restaurantId));
		r2.setName("Myramzi2");
		r2.setCity(219);
		r2.setArea("MyRamziArea2");
		r2.setRecommended(0);
		r2.setCostForTwo(0);
		r2.setDeliveryCharge(0);
		r2.setMinimumOrder(0);
		r2.setOpened(0);
		r2.setDeliveryTime(0);
		r2.setTmpClosed(0);
		r2.setPostalCode(0);
		r2.setCityDeliveryCharge(0);
		r2.setThreshold(0);
		r2.setDiscountType(0);
		r2.setTradeCampaignHeaders(new ArrayList());
		r2.setSelect("false");
		r2.setNew("false");
		r2.setCategories(new ArrayList());
		restList2.add(r2);
		RuleDiscount rd2 = new RuleDiscount();
		rd2.setType("Flat");
		rd2.setDiscountLevel("Restaurant");
		rd2.setFlatDiscountAmount(300);
		rd2.setMinCartAmount(0);

		RMSCreateTDEntry entry2 = new RMSTDBuilder().setEnabled("true").setRestaurantIds(restIds2)
				.setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli()))
				.setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli()))
				.setCampaignType("Flat").setRuleDiscountType("Flat").setCreatedBy(9886379321l)
				.setDiscountLevel("Restaurant").setRestaurantList(restList2).setSlots(new ArrayList<>())
				.setFirstOrderRestriction("false").setUserRestriction("false").setRestaurantFirstOrder("false")
				.setRuleDiscount(rd2).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false")
				.setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		HashMap<String, Object> orderInfo = new HashMap<>();
		orderInfo.put("orderReponse", null);
		orderInfo.put("orderId", null);
		myFetchOrderData.put("orderData", orderInfo);
		myFetchOrderData.put("orderData", orderInfo);
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("omsSessionData",
				RMSCommonHelper.getOMSSessionData(RMSConstants.OMS_UAT_USERNAME, RMSConstants.OMS_UAT_PASSWORD));
		myFetchOrderData.put("rmsSessionData",
				RMSCommonHelper.getRMSSessionData(RMSConstants.VENDOR_UAT_USERNAME, RMSConstants.VENDOR_UAT_PASSWORD));
		return new Object[][] { { myFetchOrderData, jsonHelper.getObjectToJSON(entry2) } };
	}

	@DataProvider(name = "verifyFetchOrderWithFlatTDMorethanMinAmount")
	public static Object[][] verifyFetchOrderWithFlatTDMorethanMinAmount() throws Exception {
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		String restaurantId = "9990";
		JsonHelper jsonHelper = new JsonHelper();

		ArrayList<Integer> restIds2 = new ArrayList<>();
		restIds2.add(9990);
		ArrayList<Restaurant> restList2 = new ArrayList<>();
		Restaurant r2 = new Restaurant();
		r2.setId(Integer.parseInt(restaurantId));
		r2.setName("Myramzi2");
		r2.setCity(219);
		r2.setArea("MyRamziArea2");
		r2.setRecommended(0);
		r2.setCostForTwo(0);
		r2.setDeliveryCharge(0);
		r2.setMinimumOrder(0);
		r2.setOpened(0);
		r2.setDeliveryTime(0);
		r2.setTmpClosed(0);
		r2.setPostalCode(0);
		r2.setCityDeliveryCharge(0);
		r2.setThreshold(0);
		r2.setDiscountType(0);
		r2.setTradeCampaignHeaders(new ArrayList());
		r2.setSelect("false");
		r2.setNew("false");
		r2.setCategories(new ArrayList());
		restList2.add(r2);
		RuleDiscount rd2 = new RuleDiscount();
		rd2.setType("Flat");
		rd2.setDiscountLevel("Restaurant");
		rd2.setFlatDiscountAmount(300);
		rd2.setMinCartAmount(0);

		RMSCreateTDEntry entry2 = new RMSTDBuilder().setEnabled("true").setRestaurantIds(restIds2)
				.setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli()))
				.setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli()))
				.setCampaignType("Flat").setRuleDiscountType("Flat").setCreatedBy(9886379321l)
				.setDiscountLevel("Restaurant").setRestaurantList(restList2).setSlots(new ArrayList<>())
				.setFirstOrderRestriction("false").setUserRestriction("false").setRestaurantFirstOrder("false")
				.setRuleDiscount(rd2).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false")
				.setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		HashMap<String, Object> orderInfo = new HashMap<>();
		orderInfo.put("orderReponse", null);
		orderInfo.put("orderId", null);
		myFetchOrderData.put("orderData", orderInfo);
		myFetchOrderData.put("orderData", orderInfo);
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("omsSessionData",
				RMSCommonHelper.getOMSSessionData(RMSConstants.OMS_UAT_USERNAME, RMSConstants.OMS_UAT_PASSWORD));
		myFetchOrderData.put("rmsSessionData",
				RMSCommonHelper.getRMSSessionData(RMSConstants.VENDOR_UAT_USERNAME, RMSConstants.VENDOR_UAT_PASSWORD));
		return new Object[][] { { myFetchOrderData, jsonHelper.getObjectToJSON(entry2) } };
	}

}
