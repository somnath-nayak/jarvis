package com.swiggy.api.erp.vms.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import org.codehaus.jackson.JsonNode;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import com.swiggy.automation.common.utils.APIUtils;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import java.text.DateFormat;
import java.time.Instant;


public class RMSCommonHelper {
	String sessionId = null;
	String token = null;
	String tid = null;
	Long serviceableAddress = null;
	static String env = null;

	static Initialize gameofthrones = new Initialize();
	SnDHelper sndHelper = new SnDHelper();
	CMSHelper cmsHelper = new CMSHelper();
	SANDHelper sandHelper = new SANDHelper();
	static JsonHelper jsonHelper = new JsonHelper();


	static {
		gameofthrones = new Initialize();
	}

	public static HashMap<String, String> getOMSSessionData(String userName, String password) {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> obj = omsHelper.getSession(userName, password);
		String cookie = "csrftoken=" + obj.get("csrfToken") + "; sessionid=" + obj.get("sessionId");
		HashMap<String, String> sessionData = new HashMap<String, String>();
		sessionData.put("X-CSRFToken", obj.get("csrfToken"));
		sessionData.put("Cookie", cookie);
		sessionData.put("csrftoken", obj.get("csrfToken"));
		sessionData.put("sessionid", obj.get("sessionId"));
		return sessionData;
	}

	public static HashMap<String, String> getRMSSessionData(String userName, String password) {
		HashMap<String, String> hs = new HashMap<>();
		RMSHelper rhelper = new RMSHelper();
		hs.put("accessToken", rhelper.getLoginToken(userName, password));
		return hs;
	}

	public static String getRMSAccessToken(String userName, String password) {
		RMSHelper rhelper = new RMSHelper();
		return rhelper.getLoginToken(userName, password);
	}

	public static boolean isOrderAvailableAtFF(String orderId) throws Exception {
		OMSHelper omsHellper = new OMSHelper();
		int counter = 0;
		boolean status = false;
		do {
			String orderInfo = omsHellper.getOrderInfoFromOMS(orderId);
			JsonNode actualData = APIUtils.convertStringtoJSON(orderInfo);
			System.out.println("OMS QUERY RESPONSE :" + actualData);
			System.out.println("OMS QUERY RESPONSE OBJECT SIZE:" + actualData.get("objects").size());
			if (actualData.get("objects").size() < 1) {
				System.out.println("Order Not appear on the dashboard");
				counter++;
				status = false;
			} else {
				System.out.println("Order appear on the OMS dashboard...So proceeding");
				status = true;
				break;

			}
			Thread.sleep(3000);
		} while (counter < 20);
		return status;
	}

	public static boolean isOrderAvailableAtFF(String orderId, String user, String pass) throws Exception {
		OMSHelper omsHellper = new OMSHelper();
		int counter = 0;
		boolean status = false;
		do {
			String orderInfo = omsHellper.getOrderInfoFromOMS(orderId, user, pass);
			JsonNode actualData = APIUtils.convertStringtoJSON(orderInfo);
			System.out.println("OMS QUERY RESPONSE :" + actualData);
			System.out.println("OMS QUERY RESPONSE OBJECT SIZE:" + actualData.get("objects").size());
			if (actualData.get("objects").size() < 1) {
				System.out.println("Order Not appear on the dashboard");
				counter++;
				status = false;
			} else {
				System.out.println("Order appear on the OMS dashboard...So proceeding");
				status = true;
				break;

			}
			Thread.sleep(3000);
		} while (counter < 20);
		return status;
	}

	public static String getCurrentDateTimeInRequiredFormat(Date date, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date);
	}

	public static String getMeObjectOrderObjectFromList(String orderId, long restaurantId, JsonNode nodeObject) {

		System.out.println(nodeObject);
		int restaurantDataSize = nodeObject.get("restaurantData").size();
		if (restaurantDataSize == 0) {
			return null;
		}
		for (int i = 0; i < restaurantDataSize; i++) {
			if (nodeObject.get("restaurantData").get(i).get("restaurantId").asLong() == restaurantId) {
				int ordersSize = nodeObject.get("restaurantData").get(i).get("orders").size();
				if (ordersSize == 0) {
					return null;
				}
				for (int j = 0; i < ordersSize; j++) {

					if (nodeObject.get("restaurantData").get(i).get("orders").get(j).get("order_id").asText()
							.equals(orderId)) {
						return nodeObject.get("restaurantData").get(i).get("orders").get(j).toString();
					}
				}
			}

		}
		return null;
	}

	public static String[] createOrder(String restaurantId, long consumerAppMobile, String consumerAppPassword,
			String paymentMode, String comments) throws Exception {

		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		items.add(new Cart(null, null, String.valueOf(itemList.get(new Random().nextInt(itemList.size() / 2))), 1));
		return RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng, items, restaurantId,
				paymentMode, comments);
	}

	public static HashMap<String, Object> createOrder(String restaurantId, long consumerAppMobile,
			String consumerAppPassword, String paymentMode, String comments, int numberOfOrders) throws Exception {

		List<Cart> items = new ArrayList<>();
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		items.add(new Cart(null, null, String.valueOf(itemList.get(new Random().nextInt(itemList.size() / 2))), 1));
		myFetchOrderData.put("orderReponse", RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng,
				items, restaurantId, paymentMode, comments, numberOfOrders));
		return myFetchOrderData;
	}

	public static HashMap<String, String> createOrder(long mobileNumber, String password, String userLatLang,
			List<Cart> items, String restaurantId, String paymentMethod, String OrderComments, int numberOfOrders) {
		CreateMenuEntry orderData = null;
		HashMap<String, String> hm = new HashMap<>();
		if (numberOfOrders < 1) {
			numberOfOrders = 1;
		}
		for (int i = 0; i < numberOfOrders; i++) {
			String orderReponse = null;
			String orderId = null;
			try {

				orderData = new CreateOrderBuilder().mobile(mobileNumber).password(password).cart(items)
						.restaurant(Integer.parseInt(restaurantId)).Address(12345).paymentMethod(paymentMethod)
						.orderComments(OrderComments).buildAll();
				orderReponse = createOrderData(orderData, userLatLang);
				if (orderReponse.equals(null)) {
					throw new Exception("Exception in order Creation Please Check");
				}
				orderId = JsonPath.read(orderReponse, "$.data.order_id").toString().replace("[", "").replace("]", "");
				hm.put(orderId, orderReponse);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return hm;

	}

	public static String[] createOrder(long mobileNumber, String password, String userLatLang, List<Cart> items,
			String restaurantId, String paymentMethod, String OrderComments) {
		CreateMenuEntry orderData = null;
		String orderReponse = null;
		String[] str = new String[2];

		try {

			orderData = new CreateOrderBuilder().mobile(mobileNumber).password(password).cart(items)
					.restaurant(Integer.parseInt(restaurantId)).Address(12345).paymentMethod(paymentMethod)
					.orderComments(OrderComments).buildAll();
			orderReponse = createOrderData(orderData, userLatLang);
			if (orderReponse.equals(null)) {
				throw new Exception("Exception in order Creation Please Check");
			}
			str[0] = JsonPath.read(orderReponse, "$.data.order_id").toString().replace("[", "").replace("]", "");
			str[1] = orderReponse;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	public static ArrayList<Long> getInStockItemsFromMenu(Processor response) throws Exception {
		return getInStockItems(response.ResponseValidator.GetBodyAsText());
	}

	public static String getRestLatLngFromMenu(Processor response) throws Exception {
		return response.ResponseValidator.GetNodeValue("data.latLong");
	}

	private static ArrayList<Long> getInStockItems(String response) throws Exception {
		JsonNode nodeObject = APIUtils.convertStringtoJSON(response);
		ArrayList<Long> list = new ArrayList<Long>();
		int itemsize = nodeObject.get("data").get("menu").get("items").size();
		if (itemsize == 0) {

			return null;
		}
		JsonNode ndeObj = APIUtils.convertStringtoJSON(nodeObject.get("data").get("menu").get("items").toString());
		Map.Entry<String, JsonNode> field = null;
		Iterator<Map.Entry<String, JsonNode>> fieldsIterator = ndeObj.getFields();
		while (fieldsIterator.hasNext()) {
			field = fieldsIterator.next();
			try {
				if (field.getValue().get("inStock").asInt() == 1 && field.getValue().get("variants_new").size() == 0) {
					list.add(field.getValue().get("id").asLong());
				}
			} catch (Exception e) {
				e.printStackTrace();// TODO: handle exception
			}

		}
		return list;
	}

	public static List<Integer> getItemListFromFetchItems(Processor p) {
		return (List<Integer>) (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
				"$.data.categories..subCategories..menu..id"));
	}

	public static HashMap<String, String> getItemListFromFetchItemsWithVariants(Processor p) throws Exception {
		HashMap<String, String> itemIdVariantIdGroupId = new HashMap<>();
		List<Integer> itemList = getItemListFromFetchItems(p);
		if(itemList.size() == 0) {
			System.out.println("itemId list is empty");
			return null;
		}
		for(int i=0; i<itemList.size(); i++) {
			List<String> variantGroupIdList = (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
					"$.data.categories..subCategories..menu[?(@.id == " + itemList.get(i)
							+ ")].variants_new.variant_groups..group_id"));
			if(variantGroupIdList.size() != 0) {
				for(int j=0; j<variantGroupIdList.size(); j++) {
					List<Integer> variantIdList = (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
							"$.data.categories..subCategories..menu..variants_new.variant_groups[?(@.group_id == " + variantGroupIdList.get(j)
									+ ")].variations..id"));
					if(variantIdList.size() != 0) {

						itemIdVariantIdGroupId.put("itemId", itemList.get(i).toString());
						itemIdVariantIdGroupId.put("variantGroupId", variantGroupIdList.get(j));
						itemIdVariantIdGroupId.put("variantId", variantIdList.get(0).toString());

						System.out.println("itemidddddddddd" + itemIdVariantIdGroupId.get("itemId"));
						System.out.println("variantGroupIdddddddddd" + itemIdVariantIdGroupId.get("variantGroupId"));
						System.out.println("variantIdddddddddd" + itemIdVariantIdGroupId.get("variantId"));

						return itemIdVariantIdGroupId;
					}
				}
			}
		}
		return null;
	}

	public static HashMap<String, String> getInStockVariantFromFetchItems(Processor p) throws Exception {
		HashMap<String, String> itemIdVariantIdGroupId = new HashMap<>();
		List<Integer> itemList = getItemListFromFetchItems(p);
		if(itemList.size() == 0) {
			System.out.println("itemId list is empty");
			return null;
		}
		for(int i=0; i<itemList.size(); i++) {
			List<String> variantGroupIdList = (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
					"$.data.categories..subCategories..menu[?(@.id == " + itemList.get(i)
							+ ")].variants_new.variant_groups..group_id"));
			if(variantGroupIdList.size() != 0) {
				for(int j=0; j<variantGroupIdList.size(); j++) {
					List<Integer> variantIdList = (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
							"$.data.categories..subCategories..menu..variants_new.variant_groups[?(@.group_id == " + variantGroupIdList.get(j)
									+ ")].variations[?(@.inStock == 1)].id"));
					if(variantIdList.size() != 0) {

						itemIdVariantIdGroupId.put("itemId", itemList.get(i).toString());
						itemIdVariantIdGroupId.put("variantGroupId", variantGroupIdList.get(j));
						itemIdVariantIdGroupId.put("variantId", variantIdList.get(0).toString());

						System.out.println("itemidddddddddd" + itemIdVariantIdGroupId.get("itemId"));
						System.out.println("variantGroupIdddddddddd" + itemIdVariantIdGroupId.get("variantGroupId"));
						System.out.println("variantIdddddddddd" + itemIdVariantIdGroupId.get("variantId"));

						return itemIdVariantIdGroupId;
					}
				}
			}
		}
		return null;
	}

	public static HashMap<String, List<Integer>> getInStockCategoryFromFetchItems(Processor p) throws Exception {

		HashMap<String, List<Integer>> categoryItemVariantList = new HashMap<>();
		List<Integer> catList = new ArrayList<>();
		List<Integer> subCatList = null;
		List<Integer> itemList = null;
		List<Integer> variantList = null;
		List<Integer> categoryList = JsonPath.read(p.ResponseValidator.GetBodyAsText(),
				"$.data.categories[?(@.inStock == 1)].id");
		if(categoryList.size()==0) {
			System.out.println("Category list is empty or no category is in stock");
			return null;
		}

		for(int i=0; i<categoryList.size(); i++) {

			subCatList = JsonPath.read(p.ResponseValidator.GetBodyAsText(),
					"$.data.categories[?(@.id == " + categoryList.get(i) + ")].subCategories.*.id");
			itemList = JsonPath.read(p.ResponseValidator.GetBodyAsText(),
					"$.data.categories[?(@.id == " + categoryList.get(i) + ")].subCategories..menu.*.id");
			variantList = JsonPath.read(p.ResponseValidator.GetBodyAsText(),
					"$.data.categories[?(@.id == " + categoryList.get(i) + ")].subCategories..menu..variants_new.variant_groups..variations..id");

			if(itemList.size()!=0) {
				catList.add(categoryList.get(i));
				break;
			}
		}

		categoryItemVariantList.put("categoryList", catList);
		System.out.println("CategoryListtttt" + categoryItemVariantList.get("categoryList"));
		if(subCatList.size()!=0) {
			categoryItemVariantList.put("subCategoryList", subCatList);
			System.out.println("subCategoryListtttt" + categoryItemVariantList.get("subCategoryList"));
		}
		categoryItemVariantList.put("itemList", itemList);
		System.out.println("itemListttttt" + categoryItemVariantList.get("itemList"));
		if(variantList.size()!=0) {
			categoryItemVariantList.put("variantList", variantList);
			System.out.println("variantListttttt" + categoryItemVariantList.get("variantList"));
		}

		return categoryItemVariantList;

	}

	public static HashMap<String, String> getOOSVariantFromFetchItems(Processor p) throws Exception {
		HashMap<String, String> itemIdVariantIdGroupId = new HashMap<>();
		List<Integer> itemList = getItemListFromFetchItems(p);
		if(itemList.size() == 0) {
			System.out.println("itemId list is empty");
			return null;
		}
		for(int i=0; i<itemList.size(); i++) {
			List<String> variantGroupIdList = (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
					"$.data.categories..subCategories..menu[?(@.id == " + itemList.get(i)
							+ ")].variants_new.variant_groups..group_id"));
			if(variantGroupIdList.size() != 0) {
				for(int j=0; j<variantGroupIdList.size(); j++) {
					List<Integer> variantIdList = (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
							"$.data.categories..subCategories..menu..variants_new.variant_groups[?(@.group_id == " + variantGroupIdList.get(j)
									+ ")].variations[?(@.inStock == 0)].id"));
					if(variantIdList.size() != 0) {

						itemIdVariantIdGroupId.put("itemId", itemList.get(i).toString());
						itemIdVariantIdGroupId.put("variantGroupId", variantGroupIdList.get(j));
						itemIdVariantIdGroupId.put("variantId", variantIdList.get(0).toString());

						System.out.println("itemidddddddddd" + itemIdVariantIdGroupId.get("itemId"));
						System.out.println("variantGroupIdddddddddd" + itemIdVariantIdGroupId.get("variantGroupId"));
						System.out.println("variantIdddddddddd" + itemIdVariantIdGroupId.get("variantId"));

						return itemIdVariantIdGroupId;
					}
				}
			}
		}
		return null;
	}

	public static void getInStockItemsWithAddons() {

	}

	public static void getInStockItemsWithVariants() {

	}

	public static void getInStockItemsWithAddonsNVariants() {

	}

	private static String createOrderData(CreateMenuEntry payload, String userLatLng) throws Exception {

		String cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		CheckoutHelper chkHelper = new CheckoutHelper();
		HashMap<String, String> hashMap = chkHelper.createLogin(payload.getpassword(), payload.getmobile());
		String response = chkHelper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload,
				payload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		String AddressId = JsonPath.read(response, "$.data.addresses..id").toString().replace("[", "").replace("]", "")
				.replace("\"", "");
		String[] AddressIdArray = (AddressId.split(","));
		String deliveryValid = JsonPath.read(response, "$.data..addresses..delivery_valid").toString().replace("[", "")
				.replace("]", "");
		String[] delivery = deliveryValid.split(",");
		List<String> addressIdList = new ArrayList<>();
		for (int i = 0; i < delivery.length; i++) {
			if (delivery[i].equals("1")) {
				addressIdList.add(AddressIdArray[i]);
			}
		}
		if (addressIdList.isEmpty()) {
			// Processor response1 =
			// SnDHelper.menuV3RestId(String.valueOf(payload.getRestaurantId()));
			// String latlng = getRestLatLngFromMenu(response1);
			Processor p = CheckoutHelper.addNewAddress(newAddressObject(userLatLng), hashMap.get("Tid"),
					hashMap.get("Token"));
			if (p.ResponseValidator.GetNodeValueAsInt("data.delivery_valid") == 1) {
				addressIdList.add(String.valueOf(p.ResponseValidator.GetNodeValueAsInt("data.address_id")));
			} else {
				throw new Exception(
						"=====>Unable to Find Serviceable Address..Even After Trying AddingNewAddress On RestLocation=====!!!!!");
			}

		}
		return new CheckoutHelper().orderPlace(hashMap.get("Tid"), hashMap.get("Token"),
				Integer.parseInt(addressIdList.get(0)), payload.getpayment_cod_method(),
				payload.getorder_comments()).ResponseValidator.GetBodyAsText();
	}

	// private static Processor orderPlace1(String tid, String token, Integer
	// addressId, String payment, String comments) {
	// HashMap<String, String> requestheaders = requestHeader(tid, token);
	// GameOfThronesService service = new GameOfThronesService("checkout",
	// "orderplacev1", gameofthrones);
	// String[] payloadparams = { String.valueOf(addressId), payment, comments };
	// Processor processor = new Processor(service, requestheaders, payloadparams);
	// return processor;
	// }

	// private static HashMap<String, String> requestHeader1(String tid, String
	// token) {
	// HashMap<String, String> requestheaders = new HashMap<String, String>();
	// requestheaders.put("content-type", "application/json");
	// requestheaders.put("tid", tid);
	// requestheaders.put("token", token);
	// return requestheaders;
	// }

	public static Processor menuV3RestId(String restID) {
		Initialize initilizer = new Initialize();
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("checkout", "menuv3RestId", initilizer);
		Processor processor = new Processor(service, requestHeader, null, new String[] { restID });
		return processor;
	}

	// private static Processor CreateCartAddon1(String tid, String token, String
	// cart, Integer rest) {
	// HashMap<String, String> requestheaders = requestHeader(tid, token);
	// GameOfThronesService service = new GameOfThronesService("checkout",
	// "createcartv2nitems", gameofthrones);
	// String[] payloadparams = { cart, String.valueOf(rest) };
	// Processor processor = new Processor(service, requestheaders, payloadparams);
	// return processor;
	// }

	private static HashMap<String, String> newAddressObject(String latlng) {
		HashMap<String, String> hMap = new HashMap<String, String>();
		hMap.put("name", "RamziMohammed");
		hMap.put("mobile", "1010101010");
		hMap.put("address", "199, Near Sai Baba Temple, 7th Block, Koramangala");
		hMap.put("landmark", "Some office Land Mark");
		hMap.put("area", "Swiggy Serviceable Area");
		hMap.put("lat", latlng.split(",")[0]);
		hMap.put("lng", latlng.split(",")[1]);
		hMap.put("flat_no", "4321");
		hMap.put("city", "Hyderabad");
		hMap.put("annotation", "HOME");

		return hMap;
	}

	public static String getEnv() throws Exception {

		String env = gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getName();
		System.out.println("Envi :" + env);
		return env;
	}

	public static String generatePhoneNo() {

		Random r = new Random();
		String phoneNo = "8000" + (100000 + r.nextInt(900000));
		System.out.println("Generated Phone Number :" + phoneNo);
		return phoneNo;
	}

	public static String incrementPhoneNo(String mob_no) {
		long a = Long.parseLong(mob_no);
		return String.valueOf(++a);
	}

	public static String createOrder(String user, String pass) throws Exception {

		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String order_id = order_idFormat.format(date);
		String order_time = dateFormat.format(date);
		System.out.println("Order time = " + order_time);
		String epochTime = String.valueOf(Instant.now().getEpochSecond());
		System.out.println("Epoch time = " + epochTime);

		LOSHelper losHelper = new LOSHelper();
		OMSHelper omsHelper = new OMSHelper();
		losHelper.createOrderWithPartner(order_id, order_time, epochTime);

		if (RMSCommonHelper.isOrderAvailableAtFF(order_id, user, pass)) {
			boolean flg = omsHelper.verifyOrders(order_id, "010", user, pass);
		} else {
			throw new Exception("Order Not Reached FF...So DownStream Dependency Failed For FF");
		}

		return order_id;
	}

	public static boolean assignDE(String orderId) throws Exception{

		DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
		String zone_id=SystemConfigProvider.getTemplate("deliverydb").queryForMap("select zone_id from area where id = 149").get("zone_id").toString();
		String de_id=deliveryDataHelper.CreateDE(Integer.parseInt(zone_id));
		deliveryDataHelper.updateDELocationInRedis(de_id);
		boolean assignStatus = deliveryDataHelper.doAssignment(orderId, de_id);

		return assignStatus;
	}

	public static boolean assignDE(String orderId, String de_id) {

		DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
		DeliveryServiceHelper deHelper = new DeliveryServiceHelper();
		deHelper.makeDEFree(de_id);
		boolean assignStatus = deliveryDataHelper.doAssignment(orderId, de_id);
		return assignStatus;
	}

}
