package com.swiggy.api.erp.cms.pojo;

import com.swiggy.api.erp.cms.constants.Constants;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class SlotBuilder {
    private HolidaySlot holidaySlot;
    String id;

    public SlotBuilder()
    {
        holidaySlot=new HolidaySlot();
    }

    public SlotBuilder restaurantId(Integer restaurant) {
        holidaySlot.setRestaurantId(restaurant);
        return this;
    }

    public SlotBuilder fromTime(String fromTime) {
        holidaySlot.setFromTime(fromTime);
        return this;
    }

    public SlotBuilder toTime(String toTime) {
        holidaySlot.setToTime(toTime);
        return this;
    }

    private void defaultHolidaySlot() throws IOException {
        if(holidaySlot.getRestaurantId()==null){
            holidaySlot.setRestaurantId(273);
        }
        if(holidaySlot.getFromTime()==null){
            holidaySlot.setFromTime(dateTime());
        }
        if(holidaySlot.getToTime()==null){
            holidaySlot.setToTime(extendedTime());
        }
    }

    public HolidaySlot buildHolidaySlot() throws IOException {
        defaultHolidaySlot();
        return holidaySlot;
    }

    public String dateTime()
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        String formatTime = new SimpleDateFormat("HH:mm:ss").format(date);
        String DT=modifiedDate+"T"+formatTime;
        return DT;
    }

    public String extendedTime()
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        LocalDateTime d = LocalDateTime.now();
        String dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss").format(d.plusMinutes(10));
        String DT=modifiedDate+"T"+dateFormat;
        return DT;
    }

    public SlotBuilder Data(ItemData data)
    {
        holidaySlot.setData(data);
        return this;
    }

    public SlotBuilder userMeta(ItemUserMeta userMeta)
    {
        holidaySlot.setUserMeta(userMeta);
        return this;
    }

    private void defaultItemSlot() throws IOException {
        if(holidaySlot.getData()==null){
            holidaySlot.setData(new ItemData(Constants.defaultItem,Constants.noOfWeek,Constants.defaultOpenTime,Constants.defaultCloseTime));
        }
        if(holidaySlot.getUserMeta()==null){
            holidaySlot.setUserMeta(new ItemUserMeta(Constants.defaultSource, new ItemMeta(Constants.defaultUser)));
        }
    }

    public HolidaySlot buildItemSlot() throws IOException {
        defaultItemSlot();
        return holidaySlot;
    }



   /* public SlotBuilder Data1(Data1 data1)
    {
        holidaySlot.setData1(data1);
        return this;
    }

    private void defaultItemHolidaySlot() throws IOException {
        if(holidaySlot.getData1()==null){
            holidaySlot.setData1(new Data1(228943,String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000),String.valueOf(DateUtils.addHours(new Date(), 10).toInstant().getEpochSecond() * 1000)));
        }
        if(holidaySlot.getUserMeta()==null){
            holidaySlot.setUserMeta(new UserMeta("cms", new Meta("cms-tester")));
        }
    }*/

    /*public HolidaySlot buildItemHolidaySlot() throws IOException {
        defaultItemHolidaySlot();
        return holidaySlot;
    }*/

}
