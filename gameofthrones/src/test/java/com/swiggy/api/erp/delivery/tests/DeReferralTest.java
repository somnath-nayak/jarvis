package com.swiggy.api.erp.delivery.tests;


import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.DeliveryReferralDataProvider;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryReferalHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;

import framework.gameofthrones.JonSnow.Processor;

public class DeReferralTest extends DeliveryReferralDataProvider {
		

	DeliveryServiceHelper helpdel;
	DeliveryHelperMethods delmeth;
	DeliveryReferalHelper DRhelper;
		
		@Test(dataProvider = "Referral1", enabled = true, groups = {"refaralRegression"}, description = "Verify DE is able to refer candidate", priority=0)
		public void createReferral(String name, String mobile, String city_id,String ref_type,String ref_id,String auth) throws InterruptedException {
			System.out.println("*****************************Referral Regression starts here*******************************");
			this.DRhelper=new DeliveryReferalHelper(auth);
			Processor referral=DRhelper.createReferal(name, mobile, city_id, ref_type, ref_id);
			JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("referralId").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    if(eligibilitySt!=null)
		    	{
		    		System.out.println("Candidate referred successfully");
		    		Assert.assertTrue(true);
		    	}
		    else
		       {
		    	System.out.println("Candidate is not eligible to refer");
		       }
		}

		@Test(dataProvider = "Referral1", enabled = true, groups = {"refaralRegression"}, description = "Verify referred candidate is not able to refer again",priority=1)
		public void createReferralNegative(String name, String mobile, String city_id,String ref_type,String ref_id,String auth) throws InterruptedException {
			Thread.sleep(3000);
			this.DRhelper=new DeliveryReferalHelper(auth);
			Processor referral=DRhelper.createReferal(name, DeliveryConstant.mobile2, city_id, ref_type, ref_id);
			JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("referralId").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    if(eligibilitySt!=null)
		    	{
		    		System.out.println("Candidate referred successfully");
		    		Assert.assertTrue(false);
		    	}
		    else
		    {
	    		System.out.println("Candidate is not eligible to refer");
	    		Assert.assertTrue(true);
		     }   

		}
		
		@Test(dataProvider = "Referral2", enabled = true, description = "Verify the referral status",priority=2)
		public void getReferralStatus(String mobileno, String auth) throws InterruptedException {
			this.DRhelper=new DeliveryReferalHelper(auth);
			Processor referral=DRhelper.getReferal("1");
			JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			String eligibilitySt2=null;
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("referralId").toString();
				 eligibilitySt2=eligibilityOb.getString("status").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    if(eligibilitySt.equalsIgnoreCase("1"))
				 {
			  switch(eligibilitySt2)
	             {
	             case "pending":
	            	 System.out.println("Referral status is pending");
	            	 Assert.assertTrue(true);
	            	 break;
	             case "Working with swiggy":
	            	 System.out.println("Refered candidate is already working with swiggy");
	            	 Assert.assertTrue(true);
	            	 break;
	             case "In-progress":
	            	 System.out.println("Referral status is in-progress");
	            	 Assert.assertTrue(true);
	            	 break;
	             default:
	            	 System.out.println("Unable to get the status");
	            	 Assert.assertFalse(false);	 
	            }
			}

		}
		
		@Test(dataProvider = "Referral3", enabled = true, groups = {"reffaralRegression"}, description = "Verify DE is able to get the referral list",priority=3)
		public void getReferralList1(String ref_id,String auth) throws InterruptedException {
			this.DRhelper=new DeliveryReferalHelper(auth);
			Processor referral=DRhelper.getReferalList(ref_id);
			JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("referralList").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    if(eligibilitySt!=null)
		    	{
		    		System.out.println("Able to get the list");
		    		Assert.assertTrue(true);
		    	}
		    else
		    {
	    		System.out.println("Unable to get the list");
	    		Assert.assertTrue(false);
		     }

		}
		
		
		@Test(dataProvider = "Referral2", enabled = true, groups = {"reffaralRegression"}, description = "Verify to check the number eligibility when number not present",priority=4)
		public void getLeadEligibility(String mobileno, String auth) throws InterruptedException {
			this.DRhelper=new DeliveryReferalHelper(auth);
			Processor referral=DRhelper.getLeadEligibility(mobileno);	JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("isEligible").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    if(eligibilitySt.equalsIgnoreCase("true"))
		    	{
		    		System.out.println("Candidate is eligible ");
		    		Assert.assertTrue(false);
		    	}
		    else
		    {
	    		System.out.println("Candidate is not eligible to refer");
	    		Assert.assertTrue(true);


		     }

		}
		
		@Test(dataProvider = "Referral2", enabled = true, groups = {"reffaralRegression"}, description = "Verify to check the number eligibility when number is already present",priority=5)
		public void getLeadEligibilityNegative(String mobileno, String auth) throws InterruptedException {
			this.DRhelper=new DeliveryReferalHelper(auth);
			Processor referral=DRhelper.getLeadEligibility("9886964553");	JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("isEligible").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		    if(eligibilitySt.equalsIgnoreCase("true"))
		    	{
		    		System.out.println("Candidate is eligible ");
		    		Assert.assertTrue(true);
		    	}
		    else
		    {
	    		System.out.println("Candidate is not eligible to refer");
	    		Assert.assertTrue(false);


		     }

		}
		    
		@Test(dataProvider = "Referral3", enabled = true, description = "Verify to get the referral config",priority=6)
		public void getReferralConfig(String de_id, String auth) throws InterruptedException {
			this.DRhelper=new DeliveryReferalHelper(auth);
			Processor referral=DRhelper.getReferalConfig(de_id);
			JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("referralMessage").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if(eligibilitySt.equalsIgnoreCase("Test_city1"))
			{
	    		Assert.assertTrue(true);
			}
			else
			{
	    		Assert.assertTrue(false);

			}
            System.out.println("************************************Referral Regression stops here***********************");

			}
	    

		
}
