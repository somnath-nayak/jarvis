package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

import com.swiggy.api.erp.cms.constants.SelfServeConstants;

public class SubmitTicket
{
	private Metadata metadata;

	private Tickets[] tickets;

	public Metadata getMetadata ()
	{
		return metadata;
	}

	public void setMetadata (Metadata metadata)
	{
		this.metadata = metadata;
	}

	public Tickets[] getTickets ()
	{
		return tickets;
	}

	public void setTickets (Tickets[] tickets)
	{
		this.tickets = tickets;
	}

	public SubmitTicket build() {
		setDefaultValues();
		return this;
	}

	private void setDefaultValues() {
		Metadata md=new Metadata();
		md.build();
		Tickets t=new Tickets();
		t.build();
	}
	@Override
	public String toString()
	{
		return "ClassPojo [metadata = "+metadata+", tickets = "+tickets+"]";
	}
}

